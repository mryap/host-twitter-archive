Grailbird.data.tweets_2017_09 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914162599704580097",
  "text" : "Have your bought something using a mobile devices (including tablets)?",
  "id" : 914162599704580097,
  "created_at" : "2017-09-30 16:18:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/914115777640452100\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/b8LD8Lwjd0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DK-XLt4WsAAF1yN.jpg",
      "id_str" : "914115476929818624",
      "id" : 914115476929818624,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK-XLt4WsAAF1yN.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 169,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 1137
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 1137
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 1137
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/b8LD8Lwjd0"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/iidPazELvE",
      "expanded_url" : "http:\/\/notebooks.azure.com",
      "display_url" : "notebooks.azure.com"
    } ]
  },
  "geo" : { },
  "id_str" : "914115777640452100",
  "text" : "I have no problem running this in Jupyter on my local machine but I got problem on https:\/\/t.co\/iidPazELvE https:\/\/t.co\/b8LD8Lwjd0",
  "id" : 914115777640452100,
  "created_at" : "2017-09-30 13:12:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/1meonm7rRN",
      "expanded_url" : "http:\/\/www.cso.ie\/px\/pxeirestat\/pssn\/doehlg\/Database\/DoEHLG\/Housing%20Statistics\/Housing%20Statistics.asp",
      "display_url" : "cso.ie\/px\/pxeirestat\/\u2026"
    }, {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/TNxm1DTNKJ",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/578fdd5fa74a28d278a3c4722ce21f3a",
      "display_url" : "gist.github.com\/mryap\/578fdd5f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "914099372044046336",
  "text" : "Need access to CSO data on housing in Ireland? https:\/\/t.co\/1meonm7rRN Use my R code https:\/\/t.co\/TNxm1DTNKJ",
  "id" : 914099372044046336,
  "created_at" : "2017-09-30 12:07:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Business Insider UK",
      "screen_name" : "BIUK",
      "indices" : [ 121, 126 ],
      "id_str" : "294176860",
      "id" : 294176860
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/5kl9c4VAYg",
      "expanded_url" : "http:\/\/read.bi\/2fQM5v5",
      "display_url" : "read.bi\/2fQM5v5"
    } ]
  },
  "geo" : { },
  "id_str" : "914061164761542656",
  "text" : "Norwegian has launched the world's longest low-cost flight to Singapore \u2014 for less than \u00A3150 https:\/\/t.co\/5kl9c4VAYg via @BIUK",
  "id" : 914061164761542656,
  "created_at" : "2017-09-30 09:35:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alison Cowzer",
      "screen_name" : "Alisoncowzer",
      "indices" : [ 3, 16 ],
      "id_str" : "239863549",
      "id" : 239863549
    }, {
      "name" : "PeterMurtagh",
      "screen_name" : "PeterMurtagh",
      "indices" : [ 104, 117 ],
      "id_str" : "18675985",
      "id" : 18675985
    }, {
      "name" : "Fintan O'Toole",
      "screen_name" : "fotoole",
      "indices" : [ 118, 126 ],
      "id_str" : "17340546",
      "id" : 17340546
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MoreMen",
      "indices" : [ 30, 38 ]
    }, {
      "text" : "March4Choice",
      "indices" : [ 46, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914056403916115970",
  "text" : "RT @Alisoncowzer: Hope to see #MoreMen on the #March4Choice today. Choice is not just a 'women's' issue @PeterMurtagh @fotoole https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "PeterMurtagh",
        "screen_name" : "PeterMurtagh",
        "indices" : [ 86, 99 ],
        "id_str" : "18675985",
        "id" : 18675985
      }, {
        "name" : "Fintan O'Toole",
        "screen_name" : "fotoole",
        "indices" : [ 100, 108 ],
        "id_str" : "17340546",
        "id" : 17340546
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "MoreMen",
        "indices" : [ 12, 20 ]
      }, {
        "text" : "March4Choice",
        "indices" : [ 28, 41 ]
      } ],
      "urls" : [ {
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/aVURhwzCGx",
        "expanded_url" : "https:\/\/twitter.com\/itwomenspodcast\/status\/914044793952628736",
        "display_url" : "twitter.com\/itwomenspodcas\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "914048292497117184",
    "text" : "Hope to see #MoreMen on the #March4Choice today. Choice is not just a 'women's' issue @PeterMurtagh @fotoole https:\/\/t.co\/aVURhwzCGx",
    "id" : 914048292497117184,
    "created_at" : "2017-09-30 08:44:29 +0000",
    "user" : {
      "name" : "Alison Cowzer",
      "screen_name" : "Alisoncowzer",
      "protected" : false,
      "id_str" : "239863549",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/411930798573371392\/7brMkOak_normal.jpeg",
      "id" : 239863549,
      "verified" : false
    }
  },
  "id" : 914056403916115970,
  "created_at" : "2017-09-30 09:16:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kimberly Boswell",
      "screen_name" : "KSBoswell",
      "indices" : [ 3, 13 ],
      "id_str" : "1434619693",
      "id" : 1434619693
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/KSBoswell\/status\/913444594641141760\/photo\/1",
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/38ejxdUANN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DK01AxEXkAAHZAF.jpg",
      "id_str" : "913444586714009600",
      "id" : 913444586714009600,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK01AxEXkAAHZAF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/38ejxdUANN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914046606235881472",
  "text" : "RT @KSBoswell: so two of my classmates just asked  our professor if his shirt is missing a 2nd part. https:\/\/t.co\/38ejxdUANN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KSBoswell\/status\/913444594641141760\/photo\/1",
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/38ejxdUANN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DK01AxEXkAAHZAF.jpg",
        "id_str" : "913444586714009600",
        "id" : 913444586714009600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK01AxEXkAAHZAF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/38ejxdUANN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "913444594641141760",
    "text" : "so two of my classmates just asked  our professor if his shirt is missing a 2nd part. https:\/\/t.co\/38ejxdUANN",
    "id" : 913444594641141760,
    "created_at" : "2017-09-28 16:45:36 +0000",
    "user" : {
      "name" : "Kimberly Boswell",
      "screen_name" : "KSBoswell",
      "protected" : false,
      "id_str" : "1434619693",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1015386117837918214\/Bs_8TiGR_normal.jpg",
      "id" : 1434619693,
      "verified" : false
    }
  },
  "id" : 914046606235881472,
  "created_at" : "2017-09-30 08:37:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "March4Choice",
      "indices" : [ 35, 48 ]
    }, {
      "text" : "repealthe8th",
      "indices" : [ 56, 69 ]
    }, {
      "text" : "ARCMarch17",
      "indices" : [ 70, 81 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914043025285926912",
  "text" : "Can't be there but I am supporting #March4Choice today  #repealthe8th #ARCMarch17",
  "id" : 914043025285926912,
  "created_at" : "2017-09-30 08:23:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Coconuts",
      "screen_name" : "coconutsdotco",
      "indices" : [ 85, 99 ],
      "id_str" : "374591173",
      "id" : 374591173
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/BCoW4lIL8F",
      "expanded_url" : "https:\/\/coconuts.co\/singapore\/news\/american-man-wages-twitter-war-straits-times-usage-british-english-murica\/",
      "display_url" : "coconuts.co\/singapore\/news\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "914041997140348930",
  "text" : "\"A man thinks his English is the only English standard.\" https:\/\/t.co\/BCoW4lIL8F via @coconutsdotco",
  "id" : 914041997140348930,
  "created_at" : "2017-09-30 08:19:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    }, {
      "name" : "Tableau Software",
      "screen_name" : "tableau",
      "indices" : [ 107, 115 ],
      "id_str" : "14792516",
      "id" : 14792516
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/7adS4kMQec",
      "expanded_url" : "http:\/\/po.st\/9cn307",
      "display_url" : "po.st\/9cn307"
    } ]
  },
  "geo" : { },
  "id_str" : "914022679765757952",
  "text" : "RT @GovTechSG: Equipping Singapore's public officers with visual analytics skills- GovTech signs deal with @Tableau https:\/\/t.co\/7adS4kMQec\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tableau Software",
        "screen_name" : "tableau",
        "indices" : [ 92, 100 ],
        "id_str" : "14792516",
        "id" : 14792516
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dataviz",
        "indices" : [ 125, 133 ]
      } ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/7adS4kMQec",
        "expanded_url" : "http:\/\/po.st\/9cn307",
        "display_url" : "po.st\/9cn307"
      } ]
    },
    "geo" : { },
    "id_str" : "913673023709888512",
    "text" : "Equipping Singapore's public officers with visual analytics skills- GovTech signs deal with @Tableau https:\/\/t.co\/7adS4kMQec #dataviz",
    "id" : 913673023709888512,
    "created_at" : "2017-09-29 07:53:18 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 914022679765757952,
  "created_at" : "2017-09-30 07:02:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913786583425454082",
  "text" : "My friend sounds excited on FB that her son got enlisted early for national service...",
  "id" : 913786583425454082,
  "created_at" : "2017-09-29 15:24:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andrew Ng",
      "screen_name" : "AndrewYNg",
      "indices" : [ 3, 13 ],
      "id_str" : "216939636",
      "id" : 216939636
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913779500005101574",
  "text" : "RT @AndrewYNg: Stanford's first day of class--record-breaking 1040 people already enrolled for on-campus Machine Learning (CS229). Wow! @da\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dan Boneh",
        "screen_name" : "danboneh",
        "indices" : [ 121, 130 ],
        "id_str" : "15875851",
        "id" : 15875851
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "912382154155352064",
    "text" : "Stanford's first day of class--record-breaking 1040 people already enrolled for on-campus Machine Learning (CS229). Wow! @danboneh",
    "id" : 912382154155352064,
    "created_at" : "2017-09-25 18:23:51 +0000",
    "user" : {
      "name" : "Andrew Ng",
      "screen_name" : "AndrewYNg",
      "protected" : false,
      "id_str" : "216939636",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/733174243714682880\/oyG30NEH_normal.jpg",
      "id" : 216939636,
      "verified" : true
    }
  },
  "id" : 913779500005101574,
  "created_at" : "2017-09-29 14:56:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/Ou8nTMQdfC",
      "expanded_url" : "http:\/\/www.straitstimes.com\/asia\/se-asia\/malaysian-contraceptive-maker-comes-up-with-nasi-lemak-condom",
      "display_url" : "straitstimes.com\/asia\/se-asia\/m\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913712488679661568",
  "text" : "A little bird told me that this was mentioned yesterday on German radio Bayern 3....https:\/\/t.co\/Ou8nTMQdfC",
  "id" : 913712488679661568,
  "created_at" : "2017-09-29 10:30:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913711657406406656\/photo\/1",
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/wyGoDj0iUB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DK4nyX2X0AALLFl.jpg",
      "id_str" : "913711520751800320",
      "id" : 913711520751800320,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK4nyX2X0AALLFl.jpg",
      "sizes" : [ {
        "h" : 555,
        "resize" : "fit",
        "w" : 1025
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 555,
        "resize" : "fit",
        "w" : 1025
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 555,
        "resize" : "fit",
        "w" : 1025
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wyGoDj0iUB"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913711657406406656",
  "text" : "How do I go about retain the same column name while changing the remaining in R? Thanks https:\/\/t.co\/wyGoDj0iUB",
  "id" : 913711657406406656,
  "created_at" : "2017-09-29 10:26:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clifford Coonan",
      "screen_name" : "cliffordcoonan",
      "indices" : [ 3, 18 ],
      "id_str" : "23587089",
      "id" : 23587089
    }, {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 95, 109 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/PNGx2EnVLa",
      "expanded_url" : "https:\/\/www.irishtimes.com\/business\/economy\/irish-companies-expanding-horizons-in-far-east-1.3237028#.Wc4LOoezo18.twitter",
      "display_url" : "irishtimes.com\/business\/econo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913709657033764869",
  "text" : "RT @cliffordcoonan: Irish companies expanding horizons in Far East https:\/\/t.co\/PNGx2EnVLa via @IrishTimesBiz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Times Business",
        "screen_name" : "IrishTimesBiz",
        "indices" : [ 75, 89 ],
        "id_str" : "16737418",
        "id" : 16737418
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/PNGx2EnVLa",
        "expanded_url" : "https:\/\/www.irishtimes.com\/business\/economy\/irish-companies-expanding-horizons-in-far-east-1.3237028#.Wc4LOoezo18.twitter",
        "display_url" : "irishtimes.com\/business\/econo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "913689548231553024",
    "text" : "Irish companies expanding horizons in Far East https:\/\/t.co\/PNGx2EnVLa via @IrishTimesBiz",
    "id" : 913689548231553024,
    "created_at" : "2017-09-29 08:58:58 +0000",
    "user" : {
      "name" : "Clifford Coonan",
      "screen_name" : "cliffordcoonan",
      "protected" : false,
      "id_str" : "23587089",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2954682975\/c1486590b5e5ecce96357cd8b38ea73a_normal.jpeg",
      "id" : 23587089,
      "verified" : false
    }
  },
  "id" : 913709657033764869,
  "created_at" : "2017-09-29 10:18:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/EZJSViiH42",
      "expanded_url" : "https:\/\/www.themarketingtechnologist.co\/google-oauth-2-enable-your-application-to-access-data-from-a-google-user\/",
      "display_url" : "themarketingtechnologist.co\/google-oauth-2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913697420793581569",
  "text" : "How to enable users to authorize your application to access their data. https:\/\/t.co\/EZJSViiH42",
  "id" : 913697420793581569,
  "created_at" : "2017-09-29 09:30:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913695311352262657",
  "text" : "Out of every 9 commuter, 6  is using mobile devices on MRT is not an insight. It observations.",
  "id" : 913695311352262657,
  "created_at" : "2017-09-29 09:21:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/hbr.org\" rel=\"nofollow\"\u003EHarvard Business Review\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/aT1kxjbbXi",
      "expanded_url" : "http:\/\/s.hbr.org\/2bnevbx",
      "display_url" : "s.hbr.org\/2bnevbx"
    } ]
  },
  "geo" : { },
  "id_str" : "913694982053167104",
  "text" : "CMOs don\u2019t need more data; they need useful insights from data. https:\/\/t.co\/aT1kxjbbXi",
  "id" : 913694982053167104,
  "created_at" : "2017-09-29 09:20:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 3, 26 ],
      "url" : "https:\/\/t.co\/5msVezzhfO",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Jonathan_Lee_(musician)",
      "display_url" : "en.wikipedia.org\/wiki\/Jonathan_\u2026"
    }, {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/aYPBUP6bgU",
      "expanded_url" : "https:\/\/twitter.com\/Chinacultureorg\/status\/913674812765212672",
      "display_url" : "twitter.com\/Chinacultureor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913694604024836097",
  "text" : "\u674E\u5B97\u76DBhttps:\/\/t.co\/5msVezzhfO https:\/\/t.co\/aYPBUP6bgU",
  "id" : 913694604024836097,
  "created_at" : "2017-09-29 09:19:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marina Nicholas",
      "screen_name" : "_marinanicholas",
      "indices" : [ 3, 19 ],
      "id_str" : "783311779795505152",
      "id" : 783311779795505152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/Dv1gpRVxOt",
      "expanded_url" : "https:\/\/techcrunch.com\/2017\/09\/27\/microsoft-becomes-a-sponsor-of-the-open-source-initiative\/?ncid=rss",
      "display_url" : "techcrunch.com\/2017\/09\/27\/mic\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913694102935556097",
  "text" : "RT @_marinanicholas: Microsoft becomes a sponsor of the Open Source Initiative\nhttps:\/\/t.co\/Dv1gpRVxOt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/Dv1gpRVxOt",
        "expanded_url" : "https:\/\/techcrunch.com\/2017\/09\/27\/microsoft-becomes-a-sponsor-of-the-open-source-initiative\/?ncid=rss",
        "display_url" : "techcrunch.com\/2017\/09\/27\/mic\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "913686974300639233",
    "text" : "Microsoft becomes a sponsor of the Open Source Initiative\nhttps:\/\/t.co\/Dv1gpRVxOt",
    "id" : 913686974300639233,
    "created_at" : "2017-09-29 08:48:44 +0000",
    "user" : {
      "name" : "Marina Nicholas",
      "screen_name" : "_marinanicholas",
      "protected" : false,
      "id_str" : "783311779795505152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/783313433307602944\/4bhC1dmr_normal.jpg",
      "id" : 783311779795505152,
      "verified" : false
    }
  },
  "id" : 913694102935556097,
  "created_at" : "2017-09-29 09:17:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jory Raphael",
      "screen_name" : "sensibleworld",
      "indices" : [ 3, 17 ],
      "id_str" : "85306808",
      "id" : 85306808
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913693683735810048",
  "text" : "RT @sensibleworld: It's rare, but every once in a while I get to work on icons that will change the course of history. https:\/\/t.co\/dRq3eyo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sensibleworld\/status\/913198085110665216\/photo\/1",
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/dRq3eyo2B7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKxUuysXcAAx6Sg.jpg",
        "id_str" : "913197987307941888",
        "id" : 913197987307941888,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKxUuysXcAAx6Sg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 344,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 607,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 644,
          "resize" : "fit",
          "w" : 1274
        }, {
          "h" : 644,
          "resize" : "fit",
          "w" : 1274
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dRq3eyo2B7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "913198085110665216",
    "text" : "It's rare, but every once in a while I get to work on icons that will change the course of history. https:\/\/t.co\/dRq3eyo2B7",
    "id" : 913198085110665216,
    "created_at" : "2017-09-28 00:26:04 +0000",
    "user" : {
      "name" : "Jory Raphael",
      "screen_name" : "sensibleworld",
      "protected" : false,
      "id_str" : "85306808",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/500374023729975298\/_A7JoGPj_normal.jpeg",
      "id" : 85306808,
      "verified" : false
    }
  },
  "id" : 913693683735810048,
  "created_at" : "2017-09-29 09:15:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/210xOaqnrI",
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/913578013073170432",
      "display_url" : "twitter.com\/mrbrown\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913646909771931648",
  "text" : "He demonstrated the kind of moral clarity one would expect from a leader. https:\/\/t.co\/210xOaqnrI",
  "id" : 913646909771931648,
  "created_at" : "2017-09-29 06:09:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/QYnCHaFdrw",
      "expanded_url" : "https:\/\/twitter.com\/ottocrat\/status\/912998997316513792",
      "display_url" : "twitter.com\/ottocrat\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913505605125656576",
  "text" : "This is a good tread to check out https:\/\/t.co\/QYnCHaFdrw",
  "id" : 913505605125656576,
  "created_at" : "2017-09-28 20:48:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amelia McNamara",
      "screen_name" : "AmeliaMN",
      "indices" : [ 3, 12 ],
      "id_str" : "19520842",
      "id" : 19520842
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AmeliaMN\/status\/913502530088423424\/photo\/1",
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/BmJUwlExVq",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DK1ptIMXoAYugGd.jpg",
      "id_str" : "913502523440472070",
      "id" : 913502523440472070,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK1ptIMXoAYugGd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 811,
        "resize" : "fit",
        "w" : 1664
      }, {
        "h" : 331,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 585,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 811,
        "resize" : "fit",
        "w" : 1664
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BmJUwlExVq"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913504089681645569",
  "text" : "RT @AmeliaMN: This seems right https:\/\/t.co\/BmJUwlExVq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AmeliaMN\/status\/913502530088423424\/photo\/1",
        "indices" : [ 17, 40 ],
        "url" : "https:\/\/t.co\/BmJUwlExVq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DK1ptIMXoAYugGd.jpg",
        "id_str" : "913502523440472070",
        "id" : 913502523440472070,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK1ptIMXoAYugGd.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 811,
          "resize" : "fit",
          "w" : 1664
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 585,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 811,
          "resize" : "fit",
          "w" : 1664
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BmJUwlExVq"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "913502530088423424",
    "text" : "This seems right https:\/\/t.co\/BmJUwlExVq",
    "id" : 913502530088423424,
    "created_at" : "2017-09-28 20:35:49 +0000",
    "user" : {
      "name" : "Amelia McNamara",
      "screen_name" : "AmeliaMN",
      "protected" : false,
      "id_str" : "19520842",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/707607764344967168\/TvVZobj-_normal.jpg",
      "id" : 19520842,
      "verified" : false
    }
  },
  "id" : 913504089681645569,
  "created_at" : "2017-09-28 20:42:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SMRT Corporation",
      "screen_name" : "SMRT_Singapore",
      "indices" : [ 81, 96 ],
      "id_str" : "307781209",
      "id" : 307781209
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/rD5OSByAUR",
      "expanded_url" : "http:\/\/www.scmp.com\/news\/hong-kong\/article\/1512220\/mtr-management-face-pay-cuts-over-rail-disruptions",
      "display_url" : "scmp.com\/news\/hong-kong\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913437928545800192",
  "text" : "MTR management to face pay cuts over rail disruptions https:\/\/t.co\/rD5OSByAUR CC @SMRT_Singapore",
  "id" : 913437928545800192,
  "created_at" : "2017-09-28 16:19:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/D5sCSYcc08",
      "expanded_url" : "https:\/\/iworkwithdata.blogspot.ie\/2017\/09\/test-my-environment.html",
      "display_url" : "iworkwithdata.blogspot.ie\/2017\/09\/test-m\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913437513737539584",
  "text" : "Without Pelican plugin, convert .ipynb to HTML &amp; paste html to blogger option https:\/\/t.co\/D5sCSYcc08 (layout is off)",
  "id" : 913437513737539584,
  "created_at" : "2017-09-28 16:17:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "indices" : [ 0, 13 ],
      "id_str" : "2300614526",
      "id" : 2300614526
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "913392847705006080",
  "geo" : { },
  "id_str" : "913397767422267392",
  "in_reply_to_user_id" : 2300614526,
  "text" : "@pachamaltese I seeking a workflow that can publish what I have done with R and Python as a blog-post with minimal fuss. Any advice?",
  "id" : 913397767422267392,
  "in_reply_to_status_id" : 913392847705006080,
  "created_at" : "2017-09-28 13:39:32 +0000",
  "in_reply_to_screen_name" : "pachamaltese",
  "in_reply_to_user_id_str" : "2300614526",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Lian",
      "screen_name" : "davidlian",
      "indices" : [ 0, 10 ],
      "id_str" : "2000251",
      "id" : 2000251
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "913390658844422145",
  "geo" : { },
  "id_str" : "913390811248828417",
  "in_reply_to_user_id" : 2000251,
  "text" : "@davidlian Eureka?",
  "id" : 913390811248828417,
  "in_reply_to_status_id" : 913390658844422145,
  "created_at" : "2017-09-28 13:11:54 +0000",
  "in_reply_to_screen_name" : "davidlian",
  "in_reply_to_user_id_str" : "2000251",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913390641744416768\/photo\/1",
      "indices" : [ 118, 141 ],
      "url" : "https:\/\/t.co\/BVRWpAH4RH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DK0D5OIXUAYLncb.jpg",
      "id_str" : "913390581006684166",
      "id" : 913390581006684166,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DK0D5OIXUAYLncb.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 621,
        "resize" : "fit",
        "w" : 1299
      }, {
        "h" : 325,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 574,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 621,
        "resize" : "fit",
        "w" : 1299
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BVRWpAH4RH"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913390641744416768",
  "text" : "Appeal to the air-force to reschedule their flight to accommodate a primary school national examination in Singapore. https:\/\/t.co\/BVRWpAH4RH",
  "id" : 913390641744416768,
  "created_at" : "2017-09-28 13:11:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/Ld2e1f7hix",
      "expanded_url" : "https:\/\/petapixel.com\/2017\/09\/25\/japan-camera-hunter-making-new-premium-35mm-compact-camera\/",
      "display_url" : "petapixel.com\/2017\/09\/25\/jap\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "913359903389028352",
  "text" : "Japan Camera Hunter is Making a New Premium 35mm Compact Camera https:\/\/t.co\/Ld2e1f7hix",
  "id" : 913359903389028352,
  "created_at" : "2017-09-28 11:09:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913358360921169923\/photo\/1",
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/4L5P430uEV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKzmlEaXkAA74V9.jpg",
      "id_str" : "913358348963254272",
      "id" : 913358348963254272,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKzmlEaXkAA74V9.jpg",
      "sizes" : [ {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4L5P430uEV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913358360921169923",
  "text" : "Classic bots joke https:\/\/t.co\/4L5P430uEV",
  "id" : 913358360921169923,
  "created_at" : "2017-09-28 11:02:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TOC News",
      "screen_name" : "tocsg",
      "indices" : [ 3, 9 ],
      "id_str" : "44278933",
      "id" : 44278933
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/psYcRGU7XG",
      "expanded_url" : "http:\/\/fb.me\/84QcAEDPo",
      "display_url" : "fb.me\/84QcAEDPo"
    } ]
  },
  "geo" : { },
  "id_str" : "913336808997605376",
  "text" : "RT @tocsg: Dr Chee Soon Juan: The PAP failing elitist model https:\/\/t.co\/psYcRGU7XG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 72 ],
        "url" : "https:\/\/t.co\/psYcRGU7XG",
        "expanded_url" : "http:\/\/fb.me\/84QcAEDPo",
        "display_url" : "fb.me\/84QcAEDPo"
      } ]
    },
    "geo" : { },
    "id_str" : "913268134529904641",
    "text" : "Dr Chee Soon Juan: The PAP failing elitist model https:\/\/t.co\/psYcRGU7XG",
    "id" : 913268134529904641,
    "created_at" : "2017-09-28 05:04:25 +0000",
    "user" : {
      "name" : "TOC News",
      "screen_name" : "tocsg",
      "protected" : false,
      "id_str" : "44278933",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/837662162524856320\/_hEtmxdE_normal.jpg",
      "id" : 44278933,
      "verified" : true
    }
  },
  "id" : 913336808997605376,
  "created_at" : "2017-09-28 09:37:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Derek Greene",
      "screen_name" : "derekgreene",
      "indices" : [ 0, 12 ],
      "id_str" : "3526601",
      "id" : 3526601
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913134624611749888\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/19xjToaemS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKwbFk-XUAEb9ZW.jpg",
      "id_str" : "913134607087915009",
      "id" : 913134607087915009,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKwbFk-XUAEb9ZW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/19xjToaemS"
    } ],
    "hashtags" : [ {
      "text" : "PyDataDublin",
      "indices" : [ 86, 99 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913134624611749888",
  "in_reply_to_user_id" : 3526601,
  "text" : "@derekgreene on the practical issues of topic modelling. Domain knowledge is critical.#PyDataDublin https:\/\/t.co\/19xjToaemS",
  "id" : 913134624611749888,
  "created_at" : "2017-09-27 20:13:54 +0000",
  "in_reply_to_screen_name" : "derekgreene",
  "in_reply_to_user_id_str" : "3526601",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "indices" : [ 3, 13 ],
      "id_str" : "77901568",
      "id" : 77901568
    }, {
      "name" : "PyData Dublin",
      "screen_name" : "PyDataDublin",
      "indices" : [ 15, 28 ],
      "id_str" : "751790806290292737",
      "id" : 751790806290292737
    }, {
      "name" : "Workday",
      "screen_name" : "Workday",
      "indices" : [ 39, 47 ],
      "id_str" : "54983317",
      "id" : 54983317
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/allentv4u\/status\/913111092523671561\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/kyM8vBH14t",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKwFpz-XoAEC8lZ.jpg",
      "id_str" : "913111040333946881",
      "id" : 913111040333946881,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKwFpz-XoAEC8lZ.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/kyM8vBH14t"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/allentv4u\/status\/913111092523671561\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/kyM8vBH14t",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKwFpz-XoAI-LCS.jpg",
      "id_str" : "913111040333946882",
      "id" : 913111040333946882,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKwFpz-XoAI-LCS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/kyM8vBH14t"
    } ],
    "hashtags" : [ {
      "text" : "Dublin",
      "indices" : [ 48, 55 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913116000677187585",
  "text" : "RT @allentv4u: @PyDataDublin launch at @Workday #Dublin office by 2 wonderful speakers from UCD. https:\/\/t.co\/kyM8vBH14t",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "PyData Dublin",
        "screen_name" : "PyDataDublin",
        "indices" : [ 0, 13 ],
        "id_str" : "751790806290292737",
        "id" : 751790806290292737
      }, {
        "name" : "Workday",
        "screen_name" : "Workday",
        "indices" : [ 24, 32 ],
        "id_str" : "54983317",
        "id" : 54983317
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/allentv4u\/status\/913111092523671561\/photo\/1",
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/kyM8vBH14t",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKwFpz-XoAEC8lZ.jpg",
        "id_str" : "913111040333946881",
        "id" : 913111040333946881,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKwFpz-XoAEC8lZ.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kyM8vBH14t"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/allentv4u\/status\/913111092523671561\/photo\/1",
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/kyM8vBH14t",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKwFpz-XoAI-LCS.jpg",
        "id_str" : "913111040333946882",
        "id" : 913111040333946882,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKwFpz-XoAI-LCS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kyM8vBH14t"
      } ],
      "hashtags" : [ {
        "text" : "Dublin",
        "indices" : [ 33, 40 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "913111092523671561",
    "in_reply_to_user_id" : 751790806290292737,
    "text" : "@PyDataDublin launch at @Workday #Dublin office by 2 wonderful speakers from UCD. https:\/\/t.co\/kyM8vBH14t",
    "id" : 913111092523671561,
    "created_at" : "2017-09-27 18:40:23 +0000",
    "in_reply_to_screen_name" : "PyDataDublin",
    "in_reply_to_user_id_str" : "751790806290292737",
    "user" : {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "protected" : false,
      "id_str" : "77901568",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/787980171202686976\/QbSu0IKB_normal.jpg",
      "id" : 77901568,
      "verified" : false
    }
  },
  "id" : 913116000677187585,
  "created_at" : "2017-09-27 18:59:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Siobh\u00E1n Grayson",
      "screen_name" : "siobhan_grayson",
      "indices" : [ 3, 19 ],
      "id_str" : "4120580026",
      "id" : 4120580026
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PyDataDUB",
      "indices" : [ 111, 121 ]
    }, {
      "text" : "DataScience",
      "indices" : [ 122, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913110999657533440",
  "text" : "RT @siobhan_grayson: Python Scrapy (scraping and web crawling framework) was used to collect the marathon data #PyDataDUB #DataScience #Pyt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/siobhan_grayson\/status\/913110772057874436\/photo\/1",
        "indices" : [ 122, 145 ],
        "url" : "https:\/\/t.co\/q6hQkU7XIO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKwFZnDXoAIoRdp.jpg",
        "id_str" : "913110761987350530",
        "id" : 913110761987350530,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKwFZnDXoAIoRdp.jpg",
        "sizes" : [ {
          "h" : 378,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 378,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 251,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 378,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/q6hQkU7XIO"
      } ],
      "hashtags" : [ {
        "text" : "PyDataDUB",
        "indices" : [ 90, 100 ]
      }, {
        "text" : "DataScience",
        "indices" : [ 101, 113 ]
      }, {
        "text" : "Python",
        "indices" : [ 114, 121 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "913110772057874436",
    "text" : "Python Scrapy (scraping and web crawling framework) was used to collect the marathon data #PyDataDUB #DataScience #Python https:\/\/t.co\/q6hQkU7XIO",
    "id" : 913110772057874436,
    "created_at" : "2017-09-27 18:39:07 +0000",
    "user" : {
      "name" : "Siobh\u00E1n Grayson",
      "screen_name" : "siobhan_grayson",
      "protected" : false,
      "id_str" : "4120580026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033798528093626368\/TIrLGiHb_normal.jpg",
      "id" : 4120580026,
      "verified" : false
    }
  },
  "id" : 913110999657533440,
  "created_at" : "2017-09-27 18:40:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913100201484783616\/photo\/1",
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/RhVlEHZhSH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKv7yNFXoAE3GUS.jpg",
      "id_str" : "913100189396869121",
      "id" : 913100189396869121,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKv7yNFXoAE3GUS.jpg",
      "sizes" : [ {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RhVlEHZhSH"
    } ],
    "hashtags" : [ {
      "text" : "PyDatadublin",
      "indices" : [ 0, 13 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913100201484783616",
  "text" : "#PyDatadublin https:\/\/t.co\/RhVlEHZhSH",
  "id" : 913100201484783616,
  "created_at" : "2017-09-27 17:57:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913098308482105344\/photo\/1",
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/sOFmLxMzo4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKv6D8vXoAAVTX6.jpg",
      "id_str" : "913098295224016896",
      "id" : 913098295224016896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKv6D8vXoAAVTX6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1440
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1440
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sOFmLxMzo4"
    } ],
    "hashtags" : [ {
      "text" : "Pydatadublin",
      "indices" : [ 0, 13 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913098308482105344",
  "text" : "#Pydatadublin https:\/\/t.co\/sOFmLxMzo4",
  "id" : 913098308482105344,
  "created_at" : "2017-09-27 17:49:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/913063098013151233\/photo\/1",
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/Gh04DoG0DW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKvaDFBX0AAuITv.jpg",
      "id_str" : "913063095895052288",
      "id" : 913063095895052288,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKvaDFBX0AAuITv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Gh04DoG0DW"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 35 ],
      "url" : "https:\/\/t.co\/sHtXoYwurT",
      "expanded_url" : "http:\/\/ift.tt\/2wWxune",
      "display_url" : "ift.tt\/2wWxune"
    } ]
  },
  "geo" : { },
  "id_str" : "913063098013151233",
  "text" : "Yummy Tummy https:\/\/t.co\/sHtXoYwurT https:\/\/t.co\/Gh04DoG0DW",
  "id" : 913063098013151233,
  "created_at" : "2017-09-27 15:29:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "913001262492372992",
  "text" : "Hotel Check In with Facial recognition it already happening in China!",
  "id" : 913001262492372992,
  "created_at" : "2017-09-27 11:23:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 3, 15 ],
      "id_str" : "168148402",
      "id" : 168148402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912994220230762496",
  "text" : "RT @m4riannelee: At a conference today to explore the most advanced digital society - Estonia. At the Opera House, Oslo. https:\/\/t.co\/CyFM2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/912936416778375168\/photo\/1",
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/CyFM2uI0gJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKtmy-QW0AIH5VI.jpg",
        "id_str" : "912936375363751938",
        "id" : 912936375363751938,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKtmy-QW0AIH5VI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CyFM2uI0gJ"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/912936416778375168\/photo\/1",
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/CyFM2uI0gJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKtm0Z1WsAEXqih.jpg",
        "id_str" : "912936399946559489",
        "id" : 912936399946559489,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKtm0Z1WsAEXqih.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CyFM2uI0gJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "912936416778375168",
    "text" : "At a conference today to explore the most advanced digital society - Estonia. At the Opera House, Oslo. https:\/\/t.co\/CyFM2uI0gJ",
    "id" : 912936416778375168,
    "created_at" : "2017-09-27 07:06:17 +0000",
    "user" : {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "protected" : false,
      "id_str" : "168148402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797414171038060544\/7AkMKjj5_normal.jpg",
      "id" : 168148402,
      "verified" : false
    }
  },
  "id" : 912994220230762496,
  "created_at" : "2017-09-27 10:55:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Twitter",
      "screen_name" : "Twitter",
      "indices" : [ 0, 8 ],
      "id_str" : "783214",
      "id" : 783214
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912929053266661376",
  "in_reply_to_user_id" : 783214,
  "text" : "@Twitter, take my money give me back 140 char limit and the options to edit. Thank you",
  "id" : 912929053266661376,
  "created_at" : "2017-09-27 06:37:02 +0000",
  "in_reply_to_screen_name" : "Twitter",
  "in_reply_to_user_id_str" : "783214",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 145, 168 ],
      "url" : "https:\/\/t.co\/Zm4x5nRnsh",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/912928477849976833",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912928742909071362",
  "text" : "Labels are only derogatory if they are meant so. Being politically correct doesn't end racism &amp; prejudice. Acknowledging our differences do. https:\/\/t.co\/Zm4x5nRnsh",
  "id" : 912928742909071362,
  "created_at" : "2017-09-27 06:35:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/hsa5GW1yYV",
      "expanded_url" : "http:\/\/str.sg\/4jC5",
      "display_url" : "str.sg\/4jC5"
    } ]
  },
  "geo" : { },
  "id_str" : "912928477849976833",
  "text" : "Malayalee father tells what Chinese gangster taught him  https:\/\/t.co\/hsa5GW1yYV",
  "id" : 912928477849976833,
  "created_at" : "2017-09-27 06:34:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caitlin Kelly",
      "screen_name" : "caitlin__kelly",
      "indices" : [ 3, 18 ],
      "id_str" : "243896198",
      "id" : 243896198
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/caitlin__kelly\/status\/912795950476857344\/photo\/1",
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/WkfdXL8oLh",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKrm0PdXUAIj95-.jpg",
      "id_str" : "912795659673227266",
      "id" : 912795659673227266,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKrm0PdXUAIj95-.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 315,
        "resize" : "fit",
        "w" : 591
      }, {
        "h" : 315,
        "resize" : "fit",
        "w" : 591
      }, {
        "h" : 315,
        "resize" : "fit",
        "w" : 591
      }, {
        "h" : 315,
        "resize" : "fit",
        "w" : 591
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WkfdXL8oLh"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912924091572768768",
  "text" : "RT @caitlin__kelly: 139 characters https:\/\/t.co\/WkfdXL8oLh",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/caitlin__kelly\/status\/912795950476857344\/photo\/1",
        "indices" : [ 15, 38 ],
        "url" : "https:\/\/t.co\/WkfdXL8oLh",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKrm0PdXUAIj95-.jpg",
        "id_str" : "912795659673227266",
        "id" : 912795659673227266,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKrm0PdXUAIj95-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 591
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 591
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 591
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 591
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WkfdXL8oLh"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "912795950476857344",
    "text" : "139 characters https:\/\/t.co\/WkfdXL8oLh",
    "id" : 912795950476857344,
    "created_at" : "2017-09-26 21:48:08 +0000",
    "user" : {
      "name" : "Caitlin Kelly",
      "screen_name" : "caitlin__kelly",
      "protected" : false,
      "id_str" : "243896198",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031996478305259520\/iBWLQx8Z_normal.jpg",
      "id" : 243896198,
      "verified" : true
    }
  },
  "id" : 912924091572768768,
  "created_at" : "2017-09-27 06:17:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 28 ],
      "url" : "https:\/\/t.co\/K3OJ05CMhy",
      "expanded_url" : "https:\/\/twitter.com\/AFP\/status\/912691440861483008",
      "display_url" : "twitter.com\/AFP\/status\/912\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912815928806690816",
  "text" : "Yes! https:\/\/t.co\/K3OJ05CMhy",
  "id" : 912815928806690816,
  "created_at" : "2017-09-26 23:07:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 3, 15 ],
      "id_str" : "168148402",
      "id" : 168148402
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/912616538318000129\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/9Bh7L6HFPT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKpDsB6WAAAhn-Q.jpg",
      "id_str" : "912616298202398720",
      "id" : 912616298202398720,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKpDsB6WAAAhn-Q.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9Bh7L6HFPT"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/912616538318000129\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/9Bh7L6HFPT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKpDzqWW4AIUrWb.jpg",
      "id_str" : "912616429316399106",
      "id" : 912616429316399106,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKpDzqWW4AIUrWb.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9Bh7L6HFPT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912619069160992769",
  "text" : "RT @m4riannelee: Only for girls!!  Yay!!\nGirl Tech Fest in Oslo. \uD83D\uDE01 https:\/\/t.co\/9Bh7L6HFPT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/912616538318000129\/photo\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/9Bh7L6HFPT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKpDsB6WAAAhn-Q.jpg",
        "id_str" : "912616298202398720",
        "id" : 912616298202398720,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKpDsB6WAAAhn-Q.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9Bh7L6HFPT"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/912616538318000129\/photo\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/9Bh7L6HFPT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKpDzqWW4AIUrWb.jpg",
        "id_str" : "912616429316399106",
        "id" : 912616429316399106,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKpDzqWW4AIUrWb.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9Bh7L6HFPT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "912616538318000129",
    "text" : "Only for girls!!  Yay!!\nGirl Tech Fest in Oslo. \uD83D\uDE01 https:\/\/t.co\/9Bh7L6HFPT",
    "id" : 912616538318000129,
    "created_at" : "2017-09-26 09:55:12 +0000",
    "user" : {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "protected" : false,
      "id_str" : "168148402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797414171038060544\/7AkMKjj5_normal.jpg",
      "id" : 168148402,
      "verified" : false
    }
  },
  "id" : 912619069160992769,
  "created_at" : "2017-09-26 10:05:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/6mJyf3oz51",
      "expanded_url" : "http:\/\/insight.kellogg.northwestern.edu\/article\/why-warmth-is-the-underappreciated-skill-leaders-need",
      "display_url" : "insight.kellogg.northwestern.edu\/article\/why-wa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912611599843758081",
  "text" : "https:\/\/t.co\/6mJyf3oz51",
  "id" : 912611599843758081,
  "created_at" : "2017-09-26 09:35:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/J33dJLwDJW",
      "expanded_url" : "https:\/\/www.mockaroo.com\/",
      "display_url" : "mockaroo.com"
    } ]
  },
  "geo" : { },
  "id_str" : "912433824113610752",
  "text" : "Need some mock data to test your app or tutorial? https:\/\/t.co\/J33dJLwDJW",
  "id" : 912433824113610752,
  "created_at" : "2017-09-25 21:49:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "EI Asia Pacific",
      "screen_name" : "EI_AsiaPacific",
      "indices" : [ 3, 18 ],
      "id_str" : "907941453321478144",
      "id" : 907941453321478144
    }, {
      "name" : "Enterprise Ireland",
      "screen_name" : "Entirl",
      "indices" : [ 21, 28 ],
      "id_str" : "60575311",
      "id" : 60575311
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912395206997180416",
  "text" : "RT @EI_AsiaPacific: .@Entirl Asia Pacific is officially on Twitter &amp; kicking off the T\u00E1naiste's Trade &amp; Investment Mission to Singapore &amp; J\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Enterprise Ireland",
        "screen_name" : "Entirl",
        "indices" : [ 1, 8 ],
        "id_str" : "60575311",
        "id" : 60575311
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EI_AsiaPacific\/status\/912131916958183424\/photo\/1",
        "indices" : [ 152, 175 ],
        "url" : "https:\/\/t.co\/5cUoTZg79K",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKiKuPIXUAAywNO.jpg",
        "id_str" : "912131451482755072",
        "id" : 912131451482755072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKiKuPIXUAAywNO.jpg",
        "sizes" : [ {
          "h" : 1280,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5cUoTZg79K"
      } ],
      "hashtags" : [ {
        "text" : "IrishAdvantage",
        "indices" : [ 136, 151 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "912131916958183424",
    "text" : ".@Entirl Asia Pacific is officially on Twitter &amp; kicking off the T\u00E1naiste's Trade &amp; Investment Mission to Singapore &amp; Japan #IrishAdvantage https:\/\/t.co\/5cUoTZg79K",
    "id" : 912131916958183424,
    "created_at" : "2017-09-25 01:49:30 +0000",
    "user" : {
      "name" : "EI Asia Pacific",
      "screen_name" : "EI_AsiaPacific",
      "protected" : false,
      "id_str" : "907941453321478144",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/911050187740667905\/-zEFOyi-_normal.jpg",
      "id" : 907941453321478144,
      "verified" : false
    }
  },
  "id" : 912395206997180416,
  "created_at" : "2017-09-25 19:15:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/DynSyj0DAH",
      "expanded_url" : "https:\/\/www.irishtimes.com\/business\/economy\/tanaiste-says-post-brexit-world-offers-opportunities-for-ireland-in-asia-1.3232866",
      "display_url" : "irishtimes.com\/business\/econo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912394794953043970",
  "text" : "Ireland's Deputy Prime Minister visit to Singapore to explore opportunities &amp; strengthen economic ties. https:\/\/t.co\/DynSyj0DAH",
  "id" : 912394794953043970,
  "created_at" : "2017-09-25 19:14:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912376919617916929",
  "text" : "Did Ireland has the infrastructure for rugby world cup?  Asking for a friend.",
  "id" : 912376919617916929,
  "created_at" : "2017-09-25 18:03:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jim Sterne",
      "screen_name" : "jimsterne",
      "indices" : [ 19, 29 ],
      "id_str" : "3801151",
      "id" : 3801151
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/912375767153221633\/photo\/1",
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/5icUc2TgbW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKloV6OWkAEa02I.jpg",
      "id_str" : "912375125135233025",
      "id" : 912375125135233025,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKloV6OWkAEa02I.jpg",
      "sizes" : [ {
        "h" : 129,
        "resize" : "fit",
        "w" : 869
      }, {
        "h" : 129,
        "resize" : "crop",
        "w" : 129
      }, {
        "h" : 129,
        "resize" : "fit",
        "w" : 869
      }, {
        "h" : 101,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 129,
        "resize" : "fit",
        "w" : 869
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5icUc2TgbW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912375767153221633",
  "text" : "The sausage \uD83C\uDF2D from @jimsterne 's book Artificial Intelligence for Marketing https:\/\/t.co\/5icUc2TgbW",
  "id" : 912375767153221633,
  "created_at" : "2017-09-25 17:58:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/kEmwwZ6rFS",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/812809310308798465",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912369393945980928",
  "text" : "Remember RFM  are really from a financial, and not a customer, point of view. https:\/\/t.co\/kEmwwZ6rFS",
  "id" : 912369393945980928,
  "created_at" : "2017-09-25 17:33:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/Jt81BUZ16j",
      "expanded_url" : "http:\/\/a.msn.com\/0E\/en-ie\/AAsqiYk?ocid=st",
      "display_url" : "a.msn.com\/0E\/en-ie\/AAsqi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912365313550901248",
  "text" : "The four letters you really don't want to find on your boarding pass https:\/\/t.co\/Jt81BUZ16j",
  "id" : 912365313550901248,
  "created_at" : "2017-09-25 17:16:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912363790825598978",
  "text" : "You know there are new tenants in the apartment when fire alarms trigger every now and then during dinner time?",
  "id" : 912363790825598978,
  "created_at" : "2017-09-25 17:10:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iPhone8Plus",
      "indices" : [ 13, 25 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/xlZNe4Cvzb",
      "expanded_url" : "http:\/\/austinmann.com\/trek\/iphone-8-camera-review-india",
      "display_url" : "austinmann.com\/trek\/iphone-8-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912357712180150272",
  "text" : "RT @mrbrown: #iPhone8Plus Camera Review: India https:\/\/t.co\/xlZNe4Cvzb Wow.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "iPhone8Plus",
        "indices" : [ 0, 12 ]
      } ],
      "urls" : [ {
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/xlZNe4Cvzb",
        "expanded_url" : "http:\/\/austinmann.com\/trek\/iphone-8-camera-review-india",
        "display_url" : "austinmann.com\/trek\/iphone-8-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "912211411018825728",
    "text" : "#iPhone8Plus Camera Review: India https:\/\/t.co\/xlZNe4Cvzb Wow.",
    "id" : 912211411018825728,
    "created_at" : "2017-09-25 07:05:23 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 912357712180150272,
  "created_at" : "2017-09-25 16:46:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/DXNOL90rDi",
      "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/912327306869399552",
      "display_url" : "twitter.com\/lisaocarroll\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912356794592358402",
  "text" : "Why people can't accept the fact that when u are in a sovereign state you are subject to finer printed or ID card? https:\/\/t.co\/DXNOL90rDi",
  "id" : 912356794592358402,
  "created_at" : "2017-09-25 16:43:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elton Stoneman",
      "screen_name" : "EltonStoneman",
      "indices" : [ 3, 17 ],
      "id_str" : "2260115140",
      "id" : 2260115140
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/EltonStoneman\/status\/912341525819949057\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/P4QFenfc0l",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKlJuhkW4AAu17w.jpg",
      "id_str" : "912341463152910336",
      "id" : 912341463152910336,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKlJuhkW4AAu17w.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 865,
        "resize" : "fit",
        "w" : 1600
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 865,
        "resize" : "fit",
        "w" : 1600
      }, {
        "h" : 649,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/P4QFenfc0l"
    } ],
    "hashtags" : [ {
      "text" : "SQLServer",
      "indices" : [ 31, 41 ]
    }, {
      "text" : "Docker",
      "indices" : [ 53, 60 ]
    }, {
      "text" : "MSIgnite",
      "indices" : [ 94, 103 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912347151287635968",
  "text" : "RT @EltonStoneman: Yes, that's #SQLServer running in #Docker on a Linux container on a Mac :) #MSIgnite https:\/\/t.co\/P4QFenfc0l",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EltonStoneman\/status\/912341525819949057\/photo\/1",
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/P4QFenfc0l",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKlJuhkW4AAu17w.jpg",
        "id_str" : "912341463152910336",
        "id" : 912341463152910336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKlJuhkW4AAu17w.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 865,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 368,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 865,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/P4QFenfc0l"
      } ],
      "hashtags" : [ {
        "text" : "SQLServer",
        "indices" : [ 12, 22 ]
      }, {
        "text" : "Docker",
        "indices" : [ 34, 41 ]
      }, {
        "text" : "MSIgnite",
        "indices" : [ 75, 84 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "912341525819949057",
    "text" : "Yes, that's #SQLServer running in #Docker on a Linux container on a Mac :) #MSIgnite https:\/\/t.co\/P4QFenfc0l",
    "id" : 912341525819949057,
    "created_at" : "2017-09-25 15:42:24 +0000",
    "user" : {
      "name" : "Elton Stoneman",
      "screen_name" : "EltonStoneman",
      "protected" : false,
      "id_str" : "2260115140",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/870307061665591301\/pDug6NxY_normal.jpg",
      "id" : 2260115140,
      "verified" : false
    }
  },
  "id" : 912347151287635968,
  "created_at" : "2017-09-25 16:04:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/EdFzJV4gsv",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/912276346340089857",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912317415782064128",
  "text" : "You can install Ubuntu on your Windows VM, type some command line and you are done or just hire me. https:\/\/t.co\/EdFzJV4gsv",
  "id" : 912317415782064128,
  "created_at" : "2017-09-25 14:06:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/i4ukgbntCS",
      "expanded_url" : "https:\/\/planb.nicecupoftea.org\/2017\/09\/16\/leaving-flickr\/",
      "display_url" : "planb.nicecupoftea.org\/2017\/09\/16\/lea\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912276346340089857",
  "text" : "Scripts and tools to get your photo back from Flickr. https:\/\/t.co\/i4ukgbntCS &lt; If you need help on this, DM me.",
  "id" : 912276346340089857,
  "created_at" : "2017-09-25 11:23:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/RrgURQjTIM",
      "expanded_url" : "http:\/\/blog.ibmjstart.net\/2016\/01\/28\/jupyter-notebooks-as-restful-microservices\/",
      "display_url" : "blog.ibmjstart.net\/2016\/01\/28\/jup\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "912273287186731010",
  "text" : "Data people can make their work immediately useful to software developers to turn into products.  https:\/\/t.co\/RrgURQjTIM",
  "id" : 912273287186731010,
  "created_at" : "2017-09-25 11:11:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "912251601443786752",
  "text" : "I seldom say this : I so looking forward to Saturday...",
  "id" : 912251601443786752,
  "created_at" : "2017-09-25 09:45:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cond\u00E9 Nast Traveler",
      "screen_name" : "CNTraveler",
      "indices" : [ 78, 89 ],
      "id_str" : "17219108",
      "id" : 17219108
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/ThUU1L1frd",
      "expanded_url" : "https:\/\/www.cntraveler.com\/story\/singapores-famed-hawker-centers-are-under-threat",
      "display_url" : "cntraveler.com\/story\/singapor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "911998495397175297",
  "text" : "Singapore's Famed Hawker Centers Are Under Threat https:\/\/t.co\/ThUU1L1frd via @CNTraveler",
  "id" : 911998495397175297,
  "created_at" : "2017-09-24 16:59:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Thomas LaRock",
      "screen_name" : "SQLRockstar",
      "indices" : [ 3, 15 ],
      "id_str" : "15710997",
      "id" : 15710997
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PieChartsKillKittens",
      "indices" : [ 111, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "911929529484095488",
  "text" : "RT @SQLRockstar: CUT TO DESIGN MEETING\n\n\u201CWe need something like a pie chart, but worse.\u201D\n\n\u201CSure thing, boss.\u201D\n\n#PieChartsKillKittens https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SQLRockstar\/status\/897858912015220736\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/lXJ6SKYxNc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DHXVjLdWAAAeQfW.jpg",
        "id_str" : "897858501078286336",
        "id" : 897858501078286336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DHXVjLdWAAAeQfW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 708,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 708,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 672
        }, {
          "h" : 708,
          "resize" : "fit",
          "w" : 700
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/lXJ6SKYxNc"
      } ],
      "hashtags" : [ {
        "text" : "PieChartsKillKittens",
        "indices" : [ 94, 115 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "897858912015220736",
    "text" : "CUT TO DESIGN MEETING\n\n\u201CWe need something like a pie chart, but worse.\u201D\n\n\u201CSure thing, boss.\u201D\n\n#PieChartsKillKittens https:\/\/t.co\/lXJ6SKYxNc",
    "id" : 897858912015220736,
    "created_at" : "2017-08-16 16:33:40 +0000",
    "user" : {
      "name" : "Thomas LaRock",
      "screen_name" : "SQLRockstar",
      "protected" : false,
      "id_str" : "15710997",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/821858306134315010\/G_wrMyC9_normal.jpg",
      "id" : 15710997,
      "verified" : true
    }
  },
  "id" : 911929529484095488,
  "created_at" : "2017-09-24 12:25:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "911929028294168576",
  "text" : "\"Being data driven shouldn't mean driving around until you find data that supports your opinion\"",
  "id" : 911929028294168576,
  "created_at" : "2017-09-24 12:23:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aidan McKeown",
      "screen_name" : "AidanKeown",
      "indices" : [ 3, 14 ],
      "id_str" : "528672147",
      "id" : 528672147
    }, {
      "name" : "Kevin Byrne",
      "screen_name" : "byrne_km",
      "indices" : [ 16, 25 ],
      "id_str" : "595821857",
      "id" : 595821857
    }, {
      "name" : "Stephen Donnelly",
      "screen_name" : "DonnellyStephen",
      "indices" : [ 26, 42 ],
      "id_str" : "237826875",
      "id" : 237826875
    }, {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 43, 54 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "911889794430586882",
  "text" : "RT @AidanKeown: @byrne_km @DonnellyStephen @ronanlyons In Netherlands, families live very comfortably in apartments. It's the quality of th\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Kevin Byrne",
        "screen_name" : "byrne_km",
        "indices" : [ 0, 9 ],
        "id_str" : "595821857",
        "id" : 595821857
      }, {
        "name" : "Stephen Donnelly",
        "screen_name" : "DonnellyStephen",
        "indices" : [ 10, 26 ],
        "id_str" : "237826875",
        "id" : 237826875
      }, {
        "name" : "Ronan Lyons",
        "screen_name" : "ronanlyons",
        "indices" : [ 27, 38 ],
        "id_str" : "18189672",
        "id" : 18189672
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "911615786824978437",
    "geo" : { },
    "id_str" : "911883021975982080",
    "in_reply_to_user_id" : 595821857,
    "text" : "@byrne_km @DonnellyStephen @ronanlyons In Netherlands, families live very comfortably in apartments. It's the quality of the space, build and nearby amenities that matter.",
    "id" : 911883021975982080,
    "in_reply_to_status_id" : 911615786824978437,
    "created_at" : "2017-09-24 09:20:29 +0000",
    "in_reply_to_screen_name" : "byrne_km",
    "in_reply_to_user_id_str" : "595821857",
    "user" : {
      "name" : "Aidan McKeown",
      "screen_name" : "AidanKeown",
      "protected" : false,
      "id_str" : "528672147",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/886614938097209344\/qoKuPETt_normal.jpg",
      "id" : 528672147,
      "verified" : false
    }
  },
  "id" : 911889794430586882,
  "created_at" : "2017-09-24 09:47:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/Jsg2Rz5Gld",
      "expanded_url" : "https:\/\/mothership.sg\/2017\/09\/iconic-spore-takeaway-kopi-plastic-bag-is-now-an-actual-bag\/",
      "display_url" : "mothership.sg\/2017\/09\/iconic\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "911883234274955265",
  "text" : "Iconic Singapore coffee takeaway is now an actual bag https:\/\/t.co\/Jsg2Rz5Gld",
  "id" : 911883234274955265,
  "created_at" : "2017-09-24 09:21:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CultureNight",
      "indices" : [ 42, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/G0vBLVTL0p",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/911707951844462593",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "911880896776720385",
  "text" : "I just added in the information for this  #CultureNight video https:\/\/t.co\/G0vBLVTL0p",
  "id" : 911880896776720385,
  "created_at" : "2017-09-24 09:12:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "911880490914926592",
  "text" : "There is no point to analytics unless it has a payoff.  It\u2019s not what is interesting to the analyst, but what is impactful to the business",
  "id" : 911880490914926592,
  "created_at" : "2017-09-24 09:10:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CultureNight",
      "indices" : [ 16, 29 ]
    } ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/xoEEvciBWK",
      "expanded_url" : "https:\/\/youtu.be\/5cQFpgL5RVE",
      "display_url" : "youtu.be\/5cQFpgL5RVE"
    } ]
  },
  "geo" : { },
  "id_str" : "911707951844462593",
  "text" : "At last evening #CultureNight in Dublin. Mesmerising. https:\/\/t.co\/xoEEvciBWK",
  "id" : 911707951844462593,
  "created_at" : "2017-09-23 21:44:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/3S3XKGCgBj",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/make-every-customer-interaction-more-profitable-shiao-shyan-yap-%E5%8F%B6%E6%99%93%E8%BB%92",
      "display_url" : "linkedin.com\/pulse\/make-eve\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "911666605129715712",
  "text" : "Data from a wide variety of sources makes analytic job more robust.https:\/\/t.co\/3S3XKGCgBj",
  "id" : 911666605129715712,
  "created_at" : "2017-09-23 19:00:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Jane Coffey",
      "screen_name" : "savvysarahjane",
      "indices" : [ 3, 18 ],
      "id_str" : "17828777",
      "id" : 17828777
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "911385479769149440",
  "text" : "RT @savvysarahjane: Sigh. Dear CEOs, HR people, everyone: Not Everyone Drinks. Making it part of your \"team culture\" will alienate talented\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "911357833664356352",
    "text" : "Sigh. Dear CEOs, HR people, everyone: Not Everyone Drinks. Making it part of your \"team culture\" will alienate talented people.",
    "id" : 911357833664356352,
    "created_at" : "2017-09-22 22:33:34 +0000",
    "user" : {
      "name" : "Sarah Jane Coffey",
      "screen_name" : "savvysarahjane",
      "protected" : false,
      "id_str" : "17828777",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/732050656123453442\/vU8aCZ7h_normal.jpg",
      "id" : 17828777,
      "verified" : false
    }
  },
  "id" : 911385479769149440,
  "created_at" : "2017-09-23 00:23:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/911349235106422784\/photo\/1",
      "indices" : [ 140, 163 ],
      "url" : "https:\/\/t.co\/RvF3WvJ1sA",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKXDTJPXUAAoPC1.jpg",
      "id_str" : "911349233277685760",
      "id" : 911349233277685760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKXDTJPXUAAoPC1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RvF3WvJ1sA"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/PXGbte611k",
      "expanded_url" : "http:\/\/ift.tt\/2xufSTf",
      "display_url" : "ift.tt\/2xufSTf"
    } ]
  },
  "geo" : { },
  "id_str" : "911349235106422784",
  "text" : "Highlights for me is the stunning performance by RTE National Symphony orchestra under the barton of  Robert Trevi\u2026 https:\/\/t.co\/PXGbte611k https:\/\/t.co\/RvF3WvJ1sA",
  "id" : 911349235106422784,
  "created_at" : "2017-09-22 21:59:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/911348452482781185\/photo\/1",
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/OJbZqdQMhW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKXClmiW4AAHMDs.jpg",
      "id_str" : "911348450867994624",
      "id" : 911348450867994624,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKXClmiW4AAHMDs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OJbZqdQMhW"
    } ],
    "hashtags" : [ {
      "text" : "culturenight",
      "indices" : [ 21, 34 ]
    } ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/IlWAU6X1Nu",
      "expanded_url" : "http:\/\/ift.tt\/2wNq1Hi",
      "display_url" : "ift.tt\/2wNq1Hi"
    } ]
  },
  "geo" : { },
  "id_str" : "911348452482781185",
  "text" : "Algorithm and music. #culturenight https:\/\/t.co\/IlWAU6X1Nu https:\/\/t.co\/OJbZqdQMhW",
  "id" : 911348452482781185,
  "created_at" : "2017-09-22 21:56:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/911348100794585088\/photo\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/vXicrdjfcG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKXCRCUWkAATvQV.jpg",
      "id_str" : "911348097548193792",
      "id" : 911348097548193792,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKXCRCUWkAATvQV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vXicrdjfcG"
    } ],
    "hashtags" : [ {
      "text" : "culturenight",
      "indices" : [ 44, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/4ulUhNMyF6",
      "expanded_url" : "http:\/\/ift.tt\/2wMwySH",
      "display_url" : "ift.tt\/2wMwySH"
    } ]
  },
  "geo" : { },
  "id_str" : "911348100794585088",
  "text" : "First time in front of a harp. Mesmerising. #culturenight https:\/\/t.co\/4ulUhNMyF6 https:\/\/t.co\/vXicrdjfcG",
  "id" : 911348100794585088,
  "created_at" : "2017-09-22 21:54:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/911263534151032832\/photo\/1",
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/BaawbemKH2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKV1WqpWAAAuAGz.jpg",
      "id_str" : "911263531877662720",
      "id" : 911263531877662720,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKV1WqpWAAAuAGz.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BaawbemKH2"
    } ],
    "hashtags" : [ {
      "text" : "cluturenight",
      "indices" : [ 33, 46 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/BhjjFxo7Me",
      "expanded_url" : "http:\/\/ift.tt\/2yi5xr6",
      "display_url" : "ift.tt\/2yi5xr6"
    } ]
  },
  "geo" : { },
  "id_str" : "911263534151032832",
  "text" : "WE are early for the guided tour #cluturenight https:\/\/t.co\/BhjjFxo7Me https:\/\/t.co\/BaawbemKH2",
  "id" : 911263534151032832,
  "created_at" : "2017-09-22 16:18:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/hYZhlHlwlv",
      "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/911214246024896515",
      "display_url" : "twitter.com\/paulinesargent\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "911246530174181376",
  "text" : "Religious instruction should take place outside normal school hours. https:\/\/t.co\/hYZhlHlwlv",
  "id" : 911246530174181376,
  "created_at" : "2017-09-22 15:11:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/nqtplldKtb",
      "expanded_url" : "http:\/\/adage.com\/article\/digitalnext\/speaking-language-marketing-product-teams-collaborate-analytics\/310469\/",
      "display_url" : "adage.com\/article\/digita\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "911245671797346304",
  "text" : "Why Marketing, Product Teams Should Collaborate on Analytics https:\/\/t.co\/nqtplldKtb",
  "id" : 911245671797346304,
  "created_at" : "2017-09-22 15:07:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/vzfpWUoMjA",
      "expanded_url" : "http:\/\/str.sg\/4nkk",
      "display_url" : "str.sg\/4nkk"
    } ]
  },
  "geo" : { },
  "id_str" : "911203423307145217",
  "text" : "In Pictures: Public housing in Singapore through the years https:\/\/t.co\/vzfpWUoMjA",
  "id" : 911203423307145217,
  "created_at" : "2017-09-22 12:20:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 53, 63 ]
    } ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/nrMT1qN461",
      "expanded_url" : "https:\/\/omh.sg\/landlords-guide",
      "display_url" : "omh.sg\/landlords-guide"
    } ]
  },
  "geo" : { },
  "id_str" : "911201914955730950",
  "text" : "Anyone has deal with https:\/\/t.co\/nrMT1qN461 before? #Singapore Thanks.",
  "id" : 911201914955730950,
  "created_at" : "2017-09-22 12:14:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Linda (Xia) Liu",
      "screen_name" : "DrLiuBigData",
      "indices" : [ 3, 16 ],
      "id_str" : "314320335",
      "id" : 314320335
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "data",
      "indices" : [ 132, 137 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910996460061290496",
  "text" : "RT @DrLiuBigData: If machine learning were a human, then soul is math &amp; stats, body is algorithm &amp; data structure, blood is #data. Coding c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "data",
        "indices" : [ 114, 119 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "896255728197005312",
    "text" : "If machine learning were a human, then soul is math &amp; stats, body is algorithm &amp; data structure, blood is #data. Coding connect all together",
    "id" : 896255728197005312,
    "created_at" : "2017-08-12 06:23:11 +0000",
    "user" : {
      "name" : "Linda (Xia) Liu",
      "screen_name" : "DrLiuBigData",
      "protected" : false,
      "id_str" : "314320335",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/893173810781868032\/OFIlnpKo_normal.jpg",
      "id" : 314320335,
      "verified" : false
    }
  },
  "id" : 910996460061290496,
  "created_at" : "2017-09-21 22:37:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC Three",
      "screen_name" : "bbcthree",
      "indices" : [ 3, 12 ],
      "id_str" : "166215202",
      "id" : 166215202
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/bbcthree\/status\/910488336799862784\/video\/1",
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/JaAB0p6Qr2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKK0Bf1WsAASwc6.jpg",
      "id_str" : "910487416846340096",
      "id" : 910487416846340096,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKK0Bf1WsAASwc6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/JaAB0p6Qr2"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910901950857797632",
  "text" : "RT @bbcthree: Liam Gallagher making tea is the best thing you'll see today. As you were. https:\/\/t.co\/JaAB0p6Qr2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bbcthree\/status\/910488336799862784\/video\/1",
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/JaAB0p6Qr2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKK0Bf1WsAASwc6.jpg",
        "id_str" : "910487416846340096",
        "id" : 910487416846340096,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKK0Bf1WsAASwc6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JaAB0p6Qr2"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910488336799862784",
    "text" : "Liam Gallagher making tea is the best thing you'll see today. As you were. https:\/\/t.co\/JaAB0p6Qr2",
    "id" : 910488336799862784,
    "created_at" : "2017-09-20 12:58:30 +0000",
    "user" : {
      "name" : "BBC Three",
      "screen_name" : "bbcthree",
      "protected" : false,
      "id_str" : "166215202",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040210772293562369\/TU4Q19uB_normal.jpg",
      "id" : 166215202,
      "verified" : true
    }
  },
  "id" : 910901950857797632,
  "created_at" : "2017-09-21 16:22:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 0, 14 ],
      "id_str" : "80549724",
      "id" : 80549724
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "910887457129889793",
  "geo" : { },
  "id_str" : "910887924471877633",
  "in_reply_to_user_id" : 80549724,
  "text" : "@dublinbusnews I missed this one. What about the next one?",
  "id" : 910887924471877633,
  "in_reply_to_status_id" : 910887457129889793,
  "created_at" : "2017-09-21 15:26:19 +0000",
  "in_reply_to_screen_name" : "dublinbusnews",
  "in_reply_to_user_id_str" : "80549724",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 0, 14 ],
      "id_str" : "80549724",
      "id" : 80549724
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910887128241975296",
  "in_reply_to_user_id" : 80549724,
  "text" : "@dublinbusnews when is the next 18 to stop 2726? Thank You",
  "id" : 910887128241975296,
  "created_at" : "2017-09-21 15:23:09 +0000",
  "in_reply_to_screen_name" : "dublinbusnews",
  "in_reply_to_user_id_str" : "80549724",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/NLCL3Ecas4",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/cc3bbe00784e3e2c409d202bdbec937c",
      "display_url" : "gist.github.com\/mryap\/cc3bbe00\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910867040398868482",
  "text" : "Used to curate bookmarks without context &amp; now I write a blogpost just to compile them together. https:\/\/t.co\/NLCL3Ecas4",
  "id" : 910867040398868482,
  "created_at" : "2017-09-21 14:03:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910860067544666113",
  "text" : "RT @SarcasmMother: every single person you've ever loved will either go to your funeral or you will go to theirs.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910859456530890752",
    "text" : "every single person you've ever loved will either go to your funeral or you will go to theirs.",
    "id" : 910859456530890752,
    "created_at" : "2017-09-21 13:33:12 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 910860067544666113,
  "created_at" : "2017-09-21 13:35:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/ssB6NyFdqY",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-41095004",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910851317370953728",
  "text" : "RT @pdscott: In Sweden, barely 1% of the value of all payments are made using cash https:\/\/t.co\/ssB6NyFdqY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/ssB6NyFdqY",
        "expanded_url" : "http:\/\/www.bbc.com\/news\/business-41095004",
        "display_url" : "bbc.com\/news\/business-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "910850482779955200",
    "text" : "In Sweden, barely 1% of the value of all payments are made using cash https:\/\/t.co\/ssB6NyFdqY",
    "id" : 910850482779955200,
    "created_at" : "2017-09-21 12:57:32 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 910851317370953728,
  "created_at" : "2017-09-21 13:00:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 29 ],
      "url" : "https:\/\/t.co\/YmtMnTvzRw",
      "expanded_url" : "https:\/\/twitter.com\/TheTweetOfGod\/status\/910338733437632512",
      "display_url" : "twitter.com\/TheTweetOfGod\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910835953710878721",
  "text" : "\u5B9E\u9645\u7684\u884C\u52A8 https:\/\/t.co\/YmtMnTvzRw",
  "id" : 910835953710878721,
  "created_at" : "2017-09-21 11:59:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "refugeeswelcome",
      "indices" : [ 67, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/1ppNIRlF6N",
      "expanded_url" : "https:\/\/twitter.com\/FocusIreland\/status\/908069036700737536",
      "display_url" : "twitter.com\/FocusIreland\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910834150516625408",
  "text" : "How about approaching those who offer a place for abode and tweet  #refugeeswelcome to house the 600 youth? https:\/\/t.co\/1ppNIRlF6N",
  "id" : 910834150516625408,
  "created_at" : "2017-09-21 11:52:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/910825822541447169\/photo\/1",
      "indices" : [ 135, 158 ],
      "url" : "https:\/\/t.co\/tEPOEjnVlL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKPnQK1XUAELxh1.jpg",
      "id_str" : "910825814631075841",
      "id" : 910825814631075841,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKPnQK1XUAELxh1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tEPOEjnVlL"
    } ],
    "hashtags" : [ {
      "text" : "Note2Shelf",
      "indices" : [ 0, 11 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910825822541447169",
  "text" : "#Note2Shelf: Edit kernel.json at D:\\WinPython-64bit-3.5.3.1Qt5\\settings\\kernels\\ir to indicate this is running from my portable drive. https:\/\/t.co\/tEPOEjnVlL",
  "id" : 910825822541447169,
  "created_at" : "2017-09-21 11:19:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/910818725892378625\/photo\/1",
      "indices" : [ 139, 162 ],
      "url" : "https:\/\/t.co\/pGJB2YHxG5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKPgfbHW4AAfZON.jpg",
      "id_str" : "910818380118155264",
      "id" : 910818380118155264,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKPgfbHW4AAfZON.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 611,
        "resize" : "fit",
        "w" : 924
      }, {
        "h" : 611,
        "resize" : "fit",
        "w" : 924
      }, {
        "h" : 611,
        "resize" : "fit",
        "w" : 924
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/pGJB2YHxG5"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910818725892378625",
  "text" : "Which one to move my virtual machine (vbox or vdi?) to another OS or even another free virtualization solution with minimal effort? Thanks https:\/\/t.co\/pGJB2YHxG5",
  "id" : 910818725892378625,
  "created_at" : "2017-09-21 10:51:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/1xYtipJVab",
      "expanded_url" : "https:\/\/reut.rs\/2xTC6Qi",
      "display_url" : "reut.rs\/2xTC6Qi"
    } ]
  },
  "geo" : { },
  "id_str" : "910758035773116416",
  "text" : "Google to acquire HTC's Pixel smartphone division in $1.1 billion deal https:\/\/t.co\/1xYtipJVab",
  "id" : 910758035773116416,
  "created_at" : "2017-09-21 06:50:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/O7IQfIe4bn",
      "expanded_url" : "https:\/\/support.google.com\/analytics\/topic\/6009743?hl=en&ref_topic=1007027",
      "display_url" : "support.google.com\/analytics\/topi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910753146225471488",
  "text" : "Google Analytics  User-ID features enables you to follow users across devices when logged on to your site. https:\/\/t.co\/O7IQfIe4bn",
  "id" : 910753146225471488,
  "created_at" : "2017-09-21 06:30:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/XCm1UaGqor",
      "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/910539918480461824",
      "display_url" : "twitter.com\/googleanalytic\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910750383852675073",
  "text" : "\"If you're a Google Analytics 360 ( $150k\/yr) client who wants to do current-day analysis\" https:\/\/t.co\/XCm1UaGqor",
  "id" : 910750383852675073,
  "created_at" : "2017-09-21 06:19:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "indices" : [ 3, 17 ],
      "id_str" : "3686936548",
      "id" : 3686936548
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910520163996438528",
  "text" : "RT @iworkwithdata: 3. you still had a lot of problems configuring students\u2019 systems. I have a solution for you. Interested? Message me.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910518689727905792",
    "text" : "3. you still had a lot of problems configuring students\u2019 systems. I have a solution for you. Interested? Message me.",
    "id" : 910518689727905792,
    "created_at" : "2017-09-20 14:59:06 +0000",
    "user" : {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "protected" : true,
      "id_str" : "3686936548",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647779320430202880\/04Kghk7g_normal.jpg",
      "id" : 3686936548,
      "verified" : false
    }
  },
  "id" : 910520163996438528,
  "created_at" : "2017-09-20 15:04:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "indices" : [ 3, 17 ],
      "id_str" : "3686936548",
      "id" : 3686936548
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910520140105580546",
  "text" : "RT @iworkwithdata: 2. For instructor\/lecturer even though you told the students what they needed well in advance of the class,",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910518634400960513",
    "text" : "2. For instructor\/lecturer even though you told the students what they needed well in advance of the class,",
    "id" : 910518634400960513,
    "created_at" : "2017-09-20 14:58:53 +0000",
    "user" : {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "protected" : true,
      "id_str" : "3686936548",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647779320430202880\/04Kghk7g_normal.jpg",
      "id" : 3686936548,
      "verified" : false
    }
  },
  "id" : 910520140105580546,
  "created_at" : "2017-09-20 15:04:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "indices" : [ 3, 17 ],
      "id_str" : "3686936548",
      "id" : 3686936548
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910520126189031424",
  "text" : "RT @iworkwithdata: 1. Setting up a data science environment is difficult.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910518543384539137",
    "text" : "1. Setting up a data science environment is difficult.",
    "id" : 910518543384539137,
    "created_at" : "2017-09-20 14:58:32 +0000",
    "user" : {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "protected" : true,
      "id_str" : "3686936548",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647779320430202880\/04Kghk7g_normal.jpg",
      "id" : 3686936548,
      "verified" : false
    }
  },
  "id" : 910520126189031424,
  "created_at" : "2017-09-20 15:04:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wilfred",
      "screen_name" : "wilfredphua",
      "indices" : [ 3, 15 ],
      "id_str" : "800887",
      "id" : 800887
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910519877311586305",
  "text" : "RT @wilfredphua: Does anyone have recommendations on what to see\/do\/eat in Vienna? Asking for a spouse \uD83D\uDE02",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910455167631319040",
    "text" : "Does anyone have recommendations on what to see\/do\/eat in Vienna? Asking for a spouse \uD83D\uDE02",
    "id" : 910455167631319040,
    "created_at" : "2017-09-20 10:46:42 +0000",
    "user" : {
      "name" : "Wilfred",
      "screen_name" : "wilfredphua",
      "protected" : false,
      "id_str" : "800887",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2672050077\/0d97e30669671a5959f840ede81c4e55_normal.jpeg",
      "id" : 800887,
      "verified" : false
    }
  },
  "id" : 910519877311586305,
  "created_at" : "2017-09-20 15:03:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "coding2share",
      "screen_name" : "coding2share",
      "indices" : [ 3, 16 ],
      "id_str" : "852916435462623234",
      "id" : 852916435462623234
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenScience",
      "indices" : [ 127, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/vuaPYDFtZT",
      "expanded_url" : "http:\/\/bit.ly\/2f69rMg",
      "display_url" : "bit.ly\/2f69rMg"
    } ]
  },
  "geo" : { },
  "id_str" : "910508511251107840",
  "text" : "RT @coding2share: Can u do that again? How reproducible is your research? Take a survey 2 let us know! https:\/\/t.co\/vuaPYDFtZT #OpenScience\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "OpenScience",
        "indices" : [ 109, 121 ]
      }, {
        "text" : "Reproducibility",
        "indices" : [ 122, 138 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/vuaPYDFtZT",
        "expanded_url" : "http:\/\/bit.ly\/2f69rMg",
        "display_url" : "bit.ly\/2f69rMg"
      } ]
    },
    "geo" : { },
    "id_str" : "910171716109946881",
    "text" : "Can u do that again? How reproducible is your research? Take a survey 2 let us know! https:\/\/t.co\/vuaPYDFtZT #OpenScience #Reproducibility",
    "id" : 910171716109946881,
    "created_at" : "2017-09-19 16:00:21 +0000",
    "user" : {
      "name" : "coding2share",
      "screen_name" : "coding2share",
      "protected" : false,
      "id_str" : "852916435462623234",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/862330483086110720\/o8pYJXJO_normal.jpg",
      "id" : 852916435462623234,
      "verified" : false
    }
  },
  "id" : 910508511251107840,
  "created_at" : "2017-09-20 14:18:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/onduo9oUwP",
      "expanded_url" : "https:\/\/www.irishtimes.com\/life-and-style\/people\/we-didn-t-come-to-ireland-to-claim-benefits-that-would-be-an-embarrassment-1.3220887#.WcJzE5-P1bw.twitter",
      "display_url" : "irishtimes.com\/life-and-style\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910502452578930688",
  "text" : "\u2018We didn\u2019t come to Ireland to claim benefits. That would be an embarrassment\u2019 https:\/\/t.co\/onduo9oUwP",
  "id" : 910502452578930688,
  "created_at" : "2017-09-20 13:54:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/ZSm0a4qf2v",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/910496984833896449",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910498414823198720",
  "text" : "I struggled with dual boot and the endless restart and access to BIOS and all my Google chrome bookmarks gone! https:\/\/t.co\/ZSm0a4qf2v",
  "id" : 910498414823198720,
  "created_at" : "2017-09-20 13:38:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/zxA14dgJAE",
      "expanded_url" : "https:\/\/itsfoss.com\/no-grub-windows-linux\/",
      "display_url" : "itsfoss.com\/no-grub-window\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910496984833896449",
  "text" : "I am using Windows 8 and I sidestep this problem by running Ubuntu in Virtual Machine. https:\/\/t.co\/zxA14dgJAE",
  "id" : 910496984833896449,
  "created_at" : "2017-09-20 13:32:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenhouseDublin",
      "indices" : [ 23, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/QI4n3ih303",
      "expanded_url" : "https:\/\/flic.kr\/p\/YFc6k6",
      "display_url" : "flic.kr\/p\/YFc6k6"
    } ]
  },
  "geo" : { },
  "id_str" : "910447082103812096",
  "text" : "Mansion House, Dublin. #OpenhouseDublin https:\/\/t.co\/QI4n3ih303",
  "id" : 910447082103812096,
  "created_at" : "2017-09-20 10:14:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/Vaufbcec7B",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909930811851444224",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910275105724538880",
  "text" : "No headway booting up 2 different OS on one disk space. Using virtual machine to run Ubuntu within Windows instead. https:\/\/t.co\/Vaufbcec7B",
  "id" : 910275105724538880,
  "created_at" : "2017-09-19 22:51:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910243113670758400",
  "text" : "Yes!\uD83D\uDE02 It Bridget &amp; Eamon on TV now.",
  "id" : 910243113670758400,
  "created_at" : "2017-09-19 20:44:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chee Aun \uD83E\uDD84",
      "screen_name" : "cheeaun",
      "indices" : [ 3, 11 ],
      "id_str" : "76993",
      "id" : 76993
    }, {
      "name" : "Paul Bakaus",
      "screen_name" : "pbakaus",
      "indices" : [ 53, 61 ],
      "id_str" : "12999402",
      "id" : 12999402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/MYQ8cPy5pT",
      "expanded_url" : "https:\/\/events.withgoogle.com\/amp-roadshow-singapore-oct2017\/",
      "display_url" : "events.withgoogle.com\/amp-roadshow-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "910206587524050944",
  "text" : "RT @cheeaun: AMP Roadshow Singapore! Finally can see @pbakaus https:\/\/t.co\/MYQ8cPy5pT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Paul Bakaus",
        "screen_name" : "pbakaus",
        "indices" : [ 40, 48 ],
        "id_str" : "12999402",
        "id" : 12999402
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 72 ],
        "url" : "https:\/\/t.co\/MYQ8cPy5pT",
        "expanded_url" : "https:\/\/events.withgoogle.com\/amp-roadshow-singapore-oct2017\/",
        "display_url" : "events.withgoogle.com\/amp-roadshow-s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "909639839678525440",
    "text" : "AMP Roadshow Singapore! Finally can see @pbakaus https:\/\/t.co\/MYQ8cPy5pT",
    "id" : 909639839678525440,
    "created_at" : "2017-09-18 04:46:52 +0000",
    "user" : {
      "name" : "Chee Aun \uD83E\uDD84",
      "screen_name" : "cheeaun",
      "protected" : false,
      "id_str" : "76993",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/732980057715531785\/RFc1gIv0_normal.jpg",
      "id" : 76993,
      "verified" : false
    }
  },
  "id" : 910206587524050944,
  "created_at" : "2017-09-19 18:18:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Min-Liang Tan",
      "screen_name" : "minliangtan",
      "indices" : [ 3, 15 ],
      "id_str" : "26906309",
      "id" : 26906309
    }, {
      "name" : "R \u039B Z \u039E R",
      "screen_name" : "Razer",
      "indices" : [ 29, 35 ],
      "id_str" : "15880163",
      "id" : 15880163
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "910204806068875264",
  "text" : "RT @minliangtan: Celebrating @Razer\u2019s 8 million Facebook fan milestone with some giveaways. Win a Razer Mercury suite here, and Gunmetal ov\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "R \u039B Z \u039E R",
        "screen_name" : "Razer",
        "indices" : [ 12, 18 ],
        "id_str" : "15880163",
        "id" : 15880163
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/minliangtan\/status\/910179188061196288\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/MXl5IUfzS9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DKGPCJbV4AErrwv.jpg",
        "id_str" : "910165866758660097",
        "id" : 910165866758660097,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKGPCJbV4AErrwv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 356,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MXl5IUfzS9"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "910179188061196288",
    "text" : "Celebrating @Razer\u2019s 8 million Facebook fan milestone with some giveaways. Win a Razer Mercury suite here, and Gunmetal over at my FB! https:\/\/t.co\/MXl5IUfzS9",
    "id" : 910179188061196288,
    "created_at" : "2017-09-19 16:30:03 +0000",
    "user" : {
      "name" : "Min-Liang Tan",
      "screen_name" : "minliangtan",
      "protected" : false,
      "id_str" : "26906309",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/530479661294551040\/PQf8P493_normal.jpeg",
      "id" : 26906309,
      "verified" : true
    }
  },
  "id" : 910204806068875264,
  "created_at" : "2017-09-19 18:11:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909930811851444224\/photo\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/oYDjY2bGtS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKC5MOxXcAA35zM.jpg",
      "id_str" : "909930744503562240",
      "id" : 909930744503562240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKC5MOxXcAA35zM.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oYDjY2bGtS"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909930811851444224",
  "text" : "To me, using Ubuntu OS is like using Apple OS https:\/\/t.co\/oYDjY2bGtS",
  "id" : 909930811851444224,
  "created_at" : "2017-09-19 00:03:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Resin.io",
      "screen_name" : "resin_io",
      "indices" : [ 38, 47 ],
      "id_str" : "1962817616",
      "id" : 1962817616
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "etcher",
      "indices" : [ 27, 34 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/bRMxPUTVce",
      "expanded_url" : "https:\/\/etcher.io",
      "display_url" : "etcher.io"
    } ]
  },
  "geo" : { },
  "id_str" : "909922904674009090",
  "text" : "Just flashed an image with #etcher by @resin_io! Check it out: https:\/\/t.co\/bRMxPUTVce",
  "id" : 909922904674009090,
  "created_at" : "2017-09-18 23:31:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909921502291742720\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/R9nMFd8cQo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKCwZ8uW0AAWcnn.jpg",
      "id_str" : "909921084572618752",
      "id" : 909921084572618752,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKCwZ8uW0AAWcnn.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 455,
        "resize" : "fit",
        "w" : 839
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 455,
        "resize" : "fit",
        "w" : 839
      }, {
        "h" : 455,
        "resize" : "fit",
        "w" : 839
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/R9nMFd8cQo"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909921502291742720",
  "text" : "A very neat pleasant looking visual interface that promise to create a bootable Ubuntu USB Drive https:\/\/t.co\/R9nMFd8cQo",
  "id" : 909921502291742720,
  "created_at" : "2017-09-18 23:26:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "I am a DataScientist",
      "screen_name" : "ImDataScientist",
      "indices" : [ 3, 19 ],
      "id_str" : "1480718904",
      "id" : 1480718904
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/904811319458766856\/photo\/1",
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/asSFO67TRu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DI6JGMwUMAAqtvj.jpg",
      "id_str" : "904811314744143872",
      "id" : 904811314744143872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI6JGMwUMAAqtvj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 356,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 723,
        "resize" : "fit",
        "w" : 1380
      }, {
        "h" : 723,
        "resize" : "fit",
        "w" : 1380
      }, {
        "h" : 629,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/asSFO67TRu"
    } ],
    "hashtags" : [ {
      "text" : "technology",
      "indices" : [ 97, 108 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/IpX5TskrBZ",
      "expanded_url" : "http:\/\/wef.ch\/2wyxxZI",
      "display_url" : "wef.ch\/2wyxxZI"
    } ]
  },
  "geo" : { },
  "id_str" : "909812254958342145",
  "text" : "RT @ImDataScientist: These are the 25 most high-tech cities in the world https:\/\/t.co\/IpX5TskrBZ #technology https:\/\/t.co\/asSFO67TRu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/datasciencepakistan.com\" rel=\"nofollow\"\u003EData Pakistan\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/904811319458766856\/photo\/1",
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/asSFO67TRu",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DI6JGMwUMAAqtvj.jpg",
        "id_str" : "904811314744143872",
        "id" : 904811314744143872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI6JGMwUMAAqtvj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 356,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 723,
          "resize" : "fit",
          "w" : 1380
        }, {
          "h" : 723,
          "resize" : "fit",
          "w" : 1380
        }, {
          "h" : 629,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/asSFO67TRu"
      } ],
      "hashtags" : [ {
        "text" : "technology",
        "indices" : [ 76, 87 ]
      } ],
      "urls" : [ {
        "indices" : [ 52, 75 ],
        "url" : "https:\/\/t.co\/IpX5TskrBZ",
        "expanded_url" : "http:\/\/wef.ch\/2wyxxZI",
        "display_url" : "wef.ch\/2wyxxZI"
      } ]
    },
    "geo" : { },
    "id_str" : "909179700773359618",
    "text" : "These are the 25 most high-tech cities in the world https:\/\/t.co\/IpX5TskrBZ #technology https:\/\/t.co\/asSFO67TRu",
    "id" : 909179700773359618,
    "created_at" : "2017-09-16 22:18:27 +0000",
    "user" : {
      "name" : "I am a DataScientist",
      "screen_name" : "ImDataScientist",
      "protected" : false,
      "id_str" : "1480718904",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/589532033723957248\/lO5EOsTh_normal.jpg",
      "id" : 1480718904,
      "verified" : false
    }
  },
  "id" : 909812254958342145,
  "created_at" : "2017-09-18 16:11:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909809603495555072\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/sJmAfa7t6T",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DKBLAeLXoAA7Opc.jpg",
      "id_str" : "909809596201672704",
      "id" : 909809596201672704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DKBLAeLXoAA7Opc.jpg",
      "sizes" : [ {
        "h" : 82,
        "resize" : "fit",
        "w" : 518
      }, {
        "h" : 82,
        "resize" : "fit",
        "w" : 518
      }, {
        "h" : 82,
        "resize" : "fit",
        "w" : 518
      }, {
        "h" : 82,
        "resize" : "crop",
        "w" : 82
      }, {
        "h" : 82,
        "resize" : "fit",
        "w" : 518
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sJmAfa7t6T"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909809603495555072",
  "text" : "The peril of mistrust intuition and defer to algorithms. https:\/\/t.co\/sJmAfa7t6T",
  "id" : 909809603495555072,
  "created_at" : "2017-09-18 16:01:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/b1eppabga8",
      "expanded_url" : "http:\/\/shr.gs\/CrFgfmh",
      "display_url" : "shr.gs\/CrFgfmh"
    } ]
  },
  "geo" : { },
  "id_str" : "909803504360882176",
  "text" : "To retire is to die. --\u200APablo Casals \nPrepare to work until you are 70: rise in age for the State pension on cards https:\/\/t.co\/b1eppabga8",
  "id" : 909803504360882176,
  "created_at" : "2017-09-18 15:37:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Stefanovic",
      "screen_name" : "PeterStefanovi2",
      "indices" : [ 3, 19 ],
      "id_str" : "2441666255",
      "id" : 2441666255
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/PeterStefanovi2\/status\/909034709468028930\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/uoJHc6k2SU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJ2KPtOXkAAq2XV.jpg",
      "id_str" : "909034702241239040",
      "id" : 909034702241239040,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJ2KPtOXkAAq2XV.jpg",
      "sizes" : [ {
        "h" : 352,
        "resize" : "fit",
        "w" : 474
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 352,
        "resize" : "fit",
        "w" : 474
      }, {
        "h" : 352,
        "resize" : "fit",
        "w" : 474
      }, {
        "h" : 352,
        "resize" : "fit",
        "w" : 474
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uoJHc6k2SU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909699322362679296",
  "text" : "RT @PeterStefanovi2: If you only read one thing today make it this https:\/\/t.co\/uoJHc6k2SU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PeterStefanovi2\/status\/909034709468028930\/photo\/1",
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/uoJHc6k2SU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJ2KPtOXkAAq2XV.jpg",
        "id_str" : "909034702241239040",
        "id" : 909034702241239040,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJ2KPtOXkAAq2XV.jpg",
        "sizes" : [ {
          "h" : 352,
          "resize" : "fit",
          "w" : 474
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 474
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 474
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 474
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uoJHc6k2SU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "909034709468028930",
    "text" : "If you only read one thing today make it this https:\/\/t.co\/uoJHc6k2SU",
    "id" : 909034709468028930,
    "created_at" : "2017-09-16 12:42:18 +0000",
    "user" : {
      "name" : "Peter Stefanovic",
      "screen_name" : "PeterStefanovi2",
      "protected" : false,
      "id_str" : "2441666255",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028924032480161792\/k-VFxNmX_normal.jpg",
      "id" : 2441666255,
      "verified" : true
    }
  },
  "id" : 909699322362679296,
  "created_at" : "2017-09-18 08:43:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rudy Agovic",
      "screen_name" : "rudyagovic",
      "indices" : [ 3, 14 ],
      "id_str" : "856952755046449152",
      "id" : 856952755046449152
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ComputerVision",
      "indices" : [ 54, 69 ]
    }, {
      "text" : "ArtificialIntelligence",
      "indices" : [ 70, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/zKIdDMcqdD",
      "expanded_url" : "http:\/\/bit.ly\/2wyeQCh",
      "display_url" : "bit.ly\/2wyeQCh"
    } ]
  },
  "geo" : { },
  "id_str" : "909530637069115392",
  "text" : "RT @rudyagovic: How Face ID Works On Apple's iPhone X #ComputerVision #ArtificialIntelligence  https:\/\/t.co\/zKIdDMcqdD https:\/\/t.co\/qETrNpK\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rudyagovic\/status\/909465978752454657\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/qETrNpKhUm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJ8SfLXVYAAgorJ.jpg",
        "id_str" : "909465976588034048",
        "id" : 909465976588034048,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJ8SfLXVYAAgorJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qETrNpKhUm"
      } ],
      "hashtags" : [ {
        "text" : "ComputerVision",
        "indices" : [ 38, 53 ]
      }, {
        "text" : "ArtificialIntelligence",
        "indices" : [ 54, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/zKIdDMcqdD",
        "expanded_url" : "http:\/\/bit.ly\/2wyeQCh",
        "display_url" : "bit.ly\/2wyeQCh"
      } ]
    },
    "geo" : { },
    "id_str" : "909465978752454657",
    "text" : "How Face ID Works On Apple's iPhone X #ComputerVision #ArtificialIntelligence  https:\/\/t.co\/zKIdDMcqdD https:\/\/t.co\/qETrNpKhUm",
    "id" : 909465978752454657,
    "created_at" : "2017-09-17 17:16:01 +0000",
    "user" : {
      "name" : "Rudy Agovic",
      "screen_name" : "rudyagovic",
      "protected" : false,
      "id_str" : "856952755046449152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/856955619638685697\/tCiOYXBj_normal.jpg",
      "id" : 856952755046449152,
      "verified" : false
    }
  },
  "id" : 909530637069115392,
  "created_at" : "2017-09-17 21:32:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ivan Sim \uD83C\uDDE8\uD83C\uDDE6",
      "screen_name" : "IvanHCSim",
      "indices" : [ 3, 13 ],
      "id_str" : "26062579",
      "id" : 26062579
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909452021878657028",
  "text" : "RT @IvanHCSim: So many kubernetes and docker questions on SO are network-related. Children, don't skip your computer networking lectures in\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "896939595794857984",
    "text" : "So many kubernetes and docker questions on SO are network-related. Children, don't skip your computer networking lectures in school!",
    "id" : 896939595794857984,
    "created_at" : "2017-08-14 03:40:38 +0000",
    "user" : {
      "name" : "Ivan Sim \uD83C\uDDE8\uD83C\uDDE6",
      "screen_name" : "IvanHCSim",
      "protected" : false,
      "id_str" : "26062579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000686177585\/6e305d93cb7a3c43abd2275645306683_normal.jpeg",
      "id" : 26062579,
      "verified" : false
    }
  },
  "id" : 909452021878657028,
  "created_at" : "2017-09-17 16:20:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gary H Goodridge",
      "screen_name" : "garyhgoodridge",
      "indices" : [ 3, 18 ],
      "id_str" : "287453721",
      "id" : 287453721
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/garyhgoodridge\/status\/900908769785380866\/photo\/1",
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/0Teb9lgood",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DICrvz_XkAAq_zO.jpg",
      "id_str" : "900908763372294144",
      "id" : 900908763372294144,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DICrvz_XkAAq_zO.jpg",
      "sizes" : [ {
        "h" : 395,
        "resize" : "fit",
        "w" : 702
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 395,
        "resize" : "fit",
        "w" : 702
      }, {
        "h" : 395,
        "resize" : "fit",
        "w" : 702
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0Teb9lgood"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909447991177748480",
  "text" : "RT @garyhgoodridge: Appreciate what you have https:\/\/t.co\/0Teb9lgood",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/garyhgoodridge\/status\/900908769785380866\/photo\/1",
        "indices" : [ 25, 48 ],
        "url" : "https:\/\/t.co\/0Teb9lgood",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DICrvz_XkAAq_zO.jpg",
        "id_str" : "900908763372294144",
        "id" : 900908763372294144,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DICrvz_XkAAq_zO.jpg",
        "sizes" : [ {
          "h" : 395,
          "resize" : "fit",
          "w" : 702
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 395,
          "resize" : "fit",
          "w" : 702
        }, {
          "h" : 395,
          "resize" : "fit",
          "w" : 702
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0Teb9lgood"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "900908769785380866",
    "text" : "Appreciate what you have https:\/\/t.co\/0Teb9lgood",
    "id" : 900908769785380866,
    "created_at" : "2017-08-25 02:32:43 +0000",
    "user" : {
      "name" : "Gary H Goodridge",
      "screen_name" : "garyhgoodridge",
      "protected" : false,
      "id_str" : "287453721",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030223174133145600\/-3vy3dQJ_normal.jpg",
      "id" : 287453721,
      "verified" : true
    }
  },
  "id" : 909447991177748480,
  "created_at" : "2017-09-17 16:04:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/lyQU44sRcU",
      "expanded_url" : "http:\/\/unthinkable.fm\/data-creativity-stories-predictive-analytics-expert-rapper-ex-disney-behavior-expert\/",
      "display_url" : "unthinkable.fm\/data-creativit\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "909446835114319872",
  "text" : "Customers who personalize something about their purchase are more likely to show up! https:\/\/t.co\/lyQU44sRcU",
  "id" : 909446835114319872,
  "created_at" : "2017-09-17 15:59:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/ZVbZpH4T9J",
      "expanded_url" : "https:\/\/twitter.com\/gianniponzi\/status\/909361943273725952",
      "display_url" : "twitter.com\/gianniponzi\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "909436365141610497",
  "text" : "Day 176 - How was our service? \nAnother email from eir, wondering how their service was. Yeah, right. \uD83D\uDE2C https:\/\/t.co\/ZVbZpH4T9J",
  "id" : 909436365141610497,
  "created_at" : "2017-09-17 15:18:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909425256078733313\/photo\/1",
      "indices" : [ 140, 163 ],
      "url" : "https:\/\/t.co\/ikaMtQTMtU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJ7tc0YWsAAttRp.jpg",
      "id_str" : "909425254128332800",
      "id" : 909425254128332800,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJ7tc0YWsAAttRp.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ikaMtQTMtU"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/v49yIyjQFF",
      "expanded_url" : "http:\/\/ift.tt\/2wyJYl4",
      "display_url" : "ift.tt\/2wyJYl4"
    } ]
  },
  "geo" : { },
  "id_str" : "909425256078733313",
  "text" : "Step out of the house and seemd everyone wear blue in my neighborhood. Turn on the tv just to listen to Ireland na\u2026 https:\/\/t.co\/v49yIyjQFF https:\/\/t.co\/ikaMtQTMtU",
  "id" : 909425256078733313,
  "created_at" : "2017-09-17 14:34:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 127, 150 ],
      "url" : "https:\/\/t.co\/bQUYSeEC00",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909331341971460096",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "909350993745317888",
  "text" : "At USD 15,000, that an investment to design and train your model to detect and identify \u201CEmpty\u201D and \u201COccupied\u201D Parking spots.\uD83D\uDE01 https:\/\/t.co\/bQUYSeEC00",
  "id" : 909350993745317888,
  "created_at" : "2017-09-17 09:39:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/909338214376329216\/photo\/1",
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/x4Kb7Jn2C6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJ6d7yXWAAEnI7_.jpg",
      "id_str" : "909337825232945153",
      "id" : 909337825232945153,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJ6d7yXWAAEnI7_.jpg",
      "sizes" : [ {
        "h" : 653,
        "resize" : "fit",
        "w" : 1297
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 342,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 653,
        "resize" : "fit",
        "w" : 1297
      }, {
        "h" : 604,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/x4Kb7Jn2C6"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/fuOYCnHIvH",
      "expanded_url" : "https:\/\/support.google.com\/merchants\/answer\/6149970?visit_id=1-636200856174756068-2703932183&rd=1",
      "display_url" : "support.google.com\/merchants\/answ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "909338214376329216",
  "text" : "Google shopping ads arrives in Ireland. A list of do\u2019s and don\u2019ts https:\/\/t.co\/fuOYCnHIvH https:\/\/t.co\/x4Kb7Jn2C6",
  "id" : 909338214376329216,
  "created_at" : "2017-09-17 08:48:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/67gKF0aRyx",
      "expanded_url" : "http:\/\/docs.nvidia.com\/dgx\/digits-devbox-user-guide\/index.html",
      "display_url" : "docs.nvidia.com\/dgx\/digits-dev\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "909331341971460096",
  "text" : "The hardware requirement for someone embarking on a deep learning project also need a deep pocket. https:\/\/t.co\/67gKF0aRyx",
  "id" : 909331341971460096,
  "created_at" : "2017-09-17 08:21:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909307919061127169",
  "text" : "15 minutes to fill up an online form. Thank you for having this \"Save As Draft\" feature so I can come back latter,",
  "id" : 909307919061127169,
  "created_at" : "2017-09-17 06:47:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/FGngvMrnxn",
      "expanded_url" : "http:\/\/www.speedtest.net\/global-index",
      "display_url" : "speedtest.net\/global-index"
    } ]
  },
  "geo" : { },
  "id_str" : "909302411012997120",
  "text" : "See how your country\u2019s internet speeds stack up in this new monthly global ranking. https:\/\/t.co\/FGngvMrnxn",
  "id" : 909302411012997120,
  "created_at" : "2017-09-17 06:26:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "the grugq",
      "screen_name" : "thegrugq",
      "indices" : [ 3, 12 ],
      "id_str" : "18983429",
      "id" : 18983429
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "909189704653983744",
  "text" : "RT @thegrugq: Some of the best people in infosec have no degree, no high school diploma, or degrees in unrelated fields. This is data point\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/J1OU3lcyst",
        "expanded_url" : "https:\/\/twitter.com\/Freeyourmindkid\/status\/908739486967791621",
        "display_url" : "twitter.com\/Freeyourmindki\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "908781735135010816",
    "text" : "Some of the best people in infosec have no degree, no high school diploma, or degrees in unrelated fields. This is data point is irrelevant. https:\/\/t.co\/J1OU3lcyst",
    "id" : 908781735135010816,
    "created_at" : "2017-09-15 19:57:04 +0000",
    "user" : {
      "name" : "the grugq",
      "screen_name" : "thegrugq",
      "protected" : false,
      "id_str" : "18983429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/534977903377666048\/yWe1PyiZ_normal.jpeg",
      "id" : 18983429,
      "verified" : false
    }
  },
  "id" : 909189704653983744,
  "created_at" : "2017-09-16 22:58:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Spectator Life",
      "screen_name" : "Spectator_LIFE",
      "indices" : [ 3, 18 ],
      "id_str" : "608241675",
      "id" : 608241675
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/tNCjznZGGm",
      "expanded_url" : "http:\/\/specc.ie\/2wEv9Ps",
      "display_url" : "specc.ie\/2wEv9Ps"
    } ]
  },
  "geo" : { },
  "id_str" : "909118366618673152",
  "text" : "RT @Spectator_LIFE: Everything you need to know before saddling up for a Great British cycling holiday https:\/\/t.co\/tNCjznZGGm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/tNCjznZGGm",
        "expanded_url" : "http:\/\/specc.ie\/2wEv9Ps",
        "display_url" : "specc.ie\/2wEv9Ps"
      } ]
    },
    "geo" : { },
    "id_str" : "906223119119081472",
    "text" : "Everything you need to know before saddling up for a Great British cycling holiday https:\/\/t.co\/tNCjznZGGm",
    "id" : 906223119119081472,
    "created_at" : "2017-09-08 18:30:03 +0000",
    "user" : {
      "name" : "Spectator Life",
      "screen_name" : "Spectator_LIFE",
      "protected" : false,
      "id_str" : "608241675",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/842058830594875392\/UbShK320_normal.jpg",
      "id" : 608241675,
      "verified" : false
    }
  },
  "id" : 909118366618673152,
  "created_at" : "2017-09-16 18:14:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "indices" : [ 3, 16 ],
      "id_str" : "768968",
      "id" : 768968
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DHL",
      "indices" : [ 38, 42 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/0S9gStcHAv",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BZGMWVonUeT\/",
      "display_url" : "instagram.com\/p\/BZGMWVonUeT\/"
    } ]
  },
  "geo" : { },
  "id_str" : "908985637713465344",
  "text" : "RT @coolinsights: What a clever ad by #DHL! @ Singapore https:\/\/t.co\/0S9gStcHAv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DHL",
        "indices" : [ 20, 24 ]
      } ],
      "urls" : [ {
        "indices" : [ 38, 61 ],
        "url" : "https:\/\/t.co\/0S9gStcHAv",
        "expanded_url" : "https:\/\/www.instagram.com\/p\/BZGMWVonUeT\/",
        "display_url" : "instagram.com\/p\/BZGMWVonUeT\/"
      } ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 1.29306, 103.856 ]
    },
    "id_str" : "908984939151896576",
    "text" : "What a clever ad by #DHL! @ Singapore https:\/\/t.co\/0S9gStcHAv",
    "id" : 908984939151896576,
    "created_at" : "2017-09-16 09:24:32 +0000",
    "user" : {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "protected" : false,
      "id_str" : "768968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665181228841046016\/dvZ4PiEU_normal.jpg",
      "id" : 768968,
      "verified" : false
    }
  },
  "id" : 908985637713465344,
  "created_at" : "2017-09-16 09:27:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Levy",
      "screen_name" : "justsml",
      "indices" : [ 3, 11 ],
      "id_str" : "34569742",
      "id" : 34569742
    }, {
      "name" : "Portainer.io",
      "screen_name" : "portainerio",
      "indices" : [ 13, 25 ],
      "id_str" : "779234043669774336",
      "id" : 779234043669774336
    }, {
      "name" : "Gor",
      "screen_name" : "goranhalusa",
      "indices" : [ 26, 38 ],
      "id_str" : "328458650",
      "id" : 328458650
    }, {
      "name" : "Docker",
      "screen_name" : "Docker",
      "indices" : [ 47, 54 ],
      "id_str" : "1138959692",
      "id" : 1138959692
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908939148517629952",
  "text" : "RT @justsml: @portainerio @goranhalusa Clearly @Docker made the critical error of over-budgeting an attorney. \n\nJust wait until they find o\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Portainer.io",
        "screen_name" : "portainerio",
        "indices" : [ 0, 12 ],
        "id_str" : "779234043669774336",
        "id" : 779234043669774336
      }, {
        "name" : "Gor",
        "screen_name" : "goranhalusa",
        "indices" : [ 13, 25 ],
        "id_str" : "328458650",
        "id" : 328458650
      }, {
        "name" : "Docker",
        "screen_name" : "Docker",
        "indices" : [ 34, 41 ],
        "id_str" : "1138959692",
        "id" : 1138959692
      }, {
        "name" : "Dockers",
        "screen_name" : "Dockers",
        "indices" : [ 135, 143 ],
        "id_str" : "97016987",
        "id" : 97016987
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "908871872950841344",
    "geo" : { },
    "id_str" : "908891288300224512",
    "in_reply_to_user_id" : 779234043669774336,
    "text" : "@portainerio @goranhalusa Clearly @Docker made the critical error of over-budgeting an attorney. \n\nJust wait until they find out about @Dockers slacks\/pants. \uD83D\uDE02",
    "id" : 908891288300224512,
    "in_reply_to_status_id" : 908871872950841344,
    "created_at" : "2017-09-16 03:12:24 +0000",
    "in_reply_to_screen_name" : "portainerio",
    "in_reply_to_user_id_str" : "779234043669774336",
    "user" : {
      "name" : "Dan Levy",
      "screen_name" : "justsml",
      "protected" : false,
      "id_str" : "34569742",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/909245748625080320\/lsGX9OYY_normal.jpg",
      "id" : 34569742,
      "verified" : false
    }
  },
  "id" : 908939148517629952,
  "created_at" : "2017-09-16 06:22:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LinkedIn",
      "screen_name" : "LinkedIn",
      "indices" : [ 65, 74 ],
      "id_str" : "13058772",
      "id" : 13058772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/EK6xQAI0al",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/mathematics-machine-learning-wale-akinfaderin",
      "display_url" : "linkedin.com\/pulse\/mathemat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "908749909406441472",
  "text" : "\"The Mathematics of Machine Learning\" https:\/\/t.co\/EK6xQAI0al on @LinkedIn",
  "id" : 908749909406441472,
  "created_at" : "2017-09-15 17:50:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908745746043883520",
  "text" : "Should I read this book? I always checkout the Amazon Review first.",
  "id" : 908745746043883520,
  "created_at" : "2017-09-15 17:34:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908741571994165248",
  "text" : "Do you keep track of what you read? If yes, how?",
  "id" : 908741571994165248,
  "created_at" : "2017-09-15 17:17:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/LJLbfBO7y4",
      "expanded_url" : "https:\/\/uk.pcpartpicker.com\/guide\/#sort=a2&page=1",
      "display_url" : "uk.pcpartpicker.com\/guide\/#sort=a2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "908642215970385921",
  "text" : "I am not well verse with hardware. Any gaming setup should be good for data analysis?  https:\/\/t.co\/LJLbfBO7y4",
  "id" : 908642215970385921,
  "created_at" : "2017-09-15 10:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/jcvUiRpbDH",
      "expanded_url" : "https:\/\/twitter.com\/scottcowley\/status\/908422732764913664",
      "display_url" : "twitter.com\/scottcowley\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "908631011969388544",
  "text" : "Looking for a new pen? Personal fav is  Pilot G-2 0.5mm https:\/\/t.co\/jcvUiRpbDH",
  "id" : 908631011969388544,
  "created_at" : "2017-09-15 09:58:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SimonNRicketts",
      "screen_name" : "SimonNRicketts",
      "indices" : [ 3, 18 ],
      "id_str" : "18991263",
      "id" : 18991263
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/NatureisScary\/status\/778777668162613248\/video\/1",
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/Md58EXOSRV",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/778777619810643968\/pu\/img\/OWaR2k3Be8gAXe4l.jpg",
      "id_str" : "778777619810643968",
      "id" : 778777619810643968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/778777619810643968\/pu\/img\/OWaR2k3Be8gAXe4l.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Md58EXOSRV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908451474472128513",
  "text" : "RT @SimonNRicketts: How fast are hippos in water? This fast. https:\/\/t.co\/Md58EXOSRV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/NatureisScary\/status\/778777668162613248\/video\/1",
        "indices" : [ 41, 64 ],
        "url" : "https:\/\/t.co\/Md58EXOSRV",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/778777619810643968\/pu\/img\/OWaR2k3Be8gAXe4l.jpg",
        "id_str" : "778777619810643968",
        "id" : 778777619810643968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/778777619810643968\/pu\/img\/OWaR2k3Be8gAXe4l.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Md58EXOSRV"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "908316073824571392",
    "text" : "How fast are hippos in water? This fast. https:\/\/t.co\/Md58EXOSRV",
    "id" : 908316073824571392,
    "created_at" : "2017-09-14 13:06:42 +0000",
    "user" : {
      "name" : "SimonNRicketts",
      "screen_name" : "SimonNRicketts",
      "protected" : false,
      "id_str" : "18991263",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000759409678\/0f9477b787db017cfb2c0c4aac78cbf3_normal.jpeg",
      "id" : 18991263,
      "verified" : false
    }
  },
  "id" : 908451474472128513,
  "created_at" : "2017-09-14 22:04:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IHME at UW",
      "screen_name" : "IHME_UW",
      "indices" : [ 3, 11 ],
      "id_str" : "76780096",
      "id" : 76780096
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SDG",
      "indices" : [ 120, 124 ]
    }, {
      "text" : "GBDstudy",
      "indices" : [ 125, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908417949081403392",
  "text" : "RT @IHME_UW: Singapore, Iceland &amp; Sweden rank highest. Somalia, CAR &amp; Afghanistan rank lowest on health-related #SDG #GBDstudy https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SDG",
        "indices" : [ 107, 111 ]
      }, {
        "text" : "GBDstudy",
        "indices" : [ 112, 121 ]
      } ],
      "urls" : [ {
        "indices" : [ 122, 145 ],
        "url" : "https:\/\/t.co\/lpRVzUwGKM",
        "expanded_url" : "http:\/\/www.healthdata.org\/news-release\/ihme-releases-second-annual-report-sustainable-development-goal-indicators",
        "display_url" : "healthdata.org\/news-release\/i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "907997371128311809",
    "text" : "Singapore, Iceland &amp; Sweden rank highest. Somalia, CAR &amp; Afghanistan rank lowest on health-related #SDG #GBDstudy https:\/\/t.co\/lpRVzUwGKM",
    "id" : 907997371128311809,
    "created_at" : "2017-09-13 16:00:17 +0000",
    "user" : {
      "name" : "IHME at UW",
      "screen_name" : "IHME_UW",
      "protected" : false,
      "id_str" : "76780096",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/509474832983920640\/ijAkDalA_normal.png",
      "id" : 76780096,
      "verified" : false
    }
  },
  "id" : 908417949081403392,
  "created_at" : "2017-09-14 19:51:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Algobeans",
      "screen_name" : "algobeans",
      "indices" : [ 3, 13 ],
      "id_str" : "759408783055462402",
      "id" : 759408783055462402
    }, {
      "name" : "Raspberry Pi",
      "screen_name" : "Raspberry_Pi",
      "indices" : [ 15, 28 ],
      "id_str" : "302666251",
      "id" : 302666251
    }, {
      "name" : "Mag P1",
      "screen_name" : "TheMagP1",
      "indices" : [ 40, 49 ],
      "id_str" : "992751596458201089",
      "id" : 992751596458201089
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DataScience",
      "indices" : [ 77, 89 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908416959821369345",
  "text" : "RT @algobeans: @Raspberry_Pi's magazine @TheMagP1 rated the book 'Numsense!  #DataScience for the Layman' 5\u2B50\uFE0F! Grab your copy now:  https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Raspberry Pi",
        "screen_name" : "Raspberry_Pi",
        "indices" : [ 0, 13 ],
        "id_str" : "302666251",
        "id" : 302666251
      }, {
        "name" : "Mag P1",
        "screen_name" : "TheMagP1",
        "indices" : [ 25, 34 ],
        "id_str" : "992751596458201089",
        "id" : 992751596458201089
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/algobeans\/status\/903279024494977024\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/Ku8dtnV6J3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DIkXdkPV4AAXTYk.jpg",
        "id_str" : "903278996976164864",
        "id" : 903278996976164864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIkXdkPV4AAXTYk.jpg",
        "sizes" : [ {
          "h" : 1398,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 584
        }, {
          "h" : 1398,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1030
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ku8dtnV6J3"
      } ],
      "hashtags" : [ {
        "text" : "DataScience",
        "indices" : [ 62, 74 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/gusIrL8rja",
        "expanded_url" : "http:\/\/getbook.at\/numsense",
        "display_url" : "getbook.at\/numsense"
      } ]
    },
    "geo" : { },
    "id_str" : "903279024494977024",
    "in_reply_to_user_id" : 302666251,
    "text" : "@Raspberry_Pi's magazine @TheMagP1 rated the book 'Numsense!  #DataScience for the Layman' 5\u2B50\uFE0F! Grab your copy now:  https:\/\/t.co\/gusIrL8rja https:\/\/t.co\/Ku8dtnV6J3",
    "id" : 903279024494977024,
    "created_at" : "2017-08-31 15:31:16 +0000",
    "in_reply_to_screen_name" : "Raspberry_Pi",
    "in_reply_to_user_id_str" : "302666251",
    "user" : {
      "name" : "Algobeans",
      "screen_name" : "algobeans",
      "protected" : false,
      "id_str" : "759408783055462402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/903279189779931137\/Fqdx_9N4_normal.jpg",
      "id" : 759408783055462402,
      "verified" : false
    }
  },
  "id" : 908416959821369345,
  "created_at" : "2017-09-14 19:47:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/tykWlCShkA",
      "expanded_url" : "http:\/\/www.pardot.com\/buyer-journey\/",
      "display_url" : "pardot.com\/buyer-journey\/"
    } ]
  },
  "geo" : { },
  "id_str" : "908415758128041985",
  "text" : "Excellent explanations of the customer journey https:\/\/t.co\/tykWlCShkA",
  "id" : 908415758128041985,
  "created_at" : "2017-09-14 19:42:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DPBill",
      "indices" : [ 24, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908412244194717696",
  "text" : "RT @EmeraldDeLeeuw: The #DPBill is incredibly badly drafted. Painful to read. Re-draft required for non-legal folks to make sense of their\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DPBill",
        "indices" : [ 4, 11 ]
      }, {
        "text" : "gdpr",
        "indices" : [ 127, 132 ]
      }, {
        "text" : "brexit",
        "indices" : [ 133, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "908392728677019648",
    "text" : "The #DPBill is incredibly badly drafted. Painful to read. Re-draft required for non-legal folks to make sense of their rights. #gdpr #brexit",
    "id" : 908392728677019648,
    "created_at" : "2017-09-14 18:11:18 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 908412244194717696,
  "created_at" : "2017-09-14 19:28:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jose Proenca",
      "screen_name" : "plime42",
      "indices" : [ 3, 11 ],
      "id_str" : "22466795",
      "id" : 22466795
    }, {
      "name" : "Yoroomie",
      "screen_name" : "Yoroomie",
      "indices" : [ 16, 25 ],
      "id_str" : "2778655470",
      "id" : 2778655470
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Yoroomie\/status\/908063410549485568\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/Ubx8pskagB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJoWO6cUMAABxj5.jpg",
      "id_str" : "908062720330575872",
      "id" : 908062720330575872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJoWO6cUMAABxj5.jpg",
      "sizes" : [ {
        "h" : 613,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 347,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1002,
        "resize" : "fit",
        "w" : 1962
      }, {
        "h" : 1002,
        "resize" : "fit",
        "w" : 1962
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Ubx8pskagB"
    } ],
    "hashtags" : [ {
      "text" : "startup",
      "indices" : [ 47, 55 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908305297596452865",
  "text" : "RT @plime42: RT @Yoroomie: Tools to build your #startup MVP with minimal code \uD83D\uDE80 https:\/\/t.co\/Ubx8pskagB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yoroomie",
        "screen_name" : "Yoroomie",
        "indices" : [ 3, 12 ],
        "id_str" : "2778655470",
        "id" : 2778655470
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Yoroomie\/status\/908063410549485568\/photo\/1",
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/Ubx8pskagB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJoWO6cUMAABxj5.jpg",
        "id_str" : "908062720330575872",
        "id" : 908062720330575872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJoWO6cUMAABxj5.jpg",
        "sizes" : [ {
          "h" : 613,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 347,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1002,
          "resize" : "fit",
          "w" : 1962
        }, {
          "h" : 1002,
          "resize" : "fit",
          "w" : 1962
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ubx8pskagB"
      } ],
      "hashtags" : [ {
        "text" : "startup",
        "indices" : [ 34, 42 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "908091137029795840",
    "text" : "RT @Yoroomie: Tools to build your #startup MVP with minimal code \uD83D\uDE80 https:\/\/t.co\/Ubx8pskagB",
    "id" : 908091137029795840,
    "created_at" : "2017-09-13 22:12:53 +0000",
    "user" : {
      "name" : "Jose Proenca",
      "screen_name" : "plime42",
      "protected" : false,
      "id_str" : "22466795",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/563950946938400768\/uiplWK8w_normal.jpeg",
      "id" : 22466795,
      "verified" : false
    }
  },
  "id" : 908305297596452865,
  "created_at" : "2017-09-14 12:23:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/v0EZOfNZ1A",
      "expanded_url" : "https:\/\/www.saga.co.uk\/magazine\/motoring\/cars\/selling\/selling-cars-on-public-road",
      "display_url" : "saga.co.uk\/magazine\/motor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "908274709908901888",
  "text" : "In  UK, you will be given fine by local authority if you sell your car on the roadside. https:\/\/t.co\/v0EZOfNZ1A",
  "id" : 908274709908901888,
  "created_at" : "2017-09-14 10:22:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dennis de Groot",
      "screen_name" : "punchdouble",
      "indices" : [ 3, 15 ],
      "id_str" : "7149382",
      "id" : 7149382
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908268883433463808",
  "text" : "RT @punchdouble: I'm starting a startup called Nah, that aims to disrupt the launching of startups aiming to disrupt things that don't need\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907986831832178688",
    "text" : "I'm starting a startup called Nah, that aims to disrupt the launching of startups aiming to disrupt things that don't need disrupting.",
    "id" : 907986831832178688,
    "created_at" : "2017-09-13 15:18:24 +0000",
    "user" : {
      "name" : "Dennis de Groot",
      "screen_name" : "punchdouble",
      "protected" : false,
      "id_str" : "7149382",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009737980242792448\/BZhOySBj_normal.jpg",
      "id" : 7149382,
      "verified" : false
    }
  },
  "id" : 908268883433463808,
  "created_at" : "2017-09-14 09:59:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/A27WLBY8nw",
      "expanded_url" : "https:\/\/stackoverflow.com\/questions\/16281910\/ipython-notebook-bash-magic-error",
      "display_url" : "stackoverflow.com\/questions\/1628\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "908268643187916801",
  "text" : "TIL to use %%bash magic on Windows, you need to install cygwin and run ipython from there. https:\/\/t.co\/A27WLBY8nw",
  "id" : 908268643187916801,
  "created_at" : "2017-09-14 09:58:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 3, 15 ],
      "id_str" : "19793936",
      "id" : 19793936
    }, {
      "name" : "Shopify",
      "screen_name" : "Shopify",
      "indices" : [ 48, 56 ],
      "id_str" : "17136315",
      "id" : 17136315
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "908268517404893184",
  "text" : "RT @gianniponzi: Anyone following me working at @Shopify in Galway ? Any referrals appreciated :-D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Shopify",
        "screen_name" : "Shopify",
        "indices" : [ 31, 39 ],
        "id_str" : "17136315",
        "id" : 17136315
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "908266336610082816",
    "text" : "Anyone following me working at @Shopify in Galway ? Any referrals appreciated :-D",
    "id" : 908266336610082816,
    "created_at" : "2017-09-14 09:49:04 +0000",
    "user" : {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "protected" : false,
      "id_str" : "19793936",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034160433949761536\/GdlrLPNt_normal.jpg",
      "id" : 19793936,
      "verified" : false
    }
  },
  "id" : 908268517404893184,
  "created_at" : "2017-09-14 09:57:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    }, {
      "name" : "The New York Times",
      "screen_name" : "nytimes",
      "indices" : [ 77, 85 ],
      "id_str" : "807095",
      "id" : 807095
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/Nn94yAaHWw",
      "expanded_url" : "https:\/\/www.nytimes.com\/2017\/08\/17\/business\/economy\/san-francisco-commute.html?smprod=nytcore-ipad&smid=nytcore-ipad-share",
      "display_url" : "nytimes.com\/2017\/08\/17\/bus\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "908259224110673920",
  "text" : "RT @mrbrown: A 2:15 Alarm, 2 Trains and a Bus Get Her to Work by 7 A.M., via @nytimes https:\/\/t.co\/Nn94yAaHWw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The New York Times",
        "screen_name" : "nytimes",
        "indices" : [ 64, 72 ],
        "id_str" : "807095",
        "id" : 807095
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/Nn94yAaHWw",
        "expanded_url" : "https:\/\/www.nytimes.com\/2017\/08\/17\/business\/economy\/san-francisco-commute.html?smprod=nytcore-ipad&smid=nytcore-ipad-share",
        "display_url" : "nytimes.com\/2017\/08\/17\/bus\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "908237221274173440",
    "text" : "A 2:15 Alarm, 2 Trains and a Bus Get Her to Work by 7 A.M., via @nytimes https:\/\/t.co\/Nn94yAaHWw",
    "id" : 908237221274173440,
    "created_at" : "2017-09-14 07:53:22 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 908259224110673920,
  "created_at" : "2017-09-14 09:20:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Plombo",
      "screen_name" : "_Plombo",
      "indices" : [ 3, 11 ],
      "id_str" : "1192490220",
      "id" : 1192490220
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/_Plombo\/status\/906731883437981696\/photo\/1",
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/vwZCvHsRW3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJVb064XgAAWwji.jpg",
      "id_str" : "906731864702025728",
      "id" : 906731864702025728,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJVb064XgAAWwji.jpg",
      "sizes" : [ {
        "h" : 854,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 854,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 284,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vwZCvHsRW3"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907989968965992458",
  "text" : "RT @_Plombo: What an apt description of translation this is. https:\/\/t.co\/vwZCvHsRW3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/_Plombo\/status\/906731883437981696\/photo\/1",
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/vwZCvHsRW3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJVb064XgAAWwji.jpg",
        "id_str" : "906731864702025728",
        "id" : 906731864702025728,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJVb064XgAAWwji.jpg",
        "sizes" : [ {
          "h" : 854,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 854,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 284,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vwZCvHsRW3"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906731883437981696",
    "text" : "What an apt description of translation this is. https:\/\/t.co\/vwZCvHsRW3",
    "id" : 906731883437981696,
    "created_at" : "2017-09-10 04:11:41 +0000",
    "user" : {
      "name" : "Plombo",
      "screen_name" : "_Plombo",
      "protected" : false,
      "id_str" : "1192490220",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/925870797754843136\/Tz37apg5_normal.jpg",
      "id" : 1192490220,
      "verified" : false
    }
  },
  "id" : 907989968965992458,
  "created_at" : "2017-09-13 15:30:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907989798048190464",
  "text" : "Four in a Bed - a British reality show is just a peer review that goes sour on TV?",
  "id" : 907989798048190464,
  "created_at" : "2017-09-13 15:30:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u767E\u5584\u5B5D\u4E3A\u5148",
      "indices" : [ 0, 6 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/42D1OicUEn",
      "expanded_url" : "https:\/\/twitter.com\/iheartbeijing\/status\/907983535864033280",
      "display_url" : "twitter.com\/iheartbeijing\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907983946616635392",
  "text" : "#\u767E\u5584\u5B5D\u4E3A\u5148 means Filial piety is the most important of all virtues https:\/\/t.co\/42D1OicUEn",
  "id" : 907983946616635392,
  "created_at" : "2017-09-13 15:06:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jerry",
      "screen_name" : "jerry2623",
      "indices" : [ 3, 13 ],
      "id_str" : "20921028",
      "id" : 20921028
    }, {
      "name" : "Conor Pope",
      "screen_name" : "conor_pope",
      "indices" : [ 15, 26 ],
      "id_str" : "21523363",
      "id" : 21523363
    }, {
      "name" : "Ryanair",
      "screen_name" : "Ryanair",
      "indices" : [ 116, 124 ],
      "id_str" : "1542862735",
      "id" : 1542862735
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907982688430301184",
  "text" : "RT @jerry2623: @conor_pope Yep..Can't feel sorry at all for this family.  Travel insurance is not something new and @Ryanair actively try t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Conor Pope",
        "screen_name" : "conor_pope",
        "indices" : [ 0, 11 ],
        "id_str" : "21523363",
        "id" : 21523363
      }, {
        "name" : "Ryanair",
        "screen_name" : "Ryanair",
        "indices" : [ 101, 109 ],
        "id_str" : "1542862735",
        "id" : 1542862735
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "907612210419179522",
    "geo" : { },
    "id_str" : "907643484059963399",
    "in_reply_to_user_id" : 21523363,
    "text" : "@conor_pope Yep..Can't feel sorry at all for this family.  Travel insurance is not something new and @Ryanair actively try to sell it  when booking",
    "id" : 907643484059963399,
    "in_reply_to_status_id" : 907612210419179522,
    "created_at" : "2017-09-12 16:34:04 +0000",
    "in_reply_to_screen_name" : "conor_pope",
    "in_reply_to_user_id_str" : "21523363",
    "user" : {
      "name" : "Jerry",
      "screen_name" : "jerry2623",
      "protected" : false,
      "id_str" : "20921028",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034478075713597447\/vQc2UEpQ_normal.jpg",
      "id" : 20921028,
      "verified" : false
    }
  },
  "id" : 907982688430301184,
  "created_at" : "2017-09-13 15:01:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/GRoGuMhArr",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/AArRxcM?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/AArRx\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907979395348004866",
  "text" : "I don't get it as an Asian. How on earth is this racist? https:\/\/t.co\/GRoGuMhArr",
  "id" : 907979395348004866,
  "created_at" : "2017-09-13 14:48:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "British Museum",
      "screen_name" : "britishmuseum",
      "indices" : [ 3, 17 ],
      "id_str" : "19066345",
      "id" : 19066345
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907979140627877888",
  "text" : "RT @britishmuseum: First up is Jane Portal, Keeper of Asia. She\u2019s been busy with a new gallery to redisplay objects from South Asia &amp; China\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/britishmuseum\/status\/907890680143515648\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/wLmwoXyWVC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJl5rMrX0AA4MnG.jpg",
        "id_str" : "907890582936342528",
        "id" : 907890582936342528,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJl5rMrX0AA4MnG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1607,
          "resize" : "fit",
          "w" : 1142
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 853
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 483
        }, {
          "h" : 1607,
          "resize" : "fit",
          "w" : 1142
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wLmwoXyWVC"
      } ],
      "hashtags" : [ {
        "text" : "AskACurator",
        "indices" : [ 125, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907890680143515648",
    "text" : "First up is Jane Portal, Keeper of Asia. She\u2019s been busy with a new gallery to redisplay objects from South Asia &amp; China #AskACurator https:\/\/t.co\/wLmwoXyWVC",
    "id" : 907890680143515648,
    "created_at" : "2017-09-13 08:56:20 +0000",
    "user" : {
      "name" : "British Museum",
      "screen_name" : "britishmuseum",
      "protected" : false,
      "id_str" : "19066345",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/552394937585500160\/BN7fW_Et_normal.jpeg",
      "id" : 19066345,
      "verified" : true
    }
  },
  "id" : 907979140627877888,
  "created_at" : "2017-09-13 14:47:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907971901418622977",
  "text" : "Once of the challenges with digital is just because you can measure it doesn't mean you should. Forget about counting Facebook fans &amp; Like.",
  "id" : 907971901418622977,
  "created_at" : "2017-09-13 14:19:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907969633075986432",
  "text" : "Is it just me only? Employers hiring a candidate is like NASA seeking astronauts candidates...",
  "id" : 907969633075986432,
  "created_at" : "2017-09-13 14:10:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/907969309955248128\/photo\/1",
      "indices" : [ 121, 144 ],
      "url" : "https:\/\/t.co\/i4UAAyYS0f",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJnBRK5WsAAWW3P.jpg",
      "id_str" : "907969300618719232",
      "id" : 907969300618719232,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJnBRK5WsAAWW3P.jpg",
      "sizes" : [ {
        "h" : 98,
        "resize" : "fit",
        "w" : 1094
      }, {
        "h" : 98,
        "resize" : "fit",
        "w" : 1094
      }, {
        "h" : 61,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 98,
        "resize" : "crop",
        "w" : 98
      }, {
        "h" : 98,
        "resize" : "fit",
        "w" : 1094
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/i4UAAyYS0f"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907969309955248128",
  "text" : "Run this first in Jupyter to avoid  \"Error in contrib.url(repos, \"source\"): trying to use CRAN without setting a mirror\" https:\/\/t.co\/i4UAAyYS0f",
  "id" : 907969309955248128,
  "created_at" : "2017-09-13 14:08:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "vb",
      "screen_name" : "vbzu",
      "indices" : [ 3, 8 ],
      "id_str" : "99722646",
      "id" : 99722646
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HalimahYacob",
      "indices" : [ 10, 23 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907958521295237122",
  "text" : "RT @vbzu: #HalimahYacob - Singapore\u2019s first women president has her own emoji!! \uD83D\uDC4C\uD83C\uDFFC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "HalimahYacob",
        "indices" : [ 0, 13 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907913772982968320",
    "text" : "#HalimahYacob - Singapore\u2019s first women president has her own emoji!! \uD83D\uDC4C\uD83C\uDFFC",
    "id" : 907913772982968320,
    "created_at" : "2017-09-13 10:28:06 +0000",
    "user" : {
      "name" : "vb",
      "screen_name" : "vbzu",
      "protected" : false,
      "id_str" : "99722646",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035487779549900802\/2kOQ4Qem_normal.jpg",
      "id" : 99722646,
      "verified" : true
    }
  },
  "id" : 907958521295237122,
  "created_at" : "2017-09-13 13:25:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "juan",
      "screen_name" : "juanbuis",
      "indices" : [ 3, 12 ],
      "id_str" : "36672594",
      "id" : 36672594
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/juanbuis\/status\/907883402195595265\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/K3IbdF4WNL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJlyQkzXgAAkdvd.jpg",
      "id_str" : "907882428974465024",
      "id" : 907882428974465024,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJlyQkzXgAAkdvd.jpg",
      "sizes" : [ {
        "h" : 990,
        "resize" : "fit",
        "w" : 1820
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 990,
        "resize" : "fit",
        "w" : 1820
      }, {
        "h" : 653,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/K3IbdF4WNL"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907958313224228865",
  "text" : "RT @juanbuis: buying iPhone X in europe? why not add a free weekend trip to NYC https:\/\/t.co\/K3IbdF4WNL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/juanbuis\/status\/907883402195595265\/photo\/1",
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/K3IbdF4WNL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJlyQkzXgAAkdvd.jpg",
        "id_str" : "907882428974465024",
        "id" : 907882428974465024,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJlyQkzXgAAkdvd.jpg",
        "sizes" : [ {
          "h" : 990,
          "resize" : "fit",
          "w" : 1820
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 990,
          "resize" : "fit",
          "w" : 1820
        }, {
          "h" : 653,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/K3IbdF4WNL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907883402195595265",
    "text" : "buying iPhone X in europe? why not add a free weekend trip to NYC https:\/\/t.co\/K3IbdF4WNL",
    "id" : 907883402195595265,
    "created_at" : "2017-09-13 08:27:25 +0000",
    "user" : {
      "name" : "juan",
      "screen_name" : "juanbuis",
      "protected" : false,
      "id_str" : "36672594",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/757924077629743105\/D-8v_kZh_normal.jpg",
      "id" : 36672594,
      "verified" : true
    }
  },
  "id" : 907958313224228865,
  "created_at" : "2017-09-13 13:25:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/BC14zF620y",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-41237318",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907867158637682688",
  "text" : "BBC News - Why Singaporeans aren't all glad to get the president they wanted https:\/\/t.co\/BC14zF620y",
  "id" : 907867158637682688,
  "created_at" : "2017-09-13 07:22:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan McIntosh",
      "screen_name" : "radicalbytes",
      "indices" : [ 3, 16 ],
      "id_str" : "16823185",
      "id" : 16823185
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907680497349877763",
  "text" : "RT @radicalbytes: So all the police have to do is hold your phone up to your face while you are in cuffs then? Maybe not the best idea, App\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/Jk7Fz8o7fH",
        "expanded_url" : "https:\/\/twitter.com\/verge\/status\/907671351573479431",
        "display_url" : "twitter.com\/verge\/status\/9\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "907674050612297728",
    "text" : "So all the police have to do is hold your phone up to your face while you are in cuffs then? Maybe not the best idea, Apple. https:\/\/t.co\/Jk7Fz8o7fH",
    "id" : 907674050612297728,
    "created_at" : "2017-09-12 18:35:32 +0000",
    "user" : {
      "name" : "Jonathan McIntosh",
      "screen_name" : "radicalbytes",
      "protected" : false,
      "id_str" : "16823185",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/636279253772840960\/llaz-W8-_normal.jpg",
      "id" : 16823185,
      "verified" : false
    }
  },
  "id" : 907680497349877763,
  "created_at" : "2017-09-12 19:01:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/907652758559264768\/photo\/1",
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/3kjEZfl7TD",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJihXOMU8AEQ8cF.jpg",
      "id_str" : "907652745234018305",
      "id" : 907652745234018305,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJihXOMU8AEQ8cF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 3024,
        "resize" : "fit",
        "w" : 4032
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3kjEZfl7TD"
    } ],
    "hashtags" : [ {
      "text" : "ApplePark",
      "indices" : [ 36, 46 ]
    }, {
      "text" : "appleEvent",
      "indices" : [ 48, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907653148071927809",
  "text" : "RT @mrbrown: The new Apple Store at #ApplePark. #appleEvent https:\/\/t.co\/3kjEZfl7TD",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/907652758559264768\/photo\/1",
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/3kjEZfl7TD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJihXOMU8AEQ8cF.jpg",
        "id_str" : "907652745234018305",
        "id" : 907652745234018305,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJihXOMU8AEQ8cF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 3024,
          "resize" : "fit",
          "w" : 4032
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3kjEZfl7TD"
      } ],
      "hashtags" : [ {
        "text" : "ApplePark",
        "indices" : [ 23, 33 ]
      }, {
        "text" : "appleEvent",
        "indices" : [ 35, 46 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907652758559264768",
    "text" : "The new Apple Store at #ApplePark. #appleEvent https:\/\/t.co\/3kjEZfl7TD",
    "id" : 907652758559264768,
    "created_at" : "2017-09-12 17:10:55 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 907653148071927809,
  "created_at" : "2017-09-12 17:12:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/XccHKfXXG2",
      "expanded_url" : "http:\/\/str.sg\/4V6U",
      "display_url" : "str.sg\/4V6U"
    } ]
  },
  "geo" : { },
  "id_str" : "907645333668233216",
  "text" : "Parents of heart donor to meet recipient https:\/\/t.co\/XccHKfXXG2",
  "id" : 907645333668233216,
  "created_at" : "2017-09-12 16:41:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 0, 8 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "907631967218356224",
  "geo" : { },
  "id_str" : "907633535128924160",
  "in_reply_to_user_id" : 574253,
  "text" : "@mrbrown It this far away from the usual venue?",
  "id" : 907633535128924160,
  "in_reply_to_status_id" : 907631967218356224,
  "created_at" : "2017-09-12 15:54:32 +0000",
  "in_reply_to_screen_name" : "mrbrown",
  "in_reply_to_user_id_str" : "574253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907621696626184192",
  "text" : "RT @EimearMcCormack: 2 bedroom apartment to rent in Ringsend for \u20AC2,750!!!!!!!! We have a serious crisis on our hands folks.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907617180543541248",
    "text" : "2 bedroom apartment to rent in Ringsend for \u20AC2,750!!!!!!!! We have a serious crisis on our hands folks.",
    "id" : 907617180543541248,
    "created_at" : "2017-09-12 14:49:33 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 907621696626184192,
  "created_at" : "2017-09-12 15:07:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Science and Technology Committee",
      "screen_name" : "CommonsSTC",
      "indices" : [ 0, 11 ],
      "id_str" : "988974846",
      "id" : 988974846
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "907525088878178304",
  "geo" : { },
  "id_str" : "907587508539727873",
  "in_reply_to_user_id" : 988974846,
  "text" : "@CommonsSTC Have u consider Dr Maggie Aderin-Pocock,  Professor Alice Roberts?",
  "id" : 907587508539727873,
  "in_reply_to_status_id" : 907525088878178304,
  "created_at" : "2017-09-12 12:51:38 +0000",
  "in_reply_to_screen_name" : "CommonsSTC",
  "in_reply_to_user_id_str" : "988974846",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "indices" : [ 3, 13 ],
      "id_str" : "27819449",
      "id" : 27819449
    }, {
      "name" : "James Vincent",
      "screen_name" : "jjvincent",
      "indices" : [ 20, 30 ],
      "id_str" : "237920763",
      "id" : 237920763
    }, {
      "name" : "The Verge",
      "screen_name" : "verge",
      "indices" : [ 110, 116 ],
      "id_str" : "275686563",
      "id" : 275686563
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 38, 41 ]
    } ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/SDymoKqsQw",
      "expanded_url" : "http:\/\/micfarris.us\/2jdweec",
      "display_url" : "micfarris.us\/2jdweec"
    } ]
  },
  "geo" : { },
  "id_str" : "907585050765664256",
  "text" : "RT @MicFarris: From @jjvincent --&gt; #AI is so hot right now researchers are posing for Yves Saint Laurent | @verge https:\/\/t.co\/SDymoKqsQw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "James Vincent",
        "screen_name" : "jjvincent",
        "indices" : [ 5, 15 ],
        "id_str" : "237920763",
        "id" : 237920763
      }, {
        "name" : "The Verge",
        "screen_name" : "verge",
        "indices" : [ 95, 101 ],
        "id_str" : "275686563",
        "id" : 275686563
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 23, 26 ]
      } ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/SDymoKqsQw",
        "expanded_url" : "http:\/\/micfarris.us\/2jdweec",
        "display_url" : "micfarris.us\/2jdweec"
      } ]
    },
    "geo" : { },
    "id_str" : "907580838258278401",
    "text" : "From @jjvincent --&gt; #AI is so hot right now researchers are posing for Yves Saint Laurent | @verge https:\/\/t.co\/SDymoKqsQw",
    "id" : 907580838258278401,
    "created_at" : "2017-09-12 12:25:08 +0000",
    "user" : {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "protected" : false,
      "id_str" : "27819449",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3055549457\/94b094a6e85db296aa251b3a775d7439_normal.jpeg",
      "id" : 27819449,
      "verified" : false
    }
  },
  "id" : 907585050765664256,
  "created_at" : "2017-09-12 12:41:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907568391099219969",
  "text" : "Daughter : Daddy, I win the race \nDaddy : I'm so proud of you . How many people involve in the race \nDaughter : I am the one running.",
  "id" : 907568391099219969,
  "created_at" : "2017-09-12 11:35:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/QDNSG7Zu2r",
      "expanded_url" : "https:\/\/winpython.github.io\/",
      "display_url" : "winpython.github.io"
    } ]
  },
  "geo" : { },
  "id_str" : "907565900135632897",
  "text" : "A data science toolbox you can take with you. https:\/\/t.co\/QDNSG7Zu2r It works when I move WinPython from one portable drive to another.",
  "id" : 907565900135632897,
  "created_at" : "2017-09-12 11:25:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Changi Airport",
      "screen_name" : "ChangiAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "65268724",
      "id" : 65268724
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907563017172389888",
  "text" : "RT @ChangiAirport: &lt;3 that feeling when the cabin crew announces \"... and to all Singaporeans and residents of Singapore, a warm welcome ho\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.percolate.com\" rel=\"nofollow\"\u003EPercolate\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChangiAirport\/status\/907544332298633216\/photo\/1",
        "indices" : [ 128, 151 ],
        "url" : "https:\/\/t.co\/NObl0MJaJx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJg-wmwUMAUKiY0.jpg",
        "id_str" : "907544329672994821",
        "id" : 907544329672994821,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJg-wmwUMAUKiY0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1333,
          "resize" : "fit",
          "w" : 2000
        }, {
          "h" : 1333,
          "resize" : "fit",
          "w" : 2000
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NObl0MJaJx"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "907544332298633216",
    "text" : "&lt;3 that feeling when the cabin crew announces \"... and to all Singaporeans and residents of Singapore, a warm welcome home\". https:\/\/t.co\/NObl0MJaJx",
    "id" : 907544332298633216,
    "created_at" : "2017-09-12 10:00:04 +0000",
    "user" : {
      "name" : "Changi Airport",
      "screen_name" : "ChangiAirport",
      "protected" : false,
      "id_str" : "65268724",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894423767484465152\/kB4X-t8m_normal.jpg",
      "id" : 65268724,
      "verified" : true
    }
  },
  "id" : 907563017172389888,
  "created_at" : "2017-09-12 11:14:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/vHNHs4NV73",
      "expanded_url" : "https:\/\/twitter.com\/AdamRutherford\/status\/907542485135749120",
      "display_url" : "twitter.com\/AdamRutherford\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907562117598982145",
  "text" : "To pile on, there is no Asian people. https:\/\/t.co\/vHNHs4NV73",
  "id" : 907562117598982145,
  "created_at" : "2017-09-12 11:10:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907320326689882114",
  "text" : "RT someone who been singing the praises of you is so...",
  "id" : 907320326689882114,
  "created_at" : "2017-09-11 19:09:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Financial Times",
      "screen_name" : "FT",
      "indices" : [ 67, 70 ],
      "id_str" : "18949452",
      "id" : 18949452
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/jRudTYEDFU",
      "expanded_url" : "https:\/\/www.ft.com\/content\/c8334df6-8da8-11e7-a352-e46f43c5825d",
      "display_url" : "ft.com\/content\/c8334d\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907311756531453952",
  "text" : "A hard landing for the Gulf\u2019s airlines https:\/\/t.co\/jRudTYEDFU via @FT",
  "id" : 907311756531453952,
  "created_at" : "2017-09-11 18:35:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 0, 16 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/Af6Zo1OPlZ",
      "expanded_url" : "https:\/\/soundcloud.com\/user-670157912\/influencer-outraged-after-being-refused-free-hotel-stay",
      "display_url" : "soundcloud.com\/user-670157912\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907280415349669888",
  "in_reply_to_user_id" : 22700048,
  "text" : "@EimearMcCormack shares an all too familair story about self proclaimed influencers https:\/\/t.co\/Af6Zo1OPlZ",
  "id" : 907280415349669888,
  "created_at" : "2017-09-11 16:31:22 +0000",
  "in_reply_to_screen_name" : "EimearMcCormack",
  "in_reply_to_user_id_str" : "22700048",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bloomberg",
      "screen_name" : "business",
      "indices" : [ 3, 12 ],
      "id_str" : "34713362",
      "id" : 34713362
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/business\/status\/904867162585784321\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/UVp1gKL7Qe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DI674zOXcAAZHMu.jpg",
      "id_str" : "904867159645581312",
      "id" : 904867159645581312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI674zOXcAAZHMu.jpg",
      "sizes" : [ {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UVp1gKL7Qe"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/pabkkfZB3N",
      "expanded_url" : "https:\/\/bloom.bg\/2wzL3wc",
      "display_url" : "bloom.bg\/2wzL3wc"
    } ]
  },
  "geo" : { },
  "id_str" : "907278147133026304",
  "text" : "RT @business: For all their tech savvy, Singaporeans still prefer to pay with cash https:\/\/t.co\/pabkkfZB3N https:\/\/t.co\/UVp1gKL7Qe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/business\/status\/904867162585784321\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/UVp1gKL7Qe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DI674zOXcAAZHMu.jpg",
        "id_str" : "904867159645581312",
        "id" : 904867159645581312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI674zOXcAAZHMu.jpg",
        "sizes" : [ {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UVp1gKL7Qe"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/pabkkfZB3N",
        "expanded_url" : "https:\/\/bloom.bg\/2wzL3wc",
        "display_url" : "bloom.bg\/2wzL3wc"
      } ]
    },
    "geo" : { },
    "id_str" : "904867162585784321",
    "text" : "For all their tech savvy, Singaporeans still prefer to pay with cash https:\/\/t.co\/pabkkfZB3N https:\/\/t.co\/UVp1gKL7Qe",
    "id" : 904867162585784321,
    "created_at" : "2017-09-05 00:41:57 +0000",
    "user" : {
      "name" : "Bloomberg",
      "screen_name" : "business",
      "protected" : false,
      "id_str" : "34713362",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991818020233404416\/alrBF_dr_normal.jpg",
      "id" : 34713362,
      "verified" : true
    }
  },
  "id" : 907278147133026304,
  "created_at" : "2017-09-11 16:22:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/907263798058340352\/photo\/1",
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/oqwPQnF60A",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJc_RMWX0AA67UK.jpg",
      "id_str" : "907263414543831040",
      "id" : 907263414543831040,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJc_RMWX0AA67UK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1360
      }, {
        "h" : 616,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 349,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1360
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oqwPQnF60A"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907263798058340352",
  "text" : "Issued resolved by running the setup separately on a tethered recycle HDD . https:\/\/t.co\/oqwPQnF60A",
  "id" : 907263798058340352,
  "created_at" : "2017-09-11 15:25:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/IpBbCNBCt3",
      "expanded_url" : "http:\/\/www.todayonline.com\/singapore\/why-presidential-election-should-not-be-walkover",
      "display_url" : "todayonline.com\/singapore\/why-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907221228083142656",
  "text" : "Why the Presidential Election should not be a walkover | TODAYonline https:\/\/t.co\/IpBbCNBCt3",
  "id" : 907221228083142656,
  "created_at" : "2017-09-11 12:36:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 3, 26 ],
      "url" : "https:\/\/t.co\/iidPazELvE",
      "expanded_url" : "http:\/\/notebooks.azure.com",
      "display_url" : "notebooks.azure.com"
    }, {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/olECG5r2C8",
      "expanded_url" : "http:\/\/notes.azure.com",
      "display_url" : "notes.azure.com"
    }, {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/n2y9ZRyxo0",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/907218402036592640",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907220018336866306",
  "text" : "It https:\/\/t.co\/iidPazELvE not  https:\/\/t.co\/olECG5r2C8 https:\/\/t.co\/n2y9ZRyxo0",
  "id" : 907220018336866306,
  "created_at" : "2017-09-11 12:31:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 106, 120 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/hOIYqT7rOt",
      "expanded_url" : "https:\/\/www.irishtimes.com\/business\/financial-services\/dublin-s-reputation-as-a-global-financial-centre-improves-further-1.3216706#.WbaBgmg6QUk.twitter",
      "display_url" : "irishtimes.com\/business\/finan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907219744473960449",
  "text" : "Dublin does not give me the feel is a global financial centre. Just saying.   https:\/\/t.co\/hOIYqT7rOt via @IrishTimesBiz",
  "id" : 907219744473960449,
  "created_at" : "2017-09-11 12:30:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/olECG5r2C8",
      "expanded_url" : "http:\/\/notes.azure.com",
      "display_url" : "notes.azure.com"
    }, {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/LEYM3sV4bw",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/906797417827356672",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907218402036592640",
  "text" : "Still unresolve. Move on to cloud options https:\/\/t.co\/olECG5r2C8 instead https:\/\/t.co\/LEYM3sV4bw",
  "id" : 907218402036592640,
  "created_at" : "2017-09-11 12:24:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907190776471461888",
  "text" : "One wonder what Robert the Otter would say...",
  "id" : 907190776471461888,
  "created_at" : "2017-09-11 10:35:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/gJQEYIIbXC",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/907168260000407553",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907185811552194560",
  "text" : "You should checkout those comments. https:\/\/t.co\/gJQEYIIbXC",
  "id" : 907185811552194560,
  "created_at" : "2017-09-11 10:15:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/mBOPCmkijV",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/machine-learning-non-machine-learners-recipe-right-new-zara-palevani",
      "display_url" : "linkedin.com\/pulse\/machine-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "907168585474301952",
  "text" : "Do you really need machine learning?https:\/\/t.co\/mBOPCmkijV",
  "id" : 907168585474301952,
  "created_at" : "2017-09-11 09:06:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Audi Khalid",
      "screen_name" : "audikhalidphoto",
      "indices" : [ 3, 19 ],
      "id_str" : "803539002515070977",
      "id" : 803539002515070977
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907131229522337792",
  "text" : "RT @audikhalidphoto: Amusing find out in the forest today. A  smart toilet. I paid for toilet with Apple Pay out in the middle of nowhere.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/906939557349871617\/photo\/1",
        "indices" : [ 129, 152 ],
        "url" : "https:\/\/t.co\/izYkjv9ZTl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJYYsb1XgAAWVfG.jpg",
        "id_str" : "906939526626639872",
        "id" : 906939526626639872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJYYsb1XgAAWVfG.jpg",
        "sizes" : [ {
          "h" : 1424,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1424,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 504,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 890,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/izYkjv9ZTl"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/906939557349871617\/photo\/1",
        "indices" : [ 129, 152 ],
        "url" : "https:\/\/t.co\/izYkjv9ZTl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJYYsbmXgAAv_k7.jpg",
        "id_str" : "906939526563725312",
        "id" : 906939526563725312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJYYsbmXgAAv_k7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 453
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/izYkjv9ZTl"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/906939557349871617\/photo\/1",
        "indices" : [ 129, 152 ],
        "url" : "https:\/\/t.co\/izYkjv9ZTl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJYYsbkXUAAvoLd.jpg",
        "id_str" : "906939526555324416",
        "id" : 906939526555324416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJYYsbkXUAAvoLd.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 453
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/izYkjv9ZTl"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/906939557349871617\/photo\/1",
        "indices" : [ 129, 152 ],
        "url" : "https:\/\/t.co\/izYkjv9ZTl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJYYsbpW0AAK7M7.jpg",
        "id_str" : "906939526576263168",
        "id" : 906939526576263168,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJYYsbpW0AAK7M7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/izYkjv9ZTl"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906939557349871617",
    "text" : "Amusing find out in the forest today. A  smart toilet. I paid for toilet with Apple Pay out in the middle of nowhere. 10Kr = $2. https:\/\/t.co\/izYkjv9ZTl",
    "id" : 906939557349871617,
    "created_at" : "2017-09-10 17:56:55 +0000",
    "user" : {
      "name" : "Audi Khalid",
      "screen_name" : "audikhalidphoto",
      "protected" : false,
      "id_str" : "803539002515070977",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/803540398815645696\/eRcv9E1D_normal.jpg",
      "id" : 803539002515070977,
      "verified" : false
    }
  },
  "id" : 907131229522337792,
  "created_at" : "2017-09-11 06:38:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Barry Rogers",
      "screen_name" : "TheBarryRogers",
      "indices" : [ 3, 18 ],
      "id_str" : "2168576827",
      "id" : 2168576827
    }, {
      "name" : "Irish Film Institute",
      "screen_name" : "IFI_Dub",
      "indices" : [ 24, 32 ],
      "id_str" : "42610203",
      "id" : 42610203
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "907130652973305857",
  "text" : "RT @TheBarryRogers: The @IFI_Dub archive is a great resource for old Irish tourist films. Check out this one of a 1940s Dublin city. https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Film Institute",
        "screen_name" : "IFI_Dub",
        "indices" : [ 4, 12 ],
        "id_str" : "42610203",
        "id" : 42610203
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/ntbRKsMj2j",
        "expanded_url" : "http:\/\/ifiplayer.ie\/dublin-capital-city-of-ireland\/",
        "display_url" : "ifiplayer.ie\/dublin-capital\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "906947079943213057",
    "text" : "The @IFI_Dub archive is a great resource for old Irish tourist films. Check out this one of a 1940s Dublin city. https:\/\/t.co\/ntbRKsMj2j",
    "id" : 906947079943213057,
    "created_at" : "2017-09-10 18:26:48 +0000",
    "user" : {
      "name" : "Barry Rogers",
      "screen_name" : "TheBarryRogers",
      "protected" : false,
      "id_str" : "2168576827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/951186092572794880\/ovpRttTT_normal.jpg",
      "id" : 2168576827,
      "verified" : false
    }
  },
  "id" : 907130652973305857,
  "created_at" : "2017-09-11 06:36:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "opendata",
      "indices" : [ 95, 104 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/n1Ff89nLhK",
      "expanded_url" : "http:\/\/www.fao.org\/nr\/water\/aquastat\/data\/query\/index.html",
      "display_url" : "fao.org\/nr\/water\/aquas\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906857677514473472",
  "text" : "Food and Agriculture Organization (FAO) of the UN's AQUASTAT dataset.  https:\/\/t.co\/n1Ff89nLhK #opendata",
  "id" : 906857677514473472,
  "created_at" : "2017-09-10 12:31:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "YouTube",
      "screen_name" : "YouTube",
      "indices" : [ 59, 67 ],
      "id_str" : "10228272",
      "id" : 10228272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/GZL71zY4sX",
      "expanded_url" : "https:\/\/youtu.be\/5_KbK1ixobI",
      "display_url" : "youtu.be\/5_KbK1ixobI"
    } ]
  },
  "geo" : { },
  "id_str" : "906851797729468417",
  "text" : "\u7537\u513F\u5F53\u81EA\u5F3A - \u6797\u5B50\u7965 &amp; \u6E2F\u4E50[2002].avi https:\/\/t.co\/GZL71zY4sX via @YouTube",
  "id" : 906851797729468417,
  "created_at" : "2017-09-10 12:08:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/sBKh1zEIKW",
      "expanded_url" : "https:\/\/twitter.com\/dragonflystats\/status\/906812332180955136",
      "display_url" : "twitter.com\/dragonflystats\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906813358254559232",
  "text" : "\uD83D\uDE05 https:\/\/t.co\/sBKh1zEIKW",
  "id" : 906813358254559232,
  "created_at" : "2017-09-10 09:35:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/G3cQZ3qPDW",
      "expanded_url" : "https:\/\/github.com\/IRkernel\/IRkernel\/issues",
      "display_url" : "github.com\/IRkernel\/IRker\u2026"
    }, {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/NO61RhZiVS",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/906802466771042304",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906807645172101120",
  "text" : "It seems to be a prevailing issues https:\/\/t.co\/G3cQZ3qPDW https:\/\/t.co\/NO61RhZiVS",
  "id" : 906807645172101120,
  "created_at" : "2017-09-10 09:12:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/LEYM3sV4bw",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/906797417827356672",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906802466771042304",
  "text" : "Install Anaconda is not the solution I seek. :) https:\/\/t.co\/LEYM3sV4bw",
  "id" : 906802466771042304,
  "created_at" : "2017-09-10 08:52:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906797417827356672",
  "text" : "IRKernel fails to Launch in Jupyter notebook on local PC even I make the kernel available using IRkernel::installspec( ) Any advice? Thks",
  "id" : 906797417827356672,
  "created_at" : "2017-09-10 08:32:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Internet of Shit",
      "screen_name" : "internetofshit",
      "indices" : [ 3, 18 ],
      "id_str" : "3356531254",
      "id" : 3356531254
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/internetofshit\/status\/906640862968406016\/photo\/1",
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/jaNACygAfe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJT_nk2WsAE3kiV.jpg",
      "id_str" : "906630480379949057",
      "id" : 906630480379949057,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJT_nk2WsAE3kiV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 537
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 537
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 537
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 537
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jaNACygAfe"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906764738780069888",
  "text" : "RT @internetofshit: whoever said html never killed anyone was wrong https:\/\/t.co\/jaNACygAfe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/internetofshit\/status\/906640862968406016\/photo\/1",
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/jaNACygAfe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJT_nk2WsAE3kiV.jpg",
        "id_str" : "906630480379949057",
        "id" : 906630480379949057,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJT_nk2WsAE3kiV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 537
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 537
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 537
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 537
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jaNACygAfe"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906640862968406016",
    "text" : "whoever said html never killed anyone was wrong https:\/\/t.co\/jaNACygAfe",
    "id" : 906640862968406016,
    "created_at" : "2017-09-09 22:10:00 +0000",
    "user" : {
      "name" : "Internet of Shit",
      "screen_name" : "internetofshit",
      "protected" : false,
      "id_str" : "3356531254",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/616895706150797312\/ol4PeiHz_normal.png",
      "id" : 3356531254,
      "verified" : false
    }
  },
  "id" : 906764738780069888,
  "created_at" : "2017-09-10 06:22:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Videcette",
      "screen_name" : "DavidVidecette",
      "indices" : [ 3, 18 ],
      "id_str" : "2450641105",
      "id" : 2450641105
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/DignityHealth\/status\/906266319527563265\/video\/1",
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/AYAA5fNTDA",
      "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/906265895294701568\/img\/NUnO5P-gNz7zfS9Q.jpg",
      "id_str" : "906265895294701568",
      "id" : 906265895294701568,
      "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/906265895294701568\/img\/NUnO5P-gNz7zfS9Q.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/AYAA5fNTDA"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906621888826417154",
  "text" : "RT @DavidVidecette: And the quick thinking, kind hearted hearted human, right time right place award goes to... https:\/\/t.co\/AYAA5fNTDA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DignityHealth\/status\/906266319527563265\/video\/1",
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/AYAA5fNTDA",
        "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/906265895294701568\/img\/NUnO5P-gNz7zfS9Q.jpg",
        "id_str" : "906265895294701568",
        "id" : 906265895294701568,
        "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/906265895294701568\/img\/NUnO5P-gNz7zfS9Q.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AYAA5fNTDA"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906458491480625152",
    "text" : "And the quick thinking, kind hearted hearted human, right time right place award goes to... https:\/\/t.co\/AYAA5fNTDA",
    "id" : 906458491480625152,
    "created_at" : "2017-09-09 10:05:20 +0000",
    "user" : {
      "name" : "David Videcette",
      "screen_name" : "DavidVidecette",
      "protected" : false,
      "id_str" : "2450641105",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/623578026018754560\/KWVkGSpn_normal.jpg",
      "id" : 2450641105,
      "verified" : true
    }
  },
  "id" : 906621888826417154,
  "created_at" : "2017-09-09 20:54:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/fBSQ6XUl7P",
      "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/906525031248699392",
      "display_url" : "twitter.com\/eisen\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906558012696997888",
  "text" : "Robert the Otter is the most political animal in Singapore. https:\/\/t.co\/fBSQ6XUl7P",
  "id" : 906558012696997888,
  "created_at" : "2017-09-09 16:40:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/yQg137PtYE",
      "expanded_url" : "http:\/\/www.herworldplus.com\/solutions\/solutions\/durian-101-guide-10-best-durian-varieties-malaysia",
      "display_url" : "herworldplus.com\/solutions\/solu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906554074161729536",
  "text" : "D24, D13 are part of postcode in Dublin but back home it means durian! https:\/\/t.co\/yQg137PtYE",
  "id" : 906554074161729536,
  "created_at" : "2017-09-09 16:25:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u00F3n\u00E1n Foley",
      "screen_name" : "ronanfoley0",
      "indices" : [ 3, 15 ],
      "id_str" : "3355802333",
      "id" : 3355802333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906553126853005314",
  "text" : "RT @ronanfoley0: Any grandchild of mine that takes pics of me in a hospital bed and plasters it on any social media is being conveniently l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "904298591501656064",
    "text" : "Any grandchild of mine that takes pics of me in a hospital bed and plasters it on any social media is being conveniently left outta the will",
    "id" : 904298591501656064,
    "created_at" : "2017-09-03 11:02:39 +0000",
    "user" : {
      "name" : "R\u00F3n\u00E1n Foley",
      "screen_name" : "ronanfoley0",
      "protected" : false,
      "id_str" : "3355802333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013745476062142464\/lEdqzLm8_normal.jpg",
      "id" : 3355802333,
      "verified" : false
    }
  },
  "id" : 906553126853005314,
  "created_at" : "2017-09-09 16:21:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Financial Express",
      "screen_name" : "FinancialXpress",
      "indices" : [ 3, 19 ],
      "id_str" : "50279781",
      "id" : 50279781
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/FinancialXpress\/status\/906316185481781248\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/qSdHGs9c7w",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJPhwZ-V4AEiBtn.jpg",
      "id_str" : "906316171753938945",
      "id" : 906316171753938945,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJPhwZ-V4AEiBtn.jpg",
      "sizes" : [ {
        "h" : 440,
        "resize" : "fit",
        "w" : 660
      }, {
        "h" : 440,
        "resize" : "fit",
        "w" : 660
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 440,
        "resize" : "fit",
        "w" : 660
      }, {
        "h" : 440,
        "resize" : "fit",
        "w" : 660
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qSdHGs9c7w"
    } ],
    "hashtags" : [ {
      "text" : "UN",
      "indices" : [ 21, 24 ]
    }, {
      "text" : "drone",
      "indices" : [ 60, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/PYe8NYWTwc",
      "expanded_url" : "http:\/\/www.financialexpress.com\/world-news\/un-aviation-agency-to-call-for-global-drone-registry\/847808\/",
      "display_url" : "financialexpress.com\/world-news\/un-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906543006190370816",
  "text" : "RT @FinancialXpress: #UN aviation agency to call for global #drone registry\nhttps:\/\/t.co\/PYe8NYWTwc https:\/\/t.co\/qSdHGs9c7w",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FinancialXpress\/status\/906316185481781248\/photo\/1",
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/qSdHGs9c7w",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJPhwZ-V4AEiBtn.jpg",
        "id_str" : "906316171753938945",
        "id" : 906316171753938945,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJPhwZ-V4AEiBtn.jpg",
        "sizes" : [ {
          "h" : 440,
          "resize" : "fit",
          "w" : 660
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 660
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 660
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 660
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qSdHGs9c7w"
      } ],
      "hashtags" : [ {
        "text" : "UN",
        "indices" : [ 0, 3 ]
      }, {
        "text" : "drone",
        "indices" : [ 39, 45 ]
      } ],
      "urls" : [ {
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/PYe8NYWTwc",
        "expanded_url" : "http:\/\/www.financialexpress.com\/world-news\/un-aviation-agency-to-call-for-global-drone-registry\/847808\/",
        "display_url" : "financialexpress.com\/world-news\/un-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "906316185481781248",
    "text" : "#UN aviation agency to call for global #drone registry\nhttps:\/\/t.co\/PYe8NYWTwc https:\/\/t.co\/qSdHGs9c7w",
    "id" : 906316185481781248,
    "created_at" : "2017-09-09 00:39:51 +0000",
    "user" : {
      "name" : "Financial Express",
      "screen_name" : "FinancialXpress",
      "protected" : false,
      "id_str" : "50279781",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1195554309\/FE-for-Facebook_normal.jpg",
      "id" : 50279781,
      "verified" : true
    }
  },
  "id" : 906543006190370816,
  "created_at" : "2017-09-09 15:41:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Womenscouncilireland",
      "screen_name" : "NWCI",
      "indices" : [ 3, 8 ],
      "id_str" : "58796971",
      "id" : 58796971
    }, {
      "name" : "Newstalk",
      "screen_name" : "NewstalkFM",
      "indices" : [ 23, 34 ],
      "id_str" : "22646514",
      "id" : 22646514
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906524608974790657",
  "text" : "RT @NWCI: We're asking @NewstalkFM what they're doing about these comments. As a broadcaster, they have a responsibility toward their women\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Newstalk",
        "screen_name" : "NewstalkFM",
        "indices" : [ 13, 24 ],
        "id_str" : "22646514",
        "id" : 22646514
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "906461178716676097",
    "geo" : { },
    "id_str" : "906461512163844096",
    "in_reply_to_user_id" : 58796971,
    "text" : "We're asking @NewstalkFM what they're doing about these comments. As a broadcaster, they have a responsibility toward their women listeners.",
    "id" : 906461512163844096,
    "in_reply_to_status_id" : 906461178716676097,
    "created_at" : "2017-09-09 10:17:20 +0000",
    "in_reply_to_screen_name" : "NWCI",
    "in_reply_to_user_id_str" : "58796971",
    "user" : {
      "name" : "Womenscouncilireland",
      "screen_name" : "NWCI",
      "protected" : false,
      "id_str" : "58796971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1000657171552579584\/ZjQuVpXr_normal.jpg",
      "id" : 58796971,
      "verified" : true
    }
  },
  "id" : 906524608974790657,
  "created_at" : "2017-09-09 14:28:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/906524136985513984\/photo\/1",
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/yb7ZgD19EH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJSe5dNX0AAdpIZ.jpg",
      "id_str" : "906524134938759168",
      "id" : 906524134938759168,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJSe5dNX0AAdpIZ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yb7ZgD19EH"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/C7bs7bHGsM",
      "expanded_url" : "http:\/\/ift.tt\/2xdckoX",
      "display_url" : "ift.tt\/2xdckoX"
    } ]
  },
  "geo" : { },
  "id_str" : "906524136985513984",
  "text" : "At the garden https:\/\/t.co\/C7bs7bHGsM https:\/\/t.co\/yb7ZgD19EH",
  "id" : 906524136985513984,
  "created_at" : "2017-09-09 14:26:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906446229256761345",
  "text" : "RT @rshotton: Wheelchair protest in Portugal - each one has a note on back saying \"Be right back\" or \"Just getting something\"\n\nVia @Brillia\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Brilliant Ads",
        "screen_name" : "Brilliant_Ads",
        "indices" : [ 117, 131 ],
        "id_str" : "564686965",
        "id" : 564686965
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/906436784162304000\/photo\/1",
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/JW0Rxz20Dq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJRPXKGW0AAeK7c.jpg",
        "id_str" : "906436684274913280",
        "id" : 906436684274913280,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJRPXKGW0AAeK7c.jpg",
        "sizes" : [ {
          "h" : 359,
          "resize" : "fit",
          "w" : 482
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 482
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 482
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 482
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JW0Rxz20Dq"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906436784162304000",
    "text" : "Wheelchair protest in Portugal - each one has a note on back saying \"Be right back\" or \"Just getting something\"\n\nVia @Brilliant_Ads https:\/\/t.co\/JW0Rxz20Dq",
    "id" : 906436784162304000,
    "created_at" : "2017-09-09 08:39:04 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 906446229256761345,
  "created_at" : "2017-09-09 09:16:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Tamir, PhD",
      "screen_name" : "MikeTamir",
      "indices" : [ 3, 13 ],
      "id_str" : "331659804",
      "id" : 331659804
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MikeTamir\/status\/906200816398327808\/photo\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/EPVmjgkJGk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJN41p_VoAEOCiM.jpg",
      "id_str" : "906200813231513601",
      "id" : 906200813231513601,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJN41p_VoAEOCiM.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 490
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 490
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 490
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 490
      } ],
      "media_alt" : "null",
      "display_url" : "pic.twitter.com\/EPVmjgkJGk"
    } ],
    "hashtags" : [ {
      "text" : "funnyML",
      "indices" : [ 55, 63 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906202599589912576",
  "text" : "RT @MikeTamir: Cartoon: Citizen Data Scientist At Work #funnyML https:\/\/t.co\/EPVmjgkJGk",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MikeTamir\/status\/906200816398327808\/photo\/1",
        "indices" : [ 49, 72 ],
        "url" : "https:\/\/t.co\/EPVmjgkJGk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJN41p_VoAEOCiM.jpg",
        "id_str" : "906200813231513601",
        "id" : 906200813231513601,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJN41p_VoAEOCiM.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 490
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 490
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 490
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 490
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EPVmjgkJGk"
      } ],
      "hashtags" : [ {
        "text" : "funnyML",
        "indices" : [ 40, 48 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906200816398327808",
    "text" : "Cartoon: Citizen Data Scientist At Work #funnyML https:\/\/t.co\/EPVmjgkJGk",
    "id" : 906200816398327808,
    "created_at" : "2017-09-08 17:01:25 +0000",
    "user" : {
      "name" : "Mike Tamir, PhD",
      "screen_name" : "MikeTamir",
      "protected" : false,
      "id_str" : "331659804",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/674658758472024064\/1BWd5LDj_normal.jpg",
      "id" : 331659804,
      "verified" : false
    }
  },
  "id" : 906202599589912576,
  "created_at" : "2017-09-08 17:08:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "juzcrap",
      "screen_name" : "juzcrap",
      "indices" : [ 0, 8 ],
      "id_str" : "64356461",
      "id" : 64356461
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "906179312025387008",
  "geo" : { },
  "id_str" : "906197666102726657",
  "in_reply_to_user_id" : 64356461,
  "text" : "@juzcrap U can consider Sats Auxiliary Police Officer. Came across the recruitment ads when I was back home.",
  "id" : 906197666102726657,
  "in_reply_to_status_id" : 906179312025387008,
  "created_at" : "2017-09-08 16:48:54 +0000",
  "in_reply_to_screen_name" : "juzcrap",
  "in_reply_to_user_id_str" : "64356461",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/s6qBvyjxHE",
      "expanded_url" : "https:\/\/twitter.com\/unapower\/status\/906157243044945920",
      "display_url" : "twitter.com\/unapower\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906195934241009664",
  "text" : "Blaming rape victim is like blaming the bank inciting you to rob. https:\/\/t.co\/s6qBvyjxHE",
  "id" : 906195934241009664,
  "created_at" : "2017-09-08 16:42:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "openhousedublin",
      "indices" : [ 3, 19 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/vZVGJVT2mA",
      "expanded_url" : "https:\/\/openhousedublin.com\/index.php\/user\/list-share\/xSAbGrAjxEbfKsNv",
      "display_url" : "openhousedublin.com\/index.php\/user\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906164608133722112",
  "text" : "My #openhousedublin shortlist https:\/\/t.co\/vZVGJVT2mA",
  "id" : 906164608133722112,
  "created_at" : "2017-09-08 14:37:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Little Things",
      "screen_name" : "littlethingshub",
      "indices" : [ 3, 19 ],
      "id_str" : "2819507992",
      "id" : 2819507992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906160282371805184",
  "text" : "RT @littlethingshub: Men are around 5x more likely to take their own lives. Let's change the statistics, one conversation at a time. https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/5tAcMD8Ohy",
        "expanded_url" : "https:\/\/cards.twitter.com\/cards\/18ce53x0f75\/4qchf",
        "display_url" : "cards.twitter.com\/cards\/18ce53x0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "905324695276240896",
    "text" : "Men are around 5x more likely to take their own lives. Let's change the statistics, one conversation at a time. https:\/\/t.co\/5tAcMD8Ohy",
    "id" : 905324695276240896,
    "created_at" : "2017-09-06 07:00:02 +0000",
    "user" : {
      "name" : "Little Things",
      "screen_name" : "littlethingshub",
      "protected" : false,
      "id_str" : "2819507992",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/880740649900888064\/TTQbcyGv_normal.jpg",
      "id" : 2819507992,
      "verified" : false
    }
  },
  "id" : 906160282371805184,
  "created_at" : "2017-09-08 14:20:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906149061799153665",
  "text" : "RT @Dotnetster: Went live with a new version of favourites.io today! Lots of nice new features including edit\/remove collections, advanced\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "906141779438825472",
    "text" : "Went live with a new version of favourites.io today! Lots of nice new features including edit\/remove collections, advanced search and more!",
    "id" : 906141779438825472,
    "created_at" : "2017-09-08 13:06:50 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 906149061799153665,
  "created_at" : "2017-09-08 13:35:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "National Geographic",
      "screen_name" : "NatGeo",
      "indices" : [ 3, 10 ],
      "id_str" : "17471979",
      "id" : 17471979
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906107360908890112",
  "text" : "RT @NatGeo: This rare view of the capital was one of three flights granted to the photographer from the North Korean government https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.spredfast.com\/\" rel=\"nofollow\"\u003ESpredfast app\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/oOVQD2Km3L",
        "expanded_url" : "http:\/\/on.natgeo.com\/2wNl7gR",
        "display_url" : "on.natgeo.com\/2wNl7gR"
      } ]
    },
    "geo" : { },
    "id_str" : "906096538702753793",
    "text" : "This rare view of the capital was one of three flights granted to the photographer from the North Korean government https:\/\/t.co\/oOVQD2Km3L",
    "id" : 906096538702753793,
    "created_at" : "2017-09-08 10:07:03 +0000",
    "user" : {
      "name" : "National Geographic",
      "screen_name" : "NatGeo",
      "protected" : false,
      "id_str" : "17471979",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/921336759979597825\/VTSJ5mRt_normal.jpg",
      "id" : 17471979,
      "verified" : true
    }
  },
  "id" : 906107360908890112,
  "created_at" : "2017-09-08 10:50:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906089027924893696",
  "text" : "The desktop Google Drive app is going away. So no impact on the gdrive package for R?",
  "id" : 906089027924893696,
  "created_at" : "2017-09-08 09:37:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "indices" : [ 3, 7 ],
      "id_str" : "5120691",
      "id" : 5120691
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/906077103191359488\/video\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/YMPLl47kz2",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/906074877903470592\/pu\/img\/aRxhWQveQIOwuEOV.jpg",
      "id_str" : "906074877903470592",
      "id" : 906074877903470592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/906074877903470592\/pu\/img\/aRxhWQveQIOwuEOV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YMPLl47kz2"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/REB9nhbzsk",
      "expanded_url" : "http:\/\/wef.ch\/2gNKxBE",
      "display_url" : "wef.ch\/2gNKxBE"
    } ]
  },
  "geo" : { },
  "id_str" : "906082832090632192",
  "text" : "RT @wef: In Norway, they're planning to sail ships under mountains. Read more: https:\/\/t.co\/REB9nhbzsk https:\/\/t.co\/YMPLl47kz2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/906077103191359488\/video\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/YMPLl47kz2",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/906074877903470592\/pu\/img\/aRxhWQveQIOwuEOV.jpg",
        "id_str" : "906074877903470592",
        "id" : 906074877903470592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/906074877903470592\/pu\/img\/aRxhWQveQIOwuEOV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YMPLl47kz2"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/REB9nhbzsk",
        "expanded_url" : "http:\/\/wef.ch\/2gNKxBE",
        "display_url" : "wef.ch\/2gNKxBE"
      } ]
    },
    "geo" : { },
    "id_str" : "906077103191359488",
    "text" : "In Norway, they're planning to sail ships under mountains. Read more: https:\/\/t.co\/REB9nhbzsk https:\/\/t.co\/YMPLl47kz2",
    "id" : 906077103191359488,
    "created_at" : "2017-09-08 08:49:50 +0000",
    "user" : {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "protected" : false,
      "id_str" : "5120691",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/565498192171507712\/r2Hb2gvX_normal.png",
      "id" : 5120691,
      "verified" : true
    }
  },
  "id" : 906082832090632192,
  "created_at" : "2017-09-08 09:12:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/xjHcX78xJs",
      "expanded_url" : "https:\/\/www.beatthestreet.me\/dublin\/howtoplay",
      "display_url" : "beatthestreet.me\/dublin\/howtopl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "906081884425334784",
  "text" : "This going to be fun for me who walk. Why not let us download apps instead? https:\/\/t.co\/xjHcX78xJs",
  "id" : 906081884425334784,
  "created_at" : "2017-09-08 09:08:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SwissSharePoint News",
      "screen_name" : "SharePointSwiss",
      "indices" : [ 3, 19 ],
      "id_str" : "713699873414127617",
      "id" : 713699873414127617
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Azure",
      "indices" : [ 104, 110 ]
    }, {
      "text" : "Jupyter",
      "indices" : [ 111, 119 ]
    } ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/Y2IqJiSwDt",
      "expanded_url" : "https:\/\/goo.gl\/xRGcdz",
      "display_url" : "goo.gl\/xRGcdz"
    } ]
  },
  "geo" : { },
  "id_str" : "906046076381274112",
  "text" : "RT @SharePointSwiss: How powerful are Microsoft Azure\u2019s free Jupyter notebooks? https:\/\/t.co\/Y2IqJiSwDt #Azure #Jupyter https:\/\/t.co\/YKdD0i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.necio.ch\" rel=\"nofollow\"\u003ESwiss SharePoint News\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SharePointSwiss\/status\/894967696374345728\/photo\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/YKdD0ib0fD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DGuQYCJXcAQXj8J.png",
        "id_str" : "894967693530591236",
        "id" : 894967693530591236,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DGuQYCJXcAQXj8J.png",
        "sizes" : [ {
          "h" : 271,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 271,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 271,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 271,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YKdD0ib0fD"
      } ],
      "hashtags" : [ {
        "text" : "Azure",
        "indices" : [ 83, 89 ]
      }, {
        "text" : "Jupyter",
        "indices" : [ 90, 98 ]
      } ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/Y2IqJiSwDt",
        "expanded_url" : "https:\/\/goo.gl\/xRGcdz",
        "display_url" : "goo.gl\/xRGcdz"
      } ]
    },
    "geo" : { },
    "id_str" : "894967696374345728",
    "text" : "How powerful are Microsoft Azure\u2019s free Jupyter notebooks? https:\/\/t.co\/Y2IqJiSwDt #Azure #Jupyter https:\/\/t.co\/YKdD0ib0fD",
    "id" : 894967696374345728,
    "created_at" : "2017-08-08 17:05:01 +0000",
    "user" : {
      "name" : "SwissSharePoint News",
      "screen_name" : "SharePointSwiss",
      "protected" : false,
      "id_str" : "713699873414127617",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/713743666825797633\/HzKvP39x_normal.jpg",
      "id" : 713699873414127617,
      "verified" : false
    }
  },
  "id" : 906046076381274112,
  "created_at" : "2017-09-08 06:46:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/906044033650360320\/photo\/1",
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/vSq4nrpd1w",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJLqFWrWsAAM-Zg.jpg",
      "id_str" : "906043852762558464",
      "id" : 906043852762558464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJLqFWrWsAAM-Zg.jpg",
      "sizes" : [ {
        "h" : 615,
        "resize" : "fit",
        "w" : 1204
      }, {
        "h" : 613,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 615,
        "resize" : "fit",
        "w" : 1204
      }, {
        "h" : 347,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vSq4nrpd1w"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906044033650360320",
  "text" : "Use case of Jupyter notebook hosted on the cloud. If you are a lecturer and IT support is not forthcoming, contact me. https:\/\/t.co\/vSq4nrpd1w",
  "id" : 906044033650360320,
  "created_at" : "2017-09-08 06:38:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/eWhDTyGUF8",
      "expanded_url" : "https:\/\/notebooks.azure.com\/mryap\/libraries",
      "display_url" : "notebooks.azure.com\/mryap\/libraries"
    } ]
  },
  "geo" : { },
  "id_str" : "906042751141150720",
  "text" : "Here are some of my collection of notebooks on Azure Jupyter https:\/\/t.co\/eWhDTyGUF8",
  "id" : 906042751141150720,
  "created_at" : "2017-09-08 06:33:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906042003577872384",
  "text" : "Dear conference presenter, it helps if you can quote the date of the data source even it might not be recent.",
  "id" : 906042003577872384,
  "created_at" : "2017-09-08 06:30:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 3, 15 ],
      "id_str" : "168148402",
      "id" : 168148402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "906039110078349313",
  "text" : "RT @m4riannelee: Wow, Norway is among those with high tertiary education.  Good to know tt Germany has apprenticeship programmes. I advocat\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/L5PYNEj5H1",
        "expanded_url" : "https:\/\/twitter.com\/simongerman600\/status\/906035129918808064",
        "display_url" : "twitter.com\/simongerman600\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "906039027039576064",
    "text" : "Wow, Norway is among those with high tertiary education.  Good to know tt Germany has apprenticeship programmes. I advocate apprenticeship. https:\/\/t.co\/L5PYNEj5H1",
    "id" : 906039027039576064,
    "created_at" : "2017-09-08 06:18:32 +0000",
    "user" : {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "protected" : false,
      "id_str" : "168148402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797414171038060544\/7AkMKjj5_normal.jpg",
      "id" : 168148402,
      "verified" : false
    }
  },
  "id" : 906039110078349313,
  "created_at" : "2017-09-08 06:18:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/49aLau90E8",
      "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6311321589631713280",
      "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905885062322954240",
  "text" : "Take a swipe at those morning routine.  https:\/\/t.co\/49aLau90E8 \uD83D\uDE02",
  "id" : 905885062322954240,
  "created_at" : "2017-09-07 20:06:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/medium.com\" rel=\"nofollow\"\u003EMedium\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Trey Causey",
      "screen_name" : "treycausey",
      "indices" : [ 37, 48 ],
      "id_str" : "237254045",
      "id" : 237254045
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/905883282574856193\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/LTATYzTPMj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJJYCy9V4AAAmnb.jpg",
      "id_str" : "905883280116932608",
      "id" : 905883280116932608,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJJYCy9V4AAAmnb.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 374,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LTATYzTPMj"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/JlYBkeSX9X",
      "expanded_url" : "https:\/\/medium.com\/@treycausey\/rise-of-the-data-product-manager-2fb9961b21d1#---241-349",
      "display_url" : "medium.com\/@treycausey\/ri\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905883282574856193",
  "text" : "\u201CRise of the Data Product Manager\u201D\u200A\u2014\u200A@treycausey https:\/\/t.co\/JlYBkeSX9X https:\/\/t.co\/LTATYzTPMj",
  "id" : 905883282574856193,
  "created_at" : "2017-09-07 19:59:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/OW7jfDFRJR",
      "expanded_url" : "https:\/\/sg.yahoo.com\/news\/melissa-koh-wedding-sponsored-guests-034918551.html",
      "display_url" : "sg.yahoo.com\/news\/melissa-k\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905880358180319233",
  "text" : "Instagram influencer wedding was so sponsored, guests and netizens don\u2019t know if they\u2019re mad or sad https:\/\/t.co\/OW7jfDFRJR",
  "id" : 905880358180319233,
  "created_at" : "2017-09-07 19:48:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/fT8pdPJgAN",
      "expanded_url" : "http:\/\/sc.mp\/jZP6QL",
      "display_url" : "sc.mp\/jZP6QL"
    } ]
  },
  "geo" : { },
  "id_str" : "905879277886984192",
  "text" : "It will never be even we share the same culture heritage. Period. https:\/\/t.co\/fT8pdPJgAN",
  "id" : 905879277886984192,
  "created_at" : "2017-09-07 19:43:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/PhAtZemsL0",
      "expanded_url" : "https:\/\/twitter.com\/mikecosgrave\/status\/905768431466708993",
      "display_url" : "twitter.com\/mikecosgrave\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905871699278745605",
  "text" : "I like to know too. https:\/\/t.co\/PhAtZemsL0",
  "id" : 905871699278745605,
  "created_at" : "2017-09-07 19:13:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jamie (democrats' anger translator) Bowen",
      "screen_name" : "jamiesbowen",
      "indices" : [ 3, 15 ],
      "id_str" : "802868663095279616",
      "id" : 802868663095279616
    }, {
      "name" : "\uD83C\uDF99Jean Bolduc",
      "screen_name" : "JeanBolduc",
      "indices" : [ 17, 28 ],
      "id_str" : "16131991",
      "id" : 16131991
    }, {
      "name" : "Liz Plank",
      "screen_name" : "feministabulous",
      "indices" : [ 29, 45 ],
      "id_str" : "267915933",
      "id" : 267915933
    }, {
      "name" : "ROSIE",
      "screen_name" : "Rosie",
      "indices" : [ 46, 52 ],
      "id_str" : "25203361",
      "id" : 25203361
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905821821261447168",
  "text" : "RT @jamiesbowen: @JeanBolduc @feministabulous @Rosie No. They don't. They can't apply for food stamps, or other assistance. Not even social\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\uD83C\uDF99Jean Bolduc",
        "screen_name" : "JeanBolduc",
        "indices" : [ 0, 11 ],
        "id_str" : "16131991",
        "id" : 16131991
      }, {
        "name" : "Liz Plank",
        "screen_name" : "feministabulous",
        "indices" : [ 12, 28 ],
        "id_str" : "267915933",
        "id" : 267915933
      }, {
        "name" : "ROSIE",
        "screen_name" : "Rosie",
        "indices" : [ 29, 35 ],
        "id_str" : "25203361",
        "id" : 25203361
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "905518711397896192",
    "geo" : { },
    "id_str" : "905520506157322240",
    "in_reply_to_user_id" : 16131991,
    "text" : "@JeanBolduc @feministabulous @Rosie No. They don't. They can't apply for food stamps, or other assistance. Not even social security which they pay into. This includes sales tax",
    "id" : 905520506157322240,
    "in_reply_to_status_id" : 905518711397896192,
    "created_at" : "2017-09-06 19:58:07 +0000",
    "in_reply_to_screen_name" : "JeanBolduc",
    "in_reply_to_user_id_str" : "16131991",
    "user" : {
      "name" : "Jamie (democrats' anger translator) Bowen",
      "screen_name" : "jamiesbowen",
      "protected" : false,
      "id_str" : "802868663095279616",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/819750757549436928\/ZTqVCgDl_normal.jpg",
      "id" : 802868663095279616,
      "verified" : false
    }
  },
  "id" : 905821821261447168,
  "created_at" : "2017-09-07 15:55:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/Qge1nEoBQB",
      "expanded_url" : "https:\/\/twitter.com\/susie_dent\/status\/905762389404119042",
      "display_url" : "twitter.com\/susie_dent\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905821563643195392",
  "text" : "I prefer \"gigglemug\" https:\/\/t.co\/Qge1nEoBQB",
  "id" : 905821563643195392,
  "created_at" : "2017-09-07 15:54:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/b8QtD6cTL2",
      "expanded_url" : "https:\/\/twitter.com\/dtunkelang\/status\/905122145423966209",
      "display_url" : "twitter.com\/dtunkelang\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905761534047084544",
  "text" : "My fav part \"Most of the hard work for machine learning is data transformation.\" https:\/\/t.co\/b8QtD6cTL2",
  "id" : 905761534047084544,
  "created_at" : "2017-09-07 11:55:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Page \u88F4\u6770",
      "screen_name" : "JNBPage",
      "indices" : [ 3, 11 ],
      "id_str" : "118062152",
      "id" : 118062152
    }, {
      "name" : "Alastair Gale",
      "screen_name" : "AlastairGale",
      "indices" : [ 115, 128 ],
      "id_str" : "222678761",
      "id" : 222678761
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905729008914108416",
  "text" : "RT @JNBPage: How did North Korea's nuke program move so fast? Clue: scientists who studied abroad, esp. in China w\/@AlastairGale \nhttps:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Alastair Gale",
        "screen_name" : "AlastairGale",
        "indices" : [ 102, 115 ],
        "id_str" : "222678761",
        "id" : 222678761
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JNBPage\/status\/905458918595346432\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/L4KkC26lZJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJDWBDzV4AAVORf.jpg",
        "id_str" : "905458838790332416",
        "id" : 905458838790332416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJDWBDzV4AAVORf.jpg",
        "sizes" : [ {
          "h" : 560,
          "resize" : "fit",
          "w" : 1059
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 1059
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 1059
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/L4KkC26lZJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/PDXZnQTEzN",
        "expanded_url" : "http:\/\/on.wsj.com\/2eLwlMC",
        "display_url" : "on.wsj.com\/2eLwlMC"
      } ]
    },
    "geo" : { },
    "id_str" : "905458918595346432",
    "text" : "How did North Korea's nuke program move so fast? Clue: scientists who studied abroad, esp. in China w\/@AlastairGale \nhttps:\/\/t.co\/PDXZnQTEzN https:\/\/t.co\/L4KkC26lZJ",
    "id" : 905458918595346432,
    "created_at" : "2017-09-06 15:53:23 +0000",
    "user" : {
      "name" : "Jeremy Page \u88F4\u6770",
      "screen_name" : "JNBPage",
      "protected" : false,
      "id_str" : "118062152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000805174153\/c1bff9efffec03a3f846ee1cccfc559b_normal.jpeg",
      "id" : 118062152,
      "verified" : true
    }
  },
  "id" : 905729008914108416,
  "created_at" : "2017-09-07 09:46:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDC69\u200D\uD83D\uDCBB @DynamicWebPaige \u2728",
      "screen_name" : "DynamicWebPaige",
      "indices" : [ 0, 16 ],
      "id_str" : "18104734",
      "id" : 18104734
    }, {
      "name" : "Microsoft Azure",
      "screen_name" : "Azure",
      "indices" : [ 17, 23 ],
      "id_str" : "17000457",
      "id" : 17000457
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/N8lS0ttZHy",
      "expanded_url" : "https:\/\/notebooks.azure.com\/mryap\/libraries\/test123\/html\/Performance_Testing_Azure_Local.ipynb",
      "display_url" : "notebooks.azure.com\/mryap\/librarie\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "897114538746802176",
  "geo" : { },
  "id_str" : "905728703916900352",
  "in_reply_to_user_id" : 18104734,
  "text" : "@DynamicWebPaige @Azure My locally-hosted Jupyter seems to give a better performance https:\/\/t.co\/N8lS0ttZHy",
  "id" : 905728703916900352,
  "in_reply_to_status_id" : 897114538746802176,
  "created_at" : "2017-09-07 09:45:25 +0000",
  "in_reply_to_screen_name" : "DynamicWebPaige",
  "in_reply_to_user_id_str" : "18104734",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ghostmonth",
      "indices" : [ 50, 61 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 62, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/uZPAnYBOaK",
      "expanded_url" : "http:\/\/www.zaobao.com.sg\/znews\/singapore\/story20170907-793401",
      "display_url" : "zaobao.com.sg\/znews\/singapor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905723769263677440",
  "text" : "Inviting the departed soul to return for a treat. #Ghostmonth #Singapore  https:\/\/t.co\/uZPAnYBOaK",
  "id" : 905723769263677440,
  "created_at" : "2017-09-07 09:25:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/905723010916773888\/photo\/1",
      "indices" : [ 4, 27 ],
      "url" : "https:\/\/t.co\/a1JDONt2dl",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJHGRdfXoAAgh3-.jpg",
      "id_str" : "905723003354456064",
      "id" : 905723003354456064,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJHGRdfXoAAgh3-.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 898,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 898,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 636,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 898,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/a1JDONt2dl"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905723010916773888",
  "text" : "FYI https:\/\/t.co\/a1JDONt2dl",
  "id" : 905723010916773888,
  "created_at" : "2017-09-07 09:22:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Prof Jackie Carter",
      "screen_name" : "JackieCarter",
      "indices" : [ 3, 16 ],
      "id_str" : "19656274",
      "id" : 19656274
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/JackieCarter\/status\/905684326142144512\/photo\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/RL043YjOKw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DJGjFjRXoAEQ-C2.jpg",
      "id_str" : "905684315840946177",
      "id" : 905684315840946177,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJGjFjRXoAEQ-C2.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 491
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1022,
        "resize" : "fit",
        "w" : 738
      }, {
        "h" : 1022,
        "resize" : "fit",
        "w" : 738
      }, {
        "h" : 1022,
        "resize" : "fit",
        "w" : 738
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RL043YjOKw"
    } ],
    "hashtags" : [ {
      "text" : "wiasn",
      "indices" : [ 75, 81 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905684658104537089",
  "text" : "RT @JackieCarter: Imposter Syndrome? Some good advice here (via LinkedIn). #wiasn https:\/\/t.co\/RL043YjOKw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JackieCarter\/status\/905684326142144512\/photo\/1",
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/RL043YjOKw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DJGjFjRXoAEQ-C2.jpg",
        "id_str" : "905684315840946177",
        "id" : 905684315840946177,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DJGjFjRXoAEQ-C2.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 491
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1022,
          "resize" : "fit",
          "w" : 738
        }, {
          "h" : 1022,
          "resize" : "fit",
          "w" : 738
        }, {
          "h" : 1022,
          "resize" : "fit",
          "w" : 738
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RL043YjOKw"
      } ],
      "hashtags" : [ {
        "text" : "wiasn",
        "indices" : [ 57, 63 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "905684326142144512",
    "text" : "Imposter Syndrome? Some good advice here (via LinkedIn). #wiasn https:\/\/t.co\/RL043YjOKw",
    "id" : 905684326142144512,
    "created_at" : "2017-09-07 06:49:04 +0000",
    "user" : {
      "name" : "Prof Jackie Carter",
      "screen_name" : "JackieCarter",
      "protected" : false,
      "id_str" : "19656274",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1345208779\/Jackie_5_normal.JPG",
      "id" : 19656274,
      "verified" : false
    }
  },
  "id" : 905684658104537089,
  "created_at" : "2017-09-07 06:50:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/kIvNvX9SQb",
      "expanded_url" : "https:\/\/github.com\/jennybc\/operation-chromebook#readme",
      "display_url" : "github.com\/jennybc\/operat\u2026"
    }, {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/geTs5dV5eJ",
      "expanded_url" : "https:\/\/jwhollister.com\/r\/2017\/04\/14\/chromebook-4-rstats.html",
      "display_url" : "jwhollister.com\/r\/2017\/04\/14\/c\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905517737585364992",
  "text" : "A lot of command line to set up Chrome book for data science development. https:\/\/t.co\/kIvNvX9SQb | https:\/\/t.co\/geTs5dV5eJ",
  "id" : 905517737585364992,
  "created_at" : "2017-09-06 19:47:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Siemon",
      "screen_name" : "BenjaminJS",
      "indices" : [ 3, 14 ],
      "id_str" : "23258865",
      "id" : 23258865
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905510273779470336",
  "text" : "RT @BenjaminJS: Anyone who woke up from a 2 year coma and was shown this picture would 100% think it was a new CBS sitcom. https:\/\/t.co\/hsd\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BenjaminJS\/status\/876294250043920386\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/hsdsyP9TRx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCk48nkV0AExjMT.jpg",
        "id_str" : "876294216564985857",
        "id" : 876294216564985857,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCk48nkV0AExjMT.jpg",
        "sizes" : [ {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 376,
          "resize" : "fit",
          "w" : 704
        }, {
          "h" : 376,
          "resize" : "fit",
          "w" : 704
        }, {
          "h" : 376,
          "resize" : "fit",
          "w" : 704
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hsdsyP9TRx"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "876294250043920386",
    "text" : "Anyone who woke up from a 2 year coma and was shown this picture would 100% think it was a new CBS sitcom. https:\/\/t.co\/hsdsyP9TRx",
    "id" : 876294250043920386,
    "created_at" : "2017-06-18 04:23:24 +0000",
    "user" : {
      "name" : "Ben Siemon",
      "screen_name" : "BenjaminJS",
      "protected" : false,
      "id_str" : "23258865",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/718113306976714752\/phVyfbvO_normal.jpg",
      "id" : 23258865,
      "verified" : true
    }
  },
  "id" : 905510273779470336,
  "created_at" : "2017-09-06 19:17:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chin from Taiwan",
      "screen_name" : "chinfromtaiwan",
      "indices" : [ 3, 18 ],
      "id_str" : "796238169939189761",
      "id" : 796238169939189761
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Taichung",
      "indices" : [ 115, 124 ]
    }, {
      "text" : "Taiwan",
      "indices" : [ 125, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905472998672670724",
  "text" : "RT @chinfromtaiwan: Middle of ghost month here in Taiwan. We worship the ghosts, pray for them and give them food. #Taichung #Taiwan #Ghost\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/chinfromtaiwan\/status\/904990565464391680\/video\/1",
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/vDwGQeJnsH",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/904990164480434176\/pu\/img\/XN-6dmtMmtyncT-7.jpg",
        "id_str" : "904990164480434176",
        "id" : 904990164480434176,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/904990164480434176\/pu\/img\/XN-6dmtMmtyncT-7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vDwGQeJnsH"
      } ],
      "hashtags" : [ {
        "text" : "Taichung",
        "indices" : [ 95, 104 ]
      }, {
        "text" : "Taiwan",
        "indices" : [ 105, 112 ]
      }, {
        "text" : "Ghostmonth",
        "indices" : [ 113, 124 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "904990565464391680",
    "text" : "Middle of ghost month here in Taiwan. We worship the ghosts, pray for them and give them food. #Taichung #Taiwan #Ghostmonth https:\/\/t.co\/vDwGQeJnsH",
    "id" : 904990565464391680,
    "created_at" : "2017-09-05 08:52:19 +0000",
    "user" : {
      "name" : "Chin from Taiwan",
      "screen_name" : "chinfromtaiwan",
      "protected" : false,
      "id_str" : "796238169939189761",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1016207139121971200\/MGpqyX4b_normal.jpg",
      "id" : 796238169939189761,
      "verified" : false
    }
  },
  "id" : 905472998672670724,
  "created_at" : "2017-09-06 16:49:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/Iakh8uOgVl",
      "expanded_url" : "https:\/\/twitter.com\/BlueshiftMaps\/status\/905470760797388800",
      "display_url" : "twitter.com\/BlueshiftMaps\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905472623538307072",
  "text" : "Good to know https:\/\/t.co\/Iakh8uOgVl",
  "id" : 905472623538307072,
  "created_at" : "2017-09-06 16:47:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/fWou3n9DRP",
      "expanded_url" : "https:\/\/www.instagram.com\/everydaycarry\/",
      "display_url" : "instagram.com\/everydaycarry\/"
    } ]
  },
  "geo" : { },
  "id_str" : "905456725993639936",
  "text" : "The one reason why I am luring on Instagram nowadays. https:\/\/t.co\/fWou3n9DRP",
  "id" : 905456725993639936,
  "created_at" : "2017-09-06 15:44:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/A2dC0QZcnz",
      "expanded_url" : "http:\/\/aquadam-europe.com\/floodprotection.html",
      "display_url" : "aquadam-europe.com\/floodprotectio\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905428703785754624",
  "text" : "Just as I was thinking to bring the flood protection barriers into Europe, someone has already done it...  https:\/\/t.co\/A2dC0QZcnz",
  "id" : 905428703785754624,
  "created_at" : "2017-09-06 13:53:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/q0G6wtdF1H",
      "expanded_url" : "http:\/\/www.manchestereveningnews.co.uk\/news\/greater-manchester-news\/post-van-blocks-metrolink-trams-13436262",
      "display_url" : "manchestereveningnews.co.uk\/news\/greater-m\u2026"
    }, {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/QBp7JTTefs",
      "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/parking-pain-luas-nose-to-nose-with-trucks-as-tram-tests-begin-1.3210393",
      "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905424973724045313",
  "text" : "In these part of the world, they like Tram https:\/\/t.co\/q0G6wtdF1H | https:\/\/t.co\/QBp7JTTefs",
  "id" : 905424973724045313,
  "created_at" : "2017-09-06 13:38:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/EV8nied4Gb",
      "expanded_url" : "http:\/\/www.disclose.tv\/news\/prepper_saves_home_with_aqua_dam_after_everyone_called_him_crazy\/134475",
      "display_url" : "disclose.tv\/news\/prepper_s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905423895213342721",
  "text" : "Prepper Saves Home With Aqua Dam After Everyone Called Him Crazy - &gt; https:\/\/t.co\/EV8nied4Gb",
  "id" : 905423895213342721,
  "created_at" : "2017-09-06 13:34:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/K66PPjrvH3",
      "expanded_url" : "https:\/\/twitter.com\/DynamicWebPaige\/status\/897114538746802176",
      "display_url" : "twitter.com\/DynamicWebPaig\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905388025018032129",
  "text" : "I need to try this. https:\/\/t.co\/K66PPjrvH3",
  "id" : 905388025018032129,
  "created_at" : "2017-09-06 11:11:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/EYPF5fInDJ",
      "expanded_url" : "https:\/\/notebooks.azure.com\/mryap\/libraries\/ml-model",
      "display_url" : "notebooks.azure.com\/mryap\/librarie\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905385062425911296",
  "text" : "I came across this yesterday. Jupyter on Azure platform and I give it a spin https:\/\/t.co\/EYPF5fInDJ",
  "id" : 905385062425911296,
  "created_at" : "2017-09-06 10:59:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Haig",
      "screen_name" : "matthaig1",
      "indices" : [ 3, 13 ],
      "id_str" : "35270579",
      "id" : 35270579
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905350058169823232",
  "text" : "RT @matthaig1: America first. Britain first. Humans last. Nationalism is just the public relations version of racism.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "905183072651530240",
    "text" : "America first. Britain first. Humans last. Nationalism is just the public relations version of racism.",
    "id" : 905183072651530240,
    "created_at" : "2017-09-05 21:37:16 +0000",
    "user" : {
      "name" : "Matt Haig",
      "screen_name" : "matthaig1",
      "protected" : false,
      "id_str" : "35270579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977116656278409217\/VvItuPAI_normal.jpg",
      "id" : 35270579,
      "verified" : true
    }
  },
  "id" : 905350058169823232,
  "created_at" : "2017-09-06 08:40:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joy Reid",
      "screen_name" : "JoyAnnReid",
      "indices" : [ 3, 14 ],
      "id_str" : "49698134",
      "id" : 49698134
    }, {
      "name" : "MSNBC",
      "screen_name" : "MSNBC",
      "indices" : [ 20, 26 ],
      "id_str" : "2836421",
      "id" : 2836421
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905136853136834563",
  "text" : "RT @JoyAnnReid: Per @MSNBC just now. The Trump administration is potentially about to deport the wife of a U.S. soldier set to be deployed\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "MSNBC",
        "screen_name" : "MSNBC",
        "indices" : [ 4, 10 ],
        "id_str" : "2836421",
        "id" : 2836421
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "905127477009362949",
    "text" : "Per @MSNBC just now. The Trump administration is potentially about to deport the wife of a U.S. soldier set to be deployed to Afghanistan.",
    "id" : 905127477009362949,
    "created_at" : "2017-09-05 17:56:21 +0000",
    "user" : {
      "name" : "Joy Reid",
      "screen_name" : "JoyAnnReid",
      "protected" : false,
      "id_str" : "49698134",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/868433375706701824\/xJ3Me1KK_normal.jpg",
      "id" : 49698134,
      "verified" : true
    }
  },
  "id" : 905136853136834563,
  "created_at" : "2017-09-05 18:33:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/RqaBal5Z0p",
      "expanded_url" : "https:\/\/venturebeat.com\/2015\/05\/09\/goodbye-saas-hello-containers-as-a-service\/",
      "display_url" : "venturebeat.com\/2015\/05\/09\/goo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905080326875176960",
  "text" : "Goodbye, SaaS \u2014 hello, Containers-as-a-Service https:\/\/t.co\/RqaBal5Z0p",
  "id" : 905080326875176960,
  "created_at" : "2017-09-05 14:49:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Kiernan",
      "screen_name" : "mikier",
      "indices" : [ 3, 10 ],
      "id_str" : "18778002",
      "id" : 18778002
    }, {
      "name" : "I BIKE Dublin",
      "screen_name" : "IBIKEDublin",
      "indices" : [ 101, 113 ],
      "id_str" : "877852976160931841",
      "id" : 877852976160931841
    }, {
      "name" : "bike in Dublin",
      "screen_name" : "bike_inDub",
      "indices" : [ 114, 125 ],
      "id_str" : "3422094604",
      "id" : 3422094604
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905051695658491904",
  "text" : "RT @mikier: School's back,which means parents just park in cycle lanes all they want,9 cars in total @IBIKEDublin @bike_inDub @clontarfIE @\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "I BIKE Dublin",
        "screen_name" : "IBIKEDublin",
        "indices" : [ 89, 101 ],
        "id_str" : "877852976160931841",
        "id" : 877852976160931841
      }, {
        "name" : "bike in Dublin",
        "screen_name" : "bike_inDub",
        "indices" : [ 102, 113 ],
        "id_str" : "3422094604",
        "id" : 3422094604
      }, {
        "name" : "An Garda S\u00EDoch\u00E1na",
        "screen_name" : "GardaTraffic",
        "indices" : [ 126, 139 ],
        "id_str" : "295705264",
        "id" : 295705264
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mikier\/status\/905042586267734016\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/c2k6uNxv9D",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DI9baxrXUAAQ1Nm.jpg",
        "id_str" : "905042565694705664",
        "id" : 905042565694705664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI9baxrXUAAQ1Nm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 350
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 617
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1053
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1053
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/c2k6uNxv9D"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "905042586267734016",
    "text" : "School's back,which means parents just park in cycle lanes all they want,9 cars in total @IBIKEDublin @bike_inDub @clontarfIE @GardaTraffic https:\/\/t.co\/c2k6uNxv9D",
    "id" : 905042586267734016,
    "created_at" : "2017-09-05 12:19:02 +0000",
    "user" : {
      "name" : "Michael Kiernan",
      "screen_name" : "mikier",
      "protected" : false,
      "id_str" : "18778002",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/825415635534491648\/ATKFPt82_normal.jpg",
      "id" : 18778002,
      "verified" : false
    }
  },
  "id" : 905051695658491904,
  "created_at" : "2017-09-05 12:55:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/905049930531786752\/photo\/1",
      "indices" : [ 134, 157 ],
      "url" : "https:\/\/t.co\/ovNgwID0ZQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DI9hQf8WAAAClqE.jpg",
      "id_str" : "905048986205159424",
      "id" : 905048986205159424,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI9hQf8WAAAClqE.jpg",
      "sizes" : [ {
        "h" : 148,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 202,
        "resize" : "fit",
        "w" : 931
      }, {
        "h" : 202,
        "resize" : "fit",
        "w" : 931
      }, {
        "h" : 202,
        "resize" : "fit",
        "w" : 931
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ovNgwID0ZQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905049930531786752",
  "text" : "IS there a way to setup in pandas DataFrame (like Rprofile) where float64 value is automatically reformat as dollars and cent? Thanks https:\/\/t.co\/ovNgwID0ZQ",
  "id" : 905049930531786752,
  "created_at" : "2017-09-05 12:48:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andreeeeeeeeaaaaaa!",
      "screen_name" : "brandalisms",
      "indices" : [ 0, 12 ],
      "id_str" : "249318820",
      "id" : 249318820
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "905034890995007488",
  "geo" : { },
  "id_str" : "905043529143132162",
  "in_reply_to_user_id" : 249318820,
  "text" : "@brandalisms I don't understand deep learning and snapchat.",
  "id" : 905043529143132162,
  "in_reply_to_status_id" : 905034890995007488,
  "created_at" : "2017-09-05 12:22:46 +0000",
  "in_reply_to_screen_name" : "brandalisms",
  "in_reply_to_user_id_str" : "249318820",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/905019141098266625\/photo\/1",
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/onl93CdkqB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DI9F6jnXkAARS4z.png",
      "id_str" : "905018922419851264",
      "id" : 905018922419851264,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI9F6jnXkAARS4z.png",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 470
      }, {
        "h" : 2527,
        "resize" : "fit",
        "w" : 580
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 275
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 156
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/onl93CdkqB"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905019141098266625",
  "text" : "Battle for e-commerce dominance in SE Asia. https:\/\/t.co\/onl93CdkqB",
  "id" : 905019141098266625,
  "created_at" : "2017-09-05 10:45:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conor Behan",
      "screen_name" : "platinumjones",
      "indices" : [ 3, 17 ],
      "id_str" : "18449042",
      "id" : 18449042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905018630328463360",
  "text" : "RT @platinumjones: This is amazing esp for young audiences to see someone like them &lt;3 Afro-Irish woman is newest RT\u00C9 News TV presenter htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/z19YzklME0",
        "expanded_url" : "http:\/\/www.rte.ie\/entertainment\/2017\/0904\/902238-afro-irish-woman-is-newest-rte-news-tv-presenter\/",
        "display_url" : "rte.ie\/entertainment\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "904752235401314304",
    "text" : "This is amazing esp for young audiences to see someone like them &lt;3 Afro-Irish woman is newest RT\u00C9 News TV presenter https:\/\/t.co\/z19YzklME0",
    "id" : 904752235401314304,
    "created_at" : "2017-09-04 17:05:17 +0000",
    "user" : {
      "name" : "Conor Behan",
      "screen_name" : "platinumjones",
      "protected" : false,
      "id_str" : "18449042",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/722572397069615104\/K5P_qDpu_normal.jpg",
      "id" : 18449042,
      "verified" : false
    }
  },
  "id" : 905018630328463360,
  "created_at" : "2017-09-05 10:43:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "905012848828538880",
  "text" : "Which app do you usually use first when you get up in the morning? Alarm clock doesn't count!",
  "id" : 905012848828538880,
  "created_at" : "2017-09-05 10:20:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/O0w8B1DFmF",
      "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/asiapacific\/for-chinese-millennials--despondency-has-a-brand-name-9185520",
      "display_url" : "channelnewsasia.com\/news\/asiapacif\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "905012222740574208",
  "text" : "Best-selling \"sitting-around-and-waiting-to-die\" matcha milk tea https:\/\/t.co\/O0w8B1DFmF\"",
  "id" : 905012222740574208,
  "created_at" : "2017-09-05 10:18:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/Vsf3aFbdrS",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/904676214740312065",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904985289927659521",
  "text" : "Another significant of this setup runs in virtualenv,  a tool to create isolated Python environments. https:\/\/t.co\/Vsf3aFbdrS",
  "id" : 904985289927659521,
  "created_at" : "2017-09-05 08:31:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904984742260592641",
  "text" : "Employers always ask for experience. Would NASA able to embark moonshot project if they demand people who has experience flying to the moon?",
  "id" : 904984742260592641,
  "created_at" : "2017-09-05 08:29:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904982030924083201",
  "text" : "Rise and shine\nThere is no shine today\nThe only shine is from my mobile screen.",
  "id" : 904982030924083201,
  "created_at" : "2017-09-05 08:18:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/S2ExkYP66h",
      "expanded_url" : "https:\/\/2017.dublin.wordcamp.org",
      "display_url" : "2017.dublin.wordcamp.org"
    } ]
  },
  "geo" : { },
  "id_str" : "904981763960819712",
  "text" : "Wordpress Camp is back https:\/\/t.co\/S2ExkYP66h",
  "id" : 904981763960819712,
  "created_at" : "2017-09-05 08:17:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/jqiYT6yvWd",
      "expanded_url" : "https:\/\/twitter.com\/chrisjhorn\/status\/904757742451163137",
      "display_url" : "twitter.com\/chrisjhorn\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904763968954216448",
  "text" : "\"When you live in another country, you also learn a lot more about your own culture.\" https:\/\/t.co\/jqiYT6yvWd",
  "id" : 904763968954216448,
  "created_at" : "2017-09-04 17:51:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/i9tpK1ABwL",
      "expanded_url" : "https:\/\/medium.com\/towards-data-science\/how-do-machine-learning-algorithms-learn-bias-555809a1decb",
      "display_url" : "medium.com\/towards-data-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904753641365569536",
  "text" : "\u201CHow Do Machine Learning Algorithms Learn Bias?\u201D https:\/\/t.co\/i9tpK1ABwL",
  "id" : 904753641365569536,
  "created_at" : "2017-09-04 17:10:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gacpsummit",
      "indices" : [ 68, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904710738169786368",
  "text" : "I must have miss the memo. Any new Google Analytics announcement at #gacpsummit?",
  "id" : 904710738169786368,
  "created_at" : "2017-09-04 14:20:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "UK in Singapore \uD83C\uDDEC\uD83C\uDDE7",
      "screen_name" : "UKinSingapore",
      "indices" : [ 3, 17 ],
      "id_str" : "739657886",
      "id" : 739657886
    }, {
      "name" : "Sajid Javid",
      "screen_name" : "sajidjavid",
      "indices" : [ 20, 31 ],
      "id_str" : "20052899",
      "id" : 20052899
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 93, 103 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904694052666437632",
  "text" : "RT @UKinSingapore: .@sajidjavid taking a break from a packed programme to have a quick lunch #Singapore-style at Newton Hawker Centre. http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Sajid Javid",
        "screen_name" : "sajidjavid",
        "indices" : [ 1, 12 ],
        "id_str" : "20052899",
        "id" : 20052899
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/UKinSingapore\/status\/900619319767257088\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/czUZ47SoPD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DH-jQ20UAAApsk4.jpg",
        "id_str" : "900617960485552128",
        "id" : 900617960485552128,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DH-jQ20UAAApsk4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 898
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 898
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 898
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/czUZ47SoPD"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 74, 84 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "900619319767257088",
    "text" : ".@sajidjavid taking a break from a packed programme to have a quick lunch #Singapore-style at Newton Hawker Centre. https:\/\/t.co\/czUZ47SoPD",
    "id" : 900619319767257088,
    "created_at" : "2017-08-24 07:22:33 +0000",
    "user" : {
      "name" : "UK in Singapore \uD83C\uDDEC\uD83C\uDDE7",
      "screen_name" : "UKinSingapore",
      "protected" : false,
      "id_str" : "739657886",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876985092299661312\/sVQykTuJ_normal.jpg",
      "id" : 739657886,
      "verified" : true
    }
  },
  "id" : 904694052666437632,
  "created_at" : "2017-09-04 13:14:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904682290042851328",
  "text" : "One wonders does all those condemnations from countries around the world is going to make any different to that country?",
  "id" : 904682290042851328,
  "created_at" : "2017-09-04 12:27:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/904676214740312065\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/BwFkIyKaHH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DI4OMzdW0AErFYU.jpg",
      "id_str" : "904676188282605569",
      "id" : 904676188282605569,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DI4OMzdW0AErFYU.jpg",
      "sizes" : [ {
        "h" : 398,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 399,
        "resize" : "fit",
        "w" : 1204
      }, {
        "h" : 399,
        "resize" : "fit",
        "w" : 1204
      }, {
        "h" : 225,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BwFkIyKaHH"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904676214740312065",
  "text" : "First time using Terminal to install Jupyter on Windows. https:\/\/t.co\/BwFkIyKaHH",
  "id" : 904676214740312065,
  "created_at" : "2017-09-04 12:03:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Beau Willimon",
      "screen_name" : "BeauWillimon",
      "indices" : [ 3, 16 ],
      "id_str" : "28185163",
      "id" : 28185163
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DREAMers",
      "indices" : [ 124, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904495732811595776",
  "text" : "RT @BeauWillimon: To those of you who rail against DACA, the iPhone on which you type your tweets is partly a result of the #DREAMers who m\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DREAMers",
        "indices" : [ 106, 115 ]
      } ],
      "urls" : [ {
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/ZwLePv33ss",
        "expanded_url" : "https:\/\/twitter.com\/tim_cook\/status\/904322640654897152",
        "display_url" : "twitter.com\/tim_cook\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "904427291480133641",
    "text" : "To those of you who rail against DACA, the iPhone on which you type your tweets is partly a result of the #DREAMers who made it possible. https:\/\/t.co\/ZwLePv33ss",
    "id" : 904427291480133641,
    "created_at" : "2017-09-03 19:34:04 +0000",
    "user" : {
      "name" : "Beau Willimon",
      "screen_name" : "BeauWillimon",
      "protected" : false,
      "id_str" : "28185163",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839725443485925376\/KA2moEEO_normal.jpg",
      "id" : 28185163,
      "verified" : true
    }
  },
  "id" : 904495732811595776,
  "created_at" : "2017-09-04 00:06:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/bznXCcjjS6",
      "expanded_url" : "http:\/\/www.istartedsomething.com\/20140616\/the-12-step-process-to-download-microsoft-sql-server-express-2014\/",
      "display_url" : "istartedsomething.com\/20140616\/the-1\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904352656487063553",
  "text" : "The convoluted way to download a software. Why?! https:\/\/t.co\/bznXCcjjS6",
  "id" : 904352656487063553,
  "created_at" : "2017-09-03 14:37:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michelle Cooke",
      "screen_name" : "Mich_Cooke",
      "indices" : [ 3, 14 ],
      "id_str" : "331953702",
      "id" : 331953702
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "904337352641974272",
  "text" : "RT @Mich_Cooke: My friend is staying in a hotel in Belgium. They've offered her the option of renting a fish for the night, in case she's l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Mich_Cooke\/status\/903918405949972480\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/DG74iRSfhY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DItc5IAVAAAmTPg.jpg",
        "id_str" : "903918286689075200",
        "id" : 903918286689075200,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DItc5IAVAAAmTPg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DG74iRSfhY"
      } ],
      "hashtags" : [ {
        "text" : "noshit",
        "indices" : [ 130, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "903918405949972480",
    "text" : "My friend is staying in a hotel in Belgium. They've offered her the option of renting a fish for the night, in case she's lonely. #noshit https:\/\/t.co\/DG74iRSfhY",
    "id" : 903918405949972480,
    "created_at" : "2017-09-02 09:51:56 +0000",
    "user" : {
      "name" : "Michelle Cooke",
      "screen_name" : "Mich_Cooke",
      "protected" : false,
      "id_str" : "331953702",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/889805308519792640\/8WAWgHaq_normal.jpg",
      "id" : 331953702,
      "verified" : false
    }
  },
  "id" : 904337352641974272,
  "created_at" : "2017-09-03 13:36:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/ZCZFHRtxwH",
      "expanded_url" : "https:\/\/www.oecd.org\/tax\/automatic-exchange\/crs-implementation-and-assistance\/tax-identification-numbers\/Ireland-TIN.pdf",
      "display_url" : "oecd.org\/tax\/automatic-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904327989898477570",
  "text" : "Am I correct to say that TIN and PPS are the same for natural individuals?  https:\/\/t.co\/ZCZFHRtxwH",
  "id" : 904327989898477570,
  "created_at" : "2017-09-03 12:59:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/1CBiZELwlw",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Common_Reporting_Standard",
      "display_url" : "en.wikipedia.org\/wiki\/Common_Re\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904325815080550401",
  "text" : "First reporting is planned September 2017. https:\/\/t.co\/1CBiZELwlw",
  "id" : 904325815080550401,
  "created_at" : "2017-09-03 12:50:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/2IoyXQMgG2",
      "expanded_url" : "https:\/\/stackoverflow.com\/questions\/5352099\/how-to-disable-scientific-notation-in-r",
      "display_url" : "stackoverflow.com\/questions\/5352\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904053909806866432",
  "text" : "How to notate p-values like: 0.0004835312 instead of 4.835312e-04 https:\/\/t.co\/2IoyXQMgG2",
  "id" : 904053909806866432,
  "created_at" : "2017-09-02 18:50:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 122, 145 ],
      "url" : "https:\/\/t.co\/b0uauyrxeH",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/901810453671763968",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904042942561755141",
  "text" : "I went to just a PC store today in Dublin. There are PC with i3 and i7 but no i5. Price of PC with i7 processor is crazy. https:\/\/t.co\/b0uauyrxeH",
  "id" : 904042942561755141,
  "created_at" : "2017-09-02 18:06:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Kirby",
      "screen_name" : "paul1kirby",
      "indices" : [ 3, 14 ],
      "id_str" : "1861764991",
      "id" : 1861764991
    }, {
      "name" : "Information is Beautiful",
      "screen_name" : "infobeautiful",
      "indices" : [ 48, 62 ],
      "id_str" : "54680395",
      "id" : 54680395
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/paul1kirby\/status\/903913695025258497\/photo\/1",
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/Z1oREUMp2d",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DItYtwbWAAMiuzk.jpg",
      "id_str" : "903913693334863875",
      "id" : 903913693334863875,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DItYtwbWAAMiuzk.jpg",
      "sizes" : [ {
        "h" : 655,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 435,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 655,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 655,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Z1oREUMp2d"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/a3q9LZQbCV",
      "expanded_url" : "http:\/\/www.informationisbeautiful.net\/visualizations\/uk-brexit-options-in-the-eurozone-complex-visualized\/",
      "display_url" : "informationisbeautiful.net\/visualizations\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "904010360784605184",
  "text" : "RT @paul1kirby: In &amp; Out of The EU. Updated @infobeautiful chart https:\/\/t.co\/a3q9LZQbCV https:\/\/t.co\/Z1oREUMp2d",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Information is Beautiful",
        "screen_name" : "infobeautiful",
        "indices" : [ 32, 46 ],
        "id_str" : "54680395",
        "id" : 54680395
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/paul1kirby\/status\/903913695025258497\/photo\/1",
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/Z1oREUMp2d",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DItYtwbWAAMiuzk.jpg",
        "id_str" : "903913693334863875",
        "id" : 903913693334863875,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DItYtwbWAAMiuzk.jpg",
        "sizes" : [ {
          "h" : 655,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 435,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 655,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 655,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Z1oREUMp2d"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/a3q9LZQbCV",
        "expanded_url" : "http:\/\/www.informationisbeautiful.net\/visualizations\/uk-brexit-options-in-the-eurozone-complex-visualized\/",
        "display_url" : "informationisbeautiful.net\/visualizations\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "903913695025258497",
    "text" : "In &amp; Out of The EU. Updated @infobeautiful chart https:\/\/t.co\/a3q9LZQbCV https:\/\/t.co\/Z1oREUMp2d",
    "id" : 903913695025258497,
    "created_at" : "2017-09-02 09:33:13 +0000",
    "user" : {
      "name" : "Paul Kirby",
      "screen_name" : "paul1kirby",
      "protected" : false,
      "id_str" : "1861764991",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/419516029203922944\/iq8z0O5N_normal.jpeg",
      "id" : 1861764991,
      "verified" : false
    }
  },
  "id" : 904010360784605184,
  "created_at" : "2017-09-02 15:57:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/NQJVtdo73Q",
      "expanded_url" : "https:\/\/twitter.com\/pdscott\/status\/903959995414446080",
      "display_url" : "twitter.com\/pdscott\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903997243564445698",
  "text" : "\uD83D\uDE2C https:\/\/t.co\/NQJVtdo73Q",
  "id" : 903997243564445698,
  "created_at" : "2017-09-02 15:05:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 0, 15 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "903915698312945664",
  "geo" : { },
  "id_str" : "903995839844057088",
  "in_reply_to_user_id" : 18319658,
  "text" : "@EmeraldDeLeeuw Not until after Halloween!",
  "id" : 903995839844057088,
  "in_reply_to_status_id" : 903915698312945664,
  "created_at" : "2017-09-02 14:59:38 +0000",
  "in_reply_to_screen_name" : "EmeraldDeLeeuw",
  "in_reply_to_user_id_str" : "18319658",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/903921067336626181\/photo\/1",
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/rFzWaftDqn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DItfa1yWsAAT_ix.jpg",
      "id_str" : "903921064937435136",
      "id" : 903921064937435136,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DItfa1yWsAAT_ix.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rFzWaftDqn"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/LmoeWIvXuA",
      "expanded_url" : "http:\/\/ift.tt\/2exoge7",
      "display_url" : "ift.tt\/2exoge7"
    } ]
  },
  "geo" : { },
  "id_str" : "903921067336626181",
  "text" : "AT the market https:\/\/t.co\/LmoeWIvXuA https:\/\/t.co\/rFzWaftDqn",
  "id" : 903921067336626181,
  "created_at" : "2017-09-02 10:02:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shannon Hillis",
      "screen_name" : "hillisthekillis",
      "indices" : [ 3, 19 ],
      "id_str" : "396036120",
      "id" : 396036120
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/hillisthekillis\/status\/903720326231719936\/photo\/1",
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/KtswW7GLSI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DIqoygIUIAEfma_.jpg",
      "id_str" : "903720260812939265",
      "id" : 903720260812939265,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIqoygIUIAEfma_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1440
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1440
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KtswW7GLSI"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903904523416199168",
  "text" : "RT @hillisthekillis: Even in a disaster no one wants the vegan food. https:\/\/t.co\/KtswW7GLSI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/hillisthekillis\/status\/903720326231719936\/photo\/1",
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/KtswW7GLSI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DIqoygIUIAEfma_.jpg",
        "id_str" : "903720260812939265",
        "id" : 903720260812939265,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIqoygIUIAEfma_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1440
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1440
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KtswW7GLSI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "903720326231719936",
    "text" : "Even in a disaster no one wants the vegan food. https:\/\/t.co\/KtswW7GLSI",
    "id" : 903720326231719936,
    "created_at" : "2017-09-01 20:44:50 +0000",
    "user" : {
      "name" : "Shannon Hillis",
      "screen_name" : "hillisthekillis",
      "protected" : false,
      "id_str" : "396036120",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/767135065268744196\/-lVFT6BR_normal.jpg",
      "id" : 396036120,
      "verified" : false
    }
  },
  "id" : 903904523416199168,
  "created_at" : "2017-09-02 08:56:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/g3JxIAysfX",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/902894035622391809",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903769229450727425",
  "text" : "Jupyter is emerging as the standard for sharing reproducible research in the sciences. https:\/\/t.co\/g3JxIAysfX",
  "id" : 903769229450727425,
  "created_at" : "2017-09-01 23:59:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ireland",
      "indices" : [ 24, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903768484630417408",
  "text" : "Are you a journalist in #ireland looking into using Data to sell a story? I can teach u how to use open source data tools to work with data.",
  "id" : 903768484630417408,
  "created_at" : "2017-09-01 23:56:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/903760487552253953\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/9YC7DbMlmo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DIrMj-9W4AE7Ip6.jpg",
      "id_str" : "903759593809043457",
      "id" : 903759593809043457,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIrMj-9W4AE7Ip6.jpg",
      "sizes" : [ {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9YC7DbMlmo"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903760487552253953",
  "text" : "Evaluated &amp; plotted 5 algorithms model and estimate their accuracy on unseen data using \u201CAccuracy\u201D metric https:\/\/t.co\/9YC7DbMlmo",
  "id" : 903760487552253953,
  "created_at" : "2017-09-01 23:24:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/xvnCmaGW5s",
      "expanded_url" : "https:\/\/shar.es\/1Syspi",
      "display_url" : "shar.es\/1Syspi"
    } ]
  },
  "geo" : { },
  "id_str" : "903696319977586689",
  "text" : "Six in ten employees willing to leave Singapore for a better job | Singapore Business Review https:\/\/t.co\/xvnCmaGW5s",
  "id" : 903696319977586689,
  "created_at" : "2017-09-01 19:09:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "President of Ireland",
      "screen_name" : "PresidentIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "569892832",
      "id" : 569892832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903690799984107521",
  "text" : "RT @PresidentIRL: \"As Muslims in Ireland and around the world celebrate today, I would like to wish everyone health and happiness. Eid ul A\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "903670775739273216",
    "text" : "\"As Muslims in Ireland and around the world celebrate today, I would like to wish everyone health and happiness. Eid ul Adha mubarak.\"",
    "id" : 903670775739273216,
    "created_at" : "2017-09-01 17:27:56 +0000",
    "user" : {
      "name" : "President of Ireland",
      "screen_name" : "PresidentIRL",
      "protected" : false,
      "id_str" : "569892832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/768749418187292672\/TSQViQLu_normal.jpg",
      "id" : 569892832,
      "verified" : true
    }
  },
  "id" : 903690799984107521,
  "created_at" : "2017-09-01 18:47:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SENSORPRO",
      "screen_name" : "sensorpro",
      "indices" : [ 3, 13 ],
      "id_str" : "14407527",
      "id" : 14407527
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903686995884552194",
  "text" : "RT @sensorpro: Arguably the most important slide from this morning's breakfast briefing at Google. Great presentation. Food for thought. #g\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sensorpro\/status\/903535770849423361\/photo\/1",
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/38jxnCgpe6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DIoA-nmW4AAV5jB.jpg",
        "id_str" : "903535751022960640",
        "id" : 903535751022960640,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIoA-nmW4AAV5jB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 661,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1128,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 375,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1128,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/38jxnCgpe6"
      } ],
      "hashtags" : [ {
        "text" : "googlebb",
        "indices" : [ 122, 131 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "903535770849423361",
    "text" : "Arguably the most important slide from this morning's breakfast briefing at Google. Great presentation. Food for thought. #googlebb https:\/\/t.co\/38jxnCgpe6",
    "id" : 903535770849423361,
    "created_at" : "2017-09-01 08:31:29 +0000",
    "user" : {
      "name" : "SENSORPRO",
      "screen_name" : "sensorpro",
      "protected" : false,
      "id_str" : "14407527",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/687234495578259456\/YlOE31Lv_normal.png",
      "id" : 14407527,
      "verified" : false
    }
  },
  "id" : 903686995884552194,
  "created_at" : "2017-09-01 18:32:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/EKGMUunMwK",
      "expanded_url" : "https:\/\/twitter.com\/eleafeit\/status\/903638001292541952",
      "display_url" : "twitter.com\/eleafeit\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903681730170822657",
  "text" : "Learn how the 3 methods compare to evaluate advertising response using R https:\/\/t.co\/EKGMUunMwK",
  "id" : 903681730170822657,
  "created_at" : "2017-09-01 18:11:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Tran-Adams",
      "screen_name" : "MTranAdams",
      "indices" : [ 3, 14 ],
      "id_str" : "2506231034",
      "id" : 2506231034
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 16, 22 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903647234583130114",
  "text" : "RT @MTranAdams: @mryap In Canada we have a Health Card but we're not allowed to use it as ID.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "903637998939582464",
    "geo" : { },
    "id_str" : "903647079708463105",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap In Canada we have a Health Card but we're not allowed to use it as ID.",
    "id" : 903647079708463105,
    "in_reply_to_status_id" : 903637998939582464,
    "created_at" : "2017-09-01 15:53:47 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Matthew Tran-Adams",
      "screen_name" : "MTranAdams",
      "protected" : false,
      "id_str" : "2506231034",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/468232662407065600\/Mtfyu2ON_normal.jpeg",
      "id" : 2506231034,
      "verified" : false
    }
  },
  "id" : 903647234583130114,
  "created_at" : "2017-09-01 15:54:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/YBOODbphfv",
      "expanded_url" : "https:\/\/en.m.wikipedia.org\/wiki\/German_identity_card",
      "display_url" : "en.m.wikipedia.org\/wiki\/German_id\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903639577549791232",
  "text" : "3. German has also an ID card too https:\/\/t.co\/YBOODbphfv",
  "id" : 903639577549791232,
  "created_at" : "2017-09-01 15:23:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/wjL8Fls0mM",
      "expanded_url" : "https:\/\/en.m.wikipedia.org\/wiki\/Polish_identity_card",
      "display_url" : "en.m.wikipedia.org\/wiki\/Polish_id\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903638196097081344",
  "text" : "2. Polish neighbor once show me his country ID card https:\/\/t.co\/wjL8Fls0mM",
  "id" : 903638196097081344,
  "created_at" : "2017-09-01 15:18:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903637998939582464",
  "text" : "1. Debates in Ireland on a Public Service Card to access public services (some fear could turn into a defacto ID card)",
  "id" : 903637998939582464,
  "created_at" : "2017-09-01 15:17:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903575011625193472",
  "text" : "Any lawyers\/accountants\/Peace Commissioner on my Twitter network? Need docs to be certified today. Thanks",
  "id" : 903575011625193472,
  "created_at" : "2017-09-01 11:07:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "P\u00F3l \u00D3 Conghaile",
      "screen_name" : "poloconghaile",
      "indices" : [ 3, 17 ],
      "id_str" : "107222035",
      "id" : 107222035
    }, {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 61, 75 ],
      "id_str" : "179569408",
      "id" : 179569408
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903571808431026176",
  "text" : "RT @poloconghaile: Dublin to Hong Kong direct! Big news from @DublinAirport. Ireland\u2019s first ever direct Asia-Pacific service \u2708\uFE0F https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dublin Airport",
        "screen_name" : "DublinAirport",
        "indices" : [ 42, 56 ],
        "id_str" : "179569408",
        "id" : 179569408
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/n6UwesydyG",
        "expanded_url" : "https:\/\/twitter.com\/Indo_Travel_\/status\/903211082264846336",
        "display_url" : "twitter.com\/Indo_Travel_\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "903211512940163072",
    "text" : "Dublin to Hong Kong direct! Big news from @DublinAirport. Ireland\u2019s first ever direct Asia-Pacific service \u2708\uFE0F https:\/\/t.co\/n6UwesydyG",
    "id" : 903211512940163072,
    "created_at" : "2017-08-31 11:03:00 +0000",
    "user" : {
      "name" : "P\u00F3l \u00D3 Conghaile",
      "screen_name" : "poloconghaile",
      "protected" : false,
      "id_str" : "107222035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2963058891\/279e43b644ab5c2df6652c2cf38c30b6_normal.jpeg",
      "id" : 107222035,
      "verified" : true
    }
  },
  "id" : 903571808431026176,
  "created_at" : "2017-09-01 10:54:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/OFj1jJDzpU",
      "expanded_url" : "http:\/\/thediplomat.com\/2017\/08\/singapore-deploys-military-helicopters-for-us-hurricane-operations\/",
      "display_url" : "thediplomat.com\/2017\/08\/singap\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903564517547929600",
  "text" : "The RSAF has five training detachments in the United States. https:\/\/t.co\/OFj1jJDzpU",
  "id" : 903564517547929600,
  "created_at" : "2017-09-01 10:25:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WW2 Tweets from 1940",
      "screen_name" : "RealTimeWWII",
      "indices" : [ 3, 16 ],
      "id_str" : "364488011",
      "id" : 364488011
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903514033378164736",
  "text" : "RT @RealTimeWWII: Huge crowds across China celebrate news of Japanese surrender, finally ending 8 year war that has killed 20-30 million Ch\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RealTimeWWII\/status\/897592152292720640\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/R1xvaSyhb7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DHTjTALXkAAb5m3.jpg",
        "id_str" : "897592141358272512",
        "id" : 897592141358272512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DHTjTALXkAAb5m3.jpg",
        "sizes" : [ {
          "h" : 382,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/R1xvaSyhb7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "897592152292720640",
    "text" : "Huge crowds across China celebrate news of Japanese surrender, finally ending 8 year war that has killed 20-30 million Chinese people. https:\/\/t.co\/R1xvaSyhb7",
    "id" : 897592152292720640,
    "created_at" : "2017-08-15 22:53:40 +0000",
    "user" : {
      "name" : "WW2 Tweets from 1940",
      "screen_name" : "RealTimeWWII",
      "protected" : false,
      "id_str" : "364488011",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1000873869228552193\/RiOi-P6c_normal.jpg",
      "id" : 364488011,
      "verified" : false
    }
  },
  "id" : 903514033378164736,
  "created_at" : "2017-09-01 07:05:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Pierotti",
      "screen_name" : "paul73p",
      "indices" : [ 3, 11 ],
      "id_str" : "298424747",
      "id" : 298424747
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/EbsXxGYDyL",
      "expanded_url" : "https:\/\/twitter.com\/iaindale\/status\/903247466413350912",
      "display_url" : "twitter.com\/iaindale\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "903506722446299136",
  "text" : "RT @paul73p: Husband shocked that the wife he is divorcing isn't putting him first https:\/\/t.co\/EbsXxGYDyL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/EbsXxGYDyL",
        "expanded_url" : "https:\/\/twitter.com\/iaindale\/status\/903247466413350912",
        "display_url" : "twitter.com\/iaindale\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "903499891397148672",
    "text" : "Husband shocked that the wife he is divorcing isn't putting him first https:\/\/t.co\/EbsXxGYDyL",
    "id" : 903499891397148672,
    "created_at" : "2017-09-01 06:08:54 +0000",
    "user" : {
      "name" : "Paul Pierotti",
      "screen_name" : "paul73p",
      "protected" : false,
      "id_str" : "298424747",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/462206785634304000\/gOFQRSAc_normal.jpeg",
      "id" : 298424747,
      "verified" : false
    }
  },
  "id" : 903506722446299136,
  "created_at" : "2017-09-01 06:36:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cerys Matthews",
      "screen_name" : "cerysmatthews",
      "indices" : [ 3, 17 ],
      "id_str" : "36414454",
      "id" : 36414454
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "903498635769389056",
  "text" : "RT @cerysmatthews: How in the hell does the death of 1200 people in Indian floods warrant just 1 min of news,whilst we get 10 mins on Texas\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC News (UK)",
        "screen_name" : "BBCNews",
        "indices" : [ 129, 137 ],
        "id_str" : "612473",
        "id" : 612473
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "902977144690761728",
    "text" : "How in the hell does the death of 1200 people in Indian floods warrant just 1 min of news,whilst we get 10 mins on Texas floods? @BBCNews",
    "id" : 902977144690761728,
    "created_at" : "2017-08-30 19:31:42 +0000",
    "user" : {
      "name" : "Cerys Matthews",
      "screen_name" : "cerysmatthews",
      "protected" : false,
      "id_str" : "36414454",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/911181562372349952\/HJJ1c0r9_normal.jpg",
      "id" : 36414454,
      "verified" : true
    }
  },
  "id" : 903498635769389056,
  "created_at" : "2017-09-01 06:03:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]