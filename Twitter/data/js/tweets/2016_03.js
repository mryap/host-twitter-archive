Grailbird.data.tweets_2016_03 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marta Carluci",
      "screen_name" : "Mapartapa",
      "indices" : [ 3, 13 ],
      "id_str" : "90988366",
      "id" : 90988366
    }, {
      "name" : "TIMEWorld",
      "screen_name" : "TIMEWorld",
      "indices" : [ 89, 99 ],
      "id_str" : "15723281",
      "id" : 15723281
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/D1UbElFW0X",
      "expanded_url" : "http:\/\/ti.me\/1Vbd9mP",
      "display_url" : "ti.me\/1Vbd9mP"
    } ]
  },
  "geo" : { },
  "id_str" : "715663875635601408",
  "text" : "RT @Mapartapa: See Zaha Hadid's most awe-inspiring buildings https:\/\/t.co\/D1UbElFW0X via @TIMEWorld",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TIMEWorld",
        "screen_name" : "TIMEWorld",
        "indices" : [ 74, 84 ],
        "id_str" : "15723281",
        "id" : 15723281
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/D1UbElFW0X",
        "expanded_url" : "http:\/\/ti.me\/1Vbd9mP",
        "display_url" : "ti.me\/1Vbd9mP"
      } ]
    },
    "geo" : { },
    "id_str" : "715662171779309568",
    "text" : "See Zaha Hadid's most awe-inspiring buildings https:\/\/t.co\/D1UbElFW0X via @TIMEWorld",
    "id" : 715662171779309568,
    "created_at" : "2016-03-31 22:08:52 +0000",
    "user" : {
      "name" : "Marta Carluci",
      "screen_name" : "Mapartapa",
      "protected" : false,
      "id_str" : "90988366",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/556662300958924800\/ZXIR3N68_normal.png",
      "id" : 90988366,
      "verified" : false
    }
  },
  "id" : 715663875635601408,
  "created_at" : "2016-03-31 22:15:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christophe HUBERT",
      "screen_name" : "chris75sf",
      "indices" : [ 3, 13 ],
      "id_str" : "77786418",
      "id" : 77786418
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/chris75sf\/status\/715478332695928832\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/LGBv57eUbw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Ce3jv-RUsAAvZnV.jpg",
      "id_str" : "715478329130790912",
      "id" : 715478329130790912,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ce3jv-RUsAAvZnV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LGBv57eUbw"
    } ],
    "hashtags" : [ {
      "text" : "Tesla",
      "indices" : [ 26, 32 ]
    }, {
      "text" : "Model3",
      "indices" : [ 33, 40 ]
    }, {
      "text" : "Apple",
      "indices" : [ 58, 64 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715656220825268225",
  "text" : "RT @chris75sf: D\u00FCsseldorf #Tesla #Model3 line next to the #Apple Store ;) https:\/\/t.co\/LGBv57eUbw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/chris75sf\/status\/715478332695928832\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/LGBv57eUbw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Ce3jv-RUsAAvZnV.jpg",
        "id_str" : "715478329130790912",
        "id" : 715478329130790912,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ce3jv-RUsAAvZnV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LGBv57eUbw"
      } ],
      "hashtags" : [ {
        "text" : "Tesla",
        "indices" : [ 11, 17 ]
      }, {
        "text" : "Model3",
        "indices" : [ 18, 25 ]
      }, {
        "text" : "Apple",
        "indices" : [ 43, 49 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "715478332695928832",
    "text" : "D\u00FCsseldorf #Tesla #Model3 line next to the #Apple Store ;) https:\/\/t.co\/LGBv57eUbw",
    "id" : 715478332695928832,
    "created_at" : "2016-03-31 09:58:22 +0000",
    "user" : {
      "name" : "Christophe HUBERT",
      "screen_name" : "chris75sf",
      "protected" : false,
      "id_str" : "77786418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/672166226789974016\/Qze3DKTx_normal.jpg",
      "id" : 77786418,
      "verified" : false
    }
  },
  "id" : 715656220825268225,
  "created_at" : "2016-03-31 21:45:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/NwEDR0OBdv",
      "expanded_url" : "https:\/\/twitter.com\/ElishBulGodley\/status\/715652935192035328",
      "display_url" : "twitter.com\/ElishBulGodley\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "715654840148500480",
  "text" : "Another icon gone today.  https:\/\/t.co\/NwEDR0OBdv",
  "id" : 715654840148500480,
  "created_at" : "2016-03-31 21:39:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/715568417940615168\/photo\/1",
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/DRRvv6dxHw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Ce41q-ZXEAAaoxt.jpg",
      "id_str" : "715568403218632704",
      "id" : 715568403218632704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ce41q-ZXEAAaoxt.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DRRvv6dxHw"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715568417940615168",
  "text" : "Walk from D8 to D1. Lovely weather. https:\/\/t.co\/DRRvv6dxHw",
  "id" : 715568417940615168,
  "created_at" : "2016-03-31 15:56:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Derek J Collins   \u9AD8\u5FB7\u5049 \u2618\uFE0F",
      "screen_name" : "dcollinshk",
      "indices" : [ 3, 14 ],
      "id_str" : "2349337794",
      "id" : 2349337794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/aungI61Y1z",
      "expanded_url" : "http:\/\/youtu.be\/kAG39jKi0lI",
      "display_url" : "youtu.be\/kAG39jKi0lI"
    } ]
  },
  "geo" : { },
  "id_str" : "715506420419051524",
  "text" : "RT @dcollinshk: My Blackberry Is Not Working! - The One Ronnie, Preview - BBC One https:\/\/t.co\/aungI61Y1z",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/aungI61Y1z",
        "expanded_url" : "http:\/\/youtu.be\/kAG39jKi0lI",
        "display_url" : "youtu.be\/kAG39jKi0lI"
      } ]
    },
    "geo" : { },
    "id_str" : "715502650008215552",
    "text" : "My Blackberry Is Not Working! - The One Ronnie, Preview - BBC One https:\/\/t.co\/aungI61Y1z",
    "id" : 715502650008215552,
    "created_at" : "2016-03-31 11:34:59 +0000",
    "user" : {
      "name" : "Derek J Collins   \u9AD8\u5FB7\u5049 \u2618\uFE0F",
      "screen_name" : "dcollinshk",
      "protected" : false,
      "id_str" : "2349337794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/435589332288544769\/Ag84PVDR_normal.png",
      "id" : 2349337794,
      "verified" : false
    }
  },
  "id" : 715506420419051524,
  "created_at" : "2016-03-31 11:49:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715501015290814464",
  "text" : "The last Ronnie passed away. RIP",
  "id" : 715501015290814464,
  "created_at" : "2016-03-31 11:28:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "thinkery",
      "screen_name" : "thinkeryme",
      "indices" : [ 0, 11 ],
      "id_str" : "228677991",
      "id" : 228677991
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715494018319147012",
  "in_reply_to_user_id" : 228677991,
  "text" : "@thinkeryme is brilliant. When I saved a PDF from the web, it reproduce the content in text.",
  "id" : 715494018319147012,
  "created_at" : "2016-03-31 11:00:41 +0000",
  "in_reply_to_screen_name" : "thinkeryme",
  "in_reply_to_user_id_str" : "228677991",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715489333700993024",
  "text" : "RT @kashmi_pay: Singapore, China &amp; India among the fastest growing countries in Asia Pacific to adopt P2P Payment System. Digital w\u2026 https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/aRVLBqkMRj",
        "expanded_url" : "http:\/\/buff.ly\/1Y1u4Y1",
        "display_url" : "buff.ly\/1Y1u4Y1"
      } ]
    },
    "geo" : { },
    "id_str" : "715486481788248064",
    "text" : "Singapore, China &amp; India among the fastest growing countries in Asia Pacific to adopt P2P Payment System. Digital w\u2026 https:\/\/t.co\/aRVLBqkMRj",
    "id" : 715486481788248064,
    "created_at" : "2016-03-31 10:30:45 +0000",
    "user" : {
      "name" : "Kashmi",
      "screen_name" : "KashmiNow",
      "protected" : false,
      "id_str" : "2605209300",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/700534871676948480\/VrAdoTav_normal.jpg",
      "id" : 2605209300,
      "verified" : false
    }
  },
  "id" : 715489333700993024,
  "created_at" : "2016-03-31 10:42:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anna Ossowski",
      "screen_name" : "OssAnna16",
      "indices" : [ 3, 13 ],
      "id_str" : "1049495413",
      "id" : 1049495413
    }, {
      "name" : "codingmama.labs",
      "screen_name" : "erikaheidi",
      "indices" : [ 89, 100 ],
      "id_str" : "19625601",
      "id" : 19625601
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "djangocon",
      "indices" : [ 101, 111 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715472143497109505",
  "text" : "RT @OssAnna16: \"There's no great open source project without a community to support it.\" @erikaheidi #djangocon",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "codingmama.labs",
        "screen_name" : "erikaheidi",
        "indices" : [ 74, 85 ],
        "id_str" : "19625601",
        "id" : 19625601
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "djangocon",
        "indices" : [ 86, 96 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "715457096389632000",
    "text" : "\"There's no great open source project without a community to support it.\" @erikaheidi #djangocon",
    "id" : 715457096389632000,
    "created_at" : "2016-03-31 08:33:58 +0000",
    "user" : {
      "name" : "Anna Ossowski",
      "screen_name" : "OssAnna16",
      "protected" : false,
      "id_str" : "1049495413",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1022994979214503936\/0zfIr_vw_normal.jpg",
      "id" : 1049495413,
      "verified" : false
    }
  },
  "id" : 715472143497109505,
  "created_at" : "2016-03-31 09:33:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "715470873432498176",
  "geo" : { },
  "id_str" : "715471716395290625",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold yeah well insulated. Also put on extra layer of clothing.",
  "id" : 715471716395290625,
  "in_reply_to_status_id" : 715470873432498176,
  "created_at" : "2016-03-31 09:32:04 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/jBwX8frBBJ",
      "expanded_url" : "http:\/\/www.realclear.com\/inspiration\/2016\/03\/20\/mom_buys_homeless_man_breakfast_then_he_slips_her_a_note_with_a_shocking_confession_13056.html",
      "display_url" : "realclear.com\/inspiration\/20\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "715471232980754432",
  "text" : "RT @yapphenghui: Mom Buys Homeless Man Breakfast, Then He Slips Her A Note With A Shocking Confession https:\/\/t.co\/jBwX8frBBJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/jBwX8frBBJ",
        "expanded_url" : "http:\/\/www.realclear.com\/inspiration\/2016\/03\/20\/mom_buys_homeless_man_breakfast_then_he_slips_her_a_note_with_a_shocking_confession_13056.html",
        "display_url" : "realclear.com\/inspiration\/20\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "715373483975319553",
    "text" : "Mom Buys Homeless Man Breakfast, Then He Slips Her A Note With A Shocking Confession https:\/\/t.co\/jBwX8frBBJ",
    "id" : 715373483975319553,
    "created_at" : "2016-03-31 03:01:44 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 715471232980754432,
  "created_at" : "2016-03-31 09:30:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "715456587687673857",
  "geo" : { },
  "id_str" : "715469942343094273",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold I made it w\/o turning on the heater.",
  "id" : 715469942343094273,
  "in_reply_to_status_id" : 715456587687673857,
  "created_at" : "2016-03-31 09:25:01 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/HvLThdIL4Q",
      "expanded_url" : "https:\/\/sense.io\/",
      "display_url" : "sense.io"
    } ]
  },
  "geo" : { },
  "id_str" : "715323891288260608",
  "text" : "Any alternative for https:\/\/t.co\/HvLThdIL4Q as it being recently taken over by Cloudera?",
  "id" : 715323891288260608,
  "created_at" : "2016-03-30 23:44:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Google Docs",
      "screen_name" : "googledocs",
      "indices" : [ 3, 14 ],
      "id_str" : "488683847",
      "id" : 488683847
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gHelp",
      "indices" : [ 108, 114 ]
    }, {
      "text" : "TuesdayTip",
      "indices" : [ 115, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715269817373163520",
  "text" : "RT @googledocs: Conditional formatting is a fancy way of saying \"use colors to help me understand my data.\" #gHelp #TuesdayTip https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/googledocs\/status\/714840596779823104\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/BiwbtYxmbB",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CeufXBWWwAAtcUc.jpg",
        "id_str" : "714840183716364288",
        "id" : 714840183716364288,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CeufXBWWwAAtcUc.jpg",
        "sizes" : [ {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BiwbtYxmbB"
      } ],
      "hashtags" : [ {
        "text" : "gHelp",
        "indices" : [ 92, 98 ]
      }, {
        "text" : "TuesdayTip",
        "indices" : [ 99, 110 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714840596779823104",
    "text" : "Conditional formatting is a fancy way of saying \"use colors to help me understand my data.\" #gHelp #TuesdayTip https:\/\/t.co\/BiwbtYxmbB",
    "id" : 714840596779823104,
    "created_at" : "2016-03-29 15:44:14 +0000",
    "user" : {
      "name" : "Google Docs",
      "screen_name" : "googledocs",
      "protected" : false,
      "id_str" : "488683847",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/940690364410748929\/erFsVrcc_normal.jpg",
      "id" : 488683847,
      "verified" : true
    }
  },
  "id" : 715269817373163520,
  "created_at" : "2016-03-30 20:09:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alyssa Fu Ward",
      "screen_name" : "datasciencefu",
      "indices" : [ 3, 17 ],
      "id_str" : "2995044205",
      "id" : 2995044205
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/datasciencefu\/status\/715226793276379137\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/Iq2gKW7oDc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cez--HgWwAAxomR.jpg",
      "id_str" : "715226783965167616",
      "id" : 715226783965167616,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cez--HgWwAAxomR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Iq2gKW7oDc"
    } ],
    "hashtags" : [ {
      "text" : "StrataHadoop",
      "indices" : [ 61, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715255315579666432",
  "text" : "RT @datasciencefu: Love this representation of the long tail #StrataHadoop https:\/\/t.co\/Iq2gKW7oDc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/datasciencefu\/status\/715226793276379137\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/Iq2gKW7oDc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cez--HgWwAAxomR.jpg",
        "id_str" : "715226783965167616",
        "id" : 715226783965167616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cez--HgWwAAxomR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Iq2gKW7oDc"
      } ],
      "hashtags" : [ {
        "text" : "StrataHadoop",
        "indices" : [ 42, 55 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "715226793276379137",
    "text" : "Love this representation of the long tail #StrataHadoop https:\/\/t.co\/Iq2gKW7oDc",
    "id" : 715226793276379137,
    "created_at" : "2016-03-30 17:18:50 +0000",
    "user" : {
      "name" : "Alyssa Fu Ward",
      "screen_name" : "datasciencefu",
      "protected" : false,
      "id_str" : "2995044205",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/604435365307424769\/4r5dzsae_normal.jpg",
      "id" : 2995044205,
      "verified" : false
    }
  },
  "id" : 715255315579666432,
  "created_at" : "2016-03-30 19:12:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sharon Lin",
      "screen_name" : "sharonxlin",
      "indices" : [ 3, 14 ],
      "id_str" : "22606642",
      "id" : 22606642
    }, {
      "name" : "Airbnb",
      "screen_name" : "Airbnb",
      "indices" : [ 33, 40 ],
      "id_str" : "17416571",
      "id" : 17416571
    }, {
      "name" : "O'Reilly Strata",
      "screen_name" : "strataconf",
      "indices" : [ 64, 75 ],
      "id_str" : "167169119",
      "id" : 167169119
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/sharonxlin\/status\/715244267703132161\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/z7XBmEC42H",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Ce0O3jgVAAApd-u.jpg",
      "id_str" : "715244263408205824",
      "id" : 715244263408205824,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ce0O3jgVAAApd-u.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/z7XBmEC42H"
    } ],
    "hashtags" : [ {
      "text" : "Panoramix",
      "indices" : [ 41, 51 ]
    }, {
      "text" : "Caravel",
      "indices" : [ 54, 62 ]
    }, {
      "text" : "StrataHadoop",
      "indices" : [ 76, 89 ]
    }, {
      "text" : "dataviz",
      "indices" : [ 90, 98 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715245736666001409",
  "text" : "RT @sharonxlin: The stack behind @Airbnb #Panoramix \/ #Caravel.\n@strataconf #StrataHadoop #dataviz https:\/\/t.co\/z7XBmEC42H",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Airbnb",
        "screen_name" : "Airbnb",
        "indices" : [ 17, 24 ],
        "id_str" : "17416571",
        "id" : 17416571
      }, {
        "name" : "O'Reilly Strata",
        "screen_name" : "strataconf",
        "indices" : [ 48, 59 ],
        "id_str" : "167169119",
        "id" : 167169119
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sharonxlin\/status\/715244267703132161\/photo\/1",
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/z7XBmEC42H",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Ce0O3jgVAAApd-u.jpg",
        "id_str" : "715244263408205824",
        "id" : 715244263408205824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ce0O3jgVAAApd-u.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/z7XBmEC42H"
      } ],
      "hashtags" : [ {
        "text" : "Panoramix",
        "indices" : [ 25, 35 ]
      }, {
        "text" : "Caravel",
        "indices" : [ 38, 46 ]
      }, {
        "text" : "StrataHadoop",
        "indices" : [ 60, 73 ]
      }, {
        "text" : "dataviz",
        "indices" : [ 74, 82 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "715244267703132161",
    "text" : "The stack behind @Airbnb #Panoramix \/ #Caravel.\n@strataconf #StrataHadoop #dataviz https:\/\/t.co\/z7XBmEC42H",
    "id" : 715244267703132161,
    "created_at" : "2016-03-30 18:28:16 +0000",
    "user" : {
      "name" : "Sharon Lin",
      "screen_name" : "sharonxlin",
      "protected" : false,
      "id_str" : "22606642",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/489470676697219072\/ExyfvXfN_normal.jpeg",
      "id" : 22606642,
      "verified" : false
    }
  },
  "id" : 715245736666001409,
  "created_at" : "2016-03-30 18:34:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/DgTzRGYuAN",
      "expanded_url" : "http:\/\/foreignpolicy.com\/2016\/03\/30\/a-lost-daughter-speaks-and-all-of-china-listens-adoption\/",
      "display_url" : "foreignpolicy.com\/2016\/03\/30\/a-l\u2026"
    }, {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/UVFmhu6WZs",
      "expanded_url" : "https:\/\/twitter.com\/BethanyAllenEbr\/status\/715239463279063040",
      "display_url" : "twitter.com\/BethanyAllenEb\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "715242816868376577",
  "text" : "\"I went to China to find the birth mother who left me on a street corner...\"  https:\/\/t.co\/DgTzRGYuAN https:\/\/t.co\/UVFmhu6WZs",
  "id" : 715242816868376577,
  "created_at" : "2016-03-30 18:22:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Informatica",
      "screen_name" : "Informatica",
      "indices" : [ 3, 15 ],
      "id_str" : "17350542",
      "id" : 17350542
    }, {
      "name" : "Nara Logics",
      "screen_name" : "NaraLogics",
      "indices" : [ 82, 93 ],
      "id_str" : "2872379039",
      "id" : 2872379039
    }, {
      "name" : "Jana Eggers",
      "screen_name" : "jeggers",
      "indices" : [ 98, 106 ],
      "id_str" : "6792792",
      "id" : 6792792
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StrataHadoop",
      "indices" : [ 107, 120 ]
    }, {
      "text" : "AI",
      "indices" : [ 121, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715241576235200513",
  "text" : "RT @Informatica: \"Data is the 'Key to the Kingdom' of Artificial Intelligence.\" - @NaraLogics CEO @Jeggers #StrataHadoop #AI https:\/\/t.co\/Q\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Nara Logics",
        "screen_name" : "NaraLogics",
        "indices" : [ 65, 76 ],
        "id_str" : "2872379039",
        "id" : 2872379039
      }, {
        "name" : "Jana Eggers",
        "screen_name" : "jeggers",
        "indices" : [ 81, 89 ],
        "id_str" : "6792792",
        "id" : 6792792
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Informatica\/status\/715222648754561025\/photo\/1",
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/QqruqekxhI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cez7NWnXEAAdDzN.jpg",
        "id_str" : "715222647672606720",
        "id" : 715222647672606720,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cez7NWnXEAAdDzN.jpg",
        "sizes" : [ {
          "h" : 952,
          "resize" : "fit",
          "w" : 1427
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 952,
          "resize" : "fit",
          "w" : 1427
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QqruqekxhI"
      } ],
      "hashtags" : [ {
        "text" : "StrataHadoop",
        "indices" : [ 90, 103 ]
      }, {
        "text" : "AI",
        "indices" : [ 104, 107 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "715222648754561025",
    "text" : "\"Data is the 'Key to the Kingdom' of Artificial Intelligence.\" - @NaraLogics CEO @Jeggers #StrataHadoop #AI https:\/\/t.co\/QqruqekxhI",
    "id" : 715222648754561025,
    "created_at" : "2016-03-30 17:02:22 +0000",
    "user" : {
      "name" : "Informatica",
      "screen_name" : "Informatica",
      "protected" : false,
      "id_str" : "17350542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/863223889047937024\/cUoPjCIl_normal.jpg",
      "id" : 17350542,
      "verified" : true
    }
  },
  "id" : 715241576235200513,
  "created_at" : "2016-03-30 18:17:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715240022329466880",
  "text" : "Prior experience really helps. I can relate how a particular stats got to do with A\/B testing.",
  "id" : 715240022329466880,
  "created_at" : "2016-03-30 18:11:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Open Minds",
      "screen_name" : "MindsOpened",
      "indices" : [ 3, 15 ],
      "id_str" : "1870056282",
      "id" : 1870056282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715187139940626432",
  "text" : "RT @MindsOpened: You flush cleaner water than what some people strive to drink to survive everyday.. Just something to think about..",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.kucukbeyin.com\/\" rel=\"nofollow\"\u003Efahdjfasdfajsdlkfjalsj\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "554065164656717824",
    "text" : "You flush cleaner water than what some people strive to drink to survive everyday.. Just something to think about..",
    "id" : 554065164656717824,
    "created_at" : "2015-01-11 00:00:03 +0000",
    "user" : {
      "name" : "Open Minds",
      "screen_name" : "MindsOpened",
      "protected" : false,
      "id_str" : "1870056282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/473116416615280641\/_NLFmh-w_normal.jpeg",
      "id" : 1870056282,
      "verified" : false
    }
  },
  "id" : 715187139940626432,
  "created_at" : "2016-03-30 14:41:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/9QqdL6g5AQ",
      "expanded_url" : "https:\/\/workflowy.com\/s\/ddXASzTHSG#",
      "display_url" : "workflowy.com\/s\/ddXASzTHSG#"
    } ]
  },
  "geo" : { },
  "id_str" : "715179656673005569",
  "text" : "I am curating a list of data source. Let me know if I miss out any. https:\/\/t.co\/9QqdL6g5AQ Thanks",
  "id" : 715179656673005569,
  "created_at" : "2016-03-30 14:11:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "indices" : [ 3, 11 ],
      "id_str" : "20697268",
      "id" : 20697268
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 13, 19 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715175214997495809",
  "text" : "RT @KJoanne: @mryap No. I keep mine very clean as I have a dust mite allergy.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "715174602553565184",
    "geo" : { },
    "id_str" : "715174907550769153",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap No. I keep mine very clean as I have a dust mite allergy.",
    "id" : 715174907550769153,
    "in_reply_to_status_id" : 715174602553565184,
    "created_at" : "2016-03-30 13:52:39 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "protected" : false,
      "id_str" : "20697268",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005549984522043392\/YDQyk6cL_normal.jpg",
      "id" : 20697268,
      "verified" : false
    }
  },
  "id" : 715175214997495809,
  "created_at" : "2016-03-30 13:53:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "715174602553565184",
  "text" : "Do you have any skeletons in your closet? Reply and I will retweet.",
  "id" : 715174602553565184,
  "created_at" : "2016-03-30 13:51:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/6kBgmcB6Rw",
      "expanded_url" : "https:\/\/twitter.com\/ailieirv\/status\/715088562803310592",
      "display_url" : "twitter.com\/ailieirv\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "715111086543003648",
  "text" : "I can relate to this.  https:\/\/t.co\/6kBgmcB6Rw",
  "id" : 715111086543003648,
  "created_at" : "2016-03-30 09:39:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vanessa Tan",
      "screen_name" : "VanessaTanLB",
      "indices" : [ 3, 16 ],
      "id_str" : "20676765",
      "id" : 20676765
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/ikkl9ddzG2",
      "expanded_url" : "http:\/\/ift.tt\/1UBxxyW",
      "display_url" : "ift.tt\/1UBxxyW"
    } ]
  },
  "geo" : { },
  "id_str" : "715081421476462592",
  "text" : "RT @VanessaTanLB: Spotify is now in Indonesia. It\u2019s cheap and even accepts cash https:\/\/t.co\/ikkl9ddzG2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/ikkl9ddzG2",
        "expanded_url" : "http:\/\/ift.tt\/1UBxxyW",
        "display_url" : "ift.tt\/1UBxxyW"
      } ]
    },
    "geo" : { },
    "id_str" : "715069206522437632",
    "text" : "Spotify is now in Indonesia. It\u2019s cheap and even accepts cash https:\/\/t.co\/ikkl9ddzG2",
    "id" : 715069206522437632,
    "created_at" : "2016-03-30 06:52:38 +0000",
    "user" : {
      "name" : "Vanessa Tan",
      "screen_name" : "VanessaTanLB",
      "protected" : false,
      "id_str" : "20676765",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/699067418828615680\/tj1WKs3w_normal.jpg",
      "id" : 20676765,
      "verified" : false
    }
  },
  "id" : 715081421476462592,
  "created_at" : "2016-03-30 07:41:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laura",
      "screen_name" : "HowToGYST",
      "indices" : [ 0, 10 ],
      "id_str" : "2873931282",
      "id" : 2873931282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/G6x7DGHE2P",
      "expanded_url" : "https:\/\/workflowy.com\/invite\/376d05f5.twx",
      "display_url" : "workflowy.com\/invite\/376d05f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714961567205470208",
  "in_reply_to_user_id" : 2873931282,
  "text" : "@HowToGYST Do checkout https:\/\/t.co\/G6x7DGHE2P",
  "id" : 714961567205470208,
  "created_at" : "2016-03-29 23:44:55 +0000",
  "in_reply_to_screen_name" : "HowToGYST",
  "in_reply_to_user_id_str" : "2873931282",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714948610652286976",
  "text" : "I need to keep a kind of personal logbook, diary, notebook to help me remember what I had done and why I had done it that way.",
  "id" : 714948610652286976,
  "created_at" : "2016-03-29 22:53:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Elsom",
      "screen_name" : "JasonElsom",
      "indices" : [ 3, 14 ],
      "id_str" : "33552794",
      "id" : 33552794
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/JasonElsom\/status\/714918443720568833\/photo\/1",
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/UfdeJm8Ghp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CevmiQuWAAEHbls.jpg",
      "id_str" : "714918442147643393",
      "id" : 714918442147643393,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CevmiQuWAAEHbls.jpg",
      "sizes" : [ {
        "h" : 627,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 592,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 627,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 627,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UfdeJm8Ghp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714928822605504512",
  "text" : "RT @JasonElsom: Deep... https:\/\/t.co\/UfdeJm8Ghp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JasonElsom\/status\/714918443720568833\/photo\/1",
        "indices" : [ 8, 31 ],
        "url" : "https:\/\/t.co\/UfdeJm8Ghp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CevmiQuWAAEHbls.jpg",
        "id_str" : "714918442147643393",
        "id" : 714918442147643393,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CevmiQuWAAEHbls.jpg",
        "sizes" : [ {
          "h" : 627,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 627,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 627,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UfdeJm8Ghp"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714918443720568833",
    "text" : "Deep... https:\/\/t.co\/UfdeJm8Ghp",
    "id" : 714918443720568833,
    "created_at" : "2016-03-29 20:53:34 +0000",
    "user" : {
      "name" : "Jason Elsom",
      "screen_name" : "JasonElsom",
      "protected" : false,
      "id_str" : "33552794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930570946284605441\/pUTEgAby_normal.jpg",
      "id" : 33552794,
      "verified" : false
    }
  },
  "id" : 714928822605504512,
  "created_at" : "2016-03-29 21:34:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eco Galway Girl",
      "screen_name" : "EcoGalwayGal",
      "indices" : [ 3, 16 ],
      "id_str" : "703338042803748864",
      "id" : 703338042803748864
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "EatForThePlanet",
      "indices" : [ 122, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/dFsusdseB4",
      "expanded_url" : "http:\/\/greennews.ie\/a-growing-number-of-irish-people-are-shunning-meat-and-not-just-for-good-friday\/",
      "display_url" : "greennews.ie\/a-growing-numb\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714910909160939520",
  "text" : "RT @EcoGalwayGal: A growing number of Irish people are shunning meat and not just for Good Friday https:\/\/t.co\/dFsusdseB4 #EatForThePlanet \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "EatForThePlanet",
        "indices" : [ 104, 120 ]
      }, {
        "text" : "vegetarian",
        "indices" : [ 121, 132 ]
      }, {
        "text" : "vegan",
        "indices" : [ 133, 139 ]
      } ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/dFsusdseB4",
        "expanded_url" : "http:\/\/greennews.ie\/a-growing-number-of-irish-people-are-shunning-meat-and-not-just-for-good-friday\/",
        "display_url" : "greennews.ie\/a-growing-numb\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712938586048503809",
    "text" : "A growing number of Irish people are shunning meat and not just for Good Friday https:\/\/t.co\/dFsusdseB4 #EatForThePlanet #vegetarian #vegan",
    "id" : 712938586048503809,
    "created_at" : "2016-03-24 09:46:19 +0000",
    "user" : {
      "name" : "Eco Galway Girl",
      "screen_name" : "EcoGalwayGal",
      "protected" : false,
      "id_str" : "703338042803748864",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/704433705666875399\/44ZJWrbO_normal.jpg",
      "id" : 703338042803748864,
      "verified" : false
    }
  },
  "id" : 714910909160939520,
  "created_at" : "2016-03-29 20:23:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "chris horn",
      "screen_name" : "chrisjhorn",
      "indices" : [ 3, 14 ],
      "id_str" : "24028945",
      "id" : 24028945
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714908302593892352",
  "text" : "RT @chrisjhorn: I\u2019ve always wondered about the US \u201CAdopt a Highway\u201D notices.  Heres an article on the economics of the schemes.. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/SAADoqRd1H",
        "expanded_url" : "http:\/\/priceonomics.com\/the-business-of-highway-adoption\/",
        "display_url" : "priceonomics.com\/the-business-o\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "714883648982540289",
    "text" : "I\u2019ve always wondered about the US \u201CAdopt a Highway\u201D notices.  Heres an article on the economics of the schemes.. https:\/\/t.co\/SAADoqRd1H",
    "id" : 714883648982540289,
    "created_at" : "2016-03-29 18:35:18 +0000",
    "user" : {
      "name" : "chris horn",
      "screen_name" : "chrisjhorn",
      "protected" : false,
      "id_str" : "24028945",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/765951619175424000\/50XyH-AD_normal.jpg",
      "id" : 24028945,
      "verified" : false
    }
  },
  "id" : 714908302593892352,
  "created_at" : "2016-03-29 20:13:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 66, 77 ],
      "id_str" : "816653",
      "id" : 816653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/1W0b5106O5",
      "expanded_url" : "http:\/\/tcrn.ch\/1WyI631",
      "display_url" : "tcrn.ch\/1WyI631"
    } ]
  },
  "geo" : { },
  "id_str" : "714846267990491136",
  "text" : "Algorithmic feeds force us to\u00A0compete https:\/\/t.co\/1W0b5106O5 via @techcrunch &lt;-  Algorithmic is define by us.",
  "id" : 714846267990491136,
  "created_at" : "2016-03-29 16:06:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gita",
      "screen_name" : "gita_m",
      "indices" : [ 0, 7 ],
      "id_str" : "16012316",
      "id" : 16012316
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "714803343152037889",
  "geo" : { },
  "id_str" : "714823987512152064",
  "in_reply_to_user_id" : 16012316,
  "text" : "@gita_m sign in sounds friendly. Login or Log In sounds u dealing a very technical entity.",
  "id" : 714823987512152064,
  "in_reply_to_status_id" : 714803343152037889,
  "created_at" : "2016-03-29 14:38:14 +0000",
  "in_reply_to_screen_name" : "gita_m",
  "in_reply_to_user_id_str" : "16012316",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714822505232527361",
  "text" : "RT @klillington: Can we just ban the ridiculous phrase \u2018reached out\u2019 &amp; use \u2018asked\u2019 or \u2018contacted\u2019 (unless you\u2019re actually talking arms, or \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714804635391893504",
    "text" : "Can we just ban the ridiculous phrase \u2018reached out\u2019 &amp; use \u2018asked\u2019 or \u2018contacted\u2019 (unless you\u2019re actually talking arms, or an emotive appeal)",
    "id" : 714804635391893504,
    "created_at" : "2016-03-29 13:21:20 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 714822505232527361,
  "created_at" : "2016-03-29 14:32:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Pulch",
      "screen_name" : "MichaelPulchEU",
      "indices" : [ 3, 18 ],
      "id_str" : "2852724951",
      "id" : 2852724951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714757721275412480",
  "text" : "RT @MichaelPulchEU: Amazing stat from WEF: 9 out of 10 most globalised countries in  the EU - S'pore only other on 4th place https:\/\/t.co\/T\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/TWYBRhorlU",
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/714723852169908224",
        "display_url" : "twitter.com\/wef\/status\/714\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "714744811572621312",
    "text" : "Amazing stat from WEF: 9 out of 10 most globalised countries in  the EU - S'pore only other on 4th place https:\/\/t.co\/TWYBRhorlU",
    "id" : 714744811572621312,
    "created_at" : "2016-03-29 09:23:37 +0000",
    "user" : {
      "name" : "Michael Pulch",
      "screen_name" : "MichaelPulchEU",
      "protected" : false,
      "id_str" : "2852724951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966510151942389761\/OeWG1oUG_normal.jpg",
      "id" : 2852724951,
      "verified" : true
    }
  },
  "id" : 714757721275412480,
  "created_at" : "2016-03-29 10:14:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/PN41L4UaM5",
      "expanded_url" : "http:\/\/rank-checker.online",
      "display_url" : "rank-checker.online"
    } ]
  },
  "geo" : { },
  "id_str" : "714727565206949889",
  "text" : "New Referrer spam spotted in my client's Google Analytics account  - https:\/\/t.co\/PN41L4UaM5. Update your spam filter now.",
  "id" : 714727565206949889,
  "created_at" : "2016-03-29 08:15:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/BKTQRGmGEa",
      "expanded_url" : "http:\/\/spectrum.ieee.org\/computing\/software\/the-2015-top-ten-programming-languages",
      "display_url" : "spectrum.ieee.org\/computing\/soft\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714596474747797504",
  "text" : "R is becoming the lingua franca of Data Science. IEEE listed R in the top ten languages of 2015. https:\/\/t.co\/BKTQRGmGEa",
  "id" : 714596474747797504,
  "created_at" : "2016-03-28 23:34:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714595811754188800",
  "text" : "Application of Machine learning : predict whether user will be a paid customer by analysing the user activities during 1st week\/1st month.",
  "id" : 714595811754188800,
  "created_at" : "2016-03-28 23:31:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anatoly Shashkin\uD83D\uDCBE",
      "screen_name" : "dosnostalgic",
      "indices" : [ 3, 16 ],
      "id_str" : "34132596",
      "id" : 34132596
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/dosnostalgic\/status\/707649581560012800\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/z1ZykY99t6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdITi1RXIAEJKy8.jpg",
      "id_str" : "707649580586967041",
      "id" : 707649580586967041,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdITi1RXIAEJKy8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 378,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/z1ZykY99t6"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714594203259564032",
  "text" : "RT @dosnostalgic: RT if you remember https:\/\/t.co\/z1ZykY99t6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dosnostalgic\/status\/707649581560012800\/photo\/1",
        "indices" : [ 19, 42 ],
        "url" : "https:\/\/t.co\/z1ZykY99t6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdITi1RXIAEJKy8.jpg",
        "id_str" : "707649580586967041",
        "id" : 707649580586967041,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdITi1RXIAEJKy8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 378,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/z1ZykY99t6"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707649581560012800",
    "text" : "RT if you remember https:\/\/t.co\/z1ZykY99t6",
    "id" : 707649581560012800,
    "created_at" : "2016-03-09 19:29:42 +0000",
    "user" : {
      "name" : "Anatoly Shashkin\uD83D\uDCBE",
      "screen_name" : "dosnostalgic",
      "protected" : false,
      "id_str" : "34132596",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/925818964055175168\/CZYce9KA_normal.jpg",
      "id" : 34132596,
      "verified" : false
    }
  },
  "id" : 714594203259564032,
  "created_at" : "2016-03-28 23:25:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "9 Dash Line \u4E5D\u6BB5\u7EBF",
      "screen_name" : "9DashLine",
      "indices" : [ 3, 13 ],
      "id_str" : "2847258009",
      "id" : 2847258009
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/0ETE9spzIX",
      "expanded_url" : "http:\/\/bit.ly\/25qI2Za",
      "display_url" : "bit.ly\/25qI2Za"
    } ]
  },
  "geo" : { },
  "id_str" : "714584215606706176",
  "text" : "RT @9DashLine: US Navy captain jailed for massive bribery scandal https:\/\/t.co\/0ETE9spzIX Gave Malaysian defence contractor classified info\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 51, 74 ],
        "url" : "https:\/\/t.co\/0ETE9spzIX",
        "expanded_url" : "http:\/\/bit.ly\/25qI2Za",
        "display_url" : "bit.ly\/25qI2Za"
      } ]
    },
    "geo" : { },
    "id_str" : "714582333840613376",
    "text" : "US Navy captain jailed for massive bribery scandal https:\/\/t.co\/0ETE9spzIX Gave Malaysian defence contractor classified info for prostitutes",
    "id" : 714582333840613376,
    "created_at" : "2016-03-28 22:37:59 +0000",
    "user" : {
      "name" : "9 Dash Line \u4E5D\u6BB5\u7EBF",
      "screen_name" : "9DashLine",
      "protected" : false,
      "id_str" : "2847258009",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/545724611950678018\/rwdvgMdk_normal.jpeg",
      "id" : 2847258009,
      "verified" : false
    }
  },
  "id" : 714584215606706176,
  "created_at" : "2016-03-28 22:45:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714583687027892224",
  "text" : "The documentary ends with the idea that the 1916 uprising since 1765 inspires other nations from India to Africa seeking their independence.",
  "id" : 714583687027892224,
  "created_at" : "2016-03-28 22:43:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bbc4",
      "indices" : [ 54, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714581716598435840",
  "text" : "The Irish 1916 documentary narrated by Liam Neeson on #bbc4 is brilliant.",
  "id" : 714581716598435840,
  "created_at" : "2016-03-28 22:35:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex.Kane",
      "screen_name" : "AlexKane221b",
      "indices" : [ 3, 16 ],
      "id_str" : "624112946",
      "id" : 624112946
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AlexKane221b\/status\/714410239404220417\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/ipD0LcPF0Y",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeoYUBrWIAEusl4.jpg",
      "id_str" : "714410223218335745",
      "id" : 714410223218335745,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeoYUBrWIAEusl4.jpg",
      "sizes" : [ {
        "h" : 436,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 656,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 656,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 656,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ipD0LcPF0Y"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714559404054593540",
  "text" : "RT @AlexKane221b: This is on tonight (Mon) on BBC4: Sky116\/Virgin107\/Freeview9 https:\/\/t.co\/ipD0LcPF0Y",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AlexKane221b\/status\/714410239404220417\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/ipD0LcPF0Y",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeoYUBrWIAEusl4.jpg",
        "id_str" : "714410223218335745",
        "id" : 714410223218335745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeoYUBrWIAEusl4.jpg",
        "sizes" : [ {
          "h" : 436,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ipD0LcPF0Y"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714410239404220417",
    "text" : "This is on tonight (Mon) on BBC4: Sky116\/Virgin107\/Freeview9 https:\/\/t.co\/ipD0LcPF0Y",
    "id" : 714410239404220417,
    "created_at" : "2016-03-28 11:14:08 +0000",
    "user" : {
      "name" : "Alex.Kane",
      "screen_name" : "AlexKane221b",
      "protected" : false,
      "id_str" : "624112946",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023141649910784000\/Ybjl-3rc_normal.jpg",
      "id" : 624112946,
      "verified" : false
    }
  },
  "id" : 714559404054593540,
  "created_at" : "2016-03-28 21:06:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Visit Wexford",
      "screen_name" : "visitwexford",
      "indices" : [ 3, 16 ],
      "id_str" : "189160801",
      "id" : 189160801
    }, {
      "name" : "Hook Lighthouse",
      "screen_name" : "hooklighthouse",
      "indices" : [ 76, 91 ],
      "id_str" : "97414866",
      "id" : 97414866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Wexford",
      "indices" : [ 92, 100 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 101, 109 ]
    }, {
      "text" : "irelandsancienteast",
      "indices" : [ 111, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714551050011852800",
  "text" : "RT @visitwexford: What a stunning image captured by 'Thru my eyes by Jamie' @hooklighthouse #Wexford #Ireland  #irelandsancienteast https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hook Lighthouse",
        "screen_name" : "hooklighthouse",
        "indices" : [ 58, 73 ],
        "id_str" : "97414866",
        "id" : 97414866
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/visitwexford\/status\/714527945121882112\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/hszFCWGDA0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeqDX4wXEAA5qjG.jpg",
        "id_str" : "714527937287098368",
        "id" : 714527937287098368,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeqDX4wXEAA5qjG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 582,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 876,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 876,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 876,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hszFCWGDA0"
      } ],
      "hashtags" : [ {
        "text" : "Wexford",
        "indices" : [ 74, 82 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 83, 91 ]
      }, {
        "text" : "irelandsancienteast",
        "indices" : [ 93, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714527945121882112",
    "text" : "What a stunning image captured by 'Thru my eyes by Jamie' @hooklighthouse #Wexford #Ireland  #irelandsancienteast https:\/\/t.co\/hszFCWGDA0",
    "id" : 714527945121882112,
    "created_at" : "2016-03-28 19:01:52 +0000",
    "user" : {
      "name" : "Visit Wexford",
      "screen_name" : "visitwexford",
      "protected" : false,
      "id_str" : "189160801",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784399463733399552\/NPF9ndmB_normal.jpg",
      "id" : 189160801,
      "verified" : false
    }
  },
  "id" : 714551050011852800,
  "created_at" : "2016-03-28 20:33:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 34 ],
      "url" : "https:\/\/t.co\/mEqRq5jFVI",
      "expanded_url" : "http:\/\/www.pindex.com\/",
      "display_url" : "pindex.com"
    } ]
  },
  "geo" : { },
  "id_str" : "714439369780817920",
  "text" : "Seems that https:\/\/t.co\/mEqRq5jFVI does not have the option for private board",
  "id" : 714439369780817920,
  "created_at" : "2016-03-28 13:09:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714380211895328768",
  "text" : "RT @rshotton: A great, free LSE lecture soon: Nisbett, who Gladwell said influenced him more than any other psychologist https:\/\/t.co\/EOWH4\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/EOWH4qSTiS",
        "expanded_url" : "http:\/\/www.lse.ac.uk\/publicEvents\/events\/2016\/04\/20160412t1830vOT.aspx",
        "display_url" : "lse.ac.uk\/publicEvents\/e\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "713071307022274564",
    "text" : "A great, free LSE lecture soon: Nisbett, who Gladwell said influenced him more than any other psychologist https:\/\/t.co\/EOWH4qSTiS",
    "id" : 713071307022274564,
    "created_at" : "2016-03-24 18:33:42 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 714380211895328768,
  "created_at" : "2016-03-28 09:14:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/5J5R5bbgie",
      "expanded_url" : "http:\/\/econ.st\/1R97gBN",
      "display_url" : "econ.st\/1R97gBN"
    } ]
  },
  "geo" : { },
  "id_str" : "714313399874752512",
  "text" : "It possible to target voters individually, thanks to data,cheaper computing power &amp; better methods to mine them. https:\/\/t.co\/5J5R5bbgie",
  "id" : 714313399874752512,
  "created_at" : "2016-03-28 04:49:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714306938549444610",
  "text" : "waking up early so to get more things done like wasting time on social media....",
  "id" : 714306938549444610,
  "created_at" : "2016-03-28 04:23:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714258914364289024",
  "text" : "RT @SimonPRepublic: Today also remember all the British kids that were forced into the British Army during WW1 &amp; died on the streets of Dub\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714076109562728448",
    "text" : "Today also remember all the British kids that were forced into the British Army during WW1 &amp; died on the streets of Dublin during the Rising",
    "id" : 714076109562728448,
    "created_at" : "2016-03-27 13:06:26 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 714258914364289024,
  "created_at" : "2016-03-28 01:12:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    }, {
      "name" : "BrilliantAds.com",
      "screen_name" : "brilliantads",
      "indices" : [ 54, 67 ],
      "id_str" : "2252098994",
      "id" : 2252098994
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/714065175368888320\/photo\/1",
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/5ujFwXruAp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CejefgpWEAQUpVc.jpg",
      "id_str" : "714065173858881540",
      "id" : 714065173858881540,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CejefgpWEAQUpVc.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 514
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5ujFwXruAp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714255414225997824",
  "text" : "RT @rshotton: Smart, contextual organ donation ad via @brilliantads https:\/\/t.co\/5ujFwXruAp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BrilliantAds.com",
        "screen_name" : "brilliantads",
        "indices" : [ 40, 53 ],
        "id_str" : "2252098994",
        "id" : 2252098994
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/714065175368888320\/photo\/1",
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/5ujFwXruAp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CejefgpWEAQUpVc.jpg",
        "id_str" : "714065173858881540",
        "id" : 714065173858881540,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CejefgpWEAQUpVc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 794,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 794,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 794,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 514
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5ujFwXruAp"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714065175368888320",
    "text" : "Smart, contextual organ donation ad via @brilliantads https:\/\/t.co\/5ujFwXruAp",
    "id" : 714065175368888320,
    "created_at" : "2016-03-27 12:22:59 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 714255414225997824,
  "created_at" : "2016-03-28 00:58:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714239487178616833",
  "text" : "RT @bellasmiille: The person you took for granted today, may turn out to be the person you need tomorrow..",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710838676196986887",
    "text" : "The person you took for granted today, may turn out to be the person you need tomorrow..",
    "id" : 710838676196986887,
    "created_at" : "2016-03-18 14:42:01 +0000",
    "user" : {
      "name" : "Bella",
      "screen_name" : "Bellala",
      "protected" : false,
      "id_str" : "1849193754",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/729268966607101952\/Oxa4Dz66_normal.jpg",
      "id" : 1849193754,
      "verified" : false
    }
  },
  "id" : 714239487178616833,
  "created_at" : "2016-03-27 23:55:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714203668510339073",
  "text" : "Which Adjustable Laptop Stand would you recommend?",
  "id" : 714203668510339073,
  "created_at" : "2016-03-27 21:33:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714191640349515776",
  "text" : "Irish traditional music on TG 4 -  Chieftains 2016.",
  "id" : 714191640349515776,
  "created_at" : "2016-03-27 20:45:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714165032477728769",
  "text" : "What can I cook for dinner?  I got cabbage, mushroom, mozzarella. thanks.",
  "id" : 714165032477728769,
  "created_at" : "2016-03-27 18:59:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714158813004304384",
  "text" : "Lurking at a corner waiting for someone to brandish some data to support a claim so I can sprang on it ask : What is the Effect size?",
  "id" : 714158813004304384,
  "created_at" : "2016-03-27 18:35:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714155371192066049",
  "text" : "Can I say that 2016 is the year of the Snapchat?",
  "id" : 714155371192066049,
  "created_at" : "2016-03-27 18:21:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/6Wj3zS5eY5",
      "expanded_url" : "https:\/\/twitter.com\/AdobeDocCloud\/status\/712002101556023297",
      "display_url" : "twitter.com\/AdobeDocCloud\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714152949749039104",
  "text" : "Fine print: This feature works in conjunction with an Acrobat DC subscription. https:\/\/t.co\/6Wj3zS5eY5",
  "id" : 714152949749039104,
  "created_at" : "2016-03-27 18:11:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mehdi Hasan",
      "screen_name" : "mehdirhasan",
      "indices" : [ 3, 15 ],
      "id_str" : "130557513",
      "id" : 130557513
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "standwithchristians",
      "indices" : [ 34, 54 ]
    }, {
      "text" : "solidarity",
      "indices" : [ 55, 66 ]
    }, {
      "text" : "EasterSunday",
      "indices" : [ 67, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/jCPYkzTpo7",
      "expanded_url" : "https:\/\/twitter.com\/dhume\/status\/714148203759812608",
      "display_url" : "twitter.com\/dhume\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714152029808148480",
  "text" : "RT @mehdirhasan: Disgusting news. #standwithchristians #solidarity #EasterSunday  https:\/\/t.co\/jCPYkzTpo7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "standwithchristians",
        "indices" : [ 17, 37 ]
      }, {
        "text" : "solidarity",
        "indices" : [ 38, 49 ]
      }, {
        "text" : "EasterSunday",
        "indices" : [ 50, 63 ]
      } ],
      "urls" : [ {
        "indices" : [ 65, 88 ],
        "url" : "https:\/\/t.co\/jCPYkzTpo7",
        "expanded_url" : "https:\/\/twitter.com\/dhume\/status\/714148203759812608",
        "display_url" : "twitter.com\/dhume\/status\/7\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "714149988318707712",
    "text" : "Disgusting news. #standwithchristians #solidarity #EasterSunday  https:\/\/t.co\/jCPYkzTpo7",
    "id" : 714149988318707712,
    "created_at" : "2016-03-27 18:00:00 +0000",
    "user" : {
      "name" : "Mehdi Hasan",
      "screen_name" : "mehdirhasan",
      "protected" : false,
      "id_str" : "130557513",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/848875864917979137\/C2nZtLkd_normal.jpg",
      "id" : 130557513,
      "verified" : true
    }
  },
  "id" : 714152029808148480,
  "created_at" : "2016-03-27 18:08:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714129661987528705",
  "text" : "RT @klillington: Breaking: from an activist in Pakistan; journalists on the ground at suicide bomb that hit park full of families now say 2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "714126134879891456",
    "text" : "Breaking: from an activist in Pakistan; journalists on the ground at suicide bomb that hit park full of families now say 200 dead",
    "id" : 714126134879891456,
    "created_at" : "2016-03-27 16:25:13 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 714129661987528705,
  "created_at" : "2016-03-27 16:39:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dorothy Kenny",
      "screen_name" : "DorothyKenny6",
      "indices" : [ 3, 17 ],
      "id_str" : "3364045300",
      "id" : 3364045300
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 19, 25 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714109663726407680",
  "text" : "RT @DorothyKenny6: @mryap learned it off by heart at school in the 50s Have not forgotten a word.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "714108461852786688",
    "geo" : { },
    "id_str" : "714109599373271044",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap learned it off by heart at school in the 50s Have not forgotten a word.",
    "id" : 714109599373271044,
    "in_reply_to_status_id" : 714108461852786688,
    "created_at" : "2016-03-27 15:19:30 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Dorothy Kenny",
      "screen_name" : "DorothyKenny6",
      "protected" : false,
      "id_str" : "3364045300",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964313108826206208\/i8WYEWCd_normal.jpg",
      "id" : 3364045300,
      "verified" : false
    }
  },
  "id" : 714109663726407680,
  "created_at" : "2016-03-27 15:19:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/714109544281071616\/photo\/1",
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/Rieq9JPcoW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CekG2J3WQAAUE-l.jpg",
      "id_str" : "714109543345700864",
      "id" : 714109543345700864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CekG2J3WQAAUE-l.jpg",
      "sizes" : [ {
        "h" : 981,
        "resize" : "fit",
        "w" : 876
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 981,
        "resize" : "fit",
        "w" : 876
      }, {
        "h" : 981,
        "resize" : "fit",
        "w" : 876
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 607
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Rieq9JPcoW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714109544281071616",
  "text" : "Using Jupyter notebook? I recommend pandas-profiling. It autoplot variables,correlations &amp; highlight missing data. https:\/\/t.co\/Rieq9JPcoW",
  "id" : 714109544281071616,
  "created_at" : "2016-03-27 15:19:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland2016",
      "indices" : [ 60, 72 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714108461852786688",
  "text" : "Quick question: How many of you can recite the Proclamation?#Ireland2016",
  "id" : 714108461852786688,
  "created_at" : "2016-03-27 15:14:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "714102987443056640",
  "text" : "For children in England, there is Easter Break school works but none in Ireland.",
  "id" : 714102987443056640,
  "created_at" : "2016-03-27 14:53:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Rehm",
      "screen_name" : "alexanderrehm",
      "indices" : [ 3, 17 ],
      "id_str" : "17129773",
      "id" : 17129773
    }, {
      "name" : "StackSocial",
      "screen_name" : "StackSocial",
      "indices" : [ 94, 106 ],
      "id_str" : "243742269",
      "id" : 243742269
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/Z92raMIMVS",
      "expanded_url" : "https:\/\/stacksocial.com\/sales\/free-learn-to-cook-learn-to-learn-with-the-4-hour-chef-audiobook?rid=1235764",
      "display_url" : "stacksocial.com\/sales\/free-lea\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714097274343829504",
  "text" : "RT @alexanderrehm: Get it free: Free: 'The 4-Hour Chef' Audiobook https:\/\/t.co\/Z92raMIMVS via @StackSocial",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "StackSocial",
        "screen_name" : "StackSocial",
        "indices" : [ 75, 87 ],
        "id_str" : "243742269",
        "id" : 243742269
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/Z92raMIMVS",
        "expanded_url" : "https:\/\/stacksocial.com\/sales\/free-learn-to-cook-learn-to-learn-with-the-4-hour-chef-audiobook?rid=1235764",
        "display_url" : "stacksocial.com\/sales\/free-lea\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "714035652627664896",
    "text" : "Get it free: Free: 'The 4-Hour Chef' Audiobook https:\/\/t.co\/Z92raMIMVS via @StackSocial",
    "id" : 714035652627664896,
    "created_at" : "2016-03-27 10:25:40 +0000",
    "user" : {
      "name" : "Alexander Rehm",
      "screen_name" : "alexanderrehm",
      "protected" : false,
      "id_str" : "17129773",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1557332343\/275537_581111274_804477091_n_normal.jpg",
      "id" : 17129773,
      "verified" : false
    }
  },
  "id" : 714097274343829504,
  "created_at" : "2016-03-27 14:30:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/n0uz9amScS",
      "expanded_url" : "https:\/\/twitter.com\/DanBlueChief\/status\/714045817238073344",
      "display_url" : "twitter.com\/DanBlueChief\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "714069100226613248",
  "text" : "RT @topgold: You never tweet alone. https:\/\/t.co\/n0uz9amScS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/n0uz9amScS",
        "expanded_url" : "https:\/\/twitter.com\/DanBlueChief\/status\/714045817238073344",
        "display_url" : "twitter.com\/DanBlueChief\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "714060713401171968",
    "text" : "You never tweet alone. https:\/\/t.co\/n0uz9amScS",
    "id" : 714060713401171968,
    "created_at" : "2016-03-27 12:05:15 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 714069100226613248,
  "created_at" : "2016-03-27 12:38:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    }, {
      "name" : "Quartz",
      "screen_name" : "qz",
      "indices" : [ 135, 138 ],
      "id_str" : "573918122",
      "id" : 573918122
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/cOe4Ia8NFY",
      "expanded_url" : "http:\/\/qz.com\/643234",
      "display_url" : "qz.com\/643234"
    } ]
  },
  "geo" : { },
  "id_str" : "714007189485195264",
  "text" : "RT @yapphenghui: A Cambridge professor on how to stop being so easily manipulated by misleading statistics https:\/\/t.co\/cOe4Ia8NFY via @qz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Quartz",
        "screen_name" : "qz",
        "indices" : [ 118, 121 ],
        "id_str" : "573918122",
        "id" : 573918122
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/cOe4Ia8NFY",
        "expanded_url" : "http:\/\/qz.com\/643234",
        "display_url" : "qz.com\/643234"
      } ]
    },
    "geo" : { },
    "id_str" : "713952206437818370",
    "text" : "A Cambridge professor on how to stop being so easily manipulated by misleading statistics https:\/\/t.co\/cOe4Ia8NFY via @qz",
    "id" : 713952206437818370,
    "created_at" : "2016-03-27 04:54:05 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 714007189485195264,
  "created_at" : "2016-03-27 08:32:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/713756160516616196\/photo\/1",
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/P45ZCL5yXv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CefFchyWQAENYLP.jpg",
      "id_str" : "713756159858065409",
      "id" : 713756159858065409,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CefFchyWQAENYLP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 595
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 595
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 595
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 595
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/P45ZCL5yXv"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/0RTyMq4NpA",
      "expanded_url" : "http:\/\/econ.st\/1UhUxT9",
      "display_url" : "econ.st\/1UhUxT9"
    } ]
  },
  "geo" : { },
  "id_str" : "713828735452069888",
  "text" : "RT @TheEconomist: What was the Easter Rising, and why is it still important? https:\/\/t.co\/0RTyMq4NpA https:\/\/t.co\/P45ZCL5yXv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/713756160516616196\/photo\/1",
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/P45ZCL5yXv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CefFchyWQAENYLP.jpg",
        "id_str" : "713756159858065409",
        "id" : 713756159858065409,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CefFchyWQAENYLP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 595
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/P45ZCL5yXv"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/0RTyMq4NpA",
        "expanded_url" : "http:\/\/econ.st\/1UhUxT9",
        "display_url" : "econ.st\/1UhUxT9"
      } ]
    },
    "geo" : { },
    "id_str" : "713756160516616196",
    "text" : "What was the Easter Rising, and why is it still important? https:\/\/t.co\/0RTyMq4NpA https:\/\/t.co\/P45ZCL5yXv",
    "id" : 713756160516616196,
    "created_at" : "2016-03-26 15:55:04 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 713828735452069888,
  "created_at" : "2016-03-26 20:43:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Automattic",
      "screen_name" : "automattic",
      "indices" : [ 3, 14 ],
      "id_str" : "15919116",
      "id" : 15919116
    }, {
      "name" : "Steph Yiu",
      "screen_name" : "crushgear",
      "indices" : [ 49, 59 ],
      "id_str" : "22504591",
      "id" : 22504591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/buOzGlHnz6",
      "expanded_url" : "https:\/\/twitter.com\/workdif\/status\/710867122969247744",
      "display_url" : "twitter.com\/workdif\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713772780202946560",
  "text" : "RT @automattic: Meet Automattician Steph Yiu aka @crushgear, VIP Account Engineer and Team Lead! https:\/\/t.co\/buOzGlHnz6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Steph Yiu",
        "screen_name" : "crushgear",
        "indices" : [ 33, 43 ],
        "id_str" : "22504591",
        "id" : 22504591
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/buOzGlHnz6",
        "expanded_url" : "https:\/\/twitter.com\/workdif\/status\/710867122969247744",
        "display_url" : "twitter.com\/workdif\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710892830273757184",
    "text" : "Meet Automattician Steph Yiu aka @crushgear, VIP Account Engineer and Team Lead! https:\/\/t.co\/buOzGlHnz6",
    "id" : 710892830273757184,
    "created_at" : "2016-03-18 18:17:13 +0000",
    "user" : {
      "name" : "Automattic",
      "screen_name" : "automattic",
      "protected" : false,
      "id_str" : "15919116",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/453860857923923968\/Rg_FuLjK_normal.png",
      "id" : 15919116,
      "verified" : true
    }
  },
  "id" : 713772780202946560,
  "created_at" : "2016-03-26 17:01:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "9 Dash Line \u4E5D\u6BB5\u7EBF",
      "screen_name" : "9DashLine",
      "indices" : [ 3, 13 ],
      "id_str" : "2847258009",
      "id" : 2847258009
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 57, 67 ]
    } ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/fSrfjwFHP2",
      "expanded_url" : "http:\/\/bloom.bg\/1PunkvI",
      "display_url" : "bloom.bg\/1PunkvI"
    } ]
  },
  "geo" : { },
  "id_str" : "713714634818240512",
  "text" : "RT @9DashLine: Arms Spending Spree in Southeast Asia Has #Singapore Worried https:\/\/t.co\/fSrfjwFHP2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 42, 52 ]
      } ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/fSrfjwFHP2",
        "expanded_url" : "http:\/\/bloom.bg\/1PunkvI",
        "display_url" : "bloom.bg\/1PunkvI"
      } ]
    },
    "geo" : { },
    "id_str" : "713702838568886273",
    "text" : "Arms Spending Spree in Southeast Asia Has #Singapore Worried https:\/\/t.co\/fSrfjwFHP2",
    "id" : 713702838568886273,
    "created_at" : "2016-03-26 12:23:11 +0000",
    "user" : {
      "name" : "9 Dash Line \u4E5D\u6BB5\u7EBF",
      "screen_name" : "9DashLine",
      "protected" : false,
      "id_str" : "2847258009",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/545724611950678018\/rwdvgMdk_normal.jpeg",
      "id" : 2847258009,
      "verified" : false
    }
  },
  "id" : 713714634818240512,
  "created_at" : "2016-03-26 13:10:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 39 ],
      "url" : "https:\/\/t.co\/BHWrmaAu7g",
      "expanded_url" : "https:\/\/twitter.com\/avalanchelynn\/status\/713678385600266240",
      "display_url" : "twitter.com\/avalanchelynn\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713685710142902272",
  "text" : "I missed this.  https:\/\/t.co\/BHWrmaAu7g",
  "id" : 713685710142902272,
  "created_at" : "2016-03-26 11:15:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713671771518582784",
  "text" : "When it comes to book, my recent experience is dead-tree format still rules.",
  "id" : 713671771518582784,
  "created_at" : "2016-03-26 10:19:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laura",
      "screen_name" : "HowToGYST",
      "indices" : [ 0, 10 ],
      "id_str" : "2873931282",
      "id" : 2873931282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "713669016938790913",
  "geo" : { },
  "id_str" : "713671142138126337",
  "in_reply_to_user_id" : 2873931282,
  "text" : "@HowToGYST Good morning!",
  "id" : 713671142138126337,
  "in_reply_to_status_id" : 713669016938790913,
  "created_at" : "2016-03-26 10:17:14 +0000",
  "in_reply_to_screen_name" : "HowToGYST",
  "in_reply_to_user_id_str" : "2873931282",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Al Jazeera News",
      "screen_name" : "AJENews",
      "indices" : [ 3, 11 ],
      "id_str" : "18424289",
      "id" : 18424289
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/Pjy6z2SFAq",
      "expanded_url" : "http:\/\/aje.io\/8uk6",
      "display_url" : "aje.io\/8uk6"
    } ]
  },
  "geo" : { },
  "id_str" : "713658614062456832",
  "text" : "RT @AJENews: Death toll in ISIL attack on football stadium in Iraq rises to 30, with some 95 wounded. https:\/\/t.co\/Pjy6z2SFAq https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AJENews\/status\/713626076510097408\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/Aehu5Ck6Zn",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CedPIpNW4AE0sea.jpg",
        "id_str" : "713626075880939521",
        "id" : 713626075880939521,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CedPIpNW4AE0sea.jpg",
        "sizes" : [ {
          "h" : 562,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 562,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 562,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Aehu5Ck6Zn"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/Pjy6z2SFAq",
        "expanded_url" : "http:\/\/aje.io\/8uk6",
        "display_url" : "aje.io\/8uk6"
      } ]
    },
    "geo" : { },
    "id_str" : "713626076510097408",
    "text" : "Death toll in ISIL attack on football stadium in Iraq rises to 30, with some 95 wounded. https:\/\/t.co\/Pjy6z2SFAq https:\/\/t.co\/Aehu5Ck6Zn",
    "id" : 713626076510097408,
    "created_at" : "2016-03-26 07:18:09 +0000",
    "user" : {
      "name" : "Al Jazeera News",
      "screen_name" : "AJENews",
      "protected" : false,
      "id_str" : "18424289",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875639380781563906\/b1J1YbIE_normal.jpg",
      "id" : 18424289,
      "verified" : true
    }
  },
  "id" : 713658614062456832,
  "created_at" : "2016-03-26 09:27:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Independent",
      "screen_name" : "Independent",
      "indices" : [ 3, 15 ],
      "id_str" : "16973333",
      "id" : 16973333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/MXrBgLLcT6",
      "expanded_url" : "http:\/\/i100.io\/AiZstsN",
      "display_url" : "i100.io\/AiZstsN"
    } ]
  },
  "geo" : { },
  "id_str" : "713657322330066944",
  "text" : "RT @Independent: Turkish people are sharing this cartoon asking where our sympathy was for them https:\/\/t.co\/MXrBgLLcT6 https:\/\/t.co\/wpyzxw\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Independent\/status\/713637909086269440\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/wpyzxwFRjo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CedZ5arWsAA-FrI.jpg",
        "id_str" : "713637908910092288",
        "id" : 713637908910092288,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CedZ5arWsAA-FrI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1003,
          "resize" : "fit",
          "w" : 1520
        }, {
          "h" : 449,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 792,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1003,
          "resize" : "fit",
          "w" : 1520
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wpyzxwFRjo"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/MXrBgLLcT6",
        "expanded_url" : "http:\/\/i100.io\/AiZstsN",
        "display_url" : "i100.io\/AiZstsN"
      } ]
    },
    "geo" : { },
    "id_str" : "713637909086269440",
    "text" : "Turkish people are sharing this cartoon asking where our sympathy was for them https:\/\/t.co\/MXrBgLLcT6 https:\/\/t.co\/wpyzxwFRjo",
    "id" : 713637909086269440,
    "created_at" : "2016-03-26 08:05:10 +0000",
    "user" : {
      "name" : "The Independent",
      "screen_name" : "Independent",
      "protected" : false,
      "id_str" : "16973333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1015965267116089347\/_Aaz8Ff7_normal.jpg",
      "id" : 16973333,
      "verified" : true
    }
  },
  "id" : 713657322330066944,
  "created_at" : "2016-03-26 09:22:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/Lx46pCYIbL",
      "expanded_url" : "https:\/\/www.diigo.com\/item\/image\/5hsez\/3hv5?size=o",
      "display_url" : "diigo.com\/item\/image\/5hs\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713516798935756800",
  "text" : "Best use of analogy to explain Machine Learning https:\/\/t.co\/Lx46pCYIbL",
  "id" : 713516798935756800,
  "created_at" : "2016-03-26 00:03:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713500994416545793",
  "text" : "There is no chocolate in the kitchen!",
  "id" : 713500994416545793,
  "created_at" : "2016-03-25 23:01:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tamim Mobayed",
      "screen_name" : "tammob88",
      "indices" : [ 3, 12 ],
      "id_str" : "90619565",
      "id" : 90619565
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "muslims",
      "indices" : [ 119, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713487078105948164",
  "text" : "RT @tammob88: Love it-lightheartedly illuminating darkness in Dublin.\"Sound\" is an Irish colloquialism used for \"good\" #muslims https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tammob88\/status\/713080780973539328\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/1ZN3PYGhnQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeVfLT7WEAAUHKc.jpg",
        "id_str" : "713080763940474880",
        "id" : 713080763940474880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeVfLT7WEAAUHKc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 242,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 342,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 342,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 342,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1ZN3PYGhnQ"
      } ],
      "hashtags" : [ {
        "text" : "muslims",
        "indices" : [ 105, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "713080780973539328",
    "text" : "Love it-lightheartedly illuminating darkness in Dublin.\"Sound\" is an Irish colloquialism used for \"good\" #muslims https:\/\/t.co\/1ZN3PYGhnQ",
    "id" : 713080780973539328,
    "created_at" : "2016-03-24 19:11:21 +0000",
    "user" : {
      "name" : "Tamim Mobayed",
      "screen_name" : "tammob88",
      "protected" : false,
      "id_str" : "90619565",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038892767605391363\/MqxarCT9_normal.jpg",
      "id" : 90619565,
      "verified" : false
    }
  },
  "id" : 713487078105948164,
  "created_at" : "2016-03-25 22:05:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "indices" : [ 3, 17 ],
      "id_str" : "52052800",
      "id" : 52052800
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 143 ],
      "url" : "https:\/\/t.co\/KCs9AgZpA3",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/offensive-graffiti-in-dublin-altered-to-all-muslims-are-sound-1.2587020",
      "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713486862401331200",
  "text" : "RT @DrUmarAlQadri: Dublin is making news around the world for being tolerant &amp; sound.Proud to be Muslim &amp; Irish https:\/\/t.co\/KCs9AgZpA3 htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DrUmarAlQadri\/status\/713472934292144129\/photo\/1",
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/TILyAz80CD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CebD2lCW8AAKl3G.jpg",
        "id_str" : "713472933407158272",
        "id" : 713472933407158272,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CebD2lCW8AAKl3G.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 214,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 214,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 214,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 214,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/TILyAz80CD"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/KCs9AgZpA3",
        "expanded_url" : "http:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/offensive-graffiti-in-dublin-altered-to-all-muslims-are-sound-1.2587020",
        "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "713472934292144129",
    "text" : "Dublin is making news around the world for being tolerant &amp; sound.Proud to be Muslim &amp; Irish https:\/\/t.co\/KCs9AgZpA3 https:\/\/t.co\/TILyAz80CD",
    "id" : 713472934292144129,
    "created_at" : "2016-03-25 21:09:37 +0000",
    "user" : {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "protected" : false,
      "id_str" : "52052800",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026918392907919360\/mMi9i844_normal.jpg",
      "id" : 52052800,
      "verified" : false
    }
  },
  "id" : 713486862401331200,
  "created_at" : "2016-03-25 22:04:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "J.K. Rowling",
      "screen_name" : "jk_rowling",
      "indices" : [ 3, 14 ],
      "id_str" : "62513246",
      "id" : 62513246
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713485128174026752",
  "text" : "RT @jk_rowling: I wasn't going to give up until every single publisher turned me down, but I often feared that would happen. https:\/\/t.co\/b\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/bMKu4zJ3nm",
        "expanded_url" : "https:\/\/twitter.com\/givemhelldarlin\/status\/713288416998232064",
        "display_url" : "twitter.com\/givemhelldarli\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "713292055284424704",
    "text" : "I wasn't going to give up until every single publisher turned me down, but I often feared that would happen. https:\/\/t.co\/bMKu4zJ3nm",
    "id" : 713292055284424704,
    "created_at" : "2016-03-25 09:10:52 +0000",
    "user" : {
      "name" : "J.K. Rowling",
      "screen_name" : "jk_rowling",
      "protected" : false,
      "id_str" : "62513246",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031233484419948544\/eUt5_nXy_normal.jpg",
      "id" : 62513246,
      "verified" : true
    }
  },
  "id" : 713485128174026752,
  "created_at" : "2016-03-25 21:58:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rebecca Lynn",
      "screen_name" : "VCRebecca",
      "indices" : [ 3, 13 ],
      "id_str" : "144400453",
      "id" : 144400453
    }, {
      "name" : "Lukas Biewald",
      "screen_name" : "l2k",
      "indices" : [ 90, 94 ],
      "id_str" : "13920962",
      "id" : 13920962
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/VCRebecca\/status\/712756861125021696\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/96eDxPRoKG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeQ4lraUUAANus5.jpg",
      "id_str" : "712756860990803968",
      "id" : 712756860990803968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeQ4lraUUAANus5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 522,
        "resize" : "fit",
        "w" : 738
      }, {
        "h" : 522,
        "resize" : "fit",
        "w" : 738
      }, {
        "h" : 522,
        "resize" : "fit",
        "w" : 738
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/96eDxPRoKG"
    } ],
    "hashtags" : [ {
      "text" : "machinelearning",
      "indices" : [ 45, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/zUsjjPlFq9",
      "expanded_url" : "http:\/\/techcrunch.com\/2016\/03\/19\/how-real-businesses-are-using-machine-learning\/",
      "display_url" : "techcrunch.com\/2016\/03\/19\/how\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713472931184173056",
  "text" : "RT @VCRebecca: How real businesses are using #machinelearning  https:\/\/t.co\/zUsjjPlFq9 by @l2k https:\/\/t.co\/96eDxPRoKG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/6builder.com\" rel=\"nofollow\"\u003E6Builder\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Lukas Biewald",
        "screen_name" : "l2k",
        "indices" : [ 75, 79 ],
        "id_str" : "13920962",
        "id" : 13920962
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/VCRebecca\/status\/712756861125021696\/photo\/1",
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/96eDxPRoKG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeQ4lraUUAANus5.jpg",
        "id_str" : "712756860990803968",
        "id" : 712756860990803968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeQ4lraUUAANus5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 522,
          "resize" : "fit",
          "w" : 738
        }, {
          "h" : 522,
          "resize" : "fit",
          "w" : 738
        }, {
          "h" : 522,
          "resize" : "fit",
          "w" : 738
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/96eDxPRoKG"
      } ],
      "hashtags" : [ {
        "text" : "machinelearning",
        "indices" : [ 30, 46 ]
      } ],
      "urls" : [ {
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/zUsjjPlFq9",
        "expanded_url" : "http:\/\/techcrunch.com\/2016\/03\/19\/how-real-businesses-are-using-machine-learning\/",
        "display_url" : "techcrunch.com\/2016\/03\/19\/how\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712756861125021696",
    "text" : "How real businesses are using #machinelearning  https:\/\/t.co\/zUsjjPlFq9 by @l2k https:\/\/t.co\/96eDxPRoKG",
    "id" : 712756861125021696,
    "created_at" : "2016-03-23 21:44:12 +0000",
    "user" : {
      "name" : "Rebecca Lynn",
      "screen_name" : "VCRebecca",
      "protected" : false,
      "id_str" : "144400453",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/581686231110590465\/bPrcWPHT_normal.jpg",
      "id" : 144400453,
      "verified" : false
    }
  },
  "id" : 713472931184173056,
  "created_at" : "2016-03-25 21:09:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pavan Mirla",
      "screen_name" : "pavanmirla",
      "indices" : [ 3, 14 ],
      "id_str" : "34576524",
      "id" : 34576524
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pavanmirla\/status\/600682060886450176\/photo\/1",
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/b0Jcuza7Fc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CFYNJpVW8AAlmts.png",
      "id_str" : "600682059665960960",
      "id" : 600682059665960960,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CFYNJpVW8AAlmts.png",
      "sizes" : [ {
        "h" : 359,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 359,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 359,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 359,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/b0Jcuza7Fc"
    } ],
    "hashtags" : [ {
      "text" : "statistics",
      "indices" : [ 52, 63 ]
    }, {
      "text" : "dataviz",
      "indices" : [ 65, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/xSU3M09Pjx",
      "expanded_url" : "https:\/\/youtu.be\/f5liqUk0ZTw",
      "display_url" : "youtu.be\/f5liqUk0ZTw"
    } ]
  },
  "geo" : { },
  "id_str" : "713471835426463744",
  "text" : "RT @pavanmirla: Watch this video for vector basics. #statistics  #dataviz\nhttps:\/\/t.co\/xSU3M09Pjx http:\/\/t.co\/b0Jcuza7Fc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pavanmirla\/status\/600682060886450176\/photo\/1",
        "indices" : [ 82, 104 ],
        "url" : "http:\/\/t.co\/b0Jcuza7Fc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CFYNJpVW8AAlmts.png",
        "id_str" : "600682059665960960",
        "id" : 600682059665960960,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CFYNJpVW8AAlmts.png",
        "sizes" : [ {
          "h" : 359,
          "resize" : "fit",
          "w" : 577
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 577
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 577
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 577
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/b0Jcuza7Fc"
      } ],
      "hashtags" : [ {
        "text" : "statistics",
        "indices" : [ 36, 47 ]
      }, {
        "text" : "dataviz",
        "indices" : [ 49, 57 ]
      } ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/xSU3M09Pjx",
        "expanded_url" : "https:\/\/youtu.be\/f5liqUk0ZTw",
        "display_url" : "youtu.be\/f5liqUk0ZTw"
      } ]
    },
    "geo" : { },
    "id_str" : "600682060886450176",
    "text" : "Watch this video for vector basics. #statistics  #dataviz\nhttps:\/\/t.co\/xSU3M09Pjx http:\/\/t.co\/b0Jcuza7Fc",
    "id" : 600682060886450176,
    "created_at" : "2015-05-19 15:18:58 +0000",
    "user" : {
      "name" : "Pavan Mirla",
      "screen_name" : "pavanmirla",
      "protected" : false,
      "id_str" : "34576524",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/799655728755548160\/fNjXeFPn_normal.jpg",
      "id" : 34576524,
      "verified" : false
    }
  },
  "id" : 713471835426463744,
  "created_at" : "2016-03-25 21:05:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gerry Doyle",
      "screen_name" : "mgerrydoyle",
      "indices" : [ 3, 15 ],
      "id_str" : "788697546",
      "id" : 788697546
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/JfVoDxbUz0",
      "expanded_url" : "https:\/\/twitter.com\/SeanLotman\/status\/712986900269637633",
      "display_url" : "twitter.com\/SeanLotman\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713463602964008961",
  "text" : "RT @mgerrydoyle: I use a very popular app called \"sit down and read a book.\"  https:\/\/t.co\/JfVoDxbUz0",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/JfVoDxbUz0",
        "expanded_url" : "https:\/\/twitter.com\/SeanLotman\/status\/712986900269637633",
        "display_url" : "twitter.com\/SeanLotman\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "713215294202716162",
    "text" : "I use a very popular app called \"sit down and read a book.\"  https:\/\/t.co\/JfVoDxbUz0",
    "id" : 713215294202716162,
    "created_at" : "2016-03-25 04:05:51 +0000",
    "user" : {
      "name" : "Gerry Doyle",
      "screen_name" : "mgerrydoyle",
      "protected" : false,
      "id_str" : "788697546",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2554916706\/headshot_normal.jpg",
      "id" : 788697546,
      "verified" : false
    }
  },
  "id" : 713463602964008961,
  "created_at" : "2016-03-25 20:32:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/I2fUBCz8eC",
      "expanded_url" : "https:\/\/twitter.com\/Clearpreso\/status\/713457408111198210",
      "display_url" : "twitter.com\/Clearpreso\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713460979326132224",
  "text" : "Maybe a new venture idea here. Turn your PDF into website!  https:\/\/t.co\/I2fUBCz8eC",
  "id" : 713460979326132224,
  "created_at" : "2016-03-25 20:22:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/pNl1YQN2l9",
      "expanded_url" : "http:\/\/www.fastcodesign.com\/3058123\/the-most-opulent-meals-served-on-flights-around-the-world",
      "display_url" : "fastcodesign.com\/3058123\/the-mo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713380144103223296",
  "text" : "The most opulent meals you can eat in the air https:\/\/t.co\/pNl1YQN2l9",
  "id" : 713380144103223296,
  "created_at" : "2016-03-25 15:00:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iworkwithdata",
      "indices" : [ 129, 143 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713364821777981440",
  "text" : "Filters in Google Analytics determine what data are presented in a profile &amp; how those data are presented. Use it with care. #iworkwithdata",
  "id" : 713364821777981440,
  "created_at" : "2016-03-25 14:00:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713356492490063872",
  "text" : "So how is everyone keeping dry today in Ireland?",
  "id" : 713356492490063872,
  "created_at" : "2016-03-25 13:26:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CPF Board",
      "screen_name" : "CPF_Board",
      "indices" : [ 0, 10 ],
      "id_str" : "21959654",
      "id" : 21959654
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/cOYDN8gJlV",
      "expanded_url" : "https:\/\/www.facebook.com\/CPFBoard\/photos\/a.71363925922.107143.70638270922\/10153877028145923\/?type=3&theater",
      "display_url" : "facebook.com\/CPFBoard\/photo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713318500241158145",
  "in_reply_to_user_id" : 21959654,
  "text" : "@CPF_Board I'm overseas. How do I get this OneKey? https:\/\/t.co\/cOYDN8gJlV Thanks",
  "id" : 713318500241158145,
  "created_at" : "2016-03-25 10:55:57 +0000",
  "in_reply_to_screen_name" : "CPF_Board",
  "in_reply_to_user_id_str" : "21959654",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mona Eltahawy",
      "screen_name" : "monaeltahawy",
      "indices" : [ 3, 16 ],
      "id_str" : "29979814",
      "id" : 29979814
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713315547824451584",
  "text" : "RT @monaeltahawy: She was once my hero. What a disgrace: \"No one told me I was going to be interviewed by a Muslim,\" Aung San Suu Kyi https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/kE5q1bitHI",
        "expanded_url" : "http:\/\/www.dailymail.co.uk\/news\/article-3508710\/Moment-Burma-heroine-lost-cool-Today-s-Mishal-Suu-Kyi-s-anger-no-one-told-going-interviewed-Muslim-heated-questioning-BBC.html",
        "display_url" : "dailymail.co.uk\/news\/article-3\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "713307505150074880",
    "text" : "She was once my hero. What a disgrace: \"No one told me I was going to be interviewed by a Muslim,\" Aung San Suu Kyi https:\/\/t.co\/kE5q1bitHI",
    "id" : 713307505150074880,
    "created_at" : "2016-03-25 10:12:16 +0000",
    "user" : {
      "name" : "Mona Eltahawy",
      "screen_name" : "monaeltahawy",
      "protected" : false,
      "id_str" : "29979814",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024869858171912192\/1i_hvFkm_normal.jpg",
      "id" : 29979814,
      "verified" : true
    }
  },
  "id" : 713315547824451584,
  "created_at" : "2016-03-25 10:44:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iworkwithdata",
      "indices" : [ 97, 111 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713314608245841920",
  "text" : "How do you actually measure the deciding factor between loyal and disloyal customers using data? #iworkwithdata",
  "id" : 713314608245841920,
  "created_at" : "2016-03-25 10:40:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eoin O'Malley",
      "screen_name" : "AnMailleach",
      "indices" : [ 3, 15 ],
      "id_str" : "250131497",
      "id" : 250131497
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoodFriday",
      "indices" : [ 98, 109 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713308981108809728",
  "text" : "RT @AnMailleach: Any sightings yet of the first English stag party to realise coming to Dublin on #GoodFriday isn't the genius idea they th\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GoodFriday",
        "indices" : [ 81, 92 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "713290788155867136",
    "text" : "Any sightings yet of the first English stag party to realise coming to Dublin on #GoodFriday isn't the genius idea they thought?",
    "id" : 713290788155867136,
    "created_at" : "2016-03-25 09:05:50 +0000",
    "user" : {
      "name" : "Eoin O'Malley",
      "screen_name" : "AnMailleach",
      "protected" : false,
      "id_str" : "250131497",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/591976610594693121\/uQDtBi6S_normal.jpg",
      "id" : 250131497,
      "verified" : false
    }
  },
  "id" : 713308981108809728,
  "created_at" : "2016-03-25 10:18:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/4eirqBYmuf",
      "expanded_url" : "https:\/\/play.google.com\/store\/apps\/details?id=com.google.android.apps.accessibility.auditor",
      "display_url" : "play.google.com\/store\/apps\/det\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713298500503658496",
  "text" : "Google launches a tool to test your Android app for accessibility issues https:\/\/t.co\/4eirqBYmuf",
  "id" : 713298500503658496,
  "created_at" : "2016-03-25 09:36:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Miss Texas 1967",
      "screen_name" : "MsTexas1967",
      "indices" : [ 3, 15 ],
      "id_str" : "283610466",
      "id" : 283610466
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713297217931624449",
  "text" : "RT @MsTexas1967: My mother remarried a Frenchman\n\nI thought of him less as a stepfather and more as a faux pa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710216230985146370",
    "text" : "My mother remarried a Frenchman\n\nI thought of him less as a stepfather and more as a faux pa",
    "id" : 710216230985146370,
    "created_at" : "2016-03-16 21:28:39 +0000",
    "user" : {
      "name" : "Miss Texas 1967",
      "screen_name" : "MsTexas1967",
      "protected" : false,
      "id_str" : "283610466",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/510788008854183937\/-_ZXzalQ_normal.jpeg",
      "id" : 283610466,
      "verified" : false
    }
  },
  "id" : 713297217931624449,
  "created_at" : "2016-03-25 09:31:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/jN49jtY8Qm",
      "expanded_url" : "http:\/\/searchengineland.com\/google-analytics-360-suite-launch-audience-dmp-244713",
      "display_url" : "searchengineland.com\/google-analyti\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713293985234731008",
  "text" : "With Google Analytics 360, The Adometry and Google Analytics Premium brands will eventually be retired. https:\/\/t.co\/jN49jtY8Qm?",
  "id" : 713293985234731008,
  "created_at" : "2016-03-25 09:18:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713290698011844608",
  "text" : "How can you be a data Top influencer if you can't point me to notable research paper on \"More data or better algorithms in Data Mining?\"",
  "id" : 713290698011844608,
  "created_at" : "2016-03-25 09:05:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bigdata",
      "indices" : [ 32, 40 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713289101663842304",
  "text" : "A lot of sound bites out there. #bigdata",
  "id" : 713289101663842304,
  "created_at" : "2016-03-25 08:59:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713264722506825729",
  "text" : "Have a good Friday people.",
  "id" : 713264722506825729,
  "created_at" : "2016-03-25 07:22:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/713256416719593472\/photo\/1",
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/NslHcQH4Yt",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeX-6boXIAA7NsC.jpg",
      "id_str" : "713256395811004416",
      "id" : 713256395811004416,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeX-6boXIAA7NsC.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NslHcQH4Yt"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713256416719593472",
  "text" : "Good morning. https:\/\/t.co\/NslHcQH4Yt",
  "id" : 713256416719593472,
  "created_at" : "2016-03-25 06:49:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Google",
      "screen_name" : "Google",
      "indices" : [ 3, 10 ],
      "id_str" : "20536157",
      "id" : 20536157
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WeAreNotThis",
      "indices" : [ 118, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713162809463476229",
  "text" : "RT @google: We believe in equal rights and equal treatment for all. This North Carolina law is misguided &amp; wrong. #WeAreNotThis https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/google\/status\/713128258607390720\/photo\/1",
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/3yCayn7Tum",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeWKX2jWQAAV0RF.jpg",
        "id_str" : "713128258393489408",
        "id" : 713128258393489408,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeWKX2jWQAAV0RF.jpg",
        "sizes" : [ {
          "h" : 513,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 341,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3yCayn7Tum"
      } ],
      "hashtags" : [ {
        "text" : "WeAreNotThis",
        "indices" : [ 106, 119 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "713128258607390720",
    "text" : "We believe in equal rights and equal treatment for all. This North Carolina law is misguided &amp; wrong. #WeAreNotThis https:\/\/t.co\/3yCayn7Tum",
    "id" : 713128258607390720,
    "created_at" : "2016-03-24 22:20:00 +0000",
    "user" : {
      "name" : "Google",
      "screen_name" : "Google",
      "protected" : false,
      "id_str" : "20536157",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972154872261853184\/RnOg6UyU_normal.jpg",
      "id" : 20536157,
      "verified" : true
    }
  },
  "id" : 713162809463476229,
  "created_at" : "2016-03-25 00:37:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/6rQr6NG7B5",
      "expanded_url" : "http:\/\/www.ncbi.nlm.nih.gov\/pmc\/articles\/PMC3444174\/",
      "display_url" : "ncbi.nlm.nih.gov\/pmc\/articles\/P\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713152910901776384",
  "text" : "Result deemed to be statistically significant can be completely devoid of any practical significance. https:\/\/t.co\/6rQr6NG7B5",
  "id" : 713152910901776384,
  "created_at" : "2016-03-24 23:57:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713144694939783168",
  "text" : "How many it takes to participate in a Kaggle competition? 4 data analytics student.",
  "id" : 713144694939783168,
  "created_at" : "2016-03-24 23:25:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/713144388298387456\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/DkKTgjOW2k",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeWZCrSWAAAb-Lz.jpg",
      "id_str" : "713144387266543616",
      "id" : 713144387266543616,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeWZCrSWAAAb-Lz.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 407,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 407,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 407,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 407,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DkKTgjOW2k"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713144388298387456",
  "text" : "https:\/\/t.co\/DkKTgjOW2k",
  "id" : 713144388298387456,
  "created_at" : "2016-03-24 23:24:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/OXUQYrvQhY",
      "expanded_url" : "https:\/\/twitter.com\/CiamhieMc\/status\/713137885566865408",
      "display_url" : "twitter.com\/CiamhieMc\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "713141039494770688",
  "text" : "Yes, no alcohol for me! Have being dry for a long time.  https:\/\/t.co\/OXUQYrvQhY",
  "id" : 713141039494770688,
  "created_at" : "2016-03-24 23:10:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713140172502745088",
  "text" : "Problem with using trackpad. Just deleted my own tweets...",
  "id" : 713140172502745088,
  "created_at" : "2016-03-24 23:07:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft GigJam",
      "screen_name" : "GigJamApp",
      "indices" : [ 0, 10 ],
      "id_str" : "3256121041",
      "id" : 3256121041
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "713138349368717312",
  "geo" : { },
  "id_str" : "713139395172376576",
  "in_reply_to_user_id" : 3256121041,
  "text" : "@GigJamApp When will the app be already? I signed up more than half a year ago.",
  "id" : 713139395172376576,
  "in_reply_to_status_id" : 713138349368717312,
  "created_at" : "2016-03-24 23:04:15 +0000",
  "in_reply_to_screen_name" : "GigJamApp",
  "in_reply_to_user_id_str" : "3256121041",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713114159500640256",
  "text" : "How long does it take to train to be a Luas driver? Genuine question.",
  "id" : 713114159500640256,
  "created_at" : "2016-03-24 21:23:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Cummins",
      "screen_name" : "adriancummins",
      "indices" : [ 3, 17 ],
      "id_str" : "34912346",
      "id" : 34912346
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Luas",
      "indices" : [ 29, 34 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713112700029693952",
  "text" : "RT @adriancummins: I see the #Luas drivers are going on strike over the Easter weekend. Was it for this that men like Connolly died?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Luas",
        "indices" : [ 10, 15 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "713058753772396549",
    "text" : "I see the #Luas drivers are going on strike over the Easter weekend. Was it for this that men like Connolly died?",
    "id" : 713058753772396549,
    "created_at" : "2016-03-24 17:43:49 +0000",
    "user" : {
      "name" : "Adrian Cummins",
      "screen_name" : "adriancummins",
      "protected" : false,
      "id_str" : "34912346",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035219089676034048\/oAdudlrd_normal.jpg",
      "id" : 34912346,
      "verified" : false
    }
  },
  "id" : 713112700029693952,
  "created_at" : "2016-03-24 21:18:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Angela Nicholson",
      "screen_name" : "AngeNicholson",
      "indices" : [ 3, 17 ],
      "id_str" : "158810842",
      "id" : 158810842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713107571649937409",
  "text" : "RT @AngeNicholson: Good news! Google has made its excellent image editing software, Nik Collection (Silver Efex etc) free https:\/\/t.co\/KHE5\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/KHE5yREfJ1",
        "expanded_url" : "https:\/\/www.google.com\/nikcollection\/",
        "display_url" : "google.com\/nikcollection\/"
      } ]
    },
    "geo" : { },
    "id_str" : "713074811690422272",
    "text" : "Good news! Google has made its excellent image editing software, Nik Collection (Silver Efex etc) free https:\/\/t.co\/KHE5yREfJ1",
    "id" : 713074811690422272,
    "created_at" : "2016-03-24 18:47:38 +0000",
    "user" : {
      "name" : "Angela Nicholson",
      "screen_name" : "AngeNicholson",
      "protected" : false,
      "id_str" : "158810842",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/752931741036322816\/rcj7BaiY_normal.jpg",
      "id" : 158810842,
      "verified" : true
    }
  },
  "id" : 713107571649937409,
  "created_at" : "2016-03-24 20:57:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scarlett Sieber",
      "screen_name" : "ScarlettSieber",
      "indices" : [ 3, 18 ],
      "id_str" : "46691793",
      "id" : 46691793
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "crowdfunding",
      "indices" : [ 69, 82 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "713107015346864129",
  "text" : "RT @ScarlettSieber: Anyone have recommendations for best white label #crowdfunding platform?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/manageflitter.com\" rel=\"nofollow\"\u003EManageFlitter\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "crowdfunding",
        "indices" : [ 49, 62 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "713076771130769408",
    "text" : "Anyone have recommendations for best white label #crowdfunding platform?",
    "id" : 713076771130769408,
    "created_at" : "2016-03-24 18:55:25 +0000",
    "user" : {
      "name" : "Scarlett Sieber",
      "screen_name" : "ScarlettSieber",
      "protected" : false,
      "id_str" : "46691793",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/457607042144821249\/YFtBJqzj_normal.jpeg",
      "id" : 46691793,
      "verified" : true
    }
  },
  "id" : 713107015346864129,
  "created_at" : "2016-03-24 20:55:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "712986144309383168",
  "geo" : { },
  "id_str" : "712997623028912128",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold u referring to the Singapore budget rite?",
  "id" : 712997623028912128,
  "in_reply_to_status_id" : 712986144309383168,
  "created_at" : "2016-03-24 13:40:54 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "712986144309383168",
  "geo" : { },
  "id_str" : "712997461741215745",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold I don't get it. The govt giving handout. FFS.",
  "id" : 712997461741215745,
  "in_reply_to_status_id" : 712986144309383168,
  "created_at" : "2016-03-24 13:40:16 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason H. Moore, PhD",
      "screen_name" : "moorejh",
      "indices" : [ 3, 11 ],
      "id_str" : "15496407",
      "id" : 15496407
    }, {
      "name" : "Graciela Gonzalez",
      "screen_name" : "gracielagon",
      "indices" : [ 49, 61 ],
      "id_str" : "54320754",
      "id" : 54320754
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/moorejh\/status\/712711629981499392\/photo\/1",
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/FAyMPs7Vmv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeQPPxLXEAEl9uc.jpg",
      "id_str" : "712711404604821505",
      "id" : 712711404604821505,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeQPPxLXEAEl9uc.jpg",
      "sizes" : [ {
        "h" : 758,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 430,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1067,
        "resize" : "fit",
        "w" : 1689
      }, {
        "h" : 1067,
        "resize" : "fit",
        "w" : 1689
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FAyMPs7Vmv"
    } ],
    "hashtags" : [ {
      "text" : "java",
      "indices" : [ 62, 67 ]
    }, {
      "text" : "python",
      "indices" : [ 68, 75 ]
    }, {
      "text" : "coding",
      "indices" : [ 76, 83 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712967807797997568",
  "text" : "RT @moorejh: Comparing programming languages. HT @gracielagon #java #python #coding https:\/\/t.co\/FAyMPs7Vmv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Graciela Gonzalez",
        "screen_name" : "gracielagon",
        "indices" : [ 36, 48 ],
        "id_str" : "54320754",
        "id" : 54320754
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/moorejh\/status\/712711629981499392\/photo\/1",
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/FAyMPs7Vmv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeQPPxLXEAEl9uc.jpg",
        "id_str" : "712711404604821505",
        "id" : 712711404604821505,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeQPPxLXEAEl9uc.jpg",
        "sizes" : [ {
          "h" : 758,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 430,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1067,
          "resize" : "fit",
          "w" : 1689
        }, {
          "h" : 1067,
          "resize" : "fit",
          "w" : 1689
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FAyMPs7Vmv"
      } ],
      "hashtags" : [ {
        "text" : "java",
        "indices" : [ 49, 54 ]
      }, {
        "text" : "python",
        "indices" : [ 55, 62 ]
      }, {
        "text" : "coding",
        "indices" : [ 63, 70 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "712711629981499392",
    "text" : "Comparing programming languages. HT @gracielagon #java #python #coding https:\/\/t.co\/FAyMPs7Vmv",
    "id" : 712711629981499392,
    "created_at" : "2016-03-23 18:44:28 +0000",
    "user" : {
      "name" : "Jason H. Moore, PhD",
      "screen_name" : "moorejh",
      "protected" : false,
      "id_str" : "15496407",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/550024402142625793\/5aw9u9E8_normal.jpeg",
      "id" : 15496407,
      "verified" : false
    }
  },
  "id" : 712967807797997568,
  "created_at" : "2016-03-24 11:42:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "datagovsg",
      "screen_name" : "datagovsg",
      "indices" : [ 3, 13 ],
      "id_str" : "3739197619",
      "id" : 3739197619
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/datagovsg\/status\/712959161575669760\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/YuwuneD4bc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeTv-KpUsAEjlVV.jpg",
      "id_str" : "712958492319985665",
      "id" : 712958492319985665,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeTv-KpUsAEjlVV.jpg",
      "sizes" : [ {
        "h" : 1314,
        "resize" : "fit",
        "w" : 2250
      }, {
        "h" : 1196,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 701,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 397,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YuwuneD4bc"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/datagovsg\/status\/712959161575669760\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/YuwuneD4bc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeTwKHrUMAEqTRn.jpg",
      "id_str" : "712958697681465345",
      "id" : 712958697681465345,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeTwKHrUMAEqTRn.jpg",
      "sizes" : [ {
        "h" : 386,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 2254
      }, {
        "h" : 681,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1163,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YuwuneD4bc"
    } ],
    "hashtags" : [ {
      "text" : "SGBudget2016",
      "indices" : [ 58, 71 ]
    }, {
      "text" : "dataviz",
      "indices" : [ 72, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/oBPal9YSrL",
      "expanded_url" : "https:\/\/data.gov.sg\/dataset\/government-fiscal-position-annual",
      "display_url" : "data.gov.sg\/dataset\/govern\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712963306479296513",
  "text" : "RT @datagovsg: The latest Singapore budget data in charts #SGBudget2016 #dataviz https:\/\/t.co\/oBPal9YSrL https:\/\/t.co\/YuwuneD4bc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/datagovsg\/status\/712959161575669760\/photo\/1",
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/YuwuneD4bc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeTv-KpUsAEjlVV.jpg",
        "id_str" : "712958492319985665",
        "id" : 712958492319985665,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeTv-KpUsAEjlVV.jpg",
        "sizes" : [ {
          "h" : 1314,
          "resize" : "fit",
          "w" : 2250
        }, {
          "h" : 1196,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 701,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 397,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YuwuneD4bc"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/datagovsg\/status\/712959161575669760\/photo\/1",
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/YuwuneD4bc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeTwKHrUMAEqTRn.jpg",
        "id_str" : "712958697681465345",
        "id" : 712958697681465345,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeTwKHrUMAEqTRn.jpg",
        "sizes" : [ {
          "h" : 386,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 2254
        }, {
          "h" : 681,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1163,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YuwuneD4bc"
      } ],
      "hashtags" : [ {
        "text" : "SGBudget2016",
        "indices" : [ 43, 56 ]
      }, {
        "text" : "dataviz",
        "indices" : [ 57, 65 ]
      } ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/oBPal9YSrL",
        "expanded_url" : "https:\/\/data.gov.sg\/dataset\/government-fiscal-position-annual",
        "display_url" : "data.gov.sg\/dataset\/govern\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712959161575669760",
    "text" : "The latest Singapore budget data in charts #SGBudget2016 #dataviz https:\/\/t.co\/oBPal9YSrL https:\/\/t.co\/YuwuneD4bc",
    "id" : 712959161575669760,
    "created_at" : "2016-03-24 11:08:04 +0000",
    "user" : {
      "name" : "datagovsg",
      "screen_name" : "datagovsg",
      "protected" : false,
      "id_str" : "3739197619",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/651636753116192768\/UFNiGBq3_normal.png",
      "id" : 3739197619,
      "verified" : false
    }
  },
  "id" : 712963306479296513,
  "created_at" : "2016-03-24 11:24:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712957890613612544",
  "text" : "Got transfer 4 times on phone just to reach a department in a hospital.",
  "id" : 712957890613612544,
  "created_at" : "2016-03-24 11:03:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712949897142644736",
  "text" : "I suppose this Easter weekend break is not a good idea to meet up for coffee as everyone is out of town?",
  "id" : 712949897142644736,
  "created_at" : "2016-03-24 10:31:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712917007595933698",
  "text" : "RT @yapphenghui: Lee Kuan Yew appears in sky, tells S\u2019poreans paying tribute to him to go back to work as economy stalling at 2% per  https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/sfGhqBuYhR",
        "expanded_url" : "http:\/\/newnation.sg\/2016\/03\/lee-kuan-yew-appears-in-sky-tells-sporeans-paying-tribute-to-him-to-go-back-to-work-as-economy-stalling-at-2-per-annum\/",
        "display_url" : "newnation.sg\/2016\/03\/lee-ku\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712861227836379136",
    "text" : "Lee Kuan Yew appears in sky, tells S\u2019poreans paying tribute to him to go back to work as economy stalling at 2% per  https:\/\/t.co\/sfGhqBuYhR",
    "id" : 712861227836379136,
    "created_at" : "2016-03-24 04:38:55 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 712917007595933698,
  "created_at" : "2016-03-24 08:20:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 29 ],
      "url" : "https:\/\/t.co\/Tj9jnpRg35",
      "expanded_url" : "https:\/\/twitter.com\/feliciasonmez\/status\/712893653660876800",
      "display_url" : "twitter.com\/feliciasonmez\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712916374969040896",
  "text" : "LoL.  https:\/\/t.co\/Tj9jnpRg35",
  "id" : 712916374969040896,
  "created_at" : "2016-03-24 08:18:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 3, 12 ],
      "id_str" : "19338425",
      "id" : 19338425
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/DqejgarfPW",
      "expanded_url" : "https:\/\/twitter.com\/HillaryClinton\/status\/712758090614046720",
      "display_url" : "twitter.com\/HillaryClinton\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712777467178663936",
  "text" : "RT @Fondacey: You would think in a country that worships individual freedoms &amp; rights, this would be a no-brainer. https:\/\/t.co\/DqejgarfPW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/DqejgarfPW",
        "expanded_url" : "https:\/\/twitter.com\/HillaryClinton\/status\/712758090614046720",
        "display_url" : "twitter.com\/HillaryClinton\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712758357438959616",
    "text" : "You would think in a country that worships individual freedoms &amp; rights, this would be a no-brainer. https:\/\/t.co\/DqejgarfPW",
    "id" : 712758357438959616,
    "created_at" : "2016-03-23 21:50:09 +0000",
    "user" : {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "protected" : false,
      "id_str" : "19338425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3157014212\/d431af4ae6cd671dfa76a92f7ceceee4_normal.jpeg",
      "id" : 19338425,
      "verified" : false
    }
  },
  "id" : 712777467178663936,
  "created_at" : "2016-03-23 23:06:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LeapCard",
      "screen_name" : "LeapCard",
      "indices" : [ 3, 12 ],
      "id_str" : "301404575",
      "id" : 301404575
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland2016",
      "indices" : [ 70, 82 ]
    }, {
      "text" : "win",
      "indices" : [ 87, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/m5QauADANd",
      "expanded_url" : "http:\/\/bit.ly\/1oY8tF3",
      "display_url" : "bit.ly\/1oY8tF3"
    } ]
  },
  "geo" : { },
  "id_str" : "712632028475367425",
  "text" : "RT @LeapCard: We have 10 Leap Family Cards to give away, to celebrate #Ireland2016 ,to #win follow us &amp; RT https:\/\/t.co\/m5QauADANd https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LeapCard\/status\/712629555903770625\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/JoJFvTQhTe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CePEzc4WsAA9gCZ.jpg",
        "id_str" : "712629554259603456",
        "id" : 712629554259603456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CePEzc4WsAA9gCZ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 375,
          "resize" : "fit",
          "w" : 435
        }, {
          "h" : 375,
          "resize" : "fit",
          "w" : 435
        }, {
          "h" : 375,
          "resize" : "fit",
          "w" : 435
        }, {
          "h" : 375,
          "resize" : "fit",
          "w" : 435
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JoJFvTQhTe"
      } ],
      "hashtags" : [ {
        "text" : "Ireland2016",
        "indices" : [ 56, 68 ]
      }, {
        "text" : "win",
        "indices" : [ 73, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/m5QauADANd",
        "expanded_url" : "http:\/\/bit.ly\/1oY8tF3",
        "display_url" : "bit.ly\/1oY8tF3"
      } ]
    },
    "geo" : { },
    "id_str" : "712629555903770625",
    "text" : "We have 10 Leap Family Cards to give away, to celebrate #Ireland2016 ,to #win follow us &amp; RT https:\/\/t.co\/m5QauADANd https:\/\/t.co\/JoJFvTQhTe",
    "id" : 712629555903770625,
    "created_at" : "2016-03-23 13:18:20 +0000",
    "user" : {
      "name" : "LeapCard",
      "screen_name" : "LeapCard",
      "protected" : false,
      "id_str" : "301404575",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/999316153812975619\/y4oV81FO_normal.jpg",
      "id" : 301404575,
      "verified" : false
    }
  },
  "id" : 712632028475367425,
  "created_at" : "2016-03-23 13:28:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712574568838406144",
  "text" : "Among all the customers of your business, how do you know which are likely to respond to a given offer?",
  "id" : 712574568838406144,
  "created_at" : "2016-03-23 09:39:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ETI Evaluation Team",
      "screen_name" : "ETI_org",
      "indices" : [ 3, 11 ],
      "id_str" : "2190911335",
      "id" : 2190911335
    }, {
      "name" : "ASA",
      "screen_name" : "AmstatNews",
      "indices" : [ 91, 102 ],
      "id_str" : "94111457",
      "id" : 94111457
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "eval",
      "indices" : [ 69, 74 ]
    }, {
      "text" : "Stats",
      "indices" : [ 75, 81 ]
    }, {
      "text" : "pvalues",
      "indices" : [ 82, 90 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/xAhyvBc5yl",
      "expanded_url" : "http:\/\/goo.gl\/kDYFz7",
      "display_url" : "goo.gl\/kDYFz7"
    } ]
  },
  "geo" : { },
  "id_str" : "712573241735385088",
  "text" : "RT @ETI_org: P isn't perfect, evaluators let\u2019s pay attention to no.3 #eval #Stats #pvalues @AmstatNews https:\/\/t.co\/xAhyvBc5yl https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "ASA",
        "screen_name" : "AmstatNews",
        "indices" : [ 78, 89 ],
        "id_str" : "94111457",
        "id" : 94111457
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ETI_org\/status\/707714663853588480\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/IM396lBdYU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdJOvIPUUAA7NI5.jpg",
        "id_str" : "707714663023136768",
        "id" : 707714663023136768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdJOvIPUUAA7NI5.jpg",
        "sizes" : [ {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IM396lBdYU"
      } ],
      "hashtags" : [ {
        "text" : "eval",
        "indices" : [ 56, 61 ]
      }, {
        "text" : "Stats",
        "indices" : [ 62, 68 ]
      }, {
        "text" : "pvalues",
        "indices" : [ 69, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/xAhyvBc5yl",
        "expanded_url" : "http:\/\/goo.gl\/kDYFz7",
        "display_url" : "goo.gl\/kDYFz7"
      } ]
    },
    "geo" : { },
    "id_str" : "707714663853588480",
    "text" : "P isn't perfect, evaluators let\u2019s pay attention to no.3 #eval #Stats #pvalues @AmstatNews https:\/\/t.co\/xAhyvBc5yl https:\/\/t.co\/IM396lBdYU",
    "id" : 707714663853588480,
    "created_at" : "2016-03-09 23:48:19 +0000",
    "user" : {
      "name" : "ETI Evaluation Team",
      "screen_name" : "ETI_org",
      "protected" : false,
      "id_str" : "2190911335",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/702980589066911744\/YDG1EXlS_normal.jpg",
      "id" : 2190911335,
      "verified" : false
    }
  },
  "id" : 712573241735385088,
  "created_at" : "2016-03-23 09:34:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712572766600478720",
  "text" : "When the control wins in an A\/B test, how do you report your results?",
  "id" : 712572766600478720,
  "created_at" : "2016-03-23 09:32:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/VabcDjX174",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BDSewjZBZFp\/",
      "display_url" : "instagram.com\/p\/BDSewjZBZFp\/"
    } ]
  },
  "geo" : { },
  "id_str" : "712555872648622080",
  "text" : "Good Morning. Breakfast of the day. https:\/\/t.co\/VabcDjX174",
  "id" : 712555872648622080,
  "created_at" : "2016-03-23 08:25:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dzirhan Mahadzir",
      "screen_name" : "DzirhanDefence",
      "indices" : [ 3, 18 ],
      "id_str" : "132397046",
      "id" : 132397046
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/rfQD2O5fd8",
      "expanded_url" : "http:\/\/www.janes.com\/article\/58959\/rsaf-forms-second-local-f-15sg-squadron#.VvI43DaMreA.twitter",
      "display_url" : "janes.com\/article\/58959\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712529417105502208",
  "text" : "RT @DzirhanDefence: RSAF forms second local F-15SG squadron | IHS Jane's 360 https:\/\/t.co\/rfQD2O5fd8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/rfQD2O5fd8",
        "expanded_url" : "http:\/\/www.janes.com\/article\/58959\/rsaf-forms-second-local-f-15sg-squadron#.VvI43DaMreA.twitter",
        "display_url" : "janes.com\/article\/58959\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712527830614204416",
    "text" : "RSAF forms second local F-15SG squadron | IHS Jane's 360 https:\/\/t.co\/rfQD2O5fd8",
    "id" : 712527830614204416,
    "created_at" : "2016-03-23 06:34:07 +0000",
    "user" : {
      "name" : "Dzirhan Mahadzir",
      "screen_name" : "DzirhanDefence",
      "protected" : false,
      "id_str" : "132397046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3584472112\/32ac3860e23b3355ef65ff00f71ba855_normal.jpeg",
      "id" : 132397046,
      "verified" : false
    }
  },
  "id" : 712529417105502208,
  "created_at" : "2016-03-23 06:40:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/RMO75YIfH5",
      "expanded_url" : "https:\/\/books.google.com\/ngrams\/graph?content=coffee&year_start=1500&year_end=2000&corpus=15&smoothing=3&share=&direct_url=t1%3B%2Ccoffee%3B%2Cc0",
      "display_url" : "books.google.com\/ngrams\/graph?c\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712527643636314113",
  "text" : "Coffee that fueled the Age of Enlightenment.\n https:\/\/t.co\/RMO75YIfH5",
  "id" : 712527643636314113,
  "created_at" : "2016-03-23 06:33:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "zellie",
      "screen_name" : "zellieimani",
      "indices" : [ 3, 15 ],
      "id_str" : "325076723",
      "id" : 325076723
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712425073412542465",
  "text" : "RT @zellieimani: We are all France. We are all Belgium. We are never Nigeria. Never Palestine or Lebanon. Ivory Coast or Burkina Faso.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "712307190191824897",
    "geo" : { },
    "id_str" : "712379484964593666",
    "in_reply_to_user_id" : 325076723,
    "text" : "We are all France. We are all Belgium. We are never Nigeria. Never Palestine or Lebanon. Ivory Coast or Burkina Faso.",
    "id" : 712379484964593666,
    "in_reply_to_status_id" : 712307190191824897,
    "created_at" : "2016-03-22 20:44:39 +0000",
    "in_reply_to_screen_name" : "zellieimani",
    "in_reply_to_user_id_str" : "325076723",
    "user" : {
      "name" : "zellie",
      "screen_name" : "zellieimani",
      "protected" : false,
      "id_str" : "325076723",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000179362439\/90f034432122f61df63e471932fdfc62_normal.jpeg",
      "id" : 325076723,
      "verified" : true
    }
  },
  "id" : 712425073412542465,
  "created_at" : "2016-03-22 23:45:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LensCulture",
      "screen_name" : "lensculture",
      "indices" : [ 3, 15 ],
      "id_str" : "23179472",
      "id" : 23179472
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WorldWaterDay",
      "indices" : [ 17, 31 ]
    }, {
      "text" : "Photos",
      "indices" : [ 67, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/GsV0soFbkU",
      "expanded_url" : "http:\/\/bit.ly\/15-water",
      "display_url" : "bit.ly\/15-water"
    } ]
  },
  "geo" : { },
  "id_str" : "712389720878227456",
  "text" : "RT @lensculture: #WorldWaterDay 15 Global Perspectives on Water in #Photos \u2014 Selected by the editors of LensCulture https:\/\/t.co\/GsV0soFbkU\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WorldWaterDay",
        "indices" : [ 0, 14 ]
      }, {
        "text" : "Photos",
        "indices" : [ 50, 57 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/GsV0soFbkU",
        "expanded_url" : "http:\/\/bit.ly\/15-water",
        "display_url" : "bit.ly\/15-water"
      } ]
    },
    "geo" : { },
    "id_str" : "712338072336261122",
    "text" : "#WorldWaterDay 15 Global Perspectives on Water in #Photos \u2014 Selected by the editors of LensCulture https:\/\/t.co\/GsV0soFbkU Pls read + RT",
    "id" : 712338072336261122,
    "created_at" : "2016-03-22 18:00:05 +0000",
    "user" : {
      "name" : "LensCulture",
      "screen_name" : "lensculture",
      "protected" : false,
      "id_str" : "23179472",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877172804625608705\/hZX_mk4a_normal.jpg",
      "id" : 23179472,
      "verified" : false
    }
  },
  "id" : 712389720878227456,
  "created_at" : "2016-03-22 21:25:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Carter",
      "screen_name" : "withoutdoing",
      "indices" : [ 3, 16 ],
      "id_str" : "435020266",
      "id" : 435020266
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712294122539913216",
  "text" : "RT @withoutdoing: The \"grassland on your head\" thing references the idea of \"wearing a green hat\" - \u6234\u7EFF\u5E3D\u5B50 - which is Chinese slang for being\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "712283630341464065",
    "text" : "The \"grassland on your head\" thing references the idea of \"wearing a green hat\" - \u6234\u7EFF\u5E3D\u5B50 - which is Chinese slang for being cheated on",
    "id" : 712283630341464065,
    "created_at" : "2016-03-22 14:23:45 +0000",
    "user" : {
      "name" : "Liz Carter",
      "screen_name" : "withoutdoing",
      "protected" : false,
      "id_str" : "435020266",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/600731240812032001\/Z4afrP4k_normal.jpg",
      "id" : 435020266,
      "verified" : true
    }
  },
  "id" : 712294122539913216,
  "created_at" : "2016-03-22 15:05:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Open Data Manchester",
      "screen_name" : "opendatamcr",
      "indices" : [ 3, 15 ],
      "id_str" : "149120416",
      "id" : 149120416
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "open",
      "indices" : [ 114, 119 ]
    }, {
      "text" : "free",
      "indices" : [ 124, 129 ]
    }, {
      "text" : "opendata",
      "indices" : [ 130, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/Ub5F27Q8GN",
      "expanded_url" : "https:\/\/www.eventbrite.co.uk\/e\/open-policy-making-data-ethics-and-practice-tickets-22625680018",
      "display_url" : "eventbrite.co.uk\/e\/open-policy-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712293523194830850",
  "text" : "RT @opendatamcr: Open Policy Making - data, ethics and practice Tues 26th April. https:\/\/t.co\/Ub5F27Q8GN Event is #open and #free #opendata\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "open",
        "indices" : [ 97, 102 ]
      }, {
        "text" : "free",
        "indices" : [ 107, 112 ]
      }, {
        "text" : "opendata",
        "indices" : [ 113, 122 ]
      }, {
        "text" : "openpolicy",
        "indices" : [ 123, 134 ]
      } ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/Ub5F27Q8GN",
        "expanded_url" : "https:\/\/www.eventbrite.co.uk\/e\/open-policy-making-data-ethics-and-practice-tickets-22625680018",
        "display_url" : "eventbrite.co.uk\/e\/open-policy-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712285850671964160",
    "text" : "Open Policy Making - data, ethics and practice Tues 26th April. https:\/\/t.co\/Ub5F27Q8GN Event is #open and #free #opendata #openpolicy",
    "id" : 712285850671964160,
    "created_at" : "2016-03-22 14:32:35 +0000",
    "user" : {
      "name" : "Open Data Manchester",
      "screen_name" : "opendatamcr",
      "protected" : false,
      "id_str" : "149120416",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034345641906982912\/Dnp7Lzr1_normal.jpg",
      "id" : 149120416,
      "verified" : false
    }
  },
  "id" : 712293523194830850,
  "created_at" : "2016-03-22 15:03:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pratiik Rege",
      "screen_name" : "PratiikRege",
      "indices" : [ 3, 15 ],
      "id_str" : "201323928",
      "id" : 201323928
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WorldWaterDay",
      "indices" : [ 22, 36 ]
    }, {
      "text" : "ThirstForPurity",
      "indices" : [ 85, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712201204265316352",
  "text" : "RT @PratiikRege: This #WorldWaterDay, pledge to save water and thus save our future! #ThirstForPurity",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WorldWaterDay",
        "indices" : [ 5, 19 ]
      }, {
        "text" : "ThirstForPurity",
        "indices" : [ 68, 84 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "712185948340092928",
    "text" : "This #WorldWaterDay, pledge to save water and thus save our future! #ThirstForPurity",
    "id" : 712185948340092928,
    "created_at" : "2016-03-22 07:55:36 +0000",
    "user" : {
      "name" : "Pratiik Rege",
      "screen_name" : "PratiikRege",
      "protected" : false,
      "id_str" : "201323928",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/886742795678957568\/AgmkbfZ8_normal.jpg",
      "id" : 201323928,
      "verified" : false
    }
  },
  "id" : 712201204265316352,
  "created_at" : "2016-03-22 08:56:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "harishpillay",
      "screen_name" : "harishpillay",
      "indices" : [ 3, 16 ],
      "id_str" : "12486322",
      "id" : 12486322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/WhTJgwmMaS",
      "expanded_url" : "http:\/\/seekingalpha.com\/news\/3168839-former-intel-ceo-andy-grove-dies-79?uprof=45&dr=1#email_link",
      "display_url" : "seekingalpha.com\/news\/3168839-f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712195209833127937",
  "text" : "RT @harishpillay: RIP Andy Grove.  https:\/\/t.co\/WhTJgwmMaS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 17, 40 ],
        "url" : "https:\/\/t.co\/WhTJgwmMaS",
        "expanded_url" : "http:\/\/seekingalpha.com\/news\/3168839-former-intel-ceo-andy-grove-dies-79?uprof=45&dr=1#email_link",
        "display_url" : "seekingalpha.com\/news\/3168839-f\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "712191462574657536",
    "text" : "RIP Andy Grove.  https:\/\/t.co\/WhTJgwmMaS",
    "id" : 712191462574657536,
    "created_at" : "2016-03-22 08:17:31 +0000",
    "user" : {
      "name" : "harishpillay",
      "screen_name" : "harishpillay",
      "protected" : false,
      "id_str" : "12486322",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/99978402\/HarishPillaycloseupshot_normal.jpg",
      "id" : 12486322,
      "verified" : false
    }
  },
  "id" : 712195209833127937,
  "created_at" : "2016-03-22 08:32:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0639\u0631\u0648\u0636 \u0647\u0627\u064A\u0628\u0631 \u0628\u0646\u062F\u0647",
      "screen_name" : "DUB_Aviation",
      "indices" : [ 3, 16 ],
      "id_str" : "805165935833350144",
      "id" : 805165935833350144
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712194814293450753",
  "text" : "RT @DUB_Aviation: EI631 DUB-BRU has diverted to Amsterdam Schipol.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "712194096736108544",
    "text" : "EI631 DUB-BRU has diverted to Amsterdam Schipol.",
    "id" : 712194096736108544,
    "created_at" : "2016-03-22 08:27:59 +0000",
    "user" : {
      "name" : "Aviation Worldwide",
      "screen_name" : "WorldwideAvGeek",
      "protected" : false,
      "id_str" : "2945597584",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815918890920382465\/jhiulRvC_normal.jpg",
      "id" : 2945597584,
      "verified" : false
    }
  },
  "id" : 712194814293450753,
  "created_at" : "2016-03-22 08:30:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "Irish Foreign Ministry",
      "screen_name" : "dfatirl",
      "indices" : [ 52, 60 ],
      "id_str" : "364198635",
      "id" : 364198635
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/712184756515971072\/photo\/1",
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/xlbXYgcLTw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeIwQztUAAA0FkR.jpg",
      "id_str" : "712184756394262528",
      "id" : 712184756394262528,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeIwQztUAAA0FkR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 534,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 534,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 534,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 355,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xlbXYgcLTw"
    } ],
    "hashtags" : [ {
      "text" : "Brexit",
      "indices" : [ 44, 51 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712186712986935296",
  "text" : "RT @GeoffreyIRL: This should be interesting #Brexit @dfatirl https:\/\/t.co\/xlbXYgcLTw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Foreign Ministry",
        "screen_name" : "dfatirl",
        "indices" : [ 35, 43 ],
        "id_str" : "364198635",
        "id" : 364198635
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/712184756515971072\/photo\/1",
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/xlbXYgcLTw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeIwQztUAAA0FkR.jpg",
        "id_str" : "712184756394262528",
        "id" : 712184756394262528,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeIwQztUAAA0FkR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 534,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 534,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 534,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 355,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xlbXYgcLTw"
      } ],
      "hashtags" : [ {
        "text" : "Brexit",
        "indices" : [ 27, 34 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "712184756515971072",
    "text" : "This should be interesting #Brexit @dfatirl https:\/\/t.co\/xlbXYgcLTw",
    "id" : 712184756515971072,
    "created_at" : "2016-03-22 07:50:52 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 712186712986935296,
  "created_at" : "2016-03-22 07:58:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/Vun7rUWEIf",
      "expanded_url" : "http:\/\/www.casi.ie\/CASI_2016\/index.html",
      "display_url" : "casi.ie\/CASI_2016\/inde\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712184677054996480",
  "text" : "The Irish Statistical Associations national conference will be hosted in the Castletroy Park Hotel Limerick. https:\/\/t.co\/Vun7rUWEIf",
  "id" : 712184677054996480,
  "created_at" : "2016-03-22 07:50:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Al Jazeera News",
      "screen_name" : "AJENews",
      "indices" : [ 3, 11 ],
      "id_str" : "18424289",
      "id" : 18424289
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AJENews\/status\/712177702451724288\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/WxsSyAQ7wv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeIp2LpW0AABp3z.jpg",
      "id_str" : "712177701893885952",
      "id" : 712177701893885952,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeIp2LpW0AABp3z.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 225,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 225,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 225,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 225,
        "resize" : "fit",
        "w" : 400
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WxsSyAQ7wv"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/k2F7cxiV1x",
      "expanded_url" : "http:\/\/aje.io\/9w4b",
      "display_url" : "aje.io\/9w4b"
    } ]
  },
  "geo" : { },
  "id_str" : "712178924248289280",
  "text" : "RT @AJENews: Two explosions have been heard at Brussels Airport - Belgian media. More soon: https:\/\/t.co\/k2F7cxiV1x https:\/\/t.co\/WxsSyAQ7wv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AJENews\/status\/712177702451724288\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/WxsSyAQ7wv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeIp2LpW0AABp3z.jpg",
        "id_str" : "712177701893885952",
        "id" : 712177701893885952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeIp2LpW0AABp3z.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 225,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 225,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 225,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 225,
          "resize" : "fit",
          "w" : 400
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WxsSyAQ7wv"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/k2F7cxiV1x",
        "expanded_url" : "http:\/\/aje.io\/9w4b",
        "display_url" : "aje.io\/9w4b"
      } ]
    },
    "geo" : { },
    "id_str" : "712177702451724288",
    "text" : "Two explosions have been heard at Brussels Airport - Belgian media. More soon: https:\/\/t.co\/k2F7cxiV1x https:\/\/t.co\/WxsSyAQ7wv",
    "id" : 712177702451724288,
    "created_at" : "2016-03-22 07:22:50 +0000",
    "user" : {
      "name" : "Al Jazeera News",
      "screen_name" : "AJENews",
      "protected" : false,
      "id_str" : "18424289",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875639380781563906\/b1J1YbIE_normal.jpg",
      "id" : 18424289,
      "verified" : true
    }
  },
  "id" : 712178924248289280,
  "created_at" : "2016-03-22 07:27:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The OR Society",
      "screen_name" : "TheORSociety",
      "indices" : [ 3, 16 ],
      "id_str" : "124413684",
      "id" : 124413684
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Analytics",
      "indices" : [ 45, 55 ]
    }, {
      "text" : "ThisisOR",
      "indices" : [ 101, 110 ]
    } ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/s0wIwhR2Kw",
      "expanded_url" : "http:\/\/www.theorsociety.com\/analytics-summit",
      "display_url" : "theorsociety.com\/analytics-summ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "712171923099283456",
  "text" : "RT @TheORSociety: Book your place at our 5th #Analytics Summit\n21 June 2016 Tavistock Square, London #ThisisOR\nhttps:\/\/t.co\/s0wIwhR2Kw http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheORSociety\/status\/705684302206517249\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/Y6V1iJ3rL6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CcsYIhmXEAIILTg.jpg",
        "id_str" : "705684301350899714",
        "id" : 705684301350899714,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcsYIhmXEAIILTg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1210,
          "resize" : "fit",
          "w" : 1186
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1176
        }, {
          "h" : 1210,
          "resize" : "fit",
          "w" : 1186
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 667
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Y6V1iJ3rL6"
      } ],
      "hashtags" : [ {
        "text" : "Analytics",
        "indices" : [ 27, 37 ]
      }, {
        "text" : "ThisisOR",
        "indices" : [ 83, 92 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/s0wIwhR2Kw",
        "expanded_url" : "http:\/\/www.theorsociety.com\/analytics-summit",
        "display_url" : "theorsociety.com\/analytics-summ\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705684302206517249",
    "text" : "Book your place at our 5th #Analytics Summit\n21 June 2016 Tavistock Square, London #ThisisOR\nhttps:\/\/t.co\/s0wIwhR2Kw https:\/\/t.co\/Y6V1iJ3rL6",
    "id" : 705684302206517249,
    "created_at" : "2016-03-04 09:20:23 +0000",
    "user" : {
      "name" : "The OR Society",
      "screen_name" : "TheORSociety",
      "protected" : false,
      "id_str" : "124413684",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/724948796833157120\/Q--o9dKa_normal.jpg",
      "id" : 124413684,
      "verified" : false
    }
  },
  "id" : 712171923099283456,
  "created_at" : "2016-03-22 06:59:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712054980040126464",
  "text" : "RT @Clearpreso: Do we eh... have a government yet?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "712053834634366976",
    "text" : "Do we eh... have a government yet?",
    "id" : 712053834634366976,
    "created_at" : "2016-03-21 23:10:38 +0000",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 712054980040126464,
  "created_at" : "2016-03-21 23:15:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "712044933172088832",
  "text" : "\"... the 4-inch iPhone SE, which is essentially an iPhone 5S with 6S specs and a new low price of $399.\"  That all I need to know",
  "id" : 712044933172088832,
  "created_at" : "2016-03-21 22:35:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/711977377262272512\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/PqoF4QXLod",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeFzpsfWwAAgKZu.jpg",
      "id_str" : "711977376255754240",
      "id" : 711977376255754240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeFzpsfWwAAgKZu.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 158,
        "resize" : "fit",
        "w" : 639
      }, {
        "h" : 158,
        "resize" : "fit",
        "w" : 639
      }, {
        "h" : 158,
        "resize" : "fit",
        "w" : 639
      }, {
        "h" : 158,
        "resize" : "fit",
        "w" : 639
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PqoF4QXLod"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/Ug02ZjWFDd",
      "expanded_url" : "http:\/\/www.independent.ie\/irish-news\/news\/were-not-a-charity-but-i-regret-how-it-was-handled-developer-behind-tyrrelstown-estate-34558525.html",
      "display_url" : "independent.ie\/irish-news\/new\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711977377262272512",
  "text" : "I agreed with this comment. https:\/\/t.co\/Ug02ZjWFDd. https:\/\/t.co\/PqoF4QXLod",
  "id" : 711977377262272512,
  "created_at" : "2016-03-21 18:06:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Teija Avila",
      "screen_name" : "TeijaRuo",
      "indices" : [ 0, 9 ],
      "id_str" : "3130789167",
      "id" : 3130789167
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "711910965097472000",
  "geo" : { },
  "id_str" : "711975981519515649",
  "in_reply_to_user_id" : 3130789167,
  "text" : "@TeijaRuo happy to connect with you too.",
  "id" : 711975981519515649,
  "in_reply_to_status_id" : 711910965097472000,
  "created_at" : "2016-03-21 18:01:16 +0000",
  "in_reply_to_screen_name" : "TeijaRuo",
  "in_reply_to_user_id_str" : "3130789167",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DataScienceJohn",
      "screen_name" : "DataSciJ",
      "indices" : [ 3, 12 ],
      "id_str" : "554378917",
      "id" : 554378917
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/DataSciJ\/status\/711909805435297792\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/xldUBiFcqf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeE2MgzXEAAjz5f.jpg",
      "id_str" : "711909804692934656",
      "id" : 711909804692934656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeE2MgzXEAAjz5f.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 277,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 326,
        "resize" : "fit",
        "w" : 799
      }, {
        "h" : 326,
        "resize" : "fit",
        "w" : 799
      }, {
        "h" : 326,
        "resize" : "fit",
        "w" : 799
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xldUBiFcqf"
    } ],
    "hashtags" : [ {
      "text" : "Python",
      "indices" : [ 80, 87 ]
    }, {
      "text" : "OpenData",
      "indices" : [ 88, 97 ]
    }, {
      "text" : "DataSci",
      "indices" : [ 98, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/ibYB323q24",
      "expanded_url" : "http:\/\/opendata.stackexchange.com\/questions\/1208\/a-python-guide-for-open-data-file-formats\/1210#1210",
      "display_url" : "opendata.stackexchange.com\/questions\/1208\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711910412720197632",
  "text" : "RT @DataSciJ: A Python guide for open data file formats https:\/\/t.co\/ibYB323q24 #Python #OpenData #DataSci https:\/\/t.co\/xldUBiFcqf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DataSciJ\/status\/711909805435297792\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/xldUBiFcqf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeE2MgzXEAAjz5f.jpg",
        "id_str" : "711909804692934656",
        "id" : 711909804692934656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeE2MgzXEAAjz5f.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 277,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 799
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 799
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 799
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xldUBiFcqf"
      } ],
      "hashtags" : [ {
        "text" : "Python",
        "indices" : [ 66, 73 ]
      }, {
        "text" : "OpenData",
        "indices" : [ 74, 83 ]
      }, {
        "text" : "DataSci",
        "indices" : [ 84, 92 ]
      } ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/ibYB323q24",
        "expanded_url" : "http:\/\/opendata.stackexchange.com\/questions\/1208\/a-python-guide-for-open-data-file-formats\/1210#1210",
        "display_url" : "opendata.stackexchange.com\/questions\/1208\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711909805435297792",
    "text" : "A Python guide for open data file formats https:\/\/t.co\/ibYB323q24 #Python #OpenData #DataSci https:\/\/t.co\/xldUBiFcqf",
    "id" : 711909805435297792,
    "created_at" : "2016-03-21 13:38:18 +0000",
    "user" : {
      "name" : "DataScienceJohn",
      "screen_name" : "DataSciJ",
      "protected" : false,
      "id_str" : "554378917",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2127308160\/photo12_normal.jpg",
      "id" : 554378917,
      "verified" : false
    }
  },
  "id" : 711910412720197632,
  "created_at" : "2016-03-21 13:40:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/vU04cz5d6n",
      "expanded_url" : "https:\/\/twitter.com\/Jason\/status\/711246694235512832",
      "display_url" : "twitter.com\/Jason\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711908362674442240",
  "text" : "Example of poor work ethics.  https:\/\/t.co\/vU04cz5d6n",
  "id" : 711908362674442240,
  "created_at" : "2016-03-21 13:32:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5B64\u519B\u672A\u6B7B",
      "screen_name" : "Scswga",
      "indices" : [ 3, 10 ],
      "id_str" : "257844864",
      "id" : 257844864
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711890045481967616",
  "text" : "RT @Scswga: \u4E00\u54E5\u4EEC\u4E70\u5F69\u7968\u4E2D\u4E86\u4E94\u767E\u4E07\uFF0C\u5BF9\u5979\u5973\u670B\u53CB\u8BF4\uFF1A\u201C\u6211\u4E2D\u4E86\u4E94\u767E\u4E07\uFF0C\u54B1\u4EEC\u5206\u4E86\u5427\uFF01\u201D\u5973\u53CB\u7279\u5E78\u798F\u7684\u70B9\u4E86\u70B9\u5934\u3002\n\n\u3000\u3000\u6CA1\u8FC7\u51E0\u5929\uFF0C\u5973\u7684\u6C14\u6124\u7684\u9A82\u9053\uFF1A\u201C\u72D7\u5A18\u517B\u7684\uFF0C\u8FD8\u4EE5\u4E3A\u4ED6\u8981\u548C\u6211\u5206\u94B1\u5462\uFF01\u201D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710695853866921986",
    "text" : "\u4E00\u54E5\u4EEC\u4E70\u5F69\u7968\u4E2D\u4E86\u4E94\u767E\u4E07\uFF0C\u5BF9\u5979\u5973\u670B\u53CB\u8BF4\uFF1A\u201C\u6211\u4E2D\u4E86\u4E94\u767E\u4E07\uFF0C\u54B1\u4EEC\u5206\u4E86\u5427\uFF01\u201D\u5973\u53CB\u7279\u5E78\u798F\u7684\u70B9\u4E86\u70B9\u5934\u3002\n\n\u3000\u3000\u6CA1\u8FC7\u51E0\u5929\uFF0C\u5973\u7684\u6C14\u6124\u7684\u9A82\u9053\uFF1A\u201C\u72D7\u5A18\u517B\u7684\uFF0C\u8FD8\u4EE5\u4E3A\u4ED6\u8981\u548C\u6211\u5206\u94B1\u5462\uFF01\u201D",
    "id" : 710695853866921986,
    "created_at" : "2016-03-18 05:14:30 +0000",
    "user" : {
      "name" : "\u5B64\u519B\u672A\u6B7B",
      "screen_name" : "Scswga",
      "protected" : false,
      "id_str" : "257844864",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1703168532\/Scswga_normal.jpg",
      "id" : 257844864,
      "verified" : false
    }
  },
  "id" : 711890045481967616,
  "created_at" : "2016-03-21 12:19:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711888415361527808",
  "text" : "Why it is I always crave for pasta when I wearing white...",
  "id" : 711888415361527808,
  "created_at" : "2016-03-21 12:13:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geraldine Herbert",
      "screen_name" : "GerHerbert1",
      "indices" : [ 3, 15 ],
      "id_str" : "462083773",
      "id" : 462083773
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711877836768985088",
  "text" : "RT @GerHerbert1: European new car registrations grew by 13.8% in February 2016, marking the 30th consecutive month of year-on-year growth.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "711877605746745345",
    "text" : "European new car registrations grew by 13.8% in February 2016, marking the 30th consecutive month of year-on-year growth.",
    "id" : 711877605746745345,
    "created_at" : "2016-03-21 11:30:21 +0000",
    "user" : {
      "name" : "Geraldine Herbert",
      "screen_name" : "GerHerbert1",
      "protected" : false,
      "id_str" : "462083773",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/492655680470282242\/Du5bcunu_normal.jpeg",
      "id" : 462083773,
      "verified" : false
    }
  },
  "id" : 711877836768985088,
  "created_at" : "2016-03-21 11:31:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/pXXXerC64s",
      "expanded_url" : "https:\/\/www.ted.com\/talks\/hans_and_ola_rosling_how_not_to_be_ignorant_about_the_world?language=en&utm_source=twitter.com&utm_medium=social&utm_campaign=tedspread",
      "display_url" : "ted.com\/talks\/hans_and\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711873966357540864",
  "text" : "How not to be ignorant about the world https:\/\/t.co\/pXXXerC64s",
  "id" : 711873966357540864,
  "created_at" : "2016-03-21 11:15:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philippe Legrain",
      "screen_name" : "plegrain",
      "indices" : [ 3, 12 ],
      "id_str" : "118680576",
      "id" : 118680576
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/6a6PHk7TXl",
      "expanded_url" : "http:\/\/capx.co\/external\/immigrants-founded-51-of\/",
      "display_url" : "capx.co\/external\/immig\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711873522302320640",
  "text" : "RT @plegrain: Immigrants founded more than half of the US's start-ups worth $1 billion or more https:\/\/t.co\/6a6PHk7TXl",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/6a6PHk7TXl",
        "expanded_url" : "http:\/\/capx.co\/external\/immigrants-founded-51-of\/",
        "display_url" : "capx.co\/external\/immig\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711869110523727873",
    "text" : "Immigrants founded more than half of the US's start-ups worth $1 billion or more https:\/\/t.co\/6a6PHk7TXl",
    "id" : 711869110523727873,
    "created_at" : "2016-03-21 10:56:36 +0000",
    "user" : {
      "name" : "Philippe Legrain",
      "screen_name" : "plegrain",
      "protected" : false,
      "id_str" : "118680576",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882952723901149184\/NWu-Czta_normal.jpg",
      "id" : 118680576,
      "verified" : false
    }
  },
  "id" : 711873522302320640,
  "created_at" : "2016-03-21 11:14:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/umfyEcLMk9",
      "expanded_url" : "https:\/\/twitter.com\/MargaretMolloy\/status\/711857725651722240",
      "display_url" : "twitter.com\/MargaretMolloy\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711858076710785024",
  "text" : "Same over here.  https:\/\/t.co\/umfyEcLMk9",
  "id" : 711858076710785024,
  "created_at" : "2016-03-21 10:12:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/Dxgnynedf5",
      "expanded_url" : "http:\/\/alexmuir.com\/facebook-is-the-new-excel",
      "display_url" : "alexmuir.com\/facebook-is-th\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711857904958218240",
  "text" : "Startup should include Facebook in its competitive analysis. It has already has 99% of your potential user base. https:\/\/t.co\/Dxgnynedf5",
  "id" : 711857904958218240,
  "created_at" : "2016-03-21 10:12:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Al Jazeera English",
      "screen_name" : "AJEnglish",
      "indices" : [ 3, 13 ],
      "id_str" : "4970411",
      "id" : 4970411
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/J8TpSCwaKP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2gq6XEAAhflA.jpg",
      "id_str" : "711839782259855360",
      "id" : 711839782259855360,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2gq6XEAAhflA.jpg",
      "sizes" : [ {
        "h" : 802,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 802,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 802,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/J8TpSCwaKP"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/J8TpSCwaKP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2gwFWoAAkpCV.jpg",
      "id_str" : "711839783648141312",
      "id" : 711839783648141312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2gwFWoAAkpCV.jpg",
      "sizes" : [ {
        "h" : 801,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 801,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 801,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/J8TpSCwaKP"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/J8TpSCwaKP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2g1eWwAERckg.jpg",
      "id_str" : "711839785095184385",
      "id" : 711839785095184385,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2g1eWwAERckg.jpg",
      "sizes" : [ {
        "h" : 802,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 802,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 802,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/J8TpSCwaKP"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/J8TpSCwaKP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2hF9WEAED284.jpg",
      "id_str" : "711839789520130049",
      "id" : 711839789520130049,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2hF9WEAED284.jpg",
      "sizes" : [ {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/J8TpSCwaKP"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/QmDgQJCmpp",
      "expanded_url" : "http:\/\/aje.io\/heax",
      "display_url" : "aje.io\/heax"
    } ]
  },
  "geo" : { },
  "id_str" : "711843555082428416",
  "text" : "RT @AJEnglish: In Pictures: Hungary's criminalised homeless struggle to survive https:\/\/t.co\/QmDgQJCmpp https:\/\/t.co\/J8TpSCwaKP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/J8TpSCwaKP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2gq6XEAAhflA.jpg",
        "id_str" : "711839782259855360",
        "id" : 711839782259855360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2gq6XEAAhflA.jpg",
        "sizes" : [ {
          "h" : 802,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 802,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 802,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J8TpSCwaKP"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/J8TpSCwaKP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2gwFWoAAkpCV.jpg",
        "id_str" : "711839783648141312",
        "id" : 711839783648141312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2gwFWoAAkpCV.jpg",
        "sizes" : [ {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J8TpSCwaKP"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/J8TpSCwaKP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2g1eWwAERckg.jpg",
        "id_str" : "711839785095184385",
        "id" : 711839785095184385,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2g1eWwAERckg.jpg",
        "sizes" : [ {
          "h" : 802,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 802,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 802,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J8TpSCwaKP"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AJEnglish\/status\/711839790094794752\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/J8TpSCwaKP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeD2hF9WEAED284.jpg",
        "id_str" : "711839789520130049",
        "id" : 711839789520130049,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeD2hF9WEAED284.jpg",
        "sizes" : [ {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J8TpSCwaKP"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 65, 88 ],
        "url" : "https:\/\/t.co\/QmDgQJCmpp",
        "expanded_url" : "http:\/\/aje.io\/heax",
        "display_url" : "aje.io\/heax"
      } ]
    },
    "geo" : { },
    "id_str" : "711839790094794752",
    "text" : "In Pictures: Hungary's criminalised homeless struggle to survive https:\/\/t.co\/QmDgQJCmpp https:\/\/t.co\/J8TpSCwaKP",
    "id" : 711839790094794752,
    "created_at" : "2016-03-21 09:00:05 +0000",
    "user" : {
      "name" : "Al Jazeera English",
      "screen_name" : "AJEnglish",
      "protected" : false,
      "id_str" : "4970411",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875638617606987776\/YBOKib96_normal.jpg",
      "id" : 4970411,
      "verified" : true
    }
  },
  "id" : 711843555082428416,
  "created_at" : "2016-03-21 09:15:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/HeKH2uKYY7",
      "expanded_url" : "https:\/\/twitter.com\/astro_tim\/status\/711276897133596672",
      "display_url" : "twitter.com\/astro_tim\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711840071851384833",
  "text" : "RT @mrbrown: Our beautiful home. https:\/\/t.co\/HeKH2uKYY7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 20, 43 ],
        "url" : "https:\/\/t.co\/HeKH2uKYY7",
        "expanded_url" : "https:\/\/twitter.com\/astro_tim\/status\/711276897133596672",
        "display_url" : "twitter.com\/astro_tim\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711832568950206464",
    "text" : "Our beautiful home. https:\/\/t.co\/HeKH2uKYY7",
    "id" : 711832568950206464,
    "created_at" : "2016-03-21 08:31:24 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 711840071851384833,
  "created_at" : "2016-03-21 09:01:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IrishCycle.com",
      "screen_name" : "IrishCycle",
      "indices" : [ 3, 14 ],
      "id_str" : "2172961105",
      "id" : 2172961105
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IrishCycle\/status\/711832575392677888\/photo\/1",
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/NBX0AQAzdV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeDv9F9UMAAbw86.jpg",
      "id_str" : "711832573974949888",
      "id" : 711832573974949888,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeDv9F9UMAAbw86.jpg",
      "sizes" : [ {
        "h" : 708,
        "resize" : "fit",
        "w" : 944
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 708,
        "resize" : "fit",
        "w" : 944
      }, {
        "h" : 708,
        "resize" : "fit",
        "w" : 944
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NBX0AQAzdV"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/UmjR5Fbw1Q",
      "expanded_url" : "http:\/\/irishcycle.com\/2016\/03\/21\/popular-dublin-city-cycle-route-widened-and-bicycle-counter-replaced\/",
      "display_url" : "irishcycle.com\/2016\/03\/21\/pop\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711839680577282048",
  "text" : "RT @IrishCycle: Popular Dublin City cycle route widened and bicycle counter\u00A0replaced https:\/\/t.co\/UmjR5Fbw1Q https:\/\/t.co\/NBX0AQAzdV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishCycle\/status\/711832575392677888\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/NBX0AQAzdV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeDv9F9UMAAbw86.jpg",
        "id_str" : "711832573974949888",
        "id" : 711832573974949888,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeDv9F9UMAAbw86.jpg",
        "sizes" : [ {
          "h" : 708,
          "resize" : "fit",
          "w" : 944
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 708,
          "resize" : "fit",
          "w" : 944
        }, {
          "h" : 708,
          "resize" : "fit",
          "w" : 944
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NBX0AQAzdV"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/UmjR5Fbw1Q",
        "expanded_url" : "http:\/\/irishcycle.com\/2016\/03\/21\/popular-dublin-city-cycle-route-widened-and-bicycle-counter-replaced\/",
        "display_url" : "irishcycle.com\/2016\/03\/21\/pop\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711832575392677888",
    "text" : "Popular Dublin City cycle route widened and bicycle counter\u00A0replaced https:\/\/t.co\/UmjR5Fbw1Q https:\/\/t.co\/NBX0AQAzdV",
    "id" : 711832575392677888,
    "created_at" : "2016-03-21 08:31:25 +0000",
    "user" : {
      "name" : "IrishCycle.com",
      "screen_name" : "IrishCycle",
      "protected" : false,
      "id_str" : "2172961105",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/623550850162688001\/WkNSyheU_normal.jpg",
      "id" : 2172961105,
      "verified" : false
    }
  },
  "id" : 711839680577282048,
  "created_at" : "2016-03-21 08:59:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dam\u00EFe\u00F1 M\u00FAlley \u00B8",
      "screen_name" : "damienmulley",
      "indices" : [ 3, 16 ],
      "id_str" : "193753",
      "id" : 193753
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711683151362400257",
  "text" : "RT @damienmulley: \"I have a big ass, I'll become a Kardashian\" is same logic when someone that has a large Twitter following becomes a soci\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710772345963724800",
    "text" : "\"I have a big ass, I'll become a Kardashian\" is same logic when someone that has a large Twitter following becomes a social media consultant",
    "id" : 710772345963724800,
    "created_at" : "2016-03-18 10:18:27 +0000",
    "user" : {
      "name" : "Dam\u00EFe\u00F1 M\u00FAlley \u00B8",
      "screen_name" : "damienmulley",
      "protected" : false,
      "id_str" : "193753",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004142705511944193\/y_8rA5rW_normal.jpg",
      "id" : 193753,
      "verified" : false
    }
  },
  "id" : 711683151362400257,
  "created_at" : "2016-03-20 22:37:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Gray",
      "screen_name" : "davegray",
      "indices" : [ 3, 12 ],
      "id_str" : "456",
      "id" : 456
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/davegray\/status\/711618462943039488\/photo\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/Qe9JAlFKIM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CeAtNlxUAAE2-A_.jpg",
      "id_str" : "711618452625031169",
      "id" : 711618452625031169,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeAtNlxUAAE2-A_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1247,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1247,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 731,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Qe9JAlFKIM"
    } ],
    "hashtags" : [ {
      "text" : "visualthinking",
      "indices" : [ 70, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711642782419587072",
  "text" : "RT @davegray: Drawing is understanding, and understanding is drawing. #visualthinking https:\/\/t.co\/Qe9JAlFKIM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/davegray\/status\/711618462943039488\/photo\/1",
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/Qe9JAlFKIM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CeAtNlxUAAE2-A_.jpg",
        "id_str" : "711618452625031169",
        "id" : 711618452625031169,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CeAtNlxUAAE2-A_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1247,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1247,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 731,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Qe9JAlFKIM"
      } ],
      "hashtags" : [ {
        "text" : "visualthinking",
        "indices" : [ 56, 71 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "711618462943039488",
    "text" : "Drawing is understanding, and understanding is drawing. #visualthinking https:\/\/t.co\/Qe9JAlFKIM",
    "id" : 711618462943039488,
    "created_at" : "2016-03-20 18:20:37 +0000",
    "user" : {
      "name" : "Dave Gray",
      "screen_name" : "davegray",
      "protected" : false,
      "id_str" : "456",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/980859430676787200\/ZFRNgPfu_normal.jpg",
      "id" : 456,
      "verified" : false
    }
  },
  "id" : 711642782419587072,
  "created_at" : "2016-03-20 19:57:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 0, 7 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/0VRoXkXQ12",
      "expanded_url" : "https:\/\/www.dropbox.com\/s\/52a493ksxy53u5c\/Course_Survey.csv?dl=0",
      "display_url" : "dropbox.com\/s\/52a493ksxy53\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711534957932507136",
  "text" : "#rstats Trying to convert 1, 2 and 3 under SitArea to Front, Middle and Back. How do I do it in R? Thanks https:\/\/t.co\/0VRoXkXQ12",
  "id" : 711534957932507136,
  "created_at" : "2016-03-20 12:48:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    }, {
      "name" : "Global Irish \u2618",
      "screen_name" : "GlobalIrish",
      "indices" : [ 112, 124 ],
      "id_str" : "1872593298",
      "id" : 1872593298
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 21, 35 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 46, 56 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711484424471384064",
  "text" : "RT @IrlEmbSingapore: #StPatricksDay parade in #Singapore! Well done to all participants and organisers!@dfatirl @GlobalIrish \uD83C\uDDEE\uD83C\uDDEA\uD83C\uDDF8\uD83C\uDDEC\u2618 https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Global Irish \u2618",
        "screen_name" : "GlobalIrish",
        "indices" : [ 91, 103 ],
        "id_str" : "1872593298",
        "id" : 1872593298
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/711470674083098624\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/X2F8bshM50",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cd-mwjvVIAAcNFu.jpg",
        "id_str" : "711470619305517056",
        "id" : 711470619305517056,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cd-mwjvVIAAcNFu.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/X2F8bshM50"
      } ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 0, 14 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 25, 35 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "711470674083098624",
    "text" : "#StPatricksDay parade in #Singapore! Well done to all participants and organisers!@dfatirl @GlobalIrish \uD83C\uDDEE\uD83C\uDDEA\uD83C\uDDF8\uD83C\uDDEC\u2618 https:\/\/t.co\/X2F8bshM50",
    "id" : 711470674083098624,
    "created_at" : "2016-03-20 08:33:21 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 711484424471384064,
  "created_at" : "2016-03-20 09:28:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/JMM8WJ7ElX",
      "expanded_url" : "http:\/\/fw.to\/02kqyTk",
      "display_url" : "fw.to\/02kqyTk"
    } ]
  },
  "geo" : { },
  "id_str" : "711360407320010752",
  "text" : "Singapore a 'prime target' for terrorist attack https:\/\/t.co\/JMM8WJ7ElX",
  "id" : 711360407320010752,
  "created_at" : "2016-03-20 01:15:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/GtbBCJYOLA",
      "expanded_url" : "https:\/\/twitter.com\/ladystormhold\/status\/711291567521337344",
      "display_url" : "twitter.com\/ladystormhold\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711299118598135808",
  "text" : "It not apple to apple comparison.  https:\/\/t.co\/GtbBCJYOLA",
  "id" : 711299118598135808,
  "created_at" : "2016-03-19 21:11:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Christian",
      "screen_name" : "dchristian5",
      "indices" : [ 3, 15 ],
      "id_str" : "192909995",
      "id" : 192909995
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/v8BDuOCXYh",
      "expanded_url" : "http:\/\/interestingengineering.com\/airport-in-india-is-the-first-in-the-world-to-run-100-on-solar-energy\/",
      "display_url" : "interestingengineering.com\/airport-in-ind\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711247134335639552",
  "text" : "RT @dchristian5: \"Airport in India is the first in the world to run 100% on solar energy\" https:\/\/t.co\/v8BDuOCXYh",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/v8BDuOCXYh",
        "expanded_url" : "http:\/\/interestingengineering.com\/airport-in-india-is-the-first-in-the-world-to-run-100-on-solar-energy\/",
        "display_url" : "interestingengineering.com\/airport-in-ind\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711238797393059840",
    "text" : "\"Airport in India is the first in the world to run 100% on solar energy\" https:\/\/t.co\/v8BDuOCXYh",
    "id" : 711238797393059840,
    "created_at" : "2016-03-19 17:11:58 +0000",
    "user" : {
      "name" : "Daniel Christian",
      "screen_name" : "dchristian5",
      "protected" : false,
      "id_str" : "192909995",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/862073591776595968\/6YciHVtd_normal.jpg",
      "id" : 192909995,
      "verified" : false
    }
  },
  "id" : 711247134335639552,
  "created_at" : "2016-03-19 17:45:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711223044946206720",
  "text" : "\u20AC5K - No wifi, no access to shower, cramped conditions https:\/\/www.belmond.hopic condiontcom\/venice-simplon-orient-express\/journeys\/4_167494",
  "id" : 711223044946206720,
  "created_at" : "2016-03-19 16:09:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    }, {
      "name" : "Vox",
      "screen_name" : "voxdotcom",
      "indices" : [ 99, 109 ],
      "id_str" : "2347049341",
      "id" : 2347049341
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/tt2bc64aIW",
      "expanded_url" : "http:\/\/www.vox.com\/2016\/3\/17\/11250962\/proof-evolution-vestigial?utm_campaign=vox&utm_content=feature%3Afixed&utm_medium=social&utm_source=twitter",
      "display_url" : "vox.com\/2016\/3\/17\/1125\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711205186413436928",
  "text" : "RT @yapphenghui: Proof of evolution that you can find on your own body https:\/\/t.co\/tt2bc64aIW via @voxdotcom",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Vox",
        "screen_name" : "voxdotcom",
        "indices" : [ 82, 92 ],
        "id_str" : "2347049341",
        "id" : 2347049341
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/tt2bc64aIW",
        "expanded_url" : "http:\/\/www.vox.com\/2016\/3\/17\/11250962\/proof-evolution-vestigial?utm_campaign=vox&utm_content=feature%3Afixed&utm_medium=social&utm_source=twitter",
        "display_url" : "vox.com\/2016\/3\/17\/1125\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711126817239748610",
    "text" : "Proof of evolution that you can find on your own body https:\/\/t.co\/tt2bc64aIW via @voxdotcom",
    "id" : 711126817239748610,
    "created_at" : "2016-03-19 09:46:59 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 711205186413436928,
  "created_at" : "2016-03-19 14:58:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/0RH60catj8",
      "expanded_url" : "https:\/\/twitter.com\/ezraklein\/status\/710500912528277504",
      "display_url" : "twitter.com\/ezraklein\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711204157621977089",
  "text" : "A breath of fresh air.  https:\/\/t.co\/0RH60catj8",
  "id" : 711204157621977089,
  "created_at" : "2016-03-19 14:54:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "indices" : [ 3, 13 ],
      "id_str" : "77901568",
      "id" : 77901568
    }, {
      "name" : "Creme Global",
      "screen_name" : "cremeglobal",
      "indices" : [ 29, 41 ],
      "id_str" : "59481779",
      "id" : 59481779
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hackathon",
      "indices" : [ 109, 119 ]
    }, {
      "text" : "technology",
      "indices" : [ 120, 131 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 132, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/MUJFgaHZyI",
      "expanded_url" : "http:\/\/www.cremeglobal.com\/blog\/why-you-should-attend-hackathons",
      "display_url" : "cremeglobal.com\/blog\/why-you-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711203805635989505",
  "text" : "RT @allentv4u: My article on @cremeglobal company blog - https:\/\/t.co\/MUJFgaHZyI. Let me know your feedback! #hackathon #technology #Ireland",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Creme Global",
        "screen_name" : "cremeglobal",
        "indices" : [ 14, 26 ],
        "id_str" : "59481779",
        "id" : 59481779
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "hackathon",
        "indices" : [ 94, 104 ]
      }, {
        "text" : "technology",
        "indices" : [ 105, 116 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 117, 125 ]
      } ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/MUJFgaHZyI",
        "expanded_url" : "http:\/\/www.cremeglobal.com\/blog\/why-you-should-attend-hackathons",
        "display_url" : "cremeglobal.com\/blog\/why-you-s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711195715570110464",
    "text" : "My article on @cremeglobal company blog - https:\/\/t.co\/MUJFgaHZyI. Let me know your feedback! #hackathon #technology #Ireland",
    "id" : 711195715570110464,
    "created_at" : "2016-03-19 14:20:46 +0000",
    "user" : {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "protected" : false,
      "id_str" : "77901568",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/787980171202686976\/QbSu0IKB_normal.jpg",
      "id" : 77901568,
      "verified" : false
    }
  },
  "id" : 711203805635989505,
  "created_at" : "2016-03-19 14:52:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stephen McCarthy",
      "screen_name" : "sportsfilesteve",
      "indices" : [ 3, 19 ],
      "id_str" : "43082775",
      "id" : 43082775
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711203659594518529",
  "text" : "RT @sportsfilesteve: A new low!!! Camera bags are banned from the Aviva Stadium. Think I'll just paint the pictures instead https:\/\/t.co\/hm\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sportsfilesteve\/status\/711174899134558209\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/hmT6XziTrN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cd6ZyNAWAAE_liM.jpg",
        "id_str" : "711174878934728705",
        "id" : 711174878934728705,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cd6ZyNAWAAE_liM.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hmT6XziTrN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "711174899134558209",
    "text" : "A new low!!! Camera bags are banned from the Aviva Stadium. Think I'll just paint the pictures instead https:\/\/t.co\/hmT6XziTrN",
    "id" : 711174899134558209,
    "created_at" : "2016-03-19 12:58:03 +0000",
    "user" : {
      "name" : "Stephen McCarthy",
      "screen_name" : "sportsfilesteve",
      "protected" : false,
      "id_str" : "43082775",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/986237701270589440\/mhfEyvkE_normal.jpg",
      "id" : 43082775,
      "verified" : false
    }
  },
  "id" : 711203659594518529,
  "created_at" : "2016-03-19 14:52:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/yQYSbAYh4r",
      "expanded_url" : "https:\/\/twitter.com\/AnilBatra\/status\/711201197193957376",
      "display_url" : "twitter.com\/AnilBatra\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711203344203845632",
  "text" : "The challenge is to assign which area has more weightage than the other. https:\/\/t.co\/yQYSbAYh4r",
  "id" : 711203344203845632,
  "created_at" : "2016-03-19 14:51:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/Acbihnda4C",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/singapore-graphic-novel-makes-a-splash-in-the-us-101309984.html?soc_src=social-sh&soc_trk=tw",
      "display_url" : "sg.news.yahoo.com\/singapore-grap\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711117757123796992",
  "text" : "Singapore graphic novel makes a splash in the US https:\/\/t.co\/Acbihnda4C",
  "id" : 711117757123796992,
  "created_at" : "2016-03-19 09:10:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "Irish Times Culture",
      "screen_name" : "IrishTimesCultr",
      "indices" : [ 96, 112 ],
      "id_str" : "1111829022",
      "id" : 1111829022
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/7FOVUNX8nE",
      "expanded_url" : "http:\/\/www.irishtimes.com\/culture\/film\/new-star-wars-film-to-shoot-in-donegal-and-kerry-1.2579059#.VuzuG57Jojc.twitter",
      "display_url" : "irishtimes.com\/culture\/film\/n\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "711115645157548032",
  "text" : "RT @GeoffreyIRL: New \u2018Star Wars\u2019 film to shoot in Donegal and Kerry https:\/\/t.co\/7FOVUNX8nE via @IrishTimesCultr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Times Culture",
        "screen_name" : "IrishTimesCultr",
        "indices" : [ 79, 95 ],
        "id_str" : "1111829022",
        "id" : 1111829022
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 51, 74 ],
        "url" : "https:\/\/t.co\/7FOVUNX8nE",
        "expanded_url" : "http:\/\/www.irishtimes.com\/culture\/film\/new-star-wars-film-to-shoot-in-donegal-and-kerry-1.2579059#.VuzuG57Jojc.twitter",
        "display_url" : "irishtimes.com\/culture\/film\/n\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "711073191351820288",
    "text" : "New \u2018Star Wars\u2019 film to shoot in Donegal and Kerry https:\/\/t.co\/7FOVUNX8nE via @IrishTimesCultr",
    "id" : 711073191351820288,
    "created_at" : "2016-03-19 06:13:54 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 711115645157548032,
  "created_at" : "2016-03-19 09:02:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GlendaloughPhotoPost",
      "screen_name" : "UpperLakePhoto",
      "indices" : [ 3, 18 ],
      "id_str" : "1364725021",
      "id" : 1364725021
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/UpperLakePhoto\/status\/709315366254723072\/photo\/1",
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/24vKyIt9p0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdf-kPJW8AAx0Qt.jpg",
      "id_str" : "709315364828672000",
      "id" : 709315364828672000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdf-kPJW8AAx0Qt.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 798,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 851,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 452,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 851,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/24vKyIt9p0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711095441438470144",
  "text" : "RT @UpperLakePhoto: Monday 14th March 2016, 8:39am https:\/\/t.co\/24vKyIt9p0",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/UpperLakePhoto\/status\/709315366254723072\/photo\/1",
        "indices" : [ 31, 54 ],
        "url" : "https:\/\/t.co\/24vKyIt9p0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdf-kPJW8AAx0Qt.jpg",
        "id_str" : "709315364828672000",
        "id" : 709315364828672000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdf-kPJW8AAx0Qt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 798,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 851,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 452,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 851,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/24vKyIt9p0"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709315366254723072",
    "text" : "Monday 14th March 2016, 8:39am https:\/\/t.co\/24vKyIt9p0",
    "id" : 709315366254723072,
    "created_at" : "2016-03-14 09:48:56 +0000",
    "user" : {
      "name" : "GlendaloughPhotoPost",
      "screen_name" : "UpperLakePhoto",
      "protected" : false,
      "id_str" : "1364725021",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/463213426043408384\/_95tUsub_normal.jpeg",
      "id" : 1364725021,
      "verified" : false
    }
  },
  "id" : 711095441438470144,
  "created_at" : "2016-03-19 07:42:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoAway",
      "indices" : [ 117, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "711094094790049792",
  "text" : "Ah see people from other domain esp digital analytics people using buzzword like machine learning and deep learning. #GoAway :)",
  "id" : 711094094790049792,
  "created_at" : "2016-03-19 07:36:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Twitter",
      "indices" : [ 44, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/l5koOgS19y",
      "expanded_url" : "http:\/\/nyti.ms\/1SYOxNN",
      "display_url" : "nyti.ms\/1SYOxNN"
    } ]
  },
  "geo" : { },
  "id_str" : "711093347641909248",
  "text" : "RT @GoGallivanting: The masses have spoken: #Twitter Rules Out Long Tweets, Sticking to 140-Character Limit https:\/\/t.co\/l5koOgS19y #social\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Twitter",
        "indices" : [ 24, 32 ]
      }, {
        "text" : "socialmedia",
        "indices" : [ 112, 124 ]
      } ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/l5koOgS19y",
        "expanded_url" : "http:\/\/nyti.ms\/1SYOxNN",
        "display_url" : "nyti.ms\/1SYOxNN"
      } ]
    },
    "geo" : { },
    "id_str" : "711083510119837696",
    "text" : "The masses have spoken: #Twitter Rules Out Long Tweets, Sticking to 140-Character Limit https:\/\/t.co\/l5koOgS19y #socialmedia",
    "id" : 711083510119837696,
    "created_at" : "2016-03-19 06:54:54 +0000",
    "user" : {
      "name" : "Jennifer Kolbuc",
      "screen_name" : "JenniferKolbuc",
      "protected" : false,
      "id_str" : "17629488",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/970370674244988928\/4-ngiezh_normal.jpg",
      "id" : 17629488,
      "verified" : false
    }
  },
  "id" : 711093347641909248,
  "created_at" : "2016-03-19 07:34:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Branson",
      "screen_name" : "richardbranson",
      "indices" : [ 3, 18 ],
      "id_str" : "8161232",
      "id" : 8161232
    }, {
      "name" : "Virgin Atlantic",
      "screen_name" : "VirginAtlantic",
      "indices" : [ 113, 128 ],
      "id_str" : "20626359",
      "id" : 20626359
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710962087254749184",
  "text" : "RT @richardbranson: Really sorry to hear about an alleged incident on flight VS250. We do not tolerate abuse and @virginatlantic are invest\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Virgin Atlantic",
        "screen_name" : "VirginAtlantic",
        "indices" : [ 93, 108 ],
        "id_str" : "20626359",
        "id" : 20626359
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710634209241645056",
    "text" : "Really sorry to hear about an alleged incident on flight VS250. We do not tolerate abuse and @virginatlantic are investigating.",
    "id" : 710634209241645056,
    "created_at" : "2016-03-18 01:09:33 +0000",
    "user" : {
      "name" : "Richard Branson",
      "screen_name" : "richardbranson",
      "protected" : false,
      "id_str" : "8161232",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876792863459352577\/SBLRu4VV_normal.jpg",
      "id" : 8161232,
      "verified" : true
    }
  },
  "id" : 710962087254749184,
  "created_at" : "2016-03-18 22:52:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brum Entrepreneurs",
      "screen_name" : "B_Entrepreneurs",
      "indices" : [ 3, 19 ],
      "id_str" : "730774820",
      "id" : 730774820
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/5Aw2ApoToO",
      "expanded_url" : "http:\/\/bit.ly\/1XxrLwc",
      "display_url" : "bit.ly\/1XxrLwc"
    } ]
  },
  "geo" : { },
  "id_str" : "710936746335277056",
  "text" : "RT @B_Entrepreneurs: The 80% Energy Rule: An Old Secret to Success https:\/\/t.co\/5Aw2ApoToO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/5Aw2ApoToO",
        "expanded_url" : "http:\/\/bit.ly\/1XxrLwc",
        "display_url" : "bit.ly\/1XxrLwc"
      } ]
    },
    "geo" : { },
    "id_str" : "710741305513660416",
    "text" : "The 80% Energy Rule: An Old Secret to Success https:\/\/t.co\/5Aw2ApoToO",
    "id" : 710741305513660416,
    "created_at" : "2016-03-18 08:15:06 +0000",
    "user" : {
      "name" : "Brum Entrepreneurs",
      "screen_name" : "B_Entrepreneurs",
      "protected" : false,
      "id_str" : "730774820",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2455484166\/BirminghamEntrepreneurs_normal.png",
      "id" : 730774820,
      "verified" : false
    }
  },
  "id" : 710936746335277056,
  "created_at" : "2016-03-18 21:11:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DAVE WONG",
      "screen_name" : "davidcheerup",
      "indices" : [ 3, 16 ],
      "id_str" : "1380927715",
      "id" : 1380927715
    }, {
      "name" : "iworkwithData",
      "screen_name" : "mryap",
      "indices" : [ 18, 24 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/ctLg5Hzqb0",
      "expanded_url" : "http:\/\/m.chinadaily.com.cn\/en\/2016-03\/19\/content_23957035.htm",
      "display_url" : "m.chinadaily.com.cn\/en\/2016-03\/19\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710923450567823361",
  "text" : "RT @davidcheerup: @mryap Virgin investigates Shanghai flight incident, expresses regret  https:\/\/t.co\/ctLg5Hzqb0",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "iworkwithData",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/ctLg5Hzqb0",
        "expanded_url" : "http:\/\/m.chinadaily.com.cn\/en\/2016-03\/19\/content_23957035.htm",
        "display_url" : "m.chinadaily.com.cn\/en\/2016-03\/19\/\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "710526514949967873",
    "geo" : { },
    "id_str" : "710874573084884992",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap Virgin investigates Shanghai flight incident, expresses regret  https:\/\/t.co\/ctLg5Hzqb0",
    "id" : 710874573084884992,
    "in_reply_to_status_id" : 710526514949967873,
    "created_at" : "2016-03-18 17:04:40 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "DAVE WONG",
      "screen_name" : "davidcheerup",
      "protected" : false,
      "id_str" : "1380927715",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/678903518363717632\/gGpRaiVj_normal.jpg",
      "id" : 1380927715,
      "verified" : false
    }
  },
  "id" : 710923450567823361,
  "created_at" : "2016-03-18 20:18:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "idaireland",
      "indices" : [ 99, 110 ]
    }, {
      "text" : "onlyonerealireland",
      "indices" : [ 111, 130 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/ITzYjLEzwG",
      "expanded_url" : "https:\/\/www.onlyonerealireland.com\/",
      "display_url" : "onlyonerealireland.com"
    } ]
  },
  "geo" : { },
  "id_str" : "710855756132818944",
  "text" : "I scored 12 out of 13 on IDA's globetrotting quiz. Test your Irish eye now https:\/\/t.co\/ITzYjLEzwG #idaireland #onlyonerealireland",
  "id" : 710855756132818944,
  "created_at" : "2016-03-18 15:49:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Boxever",
      "screen_name" : "Boxever",
      "indices" : [ 3, 11 ],
      "id_str" : "178101858",
      "id" : 178101858
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "airport",
      "indices" : [ 43, 51 ]
    }, {
      "text" : "travel",
      "indices" : [ 123, 130 ]
    } ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/zf9GmjXGKo",
      "expanded_url" : "http:\/\/hubs.ly\/H02qdc80",
      "display_url" : "hubs.ly\/H02qdc80"
    } ]
  },
  "geo" : { },
  "id_str" : "710783517068759040",
  "text" : "RT @Boxever: Imagine a rooftop pool at the #airport? There\u2019s one at the best airport in the world: https:\/\/t.co\/zf9GmjXGKo #travel",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hubspot.com\/\" rel=\"nofollow\"\u003EHubSpot\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "airport",
        "indices" : [ 30, 38 ]
      }, {
        "text" : "travel",
        "indices" : [ 110, 117 ]
      } ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/zf9GmjXGKo",
        "expanded_url" : "http:\/\/hubs.ly\/H02qdc80",
        "display_url" : "hubs.ly\/H02qdc80"
      } ]
    },
    "geo" : { },
    "id_str" : "710782811909857280",
    "text" : "Imagine a rooftop pool at the #airport? There\u2019s one at the best airport in the world: https:\/\/t.co\/zf9GmjXGKo #travel",
    "id" : 710782811909857280,
    "created_at" : "2016-03-18 11:00:02 +0000",
    "user" : {
      "name" : "Boxever",
      "screen_name" : "Boxever",
      "protected" : false,
      "id_str" : "178101858",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/441240618166001664\/WuXXvJs3_normal.png",
      "id" : 178101858,
      "verified" : false
    }
  },
  "id" : 710783517068759040,
  "created_at" : "2016-03-18 11:02:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Virgin Media Ireland",
      "screen_name" : "VirginMediaIE",
      "indices" : [ 69, 83 ],
      "id_str" : "289909806",
      "id" : 289909806
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710753996139532288",
  "text" : "For some reason, you unable to find out when your contract ends with @VirginMediaIE on My Virgin Media site.",
  "id" : 710753996139532288,
  "created_at" : "2016-03-18 09:05:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    }, {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 95, 103 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/vGYasEzBQJ",
      "expanded_url" : "https:\/\/medium.com\/@mrbrown\/from-one-fortysomething-to-another-c219950272f7#---0-196.z2ypqgncu",
      "display_url" : "medium.com\/@mrbrown\/from-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710740080206471168",
  "text" : "RT @mrbrown: \u201CMr Chua was a manager at a plastic production firm earning between $8,000 to\u2026\u201D\u200A\u2014\u200A@mrbrown https:\/\/t.co\/vGYasEzBQJ https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/medium.com\" rel=\"nofollow\"\u003EMedium\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "mrbrown",
        "screen_name" : "mrbrown",
        "indices" : [ 82, 90 ],
        "id_str" : "574253",
        "id" : 574253
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/710723188901203968\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/0ZTg8pMzOR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdz--VDUAAAkF2H.jpg",
        "id_str" : "710723187974078464",
        "id" : 710723187974078464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdz--VDUAAAkF2H.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 453
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1126,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1126,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1126,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0ZTg8pMzOR"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/vGYasEzBQJ",
        "expanded_url" : "https:\/\/medium.com\/@mrbrown\/from-one-fortysomething-to-another-c219950272f7#---0-196.z2ypqgncu",
        "display_url" : "medium.com\/@mrbrown\/from-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710723188901203968",
    "text" : "\u201CMr Chua was a manager at a plastic production firm earning between $8,000 to\u2026\u201D\u200A\u2014\u200A@mrbrown https:\/\/t.co\/vGYasEzBQJ https:\/\/t.co\/0ZTg8pMzOR",
    "id" : 710723188901203968,
    "created_at" : "2016-03-18 07:03:07 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 710740080206471168,
  "created_at" : "2016-03-18 08:10:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/2rcU28fSrT",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Once_(film)",
      "display_url" : "en.wikipedia.org\/wiki\/Once_(fil\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710600352752599042",
  "text" : "Now on TV https:\/\/t.co\/2rcU28fSrT",
  "id" : 710600352752599042,
  "created_at" : "2016-03-17 22:55:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dao Nguyen",
      "screen_name" : "daozers",
      "indices" : [ 3, 11 ],
      "id_str" : "13630902",
      "id" : 13630902
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/wx5z869awq",
      "expanded_url" : "http:\/\/buzzfeed.com",
      "display_url" : "buzzfeed.com"
    } ]
  },
  "geo" : { },
  "id_str" : "710577803469656064",
  "text" : "RT @daozers: Are you a data scientist in LA? Come work for BuzzFeed! You won't regret it. Email dao at https:\/\/t.co\/wx5z869awq or https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/wx5z869awq",
        "expanded_url" : "http:\/\/buzzfeed.com",
        "display_url" : "buzzfeed.com"
      }, {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/WC2SbOAtm6",
        "expanded_url" : "http:\/\/www.buzzfeed.com\/about\/jobs?gh_jid=33031",
        "display_url" : "buzzfeed.com\/about\/jobs?gh_\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710574558374453248",
    "text" : "Are you a data scientist in LA? Come work for BuzzFeed! You won't regret it. Email dao at https:\/\/t.co\/wx5z869awq or https:\/\/t.co\/WC2SbOAtm6",
    "id" : 710574558374453248,
    "created_at" : "2016-03-17 21:12:31 +0000",
    "user" : {
      "name" : "Dao Nguyen",
      "screen_name" : "daozers",
      "protected" : false,
      "id_str" : "13630902",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3383291422\/c897e8628b50aeace29d317baf491afe_normal.jpeg",
      "id" : 13630902,
      "verified" : true
    }
  },
  "id" : 710577803469656064,
  "created_at" : "2016-03-17 21:25:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/w6U4a8nlkB",
      "expanded_url" : "http:\/\/www.watchpro.com\/esquire-big-watch-survey-lists-50-most-wanted-watch-brands\/",
      "display_url" : "watchpro.com\/esquire-big-wa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710549543046275072",
  "text" : "Apple Watch not on Esquire 50 Most Wanted watch brands https:\/\/t.co\/w6U4a8nlkB :)",
  "id" : 710549543046275072,
  "created_at" : "2016-03-17 19:33:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/KPtzbXpXnl",
      "expanded_url" : "https:\/\/twitter.com\/suzybie\/status\/704356352538316802",
      "display_url" : "twitter.com\/suzybie\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710546430948155392",
  "text" : "If your TD is not on this list, most likely he or she has not being elected... https:\/\/t.co\/KPtzbXpXnl",
  "id" : 710546430948155392,
  "created_at" : "2016-03-17 19:20:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iworkwithdata",
      "indices" : [ 67, 81 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710545186670116865",
  "text" : "The thing about multiple regression is that there is no end to it. #iworkwithdata",
  "id" : 710545186670116865,
  "created_at" : "2016-03-17 19:15:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/T9OQHGz8mA",
      "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/710538827446808576",
      "display_url" : "twitter.com\/randal_olson\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710543014037475329",
  "text" : "What about Data Analyst and Data Scientist?  https:\/\/t.co\/T9OQHGz8mA",
  "id" : 710543014037475329,
  "created_at" : "2016-03-17 19:07:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/HEOctaolkR",
      "expanded_url" : "http:\/\/arxiv.org\/abs\/1312.4617",
      "display_url" : "arxiv.org\/abs\/1312.4617"
    } ]
  },
  "geo" : { },
  "id_str" : "710538743774629888",
  "text" : "Survey of data mining techniques in mining opinion expressed on social network site. https:\/\/t.co\/HEOctaolkR",
  "id" : 710538743774629888,
  "created_at" : "2016-03-17 18:50:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "virginatlanticVS250",
      "indices" : [ 48, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/eTnJT4AUef",
      "expanded_url" : "http:\/\/news.163.com\/16\/0317\/10\/BIBQNOQP0001121M.html?outsidelink",
      "display_url" : "news.163.com\/16\/0317\/10\/BIB\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710526514949967873",
  "text" : "\u5973\u5B50\u7EF4\u73CD\u822A\u7A7A\u673A\u4E0A\u88AB\u9A82\u4E2D\u56FD\u732A \u6C42\u52A9\u7A7A\u4E58\u53CD\u88AB\u5A01\u80C1 https:\/\/t.co\/eTnJT4AUef \n#virginatlanticVS250",
  "id" : 710526514949967873,
  "created_at" : "2016-03-17 18:01:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "gmapsmania",
      "screen_name" : "gmapsmania",
      "indices" : [ 3, 14 ],
      "id_str" : "17174685",
      "id" : 17174685
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/3tomgoN1Q2",
      "expanded_url" : "http:\/\/googlemapsmania.blogspot.com\/2016\/03\/the-geo-genealogy-map-of-ireland.html",
      "display_url" : "googlemapsmania.blogspot.com\/2016\/03\/the-ge\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710489462053195776",
  "text" : "RT @gmapsmania: Pretend you have Irish ancestry with this interactive genealogy map of Ireland. https:\/\/t.co\/3tomgoN1Q2 https:\/\/t.co\/C43Cos\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gmapsmania\/status\/710488022102450176\/photo\/1",
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/C43CosKNSQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdwpCHxWwAAsv2k.jpg",
        "id_str" : "710487957640232960",
        "id" : 710487957640232960,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdwpCHxWwAAsv2k.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 240,
          "resize" : "fit",
          "w" : 523
        }, {
          "h" : 240,
          "resize" : "fit",
          "w" : 523
        }, {
          "h" : 240,
          "resize" : "fit",
          "w" : 523
        }, {
          "h" : 240,
          "resize" : "fit",
          "w" : 523
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/C43CosKNSQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/3tomgoN1Q2",
        "expanded_url" : "http:\/\/googlemapsmania.blogspot.com\/2016\/03\/the-geo-genealogy-map-of-ireland.html",
        "display_url" : "googlemapsmania.blogspot.com\/2016\/03\/the-ge\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710488022102450176",
    "text" : "Pretend you have Irish ancestry with this interactive genealogy map of Ireland. https:\/\/t.co\/3tomgoN1Q2 https:\/\/t.co\/C43CosKNSQ",
    "id" : 710488022102450176,
    "created_at" : "2016-03-17 15:28:39 +0000",
    "user" : {
      "name" : "gmapsmania",
      "screen_name" : "gmapsmania",
      "protected" : false,
      "id_str" : "17174685",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469815074199072768\/cKeVFpIN_normal.png",
      "id" : 17174685,
      "verified" : false
    }
  },
  "id" : 710489462053195776,
  "created_at" : "2016-03-17 15:34:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710480444425371648",
  "text" : "Like a tweet is my way of saying I approve this message.",
  "id" : 710480444425371648,
  "created_at" : "2016-03-17 14:58:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pieter Nooren",
      "screen_name" : "pnooren",
      "indices" : [ 3, 11 ],
      "id_str" : "130079738",
      "id" : 130079738
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pnooren\/status\/710357035217510400\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/T4bN1m1v01",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdux9P6WAAAPGC8.jpg",
      "id_str" : "710357032042364928",
      "id" : 710357032042364928,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdux9P6WAAAPGC8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 606
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 606
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 606
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 606
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/T4bN1m1v01"
    } ],
    "hashtags" : [ {
      "text" : "Trump",
      "indices" : [ 33, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710417886708375552",
  "text" : "RT @pnooren: The Economist ranks #Trump presidency as top risk factor for global risk: https:\/\/t.co\/T4bN1m1v01",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pnooren\/status\/710357035217510400\/photo\/1",
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/T4bN1m1v01",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdux9P6WAAAPGC8.jpg",
        "id_str" : "710357032042364928",
        "id" : 710357032042364928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdux9P6WAAAPGC8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 606
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 606
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 606
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 606
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/T4bN1m1v01"
      } ],
      "hashtags" : [ {
        "text" : "Trump",
        "indices" : [ 20, 26 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710357035217510400",
    "text" : "The Economist ranks #Trump presidency as top risk factor for global risk: https:\/\/t.co\/T4bN1m1v01",
    "id" : 710357035217510400,
    "created_at" : "2016-03-17 06:48:09 +0000",
    "user" : {
      "name" : "Pieter Nooren",
      "screen_name" : "pnooren",
      "protected" : false,
      "id_str" : "130079738",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/536189917219414016\/gFgOCIPA_normal.jpeg",
      "id" : 130079738,
      "verified" : false
    }
  },
  "id" : 710417886708375552,
  "created_at" : "2016-03-17 10:49:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "indices" : [ 95, 101 ],
      "id_str" : "1344951",
      "id" : 1344951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/SE15tXwKNv",
      "expanded_url" : "http:\/\/www.wired.com\/2016\/03\/open-source-devs-racing-build-better-versions-slack\/",
      "display_url" : "wired.com\/2016\/03\/open-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710417837697994752",
  "text" : "RT @edwinksl: Open Sourcers Race to Build Better Versions of Slack https:\/\/t.co\/SE15tXwKNv via @WIRED",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "WIRED",
        "screen_name" : "WIRED",
        "indices" : [ 81, 87 ],
        "id_str" : "1344951",
        "id" : 1344951
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/SE15tXwKNv",
        "expanded_url" : "http:\/\/www.wired.com\/2016\/03\/open-source-devs-racing-build-better-versions-slack\/",
        "display_url" : "wired.com\/2016\/03\/open-s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710374487150206976",
    "text" : "Open Sourcers Race to Build Better Versions of Slack https:\/\/t.co\/SE15tXwKNv via @WIRED",
    "id" : 710374487150206976,
    "created_at" : "2016-03-17 07:57:30 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 710417837697994752,
  "created_at" : "2016-03-17 10:49:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "VictoriaLive",
      "indices" : [ 33, 46 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710417338991034368",
  "text" : "A dog ripped off a woman's arm!  #VictoriaLive",
  "id" : 710417338991034368,
  "created_at" : "2016-03-17 10:47:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710399891579781120",
  "text" : "Good Morning everyone. Happy St Patrick's Day!",
  "id" : 710399891579781120,
  "created_at" : "2016-03-17 09:38:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/topgold\/status\/710396055381254144\/photo\/1",
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/O4klGDdS9J",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdvVaKyVIAAI8vI.jpg",
      "id_str" : "710396011789754368",
      "id" : 710396011789754368,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdvVaKyVIAAI8vI.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1522
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 505
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1522
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 892
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/O4klGDdS9J"
    } ],
    "hashtags" : [ {
      "text" : "inflation",
      "indices" : [ 31, 41 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 42, 50 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710399780774612992",
  "text" : "RT @topgold: And so it begins. #inflation #Ireland https:\/\/t.co\/O4klGDdS9J",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/topgold\/status\/710396055381254144\/photo\/1",
        "indices" : [ 38, 61 ],
        "url" : "https:\/\/t.co\/O4klGDdS9J",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdvVaKyVIAAI8vI.jpg",
        "id_str" : "710396011789754368",
        "id" : 710396011789754368,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdvVaKyVIAAI8vI.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1522
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 505
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1522
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 892
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/O4klGDdS9J"
      } ],
      "hashtags" : [ {
        "text" : "inflation",
        "indices" : [ 18, 28 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 29, 37 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710396055381254144",
    "text" : "And so it begins. #inflation #Ireland https:\/\/t.co\/O4klGDdS9J",
    "id" : 710396055381254144,
    "created_at" : "2016-03-17 09:23:12 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 710399780774612992,
  "created_at" : "2016-03-17 09:38:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "D\u00E9agl\u00E1n MacPh\u00E1id\u00EDn",
      "screen_name" : "declanmacfadden",
      "indices" : [ 3, 19 ],
      "id_str" : "19288130",
      "id" : 19288130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/declanmacfadden\/status\/710371704195780609\/photo\/1",
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/1dcJjAlZJM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdu_TMnUMAAtSWU.jpg",
      "id_str" : "710371702765531136",
      "id" : 710371702765531136,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdu_TMnUMAAtSWU.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 451,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 451,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 451,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1dcJjAlZJM"
    } ],
    "hashtags" : [ {
      "text" : "refugeeswelcome",
      "indices" : [ 21, 37 ]
    }, {
      "text" : "don",
      "indices" : [ 38, 42 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710392038768963584",
  "text" : "RT @declanmacfadden: #refugeeswelcome #don'tforget https:\/\/t.co\/1dcJjAlZJM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/declanmacfadden\/status\/710371704195780609\/photo\/1",
        "indices" : [ 30, 53 ],
        "url" : "https:\/\/t.co\/1dcJjAlZJM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdu_TMnUMAAtSWU.jpg",
        "id_str" : "710371702765531136",
        "id" : 710371702765531136,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdu_TMnUMAAtSWU.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1dcJjAlZJM"
      } ],
      "hashtags" : [ {
        "text" : "refugeeswelcome",
        "indices" : [ 0, 16 ]
      }, {
        "text" : "don",
        "indices" : [ 17, 21 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710371704195780609",
    "text" : "#refugeeswelcome #don'tforget https:\/\/t.co\/1dcJjAlZJM",
    "id" : 710371704195780609,
    "created_at" : "2016-03-17 07:46:27 +0000",
    "user" : {
      "name" : "D\u00E9agl\u00E1n MacPh\u00E1id\u00EDn",
      "screen_name" : "declanmacfadden",
      "protected" : false,
      "id_str" : "19288130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684614991492743168\/Rup-iIUi_normal.jpg",
      "id" : 19288130,
      "verified" : false
    }
  },
  "id" : 710392038768963584,
  "created_at" : "2016-03-17 09:07:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AldiTheRestaurant",
      "indices" : [ 31, 49 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710230791842865152",
  "text" : "Who is that good looking lady? #AldiTheRestaurant Asking for a friend....",
  "id" : 710230791842865152,
  "created_at" : "2016-03-16 22:26:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Dixon",
      "screen_name" : "cdixon",
      "indices" : [ 3, 10 ],
      "id_str" : "2529971",
      "id" : 2529971
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cdixon\/status\/709979222383697920\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/LVrhfLgayf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdpaU70WIAA5a62.jpg",
      "id_str" : "709979206965403648",
      "id" : 709979206965403648,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdpaU70WIAA5a62.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 671
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 380
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1997,
        "resize" : "fit",
        "w" : 1116
      }, {
        "h" : 1997,
        "resize" : "fit",
        "w" : 1116
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LVrhfLgayf"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/JuEhSBqqDG",
      "expanded_url" : "http:\/\/www.businessinsider.com\/steve-jobs-reaction-first-iphone-2015-9",
      "display_url" : "businessinsider.com\/steve-jobs-rea\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710230389063880704",
  "text" : "RT @cdixon: Steve Jobs: \"Your thumbs will learn.\" https:\/\/t.co\/JuEhSBqqDG https:\/\/t.co\/LVrhfLgayf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cdixon\/status\/709979222383697920\/photo\/1",
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/LVrhfLgayf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdpaU70WIAA5a62.jpg",
        "id_str" : "709979206965403648",
        "id" : 709979206965403648,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdpaU70WIAA5a62.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 671
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 380
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1997,
          "resize" : "fit",
          "w" : 1116
        }, {
          "h" : 1997,
          "resize" : "fit",
          "w" : 1116
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LVrhfLgayf"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 38, 61 ],
        "url" : "https:\/\/t.co\/JuEhSBqqDG",
        "expanded_url" : "http:\/\/www.businessinsider.com\/steve-jobs-reaction-first-iphone-2015-9",
        "display_url" : "businessinsider.com\/steve-jobs-rea\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "709979222383697920",
    "text" : "Steve Jobs: \"Your thumbs will learn.\" https:\/\/t.co\/JuEhSBqqDG https:\/\/t.co\/LVrhfLgayf",
    "id" : 709979222383697920,
    "created_at" : "2016-03-16 05:46:52 +0000",
    "user" : {
      "name" : "Chris Dixon",
      "screen_name" : "cdixon",
      "protected" : false,
      "id_str" : "2529971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/683496924104658944\/8Oa5XAso_normal.png",
      "id" : 2529971,
      "verified" : true
    }
  },
  "id" : 710230389063880704,
  "created_at" : "2016-03-16 22:24:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tarn Rodgers Johns",
      "screen_name" : "TarnRJ",
      "indices" : [ 3, 10 ],
      "id_str" : "72655206",
      "id" : 72655206
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "shopwellforless",
      "indices" : [ 12, 28 ]
    }, {
      "text" : "thetruecost",
      "indices" : [ 112, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710215278471340032",
  "text" : "RT @TarnRJ: #shopwellforless so people can die making your clothes but at least you can go to Disneyland right? #thetruecost",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "shopwellforless",
        "indices" : [ 0, 16 ]
      }, {
        "text" : "thetruecost",
        "indices" : [ 100, 112 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710209246982492165",
    "text" : "#shopwellforless so people can die making your clothes but at least you can go to Disneyland right? #thetruecost",
    "id" : 710209246982492165,
    "created_at" : "2016-03-16 21:00:54 +0000",
    "user" : {
      "name" : "Tarn Rodgers Johns",
      "screen_name" : "TarnRJ",
      "protected" : false,
      "id_str" : "72655206",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017739792182898693\/KEMCPYVU_normal.jpg",
      "id" : 72655206,
      "verified" : false
    }
  },
  "id" : 710215278471340032,
  "created_at" : "2016-03-16 21:24:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/wgWrdJdth0",
      "expanded_url" : "http:\/\/www.pcworld.com\/article\/3041464\/analytics\/food-poisoning-in-las-vegas-not-if-this-machine-learning-algorithm-can-help-it.html",
      "display_url" : "pcworld.com\/article\/304146\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710213907424665601",
  "text" : "This reminds me of Dr. John Snow's map of the 1854 London cholera outbreak\nhttps:\/\/t.co\/wgWrdJdth0",
  "id" : 710213907424665601,
  "created_at" : "2016-03-16 21:19:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steve Portigal \uD83C\uDDE8\uD83C\uDDE6\uD83C\uDDFA\uD83C\uDDF8",
      "screen_name" : "steveportigal",
      "indices" : [ 3, 17 ],
      "id_str" : "3922921",
      "id" : 3922921
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/steveportigal\/status\/710205620033458176\/photo\/1",
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/yEn2ujjnGN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdsoP18UYAAZYZ_.jpg",
      "id_str" : "710205618884206592",
      "id" : 710205618884206592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdsoP18UYAAZYZ_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yEn2ujjnGN"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/E17AodtPD4",
      "expanded_url" : "http:\/\/www.portigal.com\/the-chinese-edition-of-interviewing-users\/",
      "display_url" : "portigal.com\/the-chinese-ed\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710208429835591680",
  "text" : "RT @steveportigal: Announcing the Chinese Edition of Interviewing Users https:\/\/t.co\/E17AodtPD4 https:\/\/t.co\/yEn2ujjnGN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/steveportigal\/status\/710205620033458176\/photo\/1",
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/yEn2ujjnGN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdsoP18UYAAZYZ_.jpg",
        "id_str" : "710205618884206592",
        "id" : 710205618884206592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdsoP18UYAAZYZ_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yEn2ujjnGN"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/E17AodtPD4",
        "expanded_url" : "http:\/\/www.portigal.com\/the-chinese-edition-of-interviewing-users\/",
        "display_url" : "portigal.com\/the-chinese-ed\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710205620033458176",
    "text" : "Announcing the Chinese Edition of Interviewing Users https:\/\/t.co\/E17AodtPD4 https:\/\/t.co\/yEn2ujjnGN",
    "id" : 710205620033458176,
    "created_at" : "2016-03-16 20:46:29 +0000",
    "user" : {
      "name" : "Steve Portigal \uD83C\uDDE8\uD83C\uDDE6\uD83C\uDDFA\uD83C\uDDF8",
      "screen_name" : "steveportigal",
      "protected" : false,
      "id_str" : "3922921",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035054588670312448\/DbBf7Qu8_normal.jpg",
      "id" : 3922921,
      "verified" : false
    }
  },
  "id" : 710208429835591680,
  "created_at" : "2016-03-16 20:57:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Changi Airport",
      "screen_name" : "ChangiAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "65268724",
      "id" : 65268724
    }, {
      "name" : "Changi Airport",
      "screen_name" : "ChangiAirport",
      "indices" : [ 33, 47 ],
      "id_str" : "65268724",
      "id" : 65268724
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Breaking",
      "indices" : [ 19, 28 ]
    }, {
      "text" : "WorldsBestAirport",
      "indices" : [ 111, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710175601878032384",
  "text" : "RT @ChangiAirport: #Breaking: \uD83C\uDDF8\uD83C\uDDEC @ChangiAirport has been \uD83D\uDC51\uD83C\uDF0F#\u20E31\u20E3 airport by Skytrax for the 4th year \uD83C\uDFC3\uD83C\uDFFB! \uD83C\uDF89\uD83D\uDE4C\uD83C\uDFFC\uD83D\uDCAA\uD83C\uDFFC\uD83C\uDF89 #WorldsBestAirport https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Changi Airport",
        "screen_name" : "ChangiAirport",
        "indices" : [ 14, 28 ],
        "id_str" : "65268724",
        "id" : 65268724
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChangiAirport\/status\/710156234465214464\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/sActSdzEgC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cdr7Rw2VAAAN2X1.jpg",
        "id_str" : "710156173853392896",
        "id" : 710156173853392896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cdr7Rw2VAAAN2X1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sActSdzEgC"
      } ],
      "hashtags" : [ {
        "text" : "Breaking",
        "indices" : [ 0, 9 ]
      }, {
        "text" : "WorldsBestAirport",
        "indices" : [ 92, 110 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710156234465214464",
    "text" : "#Breaking: \uD83C\uDDF8\uD83C\uDDEC @ChangiAirport has been \uD83D\uDC51\uD83C\uDF0F#\u20E31\u20E3 airport by Skytrax for the 4th year \uD83C\uDFC3\uD83C\uDFFB! \uD83C\uDF89\uD83D\uDE4C\uD83C\uDFFC\uD83D\uDCAA\uD83C\uDFFC\uD83C\uDF89 #WorldsBestAirport https:\/\/t.co\/sActSdzEgC",
    "id" : 710156234465214464,
    "created_at" : "2016-03-16 17:30:15 +0000",
    "user" : {
      "name" : "Changi Airport",
      "screen_name" : "ChangiAirport",
      "protected" : false,
      "id_str" : "65268724",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894423767484465152\/kB4X-t8m_normal.jpg",
      "id" : 65268724,
      "verified" : true
    }
  },
  "id" : 710175601878032384,
  "created_at" : "2016-03-16 18:47:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Virgin Media Ireland",
      "screen_name" : "VirginMediaIE",
      "indices" : [ 0, 14 ],
      "id_str" : "289909806",
      "id" : 289909806
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "710174874514407424",
  "geo" : { },
  "id_str" : "710175118471856128",
  "in_reply_to_user_id" : 289909806,
  "text" : "@VirginMediaIE we tried everything.",
  "id" : 710175118471856128,
  "in_reply_to_status_id" : 710174874514407424,
  "created_at" : "2016-03-16 18:45:17 +0000",
  "in_reply_to_screen_name" : "VirginMediaIE",
  "in_reply_to_user_id_str" : "289909806",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Virgin Media Ireland",
      "screen_name" : "VirginMediaIE",
      "indices" : [ 0, 14 ],
      "id_str" : "289909806",
      "id" : 289909806
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710174011993538560",
  "in_reply_to_user_id" : 289909806,
  "text" : "@VirginMediaIE I have error code 2020 when I switch on the TV and there no internet connection. Pls advise. Thanks.",
  "id" : 710174011993538560,
  "created_at" : "2016-03-16 18:40:53 +0000",
  "in_reply_to_screen_name" : "VirginMediaIE",
  "in_reply_to_user_id_str" : "289909806",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Kelly",
      "screen_name" : "pkellyonline",
      "indices" : [ 3, 16 ],
      "id_str" : "168134048",
      "id" : 168134048
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Luas",
      "indices" : [ 53, 58 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710128628131094528",
  "text" : "RT @pkellyonline: Can we just go driverless already? #Luas",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Luas",
        "indices" : [ 35, 40 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710100905996230656",
    "text" : "Can we just go driverless already? #Luas",
    "id" : 710100905996230656,
    "created_at" : "2016-03-16 13:50:23 +0000",
    "user" : {
      "name" : "Peter Kelly",
      "screen_name" : "pkellyonline",
      "protected" : false,
      "id_str" : "168134048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/961548503959949312\/T-S82qwX_normal.jpg",
      "id" : 168134048,
      "verified" : false
    }
  },
  "id" : 710128628131094528,
  "created_at" : "2016-03-16 15:40:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AlanChristopherMusic",
      "screen_name" : "alancoyle",
      "indices" : [ 3, 13 ],
      "id_str" : "22347400",
      "id" : 22347400
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Luas",
      "indices" : [ 39, 44 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710127316815499264",
  "text" : "RT @alancoyle: The cancellation of the #Luas strike tomorrow will bring in a lot of extra revenue to the company... you know, if people act\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Luas",
        "indices" : [ 24, 29 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710120892932734976",
    "text" : "The cancellation of the #Luas strike tomorrow will bring in a lot of extra revenue to the company... you know, if people actually paid.",
    "id" : 710120892932734976,
    "created_at" : "2016-03-16 15:09:48 +0000",
    "user" : {
      "name" : "AlanChristopherMusic",
      "screen_name" : "alancoyle",
      "protected" : false,
      "id_str" : "22347400",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/631578617940586496\/JLdeJWIt_normal.jpg",
      "id" : 22347400,
      "verified" : false
    }
  },
  "id" : 710127316815499264,
  "created_at" : "2016-03-16 15:35:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710127141397110785",
  "text" : "Luas driver if you want your pay rise, go and chase down those free rider esp. those on the Redline!",
  "id" : 710127141397110785,
  "created_at" : "2016-03-16 15:34:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "710125395207651328",
  "geo" : { },
  "id_str" : "710126803541696514",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold I not sure about ROI, So difficult to talk to them, they are very shy people...",
  "id" : 710126803541696514,
  "in_reply_to_status_id" : 710125395207651328,
  "created_at" : "2016-03-16 15:33:18 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    }, {
      "name" : "why vo",
      "screen_name" : "why_vo",
      "indices" : [ 15, 22 ],
      "id_str" : "725367012533264384",
      "id" : 725367012533264384
    }, {
      "name" : "k",
      "screen_name" : "keeweeeee",
      "indices" : [ 23, 33 ],
      "id_str" : "25675138",
      "id" : 25675138
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "710117830679777282",
  "geo" : { },
  "id_str" : "710125064331640832",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold @why_vo @keeweeeee U pay more tax than FaceBook UK!",
  "id" : 710125064331640832,
  "in_reply_to_status_id" : 710117830679777282,
  "created_at" : "2016-03-16 15:26:23 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "710116583117553664",
  "geo" : { },
  "id_str" : "710124814401458178",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold Are u able to withdraw all when you go back to S'pore?",
  "id" : 710124814401458178,
  "in_reply_to_status_id" : 710116583117553664,
  "created_at" : "2016-03-16 15:25:23 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Claudio Toyama",
      "screen_name" : "ClaudioGT",
      "indices" : [ 0, 10 ],
      "id_str" : "47503235",
      "id" : 47503235
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "710121274517884929",
  "geo" : { },
  "id_str" : "710122951987884032",
  "in_reply_to_user_id" : 47503235,
  "text" : "@ClaudioGT @ColumbiaWFM wonderful day over there! It dull overhere in Dublin.",
  "id" : 710122951987884032,
  "in_reply_to_status_id" : 710121274517884929,
  "created_at" : "2016-03-16 15:17:59 +0000",
  "in_reply_to_screen_name" : "ClaudioGT",
  "in_reply_to_user_id_str" : "47503235",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "indices" : [ 3, 11 ],
      "id_str" : "41025492",
      "id" : 41025492
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/mllVzZK1aP",
      "expanded_url" : "https:\/\/twitter.com\/PohJacqueline\/status\/710084396875849728",
      "display_url" : "twitter.com\/PohJacqueline\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710102306122043393",
  "text" : "RT @marklcl: Python is the right language to learn.. Heehee...  https:\/\/t.co\/mllVzZK1aP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 51, 74 ],
        "url" : "https:\/\/t.co\/mllVzZK1aP",
        "expanded_url" : "https:\/\/twitter.com\/PohJacqueline\/status\/710084396875849728",
        "display_url" : "twitter.com\/PohJacqueline\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "710102036553961472",
    "text" : "Python is the right language to learn.. Heehee...  https:\/\/t.co\/mllVzZK1aP",
    "id" : 710102036553961472,
    "created_at" : "2016-03-16 13:54:53 +0000",
    "user" : {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "protected" : false,
      "id_str" : "41025492",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684550252083298306\/DmeNwyFV_normal.jpg",
      "id" : 41025492,
      "verified" : false
    }
  },
  "id" : 710102306122043393,
  "created_at" : "2016-03-16 13:55:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/w9CIajaIF7",
      "expanded_url" : "https:\/\/tmblr.co\/Zyf6qo23PJC_1",
      "display_url" : "tmblr.co\/Zyf6qo23PJC_1"
    } ]
  },
  "geo" : { },
  "id_str" : "710097338635849728",
  "text" : "\uD83D\uDCF9 Setting idea for X-T10 - assign ISO to front wheel to trim exposure &amp; assign shutter speed to rear wheel https:\/\/t.co\/w9CIajaIF7",
  "id" : 710097338635849728,
  "created_at" : "2016-03-16 13:36:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ethan White",
      "screen_name" : "ethanwhite",
      "indices" : [ 3, 14 ],
      "id_str" : "34540379",
      "id" : 34540379
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710067209712898048",
  "text" : "RT @ethanwhite: Please do not require logins or CAPTCHA's on publicly available data. Data is much more valuable when it can be accessed pr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709725864934686721",
    "text" : "Please do not require logins or CAPTCHA's on publicly available data. Data is much more valuable when it can be accessed programmatically.",
    "id" : 709725864934686721,
    "created_at" : "2016-03-15 13:00:06 +0000",
    "user" : {
      "name" : "Ethan White",
      "screen_name" : "ethanwhite",
      "protected" : false,
      "id_str" : "34540379",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875364025005096961\/8_fgpYGC_normal.jpg",
      "id" : 34540379,
      "verified" : false
    }
  },
  "id" : 710067209712898048,
  "created_at" : "2016-03-16 11:36:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/z9IrxQAJaq",
      "expanded_url" : "http:\/\/adage.com\/article\/print-edition\/10-things-programmatic-buying\/298811\/",
      "display_url" : "adage.com\/article\/print-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710050997394137088",
  "text" : "Came across a LinkedIn profile with the word Programmatic. Things You Need to Know Now About Programmatic Buying\n https:\/\/t.co\/z9IrxQAJaq",
  "id" : 710050997394137088,
  "created_at" : "2016-03-16 10:32:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710044812687310848",
  "text" : "Royal Brunei Airlines' first all-female pilot crew has being tweeted to death. Do you know Brunei has implemented Shariah law?",
  "id" : 710044812687310848,
  "created_at" : "2016-03-16 10:07:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Expat Explorer",
      "screen_name" : "expatexplorer",
      "indices" : [ 3, 17 ],
      "id_str" : "27960823",
      "id" : 27960823
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 88, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/kko9TXIXlv",
      "expanded_url" : "https:\/\/start.yougov.com\/refer\/vysfh7HYcJdChF?&source=13",
      "display_url" : "start.yougov.com\/refer\/vysfh7HY\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710040549890326528",
  "text" : "RT @expatexplorer: Over 20,000 expats took our survey in 2015. Have your say on life in #Ireland at:\nhttps:\/\/t.co\/kko9TXIXlv https:\/\/t.co\/E\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/expatexplorer\/status\/705044532501442560\/photo\/1",
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/EdXqOMGzdR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CcjSOPtWwAE-h1y.jpg",
        "id_str" : "705044483860250625",
        "id" : 705044483860250625,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcjSOPtWwAE-h1y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EdXqOMGzdR"
      } ],
      "hashtags" : [ {
        "text" : "Ireland",
        "indices" : [ 69, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/kko9TXIXlv",
        "expanded_url" : "https:\/\/start.yougov.com\/refer\/vysfh7HYcJdChF?&source=13",
        "display_url" : "start.yougov.com\/refer\/vysfh7HY\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705044532501442560",
    "text" : "Over 20,000 expats took our survey in 2015. Have your say on life in #Ireland at:\nhttps:\/\/t.co\/kko9TXIXlv https:\/\/t.co\/EdXqOMGzdR",
    "id" : 705044532501442560,
    "created_at" : "2016-03-02 14:58:10 +0000",
    "user" : {
      "name" : "Expat Explorer",
      "screen_name" : "expatexplorer",
      "protected" : false,
      "id_str" : "27960823",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1280284003\/World-faceTOOL_normal.jpg",
      "id" : 27960823,
      "verified" : true
    }
  },
  "id" : 710040549890326528,
  "created_at" : "2016-03-16 09:50:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jane Frazier",
      "screen_name" : "mignon1915",
      "indices" : [ 3, 14 ],
      "id_str" : "373629495",
      "id" : 373629495
    }, {
      "name" : "Rob J Hyndman",
      "screen_name" : "robjhyndman",
      "indices" : [ 116, 128 ],
      "id_str" : "229638179",
      "id" : 229638179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/6QyVeTSOTB",
      "expanded_url" : "http:\/\/bit.ly\/1RAIha1",
      "display_url" : "bit.ly\/1RAIha1"
    } ]
  },
  "geo" : { },
  "id_str" : "710019841617432576",
  "text" : "RT @mignon1915: \"Science Code Manifesto\" for code, c'right, citation, credit &amp; curation https:\/\/t.co\/6QyVeTSOTB @robjhyndman @cammerschoone\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rob J Hyndman",
        "screen_name" : "robjhyndman",
        "indices" : [ 100, 112 ],
        "id_str" : "229638179",
        "id" : 229638179
      }, {
        "name" : "Cameron McLean",
        "screen_name" : "cammerschooner",
        "indices" : [ 113, 128 ],
        "id_str" : "306085021",
        "id" : 306085021
      }, {
        "name" : "David Nickerson",
        "screen_name" : "andreshouse",
        "indices" : [ 129, 141 ],
        "id_str" : "20031199",
        "id" : 20031199
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/6QyVeTSOTB",
        "expanded_url" : "http:\/\/bit.ly\/1RAIha1",
        "display_url" : "bit.ly\/1RAIha1"
      } ]
    },
    "geo" : { },
    "id_str" : "707723746409259008",
    "text" : "\"Science Code Manifesto\" for code, c'right, citation, credit &amp; curation https:\/\/t.co\/6QyVeTSOTB @robjhyndman @cammerschooner @andreshouse",
    "id" : 707723746409259008,
    "created_at" : "2016-03-10 00:24:24 +0000",
    "user" : {
      "name" : "Jane Frazier",
      "screen_name" : "mignon1915",
      "protected" : false,
      "id_str" : "373629495",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/717662274538713088\/_gSsBxY7_normal.jpg",
      "id" : 373629495,
      "verified" : false
    }
  },
  "id" : 710019841617432576,
  "created_at" : "2016-03-16 08:28:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710019262593830912",
  "text" : "Am I the only one who find data for a published research paper not available to the readers?",
  "id" : 710019262593830912,
  "created_at" : "2016-03-16 08:25:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 47, 57 ]
    }, {
      "text" : "StPatricksDay",
      "indices" : [ 62, 76 ]
    } ],
    "urls" : [ {
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/PgXyMe9t7i",
      "expanded_url" : "https:\/\/www.dfa.ie\/irish-embassy\/singapore\/news-and-events\/2016\/st-patricks-day-2016-singapore\/",
      "display_url" : "dfa.ie\/irish-embassy\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "710015896497033216",
  "text" : "RT @IrlEmbSingapore: Want to know what's on in #Singapore for #StPatricksDay? Take a look on our website https:\/\/t.co\/PgXyMe9t7i https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/707468405310365697\/photo\/1",
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/y2Mwjz1nwt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdFuw6MXIAAjCMt.jpg",
        "id_str" : "707468403007758336",
        "id" : 707468403007758336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdFuw6MXIAAjCMt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1297,
          "resize" : "fit",
          "w" : 1946
        }, {
          "h" : 1297,
          "resize" : "fit",
          "w" : 1946
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y2Mwjz1nwt"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 26, 36 ]
      }, {
        "text" : "StPatricksDay",
        "indices" : [ 41, 55 ]
      } ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/PgXyMe9t7i",
        "expanded_url" : "https:\/\/www.dfa.ie\/irish-embassy\/singapore\/news-and-events\/2016\/st-patricks-day-2016-singapore\/",
        "display_url" : "dfa.ie\/irish-embassy\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "707468405310365697",
    "text" : "Want to know what's on in #Singapore for #StPatricksDay? Take a look on our website https:\/\/t.co\/PgXyMe9t7i https:\/\/t.co\/y2Mwjz1nwt",
    "id" : 707468405310365697,
    "created_at" : "2016-03-09 07:29:46 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 710015896497033216,
  "created_at" : "2016-03-16 08:12:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "indices" : [ 3, 11 ],
      "id_str" : "26425084",
      "id" : 26425084
    }, {
      "name" : "Damion Chee",
      "screen_name" : "damionchee",
      "indices" : [ 26, 37 ],
      "id_str" : "128823407",
      "id" : 128823407
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710015627042398208",
  "text" : "RT @oiwoods: Insight from @damionchee: Malaysian Chinese refer to beer brands by their old labels (e.g. \u7687\u5E3D (Crown) = Carlsberg, \u9ED1\u72D7 (Black D\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Damion Chee",
        "screen_name" : "damionchee",
        "indices" : [ 13, 24 ],
        "id_str" : "128823407",
        "id" : 128823407
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "710011251737403392",
    "text" : "Insight from @damionchee: Malaysian Chinese refer to beer brands by their old labels (e.g. \u7687\u5E3D (Crown) = Carlsberg, \u9ED1\u72D7 (Black Dog)\uFF1DGuinness)",
    "id" : 710011251737403392,
    "created_at" : "2016-03-16 07:54:08 +0000",
    "user" : {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "protected" : false,
      "id_str" : "26425084",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914900128707764225\/Vmx_JkpE_normal.jpg",
      "id" : 26425084,
      "verified" : false
    }
  },
  "id" : 710015627042398208,
  "created_at" : "2016-03-16 08:11:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    }, {
      "name" : "Trinity College Dublin",
      "screen_name" : "tcddublin",
      "indices" : [ 96, 106 ],
      "id_str" : "31537951",
      "id" : 31537951
    }, {
      "name" : "Ireland 2016",
      "screen_name" : "ireland2016",
      "indices" : [ 123, 135 ],
      "id_str" : "2896407763",
      "id" : 2896407763
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ProclamationDay",
      "indices" : [ 21, 37 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "710015248971997184",
  "text" : "RT @IrlEmbSingapore: #ProclamationDay here's the Proclamation in simplified Mandarin! Thanks to @tcddublin for translating @ireland2016 htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Trinity College Dublin",
        "screen_name" : "tcddublin",
        "indices" : [ 75, 85 ],
        "id_str" : "31537951",
        "id" : 31537951
      }, {
        "name" : "Ireland 2016",
        "screen_name" : "ireland2016",
        "indices" : [ 102, 114 ],
        "id_str" : "2896407763",
        "id" : 2896407763
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/709936605335838720\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/GQQzxUmh6c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdozlAoWAAAFGk6.jpg",
        "id_str" : "709936602181664768",
        "id" : 709936602181664768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdozlAoWAAAFGk6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 799
        }, {
          "h" : 4961,
          "resize" : "fit",
          "w" : 3303
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1364
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 453
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GQQzxUmh6c"
      } ],
      "hashtags" : [ {
        "text" : "ProclamationDay",
        "indices" : [ 0, 16 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709936605335838720",
    "text" : "#ProclamationDay here's the Proclamation in simplified Mandarin! Thanks to @tcddublin for translating @ireland2016 https:\/\/t.co\/GQQzxUmh6c",
    "id" : 709936605335838720,
    "created_at" : "2016-03-16 02:57:31 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 710015248971997184,
  "created_at" : "2016-03-16 08:10:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Womersley",
      "screen_name" : "LisaWomersley",
      "indices" : [ 75, 89 ],
      "id_str" : "789148770869080064",
      "id" : 789148770869080064
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/iId1j0DeIw",
      "expanded_url" : "https:\/\/medium.com\/@S.Hamel\/google-has-a-360-view-of-your-digital-life-2c288354b763#.yz11jm7x5",
      "display_url" : "medium.com\/@S.Hamel\/googl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709883776990617600",
  "text" : "Is this Google answer to Adobe Marketing Cloud? https:\/\/t.co\/iId1j0DeIw cc @lisawomersley @Azhreicb",
  "id" : 709883776990617600,
  "created_at" : "2016-03-15 23:27:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathew Prentice",
      "screen_name" : "PrenticeMathew",
      "indices" : [ 63, 78 ],
      "id_str" : "796040639360147456",
      "id" : 796040639360147456
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/Ftx0zj97W0",
      "expanded_url" : "https:\/\/medium.com\/@prenticemathew\/why-singapore-isnt-the-most-expensive-city-in-the-world-11fcad31c8b4#.xs1wp5z9p",
      "display_url" : "medium.com\/@prenticemathe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709882824128655361",
  "text" : "\u201CWhy Singapore isn\u2019t the most expensive city in the world.\u201D by @prenticemathew https:\/\/t.co\/Ftx0zj97W0",
  "id" : 709882824128655361,
  "created_at" : "2016-03-15 23:23:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gwen",
      "screen_name" : "Claredaisy",
      "indices" : [ 0, 11 ],
      "id_str" : "217099872",
      "id" : 217099872
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 77, 91 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "709799130651090944",
  "geo" : { },
  "id_str" : "709880354165956609",
  "in_reply_to_user_id" : 217099872,
  "text" : "@Claredaisy  I wonder whether this year will it be a bowl of shamrock again? #StPatricksDay",
  "id" : 709880354165956609,
  "in_reply_to_status_id" : 709799130651090944,
  "created_at" : "2016-03-15 23:14:00 +0000",
  "in_reply_to_screen_name" : "Claredaisy",
  "in_reply_to_user_id_str" : "217099872",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "9 Dash Line \u4E5D\u6BB5\u7EBF",
      "screen_name" : "9DashLine",
      "indices" : [ 3, 13 ],
      "id_str" : "2847258009",
      "id" : 2847258009
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SouthChinaSea",
      "indices" : [ 61, 75 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/vU3n0A673c",
      "expanded_url" : "https:\/\/twitter.com\/SputnikInt\/status\/709637781203525632",
      "display_url" : "twitter.com\/SputnikInt\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709788174982246400",
  "text" : "RT @9DashLine: Putin's latest pivot brings him closer to the #SouthChinaSea every day https:\/\/t.co\/vU3n0A673c",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SouthChinaSea",
        "indices" : [ 46, 60 ]
      } ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/vU3n0A673c",
        "expanded_url" : "https:\/\/twitter.com\/SputnikInt\/status\/709637781203525632",
        "display_url" : "twitter.com\/SputnikInt\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "709781005482237952",
    "text" : "Putin's latest pivot brings him closer to the #SouthChinaSea every day https:\/\/t.co\/vU3n0A673c",
    "id" : 709781005482237952,
    "created_at" : "2016-03-15 16:39:13 +0000",
    "user" : {
      "name" : "9 Dash Line \u4E5D\u6BB5\u7EBF",
      "screen_name" : "9DashLine",
      "protected" : false,
      "id_str" : "2847258009",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/545724611950678018\/rwdvgMdk_normal.jpeg",
      "id" : 2847258009,
      "verified" : false
    }
  },
  "id" : 709788174982246400,
  "created_at" : "2016-03-15 17:07:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "indices" : [ 3, 19 ],
      "id_str" : "51263711",
      "id" : 51263711
    }, {
      "name" : "The Wall Street Journal",
      "screen_name" : "WSJ",
      "indices" : [ 95, 99 ],
      "id_str" : "3108351",
      "id" : 3108351
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "measure",
      "indices" : [ 100, 108 ]
    }, {
      "text" : "marketing",
      "indices" : [ 109, 119 ]
    } ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/4vr0AE8yhe",
      "expanded_url" : "http:\/\/www.wsj.com\/articles\/google-launches-new-data-tools-for-marketers-1458043217",
      "display_url" : "wsj.com\/articles\/googl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709772714463072256",
  "text" : "RT @googleanalytics: Google Launches New Data Tools for Marketers https:\/\/t.co\/4vr0AE8yhe \/via @WSJ #measure #marketing https:\/\/t.co\/ERKm8m\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Wall Street Journal",
        "screen_name" : "WSJ",
        "indices" : [ 74, 78 ],
        "id_str" : "3108351",
        "id" : 3108351
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/709745480255692801\/photo\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/ERKm8mFWj6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdmFwKJUMAALypZ.jpg",
        "id_str" : "709745478691205120",
        "id" : 709745478691205120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdmFwKJUMAALypZ.jpg",
        "sizes" : [ {
          "h" : 413,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 620
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ERKm8mFWj6"
      } ],
      "hashtags" : [ {
        "text" : "measure",
        "indices" : [ 79, 87 ]
      }, {
        "text" : "marketing",
        "indices" : [ 88, 98 ]
      } ],
      "urls" : [ {
        "indices" : [ 45, 68 ],
        "url" : "https:\/\/t.co\/4vr0AE8yhe",
        "expanded_url" : "http:\/\/www.wsj.com\/articles\/google-launches-new-data-tools-for-marketers-1458043217",
        "display_url" : "wsj.com\/articles\/googl\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "709745480255692801",
    "text" : "Google Launches New Data Tools for Marketers https:\/\/t.co\/4vr0AE8yhe \/via @WSJ #measure #marketing https:\/\/t.co\/ERKm8mFWj6",
    "id" : 709745480255692801,
    "created_at" : "2016-03-15 14:18:03 +0000",
    "user" : {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "protected" : false,
      "id_str" : "51263711",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021848775885651968\/cU74ahCn_normal.jpg",
      "id" : 51263711,
      "verified" : true
    }
  },
  "id" : 709772714463072256,
  "created_at" : "2016-03-15 16:06:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mona Eltahawy",
      "screen_name" : "monaeltahawy",
      "indices" : [ 3, 16 ],
      "id_str" : "29979814",
      "id" : 29979814
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709695834988945408",
  "text" : "RT @monaeltahawy: All-woman crew just flew us EWR-LHR. This crew flew Royal Brunei to Saudi: they can fly a plane but not drive a car https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/monaeltahawy\/status\/708960937185812481\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/YSmxrvQZBR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cda8MsNW4AAPczC.jpg",
        "id_str" : "708960917569069056",
        "id" : 708960917569069056,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cda8MsNW4AAPczC.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 511,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 770,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 770,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 770,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YSmxrvQZBR"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "708960937185812481",
    "text" : "All-woman crew just flew us EWR-LHR. This crew flew Royal Brunei to Saudi: they can fly a plane but not drive a car https:\/\/t.co\/YSmxrvQZBR",
    "id" : 708960937185812481,
    "created_at" : "2016-03-13 10:20:33 +0000",
    "user" : {
      "name" : "Mona Eltahawy",
      "screen_name" : "monaeltahawy",
      "protected" : false,
      "id_str" : "29979814",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024869858171912192\/1i_hvFkm_normal.jpg",
      "id" : 29979814,
      "verified" : true
    }
  },
  "id" : 709695834988945408,
  "created_at" : "2016-03-15 11:00:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iworkwithdata",
      "indices" : [ 56, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/fXf0bx44QX",
      "expanded_url" : "http:\/\/www.iworkwithdata.com\/2016\/03\/14\/which-r-reference-book-for-me\/",
      "display_url" : "iworkwithdata.com\/2016\/03\/14\/whi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709524093167669248",
  "text" : "Which R reference book for you? https:\/\/t.co\/fXf0bx44QX #iworkwithdata",
  "id" : 709524093167669248,
  "created_at" : "2016-03-14 23:38:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ireland Live News",
      "screen_name" : "IrelandLive",
      "indices" : [ 113, 125 ],
      "id_str" : "2463372692",
      "id" : 2463372692
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Tyrrelstown",
      "indices" : [ 47, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709497273730928641",
  "text" : "RT @ZaraKing_UTV: Residents facing eviction in #Tyrrelstown are meeting tonight to discuss their options-More on @IrelandLive news10pm http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ireland Live News",
        "screen_name" : "IrelandLive",
        "indices" : [ 95, 107 ],
        "id_str" : "2463372692",
        "id" : 2463372692
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ZaraKing_UTV\/status\/709482184932003840\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/LKPhPXxdq9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdiWRY0WoAAX40a.jpg",
        "id_str" : "709482166774833152",
        "id" : 709482166774833152,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdiWRY0WoAAX40a.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LKPhPXxdq9"
      } ],
      "hashtags" : [ {
        "text" : "Tyrrelstown",
        "indices" : [ 29, 41 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709482184932003840",
    "text" : "Residents facing eviction in #Tyrrelstown are meeting tonight to discuss their options-More on @IrelandLive news10pm https:\/\/t.co\/LKPhPXxdq9",
    "id" : 709482184932003840,
    "created_at" : "2016-03-14 20:51:49 +0000",
    "user" : {
      "name" : "Zara King",
      "screen_name" : "ZaraKing",
      "protected" : false,
      "id_str" : "22397745",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035270443203211269\/RLQV3sKC_normal.jpg",
      "id" : 22397745,
      "verified" : false
    }
  },
  "id" : 709497273730928641,
  "created_at" : "2016-03-14 21:51:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/SHFbsa0BsO",
      "expanded_url" : "https:\/\/twitter.com\/davymac\/status\/709332791817805824",
      "display_url" : "twitter.com\/davymac\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709486840936579072",
  "text" : "A fujifilm camera on a Nikon poster. LOL  https:\/\/t.co\/SHFbsa0BsO",
  "id" : 709486840936579072,
  "created_at" : "2016-03-14 21:10:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/HdQyi7fEXL",
      "expanded_url" : "http:\/\/gu.com\/p\/4hgtb\/stw",
      "display_url" : "gu.com\/p\/4hgtb\/stw"
    } ]
  },
  "geo" : { },
  "id_str" : "709456206348357633",
  "text" : "How the 'digital exhaust' of social media data can predict gun violence https:\/\/t.co\/HdQyi7fEXL",
  "id" : 709456206348357633,
  "created_at" : "2016-03-14 19:08:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ciara Kelly",
      "screen_name" : "ciarakellydoc",
      "indices" : [ 3, 17 ],
      "id_str" : "2177175701",
      "id" : 2177175701
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hateverifiedbyvisa",
      "indices" : [ 118, 137 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709454220685135872",
  "text" : "RT @ciarakellydoc: Verified by Visa has saved me fortune... by stopping me from buying things I actually bloody need \n#hateverifiedbyvisa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "hateverifiedbyvisa",
        "indices" : [ 99, 118 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709432706749341696",
    "text" : "Verified by Visa has saved me fortune... by stopping me from buying things I actually bloody need \n#hateverifiedbyvisa",
    "id" : 709432706749341696,
    "created_at" : "2016-03-14 17:35:12 +0000",
    "user" : {
      "name" : "Ciara Kelly",
      "screen_name" : "ciarakellydoc",
      "protected" : false,
      "id_str" : "2177175701",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1036889685862350848\/TY1fLMVm_normal.jpg",
      "id" : 2177175701,
      "verified" : true
    }
  },
  "id" : 709454220685135872,
  "created_at" : "2016-03-14 19:00:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Connector",
      "screen_name" : "connector",
      "indices" : [ 3, 13 ],
      "id_str" : "14667624",
      "id" : 14667624
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/connector\/status\/709418549895417856\/photo\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/b32lMW9mI1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdhcaYNWIAEmY3W.jpg",
      "id_str" : "709418549555634177",
      "id" : 709418549555634177,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdhcaYNWIAEmY3W.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 700
      }, {
        "h" : 412,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 700
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 700
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/b32lMW9mI1"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/oKsm2PcssI",
      "expanded_url" : "http:\/\/ift.tt\/1SN7q6s",
      "display_url" : "ift.tt\/1SN7q6s"
    } ]
  },
  "geo" : { },
  "id_str" : "709423072185610240",
  "text" : "RT @connector: Meta-Materials Bring Us Another Step Closer to an Invisibility Cloak https:\/\/t.co\/oKsm2PcssI https:\/\/t.co\/b32lMW9mI1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/connector\/status\/709418549895417856\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/b32lMW9mI1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdhcaYNWIAEmY3W.jpg",
        "id_str" : "709418549555634177",
        "id" : 709418549555634177,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdhcaYNWIAEmY3W.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 412,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 700
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/b32lMW9mI1"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/oKsm2PcssI",
        "expanded_url" : "http:\/\/ift.tt\/1SN7q6s",
        "display_url" : "ift.tt\/1SN7q6s"
      } ]
    },
    "geo" : { },
    "id_str" : "709418549895417856",
    "text" : "Meta-Materials Bring Us Another Step Closer to an Invisibility Cloak https:\/\/t.co\/oKsm2PcssI https:\/\/t.co\/b32lMW9mI1",
    "id" : 709418549895417856,
    "created_at" : "2016-03-14 16:38:57 +0000",
    "user" : {
      "name" : "Connector",
      "screen_name" : "connector",
      "protected" : false,
      "id_str" : "14667624",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/890504047953928192\/puy0p4DY_normal.jpg",
      "id" : 14667624,
      "verified" : false
    }
  },
  "id" : 709423072185610240,
  "created_at" : "2016-03-14 16:56:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 3, 12 ],
      "id_str" : "1291131",
      "id" : 1291131
    }, {
      "name" : "Congregation",
      "screen_name" : "congregation13",
      "indices" : [ 25, 40 ],
      "id_str" : "1969116109",
      "id" : 1969116109
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/atkxft4Do9",
      "expanded_url" : "https:\/\/twitter.com\/wonderlandblog\/status\/709417852835463168",
      "display_url" : "twitter.com\/wonderlandblog\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709422818979618816",
  "text" : "RT @maryrose: Similar to @congregation13  https:\/\/t.co\/atkxft4Do9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Congregation",
        "screen_name" : "congregation13",
        "indices" : [ 11, 26 ],
        "id_str" : "1969116109",
        "id" : 1969116109
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 51 ],
        "url" : "https:\/\/t.co\/atkxft4Do9",
        "expanded_url" : "https:\/\/twitter.com\/wonderlandblog\/status\/709417852835463168",
        "display_url" : "twitter.com\/wonderlandblog\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "709419363351269376",
    "text" : "Similar to @congregation13  https:\/\/t.co\/atkxft4Do9",
    "id" : 709419363351269376,
    "created_at" : "2016-03-14 16:42:11 +0000",
    "user" : {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "protected" : false,
      "id_str" : "1291131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034068926370594816\/eS9-sUNe_normal.jpg",
      "id" : 1291131,
      "verified" : false
    }
  },
  "id" : 709422818979618816,
  "created_at" : "2016-03-14 16:55:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709127661243777024",
  "text" : "RT @patphelan: Hopefully\/eventually\/I pray that startups will discover events\/trips\/shows are just bullshit and the real work has to be don\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709118311162847232",
    "text" : "Hopefully\/eventually\/I pray that startups will discover events\/trips\/shows are just bullshit and the real work has to be done too",
    "id" : 709118311162847232,
    "created_at" : "2016-03-13 20:45:54 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 709127661243777024,
  "created_at" : "2016-03-13 21:23:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geraldine Herbert",
      "screen_name" : "GerHerbert1",
      "indices" : [ 3, 15 ],
      "id_str" : "462083773",
      "id" : 462083773
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Poll",
      "indices" : [ 17, 22 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709122930286981121",
  "text" : "RT @GerHerbert1: #Poll If you were buying a used car would you do a history check on the car?\nRTs appreciated",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Poll",
        "indices" : [ 0, 5 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709105345252204545",
    "text" : "#Poll If you were buying a used car would you do a history check on the car?\nRTs appreciated",
    "id" : 709105345252204545,
    "created_at" : "2016-03-13 19:54:23 +0000",
    "user" : {
      "name" : "Geraldine Herbert",
      "screen_name" : "GerHerbert1",
      "protected" : false,
      "id_str" : "462083773",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/492655680470282242\/Du5bcunu_normal.jpeg",
      "id" : 462083773,
      "verified" : false
    }
  },
  "id" : 709122930286981121,
  "created_at" : "2016-03-13 21:04:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edward Tufte",
      "screen_name" : "EdwardTufte",
      "indices" : [ 3, 15 ],
      "id_str" : "152862026",
      "id" : 152862026
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "visualization",
      "indices" : [ 98, 112 ]
    }, {
      "text" : "dataviz",
      "indices" : [ 113, 121 ]
    }, {
      "text" : "journalism",
      "indices" : [ 122, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/EjwCxMG3Sk",
      "expanded_url" : "http:\/\/www.edwardtufte.com\/bboard\/q-and-a-fetch-msg?msg_id=0003zP",
      "display_url" : "edwardtufte.com\/bboard\/q-and-a\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709122069066338304",
  "text" : "RT @EdwardTufte: Graphical schedule,times\/days+figurative map.Lots more: https:\/\/t.co\/EjwCxMG3Sk \n#visualization #dataviz #journalism https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EdwardTufte\/status\/709108451381739520\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/NJBrJJR0Y1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CddCYOpXIAA8tCn.jpg",
        "id_str" : "709108450350014464",
        "id" : 709108450350014464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CddCYOpXIAA8tCn.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 524
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1580
        }, {
          "h" : 2492,
          "resize" : "fit",
          "w" : 1922
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 926
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NJBrJJR0Y1"
      } ],
      "hashtags" : [ {
        "text" : "visualization",
        "indices" : [ 81, 95 ]
      }, {
        "text" : "dataviz",
        "indices" : [ 96, 104 ]
      }, {
        "text" : "journalism",
        "indices" : [ 105, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/EjwCxMG3Sk",
        "expanded_url" : "http:\/\/www.edwardtufte.com\/bboard\/q-and-a-fetch-msg?msg_id=0003zP",
        "display_url" : "edwardtufte.com\/bboard\/q-and-a\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "709108451381739520",
    "text" : "Graphical schedule,times\/days+figurative map.Lots more: https:\/\/t.co\/EjwCxMG3Sk \n#visualization #dataviz #journalism https:\/\/t.co\/NJBrJJR0Y1",
    "id" : 709108451381739520,
    "created_at" : "2016-03-13 20:06:44 +0000",
    "user" : {
      "name" : "Edward Tufte",
      "screen_name" : "EdwardTufte",
      "protected" : false,
      "id_str" : "152862026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/458847900957556736\/uArnnoBy_normal.png",
      "id" : 152862026,
      "verified" : false
    }
  },
  "id" : 709122069066338304,
  "created_at" : "2016-03-13 21:00:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/esFTlZjLog",
      "expanded_url" : "https:\/\/blogs.technet.microsoft.com\/machinelearning\/2016\/03\/09\/announcing-r-tools-for-visual-studio-2\/",
      "display_url" : "blogs.technet.microsoft.com\/machinelearnin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709098727294816261",
  "text" : "Announcing R Tools for Visual Studio https:\/\/t.co\/esFTlZjLog",
  "id" : 709098727294816261,
  "created_at" : "2016-03-13 19:28:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/fvqNyZvyEX",
      "expanded_url" : "https:\/\/www.getdatajoy.com\/examples\/56e59d69697743550fc1c3ea",
      "display_url" : "getdatajoy.com\/examples\/56e59\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709064518660390912",
  "text" : "Using R, I sorted the variables and I can get the data point in line. https:\/\/t.co\/fvqNyZvyEX Is this ethical?",
  "id" : 709064518660390912,
  "created_at" : "2016-03-13 17:12:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709051301976543232",
  "text" : "@avalanchelynn Love is blind. :P",
  "id" : 709051301976543232,
  "created_at" : "2016-03-13 16:19:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "indices" : [ 3, 14 ],
      "id_str" : "166540104",
      "id" : 166540104
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/longhowlam\/status\/709046922359808004\/photo\/1",
      "indices" : [ 16, 39 ],
      "url" : "https:\/\/t.co\/HHTEEb95cd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdcKaVpVIAAY7G5.jpg",
      "id_str" : "709046913937514496",
      "id" : 709046913937514496,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdcKaVpVIAAY7G5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 553,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 610,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 610,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 610,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HHTEEb95cd"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709051010094858240",
  "text" : "RT @longhowlam: https:\/\/t.co\/HHTEEb95cd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/longhowlam\/status\/709046922359808004\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/HHTEEb95cd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdcKaVpVIAAY7G5.jpg",
        "id_str" : "709046913937514496",
        "id" : 709046913937514496,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdcKaVpVIAAY7G5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 553,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 610,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 610,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 610,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HHTEEb95cd"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "709046922359808004",
    "text" : "https:\/\/t.co\/HHTEEb95cd",
    "id" : 709046922359808004,
    "created_at" : "2016-03-13 16:02:14 +0000",
    "user" : {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "protected" : false,
      "id_str" : "166540104",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/677041208711970821\/BYuLlEyn_normal.jpg",
      "id" : 166540104,
      "verified" : false
    }
  },
  "id" : 709051010094858240,
  "created_at" : "2016-03-13 16:18:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709045151067459584",
  "text" : "What probably single best book you own on data analysis?",
  "id" : 709045151067459584,
  "created_at" : "2016-03-13 15:55:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 3, 17 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709030993865383936",
  "text" : "RT @ladystormhold: I don't understand. Why are Singaporeans so desperate for battery chargers, power banks and USB outlets on buses??? http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/ASs7CgllUe",
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/709024259599781888",
        "display_url" : "twitter.com\/eisen\/status\/7\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "709028209887735808",
    "text" : "I don't understand. Why are Singaporeans so desperate for battery chargers, power banks and USB outlets on buses??? https:\/\/t.co\/ASs7CgllUe",
    "id" : 709028209887735808,
    "created_at" : "2016-03-13 14:47:53 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 709030993865383936,
  "created_at" : "2016-03-13 14:58:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/gKj7fgPqWz",
      "expanded_url" : "http:\/\/lovindublin.com\/dublin\/5-dublin-walks-that-are-absolutely-perfect-on-a-sunday",
      "display_url" : "lovindublin.com\/dublin\/5-dubli\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "709030752697098240",
  "text" : "So far I have done The Great South Wall  - \n5 Dublin Walks That Are Absolutely Perfect On A Sunday  https:\/\/t.co\/gKj7fgPqWz",
  "id" : 709030752697098240,
  "created_at" : "2016-03-13 14:57:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lonely Planet",
      "screen_name" : "lonelyplanet",
      "indices" : [ 3, 16 ],
      "id_str" : "15066760",
      "id" : 15066760
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 53, 63 ]
    }, {
      "text" : "travel",
      "indices" : [ 120, 127 ]
    }, {
      "text" : "lp",
      "indices" : [ 128, 131 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/6wjp9b0zdl",
      "expanded_url" : "http:\/\/lptravel.to\/pQeEuD",
      "display_url" : "lptravel.to\/pQeEuD"
    } ]
  },
  "geo" : { },
  "id_str" : "709025401847287808",
  "text" : "RT @lonelyplanet: They won't stay a secret for long. #Singapore's up-and-coming neighbourhoods: https:\/\/t.co\/6wjp9b0zdl #travel #lp https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lonelyplanet\/status\/708911748259127296\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/T4Exntk9W6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdaPeoJXIAA_Jdm.jpg",
        "id_str" : "708911747692961792",
        "id" : 708911747692961792,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdaPeoJXIAA_Jdm.jpg",
        "sizes" : [ {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/T4Exntk9W6"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 35, 45 ]
      }, {
        "text" : "travel",
        "indices" : [ 102, 109 ]
      }, {
        "text" : "lp",
        "indices" : [ 110, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/6wjp9b0zdl",
        "expanded_url" : "http:\/\/lptravel.to\/pQeEuD",
        "display_url" : "lptravel.to\/pQeEuD"
      } ]
    },
    "geo" : { },
    "id_str" : "708911748259127296",
    "text" : "They won't stay a secret for long. #Singapore's up-and-coming neighbourhoods: https:\/\/t.co\/6wjp9b0zdl #travel #lp https:\/\/t.co\/T4Exntk9W6",
    "id" : 708911748259127296,
    "created_at" : "2016-03-13 07:05:06 +0000",
    "user" : {
      "name" : "Lonely Planet",
      "screen_name" : "lonelyplanet",
      "protected" : false,
      "id_str" : "15066760",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659349744532246528\/oJDWTI75_normal.png",
      "id" : 15066760,
      "verified" : true
    }
  },
  "id" : 709025401847287808,
  "created_at" : "2016-03-13 14:36:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WhyIreland",
      "indices" : [ 123, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "709017932492242944",
  "text" : "Submit an application and no news as to whether that being received by that govt agency. What so difficult to acknowledge? #WhyIreland?",
  "id" : 709017932492242944,
  "created_at" : "2016-03-13 14:07:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "708965132894924800",
  "text" : "Singaporean expect their politicians to be tertiary educated and to be whiter and white.",
  "id" : 708965132894924800,
  "created_at" : "2016-03-13 10:37:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/8RxRXRN1RR",
      "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/708930885240770561",
      "display_url" : "twitter.com\/TheEconomist\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708941117824311296",
  "text" : "\"Nearby Singapore, with its different mix of the same ethnic groups, should also be alarmed.\" https:\/\/t.co\/8RxRXRN1RR",
  "id" : 708941117824311296,
  "created_at" : "2016-03-13 09:01:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ari Kopoulos",
      "screen_name" : "aricochet",
      "indices" : [ 3, 13 ],
      "id_str" : "21166399",
      "id" : 21166399
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/88JmlU285M",
      "expanded_url" : "http:\/\/www.businessinsider.com\/absurd-decor-modern-tech-startup-offices-chappell-ellison-2016-3",
      "display_url" : "businessinsider.com\/absurd-decor-m\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708930952790204416",
  "text" : "RT @aricochet: These tweets nail the absurdity of many tech offices https:\/\/t.co\/88JmlU285M",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/88JmlU285M",
        "expanded_url" : "http:\/\/www.businessinsider.com\/absurd-decor-modern-tech-startup-offices-chappell-ellison-2016-3",
        "display_url" : "businessinsider.com\/absurd-decor-m\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "708929342080811008",
    "text" : "These tweets nail the absurdity of many tech offices https:\/\/t.co\/88JmlU285M",
    "id" : 708929342080811008,
    "created_at" : "2016-03-13 08:15:01 +0000",
    "user" : {
      "name" : "Ari Kopoulos",
      "screen_name" : "aricochet",
      "protected" : false,
      "id_str" : "21166399",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/690048969997697025\/-DH3tJvK_normal.jpg",
      "id" : 21166399,
      "verified" : false
    }
  },
  "id" : 708930952790204416,
  "created_at" : "2016-03-13 08:21:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/FspnbAUbd7",
      "expanded_url" : "https:\/\/twitter.com\/klillington\/status\/707580085390458880",
      "display_url" : "twitter.com\/klillington\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708925372423917569",
  "text" : "Based on OECD averages.  https:\/\/t.co\/FspnbAUbd7",
  "id" : 708925372423917569,
  "created_at" : "2016-03-13 07:59:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/6HWNrqMNrZ",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BC3bH5OhZIM\/",
      "display_url" : "instagram.com\/p\/BC3bH5OhZIM\/"
    } ]
  },
  "geo" : { },
  "id_str" : "708747966258008065",
  "text" : "First attempt at Char Kway Teow (stir-fried ricecake strips) a popular noodle dish in Singapore,\u2026 https:\/\/t.co\/6HWNrqMNrZ",
  "id" : 708747966258008065,
  "created_at" : "2016-03-12 20:14:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "708680117321637889",
  "text" : "Anyway to call out and display the Adjusted R-squared side by side in R? Thanks",
  "id" : 708680117321637889,
  "created_at" : "2016-03-12 15:44:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mahdi Yusuf",
      "screen_name" : "myusuf3",
      "indices" : [ 3, 11 ],
      "id_str" : "14233236",
      "id" : 14233236
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/myusuf3\/status\/676541219007164416\/photo\/1",
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/t2BTTOXPa7",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CWOOlGfXAAEl8wp.png",
      "id_str" : "676541137084153857",
      "id" : 676541137084153857,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CWOOlGfXAAEl8wp.png",
      "sizes" : [ {
        "h" : 180,
        "resize" : "fit",
        "w" : 270
      }, {
        "h" : 180,
        "resize" : "fit",
        "w" : 270
      }, {
        "h" : 180,
        "resize" : "fit",
        "w" : 270
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 180,
        "resize" : "fit",
        "w" : 270
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/t2BTTOXPa7"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "708674405757538304",
  "text" : "RT @myusuf3: Fixing bugs in production https:\/\/t.co\/t2BTTOXPa7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/myusuf3\/status\/676541219007164416\/photo\/1",
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/t2BTTOXPa7",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CWOOlGfXAAEl8wp.png",
        "id_str" : "676541137084153857",
        "id" : 676541137084153857,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CWOOlGfXAAEl8wp.png",
        "sizes" : [ {
          "h" : 180,
          "resize" : "fit",
          "w" : 270
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 270
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 270
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 270
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/t2BTTOXPa7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "676541219007164416",
    "text" : "Fixing bugs in production https:\/\/t.co\/t2BTTOXPa7",
    "id" : 676541219007164416,
    "created_at" : "2015-12-14 23:16:10 +0000",
    "user" : {
      "name" : "Mahdi Yusuf",
      "screen_name" : "myusuf3",
      "protected" : false,
      "id_str" : "14233236",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/928508249011130368\/0RWQyVrw_normal.jpg",
      "id" : 14233236,
      "verified" : false
    }
  },
  "id" : 708674405757538304,
  "created_at" : "2016-03-12 15:21:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/I95mfDRh8m",
      "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/708199813662134272",
      "display_url" : "twitter.com\/paulinesargent\u2026"
    }, {
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/KnDj5gVpMX",
      "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/708199185296728065",
      "display_url" : "twitter.com\/paulinesargent\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708668300742496256",
  "text" : "https:\/\/t.co\/I95mfDRh8m  https:\/\/t.co\/KnDj5gVpMX",
  "id" : 708668300742496256,
  "created_at" : "2016-03-12 14:57:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adam",
      "screen_name" : "AdamFlinter",
      "indices" : [ 3, 15 ],
      "id_str" : "24678294",
      "id" : 24678294
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/rJrseCqOSU",
      "expanded_url" : "https:\/\/twitter.com\/ShunfuMart\/status\/708644410007793665",
      "display_url" : "twitter.com\/ShunfuMart\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708658026471923712",
  "text" : "RT @AdamFlinter: In France they'd promote him  https:\/\/t.co\/rJrseCqOSU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 30, 53 ],
        "url" : "https:\/\/t.co\/rJrseCqOSU",
        "expanded_url" : "https:\/\/twitter.com\/ShunfuMart\/status\/708644410007793665",
        "display_url" : "twitter.com\/ShunfuMart\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "708644815194312704",
    "text" : "In France they'd promote him  https:\/\/t.co\/rJrseCqOSU",
    "id" : 708644815194312704,
    "created_at" : "2016-03-12 13:24:24 +0000",
    "user" : {
      "name" : "Adam",
      "screen_name" : "AdamFlinter",
      "protected" : false,
      "id_str" : "24678294",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972013262710738944\/Sg9nGyuY_normal.jpg",
      "id" : 24678294,
      "verified" : false
    }
  },
  "id" : 708658026471923712,
  "created_at" : "2016-03-12 14:16:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/tdRiUc5bO8",
      "expanded_url" : "https:\/\/twitter.com\/Azhreicb\/status\/708589052707082241",
      "display_url" : "twitter.com\/Azhreicb\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708590615525777408",
  "text" : "You are not alone. These labels are marketing people social construct. https:\/\/t.co\/tdRiUc5bO8",
  "id" : 708590615525777408,
  "created_at" : "2016-03-12 09:49:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Trisha Carter",
      "screen_name" : "TrishaCarter",
      "indices" : [ 3, 16 ],
      "id_str" : "24513030",
      "id" : 24513030
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TrishaCarter\/status\/708586377332428801\/photo\/1",
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/KmqCCPKvJv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdVnjkqUkAQVvCQ.jpg",
      "id_str" : "708586377214988292",
      "id" : 708586377214988292,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdVnjkqUkAQVvCQ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2259,
        "resize" : "fit",
        "w" : 1694
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KmqCCPKvJv"
    } ],
    "hashtags" : [ {
      "text" : "FIGTNL16",
      "indices" : [ 88, 97 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "708586814353883136",
  "text" : "RT @TrishaCarter: The floating tribe of expatriates - info from Melissa Taylor-Bradford #FIGTNL16 https:\/\/t.co\/KmqCCPKvJv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TrishaCarter\/status\/708586377332428801\/photo\/1",
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/KmqCCPKvJv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdVnjkqUkAQVvCQ.jpg",
        "id_str" : "708586377214988292",
        "id" : 708586377214988292,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdVnjkqUkAQVvCQ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2259,
          "resize" : "fit",
          "w" : 1694
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KmqCCPKvJv"
      } ],
      "hashtags" : [ {
        "text" : "FIGTNL16",
        "indices" : [ 70, 79 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "708586377332428801",
    "text" : "The floating tribe of expatriates - info from Melissa Taylor-Bradford #FIGTNL16 https:\/\/t.co\/KmqCCPKvJv",
    "id" : 708586377332428801,
    "created_at" : "2016-03-12 09:32:11 +0000",
    "user" : {
      "name" : "Trisha Carter",
      "screen_name" : "TrishaCarter",
      "protected" : false,
      "id_str" : "24513030",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/728802787765551106\/Jhs_p9Dq_normal.jpg",
      "id" : 24513030,
      "verified" : false
    }
  },
  "id" : 708586814353883136,
  "created_at" : "2016-03-12 09:33:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "708576782228783104",
  "text" : "From an ICU nurse in Dublin. I going to resign to be a Luas driver. They earn so much more than me.",
  "id" : 708576782228783104,
  "created_at" : "2016-03-12 08:54:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "indices" : [ 3, 13 ],
      "id_str" : "911413417",
      "id" : 911413417
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Dr_Draper\/status\/707128359507984385\/photo\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/N8nOJUpuoT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdA5d6IUEAAhPPr.jpg",
      "id_str" : "707128327480283136",
      "id" : 707128327480283136,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdA5d6IUEAAhPPr.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 608,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 756,
        "resize" : "fit",
        "w" : 846
      }, {
        "h" : 756,
        "resize" : "fit",
        "w" : 846
      }, {
        "h" : 756,
        "resize" : "fit",
        "w" : 846
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/N8nOJUpuoT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "708440749881364480",
  "text" : "RT @Dr_Draper: It\u2019s the hottest new event on the marketing calendar \u2013 The Emperor\u2019s New Clothes Conference. https:\/\/t.co\/N8nOJUpuoT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Dr_Draper\/status\/707128359507984385\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/N8nOJUpuoT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdA5d6IUEAAhPPr.jpg",
        "id_str" : "707128327480283136",
        "id" : 707128327480283136,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdA5d6IUEAAhPPr.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 756,
          "resize" : "fit",
          "w" : 846
        }, {
          "h" : 756,
          "resize" : "fit",
          "w" : 846
        }, {
          "h" : 756,
          "resize" : "fit",
          "w" : 846
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/N8nOJUpuoT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707128359507984385",
    "text" : "It\u2019s the hottest new event on the marketing calendar \u2013 The Emperor\u2019s New Clothes Conference. https:\/\/t.co\/N8nOJUpuoT",
    "id" : 707128359507984385,
    "created_at" : "2016-03-08 08:58:33 +0000",
    "user" : {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "protected" : false,
      "id_str" : "911413417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2778032626\/f43efb67d690165e4c0003ca3fa2694b_normal.jpeg",
      "id" : 911413417,
      "verified" : false
    }
  },
  "id" : 708440749881364480,
  "created_at" : "2016-03-11 23:53:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John FX Berns",
      "screen_name" : "jfxberns",
      "indices" : [ 3, 12 ],
      "id_str" : "6024062",
      "id" : 6024062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/LAfGPmPpuS",
      "expanded_url" : "http:\/\/ow.ly\/ZbPGf",
      "display_url" : "ow.ly\/ZbPGf"
    } ]
  },
  "geo" : { },
  "id_str" : "708386652192940032",
  "text" : "RT @jfxberns: Singapore launches fellowship to attract world\u2019s best data scientists and technologists https:\/\/t.co\/LAfGPmPpuS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/LAfGPmPpuS",
        "expanded_url" : "http:\/\/ow.ly\/ZbPGf",
        "display_url" : "ow.ly\/ZbPGf"
      } ]
    },
    "geo" : { },
    "id_str" : "707030891093172224",
    "text" : "Singapore launches fellowship to attract world\u2019s best data scientists and technologists https:\/\/t.co\/LAfGPmPpuS",
    "id" : 707030891093172224,
    "created_at" : "2016-03-08 02:31:15 +0000",
    "user" : {
      "name" : "John FX Berns",
      "screen_name" : "jfxberns",
      "protected" : false,
      "id_str" : "6024062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/616093486631157760\/LG205jzc_normal.jpg",
      "id" : 6024062,
      "verified" : false
    }
  },
  "id" : 708386652192940032,
  "created_at" : "2016-03-11 20:18:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter K",
      "screen_name" : "peterkz_swe",
      "indices" : [ 3, 15 ],
      "id_str" : "8769542",
      "id" : 8769542
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "odnextsteps",
      "indices" : [ 119, 131 ]
    } ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/f8Ib80Jfc3",
      "expanded_url" : "https:\/\/cran.r-project.org\/web\/packages\/eurostat\/index.html",
      "display_url" : "cran.r-project.org\/web\/packages\/e\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708282409939308545",
  "text" : "RT @peterkz_swe: Eurostat has an R package to make it easy to work with open data immediately: https:\/\/t.co\/f8Ib80Jfc3 #odnextsteps #oppnad\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "odnextsteps",
        "indices" : [ 102, 114 ]
      }, {
        "text" : "oppnadata",
        "indices" : [ 115, 125 ]
      } ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/f8Ib80Jfc3",
        "expanded_url" : "https:\/\/cran.r-project.org\/web\/packages\/eurostat\/index.html",
        "display_url" : "cran.r-project.org\/web\/packages\/e\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "708263003809038336",
    "text" : "Eurostat has an R package to make it easy to work with open data immediately: https:\/\/t.co\/f8Ib80Jfc3 #odnextsteps #oppnadata",
    "id" : 708263003809038336,
    "created_at" : "2016-03-11 12:07:13 +0000",
    "user" : {
      "name" : "Peter K",
      "screen_name" : "peterkz_swe",
      "protected" : false,
      "id_str" : "8769542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/781206851677515776\/MWNHQro4_normal.jpg",
      "id" : 8769542,
      "verified" : false
    }
  },
  "id" : 708282409939308545,
  "created_at" : "2016-03-11 13:24:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/aDwjNffuEI",
      "expanded_url" : "https:\/\/twitter.com\/IEDR_dot_ie\/status\/708225621374210048",
      "display_url" : "twitter.com\/IEDR_dot_ie\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708280886542901248",
  "text" : "#getoptimise  https:\/\/t.co\/aDwjNffuEI",
  "id" : 708280886542901248,
  "created_at" : "2016-03-11 13:18:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mashable",
      "screen_name" : "mashable",
      "indices" : [ 101, 110 ],
      "id_str" : "972651",
      "id" : 972651
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/HrRjWdsmA5",
      "expanded_url" : "http:\/\/mashable.com\/2016\/03\/09\/singapore-prime-minister-eclipse-pictures\/#it4FR5e4a05P",
      "display_url" : "mashable.com\/2016\/03\/09\/sin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708242007605514241",
  "text" : "Singapore prime minister calls for solar eclipse photos, hilarity ensues https:\/\/t.co\/HrRjWdsmA5 via @mashable",
  "id" : 708242007605514241,
  "created_at" : "2016-03-11 10:43:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alissa Walker",
      "screen_name" : "awalkerinLA",
      "indices" : [ 3, 15 ],
      "id_str" : "14247155",
      "id" : 14247155
    }, {
      "name" : "\u1D04\u029C\u1D00\u1D18\u1D18\u1D07\u029F\u029F \u1D07\u029F\u029F\u026As\u1D0F\u0274\u0669( \u141B )\u0648",
      "screen_name" : "ChappellTracker",
      "indices" : [ 34, 50 ],
      "id_str" : "22826779",
      "id" : 22826779
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/TmCl2s4qmt",
      "expanded_url" : "http:\/\/gizmodo.com\/these-hilarious-captions-lampooning-ludicrous-tech-comp-1764166554",
      "display_url" : "gizmodo.com\/these-hilariou\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "708202228448768001",
  "text" : "RT @awalkerinLA: These stories by @ChappellTracker that eviscerate tech bro decor are the best thing ever https:\/\/t.co\/TmCl2s4qmt https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\u1D04\u029C\u1D00\u1D18\u1D18\u1D07\u029F\u029F \u1D07\u029F\u029F\u026As\u1D0F\u0274\u0669( \u141B )\u0648",
        "screen_name" : "ChappellTracker",
        "indices" : [ 17, 33 ],
        "id_str" : "22826779",
        "id" : 22826779
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/awalkerinLA\/status\/708087675396366337\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/C7446yotM4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdOh_NpUIAAbpzL.jpg",
        "id_str" : "708087673794076672",
        "id" : 708087673794076672,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdOh_NpUIAAbpzL.jpg",
        "sizes" : [ {
          "h" : 578,
          "resize" : "fit",
          "w" : 723
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 544,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 723
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 723
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/C7446yotM4"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/TmCl2s4qmt",
        "expanded_url" : "http:\/\/gizmodo.com\/these-hilarious-captions-lampooning-ludicrous-tech-comp-1764166554",
        "display_url" : "gizmodo.com\/these-hilariou\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "708087675396366337",
    "text" : "These stories by @ChappellTracker that eviscerate tech bro decor are the best thing ever https:\/\/t.co\/TmCl2s4qmt https:\/\/t.co\/C7446yotM4",
    "id" : 708087675396366337,
    "created_at" : "2016-03-11 00:30:32 +0000",
    "user" : {
      "name" : "Alissa Walker",
      "screen_name" : "awalkerinLA",
      "protected" : false,
      "id_str" : "14247155",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1018901698020904960\/Xht9L3n1_normal.jpg",
      "id" : 14247155,
      "verified" : true
    }
  },
  "id" : 708202228448768001,
  "created_at" : "2016-03-11 08:05:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/WE9tJJbwRj",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/another-petition-against-najib-remove-074352062.html?soc_src=social-sh&soc_trk=tw",
      "display_url" : "sg.news.yahoo.com\/another-petiti\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707929122820005890",
  "text" : "Petition against Malaysia PM to remove portrait at UK\u2019s Nottingham University https:\/\/t.co\/WE9tJJbwRj",
  "id" : 707929122820005890,
  "created_at" : "2016-03-10 14:00:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ConferenceIdea",
      "indices" : [ 0, 15 ]
    } ],
    "urls" : [ {
      "indices" : [ 16, 39 ],
      "url" : "https:\/\/t.co\/jWpkv4bmuP",
      "expanded_url" : "https:\/\/twitter.com\/EJMcEwan\/status\/707866424342093824",
      "display_url" : "twitter.com\/EJMcEwan\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707871488498606081",
  "text" : "#ConferenceIdea https:\/\/t.co\/jWpkv4bmuP",
  "id" : 707871488498606081,
  "created_at" : "2016-03-10 10:11:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Henry",
      "screen_name" : "Mark_J_Henry",
      "indices" : [ 3, 16 ],
      "id_str" : "45539859",
      "id" : 45539859
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CAPAsummit",
      "indices" : [ 121, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707862003759648768",
  "text" : "RT @Mark_J_Henry: Airlines in Asia \/ Middle East have more airplanes on order than European+N.American airlines combined #CAPAsummit https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Mark_J_Henry\/status\/707861890530222080\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/fBxhKBEEZw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdLUomXWIAABzwq.jpg",
        "id_str" : "707861885408911360",
        "id" : 707861885408911360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdLUomXWIAABzwq.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fBxhKBEEZw"
      } ],
      "hashtags" : [ {
        "text" : "CAPAsummit",
        "indices" : [ 103, 114 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707861890530222080",
    "text" : "Airlines in Asia \/ Middle East have more airplanes on order than European+N.American airlines combined #CAPAsummit https:\/\/t.co\/fBxhKBEEZw",
    "id" : 707861890530222080,
    "created_at" : "2016-03-10 09:33:20 +0000",
    "user" : {
      "name" : "Mark Henry",
      "screen_name" : "Mark_J_Henry",
      "protected" : false,
      "id_str" : "45539859",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/421404033039859712\/Uph4FkzB_normal.jpeg",
      "id" : 45539859,
      "verified" : false
    }
  },
  "id" : 707862003759648768,
  "created_at" : "2016-03-10 09:33:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philip Bromwell",
      "screen_name" : "philipbromwell",
      "indices" : [ 3, 18 ],
      "id_str" : "241122100",
      "id" : 241122100
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/philipbromwell\/status\/707824293195948033\/photo\/1",
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/GLBBs6iO8B",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdKyZ-OWwAAyxMv.jpg",
      "id_str" : "707824250720272384",
      "id" : 707824250720272384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdKyZ-OWwAAyxMv.jpg",
      "sizes" : [ {
        "h" : 1732,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1732,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 575,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1015,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GLBBs6iO8B"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707858579534061572",
  "text" : "RT @philipbromwell: Thursday's front pages in one tweet https:\/\/t.co\/GLBBs6iO8B",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/philipbromwell\/status\/707824293195948033\/photo\/1",
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/GLBBs6iO8B",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdKyZ-OWwAAyxMv.jpg",
        "id_str" : "707824250720272384",
        "id" : 707824250720272384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdKyZ-OWwAAyxMv.jpg",
        "sizes" : [ {
          "h" : 1732,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1732,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 575,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1015,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GLBBs6iO8B"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707824293195948033",
    "text" : "Thursday's front pages in one tweet https:\/\/t.co\/GLBBs6iO8B",
    "id" : 707824293195948033,
    "created_at" : "2016-03-10 07:03:56 +0000",
    "user" : {
      "name" : "Philip Bromwell",
      "screen_name" : "philipbromwell",
      "protected" : false,
      "id_str" : "241122100",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665681336997425152\/li9qAdlp_normal.jpg",
      "id" : 241122100,
      "verified" : true
    }
  },
  "id" : 707858579534061572,
  "created_at" : "2016-03-10 09:20:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707721413109665792",
  "text" : "Just did an audit and I got 10 email address account...",
  "id" : 707721413109665792,
  "created_at" : "2016-03-10 00:15:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 3, 14 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707695742035619840",
  "text" : "RT @davegantly: US gun-rights activist who has guns \"to protect her child\", gets shot in the back by her child. You couldn't make it up.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707691720369152001",
    "text" : "US gun-rights activist who has guns \"to protect her child\", gets shot in the back by her child. You couldn't make it up.",
    "id" : 707691720369152001,
    "created_at" : "2016-03-09 22:17:09 +0000",
    "user" : {
      "name" : "David",
      "screen_name" : "davegantly",
      "protected" : false,
      "id_str" : "16738907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001158218519863296\/rY-yu2r7_normal.jpg",
      "id" : 16738907,
      "verified" : false
    }
  },
  "id" : 707695742035619840,
  "created_at" : "2016-03-09 22:33:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IWD2016",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/LbCj7fAyJS",
      "expanded_url" : "https:\/\/twitter.com\/coryannj\/status\/574540645592866816",
      "display_url" : "twitter.com\/coryannj\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707620689960951809",
  "text" : "#IWD2016  https:\/\/t.co\/LbCj7fAyJS",
  "id" : 707620689960951809,
  "created_at" : "2016-03-09 17:34:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/5NbmwhzPp7",
      "expanded_url" : "http:\/\/www.wired.com\/2016\/03\/europe-youll-need-vpn-see-real-google-search-results\/",
      "display_url" : "wired.com\/2016\/03\/europe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707602284147056640",
  "text" : "RT @pdscott: In Europe, You\u2019ll Need a VPN to See Real Google Search Results https:\/\/t.co\/5NbmwhzPp7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/5NbmwhzPp7",
        "expanded_url" : "http:\/\/www.wired.com\/2016\/03\/europe-youll-need-vpn-see-real-google-search-results\/",
        "display_url" : "wired.com\/2016\/03\/europe\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "707488479832383488",
    "text" : "In Europe, You\u2019ll Need a VPN to See Real Google Search Results https:\/\/t.co\/5NbmwhzPp7",
    "id" : 707488479832383488,
    "created_at" : "2016-03-09 08:49:32 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 707602284147056640,
  "created_at" : "2016-03-09 16:21:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707538687769354240",
  "text" : "Which tools you use to collect, organise, cite, and share your research sources? Thanks",
  "id" : 707538687769354240,
  "created_at" : "2016-03-09 12:09:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/S5WzwGdhNE",
      "expanded_url" : "https:\/\/support.google.com\/analytics\/answer\/2844870?hl=en",
      "display_url" : "support.google.com\/analytics\/answ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707530504678088704",
  "text" : "The statistical model behind Content Experiments, a tool to create A\/B tests within Google Analytics. https:\/\/t.co\/S5WzwGdhNE",
  "id" : 707530504678088704,
  "created_at" : "2016-03-09 11:36:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707518701923336192",
  "text" : "2. Let start now from my Twitter follower list one by one who have an impact on me.",
  "id" : 707518701923336192,
  "created_at" : "2016-03-09 10:49:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707518641479294976",
  "text" : "1. Why do we have to wait for someone to die before we start paying tribute?",
  "id" : 707518641479294976,
  "created_at" : "2016-03-09 10:49:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/9lbZZwz5DR",
      "expanded_url" : "https:\/\/twitter.com\/Cbarron15\/status\/707511348599590912",
      "display_url" : "twitter.com\/Cbarron15\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707515668669833216",
  "text" : "interested to know what are the purchase and which segmentation? https:\/\/t.co\/9lbZZwz5DR",
  "id" : 707515668669833216,
  "created_at" : "2016-03-09 10:37:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GMIC",
      "screen_name" : "theGMIC",
      "indices" : [ 3, 11 ],
      "id_str" : "187044118",
      "id" : 187044118
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GMICTLV",
      "indices" : [ 111, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707482086534488064",
  "text" : "RT @theGMIC: We are delighted to share that senior leaders from China\u2019s internet giants will be taking part in #GMICTLV https:\/\/t.co\/vG6zzI\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/theGMIC\/status\/707473699042099200\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/vG6zzIBlZH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdFzlCVXIAAgxEi.jpg",
        "id_str" : "707473696592699392",
        "id" : 707473696592699392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdFzlCVXIAAgxEi.jpg",
        "sizes" : [ {
          "h" : 720,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1250,
          "resize" : "fit",
          "w" : 2083
        }, {
          "h" : 1229,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 408,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vG6zzIBlZH"
      } ],
      "hashtags" : [ {
        "text" : "GMICTLV",
        "indices" : [ 98, 106 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707473699042099200",
    "text" : "We are delighted to share that senior leaders from China\u2019s internet giants will be taking part in #GMICTLV https:\/\/t.co\/vG6zzIBlZH",
    "id" : 707473699042099200,
    "created_at" : "2016-03-09 07:50:48 +0000",
    "user" : {
      "name" : "GMIC",
      "screen_name" : "theGMIC",
      "protected" : false,
      "id_str" : "187044118",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/491212654917087232\/BYAjkoom_normal.png",
      "id" : 187044118,
      "verified" : false
    }
  },
  "id" : 707482086534488064,
  "created_at" : "2016-03-09 08:24:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lindsey Fitzharris",
      "screen_name" : "DrLindseyFitz",
      "indices" : [ 3, 17 ],
      "id_str" : "229509740",
      "id" : 229509740
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IWD2016",
      "indices" : [ 123, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707473241787527168",
  "text" : "RT @DrLindseyFitz: Marie Curie's notebooks are still radioactive. Researchers wishing to view them must sign a disclaimer. #IWD2016 https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DrLindseyFitz\/status\/707301634657927168\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/mym7H5CCpe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdDXFpwW0AENY7J.jpg",
        "id_str" : "707301633605160961",
        "id" : 707301633605160961,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdDXFpwW0AENY7J.jpg",
        "sizes" : [ {
          "h" : 407,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 346,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 407,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 407,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mym7H5CCpe"
      } ],
      "hashtags" : [ {
        "text" : "IWD2016",
        "indices" : [ 104, 112 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707301634657927168",
    "text" : "Marie Curie's notebooks are still radioactive. Researchers wishing to view them must sign a disclaimer. #IWD2016 https:\/\/t.co\/mym7H5CCpe",
    "id" : 707301634657927168,
    "created_at" : "2016-03-08 20:27:05 +0000",
    "user" : {
      "name" : "Lindsey Fitzharris",
      "screen_name" : "DrLindseyFitz",
      "protected" : false,
      "id_str" : "229509740",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/698912111297171456\/Qeu3GBkp_normal.jpg",
      "id" : 229509740,
      "verified" : true
    }
  },
  "id" : 707473241787527168,
  "created_at" : "2016-03-09 07:48:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707471901166313472",
  "text" : "For 3 days this week, school days in Public school here are allocated to communion...that could have spend on sports, science and coding.",
  "id" : 707471901166313472,
  "created_at" : "2016-03-09 07:43:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CuteCatriona For YES \uD83D\uDDF3",
      "screen_name" : "CuteCatriona",
      "indices" : [ 0, 13 ],
      "id_str" : "497270620",
      "id" : 497270620
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "707148711877484544",
  "geo" : { },
  "id_str" : "707471140965502977",
  "in_reply_to_user_id" : 497270620,
  "text" : "@CuteCatriona can't go wrong buying electronic in this 2 Asia cities. I think camera is cheaper in HK.",
  "id" : 707471140965502977,
  "in_reply_to_status_id" : 707148711877484544,
  "created_at" : "2016-03-09 07:40:38 +0000",
  "in_reply_to_screen_name" : "CuteCatriona",
  "in_reply_to_user_id_str" : "497270620",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707464959727370240",
  "text" : "Looking to turn tweets into a book. Any recommendation out there? Thanks.",
  "id" : 707464959727370240,
  "created_at" : "2016-03-09 07:16:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "indices" : [ 3, 11 ],
      "id_str" : "15019743",
      "id" : 15019743
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/5zR7FChQfq",
      "expanded_url" : "http:\/\/m.independent.ie\/irish-news\/water\/irish-water-crisis\/irish-words-on-northern-ireland-manhole-covers-sparks-unionists-demand-for-their-removal-34520235.html",
      "display_url" : "m.independent.ie\/irish-news\/wat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707319974352842752",
  "text" : "RT @2bscene: How sad is this, it's no wonder things will never move on... https:\/\/t.co\/5zR7FChQfq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/5zR7FChQfq",
        "expanded_url" : "http:\/\/m.independent.ie\/irish-news\/water\/irish-water-crisis\/irish-words-on-northern-ireland-manhole-covers-sparks-unionists-demand-for-their-removal-34520235.html",
        "display_url" : "m.independent.ie\/irish-news\/wat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "707318709799526400",
    "text" : "How sad is this, it's no wonder things will never move on... https:\/\/t.co\/5zR7FChQfq",
    "id" : 707318709799526400,
    "created_at" : "2016-03-08 21:34:56 +0000",
    "user" : {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "protected" : false,
      "id_str" : "15019743",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/779435193312608256\/NJW57jaX_normal.jpg",
      "id" : 15019743,
      "verified" : false
    }
  },
  "id" : 707319974352842752,
  "created_at" : "2016-03-08 21:39:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/cmnSqHT4X0",
      "expanded_url" : "http:\/\/www.dt.fee.unicamp.br\/~tiago\/smsspamcollection\/",
      "display_url" : "dt.fee.unicamp.br\/~tiago\/smsspam\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707304817924960256",
  "text" : "Today lesson, I came across some Singlish in a research data. https:\/\/t.co\/cmnSqHT4X0",
  "id" : 707304817924960256,
  "created_at" : "2016-03-08 20:39:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "indices" : [ 3, 13 ],
      "id_str" : "17524108",
      "id" : 17524108
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 15, 21 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ericknopf\/status\/707289143588495363\/photo\/1",
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/5V8xy4K6Z7",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CdDLuflUMAEh9AS.jpg",
      "id_str" : "707289141109600257",
      "id" : 707289141109600257,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdDLuflUMAEh9AS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 672
      }, {
        "h" : 682,
        "resize" : "fit",
        "w" : 674
      }, {
        "h" : 682,
        "resize" : "fit",
        "w" : 674
      }, {
        "h" : 682,
        "resize" : "fit",
        "w" : 674
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5V8xy4K6Z7"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707304008227098625",
  "text" : "RT @ericknopf: @mryap https:\/\/t.co\/5V8xy4K6Z7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ericknopf\/status\/707289143588495363\/photo\/1",
        "indices" : [ 7, 30 ],
        "url" : "https:\/\/t.co\/5V8xy4K6Z7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdDLuflUMAEh9AS.jpg",
        "id_str" : "707289141109600257",
        "id" : 707289141109600257,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdDLuflUMAEh9AS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 672
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 674
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 674
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 674
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5V8xy4K6Z7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "707239262874374145",
    "geo" : { },
    "id_str" : "707289143588495363",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap https:\/\/t.co\/5V8xy4K6Z7",
    "id" : 707289143588495363,
    "in_reply_to_status_id" : 707239262874374145,
    "created_at" : "2016-03-08 19:37:27 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "protected" : false,
      "id_str" : "17524108",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460579737018970113\/3x_hi1Zc_normal.jpeg",
      "id" : 17524108,
      "verified" : false
    }
  },
  "id" : 707304008227098625,
  "created_at" : "2016-03-08 20:36:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 0, 10 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 34 ],
      "url" : "https:\/\/t.co\/9Hobl28j1y",
      "expanded_url" : "http:\/\/compub.com\/our-stores\/",
      "display_url" : "compub.com\/our-stores\/"
    } ]
  },
  "in_reply_to_status_id_str" : "707258456319909889",
  "geo" : { },
  "id_str" : "707260611575599105",
  "in_reply_to_user_id" : 18849187,
  "text" : "@barryhand https:\/\/t.co\/9Hobl28j1y",
  "id" : 707260611575599105,
  "in_reply_to_status_id" : 707258456319909889,
  "created_at" : "2016-03-08 17:44:04 +0000",
  "in_reply_to_screen_name" : "barryhand",
  "in_reply_to_user_id_str" : "18849187",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/Bh2zPDu44i",
      "expanded_url" : "https:\/\/twitter.com\/ericknopf\/status\/707238955024982017",
      "display_url" : "twitter.com\/ericknopf\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707239262874374145",
  "text" : "or a lego brick  https:\/\/t.co\/Bh2zPDu44i",
  "id" : 707239262874374145,
  "created_at" : "2016-03-08 16:19:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Ascent of Woman",
      "screen_name" : "ascentofwoman",
      "indices" : [ 3, 17 ],
      "id_str" : "3270500760",
      "id" : 3270500760
    }, {
      "name" : "Netflix US",
      "screen_name" : "netflix",
      "indices" : [ 116, 124 ],
      "id_str" : "16573941",
      "id" : 16573941
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "InternationalWomensDay",
      "indices" : [ 56, 79 ]
    }, {
      "text" : "TheAscentofWoman",
      "indices" : [ 95, 112 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707229364216324096",
  "text" : "RT @ascentofwoman: Remembering those who went before on #InternationalWomensDay: Empress Wu on #TheAscentofWoman on @NETFLIX April 1 https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Netflix US",
        "screen_name" : "netflix",
        "indices" : [ 97, 105 ],
        "id_str" : "16573941",
        "id" : 16573941
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ascentofwoman\/status\/707226956610330625\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/g5bmxICdMS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdCTK2_XIAExO_E.jpg",
        "id_str" : "707226956266414081",
        "id" : 707226956266414081,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdCTK2_XIAExO_E.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/g5bmxICdMS"
      } ],
      "hashtags" : [ {
        "text" : "InternationalWomensDay",
        "indices" : [ 37, 60 ]
      }, {
        "text" : "TheAscentofWoman",
        "indices" : [ 76, 93 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "707226956610330625",
    "text" : "Remembering those who went before on #InternationalWomensDay: Empress Wu on #TheAscentofWoman on @NETFLIX April 1 https:\/\/t.co\/g5bmxICdMS",
    "id" : 707226956610330625,
    "created_at" : "2016-03-08 15:30:20 +0000",
    "user" : {
      "name" : "The Ascent of Woman",
      "screen_name" : "ascentofwoman",
      "protected" : false,
      "id_str" : "3270500760",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/618252716264026112\/vqqChM0n_normal.jpg",
      "id" : 3270500760,
      "verified" : false
    }
  },
  "id" : 707229364216324096,
  "created_at" : "2016-03-08 15:39:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707229197748584449",
  "text" : "RT @SimonPRepublic: On International Women's Day We Give Solidarity to the Women From Ireland Who Travel Here for Their Abortions https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/q6ZYbpH3Ly",
        "expanded_url" : "http:\/\/www.huffingtonpost.co.uk\/kerry-abel\/ireland-abortion-rights_b_9381818.html",
        "display_url" : "huffingtonpost.co.uk\/kerry-abel\/ire\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "707228051831177216",
    "text" : "On International Women's Day We Give Solidarity to the Women From Ireland Who Travel Here for Their Abortions https:\/\/t.co\/q6ZYbpH3Ly",
    "id" : 707228051831177216,
    "created_at" : "2016-03-08 15:34:41 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 707229197748584449,
  "created_at" : "2016-03-08 15:39:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Connolly for Kids",
      "screen_name" : "ConnollyForKids",
      "indices" : [ 3, 19 ],
      "id_str" : "4825761837",
      "id" : 4825761837
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "707187821656801281",
  "text" : "RT @ConnollyForKids: If you want the Children's Hospital to be built on 145 acre M50 Connolly site instead of St James's sign here: https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/zZg4a65Fsy",
        "expanded_url" : "http:\/\/connollyforkidshospital.com",
        "display_url" : "connollyforkidshospital.com"
      } ]
    },
    "geo" : { },
    "id_str" : "706950861055004678",
    "text" : "If you want the Children's Hospital to be built on 145 acre M50 Connolly site instead of St James's sign here: https:\/\/t.co\/zZg4a65Fsy",
    "id" : 706950861055004678,
    "created_at" : "2016-03-07 21:13:14 +0000",
    "user" : {
      "name" : "Connolly for Kids",
      "screen_name" : "ConnollyForKids",
      "protected" : false,
      "id_str" : "4825761837",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/695171378547417088\/91FBY3cu_normal.png",
      "id" : 4825761837,
      "verified" : false
    }
  },
  "id" : 707187821656801281,
  "created_at" : "2016-03-08 12:54:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/wVJGPgBLjn",
      "expanded_url" : "http:\/\/fastml.com\/the-emperors-new-clothes-distributed-machine-learning\/",
      "display_url" : "fastml.com\/the-emperors-n\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707157954932383744",
  "text" : "You can train machine learning models on the RAM of a single high-end server without distributed computing. https:\/\/t.co\/wVJGPgBLjn",
  "id" : 707157954932383744,
  "created_at" : "2016-03-08 10:56:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dermot Casey",
      "screen_name" : "dermotcasey",
      "indices" : [ 3, 15 ],
      "id_str" : "6297382",
      "id" : 6297382
    }, {
      "name" : "NDRC",
      "screen_name" : "NDRC_hq",
      "indices" : [ 47, 55 ],
      "id_str" : "42607907",
      "id" : 42607907
    }, {
      "name" : "Enterprise Ireland",
      "screen_name" : "Entirl",
      "indices" : [ 60, 67 ],
      "id_str" : "60575311",
      "id" : 60575311
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iwd2016",
      "indices" : [ 116, 124 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/8rna0Yu7qI",
      "expanded_url" : "http:\/\/www.ndrc.ie\/female-founders\/",
      "display_url" : "ndrc.ie\/female-founder\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707156617037529088",
  "text" : "RT @dermotcasey: Female founder or co-founder? @NDRC_hq and @Entirl want to invest in you  https:\/\/t.co\/8rna0Yu7qI  #iwd2016 https:\/\/t.co\/T\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "NDRC",
        "screen_name" : "NDRC_hq",
        "indices" : [ 30, 38 ],
        "id_str" : "42607907",
        "id" : 42607907
      }, {
        "name" : "Enterprise Ireland",
        "screen_name" : "Entirl",
        "indices" : [ 43, 50 ],
        "id_str" : "60575311",
        "id" : 60575311
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dermotcasey\/status\/707142388293214208\/photo\/1",
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/TztwKguesQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CdBGQTUXIAA94aU.jpg",
        "id_str" : "707142387374694400",
        "id" : 707142387374694400,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CdBGQTUXIAA94aU.jpg",
        "sizes" : [ {
          "h" : 603,
          "resize" : "fit",
          "w" : 616
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 616
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 616
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 616
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/TztwKguesQ"
      } ],
      "hashtags" : [ {
        "text" : "iwd2016",
        "indices" : [ 99, 107 ]
      } ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/8rna0Yu7qI",
        "expanded_url" : "http:\/\/www.ndrc.ie\/female-founders\/",
        "display_url" : "ndrc.ie\/female-founder\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "707142388293214208",
    "text" : "Female founder or co-founder? @NDRC_hq and @Entirl want to invest in you  https:\/\/t.co\/8rna0Yu7qI  #iwd2016 https:\/\/t.co\/TztwKguesQ",
    "id" : 707142388293214208,
    "created_at" : "2016-03-08 09:54:18 +0000",
    "user" : {
      "name" : "Dermot Casey",
      "screen_name" : "dermotcasey",
      "protected" : false,
      "id_str" : "6297382",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004359497559740416\/CCWk8-wq_normal.jpg",
      "id" : 6297382,
      "verified" : false
    }
  },
  "id" : 707156617037529088,
  "created_at" : "2016-03-08 10:50:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marie Keating Fndtn",
      "screen_name" : "MarieKeating",
      "indices" : [ 3, 16 ],
      "id_str" : "72808326",
      "id" : 72808326
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "YourHealthYourChoice",
      "indices" : [ 96, 117 ]
    } ],
    "urls" : [ {
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/uBcJi0IMEU",
      "expanded_url" : "https:\/\/cards.twitter.com\/cards\/18ce53x2452\/1foxd",
      "display_url" : "cards.twitter.com\/cards\/18ce53x2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "707152447207292929",
  "text" : "RT @MarieKeating: Alcohol causes 7 types of cancer, including breast, mouth &amp; bowel cancers\n#YourHealthYourChoice\n https:\/\/t.co\/uBcJi0IMEU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "YourHealthYourChoice",
        "indices" : [ 78, 99 ]
      } ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/uBcJi0IMEU",
        "expanded_url" : "https:\/\/cards.twitter.com\/cards\/18ce53x2452\/1foxd",
        "display_url" : "cards.twitter.com\/cards\/18ce53x2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "695564401865154561",
    "text" : "Alcohol causes 7 types of cancer, including breast, mouth &amp; bowel cancers\n#YourHealthYourChoice\n https:\/\/t.co\/uBcJi0IMEU",
    "id" : 695564401865154561,
    "created_at" : "2016-02-05 11:07:31 +0000",
    "user" : {
      "name" : "Marie Keating Fndtn",
      "screen_name" : "MarieKeating",
      "protected" : false,
      "id_str" : "72808326",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/405986017\/Marie-Keating-Logo_Original_normal.jpg",
      "id" : 72808326,
      "verified" : true
    }
  },
  "id" : 707152447207292929,
  "created_at" : "2016-03-08 10:34:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Bull",
      "screen_name" : "larrymeath",
      "indices" : [ 3, 14 ],
      "id_str" : "277717058",
      "id" : 277717058
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706962885872914434",
  "text" : "RT @larrymeath: Frontline Doctors: Winter Migrant Crisis on BBC1\nHeart-breaking, simply heart-breaking.\nAlways kids who suffer the most.\n\n#\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RefugeeCrisis",
        "indices" : [ 122, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "706961953059692544",
    "text" : "Frontline Doctors: Winter Migrant Crisis on BBC1\nHeart-breaking, simply heart-breaking.\nAlways kids who suffer the most.\n\n#RefugeeCrisis",
    "id" : 706961953059692544,
    "created_at" : "2016-03-07 21:57:18 +0000",
    "user" : {
      "name" : "John Bull",
      "screen_name" : "larrymeath",
      "protected" : false,
      "id_str" : "277717058",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/979773060650885121\/bxIP0-i__normal.jpg",
      "id" : 277717058,
      "verified" : false
    }
  },
  "id" : 706962885872914434,
  "created_at" : "2016-03-07 22:01:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Chowdhry",
      "screen_name" : "paulchowdhry",
      "indices" : [ 0, 13 ],
      "id_str" : "21948974",
      "id" : 21948974
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/uHBXSbgPLz",
      "expanded_url" : "https:\/\/twitter.com\/TSTContinental\/status\/706213969237811201",
      "display_url" : "twitter.com\/TSTContinental\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706818536832954369",
  "in_reply_to_user_id" : 21948974,
  "text" : "@paulchowdhry This might be a good material for your next gig :) https:\/\/t.co\/uHBXSbgPLz",
  "id" : 706818536832954369,
  "created_at" : "2016-03-07 12:27:25 +0000",
  "in_reply_to_screen_name" : "paulchowdhry",
  "in_reply_to_user_id_str" : "21948974",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/vLDRHqYbRL",
      "expanded_url" : "https:\/\/twitter.com\/ladystormhold\/status\/706816439433498624",
      "display_url" : "twitter.com\/ladystormhold\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706817420749574144",
  "text" : "I always carry my ID card just like when I am back home. For some, this is an alien concept.  https:\/\/t.co\/vLDRHqYbRL",
  "id" : 706817420749574144,
  "created_at" : "2016-03-07 12:22:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DreamHost Status",
      "screen_name" : "dhstatus",
      "indices" : [ 0, 9 ],
      "id_str" : "8210302",
      "id" : 8210302
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706789414849155072",
  "in_reply_to_user_id" : 8210302,
  "text" : "@dhstatus I unable to access my website for the past 15 minutes. What the story? Thanks",
  "id" : 706789414849155072,
  "created_at" : "2016-03-07 10:31:42 +0000",
  "in_reply_to_screen_name" : "dhstatus",
  "in_reply_to_user_id_str" : "8210302",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/uWiyBhyakp",
      "expanded_url" : "http:\/\/www.bbc.com\/travel\/story\/20151105-the-singapore-dish-worth-a-15-hour-flight",
      "display_url" : "bbc.com\/travel\/story\/2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706783894415212545",
  "text" : "The dish worth a 15-hour flight https:\/\/t.co\/uWiyBhyakp",
  "id" : 706783894415212545,
  "created_at" : "2016-03-07 10:09:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/QkD2oTzkLZ",
      "expanded_url" : "http:\/\/nextshark.com\/joe-choe-donald-trump-asian-question\/",
      "display_url" : "nextshark.com\/joe-choe-donal\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706636207153946624",
  "text" : "RT @yapphenghui: Donald Trump Asked a Harvard Student The One Question Asian Americans Hate Being Asked https:\/\/t.co\/QkD2oTzkLZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 110 ],
        "url" : "https:\/\/t.co\/QkD2oTzkLZ",
        "expanded_url" : "http:\/\/nextshark.com\/joe-choe-donald-trump-asian-question\/",
        "display_url" : "nextshark.com\/joe-choe-donal\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "706633342565765121",
    "text" : "Donald Trump Asked a Harvard Student The One Question Asian Americans Hate Being Asked https:\/\/t.co\/QkD2oTzkLZ",
    "id" : 706633342565765121,
    "created_at" : "2016-03-07 00:11:32 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 706636207153946624,
  "created_at" : "2016-03-07 00:22:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gmail",
      "screen_name" : "gmail",
      "indices" : [ 3, 9 ],
      "id_str" : "38679388",
      "id" : 38679388
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RIP",
      "indices" : [ 92, 96 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706627898057297920",
  "text" : "RT @gmail: Thank you, Ray Tomlinson, for inventing email and putting the @ sign on the map. #RIP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RIP",
        "indices" : [ 81, 85 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "706534596767141889",
    "text" : "Thank you, Ray Tomlinson, for inventing email and putting the @ sign on the map. #RIP",
    "id" : 706534596767141889,
    "created_at" : "2016-03-06 17:39:09 +0000",
    "user" : {
      "name" : "Gmail",
      "screen_name" : "gmail",
      "protected" : false,
      "id_str" : "38679388",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/892067670970978305\/_K34MGL8_normal.jpg",
      "id" : 38679388,
      "verified" : true
    }
  },
  "id" : 706627898057297920,
  "created_at" : "2016-03-06 23:49:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/706626011954331648\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/HnPyqUrKaw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc5wnP_WwAAE_QD.jpg",
      "id_str" : "706626011153219584",
      "id" : 706626011153219584,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc5wnP_WwAAE_QD.jpg",
      "sizes" : [ {
        "h" : 190,
        "resize" : "fit",
        "w" : 580
      }, {
        "h" : 190,
        "resize" : "fit",
        "w" : 580
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 190,
        "resize" : "fit",
        "w" : 580
      }, {
        "h" : 190,
        "resize" : "fit",
        "w" : 580
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HnPyqUrKaw"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706626011954331648",
  "text" : "Someone reckon I'm talking about the other modeling. (i.e.catwalk) https:\/\/t.co\/HnPyqUrKaw",
  "id" : 706626011954331648,
  "created_at" : "2016-03-06 23:42:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/INIqrUXM2N",
      "expanded_url" : "https:\/\/www.kaggle.com\/c\/titanic",
      "display_url" : "kaggle.com\/c\/titanic"
    } ]
  },
  "geo" : { },
  "id_str" : "706603391460499456",
  "text" : "First time on a competitive predictive modeling. https:\/\/t.co\/INIqrUXM2N",
  "id" : 706603391460499456,
  "created_at" : "2016-03-06 22:12:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Dixon",
      "screen_name" : "cdixon",
      "indices" : [ 3, 10 ],
      "id_str" : "2529971",
      "id" : 2529971
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cdixon\/status\/706206828925992961\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/2jP7EsCdqb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CczzXm1WEAAoN44.jpg",
      "id_str" : "706206828477157376",
      "id" : 706206828477157376,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CczzXm1WEAAoN44.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 557
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 557
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 557
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 557
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2jP7EsCdqb"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/93tAIVT4Yn",
      "expanded_url" : "https:\/\/www.thestar.com\/news\/insight\/2016\/01\/16\/when-us-air-force-discovered-the-flaw-of-averages.html",
      "display_url" : "thestar.com\/news\/insight\/2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706575376642936832",
  "text" : "RT @cdixon: \"Any system designed around the average person is doomed to fail.\" https:\/\/t.co\/93tAIVT4Yn https:\/\/t.co\/2jP7EsCdqb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cdixon\/status\/706206828925992961\/photo\/1",
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/2jP7EsCdqb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CczzXm1WEAAoN44.jpg",
        "id_str" : "706206828477157376",
        "id" : 706206828477157376,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CczzXm1WEAAoN44.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 557
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 557
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 557
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 557
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2jP7EsCdqb"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/93tAIVT4Yn",
        "expanded_url" : "https:\/\/www.thestar.com\/news\/insight\/2016\/01\/16\/when-us-air-force-discovered-the-flaw-of-averages.html",
        "display_url" : "thestar.com\/news\/insight\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "706206828925992961",
    "text" : "\"Any system designed around the average person is doomed to fail.\" https:\/\/t.co\/93tAIVT4Yn https:\/\/t.co\/2jP7EsCdqb",
    "id" : 706206828925992961,
    "created_at" : "2016-03-05 19:56:43 +0000",
    "user" : {
      "name" : "Chris Dixon",
      "screen_name" : "cdixon",
      "protected" : false,
      "id_str" : "2529971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/683496924104658944\/8Oa5XAso_normal.png",
      "id" : 2529971,
      "verified" : true
    }
  },
  "id" : 706575376642936832,
  "created_at" : "2016-03-06 20:21:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNBC",
      "screen_name" : "CNBC",
      "indices" : [ 3, 8 ],
      "id_str" : "20402945",
      "id" : 20402945
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/CNBC\/status\/705671726261248000\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/en6FzQALJa",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CcsMsiuWAAA6U0b.jpg",
      "id_str" : "705671725988577280",
      "id" : 705671725988577280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcsMsiuWAAA6U0b.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 298,
        "resize" : "fit",
        "w" : 530
      }, {
        "h" : 298,
        "resize" : "fit",
        "w" : 530
      }, {
        "h" : 298,
        "resize" : "fit",
        "w" : 530
      }, {
        "h" : 298,
        "resize" : "fit",
        "w" : 530
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/en6FzQALJa"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/9UogBKV6ak",
      "expanded_url" : "http:\/\/cnb.cx\/1QwqyRd",
      "display_url" : "cnb.cx\/1QwqyRd"
    } ]
  },
  "geo" : { },
  "id_str" : "706482333340323840",
  "text" : "RT @CNBC: Singapore won\u2019t let planes out of sight https:\/\/t.co\/9UogBKV6ak https:\/\/t.co\/en6FzQALJa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CNBC\/status\/705671726261248000\/photo\/1",
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/en6FzQALJa",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CcsMsiuWAAA6U0b.jpg",
        "id_str" : "705671725988577280",
        "id" : 705671725988577280,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcsMsiuWAAA6U0b.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 298,
          "resize" : "fit",
          "w" : 530
        }, {
          "h" : 298,
          "resize" : "fit",
          "w" : 530
        }, {
          "h" : 298,
          "resize" : "fit",
          "w" : 530
        }, {
          "h" : 298,
          "resize" : "fit",
          "w" : 530
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/en6FzQALJa"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/9UogBKV6ak",
        "expanded_url" : "http:\/\/cnb.cx\/1QwqyRd",
        "display_url" : "cnb.cx\/1QwqyRd"
      } ]
    },
    "geo" : { },
    "id_str" : "705671726261248000",
    "text" : "Singapore won\u2019t let planes out of sight https:\/\/t.co\/9UogBKV6ak https:\/\/t.co\/en6FzQALJa",
    "id" : 705671726261248000,
    "created_at" : "2016-03-04 08:30:24 +0000",
    "user" : {
      "name" : "CNBC",
      "screen_name" : "CNBC",
      "protected" : false,
      "id_str" : "20402945",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/997500309617328129\/E2rIo31Y_normal.jpg",
      "id" : 20402945,
      "verified" : true
    }
  },
  "id" : 706482333340323840,
  "created_at" : "2016-03-06 14:11:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jennifer Aldrich",
      "screen_name" : "jma245",
      "indices" : [ 3, 10 ],
      "id_str" : "32696488",
      "id" : 32696488
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jma245\/status\/440946484167864320\/photo\/1",
      "indices" : [ 96, 118 ],
      "url" : "http:\/\/t.co\/p2st4sG7NB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bh6OfbDIQAAmNq1.jpg",
      "id_str" : "440946484016857088",
      "id" : 440946484016857088,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bh6OfbDIQAAmNq1.jpg",
      "sizes" : [ {
        "h" : 488,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 488,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 488,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/p2st4sG7NB"
    } ],
    "hashtags" : [ {
      "text" : "ux",
      "indices" : [ 80, 83 ]
    }, {
      "text" : "design",
      "indices" : [ 84, 91 ]
    }, {
      "text" : "ui",
      "indices" : [ 92, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706426856631898112",
  "text" : "RT @jma245: And then my kiddo asked, \"What's the difference between UX and UI?\" #ux #design #ui http:\/\/t.co\/p2st4sG7NB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jma245\/status\/440946484167864320\/photo\/1",
        "indices" : [ 84, 106 ],
        "url" : "http:\/\/t.co\/p2st4sG7NB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Bh6OfbDIQAAmNq1.jpg",
        "id_str" : "440946484016857088",
        "id" : 440946484016857088,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bh6OfbDIQAAmNq1.jpg",
        "sizes" : [ {
          "h" : 488,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 488,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 324,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 488,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/p2st4sG7NB"
      } ],
      "hashtags" : [ {
        "text" : "ux",
        "indices" : [ 68, 71 ]
      }, {
        "text" : "design",
        "indices" : [ 72, 79 ]
      }, {
        "text" : "ui",
        "indices" : [ 80, 83 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "440946484167864320",
    "text" : "And then my kiddo asked, \"What's the difference between UX and UI?\" #ux #design #ui http:\/\/t.co\/p2st4sG7NB",
    "id" : 440946484167864320,
    "created_at" : "2014-03-04 20:26:48 +0000",
    "user" : {
      "name" : "Jennifer Aldrich",
      "screen_name" : "jma245",
      "protected" : false,
      "id_str" : "32696488",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/940363440547352577\/8X18qPIp_normal.jpg",
      "id" : 32696488,
      "verified" : false
    }
  },
  "id" : 706426856631898112,
  "created_at" : "2016-03-06 10:31:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Menninger",
      "screen_name" : "dmenningervr",
      "indices" : [ 3, 16 ],
      "id_str" : "193034008",
      "id" : 193034008
    }, {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "indices" : [ 19, 30 ],
      "id_str" : "166540104",
      "id" : 166540104
    }, {
      "name" : "Tony Baer",
      "screen_name" : "TonyBaer",
      "indices" : [ 31, 40 ],
      "id_str" : "14066062",
      "id" : 14066062
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 117, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706426466419011584",
  "text" : "RT @dmenningervr: .@longhowlam @tonybaer Our research shows 57% using R (most widely used predictive analytics tool) #rstats  https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
        "screen_name" : "longhowlam",
        "indices" : [ 1, 12 ],
        "id_str" : "166540104",
        "id" : 166540104
      }, {
        "name" : "Tony Baer",
        "screen_name" : "TonyBaer",
        "indices" : [ 13, 22 ],
        "id_str" : "14066062",
        "id" : 14066062
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 99, 106 ]
      } ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/WO1MiYjAp4",
        "expanded_url" : "https:\/\/twitter.com\/longhowlam\/status\/704968397033967617",
        "display_url" : "twitter.com\/longhowlam\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705775333799280641",
    "text" : ".@longhowlam @tonybaer Our research shows 57% using R (most widely used predictive analytics tool) #rstats  https:\/\/t.co\/WO1MiYjAp4",
    "id" : 705775333799280641,
    "created_at" : "2016-03-04 15:22:06 +0000",
    "user" : {
      "name" : "David Menninger",
      "screen_name" : "dmenningervr",
      "protected" : false,
      "id_str" : "193034008",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2906724072\/39cd06210e0dafcf3894209b5fba0667_normal.png",
      "id" : 193034008,
      "verified" : false
    }
  },
  "id" : 706426466419011584,
  "created_at" : "2016-03-06 10:29:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "indices" : [ 3, 16 ],
      "id_str" : "14826420",
      "id" : 14826420
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/iamkelvinlee\/status\/706397316677300224\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/F2V6IdaZJC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc2gbMTWIAEkiLz.jpg",
      "id_str" : "706397105586315265",
      "id" : 706397105586315265,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc2gbMTWIAEkiLz.jpg",
      "sizes" : [ {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/F2V6IdaZJC"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/iamkelvinlee\/status\/706397316677300224\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/F2V6IdaZJC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc2gbPxW4AA1VuP.jpg",
      "id_str" : "706397106517499904",
      "id" : 706397106517499904,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc2gbPxW4AA1VuP.jpg",
      "sizes" : [ {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/F2V6IdaZJC"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/iamkelvinlee\/status\/706397316677300224\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/F2V6IdaZJC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc2gbTXWIAESOSw.jpg",
      "id_str" : "706397107482140673",
      "id" : 706397107482140673,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc2gbTXWIAESOSw.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/F2V6IdaZJC"
    } ],
    "hashtags" : [ {
      "text" : "architecture",
      "indices" : [ 30, 43 ]
    }, {
      "text" : "travel",
      "indices" : [ 82, 89 ]
    }, {
      "text" : "design",
      "indices" : [ 90, 97 ]
    }, {
      "text" : "moderncity",
      "indices" : [ 98, 109 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706425600865599488",
  "text" : "RT @iamkelvinlee: If you like #architecture, Singapore's like a giant playground. #travel #design #moderncity https:\/\/t.co\/F2V6IdaZJC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iamkelvinlee\/status\/706397316677300224\/photo\/1",
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/F2V6IdaZJC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc2gbMTWIAEkiLz.jpg",
        "id_str" : "706397105586315265",
        "id" : 706397105586315265,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc2gbMTWIAEkiLz.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/F2V6IdaZJC"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/iamkelvinlee\/status\/706397316677300224\/photo\/1",
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/F2V6IdaZJC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc2gbPxW4AA1VuP.jpg",
        "id_str" : "706397106517499904",
        "id" : 706397106517499904,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc2gbPxW4AA1VuP.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/F2V6IdaZJC"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/iamkelvinlee\/status\/706397316677300224\/photo\/1",
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/F2V6IdaZJC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc2gbTXWIAESOSw.jpg",
        "id_str" : "706397107482140673",
        "id" : 706397107482140673,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc2gbTXWIAESOSw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/F2V6IdaZJC"
      } ],
      "hashtags" : [ {
        "text" : "architecture",
        "indices" : [ 12, 25 ]
      }, {
        "text" : "travel",
        "indices" : [ 64, 71 ]
      }, {
        "text" : "design",
        "indices" : [ 72, 79 ]
      }, {
        "text" : "moderncity",
        "indices" : [ 80, 91 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "706397316677300224",
    "text" : "If you like #architecture, Singapore's like a giant playground. #travel #design #moderncity https:\/\/t.co\/F2V6IdaZJC",
    "id" : 706397316677300224,
    "created_at" : "2016-03-06 08:33:39 +0000",
    "user" : {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "protected" : false,
      "id_str" : "14826420",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996378142682923010\/A8QOlqWt_normal.jpg",
      "id" : 14826420,
      "verified" : false
    }
  },
  "id" : 706425600865599488,
  "created_at" : "2016-03-06 10:26:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Urban Renstrom",
      "screen_name" : "UrbanRenstrom",
      "indices" : [ 3, 17 ],
      "id_str" : "253490278",
      "id" : 253490278
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Humor",
      "indices" : [ 19, 25 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/yhHhBs9c8V",
      "expanded_url" : "https:\/\/twitter.com\/Myvouchersie\/status\/706424114609852416",
      "display_url" : "twitter.com\/Myvouchersie\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706425222585556993",
  "text" : "RT @UrbanRenstrom: #Humor  https:\/\/t.co\/yhHhBs9c8V",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.meshfire.com\/\" rel=\"nofollow\"\u003EMeshfire\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Humor",
        "indices" : [ 0, 6 ]
      } ],
      "urls" : [ {
        "indices" : [ 8, 31 ],
        "url" : "https:\/\/t.co\/yhHhBs9c8V",
        "expanded_url" : "https:\/\/twitter.com\/Myvouchersie\/status\/706424114609852416",
        "display_url" : "twitter.com\/Myvouchersie\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "706424664650850304",
    "text" : "#Humor  https:\/\/t.co\/yhHhBs9c8V",
    "id" : 706424664650850304,
    "created_at" : "2016-03-06 10:22:19 +0000",
    "user" : {
      "name" : "Urban Renstrom",
      "screen_name" : "UrbanRenstrom",
      "protected" : false,
      "id_str" : "253490278",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614041791369768964\/AjNLeZvV_normal.jpg",
      "id" : 253490278,
      "verified" : false
    }
  },
  "id" : 706425222585556993,
  "created_at" : "2016-03-06 10:24:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/jdRG1M8nhF",
      "expanded_url" : "http:\/\/www.trustev.com\/jobs",
      "display_url" : "trustev.com\/jobs"
    } ]
  },
  "geo" : { },
  "id_str" : "706137232927760384",
  "text" : "RT @patphelan: Notice how all the jobs announcements have dried up? We are still recruiting https:\/\/t.co\/jdRG1M8nhF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/jdRG1M8nhF",
        "expanded_url" : "http:\/\/www.trustev.com\/jobs",
        "display_url" : "trustev.com\/jobs"
      } ]
    },
    "geo" : { },
    "id_str" : "706136707666571264",
    "text" : "Notice how all the jobs announcements have dried up? We are still recruiting https:\/\/t.co\/jdRG1M8nhF",
    "id" : 706136707666571264,
    "created_at" : "2016-03-05 15:18:05 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 706137232927760384,
  "created_at" : "2016-03-05 15:20:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "indices" : [ 3, 19 ],
      "id_str" : "2735246778",
      "id" : 2735246778
    }, {
      "name" : "O'Reilly Media",
      "screen_name" : "OReillyMedia",
      "indices" : [ 87, 100 ],
      "id_str" : "11069462",
      "id" : 11069462
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706121878822379520",
  "text" : "RT @ThePracticalDev: Stack Overflow has officially endorsed the book, still waiting on @OReillyMedia for the go-ahead. https:\/\/t.co\/091jtof\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "O'Reilly Media",
        "screen_name" : "OReillyMedia",
        "indices" : [ 66, 79 ],
        "id_str" : "11069462",
        "id" : 11069462
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/091jtofOdi",
        "expanded_url" : "https:\/\/twitter.com\/StackOverflow\/status\/705908081163546625",
        "display_url" : "twitter.com\/StackOverflow\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705914567348658176",
    "text" : "Stack Overflow has officially endorsed the book, still waiting on @OReillyMedia for the go-ahead. https:\/\/t.co\/091jtofOdi",
    "id" : 705914567348658176,
    "created_at" : "2016-03-05 00:35:22 +0000",
    "user" : {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "protected" : false,
      "id_str" : "2735246778",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002604104194056192\/IEoNsLNM_normal.jpg",
      "id" : 2735246778,
      "verified" : false
    }
  },
  "id" : 706121878822379520,
  "created_at" : "2016-03-05 14:19:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/laODcTvfUR",
      "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/705006005441187840",
      "display_url" : "twitter.com\/randal_olson\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706120722956029952",
  "text" : "Could all her colouring book require more blue than pink?  https:\/\/t.co\/laODcTvfUR",
  "id" : 706120722956029952,
  "created_at" : "2016-03-05 14:14:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/mtM35il2dD",
      "expanded_url" : "https:\/\/www.swarmapp.com\/c\/8PmYTXCfJpu",
      "display_url" : "swarmapp.com\/c\/8PmYTXCfJpu"
    } ]
  },
  "geo" : { },
  "id_str" : "706113250400800768",
  "text" : "I'm at Quinn School of Business in Dublin https:\/\/t.co\/mtM35il2dD",
  "id" : 706113250400800768,
  "created_at" : "2016-03-05 13:44:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "indices" : [ 3, 12 ],
      "id_str" : "11503282",
      "id" : 11503282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706047701343977472",
  "text" : "RT @ladyxtel: Metrics aren't a form of control - they monitor progress, provide direction, and remove blindfolds.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "706046342720651264",
    "text" : "Metrics aren't a form of control - they monitor progress, provide direction, and remove blindfolds.",
    "id" : 706046342720651264,
    "created_at" : "2016-03-05 09:19:00 +0000",
    "user" : {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "protected" : false,
      "id_str" : "11503282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849872363671830528\/5iExzLy9_normal.jpg",
      "id" : 11503282,
      "verified" : true
    }
  },
  "id" : 706047701343977472,
  "created_at" : "2016-03-05 09:24:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/706047098899267584\/photo\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/8LqEIviTvN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CcxiDajW0AExrdr.jpg",
      "id_str" : "706047052397072385",
      "id" : 706047052397072385,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcxiDajW0AExrdr.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8LqEIviTvN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "706047098899267584",
  "text" : "Poster still not taken down. I suppose this candidate is waiting for re-election. https:\/\/t.co\/8LqEIviTvN",
  "id" : 706047098899267584,
  "created_at" : "2016-03-05 09:22:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "indices" : [ 3, 11 ],
      "id_str" : "13311832",
      "id" : 13311832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/dpvgfgLVxx",
      "expanded_url" : "https:\/\/www.google.com\/search?q=what+is+a+googol",
      "display_url" : "google.com\/search?q=what+\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "706038488546861056",
  "text" : "RT @dr_pete: Not sure I've ever seen two Knowledge Cards - kind of makes sense this one is important to Google -- https:\/\/t.co\/dpvgfgLVxx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/dpvgfgLVxx",
        "expanded_url" : "https:\/\/www.google.com\/search?q=what+is+a+googol",
        "display_url" : "google.com\/search?q=what+\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705851221836312576",
    "text" : "Not sure I've ever seen two Knowledge Cards - kind of makes sense this one is important to Google -- https:\/\/t.co\/dpvgfgLVxx",
    "id" : 705851221836312576,
    "created_at" : "2016-03-04 20:23:40 +0000",
    "user" : {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "protected" : false,
      "id_str" : "13311832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/542152616487038976\/-UUMowce_normal.jpeg",
      "id" : 13311832,
      "verified" : false
    }
  },
  "id" : 706038488546861056,
  "created_at" : "2016-03-05 08:47:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/Ikj1I1z3zg",
      "expanded_url" : "http:\/\/mothership.sg\/2016\/03\/aia-singapore-tried-asking-photographer-for-free-content-told-to-get-lost\/",
      "display_url" : "mothership.sg\/2016\/03\/aia-si\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "705805670973575168",
  "text" : "RT @yapphenghui: AIA Singapore tried asking photographer for free content, told to get lost https:\/\/t.co\/Ikj1I1z3zg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/Ikj1I1z3zg",
        "expanded_url" : "http:\/\/mothership.sg\/2016\/03\/aia-singapore-tried-asking-photographer-for-free-content-told-to-get-lost\/",
        "display_url" : "mothership.sg\/2016\/03\/aia-si\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705787932590809088",
    "text" : "AIA Singapore tried asking photographer for free content, told to get lost https:\/\/t.co\/Ikj1I1z3zg",
    "id" : 705787932590809088,
    "created_at" : "2016-03-04 16:12:10 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 705805670973575168,
  "created_at" : "2016-03-04 17:22:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melissa Goh",
      "screen_name" : "MelGohCNA",
      "indices" : [ 3, 13 ],
      "id_str" : "64449859",
      "id" : 64449859
    }, {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 110, 126 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SaveMalaysia",
      "indices" : [ 61, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "705693936396976128",
  "text" : "RT @MelGohCNA: There's no forever friend or foe in politics. #SaveMalaysia citizen movement officially begins @ChannelNewsAsia https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Channel NewsAsia",
        "screen_name" : "ChannelNewsAsia",
        "indices" : [ 95, 111 ],
        "id_str" : "38400130",
        "id" : 38400130
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MelGohCNA\/status\/705686161193197568\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/2I4yy9I4F5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CcsZzf7UcAAF-7d.jpg",
        "id_str" : "705686139147939840",
        "id" : 705686139147939840,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcsZzf7UcAAF-7d.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2I4yy9I4F5"
      } ],
      "hashtags" : [ {
        "text" : "SaveMalaysia",
        "indices" : [ 46, 59 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "705686161193197568",
    "text" : "There's no forever friend or foe in politics. #SaveMalaysia citizen movement officially begins @ChannelNewsAsia https:\/\/t.co\/2I4yy9I4F5",
    "id" : 705686161193197568,
    "created_at" : "2016-03-04 09:27:46 +0000",
    "user" : {
      "name" : "Melissa Goh",
      "screen_name" : "MelGohCNA",
      "protected" : false,
      "id_str" : "64449859",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/912967209286119424\/jjBPMWqo_normal.jpg",
      "id" : 64449859,
      "verified" : false
    }
  },
  "id" : 705693936396976128,
  "created_at" : "2016-03-04 09:58:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Theo Lynn",
      "screen_name" : "theolynn",
      "indices" : [ 3, 12 ],
      "id_str" : "22193590",
      "id" : 22193590
    }, {
      "name" : "mark little",
      "screen_name" : "marklittlenews",
      "indices" : [ 37, 52 ],
      "id_str" : "59503113",
      "id" : 59503113
    }, {
      "name" : "Clay Shirky",
      "screen_name" : "cshirky",
      "indices" : [ 88, 96 ],
      "id_str" : "6141832",
      "id" : 6141832
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/theolynn\/status\/705486182675779584\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/sH7AI87tdm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Ccpj8chW0AEx3Gz.png",
      "id_str" : "705486181736304641",
      "id" : 705486181736304641,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ccpj8chW0AEx3Gz.png",
      "sizes" : [ {
        "h" : 270,
        "resize" : "fit",
        "w" : 629
      }, {
        "h" : 270,
        "resize" : "fit",
        "w" : 629
      }, {
        "h" : 270,
        "resize" : "fit",
        "w" : 629
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 270,
        "resize" : "fit",
        "w" : 629
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sH7AI87tdm"
    } ],
    "hashtags" : [ {
      "text" : "quote",
      "indices" : [ 97, 103 ]
    }, {
      "text" : "ge16",
      "indices" : [ 104, 109 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "705494224788168705",
  "text" : "RT @theolynn: Not wholly relevant to @marklittlenews presentation but always liked this @cshirky #quote #ge16 https:\/\/t.co\/sH7AI87tdm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "mark little",
        "screen_name" : "marklittlenews",
        "indices" : [ 23, 38 ],
        "id_str" : "59503113",
        "id" : 59503113
      }, {
        "name" : "Clay Shirky",
        "screen_name" : "cshirky",
        "indices" : [ 74, 82 ],
        "id_str" : "6141832",
        "id" : 6141832
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/theolynn\/status\/705486182675779584\/photo\/1",
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/sH7AI87tdm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Ccpj8chW0AEx3Gz.png",
        "id_str" : "705486181736304641",
        "id" : 705486181736304641,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ccpj8chW0AEx3Gz.png",
        "sizes" : [ {
          "h" : 270,
          "resize" : "fit",
          "w" : 629
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 629
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 629
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 629
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sH7AI87tdm"
      } ],
      "hashtags" : [ {
        "text" : "quote",
        "indices" : [ 83, 89 ]
      }, {
        "text" : "ge16",
        "indices" : [ 90, 95 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "705486182675779584",
    "text" : "Not wholly relevant to @marklittlenews presentation but always liked this @cshirky #quote #ge16 https:\/\/t.co\/sH7AI87tdm",
    "id" : 705486182675779584,
    "created_at" : "2016-03-03 20:13:07 +0000",
    "user" : {
      "name" : "Theo Lynn",
      "screen_name" : "theolynn",
      "protected" : false,
      "id_str" : "22193590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/623594385150709760\/vqt1kYEu_normal.jpg",
      "id" : 22193590,
      "verified" : false
    }
  },
  "id" : 705494224788168705,
  "created_at" : "2016-03-03 20:45:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/6iSRIo3N2G",
      "expanded_url" : "https:\/\/twitter.com\/jonboyes\/status\/705331534123892736",
      "display_url" : "twitter.com\/jonboyes\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "705468936414961664",
  "text" : "Same over here. Not sure when will I pop in.  https:\/\/t.co\/6iSRIo3N2G",
  "id" : 705468936414961664,
  "created_at" : "2016-03-03 19:04:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CIO.com",
      "screen_name" : "CIOonline",
      "indices" : [ 69, 79 ],
      "id_str" : "22873424",
      "id" : 22873424
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/B4yI0gsoSE",
      "expanded_url" : "http:\/\/www.cio.com\/article\/3040057\/internet\/how-to-get-started-with-google-analytics.html",
      "display_url" : "cio.com\/article\/304005\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "705299327107915776",
  "text" : "How to get started with Google Analytics https:\/\/t.co\/B4yI0gsoSE via @CIOonline",
  "id" : 705299327107915776,
  "created_at" : "2016-03-03 07:50:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/8UYJYh2et1",
      "expanded_url" : "https:\/\/ga-dev-tools.appspot.com\/autotrack\/",
      "display_url" : "ga-dev-tools.appspot.com\/autotrack\/"
    } ]
  },
  "geo" : { },
  "id_str" : "705292852063031296",
  "text" : "Autotrack makes implementation of tracking user interaction on your webpage easier https:\/\/t.co\/8UYJYh2et1",
  "id" : 705292852063031296,
  "created_at" : "2016-03-03 07:24:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "indices" : [ 3, 11 ],
      "id_str" : "14213600",
      "id" : 14213600
    }, {
      "name" : "CMO Innovation",
      "screen_name" : "CMOInnovation",
      "indices" : [ 71, 85 ],
      "id_str" : "2909752700",
      "id" : 2909752700
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/rWwGiIyzbh",
      "expanded_url" : "http:\/\/techblo.gr\/1OSmGIh",
      "display_url" : "techblo.gr\/1OSmGIh"
    } ]
  },
  "geo" : { },
  "id_str" : "705284293694771200",
  "text" : "RT @paulmah: Why marketing managers should invest in digital analytics @CMOInnovation https:\/\/t.co\/rWwGiIyzbh",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "CMO Innovation",
        "screen_name" : "CMOInnovation",
        "indices" : [ 58, 72 ],
        "id_str" : "2909752700",
        "id" : 2909752700
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/rWwGiIyzbh",
        "expanded_url" : "http:\/\/techblo.gr\/1OSmGIh",
        "display_url" : "techblo.gr\/1OSmGIh"
      } ]
    },
    "geo" : { },
    "id_str" : "705281289767809024",
    "text" : "Why marketing managers should invest in digital analytics @CMOInnovation https:\/\/t.co\/rWwGiIyzbh",
    "id" : 705281289767809024,
    "created_at" : "2016-03-03 06:38:57 +0000",
    "user" : {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "protected" : false,
      "id_str" : "14213600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882186001157832705\/gOEbAj77_normal.jpg",
      "id" : 14213600,
      "verified" : true
    }
  },
  "id" : 705284293694771200,
  "created_at" : "2016-03-03 06:50:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Chamber Commerce SG",
      "screen_name" : "IrishChambSing",
      "indices" : [ 3, 18 ],
      "id_str" : "2825780695",
      "id" : 2825780695
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fintech",
      "indices" : [ 65, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/V68HM62GsR",
      "expanded_url" : "https:\/\/www.finextra.com\/pressarticle\/63401\/fenergo-opens-singapore-office",
      "display_url" : "finextra.com\/pressarticle\/6\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "705282905539534848",
  "text" : "RT @IrishChambSing: Irish company Fenergo opens Singapore office #fintech https:\/\/t.co\/V68HM62GsR",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "fintech",
        "indices" : [ 45, 53 ]
      } ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/V68HM62GsR",
        "expanded_url" : "https:\/\/www.finextra.com\/pressarticle\/63401\/fenergo-opens-singapore-office",
        "display_url" : "finextra.com\/pressarticle\/6\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705242249035010048",
    "text" : "Irish company Fenergo opens Singapore office #fintech https:\/\/t.co\/V68HM62GsR",
    "id" : 705242249035010048,
    "created_at" : "2016-03-03 04:03:49 +0000",
    "user" : {
      "name" : "Irish Chamber Commerce SG",
      "screen_name" : "IrishChambSing",
      "protected" : false,
      "id_str" : "2825780695",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001728211573587968\/e19uYma7_normal.jpg",
      "id" : 2825780695,
      "verified" : false
    }
  },
  "id" : 705282905539534848,
  "created_at" : "2016-03-03 06:45:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicole Chung",
      "screen_name" : "nicole_soojung",
      "indices" : [ 3, 18 ],
      "id_str" : "739602678",
      "id" : 739602678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/b0boWt0fuV",
      "expanded_url" : "http:\/\/www.pri.org\/stories\/2016-03-02\/what-its-be-butt-joke-one-kids-oscars-speaks-out",
      "display_url" : "pri.org\/stories\/2016-0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "705282802871308288",
  "text" : "RT @nicole_soojung: the family of 8yo Estie Kung, one of the 3 Asian kids at the Oscars, speaks out: https:\/\/t.co\/b0boWt0fuV https:\/\/t.co\/f\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nicole_soojung\/status\/705177457595424768\/photo\/1",
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/fET83CsPX1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CclLKTxXEAAXdZv.jpg",
        "id_str" : "705177457138274304",
        "id" : 705177457138274304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CclLKTxXEAAXdZv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 821
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 821
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 821
        }, {
          "h" : 166,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fET83CsPX1"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/b0boWt0fuV",
        "expanded_url" : "http:\/\/www.pri.org\/stories\/2016-03-02\/what-its-be-butt-joke-one-kids-oscars-speaks-out",
        "display_url" : "pri.org\/stories\/2016-0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "705177457595424768",
    "text" : "the family of 8yo Estie Kung, one of the 3 Asian kids at the Oscars, speaks out: https:\/\/t.co\/b0boWt0fuV https:\/\/t.co\/fET83CsPX1",
    "id" : 705177457595424768,
    "created_at" : "2016-03-02 23:46:22 +0000",
    "user" : {
      "name" : "Nicole Chung",
      "screen_name" : "nicole_soojung",
      "protected" : false,
      "id_str" : "739602678",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023216170500014080\/s1MqdaZk_normal.jpg",
      "id" : 739602678,
      "verified" : true
    }
  },
  "id" : 705282802871308288,
  "created_at" : "2016-03-03 06:44:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "705282511442714625",
  "text" : "Forming a group for assignment is like trying to form a new govt....",
  "id" : 705282511442714625,
  "created_at" : "2016-03-03 06:43:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/YkokNXBSXW",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/dorieclark\/2014\/09\/02\/reframing-your-way-to-happiness-at-work\/#16df16006d1e",
      "display_url" : "forbes.com\/sites\/doriecla\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "705197057787752448",
  "text" : "I can smell Monday and here the article to help you look forward to the new week. https:\/\/t.co\/YkokNXBSXW",
  "id" : 705197057787752448,
  "created_at" : "2016-03-03 01:04:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lucian Teo",
      "screen_name" : "lucian",
      "indices" : [ 3, 10 ],
      "id_str" : "860351",
      "id" : 860351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "705194846097707008",
  "text" : "RT @lucian: Startups are enticing because they are the closest representation of the original idea before it gets tainted by the complexiti\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "684528143047077888",
    "text" : "Startups are enticing because they are the closest representation of the original idea before it gets tainted by the complexities of reality",
    "id" : 684528143047077888,
    "created_at" : "2016-01-06 00:13:21 +0000",
    "user" : {
      "name" : "Lucian Teo",
      "screen_name" : "lucian",
      "protected" : false,
      "id_str" : "860351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/749394642895040514\/RqC_aepW_normal.jpg",
      "id" : 860351,
      "verified" : false
    }
  },
  "id" : 705194846097707008,
  "created_at" : "2016-03-03 00:55:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "indices" : [ 3, 7 ],
      "id_str" : "380648579",
      "id" : 380648579
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UPDATE",
      "indices" : [ 9, 16 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "705176782207590400",
  "text" : "RT @AFP: #UPDATE Malaysia says 'high possibility' Mozambique debris is from Boeing 777 -- the same plane as flight MH370 https:\/\/t.co\/VVAzR\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "UPDATE",
        "indices" : [ 0, 7 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/VVAzRC6Eqr",
        "expanded_url" : "http:\/\/u.afp.com\/ZhWK",
        "display_url" : "u.afp.com\/ZhWK"
      } ]
    },
    "geo" : { },
    "id_str" : "705067672036352000",
    "text" : "#UPDATE Malaysia says 'high possibility' Mozambique debris is from Boeing 777 -- the same plane as flight MH370 https:\/\/t.co\/VVAzRC6Eqr",
    "id" : 705067672036352000,
    "created_at" : "2016-03-02 16:30:07 +0000",
    "user" : {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "protected" : false,
      "id_str" : "380648579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/697343883630481408\/c08JBfBB_normal.jpg",
      "id" : 380648579,
      "verified" : true
    }
  },
  "id" : 705176782207590400,
  "created_at" : "2016-03-02 23:43:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/705163793412845568\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/th5fM5f7rW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cck-u7HW0AAuMtq.jpg",
      "id_str" : "705163792523644928",
      "id" : 705163792523644928,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cck-u7HW0AAuMtq.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 1336
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 377,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 1336
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/th5fM5f7rW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "705163793412845568",
  "text" : "idonethis has a new look on the homepage but still retain the old design when you login...Nice facade https:\/\/t.co\/th5fM5f7rW",
  "id" : 705163793412845568,
  "created_at" : "2016-03-02 22:52:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704996797366870016",
  "text" : "What the best way to export Tumblr content ? Thanks",
  "id" : 704996797366870016,
  "created_at" : "2016-03-02 11:48:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/OaTuas9XIN",
      "expanded_url" : "http:\/\/bit.ly\/1QSH6IE",
      "display_url" : "bit.ly\/1QSH6IE"
    } ]
  },
  "geo" : { },
  "id_str" : "704929573121728512",
  "text" : "RT @Azhreicb: IoT Watch: Amazon &amp; Brita launch water pitcher that reorders your filters automatically https:\/\/t.co\/OaTuas9XIN https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.everyonesocial.com\" rel=\"nofollow\"\u003EEveryoneSocial\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Azhreicb\/status\/704919879049150465\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/GNqbyujLOX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cchg5OTXEAAog8r.jpg",
        "id_str" : "704919877891592192",
        "id" : 704919877891592192,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cchg5OTXEAAog8r.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 637,
          "resize" : "fit",
          "w" : 930
        }, {
          "h" : 637,
          "resize" : "fit",
          "w" : 930
        }, {
          "h" : 637,
          "resize" : "fit",
          "w" : 930
        }, {
          "h" : 466,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GNqbyujLOX"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/OaTuas9XIN",
        "expanded_url" : "http:\/\/bit.ly\/1QSH6IE",
        "display_url" : "bit.ly\/1QSH6IE"
      } ]
    },
    "geo" : { },
    "id_str" : "704919879049150465",
    "text" : "IoT Watch: Amazon &amp; Brita launch water pitcher that reorders your filters automatically https:\/\/t.co\/OaTuas9XIN https:\/\/t.co\/GNqbyujLOX",
    "id" : 704919879049150465,
    "created_at" : "2016-03-02 06:42:50 +0000",
    "user" : {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "protected" : false,
      "id_str" : "205451951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/910574324540674048\/PkMoaUAB_normal.jpg",
      "id" : 205451951,
      "verified" : false
    }
  },
  "id" : 704929573121728512,
  "created_at" : "2016-03-02 07:21:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/I7n01rdOWX",
      "expanded_url" : "http:\/\/roll.sohu.com\/20160301\/n438963959.shtml",
      "display_url" : "roll.sohu.com\/20160301\/n4389\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "704826981582512128",
  "text" : "Colm T\u00F3ib\u00EDn's novel Brooklyn and the movie strike a cord with Chinese diaspora. https:\/\/t.co\/I7n01rdOWX",
  "id" : 704826981582512128,
  "created_at" : "2016-03-02 00:33:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MargaretEWard, MAJ",
      "screen_name" : "MargaretEWard",
      "indices" : [ 3, 17 ],
      "id_str" : "17868868",
      "id" : 17868868
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/HiSOwYFrFQ",
      "expanded_url" : "http:\/\/go.shr.lc\/1gtJjJc",
      "display_url" : "go.shr.lc\/1gtJjJc"
    } ]
  },
  "geo" : { },
  "id_str" : "704787572447440896",
  "text" : "RT @MargaretEWard: Can't find ANY women to speak at your conference? Try 42 awesome tech women making a diff - https:\/\/t.co\/HiSOwYFrFQ via \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Tech News",
        "screen_name" : "Irish_TechNews",
        "indices" : [ 120, 135 ],
        "id_str" : "360445368",
        "id" : 360445368
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/HiSOwYFrFQ",
        "expanded_url" : "http:\/\/go.shr.lc\/1gtJjJc",
        "display_url" : "go.shr.lc\/1gtJjJc"
      } ]
    },
    "geo" : { },
    "id_str" : "704710049713799170",
    "text" : "Can't find ANY women to speak at your conference? Try 42 awesome tech women making a diff - https:\/\/t.co\/HiSOwYFrFQ via @Irish_TechNews",
    "id" : 704710049713799170,
    "created_at" : "2016-03-01 16:49:03 +0000",
    "user" : {
      "name" : "MargaretEWard, MAJ",
      "screen_name" : "MargaretEWard",
      "protected" : false,
      "id_str" : "17868868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1359910112\/56646e4a-278c-4b2d-bc6a-60ec56953e4c_normal.png",
      "id" : 17868868,
      "verified" : false
    }
  },
  "id" : 704787572447440896,
  "created_at" : "2016-03-01 21:57:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 30 ],
      "url" : "https:\/\/t.co\/iNfPnIadwk",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/704709664718647296",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "704785997960183808",
  "text" : "ICYMI, https:\/\/t.co\/iNfPnIadwk",
  "id" : 704785997960183808,
  "created_at" : "2016-03-01 21:50:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/704773908512305152\/photo\/1",
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/eV1unSyr8G",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CcfcIkuWIAEnk1d.jpg",
      "id_str" : "704773906561900545",
      "id" : 704773906561900545,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcfcIkuWIAEnk1d.jpg",
      "sizes" : [ {
        "h" : 714,
        "resize" : "fit",
        "w" : 892
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 544,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 714,
        "resize" : "fit",
        "w" : 892
      }, {
        "h" : 714,
        "resize" : "fit",
        "w" : 892
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eV1unSyr8G"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704773908512305152",
  "text" : "On Channel News Asia site, there is only one name Benjamin https:\/\/t.co\/eV1unSyr8G",
  "id" : 704773908512305152,
  "created_at" : "2016-03-01 21:02:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "woden",
      "screen_name" : "wodenworks",
      "indices" : [ 3, 14 ],
      "id_str" : "2391884858",
      "id" : 2391884858
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/LRphdla1Yg",
      "expanded_url" : "http:\/\/wode.nz\/1nVnTJ7",
      "display_url" : "wode.nz\/1nVnTJ7"
    } ]
  },
  "geo" : { },
  "id_str" : "704712080025382912",
  "text" : "RT @wodenworks: Explaining complex ideas with stories: https:\/\/t.co\/LRphdla1Yg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/LRphdla1Yg",
        "expanded_url" : "http:\/\/wode.nz\/1nVnTJ7",
        "display_url" : "wode.nz\/1nVnTJ7"
      } ]
    },
    "geo" : { },
    "id_str" : "704710110996783104",
    "text" : "Explaining complex ideas with stories: https:\/\/t.co\/LRphdla1Yg",
    "id" : 704710110996783104,
    "created_at" : "2016-03-01 16:49:17 +0000",
    "user" : {
      "name" : "woden",
      "screen_name" : "wodenworks",
      "protected" : false,
      "id_str" : "2391884858",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/473602099414966272\/YJ3EKdtc_normal.jpeg",
      "id" : 2391884858,
      "verified" : false
    }
  },
  "id" : 704712080025382912,
  "created_at" : "2016-03-01 16:57:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704709664718647296",
  "text" : "Will you pay for water if Irish water is under public ownership &amp; USC is abolish?",
  "id" : 704709664718647296,
  "created_at" : "2016-03-01 16:47:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/MgHBj4OnH0",
      "expanded_url" : "http:\/\/reut.rs\/1oL4kUe",
      "display_url" : "reut.rs\/1oL4kUe"
    } ]
  },
  "geo" : { },
  "id_str" : "704708876214640640",
  "text" : "https:\/\/t.co\/MgHBj4OnH0",
  "id" : 704708876214640640,
  "created_at" : "2016-03-01 16:44:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/R1o4iEN4fW",
      "expanded_url" : "http:\/\/www.eventbrite.com\/e\/predict-conference-2016-tickets-18990538211?aff=estw",
      "display_url" : "eventbrite.com\/e\/predict-conf\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "704678865768153088",
  "text" : "Just got my ticket. I'm going to \"Predict Conference 2016\".  See you there? https:\/\/t.co\/R1o4iEN4fW",
  "id" : 704678865768153088,
  "created_at" : "2016-03-01 14:45:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/rviWRNEXcd",
      "expanded_url" : "https:\/\/twitter.com\/borna_hooligan\/status\/704634601080225792",
      "display_url" : "twitter.com\/borna_hooligan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "704639290131795968",
  "text" : "RT @How2BeTheBiz: Lol!  https:\/\/t.co\/rviWRNEXcd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 6, 29 ],
        "url" : "https:\/\/t.co\/rviWRNEXcd",
        "expanded_url" : "https:\/\/twitter.com\/borna_hooligan\/status\/704634601080225792",
        "display_url" : "twitter.com\/borna_hooligan\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "704638305921601536",
    "text" : "Lol!  https:\/\/t.co\/rviWRNEXcd",
    "id" : 704638305921601536,
    "created_at" : "2016-03-01 12:03:58 +0000",
    "user" : {
      "name" : "Roz",
      "screen_name" : "EcoActiveIrl",
      "protected" : false,
      "id_str" : "708825894",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034061515777826816\/GSApyCx0_normal.jpg",
      "id" : 708825894,
      "verified" : false
    }
  },
  "id" : 704639290131795968,
  "created_at" : "2016-03-01 12:07:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Womersley",
      "screen_name" : "LisaWomersley",
      "indices" : [ 3, 17 ],
      "id_str" : "789148770869080064",
      "id" : 789148770869080064
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/OnCfTjCdIC",
      "expanded_url" : "http:\/\/www.meetup.com\/Web-Analytics-Wednesdays-Singapore\/",
      "display_url" : "meetup.com\/Web-Analytics-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "704637774394171392",
  "text" : "RT @lisawomersley: Only 6 spots left for Digital Analytics Wednesday Singapore meet up on 2 Mar https:\/\/t.co\/OnCfTjCdIC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/OnCfTjCdIC",
        "expanded_url" : "http:\/\/www.meetup.com\/Web-Analytics-Wednesdays-Singapore\/",
        "display_url" : "meetup.com\/Web-Analytics-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "704288066278559745",
    "text" : "Only 6 spots left for Digital Analytics Wednesday Singapore meet up on 2 Mar https:\/\/t.co\/OnCfTjCdIC",
    "id" : 704288066278559745,
    "created_at" : "2016-02-29 12:52:14 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 704637774394171392,
  "created_at" : "2016-03-01 12:01:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 3, 17 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704635186756128768",
  "text" : "RT @ladystormhold: Ladies, you can't have gender equality by picking &amp; choosing what you want equality on, alright?   Or maybe you don't ac\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "704634708378972160",
    "text" : "Ladies, you can't have gender equality by picking &amp; choosing what you want equality on, alright?   Or maybe you don't actually want equality",
    "id" : 704634708378972160,
    "created_at" : "2016-03-01 11:49:40 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 704635186756128768,
  "created_at" : "2016-03-01 11:51:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 3, 17 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704635066601832448",
  "text" : "RT @ladystormhold: NIce to see that the amendment to the Women's Charter towards gender equality has received lots of negative feedback fro\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "704634453826609152",
    "text" : "NIce to see that the amendment to the Women's Charter towards gender equality has received lots of negative feedback from women.",
    "id" : 704634453826609152,
    "created_at" : "2016-03-01 11:48:39 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 704635066601832448,
  "created_at" : "2016-03-01 11:51:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 3, 17 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/Abpu90KJlr",
      "expanded_url" : "https:\/\/twitter.com\/azyintl\/status\/704606340233056256",
      "display_url" : "twitter.com\/azyintl\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "704612372267323393",
  "text" : "RT @ofmeowandbake: Totally agree https:\/\/t.co\/Abpu90KJlr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 14, 37 ],
        "url" : "https:\/\/t.co\/Abpu90KJlr",
        "expanded_url" : "https:\/\/twitter.com\/azyintl\/status\/704606340233056256",
        "display_url" : "twitter.com\/azyintl\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "704610487304990720",
    "text" : "Totally agree https:\/\/t.co\/Abpu90KJlr",
    "id" : 704610487304990720,
    "created_at" : "2016-03-01 10:13:25 +0000",
    "user" : {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "protected" : false,
      "id_str" : "815066809",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/988013070067810304\/AJgYqxuR_normal.jpg",
      "id" : 815066809,
      "verified" : false
    }
  },
  "id" : 704612372267323393,
  "created_at" : "2016-03-01 10:20:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "indices" : [ 3, 11 ],
      "id_str" : "115624161",
      "id" : 115624161
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Parliament",
      "indices" : [ 13, 24 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704612134240567296",
  "text" : "RT @YahooSG: #Parliament: Would Benjamin's case \"die down\" if not for social media? K Shanmugam rebuts, saying that there's still the coron\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Parliament",
        "indices" : [ 0, 11 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "704607449920065537",
    "text" : "#Parliament: Would Benjamin's case \"die down\" if not for social media? K Shanmugam rebuts, saying that there's still the coroner's inquiry.",
    "id" : 704607449920065537,
    "created_at" : "2016-03-01 10:01:21 +0000",
    "user" : {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "protected" : false,
      "id_str" : "115624161",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630948009312784384\/3fz0flC2_normal.jpg",
      "id" : 115624161,
      "verified" : false
    }
  },
  "id" : 704612134240567296,
  "created_at" : "2016-03-01 10:19:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "704582356817092608",
  "text" : "Let settle for the no. of twitter followers of those politicians if the make up of the govt cannot be decided?",
  "id" : 704582356817092608,
  "created_at" : "2016-03-01 08:21:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]