Grailbird.data.tweets_2017_12 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alastair Somerville",
      "screen_name" : "Acuity_Design",
      "indices" : [ 3, 17 ],
      "id_str" : "23757410",
      "id" : 23757410
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947519328320147456",
  "text" : "RT @Acuity_Design: Requiring survey question answers before allowing access to web content is going to lead to a lot of really bad survey d\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "947099214810673152",
    "text" : "Requiring survey question answers before allowing access to web content is going to lead to a lot of really bad survey data",
    "id" : 947099214810673152,
    "created_at" : "2017-12-30 13:37:03 +0000",
    "user" : {
      "name" : "Alastair Somerville",
      "screen_name" : "Acuity_Design",
      "protected" : false,
      "id_str" : "23757410",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947888835907768322\/jrW1k6FU_normal.jpg",
      "id" : 23757410,
      "verified" : false
    }
  },
  "id" : 947519328320147456,
  "created_at" : "2017-12-31 17:26:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947511902116241408",
  "text" : "\"Perhaps Adobe should focus more on creating great HTML5 tools for the future, and less on criticizing Apple for leaving the past behind.\"\n\n\u2014 Steve Jobs, Thoughts on Flash, April 2010",
  "id" : 947511902116241408,
  "created_at" : "2017-12-31 16:56:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul J",
      "screen_name" : "pivotservices",
      "indices" : [ 3, 17 ],
      "id_str" : "2211318180",
      "id" : 2211318180
    }, {
      "name" : "First Round",
      "screen_name" : "firstround",
      "indices" : [ 110, 121 ],
      "id_str" : "15307727",
      "id" : 15307727
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "prodmgmt",
      "indices" : [ 122, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947397576181080065",
  "text" : "RT @pivotservices: Great primer on A\/B Testing for Product Managers by Duolingo\u2019s Master Growth Hacker | Thnx @firstround #prodmgmt https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "First Round",
        "screen_name" : "firstround",
        "indices" : [ 91, 102 ],
        "id_str" : "15307727",
        "id" : 15307727
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pivotservices\/status\/925281495618588673\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/BNOwTR6yT2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNdCnazVQAENFUj.jpg",
        "id_str" : "925281493424881665",
        "id" : 925281493424881665,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNdCnazVQAENFUj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 256,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 256,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 256,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 256,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BNOwTR6yT2"
      } ],
      "hashtags" : [ {
        "text" : "prodmgmt",
        "indices" : [ 103, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/Fix2aT1kax",
        "expanded_url" : "https:\/\/buff.ly\/2yqjIy8",
        "display_url" : "buff.ly\/2yqjIy8"
      } ]
    },
    "geo" : { },
    "id_str" : "925281495618588673",
    "text" : "Great primer on A\/B Testing for Product Managers by Duolingo\u2019s Master Growth Hacker | Thnx @firstround #prodmgmt https:\/\/t.co\/Fix2aT1kax https:\/\/t.co\/BNOwTR6yT2",
    "id" : 925281495618588673,
    "created_at" : "2017-10-31 08:41:14 +0000",
    "user" : {
      "name" : "Paul J",
      "screen_name" : "pivotservices",
      "protected" : false,
      "id_str" : "2211318180",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/988031373133967361\/akeFIU5Y_normal.jpg",
      "id" : 2211318180,
      "verified" : false
    }
  },
  "id" : 947397576181080065,
  "created_at" : "2017-12-31 09:22:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aaron Levie",
      "screen_name" : "levie",
      "indices" : [ 3, 9 ],
      "id_str" : "914061",
      "id" : 914061
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947396953868066819",
  "text" : "RT @levie: Amazon's next innovation: Amazon House. You literally just live inside of their warehouse and get instant delivery.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "923219856635314177",
    "text" : "Amazon's next innovation: Amazon House. You literally just live inside of their warehouse and get instant delivery.",
    "id" : 923219856635314177,
    "created_at" : "2017-10-25 16:09:01 +0000",
    "user" : {
      "name" : "Aaron Levie",
      "screen_name" : "levie",
      "protected" : false,
      "id_str" : "914061",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885529357904510976\/tM0vLiYS_normal.jpg",
      "id" : 914061,
      "verified" : true
    }
  },
  "id" : 947396953868066819,
  "created_at" : "2017-12-31 09:20:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u2B50 amy o'connor \u2B50",
      "screen_name" : "amyohconnor",
      "indices" : [ 3, 15 ],
      "id_str" : "268478664",
      "id" : 268478664
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947392426062286848",
  "text" : "RT @amyohconnor: A group of asylum seekers in direct provision pooled their money for their own little Christmas celebration and were denie\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 212, 235 ],
        "url" : "https:\/\/t.co\/bTh531sIFY",
        "expanded_url" : "http:\/\/www.limerickleader.ie\/news\/home\/289094\/asylum-seekers-at-limerick-centre-denied-christmas-celebrations.html#.WkdnfOQHaTQ.twitter",
        "display_url" : "limerickleader.ie\/news\/home\/2890\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "947103407726317569",
    "text" : "A group of asylum seekers in direct provision pooled their money for their own little Christmas celebration and were denied permission to access the communal space. Just unbelievably mean-spirited and miserable: https:\/\/t.co\/bTh531sIFY",
    "id" : 947103407726317569,
    "created_at" : "2017-12-30 13:53:43 +0000",
    "user" : {
      "name" : "\u2B50 amy o'connor \u2B50",
      "screen_name" : "amyohconnor",
      "protected" : false,
      "id_str" : "268478664",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977693465726382080\/DJ60oCMh_normal.jpg",
      "id" : 268478664,
      "verified" : true
    }
  },
  "id" : 947392426062286848,
  "created_at" : "2017-12-31 09:02:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9",
      "screen_name" : "rte",
      "indices" : [ 3, 7 ],
      "id_str" : "1245699895",
      "id" : 1245699895
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947179033267200000",
  "text" : "Hi @rte thanks for the Harry potter movies instalment over the festive season but I think you skip The Half Blood Prince.",
  "id" : 947179033267200000,
  "created_at" : "2017-12-30 18:54:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TONI",
      "screen_name" : "t0nit0ne",
      "indices" : [ 3, 12 ],
      "id_str" : "23185323",
      "id" : 23185323
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "947132077044129792",
  "text" : "RT @t0nit0ne: I'm so envious of the way men can network with such confidence, without regularly having to consider what we women have to co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "946485577754533888",
    "text" : "I'm so envious of the way men can network with such confidence, without regularly having to consider what we women have to consider. \n\nNetworking and following up business propositions is much harder for women because honestly, a lot of men try to hit on us at some stage.",
    "id" : 946485577754533888,
    "created_at" : "2017-12-28 20:58:41 +0000",
    "user" : {
      "name" : "TONI",
      "screen_name" : "t0nit0ne",
      "protected" : false,
      "id_str" : "23185323",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/969001917803266050\/qbBI6sE4_normal.jpg",
      "id" : 23185323,
      "verified" : false
    }
  },
  "id" : 947132077044129792,
  "created_at" : "2017-12-30 15:47:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chatbot Me Up \uD83E\uDD16",
      "screen_name" : "chatbotmeup",
      "indices" : [ 3, 15 ],
      "id_str" : "919457282470977536",
      "id" : 919457282470977536
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bots",
      "indices" : [ 88, 93 ]
    }, {
      "text" : "AI",
      "indices" : [ 94, 97 ]
    }, {
      "text" : "chatbots",
      "indices" : [ 98, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/UMQXrVTOdv",
      "expanded_url" : "https:\/\/buff.ly\/2AyZGml",
      "display_url" : "buff.ly\/2AyZGml"
    } ]
  },
  "geo" : { },
  "id_str" : "946917160521273344",
  "text" : "RT @chatbotmeup: \"OCBC Bank launches Emma, the mortgage chatbot https:\/\/t.co\/UMQXrVTOdv #bots #AI #chatbots\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "bots",
        "indices" : [ 71, 76 ]
      }, {
        "text" : "AI",
        "indices" : [ 77, 80 ]
      }, {
        "text" : "chatbots",
        "indices" : [ 81, 90 ]
      } ],
      "urls" : [ {
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/UMQXrVTOdv",
        "expanded_url" : "https:\/\/buff.ly\/2AyZGml",
        "display_url" : "buff.ly\/2AyZGml"
      } ]
    },
    "geo" : { },
    "id_str" : "946765364091899904",
    "text" : "\"OCBC Bank launches Emma, the mortgage chatbot https:\/\/t.co\/UMQXrVTOdv #bots #AI #chatbots\"",
    "id" : 946765364091899904,
    "created_at" : "2017-12-29 15:30:27 +0000",
    "user" : {
      "name" : "Chatbot Me Up \uD83E\uDD16",
      "screen_name" : "chatbotmeup",
      "protected" : false,
      "id_str" : "919457282470977536",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/933473378203082753\/Q91m3Be-_normal.jpg",
      "id" : 919457282470977536,
      "verified" : false
    }
  },
  "id" : 946917160521273344,
  "created_at" : "2017-12-30 01:33:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Barack Obama",
      "screen_name" : "BarackObama",
      "indices" : [ 3, 15 ],
      "id_str" : "813286",
      "id" : 813286
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946850578319233024",
  "text" : "RT @BarackObama: As we count down to the new year, we get to reflect and prepare for what\u2019s ahead. For all the bad news that seemed to domi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "946775081371471872",
    "text" : "As we count down to the new year, we get to reflect and prepare for what\u2019s ahead. For all the bad news that seemed to dominate our collective consciousness, there are countless stories from this year that remind us what's best about America.",
    "id" : 946775081371471872,
    "created_at" : "2017-12-29 16:09:04 +0000",
    "user" : {
      "name" : "Barack Obama",
      "screen_name" : "BarackObama",
      "protected" : false,
      "id_str" : "813286",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/822547732376207360\/5g0FC8XX_normal.jpg",
      "id" : 813286,
      "verified" : true
    }
  },
  "id" : 946850578319233024,
  "created_at" : "2017-12-29 21:09:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946836219358564352",
  "text" : "A position being filled recently going by a LinkedIn profile but it still being advertised.",
  "id" : 946836219358564352,
  "created_at" : "2017-12-29 20:12:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GreenFuzz \uD83C\uDD65",
      "screen_name" : "molesuk",
      "indices" : [ 3, 11 ],
      "id_str" : "269969154",
      "id" : 269969154
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Veganuary",
      "indices" : [ 107, 117 ]
    }, {
      "text" : "GoVegan",
      "indices" : [ 118, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946791744292704256",
  "text" : "RT @molesuk: A meat eater decides to go straight to being vegan. Well worth the read if you\u2019re wanting try #Veganuary #GoVegan   https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Veganuary",
        "indices" : [ 94, 104 ]
      }, {
        "text" : "GoVegan",
        "indices" : [ 105, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/X8Z0T5iwxI",
        "expanded_url" : "https:\/\/www.newstatesman.com\/culture\/observations\/2017\/11\/why-i-became-vegan-and-why-you-should-too",
        "display_url" : "newstatesman.com\/culture\/observ\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "946528464718442501",
    "text" : "A meat eater decides to go straight to being vegan. Well worth the read if you\u2019re wanting try #Veganuary #GoVegan   https:\/\/t.co\/X8Z0T5iwxI",
    "id" : 946528464718442501,
    "created_at" : "2017-12-28 23:49:06 +0000",
    "user" : {
      "name" : "GreenFuzz \uD83C\uDD65",
      "screen_name" : "molesuk",
      "protected" : false,
      "id_str" : "269969154",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011240902097072129\/42f9pM59_normal.jpg",
      "id" : 269969154,
      "verified" : false
    }
  },
  "id" : 946791744292704256,
  "created_at" : "2017-12-29 17:15:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946763751960776707",
  "text" : "IS there a way to find out the type of back-end that driving a mobile app?",
  "id" : 946763751960776707,
  "created_at" : "2017-12-29 15:24:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Jarrett",
      "screen_name" : "cjforms",
      "indices" : [ 3, 11 ],
      "id_str" : "144605504",
      "id" : 144605504
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946758348266983424",
  "text" : "RT @cjforms: Achieved inbox zero.\n\nDid it by moving entire inbox to folder '2017'. Is this cheating?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "946425652990873600",
    "text" : "Achieved inbox zero.\n\nDid it by moving entire inbox to folder '2017'. Is this cheating?",
    "id" : 946425652990873600,
    "created_at" : "2017-12-28 17:00:34 +0000",
    "user" : {
      "name" : "Caroline Jarrett",
      "screen_name" : "cjforms",
      "protected" : false,
      "id_str" : "144605504",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528613343381032960\/aFnqmqcK_normal.jpeg",
      "id" : 144605504,
      "verified" : false
    }
  },
  "id" : 946758348266983424,
  "created_at" : "2017-12-29 15:02:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deborah Kay",
      "screen_name" : "debbiediscovers",
      "indices" : [ 74, 90 ],
      "id_str" : "3060836348",
      "id" : 3060836348
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/r0VFrr1Yjf",
      "expanded_url" : "https:\/\/digitaldiscovery.sg\/blog\/what-analytics-do-you-get-from-twitter-api\/",
      "display_url" : "digitaldiscovery.sg\/blog\/what-anal\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "946739331145838593",
  "text" : "What Analytics do you get from Twitter's API? https:\/\/t.co\/r0VFrr1Yjf via @debbiediscovers",
  "id" : 946739331145838593,
  "created_at" : "2017-12-29 13:47:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946735016956096512",
  "text" : "Wasn't it wonderful to see Bono performing live on BBC?",
  "id" : 946735016956096512,
  "created_at" : "2017-12-29 13:29:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/gDIqjd5Os7",
      "expanded_url" : "http:\/\/www.latimes.com\/world\/asia\/la-fg-myanmar-rohingya-hate-20171225-story.html",
      "display_url" : "latimes.com\/world\/asia\/la-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "946731475180892160",
  "text" : "In Myanmar, hatred for Rohingya Muslims runs so deep that a diplomat called them 'ugly as ogres' \u2014 and got promoted https:\/\/t.co\/gDIqjd5Os7",
  "id" : 946731475180892160,
  "created_at" : "2017-12-29 13:15:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/AoWpbEbZ5J",
      "expanded_url" : "https:\/\/www.irishtimes.com\/life-and-style\/health-family\/begrudgery-what-s-behind-it-1.2546691#.WkY_U4lJpO4.twitter",
      "display_url" : "irishtimes.com\/life-and-style\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "946730790980898816",
  "text" : "Begrudgery \u2013 what\u2019s behind it? https:\/\/t.co\/AoWpbEbZ5J",
  "id" : 946730790980898816,
  "created_at" : "2017-12-29 13:13:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/946697674937466881\/photo\/1",
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/NCkbXwUe5o",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DSNYgDuWkAEnoKE.jpg",
      "id_str" : "946697654456717313",
      "id" : 946697654456717313,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DSNYgDuWkAEnoKE.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NCkbXwUe5o"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946697674937466881",
  "text" : "I am that close. https:\/\/t.co\/NCkbXwUe5o",
  "id" : 946697674937466881,
  "created_at" : "2017-12-29 11:01:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/NRHUo9cDAD",
      "expanded_url" : "https:\/\/www.forbes.com",
      "display_url" : "forbes.com"
    } ]
  },
  "geo" : { },
  "id_str" : "946656993649012737",
  "text" : "The worst layout for article https:\/\/t.co\/NRHUo9cDAD content sandwiched by ads on the left, top, right. Don't be like them.",
  "id" : 946656993649012737,
  "created_at" : "2017-12-29 08:19:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "snarky scorpio pats",
      "screen_name" : "BaritonePats",
      "indices" : [ 3, 16 ],
      "id_str" : "56555062",
      "id" : 56555062
    }, {
      "name" : "Wilfred Chan",
      "screen_name" : "wilfredchan",
      "indices" : [ 18, 30 ],
      "id_str" : "17938103",
      "id" : 17938103
    }, {
      "name" : "NYT Food",
      "screen_name" : "nytfood",
      "indices" : [ 31, 39 ],
      "id_str" : "1775731",
      "id" : 1775731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946437512880369666",
  "text" : "RT @BaritonePats: @wilfredchan @nytfood Who died",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Wilfred Chan",
        "screen_name" : "wilfredchan",
        "indices" : [ 0, 12 ],
        "id_str" : "17938103",
        "id" : 17938103
      }, {
        "name" : "NYT Food",
        "screen_name" : "nytfood",
        "indices" : [ 13, 21 ],
        "id_str" : "1775731",
        "id" : 1775731
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "946105655626047489",
    "geo" : { },
    "id_str" : "946109029259939840",
    "in_reply_to_user_id" : 17938103,
    "text" : "@wilfredchan @nytfood Who died",
    "id" : 946109029259939840,
    "in_reply_to_status_id" : 946105655626047489,
    "created_at" : "2017-12-27 20:02:25 +0000",
    "in_reply_to_screen_name" : "wilfredchan",
    "in_reply_to_user_id_str" : "17938103",
    "user" : {
      "name" : "snarky scorpio pats",
      "screen_name" : "BaritonePats",
      "protected" : false,
      "id_str" : "56555062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/312357405\/P1420928_normal.JPG",
      "id" : 56555062,
      "verified" : false
    }
  },
  "id" : 946437512880369666,
  "created_at" : "2017-12-28 17:47:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    }, {
      "name" : "The New York Times",
      "screen_name" : "nytimes",
      "indices" : [ 18, 26 ],
      "id_str" : "807095",
      "id" : 807095
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946437084407123968",
  "text" : "RT @mrbrown: Dear @nytimes,\n\nAsians do not stick chopsticks into food this way. My mother would have a fit seeing this.\n\n\u201COfferings\u201D indeed\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The New York Times",
        "screen_name" : "nytimes",
        "indices" : [ 5, 13 ],
        "id_str" : "807095",
        "id" : 807095
      }, {
        "name" : "Wilfred Chan",
        "screen_name" : "wilfredchan",
        "indices" : [ 184, 196 ],
        "id_str" : "17938103",
        "id" : 17938103
      }, {
        "name" : "Daryl Sng",
        "screen_name" : "dsng",
        "indices" : [ 201, 206 ],
        "id_str" : "609573",
        "id" : 609573
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/946432693729869824\/photo\/1",
        "indices" : [ 207, 230 ],
        "url" : "https:\/\/t.co\/ergqWbzv7X",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DSJne0HVwAA4_6v.jpg",
        "id_str" : "946432650784391168",
        "id" : 946432650784391168,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DSJne0HVwAA4_6v.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 587
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1002
        }, {
          "h" : 2300,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 333
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ergqWbzv7X"
      } ],
      "hashtags" : [ {
        "text" : "triggered",
        "indices" : [ 168, 178 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "946432693729869824",
    "text" : "Dear @nytimes,\n\nAsians do not stick chopsticks into food this way. My mother would have a fit seeing this.\n\n\u201COfferings\u201D indeed.\n\nAlso, who eats steak with chopsticks?\n\n#triggered\n\nH\/T @wilfredchan and @dsng https:\/\/t.co\/ergqWbzv7X",
    "id" : 946432693729869824,
    "created_at" : "2017-12-28 17:28:32 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 946437084407123968,
  "created_at" : "2017-12-28 17:45:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 3, 13 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/20HknAd6rq",
      "expanded_url" : "https:\/\/www.theguardian.com\/news\/2017\/nov\/23\/from-inboxing-to-thought-showers-how-business-bullshit-took-over",
      "display_url" : "theguardian.com\/news\/2017\/nov\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "946333586222931968",
  "text" : "RT @barryhand: Relevant. https:\/\/t.co\/20HknAd6rq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 10, 33 ],
        "url" : "https:\/\/t.co\/20HknAd6rq",
        "expanded_url" : "https:\/\/www.theguardian.com\/news\/2017\/nov\/23\/from-inboxing-to-thought-showers-how-business-bullshit-took-over",
        "display_url" : "theguardian.com\/news\/2017\/nov\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "946333404215349248",
    "text" : "Relevant. https:\/\/t.co\/20HknAd6rq",
    "id" : 946333404215349248,
    "created_at" : "2017-12-28 10:54:00 +0000",
    "user" : {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "protected" : false,
      "id_str" : "18849187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614532770300329984\/xJWWAtSu_normal.jpg",
      "id" : 18849187,
      "verified" : false
    }
  },
  "id" : 946333586222931968,
  "created_at" : "2017-12-28 10:54:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daisy Ridley",
      "screen_name" : "ridleydaiIy",
      "indices" : [ 3, 15 ],
      "id_str" : "814147907762823169",
      "id" : 814147907762823169
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RidleyDaiIy\/status\/946128776332828673\/video\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/zJYstjzjsb",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/946128569884921856\/pu\/img\/NaHIPtxmMZxKIdNb.jpg",
      "id_str" : "946128569884921856",
      "id" : 946128569884921856,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/946128569884921856\/pu\/img\/NaHIPtxmMZxKIdNb.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/zJYstjzjsb"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946332531682697216",
  "text" : "RT @RidleyDaiIy: \uD83C\uDFA5 | Daisy Ridley \u2019Star Wars: \u2019The Last Jedi \u2013 Exclusive Behind the Scenes in Ireland.\u2019 https:\/\/t.co\/zJYstjzjsb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RidleyDaiIy\/status\/946128776332828673\/video\/1",
        "indices" : [ 87, 110 ],
        "url" : "https:\/\/t.co\/zJYstjzjsb",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/946128569884921856\/pu\/img\/NaHIPtxmMZxKIdNb.jpg",
        "id_str" : "946128569884921856",
        "id" : 946128569884921856,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/946128569884921856\/pu\/img\/NaHIPtxmMZxKIdNb.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zJYstjzjsb"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "946128776332828673",
    "text" : "\uD83C\uDFA5 | Daisy Ridley \u2019Star Wars: \u2019The Last Jedi \u2013 Exclusive Behind the Scenes in Ireland.\u2019 https:\/\/t.co\/zJYstjzjsb",
    "id" : 946128776332828673,
    "created_at" : "2017-12-27 21:20:53 +0000",
    "user" : {
      "name" : "Daisy Ridley",
      "screen_name" : "ridleydaiIy",
      "protected" : false,
      "id_str" : "814147907762823169",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021006902044524544\/BNUmIinu_normal.jpg",
      "id" : 814147907762823169,
      "verified" : false
    }
  },
  "id" : 946332531682697216,
  "created_at" : "2017-12-28 10:50:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "notetoshelf",
      "screen_name" : "notetoshelf",
      "indices" : [ 3, 15 ],
      "id_str" : "16144929",
      "id" : 16144929
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dslearnings",
      "indices" : [ 17, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946332272814448640",
  "text" : "RT @notetoshelf: #Dslearnings SQLite doesn't support saving Python dictionaries, you need to convert dictionaries into strings.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Dslearnings",
        "indices" : [ 0, 12 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "946331660445978624",
    "text" : "#Dslearnings SQLite doesn't support saving Python dictionaries, you need to convert dictionaries into strings.",
    "id" : 946331660445978624,
    "created_at" : "2017-12-28 10:47:04 +0000",
    "user" : {
      "name" : "notetoshelf",
      "screen_name" : "notetoshelf",
      "protected" : false,
      "id_str" : "16144929",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/925003149068382209\/vycv754s_normal.jpg",
      "id" : 16144929,
      "verified" : false
    }
  },
  "id" : 946332272814448640,
  "created_at" : "2017-12-28 10:49:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/946318095559340033\/photo\/1",
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/PQwMDuR0W0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DSH_SIwWkAA_i4o.jpg",
      "id_str" : "946318083777531904",
      "id" : 946318083777531904,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DSH_SIwWkAA_i4o.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 664,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 664,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 664,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 452,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PQwMDuR0W0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946318095559340033",
  "text" : "When your application are in production and you need to tweak it... https:\/\/t.co\/PQwMDuR0W0",
  "id" : 946318095559340033,
  "created_at" : "2017-12-28 09:53:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caesar Sengupta",
      "screen_name" : "caesars",
      "indices" : [ 3, 11 ],
      "id_str" : "8310492",
      "id" : 8310492
    }, {
      "name" : "Tez is Google Pay India",
      "screen_name" : "TezByGoogle",
      "indices" : [ 98, 110 ],
      "id_str" : "1001058023345827840",
      "id" : 1001058023345827840
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/6aGMtkYIpR",
      "expanded_url" : "https:\/\/economictimes.indiatimes.com\/small-biz\/startups\/newsbuzz\/xiaomi-launches-google-tez-powered-payment-service\/articleshow\/62178236.cms",
      "display_url" : "economictimes.indiatimes.com\/small-biz\/star\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "946302118205493248",
  "text" : "RT @caesars: And now you can pay for your next Xiaomi phone directly from your bank account using @TezbyGoogle https:\/\/t.co\/6aGMtkYIpR",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tez is Google Pay India",
        "screen_name" : "TezByGoogle",
        "indices" : [ 85, 97 ],
        "id_str" : "1001058023345827840",
        "id" : 1001058023345827840
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/6aGMtkYIpR",
        "expanded_url" : "https:\/\/economictimes.indiatimes.com\/small-biz\/startups\/newsbuzz\/xiaomi-launches-google-tez-powered-payment-service\/articleshow\/62178236.cms",
        "display_url" : "economictimes.indiatimes.com\/small-biz\/star\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "945840314966609920",
    "text" : "And now you can pay for your next Xiaomi phone directly from your bank account using @TezbyGoogle https:\/\/t.co\/6aGMtkYIpR",
    "id" : 945840314966609920,
    "created_at" : "2017-12-27 02:14:38 +0000",
    "user" : {
      "name" : "Caesar Sengupta",
      "screen_name" : "caesars",
      "protected" : false,
      "id_str" : "8310492",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/823419573735079936\/1LL2FTqZ_normal.jpg",
      "id" : 8310492,
      "verified" : true
    }
  },
  "id" : 946302118205493248,
  "created_at" : "2017-12-28 08:49:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jacob Atkins",
      "screen_name" : "atkins_jacob1",
      "indices" : [ 3, 17 ],
      "id_str" : "967542356",
      "id" : 967542356
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946302023103901696",
  "text" : "RT @atkins_jacob1: Since I\u2019m bored and 2017 was full of negative news stories, I\u2019ve found some positive stories you probably didn\u2019t hear ab\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "943676583063277568",
    "text" : "Since I\u2019m bored and 2017 was full of negative news stories, I\u2019ve found some positive stories you probably didn\u2019t hear about. \n\nA thread:",
    "id" : 943676583063277568,
    "created_at" : "2017-12-21 02:56:44 +0000",
    "user" : {
      "name" : "Jacob Atkins",
      "screen_name" : "atkins_jacob1",
      "protected" : false,
      "id_str" : "967542356",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004913522768261121\/SaDe31OS_normal.jpg",
      "id" : 967542356,
      "verified" : false
    }
  },
  "id" : 946302023103901696,
  "created_at" : "2017-12-28 08:49:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Savin",
      "screen_name" : "alexsavinme",
      "indices" : [ 3, 15 ],
      "id_str" : "18192840",
      "id" : 18192840
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/alexsavinme\/status\/945566813190008832\/photo\/1",
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/Qhtbnc5g6w",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR9UARPX0AEbHYS.jpg",
      "id_str" : "945566810375704577",
      "id" : 945566810375704577,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR9UARPX0AEbHYS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 948,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 537,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 981,
        "resize" : "fit",
        "w" : 1242
      }, {
        "h" : 981,
        "resize" : "fit",
        "w" : 1242
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Qhtbnc5g6w"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946054533960159232",
  "text" : "RT @alexsavinme: Why Amazon delivers small things in huge boxes occasionally https:\/\/t.co\/Qhtbnc5g6w",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/alexsavinme\/status\/945566813190008832\/photo\/1",
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/Qhtbnc5g6w",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DR9UARPX0AEbHYS.jpg",
        "id_str" : "945566810375704577",
        "id" : 945566810375704577,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR9UARPX0AEbHYS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 948,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 537,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1242
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1242
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Qhtbnc5g6w"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "945566813190008832",
    "text" : "Why Amazon delivers small things in huge boxes occasionally https:\/\/t.co\/Qhtbnc5g6w",
    "id" : 945566813190008832,
    "created_at" : "2017-12-26 08:07:50 +0000",
    "user" : {
      "name" : "Alexander Savin",
      "screen_name" : "alexsavinme",
      "protected" : false,
      "id_str" : "18192840",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/861265309709348868\/U43IoI5t_normal.jpg",
      "id" : 18192840,
      "verified" : false
    }
  },
  "id" : 946054533960159232,
  "created_at" : "2017-12-27 16:25:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "From the Flight Deck",
      "screen_name" : "Golfcharlie232",
      "indices" : [ 3, 18 ],
      "id_str" : "930755719",
      "id" : 930755719
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "946047472996159493",
  "text" : "RT @Golfcharlie232: A new type of chemtrail is being sprayed over London ... \nJoking of course, this is the typical London condensation in\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "avgeek",
        "indices" : [ 203, 210 ]
      } ],
      "urls" : [ {
        "indices" : [ 179, 202 ],
        "url" : "https:\/\/t.co\/hcpqNhhNFG",
        "expanded_url" : "http:\/\/www.airliners.net\/photo\/Air-Canada\/Boeing-787-8-Dreamliner\/4759643\/L",
        "display_url" : "airliners.net\/photo\/Air-Cana\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "946042875535659008",
    "text" : "A new type of chemtrail is being sprayed over London ... \nJoking of course, this is the typical London condensation in the early morning hours, great photography by Wael AL-Qutub https:\/\/t.co\/hcpqNhhNFG #avgeek",
    "id" : 946042875535659008,
    "created_at" : "2017-12-27 15:39:32 +0000",
    "user" : {
      "name" : "From the Flight Deck",
      "screen_name" : "Golfcharlie232",
      "protected" : false,
      "id_str" : "930755719",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/675437992551559173\/Fdru0LpD_normal.jpg",
      "id" : 930755719,
      "verified" : false
    }
  },
  "id" : 946047472996159493,
  "created_at" : "2017-12-27 15:57:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/9jVIV9Mmuq",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/BBHmiwC?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/BBHmi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "946007404008886272",
  "text" : "His obsession with eternal life was well-known https:\/\/t.co\/9jVIV9Mmuq",
  "id" : 946007404008886272,
  "created_at" : "2017-12-27 13:18:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/6RuUbZKEuN",
      "expanded_url" : "https:\/\/twitter.com\/Berlinnaeus\/status\/945736960894742529",
      "display_url" : "twitter.com\/Berlinnaeus\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945990613467529216",
  "text" : "Who is Nathan Carter? https:\/\/t.co\/6RuUbZKEuN",
  "id" : 945990613467529216,
  "created_at" : "2017-12-27 12:11:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Carlson",
      "screen_name" : "awealthofcs",
      "indices" : [ 3, 15 ],
      "id_str" : "97530284",
      "id" : 97530284
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945809013329088517",
  "text" : "RT @awealthofcs: Fulfilled a lifelong dream today of going out to a Chinese restaurant for Christmas dinner. Took some convincing of my fam\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/awealthofcs\/status\/945395541575389184\/photo\/1",
        "indices" : [ 147, 170 ],
        "url" : "https:\/\/t.co\/abHbRjeWGL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DR64OnAUMAAcpSI.jpg",
        "id_str" : "945395532922368000",
        "id" : 945395532922368000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR64OnAUMAAcpSI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1036,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1768,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 587,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1768,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/abHbRjeWGL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "945395541575389184",
    "text" : "Fulfilled a lifelong dream today of going out to a Chinese restaurant for Christmas dinner. Took some convincing of my family but totally worth it https:\/\/t.co\/abHbRjeWGL",
    "id" : 945395541575389184,
    "created_at" : "2017-12-25 20:47:16 +0000",
    "user" : {
      "name" : "Ben Carlson",
      "screen_name" : "awealthofcs",
      "protected" : false,
      "id_str" : "97530284",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/645264354447769600\/uPZCaKkm_normal.jpg",
      "id" : 97530284,
      "verified" : false
    }
  },
  "id" : 945809013329088517,
  "created_at" : "2017-12-27 00:10:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Roberto Alonso Gonz\u00E1lez Lezcano",
      "screen_name" : "robertoglezcano",
      "indices" : [ 3, 19 ],
      "id_str" : "1186532348",
      "id" : 1186532348
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/ysjTQgvGeN",
      "expanded_url" : "https:\/\/twitter.com\/Belxab\/status\/856223221938573312\/photo\/1",
      "display_url" : "pic.twitter.com\/ysjTQgvGeN"
    } ]
  },
  "geo" : { },
  "id_str" : "945738886512369665",
  "text" : "RT @robertoglezcano: Mt. Fuji cloud sweep https:\/\/t.co\/ysjTQgvGeN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 21, 44 ],
        "url" : "https:\/\/t.co\/ysjTQgvGeN",
        "expanded_url" : "https:\/\/twitter.com\/Belxab\/status\/856223221938573312\/photo\/1",
        "display_url" : "pic.twitter.com\/ysjTQgvGeN"
      } ]
    },
    "geo" : { },
    "id_str" : "945005358753468416",
    "text" : "Mt. Fuji cloud sweep https:\/\/t.co\/ysjTQgvGeN",
    "id" : 945005358753468416,
    "created_at" : "2017-12-24 18:56:49 +0000",
    "user" : {
      "name" : "Roberto Alonso Gonz\u00E1lez Lezcano",
      "screen_name" : "robertoglezcano",
      "protected" : false,
      "id_str" : "1186532348",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/534812004632911872\/ENTd3Qsu_normal.png",
      "id" : 1186532348,
      "verified" : false
    }
  },
  "id" : 945738886512369665,
  "created_at" : "2017-12-26 19:31:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/v69WlPYEQD",
      "expanded_url" : "https:\/\/www.servethehome.com\/say-farewell-putty-microsoft-adds-openssh-client-windows-10\/",
      "display_url" : "servethehome.com\/say-farewell-p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945655874244284416",
  "text" : "Say Farewell to Putty as Microsoft adds an OpenSSH Client to Windows 10 https:\/\/t.co\/v69WlPYEQD",
  "id" : 945655874244284416,
  "created_at" : "2017-12-26 14:01:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Owens",
      "screen_name" : "OwensDamien",
      "indices" : [ 3, 15 ],
      "id_str" : "67725191",
      "id" : 67725191
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945636439458484225",
  "text" : "RT @OwensDamien: We don\u2019t call it Boxing Day in Ireland. Although you may hear references to St Stephen\u2019s Day, the correct term is in fact\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "945599167174447104",
    "text" : "We don\u2019t call it Boxing Day in Ireland. Although you may hear references to St Stephen\u2019s Day, the correct term is in fact Stephenseses Day.",
    "id" : 945599167174447104,
    "created_at" : "2017-12-26 10:16:24 +0000",
    "user" : {
      "name" : "Damien Owens",
      "screen_name" : "OwensDamien",
      "protected" : false,
      "id_str" : "67725191",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001211056210042880\/kUGWVX6A_normal.jpg",
      "id" : 67725191,
      "verified" : false
    }
  },
  "id" : 945636439458484225,
  "created_at" : "2017-12-26 12:44:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/ESy0WJQ9uM",
      "expanded_url" : "https:\/\/japantoday.com\/category\/crime\/japanese-police-officer-pursues-pulls-over-lamborghini-supercar%E2%80%A6while-on-a-bicycle#.WkISKkac9aE.twitter",
      "display_url" : "japantoday.com\/category\/crime\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945625235440701440",
  "text" : "RT @yapphenghui: Japanese police officer pursues, pulls over Lamborghini supercar\u2026while on a bicycle https:\/\/t.co\/ESy0WJQ9uM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/ESy0WJQ9uM",
        "expanded_url" : "https:\/\/japantoday.com\/category\/crime\/japanese-police-officer-pursues-pulls-over-lamborghini-supercar%E2%80%A6while-on-a-bicycle#.WkISKkac9aE.twitter",
        "display_url" : "japantoday.com\/category\/crime\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "945582755823230976",
    "text" : "Japanese police officer pursues, pulls over Lamborghini supercar\u2026while on a bicycle https:\/\/t.co\/ESy0WJQ9uM",
    "id" : 945582755823230976,
    "created_at" : "2017-12-26 09:11:11 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 945625235440701440,
  "created_at" : "2017-12-26 11:59:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/945617312304058368\/photo\/1",
      "indices" : [ 118, 141 ],
      "url" : "https:\/\/t.co\/28c5EJ8Mrp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR-BpiWX4AAuq-1.jpg",
      "id_str" : "945616997366358016",
      "id" : 945616997366358016,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR-BpiWX4AAuq-1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 402,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 686,
        "resize" : "fit",
        "w" : 1161
      }, {
        "h" : 686,
        "resize" : "fit",
        "w" : 1161
      }, {
        "h" : 686,
        "resize" : "fit",
        "w" : 1161
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/28c5EJ8Mrp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945617312304058368",
  "text" : "Outlook identified email from Overdrive as spam. I suppose both team in Microsoft do not communicate with each other. https:\/\/t.co\/28c5EJ8Mrp",
  "id" : 945617312304058368,
  "created_at" : "2017-12-26 11:28:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/MuF82TE6kA",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/2004_Indian_Ocean_earthquake_and_tsunami",
      "display_url" : "en.wikipedia.org\/wiki\/2004_Indi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945613994114670593",
  "text" : "13 years ago on this day. https:\/\/t.co\/MuF82TE6kA",
  "id" : 945613994114670593,
  "created_at" : "2017-12-26 11:15:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sitnik the Developer",
      "screen_name" : "sitnikcode",
      "indices" : [ 3, 14 ],
      "id_str" : "875500444537237506",
      "id" : 875500444537237506
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/sitnikcode\/status\/945565187792363520\/photo\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/51ppuR6Plz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR9ShoxWAAAQa7k.jpg",
      "id_str" : "945565184604635136",
      "id" : 945565184604635136,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR9ShoxWAAAQa7k.jpg",
      "sizes" : [ {
        "h" : 1457,
        "resize" : "fit",
        "w" : 2043
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 485,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1457,
        "resize" : "fit",
        "w" : 2043
      }, {
        "h" : 856,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/51ppuR6Plz"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945611753098043392",
  "text" : "RT @sitnikcode: The best pull request in 2017 https:\/\/t.co\/51ppuR6Plz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/amplifr.com\" rel=\"nofollow\"\u003EAmplifr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sitnikcode\/status\/945565187792363520\/photo\/1",
        "indices" : [ 30, 53 ],
        "url" : "https:\/\/t.co\/51ppuR6Plz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DR9ShoxWAAAQa7k.jpg",
        "id_str" : "945565184604635136",
        "id" : 945565184604635136,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR9ShoxWAAAQa7k.jpg",
        "sizes" : [ {
          "h" : 1457,
          "resize" : "fit",
          "w" : 2043
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1457,
          "resize" : "fit",
          "w" : 2043
        }, {
          "h" : 856,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/51ppuR6Plz"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "945565187792363520",
    "text" : "The best pull request in 2017 https:\/\/t.co\/51ppuR6Plz",
    "id" : 945565187792363520,
    "created_at" : "2017-12-26 08:01:23 +0000",
    "user" : {
      "name" : "Sitnik the Developer",
      "screen_name" : "sitnikcode",
      "protected" : false,
      "id_str" : "875500444537237506",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875509034656444416\/SGqgP1xx_normal.jpg",
      "id" : 875500444537237506,
      "verified" : false
    }
  },
  "id" : 945611753098043392,
  "created_at" : "2017-12-26 11:06:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/945417250294714368\/photo\/1",
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/TfLeFWPr00",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR7L-mbXUAEQjnh.jpg",
      "id_str" : "945417248122032129",
      "id" : 945417248122032129,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR7L-mbXUAEQjnh.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TfLeFWPr00"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/2JHeHNNK71",
      "expanded_url" : "http:\/\/ift.tt\/2l8Oz9M",
      "display_url" : "ift.tt\/2l8Oz9M"
    } ]
  },
  "geo" : { },
  "id_str" : "945417250294714368",
  "text" : "Xmas Present unwrapped. https:\/\/t.co\/2JHeHNNK71 https:\/\/t.co\/TfLeFWPr00",
  "id" : 945417250294714368,
  "created_at" : "2017-12-25 22:13:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Esko Hannula",
      "screen_name" : "eskohannula",
      "indices" : [ 3, 15 ],
      "id_str" : "947365579",
      "id" : 947365579
    }, {
      "name" : "toomas hendrik ilves",
      "screen_name" : "IlvesToomas",
      "indices" : [ 17, 29 ],
      "id_str" : "579747564",
      "id" : 579747564
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945404827395350528",
  "text" : "RT @eskohannula: @IlvesToomas Democratic insitutions are highly vulnerable if there is high freedom of speech but poor general education.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "toomas hendrik ilves",
        "screen_name" : "IlvesToomas",
        "indices" : [ 0, 12 ],
        "id_str" : "579747564",
        "id" : 579747564
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "945395995717746688",
    "geo" : { },
    "id_str" : "945404448578383874",
    "in_reply_to_user_id" : 579747564,
    "text" : "@IlvesToomas Democratic insitutions are highly vulnerable if there is high freedom of speech but poor general education.",
    "id" : 945404448578383874,
    "in_reply_to_status_id" : 945395995717746688,
    "created_at" : "2017-12-25 21:22:39 +0000",
    "in_reply_to_screen_name" : "IlvesToomas",
    "in_reply_to_user_id_str" : "579747564",
    "user" : {
      "name" : "Esko Hannula",
      "screen_name" : "eskohannula",
      "protected" : false,
      "id_str" : "947365579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/794259959055663104\/QvDsiNio_normal.jpg",
      "id" : 947365579,
      "verified" : false
    }
  },
  "id" : 945404827395350528,
  "created_at" : "2017-12-25 21:24:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Picture Ireland",
      "screen_name" : "PictureIreland",
      "indices" : [ 3, 18 ],
      "id_str" : "2190623134",
      "id" : 2190623134
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945403412224598017",
  "text" : "RT @PictureIreland: POTD: Christmas on Grafton Street by Emma McArdle. Hope everyone is enjoying the festivities - who\u2019s so full they can n\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Emma McArdle",
        "screen_name" : "McArdlePhoto",
        "indices" : [ 142, 155 ],
        "id_str" : "4354408581",
        "id" : 4354408581
      }, {
        "name" : "Photos of Dublin",
        "screen_name" : "PhotosOfDublin",
        "indices" : [ 180, 195 ],
        "id_str" : "1943866482",
        "id" : 1943866482
      }, {
        "name" : "AllAboutDublin",
        "screen_name" : "AllAboutDublin",
        "indices" : [ 196, 211 ],
        "id_str" : "746798742",
        "id" : 746798742
      }, {
        "name" : "Old DublinTown. com",
        "screen_name" : "OldDublinTown",
        "indices" : [ 212, 226 ],
        "id_str" : "473457952",
        "id" : 473457952
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PictureIreland\/status\/945396696191852544\/photo\/1",
        "indices" : [ 227, 250 ],
        "url" : "https:\/\/t.co\/9x89esn1WZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DR65PENXUAAjLts.jpg",
        "id_str" : "945396640273354752",
        "id" : 945396640273354752,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR65PENXUAAjLts.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9x89esn1WZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 156, 179 ],
        "url" : "https:\/\/t.co\/Szm1P8Rbxo",
        "expanded_url" : "http:\/\/www.picture-ireland.com\/blog\/",
        "display_url" : "picture-ireland.com\/blog\/"
      } ]
    },
    "geo" : { },
    "id_str" : "945396696191852544",
    "text" : "POTD: Christmas on Grafton Street by Emma McArdle. Hope everyone is enjoying the festivities - who\u2019s so full they can no longer function? \uD83D\uDE04\uD83C\uDF84\uD83C\uDF85\uD83C\uDFFB@McArdlePhoto https:\/\/t.co\/Szm1P8Rbxo @PhotosOfDublin @AllAboutDublin @OldDublinTown https:\/\/t.co\/9x89esn1WZ",
    "id" : 945396696191852544,
    "created_at" : "2017-12-25 20:51:51 +0000",
    "user" : {
      "name" : "Picture Ireland",
      "screen_name" : "PictureIreland",
      "protected" : false,
      "id_str" : "2190623134",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/912665164520001536\/9NhrklG4_normal.jpg",
      "id" : 2190623134,
      "verified" : false
    }
  },
  "id" : 945403412224598017,
  "created_at" : "2017-12-25 21:18:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/945363042216312832\/photo\/1",
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/DJyvBk7L1M",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR6arPiX4AA7JeG.jpg",
      "id_str" : "945363039490138112",
      "id" : 945363039490138112,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR6arPiX4AA7JeG.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DJyvBk7L1M"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/CJm9LoBCWb",
      "expanded_url" : "http:\/\/ift.tt\/2pxlLgM",
      "display_url" : "ift.tt\/2pxlLgM"
    } ]
  },
  "geo" : { },
  "id_str" : "945363042216312832",
  "text" : "Xmas dinner set aside for a family member who is working today. https:\/\/t.co\/CJm9LoBCWb https:\/\/t.co\/DJyvBk7L1M",
  "id" : 945363042216312832,
  "created_at" : "2017-12-25 18:38:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/945362839358844929\/photo\/1",
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/IWMrYsWxWK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR6affLWkAEBIdC.jpg",
      "id_str" : "945362837530120193",
      "id" : 945362837530120193,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR6affLWkAEBIdC.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IWMrYsWxWK"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 35 ],
      "url" : "https:\/\/t.co\/Sj4wdrhg3y",
      "expanded_url" : "http:\/\/ift.tt\/2kUf2Zz",
      "display_url" : "ift.tt\/2kUf2Zz"
    } ]
  },
  "geo" : { },
  "id_str" : "945362839358844929",
  "text" : "Xmas Dinner https:\/\/t.co\/Sj4wdrhg3y https:\/\/t.co\/IWMrYsWxWK",
  "id" : 945362839358844929,
  "created_at" : "2017-12-25 18:37:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/945349162031181824\/photo\/1",
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/8FjTAKc8LF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR6ODLPVoAAWbd8.jpg",
      "id_str" : "945349157002256384",
      "id" : 945349157002256384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR6ODLPVoAAWbd8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8FjTAKc8LF"
    } ],
    "hashtags" : [ {
      "text" : "MalaysiaBoleh",
      "indices" : [ 25, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945351467996770306",
  "text" : "RT @mrbrown: Who\u2019s Mary? #MalaysiaBoleh https:\/\/t.co\/8FjTAKc8LF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/945349162031181824\/photo\/1",
        "indices" : [ 27, 50 ],
        "url" : "https:\/\/t.co\/8FjTAKc8LF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DR6ODLPVoAAWbd8.jpg",
        "id_str" : "945349157002256384",
        "id" : 945349157002256384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR6ODLPVoAAWbd8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8FjTAKc8LF"
      } ],
      "hashtags" : [ {
        "text" : "MalaysiaBoleh",
        "indices" : [ 12, 26 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "945349162031181824",
    "text" : "Who\u2019s Mary? #MalaysiaBoleh https:\/\/t.co\/8FjTAKc8LF",
    "id" : 945349162031181824,
    "created_at" : "2017-12-25 17:42:58 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 945351467996770306,
  "created_at" : "2017-12-25 17:52:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "indices" : [ 3, 15 ],
      "id_str" : "3760488922",
      "id" : 3760488922
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/7rSpnA78YG",
      "expanded_url" : "https:\/\/blogs.scientificamerican.com\/guest-blog\/holiday-dinner-with-false-memories-for-dessert\/",
      "display_url" : "blogs.scientificamerican.com\/guest-blog\/hol\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945295689281626112",
  "text" : "RT @drjuliashaw: Merry Christmas!\n\nTry to not give each other false memories today...\n\nhttps:\/\/t.co\/7rSpnA78YG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/7rSpnA78YG",
        "expanded_url" : "https:\/\/blogs.scientificamerican.com\/guest-blog\/holiday-dinner-with-false-memories-for-dessert\/",
        "display_url" : "blogs.scientificamerican.com\/guest-blog\/hol\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "945294466318991360",
    "text" : "Merry Christmas!\n\nTry to not give each other false memories today...\n\nhttps:\/\/t.co\/7rSpnA78YG",
    "id" : 945294466318991360,
    "created_at" : "2017-12-25 14:05:38 +0000",
    "user" : {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "protected" : false,
      "id_str" : "3760488922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989936592591650817\/TRkR3cmy_normal.jpg",
      "id" : 3760488922,
      "verified" : true
    }
  },
  "id" : 945295689281626112,
  "created_at" : "2017-12-25 14:10:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/Hh9DzEbKG6",
      "expanded_url" : "https:\/\/twitter.com\/ExcelPope\/status\/944921706975637505",
      "display_url" : "twitter.com\/ExcelPope\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945293583745155072",
  "text" : "\uD83D\uDE01 https:\/\/t.co\/Hh9DzEbKG6",
  "id" : 945293583745155072,
  "created_at" : "2017-12-25 14:02:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daryl Ginn",
      "screen_name" : "darylginn",
      "indices" : [ 3, 13 ],
      "id_str" : "348429830",
      "id" : 348429830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945255127627321344",
  "text" : "RT @darylginn: Dear every website,\n\nNo we do not want to enable push notifications. Ever.\n\nSincerely,\nEveryone",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942051486745362432",
    "text" : "Dear every website,\n\nNo we do not want to enable push notifications. Ever.\n\nSincerely,\nEveryone",
    "id" : 942051486745362432,
    "created_at" : "2017-12-16 15:19:11 +0000",
    "user" : {
      "name" : "Daryl Ginn",
      "screen_name" : "darylginn",
      "protected" : false,
      "id_str" : "348429830",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040716734871887873\/ljTEsrSk_normal.jpg",
      "id" : 348429830,
      "verified" : false
    }
  },
  "id" : 945255127627321344,
  "created_at" : "2017-12-25 11:29:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "945246492536819712",
  "geo" : { },
  "id_str" : "945247370941526016",
  "in_reply_to_user_id" : 9465632,
  "text" : "In other words, you fallen in love with a concept and try to find a customer who would want it.",
  "id" : 945247370941526016,
  "in_reply_to_status_id" : 945246492536819712,
  "created_at" : "2017-12-25 10:58:29 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 0, 16 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "945245178889166848",
  "geo" : { },
  "id_str" : "945246566293622784",
  "in_reply_to_user_id" : 22700048,
  "text" : "@EimearMcCormack A Merry Xmas to you too.",
  "id" : 945246566293622784,
  "in_reply_to_status_id" : 945245178889166848,
  "created_at" : "2017-12-25 10:55:17 +0000",
  "in_reply_to_screen_name" : "EimearMcCormack",
  "in_reply_to_user_id_str" : "22700048",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "945246492536819712",
  "text" : "Should you try to convince your customers how they should use your products, rather than responding to how they wanted to use them?",
  "id" : 945246492536819712,
  "created_at" : "2017-12-25 10:55:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Co.Design",
      "screen_name" : "FastCoDesign",
      "indices" : [ 3, 16 ],
      "id_str" : "158865339",
      "id" : 158865339
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/FastCoDesign\/status\/944996602887929858\/photo\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/9ZxOdAPwEW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR1NZpnWkAANKoJ.jpg",
      "id_str" : "944996599880650752",
      "id" : 944996599880650752,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR1NZpnWkAANKoJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9ZxOdAPwEW"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/vPWJT3y5pf",
      "expanded_url" : "https:\/\/buff.ly\/2kF72eI",
      "display_url" : "buff.ly\/2kF72eI"
    } ]
  },
  "geo" : { },
  "id_str" : "945218044309266438",
  "text" : "RT @FastCoDesign: N.Y.C. passes first bill on bias and discrimination in algorithms https:\/\/t.co\/vPWJT3y5pf https:\/\/t.co\/9ZxOdAPwEW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FastCoDesign\/status\/944996602887929858\/photo\/1",
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/9ZxOdAPwEW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DR1NZpnWkAANKoJ.jpg",
        "id_str" : "944996599880650752",
        "id" : 944996599880650752,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR1NZpnWkAANKoJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9ZxOdAPwEW"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/vPWJT3y5pf",
        "expanded_url" : "https:\/\/buff.ly\/2kF72eI",
        "display_url" : "buff.ly\/2kF72eI"
      } ]
    },
    "geo" : { },
    "id_str" : "944996602887929858",
    "text" : "N.Y.C. passes first bill on bias and discrimination in algorithms https:\/\/t.co\/vPWJT3y5pf https:\/\/t.co\/9ZxOdAPwEW",
    "id" : 944996602887929858,
    "created_at" : "2017-12-24 18:22:01 +0000",
    "user" : {
      "name" : "Co.Design",
      "screen_name" : "FastCoDesign",
      "protected" : false,
      "id_str" : "158865339",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875778269324361729\/LKjZbw_s_normal.jpg",
      "id" : 158865339,
      "verified" : true
    }
  },
  "id" : 945218044309266438,
  "created_at" : "2017-12-25 09:01:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 0, 8 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "945081840527953920",
  "geo" : { },
  "id_str" : "945085674880995330",
  "in_reply_to_user_id" : 19360077,
  "text" : "@pdscott Happy Xmas to you",
  "id" : 945085674880995330,
  "in_reply_to_status_id" : 945081840527953920,
  "created_at" : "2017-12-25 00:15:58 +0000",
  "in_reply_to_screen_name" : "pdscott",
  "in_reply_to_user_id_str" : "19360077",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/945080901742071810\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/UOQsLSIMHS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR2aElYX4AA2_5H.jpg",
      "id_str" : "945080900362166272",
      "id" : 945080900362166272,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR2aElYX4AA2_5H.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UOQsLSIMHS"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/pZMvPqg0m1",
      "expanded_url" : "http:\/\/ift.tt\/2BK0uWR",
      "display_url" : "ift.tt\/2BK0uWR"
    } ]
  },
  "geo" : { },
  "id_str" : "945080901742071810",
  "text" : "Done. Xmas tree and present. https:\/\/t.co\/pZMvPqg0m1 https:\/\/t.co\/UOQsLSIMHS",
  "id" : 945080901742071810,
  "created_at" : "2017-12-24 23:57:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/HLIHRvHozf",
      "expanded_url" : "https:\/\/twitter.com\/mhmtkcn\/status\/944400193272684544",
      "display_url" : "twitter.com\/mhmtkcn\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945034029329502208",
  "text" : "COuld it be Santa doing a dry run for tonight event? https:\/\/t.co\/HLIHRvHozf",
  "id" : 945034029329502208,
  "created_at" : "2017-12-24 20:50:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 0, 9 ],
      "id_str" : "19456433",
      "id" : 19456433
    }, {
      "name" : "Channel 4",
      "screen_name" : "Channel4",
      "indices" : [ 10, 19 ],
      "id_str" : "183585551",
      "id" : 183585551
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "944979527712821248",
  "geo" : { },
  "id_str" : "945015640338911232",
  "in_reply_to_user_id" : 19456433,
  "text" : "@jonboyes @Channel4 Merry Christmas to you.",
  "id" : 945015640338911232,
  "in_reply_to_status_id" : 944979527712821248,
  "created_at" : "2017-12-24 19:37:40 +0000",
  "in_reply_to_screen_name" : "jonboyes",
  "in_reply_to_user_id_str" : "19456433",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/46qzCtWdxE",
      "expanded_url" : "https:\/\/twitter.com\/TarekFatah\/status\/944866062117625857",
      "display_url" : "twitter.com\/TarekFatah\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "945015383265808384",
  "text" : "Can't make this up. https:\/\/t.co\/46qzCtWdxE",
  "id" : 945015383265808384,
  "created_at" : "2017-12-24 19:36:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "944911322839470080",
  "geo" : { },
  "id_str" : "944990565048995840",
  "in_reply_to_user_id" : 9465632,
  "text" : "Instead of relying Likes, RT and new follow as KPI, this is going to be my new performance indicator of my activities on Twitter.",
  "id" : 944990565048995840,
  "in_reply_to_status_id" : 944911322839470080,
  "created_at" : "2017-12-24 17:58:02 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944982242966810624",
  "text" : "There are things that might be of value but may not be necessarily coveted.",
  "id" : 944982242966810624,
  "created_at" : "2017-12-24 17:24:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/944981999147732993\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/6nFr4D7O95",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DR1AGoyWsAA1idd.jpg",
      "id_str" : "944981979589685248",
      "id" : 944981979589685248,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DR1AGoyWsAA1idd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 546,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 546,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 546,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 363,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6nFr4D7O95"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944981999147732993",
  "text" : "Give your software engineering team a break. Don't deploy during the festive season. https:\/\/t.co\/6nFr4D7O95",
  "id" : 944981999147732993,
  "created_at" : "2017-12-24 17:24:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AlcoholActionIreland",
      "screen_name" : "AlcoholIreland",
      "indices" : [ 3, 18 ],
      "id_str" : "775710199",
      "id" : 775710199
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944981702597857280",
  "text" : "RT @AlcoholIreland: We applaud the public health advocates of Estonia as their parliament passes law further restricting sale of alcohol -\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Simon Harris TD",
        "screen_name" : "SimonHarrisTD",
        "indices" : [ 184, 198 ],
        "id_str" : "21117425",
        "id" : 21117425
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 159, 182 ],
        "url" : "https:\/\/t.co\/VpnWFRxFCv",
        "expanded_url" : "https:\/\/news.err.ee\/649925\/riigikogu-passes-law-further-restricting-sale-of-alcohol",
        "display_url" : "news.err.ee\/649925\/riigiko\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943805478236278784",
    "text" : "We applaud the public health advocates of Estonia as their parliament passes law further restricting sale of alcohol - another EU member making real progress: https:\/\/t.co\/VpnWFRxFCv \n@SimonHarrisTD",
    "id" : 943805478236278784,
    "created_at" : "2017-12-21 11:28:55 +0000",
    "user" : {
      "name" : "AlcoholActionIreland",
      "screen_name" : "AlcoholIreland",
      "protected" : false,
      "id_str" : "775710199",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2910276513\/bd9ddee066d7eb64915a236236cf87cf_normal.jpeg",
      "id" : 775710199,
      "verified" : false
    }
  },
  "id" : 944981702597857280,
  "created_at" : "2017-12-24 17:22:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/fCOYbBkcJ9",
      "expanded_url" : "https:\/\/twitter.com\/edwinksl\/status\/944813189778366464",
      "display_url" : "twitter.com\/edwinksl\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944980269966209025",
  "text" : "Anyone I know works at NVIDIA? https:\/\/t.co\/fCOYbBkcJ9",
  "id" : 944980269966209025,
  "created_at" : "2017-12-24 17:17:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "opendata",
      "indices" : [ 86, 95 ]
    } ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/PEKXaVzCxN",
      "expanded_url" : "http:\/\/api.census.gov\/data\/key_signup.html",
      "display_url" : "api.census.gov\/data\/key_signu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944976441070051329",
  "text" : "US Census Bureau provide a API key https:\/\/t.co\/PEKXaVzCxN to access the bureau data. #opendata",
  "id" : 944976441070051329,
  "created_at" : "2017-12-24 17:01:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chief Executive MPA",
      "screen_name" : "MaritimeHub",
      "indices" : [ 3, 15 ],
      "id_str" : "1837482440",
      "id" : 1837482440
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MaritimeHub\/status\/944622389475794944\/photo\/1",
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/M9m7TlgA3d",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRv5DlBU8AAst96.jpg",
      "id_str" : "944622386736852992",
      "id" : 944622386736852992,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRv5DlBU8AAst96.jpg",
      "sizes" : [ {
        "h" : 1021,
        "resize" : "fit",
        "w" : 1684
      }, {
        "h" : 728,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 412,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1021,
        "resize" : "fit",
        "w" : 1684
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/M9m7TlgA3d"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944963598148931584",
  "text" : "RT @MaritimeHub: https:\/\/t.co\/M9m7TlgA3d",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MaritimeHub\/status\/944622389475794944\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/M9m7TlgA3d",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRv5DlBU8AAst96.jpg",
        "id_str" : "944622386736852992",
        "id" : 944622386736852992,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRv5DlBU8AAst96.jpg",
        "sizes" : [ {
          "h" : 1021,
          "resize" : "fit",
          "w" : 1684
        }, {
          "h" : 728,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 412,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1021,
          "resize" : "fit",
          "w" : 1684
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/M9m7TlgA3d"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944622389475794944",
    "text" : "https:\/\/t.co\/M9m7TlgA3d",
    "id" : 944622389475794944,
    "created_at" : "2017-12-23 17:35:02 +0000",
    "user" : {
      "name" : "Chief Executive MPA",
      "screen_name" : "MaritimeHub",
      "protected" : false,
      "id_str" : "1837482440",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/666208966746083328\/XLr_kvV2_normal.jpg",
      "id" : 1837482440,
      "verified" : false
    }
  },
  "id" : 944963598148931584,
  "created_at" : "2017-12-24 16:10:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "944899666830913537",
  "geo" : { },
  "id_str" : "944913208866955264",
  "in_reply_to_user_id" : 9465632,
  "text" : "The inverse of Conway\u2019s Law ( Inverse Conway\u2019s Law) : the organizational structure of a company is determined by the architecture of its product.",
  "id" : 944913208866955264,
  "in_reply_to_status_id" : 944899666830913537,
  "created_at" : "2017-12-24 12:50:39 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/slR4AP4smz",
      "expanded_url" : "http:\/\/Ko-fi.com",
      "display_url" : "Ko-fi.com"
    }, {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/0GY4kYpb28",
      "expanded_url" : "https:\/\/ko-fi.com\/T6T16W2N",
      "display_url" : "ko-fi.com\/T6T16W2N"
    } ]
  },
  "geo" : { },
  "id_str" : "944911322839470080",
  "text" : "Support online content. Buy a Coffee for Mr. Yap with https:\/\/t.co\/slR4AP4smz https:\/\/t.co\/0GY4kYpb28 &lt; If you enjoy my what I share on Twitter, please consider supporting what I do. Thank you.",
  "id" : 944911322839470080,
  "created_at" : "2017-12-24 12:43:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944899666830913537",
  "text" : "TIL Conway\u2019s Law (named after programmer Melvin Conway in 1968) : that the architecture of a system will be determined by the communication and organizational structures of the company.",
  "id" : 944899666830913537,
  "created_at" : "2017-12-24 11:56:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ariane Sherine",
      "screen_name" : "ArianeSherine",
      "indices" : [ 3, 17 ],
      "id_str" : "18820955",
      "id" : 18820955
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944892600317829120",
  "text" : "RT @ArianeSherine: The most British story ever:\n\nFor 12 years, my husband gave his neighbour a six-pack of beer at Christmas, and received\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "936902302140588032",
    "text" : "The most British story ever:\n\nFor 12 years, my husband gave his neighbour a six-pack of beer at Christmas, and received a box of chocolates. They thanked each other warmly.\n\nMy husband couldn\u2019t eat the chocolates as he\u2019s vegan, and later found out that his neighbour was teetotal.",
    "id" : 936902302140588032,
    "created_at" : "2017-12-02 10:18:10 +0000",
    "user" : {
      "name" : "Ariane Sherine",
      "screen_name" : "ArianeSherine",
      "protected" : false,
      "id_str" : "18820955",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/873176482763468800\/xR8DGXwj_normal.jpg",
      "id" : 18820955,
      "verified" : true
    }
  },
  "id" : 944892600317829120,
  "created_at" : "2017-12-24 11:28:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "harryshum",
      "screen_name" : "harryshum",
      "indices" : [ 3, 13 ],
      "id_str" : "15636998",
      "id" : 15636998
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 116, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944881704711393280",
  "text" : "RT @harryshum: Interesting to read the smart thoughts of my AI colleagues around the world. 2017 was a big year for #AI and 2018 will be ev\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 101, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 160, 183 ],
        "url" : "https:\/\/t.co\/DpchLgYBU5",
        "expanded_url" : "https:\/\/www.axios.com\/leading-researchers-name-the-biggest-ai-story-of-2017-2518675638.html",
        "display_url" : "axios.com\/leading-resear\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943634534314131456",
    "text" : "Interesting to read the smart thoughts of my AI colleagues around the world. 2017 was a big year for #AI and 2018 will be even bigger \u2013 and I\u2019m excited for it! https:\/\/t.co\/DpchLgYBU5",
    "id" : 943634534314131456,
    "created_at" : "2017-12-21 00:09:39 +0000",
    "user" : {
      "name" : "harryshum",
      "screen_name" : "harryshum",
      "protected" : false,
      "id_str" : "15636998",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/473379244840480769\/Y4YeRso7_normal.png",
      "id" : 15636998,
      "verified" : true
    }
  },
  "id" : 944881704711393280,
  "created_at" : "2017-12-24 10:45:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MMitchell",
      "screen_name" : "mmitchell_ai",
      "indices" : [ 3, 16 ],
      "id_str" : "748528569064710145",
      "id" : 748528569064710145
    }, {
      "name" : "Kristian Lum",
      "screen_name" : "KLdivergence",
      "indices" : [ 41, 54 ],
      "id_str" : "32513057",
      "id" : 32513057
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/2twEwrI5jr",
      "expanded_url" : "https:\/\/datasociety.net\/events\/databite-no-90-kristian-lum\/",
      "display_url" : "datasociety.net\/events\/databit\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944881351047700482",
  "text" : "RT @mmitchell_ai: Really awesome talk by @KLdivergence on how human bias is amplified with machine learning. https:\/\/t.co\/2twEwrI5jr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Kristian Lum",
        "screen_name" : "KLdivergence",
        "indices" : [ 23, 36 ],
        "id_str" : "32513057",
        "id" : 32513057
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/2twEwrI5jr",
        "expanded_url" : "https:\/\/datasociety.net\/events\/databite-no-90-kristian-lum\/",
        "display_url" : "datasociety.net\/events\/databit\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "944762166447456256",
    "text" : "Really awesome talk by @KLdivergence on how human bias is amplified with machine learning. https:\/\/t.co\/2twEwrI5jr",
    "id" : 944762166447456256,
    "created_at" : "2017-12-24 02:50:27 +0000",
    "user" : {
      "name" : "MMitchell",
      "screen_name" : "mmitchell_ai",
      "protected" : false,
      "id_str" : "748528569064710145",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/759051761487187968\/24OG0RCC_normal.jpg",
      "id" : 748528569064710145,
      "verified" : false
    }
  },
  "id" : 944881351047700482,
  "created_at" : "2017-12-24 10:44:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/944873930833133568\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/N9tMacxiJK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRzd1QbX0AAbaLy.jpg",
      "id_str" : "944873928853540864",
      "id" : 944873928853540864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRzd1QbX0AAbaLy.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/N9tMacxiJK"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/XPV6P86hVF",
      "expanded_url" : "http:\/\/ift.tt\/2C5wILC",
      "display_url" : "ift.tt\/2C5wILC"
    } ]
  },
  "geo" : { },
  "id_str" : "944873930833133568",
  "text" : "Hypothesis: if u want to avoid the crowd, shop before the time restrictions https:\/\/t.co\/XPV6P86hVF https:\/\/t.co\/N9tMacxiJK",
  "id" : 944873930833133568,
  "created_at" : "2017-12-24 10:14:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scott Alexander",
      "screen_name" : "slatestarcodex",
      "indices" : [ 3, 18 ],
      "id_str" : "1526643050",
      "id" : 1526643050
    }, {
      "name" : "Apple",
      "screen_name" : "Apple",
      "indices" : [ 25, 31 ],
      "id_str" : "380749300",
      "id" : 380749300
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944841282677018624",
  "text" : "RT @slatestarcodex: Dear @apple - your OS has a global spellcheck that autocorrects names of medications to names of different medications,\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Apple",
        "screen_name" : "Apple",
        "indices" : [ 5, 11 ],
        "id_str" : "380749300",
        "id" : 380749300
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944739157988974592",
    "text" : "Dear @apple - your OS has a global spellcheck that autocorrects names of medications to names of different medications, eg \"duloxetine\" to \"fluoxetine\", without telling the user. Some clinics inexplicably continue to use MacBooks. Please fix this before someone gets hurt.",
    "id" : 944739157988974592,
    "created_at" : "2017-12-24 01:19:02 +0000",
    "user" : {
      "name" : "Scott Alexander",
      "screen_name" : "slatestarcodex",
      "protected" : false,
      "id_str" : "1526643050",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000009065862\/83d72de47e2a2470c482bc75406b42a7_normal.png",
      "id" : 1526643050,
      "verified" : false
    }
  },
  "id" : 944841282677018624,
  "created_at" : "2017-12-24 08:04:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944631384391462912",
  "text" : "Thought of the day : I have learned to seek my happiness by limiting my desires, rather than in attempting to satisfy them. - John Stuart Mill",
  "id" : 944631384391462912,
  "created_at" : "2017-12-23 18:10:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/r6xl4XlVWw",
      "expanded_url" : "http:\/\/www.bbc.com\/culture\/story\/20171215-how-did-a-christmas-carol-come-to-be?ocid=ww.social.link.twitter",
      "display_url" : "bbc.com\/culture\/story\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944603644216913920",
  "text" : "RT @pdscott: The story behind Dickens' A Christmas Carol  https:\/\/t.co\/r6xl4XlVWw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 45, 68 ],
        "url" : "https:\/\/t.co\/r6xl4XlVWw",
        "expanded_url" : "http:\/\/www.bbc.com\/culture\/story\/20171215-how-did-a-christmas-carol-come-to-be?ocid=ww.social.link.twitter",
        "display_url" : "bbc.com\/culture\/story\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "944567454168215552",
    "text" : "The story behind Dickens' A Christmas Carol  https:\/\/t.co\/r6xl4XlVWw",
    "id" : 944567454168215552,
    "created_at" : "2017-12-23 13:56:44 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 944603644216913920,
  "created_at" : "2017-12-23 16:20:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/RscgxjzPCj",
      "expanded_url" : "http:\/\/str.sg\/o4Hg",
      "display_url" : "str.sg\/o4Hg"
    } ]
  },
  "geo" : { },
  "id_str" : "944545588250423298",
  "text" : "Only a corrupted mind would see it that way. AirAsia, Firefly uniforms too revealing: KL lawmakers https:\/\/t.co\/RscgxjzPCj",
  "id" : 944545588250423298,
  "created_at" : "2017-12-23 12:29:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/944535587536728064\/photo\/1",
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/OzAmwnWOky",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRuqHCyXUAACH0O.jpg",
      "id_str" : "944535584848171008",
      "id" : 944535584848171008,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRuqHCyXUAACH0O.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OzAmwnWOky"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/rMQUQaLtVo",
      "expanded_url" : "http:\/\/ift.tt\/2D38cHP",
      "display_url" : "ift.tt\/2D38cHP"
    } ]
  },
  "geo" : { },
  "id_str" : "944535587536728064",
  "text" : "Good Morning. Percy French Rd, Dublin https:\/\/t.co\/rMQUQaLtVo https:\/\/t.co\/OzAmwnWOky",
  "id" : 944535587536728064,
  "created_at" : "2017-12-23 11:50:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/944518453733548032\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/aajer7wizn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRuahv0XcAAMTF6.jpg",
      "id_str" : "944518451426717696",
      "id" : 944518451426717696,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRuahv0XcAAMTF6.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/aajer7wizn"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/P8C8ASmflr",
      "expanded_url" : "http:\/\/ift.tt\/2D0V6uw",
      "display_url" : "ift.tt\/2D0V6uw"
    } ]
  },
  "geo" : { },
  "id_str" : "944518453733548032",
  "text" : "Trolley is now the most popular item at the supermarket https:\/\/t.co\/P8C8ASmflr https:\/\/t.co\/aajer7wizn",
  "id" : 944518453733548032,
  "created_at" : "2017-12-23 10:42:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944332698818146304",
  "text" : "Marketing people launching email campaign during this festive season runs the risk of getting unsubscribe.  Just unsubscribed a few. Thanks for the reminder to tidy my inbox. :)",
  "id" : 944332698818146304,
  "created_at" : "2017-12-22 22:23:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Manchester Airport",
      "screen_name" : "manairport",
      "indices" : [ 32, 43 ],
      "id_str" : "19913832",
      "id" : 19913832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944324413482328065",
  "text" : "There is no free wifi access at @manairport",
  "id" : 944324413482328065,
  "created_at" : "2017-12-22 21:50:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DBFreebie",
      "indices" : [ 0, 10 ]
    } ],
    "urls" : [ {
      "indices" : [ 11, 34 ],
      "url" : "https:\/\/t.co\/a7lJAgXLPh",
      "expanded_url" : "https:\/\/twitter.com\/dublinbusnews\/status\/943094462678470656",
      "display_url" : "twitter.com\/dublinbusnews\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944295255842656257",
  "text" : "#DBFreebie https:\/\/t.co\/a7lJAgXLPh",
  "id" : 944295255842656257,
  "created_at" : "2017-12-22 19:55:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brian Caulfield",
      "screen_name" : "BrianCVC",
      "indices" : [ 3, 12 ],
      "id_str" : "44916473",
      "id" : 44916473
    }, {
      "name" : "Michael Creed TD",
      "screen_name" : "creedcnw",
      "indices" : [ 36, 45 ],
      "id_str" : "386584224",
      "id" : 386584224
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944293875744759808",
  "text" : "RT @BrianCVC: Laughing listening to @creedcnw on RTE radio just now talking about bringing forward payments to farmers due to \"hardship\". M\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Michael Creed TD",
        "screen_name" : "creedcnw",
        "indices" : [ 22, 31 ],
        "id_str" : "386584224",
        "id" : 386584224
      }, {
        "name" : "Revenue",
        "screen_name" : "RevenueIE",
        "indices" : [ 135, 145 ],
        "id_str" : "200497731",
        "id" : 200497731
      }, {
        "name" : "Department of Finance",
        "screen_name" : "IRLDeptFinance",
        "indices" : [ 268, 283 ],
        "id_str" : "1310401350",
        "id" : 1310401350
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "startupsnotapriority",
        "indices" : [ 246, 267 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944127971106881536",
    "text" : "Laughing listening to @creedcnw on RTE radio just now talking about bringing forward payments to farmers due to \"hardship\". Meanwhile, @RevenueIE is pushing start-ups close to bankruptcy by withholding R&amp;D tax credits that are years overdue. #startupsnotapriority @IRLDeptFinance",
    "id" : 944127971106881536,
    "created_at" : "2017-12-22 08:50:24 +0000",
    "user" : {
      "name" : "Brian Caulfield",
      "screen_name" : "BrianCVC",
      "protected" : false,
      "id_str" : "44916473",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/568113014591610880\/vmc7j61S_normal.jpeg",
      "id" : 44916473,
      "verified" : false
    }
  },
  "id" : 944293875744759808,
  "created_at" : "2017-12-22 19:49:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AlcoholActionIreland",
      "screen_name" : "AlcoholIreland",
      "indices" : [ 3, 18 ],
      "id_str" : "775710199",
      "id" : 775710199
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944259487321509888",
  "text" : "RT @AlcoholIreland: At Christmas the burden of alcohol harm is especially borne by those around regular risky drinkers: family, friends, co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ibec",
        "screen_name" : "ibec_irl",
        "indices" : [ 210, 219 ],
        "id_str" : "161242354",
        "id" : 161242354
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "utterlyavoidable",
        "indices" : [ 162, 179 ]
      }, {
        "text" : "phabsaveslives",
        "indices" : [ 194, 209 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944235333591490560",
    "text" : "At Christmas the burden of alcohol harm is especially borne by those around regular risky drinkers: family, friends, co-workers, businesses, innocent bystanders! #utterlyavoidable \nTime to Act. #phabsaveslives @ibec_irl",
    "id" : 944235333591490560,
    "created_at" : "2017-12-22 15:57:01 +0000",
    "user" : {
      "name" : "AlcoholActionIreland",
      "screen_name" : "AlcoholIreland",
      "protected" : false,
      "id_str" : "775710199",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2910276513\/bd9ddee066d7eb64915a236236cf87cf_normal.jpeg",
      "id" : 775710199,
      "verified" : false
    }
  },
  "id" : 944259487321509888,
  "created_at" : "2017-12-22 17:32:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/medium.com\" rel=\"nofollow\"\u003EMedium\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/yRxxvaWRSC",
      "expanded_url" : "https:\/\/medium.com\/p\/making-data-visualise-78764f65036c",
      "display_url" : "medium.com\/p\/making-data-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944241217151303680",
  "text" : "I just published \u201CMaking Data Visualise\u201D https:\/\/t.co\/yRxxvaWRSC",
  "id" : 944241217151303680,
  "created_at" : "2017-12-22 16:20:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Olga",
      "screen_name" : "OlgaTsubiks",
      "indices" : [ 3, 15 ],
      "id_str" : "248482989",
      "id" : 248482989
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dataviz",
      "indices" : [ 51, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944212961987612674",
  "text" : "RT @OlgaTsubiks: Insightful Data Storytelling with #dataviz Seven charts that explain the plastic pollution problem - BBC News https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dataviz",
        "indices" : [ 34, 42 ]
      } ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/0kYImbFu9l",
        "expanded_url" : "http:\/\/www.bbc.com\/news\/science-environment-42264788",
        "display_url" : "bbc.com\/news\/science-e\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "944212297085530112",
    "text" : "Insightful Data Storytelling with #dataviz Seven charts that explain the plastic pollution problem - BBC News https:\/\/t.co\/0kYImbFu9l",
    "id" : 944212297085530112,
    "created_at" : "2017-12-22 14:25:28 +0000",
    "user" : {
      "name" : "Olga",
      "screen_name" : "OlgaTsubiks",
      "protected" : false,
      "id_str" : "248482989",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/708855385868869633\/TE-BZ_rA_normal.jpg",
      "id" : 248482989,
      "verified" : false
    }
  },
  "id" : 944212961987612674,
  "created_at" : "2017-12-22 14:28:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Red Dot Oz",
      "screen_name" : "RedDot_Oz",
      "indices" : [ 3, 13 ],
      "id_str" : "3223239390",
      "id" : 3223239390
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RedDot_Oz\/status\/944188568494149633\/photo\/1",
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/w8Trrr7Iki",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRpufFzUMAMlmld.jpg",
      "id_str" : "944188552299950083",
      "id" : 944188552299950083,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRpufFzUMAMlmld.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1452,
        "resize" : "fit",
        "w" : 976
      }, {
        "h" : 1452,
        "resize" : "fit",
        "w" : 976
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 807
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 457
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/w8Trrr7Iki"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944212369571500041",
  "text" : "RT @RedDot_Oz: Hahahahaha https:\/\/t.co\/w8Trrr7Iki",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RedDot_Oz\/status\/944188568494149633\/photo\/1",
        "indices" : [ 11, 34 ],
        "url" : "https:\/\/t.co\/w8Trrr7Iki",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRpufFzUMAMlmld.jpg",
        "id_str" : "944188552299950083",
        "id" : 944188552299950083,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRpufFzUMAMlmld.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1452,
          "resize" : "fit",
          "w" : 976
        }, {
          "h" : 1452,
          "resize" : "fit",
          "w" : 976
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 807
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 457
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/w8Trrr7Iki"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944188568494149633",
    "text" : "Hahahahaha https:\/\/t.co\/w8Trrr7Iki",
    "id" : 944188568494149633,
    "created_at" : "2017-12-22 12:51:11 +0000",
    "user" : {
      "name" : "Red Dot Oz",
      "screen_name" : "RedDot_Oz",
      "protected" : false,
      "id_str" : "3223239390",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006448707972227072\/Kpki4zN2_normal.jpg",
      "id" : 3223239390,
      "verified" : false
    }
  },
  "id" : 944212369571500041,
  "created_at" : "2017-12-22 14:25:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944208348068679680",
  "text" : "For freelancers and those work from home, there is no \"secret santa at work\"",
  "id" : 944208348068679680,
  "created_at" : "2017-12-22 14:09:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elea McDonnell Feit",
      "screen_name" : "eleafeit",
      "indices" : [ 3, 12 ],
      "id_str" : "107600670",
      "id" : 107600670
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/eleafeit\/status\/944198235530416129\/photo\/1",
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/iNvBGsfdMT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRp3RleW0AAmWVq.jpg",
      "id_str" : "944198215888457728",
      "id" : 944198215888457728,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRp3RleW0AAmWVq.jpg",
      "sizes" : [ {
        "h" : 1113,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 652,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1113,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/iNvBGsfdMT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944207307835150337",
  "text" : "RT @eleafeit: https:\/\/t.co\/iNvBGsfdMT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/eleafeit\/status\/944198235530416129\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/iNvBGsfdMT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRp3RleW0AAmWVq.jpg",
        "id_str" : "944198215888457728",
        "id" : 944198215888457728,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRp3RleW0AAmWVq.jpg",
        "sizes" : [ {
          "h" : 1113,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 652,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1113,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iNvBGsfdMT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944198235530416129",
    "text" : "https:\/\/t.co\/iNvBGsfdMT",
    "id" : 944198235530416129,
    "created_at" : "2017-12-22 13:29:36 +0000",
    "user" : {
      "name" : "Elea McDonnell Feit",
      "screen_name" : "eleafeit",
      "protected" : false,
      "id_str" : "107600670",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1022142398086963200\/M1AUZ37a_normal.jpg",
      "id" : 107600670,
      "verified" : false
    }
  },
  "id" : 944207307835150337,
  "created_at" : "2017-12-22 14:05:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/Rsu1YxYC8H",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Tangyuan_(food)",
      "display_url" : "en.wikipedia.org\/wiki\/Tangyuan_\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "944206704383811584",
  "geo" : { },
  "id_str" : "944206747924926464",
  "in_reply_to_user_id" : 9465632,
  "text" : "https:\/\/t.co\/Rsu1YxYC8H",
  "id" : 944206747924926464,
  "in_reply_to_status_id" : 944206704383811584,
  "created_at" : "2017-12-22 14:03:25 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u51AC\u81F3",
      "indices" : [ 0, 3 ]
    }, {
      "text" : "wintersolstice",
      "indices" : [ 4, 19 ]
    }, {
      "text" : "\u6C64\u5706",
      "indices" : [ 20, 23 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/HAhKOqLtE5",
      "expanded_url" : "https:\/\/www.instagram.com\/explore\/tags\/%E5%86%AC%E8%87%B3%E6%B1%A4%E5%9C%86\/",
      "display_url" : "instagram.com\/explore\/tags\/%\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944206704383811584",
  "text" : "#\u51AC\u81F3 #wintersolstice #\u6C64\u5706 Tangyuan (food)  \"soup ball\" https:\/\/t.co\/HAhKOqLtE5",
  "id" : 944206704383811584,
  "created_at" : "2017-12-22 14:03:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/kWonTE1LPr",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/Bc-CrfHn4Kw\/",
      "display_url" : "instagram.com\/p\/Bc-CrfHn4Kw\/"
    } ]
  },
  "geo" : { },
  "id_str" : "944197295314210816",
  "text" : "Place I call home https:\/\/t.co\/kWonTE1LPr",
  "id" : 944197295314210816,
  "created_at" : "2017-12-22 13:25:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Abroad",
      "screen_name" : "ITabroad",
      "indices" : [ 3, 12 ],
      "id_str" : "394027634",
      "id" : 394027634
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944193848217161728",
  "text" : "RT @ITabroad: \u201CWe fly regularly between Ireland and Singapore with young kids and the thought of the journey is always far worse than the r\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 166, 189 ],
        "url" : "https:\/\/t.co\/TJ5DvGPrXo",
        "expanded_url" : "https:\/\/www.irishtimes.com\/life-and-style\/abroad\/flying-home-for-christmas-with-the-kids-in-tow-1.3327341",
        "display_url" : "irishtimes.com\/life-and-style\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941754154938224640",
    "text" : "\u201CWe fly regularly between Ireland and Singapore with young kids and the thought of the journey is always far worse than the reality. Kids love 14 hours of cartoons.\u201D https:\/\/t.co\/TJ5DvGPrXo",
    "id" : 941754154938224640,
    "created_at" : "2017-12-15 19:37:42 +0000",
    "user" : {
      "name" : "Irish Times Abroad",
      "screen_name" : "ITabroad",
      "protected" : false,
      "id_str" : "394027634",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/793579775403126798\/hAgGxOnq_normal.jpg",
      "id" : 394027634,
      "verified" : false
    }
  },
  "id" : 944193848217161728,
  "created_at" : "2017-12-22 13:12:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 3, 15 ],
      "id_str" : "168148402",
      "id" : 168148402
    }, {
      "name" : "k-leb k",
      "screen_name" : "dcatdemon",
      "indices" : [ 118, 128 ],
      "id_str" : "130048954",
      "id" : 130048954
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944186957369835520",
  "text" : "RT @m4riannelee: A cosy Christmas lunch at Engebret Cafe.\nKjempe deilig og koselig julelunsj hos Engebret Cafe. 2017. @dcatdemon https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "k-leb k",
        "screen_name" : "dcatdemon",
        "indices" : [ 101, 111 ],
        "id_str" : "130048954",
        "id" : 130048954
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/944174469609934848\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/ls7jnfSyMd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRphjhDWAAAgtvm.jpg",
        "id_str" : "944174334683250688",
        "id" : 944174334683250688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRphjhDWAAAgtvm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ls7jnfSyMd"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/944174469609934848\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/ls7jnfSyMd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRphmgjWkAIPVoh.jpg",
        "id_str" : "944174386088677378",
        "id" : 944174386088677378,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRphmgjWkAIPVoh.jpg",
        "sizes" : [ {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ls7jnfSyMd"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/944174469609934848\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/ls7jnfSyMd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRphoprW4AEr9U4.jpg",
        "id_str" : "944174422897909761",
        "id" : 944174422897909761,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRphoprW4AEr9U4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ls7jnfSyMd"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/944174469609934848\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/ls7jnfSyMd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRphqC4XkAAZO-E.jpg",
        "id_str" : "944174446843236352",
        "id" : 944174446843236352,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRphqC4XkAAZO-E.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ls7jnfSyMd"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "944174469609934848",
    "text" : "A cosy Christmas lunch at Engebret Cafe.\nKjempe deilig og koselig julelunsj hos Engebret Cafe. 2017. @dcatdemon https:\/\/t.co\/ls7jnfSyMd",
    "id" : 944174469609934848,
    "created_at" : "2017-12-22 11:55:10 +0000",
    "user" : {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "protected" : false,
      "id_str" : "168148402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797414171038060544\/7AkMKjj5_normal.jpg",
      "id" : 168148402,
      "verified" : false
    }
  },
  "id" : 944186957369835520,
  "created_at" : "2017-12-22 12:44:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cronan McNamara",
      "screen_name" : "cronanmcnamara",
      "indices" : [ 3, 18 ],
      "id_str" : "14846672",
      "id" : 14846672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/EJbzpPkC7E",
      "expanded_url" : "https:\/\/www.indeedjobs.com\/creme-global\/_hl\/en_IE",
      "display_url" : "indeedjobs.com\/creme-global\/_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "944186123672223744",
  "text" : "RT @cronanmcnamara: Find your next great career opportunity at Creme Global. \nFour great jobs for the New Year! \nhttps:\/\/t.co\/EJbzpPkC7E \u2026\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "JobFairy",
        "indices" : [ 119, 128 ]
      }, {
        "text" : "NewYearNewJob",
        "indices" : [ 129, 143 ]
      }, {
        "text" : "DataScience",
        "indices" : [ 144, 156 ]
      }, {
        "text" : "SiliconDocks",
        "indices" : [ 157, 170 ]
      }, {
        "text" : "Dublin",
        "indices" : [ 171, 178 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/EJbzpPkC7E",
        "expanded_url" : "https:\/\/www.indeedjobs.com\/creme-global\/_hl\/en_IE",
        "display_url" : "indeedjobs.com\/creme-global\/_\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "944161789029298176",
    "text" : "Find your next great career opportunity at Creme Global. \nFour great jobs for the New Year! \nhttps:\/\/t.co\/EJbzpPkC7E \u2026\n#JobFairy #NewYearNewJob #DataScience #SiliconDocks #Dublin",
    "id" : 944161789029298176,
    "created_at" : "2017-12-22 11:04:46 +0000",
    "user" : {
      "name" : "Cronan McNamara",
      "screen_name" : "cronanmcnamara",
      "protected" : false,
      "id_str" : "14846672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/764055303688183808\/ctl5KPg__normal.jpg",
      "id" : 14846672,
      "verified" : true
    }
  },
  "id" : 944186123672223744,
  "created_at" : "2017-12-22 12:41:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944137732963667969",
  "text" : "How do you classify retargeting (i.e. remarketing) traffic in Google Analytics?",
  "id" : 944137732963667969,
  "created_at" : "2017-12-22 09:29:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "944121665931358208",
  "text" : "Going by\na tabloid headline, local authority plan to house homeless in prison!?",
  "id" : 944121665931358208,
  "created_at" : "2017-12-22 08:25:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "943544273626193920",
  "geo" : { },
  "id_str" : "943909649987854336",
  "in_reply_to_user_id" : 9465632,
  "text" : "If you want to avoid the pain of cross-matching your CRM\u2019s customer IDs or email addresses with your Analytics data, I can help.",
  "id" : 943909649987854336,
  "in_reply_to_status_id" : 943544273626193920,
  "created_at" : "2017-12-21 18:22:52 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Colette Browne",
      "screen_name" : "colettebrowne",
      "indices" : [ 3, 17 ],
      "id_str" : "75414115",
      "id" : 75414115
    }, {
      "name" : "Morning Ireland",
      "screen_name" : "morningireland",
      "indices" : [ 56, 71 ],
      "id_str" : "22790104",
      "id" : 22790104
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943902799779237890",
  "text" : "RT @colettebrowne: Every homeless person interviewed by @morningireland has been offered a home within a few days - so, to solve the housin\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Morning Ireland",
        "screen_name" : "morningireland",
        "indices" : [ 37, 52 ],
        "id_str" : "22790104",
        "id" : 22790104
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "943759547528773633",
    "text" : "Every homeless person interviewed by @morningireland has been offered a home within a few days - so, to solve the housing crisis, have them interview all 9,000",
    "id" : 943759547528773633,
    "created_at" : "2017-12-21 08:26:24 +0000",
    "user" : {
      "name" : "Colette Browne",
      "screen_name" : "colettebrowne",
      "protected" : false,
      "id_str" : "75414115",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/924196934826512386\/JFitWaXd_normal.jpg",
      "id" : 75414115,
      "verified" : false
    }
  },
  "id" : 943902799779237890,
  "created_at" : "2017-12-21 17:55:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943837364350316544",
  "text" : "Aer Lingus online check-in leave much to be desire about.  When you click the check-in-button on the Dashboard page, it brings you to another page asking for your check-in details again. Asking for name and family name",
  "id" : 943837364350316544,
  "created_at" : "2017-12-21 13:35:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/5KgcAidoQI",
      "expanded_url" : "https:\/\/asia.zeef.com\/mryap?ref=mryap&share=089273f22ffc4d0b8db9dfdd7d486e0d",
      "display_url" : "asia.zeef.com\/mryap?ref=mrya\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943806859743506432",
  "text" : "Handpicked curated resources on Asia https:\/\/t.co\/5KgcAidoQI",
  "id" : 943806859743506432,
  "created_at" : "2017-12-21 11:34:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NowThis",
      "screen_name" : "nowthisnews",
      "indices" : [ 3, 15 ],
      "id_str" : "701725963",
      "id" : 701725963
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/nowthisnews\/status\/941693599460204544\/video\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/ajOLxLeoR4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRGQR7LVwAAERez.jpg",
      "id_str" : "941691879065714688",
      "id" : 941691879065714688,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRGQR7LVwAAERez.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ajOLxLeoR4"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943748611896233984",
  "text" : "RT @nowthisnews: This millionaire is revealing his taxes to show just how much the GOP tax plan favors the wealthy https:\/\/t.co\/ajOLxLeoR4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nowthisnews\/status\/941693599460204544\/video\/1",
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/ajOLxLeoR4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRGQR7LVwAAERez.jpg",
        "id_str" : "941691879065714688",
        "id" : 941691879065714688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRGQR7LVwAAERez.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ajOLxLeoR4"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941693599460204544",
    "text" : "This millionaire is revealing his taxes to show just how much the GOP tax plan favors the wealthy https:\/\/t.co\/ajOLxLeoR4",
    "id" : 941693599460204544,
    "created_at" : "2017-12-15 15:37:04 +0000",
    "user" : {
      "name" : "NowThis",
      "screen_name" : "nowthisnews",
      "protected" : false,
      "id_str" : "701725963",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013778236734504966\/HWM6Dh-Q_normal.jpg",
      "id" : 701725963,
      "verified" : true
    }
  },
  "id" : 943748611896233984,
  "created_at" : "2017-12-21 07:42:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cabel",
      "screen_name" : "cabel",
      "indices" : [ 3, 9 ],
      "id_str" : "1919231",
      "id" : 1919231
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943748112320999424",
  "text" : "RT @cabel: macOS 10.13 Tip: have lots of iOS \/ Mac devices in your house? And a Mac that's usually on? Turn on \"Content Caching\" in Sharing\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cabel\/status\/943260941092667392\/photo\/1",
        "indices" : [ 253, 276 ],
        "url" : "https:\/\/t.co\/WeewCtcnnH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRch-ZlVAAAH7pm.png",
        "id_str" : "943260002860466176",
        "id" : 943260002860466176,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRch-ZlVAAAH7pm.png",
        "sizes" : [ {
          "h" : 1286,
          "resize" : "fit",
          "w" : 1560
        }, {
          "h" : 1286,
          "resize" : "fit",
          "w" : 1560
        }, {
          "h" : 561,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 989,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WeewCtcnnH"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 229, 252 ],
        "url" : "https:\/\/t.co\/xHCK8Fuot8",
        "expanded_url" : "https:\/\/support.apple.com\/guide\/mac-help\/about-content-caching-mchleaf1e61d\/mac",
        "display_url" : "support.apple.com\/guide\/mac-help\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943260941092667392",
    "text" : "macOS 10.13 Tip: have lots of iOS \/ Mac devices in your house? And a Mac that's usually on? Turn on \"Content Caching\" in Sharing prefs, and updates will be downloaded to all your devices from your Mac, saving time and bandwidth. https:\/\/t.co\/xHCK8Fuot8 https:\/\/t.co\/WeewCtcnnH",
    "id" : 943260941092667392,
    "created_at" : "2017-12-19 23:25:07 +0000",
    "user" : {
      "name" : "Cabel",
      "screen_name" : "cabel",
      "protected" : false,
      "id_str" : "1919231",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/763137848916660224\/Llcjv9V5_normal.jpg",
      "id" : 1919231,
      "verified" : true
    }
  },
  "id" : 943748112320999424,
  "created_at" : "2017-12-21 07:40:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Dean",
      "screen_name" : "JeffDean",
      "indices" : [ 3, 12 ],
      "id_str" : "911297187664949248",
      "id" : 911297187664949248
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943636843635118080",
  "text" : "RT @JeffDean: A really nice write-up (by someone outside Google) about the kinds of behind-the-scenes work that goes into taking lots of ki\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 193, 216 ],
        "url" : "https:\/\/t.co\/jcQN2zmYFy",
        "expanded_url" : "https:\/\/www.justinobeirne.com\/google-maps-moat",
        "display_url" : "justinobeirne.com\/google-maps-mo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943366264780595200",
    "text" : "A really nice write-up (by someone outside Google) about the kinds of behind-the-scenes work that goes into taking lots of kinds of raw data and turning it all into high quality Google Maps.\n \nhttps:\/\/t.co\/jcQN2zmYFy",
    "id" : 943366264780595200,
    "created_at" : "2017-12-20 06:23:39 +0000",
    "user" : {
      "name" : "Jeff Dean",
      "screen_name" : "JeffDean",
      "protected" : false,
      "id_str" : "911297187664949248",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/935325968280907776\/AcBo6zJc_normal.jpg",
      "id" : 911297187664949248,
      "verified" : true
    }
  },
  "id" : 943636843635118080,
  "created_at" : "2017-12-21 00:18:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mungerisms",
      "screen_name" : "mungerisms",
      "indices" : [ 3, 14 ],
      "id_str" : "112290748",
      "id" : 112290748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943634790338744320",
  "text" : "RT @mungerisms: The narrative about moats evaporating, buy and hold being dead etc is never more popular - and has some reality. But Ford a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "943131320246591489",
    "text" : "The narrative about moats evaporating, buy and hold being dead etc is never more popular - and has some reality. But Ford and GM still sell a lot of cars, MCD is still the fast food giant, MSFT is nearing $700 billion, 150 yr old RR's earn 25% ROE, and Wrigley still sells the gum",
    "id" : 943131320246591489,
    "created_at" : "2017-12-19 14:50:03 +0000",
    "user" : {
      "name" : "Mungerisms",
      "screen_name" : "mungerisms",
      "protected" : false,
      "id_str" : "112290748",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/676453454978531328\/m0nEvF2P_normal.jpg",
      "id" : 112290748,
      "verified" : false
    }
  },
  "id" : 943634790338744320,
  "created_at" : "2017-12-21 00:10:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/joI7Vz9Rb2",
      "expanded_url" : "https:\/\/www.huffingtonpost.com\/entry\/amazon-prime-homeless_us_5a38b6bbe4b0860bf4aaaadc?ncid=engmodushpmg00000004",
      "display_url" : "huffingtonpost.com\/entry\/amazon-p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943633842803478529",
  "text" : "Genius hack shows how to use Amazon Prime Now and other on-demand services to help the homeless https:\/\/t.co\/joI7Vz9Rb2",
  "id" : 943633842803478529,
  "created_at" : "2017-12-21 00:06:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julia Angwin",
      "screen_name" : "JuliaAngwin",
      "indices" : [ 3, 15 ],
      "id_str" : "17029640",
      "id" : 17029640
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943630493454684160",
  "text" : "RT @JuliaAngwin: Dozens of companies including Verizon, Amazon, UPS, Goldman Sachs - and Facebook itself - are using Facebook\u2019s micro targe\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "ProPublica",
        "screen_name" : "ProPublica",
        "indices" : [ 200, 211 ],
        "id_str" : "14606079",
        "id" : 14606079
      }, {
        "name" : "NYT",
        "screen_name" : "NYT",
        "indices" : [ 212, 216 ],
        "id_str" : "1255671",
        "id" : 1255671
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JuliaAngwin\/status\/943613477805019136\/photo\/1",
        "indices" : [ 258, 281 ],
        "url" : "https:\/\/t.co\/wJIYARTjC0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRhcxhLWAAA6q4v.jpg",
        "id_str" : "943606127723413504",
        "id" : 943606127723413504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRhcxhLWAAA6q4v.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 738
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1376,
          "resize" : "fit",
          "w" : 846
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 418
        }, {
          "h" : 1376,
          "resize" : "fit",
          "w" : 846
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wJIYARTjC0"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 234, 257 ],
        "url" : "https:\/\/t.co\/XBl5utfGrp",
        "expanded_url" : "https:\/\/www.propublica.org\/article\/facebook-ads-age-discrimination-targeting",
        "display_url" : "propublica.org\/article\/facebo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943613477805019136",
    "text" : "Dozens of companies including Verizon, Amazon, UPS, Goldman Sachs - and Facebook itself - are using Facebook\u2019s micro targeting to ensure that their job ads are only shown to younger viewers \u2014 a joint @Propublica @NYT investigation. \n\nhttps:\/\/t.co\/XBl5utfGrp https:\/\/t.co\/wJIYARTjC0",
    "id" : 943613477805019136,
    "created_at" : "2017-12-20 22:45:59 +0000",
    "user" : {
      "name" : "Julia Angwin",
      "screen_name" : "JuliaAngwin",
      "protected" : false,
      "id_str" : "17029640",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3429661270\/09a1e02cb01fd352d9749ab27681bb25_normal.jpeg",
      "id" : 17029640,
      "verified" : false
    }
  },
  "id" : 943630493454684160,
  "created_at" : "2017-12-20 23:53:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darragh Doyle",
      "screen_name" : "darraghdoyle",
      "indices" : [ 3, 16 ],
      "id_str" : "2200721",
      "id" : 2200721
    }, {
      "name" : "SVP - Ireland",
      "screen_name" : "SVP_Ireland",
      "indices" : [ 34, 46 ],
      "id_str" : "201755115",
      "id" : 201755115
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/darraghdoyle\/status\/943452991365025792\/photo\/1",
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/9tJbNXEyES",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRfReOAW4AA2wH_.jpg",
      "id_str" : "943452964043284480",
      "id" : 943452964043284480,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRfReOAW4AA2wH_.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9tJbNXEyES"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943620974838153217",
  "text" : "RT @darraghdoyle: Powerful ad for @SVP_Ireland https:\/\/t.co\/9tJbNXEyES",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SVP - Ireland",
        "screen_name" : "SVP_Ireland",
        "indices" : [ 16, 28 ],
        "id_str" : "201755115",
        "id" : 201755115
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/darraghdoyle\/status\/943452991365025792\/photo\/1",
        "indices" : [ 29, 52 ],
        "url" : "https:\/\/t.co\/9tJbNXEyES",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRfReOAW4AA2wH_.jpg",
        "id_str" : "943452964043284480",
        "id" : 943452964043284480,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRfReOAW4AA2wH_.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9tJbNXEyES"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "943452991365025792",
    "text" : "Powerful ad for @SVP_Ireland https:\/\/t.co\/9tJbNXEyES",
    "id" : 943452991365025792,
    "created_at" : "2017-12-20 12:08:16 +0000",
    "user" : {
      "name" : "Darragh Doyle",
      "screen_name" : "darraghdoyle",
      "protected" : false,
      "id_str" : "2200721",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/649950567951900672\/9oElFhuS_normal.jpg",
      "id" : 2200721,
      "verified" : false
    }
  },
  "id" : 943620974838153217,
  "created_at" : "2017-12-20 23:15:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943600982105247744",
  "text" : "The OAP does not looks like she is 81.  OAP in Iceland are healthy and fit. Swimming pools are available at every corner of the city. \nTHE Real Marigold On Tour on BBC1",
  "id" : 943600982105247744,
  "created_at" : "2017-12-20 21:56:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/sj9jVq9PON",
      "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/943540538057416705",
      "display_url" : "twitter.com\/getoptimise\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943544273626193920",
  "text" : "How to capture traffic referral information in a browser and the analysis and optimizations you can get out of it. https:\/\/t.co\/sj9jVq9PON",
  "id" : 943544273626193920,
  "created_at" : "2017-12-20 18:10:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 134, 157 ],
      "url" : "https:\/\/t.co\/ZUm3WXl5Sm",
      "expanded_url" : "https:\/\/www.forrester.com\/report\/Predictions+2018+The+CMO+Bar+Rises+With+More+Pressure+For+Growth\/-\/E-RES140082",
      "display_url" : "forrester.com\/report\/Predict\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943498153734623232",
  "text" : "Forrester report highlights that \u201Ccustomer obsession\u201D will be key to organizational success in 2018. Take them so long to figure out? https:\/\/t.co\/ZUm3WXl5Sm",
  "id" : 943498153734623232,
  "created_at" : "2017-12-20 15:07:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HireYap",
      "indices" : [ 66, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943495626431582208",
  "text" : "Another job applications sent in.  This itself is an achievement. #HireYap",
  "id" : 943495626431582208,
  "created_at" : "2017-12-20 14:57:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "943494361525956608",
  "geo" : { },
  "id_str" : "943495421888008192",
  "in_reply_to_user_id" : 9465632,
  "text" : "Or unused credit for cloud computing",
  "id" : 943495421888008192,
  "in_reply_to_status_id" : 943494361525956608,
  "created_at" : "2017-12-20 14:56:52 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "943494361525956608",
  "geo" : { },
  "id_str" : "943495005511081985",
  "in_reply_to_user_id" : 9465632,
  "text" : "Forget to put in \"dying\" before startups",
  "id" : 943495005511081985,
  "in_reply_to_status_id" : 943494361525956608,
  "created_at" : "2017-12-20 14:55:13 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943494361525956608",
  "text" : "Any startups in Dublin want to sell me a ping pong table?",
  "id" : 943494361525956608,
  "created_at" : "2017-12-20 14:52:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ahmed Zahid",
      "screen_name" : "ahmedzahid",
      "indices" : [ 3, 14 ],
      "id_str" : "15330518",
      "id" : 15330518
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ahmedzahid\/status\/943434444479725568\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/jVkv4SPubv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRfAmJfVQAAdiVG.jpg",
      "id_str" : "943434408572305408",
      "id" : 943434408572305408,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRfAmJfVQAAdiVG.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jVkv4SPubv"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/ahmedzahid\/status\/943434444479725568\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/jVkv4SPubv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRfAmwjUIAE4HLk.jpg",
      "id_str" : "943434419057991681",
      "id" : 943434419057991681,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRfAmwjUIAE4HLk.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 731
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1248
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1248
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 414
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jVkv4SPubv"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943463190360444928",
  "text" : "RT @ahmedzahid: Our country. Other countries. Today. https:\/\/t.co\/jVkv4SPubv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ahmedzahid\/status\/943434444479725568\/photo\/1",
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/jVkv4SPubv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRfAmJfVQAAdiVG.jpg",
        "id_str" : "943434408572305408",
        "id" : 943434408572305408,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRfAmJfVQAAdiVG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jVkv4SPubv"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/ahmedzahid\/status\/943434444479725568\/photo\/1",
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/jVkv4SPubv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRfAmwjUIAE4HLk.jpg",
        "id_str" : "943434419057991681",
        "id" : 943434419057991681,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRfAmwjUIAE4HLk.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 731
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1248
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1248
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 414
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jVkv4SPubv"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "943434444479725568",
    "text" : "Our country. Other countries. Today. https:\/\/t.co\/jVkv4SPubv",
    "id" : 943434444479725568,
    "created_at" : "2017-12-20 10:54:34 +0000",
    "user" : {
      "name" : "Ahmed Zahid",
      "screen_name" : "ahmedzahid",
      "protected" : false,
      "id_str" : "15330518",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1876210557\/AhmedZahid_dp2_normal.jpg",
      "id" : 15330518,
      "verified" : false
    }
  },
  "id" : 943463190360444928,
  "created_at" : "2017-12-20 12:48:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    }, {
      "name" : "Trinity College Dublin",
      "screen_name" : "tcddublin",
      "indices" : [ 35, 45 ],
      "id_str" : "31537951",
      "id" : 31537951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/ex5AlEUuu2",
      "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/943393638486282241",
      "display_url" : "twitter.com\/TheEconomist\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943455452385415168",
  "text" : "RT @ronanlyons: EC4020 students at @tcddublin will of course be particularly interested in this. https:\/\/t.co\/ex5AlEUuu2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Trinity College Dublin",
        "screen_name" : "tcddublin",
        "indices" : [ 19, 29 ],
        "id_str" : "31537951",
        "id" : 31537951
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/ex5AlEUuu2",
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/943393638486282241",
        "display_url" : "twitter.com\/TheEconomist\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943425291279650816",
    "text" : "EC4020 students at @tcddublin will of course be particularly interested in this. https:\/\/t.co\/ex5AlEUuu2",
    "id" : 943425291279650816,
    "created_at" : "2017-12-20 10:18:12 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 943455452385415168,
  "created_at" : "2017-12-20 12:18:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 0, 16 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "943441950799253506",
  "geo" : { },
  "id_str" : "943455111430500354",
  "in_reply_to_user_id" : 22700048,
  "text" : "@EimearMcCormack Nope from the Economy class. Not sure about biz and first class.",
  "id" : 943455111430500354,
  "in_reply_to_status_id" : 943441950799253506,
  "created_at" : "2017-12-20 12:16:41 +0000",
  "in_reply_to_screen_name" : "EimearMcCormack",
  "in_reply_to_user_id_str" : "22700048",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andrew Ng",
      "screen_name" : "AndrewYNg",
      "indices" : [ 3, 13 ],
      "id_str" : "216939636",
      "id" : 216939636
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943413445659185153",
  "text" : "RT @AndrewYNg: Announcing my new company! Landing.ai will help companies transform for the AI era. We\u2019re starting with the manufacturing in\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/QlTLJQwOkL",
        "expanded_url" : "https:\/\/medium.com\/@andrewng\/revitalizing-manufacturing-through-ai-a9ad32e07814",
        "display_url" : "medium.com\/@andrewng\/revi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941314523989815301",
    "text" : "Announcing my new company! Landing.ai will help companies transform for the AI era. We\u2019re starting with the manufacturing industry. https:\/\/t.co\/QlTLJQwOkL",
    "id" : 941314523989815301,
    "created_at" : "2017-12-14 14:30:45 +0000",
    "user" : {
      "name" : "Andrew Ng",
      "screen_name" : "AndrewYNg",
      "protected" : false,
      "id_str" : "216939636",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/733174243714682880\/oyG30NEH_normal.jpg",
      "id" : 216939636,
      "verified" : true
    }
  },
  "id" : 943413445659185153,
  "created_at" : "2017-12-20 09:31:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Whitenack",
      "screen_name" : "dwhitena",
      "indices" : [ 3, 12 ],
      "id_str" : "104241306",
      "id" : 104241306
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 25, 28 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943413247679586304",
  "text" : "RT @dwhitena: \"Practical #AI is about creating models that can be wrapped into software components for deployment into products and service\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Chris Benson",
        "screen_name" : "chrisbenson",
        "indices" : [ 129, 141 ],
        "id_str" : "14687075",
        "id" : 14687075
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 11, 14 ]
      }, {
        "text" : "machinelearning",
        "indices" : [ 142, 158 ]
      }, {
        "text" : "deeplearning",
        "indices" : [ 159, 172 ]
      }, {
        "text" : "datascience",
        "indices" : [ 173, 185 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941757296933294080",
    "text" : "\"Practical #AI is about creating models that can be wrapped into software components for deployment into products and services\" -@chrisbenson #machinelearning #deeplearning #datascience",
    "id" : 941757296933294080,
    "created_at" : "2017-12-15 19:50:11 +0000",
    "user" : {
      "name" : "Daniel Whitenack",
      "screen_name" : "dwhitena",
      "protected" : false,
      "id_str" : "104241306",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037818598306140160\/Dxs3g-1C_normal.jpg",
      "id" : 104241306,
      "verified" : false
    }
  },
  "id" : 943413247679586304,
  "created_at" : "2017-12-20 09:30:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Brake MP",
      "screen_name" : "thomasbrake",
      "indices" : [ 14, 26 ],
      "id_str" : "21666641",
      "id" : 21666641
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943396912690089984",
  "text" : "RT @RJonesUX: @thomasbrake In a rational country, Dominic Grieve and Kier Starmer would lead their parties, Nigel Farage would be deputy ma\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tom Brake MP",
        "screen_name" : "thomasbrake",
        "indices" : [ 0, 12 ],
        "id_str" : "21666641",
        "id" : 21666641
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "943064867463872512",
    "geo" : { },
    "id_str" : "943075652529516544",
    "in_reply_to_user_id" : 21666641,
    "text" : "@thomasbrake In a rational country, Dominic Grieve and Kier Starmer would lead their parties, Nigel Farage would be deputy manager of a Wetherspoons in Penge, and Boris Johnson would be a children's entertainer on administrative leave pending the outcome of a disciplinary hearing.",
    "id" : 943075652529516544,
    "in_reply_to_status_id" : 943064867463872512,
    "created_at" : "2017-12-19 11:08:51 +0000",
    "in_reply_to_screen_name" : "thomasbrake",
    "in_reply_to_user_id_str" : "21666641",
    "user" : {
      "name" : "Russ \uD83D\uDC1D",
      "screen_name" : "RussInCheshire",
      "protected" : false,
      "id_str" : "498753368",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026769424852951040\/IFAUiLH0_normal.jpg",
      "id" : 498753368,
      "verified" : false
    }
  },
  "id" : 943396912690089984,
  "created_at" : "2017-12-20 08:25:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dorothy Ryan",
      "screen_name" : "Dotwebs",
      "indices" : [ 3, 11 ],
      "id_str" : "16984046",
      "id" : 16984046
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/a3kOA7M06m",
      "expanded_url" : "https:\/\/twitter.com\/adweak\/status\/943266828490383360",
      "display_url" : "twitter.com\/adweak\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943391435956674560",
  "text" : "RT @Dotwebs: And 2016, 2015, 2014...etc etc https:\/\/t.co\/a3kOA7M06m",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 31, 54 ],
        "url" : "https:\/\/t.co\/a3kOA7M06m",
        "expanded_url" : "https:\/\/twitter.com\/adweak\/status\/943266828490383360",
        "display_url" : "twitter.com\/adweak\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943380717937020928",
    "text" : "And 2016, 2015, 2014...etc etc https:\/\/t.co\/a3kOA7M06m",
    "id" : 943380717937020928,
    "created_at" : "2017-12-20 07:21:04 +0000",
    "user" : {
      "name" : "Dorothy Ryan",
      "screen_name" : "Dotwebs",
      "protected" : false,
      "id_str" : "16984046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/754655766515253248\/e1Qkb51Y_normal.jpg",
      "id" : 16984046,
      "verified" : false
    }
  },
  "id" : 943391435956674560,
  "created_at" : "2017-12-20 08:03:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mashable",
      "screen_name" : "mashable",
      "indices" : [ 3, 12 ],
      "id_str" : "972651",
      "id" : 972651
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mashable\/status\/943381473544908800\/video\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/nOcmsJnTyI",
      "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/918131870704869376\/img\/vzGH9NcZcRRZR_pF.jpg",
      "id_str" : "918131870704869376",
      "id" : 918131870704869376,
      "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/918131870704869376\/img\/vzGH9NcZcRRZR_pF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nOcmsJnTyI"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943391332021821440",
  "text" : "RT @mashable: This shop functions without any plastic packaging https:\/\/t.co\/nOcmsJnTyI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mashable\/status\/943381473544908800\/video\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/nOcmsJnTyI",
        "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/918131870704869376\/img\/vzGH9NcZcRRZR_pF.jpg",
        "id_str" : "918131870704869376",
        "id" : 918131870704869376,
        "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/918131870704869376\/img\/vzGH9NcZcRRZR_pF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nOcmsJnTyI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "943381473544908800",
    "text" : "This shop functions without any plastic packaging https:\/\/t.co\/nOcmsJnTyI",
    "id" : 943381473544908800,
    "created_at" : "2017-12-20 07:24:05 +0000",
    "user" : {
      "name" : "Mashable",
      "screen_name" : "mashable",
      "protected" : false,
      "id_str" : "972651",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013772445243895808\/jk7SUWdn_normal.jpg",
      "id" : 972651,
      "verified" : true
    }
  },
  "id" : 943391332021821440,
  "created_at" : "2017-12-20 08:03:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/rTik0j67gn",
      "expanded_url" : "https:\/\/twitter.com\/topgold\/status\/943388945924198404",
      "display_url" : "twitter.com\/topgold\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943390912218062848",
  "text" : "My favourite is \"Use NRR to mean No Reply Required - thank you!\" https:\/\/t.co\/rTik0j67gn",
  "id" : 943390912218062848,
  "created_at" : "2017-12-20 08:01:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/zlIncALFiX",
      "expanded_url" : "https:\/\/mklnd.com\/2D6x7uI",
      "display_url" : "mklnd.com\/2D6x7uI"
    } ]
  },
  "geo" : { },
  "id_str" : "943286853779841024",
  "text" : "RT @getoptimise: If you\u2019re marketing to developers, Your API is your key marketing tool. https:\/\/t.co\/zlIncALFiX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/zlIncALFiX",
        "expanded_url" : "https:\/\/mklnd.com\/2D6x7uI",
        "display_url" : "mklnd.com\/2D6x7uI"
      } ]
    },
    "geo" : { },
    "id_str" : "943271623611822081",
    "text" : "If you\u2019re marketing to developers, Your API is your key marketing tool. https:\/\/t.co\/zlIncALFiX",
    "id" : 943271623611822081,
    "created_at" : "2017-12-20 00:07:34 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 943286853779841024,
  "created_at" : "2017-12-20 01:08:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Navdeep Bains",
      "screen_name" : "NavdeepSBains",
      "indices" : [ 3, 17 ],
      "id_str" : "234395010",
      "id" : 234395010
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startups",
      "indices" : [ 64, 73 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943285838607249408",
  "text" : "RT @NavdeepSBains: Canada\u2019s openness to people and ideas allows #startups to thrive &amp; gives us a competitive edge on the global stage. Grea\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Monocle Magazine",
        "screen_name" : "MonocleMag",
        "indices" : [ 135, 146 ],
        "id_str" : "3246035697",
        "id" : 3246035697
      }, {
        "name" : "Montr\u00E9al Intl",
        "screen_name" : "MTLINTL",
        "indices" : [ 195, 203 ],
        "id_str" : "264367497",
        "id" : 264367497
      }, {
        "name" : "Startupfest",
        "screen_name" : "startupfest",
        "indices" : [ 204, 216 ],
        "id_str" : "237330000",
        "id" : 237330000
      }, {
        "name" : "BDC",
        "screen_name" : "bdc_ca",
        "indices" : [ 217, 224 ],
        "id_str" : "57743497",
        "id" : 57743497
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "startups",
        "indices" : [ 45, 54 ]
      }, {
        "text" : "cdnpoli",
        "indices" : [ 172, 180 ]
      }, {
        "text" : "GlobalSkills",
        "indices" : [ 181, 194 ]
      } ],
      "urls" : [ {
        "indices" : [ 148, 171 ],
        "url" : "https:\/\/t.co\/0I20AkK573",
        "expanded_url" : "https:\/\/monocle.com\/film\/affairs\/why-start-ups-thrive-in-canada\/",
        "display_url" : "monocle.com\/film\/affairs\/w\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943276117804404737",
    "text" : "Canada\u2019s openness to people and ideas allows #startups to thrive &amp; gives us a competitive edge on the global stage. Great story by @MonocleMag: https:\/\/t.co\/0I20AkK573 #cdnpoli #GlobalSkills\n@MTLINTL @startupfest @bdc_ca",
    "id" : 943276117804404737,
    "created_at" : "2017-12-20 00:25:26 +0000",
    "user" : {
      "name" : "Navdeep Bains",
      "screen_name" : "NavdeepSBains",
      "protected" : false,
      "id_str" : "234395010",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/941744453378891776\/321Jsy-E_normal.jpg",
      "id" : 234395010,
      "verified" : true
    }
  },
  "id" : 943285838607249408,
  "created_at" : "2017-12-20 01:04:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HuffPost Tech",
      "screen_name" : "HuffPostTech",
      "indices" : [ 82, 95 ],
      "id_str" : "73147282",
      "id" : 73147282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/TyqHUvtSzZ",
      "expanded_url" : "https:\/\/www.huffingtonpost.com\/brian-de-haaff\/the-product-manager-vs-th_b_7659184.html?ncid=engmodushpmg00000004",
      "display_url" : "huffingtonpost.com\/brian-de-haaff\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943154324364054530",
  "text" : "The Product Manager vs. the Product Marketing Manager https:\/\/t.co\/TyqHUvtSzZ via @HuffPostTech",
  "id" : 943154324364054530,
  "created_at" : "2017-12-19 16:21:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/943105530150703106\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/hxFG2g6sz9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRaVeY0XUAAemo7.jpg",
      "id_str" : "943105521271394304",
      "id" : 943105521271394304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRaVeY0XUAAemo7.jpg",
      "sizes" : [ {
        "h" : 394,
        "resize" : "fit",
        "w" : 639
      }, {
        "h" : 394,
        "resize" : "fit",
        "w" : 639
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 394,
        "resize" : "fit",
        "w" : 639
      }, {
        "h" : 394,
        "resize" : "fit",
        "w" : 639
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hxFG2g6sz9"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "943103016802152450",
  "geo" : { },
  "id_str" : "943105530150703106",
  "in_reply_to_user_id" : 9465632,
  "text" : "https:\/\/t.co\/hxFG2g6sz9",
  "id" : 943105530150703106,
  "in_reply_to_status_id" : 943103016802152450,
  "created_at" : "2017-12-19 13:07:35 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DSlearnings",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943103016802152450",
  "text" : "#DSlearnings Looking at the correlations is an effective way to locate multicollinearity problems. Model results can be misleading if we\u2019re not familiar with the underlying correlations.",
  "id" : 943103016802152450,
  "created_at" : "2017-12-19 12:57:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gizmodo",
      "screen_name" : "Gizmodo",
      "indices" : [ 3, 11 ],
      "id_str" : "2890961",
      "id" : 2890961
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/t5ZZjg8qv1",
      "expanded_url" : "http:\/\/gizmo.do\/Zp1kWkk",
      "display_url" : "gizmo.do\/Zp1kWkk"
    } ]
  },
  "geo" : { },
  "id_str" : "943100838943363075",
  "text" : "RT @Gizmodo: British cops want to use AI to spot porn\u2014but it keeps mistaking desert pics for nudes https:\/\/t.co\/t5ZZjg8qv1 https:\/\/t.co\/gQr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Gizmodo\/status\/942797655331233792\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/gQrVOjR2DN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRV9eFjXkAAyh-x.jpg",
        "id_str" : "942797652844056576",
        "id" : 942797652844056576,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRV9eFjXkAAyh-x.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 449,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 449,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 449,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gQrVOjR2DN"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/t5ZZjg8qv1",
        "expanded_url" : "http:\/\/gizmo.do\/Zp1kWkk",
        "display_url" : "gizmo.do\/Zp1kWkk"
      } ]
    },
    "geo" : { },
    "id_str" : "942797655331233792",
    "text" : "British cops want to use AI to spot porn\u2014but it keeps mistaking desert pics for nudes https:\/\/t.co\/t5ZZjg8qv1 https:\/\/t.co\/gQrVOjR2DN",
    "id" : 942797655331233792,
    "created_at" : "2017-12-18 16:44:12 +0000",
    "user" : {
      "name" : "Gizmodo",
      "screen_name" : "Gizmodo",
      "protected" : false,
      "id_str" : "2890961",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/590508740010348544\/eS1F7mN5_normal.png",
      "id" : 2890961,
      "verified" : true
    }
  },
  "id" : 943100838943363075,
  "created_at" : "2017-12-19 12:48:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/medium.com\" rel=\"nofollow\"\u003EMedium\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/IfwoM8m5ah",
      "expanded_url" : "https:\/\/medium.com\/p\/a-seismic-shift-in-business-models-is-already-here-878895ea994",
      "display_url" : "medium.com\/p\/a-seismic-sh\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943100184258580480",
  "text" : "I just published \u201CA seismic shift in business models is already here\u201D https:\/\/t.co\/IfwoM8m5ah",
  "id" : 943100184258580480,
  "created_at" : "2017-12-19 12:46:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "indices" : [ 3, 14 ],
      "id_str" : "2870653293",
      "id" : 2870653293
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/erVpYWzqPL",
      "expanded_url" : "https:\/\/twitter.com\/munkle\/status\/943052511648321536",
      "display_url" : "twitter.com\/munkle\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "943066839671861248",
  "text" : "RT @susie_dent: It\u2019s from the 19th century and smugglers who concealed bottles in the long leg part of their boots. https:\/\/t.co\/erVpYWzqPL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/erVpYWzqPL",
        "expanded_url" : "https:\/\/twitter.com\/munkle\/status\/943052511648321536",
        "display_url" : "twitter.com\/munkle\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "943059070277255168",
    "text" : "It\u2019s from the 19th century and smugglers who concealed bottles in the long leg part of their boots. https:\/\/t.co\/erVpYWzqPL",
    "id" : 943059070277255168,
    "created_at" : "2017-12-19 10:02:58 +0000",
    "user" : {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "protected" : false,
      "id_str" : "2870653293",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531825590878228480\/4s2F9F6W_normal.jpeg",
      "id" : 2870653293,
      "verified" : true
    }
  },
  "id" : 943066839671861248,
  "created_at" : "2017-12-19 10:33:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Singler",
      "screen_name" : "Thobava",
      "indices" : [ 3, 11 ],
      "id_str" : "131877988",
      "id" : 131877988
    }, {
      "name" : "Behavioural Exchange",
      "screen_name" : "BXconference",
      "indices" : [ 80, 93 ],
      "id_str" : "2327976546",
      "id" : 2327976546
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Thobava\/status\/880689873010794496\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/AxS4rgSSzF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDjWwwbWsAAapqU.jpg",
      "id_str" : "880689860272631808",
      "id" : 880689860272631808,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDjWwwbWsAAapqU.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/AxS4rgSSzF"
    } ],
    "hashtags" : [ {
      "text" : "BX2017",
      "indices" : [ 94, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943064165375266816",
  "text" : "RT @Thobava: Behavioral Science is now mainstream in Singapore by Kok Ping Soon @BXconference #BX2017 https:\/\/t.co\/AxS4rgSSzF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Behavioural Exchange",
        "screen_name" : "BXconference",
        "indices" : [ 67, 80 ],
        "id_str" : "2327976546",
        "id" : 2327976546
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Thobava\/status\/880689873010794496\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/AxS4rgSSzF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDjWwwbWsAAapqU.jpg",
        "id_str" : "880689860272631808",
        "id" : 880689860272631808,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDjWwwbWsAAapqU.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AxS4rgSSzF"
      } ],
      "hashtags" : [ {
        "text" : "BX2017",
        "indices" : [ 81, 88 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "880689873010794496",
    "text" : "Behavioral Science is now mainstream in Singapore by Kok Ping Soon @BXconference #BX2017 https:\/\/t.co\/AxS4rgSSzF",
    "id" : 880689873010794496,
    "created_at" : "2017-06-30 07:30:02 +0000",
    "user" : {
      "name" : "Eric Singler",
      "screen_name" : "Thobava",
      "protected" : false,
      "id_str" : "131877988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/769575241882492929\/Q_cp-M4Y_normal.jpg",
      "id" : 131877988,
      "verified" : false
    }
  },
  "id" : 943064165375266816,
  "created_at" : "2017-12-19 10:23:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 3, 14 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943053753854365696",
  "text" : "RT @IrishTimes: Dublin has been rated one of the worst cities in the world to emigrate to due to the lack of affordable housing and high co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/app.sendblur.com\" rel=\"nofollow\"\u003ESocial Media Publisher App \u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/sio2YrqfBQ",
        "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/dublin-ranked-second-worst-major-european-city-for-quality-of-life-1.3292814",
        "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "931084582513807361",
    "text" : "Dublin has been rated one of the worst cities in the world to emigrate to due to the lack of affordable housing and high cost of living. https:\/\/t.co\/sio2YrqfBQ",
    "id" : 931084582513807361,
    "created_at" : "2017-11-16 09:00:37 +0000",
    "user" : {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "protected" : false,
      "id_str" : "15084853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2944517635\/5492cf569e6b0b1a9326a7d388009b60_normal.png",
      "id" : 15084853,
      "verified" : true
    }
  },
  "id" : 943053753854365696,
  "created_at" : "2017-12-19 09:41:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andreeeeeeeeaaaaaa!",
      "screen_name" : "brandalisms",
      "indices" : [ 3, 15 ],
      "id_str" : "249318820",
      "id" : 249318820
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "943034614158852097",
  "text" : "RT @brandalisms: A reminder that Eastern Europe is made of more than what you know and are shown. A lot more.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942038242131574784",
    "text" : "A reminder that Eastern Europe is made of more than what you know and are shown. A lot more.",
    "id" : 942038242131574784,
    "created_at" : "2017-12-16 14:26:33 +0000",
    "user" : {
      "name" : "Andreeeeeeeeaaaaaa!",
      "screen_name" : "brandalisms",
      "protected" : false,
      "id_str" : "249318820",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011698829190672385\/PynKXsQ__normal.jpg",
      "id" : 249318820,
      "verified" : false
    }
  },
  "id" : 943034614158852097,
  "created_at" : "2017-12-19 08:25:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "indices" : [ 3, 15 ],
      "id_str" : "21564577",
      "id" : 21564577
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942810849277771776",
  "text" : "RT @chris_byrne: 1\/3 If you plan to make a Christmas gift of Alcohol may I considerately suggest an alternative such as supporting a local\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942712116380921856",
    "text" : "1\/3 If you plan to make a Christmas gift of Alcohol may I considerately suggest an alternative such as supporting a local business especially our craftspeople, artisans and food producers.",
    "id" : 942712116380921856,
    "created_at" : "2017-12-18 11:04:17 +0000",
    "user" : {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "protected" : false,
      "id_str" : "21564577",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028212548859043840\/9i6LBuYR_normal.jpg",
      "id" : 21564577,
      "verified" : false
    }
  },
  "id" : 942810849277771776,
  "created_at" : "2017-12-18 17:36:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jake Lambert",
      "screen_name" : "LittleLostLad",
      "indices" : [ 3, 17 ],
      "id_str" : "209011131",
      "id" : 209011131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942804613559914498",
  "text" : "RT @LittleLostLad: Two men on my train are laughing at a group of young Star Wars fans because they've dressed up as their favourite charac\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942019236540702721",
    "text" : "Two men on my train are laughing at a group of young Star Wars fans because they've dressed up as their favourite characters to go and see the film. The two men are on their way to watch Arsenal, and are both wearing Arsenal shirts.",
    "id" : 942019236540702721,
    "created_at" : "2017-12-16 13:11:02 +0000",
    "user" : {
      "name" : "Jake Lambert",
      "screen_name" : "LittleLostLad",
      "protected" : false,
      "id_str" : "209011131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973490390723715073\/fxcxuxPv_normal.jpg",
      "id" : 209011131,
      "verified" : false
    }
  },
  "id" : 942804613559914498,
  "created_at" : "2017-12-18 17:11:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942786640732409858",
  "text" : "RT @getoptimise: The 5 most useful figures from Google Analytics that your company should be gathering, watching and putting to good use.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941278986226855937",
    "text" : "The 5 most useful figures from Google Analytics that your company should be gathering, watching and putting to good use.",
    "id" : 941278986226855937,
    "created_at" : "2017-12-14 12:09:33 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 942786640732409858,
  "created_at" : "2017-12-18 16:00:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Lewis",
      "screen_name" : "gattaca",
      "indices" : [ 3, 11 ],
      "id_str" : "1088411",
      "id" : 1088411
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942785840157282304",
  "text" : "RT @gattaca: My daughter went to a laser quest birthday party yesterday. She set her username to \u201Ca girl\u201D. I asked why she didn\u2019t pick \u2018blo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942413602153254914",
    "text" : "My daughter went to a laser quest birthday party yesterday. She set her username to \u201Ca girl\u201D. I asked why she didn\u2019t pick \u2018bloodstone\u2019 or \u2018skull-crusher\u2019 and she said, \u201CSo every time I zapped a boy they\u2019d know it was a girl that took them out.\u201D\n\nProud Dad.",
    "id" : 942413602153254914,
    "created_at" : "2017-12-17 15:18:06 +0000",
    "user" : {
      "name" : "Dave Lewis",
      "screen_name" : "gattaca",
      "protected" : false,
      "id_str" : "1088411",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/750107896432631808\/U-XiGxND_normal.jpg",
      "id" : 1088411,
      "verified" : true
    }
  },
  "id" : 942785840157282304,
  "created_at" : "2017-12-18 15:57:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/GnbT0vO68a",
      "expanded_url" : "http:\/\/johnnydecimal.com\/",
      "display_url" : "johnnydecimal.com"
    } ]
  },
  "geo" : { },
  "id_str" : "942780243483545600",
  "text" : "A system to organise a collection of things you keep on a computer. https:\/\/t.co\/GnbT0vO68a",
  "id" : 942780243483545600,
  "created_at" : "2017-12-18 15:35:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/U11uFkIidY",
      "expanded_url" : "https:\/\/twitter.com\/LiquidNewsroom\/status\/941807339342004225",
      "display_url" : "twitter.com\/LiquidNewsroom\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942779716964159488",
  "text" : "I just don\u2019t know enough about that to have formed an opinion. https:\/\/t.co\/U11uFkIidY",
  "id" : 942779716964159488,
  "created_at" : "2017-12-18 15:32:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942772502970994689",
  "text" : "RT @jackiepenangkit: Pre-order Christmas Menu is out today!!\nPenang street food delight:\nA) Turmeric Curry chicken rice Penang style \u00A36.90\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942772168945033216",
    "text" : "Pre-order Christmas Menu is out today!!\nPenang street food delight:\nA) Turmeric Curry chicken rice Penang style \u00A36.90\nB) Jawa Noddles \u00A37.50\nC) Asam Laksa \u00A37.30\n\nPlease place your order for a chance to sample the authentic Penang food.",
    "id" : 942772168945033216,
    "created_at" : "2017-12-18 15:02:55 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 942772502970994689,
  "created_at" : "2017-12-18 15:04:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942762834374615046",
  "text" : "RT @jackiepenangkit: Working on a Pre-order Christmas Menu. Watch this space",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942762415267213312",
    "text" : "Working on a Pre-order Christmas Menu. Watch this space",
    "id" : 942762415267213312,
    "created_at" : "2017-12-18 14:24:10 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 942762834374615046,
  "created_at" : "2017-12-18 14:25:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JobFairy",
      "indices" : [ 123, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942761017641185281",
  "text" : "RT @EimearMcCormack: I'm looking to hire a nutritionist for a project we're working on. Does anyone have a recommendation? #JobFairy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "JobFairy",
        "indices" : [ 102, 111 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942756646870290432",
    "text" : "I'm looking to hire a nutritionist for a project we're working on. Does anyone have a recommendation? #JobFairy",
    "id" : 942756646870290432,
    "created_at" : "2017-12-18 14:01:14 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 942761017641185281,
  "created_at" : "2017-12-18 14:18:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Colin Strong",
      "screen_name" : "Colinstrong",
      "indices" : [ 3, 15 ],
      "id_str" : "41788528",
      "id" : 41788528
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tech",
      "indices" : [ 71, 76 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942729631056220160",
  "text" : "RT @Colinstrong: Interesting POV on increasingly negative attitudes to #tech.  Brands engaged in digital transformation surely need to cons\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tech",
        "indices" : [ 54, 59 ]
      } ],
      "urls" : [ {
        "indices" : [ 148, 171 ],
        "url" : "https:\/\/t.co\/rDPkcLMpBy",
        "expanded_url" : "http:\/\/ow.ly\/7I9T30hhRV5",
        "display_url" : "ow.ly\/7I9T30hhRV5"
      } ]
    },
    "geo" : { },
    "id_str" : "942724912657174529",
    "text" : "Interesting POV on increasingly negative attitudes to #tech.  Brands engaged in digital transformation surely need to consider some of these issues https:\/\/t.co\/rDPkcLMpBy",
    "id" : 942724912657174529,
    "created_at" : "2017-12-18 11:55:08 +0000",
    "user" : {
      "name" : "Colin Strong",
      "screen_name" : "Colinstrong",
      "protected" : false,
      "id_str" : "41788528",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/702176915025739776\/oGSiiFsm_normal.jpg",
      "id" : 41788528,
      "verified" : false
    }
  },
  "id" : 942729631056220160,
  "created_at" : "2017-12-18 12:13:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942729446875910145",
  "text" : "RT @mrbrown: The Blackvue car camera: Singapore's Most Popular Internet Content Creation Tool.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942727928852373505",
    "text" : "The Blackvue car camera: Singapore's Most Popular Internet Content Creation Tool.",
    "id" : 942727928852373505,
    "created_at" : "2017-12-18 12:07:07 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 942729446875910145,
  "created_at" : "2017-12-18 12:13:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/v9XpFVz6sN",
      "expanded_url" : "https:\/\/medium.com\/@mryap\/return-on-ad-spend-roas-as-primary-kpi-for-search-6d079b745c15",
      "display_url" : "medium.com\/@mryap\/return-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942483075564531712",
  "text" : "Return on Ad Spend (ROAS) as primary KPI for search https:\/\/t.co\/v9XpFVz6sN",
  "id" : 942483075564531712,
  "created_at" : "2017-12-17 19:54:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "942372321863651329",
  "geo" : { },
  "id_str" : "942376029586427904",
  "in_reply_to_user_id" : 9465632,
  "text" : "Last command I use is docker rmi -f $(docker images -f \"dangling=true\" -q) to remove  items that have a &lt;none&gt; repository and a &lt;none&gt; tag.",
  "id" : 942376029586427904,
  "in_reply_to_status_id" : 942372321863651329,
  "created_at" : "2017-12-17 12:48:48 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/942372321863651329\/photo\/1",
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/NWaeaeXHLt",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRP6mcdX0AE0thW.jpg",
      "id_str" : "942372285431926785",
      "id" : 942372285431926785,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRP6mcdX0AE0thW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 361,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 637,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 725,
        "resize" : "fit",
        "w" : 1365
      }, {
        "h" : 725,
        "resize" : "fit",
        "w" : 1365
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NWaeaeXHLt"
    } ],
    "hashtags" : [ {
      "text" : "Docker",
      "indices" : [ 52, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942372321863651329",
  "text" : "All my Docker Images are gone after Windows update. #Docker https:\/\/t.co\/NWaeaeXHLt",
  "id" : 942372321863651329,
  "created_at" : "2017-12-17 12:34:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Motyl",
      "screen_name" : "MattMotyl",
      "indices" : [ 3, 13 ],
      "id_str" : "18353044",
      "id" : 18353044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942343009584271362",
  "text" : "RT @MattMotyl: Using Google Street View-based study demonstrates that types of cars predict who lives in different neighborhoods. One of th\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 255, 278 ],
        "url" : "https:\/\/t.co\/LrIlulNHjm",
        "expanded_url" : "https:\/\/www.citylab.com\/transportation\/2017\/12\/google-street-view-data-demographics-cars-research\/547436\/?utm_source=SFFB?utm_source=twb",
        "display_url" : "citylab.com\/transportation\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "940724842936127488",
    "text" : "Using Google Street View-based study demonstrates that types of cars predict who lives in different neighborhoods. One of the biggest effects? Ratio of sedans:pick-up trucks accurately identifies up to 97% of precinct-level votes for the Democratic party https:\/\/t.co\/LrIlulNHjm",
    "id" : 940724842936127488,
    "created_at" : "2017-12-12 23:27:35 +0000",
    "user" : {
      "name" : "Matt Motyl",
      "screen_name" : "MattMotyl",
      "protected" : false,
      "id_str" : "18353044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/927688201044709378\/uBps8WCP_normal.jpg",
      "id" : 18353044,
      "verified" : false
    }
  },
  "id" : 942343009584271362,
  "created_at" : "2017-12-17 10:37:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942341395746050048",
  "text" : "I think the only industry that can accurately predict lifetime value is where customers are one\u2010and\u2010done: wedding grown. Elizabeth Taylor is an outlier.",
  "id" : 942341395746050048,
  "created_at" : "2017-12-17 10:31:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/942337157766832129\/photo\/1",
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/EERM2UkoVW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRPaoKSXUAAo1J7.jpg",
      "id_str" : "942337130541568000",
      "id" : 942337130541568000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRPaoKSXUAAo1J7.jpg",
      "sizes" : [ {
        "h" : 49,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 74,
        "resize" : "crop",
        "w" : 74
      }, {
        "h" : 74,
        "resize" : "fit",
        "w" : 1035
      }, {
        "h" : 74,
        "resize" : "fit",
        "w" : 1035
      }, {
        "h" : 74,
        "resize" : "fit",
        "w" : 1035
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EERM2UkoVW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942337157766832129",
  "text" : "Anyone know how to block minecraft on internet router? Thanks https:\/\/t.co\/EERM2UkoVW",
  "id" : 942337157766832129,
  "created_at" : "2017-12-17 10:14:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/34JoMgIv9j",
      "expanded_url" : "https:\/\/twitter.com\/RTELateLateShow\/status\/941803293000347648",
      "display_url" : "twitter.com\/RTELateLateSho\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942336997787602945",
  "text" : "Is there a metoo hashtag for someone who have depression?https:\/\/t.co\/34JoMgIv9j",
  "id" : 942336997787602945,
  "created_at" : "2017-12-17 10:13:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "942332764086525952",
  "geo" : { },
  "id_str" : "942334163990011904",
  "in_reply_to_user_id" : 9465632,
  "text" : "If I drink and drive and do not buckle up, have an accident and meet an untimely death, I can't expect my family to ask the state for funeral expenses.",
  "id" : 942334163990011904,
  "in_reply_to_status_id" : 942332764086525952,
  "created_at" : "2017-12-17 10:02:27 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 0, 12 ],
      "id_str" : "168148402",
      "id" : 168148402
    }, {
      "name" : "k-leb k",
      "screen_name" : "dcatdemon",
      "indices" : [ 13, 23 ],
      "id_str" : "130048954",
      "id" : 130048954
    }, {
      "name" : "bakkwabeelee",
      "screen_name" : "muipo1",
      "indices" : [ 24, 31 ],
      "id_str" : "504283113",
      "id" : 504283113
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "942308128170938368",
  "geo" : { },
  "id_str" : "942317531158720512",
  "in_reply_to_user_id" : 168148402,
  "text" : "@m4riannelee @dcatdemon @muipo1 Do the Norway have those weather alert warning?",
  "id" : 942317531158720512,
  "in_reply_to_status_id" : 942308128170938368,
  "created_at" : "2017-12-17 08:56:21 +0000",
  "in_reply_to_screen_name" : "m4riannelee",
  "in_reply_to_user_id_str" : "168148402",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/942307649147858944\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/XEe2wo1ORZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRO_pe9W0AAVCCD.jpg",
      "id_str" : "942307466456518656",
      "id" : 942307466456518656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRO_pe9W0AAVCCD.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 301,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 532,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 595,
        "resize" : "fit",
        "w" : 1342
      }, {
        "h" : 595,
        "resize" : "fit",
        "w" : 1342
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XEe2wo1ORZ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "942306398184050688",
  "geo" : { },
  "id_str" : "942307649147858944",
  "in_reply_to_user_id" : 9465632,
  "text" : "This framework helps you to figure out how to go about moving between customer and product development. https:\/\/t.co\/XEe2wo1ORZ",
  "id" : 942307649147858944,
  "in_reply_to_status_id" : 942306398184050688,
  "created_at" : "2017-12-17 08:17:05 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "942306392093941760",
  "geo" : { },
  "id_str" : "942306398184050688",
  "in_reply_to_user_id" : 9465632,
  "text" : "When you follow unchecked assumptions about your customers, you run the risk of making mistakes, missing key business opportunities, or producing products that make little impact.",
  "id" : 942306398184050688,
  "in_reply_to_status_id" : 942306392093941760,
  "created_at" : "2017-12-17 08:12:07 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "indices" : [ 3, 19 ],
      "id_str" : "51263711",
      "id" : 51263711
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/941734973069058049\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/KLGaCtbJHW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRG292iVoAIBy2B.png",
      "id_str" : "941734970824957954",
      "id" : 941734970824957954,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRG292iVoAIBy2B.png",
      "sizes" : [ {
        "h" : 410,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KLGaCtbJHW"
    } ],
    "hashtags" : [ {
      "text" : "measure",
      "indices" : [ 97, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/gm2uSg3a3L",
      "expanded_url" : "https:\/\/goo.gl\/Cim71p",
      "display_url" : "goo.gl\/Cim71p"
    } ]
  },
  "geo" : { },
  "id_str" : "942302171965554688",
  "text" : "RT @googleanalytics: New ways to measure your users in Google Analytics: https:\/\/t.co\/gm2uSg3a3L #measure https:\/\/t.co\/KLGaCtbJHW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/941734973069058049\/photo\/1",
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/KLGaCtbJHW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRG292iVoAIBy2B.png",
        "id_str" : "941734970824957954",
        "id" : 941734970824957954,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRG292iVoAIBy2B.png",
        "sizes" : [ {
          "h" : 410,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KLGaCtbJHW"
      } ],
      "hashtags" : [ {
        "text" : "measure",
        "indices" : [ 76, 84 ]
      } ],
      "urls" : [ {
        "indices" : [ 52, 75 ],
        "url" : "https:\/\/t.co\/gm2uSg3a3L",
        "expanded_url" : "https:\/\/goo.gl\/Cim71p",
        "display_url" : "goo.gl\/Cim71p"
      } ]
    },
    "geo" : { },
    "id_str" : "941734973069058049",
    "text" : "New ways to measure your users in Google Analytics: https:\/\/t.co\/gm2uSg3a3L #measure https:\/\/t.co\/KLGaCtbJHW",
    "id" : 941734973069058049,
    "created_at" : "2017-12-15 18:21:28 +0000",
    "user" : {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "protected" : false,
      "id_str" : "51263711",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021848775885651968\/cU74ahCn_normal.jpg",
      "id" : 51263711,
      "verified" : true
    }
  },
  "id" : 942302171965554688,
  "created_at" : "2017-12-17 07:55:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SolidaritySleepout",
      "indices" : [ 0, 19 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/c6E0VDyqzd",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/942139997582512129",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942300595624562688",
  "text" : "#SolidaritySleepout https:\/\/t.co\/c6E0VDyqzd",
  "id" : 942300595624562688,
  "created_at" : "2017-12-17 07:49:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melinda Gates",
      "screen_name" : "melindagates",
      "indices" : [ 3, 16 ],
      "id_str" : "161801527",
      "id" : 161801527
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942300092765294592",
  "text" : "RT @melindagates: Even though bad news tends to dominate the headlines, by many important measures of progress, the world is actually becom\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 195, 218 ],
        "url" : "https:\/\/t.co\/knnj2fhJsB",
        "expanded_url" : "http:\/\/m-gat.es\/2CmBBgy",
        "display_url" : "m-gat.es\/2CmBBgy"
      } ]
    },
    "geo" : { },
    "id_str" : "941367951919341570",
    "text" : "Even though bad news tends to dominate the headlines, by many important measures of progress, the world is actually becoming a better place. Here is some good news you may have missed this year.\nhttps:\/\/t.co\/knnj2fhJsB",
    "id" : 941367951919341570,
    "created_at" : "2017-12-14 18:03:04 +0000",
    "user" : {
      "name" : "Melinda Gates",
      "screen_name" : "melindagates",
      "protected" : false,
      "id_str" : "161801527",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/576426383989080065\/HoFESJ2k_normal.jpeg",
      "id" : 161801527,
      "verified" : true
    }
  },
  "id" : 942300092765294592,
  "created_at" : "2017-12-17 07:47:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/xmVEeEYlwZ",
      "expanded_url" : "https:\/\/twitter.com\/thejournal_ie\/status\/942186331970686976",
      "display_url" : "twitter.com\/thejournal_ie\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942298854841552896",
  "text" : "For some reason this part of the world, people have aversion for ID card. https:\/\/t.co\/xmVEeEYlwZ",
  "id" : 942298854841552896,
  "created_at" : "2017-12-17 07:42:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942292999714885632",
  "text" : "RT @patphelan: Nothing like another huge capital gains payment tells you Ireland hates entrepreneurs",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "942004279304433664",
    "text" : "Nothing like another huge capital gains payment tells you Ireland hates entrepreneurs",
    "id" : 942004279304433664,
    "created_at" : "2017-12-16 12:11:36 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 942292999714885632,
  "created_at" : "2017-12-17 07:18:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tommy Levi",
      "screen_name" : "tslevi",
      "indices" : [ 3, 10 ],
      "id_str" : "81710054",
      "id" : 81710054
    }, {
      "name" : "Wes McKinney",
      "screen_name" : "wesmckinn",
      "indices" : [ 12, 22 ],
      "id_str" : "115494880",
      "id" : 115494880
    }, {
      "name" : "Tim Hopper \uD83D\uDCDF",
      "screen_name" : "tdhopper",
      "indices" : [ 23, 32 ],
      "id_str" : "89249164",
      "id" : 89249164
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942170062781329410",
  "text" : "RT @tslevi: @wesmckinn @tdhopper Once, someone told me they don't need data scientists to design experiments because Excel has a chi-square\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Wes McKinney",
        "screen_name" : "wesmckinn",
        "indices" : [ 0, 10 ],
        "id_str" : "115494880",
        "id" : 115494880
      }, {
        "name" : "Tim Hopper \uD83D\uDCDF",
        "screen_name" : "tdhopper",
        "indices" : [ 11, 20 ],
        "id_str" : "89249164",
        "id" : 89249164
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "941736551041478656",
    "geo" : { },
    "id_str" : "941753245902315520",
    "in_reply_to_user_id" : 115494880,
    "text" : "@wesmckinn @tdhopper Once, someone told me they don't need data scientists to design experiments because Excel has a chi-square function.",
    "id" : 941753245902315520,
    "in_reply_to_status_id" : 941736551041478656,
    "created_at" : "2017-12-15 19:34:05 +0000",
    "in_reply_to_screen_name" : "wesmckinn",
    "in_reply_to_user_id_str" : "115494880",
    "user" : {
      "name" : "Tommy Levi",
      "screen_name" : "tslevi",
      "protected" : false,
      "id_str" : "81710054",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2537513674\/image_normal.jpg",
      "id" : 81710054,
      "verified" : false
    }
  },
  "id" : 942170062781329410,
  "created_at" : "2017-12-16 23:10:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "Recode",
      "screen_name" : "Recode",
      "indices" : [ 95, 102 ],
      "id_str" : "2244340904",
      "id" : 2244340904
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/KQVCPV6gsj",
      "expanded_url" : "https:\/\/www.recode.net\/2017\/12\/13\/16773062\/global-internet-speeds-faster-2017-ookla?utm_campaign=www.recode.net&utm_content=entry&utm_medium=social&utm_source=twitter",
      "display_url" : "recode.net\/2017\/12\/13\/167\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942164236075175941",
  "text" : "RT @edwinksl: Global internet speeds got 30 percent faster in 2017 https:\/\/t.co\/KQVCPV6gsj via @Recode",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Recode",
        "screen_name" : "Recode",
        "indices" : [ 81, 88 ],
        "id_str" : "2244340904",
        "id" : 2244340904
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/KQVCPV6gsj",
        "expanded_url" : "https:\/\/www.recode.net\/2017\/12\/13\/16773062\/global-internet-speeds-faster-2017-ookla?utm_campaign=www.recode.net&utm_content=entry&utm_medium=social&utm_source=twitter",
        "display_url" : "recode.net\/2017\/12\/13\/167\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "942152933734567943",
    "text" : "Global internet speeds got 30 percent faster in 2017 https:\/\/t.co\/KQVCPV6gsj via @Recode",
    "id" : 942152933734567943,
    "created_at" : "2017-12-16 22:02:18 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 942164236075175941,
  "created_at" : "2017-12-16 22:47:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9",
      "screen_name" : "rte",
      "indices" : [ 108, 112 ],
      "id_str" : "1245699895",
      "id" : 1245699895
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/LVej8wniJt",
      "expanded_url" : "https:\/\/www.rte.ie\/sport\/gaa\/2017\/1211\/926648-its-important-to-look-past-the-gaa\/",
      "display_url" : "rte.ie\/sport\/gaa\/2017\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942139997582512129",
  "text" : "Would it be better to invite the homeless to stay with them for just one night? https:\/\/t.co\/LVej8wniJt via @rte",
  "id" : 942139997582512129,
  "created_at" : "2017-12-16 21:10:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/CJEmLChN0b",
      "expanded_url" : "https:\/\/twitter.com\/chris_byrne\/status\/941661543606964224",
      "display_url" : "twitter.com\/chris_byrne\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942135688006389760",
  "text" : "They are the NRA of Ireland. https:\/\/t.co\/CJEmLChN0b",
  "id" : 942135688006389760,
  "created_at" : "2017-12-16 20:53:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "indices" : [ 3, 12 ],
      "id_str" : "875971",
      "id" : 875971
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942123629093023749",
  "text" : "RT @Rubenerd: Let me tell you about streams of disjointed tweets to get around character limits that really should just be blog posts (1\/90\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "940496056843767809",
    "text" : "Let me tell you about streams of disjointed tweets to get around character limits that really should just be blog posts (1\/900)!",
    "id" : 940496056843767809,
    "created_at" : "2017-12-12 08:18:28 +0000",
    "user" : {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "protected" : false,
      "id_str" : "875971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011136325364301827\/hmrtp1V1_normal.jpg",
      "id" : 875971,
      "verified" : false
    }
  },
  "id" : 942123629093023749,
  "created_at" : "2017-12-16 20:05:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/u0yux25OTx",
      "expanded_url" : "https:\/\/twitter.com\/ChelseaParlett\/status\/942080831874412544",
      "display_url" : "twitter.com\/ChelseaParlett\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942121011218141184",
  "text" : "There is always an Irish connection. https:\/\/t.co\/u0yux25OTx",
  "id" : 942121011218141184,
  "created_at" : "2017-12-16 19:55:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/67CniiW0s7",
      "expanded_url" : "https:\/\/medium.com\/@mryap\/the-best-algorithm-always-wins-ac1e5b5ebfd7",
      "display_url" : "medium.com\/@mryap\/the-bes\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "942094016375554048",
  "text" : "I just published \u201CThe best algorithm always wins?\u201D https:\/\/t.co\/67CniiW0s7",
  "id" : 942094016375554048,
  "created_at" : "2017-12-16 18:08:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Women+ in ML\/DS",
      "screen_name" : "wimlds",
      "indices" : [ 3, 10 ],
      "id_str" : "2999992556",
      "id" : 2999992556
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942022016823185410",
  "text" : "RT @wimlds: Bloomberg has published the first article on the allegations of sexual misconduct in the Bayesian Stats\/ML community. https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 143, 150 ],
        "id_str" : "20536157",
        "id" : 20536157
      }, {
        "name" : "University of Minnesota",
        "screen_name" : "UMNews",
        "indices" : [ 273, 280 ],
        "id_str" : "18917572",
        "id" : 18917572
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/oYlVhEComb",
        "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2017-12-16\/google-researcher-accused-of-sexual-harassment-roiling-ai-field",
        "display_url" : "bloomberg.com\/news\/articles\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941869437010841600",
    "text" : "Bloomberg has published the first article on the allegations of sexual misconduct in the Bayesian Stats\/ML community. https:\/\/t.co\/oYlVhEComb\n\n@Google researcher, Steve Scott, has been put on leave &amp; Univ of Minnesota faculty, Brad Carlin, is also under investigation. @UMNews",
    "id" : 941869437010841600,
    "created_at" : "2017-12-16 03:15:47 +0000",
    "user" : {
      "name" : "Women+ in ML\/DS",
      "screen_name" : "wimlds",
      "protected" : false,
      "id_str" : "2999992556",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877345413115764736\/MTFNgtp3_normal.jpg",
      "id" : 2999992556,
      "verified" : false
    }
  },
  "id" : 942022016823185410,
  "created_at" : "2017-12-16 13:22:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stefanie Preissner",
      "screen_name" : "StefPreissner",
      "indices" : [ 3, 17 ],
      "id_str" : "87519932",
      "id" : 87519932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "942007094638710784",
  "text" : "RT @StefPreissner: \u201CLet\u2019s meet for coffee soon\u201D\n\u201CThat would be great!\u201D \n\nAnd they never saw each other again",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941964442593583104",
    "text" : "\u201CLet\u2019s meet for coffee soon\u201D\n\u201CThat would be great!\u201D \n\nAnd they never saw each other again",
    "id" : 941964442593583104,
    "created_at" : "2017-12-16 09:33:18 +0000",
    "user" : {
      "name" : "Stefanie Preissner",
      "screen_name" : "StefPreissner",
      "protected" : false,
      "id_str" : "87519932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1018217117014003713\/bC_KO594_normal.jpg",
      "id" : 87519932,
      "verified" : true
    }
  },
  "id" : 942007094638710784,
  "created_at" : "2017-12-16 12:22:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DrTempest van Schaik",
      "screen_name" : "Dr_Tempest",
      "indices" : [ 3, 14 ],
      "id_str" : "2370233868",
      "id" : 2370233868
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941974884661583872",
  "text" : "RT @Dr_Tempest: FT Person of the Year: Susan Fowler, software engineer who lifted the lid on sexual harassment at Uber and inspired women t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/O15GN3W7Ve",
        "expanded_url" : "https:\/\/www.ft.com\/content\/b4bc2a68-dc4f-11e7-a039-c64b1c09b482",
        "display_url" : "ft.com\/content\/b4bc2a\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941973875616821248",
    "text" : "FT Person of the Year: Susan Fowler, software engineer who lifted the lid on sexual harassment at Uber and inspired women to speak out  https:\/\/t.co\/O15GN3W7Ve",
    "id" : 941973875616821248,
    "created_at" : "2017-12-16 10:10:47 +0000",
    "user" : {
      "name" : "DrTempest van Schaik",
      "screen_name" : "Dr_Tempest",
      "protected" : false,
      "id_str" : "2370233868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037376811141922816\/vC3N7BcB_normal.jpg",
      "id" : 2370233868,
      "verified" : false
    }
  },
  "id" : 941974884661583872,
  "created_at" : "2017-12-16 10:14:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/941964268357996544\/photo\/1",
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/WEf9tbzYGC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRKHgm2XcAAbUZX.jpg",
      "id_str" : "941964266327994368",
      "id" : 941964266327994368,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRKHgm2XcAAbUZX.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WEf9tbzYGC"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/kSQqudGieG",
      "expanded_url" : "http:\/\/ift.tt\/2omURYn",
      "display_url" : "ift.tt\/2omURYn"
    } ]
  },
  "geo" : { },
  "id_str" : "941964268357996544",
  "text" : "Good Morning. https:\/\/t.co\/kSQqudGieG https:\/\/t.co\/WEf9tbzYGC",
  "id" : 941964268357996544,
  "created_at" : "2017-12-16 09:32:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Lancet",
      "screen_name" : "TheLancet",
      "indices" : [ 3, 13 ],
      "id_str" : "27013292",
      "id" : 27013292
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941941224524656640",
  "text" : "RT @TheLancet: On this week's cover: 1 in 3 dementia cases could be prevented by acting on risk factors throughout life\u2014full  Commission on\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hubspot.com\/\" rel=\"nofollow\"\u003EHubSpot\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheLancet\/status\/941616665044897792\/photo\/1",
        "indices" : [ 172, 195 ],
        "url" : "https:\/\/t.co\/FF7CtHIjOD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRFLXXuVwAAL33Q.jpg",
        "id_str" : "941616661974597632",
        "id" : 941616661974597632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRFLXXuVwAAL33Q.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 514
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1547
        }, {
          "h" : 2315,
          "resize" : "fit",
          "w" : 1749
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 907
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FF7CtHIjOD"
      } ],
      "hashtags" : [ {
        "text" : "dementia",
        "indices" : [ 125, 134 ]
      } ],
      "urls" : [ {
        "indices" : [ 148, 171 ],
        "url" : "https:\/\/t.co\/fjGcdAVpu8",
        "expanded_url" : "https:\/\/hubs.ly\/H09sMfj0",
        "display_url" : "hubs.ly\/H09sMfj0"
      } ]
    },
    "geo" : { },
    "id_str" : "941616665044897792",
    "text" : "On this week's cover: 1 in 3 dementia cases could be prevented by acting on risk factors throughout life\u2014full  Commission on #dementia free to read https:\/\/t.co\/fjGcdAVpu8 https:\/\/t.co\/FF7CtHIjOD",
    "id" : 941616665044897792,
    "created_at" : "2017-12-15 10:31:21 +0000",
    "user" : {
      "name" : "The Lancet",
      "screen_name" : "TheLancet",
      "protected" : false,
      "id_str" : "27013292",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000725668351\/16bf102fc319d621a456d9f821071253_normal.jpeg",
      "id" : 27013292,
      "verified" : true
    }
  },
  "id" : 941941224524656640,
  "created_at" : "2017-12-16 08:01:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Defra Digital",
      "screen_name" : "DefraDigital",
      "indices" : [ 3, 16 ],
      "id_str" : "757843124102176768",
      "id" : 757843124102176768
    }, {
      "name" : "Colin Banno-Thornton",
      "screen_name" : "colinbanno",
      "indices" : [ 79, 90 ],
      "id_str" : "393939528",
      "id" : 393939528
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/QEPZtcI9k5",
      "expanded_url" : "https:\/\/defradigital.blog.gov.uk\/2017\/12\/15\/platforms-can-be-products-too\/",
      "display_url" : "defradigital.blog.gov.uk\/2017\/12\/15\/pla\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941936487301558272",
  "text" : "RT @DefraDigital: \"platforms can be products too\" read our latest blog post by @colinbanno https:\/\/t.co\/QEPZtcI9k5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Colin Banno-Thornton",
        "screen_name" : "colinbanno",
        "indices" : [ 61, 72 ],
        "id_str" : "393939528",
        "id" : 393939528
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/QEPZtcI9k5",
        "expanded_url" : "https:\/\/defradigital.blog.gov.uk\/2017\/12\/15\/platforms-can-be-products-too\/",
        "display_url" : "defradigital.blog.gov.uk\/2017\/12\/15\/pla\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941611351029506049",
    "text" : "\"platforms can be products too\" read our latest blog post by @colinbanno https:\/\/t.co\/QEPZtcI9k5",
    "id" : 941611351029506049,
    "created_at" : "2017-12-15 10:10:15 +0000",
    "user" : {
      "name" : "Defra Digital",
      "screen_name" : "DefraDigital",
      "protected" : false,
      "id_str" : "757843124102176768",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/767692635825995776\/uo18moQM_normal.jpg",
      "id" : 757843124102176768,
      "verified" : false
    }
  },
  "id" : 941936487301558272,
  "created_at" : "2017-12-16 07:42:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941781853303922688",
  "text" : "The Impossible on Channel 4. Difficulty to watch...",
  "id" : 941781853303922688,
  "created_at" : "2017-12-15 21:27:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "941717921507807232",
  "geo" : { },
  "id_str" : "941718195735613445",
  "in_reply_to_user_id" : 9465632,
  "text" : "How hard can it be? Learn about the product and interact with end user on Twitter.",
  "id" : 941718195735613445,
  "in_reply_to_status_id" : 941717921507807232,
  "created_at" : "2017-12-15 17:14:48 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "940187436164026368",
  "geo" : { },
  "id_str" : "941717921507807232",
  "in_reply_to_user_id" : 9465632,
  "text" : "They get back to me. I did not even get an interview.  This position is bit different from a data science job.  It tech support using Social media.",
  "id" : 941717921507807232,
  "in_reply_to_status_id" : 940187436164026368,
  "created_at" : "2017-12-15 17:13:43 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "lazyweb",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941716725195735041",
  "text" : "#lazyweb Is it advisable as part of the job application to highlight that I know about their inner working process and tools from source x, y z?",
  "id" : 941716725195735041,
  "created_at" : "2017-12-15 17:08:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941701859181461504",
  "text" : "The TD who cries as she tells story of young homeless man. Did she offer the young man a temporary place?",
  "id" : 941701859181461504,
  "created_at" : "2017-12-15 16:09:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/nbpIraMViv",
      "expanded_url" : "https:\/\/twitter.com\/interactivemark\/status\/941690430705557505",
      "display_url" : "twitter.com\/interactivemar\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941701252999778306",
  "text" : "\"Mostly, when we see these things, we grumble on the inside, and then do nothing. But not Richard Ankrom.\" https:\/\/t.co\/nbpIraMViv",
  "id" : 941701252999778306,
  "created_at" : "2017-12-15 16:07:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Walsh's of Dunmanway",
      "screen_name" : "walshdun",
      "indices" : [ 52, 61 ],
      "id_str" : "3020019059",
      "id" : 3020019059
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941684656017149954",
  "text" : "RT @clonlad: I'm two years without alcohol in store @walshdun  blessing in disguise .. concentrating on other more profitable categories &amp;\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Walsh's of Dunmanway",
        "screen_name" : "walshdun",
        "indices" : [ 39, 48 ],
        "id_str" : "3020019059",
        "id" : 3020019059
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "alcoholbill",
        "indices" : [ 170, 182 ]
      } ],
      "urls" : [ {
        "indices" : [ 183, 206 ],
        "url" : "https:\/\/t.co\/VRASv7YuNC",
        "expanded_url" : "https:\/\/twitter.com\/irelandunlocked\/status\/941669734248001536",
        "display_url" : "twitter.com\/irelandunlocke\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941673950286884865",
    "text" : "I'm two years without alcohol in store @walshdun  blessing in disguise .. concentrating on other more profitable categories &amp; fewer security issues =better margins \uD83D\uDC4D #alcoholbill https:\/\/t.co\/VRASv7YuNC",
    "id" : 941673950286884865,
    "created_at" : "2017-12-15 14:18:59 +0000",
    "user" : {
      "name" : "peter walsh\uD83C\uDF66\uD83C\uDF66\uD83C\uDF66\uD83C\uDF66\uD83C\uDF66",
      "screen_name" : "clongent",
      "protected" : false,
      "id_str" : "86962890",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/984521035566510080\/wteUeT4U_normal.jpg",
      "id" : 86962890,
      "verified" : false
    }
  },
  "id" : 941684656017149954,
  "created_at" : "2017-12-15 15:01:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 132, 155 ],
      "url" : "https:\/\/t.co\/cWETRWioDl",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/BBGLHeh?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/BBGLH\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941671972727083009",
  "text" : "Healthcare professional like nurses don't even have a \u20AC500 dunnes vouchers and they are expect to report to work comes rain or snow https:\/\/t.co\/cWETRWioDl",
  "id" : 941671972727083009,
  "created_at" : "2017-12-15 14:11:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/941668292850274304\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/aGXGUCGiVW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRF6RyzX4AAkd1U.jpg",
      "id_str" : "941668243210756096",
      "id" : 941668243210756096,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRF6RyzX4AAkd1U.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 686,
        "resize" : "fit",
        "w" : 1086
      }, {
        "h" : 686,
        "resize" : "fit",
        "w" : 1086
      }, {
        "h" : 686,
        "resize" : "fit",
        "w" : 1086
      }, {
        "h" : 430,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/aGXGUCGiVW"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/LIZzgGNL5C",
      "expanded_url" : "https:\/\/isitonaws.com",
      "display_url" : "isitonaws.com"
    } ]
  },
  "geo" : { },
  "id_str" : "941668292850274304",
  "text" : "Eating your own dog food. https:\/\/t.co\/LIZzgGNL5C https:\/\/t.co\/aGXGUCGiVW",
  "id" : 941668292850274304,
  "created_at" : "2017-12-15 13:56:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WhyIreland",
      "indices" : [ 148, 159 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941657554970693632",
  "text" : "I got to snail mail you the referral letter, your specialist got to review before an appointment date can be set. Mind you, this is a paid service. #WhyIreland?",
  "id" : 941657554970693632,
  "created_at" : "2017-12-15 13:13:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HireYap",
      "indices" : [ 187, 195 ]
    } ],
    "urls" : [ {
      "indices" : [ 163, 186 ],
      "url" : "https:\/\/t.co\/yuQf7cAMaH",
      "expanded_url" : "https:\/\/medium.com\/@mryap\/latest",
      "display_url" : "medium.com\/@mryap\/latest"
    } ]
  },
  "geo" : { },
  "id_str" : "941605085154562048",
  "text" : "I just abandoned WordPress for Medium so I do not have to spent time tinkering the tool (btw it fun) and that should force me to focus on writing better content.  https:\/\/t.co\/yuQf7cAMaH #HireYap",
  "id" : 941605085154562048,
  "created_at" : "2017-12-15 09:45:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941582573058297856",
  "text" : "devhints.io is a handy collection of cheatsheets for web developers. You are welcome.",
  "id" : 941582573058297856,
  "created_at" : "2017-12-15 08:15:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/280.talltweets.com\/\" rel=\"nofollow\"\u003ETall Tweets\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 132, 155 ],
      "url" : "https:\/\/t.co\/YwR0pZavvY",
      "expanded_url" : "http:\/\/www.tandfonline.com\/doi\/abs\/10.1080\/10510974.2012.727941#.UlVEzkJDtqA",
      "display_url" : "tandfonline.com\/doi\/abs\/10.108\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941577128172068864",
  "text" : "1\/ According to research, pointing out that the person isn\u2019t obligated to do as you ask seems equally effective in some situations. https:\/\/t.co\/YwR0pZavvY Reminding people they have the freedom to choose makes them much more likely to be persuaded.",
  "id" : 941577128172068864,
  "created_at" : "2017-12-15 07:54:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mackenzie Astin",
      "screen_name" : "MackenzieAstin",
      "indices" : [ 3, 18 ],
      "id_str" : "30017786",
      "id" : 30017786
    }, {
      "name" : "Ajit Pai",
      "screen_name" : "AjitPaiFCC",
      "indices" : [ 25, 36 ],
      "id_str" : "575658149",
      "id" : 575658149
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941574520602791936",
  "text" : "RT @MackenzieAstin: Hey, @AjitPaiFCC, today my mom would have turned 71. But she didn't. Because she died in March of 2016. Can you please\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ajit Pai",
        "screen_name" : "AjitPaiFCC",
        "indices" : [ 5, 16 ],
        "id_str" : "575658149",
        "id" : 575658149
      }, {
        "name" : "Sean Astin",
        "screen_name" : "SeanAstin",
        "indices" : [ 261, 271 ],
        "id_str" : "286871259",
        "id" : 286871259
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MackenzieAstin\/status\/941459382864437248\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/VtdLaB0eGp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRC7FYqWsAEknDx.jpg",
        "id_str" : "941458023314272257",
        "id" : 941458023314272257,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRC7FYqWsAEknDx.jpg",
        "sizes" : [ {
          "h" : 518,
          "resize" : "fit",
          "w" : 812
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 434,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 518,
          "resize" : "fit",
          "w" : 812
        }, {
          "h" : 518,
          "resize" : "fit",
          "w" : 812
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VtdLaB0eGp"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MackenzieAstin\/status\/941459382864437248\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/VtdLaB0eGp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRC7PihXcAArS7P.jpg",
        "id_str" : "941458197759619072",
        "id" : 941458197759619072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRC7PihXcAArS7P.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 516,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 516,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 516,
          "resize" : "fit",
          "w" : 780
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VtdLaB0eGp"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MackenzieAstin\/status\/941459382864437248\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/VtdLaB0eGp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRC8AmMX0AIWhEp.jpg",
        "id_str" : "941459040558895106",
        "id" : 941459040558895106,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRC8AmMX0AIWhEp.jpg",
        "sizes" : [ {
          "h" : 524,
          "resize" : "fit",
          "w" : 789
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 524,
          "resize" : "fit",
          "w" : 789
        }, {
          "h" : 452,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 524,
          "resize" : "fit",
          "w" : 789
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VtdLaB0eGp"
      } ],
      "hashtags" : [ {
        "text" : "NetNeutrality",
        "indices" : [ 208, 222 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941459382864437248",
    "text" : "Hey, @AjitPaiFCC, today my mom would have turned 71. But she didn't. Because she died in March of 2016. Can you please take the time to explain to me how she made three separate comments in support of ending #NetNeutrality more than a year after she died?\n\ncc: @SeanAstin https:\/\/t.co\/VtdLaB0eGp",
    "id" : 941459382864437248,
    "created_at" : "2017-12-15 00:06:23 +0000",
    "user" : {
      "name" : "Mackenzie Astin",
      "screen_name" : "MackenzieAstin",
      "protected" : false,
      "id_str" : "30017786",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/632465686321741824\/RYza3oEA_normal.jpg",
      "id" : 30017786,
      "verified" : true
    }
  },
  "id" : 941574520602791936,
  "created_at" : "2017-12-15 07:43:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Krassenstein",
      "screen_name" : "EdKrassen",
      "indices" : [ 3, 13 ],
      "id_str" : "132339474",
      "id" : 132339474
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NetNeutrality",
      "indices" : [ 89, 103 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941569890665795584",
  "text" : "RT @EdKrassen: BREAKING: The following states are suing Trump's FCC in order to preserve #NetNeutrality \n\uD83D\uDCBBCalifornia\n\uD83D\uDCBBDelaware\n\uD83D\uDCBBHawaii\n\uD83D\uDCBBIll\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "NetNeutrality",
        "indices" : [ 74, 88 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941395790605508609",
    "text" : "BREAKING: The following states are suing Trump's FCC in order to preserve #NetNeutrality \n\uD83D\uDCBBCalifornia\n\uD83D\uDCBBDelaware\n\uD83D\uDCBBHawaii\n\uD83D\uDCBBIllinois\n\uD83D\uDCBBIowa\n\uD83D\uDCBBKentucky\n\uD83D\uDCBBMaine\n\uD83D\uDCBBMaryland\n\uD83D\uDCBBMassachusetts\n\uD83D\uDCBBMississippi\n\uD83D\uDCBBNY\n\uD83D\uDCBBNorth Carolina\n\uD83D\uDCBBOregon\n\uD83D\uDCBBPennsylvania\n\uD83D\uDCBBVermont\n\uD83D\uDCBBVirginia\n\uD83D\uDCBBWashington",
    "id" : 941395790605508609,
    "created_at" : "2017-12-14 19:53:41 +0000",
    "user" : {
      "name" : "Ed Krassenstein",
      "screen_name" : "EdKrassen",
      "protected" : false,
      "id_str" : "132339474",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032612349633486848\/t35esAW6_normal.jpg",
      "id" : 132339474,
      "verified" : false
    }
  },
  "id" : 941569890665795584,
  "created_at" : "2017-12-15 07:25:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Lorica \u7F57\u745E\u5361",
      "screen_name" : "bigdata",
      "indices" : [ 3, 11 ],
      "id_str" : "18318677",
      "id" : 18318677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/n352mpfYw3",
      "expanded_url" : "http:\/\/goo.gl\/KpL18b",
      "display_url" : "goo.gl\/KpL18b"
    } ]
  },
  "geo" : { },
  "id_str" : "941390180711325696",
  "text" : "RT @bigdata: new post: An overview of commercial and industrial applications of reinforcement learning https:\/\/t.co\/n352mpfYw3 https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bigdata\/status\/941385471468306432\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/Rrwd0gLXSG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DRB5D-dVAAAQltr.jpg",
        "id_str" : "941385431333011456",
        "id" : 941385431333011456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRB5D-dVAAAQltr.jpg",
        "sizes" : [ {
          "h" : 1022,
          "resize" : "fit",
          "w" : 1056
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1022,
          "resize" : "fit",
          "w" : 1056
        }, {
          "h" : 1022,
          "resize" : "fit",
          "w" : 1056
        }, {
          "h" : 658,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Rrwd0gLXSG"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/n352mpfYw3",
        "expanded_url" : "http:\/\/goo.gl\/KpL18b",
        "display_url" : "goo.gl\/KpL18b"
      } ]
    },
    "geo" : { },
    "id_str" : "941385471468306432",
    "text" : "new post: An overview of commercial and industrial applications of reinforcement learning https:\/\/t.co\/n352mpfYw3 https:\/\/t.co\/Rrwd0gLXSG",
    "id" : 941385471468306432,
    "created_at" : "2017-12-14 19:12:41 +0000",
    "user" : {
      "name" : "Ben Lorica \u7F57\u745E\u5361",
      "screen_name" : "bigdata",
      "protected" : false,
      "id_str" : "18318677",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/235298259\/bigdata_logo_center3_normal.jpg",
      "id" : 18318677,
      "verified" : false
    }
  },
  "id" : 941390180711325696,
  "created_at" : "2017-12-14 19:31:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/941386844649984000\/photo\/1",
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/DNtfLesEBf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DRB6WHSXUAMAqpR.jpg",
      "id_str" : "941386842452217859",
      "id" : 941386842452217859,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DRB6WHSXUAMAqpR.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DNtfLesEBf"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/UmEoIUoHHO",
      "expanded_url" : "http:\/\/ift.tt\/2AKuzVB",
      "display_url" : "ift.tt\/2AKuzVB"
    } ]
  },
  "geo" : { },
  "id_str" : "941386844649984000",
  "text" : "Xmas tree for sale. https:\/\/t.co\/UmEoIUoHHO https:\/\/t.co\/DNtfLesEBf",
  "id" : 941386844649984000,
  "created_at" : "2017-12-14 19:18:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/CuzcGh3meW",
      "expanded_url" : "https:\/\/www.independent.ie\/entertainment\/banter\/trending\/legal-love-actually-irish-judge-excuses-man-54-from-jury-duty-after-he-reveals-hes-in-love-for-the-first-time-36408126.html",
      "display_url" : "independent.ie\/entertainment\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941291075112710144",
  "text" : "How can you focus on jury duty when your hearts and minds are somewhere else? https:\/\/t.co\/CuzcGh3meW",
  "id" : 941291075112710144,
  "created_at" : "2017-12-14 12:57:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ethan",
      "screen_name" : "ethanyeh",
      "indices" : [ 3, 12 ],
      "id_str" : "209721412",
      "id" : 209721412
    }, {
      "name" : "Stripe",
      "screen_name" : "stripe",
      "indices" : [ 50, 57 ],
      "id_str" : "102812444",
      "id" : 102812444
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941280270807977985",
  "text" : "RT @ethanyeh: Excited about this work by our team @stripe. Fraudsters are more likely to shop repeatedly at the same business and to make c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Stripe",
        "screen_name" : "stripe",
        "indices" : [ 36, 43 ],
        "id_str" : "102812444",
        "id" : 102812444
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ethanyeh\/status\/941015229122592768\/photo\/1",
        "indices" : [ 210, 233 ],
        "url" : "https:\/\/t.co\/xWCNLtjsbm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQ8oXGXUQAAlPuQ.jpg",
        "id_str" : "941015224454234112",
        "id" : 941015224454234112,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQ8oXGXUQAAlPuQ.jpg",
        "sizes" : [ {
          "h" : 271,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 631,
          "resize" : "fit",
          "w" : 1586
        }, {
          "h" : 477,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 631,
          "resize" : "fit",
          "w" : 1586
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xWCNLtjsbm"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 186, 209 ],
        "url" : "https:\/\/t.co\/GnXNBHM10G",
        "expanded_url" : "https:\/\/stripe.com\/files\/blog\/stripe-snapshot-fraud.pdf",
        "display_url" : "stripe.com\/files\/blog\/str\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "941015229122592768",
    "text" : "Excited about this work by our team @stripe. Fraudsters are more likely to shop repeatedly at the same business and to make consecutive purchases 10x more quickly than the avg consumer. https:\/\/t.co\/GnXNBHM10G https:\/\/t.co\/xWCNLtjsbm",
    "id" : 941015229122592768,
    "created_at" : "2017-12-13 18:41:28 +0000",
    "user" : {
      "name" : "Ethan",
      "screen_name" : "ethanyeh",
      "protected" : false,
      "id_str" : "209721412",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/474399063195287552\/nl-qiZY1_normal.jpeg",
      "id" : 209721412,
      "verified" : false
    }
  },
  "id" : 941280270807977985,
  "created_at" : "2017-12-14 12:14:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/Wg2FAtxjo3",
      "expanded_url" : "https:\/\/twitter.com\/quick_minutes\/status\/941225012106792960",
      "display_url" : "twitter.com\/quick_minutes\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941280095821541376",
  "text" : "A cold shower in Winter might help. https:\/\/t.co\/Wg2FAtxjo3",
  "id" : 941280095821541376,
  "created_at" : "2017-12-14 12:13:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BackByYAP",
      "indices" : [ 141, 151 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941276621977382913",
  "text" : "Does your bank fail to share your vision? I can back you up with my skills sets and connection. Pay me back only when the new biz take off.  #BackByYAP",
  "id" : 941276621977382913,
  "created_at" : "2017-12-14 12:00:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Orla Tinsley",
      "screen_name" : "orlatinsley",
      "indices" : [ 3, 15 ],
      "id_str" : "197168427",
      "id" : 197168427
    }, {
      "name" : "Irish Kidney Association",
      "screen_name" : "IrishKidneyAs",
      "indices" : [ 106, 120 ],
      "id_str" : "531280087",
      "id" : 531280087
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941276077934153728",
  "text" : "RT @orlatinsley: Is there an app developer out there who could generously give time and speed this up for @IrishKidneyAs so everyone can re\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Kidney Association",
        "screen_name" : "IrishKidneyAs",
        "indices" : [ 89, 103 ],
        "id_str" : "531280087",
        "id" : 531280087
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LiveLifethenGiveLife",
        "indices" : [ 158, 179 ]
      } ],
      "urls" : [ {
        "indices" : [ 180, 203 ],
        "url" : "https:\/\/t.co\/Ux2uisP6MT",
        "expanded_url" : "https:\/\/twitter.com\/IrishKidneyAs\/status\/939405076510183424",
        "display_url" : "twitter.com\/IrishKidneyAs\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "939441222514544640",
    "text" : "Is there an app developer out there who could generously give time and speed this up for @IrishKidneyAs so everyone can register to donate their organs asap? #LiveLifethenGiveLife https:\/\/t.co\/Ux2uisP6MT",
    "id" : 939441222514544640,
    "created_at" : "2017-12-09 10:26:56 +0000",
    "user" : {
      "name" : "Orla Tinsley",
      "screen_name" : "orlatinsley",
      "protected" : false,
      "id_str" : "197168427",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/968884149632331776\/Glg52G7J_normal.jpg",
      "id" : 197168427,
      "verified" : false
    }
  },
  "id" : 941276077934153728,
  "created_at" : "2017-12-14 11:57:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/BCqkET6Hut",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/BBGD8Ti?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/BBGD8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941272038500528129",
  "text" : "It used to be Skype but now is WhatsApp. Migration in the Age of WhatsApp https:\/\/t.co\/BCqkET6Hut",
  "id" : 941272038500528129,
  "created_at" : "2017-12-14 11:41:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/HwPfVoion7",
      "expanded_url" : "http:\/\/a.msn.com\/09\/en-ie\/BBGHNF1?ocid=st",
      "display_url" : "a.msn.com\/09\/en-ie\/BBGHN\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941270955392892928",
  "text" : "The Russian 'Air Force One' https:\/\/t.co\/HwPfVoion7",
  "id" : 941270955392892928,
  "created_at" : "2017-12-14 11:37:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tour America",
      "screen_name" : "TourAmericaTV",
      "indices" : [ 3, 17 ],
      "id_str" : "41828103",
      "id" : 41828103
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TourAmerica",
      "indices" : [ 40, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941085437011152896",
  "text" : "RT @TourAmericaTV: RT, Follow and tweet #TourAmerica for your chance to win a \u20AC50 One 4 All Voucher! \uD83C\uDF85\uD83C\uDF84Winner announced Friday, good luck a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TourAmericaTV\/status\/940197370154946560\/photo\/1",
        "indices" : [ 126, 149 ],
        "url" : "https:\/\/t.co\/GKwClucHrd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQxAXsyV4AAo3Go.jpg",
        "id_str" : "940197198117068800",
        "id" : 940197198117068800,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQxAXsyV4AAo3Go.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GKwClucHrd"
      } ],
      "hashtags" : [ {
        "text" : "TourAmerica",
        "indices" : [ 21, 33 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "940197370154946560",
    "text" : "RT, Follow and tweet #TourAmerica for your chance to win a \u20AC50 One 4 All Voucher! \uD83C\uDF85\uD83C\uDF84Winner announced Friday, good luck all! \uD83C\uDF89 https:\/\/t.co\/GKwClucHrd",
    "id" : 940197370154946560,
    "created_at" : "2017-12-11 12:31:35 +0000",
    "user" : {
      "name" : "Tour America",
      "screen_name" : "TourAmericaTV",
      "protected" : false,
      "id_str" : "41828103",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914775839601983488\/gVOUs4yK_normal.jpg",
      "id" : 41828103,
      "verified" : false
    }
  },
  "id" : 941085437011152896,
  "created_at" : "2017-12-13 23:20:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Researc\/hers Code",
      "screen_name" : "ResearcHersCode",
      "indices" : [ 3, 19 ],
      "id_str" : "900253694255616001",
      "id" : 900253694255616001
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "941083937228517376",
  "text" : "RT @ResearcHersCode: More than 50% of researchers cannot repeat their results and more than 70% of results cannot be reproduced by another\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ResearcHersCode\/status\/941052272917798912\/photo\/1",
        "indices" : [ 169, 192 ],
        "url" : "https:\/\/t.co\/GFfNsqohBU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQ9J-qJWsAUkSYn.jpg",
        "id_str" : "941052187957964805",
        "id" : 941052187957964805,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQ9J-qJWsAUkSYn.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GFfNsqohBU"
      } ],
      "hashtags" : [ {
        "text" : "research",
        "indices" : [ 129, 138 ]
      }, {
        "text" : "openscience",
        "indices" : [ 139, 151 ]
      }, {
        "text" : "reproducibility",
        "indices" : [ 152, 168 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "941052272917798912",
    "text" : "More than 50% of researchers cannot repeat their results and more than 70% of results cannot be reproduced by another researcher #research #openscience #reproducibility https:\/\/t.co\/GFfNsqohBU",
    "id" : 941052272917798912,
    "created_at" : "2017-12-13 21:08:40 +0000",
    "user" : {
      "name" : "Researc\/hers Code",
      "screen_name" : "ResearcHersCode",
      "protected" : false,
      "id_str" : "900253694255616001",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/900254625365995520\/y5XzcwZ7_normal.jpg",
      "id" : 900253694255616001,
      "verified" : false
    }
  },
  "id" : 941083937228517376,
  "created_at" : "2017-12-13 23:14:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/lzRMsWqBc0",
      "expanded_url" : "https:\/\/twitter.com\/pachamaltese\/status\/941003271724060673",
      "display_url" : "twitter.com\/pachamaltese\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "941006086630494209",
  "text" : "\uD83D\uDE2C https:\/\/t.co\/lzRMsWqBc0",
  "id" : 941006086630494209,
  "created_at" : "2017-12-13 18:05:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Discover Ireland",
      "screen_name" : "DiscoverIreland",
      "indices" : [ 3, 19 ],
      "id_str" : "15255537",
      "id" : 15255537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940998719134162944",
  "text" : "RT @DiscoverIreland: Can you guess how many finely decorated rooms are in The Casino in Marino, County Dublin? We'll give you a clue, it is\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Emma McArdle",
        "screen_name" : "McArdlePhoto",
        "indices" : [ 139, 152 ],
        "id_str" : "4354408581",
        "id" : 4354408581
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DiscoverIreland\/status\/940910445757648896\/photo\/1",
        "indices" : [ 153, 176 ],
        "url" : "https:\/\/t.co\/8pjAeOrjq8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQ7JEAqX0AA4pdH.jpg",
        "id_str" : "940910442901393408",
        "id" : 940910442901393408,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQ7JEAqX0AA4pdH.jpg",
        "sizes" : [ {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8pjAeOrjq8"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "940910445757648896",
    "text" : "Can you guess how many finely decorated rooms are in The Casino in Marino, County Dublin? We'll give you a clue, it is more than 10...\n\nMT @McArdlePhoto https:\/\/t.co\/8pjAeOrjq8",
    "id" : 940910445757648896,
    "created_at" : "2017-12-13 11:45:06 +0000",
    "user" : {
      "name" : "Discover Ireland",
      "screen_name" : "DiscoverIreland",
      "protected" : false,
      "id_str" : "15255537",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/517248713006133248\/IqxiQUU__normal.jpeg",
      "id" : 15255537,
      "verified" : true
    }
  },
  "id" : 940998719134162944,
  "created_at" : "2017-12-13 17:35:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alberto Negron",
      "screen_name" : "Altons",
      "indices" : [ 3, 10 ],
      "id_str" : "23989269",
      "id" : 23989269
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940966152515280898",
  "text" : "RT @Altons: What\u2019s the best way of learning digital marketing theory and tools for someone with strong background in analytics? Mainly inte\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "digital",
        "indices" : [ 189, 197 ]
      }, {
        "text" : "marketing",
        "indices" : [ 198, 208 ]
      }, {
        "text" : "Analytics",
        "indices" : [ 209, 219 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "940956565770833921",
    "text" : "What\u2019s the best way of learning digital marketing theory and tools for someone with strong background in analytics? Mainly interested in  SEO, PPC, Google trend\/Analytics Facebook ads etc. #digital #marketing #Analytics",
    "id" : 940956565770833921,
    "created_at" : "2017-12-13 14:48:22 +0000",
    "user" : {
      "name" : "Alberto Negron",
      "screen_name" : "Altons",
      "protected" : false,
      "id_str" : "23989269",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/472026224416407553\/ekVld1IG_normal.jpeg",
      "id" : 23989269,
      "verified" : false
    }
  },
  "id" : 940966152515280898,
  "created_at" : "2017-12-13 15:26:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Think With Google",
      "screen_name" : "ThinkGoogleAPAC",
      "indices" : [ 179, 195 ],
      "id_str" : "737531905533345795",
      "id" : 737531905533345795
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 151, 174 ],
      "url" : "https:\/\/t.co\/MUa9yWPT6f",
      "expanded_url" : "https:\/\/goo.gl\/Cdb95Z",
      "display_url" : "goo.gl\/Cdb95Z"
    } ]
  },
  "geo" : { },
  "id_str" : "940965764219236359",
  "text" : "The SEA internet e-Conomy will hit $50B by end-2017, driven by accelerated growth and investment in travel, media, ride hailing and e-commerce sectors https:\/\/t.co\/MUa9yWPT6f via @ThinkGoogleAPAC",
  "id" : 940965764219236359,
  "created_at" : "2017-12-13 15:24:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "indices" : [ 0, 16 ],
      "id_str" : "326869253",
      "id" : 326869253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/XvPlBl0OwZ",
      "expanded_url" : "https:\/\/www.creditunion.ie\/personal-loans\/",
      "display_url" : "creditunion.ie\/personal-loans\/"
    } ]
  },
  "in_reply_to_status_id_str" : "940709428818522112",
  "geo" : { },
  "id_str" : "940905688330534912",
  "in_reply_to_user_id" : 326869253,
  "text" : "@Tweetinggoddess Check out the credit union. All the best. https:\/\/t.co\/XvPlBl0OwZ",
  "id" : 940905688330534912,
  "in_reply_to_status_id" : 940709428818522112,
  "created_at" : "2017-12-13 11:26:11 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Drift",
      "screen_name" : "Drift",
      "indices" : [ 131, 137 ],
      "id_str" : "2733318672",
      "id" : 2733318672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/tQVuqkKPLK",
      "expanded_url" : "https:\/\/blog.drift.com\/no-more-forms\/",
      "display_url" : "blog.drift.com\/no-more-forms\/"
    } ]
  },
  "geo" : { },
  "id_str" : "940853881571106816",
  "text" : "Content are just like Argos catalog? You don't ask people for email so they can get their hand on it.  https:\/\/t.co\/tQVuqkKPLK via @Drift",
  "id" : 940853881571106816,
  "created_at" : "2017-12-13 08:00:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Russell",
      "screen_name" : "jonrussell",
      "indices" : [ 3, 14 ],
      "id_str" : "6132422",
      "id" : 6132422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940850964147458052",
  "text" : "RT @jonrussell: Uber used to consider governments and taxi firms enemies, now it is openly partnering with them to develop its business in\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jonrussell\/status\/929604189952524288\/photo\/1",
        "indices" : [ 198, 221 ],
        "url" : "https:\/\/t.co\/WsxoXvTQSr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DOaeBebUQAAHy6J.jpg",
        "id_str" : "929604121283280896",
        "id" : 929604121283280896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DOaeBebUQAAHy6J.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 724,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 724,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 724,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 517
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WsxoXvTQSr"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 174, 197 ],
        "url" : "https:\/\/t.co\/fGnSnlX4lt",
        "expanded_url" : "https:\/\/techcrunch.com\/2017\/11\/11\/uber-takes-a-different-approach-to-asia\/",
        "display_url" : "techcrunch.com\/2017\/11\/11\/ube\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "929604189952524288",
    "text" : "Uber used to consider governments and taxi firms enemies, now it is openly partnering with them to develop its business in Southeast Asia and compete more squarely with Grab https:\/\/t.co\/fGnSnlX4lt https:\/\/t.co\/WsxoXvTQSr",
    "id" : 929604189952524288,
    "created_at" : "2017-11-12 06:58:04 +0000",
    "user" : {
      "name" : "Jon Russell",
      "screen_name" : "jonrussell",
      "protected" : false,
      "id_str" : "6132422",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/794545496576536576\/6b4UeCSt_normal.jpg",
      "id" : 6132422,
      "verified" : true
    }
  },
  "id" : 940850964147458052,
  "created_at" : "2017-12-13 07:48:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/28MO29Fxf9",
      "expanded_url" : "https:\/\/medium.freecodecamp.org\/why-docker-makes-sense-for-startups-e9be14a1f662",
      "display_url" : "medium.freecodecamp.org\/why-docker-mak\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940846721273384960",
  "text" : "\u201CWhy Docker makes sense for startups\u201D  https:\/\/t.co\/28MO29Fxf9",
  "id" : 940846721273384960,
  "created_at" : "2017-12-13 07:31:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/OjvmeoYtmV",
      "expanded_url" : "https:\/\/www.rd.com\/culture\/beijing-subway-audiobook-libraries\/#.WjDUwwwue2J.twitter",
      "display_url" : "rd.com\/culture\/beijin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940843902940123136",
  "text" : "This City Transformed Its Subway Cars Into Moving Libraries https:\/\/t.co\/OjvmeoYtmV",
  "id" : 940843902940123136,
  "created_at" : "2017-12-13 07:20:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HireYap",
      "indices" : [ 112, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940843418519068672",
  "text" : "Jack of all trade (except HR, Law, Accounting, Business Development) available for hire for fledgling business. #HireYap",
  "id" : 940843418519068672,
  "created_at" : "2017-12-13 07:18:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/HKQBDRMSfL",
      "expanded_url" : "https:\/\/nyti.ms\/2jZ4po8",
      "display_url" : "nyti.ms\/2jZ4po8"
    } ]
  },
  "geo" : { },
  "id_str" : "940732963372888064",
  "text" : "Which publications are costing you the most money in data? Check out this interactive chart from the New York Times  https:\/\/t.co\/HKQBDRMSfL",
  "id" : 940732963372888064,
  "created_at" : "2017-12-12 23:59:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "179569408",
      "id" : 179569408
    }, {
      "name" : "Lufthansa",
      "screen_name" : "lufthansa",
      "indices" : [ 57, 67 ],
      "id_str" : "124476322",
      "id" : 124476322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940716810176954368",
  "text" : "RT @DublinAirport: We have flights for 2 to Germany with @lufthansa up for grabs. Follow &amp; RT to be in with a chance to win &amp; choose betwee\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Lufthansa",
        "screen_name" : "lufthansa",
        "indices" : [ 38, 48 ],
        "id_str" : "124476322",
        "id" : 124476322
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DublinAirport\/status\/940700336632803328\/photo\/1",
        "indices" : [ 168, 191 ],
        "url" : "https:\/\/t.co\/GPbD2Kiw8o",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQ4J-BxV4AA8Wbx.jpg",
        "id_str" : "940700333398941696",
        "id" : 940700333398941696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQ4J-BxV4AA8Wbx.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 441,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GPbD2Kiw8o"
      } ],
      "hashtags" : [ {
        "text" : "DUBLufthansa",
        "indices" : [ 154, 167 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "940700336632803328",
    "text" : "We have flights for 2 to Germany with @lufthansa up for grabs. Follow &amp; RT to be in with a chance to win &amp; choose between Munich &amp; Frankfurt. #DUBLufthansa https:\/\/t.co\/GPbD2Kiw8o",
    "id" : 940700336632803328,
    "created_at" : "2017-12-12 21:50:12 +0000",
    "user" : {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "protected" : false,
      "id_str" : "179569408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013556776321650694\/bdnXxxoU_normal.jpg",
      "id" : 179569408,
      "verified" : true
    }
  },
  "id" : 940716810176954368,
  "created_at" : "2017-12-12 22:55:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940644586589949953",
  "text" : "There should be a Graham Norton's Big Red Chair for BS.",
  "id" : 940644586589949953,
  "created_at" : "2017-12-12 18:08:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Clear",
      "screen_name" : "james_clear",
      "indices" : [ 3, 15 ],
      "id_str" : "1036992550266171393",
      "id" : 1036992550266171393
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940641215837548544",
  "text" : "RT @james_clear: 7\/ Gutenberg's approach is the process of first principles thinking in a nutshell. Break a situation down into the core pi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "931593020847378433",
    "geo" : { },
    "id_str" : "931593228041760768",
    "in_reply_to_user_id" : 226428094,
    "text" : "7\/ Gutenberg's approach is the process of first principles thinking in a nutshell. Break a situation down into the core pieces and then put them back together in a more effective way. Deconstruct then reconstruct.",
    "id" : 931593228041760768,
    "in_reply_to_status_id" : 931593020847378433,
    "created_at" : "2017-11-17 18:41:48 +0000",
    "in_reply_to_screen_name" : "JamesClear",
    "in_reply_to_user_id_str" : "226428094",
    "user" : {
      "name" : "James Clear",
      "screen_name" : "JamesClear",
      "protected" : false,
      "id_str" : "226428094",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/958932211973152769\/FUpkmn4u_normal.jpg",
      "id" : 226428094,
      "verified" : false
    }
  },
  "id" : 940641215837548544,
  "created_at" : "2017-12-12 17:55:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Asboth",
      "screen_name" : "davidasboth",
      "indices" : [ 3, 15 ],
      "id_str" : "3869810177",
      "id" : 3869810177
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/N15JBhbyR2",
      "expanded_url" : "https:\/\/twitter.com\/netflix\/status\/940051734650503168",
      "display_url" : "twitter.com\/netflix\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940637600108417029",
  "text" : "RT @davidasboth: Slightly creepy use of user data or funny data science tweet? https:\/\/t.co\/N15JBhbyR2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/N15JBhbyR2",
        "expanded_url" : "https:\/\/twitter.com\/netflix\/status\/940051734650503168",
        "display_url" : "twitter.com\/netflix\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "940343374179168261",
    "text" : "Slightly creepy use of user data or funny data science tweet? https:\/\/t.co\/N15JBhbyR2",
    "id" : 940343374179168261,
    "created_at" : "2017-12-11 22:11:45 +0000",
    "user" : {
      "name" : "David Asboth",
      "screen_name" : "davidasboth",
      "protected" : false,
      "id_str" : "3869810177",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/650965583069097984\/u5Xy0upY_normal.jpg",
      "id" : 3869810177,
      "verified" : false
    }
  },
  "id" : 940637600108417029,
  "created_at" : "2017-12-12 17:40:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DSLearnings",
      "indices" : [ 196, 208 ]
    } ],
    "urls" : [ {
      "indices" : [ 209, 232 ],
      "url" : "https:\/\/t.co\/okOknLqtAE",
      "expanded_url" : "https:\/\/twitter.com\/dragonflystats\/status\/940283302287511553",
      "display_url" : "twitter.com\/dragonflystats\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940631087453175813",
  "text" : "Key takeaway for me is \"No effort is made to ensure reproducibility, reusability, maintainability, scalability.\" (RRMS)  Docker take care of R, R, M. I am not sure  scalability. Need to find out. #DSLearnings https:\/\/t.co\/okOknLqtAE",
  "id" : 940631087453175813,
  "created_at" : "2017-12-12 17:15:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amazon.com",
      "screen_name" : "amazon",
      "indices" : [ 109, 116 ],
      "id_str" : "20793816",
      "id" : 20793816
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/7wk3zAPvfc",
      "expanded_url" : "http:\/\/www.amazon.co.uk\/registry\/wishlist\/18I0R8EA70YA1\/ref=cm_sw_r_tw",
      "display_url" : "amazon.co.uk\/registry\/wishl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940626524826423296",
  "text" : "If you have benefit what I have shared on Twitter, here my Xmas Filler Stockings https:\/\/t.co\/7wk3zAPvfc via @amazon",
  "id" : 940626524826423296,
  "created_at" : "2017-12-12 16:56:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/aucJckltWB",
      "expanded_url" : "https:\/\/twitter.com\/irishchinese\/status\/940572861361668096",
      "display_url" : "twitter.com\/irishchinese\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940604147757408257",
  "text" : "\"The image of east Asians today has diversified beyond restaurant owners or Fu Manchu\" If I may add, Kung Fu fighting. https:\/\/t.co\/aucJckltWB",
  "id" : 940604147757408257,
  "created_at" : "2017-12-12 15:27:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/940600762136252417\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/sqO8C09l3f",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQ2vQlUWAAAHv-d.jpg",
      "id_str" : "940600596620509184",
      "id" : 940600596620509184,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQ2vQlUWAAAHv-d.jpg",
      "sizes" : [ {
        "h" : 695,
        "resize" : "fit",
        "w" : 1192
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 695,
        "resize" : "fit",
        "w" : 1192
      }, {
        "h" : 695,
        "resize" : "fit",
        "w" : 1192
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sqO8C09l3f"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/N07SJnrVoV",
      "expanded_url" : "http:\/\/www.cso.ie\/en\/interactivezone\/visualisationtools\/babynamesofireland\/",
      "display_url" : "cso.ie\/en\/interactive\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940600762136252417",
  "text" : "From 1983, Mary, a very popular Girl name in Ireland drops into 2nd position.  https:\/\/t.co\/N07SJnrVoV https:\/\/t.co\/sqO8C09l3f",
  "id" : 940600762136252417,
  "created_at" : "2017-12-12 15:14:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fiona Ashe",
      "screen_name" : "FionaAshe",
      "indices" : [ 12, 22 ],
      "id_str" : "23014040",
      "id" : 23014040
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/L7H4obSYt8",
      "expanded_url" : "https:\/\/twitter.com\/galahadclark\/status\/938982780188237824",
      "display_url" : "twitter.com\/galahadclark\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940594379076653056",
  "text" : "I recommend @FionaAshe https:\/\/t.co\/L7H4obSYt8",
  "id" : 940594379076653056,
  "created_at" : "2017-12-12 14:49:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 168, 191 ],
      "url" : "https:\/\/t.co\/yGY2eJF6ZK",
      "expanded_url" : "https:\/\/twitter.com\/romimarketer\/status\/939613912865099776",
      "display_url" : "twitter.com\/romimarketer\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940589477118074880",
  "text" : "\"As Artificial Intelligence becomes cheaper and better at predicting, other inputs into decision making become more valuable, such as judgment on the reward structure\" https:\/\/t.co\/yGY2eJF6ZK",
  "id" : 940589477118074880,
  "created_at" : "2017-12-12 14:29:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "indices" : [ 3, 13 ],
      "id_str" : "27819449",
      "id" : 27819449
    }, {
      "name" : "Dominic Galeon",
      "screen_name" : "domgaleon",
      "indices" : [ 20, 30 ],
      "id_str" : "361483197",
      "id" : 361483197
    }, {
      "name" : "Futurism",
      "screen_name" : "futurism",
      "indices" : [ 107, 116 ],
      "id_str" : "2557446343",
      "id" : 2557446343
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/3JnQoYloRw",
      "expanded_url" : "http:\/\/micfarris.us\/2nKCcp3",
      "display_url" : "micfarris.us\/2nKCcp3"
    } ]
  },
  "geo" : { },
  "id_str" : "940575530428063744",
  "text" : "RT @MicFarris: From @domgaleon --&gt; A Swarm Intelligence Correctly Predicted TIME\u2019s Person of the Year | @futurism https:\/\/t.co\/3JnQoYloRw #\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dominic Galeon",
        "screen_name" : "domgaleon",
        "indices" : [ 5, 15 ],
        "id_str" : "361483197",
        "id" : 361483197
      }, {
        "name" : "Futurism",
        "screen_name" : "futurism",
        "indices" : [ 92, 101 ],
        "id_str" : "2557446343",
        "id" : 2557446343
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 126, 129 ]
      } ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/3JnQoYloRw",
        "expanded_url" : "http:\/\/micfarris.us\/2nKCcp3",
        "display_url" : "micfarris.us\/2nKCcp3"
      } ]
    },
    "geo" : { },
    "id_str" : "940573199821148161",
    "text" : "From @domgaleon --&gt; A Swarm Intelligence Correctly Predicted TIME\u2019s Person of the Year | @futurism https:\/\/t.co\/3JnQoYloRw #AI",
    "id" : 940573199821148161,
    "created_at" : "2017-12-12 13:25:00 +0000",
    "user" : {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "protected" : false,
      "id_str" : "27819449",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3055549457\/94b094a6e85db296aa251b3a775d7439_normal.jpeg",
      "id" : 27819449,
      "verified" : false
    }
  },
  "id" : 940575530428063744,
  "created_at" : "2017-12-12 13:34:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/m26mKrIWlT",
      "expanded_url" : "https:\/\/cna.asia\/2nUp3da",
      "display_url" : "cna.asia\/2nUp3da"
    } ]
  },
  "geo" : { },
  "id_str" : "940533182595727363",
  "text" : "RT @ChannelNewsAsia: Singapore's first electric car-sharing service rolls out with 80 vehicles https:\/\/t.co\/m26mKrIWlT https:\/\/t.co\/tU6DZlQ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/940529990436663296\/photo\/1",
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/tU6DZlQWin",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQ1vCgzUEAECnJo.jpg",
        "id_str" : "940529986145882113",
        "id" : 940529986145882113,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQ1vCgzUEAECnJo.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tU6DZlQWin"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/m26mKrIWlT",
        "expanded_url" : "https:\/\/cna.asia\/2nUp3da",
        "display_url" : "cna.asia\/2nUp3da"
      } ]
    },
    "geo" : { },
    "id_str" : "940529990436663296",
    "text" : "Singapore's first electric car-sharing service rolls out with 80 vehicles https:\/\/t.co\/m26mKrIWlT https:\/\/t.co\/tU6DZlQWin",
    "id" : 940529990436663296,
    "created_at" : "2017-12-12 10:33:18 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 940533182595727363,
  "created_at" : "2017-12-12 10:45:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 165, 188 ],
      "url" : "https:\/\/t.co\/OfqVWzrUYf",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937628420783124481",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940529203874795520",
  "text" : "Docker commands tend to get very long and thus, easy to mess up. To simplify maintenance, I need to created a set of scripts to interact with it. Any advice? Thanks https:\/\/t.co\/OfqVWzrUYf",
  "id" : 940529203874795520,
  "created_at" : "2017-12-12 10:30:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 148, 171 ],
      "url" : "https:\/\/t.co\/IrRNXTW0OT",
      "expanded_url" : "http:\/\/str.sg\/o4ob",
      "display_url" : "str.sg\/o4ob"
    } ]
  },
  "geo" : { },
  "id_str" : "940487805284777984",
  "text" : "Mindef to invite hackers to break into its Internet-connected systems to detect weaknesses and can earn cash rewards between SGD 150 and SGD 20,000 https:\/\/t.co\/IrRNXTW0OT",
  "id" : 940487805284777984,
  "created_at" : "2017-12-12 07:45:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/940320494561169410\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/4Flv89PCoK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQywgWeXkAAjp2g.jpg",
      "id_str" : "940320492048846848",
      "id" : 940320492048846848,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQywgWeXkAAjp2g.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4Flv89PCoK"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/1FkfCxaFcO",
      "expanded_url" : "http:\/\/ift.tt\/2AdWaKM",
      "display_url" : "ift.tt\/2AdWaKM"
    } ]
  },
  "geo" : { },
  "id_str" : "940320494561169410",
  "text" : "Comfort food during the wintery season - noodles curry https:\/\/t.co\/1FkfCxaFcO https:\/\/t.co\/4Flv89PCoK",
  "id" : 940320494561169410,
  "created_at" : "2017-12-11 20:40:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/940316687672496128\/photo\/1",
      "indices" : [ 122, 145 ],
      "url" : "https:\/\/t.co\/baRMNVBfZB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQytCzsX0AAseki.jpg",
      "id_str" : "940316685961252864",
      "id" : 940316685961252864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQytCzsX0AAseki.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/baRMNVBfZB"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/3FSqflabIn",
      "expanded_url" : "http:\/\/ift.tt\/2yfTqdK",
      "display_url" : "ift.tt\/2yfTqdK"
    } ]
  },
  "geo" : { },
  "id_str" : "940316687672496128",
  "text" : "One pot meal with mushroom, onion, red pepper, tomatoes, garlic and cheese with Bolognese sauces. https:\/\/t.co\/3FSqflabIn https:\/\/t.co\/baRMNVBfZB",
  "id" : 940316687672496128,
  "created_at" : "2017-12-11 20:25:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/AbUPb7yXaN",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?time_continue=50&v=xjYCjsRYh6s",
      "display_url" : "youtube.com\/watch?time_con\u2026"
    }, {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/OdrmfXm8Og",
      "expanded_url" : "https:\/\/twitter.com\/ann_donnelly\/status\/940259248554037248",
      "display_url" : "twitter.com\/ann_donnelly\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940263286041448450",
  "text" : "Here\u2019s how Jamie Oliver says you should do it. https:\/\/t.co\/AbUPb7yXaN https:\/\/t.co\/OdrmfXm8Og",
  "id" : 940263286041448450,
  "created_at" : "2017-12-11 16:53:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HireYap",
      "indices" : [ 132, 140 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940187436164026368",
  "text" : "\"We have received your application and will be in touch with you in the next couple of weeks.\" I suppose that will be in 2018 then? #HireYap",
  "id" : 940187436164026368,
  "created_at" : "2017-12-11 11:52:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jim Daly",
      "screen_name" : "psneeze",
      "indices" : [ 3, 11 ],
      "id_str" : "8211522",
      "id" : 8211522
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940175830965276672",
  "text" : "RT @psneeze: Heavy snow and temps of -8\u00B0C forecast. Met \u00C9ireann warn people to stay clear of their Polish neighbours who will roll their ey\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939480246050488320",
    "text" : "Heavy snow and temps of -8\u00B0C forecast. Met \u00C9ireann warn people to stay clear of their Polish neighbours who will roll their eyes and laugh dismissively.",
    "id" : 939480246050488320,
    "created_at" : "2017-12-09 13:02:00 +0000",
    "user" : {
      "name" : "Jim Daly",
      "screen_name" : "psneeze",
      "protected" : false,
      "id_str" : "8211522",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2585184613\/8cine1lzu8a7k4odpaw3_normal.jpeg",
      "id" : 8211522,
      "verified" : false
    }
  },
  "id" : 940175830965276672,
  "created_at" : "2017-12-11 11:06:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940157062956363777",
  "text" : "So Saudi plan to lift the ban on cinema. Will Wonder Woman be the first screening?",
  "id" : 940157062956363777,
  "created_at" : "2017-12-11 09:51:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/940156312750559232\/photo\/1",
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/2ZnJspA9Uk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQwbJuLVwAAZNFS.jpg",
      "id_str" : "940156276041957376",
      "id" : 940156276041957376,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQwbJuLVwAAZNFS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2ZnJspA9Uk"
    } ],
    "hashtags" : [ {
      "text" : "talkingpoint",
      "indices" : [ 78, 91 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940156312750559232",
  "text" : "Is it just me? I can't see any British Asian participates in this discussion. #talkingpoint https:\/\/t.co\/2ZnJspA9Uk",
  "id" : 940156312750559232,
  "created_at" : "2017-12-11 09:48:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Henry",
      "screen_name" : "Mark_J_Henry",
      "indices" : [ 3, 16 ],
      "id_str" : "45539859",
      "id" : 45539859
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940152274877378560",
  "text" : "RT @Mark_J_Henry: Ireland has been ranked the 7th \"goodest\" country in the world today in terms of our global contribution \uD83D\uDC4D https:\/\/t.co\/Z\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/Zp95ZRZt0O",
        "expanded_url" : "https:\/\/goodcountry.org\/index\/results",
        "display_url" : "goodcountry.org\/index\/results"
      } ]
    },
    "geo" : { },
    "id_str" : "940149446310088704",
    "text" : "Ireland has been ranked the 7th \"goodest\" country in the world today in terms of our global contribution \uD83D\uDC4D https:\/\/t.co\/Zp95ZRZt0O",
    "id" : 940149446310088704,
    "created_at" : "2017-12-11 09:21:09 +0000",
    "user" : {
      "name" : "Mark Henry",
      "screen_name" : "Mark_J_Henry",
      "protected" : false,
      "id_str" : "45539859",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/421404033039859712\/Uph4FkzB_normal.jpeg",
      "id" : 45539859,
      "verified" : false
    }
  },
  "id" : 940152274877378560,
  "created_at" : "2017-12-11 09:32:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "indices" : [ 3, 7 ],
      "id_str" : "5120691",
      "id" : 5120691
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/940129278657777664\/photo\/1",
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/nwKoamjgUM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQwCmI7UQAADv7F.jpg",
      "id_str" : "940129276468150272",
      "id" : 940129276468150272,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQwCmI7UQAADv7F.jpg",
      "sizes" : [ {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nwKoamjgUM"
    } ],
    "hashtags" : [ {
      "text" : "recycle",
      "indices" : [ 102, 110 ]
    } ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/mEo6oxxoHP",
      "expanded_url" : "http:\/\/wef.ch\/2kFiBFG",
      "display_url" : "wef.ch\/2kFiBFG"
    } ]
  },
  "geo" : { },
  "id_str" : "940151963827736576",
  "text" : "RT @wef: Germany has come up with the best solution to single-use coffee cups https:\/\/t.co\/mEo6oxxoHP #recycle https:\/\/t.co\/nwKoamjgUM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/940129278657777664\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/nwKoamjgUM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQwCmI7UQAADv7F.jpg",
        "id_str" : "940129276468150272",
        "id" : 940129276468150272,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQwCmI7UQAADv7F.jpg",
        "sizes" : [ {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nwKoamjgUM"
      } ],
      "hashtags" : [ {
        "text" : "recycle",
        "indices" : [ 93, 101 ]
      } ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/mEo6oxxoHP",
        "expanded_url" : "http:\/\/wef.ch\/2kFiBFG",
        "display_url" : "wef.ch\/2kFiBFG"
      } ]
    },
    "geo" : { },
    "id_str" : "940129278657777664",
    "text" : "Germany has come up with the best solution to single-use coffee cups https:\/\/t.co\/mEo6oxxoHP #recycle https:\/\/t.co\/nwKoamjgUM",
    "id" : 940129278657777664,
    "created_at" : "2017-12-11 08:01:01 +0000",
    "user" : {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "protected" : false,
      "id_str" : "5120691",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/565498192171507712\/r2Hb2gvX_normal.png",
      "id" : 5120691,
      "verified" : true
    }
  },
  "id" : 940151963827736576,
  "created_at" : "2017-12-11 09:31:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940123232732172288",
  "text" : "cloudgraphite.io This comes about from an attempt to run a new venture that goes south. The domain is for sale. In the process, I get to know and learn about Docker.",
  "id" : 940123232732172288,
  "created_at" : "2017-12-11 07:36:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "KB photography Ireland",
      "screen_name" : "KBProductionsIR",
      "indices" : [ 3, 19 ],
      "id_str" : "370780685",
      "id" : 370780685
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Wicklow",
      "indices" : [ 99, 107 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 108, 116 ]
    }, {
      "text" : "Sneachta",
      "indices" : [ 117, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "940113320509689856",
  "text" : "RT @KBProductionsIR: I\u2019ve made to Lough dan on foot, do not attempt to drive, roads are impassable #Wicklow #Ireland #Sneachta https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KBProductionsIR\/status\/939798072615424000\/photo\/1",
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/cH1yC0mkzm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQrVWeXXcAA1I_H.jpg",
        "id_str" : "939798054345076736",
        "id" : 939798054345076736,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQrVWeXXcAA1I_H.jpg",
        "sizes" : [ {
          "h" : 1085,
          "resize" : "fit",
          "w" : 1805
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 721,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 409,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1085,
          "resize" : "fit",
          "w" : 1805
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cH1yC0mkzm"
      } ],
      "hashtags" : [ {
        "text" : "Wicklow",
        "indices" : [ 78, 86 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 87, 95 ]
      }, {
        "text" : "Sneachta",
        "indices" : [ 96, 105 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939798072615424000",
    "text" : "I\u2019ve made to Lough dan on foot, do not attempt to drive, roads are impassable #Wicklow #Ireland #Sneachta https:\/\/t.co\/cH1yC0mkzm",
    "id" : 939798072615424000,
    "created_at" : "2017-12-10 10:04:55 +0000",
    "user" : {
      "name" : "KB photography Ireland",
      "screen_name" : "KBProductionsIR",
      "protected" : false,
      "id_str" : "370780685",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/933840642639351808\/VSH0n45a_normal.jpg",
      "id" : 370780685,
      "verified" : false
    }
  },
  "id" : 940113320509689856,
  "created_at" : "2017-12-11 06:57:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DSlearnings",
      "indices" : [ 10, 22 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/GJOUgRMx7m",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939189136338153473",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "940012752223047681",
  "text" : "This week #DSlearnings https:\/\/t.co\/GJOUgRMx7m",
  "id" : 940012752223047681,
  "created_at" : "2017-12-11 00:17:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Kearney\uD83D\uDCCA",
      "screen_name" : "kearneymw",
      "indices" : [ 3, 13 ],
      "id_str" : "2973406683",
      "id" : 2973406683
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/kearneymw\/status\/939924132103323648\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/BY9AMi4jOY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQtHuo2UQAAcFWw.jpg",
      "id_str" : "939923813801672704",
      "id" : 939923813801672704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQtHuo2UQAAcFWw.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 351,
        "resize" : "fit",
        "w" : 389
      }, {
        "h" : 351,
        "resize" : "fit",
        "w" : 389
      }, {
        "h" : 351,
        "resize" : "fit",
        "w" : 389
      }, {
        "h" : 351,
        "resize" : "fit",
        "w" : 389
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BY9AMi4jOY"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/kearneymw\/status\/939924132103323648\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/BY9AMi4jOY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQtHxdeUEAAZeNN.jpg",
      "id_str" : "939923862287814656",
      "id" : 939923862287814656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQtHxdeUEAAZeNN.jpg",
      "sizes" : [ {
        "h" : 773,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 438,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1319,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 2241,
        "resize" : "fit",
        "w" : 3479
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BY9AMi4jOY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939984453794967553",
  "text" : "RT @kearneymw: Trump's net approval is a negative predictor of how often he tweets about \"fake news\" \uD83E\uDD14\uD83E\uDD14\uD83E\uDD14 https:\/\/t.co\/BY9AMi4jOY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/kearneymw\/status\/939924132103323648\/photo\/1",
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/BY9AMi4jOY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQtHuo2UQAAcFWw.jpg",
        "id_str" : "939923813801672704",
        "id" : 939923813801672704,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQtHuo2UQAAcFWw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 389
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 389
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 389
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 389
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BY9AMi4jOY"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/kearneymw\/status\/939924132103323648\/photo\/1",
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/BY9AMi4jOY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQtHxdeUEAAZeNN.jpg",
        "id_str" : "939923862287814656",
        "id" : 939923862287814656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQtHxdeUEAAZeNN.jpg",
        "sizes" : [ {
          "h" : 773,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 438,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1319,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 2241,
          "resize" : "fit",
          "w" : 3479
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BY9AMi4jOY"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939924132103323648",
    "text" : "Trump's net approval is a negative predictor of how often he tweets about \"fake news\" \uD83E\uDD14\uD83E\uDD14\uD83E\uDD14 https:\/\/t.co\/BY9AMi4jOY",
    "id" : 939924132103323648,
    "created_at" : "2017-12-10 18:25:50 +0000",
    "user" : {
      "name" : "Mike Kearney\uD83D\uDCCA",
      "screen_name" : "kearneymw",
      "protected" : false,
      "id_str" : "2973406683",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1000043779250884609\/F_YfqKEw_normal.jpg",
      "id" : 2973406683,
      "verified" : false
    }
  },
  "id" : 939984453794967553,
  "created_at" : "2017-12-10 22:25:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC #OurBluePlanet",
      "screen_name" : "OurBluePlanet",
      "indices" : [ 3, 17 ],
      "id_str" : "809766574194393089",
      "id" : 809766574194393089
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OurBluePlanet",
      "indices" : [ 109, 123 ]
    }, {
      "text" : "BluePlanet2",
      "indices" : [ 124, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939954978571112449",
  "text" : "RT @OurBluePlanet: These were found in the stomachs of Laysan albatross chicks. How does this make you feel? #OurBluePlanet #BluePlanet2 ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/OurBluePlanet\/status\/939953968721080320\/photo\/1",
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/mQlSxDBD1c",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DQti6-9XcAACEm7.jpg",
        "id_str" : "939953712709201920",
        "id" : 939953712709201920,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DQti6-9XcAACEm7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mQlSxDBD1c"
      } ],
      "hashtags" : [ {
        "text" : "OurBluePlanet",
        "indices" : [ 90, 104 ]
      }, {
        "text" : "BluePlanet2",
        "indices" : [ 105, 117 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939953968721080320",
    "text" : "These were found in the stomachs of Laysan albatross chicks. How does this make you feel? #OurBluePlanet #BluePlanet2 https:\/\/t.co\/mQlSxDBD1c",
    "id" : 939953968721080320,
    "created_at" : "2017-12-10 20:24:24 +0000",
    "user" : {
      "name" : "BBC #OurBluePlanet",
      "screen_name" : "OurBluePlanet",
      "protected" : false,
      "id_str" : "809766574194393089",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844966459478589441\/5npkiJIg_normal.jpg",
      "id" : 809766574194393089,
      "verified" : true
    }
  },
  "id" : 939954978571112449,
  "created_at" : "2017-12-10 20:28:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bridget Kromhout",
      "screen_name" : "bridgetkromhout",
      "indices" : [ 3, 19 ],
      "id_str" : "1465659204",
      "id" : 1465659204
    }, {
      "name" : "Andrew Clay Shafer \u96F7\u542F\u7406",
      "screen_name" : "littleidea",
      "indices" : [ 41, 52 ],
      "id_str" : "14079705",
      "id" : 14079705
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/bridgetkromhout\/status\/889859980479926273\/photo\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/B6lqO0F9gH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DFlq6fcXcAEYdvN.jpg",
      "id_str" : "889859954487816193",
      "id" : 889859954487816193,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DFlq6fcXcAEYdvN.jpg",
      "sizes" : [ {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/B6lqO0F9gH"
    } ],
    "hashtags" : [ {
      "text" : "devopsdays",
      "indices" : [ 56, 67 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939903608639950848",
  "text" : "RT @bridgetkromhout: Said no CEO ever\u2026 \uD83D\uDE02 @littleidea at #devopsdays Minneapolis. https:\/\/t.co\/B6lqO0F9gH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Andrew Clay Shafer \u96F7\u542F\u7406",
        "screen_name" : "littleidea",
        "indices" : [ 20, 31 ],
        "id_str" : "14079705",
        "id" : 14079705
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bridgetkromhout\/status\/889859980479926273\/photo\/1",
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/B6lqO0F9gH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DFlq6fcXcAEYdvN.jpg",
        "id_str" : "889859954487816193",
        "id" : 889859954487816193,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DFlq6fcXcAEYdvN.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/B6lqO0F9gH"
      } ],
      "hashtags" : [ {
        "text" : "devopsdays",
        "indices" : [ 35, 46 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "889859980479926273",
    "text" : "Said no CEO ever\u2026 \uD83D\uDE02 @littleidea at #devopsdays Minneapolis. https:\/\/t.co\/B6lqO0F9gH",
    "id" : 889859980479926273,
    "created_at" : "2017-07-25 14:48:46 +0000",
    "user" : {
      "name" : "Bridget Kromhout",
      "screen_name" : "bridgetkromhout",
      "protected" : false,
      "id_str" : "1465659204",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/980605942013161474\/UaWHHTzU_normal.jpg",
      "id" : 1465659204,
      "verified" : true
    }
  },
  "id" : 939903608639950848,
  "created_at" : "2017-12-10 17:04:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "J\u00E9r\u00F4me Petazzoni",
      "screen_name" : "jpetazzo",
      "indices" : [ 3, 12 ],
      "id_str" : "17757889",
      "id" : 17757889
    }, {
      "name" : "Docker",
      "screen_name" : "Docker",
      "indices" : [ 31, 38 ],
      "id_str" : "1138959692",
      "id" : 1138959692
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939903272470671360",
  "text" : "RT @jpetazzo: If you are using @docker (or are planning to) to help you in your DevOps initiatives, you might want to read this: https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Docker",
        "screen_name" : "Docker",
        "indices" : [ 17, 24 ],
        "id_str" : "1138959692",
        "id" : 1138959692
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/v5G8W9hrwj",
        "expanded_url" : "http:\/\/jpetazzo.github.io\/2017\/10\/31\/devops-docker-empathy\/",
        "display_url" : "jpetazzo.github.io\/2017\/10\/31\/dev\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "937761619714756610",
    "text" : "If you are using @docker (or are planning to) to help you in your DevOps initiatives, you might want to read this: https:\/\/t.co\/v5G8W9hrwj \uD83D\uDC33\u2764\uFE0F\uD83D\uDCE6\uD83C\uDF08",
    "id" : 937761619714756610,
    "created_at" : "2017-12-04 19:12:47 +0000",
    "user" : {
      "name" : "J\u00E9r\u00F4me Petazzoni",
      "screen_name" : "jpetazzo",
      "protected" : false,
      "id_str" : "17757889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/658025644865916928\/uIY6m8Sr_normal.jpg",
      "id" : 17757889,
      "verified" : false
    }
  },
  "id" : 939903272470671360,
  "created_at" : "2017-12-10 17:02:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bill",
      "screen_name" : "offdutybilll",
      "indices" : [ 0, 13 ],
      "id_str" : "1384135308",
      "id" : 1384135308
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "939853990791208960",
  "geo" : { },
  "id_str" : "939865149447593985",
  "in_reply_to_user_id" : 1384135308,
  "text" : "@offdutybilll Working on weekend?",
  "id" : 939865149447593985,
  "in_reply_to_status_id" : 939853990791208960,
  "created_at" : "2017-12-10 14:31:28 +0000",
  "in_reply_to_screen_name" : "offdutybilll",
  "in_reply_to_user_id_str" : "1384135308",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rachel Riley",
      "screen_name" : "RachelRileyRR",
      "indices" : [ 3, 17 ],
      "id_str" : "190242011",
      "id" : 190242011
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/5kJMrqDlSN",
      "expanded_url" : "https:\/\/twitter.com\/GMPCityCentre\/status\/939832867756150784",
      "display_url" : "twitter.com\/GMPCityCentre\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939864932333572097",
  "text" : "RT @RachelRileyRR: \uD83D\uDE02\uD83D\uDE02\uD83D\uDE02 spot the tourists! https:\/\/t.co\/5kJMrqDlSN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/5kJMrqDlSN",
        "expanded_url" : "https:\/\/twitter.com\/GMPCityCentre\/status\/939832867756150784",
        "display_url" : "twitter.com\/GMPCityCentre\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "939861947809484800",
    "text" : "\uD83D\uDE02\uD83D\uDE02\uD83D\uDE02 spot the tourists! https:\/\/t.co\/5kJMrqDlSN",
    "id" : 939861947809484800,
    "created_at" : "2017-12-10 14:18:44 +0000",
    "user" : {
      "name" : "Rachel Riley",
      "screen_name" : "RachelRileyRR",
      "protected" : false,
      "id_str" : "190242011",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/712393175126712329\/OA7_Fx5a_normal.jpg",
      "id" : 190242011,
      "verified" : true
    }
  },
  "id" : 939864932333572097,
  "created_at" : "2017-12-10 14:30:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/vsfy6nh7Jh",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/b4be26559d8a160f96bb4d7b565ce99f",
      "display_url" : "gist.github.com\/mryap\/b4be2655\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939829025706532865",
  "text" : "I have 3 options to run my website in the Cloud. Try to run a blog on GitHub pages does not work for me. https:\/\/t.co\/vsfy6nh7Jh",
  "id" : 939829025706532865,
  "created_at" : "2017-12-10 12:07:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939811658918187010",
  "text" : "Tableau dashboard is just one end point of a data analysis process. Any books  where you can pick up how to turn the data analysis into a product? Thanks",
  "id" : 939811658918187010,
  "created_at" : "2017-12-10 10:58:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939807198443724801",
  "text" : "\u76F8\u4EB2\u2026\u2026\n\u5973\uFF08\u6709\u70B9\u9AD8\u50B2\uFF09\uFF1A\u6709\u8F66\u6CA1\uFF1F\n\u7537\uFF1A\u6709\uFF0C\u5965\u8FEAA8\u3002\n\u5973\uFF08\u4E24\u773C\u653E\u5149\uFF09\uFF1A\u623F\u5B50\u4E70\u4E86\u6CA1\u5462\uFF1F\n\u7537\uFF1A\u4E70\u4E86\uFF0C\u4E00\u767E\u56DB\u5341\u5E73\uFF0C\u6C5F\u8FB9\u7684\u3002\n\u5973\uFF08\u82B1\u75F4\u4E00\u6837\u4E86\uFF09\uFF1A\u80FD\u4E0D\u80FD\u95EE\u95EE\u5E74\n\u85AA\u591A\u5C11\uFF1F\n\u7537\uFF1A\u5E74\u85AA\u5728\u4E00\u767E\u516D\u5341\u4E07\u5DE6\u53F3\u3002\n\u5973\uFF08\u6068\u4E0D\u5F97\u4EB2\u8FC7\u53BB\u4E86\uFF09\uFF1A\u4F60\u662F\u505A\u4EC0\n\u4E48\u7684\uFF1F\n\u7537\uFF1A\u505A\u68A6\u7684\u2026\u2026\n\u65C1\u8FB9\u51E0\u684C\u2026\u2026\u55B7\u6C34\u58F0\u2026\u2026\u54B3\u55FD\u58F0\uFF01",
  "id" : 939807198443724801,
  "created_at" : "2017-12-10 10:41:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ran NeuNer",
      "screen_name" : "cryptomanran",
      "indices" : [ 3, 16 ],
      "id_str" : "58487473",
      "id" : 58487473
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939806864698691584",
  "text" : "RT @cryptomanran: A boy asked his bitcoin-investing dad for 1 bitcoin for his birthday. \n\nDad: What? $15,554??? $14,354 is a lot of money!\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939165804809027585",
    "text" : "A boy asked his bitcoin-investing dad for 1 bitcoin for his birthday. \n\nDad: What? $15,554??? $14,354 is a lot of money! What do you need $16,782 for anyway?",
    "id" : 939165804809027585,
    "created_at" : "2017-12-08 16:12:31 +0000",
    "user" : {
      "name" : "Ran NeuNer",
      "screen_name" : "cryptomanran",
      "protected" : false,
      "id_str" : "58487473",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894926583319887872\/ZeFf-3eF_normal.jpg",
      "id" : 58487473,
      "verified" : false
    }
  },
  "id" : 939806864698691584,
  "created_at" : "2017-12-10 10:39:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TNW",
      "screen_name" : "TheNextWeb",
      "indices" : [ 3, 14 ],
      "id_str" : "10876852",
      "id" : 10876852
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/MmFKducxa3",
      "expanded_url" : "http:\/\/tnw.me\/PCvwI7h",
      "display_url" : "tnw.me\/PCvwI7h"
    } ]
  },
  "geo" : { },
  "id_str" : "939806540961402881",
  "text" : "RT @TheNextWeb: Universities finally realize that Java is a bad introductory programming language https:\/\/t.co\/MmFKducxa3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/MmFKducxa3",
        "expanded_url" : "http:\/\/tnw.me\/PCvwI7h",
        "display_url" : "tnw.me\/PCvwI7h"
      } ]
    },
    "geo" : { },
    "id_str" : "938829639291691008",
    "text" : "Universities finally realize that Java is a bad introductory programming language https:\/\/t.co\/MmFKducxa3",
    "id" : 938829639291691008,
    "created_at" : "2017-12-07 17:56:43 +0000",
    "user" : {
      "name" : "TNW",
      "screen_name" : "TheNextWeb",
      "protected" : false,
      "id_str" : "10876852",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013677783300665344\/RqhvBSM3_normal.jpg",
      "id" : 10876852,
      "verified" : true
    }
  },
  "id" : 939806540961402881,
  "created_at" : "2017-12-10 10:38:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ian O'Riordan",
      "screen_name" : "ianoriordan",
      "indices" : [ 3, 15 ],
      "id_str" : "3219425986",
      "id" : 3219425986
    }, {
      "name" : "Met \u00C9ireann",
      "screen_name" : "MetEireann",
      "indices" : [ 75, 86 ],
      "id_str" : "74394857",
      "id" : 74394857
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ianoriordan\/status\/939777995606196224\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/4zau3uZNul",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQrDGyPWsAAjKwY.jpg",
      "id_str" : "939777993592975360",
      "id" : 939777993592975360,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQrDGyPWsAAjKwY.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4zau3uZNul"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939804977496772608",
  "text" : "RT @ianoriordan: Warning: Glencullen village just put on its Christmas hat @MetEireann https:\/\/t.co\/4zau3uZNul",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Met \u00C9ireann",
        "screen_name" : "MetEireann",
        "indices" : [ 58, 69 ],
        "id_str" : "74394857",
        "id" : 74394857
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ianoriordan\/status\/939777995606196224\/photo\/1",
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/4zau3uZNul",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQrDGyPWsAAjKwY.jpg",
        "id_str" : "939777993592975360",
        "id" : 939777993592975360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQrDGyPWsAAjKwY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/4zau3uZNul"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939777995606196224",
    "text" : "Warning: Glencullen village just put on its Christmas hat @MetEireann https:\/\/t.co\/4zau3uZNul",
    "id" : 939777995606196224,
    "created_at" : "2017-12-10 08:45:09 +0000",
    "user" : {
      "name" : "Ian O'Riordan",
      "screen_name" : "ianoriordan",
      "protected" : false,
      "id_str" : "3219425986",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/891347243273515008\/ivqesKpy_normal.jpg",
      "id" : 3219425986,
      "verified" : true
    }
  },
  "id" : 939804977496772608,
  "created_at" : "2017-12-10 10:32:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tarek Fatah",
      "screen_name" : "TarekFatah",
      "indices" : [ 3, 14 ],
      "id_str" : "17537467",
      "id" : 17537467
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/J_Bachelor\/status\/931496621455695872\/video\/1",
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/SYwkFmC7A2",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/931493690459095040\/pu\/img\/1Rn0hqISqHURwMq6.jpg",
      "id_str" : "931493690459095040",
      "id" : 931493690459095040,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/931493690459095040\/pu\/img\/1Rn0hqISqHURwMq6.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/SYwkFmC7A2"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939802360645332992",
  "text" : "RT @TarekFatah: Of little girls and their grandfather.  https:\/\/t.co\/SYwkFmC7A2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/J_Bachelor\/status\/931496621455695872\/video\/1",
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/SYwkFmC7A2",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/931493690459095040\/pu\/img\/1Rn0hqISqHURwMq6.jpg",
        "id_str" : "931493690459095040",
        "id" : 931493690459095040,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/931493690459095040\/pu\/img\/1Rn0hqISqHURwMq6.jpg",
        "sizes" : [ {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/SYwkFmC7A2"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939641778373910528",
    "text" : "Of little girls and their grandfather.  https:\/\/t.co\/SYwkFmC7A2",
    "id" : 939641778373910528,
    "created_at" : "2017-12-09 23:43:52 +0000",
    "user" : {
      "name" : "Tarek Fatah",
      "screen_name" : "TarekFatah",
      "protected" : false,
      "id_str" : "17537467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/997709371348992000\/z2-qSLL4_normal.jpg",
      "id" : 17537467,
      "verified" : true
    }
  },
  "id" : 939802360645332992,
  "created_at" : "2017-12-10 10:21:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 0, 10 ],
      "id_str" : "18849187",
      "id" : 18849187
    }, {
      "name" : "Melanie May",
      "screen_name" : "_melaniemay",
      "indices" : [ 11, 23 ],
      "id_str" : "280004509",
      "id" : 280004509
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939794641683189761\/photo\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/MLuxqwCb7p",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQrSO8JXcAAeKtT.jpg",
      "id_str" : "939794626365583360",
      "id" : 939794626365583360,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQrSO8JXcAAeKtT.jpg",
      "sizes" : [ {
        "h" : 78,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 78,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 78,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 78,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 78,
        "resize" : "crop",
        "w" : 78
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MLuxqwCb7p"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "939791399045750785",
  "geo" : { },
  "id_str" : "939794641683189761",
  "in_reply_to_user_id" : 18849187,
  "text" : "@barryhand @_melaniemay Address Pal's address is in UK Mainland https:\/\/t.co\/MLuxqwCb7p",
  "id" : 939794641683189761,
  "in_reply_to_status_id" : 939791399045750785,
  "created_at" : "2017-12-10 09:51:17 +0000",
  "in_reply_to_screen_name" : "barryhand",
  "in_reply_to_user_id_str" : "18849187",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MalwareTech",
      "screen_name" : "MalwareTechBlog",
      "indices" : [ 3, 19 ],
      "id_str" : "1540951016",
      "id" : 1540951016
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939784096238227456",
  "text" : "RT @MalwareTechBlog: Every year the people doing the Xmas DDoS attacks justify it with \"they want people to spend time with their families\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939607107581571073",
    "text" : "Every year the people doing the Xmas DDoS attacks justify it with \"they want people to spend time with their families not online\", whilst causing on call network admins to be dragged into work to deal with their shit.",
    "id" : 939607107581571073,
    "created_at" : "2017-12-09 21:26:06 +0000",
    "user" : {
      "name" : "MalwareTech",
      "screen_name" : "MalwareTechBlog",
      "protected" : false,
      "id_str" : "1540951016",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/949147161446395905\/Dv_0rZbC_normal.jpg",
      "id" : 1540951016,
      "verified" : true
    }
  },
  "id" : 939784096238227456,
  "created_at" : "2017-12-10 09:09:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939783590442913792\/photo\/1",
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/vAHQo79Vsv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQrIMdVWAAA4c8w.jpg",
      "id_str" : "939783588618305536",
      "id" : 939783588618305536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQrIMdVWAAA4c8w.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vAHQo79Vsv"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/NhYRt810K2",
      "expanded_url" : "http:\/\/ift.tt\/2A8tMJQ",
      "display_url" : "ift.tt\/2A8tMJQ"
    } ]
  },
  "geo" : { },
  "id_str" : "939783590442913792",
  "text" : "Street of Dublin https:\/\/t.co\/NhYRt810K2 https:\/\/t.co\/vAHQo79Vsv",
  "id" : 939783590442913792,
  "created_at" : "2017-12-10 09:07:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    }, {
      "name" : "pptr.ie",
      "screen_name" : "PptrIe",
      "indices" : [ 34, 41 ],
      "id_str" : "900303008004800515",
      "id" : 900303008004800515
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/sMTmjpRqpd",
      "expanded_url" : "http:\/\/prtb.herokuapp.com",
      "display_url" : "prtb.herokuapp.com"
    } ]
  },
  "in_reply_to_status_id_str" : "938711053915054080",
  "geo" : { },
  "id_str" : "939561689107886080",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent A good example is @pptrie that made available Private Residential Tenancies Board (PRTB) data at https:\/\/t.co\/sMTmjpRqpd",
  "id" : 939561689107886080,
  "in_reply_to_status_id" : 938711053915054080,
  "created_at" : "2017-12-09 18:25:37 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LuasCrossCity",
      "indices" : [ 90, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939557825554145281",
  "text" : "Can we have Luas service from The Point to Dublin Airport? (like some other Europe city)  #LuasCrossCity",
  "id" : 939557825554145281,
  "created_at" : "2017-12-09 18:10:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/GfnK64cZeY",
      "expanded_url" : "https:\/\/helpx.adobe.com\/acrobat\/using\/enable-pdf-thumbnail-preview-windows-explorer.html",
      "display_url" : "helpx.adobe.com\/acrobat\/using\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939552782230720512",
  "text" : "How to show thumbnail preview of PDF files in Windows Explorer? https:\/\/t.co\/GfnK64cZeY",
  "id" : 939552782230720512,
  "created_at" : "2017-12-09 17:50:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 172, 195 ],
      "url" : "https:\/\/t.co\/EEebxuYunv",
      "expanded_url" : "https:\/\/medium.com\/@jgolden\/four-questions-every-marketplace-startup-should-be-able-to-answer-defb0590e049",
      "display_url" : "medium.com\/@jgolden\/four-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939548467235622912",
  "text" : "A former director of product discusses the 4 crucial factors shaping every marketplace: network effects, type of supply, incentives, and size and frequency of interaction. https:\/\/t.co\/EEebxuYunv",
  "id" : 939548467235622912,
  "created_at" : "2017-12-09 17:33:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Treasa Lynch",
      "screen_name" : "treasa",
      "indices" : [ 3, 10 ],
      "id_str" : "14746983",
      "id" : 14746983
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939462294878203906",
  "text" : "RT @treasa: The new tram line in Luxembourg opens tomorrow. it will be free for 6 weeks or so. It is covered by the monthly city transport\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 213, 236 ],
        "url" : "https:\/\/t.co\/AYfvd3L4rr",
        "expanded_url" : "https:\/\/twitter.com\/coilinduffy\/status\/939457213864861697",
        "display_url" : "twitter.com\/coilinduffy\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "939459855882117120",
    "text" : "The new tram line in Luxembourg opens tomorrow. it will be free for 6 weeks or so. It is covered by the monthly city transport card. Which costs 25e a month. covered by the country wide card which is 50E a month. https:\/\/t.co\/AYfvd3L4rr",
    "id" : 939459855882117120,
    "created_at" : "2017-12-09 11:40:58 +0000",
    "user" : {
      "name" : "Treasa Lynch",
      "screen_name" : "treasa",
      "protected" : false,
      "id_str" : "14746983",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/712342342674620417\/u6e44GHd_normal.jpg",
      "id" : 14746983,
      "verified" : false
    }
  },
  "id" : 939462294878203906,
  "created_at" : "2017-12-09 11:50:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "indices" : [ 3, 14 ],
      "id_str" : "166540104",
      "id" : 166540104
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 116, 123 ]
    }, {
      "text" : "hackathon",
      "indices" : [ 124, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939441572764160000",
  "text" : "RT @longhowlam: My old laptop, a coke, a cappuccino  and R is all I need to get started today for the Ikea hackaton #rstats #hackathon #iko\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/longhowlam\/status\/939426460699422720\/photo\/1",
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/NfeNtbAdIQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQmDYUSW0AAEOP0.jpg",
        "id_str" : "939426451069259776",
        "id" : 939426451069259776,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQmDYUSW0AAEOP0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NfeNtbAdIQ"
      } ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 100, 107 ]
      }, {
        "text" : "hackathon",
        "indices" : [ 108, 118 ]
      }, {
        "text" : "ikode",
        "indices" : [ 119, 125 ]
      }, {
        "text" : "IKEA",
        "indices" : [ 126, 131 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939426460699422720",
    "text" : "My old laptop, a coke, a cappuccino  and R is all I need to get started today for the Ikea hackaton #rstats #hackathon #ikode #IKEA https:\/\/t.co\/NfeNtbAdIQ",
    "id" : 939426460699422720,
    "created_at" : "2017-12-09 09:28:16 +0000",
    "user" : {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "protected" : false,
      "id_str" : "166540104",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/677041208711970821\/BYuLlEyn_normal.jpg",
      "id" : 166540104,
      "verified" : false
    }
  },
  "id" : 939441572764160000,
  "created_at" : "2017-12-09 10:28:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "indices" : [ 3, 12 ],
      "id_str" : "36823",
      "id" : 36823
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939413008413011969",
  "text" : "RT @anildash: Who is a person (not counting family) that opened doors for you in your career when they didn\u2019t have to? Anytime is a good ti\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939137973240696832",
    "text" : "Who is a person (not counting family) that opened doors for you in your career when they didn\u2019t have to? Anytime is a good time to show gratitude!",
    "id" : 939137973240696832,
    "created_at" : "2017-12-08 14:21:55 +0000",
    "user" : {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "protected" : false,
      "id_str" : "36823",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040592282032984071\/RC8We7BG_normal.jpg",
      "id" : 36823,
      "verified" : true
    }
  },
  "id" : 939413008413011969,
  "created_at" : "2017-12-09 08:34:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Roberto Alonso Gonz\u00E1lez Lezcano",
      "screen_name" : "robertoglezcano",
      "indices" : [ 3, 19 ],
      "id_str" : "1186532348",
      "id" : 1186532348
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ThingsWork\/status\/934922280676798464\/photo\/1",
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/oBbX8Sf8tS",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DPmC2lOW4AMduVy.jpg",
      "id_str" : "934922271872966659",
      "id" : 934922271872966659,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DPmC2lOW4AMduVy.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 654,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 654,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 654,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 654,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oBbX8Sf8tS"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939286751637581824",
  "text" : "RT @robertoglezcano: This is how a gearbox works https:\/\/t.co\/oBbX8Sf8tS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ThingsWork\/status\/934922280676798464\/photo\/1",
        "indices" : [ 28, 51 ],
        "url" : "https:\/\/t.co\/oBbX8Sf8tS",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DPmC2lOW4AMduVy.jpg",
        "id_str" : "934922271872966659",
        "id" : 934922271872966659,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DPmC2lOW4AMduVy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oBbX8Sf8tS"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938540931573276673",
    "text" : "This is how a gearbox works https:\/\/t.co\/oBbX8Sf8tS",
    "id" : 938540931573276673,
    "created_at" : "2017-12-06 22:49:29 +0000",
    "user" : {
      "name" : "Roberto Alonso Gonz\u00E1lez Lezcano",
      "screen_name" : "robertoglezcano",
      "protected" : false,
      "id_str" : "1186532348",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/534812004632911872\/ENTd3Qsu_normal.png",
      "id" : 1186532348,
      "verified" : false
    }
  },
  "id" : 939286751637581824,
  "created_at" : "2017-12-09 00:13:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/ISPt4TNPtN",
      "expanded_url" : "https:\/\/twitter.com\/BBCOne\/status\/936897750041468928",
      "display_url" : "twitter.com\/BBCOne\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939285936088723456",
  "text" : "This appears for few times on my timeline. FINALLY watch it on the telly tonight. https:\/\/t.co\/ISPt4TNPtN",
  "id" : 939285936088723456,
  "created_at" : "2017-12-09 00:09:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939249439528669185\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/f6uqrevvWF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQjiYzGW4AAfMvA.jpg",
      "id_str" : "939249437968359424",
      "id" : 939249437968359424,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQjiYzGW4AAfMvA.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/f6uqrevvWF"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/q5cJHTeNRU",
      "expanded_url" : "http:\/\/ift.tt\/2kG7egD",
      "display_url" : "ift.tt\/2kG7egD"
    } ]
  },
  "geo" : { },
  "id_str" : "939249439528669185",
  "text" : "The Grand Tour poster at the bus stop crack me up https:\/\/t.co\/q5cJHTeNRU https:\/\/t.co\/f6uqrevvWF",
  "id" : 939249439528669185,
  "created_at" : "2017-12-08 21:44:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "changetheratio",
      "indices" : [ 120, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939217241656250370",
  "text" : "RT @LumenStrategies: Heading to a tech conf? Perspective from a female engineer ...aka don't assume she is a caterer... #changetheratio htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "changetheratio",
        "indices" : [ 99, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/IWvC52aCo8",
        "expanded_url" : "http:\/\/ow.ly\/fM3Y30e1hEp",
        "display_url" : "ow.ly\/fM3Y30e1hEp"
      } ]
    },
    "geo" : { },
    "id_str" : "892350506303094784",
    "text" : "Heading to a tech conf? Perspective from a female engineer ...aka don't assume she is a caterer... #changetheratio https:\/\/t.co\/IWvC52aCo8",
    "id" : 892350506303094784,
    "created_at" : "2017-08-01 11:45:14 +0000",
    "user" : {
      "name" : "Kathryn Robinson",
      "screen_name" : "NCKYRobinson",
      "protected" : false,
      "id_str" : "374544811",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/682041859435311104\/d3DHjQ9b_normal.jpg",
      "id" : 374544811,
      "verified" : false
    }
  },
  "id" : 939217241656250370,
  "created_at" : "2017-12-08 19:36:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939209191968989184\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/cADQ6zTA2A",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQi9tZTW0AE8Ix4.jpg",
      "id_str" : "939209109890584577",
      "id" : 939209109890584577,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQi9tZTW0AE8Ix4.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 676,
        "resize" : "fit",
        "w" : 669
      }, {
        "h" : 676,
        "resize" : "fit",
        "w" : 669
      }, {
        "h" : 676,
        "resize" : "fit",
        "w" : 669
      }, {
        "h" : 676,
        "resize" : "fit",
        "w" : 669
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cADQ6zTA2A"
    } ],
    "hashtags" : [ {
      "text" : "TheSingaporeStory",
      "indices" : [ 98, 116 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "939099198695329792",
  "geo" : { },
  "id_str" : "939209191968989184",
  "in_reply_to_user_id" : 9465632,
  "text" : "The Ireland question. Aware from social media chatter, \"southern Ireland\" is not the word to use. #TheSingaporeStory https:\/\/t.co\/cADQ6zTA2A",
  "id" : 939209191968989184,
  "in_reply_to_status_id" : 939099198695329792,
  "created_at" : "2017-12-08 19:04:55 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ThomasFire",
      "indices" : [ 3, 14 ]
    } ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/1oNx7neX9U",
      "expanded_url" : "https:\/\/www.google.ie\/maps\/place\/Mar-a-Lago+Club\/@26.6770665,-80.107018,12z\/data=!4m5!3m4!1s0x88d8d71e9a5c409d:0x71bb1222105e5650!8m2!3d26.6770665!4d-80.0369802",
      "display_url" : "google.ie\/maps\/place\/Mar\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939204770346258432",
  "text" : "hi #ThomasFire, I believe the place you are looking for is here https:\/\/t.co\/1oNx7neX9U",
  "id" : 939204770346258432,
  "created_at" : "2017-12-08 18:47:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rtebluebook",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/ccly74d2aQ",
      "expanded_url" : "https:\/\/twitter.com\/rte\/status\/936716080894087170",
      "display_url" : "twitter.com\/rte\/status\/936\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939198009786683392",
  "text" : "#rtebluebook https:\/\/t.co\/ccly74d2aQ",
  "id" : 939198009786683392,
  "created_at" : "2017-12-08 18:20:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939189136338153473\/photo\/1",
      "indices" : [ 191, 214 ],
      "url" : "https:\/\/t.co\/A8RY1gTRqu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQirHrPWAAAl9j8.jpg",
      "id_str" : "939188670661263360",
      "id" : 939188670661263360,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQirHrPWAAAl9j8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1181
      }, {
        "h" : 192,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1181
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1181
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/A8RY1gTRqu"
    } ],
    "hashtags" : [ {
      "text" : "techienote",
      "indices" : [ 179, 190 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939189136338153473",
  "text" : "When there is no Terminal unavailable in Jupyter Notebook, running the following in the cell is an alternative. This script output all Python library installed into a text file.  #techienote https:\/\/t.co\/A8RY1gTRqu",
  "id" : 939189136338153473,
  "created_at" : "2017-12-08 17:45:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939112074470445058\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/PzqtgGVzsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQhlUh4W4AApQB0.jpg",
      "id_str" : "939111925673287680",
      "id" : 939111925673287680,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQhlUh4W4AApQB0.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 679
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 681,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 681,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 681,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PzqtgGVzsH"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939112074470445058\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/PzqtgGVzsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQhlUh2X0AECv0C.jpg",
      "id_str" : "939111925664960513",
      "id" : 939111925664960513,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQhlUh2X0AECv0C.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 363
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 363
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 363
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 363
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PzqtgGVzsH"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939112074470445058",
  "text" : "Getting rid of worldly procession calls for a change in mindsets. Recommended read for the new year https:\/\/t.co\/PzqtgGVzsH",
  "id" : 939112074470445058,
  "created_at" : "2017-12-08 12:39:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939099198695329792\/photo\/1",
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/6EjflYi67U",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQhZm_MW4AApTMb.jpg",
      "id_str" : "939099048639913984",
      "id" : 939099048639913984,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQhZm_MW4AApTMb.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 671,
        "resize" : "fit",
        "w" : 674
      }, {
        "h" : 671,
        "resize" : "fit",
        "w" : 674
      }, {
        "h" : 671,
        "resize" : "fit",
        "w" : 674
      }, {
        "h" : 671,
        "resize" : "fit",
        "w" : 674
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6EjflYi67U"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939099198695329792",
  "text" : "Currently reading The Singapore Story https:\/\/t.co\/6EjflYi67U",
  "id" : 939099198695329792,
  "created_at" : "2017-12-08 11:47:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/939047856744394752\/photo\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/8SjdC0sDkk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQgrCbtX0AAkfph.jpg",
      "id_str" : "939047843104542720",
      "id" : 939047843104542720,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQgrCbtX0AAkfph.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8SjdC0sDkk"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939056233100857344",
  "text" : "RT @lisaocarroll: Varadkar: \u201CThis is not the end but it is end of the beginning\u201D https:\/\/t.co\/8SjdC0sDkk",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/939047856744394752\/photo\/1",
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/8SjdC0sDkk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQgrCbtX0AAkfph.jpg",
        "id_str" : "939047843104542720",
        "id" : 939047843104542720,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQgrCbtX0AAkfph.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8SjdC0sDkk"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939047856744394752",
    "text" : "Varadkar: \u201CThis is not the end but it is end of the beginning\u201D https:\/\/t.co\/8SjdC0sDkk",
    "id" : 939047856744394752,
    "created_at" : "2017-12-08 08:23:50 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 939056233100857344,
  "created_at" : "2017-12-08 08:57:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939051593265287168\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/andg3Bx4dL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQgucnvX0AAifqK.jpg",
      "id_str" : "939051591545638912",
      "id" : 939051591545638912,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQgucnvX0AAifqK.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/andg3Bx4dL"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/GaCdnYJd3y",
      "expanded_url" : "http:\/\/ift.tt\/2Amj7zl",
      "display_url" : "ift.tt\/2Amj7zl"
    } ]
  },
  "geo" : { },
  "id_str" : "939051593265287168",
  "text" : "Irish Prime Minister statement on Brexit negotiations live on TV this morning. https:\/\/t.co\/GaCdnYJd3y https:\/\/t.co\/andg3Bx4dL",
  "id" : 939051593265287168,
  "created_at" : "2017-12-08 08:38:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939040197441925120",
  "text" : "RT @Dotnetster: 1. Go to Twitter Settings\n2. Mute Words\n3. Enter \"Bitcoin\", \"btc\", \"crypto\"\n\nYou're welcome.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "939039526168666114",
    "text" : "1. Go to Twitter Settings\n2. Mute Words\n3. Enter \"Bitcoin\", \"btc\", \"crypto\"\n\nYou're welcome.",
    "id" : 939039526168666114,
    "created_at" : "2017-12-08 07:50:44 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 939040197441925120,
  "created_at" : "2017-12-08 07:53:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frankie Zelnick",
      "screen_name" : "phranqueigh",
      "indices" : [ 3, 15 ],
      "id_str" : "16313155",
      "id" : 16313155
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939036795110428672",
  "text" : "RT @phranqueigh: My 2018 resolution list is just a list of everything I usually do with the word \"stop\" in front of it.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938949702674014209",
    "text" : "My 2018 resolution list is just a list of everything I usually do with the word \"stop\" in front of it.",
    "id" : 938949702674014209,
    "created_at" : "2017-12-08 01:53:48 +0000",
    "user" : {
      "name" : "Frankie Zelnick",
      "screen_name" : "phranqueigh",
      "protected" : false,
      "id_str" : "16313155",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/868484696690360320\/iiDyh1Uo_normal.jpg",
      "id" : 16313155,
      "verified" : false
    }
  },
  "id" : 939036795110428672,
  "created_at" : "2017-12-08 07:39:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darlin\u2019 Darla",
      "screen_name" : "Darlainky",
      "indices" : [ 3, 13 ],
      "id_str" : "29494126",
      "id" : 29494126
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939036518898794497",
  "text" : "RT @Darlainky: I\u2019m going green for the holidays.\nGrinch green.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938946306676002821",
    "text" : "I\u2019m going green for the holidays.\nGrinch green.",
    "id" : 938946306676002821,
    "created_at" : "2017-12-08 01:40:18 +0000",
    "user" : {
      "name" : "Darlin\u2019 Darla",
      "screen_name" : "Darlainky",
      "protected" : false,
      "id_str" : "29494126",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948044185688510464\/z9Z2KcgN_normal.jpg",
      "id" : 29494126,
      "verified" : false
    }
  },
  "id" : 939036518898794497,
  "created_at" : "2017-12-08 07:38:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Atlantic",
      "screen_name" : "TheAtlantic",
      "indices" : [ 3, 15 ],
      "id_str" : "35773039",
      "id" : 35773039
    }, {
      "name" : "Joe Pinsker",
      "screen_name" : "jpinsk",
      "indices" : [ 69, 76 ],
      "id_str" : "163966591",
      "id" : 163966591
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheAtlantic\/status\/938759537691185152\/photo\/1",
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/FsdCQQCNON",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQck0sDVQAAHvvT.jpg",
      "id_str" : "938759534926970880",
      "id" : 938759534926970880,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQck0sDVQAAHvvT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 354,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FsdCQQCNON"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/GrOlgRNIbB",
      "expanded_url" : "http:\/\/theatln.tc\/2BGkF3q",
      "display_url" : "theatln.tc\/2BGkF3q"
    } ]
  },
  "geo" : { },
  "id_str" : "939034102363066368",
  "text" : "RT @TheAtlantic: How online retail is changing product packaging, by @jpinsk https:\/\/t.co\/GrOlgRNIbB https:\/\/t.co\/FsdCQQCNON",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Joe Pinsker",
        "screen_name" : "jpinsk",
        "indices" : [ 52, 59 ],
        "id_str" : "163966591",
        "id" : 163966591
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheAtlantic\/status\/938759537691185152\/photo\/1",
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/FsdCQQCNON",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQck0sDVQAAHvvT.jpg",
        "id_str" : "938759534926970880",
        "id" : 938759534926970880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQck0sDVQAAHvvT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 354,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FsdCQQCNON"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/GrOlgRNIbB",
        "expanded_url" : "http:\/\/theatln.tc\/2BGkF3q",
        "display_url" : "theatln.tc\/2BGkF3q"
      } ]
    },
    "geo" : { },
    "id_str" : "938759537691185152",
    "text" : "How online retail is changing product packaging, by @jpinsk https:\/\/t.co\/GrOlgRNIbB https:\/\/t.co\/FsdCQQCNON",
    "id" : 938759537691185152,
    "created_at" : "2017-12-07 13:18:09 +0000",
    "user" : {
      "name" : "The Atlantic",
      "screen_name" : "TheAtlantic",
      "protected" : false,
      "id_str" : "35773039",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/880086372564041728\/rZ4SGWbE_normal.jpg",
      "id" : 35773039,
      "verified" : true
    }
  },
  "id" : 939034102363066368,
  "created_at" : "2017-12-08 07:29:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u7D05\u674F\u722C\u7246",
      "screen_name" : "2017ya2266",
      "indices" : [ 3, 14 ],
      "id_str" : "877521360108503040",
      "id" : 877521360108503040
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/2017ya2266\/status\/937658570002141184\/video\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/jggTMpjLHS",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/937657536978677760\/pu\/img\/xYSwu6-pEJujOxMb.jpg",
      "id_str" : "937657536978677760",
      "id" : 937657536978677760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/937657536978677760\/pu\/img\/xYSwu6-pEJujOxMb.jpg",
      "sizes" : [ {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jggTMpjLHS"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939033313133584386",
  "text" : "RT @2017ya2266: \u97D3\u570B\u525B\u525B\u5C55\u793A\u4E86\u5B83\u7684\u96B1\u5F62\u5DE1\u822A\u5C0E\u5F48\u5982\u4F55\u4EE5\u96E3\u4EE5\u7F6E\u4FE1\u7684\u7CBE\u5EA6\u64CA\u4E2D\u4E00\u500B\u5FAE\u5C0F\u7684\u76EE\u6A19\u3002 https:\/\/t.co\/jggTMpjLHS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/2017ya2266\/status\/937658570002141184\/video\/1",
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/jggTMpjLHS",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/937657536978677760\/pu\/img\/xYSwu6-pEJujOxMb.jpg",
        "id_str" : "937657536978677760",
        "id" : 937657536978677760,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/937657536978677760\/pu\/img\/xYSwu6-pEJujOxMb.jpg",
        "sizes" : [ {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jggTMpjLHS"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937658570002141184",
    "text" : "\u97D3\u570B\u525B\u525B\u5C55\u793A\u4E86\u5B83\u7684\u96B1\u5F62\u5DE1\u822A\u5C0E\u5F48\u5982\u4F55\u4EE5\u96E3\u4EE5\u7F6E\u4FE1\u7684\u7CBE\u5EA6\u64CA\u4E2D\u4E00\u500B\u5FAE\u5C0F\u7684\u76EE\u6A19\u3002 https:\/\/t.co\/jggTMpjLHS",
    "id" : 937658570002141184,
    "created_at" : "2017-12-04 12:23:18 +0000",
    "user" : {
      "name" : "\u7D05\u674F\u722C\u7246",
      "screen_name" : "2017ya2266",
      "protected" : false,
      "id_str" : "877521360108503040",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882803026410430464\/mCzJkvY1_normal.jpg",
      "id" : 877521360108503040,
      "verified" : false
    }
  },
  "id" : 939033313133584386,
  "created_at" : "2017-12-08 07:26:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/939031931483701248\/photo\/1",
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/2vWXRCH7kG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQgckKYUQAAbJJN.jpg",
      "id_str" : "939031929893961728",
      "id" : 939031929893961728,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQgckKYUQAAbJJN.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2vWXRCH7kG"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/kPDVs7VWnq",
      "expanded_url" : "http:\/\/ift.tt\/2AokptO",
      "display_url" : "ift.tt\/2AokptO"
    } ]
  },
  "geo" : { },
  "id_str" : "939031931483701248",
  "text" : "Xmas tree waiting to be unwrapped. https:\/\/t.co\/kPDVs7VWnq https:\/\/t.co\/2vWXRCH7kG",
  "id" : 939031931483701248,
  "created_at" : "2017-12-08 07:20:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PeterMurtagh",
      "screen_name" : "PeterMurtagh",
      "indices" : [ 3, 16 ],
      "id_str" : "18675985",
      "id" : 18675985
    }, {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 84, 95 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/qrV5ywrCcl",
      "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/defence-forces-carry-out-large-anti-terrorist-drill-in-dublin-1.3318082",
      "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939030662031355905",
  "text" : "RT @PeterMurtagh: Defence Forces carry out large anti-terrorist drill in Dublin\nvia @IrishTimes\nhttps:\/\/t.co\/qrV5ywrCcl",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Irish Times",
        "screen_name" : "IrishTimes",
        "indices" : [ 66, 77 ],
        "id_str" : "15084853",
        "id" : 15084853
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/qrV5ywrCcl",
        "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/defence-forces-carry-out-large-anti-terrorist-drill-in-dublin-1.3318082",
        "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "938544478188695552",
    "text" : "Defence Forces carry out large anti-terrorist drill in Dublin\nvia @IrishTimes\nhttps:\/\/t.co\/qrV5ywrCcl",
    "id" : 938544478188695552,
    "created_at" : "2017-12-06 23:03:35 +0000",
    "user" : {
      "name" : "PeterMurtagh",
      "screen_name" : "PeterMurtagh",
      "protected" : false,
      "id_str" : "18675985",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/960929037953781760\/ICFofx7V_normal.jpg",
      "id" : 18675985,
      "verified" : true
    }
  },
  "id" : 939030662031355905,
  "created_at" : "2017-12-08 07:15:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ahmed Abdulrahman \uD83D\uDE3B",
      "screen_name" : "_ahmed_ab",
      "indices" : [ 3, 13 ],
      "id_str" : "734983338",
      "id" : 734983338
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/yo8K4xRxd4",
      "expanded_url" : "https:\/\/twitter.com\/melmitchell1\/status\/938203862439157761",
      "display_url" : "twitter.com\/melmitchell1\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939029917097910273",
  "text" : "RT @_ahmed_ab: This is terrifying https:\/\/t.co\/yo8K4xRxd4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 19, 42 ],
        "url" : "https:\/\/t.co\/yo8K4xRxd4",
        "expanded_url" : "https:\/\/twitter.com\/melmitchell1\/status\/938203862439157761",
        "display_url" : "twitter.com\/melmitchell1\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "938955245866115072",
    "text" : "This is terrifying https:\/\/t.co\/yo8K4xRxd4",
    "id" : 938955245866115072,
    "created_at" : "2017-12-08 02:15:50 +0000",
    "user" : {
      "name" : "Ahmed Abdulrahman \uD83D\uDE3B",
      "screen_name" : "_ahmed_ab",
      "protected" : false,
      "id_str" : "734983338",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846237929492172801\/GI1ms1zr_normal.jpg",
      "id" : 734983338,
      "verified" : false
    }
  },
  "id" : 939029917097910273,
  "created_at" : "2017-12-08 07:12:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Chavez",
      "screen_name" : "sarah_calavera",
      "indices" : [ 3, 18 ],
      "id_str" : "510617026",
      "id" : 510617026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "939029384056283137",
  "text" : "RT @sarah_calavera: \uD83C\uDF84\uD83D\uDC26\uD83D\uDC80\u2764\uFE0F\nKilling a wren or robin was once a good-luck ritual performed in late December, during the late 19th century. Car\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sarah_calavera\/status\/938459327001538560\/photo\/1",
        "indices" : [ 268, 291 ],
        "url" : "https:\/\/t.co\/9vOAhRyyMv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQYTxlAUMAA1Rg3.jpg",
        "id_str" : "938459314821279744",
        "id" : 938459314821279744,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQYTxlAUMAA1Rg3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 222,
          "resize" : "fit",
          "w" : 332
        }, {
          "h" : 222,
          "resize" : "fit",
          "w" : 332
        }, {
          "h" : 222,
          "resize" : "fit",
          "w" : 332
        }, {
          "h" : 222,
          "resize" : "fit",
          "w" : 332
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9vOAhRyyMv"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/sarah_calavera\/status\/938459327001538560\/photo\/1",
        "indices" : [ 268, 291 ],
        "url" : "https:\/\/t.co\/9vOAhRyyMv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQYTxk6V4AAEn27.jpg",
        "id_str" : "938459314796224512",
        "id" : 938459314796224512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQYTxk6V4AAEn27.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9vOAhRyyMv"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/sarah_calavera\/status\/938459327001538560\/photo\/1",
        "indices" : [ 268, 291 ],
        "url" : "https:\/\/t.co\/9vOAhRyyMv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQYTxk9UQAAISdY.jpg",
        "id_str" : "938459314808700928",
        "id" : 938459314808700928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQYTxk9UQAAISdY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 566,
          "resize" : "fit",
          "w" : 850
        }, {
          "h" : 566,
          "resize" : "fit",
          "w" : 850
        }, {
          "h" : 566,
          "resize" : "fit",
          "w" : 850
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9vOAhRyyMv"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/sarah_calavera\/status\/938459327001538560\/photo\/1",
        "indices" : [ 268, 291 ],
        "url" : "https:\/\/t.co\/9vOAhRyyMv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQYTxk7UEAE2gSM.jpg",
        "id_str" : "938459314800300033",
        "id" : 938459314800300033,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQYTxk7UEAE2gSM.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 662
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 662
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 662
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 662
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9vOAhRyyMv"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 244, 267 ],
        "url" : "https:\/\/t.co\/XQtBAy9YVu",
        "expanded_url" : "https:\/\/instagram.com\/p\/BcS9MRYBeV2\/",
        "display_url" : "instagram.com\/p\/BcS9MRYBeV2\/"
      } ]
    },
    "geo" : { },
    "id_str" : "938459327001538560",
    "text" : "\uD83C\uDF84\uD83D\uDC26\uD83D\uDC80\u2764\uFE0F\nKilling a wren or robin was once a good-luck ritual performed in late December, during the late 19th century. Cards featuring the bodies of these birds were sent as Christmas greetings and to wish  good luck in the New Year. \n\nRead more: https:\/\/t.co\/XQtBAy9YVu https:\/\/t.co\/9vOAhRyyMv",
    "id" : 938459327001538560,
    "created_at" : "2017-12-06 17:25:13 +0000",
    "user" : {
      "name" : "Sarah Chavez",
      "screen_name" : "sarah_calavera",
      "protected" : false,
      "id_str" : "510617026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/965271623732375552\/GiG_PJn2_normal.jpg",
      "id" : 510617026,
      "verified" : false
    }
  },
  "id" : 939029384056283137,
  "created_at" : "2017-12-08 07:10:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/iQopr9L5t8",
      "expanded_url" : "https:\/\/twitter.com\/stephenkinsella\/status\/938916056684380160",
      "display_url" : "twitter.com\/stephenkinsell\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "939028878219030528",
  "text" : "RT @topgold: Real research for real people that hopefully gets real results. https:\/\/t.co\/iQopr9L5t8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/iQopr9L5t8",
        "expanded_url" : "https:\/\/twitter.com\/stephenkinsella\/status\/938916056684380160",
        "display_url" : "twitter.com\/stephenkinsell\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "939028199517822978",
    "text" : "Real research for real people that hopefully gets real results. https:\/\/t.co\/iQopr9L5t8",
    "id" : 939028199517822978,
    "created_at" : "2017-12-08 07:05:43 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 939028878219030528,
  "created_at" : "2017-12-08 07:08:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sachin Abeywardana",
      "screen_name" : "sachinabey",
      "indices" : [ 135, 146 ],
      "id_str" : "178562590",
      "id" : 178562590
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 147, 170 ],
      "url" : "https:\/\/t.co\/cBIn9AJzzW",
      "expanded_url" : "https:\/\/towardsdatascience.com\/docker-for-data-science-4901f35d7cf9",
      "display_url" : "towardsdatascience.com\/docker-for-dat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938859224607678465",
  "text" : "This is what keeping me occupied for past few weeks. Picked up Docker. With Git, it is part of my toolbox \u201CDocker for Data Science\u201D by @sachinabey https:\/\/t.co\/cBIn9AJzzW",
  "id" : 938859224607678465,
  "created_at" : "2017-12-07 19:54:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elea McDonnell Feit",
      "screen_name" : "eleafeit",
      "indices" : [ 3, 12 ],
      "id_str" : "107600670",
      "id" : 107600670
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 45, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938857827111723009",
  "text" : "RT @eleafeit: You know what I like about the #rstats community? The inter-discliplinary interaction. R has lead me to people working in #Po\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 31, 38 ]
      }, {
        "text" : "PoliSci",
        "indices" : [ 122, 130 ]
      }, {
        "text" : "psychology",
        "indices" : [ 131, 142 ]
      }, {
        "text" : "finance",
        "indices" : [ 143, 151 ]
      }, {
        "text" : "measure",
        "indices" : [ 152, 160 ]
      }, {
        "text" : "stats",
        "indices" : [ 161, 167 ]
      }, {
        "text" : "computationalbiology",
        "indices" : [ 168, 189 ]
      }, {
        "text" : "Transportation",
        "indices" : [ 190, 205 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938851639376728066",
    "text" : "You know what I like about the #rstats community? The inter-discliplinary interaction. R has lead me to people working in #PoliSci #psychology #finance #measure #stats #computationalbiology #Transportation",
    "id" : 938851639376728066,
    "created_at" : "2017-12-07 19:24:08 +0000",
    "user" : {
      "name" : "Elea McDonnell Feit",
      "screen_name" : "eleafeit",
      "protected" : false,
      "id_str" : "107600670",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1022142398086963200\/M1AUZ37a_normal.jpg",
      "id" : 107600670,
      "verified" : false
    }
  },
  "id" : 938857827111723009,
  "created_at" : "2017-12-07 19:48:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938763269107539968",
  "text" : "RT @lisaocarroll: Brexit select committee toeing the Irish border - the change in road surface is only marker if border https:\/\/t.co\/1qbjxe\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/938758612041850880\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/1qbjxewnpV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQcjZKiU8AAVQCL.jpg",
        "id_str" : "938757962562072576",
        "id" : 938757962562072576,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQcjZKiU8AAVQCL.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1qbjxewnpV"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/938758612041850880\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/1qbjxewnpV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQcjZLEUIAA163k.jpg",
        "id_str" : "938757962704625664",
        "id" : 938757962704625664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQcjZLEUIAA163k.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1qbjxewnpV"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/938758612041850880\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/1qbjxewnpV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQcjZKHUIAAHJoj.jpg",
        "id_str" : "938757962448773120",
        "id" : 938757962448773120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQcjZKHUIAAHJoj.jpg",
        "sizes" : [ {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1qbjxewnpV"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938758612041850880",
    "text" : "Brexit select committee toeing the Irish border - the change in road surface is only marker if border https:\/\/t.co\/1qbjxewnpV",
    "id" : 938758612041850880,
    "created_at" : "2017-12-07 13:14:29 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 938763269107539968,
  "created_at" : "2017-12-07 13:32:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hannah Ritchie",
      "screen_name" : "HannahRitchie02",
      "indices" : [ 3, 19 ],
      "id_str" : "2666969059",
      "id" : 2666969059
    }, {
      "name" : "Max Roser",
      "screen_name" : "MaxCRoser",
      "indices" : [ 39, 49 ],
      "id_str" : "610659001",
      "id" : 610659001
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938745235257085952",
  "text" : "RT @HannahRitchie02: Joe Hassell &amp; @MaxCRoser have done a great rework on global history of famines. So much good data and insightful visua\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Max Roser",
        "screen_name" : "MaxCRoser",
        "indices" : [ 18, 28 ],
        "id_str" : "610659001",
        "id" : 610659001
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/HannahRitchie02\/status\/938744327005769729\/photo\/1",
        "indices" : [ 254, 277 ],
        "url" : "https:\/\/t.co\/6pqkEmdyWm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQcW56dVwAE20wV.jpg",
        "id_str" : "938744231530708993",
        "id" : 938744231530708993,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQcW56dVwAE20wV.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 531
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 937
        }, {
          "h" : 4056,
          "resize" : "fit",
          "w" : 3168
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6pqkEmdyWm"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 230, 253 ],
        "url" : "https:\/\/t.co\/mFh7x2B05p",
        "expanded_url" : "https:\/\/ourworldindata.org\/famines\/",
        "display_url" : "ourworldindata.org\/famines\/"
      } ]
    },
    "geo" : { },
    "id_str" : "938744327005769729",
    "text" : "Joe Hassell &amp; @MaxCRoser have done a great rework on global history of famines. So much good data and insightful visualisations in there. Here are famines by world region (very few in the Americas). \n\nYou can explore more at: https:\/\/t.co\/mFh7x2B05p https:\/\/t.co\/6pqkEmdyWm",
    "id" : 938744327005769729,
    "created_at" : "2017-12-07 12:17:43 +0000",
    "user" : {
      "name" : "Hannah Ritchie",
      "screen_name" : "HannahRitchie02",
      "protected" : false,
      "id_str" : "2666969059",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846843881559937024\/yqcdQhHx_normal.jpg",
      "id" : 2666969059,
      "verified" : false
    }
  },
  "id" : 938745235257085952,
  "created_at" : "2017-12-07 12:21:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Khai",
      "screen_name" : "ThamKhaiMeng",
      "indices" : [ 3, 16 ],
      "id_str" : "478412698",
      "id" : 478412698
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938731600526888960",
  "text" : "RT @ThamKhaiMeng: The more original the idea, the stranger it appears and the more hostility there will be to greet it.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938407954813943813",
    "text" : "The more original the idea, the stranger it appears and the more hostility there will be to greet it.",
    "id" : 938407954813943813,
    "created_at" : "2017-12-06 14:01:05 +0000",
    "user" : {
      "name" : "Khai",
      "screen_name" : "ThamKhaiMeng",
      "protected" : false,
      "id_str" : "478412698",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992098001844822022\/OjTQahi0_normal.jpg",
      "id" : 478412698,
      "verified" : true
    }
  },
  "id" : 938731600526888960,
  "created_at" : "2017-12-07 11:27:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/938718617478352896\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/GZLCUoRv5D",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQb_lf3VoAAw784.jpg",
      "id_str" : "938718592027172864",
      "id" : 938718592027172864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQb_lf3VoAAw784.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GZLCUoRv5D"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938718617478352896",
  "text" : "This cartoon reimagine of the  Profumo scandal make my split out the morning coffee. https:\/\/t.co\/GZLCUoRv5D",
  "id" : 938718617478352896,
  "created_at" : "2017-12-07 10:35:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/nzjaPrne5M",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-42261948",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938717756958048256",
  "text" : "North Korea missile prompted Singapore Air plane re-route  https:\/\/t.co\/nzjaPrne5M",
  "id" : 938717756958048256,
  "created_at" : "2017-12-07 10:32:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 148, 171 ],
      "url" : "https:\/\/t.co\/Tdtbx7DNrX",
      "expanded_url" : "http:\/\/a.msn.com\/00\/en-ie\/BBGjejL?ocid=st",
      "display_url" : "a.msn.com\/00\/en-ie\/BBGje\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938676368631529472",
  "text" : "The Republic of Ireland will grant residency to non-EU nationals who invest at least $595,000 (\u00A3448k) in Irish property, businesses, bonds and more https:\/\/t.co\/Tdtbx7DNrX &lt; I don't have that much $",
  "id" : 938676368631529472,
  "created_at" : "2017-12-07 07:47:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lil zyrtec",
      "screen_name" : "dorseyshaw",
      "indices" : [ 3, 14 ],
      "id_str" : "18380182",
      "id" : 18380182
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/dorseyshaw\/status\/938485896713789441\/video\/1",
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/uUTmyNYBEa",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938485754812141568\/pu\/img\/hqZ7V3CLEbEum9F3.jpg",
      "id_str" : "938485754812141568",
      "id" : 938485754812141568,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938485754812141568\/pu\/img\/hqZ7V3CLEbEum9F3.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uUTmyNYBEa"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938672239054131201",
  "text" : "RT @dorseyshaw: Fixed https:\/\/t.co\/uUTmyNYBEa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dorseyshaw\/status\/938485896713789441\/video\/1",
        "indices" : [ 6, 29 ],
        "url" : "https:\/\/t.co\/uUTmyNYBEa",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938485754812141568\/pu\/img\/hqZ7V3CLEbEum9F3.jpg",
        "id_str" : "938485754812141568",
        "id" : 938485754812141568,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938485754812141568\/pu\/img\/hqZ7V3CLEbEum9F3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uUTmyNYBEa"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938485896713789441",
    "text" : "Fixed https:\/\/t.co\/uUTmyNYBEa",
    "id" : 938485896713789441,
    "created_at" : "2017-12-06 19:10:48 +0000",
    "user" : {
      "name" : "lil zyrtec",
      "screen_name" : "dorseyshaw",
      "protected" : false,
      "id_str" : "18380182",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021721963524501504\/TdqT_z0v_normal.jpg",
      "id" : 18380182,
      "verified" : true
    }
  },
  "id" : 938672239054131201,
  "created_at" : "2017-12-07 07:31:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD835\uDD7F\uD835\uDD8E\uD835\uDD8A\uD835\uDD8B\uD835\uDD91\uD835\uDD8E\uD835\uDD93\uD835\uDD8C",
      "screen_name" : "Mavumavu91",
      "indices" : [ 3, 14 ],
      "id_str" : "2772069148",
      "id" : 2772069148
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/DU69FAxsQ5",
      "expanded_url" : "https:\/\/twitter.com\/futurism\/status\/937701002060419072",
      "display_url" : "twitter.com\/futurism\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938671634818445312",
  "text" : "RT @Mavumavu91: Woah there. Let\u2019s start with black people, Muslims, women and immigrants first. https:\/\/t.co\/DU69FAxsQ5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/DU69FAxsQ5",
        "expanded_url" : "https:\/\/twitter.com\/futurism\/status\/937701002060419072",
        "display_url" : "twitter.com\/futurism\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "937976607087779841",
    "text" : "Woah there. Let\u2019s start with black people, Muslims, women and immigrants first. https:\/\/t.co\/DU69FAxsQ5",
    "id" : 937976607087779841,
    "created_at" : "2017-12-05 09:27:04 +0000",
    "user" : {
      "name" : "\uD835\uDD7F\uD835\uDD8E\uD835\uDD8A\uD835\uDD8B\uD835\uDD91\uD835\uDD8E\uD835\uDD93\uD835\uDD8C",
      "screen_name" : "Mavumavu91",
      "protected" : false,
      "id_str" : "2772069148",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/951032028631269376\/nRgs5lSO_normal.jpg",
      "id" : 2772069148,
      "verified" : false
    }
  },
  "id" : 938671634818445312,
  "created_at" : "2017-12-07 07:28:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "indices" : [ 3, 12 ],
      "id_str" : "875971",
      "id" : 875971
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938671339535298560",
  "text" : "RT @Rubenerd: Marriage equality passed in Australia! Boom! \u263A\uFE0F\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938669898116583424",
    "text" : "Marriage equality passed in Australia! Boom! \u263A\uFE0F\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08",
    "id" : 938669898116583424,
    "created_at" : "2017-12-07 07:21:58 +0000",
    "user" : {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "protected" : false,
      "id_str" : "875971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011136325364301827\/hmrtp1V1_normal.jpg",
      "id" : 875971,
      "verified" : false
    }
  },
  "id" : 938671339535298560,
  "created_at" : "2017-12-07 07:27:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "A. Mutzabaugh CMT",
      "screen_name" : "WLV_investor",
      "indices" : [ 3, 16 ],
      "id_str" : "2294767639",
      "id" : 2294767639
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/WLV_investor\/status\/938410022538682368\/video\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/kJIOQeqsIK",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938409897825247233\/pu\/img\/CXkAdcMXCUIA56b5.jpg",
      "id_str" : "938409897825247233",
      "id" : 938409897825247233,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938409897825247233\/pu\/img\/CXkAdcMXCUIA56b5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/kJIOQeqsIK"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938501044077842432",
  "text" : "RT @WLV_investor: Not the typical morning commute... https:\/\/t.co\/kJIOQeqsIK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WLV_investor\/status\/938410022538682368\/video\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/kJIOQeqsIK",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938409897825247233\/pu\/img\/CXkAdcMXCUIA56b5.jpg",
        "id_str" : "938409897825247233",
        "id" : 938409897825247233,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/938409897825247233\/pu\/img\/CXkAdcMXCUIA56b5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kJIOQeqsIK"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938410022538682368",
    "text" : "Not the typical morning commute... https:\/\/t.co\/kJIOQeqsIK",
    "id" : 938410022538682368,
    "created_at" : "2017-12-06 14:09:18 +0000",
    "user" : {
      "name" : "A. Mutzabaugh CMT",
      "screen_name" : "WLV_investor",
      "protected" : false,
      "id_str" : "2294767639",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964220263918288897\/5zALktvQ_normal.jpg",
      "id" : 2294767639,
      "verified" : false
    }
  },
  "id" : 938501044077842432,
  "created_at" : "2017-12-06 20:11:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "your friend David",
      "screen_name" : "Delahuntagram",
      "indices" : [ 3, 17 ],
      "id_str" : "368321336",
      "id" : 368321336
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Delahuntagram\/status\/938427485645099010\/photo\/1",
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/aZyS2J9UNp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQX20bwXUAE1VNB.jpg",
      "id_str" : "938427478040858625",
      "id" : 938427478040858625,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQX20bwXUAE1VNB.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 717
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 609
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 717
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 717
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/aZyS2J9UNp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938454427186409472",
  "text" : "RT @Delahuntagram: \uD83D\uDE02 https:\/\/t.co\/aZyS2J9UNp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Delahuntagram\/status\/938427485645099010\/photo\/1",
        "indices" : [ 2, 25 ],
        "url" : "https:\/\/t.co\/aZyS2J9UNp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQX20bwXUAE1VNB.jpg",
        "id_str" : "938427478040858625",
        "id" : 938427478040858625,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQX20bwXUAE1VNB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 609
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 717
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aZyS2J9UNp"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938427485645099010",
    "text" : "\uD83D\uDE02 https:\/\/t.co\/aZyS2J9UNp",
    "id" : 938427485645099010,
    "created_at" : "2017-12-06 15:18:42 +0000",
    "user" : {
      "name" : "your friend David",
      "screen_name" : "Delahuntagram",
      "protected" : false,
      "id_str" : "368321336",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/907369296405438464\/EhZJs9aV_normal.jpg",
      "id" : 368321336,
      "verified" : false
    }
  },
  "id" : 938454427186409472,
  "created_at" : "2017-12-06 17:05:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ESRI Dublin",
      "screen_name" : "ESRIDublin",
      "indices" : [ 3, 14 ],
      "id_str" : "52055561",
      "id" : 52055561
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938372361665105921",
  "text" : "RT @ESRIDublin: Recognising passports that are not machine readable or chip-enabled is one of the problems that Irish authorities face in p\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ESRIpublications",
        "indices" : [ 218, 235 ]
      }, {
        "text" : "EMNIreland",
        "indices" : [ 236, 247 ]
      } ],
      "urls" : [ {
        "indices" : [ 194, 217 ],
        "url" : "https:\/\/t.co\/ccB8DSdJgH",
        "expanded_url" : "http:\/\/bit.ly\/2B6nKxy",
        "display_url" : "bit.ly\/2B6nKxy"
      } ]
    },
    "geo" : { },
    "id_str" : "938358607015501825",
    "text" : "Recognising passports that are not machine readable or chip-enabled is one of the problems that Irish authorities face in processing migrants from outside of the EU who wish to live in Ireland. https:\/\/t.co\/ccB8DSdJgH #ESRIpublications #EMNIreland",
    "id" : 938358607015501825,
    "created_at" : "2017-12-06 10:45:00 +0000",
    "user" : {
      "name" : "ESRI Dublin",
      "screen_name" : "ESRIDublin",
      "protected" : false,
      "id_str" : "52055561",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876795050482651136\/Kr_KG8Ts_normal.jpg",
      "id" : 52055561,
      "verified" : true
    }
  },
  "id" : 938372361665105921,
  "created_at" : "2017-12-06 11:39:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/Xm2jwdO11w",
      "expanded_url" : "https:\/\/github.com\/mryap\/composetest",
      "display_url" : "github.com\/mryap\/composet\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "938359657957068800",
  "geo" : { },
  "id_str" : "938371751888805894",
  "in_reply_to_user_id" : 9465632,
  "text" : "The code is at https:\/\/t.co\/Xm2jwdO11w",
  "id" : 938371751888805894,
  "in_reply_to_status_id" : 938359657957068800,
  "created_at" : "2017-12-06 11:37:14 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Century Ireland",
      "screen_name" : "CenturyIRL",
      "indices" : [ 3, 14 ],
      "id_str" : "1364081917",
      "id" : 1364081917
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938360071867772928",
  "text" : "RT @CenturyIRL: In 1917 the Bishop of Achonry identified dancing as one of the great perils of the age: \u2018There is a mixing of the sexes at\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CenturyIRL\/status\/937781055851032577\/photo\/1",
        "indices" : [ 278, 301 ],
        "url" : "https:\/\/t.co\/jdwXCRc4eY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DP8zoMcXUAEoZpt.jpg",
        "id_str" : "936524013144330241",
        "id" : 936524013144330241,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DP8zoMcXUAEoZpt.jpg",
        "sizes" : [ {
          "h" : 710,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 710,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 710,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 483,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jdwXCRc4eY"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 254, 277 ],
        "url" : "https:\/\/t.co\/DTlcT3SkIY",
        "expanded_url" : "http:\/\/www.rte.ie\/centuryireland\/index.php\/articles\/dancing-one-of-the-great-perils-of-our-time",
        "display_url" : "rte.ie\/centuryireland\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "937781055851032577",
    "text" : "In 1917 the Bishop of Achonry identified dancing as one of the great perils of the age: \u2018There is a mixing of the sexes at meetings taking place in some of the young men\u2019s clubs. This is an unmitigated source of danger, and should be sternly inhibited.\u2019 https:\/\/t.co\/DTlcT3SkIY https:\/\/t.co\/jdwXCRc4eY",
    "id" : 937781055851032577,
    "created_at" : "2017-12-04 20:30:01 +0000",
    "user" : {
      "name" : "Century Ireland",
      "screen_name" : "CenturyIRL",
      "protected" : false,
      "id_str" : "1364081917",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/709777569848156161\/pITJXMwn_normal.jpg",
      "id" : 1364081917,
      "verified" : false
    }
  },
  "id" : 938360071867772928,
  "created_at" : "2017-12-06 10:50:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938359657957068800",
  "text" : "Contract with Hosting provider to end soon. Moving to GitHub. Plan to use Docker to archive the content with the WordPress CMS and it multiple components (PHP, MYSQL database)",
  "id" : 938359657957068800,
  "created_at" : "2017-12-06 10:49:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amy Sullivan",
      "screen_name" : "sullivanamy",
      "indices" : [ 3, 15 ],
      "id_str" : "26402448",
      "id" : 26402448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938189655010349056",
  "text" : "RT @sullivanamy: Less than a week after the attacks of 9\/11, when everyone's fears were heightened, George W. Bush showed up at the Islamic\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "937897909936377856",
    "geo" : { },
    "id_str" : "937898722448592897",
    "in_reply_to_user_id" : 26402448,
    "text" : "Less than a week after the attacks of 9\/11, when everyone's fears were heightened, George W. Bush showed up at the Islamic Center of Washington, DC.",
    "id" : 937898722448592897,
    "in_reply_to_status_id" : 937897909936377856,
    "created_at" : "2017-12-05 04:17:35 +0000",
    "in_reply_to_screen_name" : "sullivanamy",
    "in_reply_to_user_id_str" : "26402448",
    "user" : {
      "name" : "Amy Sullivan",
      "screen_name" : "sullivanamy",
      "protected" : false,
      "id_str" : "26402448",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922550387655225344\/UviJl4Zi_normal.jpg",
      "id" : 26402448,
      "verified" : true
    }
  },
  "id" : 938189655010349056,
  "created_at" : "2017-12-05 23:33:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nintendo Legend",
      "screen_name" : "Nintendo_Legend",
      "indices" : [ 3, 19 ],
      "id_str" : "271037929",
      "id" : 271037929
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938189289099276290",
  "text" : "RT @Nintendo_Legend: Retweet if you've ever met someone face-to-face after previously only knowing them from Twitter.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937892553080360960",
    "text" : "Retweet if you've ever met someone face-to-face after previously only knowing them from Twitter.",
    "id" : 937892553080360960,
    "created_at" : "2017-12-05 03:53:04 +0000",
    "user" : {
      "name" : "Eric Bailey",
      "screen_name" : "EricVBailey",
      "protected" : false,
      "id_str" : "117180495",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947712419740246016\/_eFI_oIV_normal.jpg",
      "id" : 117180495,
      "verified" : false
    }
  },
  "id" : 938189289099276290,
  "created_at" : "2017-12-05 23:32:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "KONO Taro",
      "screen_name" : "konotaromp",
      "indices" : [ 3, 14 ],
      "id_str" : "102325824",
      "id" : 102325824
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938174983829934080",
  "text" : "RT @konotaromp: I have appointed Pikachu and Hello Kitty as Ambassador to promote the City of Osaka for the 2025 Expo host city. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/konotaromp\/status\/935879568728121345\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/mCkiCl5T5j",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DPzpgMSVAAA8Ymb.jpg",
        "id_str" : "935879561849405440",
        "id" : 935879561849405440,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DPzpgMSVAAA8Ymb.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1009
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1009
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1009
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mCkiCl5T5j"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/konotaromp\/status\/935879568728121345\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/mCkiCl5T5j",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DPzpgMTVQAUphDd.jpg",
        "id_str" : "935879561853616133",
        "id" : 935879561853616133,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DPzpgMTVQAUphDd.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1009
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1009
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1009
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mCkiCl5T5j"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "935879568728121345",
    "text" : "I have appointed Pikachu and Hello Kitty as Ambassador to promote the City of Osaka for the 2025 Expo host city. https:\/\/t.co\/mCkiCl5T5j",
    "id" : 935879568728121345,
    "created_at" : "2017-11-29 14:34:11 +0000",
    "user" : {
      "name" : "KONO Taro",
      "screen_name" : "konotaromp",
      "protected" : false,
      "id_str" : "102325824",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/924596977970450433\/A-MHBBNk_normal.jpg",
      "id" : 102325824,
      "verified" : true
    }
  },
  "id" : 938174983829934080,
  "created_at" : "2017-12-05 22:35:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/938146312545095682\/photo\/1",
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/eM5jwJscFW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQT3GVZWkAA74ia.jpg",
      "id_str" : "938146310594662400",
      "id" : 938146310594662400,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQT3GVZWkAA74ia.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eM5jwJscFW"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/s7Gm1Q0cFI",
      "expanded_url" : "http:\/\/ift.tt\/2nvLhlh",
      "display_url" : "ift.tt\/2nvLhlh"
    } ]
  },
  "geo" : { },
  "id_str" : "938146312545095682",
  "text" : "Ireland Chinese News article on Ireland's feminine, immigration and awakening of Irish independence. https:\/\/t.co\/s7Gm1Q0cFI https:\/\/t.co\/eM5jwJscFW",
  "id" : 938146312545095682,
  "created_at" : "2017-12-05 20:41:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TRT World",
      "screen_name" : "trtworld",
      "indices" : [ 3, 12 ],
      "id_str" : "3091150576",
      "id" : 3091150576
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/trtworld\/status\/938075501364633600\/video\/1",
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/jHyJtLT8Mn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQR89s6X4AA0cGm.jpg",
      "id_str" : "938011423573184512",
      "id" : 938011423573184512,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQR89s6X4AA0cGm.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jHyJtLT8Mn"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938129262640615424",
  "text" : "RT @trtworld: Why is the EU looking to ban the doner kebab? https:\/\/t.co\/jHyJtLT8Mn",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/trtworld\/status\/938075501364633600\/video\/1",
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/jHyJtLT8Mn",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQR89s6X4AA0cGm.jpg",
        "id_str" : "938011423573184512",
        "id" : 938011423573184512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQR89s6X4AA0cGm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jHyJtLT8Mn"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "938075501364633600",
    "text" : "Why is the EU looking to ban the doner kebab? https:\/\/t.co\/jHyJtLT8Mn",
    "id" : 938075501364633600,
    "created_at" : "2017-12-05 16:00:02 +0000",
    "user" : {
      "name" : "TRT World",
      "screen_name" : "trtworld",
      "protected" : false,
      "id_str" : "3091150576",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/604916087180754945\/1aMHMBIj_normal.png",
      "id" : 3091150576,
      "verified" : true
    }
  },
  "id" : 938129262640615424,
  "created_at" : "2017-12-05 19:33:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oathello",
      "screen_name" : "oathello_law",
      "indices" : [ 0, 13 ],
      "id_str" : "741126669797330944",
      "id" : 741126669797330944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "938124881807495168",
  "geo" : { },
  "id_str" : "938127592489660416",
  "in_reply_to_user_id" : 741126669797330944,
  "text" : "@oathello_law Any plan for the Android platform? Thanks",
  "id" : 938127592489660416,
  "in_reply_to_status_id" : 938124881807495168,
  "created_at" : "2017-12-05 19:27:02 +0000",
  "in_reply_to_screen_name" : "oathello_law",
  "in_reply_to_user_id_str" : "741126669797330944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/Fx9LipkKb5",
      "expanded_url" : "https:\/\/twitter.com\/ScotRuralAction\/status\/938117345842749445",
      "display_url" : "twitter.com\/ScotRuralActio\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938120055170138113",
  "text" : "Thought local going to fill those position left by migrant workers?  No? https:\/\/t.co\/Fx9LipkKb5",
  "id" : 938120055170138113,
  "created_at" : "2017-12-05 18:57:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Drimnagh Castle CBS",
      "screen_name" : "CbsCastle",
      "indices" : [ 0, 10 ],
      "id_str" : "4845849335",
      "id" : 4845849335
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938095504126341122",
  "in_reply_to_user_id" : 4845849335,
  "text" : "@CbsCastle Hi. Is there an ongoing charity drive by your student? One came knocking on my doorstep just now. Thanks",
  "id" : 938095504126341122,
  "created_at" : "2017-12-05 17:19:31 +0000",
  "in_reply_to_screen_name" : "CbsCastle",
  "in_reply_to_user_id_str" : "4845849335",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allison Nau",
      "screen_name" : "AllisonMNau",
      "indices" : [ 3, 15 ],
      "id_str" : "2375231048",
      "id" : 2375231048
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "datascience",
      "indices" : [ 118, 130 ]
    }, {
      "text" : "AI",
      "indices" : [ 131, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938065990717530113",
  "text" : "RT @AllisonMNau: A reminder that if the data underpinning an algorithm is biased, the output is likely to be as well. #datascience #AI http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "datascience",
        "indices" : [ 101, 113 ]
      }, {
        "text" : "AI",
        "indices" : [ 114, 117 ]
      } ],
      "urls" : [ {
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/AOMomNuc6w",
        "expanded_url" : "http:\/\/ow.ly\/aFQs30gYsA0",
        "display_url" : "ow.ly\/aFQs30gYsA0"
      } ]
    },
    "geo" : { },
    "id_str" : "938057913842749440",
    "text" : "A reminder that if the data underpinning an algorithm is biased, the output is likely to be as well. #datascience #AI https:\/\/t.co\/AOMomNuc6w",
    "id" : 938057913842749440,
    "created_at" : "2017-12-05 14:50:09 +0000",
    "user" : {
      "name" : "Allison Nau",
      "screen_name" : "AllisonMNau",
      "protected" : false,
      "id_str" : "2375231048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/735531639024062464\/KMMQGJz9_normal.jpg",
      "id" : 2375231048,
      "verified" : false
    }
  },
  "id" : 938065990717530113,
  "created_at" : "2017-12-05 15:22:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "avgeek",
      "indices" : [ 75, 82 ]
    } ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/4XG4wvPV9Q",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/dutch-air-force-f35-fighters-140436545.html",
      "display_url" : "sg.news.yahoo.com\/dutch-air-forc\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938064705557598208",
  "text" : "Dutch Air Force F35 Fighters Fly Over Death Valley https:\/\/t.co\/4XG4wvPV9Q #avgeek",
  "id" : 938064705557598208,
  "created_at" : "2017-12-05 15:17:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Wang",
      "screen_name" : "jwangARK",
      "indices" : [ 3, 12 ],
      "id_str" : "14597344",
      "id" : 14597344
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938054369639006210",
  "text" : "RT @jwangARK: Netflix ML: every row in the Netflix UI (trending, drama, documentary etc.) uses a separate algorithm. The row order uses an\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jwangARK\/status\/937756792829878272\/photo\/1",
        "indices" : [ 205, 228 ],
        "url" : "https:\/\/t.co\/4bVTyoUoGY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQOU0CZUEAASkK9.jpg",
        "id_str" : "937756769140346880",
        "id" : 937756769140346880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQOU0CZUEAASkK9.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/4bVTyoUoGY"
      } ],
      "hashtags" : [ {
        "text" : "NIPS2017",
        "indices" : [ 195, 204 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937756792829878272",
    "text" : "Netflix ML: every row in the Netflix UI (trending, drama, documentary etc.) uses a separate algorithm. The row order uses an ensemble. 300 person machine learning team. All research done on AWS. #NIPS2017 https:\/\/t.co\/4bVTyoUoGY",
    "id" : 937756792829878272,
    "created_at" : "2017-12-04 18:53:36 +0000",
    "user" : {
      "name" : "James Wang",
      "screen_name" : "jwangARK",
      "protected" : false,
      "id_str" : "14597344",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/814307178852982784\/iLj92QcV_normal.jpg",
      "id" : 14597344,
      "verified" : false
    }
  },
  "id" : 938054369639006210,
  "created_at" : "2017-12-05 14:36:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ann Donnelly",
      "screen_name" : "ann_donnelly",
      "indices" : [ 0, 13 ],
      "id_str" : "17484033",
      "id" : 17484033
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "938020469843906560",
  "geo" : { },
  "id_str" : "938024423990448128",
  "in_reply_to_user_id" : 17484033,
  "text" : "@ann_donnelly I can still remember your first avatar. My fav printing is Nighthawks, 1942",
  "id" : 938024423990448128,
  "in_reply_to_status_id" : 938020469843906560,
  "created_at" : "2017-12-05 12:37:04 +0000",
  "in_reply_to_screen_name" : "ann_donnelly",
  "in_reply_to_user_id_str" : "17484033",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/wAfM9XhijR",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937330656761217025",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "938002590578761729",
  "text" : "Learn not to jump on every incoming message unless is from hiring manager. https:\/\/t.co\/wAfM9XhijR",
  "id" : 938002590578761729,
  "created_at" : "2017-12-05 11:10:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. G\u00F6rkem \u00C7etin",
      "screen_name" : "gorkemcetin",
      "indices" : [ 3, 15 ],
      "id_str" : "14576749",
      "id" : 14576749
    }, {
      "name" : "Microsoft",
      "screen_name" : "Microsoft",
      "indices" : [ 18, 28 ],
      "id_str" : "74286565",
      "id" : 74286565
    }, {
      "name" : "Microsoft Azure",
      "screen_name" : "Azure",
      "indices" : [ 29, 35 ],
      "id_str" : "17000457",
      "id" : 17000457
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/gorkemcetin\/status\/937966151811903488\/photo\/1",
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/OQBrAP3qSi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQRTPOVX0AAf2dK.jpg",
      "id_str" : "937966143410786304",
      "id" : 937966143410786304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQRTPOVX0AAf2dK.jpg",
      "sizes" : [ {
        "h" : 885,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 885,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 502,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 885,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OQBrAP3qSi"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "938000967508865024",
  "text" : "RT @gorkemcetin: .@Microsoft @Azure \uD83D\uDC4B https:\/\/t.co\/OQBrAP3qSi",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Microsoft",
        "screen_name" : "Microsoft",
        "indices" : [ 1, 11 ],
        "id_str" : "74286565",
        "id" : 74286565
      }, {
        "name" : "Microsoft Azure",
        "screen_name" : "Azure",
        "indices" : [ 12, 18 ],
        "id_str" : "17000457",
        "id" : 17000457
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gorkemcetin\/status\/937966151811903488\/photo\/1",
        "indices" : [ 21, 44 ],
        "url" : "https:\/\/t.co\/OQBrAP3qSi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQRTPOVX0AAf2dK.jpg",
        "id_str" : "937966143410786304",
        "id" : 937966143410786304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQRTPOVX0AAf2dK.jpg",
        "sizes" : [ {
          "h" : 885,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 885,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 502,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 885,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/OQBrAP3qSi"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937966151811903488",
    "text" : ".@Microsoft @Azure \uD83D\uDC4B https:\/\/t.co\/OQBrAP3qSi",
    "id" : 937966151811903488,
    "created_at" : "2017-12-05 08:45:31 +0000",
    "user" : {
      "name" : "Dr. G\u00F6rkem \u00C7etin",
      "screen_name" : "gorkemcetin",
      "protected" : false,
      "id_str" : "14576749",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/486025938195333120\/X5qyJRjP_normal.png",
      "id" : 14576749,
      "verified" : false
    }
  },
  "id" : 938000967508865024,
  "created_at" : "2017-12-05 11:03:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "indices" : [ 3, 14 ],
      "id_str" : "2870653293",
      "id" : 2870653293
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937966000573747201",
  "text" : "RT @susie_dent: Words that once wore their heart on their sleeve: a freelancer was a knight-adventurer who used his lance for anyone who pa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937954114159423489",
    "text" : "Words that once wore their heart on their sleeve: a freelancer was a knight-adventurer who used his lance for anyone who paid him; a secretary kept secrets; illustrators added lustre; and a heathen lived on the (\u2018uncivilised\u2019) heath.",
    "id" : 937954114159423489,
    "created_at" : "2017-12-05 07:57:41 +0000",
    "user" : {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "protected" : false,
      "id_str" : "2870653293",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531825590878228480\/4s2F9F6W_normal.jpeg",
      "id" : 2870653293,
      "verified" : true
    }
  },
  "id" : 937966000573747201,
  "created_at" : "2017-12-05 08:44:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Siobhan O'Brien",
      "screen_name" : "Siderophile",
      "indices" : [ 3, 15 ],
      "id_str" : "78962458",
      "id" : 78962458
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Siderophile\/status\/937693947450847232\/photo\/1",
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/4BvtVfkq8M",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQNbf0DXkAEfexH.jpg",
      "id_str" : "937693749530038273",
      "id" : 937693749530038273,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQNbf0DXkAEfexH.jpg",
      "sizes" : [ {
        "h" : 740,
        "resize" : "fit",
        "w" : 426
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 426
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 391
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 426
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4BvtVfkq8M"
    } ],
    "hashtags" : [ {
      "text" : "Brexit",
      "indices" : [ 42, 49 ]
    }, {
      "text" : "IrishBorder",
      "indices" : [ 50, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937818724970713089",
  "text" : "RT @Siderophile: Decisions, decisions.... #Brexit #IrishBorder https:\/\/t.co\/4BvtVfkq8M",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Siderophile\/status\/937693947450847232\/photo\/1",
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/4BvtVfkq8M",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQNbf0DXkAEfexH.jpg",
        "id_str" : "937693749530038273",
        "id" : 937693749530038273,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQNbf0DXkAEfexH.jpg",
        "sizes" : [ {
          "h" : 740,
          "resize" : "fit",
          "w" : 426
        }, {
          "h" : 740,
          "resize" : "fit",
          "w" : 426
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 391
        }, {
          "h" : 740,
          "resize" : "fit",
          "w" : 426
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/4BvtVfkq8M"
      } ],
      "hashtags" : [ {
        "text" : "Brexit",
        "indices" : [ 25, 32 ]
      }, {
        "text" : "IrishBorder",
        "indices" : [ 33, 45 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937693947450847232",
    "text" : "Decisions, decisions.... #Brexit #IrishBorder https:\/\/t.co\/4BvtVfkq8M",
    "id" : 937693947450847232,
    "created_at" : "2017-12-04 14:43:53 +0000",
    "user" : {
      "name" : "Siobhan O'Brien",
      "screen_name" : "Siderophile",
      "protected" : false,
      "id_str" : "78962458",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/906070759839289345\/emPDJa54_normal.jpg",
      "id" : 78962458,
      "verified" : false
    }
  },
  "id" : 937818724970713089,
  "created_at" : "2017-12-04 22:59:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "simon",
      "screen_name" : "simon_jthompson",
      "indices" : [ 3, 19 ],
      "id_str" : "534682665",
      "id" : 534682665
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937782927101956097",
  "text" : "RT @simon_jthompson: Another new one to me at least - Google's started highlighting Stack Overflow answers in the SERP. https:\/\/t.co\/ofOTof\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/simon_jthompson\/status\/937740172577378305\/photo\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/ofOTofQ3K2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQOFnLFW4AELvH7.jpg",
        "id_str" : "937740055459848193",
        "id" : 937740055459848193,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQOFnLFW4AELvH7.jpg",
        "sizes" : [ {
          "h" : 337,
          "resize" : "fit",
          "w" : 669
        }, {
          "h" : 337,
          "resize" : "fit",
          "w" : 669
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 337,
          "resize" : "fit",
          "w" : 669
        }, {
          "h" : 337,
          "resize" : "fit",
          "w" : 669
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ofOTofQ3K2"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937740172577378305",
    "text" : "Another new one to me at least - Google's started highlighting Stack Overflow answers in the SERP. https:\/\/t.co\/ofOTofQ3K2",
    "id" : 937740172577378305,
    "created_at" : "2017-12-04 17:47:34 +0000",
    "user" : {
      "name" : "simon",
      "screen_name" : "simon_jthompson",
      "protected" : false,
      "id_str" : "534682665",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998300749393088512\/RJP271sp_normal.jpg",
      "id" : 534682665,
      "verified" : false
    }
  },
  "id" : 937782927101956097,
  "created_at" : "2017-12-04 20:37:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937767284134891521",
  "text" : "London, Scotland, Wales: I want what the island of Ireland has",
  "id" : 937767284134891521,
  "created_at" : "2017-12-04 19:35:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937644340985331712",
  "text" : "Who\u2019s In charge of user onboarding in your business? Product Manager or Marketer?",
  "id" : 937644340985331712,
  "created_at" : "2017-12-04 11:26:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937634969798758402\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/CsEZ6w4vJ5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQMmB9LWsAASy-n.jpg",
      "id_str" : "937634962467106816",
      "id" : 937634962467106816,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQMmB9LWsAASy-n.jpg",
      "sizes" : [ {
        "h" : 130,
        "resize" : "fit",
        "w" : 860
      }, {
        "h" : 103,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 130,
        "resize" : "crop",
        "w" : 130
      }, {
        "h" : 130,
        "resize" : "fit",
        "w" : 860
      }, {
        "h" : 130,
        "resize" : "fit",
        "w" : 860
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/CsEZ6w4vJ5"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937634969798758402",
  "text" : "Here one way to nudge people to fill in the field. Nobody want to be seen with those skills on their profile. https:\/\/t.co\/CsEZ6w4vJ5",
  "id" : 937634969798758402,
  "created_at" : "2017-12-04 10:49:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937628420783124481",
  "text" : "Is there a way to run Docker image without typing this everytime? \ndocker run -d -v `pwd`\/notebooks:\/notebooks -v `pwd`\/data:\/data \\-v `pwd`\/logs:\/tmp\/tflearn_logs -p 8888:8888 -i docker-ds",
  "id" : 937628420783124481,
  "created_at" : "2017-12-04 10:23:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 12, 23 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/y6umT5Pkkt",
      "expanded_url" : "https:\/\/twitter.com\/louisekiely\/status\/937327016466108421",
      "display_url" : "twitter.com\/louisekiely\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937626928659550211",
  "text" : "I recommend @Clearpreso :) https:\/\/t.co\/y6umT5Pkkt",
  "id" : 937626928659550211,
  "created_at" : "2017-12-04 10:17:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/0lmHjeEGdz",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/740684197141057536",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937626194173923328",
  "text" : "ICYMI: Extracting data tables from pdfs. There is a tool for that.  https:\/\/t.co\/0lmHjeEGdz",
  "id" : 937626194173923328,
  "created_at" : "2017-12-04 10:14:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937613335650033664\/photo\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/KC7htMM7Ej",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQMSTQZWsAAG5cK.jpg",
      "id_str" : "937613269451321344",
      "id" : 937613269451321344,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQMSTQZWsAAG5cK.jpg",
      "sizes" : [ {
        "h" : 362,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 728,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 728,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KC7htMM7Ej"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937613335650033664",
  "text" : "It like watching your food being prepared ... https:\/\/t.co\/KC7htMM7Ej",
  "id" : 937613335650033664,
  "created_at" : "2017-12-04 09:23:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ha Phan",
      "screen_name" : "hpdailyrant",
      "indices" : [ 3, 15 ],
      "id_str" : "265755076",
      "id" : 265755076
    }, {
      "name" : "Jared Spool",
      "screen_name" : "jmspool",
      "indices" : [ 17, 25 ],
      "id_str" : "849101",
      "id" : 849101
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937416909913579520",
  "text" : "RT @hpdailyrant: @jmspool Another thing I never see in portfolios:\n \nTrade offs users are willing to make. For instance, automation vs cont\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jared Spool",
        "screen_name" : "jmspool",
        "indices" : [ 0, 8 ],
        "id_str" : "849101",
        "id" : 849101
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "937366289353383936",
    "in_reply_to_user_id" : 849101,
    "text" : "@jmspool Another thing I never see in portfolios:\n \nTrade offs users are willing to make. For instance, automation vs control, speed vs quality, convenience over control.\n\nThings we discover users want to do but often can\u2019t verbalize.",
    "id" : 937366289353383936,
    "created_at" : "2017-12-03 17:01:53 +0000",
    "in_reply_to_screen_name" : "jmspool",
    "in_reply_to_user_id_str" : "849101",
    "user" : {
      "name" : "Ha Phan",
      "screen_name" : "hpdailyrant",
      "protected" : false,
      "id_str" : "265755076",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/940115119081529344\/7k5cEnei_normal.jpg",
      "id" : 265755076,
      "verified" : false
    }
  },
  "id" : 937416909913579520,
  "created_at" : "2017-12-03 20:23:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/nE1p0BFbLU",
      "expanded_url" : "https:\/\/medium.com\/google-cloud\/gcp-products-described-in-4-words-or-less-f3056550e595",
      "display_url" : "medium.com\/google-cloud\/g\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937385307699367936",
  "text" : "\u201CGoogle Cloud Platform products described in 4 words or less\u201D https:\/\/t.co\/nE1p0BFbLU",
  "id" : 937385307699367936,
  "created_at" : "2017-12-03 18:17:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937330656761217025",
  "text" : "I always quickly response to email, What-apps message, SMS but people do not always return the same courtesy ...",
  "id" : 937330656761217025,
  "created_at" : "2017-12-03 14:40:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/slN4jGI2TQ",
      "expanded_url" : "https:\/\/productiveteams.io\/peter-coppinger-developerceo-role-microconf\/",
      "display_url" : "productiveteams.io\/peter-coppinge\u2026"
    }, {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/us5D57XUGO",
      "expanded_url" : "https:\/\/twitter.com\/Clearpreso\/status\/937327081557544960",
      "display_url" : "twitter.com\/Clearpreso\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937330353739632645",
  "text" : "Here their journey https:\/\/t.co\/slN4jGI2TQ https:\/\/t.co\/us5D57XUGO",
  "id" : 937330353739632645,
  "created_at" : "2017-12-03 14:39:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SiliconRepublic",
      "screen_name" : "siliconrepublic",
      "indices" : [ 102, 118 ],
      "id_str" : "14385329",
      "id" : 14385329
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/5f1OaQRjyV",
      "expanded_url" : "http:\/\/Teamwork.com",
      "display_url" : "Teamwork.com"
    }, {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/XTylUrMEWd",
      "expanded_url" : "https:\/\/www.siliconrepublic.com\/enterprise\/irish-software-company-buys-teamwork-com-domain-for-us675000",
      "display_url" : "siliconrepublic.com\/enterprise\/iri\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937329339082924038",
  "text" : "Irish software company buys https:\/\/t.co\/5f1OaQRjyV domain for US$675,000 https:\/\/t.co\/XTylUrMEWd via @siliconrepublic",
  "id" : 937329339082924038,
  "created_at" : "2017-12-03 14:35:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 0, 11 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "937327081557544960",
  "geo" : { },
  "id_str" : "937327972251553793",
  "in_reply_to_user_id" : 91166653,
  "text" : "@Clearpreso The very tech company being mocked at for paying a hefty sum for the domain name.",
  "id" : 937327972251553793,
  "in_reply_to_status_id" : 937327081557544960,
  "created_at" : "2017-12-03 14:29:37 +0000",
  "in_reply_to_screen_name" : "Clearpreso",
  "in_reply_to_user_id_str" : "91166653",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IIID",
      "screen_name" : "IIIDtweet",
      "indices" : [ 3, 13 ],
      "id_str" : "233604092",
      "id" : 233604092
    }, {
      "name" : "Dave Gray",
      "screen_name" : "davegray",
      "indices" : [ 129, 138 ],
      "id_str" : "456",
      "id" : 456
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "information",
      "indices" : [ 78, 90 ]
    }, {
      "text" : "designer",
      "indices" : [ 91, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/DISthgaSt3",
      "expanded_url" : "https:\/\/buff.ly\/2iBsgMI",
      "display_url" : "buff.ly\/2iBsgMI"
    } ]
  },
  "geo" : { },
  "id_str" : "937318768040136706",
  "text" : "RT @IIIDtweet: Meaningful categorization is one of the key superpowers of the #information #designer https:\/\/t.co\/DISthgaSt3 via @davegray\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dave Gray",
        "screen_name" : "davegray",
        "indices" : [ 114, 123 ],
        "id_str" : "456",
        "id" : 456
      }, {
        "name" : "XPLANE",
        "screen_name" : "xplane",
        "indices" : [ 124, 131 ],
        "id_str" : "8055642",
        "id" : 8055642
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IIIDtweet\/status\/937275464783220741\/photo\/1",
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/yWLBVJIxa5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQHfEUCV4AA12AI.jpg",
        "id_str" : "937275462660775936",
        "id" : 937275462660775936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQHfEUCV4AA12AI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 802
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 802
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 802
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yWLBVJIxa5"
      } ],
      "hashtags" : [ {
        "text" : "information",
        "indices" : [ 63, 75 ]
      }, {
        "text" : "designer",
        "indices" : [ 76, 85 ]
      } ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/DISthgaSt3",
        "expanded_url" : "https:\/\/buff.ly\/2iBsgMI",
        "display_url" : "buff.ly\/2iBsgMI"
      } ]
    },
    "geo" : { },
    "id_str" : "937275464783220741",
    "text" : "Meaningful categorization is one of the key superpowers of the #information #designer https:\/\/t.co\/DISthgaSt3 via @davegray @xplane https:\/\/t.co\/yWLBVJIxa5",
    "id" : 937275464783220741,
    "created_at" : "2017-12-03 11:00:59 +0000",
    "user" : {
      "name" : "IIID",
      "screen_name" : "IIIDtweet",
      "protected" : false,
      "id_str" : "233604092",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841314176073125888\/EV9f1KzZ_normal.jpg",
      "id" : 233604092,
      "verified" : false
    }
  },
  "id" : 937318768040136706,
  "created_at" : "2017-12-03 13:53:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937261162554589184\/photo\/1",
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/gAIYtwzb7T",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQHSDbOW4AEG5bJ.jpg",
      "id_str" : "937261153759191041",
      "id" : 937261153759191041,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQHSDbOW4AEG5bJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 657,
        "resize" : "fit",
        "w" : 1335
      }, {
        "h" : 335,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 591,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 657,
        "resize" : "fit",
        "w" : 1335
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gAIYtwzb7T"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937261162554589184",
  "text" : "No update on Wiki yet https:\/\/t.co\/gAIYtwzb7T",
  "id" : 937261162554589184,
  "created_at" : "2017-12-03 10:04:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bill",
      "screen_name" : "offdutybilll",
      "indices" : [ 0, 13 ],
      "id_str" : "1384135308",
      "id" : 1384135308
    }, {
      "name" : "The Andrew Marr Show",
      "screen_name" : "MarrShow",
      "indices" : [ 14, 23 ],
      "id_str" : "317173156",
      "id" : 317173156
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "937255665302757376",
  "geo" : { },
  "id_str" : "937259167835881473",
  "in_reply_to_user_id" : 1384135308,
  "text" : "@offdutybilll @MarrShow For some reason when I try to pronounce Tanaiste, Google voice shows me \"pond easter or Cornish tea\" \uD83D\uDE01",
  "id" : 937259167835881473,
  "in_reply_to_status_id" : 937255665302757376,
  "created_at" : "2017-12-03 09:56:13 +0000",
  "in_reply_to_screen_name" : "offdutybilll",
  "in_reply_to_user_id_str" : "1384135308",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937253068453838848\/photo\/1",
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/TKG0ffYFsQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQHKspcX4AIPA3Y.jpg",
      "id_str" : "937253065857687554",
      "id" : 937253065857687554,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQHKspcX4AIPA3Y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TKG0ffYFsQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/wd71eAoB3c",
      "expanded_url" : "http:\/\/ift.tt\/2nolDPA",
      "display_url" : "ift.tt\/2nolDPA"
    } ]
  },
  "geo" : { },
  "id_str" : "937253068453838848",
  "text" : "Wrap for breakfast https:\/\/t.co\/wd71eAoB3c https:\/\/t.co\/TKG0ffYFsQ",
  "id" : 937253068453838848,
  "created_at" : "2017-12-03 09:31:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hamid",
      "screen_name" : "hamid",
      "indices" : [ 3, 9 ],
      "id_str" : "19877641",
      "id" : 19877641
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937245552101789696",
  "text" : "RT @hamid: Bob: \u2018hey guys, we\u2019ve got \u00A310,000s left, what should we do?\u2019\n\nMary: \u2018Reach people buying Christmas dinners?\u2019\n\nBob: \u2018Mary, you\u2019re\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 276, 299 ],
        "url" : "https:\/\/t.co\/iY3bld66VL",
        "expanded_url" : "https:\/\/twitter.com\/sainsburys\/status\/936292006074122241",
        "display_url" : "twitter.com\/sainsburys\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "937240313579655168",
    "text" : "Bob: \u2018hey guys, we\u2019ve got \u00A310,000s left, what should we do?\u2019\n\nMary: \u2018Reach people buying Christmas dinners?\u2019\n\nBob: \u2018Mary, you\u2019re fired. Anyone else?\u2019\n\nBill: \u2018How about a Brussels sprouts Snapchat filter? Millennials love that shit.\u2019\n\nBob: \u2018Give this guy a fucking promotion.\u2019 https:\/\/t.co\/iY3bld66VL",
    "id" : 937240313579655168,
    "created_at" : "2017-12-03 08:41:18 +0000",
    "user" : {
      "name" : "Hamid",
      "screen_name" : "hamid",
      "protected" : false,
      "id_str" : "19877641",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/799542450469617666\/siyT6rdE_normal.jpg",
      "id" : 19877641,
      "verified" : false
    }
  },
  "id" : 937245552101789696,
  "created_at" : "2017-12-03 09:02:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/c3Ghmmh2lA",
      "expanded_url" : "https:\/\/www.google.ie\/search?ei=R7AjWqqKJafagAbM846YAQ&q=CBD+stands+for&oq=CBD+stands+for&gs_l=psy-ab.3...211799.214275.0.214590.0.0.0.0.0.0.0.0..0.0....0...1c.1.64.psy-ab..0.0.0....0.7AJPleYZQeU",
      "display_url" : "google.ie\/search?ei=R7Aj\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937232498500751360",
  "text" : "CBD = Central Business District as I just learn that CBD can refer to https:\/\/t.co\/c3Ghmmh2lA",
  "id" : 937232498500751360,
  "created_at" : "2017-12-03 08:10:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/kSqXHFEGXO",
      "expanded_url" : "https:\/\/twitter.com\/bigdata\/status\/937220549289185280",
      "display_url" : "twitter.com\/bigdata\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937231394488627200",
  "text" : "Right in the CBD https:\/\/t.co\/kSqXHFEGXO",
  "id" : 937231394488627200,
  "created_at" : "2017-12-03 08:05:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brickset",
      "screen_name" : "brickset",
      "indices" : [ 3, 12 ],
      "id_str" : "44920228",
      "id" : 44920228
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/PYBUymexUG",
      "expanded_url" : "http:\/\/brickset.com\/article\/32432\/2018-is-the-year-of-the-dog#.WiOtDdD5pbA.twitter",
      "display_url" : "brickset.com\/article\/32432\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "937230960243953664",
  "text" : "RT @brickset: 2018 is the Year of the Dog https:\/\/t.co\/PYBUymexUG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 51 ],
        "url" : "https:\/\/t.co\/PYBUymexUG",
        "expanded_url" : "http:\/\/brickset.com\/article\/32432\/2018-is-the-year-of-the-dog#.WiOtDdD5pbA.twitter",
        "display_url" : "brickset.com\/article\/32432\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "937227871977865216",
    "text" : "2018 is the Year of the Dog https:\/\/t.co\/PYBUymexUG",
    "id" : 937227871977865216,
    "created_at" : "2017-12-03 07:51:52 +0000",
    "user" : {
      "name" : "Brickset",
      "screen_name" : "brickset",
      "protected" : false,
      "id_str" : "44920228",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/763682853343354880\/XmnkMhtU_normal.jpg",
      "id" : 44920228,
      "verified" : false
    }
  },
  "id" : 937230960243953664,
  "created_at" : "2017-12-03 08:04:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/937229561309356032\/photo\/1",
      "indices" : [ 139, 162 ],
      "url" : "https:\/\/t.co\/ypQE64fIZM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQG1ROvWAAEaw5W.jpg",
      "id_str" : "937229505088847873",
      "id" : 937229505088847873,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQG1ROvWAAEaw5W.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 566,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 566,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 566,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 566,
        "resize" : "fit",
        "w" : 550
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ypQE64fIZM"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "937229561309356032",
  "text" : "Came across this on Tumbler. So far only an American (in Singapore) and a Polish (in Ireland) know and learn to say my name not just \"Yap\" https:\/\/t.co\/ypQE64fIZM",
  "id" : 937229561309356032,
  "created_at" : "2017-12-03 07:58:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "indices" : [ 3, 9 ],
      "id_str" : "1344951",
      "id" : 1344951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/AsIpYCUCkP",
      "expanded_url" : "http:\/\/bit.ly\/2AnnAi5",
      "display_url" : "bit.ly\/2AnnAi5"
    } ]
  },
  "geo" : { },
  "id_str" : "937078713010835456",
  "text" : "RT @WIRED: Laser blasters are officially a real thing https:\/\/t.co\/AsIpYCUCkP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 43, 66 ],
        "url" : "https:\/\/t.co\/AsIpYCUCkP",
        "expanded_url" : "http:\/\/bit.ly\/2AnnAi5",
        "display_url" : "bit.ly\/2AnnAi5"
      } ]
    },
    "geo" : { },
    "id_str" : "936462889296318465",
    "text" : "Laser blasters are officially a real thing https:\/\/t.co\/AsIpYCUCkP",
    "id" : 936462889296318465,
    "created_at" : "2017-12-01 05:12:06 +0000",
    "user" : {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "protected" : false,
      "id_str" : "1344951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/615598832726970372\/jsK-gBSt_normal.png",
      "id" : 1344951,
      "verified" : true
    }
  },
  "id" : 937078713010835456,
  "created_at" : "2017-12-02 21:59:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Mayes",
      "screen_name" : "James_Mayes",
      "indices" : [ 3, 15 ],
      "id_str" : "18897720",
      "id" : 18897720
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ux",
      "indices" : [ 41, 44 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936969933073895424",
  "text" : "RT @James_Mayes: I would love for a good #ux person to take some time off from pursuing pixel perfection and go work on a hotel shower that\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ux",
        "indices" : [ 24, 27 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "936886891953102848",
    "text" : "I would love for a good #ux person to take some time off from pursuing pixel perfection and go work on a hotel shower that's actually intuitive. Kthxbai.",
    "id" : 936886891953102848,
    "created_at" : "2017-12-02 09:16:56 +0000",
    "user" : {
      "name" : "James Mayes",
      "screen_name" : "James_Mayes",
      "protected" : false,
      "id_str" : "18897720",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/507891989501587459\/8-idXIz-_normal.png",
      "id" : 18897720,
      "verified" : false
    }
  },
  "id" : 936969933073895424,
  "created_at" : "2017-12-02 14:46:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/936955425085411328\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/TX1Nw7wzjC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQC7_kpX4AAn7bB.jpg",
      "id_str" : "936955423336423424",
      "id" : 936955423336423424,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQC7_kpX4AAn7bB.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TX1Nw7wzjC"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/uCJ9CWIDW7",
      "expanded_url" : "http:\/\/ift.tt\/2iDU7ft",
      "display_url" : "ift.tt\/2iDU7ft"
    } ]
  },
  "geo" : { },
  "id_str" : "936955425085411328",
  "text" : "At the Rathmines Library today https:\/\/t.co\/uCJ9CWIDW7 https:\/\/t.co\/TX1Nw7wzjC",
  "id" : 936955425085411328,
  "created_at" : "2017-12-02 13:49:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/936953312888356870\/photo\/1",
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/BsFY7Tg3ux",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQC6ElFW0AE_0X6.jpg",
      "id_str" : "936953310329884673",
      "id" : 936953310329884673,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQC6ElFW0AE_0X6.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BsFY7Tg3ux"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/TfizrXx8Rm",
      "expanded_url" : "http:\/\/ift.tt\/2AALOb8",
      "display_url" : "ift.tt\/2AALOb8"
    } ]
  },
  "geo" : { },
  "id_str" : "936953312888356870",
  "text" : "Headline in today's newspaper https:\/\/t.co\/TfizrXx8Rm https:\/\/t.co\/BsFY7Tg3ux",
  "id" : 936953312888356870,
  "created_at" : "2017-12-02 13:40:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/936951905544228864\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/gnQfjA8wwj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQC4ytjXcAI0QGo.jpg",
      "id_str" : "936951903853965314",
      "id" : 936951903853965314,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQC4ytjXcAI0QGo.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gnQfjA8wwj"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/n9AqYPXTfh",
      "expanded_url" : "http:\/\/ift.tt\/2i8BzQn",
      "display_url" : "ift.tt\/2i8BzQn"
    } ]
  },
  "geo" : { },
  "id_str" : "936951905544228864",
  "text" : "The Guardian https:\/\/t.co\/n9AqYPXTfh https:\/\/t.co\/gnQfjA8wwj",
  "id" : 936951905544228864,
  "created_at" : "2017-12-02 13:35:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LateLateToyShow",
      "indices" : [ 54, 70 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936872596833894400",
  "text" : "Would love to see the potty mouth Mrs. Brown host the #LateLateToyShow in 2018. I sure I will tune it.",
  "id" : 936872596833894400,
  "created_at" : "2017-12-02 08:20:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936716833750740995",
  "text" : "LLTS is really boring TV show tbh",
  "id" : 936716833750740995,
  "created_at" : "2017-12-01 22:01:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kit de Waal",
      "screen_name" : "KitdeWaal",
      "indices" : [ 3, 13 ],
      "id_str" : "2816947358",
      "id" : 2816947358
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/wp85VaudV1",
      "expanded_url" : "https:\/\/twitter.com\/RyanMStowe\/status\/935697058866098176",
      "display_url" : "twitter.com\/RyanMStowe\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "936706931351261184",
  "text" : "RT @KitdeWaal: This is great.  Just tried it.  RT. https:\/\/t.co\/wp85VaudV1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/wp85VaudV1",
        "expanded_url" : "https:\/\/twitter.com\/RyanMStowe\/status\/935697058866098176",
        "display_url" : "twitter.com\/RyanMStowe\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "936392041814200320",
    "text" : "This is great.  Just tried it.  RT. https:\/\/t.co\/wp85VaudV1",
    "id" : 936392041814200320,
    "created_at" : "2017-12-01 00:30:34 +0000",
    "user" : {
      "name" : "Kit de Waal",
      "screen_name" : "KitdeWaal",
      "protected" : false,
      "id_str" : "2816947358",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/790340528902725632\/Q-Xbj1wd_normal.jpg",
      "id" : 2816947358,
      "verified" : false
    }
  },
  "id" : 936706931351261184,
  "created_at" : "2017-12-01 21:21:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Linus Lee",
      "screen_name" : "linuslee",
      "indices" : [ 3, 12 ],
      "id_str" : "19479874",
      "id" : 19479874
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/0AKbO7eILv",
      "expanded_url" : "https:\/\/twitter.com\/monkshillvc\/status\/936464993389703168",
      "display_url" : "twitter.com\/monkshillvc\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "936706643361980417",
  "text" : "RT @linuslee: I wrote a blog post on building data science teams for your startup. please check it out! https:\/\/t.co\/0AKbO7eILv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/0AKbO7eILv",
        "expanded_url" : "https:\/\/twitter.com\/monkshillvc\/status\/936464993389703168",
        "display_url" : "twitter.com\/monkshillvc\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "936504656506654721",
    "text" : "I wrote a blog post on building data science teams for your startup. please check it out! https:\/\/t.co\/0AKbO7eILv",
    "id" : 936504656506654721,
    "created_at" : "2017-12-01 07:58:04 +0000",
    "user" : {
      "name" : "Linus Lee",
      "screen_name" : "linuslee",
      "protected" : false,
      "id_str" : "19479874",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1851497890\/dp_normal.jpg",
      "id" : 19479874,
      "verified" : false
    }
  },
  "id" : 936706643361980417,
  "created_at" : "2017-12-01 21:20:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936674229495435265",
  "text" : "It always the case when you pose a question, you somehow manage to comes up with a solution 2 to 3 hours later to the question.",
  "id" : 936674229495435265,
  "created_at" : "2017-12-01 19:11:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Lewis",
      "screen_name" : "repjohnlewis",
      "indices" : [ 3, 16 ],
      "id_str" : "29450962",
      "id" : 29450962
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936661729840427011",
  "text" : "RT @repjohnlewis: 62 years ago today, Rosa Parks stood up for what is right, what is fair, and what is just, by sitting down on a Montgomer\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/repjohnlewis\/status\/936612752289869824\/photo\/1",
        "indices" : [ 259, 282 ],
        "url" : "https:\/\/t.co\/iMHemdVzzB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DP-EIPoW0AIT49A.jpg",
        "id_str" : "936612524685971458",
        "id" : 936612524685971458,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DP-EIPoW0AIT49A.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1588,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 514
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 907
        }, {
          "h" : 1588,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iMHemdVzzB"
      } ],
      "hashtags" : [ {
        "text" : "goodtrouble",
        "indices" : [ 246, 258 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "936612752289869824",
    "text" : "62 years ago today, Rosa Parks stood up for what is right, what is fair, and what is just, by sitting down on a Montgomery city bus and refusing to give up her seat to a white passenger. Her actions took raw courage and helped ignite a movement. #goodtrouble https:\/\/t.co\/iMHemdVzzB",
    "id" : 936612752289869824,
    "created_at" : "2017-12-01 15:07:36 +0000",
    "user" : {
      "name" : "John Lewis",
      "screen_name" : "repjohnlewis",
      "protected" : false,
      "id_str" : "29450962",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/751446634429575169\/KWfWsq0W_normal.jpg",
      "id" : 29450962,
      "verified" : true
    }
  },
  "id" : 936661729840427011,
  "created_at" : "2017-12-01 18:22:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936616757103595520",
  "text" : "Looking back, have you ever feel bad saying no to your child when they are young?",
  "id" : 936616757103595520,
  "created_at" : "2017-12-01 15:23:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    }, {
      "name" : "Sky Ireland",
      "screen_name" : "SkyIreland",
      "indices" : [ 66, 77 ],
      "id_str" : "793386271",
      "id" : 793386271
    }, {
      "name" : "Vodafone Ireland",
      "screen_name" : "VodafoneIreland",
      "indices" : [ 79, 95 ],
      "id_str" : "15634642",
      "id" : 15634642
    }, {
      "name" : "Flogas",
      "screen_name" : "FlogasIreland",
      "indices" : [ 97, 111 ],
      "id_str" : "701185423",
      "id" : 701185423
    }, {
      "name" : "PINERGY",
      "screen_name" : "PINERGY",
      "indices" : [ 113, 121 ],
      "id_str" : "1412527058",
      "id" : 1412527058
    }, {
      "name" : "PrePayPower",
      "screen_name" : "PrePay_tweets",
      "indices" : [ 123, 137 ],
      "id_str" : "828994262566047744",
      "id" : 828994262566047744
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "936576405491601409",
  "text" : "RT @Dotnetster: Happy National Price Hikes Day. Brought to you by @SkyIreland, @VodafoneIreland, @FlogasIreland, @PINERGY, @PrePay_tweets a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Sky Ireland",
        "screen_name" : "SkyIreland",
        "indices" : [ 50, 61 ],
        "id_str" : "793386271",
        "id" : 793386271
      }, {
        "name" : "Vodafone Ireland",
        "screen_name" : "VodafoneIreland",
        "indices" : [ 63, 79 ],
        "id_str" : "15634642",
        "id" : 15634642
      }, {
        "name" : "Flogas",
        "screen_name" : "FlogasIreland",
        "indices" : [ 81, 95 ],
        "id_str" : "701185423",
        "id" : 701185423
      }, {
        "name" : "PINERGY",
        "screen_name" : "PINERGY",
        "indices" : [ 97, 105 ],
        "id_str" : "1412527058",
        "id" : 1412527058
      }, {
        "name" : "PrePayPower",
        "screen_name" : "PrePay_tweets",
        "indices" : [ 107, 121 ],
        "id_str" : "828994262566047744",
        "id" : 828994262566047744
      }, {
        "name" : "Dublin Bus",
        "screen_name" : "dublinbusnews",
        "indices" : [ 126, 140 ],
        "id_str" : "80549724",
        "id" : 80549724
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "pricehikeday",
        "indices" : [ 141, 154 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "936539548628213762",
    "text" : "Happy National Price Hikes Day. Brought to you by @SkyIreland, @VodafoneIreland, @FlogasIreland, @PINERGY, @PrePay_tweets and @dublinbusnews #pricehikeday",
    "id" : 936539548628213762,
    "created_at" : "2017-12-01 10:16:43 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 936576405491601409,
  "created_at" : "2017-12-01 12:43:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cole Knaflic",
      "screen_name" : "storywithdata",
      "indices" : [ 0, 14 ],
      "id_str" : "404895983",
      "id" : 404895983
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/fyl5DB2zRx",
      "expanded_url" : "https:\/\/github.com\/mryap\/src\/blob\/master\/un-comtrade\/Ireland-2015-5Month-Milk.ipynb",
      "display_url" : "github.com\/mryap\/src\/blob\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "934829580141187084",
  "geo" : { },
  "id_str" : "936532042342379525",
  "in_reply_to_user_id" : 9465632,
  "text" : "@storywithdata I put together on how to access the data via API or download https:\/\/t.co\/fyl5DB2zRx",
  "id" : 936532042342379525,
  "in_reply_to_status_id" : 934829580141187084,
  "created_at" : "2017-12-01 09:46:53 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/sCLTOXBX6m",
      "expanded_url" : "https:\/\/twitter.com\/skydavidblevins\/status\/936394422102740993",
      "display_url" : "twitter.com\/skydavidblevin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "936525668363526144",
  "text" : "When TV licence inspector comes knocking on the door, you can just move the TV either way. https:\/\/t.co\/sCLTOXBX6m",
  "id" : 936525668363526144,
  "created_at" : "2017-12-01 09:21:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "eirspiders",
      "indices" : [ 130, 141 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "936370536455827458",
  "geo" : { },
  "id_str" : "936493023126589440",
  "in_reply_to_user_id" : 9465632,
  "text" : "Or do the contenders show evidence how the use of data and analytics leads to actions being taken that results in better outcome? #eirspiders",
  "id" : 936493023126589440,
  "in_reply_to_status_id" : 936370536455827458,
  "created_at" : "2017-12-01 07:11:50 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/v1goYzfNlZ",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/936448249484869632",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "936492512423940096",
  "text" : "This is mental.... https:\/\/t.co\/v1goYzfNlZ",
  "id" : 936492512423940096,
  "created_at" : "2017-12-01 07:09:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]