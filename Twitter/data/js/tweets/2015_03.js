Grailbird.data.tweets_2015_03 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "583035403403444225",
  "text" : "I never linger around when I am a guest. After half ten, I let the host know I need to go and off I went. Esp tomorrow is a work day.",
  "id" : 583035403403444225,
  "created_at" : "2015-03-31 22:37:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Co.Design",
      "screen_name" : "FastCoDesign",
      "indices" : [ 3, 16 ],
      "id_str" : "158865339",
      "id" : 158865339
    }, {
      "name" : "Sophie Weiner",
      "screen_name" : "sophcw",
      "indices" : [ 117, 124 ],
      "id_str" : "19629038",
      "id" : 19629038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/rsZUlAzhqS",
      "expanded_url" : "http:\/\/f-st.co\/80U3ixC",
      "display_url" : "f-st.co\/80U3ixC"
    } ]
  },
  "geo" : { },
  "id_str" : "583034536310734848",
  "text" : "RT @FastCoDesign: A designer from Singapore fixes the problem of Keurig's wasteful K-Cups: http:\/\/t.co\/rsZUlAzhqS by @sophcw http:\/\/t.co\/Xb\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Sophie Weiner",
        "screen_name" : "sophcw",
        "indices" : [ 99, 106 ],
        "id_str" : "19629038",
        "id" : 19629038
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FastCoDesign\/status\/582924365064458240\/photo\/1",
        "indices" : [ 107, 129 ],
        "url" : "http:\/\/t.co\/Xbcap1V3nj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb2nxYW8AAopuA.jpg",
        "id_str" : "582924364921892864",
        "id" : 582924364921892864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb2nxYW8AAopuA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1920
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Xbcap1V3nj"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/rsZUlAzhqS",
        "expanded_url" : "http:\/\/f-st.co\/80U3ixC",
        "display_url" : "f-st.co\/80U3ixC"
      } ]
    },
    "geo" : { },
    "id_str" : "582924365064458240",
    "text" : "A designer from Singapore fixes the problem of Keurig's wasteful K-Cups: http:\/\/t.co\/rsZUlAzhqS by @sophcw http:\/\/t.co\/Xbcap1V3nj",
    "id" : 582924365064458240,
    "created_at" : "2015-03-31 15:16:13 +0000",
    "user" : {
      "name" : "Co.Design",
      "screen_name" : "FastCoDesign",
      "protected" : false,
      "id_str" : "158865339",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875778269324361729\/LKjZbw_s_normal.jpg",
      "id" : 158865339,
      "verified" : true
    }
  },
  "id" : 583034536310734848,
  "created_at" : "2015-03-31 22:34:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "583032456019865601",
  "text" : "Guests took half hours to leave the house...I am in my PJ already.",
  "id" : 583032456019865601,
  "created_at" : "2015-03-31 22:25:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 3, 13 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LordoftheDance",
      "indices" : [ 25, 40 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "583031774520979456",
  "text" : "RT @starduest: Review of #LordoftheDance: Dangerous Games - superb dancing eclipsed by lurid backdrop, horrid singing, poor pacing\u2026 http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LordoftheDance",
        "indices" : [ 10, 25 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/NPJjTN4Guq",
        "expanded_url" : "http:\/\/wp.me\/p2wxDH-fu",
        "display_url" : "wp.me\/p2wxDH-fu"
      } ]
    },
    "geo" : { },
    "id_str" : "583025028351983617",
    "text" : "Review of #LordoftheDance: Dangerous Games - superb dancing eclipsed by lurid backdrop, horrid singing, poor pacing\u2026 http:\/\/t.co\/NPJjTN4Guq",
    "id" : 583025028351983617,
    "created_at" : "2015-03-31 21:56:13 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 583031774520979456,
  "created_at" : "2015-03-31 22:23:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    }, {
      "name" : "3XE Digital",
      "screen_name" : "3XEDIGITAL",
      "indices" : [ 21, 32 ],
      "id_str" : "2902470533",
      "id" : 2902470533
    }, {
      "name" : "MargaretEWard, MAJ",
      "screen_name" : "MargaretEWard",
      "indices" : [ 87, 101 ],
      "id_str" : "17868868",
      "id" : 17868868
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "women",
      "indices" : [ 117, 123 ]
    } ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http:\/\/t.co\/93cDCBouBk",
      "expanded_url" : "http:\/\/buff.ly\/1CHNpFX",
      "display_url" : "buff.ly\/1CHNpFX"
    } ]
  },
  "geo" : { },
  "id_str" : "583024245799800832",
  "text" : "RT @DigiWomenIRL: RT @3xedigital: http:\/\/t.co\/93cDCBouBk\n\u201CListeners prefer men\u201D - what @MargaretEWard was told about #women on air http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "3XE Digital",
        "screen_name" : "3XEDIGITAL",
        "indices" : [ 3, 14 ],
        "id_str" : "2902470533",
        "id" : 2902470533
      }, {
        "name" : "MargaretEWard, MAJ",
        "screen_name" : "MargaretEWard",
        "indices" : [ 69, 83 ],
        "id_str" : "17868868",
        "id" : 17868868
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/3xedigital\/status\/582930188037701633\/photo\/1",
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/nmPpetEhNn",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb76tnWsAARW_2.png",
        "id_str" : "582930187886702592",
        "id" : 582930187886702592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb76tnWsAARW_2.png",
        "sizes" : [ {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nmPpetEhNn"
      } ],
      "hashtags" : [ {
        "text" : "women",
        "indices" : [ 99, 105 ]
      } ],
      "urls" : [ {
        "indices" : [ 16, 38 ],
        "url" : "http:\/\/t.co\/93cDCBouBk",
        "expanded_url" : "http:\/\/buff.ly\/1CHNpFX",
        "display_url" : "buff.ly\/1CHNpFX"
      } ]
    },
    "in_reply_to_status_id_str" : "582930188037701633",
    "geo" : { },
    "id_str" : "583020471165292544",
    "in_reply_to_user_id" : 2902470533,
    "text" : "RT @3xedigital: http:\/\/t.co\/93cDCBouBk\n\u201CListeners prefer men\u201D - what @MargaretEWard was told about #women on air http:\/\/t.co\/nmPpetEhNn",
    "id" : 583020471165292544,
    "in_reply_to_status_id" : 582930188037701633,
    "created_at" : "2015-03-31 21:38:06 +0000",
    "in_reply_to_screen_name" : "3XEDIGITAL",
    "in_reply_to_user_id_str" : "2902470533",
    "user" : {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "protected" : false,
      "id_str" : "1221157964",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527003323623145473\/C2yyI2ob_normal.png",
      "id" : 1221157964,
      "verified" : false
    }
  },
  "id" : 583024245799800832,
  "created_at" : "2015-03-31 21:53:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582956148208377856",
  "text" : "What I fear is complacency. When things always become better, people tend to want more for less work. - Lee Kuan Yew",
  "id" : 582956148208377856,
  "created_at" : "2015-03-31 17:22:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 99 ],
      "url" : "http:\/\/t.co\/pwbMM7PRe8",
      "expanded_url" : "http:\/\/googledrive.blogspot.com\/2015\/03\/photosindrive.html?m=1",
      "display_url" : "googledrive.blogspot.com\/2015\/03\/photos\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582952480197230592",
  "text" : "RT @pdscott: Looks like Google Photos is moving from Google+ to Google Drive http:\/\/t.co\/pwbMM7PRe8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http:\/\/t.co\/pwbMM7PRe8",
        "expanded_url" : "http:\/\/googledrive.blogspot.com\/2015\/03\/photosindrive.html?m=1",
        "display_url" : "googledrive.blogspot.com\/2015\/03\/photos\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "582952381035487233",
    "text" : "Looks like Google Photos is moving from Google+ to Google Drive http:\/\/t.co\/pwbMM7PRe8",
    "id" : 582952381035487233,
    "created_at" : "2015-03-31 17:07:33 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 582952480197230592,
  "created_at" : "2015-03-31 17:07:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Xinguozhi",
      "screen_name" : "xinguozhi",
      "indices" : [ 3, 13 ],
      "id_str" : "286215837",
      "id" : 286215837
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 52 ],
      "url" : "http:\/\/t.co\/w1UyJ3EmpQ",
      "expanded_url" : "http:\/\/wp.me\/p1tvmv-44m",
      "display_url" : "wp.me\/p1tvmv-44m"
    } ]
  },
  "geo" : { },
  "id_str" : "582950775552417792",
  "text" : "RT @xinguozhi: \u674E\u5149\u8000\uFF1A\u897F\u65B9\u8BA4\u8BC6\u4E2D\u56FD\u7684\u9886\u8DEF\u4EBA http:\/\/t.co\/w1UyJ3EmpQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 15, 37 ],
        "url" : "http:\/\/t.co\/w1UyJ3EmpQ",
        "expanded_url" : "http:\/\/wp.me\/p1tvmv-44m",
        "display_url" : "wp.me\/p1tvmv-44m"
      } ]
    },
    "geo" : { },
    "id_str" : "579819732544593921",
    "text" : "\u674E\u5149\u8000\uFF1A\u897F\u65B9\u8BA4\u8BC6\u4E2D\u56FD\u7684\u9886\u8DEF\u4EBA http:\/\/t.co\/w1UyJ3EmpQ",
    "id" : 579819732544593921,
    "created_at" : "2015-03-23 01:39:31 +0000",
    "user" : {
      "name" : "Xinguozhi",
      "screen_name" : "xinguozhi",
      "protected" : false,
      "id_str" : "286215837",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1993577268\/xinguozhi200px_normal.png",
      "id" : 286215837,
      "verified" : false
    }
  },
  "id" : 582950775552417792,
  "created_at" : "2015-03-31 17:01:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rebecca Lynn",
      "screen_name" : "VCRebecca",
      "indices" : [ 3, 13 ],
      "id_str" : "144400453",
      "id" : 144400453
    }, {
      "name" : "Joan C. Williams",
      "screen_name" : "JoanCWilliams",
      "indices" : [ 60, 74 ],
      "id_str" : "26368360",
      "id" : 26368360
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/VCRebecca\/status\/582658137099911168\/photo\/1",
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/EEiWDx0Rsv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBYEfRJU0AAv9jq.jpg",
      "id_str" : "582658137016029184",
      "id" : 582658137016029184,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBYEfRJU0AAv9jq.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 303,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 535,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 535,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 535,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EEiWDx0Rsv"
    } ],
    "hashtags" : [ {
      "text" : "Women",
      "indices" : [ 36, 42 ]
    }, {
      "text" : "STEM",
      "indices" : [ 50, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 97 ],
      "url" : "http:\/\/t.co\/WB8fa5GEiZ",
      "expanded_url" : "http:\/\/bit.ly\/1xsLLrq",
      "display_url" : "bit.ly\/1xsLLrq"
    } ]
  },
  "geo" : { },
  "id_str" : "582946328914882560",
  "text" : "RT @VCRebecca: The 5 Biases Pushing #Women Out of #STEM via @JoanCWilliams http:\/\/t.co\/WB8fa5GEiZ http:\/\/t.co\/EEiWDx0Rsv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/6builder.com\" rel=\"nofollow\"\u003E6Builder\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Joan C. Williams",
        "screen_name" : "JoanCWilliams",
        "indices" : [ 45, 59 ],
        "id_str" : "26368360",
        "id" : 26368360
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/VCRebecca\/status\/582658137099911168\/photo\/1",
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/EEiWDx0Rsv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBYEfRJU0AAv9jq.jpg",
        "id_str" : "582658137016029184",
        "id" : 582658137016029184,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBYEfRJU0AAv9jq.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 303,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 535,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 535,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 535,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EEiWDx0Rsv"
      } ],
      "hashtags" : [ {
        "text" : "Women",
        "indices" : [ 21, 27 ]
      }, {
        "text" : "STEM",
        "indices" : [ 35, 40 ]
      } ],
      "urls" : [ {
        "indices" : [ 60, 82 ],
        "url" : "http:\/\/t.co\/WB8fa5GEiZ",
        "expanded_url" : "http:\/\/bit.ly\/1xsLLrq",
        "display_url" : "bit.ly\/1xsLLrq"
      } ]
    },
    "geo" : { },
    "id_str" : "582658137099911168",
    "text" : "The 5 Biases Pushing #Women Out of #STEM via @JoanCWilliams http:\/\/t.co\/WB8fa5GEiZ http:\/\/t.co\/EEiWDx0Rsv",
    "id" : 582658137099911168,
    "created_at" : "2015-03-30 21:38:19 +0000",
    "user" : {
      "name" : "Rebecca Lynn",
      "screen_name" : "VCRebecca",
      "protected" : false,
      "id_str" : "144400453",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/581686231110590465\/bPrcWPHT_normal.jpg",
      "id" : 144400453,
      "verified" : false
    }
  },
  "id" : 582946328914882560,
  "created_at" : "2015-03-31 16:43:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http:\/\/t.co\/5h3kpNdFwI",
      "expanded_url" : "http:\/\/www.adworldexperience.it\/en\/optimizing-campaigns\/measure-your-adwords-account-optimization-with-3-kpi\/",
      "display_url" : "adworldexperience.it\/en\/optimizing-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582945630152192000",
  "text" : "Measure your AdWords account optimization looking at 3 KPIs http:\/\/t.co\/5h3kpNdFwI",
  "id" : 582945630152192000,
  "created_at" : "2015-03-31 16:40:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conns Cameras",
      "screen_name" : "ConnsCameras",
      "indices" : [ 3, 16 ],
      "id_str" : "75572913",
      "id" : 75572913
    }, {
      "name" : "Canon Ireland",
      "screen_name" : "CanonIreland",
      "indices" : [ 45, 58 ],
      "id_str" : "2422129934",
      "id" : 2422129934
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PortraitWeek",
      "indices" : [ 31, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/RX49DDhz93",
      "expanded_url" : "http:\/\/conns.ie\/NPW2015",
      "display_url" : "conns.ie\/NPW2015"
    } ]
  },
  "geo" : { },
  "id_str" : "582943301071986690",
  "text" : "RT @ConnsCameras: To celebrate #PortraitWeek @CanonIreland are giving away this Portrait Photography guide! http:\/\/t.co\/RX49DDhz93 http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Canon Ireland",
        "screen_name" : "CanonIreland",
        "indices" : [ 27, 40 ],
        "id_str" : "2422129934",
        "id" : 2422129934
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ConnsCameras\/status\/582516265404751872\/photo\/1",
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/0KQjEgXmAX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBWDdLwWgAATX0p.png",
        "id_str" : "582516264209383424",
        "id" : 582516264209383424,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBWDdLwWgAATX0p.png",
        "sizes" : [ {
          "h" : 600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0KQjEgXmAX"
      } ],
      "hashtags" : [ {
        "text" : "PortraitWeek",
        "indices" : [ 13, 26 ]
      } ],
      "urls" : [ {
        "indices" : [ 90, 112 ],
        "url" : "http:\/\/t.co\/RX49DDhz93",
        "expanded_url" : "http:\/\/conns.ie\/NPW2015",
        "display_url" : "conns.ie\/NPW2015"
      } ]
    },
    "geo" : { },
    "id_str" : "582516265404751872",
    "text" : "To celebrate #PortraitWeek @CanonIreland are giving away this Portrait Photography guide! http:\/\/t.co\/RX49DDhz93 http:\/\/t.co\/0KQjEgXmAX",
    "id" : 582516265404751872,
    "created_at" : "2015-03-30 12:14:34 +0000",
    "user" : {
      "name" : "Conns Cameras",
      "screen_name" : "ConnsCameras",
      "protected" : false,
      "id_str" : "75572913",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/727449867199610880\/m0cH6eSm_normal.jpg",
      "id" : 75572913,
      "verified" : false
    }
  },
  "id" : 582943301071986690,
  "created_at" : "2015-03-31 16:31:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 3, 15 ],
      "id_str" : "41085467",
      "id" : 41085467
    }, {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 116, 132 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/doqJy6jE0z",
      "expanded_url" : "http:\/\/tdy.sg\/1NDV8by",
      "display_url" : "tdy.sg\/1NDV8by"
    } ]
  },
  "geo" : { },
  "id_str" : "582942507098599424",
  "text" : "RT @TODAYonline: S\u2019pore team to return after firefighting efforts in Thailand: Dr Ng Eng Hen http:\/\/t.co\/doqJy6jE0z @ChannelNewsAsia http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Channel NewsAsia",
        "screen_name" : "ChannelNewsAsia",
        "indices" : [ 99, 115 ],
        "id_str" : "38400130",
        "id" : 38400130
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/582927312078827521\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/sTGbhPDELz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb5PxNWoAAqFZ-.jpg",
        "id_str" : "582927251093757952",
        "id" : 582927251093757952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb5PxNWoAAqFZ-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 371,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 818,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 818,
          "resize" : "fit",
          "w" : 1500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sTGbhPDELz"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/582927312078827521\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/sTGbhPDELz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb5QCuWgAASZGn.jpg",
        "id_str" : "582927255795564544",
        "id" : 582927255795564544,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb5QCuWgAASZGn.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 976,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 442,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 781,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 976,
          "resize" : "fit",
          "w" : 1500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sTGbhPDELz"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/582927312078827521\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/sTGbhPDELz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb5QqHWsAAe6Kg.jpg",
        "id_str" : "582927266369417216",
        "id" : 582927266369417216,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb5QqHWsAAe6Kg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 867,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 867,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 694,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 393,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sTGbhPDELz"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 98 ],
        "url" : "http:\/\/t.co\/doqJy6jE0z",
        "expanded_url" : "http:\/\/tdy.sg\/1NDV8by",
        "display_url" : "tdy.sg\/1NDV8by"
      } ]
    },
    "geo" : { },
    "id_str" : "582927312078827521",
    "text" : "S\u2019pore team to return after firefighting efforts in Thailand: Dr Ng Eng Hen http:\/\/t.co\/doqJy6jE0z @ChannelNewsAsia http:\/\/t.co\/sTGbhPDELz",
    "id" : 582927312078827521,
    "created_at" : "2015-03-31 15:27:56 +0000",
    "user" : {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "protected" : false,
      "id_str" : "41085467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/606854638801846272\/pi_RUbVK_normal.png",
      "id" : 41085467,
      "verified" : true
    }
  },
  "id" : 582942507098599424,
  "created_at" : "2015-03-31 16:28:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Startup Life",
      "screen_name" : "becausestartup",
      "indices" : [ 3, 18 ],
      "id_str" : "2795422567",
      "id" : 2795422567
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "lawmate",
      "indices" : [ 43, 51 ]
    }, {
      "text" : "entrepreneurial",
      "indices" : [ 80, 96 ]
    }, {
      "text" : "entrepreneur",
      "indices" : [ 97, 110 ]
    }, {
      "text" : "smartlegalservices",
      "indices" : [ 111, 130 ]
    }, {
      "text" : "n",
      "indices" : [ 131, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 42 ],
      "url" : "http:\/\/t.co\/nGtwREzfVK",
      "expanded_url" : "http:\/\/ift.tt\/19xpQ7c",
      "display_url" : "ift.tt\/19xpQ7c"
    } ]
  },
  "geo" : { },
  "id_str" : "582942409732026368",
  "text" : "RT @becausestartup: http:\/\/t.co\/nGtwREzfVK #lawmate.co Singapore startup market #entrepreneurial #entrepreneur #smartlegalservices #n\u2026 http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/becausestartup\/status\/582927893250109442\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/fpXtQEvKvM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb51JNW8AAoxId.jpg",
        "id_str" : "582927893191389184",
        "id" : 582927893191389184,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb51JNW8AAoxId.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fpXtQEvKvM"
      } ],
      "hashtags" : [ {
        "text" : "lawmate",
        "indices" : [ 23, 31 ]
      }, {
        "text" : "entrepreneurial",
        "indices" : [ 60, 76 ]
      }, {
        "text" : "entrepreneur",
        "indices" : [ 77, 90 ]
      }, {
        "text" : "smartlegalservices",
        "indices" : [ 91, 110 ]
      }, {
        "text" : "n",
        "indices" : [ 111, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 0, 22 ],
        "url" : "http:\/\/t.co\/nGtwREzfVK",
        "expanded_url" : "http:\/\/ift.tt\/19xpQ7c",
        "display_url" : "ift.tt\/19xpQ7c"
      } ]
    },
    "geo" : { },
    "id_str" : "582927893250109442",
    "text" : "http:\/\/t.co\/nGtwREzfVK #lawmate.co Singapore startup market #entrepreneurial #entrepreneur #smartlegalservices #n\u2026 http:\/\/t.co\/fpXtQEvKvM",
    "id" : 582927893250109442,
    "created_at" : "2015-03-31 15:30:14 +0000",
    "user" : {
      "name" : "Startup Life",
      "screen_name" : "becausestartup",
      "protected" : false,
      "id_str" : "2795422567",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/508504014434992128\/nKOPvk2C_normal.jpeg",
      "id" : 2795422567,
      "verified" : false
    }
  },
  "id" : 582942409732026368,
  "created_at" : "2015-03-31 16:27:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Atlas Obscura",
      "screen_name" : "atlasobscura",
      "indices" : [ 3, 16 ],
      "id_str" : "57047586",
      "id" : 57047586
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/atlasobscura\/status\/582938429542961154\/photo\/1",
      "indices" : [ 116, 138 ],
      "url" : "http:\/\/t.co\/t1dKZYc65X",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBbcmiCUMAAgtxA.jpg",
      "id_str" : "582895756320714752",
      "id" : 582895756320714752,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBbcmiCUMAAgtxA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 532,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 532,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 452,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 532,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/t1dKZYc65X"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/X2vGDcFBuk",
      "expanded_url" : "http:\/\/bit.ly\/1Ii987o",
      "display_url" : "bit.ly\/1Ii987o"
    } ]
  },
  "geo" : { },
  "id_str" : "582941012936552448",
  "text" : "RT @atlasobscura: These massive Slovenian caves are known as the \"Underground Grand Canyon\": http:\/\/t.co\/X2vGDcFBuk http:\/\/t.co\/t1dKZYc65X",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/atlasobscura\/status\/582938429542961154\/photo\/1",
        "indices" : [ 98, 120 ],
        "url" : "http:\/\/t.co\/t1dKZYc65X",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBbcmiCUMAAgtxA.jpg",
        "id_str" : "582895756320714752",
        "id" : 582895756320714752,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBbcmiCUMAAgtxA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 452,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/t1dKZYc65X"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 97 ],
        "url" : "http:\/\/t.co\/X2vGDcFBuk",
        "expanded_url" : "http:\/\/bit.ly\/1Ii987o",
        "display_url" : "bit.ly\/1Ii987o"
      } ]
    },
    "geo" : { },
    "id_str" : "582938429542961154",
    "text" : "These massive Slovenian caves are known as the \"Underground Grand Canyon\": http:\/\/t.co\/X2vGDcFBuk http:\/\/t.co\/t1dKZYc65X",
    "id" : 582938429542961154,
    "created_at" : "2015-03-31 16:12:06 +0000",
    "user" : {
      "name" : "Atlas Obscura",
      "screen_name" : "atlasobscura",
      "protected" : false,
      "id_str" : "57047586",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/861608340899729410\/J8SxzXcX_normal.jpg",
      "id" : 57047586,
      "verified" : true
    }
  },
  "id" : 582941012936552448,
  "created_at" : "2015-03-31 16:22:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MDP UCD",
      "screen_name" : "MDP_IE",
      "indices" : [ 3, 10 ],
      "id_str" : "193292945",
      "id" : 193292945
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MDP_IE\/status\/582933440615546880\/photo\/1",
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/ZjB6KPkCcg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb-3_eWUAAxiWQ.jpg",
      "id_str" : "582933439676043264",
      "id" : 582933439676043264,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb-3_eWUAAxiWQ.jpg",
      "sizes" : [ {
        "h" : 414,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 620
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZjB6KPkCcg"
    } ],
    "hashtags" : [ {
      "text" : "TweetToHeat",
      "indices" : [ 101, 113 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582940255013236736",
  "text" : "RT @MDP_IE: Hive have installed heaters in a bus stop in Manchester that is activated when you tweet #TweetToHeat http:\/\/t.co\/ZjB6KPkCcg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MDP_IE\/status\/582933440615546880\/photo\/1",
        "indices" : [ 102, 124 ],
        "url" : "http:\/\/t.co\/ZjB6KPkCcg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBb-3_eWUAAxiWQ.jpg",
        "id_str" : "582933439676043264",
        "id" : 582933439676043264,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBb-3_eWUAAxiWQ.jpg",
        "sizes" : [ {
          "h" : 414,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 620
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ZjB6KPkCcg"
      } ],
      "hashtags" : [ {
        "text" : "TweetToHeat",
        "indices" : [ 89, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "582933440615546880",
    "text" : "Hive have installed heaters in a bus stop in Manchester that is activated when you tweet #TweetToHeat http:\/\/t.co\/ZjB6KPkCcg",
    "id" : 582933440615546880,
    "created_at" : "2015-03-31 15:52:17 +0000",
    "user" : {
      "name" : "MDP UCD",
      "screen_name" : "MDP_IE",
      "protected" : false,
      "id_str" : "193292945",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/695585640524750848\/DIze1Eux_normal.jpg",
      "id" : 193292945,
      "verified" : false
    }
  },
  "id" : 582940255013236736,
  "created_at" : "2015-03-31 16:19:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "582896214171086848",
  "geo" : { },
  "id_str" : "582896669802553344",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose Catching up on Twitter update missed for a week. Nevertheless a welcome break.",
  "id" : 582896669802553344,
  "in_reply_to_status_id" : 582896214171086848,
  "created_at" : "2015-03-31 13:26:10 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "582895783130894336",
  "geo" : { },
  "id_str" : "582895997849899008",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose er.. the launch of your new website?",
  "id" : 582895997849899008,
  "in_reply_to_status_id" : 582895783130894336,
  "created_at" : "2015-03-31 13:23:30 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tex_n_Singapore",
      "screen_name" : "Tex_n_Singapore",
      "indices" : [ 3, 19 ],
      "id_str" : "2651067600",
      "id" : 2651067600
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Tex_n_Singapore\/status\/582888602759200768\/photo\/1",
      "indices" : [ 89, 111 ],
      "url" : "http:\/\/t.co\/oxFuC01OqT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBbWGIYWgAA0gN8.jpg",
      "id_str" : "582888602608238592",
      "id" : 582888602608238592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBbWGIYWgAA0gN8.jpg",
      "sizes" : [ {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oxFuC01OqT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582895830786506752",
  "text" : "RT @Tex_n_Singapore: The World Street Food Congress Returns From 8th to 12th April 2015! http:\/\/t.co\/oxFuC01OqT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Tex_n_Singapore\/status\/582888602759200768\/photo\/1",
        "indices" : [ 68, 90 ],
        "url" : "http:\/\/t.co\/oxFuC01OqT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBbWGIYWgAA0gN8.jpg",
        "id_str" : "582888602608238592",
        "id" : 582888602608238592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBbWGIYWgAA0gN8.jpg",
        "sizes" : [ {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oxFuC01OqT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "582888602759200768",
    "text" : "The World Street Food Congress Returns From 8th to 12th April 2015! http:\/\/t.co\/oxFuC01OqT",
    "id" : 582888602759200768,
    "created_at" : "2015-03-31 12:54:07 +0000",
    "user" : {
      "name" : "Tex_n_Singapore",
      "screen_name" : "Tex_n_Singapore",
      "protected" : false,
      "id_str" : "2651067600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/489405892517830657\/5_NmWxaN_normal.jpeg",
      "id" : 2651067600,
      "verified" : false
    }
  },
  "id" : 582895830786506752,
  "created_at" : "2015-03-31 13:22:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http:\/\/t.co\/WXaK8z9MLX",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b05nv2zw",
      "display_url" : "bbc.co.uk\/programmes\/b05\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582892009637171200",
  "text" : "Catch up this documentary on BBC iPlayer http:\/\/t.co\/WXaK8z9MLX Shocking.",
  "id" : 582892009637171200,
  "created_at" : "2015-03-31 13:07:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ZDNet",
      "screen_name" : "ZDNet",
      "indices" : [ 3, 9 ],
      "id_str" : "3819701",
      "id" : 3819701
    }, {
      "name" : "Eileen Yu",
      "screen_name" : "eileenscyu",
      "indices" : [ 90, 101 ],
      "id_str" : "112648308",
      "id" : 112648308
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/AL4o5IpDLd",
      "expanded_url" : "http:\/\/zd.net\/1Mt9tL1",
      "display_url" : "zd.net\/1Mt9tL1"
    } ]
  },
  "geo" : { },
  "id_str" : "582881232243032064",
  "text" : "RT @ZDNet: SAP unveils startup acceleration scheme in Singapore http:\/\/t.co\/AL4o5IpDLd by @eileenscyu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.zdnet.com\" rel=\"nofollow\"\u003Ezdnet API\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Eileen Yu",
        "screen_name" : "eileenscyu",
        "indices" : [ 79, 90 ],
        "id_str" : "112648308",
        "id" : 112648308
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 75 ],
        "url" : "http:\/\/t.co\/AL4o5IpDLd",
        "expanded_url" : "http:\/\/zd.net\/1Mt9tL1",
        "display_url" : "zd.net\/1Mt9tL1"
      } ]
    },
    "geo" : { },
    "id_str" : "582874274270040064",
    "text" : "SAP unveils startup acceleration scheme in Singapore http:\/\/t.co\/AL4o5IpDLd by @eileenscyu",
    "id" : 582874274270040064,
    "created_at" : "2015-03-31 11:57:10 +0000",
    "user" : {
      "name" : "ZDNet",
      "screen_name" : "ZDNet",
      "protected" : false,
      "id_str" : "3819701",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/706961982545473536\/Ibj46-DX_normal.jpg",
      "id" : 3819701,
      "verified" : true
    }
  },
  "id" : 582881232243032064,
  "created_at" : "2015-03-31 12:24:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Panic Dots",
      "screen_name" : "panicdots",
      "indices" : [ 0, 10 ],
      "id_str" : "20281026",
      "id" : 20281026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "582875174808195072",
  "geo" : { },
  "id_str" : "582880378211463168",
  "in_reply_to_user_id" : 20281026,
  "text" : "@PanicDots The driver is more relief she is not flag down cos of some traffic rules she think she might have broken. :)",
  "id" : 582880378211463168,
  "in_reply_to_status_id" : 582875174808195072,
  "created_at" : "2015-03-31 12:21:26 +0000",
  "in_reply_to_screen_name" : "panicdots",
  "in_reply_to_user_id_str" : "20281026",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "KNN",
      "indices" : [ 121, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582879138266128384",
  "text" : "2. Never see u post a tweet how Singapore manage it healthcare system so elderly does not wait 84 hours at A&amp;E dept. #KNN",
  "id" : 582879138266128384,
  "created_at" : "2015-03-31 12:16:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582878633368424448",
  "text" : "1. News of a teen blogger got arrested and you so fast post a tweet.",
  "id" : 582878633368424448,
  "created_at" : "2015-03-31 12:14:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "581063091237953536",
  "geo" : { },
  "id_str" : "582877675632623616",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose congratulations!",
  "id" : 582877675632623616,
  "in_reply_to_status_id" : 581063091237953536,
  "created_at" : "2015-03-31 12:10:41 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "SocialZavvy",
      "indices" : [ 3, 15 ],
      "id_str" : "387343951",
      "id" : 387343951
    }, {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "indices" : [ 25, 33 ],
      "id_str" : "137327916",
      "id" : 137327916
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GEC1stWC",
      "indices" : [ 124, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http:\/\/t.co\/LNF8H0VyVg",
      "expanded_url" : "http:\/\/ow.ly\/L0KIj",
      "display_url" : "ow.ly\/L0KIj"
    } ]
  },
  "geo" : { },
  "id_str" : "582877509135540224",
  "text" : "RT @SocialZavvy: Come to @gecinD8 http:\/\/t.co\/LNF8H0VyVg tomorrow for the GEC 1st Wednesday Club for startups 12.30-1.30pm  #GEC1stWC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The GEC",
        "screen_name" : "GECinD8",
        "indices" : [ 8, 16 ],
        "id_str" : "137327916",
        "id" : 137327916
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GEC1stWC",
        "indices" : [ 107, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 17, 39 ],
        "url" : "http:\/\/t.co\/LNF8H0VyVg",
        "expanded_url" : "http:\/\/ow.ly\/L0KIj",
        "display_url" : "ow.ly\/L0KIj"
      } ]
    },
    "geo" : { },
    "id_str" : "582788238651326465",
    "text" : "Come to @gecinD8 http:\/\/t.co\/LNF8H0VyVg tomorrow for the GEC 1st Wednesday Club for startups 12.30-1.30pm  #GEC1stWC",
    "id" : 582788238651326465,
    "created_at" : "2015-03-31 06:15:18 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "SocialZavvy",
      "protected" : false,
      "id_str" : "387343951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/447000331142979585\/RoucpBka_normal.jpeg",
      "id" : 387343951,
      "verified" : false
    }
  },
  "id" : 582877509135540224,
  "created_at" : "2015-03-31 12:10:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kaitlin Solimine",
      "screen_name" : "LetsGoKato",
      "indices" : [ 3, 14 ],
      "id_str" : "151972628",
      "id" : 151972628
    }, {
      "name" : "The Wall Street Journal",
      "screen_name" : "WSJ",
      "indices" : [ 126, 130 ],
      "id_str" : "3108351",
      "id" : 3108351
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "expat",
      "indices" : [ 83, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/2osTv1tMeG",
      "expanded_url" : "http:\/\/on.wsj.com\/1NtrwvB",
      "display_url" : "on.wsj.com\/1NtrwvB"
    } ]
  },
  "geo" : { },
  "id_str" : "582864581787275264",
  "text" : "RT @LetsGoKato: The Rise of the \u2018Expat-preneur\u2019 - my post on the growing subset of #expat culture  http:\/\/t.co\/2osTv1tMeG via @WSJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Wall Street Journal",
        "screen_name" : "WSJ",
        "indices" : [ 110, 114 ],
        "id_str" : "3108351",
        "id" : 3108351
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "expat",
        "indices" : [ 67, 73 ]
      } ],
      "urls" : [ {
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/2osTv1tMeG",
        "expanded_url" : "http:\/\/on.wsj.com\/1NtrwvB",
        "display_url" : "on.wsj.com\/1NtrwvB"
      } ]
    },
    "geo" : { },
    "id_str" : "582510975150112768",
    "text" : "The Rise of the \u2018Expat-preneur\u2019 - my post on the growing subset of #expat culture  http:\/\/t.co\/2osTv1tMeG via @WSJ",
    "id" : 582510975150112768,
    "created_at" : "2015-03-30 11:53:33 +0000",
    "user" : {
      "name" : "Kaitlin Solimine",
      "screen_name" : "LetsGoKato",
      "protected" : false,
      "id_str" : "151972628",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/826513922005950464\/4KfE0zpJ_normal.jpg",
      "id" : 151972628,
      "verified" : false
    }
  },
  "id" : 582864581787275264,
  "created_at" : "2015-03-31 11:18:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 3, 13 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "SMRT Feedback",
      "screen_name" : "smrtsg",
      "indices" : [ 74, 81 ],
      "id_str" : "444413055",
      "id" : 444413055
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/VXfI25mkjC",
      "expanded_url" : "https:\/\/twitter.com\/smrtsg\/status\/582763988259241984\/photo\/1",
      "display_url" : "pic.twitter.com\/VXfI25mkjC"
    } ]
  },
  "geo" : { },
  "id_str" : "582862505128665088",
  "text" : "RT @starduest: Excellent points. Kid needs counselling and a firm hand RT @smrtsg: Our opinion on Amos Yee. http:\/\/t.co\/VXfI25mkjC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SMRT Feedback",
        "screen_name" : "smrtsg",
        "indices" : [ 59, 66 ],
        "id_str" : "444413055",
        "id" : 444413055
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/VXfI25mkjC",
        "expanded_url" : "https:\/\/twitter.com\/smrtsg\/status\/582763988259241984\/photo\/1",
        "display_url" : "pic.twitter.com\/VXfI25mkjC"
      } ]
    },
    "geo" : { },
    "id_str" : "582858079588511744",
    "text" : "Excellent points. Kid needs counselling and a firm hand RT @smrtsg: Our opinion on Amos Yee. http:\/\/t.co\/VXfI25mkjC",
    "id" : 582858079588511744,
    "created_at" : "2015-03-31 10:52:49 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 582862505128665088,
  "created_at" : "2015-03-31 11:10:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/Qld3174aqM",
      "expanded_url" : "https:\/\/ifttt.com\/recipes",
      "display_url" : "ifttt.com\/recipes"
    } ]
  },
  "geo" : { },
  "id_str" : "582840142198603776",
  "text" : "Have anyone try this Do on https:\/\/t.co\/Qld3174aqM?",
  "id" : 582840142198603776,
  "created_at" : "2015-03-31 09:41:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cory-Ann Joseph",
      "screen_name" : "coryannj",
      "indices" : [ 3, 12 ],
      "id_str" : "62806814",
      "id" : 62806814
    }, {
      "name" : "Wei Ming Kam",
      "screen_name" : "weimingkam",
      "indices" : [ 74, 85 ],
      "id_str" : "2620623298",
      "id" : 2620623298
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/91DiryQmX2",
      "expanded_url" : "http:\/\/mediadiversified.org\/2015\/03\/31\/people-have-to-know-who-you-are-between-east-and-west\/",
      "display_url" : "mediadiversified.org\/2015\/03\/31\/peo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582839819132342273",
  "text" : "RT @coryannj: \"So you're Chinese right???\"\n\"So you're Malaysian then????\"\n@weimingkam nails it on in-between-ness  http:\/\/t.co\/91DiryQmX2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Wei Ming Kam",
        "screen_name" : "weimingkam",
        "indices" : [ 60, 71 ],
        "id_str" : "2620623298",
        "id" : 2620623298
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 123 ],
        "url" : "http:\/\/t.co\/91DiryQmX2",
        "expanded_url" : "http:\/\/mediadiversified.org\/2015\/03\/31\/people-have-to-know-who-you-are-between-east-and-west\/",
        "display_url" : "mediadiversified.org\/2015\/03\/31\/peo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "582816419433357312",
    "text" : "\"So you're Chinese right???\"\n\"So you're Malaysian then????\"\n@weimingkam nails it on in-between-ness  http:\/\/t.co\/91DiryQmX2",
    "id" : 582816419433357312,
    "created_at" : "2015-03-31 08:07:17 +0000",
    "user" : {
      "name" : "Cory-Ann Joseph",
      "screen_name" : "coryannj",
      "protected" : false,
      "id_str" : "62806814",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/727285913932406784\/Pr1-9f1a_normal.jpg",
      "id" : 62806814,
      "verified" : false
    }
  },
  "id" : 582839819132342273,
  "created_at" : "2015-03-31 09:40:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Telegraph",
      "screen_name" : "Telegraph",
      "indices" : [ 9, 19 ],
      "id_str" : "16343974",
      "id" : 16343974
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Telegraph\/status\/582666784005820416\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/dO6Ix5zJ5T",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBYMWPlVIAAMiDu.jpg",
      "id_str" : "582666778070818816",
      "id" : 582666778070818816,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBYMWPlVIAAMiDu.jpg",
      "sizes" : [ {
        "h" : 387,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 387,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 387,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 387,
        "resize" : "fit",
        "w" : 620
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dO6Ix5zJ5T"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/JwyOHWxEry",
      "expanded_url" : "http:\/\/www.telegraph.co.uk\/finance\/newsbysector\/mediatechnologyandtelecoms\/digital-media\/11505461\/Google-blurs-out-home-of-Germanwings-crash-pilot-Andreas-Lubitz.html",
      "display_url" : "telegraph.co.uk\/finance\/newsby\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "582666784005820416",
  "geo" : { },
  "id_str" : "582684881307717632",
  "in_reply_to_user_id" : 16343974,
  "text" : "\u6B64\u5730\u65E0\u94F6\u4E09\u767E\u4E24\uFF1F\u201C@Telegraph: Google blurs out home of Germanwings crash pilot Andreas Lubitz http:\/\/t.co\/JwyOHWxEry http:\/\/t.co\/dO6Ix5zJ5T\u201D",
  "id" : 582684881307717632,
  "in_reply_to_status_id" : 582666784005820416,
  "created_at" : "2015-03-30 23:24:36 +0000",
  "in_reply_to_screen_name" : "Telegraph",
  "in_reply_to_user_id_str" : "16343974",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TOC News",
      "screen_name" : "tocsg",
      "indices" : [ 3, 9 ],
      "id_str" : "44278933",
      "id" : 44278933
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582683901371539457",
  "text" : "RT @tocsg: Singapore was not built by just one man, or just by the Cabinet or the civil service, but by every Singaporean.... http:\/\/t.co\/V\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/V2KonxK8aM",
        "expanded_url" : "http:\/\/fb.me\/6uyg8QrVX",
        "display_url" : "fb.me\/6uyg8QrVX"
      } ]
    },
    "geo" : { },
    "id_str" : "582420684753764352",
    "text" : "Singapore was not built by just one man, or just by the Cabinet or the civil service, but by every Singaporean.... http:\/\/t.co\/V2KonxK8aM",
    "id" : 582420684753764352,
    "created_at" : "2015-03-30 05:54:46 +0000",
    "user" : {
      "name" : "TOC News",
      "screen_name" : "tocsg",
      "protected" : false,
      "id_str" : "44278933",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/837662162524856320\/_hEtmxdE_normal.jpg",
      "id" : 44278933,
      "verified" : true
    }
  },
  "id" : 582683901371539457,
  "created_at" : "2015-03-30 23:20:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dermot Casey",
      "screen_name" : "dermotcasey",
      "indices" : [ 3, 15 ],
      "id_str" : "6297382",
      "id" : 6297382
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 138 ],
      "url" : "http:\/\/t.co\/yqz7zAm7Ko",
      "expanded_url" : "http:\/\/torrentfreak.com\/court-orders-isp-to-disconnect-internet-pirates-150328\/",
      "display_url" : "torrentfreak.com\/court-orders-i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582679806342496256",
  "text" : "RT @dermotcasey: Court orders UPC to implement 3 strikes law in Ireland and to pay 80% of costs (800K) for doing so http:\/\/t.co\/yqz7zAm7Ko",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/yqz7zAm7Ko",
        "expanded_url" : "http:\/\/torrentfreak.com\/court-orders-isp-to-disconnect-internet-pirates-150328\/",
        "display_url" : "torrentfreak.com\/court-orders-i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "581767459558043648",
    "text" : "Court orders UPC to implement 3 strikes law in Ireland and to pay 80% of costs (800K) for doing so http:\/\/t.co\/yqz7zAm7Ko",
    "id" : 581767459558043648,
    "created_at" : "2015-03-28 10:39:05 +0000",
    "user" : {
      "name" : "Dermot Casey",
      "screen_name" : "dermotcasey",
      "protected" : false,
      "id_str" : "6297382",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004359497559740416\/CCWk8-wq_normal.jpg",
      "id" : 6297382,
      "verified" : false
    }
  },
  "id" : 582679806342496256,
  "created_at" : "2015-03-30 23:04:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 84 ],
      "url" : "http:\/\/t.co\/LSJ5MWODLE",
      "expanded_url" : "http:\/\/mrbrwn.co\/19rZtQ6",
      "display_url" : "mrbrwn.co\/19rZtQ6"
    } ]
  },
  "geo" : { },
  "id_str" : "582678745833730048",
  "text" : "RT @mrbrown: Lee Kuan Yew's Challenge to Democracy \u2014 Atlantic http:\/\/t.co\/LSJ5MWODLE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 71 ],
        "url" : "http:\/\/t.co\/LSJ5MWODLE",
        "expanded_url" : "http:\/\/mrbrwn.co\/19rZtQ6",
        "display_url" : "mrbrwn.co\/19rZtQ6"
      } ]
    },
    "geo" : { },
    "id_str" : "582661139361738752",
    "text" : "Lee Kuan Yew's Challenge to Democracy \u2014 Atlantic http:\/\/t.co\/LSJ5MWODLE",
    "id" : 582661139361738752,
    "created_at" : "2015-03-30 21:50:15 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 582678745833730048,
  "created_at" : "2015-03-30 23:00:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "582663730745466880",
  "geo" : { },
  "id_str" : "582678197961125888",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest I am surprise! While grateful for LKY, I have reservation about a political family dynasty in Singapore.",
  "id" : 582678197961125888,
  "in_reply_to_status_id" : 582663730745466880,
  "created_at" : "2015-03-30 22:58:02 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "582649939529981952",
  "geo" : { },
  "id_str" : "582672446072987649",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent all the best. :)",
  "id" : 582672446072987649,
  "in_reply_to_status_id" : 582649939529981952,
  "created_at" : "2015-03-30 22:35:11 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Hidden Persuader",
      "screen_name" : "hiddenpersuader",
      "indices" : [ 3, 19 ],
      "id_str" : "22652196",
      "id" : 22652196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582646041184464896",
  "text" : "RT @hiddenpersuader: If the Italians don\u2019t bring pasta and the French don\u2019t bring p\u00E2t\u00E9 you can\u2019t complain about Mrs Merkel\u2019s cabbage soup. \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/LrU7daEpIj",
        "expanded_url" : "http:\/\/econ.st\/19zVrVH",
        "display_url" : "econ.st\/19zVrVH"
      } ]
    },
    "geo" : { },
    "id_str" : "582644167970521088",
    "text" : "If the Italians don\u2019t bring pasta and the French don\u2019t bring p\u00E2t\u00E9 you can\u2019t complain about Mrs Merkel\u2019s cabbage soup. http:\/\/t.co\/LrU7daEpIj",
    "id" : 582644167970521088,
    "created_at" : "2015-03-30 20:42:49 +0000",
    "user" : {
      "name" : "The Hidden Persuader",
      "screen_name" : "hiddenpersuader",
      "protected" : false,
      "id_str" : "22652196",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3007339422\/09e6887f89f498051b4a7dd661a6f102_normal.jpeg",
      "id" : 22652196,
      "verified" : false
    }
  },
  "id" : 582646041184464896,
  "created_at" : "2015-03-30 20:50:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 135 ],
      "url" : "http:\/\/t.co\/iZZxtTF7Rx",
      "expanded_url" : "http:\/\/econ.st\/19ogmLu",
      "display_url" : "econ.st\/19ogmLu"
    } ]
  },
  "geo" : { },
  "id_str" : "582645684802842624",
  "text" : "RT @TheEconomist: More money is being spent on higher education. Too little is known about whether it's worth it http:\/\/t.co\/iZZxtTF7Rx htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/582634252812099585\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/23Zicjts1B",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBXuxA0WAAA4dtd.jpg",
        "id_str" : "582634252614893568",
        "id" : 582634252614893568,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBXuxA0WAAA4dtd.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/23Zicjts1B"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 117 ],
        "url" : "http:\/\/t.co\/iZZxtTF7Rx",
        "expanded_url" : "http:\/\/econ.st\/19ogmLu",
        "display_url" : "econ.st\/19ogmLu"
      } ]
    },
    "geo" : { },
    "id_str" : "582634252812099585",
    "text" : "More money is being spent on higher education. Too little is known about whether it's worth it http:\/\/t.co\/iZZxtTF7Rx http:\/\/t.co\/23Zicjts1B",
    "id" : 582634252812099585,
    "created_at" : "2015-03-30 20:03:25 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 582645684802842624,
  "created_at" : "2015-03-30 20:48:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 3, 19 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582643652977119232",
  "text" : "RT @interactivemark: This guy will charge you to look at your website while drunk. To think I\u2019ve been giving it away free all these years h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/8hbn3XPHgH",
        "expanded_url" : "http:\/\/bit.ly\/1ORkZ1p",
        "display_url" : "bit.ly\/1ORkZ1p"
      } ]
    },
    "geo" : { },
    "id_str" : "582641038499573760",
    "text" : "This guy will charge you to look at your website while drunk. To think I\u2019ve been giving it away free all these years http:\/\/t.co\/8hbn3XPHgH",
    "id" : 582641038499573760,
    "created_at" : "2015-03-30 20:30:23 +0000",
    "user" : {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "protected" : false,
      "id_str" : "14340736",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974702103984791552\/eHtIZ2ef_normal.jpg",
      "id" : 14340736,
      "verified" : false
    }
  },
  "id" : 582643652977119232,
  "created_at" : "2015-03-30 20:40:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 3, 18 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http:\/\/t.co\/AhNK1jrItq",
      "expanded_url" : "http:\/\/ow.ly\/KZE2q",
      "display_url" : "ow.ly\/KZE2q"
    } ]
  },
  "geo" : { },
  "id_str" : "582643458436952064",
  "text" : "RT @paulinesargent: Please do this insurance survey http:\/\/t.co\/AhNK1jrItq It will take under 2 minutes. Very much appreciated",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 54 ],
        "url" : "http:\/\/t.co\/AhNK1jrItq",
        "expanded_url" : "http:\/\/ow.ly\/KZE2q",
        "display_url" : "ow.ly\/KZE2q"
      } ]
    },
    "geo" : { },
    "id_str" : "582614994572996608",
    "text" : "Please do this insurance survey http:\/\/t.co\/AhNK1jrItq It will take under 2 minutes. Very much appreciated",
    "id" : 582614994572996608,
    "created_at" : "2015-03-30 18:46:53 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "protected" : false,
      "id_str" : "22997944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002264625130409984\/wlpdomJK_normal.jpg",
      "id" : 22997944,
      "verified" : false
    }
  },
  "id" : 582643458436952064,
  "created_at" : "2015-03-30 20:40:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582621109222244352",
  "text" : "Is Life Time Value the best metric that you can use to measure marketing ROI?",
  "id" : 582621109222244352,
  "created_at" : "2015-03-30 19:11:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/OreviApJpw",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/technology\/former-samsung-chief-executive-takes-seat-on-trakax-board-1.2153157",
      "display_url" : "irishtimes.com\/business\/techn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582608716958060544",
  "text" : "Irish tech firm announces former Samsung CEO joins Board. http:\/\/t.co\/OreviApJpw",
  "id" : 582608716958060544,
  "created_at" : "2015-03-30 18:21:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/zAYyS30q5o",
      "expanded_url" : "https:\/\/www.facebook.com\/LianZhengNet\/photos\/a.240460632707043.58708.170876719665435\/669998279753274\/?type=1&theater",
      "display_url" : "facebook.com\/LianZhengNet\/p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582605964831948800",
  "text" : "Singapore benefits as a result of Malaysia racist pro-Malay policy. https:\/\/t.co\/zAYyS30q5o Up to 450,000 Malaysia ethic Chinese in SGP",
  "id" : 582605964831948800,
  "created_at" : "2015-03-30 18:11:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Remember H. Rosling",
      "screen_name" : "HansRosling",
      "indices" : [ 3, 15 ],
      "id_str" : "20280065",
      "id" : 20280065
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582596906066407424",
  "text" : "RT @HansRosling: 90 % of the World population now have access to safe drinking water. Just 10% left to fix! Lets do it!  http:\/\/t.co\/cyDLFX\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 104, 126 ],
        "url" : "http:\/\/t.co\/cyDLFXX9PL",
        "expanded_url" : "http:\/\/uni.cf\/1DaDCKz",
        "display_url" : "uni.cf\/1DaDCKz"
      } ]
    },
    "geo" : { },
    "id_str" : "582595872493596672",
    "text" : "90 % of the World population now have access to safe drinking water. Just 10% left to fix! Lets do it!  http:\/\/t.co\/cyDLFXX9PL",
    "id" : 582595872493596672,
    "created_at" : "2015-03-30 17:30:54 +0000",
    "user" : {
      "name" : "Remember H. Rosling",
      "screen_name" : "HansRosling",
      "protected" : false,
      "id_str" : "20280065",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/116939798\/Hans_Rosling_liten_normal.jpg",
      "id" : 20280065,
      "verified" : true
    }
  },
  "id" : 582596906066407424,
  "created_at" : "2015-03-30 17:35:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laura Acklandiene",
      "screen_name" : "Laurama_",
      "indices" : [ 3, 12 ],
      "id_str" : "90725385",
      "id" : 90725385
    }, {
      "name" : "Mike Petroff",
      "screen_name" : "mikepetroff",
      "indices" : [ 63, 75 ],
      "id_str" : "12931202",
      "id" : 12931202
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/zqxbyzgALS",
      "expanded_url" : "https:\/\/medium.com\/@mikepetroff\/pageviews-vs-engaged-time-in-web-analytics-b40b4d17d8b6?source=tw-870de9b2f789-1427717422467",
      "display_url" : "medium.com\/@mikepetroff\/p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582586877720526848",
  "text" : "RT @Laurama_: \u201CPageviews vs. Engaged Time in web analytics\u201D by @mikepetroff https:\/\/t.co\/zqxbyzgALS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Mike Petroff",
        "screen_name" : "mikepetroff",
        "indices" : [ 49, 61 ],
        "id_str" : "12931202",
        "id" : 12931202
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/zqxbyzgALS",
        "expanded_url" : "https:\/\/medium.com\/@mikepetroff\/pageviews-vs-engaged-time-in-web-analytics-b40b4d17d8b6?source=tw-870de9b2f789-1427717422467",
        "display_url" : "medium.com\/@mikepetroff\/p\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "582515372089184256",
    "text" : "\u201CPageviews vs. Engaged Time in web analytics\u201D by @mikepetroff https:\/\/t.co\/zqxbyzgALS",
    "id" : 582515372089184256,
    "created_at" : "2015-03-30 12:11:01 +0000",
    "user" : {
      "name" : "Laura Acklandiene",
      "screen_name" : "Laurama_",
      "protected" : false,
      "id_str" : "90725385",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/847937412399366144\/oZ9qlEJD_normal.jpg",
      "id" : 90725385,
      "verified" : false
    }
  },
  "id" : 582586877720526848,
  "created_at" : "2015-03-30 16:55:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fast Company",
      "screen_name" : "FastCompany",
      "indices" : [ 3, 15 ],
      "id_str" : "2735591",
      "id" : 2735591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/qHe8WDfsrm",
      "expanded_url" : "http:\/\/f-st.co\/aUjFQet",
      "display_url" : "f-st.co\/aUjFQet"
    } ]
  },
  "geo" : { },
  "id_str" : "582585379062194176",
  "text" : "RT @FastCompany: Here's why psychologists say you should spend your money on experiences, not things http:\/\/t.co\/qHe8WDfsrm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 106 ],
        "url" : "http:\/\/t.co\/qHe8WDfsrm",
        "expanded_url" : "http:\/\/f-st.co\/aUjFQet",
        "display_url" : "f-st.co\/aUjFQet"
      } ]
    },
    "geo" : { },
    "id_str" : "582565331245350912",
    "text" : "Here's why psychologists say you should spend your money on experiences, not things http:\/\/t.co\/qHe8WDfsrm",
    "id" : 582565331245350912,
    "created_at" : "2015-03-30 15:29:33 +0000",
    "user" : {
      "name" : "Fast Company",
      "screen_name" : "FastCompany",
      "protected" : false,
      "id_str" : "2735591",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875769219400351744\/ib7iIvRF_normal.jpg",
      "id" : 2735591,
      "verified" : true
    }
  },
  "id" : 582585379062194176,
  "created_at" : "2015-03-30 16:49:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 0, 16 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "582491791989088256",
  "geo" : { },
  "id_str" : "582537592639492096",
  "in_reply_to_user_id" : 22700048,
  "text" : "@EimearMcCormack I can't DM you. I am keen.",
  "id" : 582537592639492096,
  "in_reply_to_status_id" : 582491791989088256,
  "created_at" : "2015-03-30 13:39:19 +0000",
  "in_reply_to_screen_name" : "EimearMcCormack",
  "in_reply_to_user_id_str" : "22700048",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rachel",
      "screen_name" : "rachelrmk",
      "indices" : [ 3, 13 ],
      "id_str" : "257844106",
      "id" : 257844106
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "liveline",
      "indices" : [ 121, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582533190566486016",
  "text" : "RT @rachelrmk: \"I thought 50 Shades of Grey had something to do with growing old... I hadn't a clue, Joe.\" - Listener on #liveline   o_O",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "liveline",
        "indices" : [ 106, 115 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "582528190347812865",
    "text" : "\"I thought 50 Shades of Grey had something to do with growing old... I hadn't a clue, Joe.\" - Listener on #liveline   o_O",
    "id" : 582528190347812865,
    "created_at" : "2015-03-30 13:01:58 +0000",
    "user" : {
      "name" : "Rachel",
      "screen_name" : "rachelrmk",
      "protected" : false,
      "id_str" : "257844106",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469623404010938368\/InsRVH-Q_normal.jpeg",
      "id" : 257844106,
      "verified" : false
    }
  },
  "id" : 582533190566486016,
  "created_at" : "2015-03-30 13:21:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582504931040620544",
  "text" : "RT @EimearMcCormack: I\u2019m looking for Beta testers in the delivery market (people to deliver goods) to trial new software for free.Intereste\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "582491791989088256",
    "text" : "I\u2019m looking for Beta testers in the delivery market (people to deliver goods) to trial new software for free.Interested? DM me. RT? :)",
    "id" : 582491791989088256,
    "created_at" : "2015-03-30 10:37:20 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 582504931040620544,
  "created_at" : "2015-03-30 11:29:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Twitter Singapore",
      "screen_name" : "TwitterSG",
      "indices" : [ 3, 13 ],
      "id_str" : "2244983430",
      "id" : 2244983430
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RIPLKY",
      "indices" : [ 103, 110 ]
    }, {
      "text" : "RememberingLKY",
      "indices" : [ 111, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582504876200091648",
  "text" : "RT @TwitterSG: An overwhelming outpouring of grief and tributes to Mr Lee Kuan Yew over the past week. #RIPLKY #RememberingLKY https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RIPLKY",
        "indices" : [ 88, 95 ]
      }, {
        "text" : "RememberingLKY",
        "indices" : [ 96, 111 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/qUuJlU3bQT",
        "expanded_url" : "https:\/\/twitter.com\/TwitterSG\/timelines\/582487404981927936",
        "display_url" : "twitter.com\/TwitterSG\/time\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "582491106752892928",
    "text" : "An overwhelming outpouring of grief and tributes to Mr Lee Kuan Yew over the past week. #RIPLKY #RememberingLKY https:\/\/t.co\/qUuJlU3bQT",
    "id" : 582491106752892928,
    "created_at" : "2015-03-30 10:34:36 +0000",
    "user" : {
      "name" : "Twitter Singapore",
      "screen_name" : "TwitterSG",
      "protected" : false,
      "id_str" : "2244983430",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875094332365090816\/tcm6HqXp_normal.jpg",
      "id" : 2244983430,
      "verified" : true
    }
  },
  "id" : 582504876200091648,
  "created_at" : "2015-03-30 11:29:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jerome",
      "screen_name" : "JeromeR",
      "indices" : [ 3, 11 ],
      "id_str" : "6630162",
      "id" : 6630162
    }, {
      "name" : "John Maeda",
      "screen_name" : "johnmaeda",
      "indices" : [ 13, 23 ],
      "id_str" : "15414807",
      "id" : 15414807
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 24, 30 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UX",
      "indices" : [ 135, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582298854403543041",
  "text" : "RT @JeromeR: @johnmaeda @mryap To me, \"fail fast\" is about iterating, trying, experimenting\u2014without FEAR of failure\u2014to get to a better #UX \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "John Maeda",
        "screen_name" : "johnmaeda",
        "indices" : [ 0, 10 ],
        "id_str" : "15414807",
        "id" : 15414807
      }, {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 11, 17 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "UX",
        "indices" : [ 122, 125 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "582281174581669888",
    "geo" : { },
    "id_str" : "582298563645853696",
    "in_reply_to_user_id" : 15414807,
    "text" : "@johnmaeda @mryap To me, \"fail fast\" is about iterating, trying, experimenting\u2014without FEAR of failure\u2014to get to a better #UX solution.",
    "id" : 582298563645853696,
    "in_reply_to_status_id" : 582281174581669888,
    "created_at" : "2015-03-29 21:49:30 +0000",
    "in_reply_to_screen_name" : "johnmaeda",
    "in_reply_to_user_id_str" : "15414807",
    "user" : {
      "name" : "Jerome",
      "screen_name" : "JeromeR",
      "protected" : false,
      "id_str" : "6630162",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/872435779347374081\/pbzYnrJw_normal.jpg",
      "id" : 6630162,
      "verified" : false
    }
  },
  "id" : 582298854403543041,
  "created_at" : "2015-03-29 21:50:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Maeda",
      "screen_name" : "johnmaeda",
      "indices" : [ 3, 13 ],
      "id_str" : "15414807",
      "id" : 15414807
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582298030944215042",
  "text" : "RT @johnmaeda: The mantra of \"fail fast\" is often misunderstood as deifying failure. What it really means is \"recover fast\" -- the comeback\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "582281174581669888",
    "text" : "The mantra of \"fail fast\" is often misunderstood as deifying failure. What it really means is \"recover fast\" -- the comeback is key.",
    "id" : 582281174581669888,
    "created_at" : "2015-03-29 20:40:24 +0000",
    "user" : {
      "name" : "John Maeda",
      "screen_name" : "johnmaeda",
      "protected" : false,
      "id_str" : "15414807",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839937736018501632\/0aXmic50_normal.jpg",
      "id" : 15414807,
      "verified" : true
    }
  },
  "id" : 582298030944215042,
  "created_at" : "2015-03-29 21:47:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582294163519078400",
  "text" : "Reading LKY book as bedtime story put my 10yo kid to sleep within 10 min.",
  "id" : 582294163519078400,
  "created_at" : "2015-03-29 21:32:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Grant",
      "screen_name" : "TradingJeremy",
      "indices" : [ 3, 17 ],
      "id_str" : "47456979",
      "id" : 47456979
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TradingJeremy\/status\/578220680161824769\/photo\/1",
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/xiZuVmZuha",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAZApRpUkAIYzUt.jpg",
      "id_str" : "578220680019218434",
      "id" : 578220680019218434,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAZApRpUkAIYzUt.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 499
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 499
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 499
      }, {
        "h" : 665,
        "resize" : "fit",
        "w" : 499
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xiZuVmZuha"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582278109304619008",
  "text" : "RT @TradingJeremy: Singapore armed forces are advertising at bus stops for.citizens to name new naval vessels http:\/\/t.co\/xiZuVmZuha",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TradingJeremy\/status\/578220680161824769\/photo\/1",
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/xiZuVmZuha",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAZApRpUkAIYzUt.jpg",
        "id_str" : "578220680019218434",
        "id" : 578220680019218434,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAZApRpUkAIYzUt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 665,
          "resize" : "fit",
          "w" : 499
        }, {
          "h" : 665,
          "resize" : "fit",
          "w" : 499
        }, {
          "h" : 665,
          "resize" : "fit",
          "w" : 499
        }, {
          "h" : 665,
          "resize" : "fit",
          "w" : 499
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xiZuVmZuha"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578220680161824769",
    "text" : "Singapore armed forces are advertising at bus stops for.citizens to name new naval vessels http:\/\/t.co\/xiZuVmZuha",
    "id" : 578220680161824769,
    "created_at" : "2015-03-18 15:45:27 +0000",
    "user" : {
      "name" : "Jeremy Grant",
      "screen_name" : "TradingJeremy",
      "protected" : false,
      "id_str" : "47456979",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/770567416728391680\/T9iyVXH5_normal.jpg",
      "id" : 47456979,
      "verified" : false
    }
  },
  "id" : 582278109304619008,
  "created_at" : "2015-03-29 20:28:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/ipiEDPje9d",
      "expanded_url" : "http:\/\/www.straitstimes.com\/news\/singapore\/more-singapore-stories\/story\/mr-lee-kuan-yew-had-close-ties-britain-was-never-colonia",
      "display_url" : "straitstimes.com\/news\/singapore\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582235549165064193",
  "text" : "He admired the NHS...but swiftly concluded that the system is unaffordable, .\nhttp:\/\/t.co\/ipiEDPje9d",
  "id" : 582235549165064193,
  "created_at" : "2015-03-29 17:39:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 105 ],
      "url" : "http:\/\/t.co\/LtD2ldQKNy",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/Halimah_Yacob",
      "display_url" : "en.wikipedia.org\/wiki\/Halimah_Y\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "582175832283734016",
  "text" : "One more thing, Singapore  speaker of parliament is a woman, Muslim and a minority http:\/\/t.co\/LtD2ldQKNy A fact bought up by my wife.",
  "id" : 582175832283734016,
  "created_at" : "2015-03-29 13:41:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582172803916525568",
  "text" : "Western Media stop harping about chewing gum ban. Maybe can cover how we solve the housing problem. We paid 20% downpayment for mortgages.",
  "id" : 582172803916525568,
  "created_at" : "2015-03-29 13:29:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/ZbmxB7SZlL",
      "expanded_url" : "http:\/\/econ.st\/1D6mBBb",
      "display_url" : "econ.st\/1D6mBBb"
    } ]
  },
  "geo" : { },
  "id_str" : "582156422458290177",
  "text" : "RT @TheEconomist: Hopes for the next mobile data standard, 5G: a true global standard that's faster than fibre http:\/\/t.co\/ZbmxB7SZlL http:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/581936392953061376\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/iNf7kP7RS5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBN0ENvW8AEp3E4.jpg",
        "id_str" : "581936392617521153",
        "id" : 581936392617521153,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBN0ENvW8AEp3E4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iNf7kP7RS5"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/ZbmxB7SZlL",
        "expanded_url" : "http:\/\/econ.st\/1D6mBBb",
        "display_url" : "econ.st\/1D6mBBb"
      } ]
    },
    "geo" : { },
    "id_str" : "581936392953061376",
    "text" : "Hopes for the next mobile data standard, 5G: a true global standard that's faster than fibre http:\/\/t.co\/ZbmxB7SZlL http:\/\/t.co\/iNf7kP7RS5",
    "id" : 581936392953061376,
    "created_at" : "2015-03-28 21:50:22 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 582156422458290177,
  "created_at" : "2015-03-29 12:24:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JoshgunAtakhanov",
      "screen_name" : "JoshgunA",
      "indices" : [ 0, 9 ],
      "id_str" : "808548594164834304",
      "id" : 808548594164834304
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582147002152173568",
  "text" : "@joshguna you can try CNA YouTube channel.",
  "id" : 582147002152173568,
  "created_at" : "2015-03-29 11:47:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kevin Lim (PhD)",
      "screen_name" : "brainopera",
      "indices" : [ 3, 14 ],
      "id_str" : "22823",
      "id" : 22823
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/ssLmcZtkoF",
      "expanded_url" : "http:\/\/ift.tt\/1OMDRyq",
      "display_url" : "ift.tt\/1OMDRyq"
    } ]
  },
  "geo" : { },
  "id_str" : "582146666909798400",
  "text" : "RT @brainopera: An Ah-Mah asked to get closer to the road so she could see Mr Lee's procession, and a seni\u2026 http:\/\/t.co\/ssLmcZtkoF http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/brainopera\/status\/582142984323510272\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/xs1i1JXp3f",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBQv9beW4AIO_cO.jpg",
        "id_str" : "582142984231247874",
        "id" : 582142984231247874,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBQv9beW4AIO_cO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xs1i1JXp3f"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/ssLmcZtkoF",
        "expanded_url" : "http:\/\/ift.tt\/1OMDRyq",
        "display_url" : "ift.tt\/1OMDRyq"
      } ]
    },
    "geo" : { },
    "id_str" : "582142984323510272",
    "text" : "An Ah-Mah asked to get closer to the road so she could see Mr Lee's procession, and a seni\u2026 http:\/\/t.co\/ssLmcZtkoF http:\/\/t.co\/xs1i1JXp3f",
    "id" : 582142984323510272,
    "created_at" : "2015-03-29 11:31:17 +0000",
    "user" : {
      "name" : "Kevin Lim (PhD)",
      "screen_name" : "brainopera",
      "protected" : false,
      "id_str" : "22823",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/820467861936369664\/gAir1s2g_normal.jpg",
      "id" : 22823,
      "verified" : false
    }
  },
  "id" : 582146666909798400,
  "created_at" : "2015-03-29 11:45:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "indices" : [ 3, 12 ],
      "id_str" : "742143",
      "id" : 742143
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http:\/\/t.co\/DspAm8vQdf",
      "expanded_url" : "http:\/\/bbc.in\/1xp9ubT",
      "display_url" : "bbc.in\/1xp9ubT"
    } ]
  },
  "geo" : { },
  "id_str" : "582138620401811456",
  "text" : "RT @BBCWorld: Thousands lined the streets of Singapore, in torrential rain, to say goodbye to Lee Kuan Yew http:\/\/t.co\/DspAm8vQdf http:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BBCWorld\/status\/582111078789865472\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/kY2MDBj7dN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBQPKpKUwAAMybe.jpg",
        "id_str" : "582106927359901696",
        "id" : 582106927359901696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBQPKpKUwAAMybe.jpg",
        "sizes" : [ {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kY2MDBj7dN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/BBCWorld\/status\/582111078789865472\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/kY2MDBj7dN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBQPMdIUIAAAl9f.jpg",
        "id_str" : "582106958489985024",
        "id" : 582106958489985024,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBQPMdIUIAAAl9f.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kY2MDBj7dN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/BBCWorld\/status\/582111078789865472\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/kY2MDBj7dN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBQPMMZUwAAWmkB.jpg",
        "id_str" : "582106953997926400",
        "id" : 582106953997926400,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBQPMMZUwAAWmkB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kY2MDBj7dN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/BBCWorld\/status\/582111078789865472\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/kY2MDBj7dN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBQPMLrUIAATRpH.jpg",
        "id_str" : "582106953804947456",
        "id" : 582106953804947456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBQPMLrUIAATRpH.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kY2MDBj7dN"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/DspAm8vQdf",
        "expanded_url" : "http:\/\/bbc.in\/1xp9ubT",
        "display_url" : "bbc.in\/1xp9ubT"
      } ]
    },
    "geo" : { },
    "id_str" : "582111078789865472",
    "text" : "Thousands lined the streets of Singapore, in torrential rain, to say goodbye to Lee Kuan Yew http:\/\/t.co\/DspAm8vQdf http:\/\/t.co\/kY2MDBj7dN",
    "id" : 582111078789865472,
    "created_at" : "2015-03-29 09:24:30 +0000",
    "user" : {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "protected" : false,
      "id_str" : "742143",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875702138680246273\/BfQLzf7G_normal.jpg",
      "id" : 742143,
      "verified" : true
    }
  },
  "id" : 582138620401811456,
  "created_at" : "2015-03-29 11:13:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 17, 27 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 41, 52 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "582135151490179072",
  "text" : "I took over from @starduest this week at @hellofrmSG.",
  "id" : 582135151490179072,
  "created_at" : "2015-03-29 11:00:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 126 ],
      "url" : "http:\/\/t.co\/fnXYTpuJb5",
      "expanded_url" : "http:\/\/btd.sg\/1ButAgo",
      "display_url" : "btd.sg\/1ButAgo"
    } ]
  },
  "geo" : { },
  "id_str" : "581984126292606977",
  "text" : "RT @GeoffreyIRL: Nicely observed piece in the Business Times: By gum, the West is wrong about Singapore http:\/\/t.co\/fnXYTpuJb5 via @shareth\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "ShareThis",
        "screen_name" : "ShareThis",
        "indices" : [ 114, 124 ],
        "id_str" : "14116807",
        "id" : 14116807
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 109 ],
        "url" : "http:\/\/t.co\/fnXYTpuJb5",
        "expanded_url" : "http:\/\/btd.sg\/1ButAgo",
        "display_url" : "btd.sg\/1ButAgo"
      } ]
    },
    "geo" : { },
    "id_str" : "581756817472024576",
    "text" : "Nicely observed piece in the Business Times: By gum, the West is wrong about Singapore http:\/\/t.co\/fnXYTpuJb5 via @sharethis",
    "id" : 581756817472024576,
    "created_at" : "2015-03-28 09:56:48 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 581984126292606977,
  "created_at" : "2015-03-29 01:00:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/myVCDu6j86",
      "expanded_url" : "http:\/\/www.nationalreview.com\/article\/416071\/singapore-lee-kuan-yew-built-welfare-state-works-john-fund",
      "display_url" : "nationalreview.com\/article\/416071\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "581981345246781440",
  "text" : "In Singapore, Lee Kuan Yew Built a Welfare State That Works, by John Fund, National Review. http:\/\/t.co\/myVCDu6j86",
  "id" : 581981345246781440,
  "created_at" : "2015-03-29 00:49:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/E7iRodz9Rn",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=OENzgWq0Q2s&feature=youtu.be",
      "display_url" : "youtube.com\/watch?v=OENzgW\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "581979305124691968",
  "text" : "The Black Knight's Missing man formation honouring Singapore's founding father. https:\/\/t.co\/E7iRodz9Rn",
  "id" : 581979305124691968,
  "created_at" : "2015-03-29 00:40:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "indices" : [ 3, 12 ],
      "id_str" : "742143",
      "id" : 742143
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http:\/\/t.co\/zkt7uaYML2",
      "expanded_url" : "http:\/\/bbc.in\/1I621yQ",
      "display_url" : "bbc.in\/1I621yQ"
    } ]
  },
  "geo" : { },
  "id_str" : "581972614555549696",
  "text" : "RT @BBCWorld: Singapore bids Lee Kuan Yew farewell http:\/\/t.co\/zkt7uaYML2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 37, 59 ],
        "url" : "http:\/\/t.co\/zkt7uaYML2",
        "expanded_url" : "http:\/\/bbc.in\/1I621yQ",
        "display_url" : "bbc.in\/1I621yQ"
      } ]
    },
    "geo" : { },
    "id_str" : "581956684454752257",
    "text" : "Singapore bids Lee Kuan Yew farewell http:\/\/t.co\/zkt7uaYML2",
    "id" : 581956684454752257,
    "created_at" : "2015-03-28 23:11:00 +0000",
    "user" : {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "protected" : false,
      "id_str" : "742143",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875702138680246273\/BfQLzf7G_normal.jpg",
      "id" : 742143,
      "verified" : true
    }
  },
  "id" : 581972614555549696,
  "created_at" : "2015-03-29 00:14:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jerome",
      "screen_name" : "JeromeR",
      "indices" : [ 3, 11 ],
      "id_str" : "6630162",
      "id" : 6630162
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Agile",
      "indices" : [ 18, 24 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "581972416659886080",
  "text" : "RT @JeromeR: OH: \"#Agile is 20 cement trucks pouring feverishly while someone looks up Architect on the Yellow Pages.\" http:\/\/t.co\/5Tnl42It\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Agile",
        "indices" : [ 5, 11 ]
      }, {
        "text" : "UX",
        "indices" : [ 129, 132 ]
      } ],
      "urls" : [ {
        "indices" : [ 106, 128 ],
        "url" : "http:\/\/t.co\/5Tnl42ItW3",
        "expanded_url" : "http:\/\/dfwux.com\/podcast\/episode-001-roger-belveal\/#transcript",
        "display_url" : "dfwux.com\/podcast\/episod\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "581965095267237888",
    "text" : "OH: \"#Agile is 20 cement trucks pouring feverishly while someone looks up Architect on the Yellow Pages.\" http:\/\/t.co\/5Tnl42ItW3 #UX",
    "id" : 581965095267237888,
    "created_at" : "2015-03-28 23:44:25 +0000",
    "user" : {
      "name" : "Jerome",
      "screen_name" : "JeromeR",
      "protected" : false,
      "id_str" : "6630162",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/872435779347374081\/pbzYnrJw_normal.jpg",
      "id" : 6630162,
      "verified" : false
    }
  },
  "id" : 581972416659886080,
  "created_at" : "2015-03-29 00:13:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNET",
      "screen_name" : "CNET",
      "indices" : [ 3, 8 ],
      "id_str" : "30261067",
      "id" : 30261067
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/CNET\/status\/581923824691748864\/photo\/1",
      "indices" : [ 81, 103 ],
      "url" : "http:\/\/t.co\/RLP9Z8gtuO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBNoopxW4AEFbGs.jpg",
      "id_str" : "581923824477855745",
      "id" : 581923824477855745,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBNoopxW4AEFbGs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 770
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 770
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 770
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RLP9Z8gtuO"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/3kC9MzLzfb",
      "expanded_url" : "http:\/\/cnet.co\/1ChjVNH",
      "display_url" : "cnet.co\/1ChjVNH"
    } ]
  },
  "geo" : { },
  "id_str" : "581961135760490496",
  "text" : "RT @CNET: Engineering students extinguish fire with sound http:\/\/t.co\/3kC9MzLzfb http:\/\/t.co\/RLP9Z8gtuO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CNET\/status\/581923824691748864\/photo\/1",
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/RLP9Z8gtuO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBNoopxW4AEFbGs.jpg",
        "id_str" : "581923824477855745",
        "id" : 581923824477855745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBNoopxW4AEFbGs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RLP9Z8gtuO"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 70 ],
        "url" : "http:\/\/t.co\/3kC9MzLzfb",
        "expanded_url" : "http:\/\/cnet.co\/1ChjVNH",
        "display_url" : "cnet.co\/1ChjVNH"
      } ]
    },
    "geo" : { },
    "id_str" : "581923824691748864",
    "text" : "Engineering students extinguish fire with sound http:\/\/t.co\/3kC9MzLzfb http:\/\/t.co\/RLP9Z8gtuO",
    "id" : 581923824691748864,
    "created_at" : "2015-03-28 21:00:26 +0000",
    "user" : {
      "name" : "CNET",
      "screen_name" : "CNET",
      "protected" : false,
      "id_str" : "30261067",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963998359001317376\/scuoOV5m_normal.jpg",
      "id" : 30261067,
      "verified" : true
    }
  },
  "id" : 581961135760490496,
  "created_at" : "2015-03-28 23:28:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hannah Witton",
      "screen_name" : "hannahwitton",
      "indices" : [ 3, 16 ],
      "id_str" : "38485467",
      "id" : 38485467
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/hannahwitton\/status\/581910916297543680\/photo\/1",
      "indices" : [ 50, 72 ],
      "url" : "http:\/\/t.co\/LStYOgc3Ey",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CBNc3cgWkAAi_iD.jpg",
      "id_str" : "581910884475375616",
      "id" : 581910884475375616,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBNc3cgWkAAi_iD.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LStYOgc3Ey"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "581961019829952512",
  "text" : "RT @hannahwitton: WE MUST PROTECT THE PROTECTION! http:\/\/t.co\/LStYOgc3Ey",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/hannahwitton\/status\/581910916297543680\/photo\/1",
        "indices" : [ 32, 54 ],
        "url" : "http:\/\/t.co\/LStYOgc3Ey",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CBNc3cgWkAAi_iD.jpg",
        "id_str" : "581910884475375616",
        "id" : 581910884475375616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CBNc3cgWkAAi_iD.jpg",
        "sizes" : [ {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LStYOgc3Ey"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "581910916297543680",
    "text" : "WE MUST PROTECT THE PROTECTION! http:\/\/t.co\/LStYOgc3Ey",
    "id" : 581910916297543680,
    "created_at" : "2015-03-28 20:09:08 +0000",
    "user" : {
      "name" : "Hannah Witton",
      "screen_name" : "hannahwitton",
      "protected" : false,
      "id_str" : "38485467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002623369475493888\/WwyYmKWJ_normal.jpg",
      "id" : 38485467,
      "verified" : true
    }
  },
  "id" : 581961019829952512,
  "created_at" : "2015-03-28 23:28:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 3, 14 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 74, 80 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "581754592486158336",
  "text" : "RT @hellofrmSG: Morning everyone!! My last day here before I hand over to @mryap who's down south at GMT0, so you've got me till 8am Sunday\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 58, 64 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "581754406342959104",
    "text" : "Morning everyone!! My last day here before I hand over to @mryap who's down south at GMT0, so you've got me till 8am Sunday in SG :)",
    "id" : 581754406342959104,
    "created_at" : "2015-03-28 09:47:13 +0000",
    "user" : {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "protected" : false,
      "id_str" : "607156756",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040905442216407040\/xF1JwbXR_normal.jpg",
      "id" : 607156756,
      "verified" : false
    }
  },
  "id" : 581754592486158336,
  "created_at" : "2015-03-28 09:47:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 0, 12 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580799370028658688",
  "geo" : { },
  "id_str" : "580800708389285888",
  "in_reply_to_user_id" : 33462545,
  "text" : "@yapphenghui One of the freedom I lost while overseas is to roam freely at night. (after 9pm)",
  "id" : 580800708389285888,
  "in_reply_to_status_id" : 580799370028658688,
  "created_at" : "2015-03-25 18:37:34 +0000",
  "in_reply_to_screen_name" : "yapphenghui",
  "in_reply_to_user_id_str" : "33462545",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RememberingLKY",
      "indices" : [ 96, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/Xxz3s1Xchk",
      "expanded_url" : "https:\/\/instagram.com\/p\/0qQqMRuO22\/",
      "display_url" : "instagram.com\/p\/0qQqMRuO22\/"
    } ]
  },
  "geo" : { },
  "id_str" : "580794755245838336",
  "text" : "RT @mrbrown: 2.11am: Greetings from Hong Lim Park. The night queue to Parliament is still long. #RememberingLKY @\u2026 https:\/\/t.co\/Xxz3s1Xchk",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RememberingLKY",
        "indices" : [ 83, 98 ]
      } ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/Xxz3s1Xchk",
        "expanded_url" : "https:\/\/instagram.com\/p\/0qQqMRuO22\/",
        "display_url" : "instagram.com\/p\/0qQqMRuO22\/"
      } ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 1.286591667, 103.846394444 ]
    },
    "id_str" : "580794575754797057",
    "text" : "2.11am: Greetings from Hong Lim Park. The night queue to Parliament is still long. #RememberingLKY @\u2026 https:\/\/t.co\/Xxz3s1Xchk",
    "id" : 580794575754797057,
    "created_at" : "2015-03-25 18:13:12 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 580794755245838336,
  "created_at" : "2015-03-25 18:13:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/K7RwYgeHKr",
      "expanded_url" : "https:\/\/twitter.com\/turochas\/status\/580792976751747072",
      "display_url" : "twitter.com\/turochas\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580794463452315648",
  "text" : "It 2am Singapore time. People form an orderly queue outside Parliament Building waiting for their turn. https:\/\/t.co\/K7RwYgeHKr",
  "id" : 580794463452315648,
  "created_at" : "2015-03-25 18:12:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ash.",
      "screen_name" : "sergeantashley",
      "indices" : [ 0, 15 ],
      "id_str" : "1372087842",
      "id" : 1372087842
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http:\/\/t.co\/BFjIM13bkk",
      "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/politics\/margaret-thatcher\/10002320\/Paxman-lack-of-hats-means-we-clap-at-funerals.html",
      "display_url" : "telegraph.co.uk\/news\/politics\/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "580778066030436352",
  "geo" : { },
  "id_str" : "580791047275593728",
  "in_reply_to_user_id" : 1372087842,
  "text" : "@sergeantashley clapping is more of a practice in the West. No idea why is done in Singapore. http:\/\/t.co\/BFjIM13bkk",
  "id" : 580791047275593728,
  "in_reply_to_status_id" : 580778066030436352,
  "created_at" : "2015-03-25 17:59:10 +0000",
  "in_reply_to_screen_name" : "sergeantashley",
  "in_reply_to_user_id_str" : "1372087842",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 0, 11 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580787437946974209",
  "geo" : { },
  "id_str" : "580788032334352384",
  "in_reply_to_user_id" : 16738907,
  "text" : "@davegantly Unbelievable a victim of workplace bullying getting dead threat!",
  "id" : 580788032334352384,
  "in_reply_to_status_id" : 580787437946974209,
  "created_at" : "2015-03-25 17:47:12 +0000",
  "in_reply_to_screen_name" : "davegantly",
  "in_reply_to_user_id_str" : "16738907",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 0, 11 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580775511665807360",
  "geo" : { },
  "id_str" : "580786954566021120",
  "in_reply_to_user_id" : 16738907,
  "text" : "@davegantly Done reporting.",
  "id" : 580786954566021120,
  "in_reply_to_status_id" : 580775511665807360,
  "created_at" : "2015-03-25 17:42:55 +0000",
  "in_reply_to_screen_name" : "davegantly",
  "in_reply_to_user_id_str" : "16738907",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anita Byrne",
      "screen_name" : "anitambyrne",
      "indices" : [ 3, 15 ],
      "id_str" : "45226744",
      "id" : 45226744
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TopGear",
      "indices" : [ 111, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580782333529354240",
  "text" : "RT @anitambyrne: The abuse Oisin Tymon is getting is horrendous. The mob are out with a vengeance. Disgusting. #TopGear",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TopGear",
        "indices" : [ 94, 102 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580763608499503104",
    "text" : "The abuse Oisin Tymon is getting is horrendous. The mob are out with a vengeance. Disgusting. #TopGear",
    "id" : 580763608499503104,
    "created_at" : "2015-03-25 16:10:08 +0000",
    "user" : {
      "name" : "Anita Byrne",
      "screen_name" : "anitambyrne",
      "protected" : false,
      "id_str" : "45226744",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037295813800669184\/uOjpcOJo_normal.jpg",
      "id" : 45226744,
      "verified" : false
    }
  },
  "id" : 580782333529354240,
  "created_at" : "2015-03-25 17:24:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/580773026389729281\/photo\/1",
      "indices" : [ 37, 59 ],
      "url" : "http:\/\/t.co\/fafO0VNut8",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CA9R_V3WEAA7b_S.jpg",
      "id_str" : "580773025596968960",
      "id" : 580773025596968960,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA9R_V3WEAA7b_S.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 211,
        "resize" : "fit",
        "w" : 239
      }, {
        "h" : 211,
        "resize" : "fit",
        "w" : 239
      }, {
        "h" : 211,
        "resize" : "fit",
        "w" : 239
      }, {
        "h" : 211,
        "resize" : "fit",
        "w" : 239
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/fafO0VNut8"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580772032524214272",
  "geo" : { },
  "id_str" : "580773026389729281",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest This how I feel like now. http:\/\/t.co\/fafO0VNut8",
  "id" : 580773026389729281,
  "in_reply_to_status_id" : 580772032524214272,
  "created_at" : "2015-03-25 16:47:34 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/580771614016581632\/photo\/1",
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/9XjU1u2cxO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CA9QtIRWQAALjiR.png",
      "id_str" : "580771613198663680",
      "id" : 580771613198663680,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA9QtIRWQAALjiR.png",
      "sizes" : [ {
        "h" : 70,
        "resize" : "fit",
        "w" : 496
      }, {
        "h" : 70,
        "resize" : "fit",
        "w" : 496
      }, {
        "h" : 70,
        "resize" : "crop",
        "w" : 70
      }, {
        "h" : 70,
        "resize" : "fit",
        "w" : 496
      }, {
        "h" : 70,
        "resize" : "fit",
        "w" : 496
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9XjU1u2cxO"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580771614016581632",
  "text" : "Booked our hotels and tickets to Singapore High Commission in London days before and I got this message just now. http:\/\/t.co\/9XjU1u2cxO",
  "id" : 580771614016581632,
  "created_at" : "2015-03-25 16:41:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "smecommunity",
      "indices" : [ 0, 13 ]
    }, {
      "text" : "irishbizparty",
      "indices" : [ 14, 28 ]
    } ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/Z7Kpq0RQL9",
      "expanded_url" : "https:\/\/www.localenterprise.ie\/DublinCity\/Training-Events\/Online-Bookings\/Digital-Marketing-Strategy-Web-Analytics.html",
      "display_url" : "localenterprise.ie\/DublinCity\/Tra\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580686466642276352",
  "text" : "#smecommunity #irishbizparty You can sign up LEO Digital Marketing Strategy &amp; Web Analytics workshop here https:\/\/t.co\/Z7Kpq0RQL9",
  "id" : 580686466642276352,
  "created_at" : "2015-03-25 11:03:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "OnlineHubIE",
      "screen_name" : "OnlineHubIE",
      "indices" : [ 0, 12 ],
      "id_str" : "2284869198",
      "id" : 2284869198
    }, {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 13, 22 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580680400894345216",
  "geo" : { },
  "id_str" : "580683565710630912",
  "in_reply_to_user_id" : 2284869198,
  "text" : "@OnlineHubIE @enormous I like your approach to this.",
  "id" : 580683565710630912,
  "in_reply_to_status_id" : 580680400894345216,
  "created_at" : "2015-03-25 10:52:05 +0000",
  "in_reply_to_screen_name" : "OnlineHubIE",
  "in_reply_to_user_id_str" : "2284869198",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hoo fengmin \u00A9",
      "screen_name" : "Fengminx3",
      "indices" : [ 0, 10 ],
      "id_str" : "94233152",
      "id" : 94233152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580681774843445248",
  "geo" : { },
  "id_str" : "580682396393168896",
  "in_reply_to_user_id" : 94233152,
  "text" : "@Fengminx3 All these companies (including Luasfilm Studio) have a very visible presence in Singapore might explain their tribute.",
  "id" : 580682396393168896,
  "in_reply_to_status_id" : 580681774843445248,
  "created_at" : "2015-03-25 10:47:26 +0000",
  "in_reply_to_screen_name" : "Fengminx3",
  "in_reply_to_user_id_str" : "94233152",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hoo fengmin \u00A9",
      "screen_name" : "Fengminx3",
      "indices" : [ 0, 10 ],
      "id_str" : "94233152",
      "id" : 94233152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http:\/\/t.co\/HyEx8KqcLm",
      "expanded_url" : "http:\/\/mothership.sg\/2015\/03\/beyond-foreign-leaders-international-brands-joined-in-to-pay-tribute-to-lky\/",
      "display_url" : "mothership.sg\/2015\/03\/beyond\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "580386187837239296",
  "geo" : { },
  "id_str" : "580681070913449984",
  "in_reply_to_user_id" : 94233152,
  "text" : "@Fengminx3 I very skeptical when I read your tweets until I google this. http:\/\/t.co\/HyEx8KqcLm",
  "id" : 580681070913449984,
  "in_reply_to_status_id" : 580386187837239296,
  "created_at" : "2015-03-25 10:42:10 +0000",
  "in_reply_to_screen_name" : "Fengminx3",
  "in_reply_to_user_id_str" : "94233152",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 79, 87 ],
      "id_str" : "1652541",
      "id" : 1652541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http:\/\/t.co\/FMyi7x0kGI",
      "expanded_url" : "http:\/\/reut.rs\/1ERsuzC",
      "display_url" : "reut.rs\/1ERsuzC"
    } ]
  },
  "geo" : { },
  "id_str" : "580675733380575232",
  "text" : "In Lee's death, Singapore efficiency shines through http:\/\/t.co\/FMyi7x0kGI via @reuters",
  "id" : 580675733380575232,
  "created_at" : "2015-03-25 10:20:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "Forget boys, get PAID!",
      "screen_name" : "blackadlerqueen",
      "indices" : [ 12, 28 ],
      "id_str" : "15689909",
      "id" : 15689909
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 66 ],
      "url" : "http:\/\/t.co\/PuE1oGJd6Z",
      "expanded_url" : "http:\/\/www.independent.ie\/irish-news\/health\/irish-woman-24-who-travelled-to-uk-for-abortion-i-dont-deserve-to-be-treated-as-a-criminal-30958987.html",
      "display_url" : "independent.ie\/irish-news\/hea\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "580673930157015040",
  "geo" : { },
  "id_str" : "580675047356030976",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG @blackadlerqueen Here the story http:\/\/t.co\/PuE1oGJd6Z",
  "id" : 580675047356030976,
  "in_reply_to_status_id" : 580673930157015040,
  "created_at" : "2015-03-25 10:18:14 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Forget boys, get PAID!",
      "screen_name" : "blackadlerqueen",
      "indices" : [ 0, 16 ],
      "id_str" : "15689909",
      "id" : 15689909
    }, {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 17, 28 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580672685648187392",
  "geo" : { },
  "id_str" : "580672898584715265",
  "in_reply_to_user_id" : 15689909,
  "text" : "@blackadlerqueen @hellofrmSG Yes it is. Not even \"special circumstances abortion\"",
  "id" : 580672898584715265,
  "in_reply_to_status_id" : 580672685648187392,
  "created_at" : "2015-03-25 10:09:42 +0000",
  "in_reply_to_screen_name" : "blackadlerqueen",
  "in_reply_to_user_id_str" : "15689909",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "indices" : [ 3, 16 ],
      "id_str" : "19698834",
      "id" : 19698834
    }, {
      "name" : "Malaysia Airlines",
      "screen_name" : "MAS",
      "indices" : [ 20, 24 ],
      "id_str" : "19106719",
      "id" : 19106719
    }, {
      "name" : "AirAsia",
      "screen_name" : "AirAsia",
      "indices" : [ 26, 34 ],
      "id_str" : "22919665",
      "id" : 22919665
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580671143574704128",
  "text" : "RT @MarketingEds: . @MAS, @AirAsia send out condolence tweets to @germanwings http:\/\/t.co\/YWIYeKJsvd #alwaysinMyHeart",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Malaysia Airlines",
        "screen_name" : "MAS",
        "indices" : [ 2, 6 ],
        "id_str" : "19106719",
        "id" : 19106719
      }, {
        "name" : "AirAsia",
        "screen_name" : "AirAsia",
        "indices" : [ 8, 16 ],
        "id_str" : "22919665",
        "id" : 22919665
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "alwaysinMyHeart",
        "indices" : [ 83, 99 ]
      } ],
      "urls" : [ {
        "indices" : [ 60, 82 ],
        "url" : "http:\/\/t.co\/YWIYeKJsvd",
        "expanded_url" : "http:\/\/nyv.me\/l\/yA4J",
        "display_url" : "nyv.me\/l\/yA4J"
      } ]
    },
    "geo" : { },
    "id_str" : "580670473287127040",
    "text" : ". @MAS, @AirAsia send out condolence tweets to @germanwings http:\/\/t.co\/YWIYeKJsvd #alwaysinMyHeart",
    "id" : 580670473287127040,
    "created_at" : "2015-03-25 10:00:03 +0000",
    "user" : {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "protected" : false,
      "id_str" : "19698834",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948003366164033536\/_ojmapns_normal.jpg",
      "id" : 19698834,
      "verified" : false
    }
  },
  "id" : 580671143574704128,
  "created_at" : "2015-03-25 10:02:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/oxY602vcxD",
      "expanded_url" : "https:\/\/www.facebook.com\/lianhewanbao\/photos\/a.132423140160421.25332.125584034177665\/819459078123487\/?type=1&theater",
      "display_url" : "facebook.com\/lianhewanbao\/p\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "580665422648860673",
  "geo" : { },
  "id_str" : "580668349560053760",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG She was not seen at yesterday Lay-In-State event https:\/\/t.co\/oxY602vcxD",
  "id" : 580668349560053760,
  "in_reply_to_status_id" : 580665422648860673,
  "created_at" : "2015-03-25 09:51:37 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 3, 14 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "www",
      "screen_name" : "www",
      "indices" : [ 89, 93 ],
      "id_str" : "1350901",
      "id" : 1350901
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 84 ],
      "url" : "http:\/\/t.co\/VbmvBxgyGt",
      "expanded_url" : "http:\/\/therealsingapore.com\/content\/lkys-daughter-why-i-choose-remain-single",
      "display_url" : "therealsingapore.com\/content\/lkys-d\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580665605252116482",
  "text" : "RT @hellofrmSG: LKY's daughter: Why I choose to remain single http:\/\/t.co\/VbmvBxgyGt via @www.twitter.com\/realsingapore",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "www",
        "screen_name" : "www",
        "indices" : [ 73, 77 ],
        "id_str" : "1350901",
        "id" : 1350901
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 68 ],
        "url" : "http:\/\/t.co\/VbmvBxgyGt",
        "expanded_url" : "http:\/\/therealsingapore.com\/content\/lkys-daughter-why-i-choose-remain-single",
        "display_url" : "therealsingapore.com\/content\/lkys-d\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "580665033757167617",
    "text" : "LKY's daughter: Why I choose to remain single http:\/\/t.co\/VbmvBxgyGt via @www.twitter.com\/realsingapore",
    "id" : 580665033757167617,
    "created_at" : "2015-03-25 09:38:26 +0000",
    "user" : {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "protected" : false,
      "id_str" : "607156756",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040905442216407040\/xF1JwbXR_normal.jpg",
      "id" : 607156756,
      "verified" : false
    }
  },
  "id" : 580665605252116482,
  "created_at" : "2015-03-25 09:40:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 3, 14 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580663955800092672",
  "text" : "RT @hellofrmSG: The interesting thing about Belfast is that you can be roasting in the sun, but move two steps into the shade and it'd be f\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580657716378648577",
    "text" : "The interesting thing about Belfast is that you can be roasting in the sun, but move two steps into the shade and it'd be frigid.",
    "id" : 580657716378648577,
    "created_at" : "2015-03-25 09:09:22 +0000",
    "user" : {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "protected" : false,
      "id_str" : "607156756",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040905442216407040\/xF1JwbXR_normal.jpg",
      "id" : 607156756,
      "verified" : false
    }
  },
  "id" : 580663955800092672,
  "created_at" : "2015-03-25 09:34:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580663012773744640",
  "text" : "RT @themonaaa18: So not only the Parliament House but now MRTs also are going to be open 24 hours. It's touching. Really touching.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 1.392454816940426, 103.8767248877669 ]
    },
    "id_str" : "580659371178332160",
    "text" : "So not only the Parliament House but now MRTs also are going to be open 24 hours. It's touching. Really touching.",
    "id" : 580659371178332160,
    "created_at" : "2015-03-25 09:15:56 +0000",
    "user" : {
      "name" : "Monalisa",
      "screen_name" : "saltyqueen18",
      "protected" : false,
      "id_str" : "283081030",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977206891901173760\/F0yn1KTn_normal.jpg",
      "id" : 283081030,
      "verified" : false
    }
  },
  "id" : 580663012773744640,
  "created_at" : "2015-03-25 09:30:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580649575830171650",
  "text" : "\"Traction Developer\"?! One of those job titles from a tech company appear on my Timeline",
  "id" : 580649575830171650,
  "created_at" : "2015-03-25 08:37:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0421\u0430\u043D\u0438\u043A\u0430 \u0414\u043C\u0438\u0442\u0440\u0438\u0435\u0432\u043D\u0430",
      "screen_name" : "IFJ_Pat",
      "indices" : [ 3, 11 ],
      "id_str" : "17912128",
      "id" : 17912128
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580648293694337024",
  "text" : "RT @IFJ_Pat: Am I the only one weary of daily headlines of shock &amp; amazement at water charges? It was never free to put into taps",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580635089312358400",
    "text" : "Am I the only one weary of daily headlines of shock &amp; amazement at water charges? It was never free to put into taps",
    "id" : 580635089312358400,
    "created_at" : "2015-03-25 07:39:27 +0000",
    "user" : {
      "name" : "Pat O'Keeffe",
      "screen_name" : "Pat_O_Keeffe",
      "protected" : false,
      "id_str" : "156039005",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005558597714108416\/iSg1FRY3_normal.jpg",
      "id" : 156039005,
      "verified" : false
    }
  },
  "id" : 580648293694337024,
  "created_at" : "2015-03-25 08:31:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore Government",
      "screen_name" : "govsingapore",
      "indices" : [ 3, 16 ],
      "id_str" : "56883209",
      "id" : 56883209
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580648140279279617",
  "text" : "RT @govsingapore: Update: The Lying in State at Parliament House will be open 24 hours daily from today until 28 Mar 2015, 8pm. #Rememberin\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RememberingLKY",
        "indices" : [ 110, 125 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580635791782637568",
    "text" : "Update: The Lying in State at Parliament House will be open 24 hours daily from today until 28 Mar 2015, 8pm. #RememberingLKY",
    "id" : 580635791782637568,
    "created_at" : "2015-03-25 07:42:15 +0000",
    "user" : {
      "name" : "Singapore Government",
      "screen_name" : "govsingapore",
      "protected" : false,
      "id_str" : "56883209",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/822346483076734976\/hSNC5IZC_normal.jpg",
      "id" : 56883209,
      "verified" : true
    }
  },
  "id" : 580648140279279617,
  "created_at" : "2015-03-25 08:31:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "XinHui Lim",
      "screen_name" : "xinnhuii",
      "indices" : [ 3, 12 ],
      "id_str" : "378357942",
      "id" : 378357942
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580647975610900480",
  "text" : "RT @xinnhuii: Remember to give the officials a pat on the back when you see them, they are working very hard to make sure everyone get to s\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580636995107557376",
    "text" : "Remember to give the officials a pat on the back when you see them, they are working very hard to make sure everyone get to see Mr Lee.",
    "id" : 580636995107557376,
    "created_at" : "2015-03-25 07:47:02 +0000",
    "user" : {
      "name" : "XinHui Lim",
      "screen_name" : "xinnhuii",
      "protected" : false,
      "id_str" : "378357942",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1041024986796875776\/pa5GkJuA_normal.jpg",
      "id" : 378357942,
      "verified" : false
    }
  },
  "id" : 580647975610900480,
  "created_at" : "2015-03-25 08:30:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Phillips",
      "screen_name" : "tomphillipsin",
      "indices" : [ 3, 17 ],
      "id_str" : "211948211",
      "id" : 211948211
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/tomphillipsin\/status\/580644593814794240\/photo\/1",
      "indices" : [ 118, 140 ],
      "url" : "http:\/\/t.co\/0crwPiShsw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CA7dLiZWwAAj2KT.png",
      "id_str" : "580644592258826240",
      "id" : 580644592258826240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA7dLiZWwAAj2KT.png",
      "sizes" : [ {
        "h" : 346,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 346,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 346,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 346,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0crwPiShsw"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/mxrwe0qGEA",
      "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/worldnews\/asia\/china\/11493546\/Britain-calls-for-release-of-Chinese-female-activists.html",
      "display_url" : "telegraph.co.uk\/news\/worldnews\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580647442720382976",
  "text" : "RT @tomphillipsin: Britain adds voice to calls for release of Chinese women's rights activists http:\/\/t.co\/mxrwe0qGEA http:\/\/t.co\/0crwPiShsw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tomphillipsin\/status\/580644593814794240\/photo\/1",
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/0crwPiShsw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CA7dLiZWwAAj2KT.png",
        "id_str" : "580644592258826240",
        "id" : 580644592258826240,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA7dLiZWwAAj2KT.png",
        "sizes" : [ {
          "h" : 346,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 346,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 346,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 346,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0crwPiShsw"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 98 ],
        "url" : "http:\/\/t.co\/mxrwe0qGEA",
        "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/worldnews\/asia\/china\/11493546\/Britain-calls-for-release-of-Chinese-female-activists.html",
        "display_url" : "telegraph.co.uk\/news\/worldnews\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "580644593814794240",
    "text" : "Britain adds voice to calls for release of Chinese women's rights activists http:\/\/t.co\/mxrwe0qGEA http:\/\/t.co\/0crwPiShsw",
    "id" : 580644593814794240,
    "created_at" : "2015-03-25 08:17:13 +0000",
    "user" : {
      "name" : "Tom Phillips",
      "screen_name" : "tomphillipsin",
      "protected" : false,
      "id_str" : "211948211",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/718659986637721600\/Qf2J4IKU_normal.jpg",
      "id" : 211948211,
      "verified" : true
    }
  },
  "id" : 580647442720382976,
  "created_at" : "2015-03-25 08:28:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 45 ],
      "url" : "http:\/\/t.co\/KUsHq4PcRn",
      "expanded_url" : "http:\/\/blog.ubernote.com\/2015\/03\/final-note-ubernote-is-shutting-down-on.html",
      "display_url" : "blog.ubernote.com\/2015\/03\/final-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580646514269241344",
  "text" : "Ubernote shutting down http:\/\/t.co\/KUsHq4PcRn",
  "id" : 580646514269241344,
  "created_at" : "2015-03-25 08:24:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 3, 15 ],
      "id_str" : "41085467",
      "id" : 41085467
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 53, 64 ]
    }, {
      "text" : "RememberingLKY",
      "indices" : [ 112, 127 ]
    } ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http:\/\/t.co\/0L3VYs9gam",
      "expanded_url" : "http:\/\/tdy.sg\/1NePVoO",
      "display_url" : "tdy.sg\/1NePVoO"
    } ]
  },
  "geo" : { },
  "id_str" : "580644668221890560",
  "text" : "RT @TODAYonline: Growing lines to pay respects to Mr #LeeKuanYew prompt acts of kindness http:\/\/t.co\/0L3VYs9gam #RememberingLKY http:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/580643849837547521\/photo\/1",
        "indices" : [ 111, 133 ],
        "url" : "http:\/\/t.co\/QQuKKGt7qk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CA7cgN8U0AAhJbz.jpg",
        "id_str" : "580643848033980416",
        "id" : 580643848033980416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA7cgN8U0AAhJbz.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 590,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1245
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1245
        }, {
          "h" : 1041,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QQuKKGt7qk"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 36, 47 ]
      }, {
        "text" : "RememberingLKY",
        "indices" : [ 95, 110 ]
      } ],
      "urls" : [ {
        "indices" : [ 72, 94 ],
        "url" : "http:\/\/t.co\/0L3VYs9gam",
        "expanded_url" : "http:\/\/tdy.sg\/1NePVoO",
        "display_url" : "tdy.sg\/1NePVoO"
      } ]
    },
    "geo" : { },
    "id_str" : "580643849837547521",
    "text" : "Growing lines to pay respects to Mr #LeeKuanYew prompt acts of kindness http:\/\/t.co\/0L3VYs9gam #RememberingLKY http:\/\/t.co\/QQuKKGt7qk",
    "id" : 580643849837547521,
    "created_at" : "2015-03-25 08:14:16 +0000",
    "user" : {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "protected" : false,
      "id_str" : "41085467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/606854638801846272\/pi_RUbVK_normal.png",
      "id" : 41085467,
      "verified" : true
    }
  },
  "id" : 580644668221890560,
  "created_at" : "2015-03-25 08:17:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "ShareThis",
      "screen_name" : "ShareThis",
      "indices" : [ 128, 138 ],
      "id_str" : "14116807",
      "id" : 14116807
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/sI8dKvqYmC",
      "expanded_url" : "http:\/\/shar.es\/1fMUps",
      "display_url" : "shar.es\/1fMUps"
    } ]
  },
  "geo" : { },
  "id_str" : "580644284682137600",
  "text" : "RT @GeoffreyIRL: Interesting analysis by Prof Tommy Koh of Lee Kuan Yew's approach to foreign policy http:\/\/t.co\/sI8dKvqYmC via @sharethis",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "ShareThis",
        "screen_name" : "ShareThis",
        "indices" : [ 111, 121 ],
        "id_str" : "14116807",
        "id" : 14116807
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 106 ],
        "url" : "http:\/\/t.co\/sI8dKvqYmC",
        "expanded_url" : "http:\/\/shar.es\/1fMUps",
        "display_url" : "shar.es\/1fMUps"
      } ]
    },
    "geo" : { },
    "id_str" : "580642938411184128",
    "text" : "Interesting analysis by Prof Tommy Koh of Lee Kuan Yew's approach to foreign policy http:\/\/t.co\/sI8dKvqYmC via @sharethis",
    "id" : 580642938411184128,
    "created_at" : "2015-03-25 08:10:38 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 580644284682137600,
  "created_at" : "2015-03-25 08:15:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Associated Press",
      "screen_name" : "AP",
      "indices" : [ 3, 6 ],
      "id_str" : "51241574",
      "id" : 51241574
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 138 ],
      "url" : "http:\/\/t.co\/XqSFJkd6fs",
      "expanded_url" : "http:\/\/apne.ws\/1GgnMh0",
      "display_url" : "apne.ws\/1GgnMh0"
    } ]
  },
  "geo" : { },
  "id_str" : "580551731056128001",
  "text" : "RT @AP: Liberia's first Ebola patient in weeks might have been infected through sex with a survivor of the disease: http:\/\/t.co\/XqSFJkd6fs",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/XqSFJkd6fs",
        "expanded_url" : "http:\/\/apne.ws\/1GgnMh0",
        "display_url" : "apne.ws\/1GgnMh0"
      } ]
    },
    "geo" : { },
    "id_str" : "580547675004755968",
    "text" : "Liberia's first Ebola patient in weeks might have been infected through sex with a survivor of the disease: http:\/\/t.co\/XqSFJkd6fs",
    "id" : 580547675004755968,
    "created_at" : "2015-03-25 01:52:06 +0000",
    "user" : {
      "name" : "The Associated Press",
      "screen_name" : "AP",
      "protected" : false,
      "id_str" : "51241574",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/461964160838803457\/8z9FImcv_normal.png",
      "id" : 51241574,
      "verified" : true
    }
  },
  "id" : 580551731056128001,
  "created_at" : "2015-03-25 02:08:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580540947576000512",
  "text" : "@avalanchelynn \"Eight pallbearers are made up of Mr Lee's long serving staff.\"",
  "id" : 580540947576000512,
  "created_at" : "2015-03-25 01:25:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RememberingLKY",
      "indices" : [ 93, 108 ]
    }, {
      "text" : "meerkat",
      "indices" : [ 109, 117 ]
    } ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http:\/\/t.co\/YHr2DBaNvK",
      "expanded_url" : "http:\/\/mrk.tv\/1GgnsPg",
      "display_url" : "mrk.tv\/1GgnsPg"
    } ]
  },
  "geo" : { },
  "id_str" : "580540186435670016",
  "text" : "RT @mrbrown: In 8 minutes: I will stream the gun carriage procession live from my spot soon. #RememberingLKY #meerkat http:\/\/t.co\/YHr2DBaNvK",
  "retweeted_status" : {
    "source" : "",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RememberingLKY",
        "indices" : [ 80, 95 ]
      }, {
        "text" : "meerkat",
        "indices" : [ 96, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 105, 127 ],
        "url" : "http:\/\/t.co\/YHr2DBaNvK",
        "expanded_url" : "http:\/\/mrk.tv\/1GgnsPg",
        "display_url" : "mrk.tv\/1GgnsPg"
      } ]
    },
    "geo" : { },
    "id_str" : "580538867700371456",
    "text" : "In 8 minutes: I will stream the gun carriage procession live from my spot soon. #RememberingLKY #meerkat http:\/\/t.co\/YHr2DBaNvK",
    "id" : 580538867700371456,
    "created_at" : "2015-03-25 01:17:06 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 580540186435670016,
  "created_at" : "2015-03-25 01:22:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 22, 38 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/580538004957208577\/photo\/1",
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/9vC3nesnUf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CA58PQoWcAAEr_e.png",
      "id_str" : "580538003581464576",
      "id" : 580538003581464576,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA58PQoWcAAEr_e.png",
      "sizes" : [ {
        "h" : 349,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 702,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 617,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 702,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9vC3nesnUf"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580538004957208577",
  "text" : "I think I will follow @ChannelNewsAsia twitter update feed. Faster than the video streaming. http:\/\/t.co\/9vC3nesnUf",
  "id" : 580538004957208577,
  "created_at" : "2015-03-25 01:13:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580537736857276416",
  "text" : "@avalanchelynn I agreed with you. Why can't they have the plant after National Mourning Week.",
  "id" : 580537736857276416,
  "created_at" : "2015-03-25 01:12:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 0, 16 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580537037704568833",
  "in_reply_to_user_id" : 38400130,
  "text" : "@ChannelNewsAsia twitter update is faster than the video streaming,",
  "id" : 580537037704568833,
  "created_at" : "2015-03-25 01:09:50 +0000",
  "in_reply_to_screen_name" : "ChannelNewsAsia",
  "in_reply_to_user_id_str" : "38400130",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580535012493234176",
  "text" : "@avalanchelynn I find it odd to have the Orchid on a somber occasion",
  "id" : 580535012493234176,
  "created_at" : "2015-03-25 01:01:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "indices" : [ 3, 16 ],
      "id_str" : "14826420",
      "id" : 14826420
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/aN6GnSx2pZ",
      "expanded_url" : "https:\/\/www.thinkwithgoogle.com\/articles\/the-changing-face-b2b-marketing.html",
      "display_url" : "thinkwithgoogle.com\/articles\/the-c\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580530698605465600",
  "text" : "RT @iamkelvinlee: Google article on the changing face of B2B marketing https:\/\/t.co\/aN6GnSx2pZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/aN6GnSx2pZ",
        "expanded_url" : "https:\/\/www.thinkwithgoogle.com\/articles\/the-changing-face-b2b-marketing.html",
        "display_url" : "thinkwithgoogle.com\/articles\/the-c\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "580433053014724609",
    "text" : "Google article on the changing face of B2B marketing https:\/\/t.co\/aN6GnSx2pZ",
    "id" : 580433053014724609,
    "created_at" : "2015-03-24 18:16:38 +0000",
    "user" : {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "protected" : false,
      "id_str" : "14826420",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996378142682923010\/A8QOlqWt_normal.jpg",
      "id" : 14826420,
      "verified" : false
    }
  },
  "id" : 580530698605465600,
  "created_at" : "2015-03-25 00:44:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fitzwilliam College",
      "screen_name" : "FitzwilliamColl",
      "indices" : [ 3, 19 ],
      "id_str" : "505963516",
      "id" : 505963516
    }, {
      "name" : "Fitzwilliam College",
      "screen_name" : "FitzwilliamColl",
      "indices" : [ 46, 62 ],
      "id_str" : "505963516",
      "id" : 505963516
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 102, 113 ]
    } ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/KBu8maBsKE",
      "expanded_url" : "http:\/\/www.fitz.cam.ac.uk\/about\/newsitem-4-248",
      "display_url" : "fitz.cam.ac.uk\/about\/newsitem\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580397038090752000",
  "text" : "RT @FitzwilliamColl: Fitzwilliam College flag @FitzwilliamColl half-mast Monday 23 March in honour of #LeeKuanYew http:\/\/t.co\/KBu8maBsKE ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Fitzwilliam College",
        "screen_name" : "FitzwilliamColl",
        "indices" : [ 25, 41 ],
        "id_str" : "505963516",
        "id" : 505963516
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FitzwilliamColl\/status\/580388040671121409\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/UpTPy1U94V",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CA3z2NRW4AAYyPZ.jpg",
        "id_str" : "580388039601610752",
        "id" : 580388039601610752,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA3z2NRW4AAYyPZ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 300
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UpTPy1U94V"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 81, 92 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/KBu8maBsKE",
        "expanded_url" : "http:\/\/www.fitz.cam.ac.uk\/about\/newsitem-4-248",
        "display_url" : "fitz.cam.ac.uk\/about\/newsitem\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "580388040671121409",
    "text" : "Fitzwilliam College flag @FitzwilliamColl half-mast Monday 23 March in honour of #LeeKuanYew http:\/\/t.co\/KBu8maBsKE http:\/\/t.co\/UpTPy1U94V",
    "id" : 580388040671121409,
    "created_at" : "2015-03-24 15:17:46 +0000",
    "user" : {
      "name" : "Fitzwilliam College",
      "screen_name" : "FitzwilliamColl",
      "protected" : false,
      "id_str" : "505963516",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/540805617653268481\/ZR1fkokN_normal.jpeg",
      "id" : 505963516,
      "verified" : false
    }
  },
  "id" : 580397038090752000,
  "created_at" : "2015-03-24 15:53:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580393523893182464",
  "text" : "RT @mrbrown: A. PLEASE don't call it Black Sunday. \n\nB. You don't need a movement to know to wear black when mourning.\n\nC. Yes, I'm in blac\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580392621559820288",
    "text" : "A. PLEASE don't call it Black Sunday. \n\nB. You don't need a movement to know to wear black when mourning.\n\nC. Yes, I'm in black all week.",
    "id" : 580392621559820288,
    "created_at" : "2015-03-24 15:35:58 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 580393523893182464,
  "created_at" : "2015-03-24 15:39:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/580380579977367552\/photo\/1",
      "indices" : [ 76, 98 ],
      "url" : "http:\/\/t.co\/DiUrwzH3g5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CA3tD8MWUAAmejz.png",
      "id_str" : "580380578953973760",
      "id" : 580380578953973760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CA3tD8MWUAAmejz.png",
      "sizes" : [ {
        "h" : 205,
        "resize" : "fit",
        "w" : 638
      }, {
        "h" : 205,
        "resize" : "fit",
        "w" : 638
      }, {
        "h" : 205,
        "resize" : "fit",
        "w" : 638
      }, {
        "h" : 205,
        "resize" : "fit",
        "w" : 638
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DiUrwzH3g5"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580380579977367552",
  "text" : "This is new to me. When you paste a Twitter link, it present like an image. http:\/\/t.co\/DiUrwzH3g5",
  "id" : 580380579977367552,
  "created_at" : "2015-03-24 14:48:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/4dEYcjkc31",
      "expanded_url" : "https:\/\/twitter.com\/tokefrello\/status\/580323539007496192",
      "display_url" : "twitter.com\/tokefrello\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580379730215596032",
  "text" : "Maybe dog owner can try this? https:\/\/t.co\/4dEYcjkc31",
  "id" : 580379730215596032,
  "created_at" : "2015-03-24 14:44:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580379486945939456",
  "text" : "RT @enormous: Project Manager required to work in creative web and graphic design studio in Dublin Ireland - Pixel Design http:\/\/t.co\/byXpK\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/byXpKOaaEH",
        "expanded_url" : "http:\/\/bit.ly\/1xeoHfZ",
        "display_url" : "bit.ly\/1xeoHfZ"
      } ]
    },
    "geo" : { },
    "id_str" : "580352921117814784",
    "text" : "Project Manager required to work in creative web and graphic design studio in Dublin Ireland - Pixel Design http:\/\/t.co\/byXpKOaaEH",
    "id" : 580352921117814784,
    "created_at" : "2015-03-24 12:58:13 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 580379486945939456,
  "created_at" : "2015-03-24 14:43:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 109, 120 ]
    } ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/zvu80fJYaM",
      "expanded_url" : "http:\/\/mrbrwn.co\/1Hz9M3o",
      "display_url" : "mrbrwn.co\/1Hz9M3o"
    } ]
  },
  "geo" : { },
  "id_str" : "580378454643904513",
  "text" : "RT @mrbrown: How Singapore Fixed Its Affordable Housing Problem http:\/\/t.co\/zvu80fJYaM The housing legacy of #LeeKuanYew",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 96, 107 ]
      } ],
      "urls" : [ {
        "indices" : [ 51, 73 ],
        "url" : "http:\/\/t.co\/zvu80fJYaM",
        "expanded_url" : "http:\/\/mrbrwn.co\/1Hz9M3o",
        "display_url" : "mrbrwn.co\/1Hz9M3o"
      } ]
    },
    "geo" : { },
    "id_str" : "580374927909367808",
    "text" : "How Singapore Fixed Its Affordable Housing Problem http:\/\/t.co\/zvu80fJYaM The housing legacy of #LeeKuanYew",
    "id" : 580374927909367808,
    "created_at" : "2015-03-24 14:25:40 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 580378454643904513,
  "created_at" : "2015-03-24 14:39:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Lee",
      "screen_name" : "AdrianLeeSA",
      "indices" : [ 3, 15 ],
      "id_str" : "33936903",
      "id" : 33936903
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tech",
      "indices" : [ 126, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580375136299286528",
  "text" : "RT @AdrianLeeSA: Did you hear that? EBITDA was doing a jig. \"Google hires 'the most powerful woman on Wall Street' as new CFO #tech http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tech",
        "indices" : [ 109, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/GFreBZxJAC",
        "expanded_url" : "http:\/\/buff.ly\/1CUUJjy",
        "display_url" : "buff.ly\/1CUUJjy"
      } ]
    },
    "geo" : { },
    "id_str" : "580374892748603392",
    "text" : "Did you hear that? EBITDA was doing a jig. \"Google hires 'the most powerful woman on Wall Street' as new CFO #tech http:\/\/t.co\/GFreBZxJAC\"",
    "id" : 580374892748603392,
    "created_at" : "2015-03-24 14:25:31 +0000",
    "user" : {
      "name" : "Adrian Lee",
      "screen_name" : "AdrianLeeSA",
      "protected" : false,
      "id_str" : "33936903",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/807449488793038848\/rQrUoExA_normal.jpg",
      "id" : 33936903,
      "verified" : false
    }
  },
  "id" : 580375136299286528,
  "created_at" : "2015-03-24 14:26:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shannon Tiezzi",
      "screen_name" : "ShannonTiezzi",
      "indices" : [ 3, 17 ],
      "id_str" : "2214810918",
      "id" : 2214810918
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 110, 121 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580367537340334081",
  "text" : "RT @ShannonTiezzi: Taiwan's Ma Ying-jeou makes 1st trip to Singapore since assuming office to pay respects to #LeeKuanYew http:\/\/t.co\/9iaM2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 91, 102 ]
      } ],
      "urls" : [ {
        "indices" : [ 103, 125 ],
        "url" : "http:\/\/t.co\/9iaM2Wqa1R",
        "expanded_url" : "http:\/\/reut.rs\/1HuAjLG",
        "display_url" : "reut.rs\/1HuAjLG"
      } ]
    },
    "geo" : { },
    "id_str" : "580361244600864768",
    "text" : "Taiwan's Ma Ying-jeou makes 1st trip to Singapore since assuming office to pay respects to #LeeKuanYew http:\/\/t.co\/9iaM2Wqa1R",
    "id" : 580361244600864768,
    "created_at" : "2015-03-24 13:31:17 +0000",
    "user" : {
      "name" : "Shannon Tiezzi",
      "screen_name" : "ShannonTiezzi",
      "protected" : false,
      "id_str" : "2214810918",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000790824111\/e355183ad4dd0d1d24b68200c2f7ff1d_normal.jpeg",
      "id" : 2214810918,
      "verified" : false
    }
  },
  "id" : 580367537340334081,
  "created_at" : "2015-03-24 13:56:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580335896433713152",
  "geo" : { },
  "id_str" : "580340552518017025",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG then another break after the few weeks...",
  "id" : 580340552518017025,
  "in_reply_to_status_id" : 580335896433713152,
  "created_at" : "2015-03-24 12:09:04 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u2729 Rachel Khoo \u2729",
      "screen_name" : "rkhooks",
      "indices" : [ 3, 11 ],
      "id_str" : "19883422",
      "id" : 19883422
    }, {
      "name" : "Vegetarian Society",
      "screen_name" : "vegsoc",
      "indices" : [ 45, 52 ],
      "id_str" : "21190085",
      "id" : 21190085
    }, {
      "name" : "Meat Free Week",
      "screen_name" : "MeatFreeWeekOrg",
      "indices" : [ 76, 92 ],
      "id_str" : "979885243",
      "id" : 979885243
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/rkhooks\/status\/580286234192277505\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/TwXahQzcMc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAyYYJZU8AAVgah.png",
      "id_str" : "580005992630382592",
      "id" : 580005992630382592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAyYYJZU8AAVgah.png",
      "sizes" : [ {
        "h" : 649,
        "resize" : "fit",
        "w" : 460
      }, {
        "h" : 649,
        "resize" : "fit",
        "w" : 460
      }, {
        "h" : 649,
        "resize" : "fit",
        "w" : 460
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 649,
        "resize" : "fit",
        "w" : 460
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TwXahQzcMc"
    } ],
    "hashtags" : [ {
      "text" : "getyourvegout",
      "indices" : [ 93, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 75 ],
      "url" : "http:\/\/t.co\/VBKyZdrbIS",
      "expanded_url" : "http:\/\/ow.ly\/KB6l4",
      "display_url" : "ow.ly\/KB6l4"
    } ]
  },
  "geo" : { },
  "id_str" : "580289061987975168",
  "text" : "RT @rkhooks: Lots of useful nutrtion info on @vegsoc http:\/\/t.co\/VBKyZdrbIS @MeatFreeWeekOrg #getyourvegout http:\/\/t.co\/TwXahQzcMc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Vegetarian Society",
        "screen_name" : "vegsoc",
        "indices" : [ 32, 39 ],
        "id_str" : "21190085",
        "id" : 21190085
      }, {
        "name" : "Meat Free Week",
        "screen_name" : "MeatFreeWeekOrg",
        "indices" : [ 63, 79 ],
        "id_str" : "979885243",
        "id" : 979885243
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rkhooks\/status\/580286234192277505\/photo\/1",
        "indices" : [ 95, 117 ],
        "url" : "http:\/\/t.co\/TwXahQzcMc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAyYYJZU8AAVgah.png",
        "id_str" : "580005992630382592",
        "id" : 580005992630382592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAyYYJZU8AAVgah.png",
        "sizes" : [ {
          "h" : 649,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 649,
          "resize" : "fit",
          "w" : 460
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/TwXahQzcMc"
      } ],
      "hashtags" : [ {
        "text" : "getyourvegout",
        "indices" : [ 80, 94 ]
      } ],
      "urls" : [ {
        "indices" : [ 40, 62 ],
        "url" : "http:\/\/t.co\/VBKyZdrbIS",
        "expanded_url" : "http:\/\/ow.ly\/KB6l4",
        "display_url" : "ow.ly\/KB6l4"
      } ]
    },
    "geo" : { },
    "id_str" : "580286234192277505",
    "text" : "Lots of useful nutrtion info on @vegsoc http:\/\/t.co\/VBKyZdrbIS @MeatFreeWeekOrg #getyourvegout http:\/\/t.co\/TwXahQzcMc",
    "id" : 580286234192277505,
    "created_at" : "2015-03-24 08:33:14 +0000",
    "user" : {
      "name" : "\u2729 Rachel Khoo \u2729",
      "screen_name" : "rkhooks",
      "protected" : false,
      "id_str" : "19883422",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659366236548108288\/G9h0MWn0_normal.jpg",
      "id" : 19883422,
      "verified" : true
    }
  },
  "id" : 580289061987975168,
  "created_at" : "2015-03-24 08:44:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "indices" : [ 3, 11 ],
      "id_str" : "14213600",
      "id" : 14213600
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580275392784760832",
  "text" : "RT @paulmah: Great tips at Festival of Media Asia Pacific as senior marketers share how they leverage social media to influence the bottom \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "foma15",
        "indices" : [ 131, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579818874301849601",
    "text" : "Great tips at Festival of Media Asia Pacific as senior marketers share how they leverage social media to influence the bottom line #foma15",
    "id" : 579818874301849601,
    "created_at" : "2015-03-23 01:36:06 +0000",
    "user" : {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "protected" : false,
      "id_str" : "14213600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882186001157832705\/gOEbAj77_normal.jpg",
      "id" : 14213600,
      "verified" : true
    }
  },
  "id" : 580275392784760832,
  "created_at" : "2015-03-24 07:50:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander Sommer",
      "screen_name" : "alexanderwear",
      "indices" : [ 3, 17 ],
      "id_str" : "26582238",
      "id" : 26582238
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Wearables",
      "indices" : [ 105, 115 ]
    }, {
      "text" : "IoT",
      "indices" : [ 116, 120 ]
    } ],
    "urls" : [ {
      "indices" : [ 82, 104 ],
      "url" : "http:\/\/t.co\/7qUHEFMVQt",
      "expanded_url" : "http:\/\/goo.gl\/rStkjz",
      "display_url" : "goo.gl\/rStkjz"
    } ]
  },
  "geo" : { },
  "id_str" : "580271031237103616",
  "text" : "RT @alexanderwear: Hitachi Developed A Wearable Device That Can Measure Happiness http:\/\/t.co\/7qUHEFMVQt #Wearables #IoT http:\/\/t.co\/gzjQXD\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/alexanderwear\/status\/579908345353543680\/photo\/1",
        "indices" : [ 102, 124 ],
        "url" : "http:\/\/t.co\/gzjQXDDzfd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAw_kUZUQAAjdmM.png",
        "id_str" : "579908345206685696",
        "id" : 579908345206685696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAw_kUZUQAAjdmM.png",
        "sizes" : [ {
          "h" : 589,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 401,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 589,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 589,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gzjQXDDzfd"
      } ],
      "hashtags" : [ {
        "text" : "Wearables",
        "indices" : [ 86, 96 ]
      }, {
        "text" : "IoT",
        "indices" : [ 97, 101 ]
      } ],
      "urls" : [ {
        "indices" : [ 63, 85 ],
        "url" : "http:\/\/t.co\/7qUHEFMVQt",
        "expanded_url" : "http:\/\/goo.gl\/rStkjz",
        "display_url" : "goo.gl\/rStkjz"
      } ]
    },
    "geo" : { },
    "id_str" : "579908345353543680",
    "text" : "Hitachi Developed A Wearable Device That Can Measure Happiness http:\/\/t.co\/7qUHEFMVQt #Wearables #IoT http:\/\/t.co\/gzjQXDDzfd",
    "id" : 579908345353543680,
    "created_at" : "2015-03-23 07:31:38 +0000",
    "user" : {
      "name" : "Alexander Sommer",
      "screen_name" : "alexanderwear",
      "protected" : false,
      "id_str" : "26582238",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/474528624423337986\/h_fR67Fs_normal.jpeg",
      "id" : 26582238,
      "verified" : false
    }
  },
  "id" : 580271031237103616,
  "created_at" : "2015-03-24 07:32:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SingaporeHCLondon",
      "screen_name" : "SHCLon",
      "indices" : [ 14, 21 ],
      "id_str" : "613311242",
      "id" : 613311242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580154541355732993",
  "text" : "Going down to @SHCLon this Friday to pay respects to the late Mr LKY on behalf of 72 Singaporeans in Ireland.",
  "id" : 580154541355732993,
  "created_at" : "2015-03-23 23:49:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aer Lingus",
      "screen_name" : "AerLingus",
      "indices" : [ 0, 10 ],
      "id_str" : "281046179",
      "id" : 281046179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "580054939478695937",
  "geo" : { },
  "id_str" : "580138229216776192",
  "in_reply_to_user_id" : 281046179,
  "text" : "@AerLingus How close to the departure time to checking in to avoid paying seat fees? It helps if this is clearly communicate on your website",
  "id" : 580138229216776192,
  "in_reply_to_status_id" : 580054939478695937,
  "created_at" : "2015-03-23 22:45:06 +0000",
  "in_reply_to_screen_name" : "AerLingus",
  "in_reply_to_user_id_str" : "281046179",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "indices" : [ 3, 11 ],
      "id_str" : "137327916",
      "id" : 137327916
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/bSh0MG0Lgm",
      "expanded_url" : "https:\/\/www.psychologicalscience.org\/index.php\/news\/minds-business\/too-much-workplace-positivity-might-dampen-employee-motivation.html",
      "display_url" : "psychologicalscience.org\/index.php\/news\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "580047271477305344",
  "text" : "RT @GECinD8: Too Much Workplace Positivity Might Dampen Employee Motivation\n\nhttps:\/\/t.co\/bSh0MG0Lgm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/bSh0MG0Lgm",
        "expanded_url" : "https:\/\/www.psychologicalscience.org\/index.php\/news\/minds-business\/too-much-workplace-positivity-might-dampen-employee-motivation.html",
        "display_url" : "psychologicalscience.org\/index.php\/news\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "580009159115960320",
    "text" : "Too Much Workplace Positivity Might Dampen Employee Motivation\n\nhttps:\/\/t.co\/bSh0MG0Lgm",
    "id" : 580009159115960320,
    "created_at" : "2015-03-23 14:12:14 +0000",
    "user" : {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "protected" : false,
      "id_str" : "137327916",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014899460806234119\/K16b7185_normal.jpg",
      "id" : 137327916,
      "verified" : false
    }
  },
  "id" : 580047271477305344,
  "created_at" : "2015-03-23 16:43:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Cohen",
      "screen_name" : "asmartbear",
      "indices" : [ 3, 14 ],
      "id_str" : "17838032",
      "id" : 17838032
    }, {
      "name" : "DHH",
      "screen_name" : "dhh",
      "indices" : [ 68, 72 ],
      "id_str" : "14561327",
      "id" : 14561327
    }, {
      "name" : "Jason Fried",
      "screen_name" : "jasonfried",
      "indices" : [ 73, 84 ],
      "id_str" : "14372143",
      "id" : 14372143
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/asmartbear\/status\/580012892713562112\/photo\/1",
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/nBhDXNtV3p",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAyepxPW4AAq_D4.jpg",
      "id_str" : "580012892453527552",
      "id" : 580012892453527552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAyepxPW4AAq_D4.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1000
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nBhDXNtV3p"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "580018276870287361",
  "text" : "RT @asmartbear: The New Yorker strikes with an awesome cartoon. \/cc @dhh @jasonfried http:\/\/t.co\/nBhDXNtV3p",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DHH",
        "screen_name" : "dhh",
        "indices" : [ 52, 56 ],
        "id_str" : "14561327",
        "id" : 14561327
      }, {
        "name" : "Jason Fried",
        "screen_name" : "jasonfried",
        "indices" : [ 57, 68 ],
        "id_str" : "14372143",
        "id" : 14372143
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/asmartbear\/status\/580012892713562112\/photo\/1",
        "indices" : [ 69, 91 ],
        "url" : "http:\/\/t.co\/nBhDXNtV3p",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAyepxPW4AAq_D4.jpg",
        "id_str" : "580012892453527552",
        "id" : 580012892453527552,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAyepxPW4AAq_D4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nBhDXNtV3p"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "580012892713562112",
    "text" : "The New Yorker strikes with an awesome cartoon. \/cc @dhh @jasonfried http:\/\/t.co\/nBhDXNtV3p",
    "id" : 580012892713562112,
    "created_at" : "2015-03-23 14:27:04 +0000",
    "user" : {
      "name" : "Jason Cohen",
      "screen_name" : "asmartbear",
      "protected" : false,
      "id_str" : "17838032",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000614606612\/0ab75c5a2506c93ec1b6c393511ec79c_normal.png",
      "id" : 17838032,
      "verified" : true
    }
  },
  "id" : 580018276870287361,
  "created_at" : "2015-03-23 14:48:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Google",
      "indices" : [ 11, 18 ]
    }, {
      "text" : "LeeKuanYew",
      "indices" : [ 50, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 100, 122 ],
      "url" : "http:\/\/t.co\/Z3VFz32jAf",
      "expanded_url" : "http:\/\/bit.ly\/1FQRNUb",
      "display_url" : "bit.ly\/1FQRNUb"
    } ]
  },
  "geo" : { },
  "id_str" : "580008037831835648",
  "text" : "RT @STcom: #Google pays tribute to the life of Mr #LeeKuanYew with simple black ribbon on home page http:\/\/t.co\/Z3VFz32jAf http:\/\/t.co\/K29l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STForeignDesk\/status\/579929733036519424\/photo\/1",
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/K29leCFLBP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAxTAgoUcAAPytB.jpg",
        "id_str" : "579929720248102912",
        "id" : 579929720248102912,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAxTAgoUcAAPytB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 387,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 740
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 740
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 740
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/K29leCFLBP"
      } ],
      "hashtags" : [ {
        "text" : "Google",
        "indices" : [ 0, 7 ]
      }, {
        "text" : "LeeKuanYew",
        "indices" : [ 39, 50 ]
      } ],
      "urls" : [ {
        "indices" : [ 89, 111 ],
        "url" : "http:\/\/t.co\/Z3VFz32jAf",
        "expanded_url" : "http:\/\/bit.ly\/1FQRNUb",
        "display_url" : "bit.ly\/1FQRNUb"
      } ]
    },
    "geo" : { },
    "id_str" : "579930041489887232",
    "text" : "#Google pays tribute to the life of Mr #LeeKuanYew with simple black ribbon on home page http:\/\/t.co\/Z3VFz32jAf http:\/\/t.co\/K29leCFLBP",
    "id" : 579930041489887232,
    "created_at" : "2015-03-23 08:57:51 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 580008037831835648,
  "created_at" : "2015-03-23 14:07:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Grehan",
      "screen_name" : "mikegrehan",
      "indices" : [ 3, 14 ],
      "id_str" : "10218832",
      "id" : 10218832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http:\/\/t.co\/zVNAvbvDiy",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/newsbeat\/32013708",
      "display_url" : "bbc.co.uk\/newsbeat\/32013\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "579975726893531137",
  "text" : "RT @mikegrehan: Taylor Swift buys .porn domain name http:\/\/t.co\/zVNAvbvDiy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 36, 58 ],
        "url" : "http:\/\/t.co\/zVNAvbvDiy",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/newsbeat\/32013708",
        "display_url" : "bbc.co.uk\/newsbeat\/32013\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "579973043633672192",
    "text" : "Taylor Swift buys .porn domain name http:\/\/t.co\/zVNAvbvDiy",
    "id" : 579973043633672192,
    "created_at" : "2015-03-23 11:48:43 +0000",
    "user" : {
      "name" : "Mike Grehan",
      "screen_name" : "mikegrehan",
      "protected" : false,
      "id_str" : "10218832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/725039960433250305\/UzjzJW72_normal.jpg",
      "id" : 10218832,
      "verified" : false
    }
  },
  "id" : 579975726893531137,
  "created_at" : "2015-03-23 11:59:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fake Lee",
      "screen_name" : "Fake_HarryLee",
      "indices" : [ 3, 17 ],
      "id_str" : "35216436",
      "id" : 35216436
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Fake_HarryLee\/status\/579802036117512192\/photo\/1",
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/YCzT4knHsd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAvex2yUsAA1Xxf.jpg",
      "id_str" : "579801925148782592",
      "id" : 579801925148782592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAvex2yUsAA1Xxf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YCzT4knHsd"
    } ],
    "hashtags" : [ {
      "text" : "Singaporean",
      "indices" : [ 19, 31 ]
    }, {
      "text" : "RememberingLKY",
      "indices" : [ 70, 85 ]
    }, {
      "text" : "ThankYouLKY",
      "indices" : [ 86, 98 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579951734543552512",
  "text" : "RT @Fake_HarryLee: #Singaporean truly grateful . All numbers sold out #RememberingLKY #ThankYouLKY http:\/\/t.co\/YCzT4knHsd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Fake_HarryLee\/status\/579802036117512192\/photo\/1",
        "indices" : [ 80, 102 ],
        "url" : "http:\/\/t.co\/YCzT4knHsd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAvex2yUsAA1Xxf.jpg",
        "id_str" : "579801925148782592",
        "id" : 579801925148782592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAvex2yUsAA1Xxf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YCzT4knHsd"
      } ],
      "hashtags" : [ {
        "text" : "Singaporean",
        "indices" : [ 0, 12 ]
      }, {
        "text" : "RememberingLKY",
        "indices" : [ 51, 66 ]
      }, {
        "text" : "ThankYouLKY",
        "indices" : [ 67, 79 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579802036117512192",
    "text" : "#Singaporean truly grateful . All numbers sold out #RememberingLKY #ThankYouLKY http:\/\/t.co\/YCzT4knHsd",
    "id" : 579802036117512192,
    "created_at" : "2015-03-23 00:29:12 +0000",
    "user" : {
      "name" : "Fake Lee",
      "screen_name" : "Fake_HarryLee",
      "protected" : false,
      "id_str" : "35216436",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875519781499043841\/n7s62aCM_normal.jpg",
      "id" : 35216436,
      "verified" : false
    }
  },
  "id" : 579951734543552512,
  "created_at" : "2015-03-23 10:24:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "indices" : [ 3, 11 ],
      "id_str" : "14213600",
      "id" : 14213600
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579951335233216512",
  "text" : "RT @paulmah: Listening to Dean Dacko, head of marketing &amp; products, Malaysia Airlines (MAS) share about how his team navigate the twin disa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579816643850678272",
    "text" : "Listening to Dean Dacko, head of marketing &amp; products, Malaysia Airlines (MAS) share about how his team navigate the twin disasters in 2014",
    "id" : 579816643850678272,
    "created_at" : "2015-03-23 01:27:15 +0000",
    "user" : {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "protected" : false,
      "id_str" : "14213600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882186001157832705\/gOEbAj77_normal.jpg",
      "id" : 14213600,
      "verified" : true
    }
  },
  "id" : 579951335233216512,
  "created_at" : "2015-03-23 10:22:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "William Pesek",
      "screen_name" : "WilliamPesek",
      "indices" : [ 3, 16 ],
      "id_str" : "185248181",
      "id" : 185248181
    }, {
      "name" : "William Pesek",
      "screen_name" : "WilliamPesek",
      "indices" : [ 69, 82 ],
      "id_str" : "185248181",
      "id" : 185248181
    }, {
      "name" : "Bloomberg View",
      "screen_name" : "BV",
      "indices" : [ 112, 115 ],
      "id_str" : "991740256293711874",
      "id" : 991740256293711874
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/4rFXc3YfbD",
      "expanded_url" : "http:\/\/www.bloombergview.com\/articles\/2015-03-23\/after-lee-kuan-yew-singapore-needs-an-economic-overhaul",
      "display_url" : "bloombergview.com\/articles\/2015-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "579834863387303936",
  "text" : "RT @WilliamPesek: After Lee, Singapore economy needs a rethink, says @WilliamPesek:  http:\/\/t.co\/4rFXc3YfbD via @BV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "William Pesek",
        "screen_name" : "WilliamPesek",
        "indices" : [ 51, 64 ],
        "id_str" : "185248181",
        "id" : 185248181
      }, {
        "name" : "Bloomberg View",
        "screen_name" : "BV",
        "indices" : [ 94, 97 ],
        "id_str" : "991740256293711874",
        "id" : 991740256293711874
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/4rFXc3YfbD",
        "expanded_url" : "http:\/\/www.bloombergview.com\/articles\/2015-03-23\/after-lee-kuan-yew-singapore-needs-an-economic-overhaul",
        "display_url" : "bloombergview.com\/articles\/2015-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "579831434124247040",
    "text" : "After Lee, Singapore economy needs a rethink, says @WilliamPesek:  http:\/\/t.co\/4rFXc3YfbD via @BV",
    "id" : 579831434124247040,
    "created_at" : "2015-03-23 02:26:01 +0000",
    "user" : {
      "name" : "William Pesek",
      "screen_name" : "WilliamPesek",
      "protected" : false,
      "id_str" : "185248181",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/786487207150235648\/ektfr2KL_normal.jpg",
      "id" : 185248181,
      "verified" : true
    }
  },
  "id" : 579834863387303936,
  "created_at" : "2015-03-23 02:39:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Changi Airport",
      "screen_name" : "FansofChangi",
      "indices" : [ 0, 13 ],
      "id_str" : "365857264",
      "id" : 365857264
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579833066874499072",
  "geo" : { },
  "id_str" : "579833726084988929",
  "in_reply_to_user_id" : 65268724,
  "text" : "@FansofChangi He used to say he will keep Changi Airport instead of SIA. (if I remember)",
  "id" : 579833726084988929,
  "in_reply_to_status_id" : 579833066874499072,
  "created_at" : "2015-03-23 02:35:07 +0000",
  "in_reply_to_screen_name" : "ChangiAirport",
  "in_reply_to_user_id_str" : "65268724",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wynthia Goh",
      "screen_name" : "verydeepsleep",
      "indices" : [ 3, 17 ],
      "id_str" : "14509984",
      "id" : 14509984
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FOMA15",
      "indices" : [ 110, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579830455991668736",
  "text" : "RT @verydeepsleep: Bravo to CMO of Huawei. For criticising the crude use of women as sex objects in Marketing #FOMA15",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "FOMA15",
        "indices" : [ 91, 98 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579828745457967104",
    "text" : "Bravo to CMO of Huawei. For criticising the crude use of women as sex objects in Marketing #FOMA15",
    "id" : 579828745457967104,
    "created_at" : "2015-03-23 02:15:20 +0000",
    "user" : {
      "name" : "Wynthia Goh",
      "screen_name" : "verydeepsleep",
      "protected" : false,
      "id_str" : "14509984",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/716496888296394753\/0vgRjYd0_normal.jpg",
      "id" : 14509984,
      "verified" : false
    }
  },
  "id" : 579830455991668736,
  "created_at" : "2015-03-23 02:22:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RIPLKY",
      "indices" : [ 109, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579828824306778112",
  "text" : "On FB, hanging around with some Singaporeans in Ireland. Lots of us can't sleep. Lots on things on our mind. #RIPLKY",
  "id" : 579828824306778112,
  "created_at" : "2015-03-23 02:15:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/flickr.com\/services\/twitter\/\" rel=\"nofollow\"\u003EFlickr\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/llw4FEpnUI",
      "expanded_url" : "https:\/\/flic.kr\/p\/6dsCbx",
      "display_url" : "flic.kr\/p\/6dsCbx"
    } ]
  },
  "geo" : { },
  "id_str" : "579825729900277760",
  "text" : "One of my favorite photo of him. https:\/\/t.co\/llw4FEpnUI",
  "id" : 579825729900277760,
  "created_at" : "2015-03-23 02:03:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Audrey Tan",
      "screen_name" : "audreytan",
      "indices" : [ 3, 13 ],
      "id_str" : "15770959",
      "id" : 15770959
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/iOvXW0s0dm",
      "expanded_url" : "https:\/\/instagram.com\/p\/0jVlmgPf7q\/",
      "display_url" : "instagram.com\/p\/0jVlmgPf7q\/"
    } ]
  },
  "geo" : { },
  "id_str" : "579823304392994816",
  "text" : "RT @audreytan: Eternal rest grant unto Mr Lee Kwan Yew.. The work of your hands have left us a legacy of a fruitful\u2026 https:\/\/t.co\/iOvXW0s0dm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/iOvXW0s0dm",
        "expanded_url" : "https:\/\/instagram.com\/p\/0jVlmgPf7q\/",
        "display_url" : "instagram.com\/p\/0jVlmgPf7q\/"
      } ]
    },
    "geo" : { },
    "id_str" : "579820250348318720",
    "text" : "Eternal rest grant unto Mr Lee Kwan Yew.. The work of your hands have left us a legacy of a fruitful\u2026 https:\/\/t.co\/iOvXW0s0dm",
    "id" : 579820250348318720,
    "created_at" : "2015-03-23 01:41:34 +0000",
    "user" : {
      "name" : "Audrey Tan",
      "screen_name" : "audreytan",
      "protected" : false,
      "id_str" : "15770959",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879656875561631744\/0xBiwpIu_normal.jpg",
      "id" : 15770959,
      "verified" : false
    }
  },
  "id" : 579823304392994816,
  "created_at" : "2015-03-23 01:53:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AspireAsia",
      "screen_name" : "AspireAsia",
      "indices" : [ 3, 14 ],
      "id_str" : "318582782",
      "id" : 318582782
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/579801070140035072\/photo\/1",
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/wHrEiOfrvb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAveAE7WEAEvYlU.png",
      "id_str" : "579801069951258625",
      "id" : 579801069951258625,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAveAE7WEAEvYlU.png",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 429
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 757
      }, {
        "h" : 1886,
        "resize" : "fit",
        "w" : 1190
      }, {
        "h" : 1886,
        "resize" : "fit",
        "w" : 1190
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wHrEiOfrvb"
    } ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 16, 27 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 30, 40 ]
    } ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/gizykD9BMr",
      "expanded_url" : "http:\/\/econ.st\/1EGYgze",
      "display_url" : "econ.st\/1EGYgze"
    } ]
  },
  "geo" : { },
  "id_str" : "579819843664416768",
  "text" : "RT @AspireAsia: #LeeKuanYew's #Singapore: An astonishing record http:\/\/t.co\/gizykD9BMr http:\/\/t.co\/wHrEiOfrvb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android Tablets\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/579801070140035072\/photo\/1",
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/wHrEiOfrvb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAveAE7WEAEvYlU.png",
        "id_str" : "579801069951258625",
        "id" : 579801069951258625,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAveAE7WEAEvYlU.png",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 429
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 757
        }, {
          "h" : 1886,
          "resize" : "fit",
          "w" : 1190
        }, {
          "h" : 1886,
          "resize" : "fit",
          "w" : 1190
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wHrEiOfrvb"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 0, 11 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 14, 24 ]
      } ],
      "urls" : [ {
        "indices" : [ 48, 70 ],
        "url" : "http:\/\/t.co\/gizykD9BMr",
        "expanded_url" : "http:\/\/econ.st\/1EGYgze",
        "display_url" : "econ.st\/1EGYgze"
      } ]
    },
    "geo" : { },
    "id_str" : "579817840817000449",
    "text" : "#LeeKuanYew's #Singapore: An astonishing record http:\/\/t.co\/gizykD9BMr http:\/\/t.co\/wHrEiOfrvb",
    "id" : 579817840817000449,
    "created_at" : "2015-03-23 01:32:00 +0000",
    "user" : {
      "name" : "AspireAsia",
      "screen_name" : "AspireAsia",
      "protected" : false,
      "id_str" : "318582782",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/702103952951611392\/BRT3ji8O_normal.png",
      "id" : 318582782,
      "verified" : false
    }
  },
  "id" : 579819843664416768,
  "created_at" : "2015-03-23 01:39:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Min-Liang Tan",
      "screen_name" : "minliangtan",
      "indices" : [ 3, 15 ],
      "id_str" : "26906309",
      "id" : 26906309
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/minliangtan\/status\/579802155218939906\/photo\/1",
      "indices" : [ 30, 52 ],
      "url" : "http:\/\/t.co\/R2VJPY5NOf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAve-rkVAAEJm-8.jpg",
      "id_str" : "579802145475592193",
      "id" : 579802145475592193,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAve-rkVAAEJm-8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/R2VJPY5NOf"
    } ],
    "hashtags" : [ {
      "text" : "RememberLKY",
      "indices" : [ 17, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579806981747269632",
  "text" : "RT @minliangtan: #RememberLKY http:\/\/t.co\/R2VJPY5NOf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/minliangtan\/status\/579802155218939906\/photo\/1",
        "indices" : [ 13, 35 ],
        "url" : "http:\/\/t.co\/R2VJPY5NOf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAve-rkVAAEJm-8.jpg",
        "id_str" : "579802145475592193",
        "id" : 579802145475592193,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAve-rkVAAEJm-8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/R2VJPY5NOf"
      } ],
      "hashtags" : [ {
        "text" : "RememberLKY",
        "indices" : [ 0, 12 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579802155218939906",
    "text" : "#RememberLKY http:\/\/t.co\/R2VJPY5NOf",
    "id" : 579802155218939906,
    "created_at" : "2015-03-23 00:29:40 +0000",
    "user" : {
      "name" : "Min-Liang Tan",
      "screen_name" : "minliangtan",
      "protected" : false,
      "id_str" : "26906309",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/530479661294551040\/PQf8P493_normal.jpeg",
      "id" : 26906309,
      "verified" : true
    }
  },
  "id" : 579806981747269632,
  "created_at" : "2015-03-23 00:48:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fake Lee",
      "screen_name" : "Fake_HarryLee",
      "indices" : [ 3, 17 ],
      "id_str" : "35216436",
      "id" : 35216436
    }, {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "indices" : [ 20, 28 ],
      "id_str" : "115624161",
      "id" : 115624161
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 109, 119 ]
    }, {
      "text" : "RIPLKY",
      "indices" : [ 121, 128 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579800881597661184",
  "text" : "RT @Fake_HarryLee: \u201C@YahooSG: LHL: \"Singapore was his abiding passion. He gave of himself in full measure to #Singapore\" #RIPLKY http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yahoo Singapore",
        "screen_name" : "YahooSG",
        "indices" : [ 1, 9 ],
        "id_str" : "115624161",
        "id" : 115624161
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/YahooSG\/status\/579796400893898752\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/GU4xHklg9m",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAvZiDzUcAA09Ed.png",
        "id_str" : "579796156206575616",
        "id" : 579796156206575616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAvZiDzUcAA09Ed.png",
        "sizes" : [ {
          "h" : 381,
          "resize" : "fit",
          "w" : 672
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 672
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 672
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 672
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GU4xHklg9m"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 90, 100 ]
      }, {
        "text" : "RIPLKY",
        "indices" : [ 102, 109 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "579796400893898752",
    "geo" : { },
    "id_str" : "579800083325042688",
    "in_reply_to_user_id" : 115624161,
    "text" : "\u201C@YahooSG: LHL: \"Singapore was his abiding passion. He gave of himself in full measure to #Singapore\" #RIPLKY http:\/\/t.co\/GU4xHklg9m\u201D",
    "id" : 579800083325042688,
    "in_reply_to_status_id" : 579796400893898752,
    "created_at" : "2015-03-23 00:21:26 +0000",
    "in_reply_to_screen_name" : "YahooSG",
    "in_reply_to_user_id_str" : "115624161",
    "user" : {
      "name" : "Fake Lee",
      "screen_name" : "Fake_HarryLee",
      "protected" : false,
      "id_str" : "35216436",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875519781499043841\/n7s62aCM_normal.jpg",
      "id" : 35216436,
      "verified" : false
    }
  },
  "id" : 579800881597661184,
  "created_at" : "2015-03-23 00:24:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "indices" : [ 3, 15 ],
      "id_str" : "15191926",
      "id" : 15191926
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RIPLKY",
      "indices" : [ 80, 87 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579798252628926465",
  "text" : "RT @clementchio: I'm sadder than I thought I would be. Strangely still tearing. #RIPLKY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RIPLKY",
        "indices" : [ 63, 70 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579792776067358720",
    "text" : "I'm sadder than I thought I would be. Strangely still tearing. #RIPLKY",
    "id" : 579792776067358720,
    "created_at" : "2015-03-22 23:52:24 +0000",
    "user" : {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "protected" : false,
      "id_str" : "15191926",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/582481791899971584\/UZf4Qfb2_normal.jpg",
      "id" : 15191926,
      "verified" : false
    }
  },
  "id" : 579798252628926465,
  "created_at" : "2015-03-23 00:14:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Respect",
      "indices" : [ 91, 99 ]
    }, {
      "text" : "LKY",
      "indices" : [ 100, 104 ]
    }, {
      "text" : "RIP",
      "indices" : [ 105, 109 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 110, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579788499836092416",
  "text" : "RT @damacfad: Goodbye Mr. Lee Kuan Yew. You were a towering leader and ahead of your time. #Respect #LKY #RIP #Singapore",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Respect",
        "indices" : [ 77, 85 ]
      }, {
        "text" : "LKY",
        "indices" : [ 86, 90 ]
      }, {
        "text" : "RIP",
        "indices" : [ 91, 95 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 96, 106 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579788048000368640",
    "text" : "Goodbye Mr. Lee Kuan Yew. You were a towering leader and ahead of your time. #Respect #LKY #RIP #Singapore",
    "id" : 579788048000368640,
    "created_at" : "2015-03-22 23:33:37 +0000",
    "user" : {
      "name" : "D\u00E9agl\u00E1n MacPh\u00E1id\u00EDn",
      "screen_name" : "declanmacfadden",
      "protected" : false,
      "id_str" : "19288130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684614991492743168\/Rup-iIUi_normal.jpg",
      "id" : 19288130,
      "verified" : false
    }
  },
  "id" : 579788499836092416,
  "created_at" : "2015-03-22 23:35:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579786674013650944",
  "text" : "\"I have never understood why Western educationalists are so much against corporal punishment\" - Lee Kuan Yew",
  "id" : 579786674013650944,
  "created_at" : "2015-03-22 23:28:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579780586790871040",
  "text" : "Thank You Mr, Lee Kuan Yew",
  "id" : 579780586790871040,
  "created_at" : "2015-03-22 23:03:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Hsien Loong",
      "screen_name" : "leehsienloong",
      "indices" : [ 3, 17 ],
      "id_str" : "34568673",
      "id" : 34568673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/sOHFeN7T5p",
      "expanded_url" : "https:\/\/www.facebook.com\/leehsienloong\/posts\/880956571967050",
      "display_url" : "facebook.com\/leehsienloong\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "579779301861662721",
  "text" : "RT @leehsienloong: PM Lee has declared a period of National Mourning from 23 March to 29 March. https:\/\/t.co\/sOHFeN7T5p",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/sOHFeN7T5p",
        "expanded_url" : "https:\/\/www.facebook.com\/leehsienloong\/posts\/880956571967050",
        "display_url" : "facebook.com\/leehsienloong\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "579778874260606976",
    "text" : "PM Lee has declared a period of National Mourning from 23 March to 29 March. https:\/\/t.co\/sOHFeN7T5p",
    "id" : 579778874260606976,
    "created_at" : "2015-03-22 22:57:10 +0000",
    "user" : {
      "name" : "Lee Hsien Loong",
      "screen_name" : "leehsienloong",
      "protected" : false,
      "id_str" : "34568673",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040783094607863808\/j9Pm25dJ_normal.jpg",
      "id" : 34568673,
      "verified" : true
    }
  },
  "id" : 579779301861662721,
  "created_at" : "2015-03-22 22:58:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/579705296404291584\/photo\/1",
      "indices" : [ 76, 98 ],
      "url" : "http:\/\/t.co\/geZNB1S0IV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAuG5RVWgAAEy58.png",
      "id_str" : "579705295510863872",
      "id" : 579705295510863872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAuG5RVWgAAEy58.png",
      "sizes" : [ {
        "h" : 284,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 1301
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 1301
      }, {
        "h" : 501,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/geZNB1S0IV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579705296404291584",
  "text" : "I am using Pinterest purely as a visual bookmarketing tool. What about you? http:\/\/t.co\/geZNB1S0IV",
  "id" : 579705296404291584,
  "created_at" : "2015-03-22 18:04:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BrianHonan",
      "screen_name" : "BrianHonan",
      "indices" : [ 3, 14 ],
      "id_str" : "14277681",
      "id" : 14277681
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579654591941328896",
  "text" : "RT @BrianHonan: Congrats to the Irish ladies rugby team on winning the six nations. A great end to a great weekend for Irish rugby :)",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579653834051555328",
    "text" : "Congrats to the Irish ladies rugby team on winning the six nations. A great end to a great weekend for Irish rugby :)",
    "id" : 579653834051555328,
    "created_at" : "2015-03-22 14:40:18 +0000",
    "user" : {
      "name" : "BrianHonan",
      "screen_name" : "BrianHonan",
      "protected" : false,
      "id_str" : "14277681",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1254742442\/Brian_Honan_normal.jpg",
      "id" : 14277681,
      "verified" : true
    }
  },
  "id" : 579654591941328896,
  "created_at" : "2015-03-22 14:43:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AmnestyInternational",
      "screen_name" : "amnesty",
      "indices" : [ 3, 11 ],
      "id_str" : "18213483",
      "id" : 18213483
    }, {
      "name" : "Saudi Embassy",
      "screen_name" : "SaudiEmbassyUSA",
      "indices" : [ 66, 82 ],
      "id_str" : "120150558",
      "id" : 120150558
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Saudi",
      "indices" : [ 24, 30 ]
    }, {
      "text" : "FreeRaif",
      "indices" : [ 84, 93 ]
    }, {
      "text" : "AmnestyM2M",
      "indices" : [ 118, 129 ]
    } ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/8Ta4YDCtRd",
      "expanded_url" : "http:\/\/owl.li\/H0Ijz",
      "display_url" : "owl.li\/H0Ijz"
    } ]
  },
  "geo" : { },
  "id_str" : "579654529261637633",
  "text" : "RT @amnesty: RT to show #Saudi authorities the world is watching: @SaudiEmbassyUSA, #FreeRaif! http:\/\/t.co\/8Ta4YDCtRd #AmnestyM2M http:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Saudi Embassy",
        "screen_name" : "SaudiEmbassyUSA",
        "indices" : [ 53, 69 ],
        "id_str" : "120150558",
        "id" : 120150558
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/amnesty\/status\/579634542513922048\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/Ypv9MdQAHj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAtGZhOVEAEvR_K.png",
        "id_str" : "579634381276385281",
        "id" : 579634381276385281,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAtGZhOVEAEvR_K.png",
        "sizes" : [ {
          "h" : 815,
          "resize" : "fit",
          "w" : 815
        }, {
          "h" : 815,
          "resize" : "fit",
          "w" : 815
        }, {
          "h" : 815,
          "resize" : "fit",
          "w" : 815
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ypv9MdQAHj"
      } ],
      "hashtags" : [ {
        "text" : "Saudi",
        "indices" : [ 11, 17 ]
      }, {
        "text" : "FreeRaif",
        "indices" : [ 71, 80 ]
      }, {
        "text" : "AmnestyM2M",
        "indices" : [ 105, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 82, 104 ],
        "url" : "http:\/\/t.co\/8Ta4YDCtRd",
        "expanded_url" : "http:\/\/owl.li\/H0Ijz",
        "display_url" : "owl.li\/H0Ijz"
      } ]
    },
    "geo" : { },
    "id_str" : "579634542513922048",
    "text" : "RT to show #Saudi authorities the world is watching: @SaudiEmbassyUSA, #FreeRaif! http:\/\/t.co\/8Ta4YDCtRd #AmnestyM2M http:\/\/t.co\/Ypv9MdQAHj",
    "id" : 579634542513922048,
    "created_at" : "2015-03-22 13:23:38 +0000",
    "user" : {
      "name" : "Amnesty International",
      "screen_name" : "amnestyusa",
      "protected" : false,
      "id_str" : "16153562",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006615853293432832\/1vtsDoqb_normal.jpg",
      "id" : 16153562,
      "verified" : true
    }
  },
  "id" : 579654529261637633,
  "created_at" : "2015-03-22 14:43:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579654292048625665",
  "text" : "Love the music from Minecraft. Sounds like those New Age thing that elevates your soul and mind to a higher level. The state of nirvana.",
  "id" : 579654292048625665,
  "created_at" : "2015-03-22 14:42:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikael Hagstrom",
      "screen_name" : "MikaelHagstrom",
      "indices" : [ 3, 18 ],
      "id_str" : "590205203",
      "id" : 590205203
    }, {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 132, 139 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IoT",
      "indices" : [ 114, 118 ]
    }, {
      "text" : "bigdata",
      "indices" : [ 119, 127 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/k9DlvruHHK",
      "expanded_url" : "http:\/\/2.sas.com\/60102DQs",
      "display_url" : "2.sas.com\/60102DQs"
    } ]
  },
  "geo" : { },
  "id_str" : "579653719937191937",
  "text" : "RT @MikaelHagstrom: Singapore aims to be a smart nation, a goal that calls for analytics: \nhttp:\/\/t.co\/k9DlvruHHK #IoT #bigdata via @Forbes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Forbes",
        "screen_name" : "Forbes",
        "indices" : [ 112, 119 ],
        "id_str" : "91478624",
        "id" : 91478624
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IoT",
        "indices" : [ 94, 98 ]
      }, {
        "text" : "bigdata",
        "indices" : [ 99, 107 ]
      } ],
      "urls" : [ {
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/k9DlvruHHK",
        "expanded_url" : "http:\/\/2.sas.com\/60102DQs",
        "display_url" : "2.sas.com\/60102DQs"
      } ]
    },
    "geo" : { },
    "id_str" : "576786115182612480",
    "text" : "Singapore aims to be a smart nation, a goal that calls for analytics: \nhttp:\/\/t.co\/k9DlvruHHK #IoT #bigdata via @Forbes",
    "id" : 576786115182612480,
    "created_at" : "2015-03-14 16:45:00 +0000",
    "user" : {
      "name" : "Mikael Hagstrom",
      "screen_name" : "MikaelHagstrom",
      "protected" : false,
      "id_str" : "590205203",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/540140867768446976\/YE9FvWJB_normal.jpeg",
      "id" : 590205203,
      "verified" : false
    }
  },
  "id" : 579653719937191937,
  "created_at" : "2015-03-22 14:39:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bloomberg",
      "screen_name" : "business",
      "indices" : [ 76, 85 ],
      "id_str" : "34713362",
      "id" : 34713362
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http:\/\/t.co\/lvafTs3d2E",
      "expanded_url" : "http:\/\/bloom.bg\/1xmHshE",
      "display_url" : "bloom.bg\/1xmHshE"
    } ]
  },
  "geo" : { },
  "id_str" : "579647040394760193",
  "text" : "Obamacare Is Spurring Startups and Creating Jobs http:\/\/t.co\/lvafTs3d2E via @business",
  "id" : 579647040394760193,
  "created_at" : "2015-03-22 14:13:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathias Rehnman",
      "screen_name" : "ThatNuttyFanboy",
      "indices" : [ 0, 16 ],
      "id_str" : "598433882",
      "id" : 598433882
    }, {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 17, 28 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579592159235461120",
  "geo" : { },
  "id_str" : "579602107092885504",
  "in_reply_to_user_id" : 598433882,
  "text" : "@ThatNuttyFanboy @hellofrmSG what if 2 days later your situation might deteriorate and ends up in A&amp;E and need to hospitalise for a week?",
  "id" : 579602107092885504,
  "in_reply_to_status_id" : 579592159235461120,
  "created_at" : "2015-03-22 11:14:45 +0000",
  "in_reply_to_screen_name" : "ThatNuttyFanboy",
  "in_reply_to_user_id_str" : "598433882",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "indices" : [ 3, 16 ],
      "id_str" : "367008160",
      "id" : 367008160
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/managerspeak\/status\/579224963292463104\/photo\/1",
      "indices" : [ 104, 126 ],
      "url" : "http:\/\/t.co\/MnkTWfeiCn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAnSCMQWcAAl-0o.jpg",
      "id_str" : "579224962185195520",
      "id" : 579224962185195520,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAnSCMQWcAAl-0o.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 746,
        "resize" : "fit",
        "w" : 540
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 492
      }, {
        "h" : 746,
        "resize" : "fit",
        "w" : 540
      }, {
        "h" : 746,
        "resize" : "fit",
        "w" : 540
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MnkTWfeiCn"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579601529235238912",
  "text" : "RT @managerspeak: Personal FAQs are reducing casual conversations and increasing efficiency. Excellent. http:\/\/t.co\/MnkTWfeiCn",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/managerspeak\/status\/579224963292463104\/photo\/1",
        "indices" : [ 86, 108 ],
        "url" : "http:\/\/t.co\/MnkTWfeiCn",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAnSCMQWcAAl-0o.jpg",
        "id_str" : "579224962185195520",
        "id" : 579224962185195520,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAnSCMQWcAAl-0o.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 746,
          "resize" : "fit",
          "w" : 540
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 492
        }, {
          "h" : 746,
          "resize" : "fit",
          "w" : 540
        }, {
          "h" : 746,
          "resize" : "fit",
          "w" : 540
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MnkTWfeiCn"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579224963292463104",
    "text" : "Personal FAQs are reducing casual conversations and increasing efficiency. Excellent. http:\/\/t.co\/MnkTWfeiCn",
    "id" : 579224963292463104,
    "created_at" : "2015-03-21 10:16:07 +0000",
    "user" : {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "protected" : false,
      "id_str" : "367008160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3689160554\/8dc837089c6fa2aa3348074a6b1e8ec1_normal.jpeg",
      "id" : 367008160,
      "verified" : false
    }
  },
  "id" : 579601529235238912,
  "created_at" : "2015-03-22 11:12:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30A2\u30A4\u30EB\u30E9\u30F3\u30C9\u5927\u4F7F\u9928IrishEmbJPN",
      "screen_name" : "IrishEmbJapan",
      "indices" : [ 3, 17 ],
      "id_str" : "473696456",
      "id" : 473696456
    }, {
      "name" : "Corvil",
      "screen_name" : "CorvilInc",
      "indices" : [ 115, 125 ],
      "id_str" : "1548510974",
      "id" : 1548510974
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Howlin",
      "indices" : [ 24, 31 ]
    }, {
      "text" : "IFS2020",
      "indices" : [ 126, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579590920007499776",
  "text" : "RT @IrishEmbJapan: MPER #Howlin at TSE,  the world's 3rd largest Stock Exchange, which uses monitoring systems of  @CorvilInc #IFS2020 http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Corvil",
        "screen_name" : "CorvilInc",
        "indices" : [ 96, 106 ],
        "id_str" : "1548510974",
        "id" : 1548510974
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishEmbJapan\/status\/577645309167153153\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/H529c6bh5l",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAQ1WNXUQAAV_iI.jpg",
        "id_str" : "577645307871117312",
        "id" : 577645307871117312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAQ1WNXUQAAV_iI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 377,
          "resize" : "fit",
          "w" : 322
        }, {
          "h" : 377,
          "resize" : "fit",
          "w" : 322
        }, {
          "h" : 377,
          "resize" : "fit",
          "w" : 322
        }, {
          "h" : 377,
          "resize" : "fit",
          "w" : 322
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/H529c6bh5l"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IrishEmbJapan\/status\/577645309167153153\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/H529c6bh5l",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAQ1WL4UMAAJxsJ.jpg",
        "id_str" : "577645307472654336",
        "id" : 577645307472654336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAQ1WL4UMAAJxsJ.jpg",
        "sizes" : [ {
          "h" : 227,
          "resize" : "fit",
          "w" : 295
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 227,
          "resize" : "fit",
          "w" : 295
        }, {
          "h" : 227,
          "resize" : "fit",
          "w" : 295
        }, {
          "h" : 227,
          "resize" : "fit",
          "w" : 295
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/H529c6bh5l"
      } ],
      "hashtags" : [ {
        "text" : "Howlin",
        "indices" : [ 5, 12 ]
      }, {
        "text" : "IFS2020",
        "indices" : [ 107, 115 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577645309167153153",
    "text" : "MPER #Howlin at TSE,  the world's 3rd largest Stock Exchange, which uses monitoring systems of  @CorvilInc #IFS2020 http:\/\/t.co\/H529c6bh5l",
    "id" : 577645309167153153,
    "created_at" : "2015-03-17 01:39:08 +0000",
    "user" : {
      "name" : "\u30A2\u30A4\u30EB\u30E9\u30F3\u30C9\u5927\u4F7F\u9928IrishEmbJPN",
      "screen_name" : "IrishEmbJapan",
      "protected" : false,
      "id_str" : "473696456",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023846013960839168\/8UhwDvXO_normal.jpg",
      "id" : 473696456,
      "verified" : true
    }
  },
  "id" : 579590920007499776,
  "created_at" : "2015-03-22 10:30:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Travis Teo",
      "screen_name" : "scenz",
      "indices" : [ 3, 9 ],
      "id_str" : "24850339",
      "id" : 24850339
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ThankYouLKY",
      "indices" : [ 59, 71 ]
    }, {
      "text" : "LeeKuanYew",
      "indices" : [ 72, 83 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http:\/\/t.co\/tz9H7VQo5P",
      "expanded_url" : "http:\/\/whereverimayroam.com\/2015\/03\/22\/sir-thanks-for-everything-really-everything\/",
      "display_url" : "whereverimayroam.com\/2015\/03\/22\/sir\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "579552059864813569",
  "text" : "RT @scenz: Sir, thanks for everything, really everything.. #ThankYouLKY #LeeKuanYew http:\/\/t.co\/tz9H7VQo5P",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ThankYouLKY",
        "indices" : [ 48, 60 ]
      }, {
        "text" : "LeeKuanYew",
        "indices" : [ 61, 72 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/tz9H7VQo5P",
        "expanded_url" : "http:\/\/whereverimayroam.com\/2015\/03\/22\/sir-thanks-for-everything-really-everything\/",
        "display_url" : "whereverimayroam.com\/2015\/03\/22\/sir\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "579477481553416192",
    "text" : "Sir, thanks for everything, really everything.. #ThankYouLKY #LeeKuanYew http:\/\/t.co\/tz9H7VQo5P",
    "id" : 579477481553416192,
    "created_at" : "2015-03-22 02:59:32 +0000",
    "user" : {
      "name" : "Travis Teo",
      "screen_name" : "scenz",
      "protected" : false,
      "id_str" : "24850339",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992574774051196929\/rS9h-HJD_normal.jpg",
      "id" : 24850339,
      "verified" : false
    }
  },
  "id" : 579552059864813569,
  "created_at" : "2015-03-22 07:55:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/vbYux1pvQC",
      "expanded_url" : "https:\/\/justgetflux.com\/",
      "display_url" : "justgetflux.com"
    } ]
  },
  "geo" : { },
  "id_str" : "579352958837633024",
  "text" : "Software that adjusts the color temperature of PC screen to keeps you alert daytime &amp; prepare you for nighttime. https:\/\/t.co\/vbYux1pvQC",
  "id" : 579352958837633024,
  "created_at" : "2015-03-21 18:44:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lynn Fisher",
      "screen_name" : "lynnandtonic",
      "indices" : [ 0, 13 ],
      "id_str" : "27965538",
      "id" : 27965538
    }, {
      "name" : "The Captain \u2388",
      "screen_name" : "nickcrohn",
      "indices" : [ 14, 24 ],
      "id_str" : "6323192",
      "id" : 6323192
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579350541605707777",
  "in_reply_to_user_id" : 27965538,
  "text" : "@lynnandtonic @nickcrohn 3 letter codes for Penang International Airport - PEN",
  "id" : 579350541605707777,
  "created_at" : "2015-03-21 18:35:07 +0000",
  "in_reply_to_screen_name" : "lynnandtonic",
  "in_reply_to_user_id_str" : "27965538",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Souchak",
      "screen_name" : "johnnsb",
      "indices" : [ 3, 11 ],
      "id_str" : "38586323",
      "id" : 38586323
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579349182793797632",
  "text" : "RT @johnnsb: Does anyone use Google Analytics to measure the success of online training? Love to chat about goals and best practices",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578261274003255296",
    "text" : "Does anyone use Google Analytics to measure the success of online training? Love to chat about goals and best practices",
    "id" : 578261274003255296,
    "created_at" : "2015-03-18 18:26:45 +0000",
    "user" : {
      "name" : "John Souchak",
      "screen_name" : "johnnsb",
      "protected" : false,
      "id_str" : "38586323",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/575290516222316544\/52RiifjZ_normal.png",
      "id" : 38586323,
      "verified" : false
    }
  },
  "id" : 579349182793797632,
  "created_at" : "2015-03-21 18:29:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "Lynn Fisher",
      "screen_name" : "lynnandtonic",
      "indices" : [ 26, 39 ],
      "id_str" : "27965538",
      "id" : 27965538
    }, {
      "name" : "The Captain \u2388",
      "screen_name" : "nickcrohn",
      "indices" : [ 44, 54 ],
      "id_str" : "6323192",
      "id" : 6323192
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579346700323672064",
  "geo" : { },
  "id_str" : "579347680440279040",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG You can tweet @lynnandtonic and @nickcrohn on the missing  Belfast's 2 airports",
  "id" : 579347680440279040,
  "in_reply_to_status_id" : 579346700323672064,
  "created_at" : "2015-03-21 18:23:45 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http:\/\/t.co\/weURUk5tJA",
      "expanded_url" : "http:\/\/airportcod.es\/#airport\/sin",
      "display_url" : "airportcod.es\/#airport\/sin"
    } ]
  },
  "geo" : { },
  "id_str" : "579343548857901056",
  "text" : "Making sense of those three-letter airport codes. SIN: http:\/\/t.co\/weURUk5tJA",
  "id" : 579343548857901056,
  "created_at" : "2015-03-21 18:07:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sky News",
      "screen_name" : "SkyNews",
      "indices" : [ 3, 11 ],
      "id_str" : "7587032",
      "id" : 7587032
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/ptbw0Wd6Kh",
      "expanded_url" : "http:\/\/trib.al\/ytAOCG8",
      "display_url" : "trib.al\/ytAOCG8"
    } ]
  },
  "geo" : { },
  "id_str" : "579343245991391232",
  "text" : "RT @SkyNews: Parents of IS runaways should take \"prime responsibility\", one of Britain's top policemen says http:\/\/t.co\/ptbw0Wd6Kh http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SkyNews\/status\/579316123759472640\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/CKA2zQEyPu",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAok8fVWsAM5YBm.jpg",
        "id_str" : "579316123692347395",
        "id" : 579316123692347395,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAok8fVWsAM5YBm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CKA2zQEyPu"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 117 ],
        "url" : "http:\/\/t.co\/ptbw0Wd6Kh",
        "expanded_url" : "http:\/\/trib.al\/ytAOCG8",
        "display_url" : "trib.al\/ytAOCG8"
      } ]
    },
    "geo" : { },
    "id_str" : "579316123759472640",
    "text" : "Parents of IS runaways should take \"prime responsibility\", one of Britain's top policemen says http:\/\/t.co\/ptbw0Wd6Kh http:\/\/t.co\/CKA2zQEyPu",
    "id" : 579316123759472640,
    "created_at" : "2015-03-21 16:18:21 +0000",
    "user" : {
      "name" : "Sky News",
      "screen_name" : "SkyNews",
      "protected" : false,
      "id_str" : "7587032",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/953341660813021185\/o2IGXWqj_normal.jpg",
      "id" : 7587032,
      "verified" : true
    }
  },
  "id" : 579343245991391232,
  "created_at" : "2015-03-21 18:06:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 127 ],
      "url" : "http:\/\/t.co\/JPAfLUi0LZ",
      "expanded_url" : "http:\/\/lumixlx100.tumblr.com\/",
      "display_url" : "lumixlx100.tumblr.com"
    } ]
  },
  "geo" : { },
  "id_str" : "579342462940004352",
  "text" : "I used to have this rule not to buy a camera that manufacture my microwave oven until I meet Lumix LX100 http:\/\/t.co\/JPAfLUi0LZ",
  "id" : 579342462940004352,
  "created_at" : "2015-03-21 18:03:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579338529920172032",
  "geo" : { },
  "id_str" : "579341580571709440",
  "in_reply_to_user_id" : 20548160,
  "text" : "@SimonPRepublic start singing God Save the Queen and yell \"let crush the rebel\"",
  "id" : 579341580571709440,
  "in_reply_to_status_id" : 579338529920172032,
  "created_at" : "2015-03-21 17:59:31 +0000",
  "in_reply_to_screen_name" : "SimonJohnPalmer",
  "in_reply_to_user_id_str" : "20548160",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Biz News & Sporela",
      "screen_name" : "welovelky",
      "indices" : [ 0, 10 ],
      "id_str" : "3101869585",
      "id" : 3101869585
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579321435673493504",
  "in_reply_to_user_id" : 3101869585,
  "text" : "@welovelky The portrait of LKY at your Twitter a\/c made him looks like a nice guy. He is someone you do not want to mess with.  :)",
  "id" : 579321435673493504,
  "created_at" : "2015-03-21 16:39:28 +0000",
  "in_reply_to_screen_name" : "welovelky",
  "in_reply_to_user_id_str" : "3101869585",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The WorldPost",
      "screen_name" : "TheWorldPost",
      "indices" : [ 49, 62 ],
      "id_str" : "26566469",
      "id" : 26566469
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http:\/\/t.co\/39QFIDjNiV",
      "expanded_url" : "http:\/\/huff.to\/1FJJlWC",
      "display_url" : "huff.to\/1FJJlWC"
    } ]
  },
  "geo" : { },
  "id_str" : "579319166768930816",
  "text" : "Lee Kuan Yew's Legacy http:\/\/t.co\/39QFIDjNiV via @theworldpost",
  "id" : 579319166768930816,
  "created_at" : "2015-03-21 16:30:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "u n r e a l",
      "screen_name" : "ponnoiyee",
      "indices" : [ 0, 10 ],
      "id_str" : "345150509",
      "id" : 345150509
    }, {
      "name" : "hellofrmsg\/\/ \u201Cfrhn\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 11, 22 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "Jinhua Kuek \u90ED\u8FDB\u534E",
      "screen_name" : "kuekj",
      "indices" : [ 23, 29 ],
      "id_str" : "288041858",
      "id" : 288041858
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579310844867010561",
  "geo" : { },
  "id_str" : "579311764329926657",
  "in_reply_to_user_id" : 345150509,
  "text" : "@ponnoiyee @hellofrmSG @kuekj the coastal route of west Ireland is breath taking!",
  "id" : 579311764329926657,
  "in_reply_to_status_id" : 579310844867010561,
  "created_at" : "2015-03-21 16:01:02 +0000",
  "in_reply_to_screen_name" : "ponnoiyee",
  "in_reply_to_user_id_str" : "345150509",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aldi Ireland",
      "screen_name" : "Aldi_Ireland",
      "indices" : [ 3, 16 ],
      "id_str" : "2377180886",
      "id" : 2377180886
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Aldi",
      "indices" : [ 87, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579310798843088897",
  "text" : "RT @Aldi_Ireland: And that\u2019s number three over the line! The battle for our third \u20AC100 #Aldi voucher starts\u2026..now! Retweet for your chance \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Aldi",
        "indices" : [ 69, 74 ]
      }, {
        "text" : "IREvSCO",
        "indices" : [ 129, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579306629923004418",
    "text" : "And that\u2019s number three over the line! The battle for our third \u20AC100 #Aldi voucher starts\u2026..now! Retweet for your chance to win! #IREvSCO",
    "id" : 579306629923004418,
    "created_at" : "2015-03-21 15:40:38 +0000",
    "user" : {
      "name" : "Aldi Ireland",
      "screen_name" : "Aldi_Ireland",
      "protected" : false,
      "id_str" : "2377180886",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/902885452356583429\/z7AFHw46_normal.jpg",
      "id" : 2377180886,
      "verified" : true
    }
  },
  "id" : 579310798843088897,
  "created_at" : "2015-03-21 15:57:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Edwards",
      "screen_name" : "JustinEducation",
      "indices" : [ 3, 19 ],
      "id_str" : "398266555",
      "id" : 398266555
    }, {
      "name" : "\u262FCoderDojo\u262F",
      "screen_name" : "CoderDojo",
      "indices" : [ 21, 31 ],
      "id_str" : "329816616",
      "id" : 329816616
    }, {
      "name" : "mary dunphy moloney",
      "screen_name" : "marydunph",
      "indices" : [ 32, 42 ],
      "id_str" : "133226044",
      "id" : 133226044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579310634443124736",
  "text" : "RT @JustinEducation: @CoderDojo @marydunph current smallbasic group finished. New groups starts 18\/04 9.30 code from zero with pc +child ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\u262FCoderDojo\u262F",
        "screen_name" : "CoderDojo",
        "indices" : [ 0, 10 ],
        "id_str" : "329816616",
        "id" : 329816616
      }, {
        "name" : "mary dunphy moloney",
        "screen_name" : "marydunph",
        "indices" : [ 11, 21 ],
        "id_str" : "133226044",
        "id" : 133226044
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JustinEducation\/status\/579276762158333952\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/UXYV5sIRtV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAoBItfXIAA-KL4.jpg",
        "id_str" : "579276751232245760",
        "id" : 579276751232245760,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAoBItfXIAA-KL4.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UXYV5sIRtV"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579276762158333952",
    "in_reply_to_user_id" : 329816616,
    "text" : "@CoderDojo @marydunph current smallbasic group finished. New groups starts 18\/04 9.30 code from zero with pc +child http:\/\/t.co\/UXYV5sIRtV",
    "id" : 579276762158333952,
    "created_at" : "2015-03-21 13:41:57 +0000",
    "in_reply_to_screen_name" : "CoderDojo",
    "in_reply_to_user_id_str" : "329816616",
    "user" : {
      "name" : "Justin Edwards",
      "screen_name" : "JustinEducation",
      "protected" : false,
      "id_str" : "398266555",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665604499961200640\/QG3F5VI2_normal.jpg",
      "id" : 398266555,
      "verified" : false
    }
  },
  "id" : 579310634443124736,
  "created_at" : "2015-03-21 15:56:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "u n r e a l",
      "screen_name" : "ponnoiyee",
      "indices" : [ 12, 22 ],
      "id_str" : "345150509",
      "id" : 345150509
    }, {
      "name" : "Jinhua Kuek \u90ED\u8FDB\u534E",
      "screen_name" : "kuekj",
      "indices" : [ 23, 29 ],
      "id_str" : "288041858",
      "id" : 288041858
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579310063367659520",
  "geo" : { },
  "id_str" : "579310529690394624",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG @ponnoiyee @kuekj what your literary like? Planning for a trip to west of Ireland.",
  "id" : 579310529690394624,
  "in_reply_to_status_id" : 579310063367659520,
  "created_at" : "2015-03-21 15:56:08 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brilliant Ads",
      "screen_name" : "Brilliant_Ads",
      "indices" : [ 3, 17 ],
      "id_str" : "564686965",
      "id" : 564686965
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Brilliant_Ads\/status\/579274520252530689\/photo\/1",
      "indices" : [ 29, 51 ],
      "url" : "http:\/\/t.co\/uHn66b7YCf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAn_GpgW4AAesCu.jpg",
      "id_str" : "579274516779687936",
      "id" : 579274516779687936,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAn_GpgW4AAesCu.jpg",
      "sizes" : [ {
        "h" : 423,
        "resize" : "fit",
        "w" : 458
      }, {
        "h" : 423,
        "resize" : "fit",
        "w" : 458
      }, {
        "h" : 423,
        "resize" : "fit",
        "w" : 458
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 423,
        "resize" : "fit",
        "w" : 458
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uHn66b7YCf"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579307405663711232",
  "text" : "RT @Brilliant_Ads: Age test: http:\/\/t.co\/uHn66b7YCf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Brilliant_Ads\/status\/579274520252530689\/photo\/1",
        "indices" : [ 10, 32 ],
        "url" : "http:\/\/t.co\/uHn66b7YCf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAn_GpgW4AAesCu.jpg",
        "id_str" : "579274516779687936",
        "id" : 579274516779687936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAn_GpgW4AAesCu.jpg",
        "sizes" : [ {
          "h" : 423,
          "resize" : "fit",
          "w" : 458
        }, {
          "h" : 423,
          "resize" : "fit",
          "w" : 458
        }, {
          "h" : 423,
          "resize" : "fit",
          "w" : 458
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 423,
          "resize" : "fit",
          "w" : 458
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uHn66b7YCf"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579274520252530689",
    "text" : "Age test: http:\/\/t.co\/uHn66b7YCf",
    "id" : 579274520252530689,
    "created_at" : "2015-03-21 13:33:02 +0000",
    "user" : {
      "name" : "Brilliant Ads",
      "screen_name" : "Brilliant_Ads",
      "protected" : false,
      "id_str" : "564686965",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/568757608471736320\/HVxk-qDA_normal.jpeg",
      "id" : 564686965,
      "verified" : false
    }
  },
  "id" : 579307405663711232,
  "created_at" : "2015-03-21 15:43:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abigail Smith",
      "screen_name" : "abigailsong",
      "indices" : [ 3, 15 ],
      "id_str" : "119061468",
      "id" : 119061468
    }, {
      "name" : "Near FM",
      "screen_name" : "nearfm",
      "indices" : [ 27, 34 ],
      "id_str" : "18759980",
      "id" : 18759980
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "579307290295201792",
  "text" : "RT @abigailsong: Tune into @nearfm on 90.3Fm TODAY between 3pm and 4pm today. Chatting to Declan Doherty about musical influences http:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Near FM",
        "screen_name" : "nearfm",
        "indices" : [ 10, 17 ],
        "id_str" : "18759980",
        "id" : 18759980
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/abigailsong\/status\/579238686145060865\/photo\/1",
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/xXPGMac6Qf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAnegdJWAAEeBDq.jpg",
        "id_str" : "579238676254818305",
        "id" : 579238676254818305,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAnegdJWAAEeBDq.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 814,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 814,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 541,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 814,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xXPGMac6Qf"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "579238686145060865",
    "text" : "Tune into @nearfm on 90.3Fm TODAY between 3pm and 4pm today. Chatting to Declan Doherty about musical influences http:\/\/t.co\/xXPGMac6Qf",
    "id" : 579238686145060865,
    "created_at" : "2015-03-21 11:10:39 +0000",
    "user" : {
      "name" : "Abigail Smith",
      "screen_name" : "abigailsong",
      "protected" : false,
      "id_str" : "119061468",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/568807860709388288\/yoTza23v_normal.jpeg",
      "id" : 119061468,
      "verified" : false
    }
  },
  "id" : 579307290295201792,
  "created_at" : "2015-03-21 15:43:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "Jinhua Kuek \u90ED\u8FDB\u534E",
      "screen_name" : "kuekj",
      "indices" : [ 12, 18 ],
      "id_str" : "288041858",
      "id" : 288041858
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "579299318802288640",
  "geo" : { },
  "id_str" : "579305365436805120",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG @kuekj just occur to me that next week is my turn?",
  "id" : 579305365436805120,
  "in_reply_to_status_id" : 579299318802288640,
  "created_at" : "2015-03-21 15:35:36 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ashley judd",
      "screen_name" : "AshleyJudd",
      "indices" : [ 3, 14 ],
      "id_str" : "248747209",
      "id" : 248747209
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http:\/\/t.co\/VLb2MZZWM3",
      "expanded_url" : "http:\/\/cjky.it\/1C6nJ5C",
      "display_url" : "cjky.it\/1C6nJ5C"
    } ]
  },
  "geo" : { },
  "id_str" : "579200326915473408",
  "text" : "RT @AshleyJudd: Thank you for sharing your experience. Time to turn back the toxic tide of social media hate http:\/\/t.co\/VLb2MZZWM3 via @co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Courier Journal",
        "screen_name" : "courierjournal",
        "indices" : [ 120, 135 ],
        "id_str" : "7972552",
        "id" : 7972552
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/VLb2MZZWM3",
        "expanded_url" : "http:\/\/cjky.it\/1C6nJ5C",
        "display_url" : "cjky.it\/1C6nJ5C"
      } ]
    },
    "geo" : { },
    "id_str" : "579158402460491776",
    "text" : "Thank you for sharing your experience. Time to turn back the toxic tide of social media hate http:\/\/t.co\/VLb2MZZWM3 via @courierjournal",
    "id" : 579158402460491776,
    "created_at" : "2015-03-21 05:51:38 +0000",
    "user" : {
      "name" : "ashley judd",
      "screen_name" : "AshleyJudd",
      "protected" : false,
      "id_str" : "248747209",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/950478745764278272\/HHRoJZMz_normal.jpg",
      "id" : 248747209,
      "verified" : true
    }
  },
  "id" : 579200326915473408,
  "created_at" : "2015-03-21 08:38:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "InternationalDayOfHappiness",
      "indices" : [ 72, 100 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578980034200932352",
  "text" : "Be grateful. Be contended. There is alwaya sliver lining in every cloud #InternationalDayOfHappiness",
  "id" : 578980034200932352,
  "created_at" : "2015-03-20 18:02:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 3, 13 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/CsatpZtiHX",
      "expanded_url" : "https:\/\/analytics.usa.gov\/",
      "display_url" : "analytics.usa.gov"
    }, {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/1IANkd4sJr",
      "expanded_url" : "https:\/\/18f.gsa.gov\/2015\/03\/19\/how-we-built-analytics-usa-gov\/",
      "display_url" : "18f.gsa.gov\/2015\/03\/19\/how\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578958850428166144",
  "text" : "RT @barryhand: Love this - https:\/\/t.co\/CsatpZtiHX and how it was built https:\/\/t.co\/1IANkd4sJr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 12, 35 ],
        "url" : "https:\/\/t.co\/CsatpZtiHX",
        "expanded_url" : "https:\/\/analytics.usa.gov\/",
        "display_url" : "analytics.usa.gov"
      }, {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/1IANkd4sJr",
        "expanded_url" : "https:\/\/18f.gsa.gov\/2015\/03\/19\/how-we-built-analytics-usa-gov\/",
        "display_url" : "18f.gsa.gov\/2015\/03\/19\/how\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "578851296888901632",
    "text" : "Love this - https:\/\/t.co\/CsatpZtiHX and how it was built https:\/\/t.co\/1IANkd4sJr",
    "id" : 578851296888901632,
    "created_at" : "2015-03-20 09:31:18 +0000",
    "user" : {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "protected" : false,
      "id_str" : "18849187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614532770300329984\/xJWWAtSu_normal.jpg",
      "id" : 18849187,
      "verified" : false
    }
  },
  "id" : 578958850428166144,
  "created_at" : "2015-03-20 16:38:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Henry",
      "screen_name" : "Mark_J_Henry",
      "indices" : [ 3, 16 ],
      "id_str" : "45539859",
      "id" : 45539859
    }, {
      "name" : "Web Summit Has Moved",
      "screen_name" : "websummithq",
      "indices" : [ 45, 57 ],
      "id_str" : "776335315183304708",
      "id" : 776335315183304708
    }, {
      "name" : "Visit Belfast News",
      "screen_name" : "VisitBelfastOrg",
      "indices" : [ 59, 75 ],
      "id_str" : "2295838286",
      "id" : 2295838286
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tourismsummit",
      "indices" : [ 106, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578957646922911745",
  "text" : "RT @Mark_J_Henry: Belfast will host the next @WebSummitHQ, @VisitBelfastOrg chairman David Gavaghan tells #tourismsummit - a great opportun\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Web Summit Has Moved",
        "screen_name" : "websummithq",
        "indices" : [ 27, 39 ],
        "id_str" : "776335315183304708",
        "id" : 776335315183304708
      }, {
        "name" : "Visit Belfast News",
        "screen_name" : "VisitBelfastOrg",
        "indices" : [ 41, 57 ],
        "id_str" : "2295838286",
        "id" : 2295838286
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tourismsummit",
        "indices" : [ 88, 102 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578905202092335104",
    "text" : "Belfast will host the next @WebSummitHQ, @VisitBelfastOrg chairman David Gavaghan tells #tourismsummit - a great opportunity for the city",
    "id" : 578905202092335104,
    "created_at" : "2015-03-20 13:05:30 +0000",
    "user" : {
      "name" : "Mark Henry",
      "screen_name" : "Mark_J_Henry",
      "protected" : false,
      "id_str" : "45539859",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/421404033039859712\/Uph4FkzB_normal.jpeg",
      "id" : 45539859,
      "verified" : false
    }
  },
  "id" : 578957646922911745,
  "created_at" : "2015-03-20 16:33:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Doha News",
      "screen_name" : "dohanews",
      "indices" : [ 3, 12 ],
      "id_str" : "23123824",
      "id" : 23123824
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/dohanews\/status\/578871407485956099\/photo\/1",
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/rgGNZXxue8",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAiQeX_UgAAWDAk.jpg",
      "id_str" : "578871403627184128",
      "id" : 578871403627184128,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAiQeX_UgAAWDAk.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 441,
        "resize" : "fit",
        "w" : 771
      }, {
        "h" : 441,
        "resize" : "fit",
        "w" : 771
      }, {
        "h" : 441,
        "resize" : "fit",
        "w" : 771
      }, {
        "h" : 389,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rgGNZXxue8"
    } ],
    "hashtags" : [ {
      "text" : "Qatar",
      "indices" : [ 14, 20 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/JNvDMYCqIc",
      "expanded_url" : "http:\/\/dohane.ws\/1Lz5egg",
      "display_url" : "dohane.ws\/1Lz5egg"
    } ]
  },
  "geo" : { },
  "id_str" : "578899672556789760",
  "text" : "RT @dohanews: #Qatar races to desalinate, store water to meet nation\u2019s growing thirst. http:\/\/t.co\/JNvDMYCqIc http:\/\/t.co\/rgGNZXxue8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dohanews\/status\/578871407485956099\/photo\/1",
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/rgGNZXxue8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAiQeX_UgAAWDAk.jpg",
        "id_str" : "578871403627184128",
        "id" : 578871403627184128,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAiQeX_UgAAWDAk.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 441,
          "resize" : "fit",
          "w" : 771
        }, {
          "h" : 441,
          "resize" : "fit",
          "w" : 771
        }, {
          "h" : 441,
          "resize" : "fit",
          "w" : 771
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rgGNZXxue8"
      } ],
      "hashtags" : [ {
        "text" : "Qatar",
        "indices" : [ 0, 6 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/JNvDMYCqIc",
        "expanded_url" : "http:\/\/dohane.ws\/1Lz5egg",
        "display_url" : "dohane.ws\/1Lz5egg"
      } ]
    },
    "geo" : { },
    "id_str" : "578871407485956099",
    "text" : "#Qatar races to desalinate, store water to meet nation\u2019s growing thirst. http:\/\/t.co\/JNvDMYCqIc http:\/\/t.co\/rgGNZXxue8",
    "id" : 578871407485956099,
    "created_at" : "2015-03-20 10:51:13 +0000",
    "user" : {
      "name" : "Doha News",
      "screen_name" : "dohanews",
      "protected" : false,
      "id_str" : "23123824",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/790832152778772480\/rp8BozXv_normal.jpg",
      "id" : 23123824,
      "verified" : true
    }
  },
  "id" : 578899672556789760,
  "created_at" : "2015-03-20 12:43:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/XQYswalv6i",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/taps-turned-off-taiwan-battles-worst-drought-decade-102835897.html",
      "display_url" : "sg.news.yahoo.com\/taps-turned-of\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578899528281165824",
  "text" : "Taps turned off as Taiwan battles worst drought in decade https:\/\/t.co\/XQYswalv6i",
  "id" : 578899528281165824,
  "created_at" : "2015-03-20 12:42:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "OnlineHubIE",
      "screen_name" : "OnlineHubIE",
      "indices" : [ 3, 15 ],
      "id_str" : "2284869198",
      "id" : 2284869198
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "3opc",
      "indices" : [ 77, 82 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 118 ],
      "url" : "http:\/\/t.co\/w0SDUjSOfp",
      "expanded_url" : "http:\/\/wp.me\/p5hZFF-2D",
      "display_url" : "wp.me\/p5hZFF-2D"
    } ]
  },
  "geo" : { },
  "id_str" : "578887792874389504",
  "text" : "RT @OnlineHubIE: Make the most of speaking opportunities. Make sure they are #3opc female first http:\/\/t.co\/w0SDUjSOfp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "3opc",
        "indices" : [ 60, 65 ]
      } ],
      "urls" : [ {
        "indices" : [ 79, 101 ],
        "url" : "http:\/\/t.co\/w0SDUjSOfp",
        "expanded_url" : "http:\/\/wp.me\/p5hZFF-2D",
        "display_url" : "wp.me\/p5hZFF-2D"
      } ]
    },
    "geo" : { },
    "id_str" : "578884161269723136",
    "text" : "Make the most of speaking opportunities. Make sure they are #3opc female first http:\/\/t.co\/w0SDUjSOfp",
    "id" : 578884161269723136,
    "created_at" : "2015-03-20 11:41:53 +0000",
    "user" : {
      "name" : "OnlineHubIE",
      "screen_name" : "OnlineHubIE",
      "protected" : false,
      "id_str" : "2284869198",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/542641430527610882\/ZhUHBh7j_normal.png",
      "id" : 2284869198,
      "verified" : false
    }
  },
  "id" : 578887792874389504,
  "created_at" : "2015-03-20 11:56:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 55, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578881543755677696",
  "text" : "Anyone wants to rent a flat? It on the Western part of #Singapore where a good swimmer can swim across to Malaysia?",
  "id" : 578881543755677696,
  "created_at" : "2015-03-20 11:31:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/7jAtpCWPWs",
      "expanded_url" : "https:\/\/www.iedr.ie\/why-choose-ie\/optimise\/",
      "display_url" : "iedr.ie\/why-choose-ie\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578871504395509760",
  "text" : "Usability issues of the day: Do you know where to sign up for the OPTIMISE Fund at https:\/\/t.co\/7jAtpCWPWs?",
  "id" : 578871504395509760,
  "created_at" : "2015-03-20 10:51:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Maria",
      "screen_name" : "KateMariaGlobal",
      "indices" : [ 3, 19 ],
      "id_str" : "3003570179",
      "id" : 3003570179
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/KateMariaGlobal\/status\/578128231397732352\/photo\/1",
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/3DY7YKyQGB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAXskDMWoAAXPl2.jpg",
      "id_str" : "578128231263543296",
      "id" : 578128231263543296,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAXskDMWoAAXPl2.jpg",
      "sizes" : [ {
        "h" : 995,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 995,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 677,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 995,
        "resize" : "fit",
        "w" : 1000
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3DY7YKyQGB"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578865176100909056",
  "text" : "RT @KateMariaGlobal: Dublin transport system http:\/\/t.co\/3DY7YKyQGB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KateMariaGlobal\/status\/578128231397732352\/photo\/1",
        "indices" : [ 24, 46 ],
        "url" : "http:\/\/t.co\/3DY7YKyQGB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAXskDMWoAAXPl2.jpg",
        "id_str" : "578128231263543296",
        "id" : 578128231263543296,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAXskDMWoAAXPl2.jpg",
        "sizes" : [ {
          "h" : 995,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 995,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 677,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 995,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3DY7YKyQGB"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578128231397732352",
    "text" : "Dublin transport system http:\/\/t.co\/3DY7YKyQGB",
    "id" : 578128231397732352,
    "created_at" : "2015-03-18 09:38:06 +0000",
    "user" : {
      "name" : "Kate Maria",
      "screen_name" : "KateMariaGlobal",
      "protected" : false,
      "id_str" : "3003570179",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/805030430298767361\/98w-seXv_normal.jpg",
      "id" : 3003570179,
      "verified" : false
    }
  },
  "id" : 578865176100909056,
  "created_at" : "2015-03-20 10:26:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/rc7rQax91J",
      "expanded_url" : "https:\/\/twitter.com\/aofarre\/status\/578854781860626432",
      "display_url" : "twitter.com\/aofarre\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578856478435577856",
  "text" : "I sweep for the children of Gaza https:\/\/t.co\/rc7rQax91J",
  "id" : 578856478435577856,
  "created_at" : "2015-03-20 09:51:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jack Board",
      "screen_name" : "jackboard",
      "indices" : [ 3, 13 ],
      "id_str" : "4855461045",
      "id" : 4855461045
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/JackBoard\/status\/578847763930038273\/photo\/1",
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/tynfvG2Fdi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAh68y9VIAAtKXP.jpg",
      "id_str" : "578847737006858240",
      "id" : 578847737006858240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAh68y9VIAAtKXP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tynfvG2Fdi"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/JackBoard\/status\/578847763930038273\/photo\/1",
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/tynfvG2Fdi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAh682FVEAAhI_b.jpg",
      "id_str" : "578847737845714944",
      "id" : 578847737845714944,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAh682FVEAAhI_b.jpg",
      "sizes" : [ {
        "h" : 774,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 774,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 514,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 774,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tynfvG2Fdi"
    } ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 86, 97 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578855004297150464",
  "text" : "RT @JackBoard: Well wishers at the hospital have been stopping to read cards left for #LeeKuanYew http:\/\/t.co\/tynfvG2Fdi",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JackBoard\/status\/578847763930038273\/photo\/1",
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/tynfvG2Fdi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAh68y9VIAAtKXP.jpg",
        "id_str" : "578847737006858240",
        "id" : 578847737006858240,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAh68y9VIAAtKXP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tynfvG2Fdi"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/JackBoard\/status\/578847763930038273\/photo\/1",
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/tynfvG2Fdi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAh682FVEAAhI_b.jpg",
        "id_str" : "578847737845714944",
        "id" : 578847737845714944,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAh682FVEAAhI_b.jpg",
        "sizes" : [ {
          "h" : 774,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 774,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 514,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 774,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tynfvG2Fdi"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 71, 82 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578847763930038273",
    "text" : "Well wishers at the hospital have been stopping to read cards left for #LeeKuanYew http:\/\/t.co\/tynfvG2Fdi",
    "id" : 578847763930038273,
    "created_at" : "2015-03-20 09:17:16 +0000",
    "user" : {
      "name" : "Jack Board",
      "screen_name" : "JackBoardCNA",
      "protected" : false,
      "id_str" : "22432787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1019586416265736192\/i9RCSuDa_normal.jpg",
      "id" : 22432787,
      "verified" : true
    }
  },
  "id" : 578855004297150464,
  "created_at" : "2015-03-20 09:46:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fake Lee",
      "screen_name" : "Fake_HarryLee",
      "indices" : [ 3, 17 ],
      "id_str" : "35216436",
      "id" : 35216436
    }, {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "indices" : [ 20, 28 ],
      "id_str" : "115624161",
      "id" : 115624161
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/YahooSG\/status\/578815998297575424\/photo\/1",
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/PtpRhYfrCl",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAheFWVWMAEEVu2.jpg",
      "id_str" : "578815998104580097",
      "id" : 578815998104580097,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAheFWVWMAEEVu2.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 683,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PtpRhYfrCl"
    } ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 79, 90 ]
    } ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/IBr8WoH590",
      "expanded_url" : "http:\/\/yhoo.it\/1H74WGT",
      "display_url" : "yhoo.it\/1H74WGT"
    } ]
  },
  "geo" : { },
  "id_str" : "578854332172484608",
  "text" : "RT @Fake_HarryLee: \u201C@YahooSG: SGH designates block 7 for cards and flowers for #LeeKuanYew. http:\/\/t.co\/IBr8WoH590 http:\/\/t.co\/PtpRhYfrCl\u201D#\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yahoo Singapore",
        "screen_name" : "YahooSG",
        "indices" : [ 1, 9 ],
        "id_str" : "115624161",
        "id" : 115624161
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/YahooSG\/status\/578815998297575424\/photo\/1",
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/PtpRhYfrCl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAheFWVWMAEEVu2.jpg",
        "id_str" : "578815998104580097",
        "id" : 578815998104580097,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAheFWVWMAEEVu2.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PtpRhYfrCl"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 60, 71 ]
      }, {
        "text" : "ThankYouLKY",
        "indices" : [ 119, 131 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/IBr8WoH590",
        "expanded_url" : "http:\/\/yhoo.it\/1H74WGT",
        "display_url" : "yhoo.it\/1H74WGT"
      } ]
    },
    "in_reply_to_status_id_str" : "578815998297575424",
    "geo" : { },
    "id_str" : "578816985976336384",
    "in_reply_to_user_id" : 115624161,
    "text" : "\u201C@YahooSG: SGH designates block 7 for cards and flowers for #LeeKuanYew. http:\/\/t.co\/IBr8WoH590 http:\/\/t.co\/PtpRhYfrCl\u201D#ThankYouLKY",
    "id" : 578816985976336384,
    "in_reply_to_status_id" : 578815998297575424,
    "created_at" : "2015-03-20 07:14:58 +0000",
    "in_reply_to_screen_name" : "YahooSG",
    "in_reply_to_user_id_str" : "115624161",
    "user" : {
      "name" : "Fake Lee",
      "screen_name" : "Fake_HarryLee",
      "protected" : false,
      "id_str" : "35216436",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875519781499043841\/n7s62aCM_normal.jpg",
      "id" : 35216436,
      "verified" : false
    }
  },
  "id" : 578854332172484608,
  "created_at" : "2015-03-20 09:43:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578852836219445248",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose Your new profile image is brilliant.",
  "id" : 578852836219445248,
  "created_at" : "2015-03-20 09:37:25 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578817022290735104",
  "text" : "RT @patphelan: Remember when TED used to be about changing the world now it's more about when can I park my jet",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578813542259916800",
    "text" : "Remember when TED used to be about changing the world now it's more about when can I park my jet",
    "id" : 578813542259916800,
    "created_at" : "2015-03-20 07:01:16 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 578817022290735104,
  "created_at" : "2015-03-20 07:15:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http:\/\/t.co\/7EP1QOOHjE",
      "expanded_url" : "http:\/\/live.slooh.com\/stadium\/live\/the-total-solar-eclipse-of-2015",
      "display_url" : "live.slooh.com\/stadium\/live\/t\u2026"
    }, {
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/hbvVjUI9Zp",
      "expanded_url" : "http:\/\/www.virtualtelescope.eu\/webtv\/",
      "display_url" : "virtualtelescope.eu\/webtv\/"
    } ]
  },
  "geo" : { },
  "id_str" : "578816851951685632",
  "text" : "Catch the Eclipse virtually at http:\/\/t.co\/7EP1QOOHjE AND http:\/\/t.co\/hbvVjUI9Zp HT @bharatidalela",
  "id" : 578816851951685632,
  "created_at" : "2015-03-20 07:14:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578816548095336449",
  "text" : "@bharatidalela Good morning! Thank you. Will check it out.",
  "id" : 578816548095336449,
  "created_at" : "2015-03-20 07:13:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HuffPost Business",
      "screen_name" : "HuffPostBiz",
      "indices" : [ 98, 110 ],
      "id_str" : "27073265",
      "id" : 27073265
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 93 ],
      "url" : "http:\/\/t.co\/l2P2v2siVy",
      "expanded_url" : "http:\/\/huff.to\/1GXrCcK",
      "display_url" : "huff.to\/1GXrCcK"
    } ]
  },
  "geo" : { },
  "id_str" : "578616204698468353",
  "text" : "Proof That Working From Home Is Here To Stay: Even Yahoo Still Does It http:\/\/t.co\/l2P2v2siVy via @HuffPostBiz",
  "id" : 578616204698468353,
  "created_at" : "2015-03-19 17:57:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Carter",
      "screen_name" : "withoutdoing",
      "indices" : [ 3, 16 ],
      "id_str" : "435020266",
      "id" : 435020266
    }, {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 29, 42 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 88 ],
      "url" : "http:\/\/t.co\/kbCusdoqES",
      "expanded_url" : "http:\/\/www.randalolson.com\/2015\/03\/08\/computing-the-optimal-road-trip-across-the-u-s\/",
      "display_url" : "randalolson.com\/2015\/03\/08\/com\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578613098300473344",
  "text" : "RT @withoutdoing: \u6700\u8FD1\u6709\u4E2A\u8BA1\u7B97\u673A\u79D1\u5B66\u5BB6\uFF08@randal_olson \uFF09\u901A\u8FC7\u7A0B\u5E8F\u7B97\u6CD5\u627E\u5230\u4E86\u6700\u4F18\u5316\u7684\u7F8E\u56FD\u65C5\u6E38\u666F\u70B9\u5730\u56FE http:\/\/t.co\/kbCusdoqES \u4ED6\u597D\u50CF\u662F\u7528\u4E86python, \u8BA1\u7B97\u673A\u8003\u8651\u4E86\u4E00\u5206\u949F\u5C31\u7ED9\u51FA\u4E86\u7ED3\u679C",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Randy Olson",
        "screen_name" : "randal_olson",
        "indices" : [ 11, 24 ],
        "id_str" : "49413866",
        "id" : 49413866
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 70 ],
        "url" : "http:\/\/t.co\/kbCusdoqES",
        "expanded_url" : "http:\/\/www.randalolson.com\/2015\/03\/08\/computing-the-optimal-road-trip-across-the-u-s\/",
        "display_url" : "randalolson.com\/2015\/03\/08\/com\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "578610582485630976",
    "text" : "\u6700\u8FD1\u6709\u4E2A\u8BA1\u7B97\u673A\u79D1\u5B66\u5BB6\uFF08@randal_olson \uFF09\u901A\u8FC7\u7A0B\u5E8F\u7B97\u6CD5\u627E\u5230\u4E86\u6700\u4F18\u5316\u7684\u7F8E\u56FD\u65C5\u6E38\u666F\u70B9\u5730\u56FE http:\/\/t.co\/kbCusdoqES \u4ED6\u597D\u50CF\u662F\u7528\u4E86python, \u8BA1\u7B97\u673A\u8003\u8651\u4E86\u4E00\u5206\u949F\u5C31\u7ED9\u51FA\u4E86\u7ED3\u679C",
    "id" : 578610582485630976,
    "created_at" : "2015-03-19 17:34:47 +0000",
    "user" : {
      "name" : "Liz Carter",
      "screen_name" : "withoutdoing",
      "protected" : false,
      "id_str" : "435020266",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/600731240812032001\/Z4afrP4k_normal.jpg",
      "id" : 435020266,
      "verified" : true
    }
  },
  "id" : 578613098300473344,
  "created_at" : "2015-03-19 17:44:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/Mbv95iMNOH",
      "expanded_url" : "http:\/\/ridewithgps.com\/routes\/7079526?privacy_code=nlCGrJPICFWT1tLy",
      "display_url" : "ridewithgps.com\/routes\/7079526\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578593230100516865",
  "text" : "54 nights self guided tour with accommodations, breakfast and dinner that \u20AC7318 http:\/\/t.co\/Mbv95iMNOH",
  "id" : 578593230100516865,
  "created_at" : "2015-03-19 16:25:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/khx8tjUhS2",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/8-valuable-lessons-learn-pore-061250457.html",
      "display_url" : "sg.news.yahoo.com\/8-valuable-les\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578574232533209088",
  "text" : "8 Valuable Lessons You Can Learn From S\u2019pore\u2019s Founding Father https:\/\/t.co\/khx8tjUhS2",
  "id" : 578574232533209088,
  "created_at" : "2015-03-19 15:10:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 3, 12 ],
      "id_str" : "19338425",
      "id" : 19338425
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578552354456358912",
  "text" : "RT @Fondacey: Bow to all donors! RT Renzo_Soprano: Chinese doctors bow to 11yr boy w\/ brain cancer saving lives by donating organs. http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Renzo_Soprano\/status\/578548597341437953\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/mhMyxO8UIV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAdq4KVWEAAd5gt.jpg",
        "id_str" : "578548590219497472",
        "id" : 578548590219497472,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAdq4KVWEAAd5gt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 392,
          "resize" : "fit",
          "w" : 592
        }, {
          "h" : 392,
          "resize" : "fit",
          "w" : 592
        }, {
          "h" : 392,
          "resize" : "fit",
          "w" : 592
        }, {
          "h" : 392,
          "resize" : "fit",
          "w" : 592
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mhMyxO8UIV"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578551191321387009",
    "text" : "Bow to all donors! RT Renzo_Soprano: Chinese doctors bow to 11yr boy w\/ brain cancer saving lives by donating organs. http:\/\/t.co\/mhMyxO8UIV",
    "id" : 578551191321387009,
    "created_at" : "2015-03-19 13:38:47 +0000",
    "user" : {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "protected" : false,
      "id_str" : "19338425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3157014212\/d431af4ae6cd671dfa76a92f7ceceee4_normal.jpeg",
      "id" : 19338425,
      "verified" : false
    }
  },
  "id" : 578552354456358912,
  "created_at" : "2015-03-19 13:43:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/GzsQn2CIX8",
      "expanded_url" : "http:\/\/www.japantimes.co.jp\/news\/2015\/03\/19\/national\/china-in-shock-after-japanese-girl-wins-brain-battle\/#.VQrSFoQlHgk.twitter",
      "display_url" : "japantimes.co.jp\/news\/2015\/03\/1\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578552187443359744",
  "text" : "RT @yapphenghui: China in shock after Japanese girl wins brain battle | The Japan Times http:\/\/t.co\/GzsQn2CIX8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/GzsQn2CIX8",
        "expanded_url" : "http:\/\/www.japantimes.co.jp\/news\/2015\/03\/19\/national\/china-in-shock-after-japanese-girl-wins-brain-battle\/#.VQrSFoQlHgk.twitter",
        "display_url" : "japantimes.co.jp\/news\/2015\/03\/1\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "578551940503576576",
    "text" : "China in shock after Japanese girl wins brain battle | The Japan Times http:\/\/t.co\/GzsQn2CIX8",
    "id" : 578551940503576576,
    "created_at" : "2015-03-19 13:41:46 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 578552187443359744,
  "created_at" : "2015-03-19 13:42:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Manuel C",
      "screen_name" : "manuel_c",
      "indices" : [ 3, 12 ],
      "id_str" : "25116235",
      "id" : 25116235
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Startup",
      "indices" : [ 85, 93 ]
    }, {
      "text" : "Entrepreneur",
      "indices" : [ 122, 135 ]
    } ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/ePmEmgkumL",
      "expanded_url" : "http:\/\/onforb.es\/1O7MELc",
      "display_url" : "onforb.es\/1O7MELc"
    } ]
  },
  "geo" : { },
  "id_str" : "578551965858336768",
  "text" : "RT @manuel_c: 24 Years Old, Waking At 4 a.m., And Supervising 70 Staff -- Who'd Be A #Startup COO? http:\/\/t.co\/ePmEmgkumL #Entrepreneur",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Startup",
        "indices" : [ 71, 79 ]
      }, {
        "text" : "Entrepreneur",
        "indices" : [ 108, 121 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/ePmEmgkumL",
        "expanded_url" : "http:\/\/onforb.es\/1O7MELc",
        "display_url" : "onforb.es\/1O7MELc"
      } ]
    },
    "geo" : { },
    "id_str" : "578542733276622848",
    "text" : "24 Years Old, Waking At 4 a.m., And Supervising 70 Staff -- Who'd Be A #Startup COO? http:\/\/t.co\/ePmEmgkumL #Entrepreneur",
    "id" : 578542733276622848,
    "created_at" : "2015-03-19 13:05:11 +0000",
    "user" : {
      "name" : "Manuel C",
      "screen_name" : "manuel_c",
      "protected" : false,
      "id_str" : "25116235",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/919731916638523392\/05G-NLrb_normal.jpg",
      "id" : 25116235,
      "verified" : false
    }
  },
  "id" : 578551965858336768,
  "created_at" : "2015-03-19 13:41:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Art Markman",
      "screen_name" : "abmarkman",
      "indices" : [ 3, 13 ],
      "id_str" : "38717507",
      "id" : 38717507
    }, {
      "name" : "Fast Company",
      "screen_name" : "FastCompany",
      "indices" : [ 128, 140 ],
      "id_str" : "2735591",
      "id" : 2735591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/3mQWjwobUA",
      "expanded_url" : "http:\/\/www.fastcompany.com\/3043789\/what-you-can-learn-from-cultural-differences",
      "display_url" : "fastcompany.com\/3043789\/what-y\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578548665498918912",
  "text" : "RT @abmarkman: Working w\/ people from different cultures can open our eyes to other ways of thinking http:\/\/t.co\/3mQWjwobUA via @FastCompany",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Fast Company",
        "screen_name" : "FastCompany",
        "indices" : [ 113, 125 ],
        "id_str" : "2735591",
        "id" : 2735591
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 86, 108 ],
        "url" : "http:\/\/t.co\/3mQWjwobUA",
        "expanded_url" : "http:\/\/www.fastcompany.com\/3043789\/what-you-can-learn-from-cultural-differences",
        "display_url" : "fastcompany.com\/3043789\/what-y\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "578548378591629312",
    "text" : "Working w\/ people from different cultures can open our eyes to other ways of thinking http:\/\/t.co\/3mQWjwobUA via @FastCompany",
    "id" : 578548378591629312,
    "created_at" : "2015-03-19 13:27:37 +0000",
    "user" : {
      "name" : "Art Markman",
      "screen_name" : "abmarkman",
      "protected" : false,
      "id_str" : "38717507",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013773198628777989\/AS7CVONT_normal.jpg",
      "id" : 38717507,
      "verified" : false
    }
  },
  "id" : 578548665498918912,
  "created_at" : "2015-03-19 13:28:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "missbossy",
      "screen_name" : "missbossy",
      "indices" : [ 3, 13 ],
      "id_str" : "12073182",
      "id" : 12073182
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/missbossy\/status\/299706604767547393\/photo\/1",
      "indices" : [ 118, 138 ],
      "url" : "http:\/\/t.co\/u6RiJNAY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BCjFkbKCEAA9Bna.jpg",
      "id_str" : "299706604775936000",
      "id" : 299706604775936000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BCjFkbKCEAA9Bna.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/u6RiJNAY"
    } ],
    "hashtags" : [ {
      "text" : "foodsys",
      "indices" : [ 109, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578542919784759296",
  "text" : "RT @missbossy: The carbon footprint of you food is as much about what's in your food as where it comes from. #foodsys http:\/\/t.co\/u6RiJNAY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/missbossy\/status\/299706604767547393\/photo\/1",
        "indices" : [ 103, 123 ],
        "url" : "http:\/\/t.co\/u6RiJNAY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BCjFkbKCEAA9Bna.jpg",
        "id_str" : "299706604775936000",
        "id" : 299706604775936000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BCjFkbKCEAA9Bna.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/u6RiJNAY"
      } ],
      "hashtags" : [ {
        "text" : "foodsys",
        "indices" : [ 94, 102 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "299706604767547393",
    "text" : "The carbon footprint of you food is as much about what's in your food as where it comes from. #foodsys http:\/\/t.co\/u6RiJNAY",
    "id" : 299706604767547393,
    "created_at" : "2013-02-08 02:29:57 +0000",
    "user" : {
      "name" : "missbossy",
      "screen_name" : "missbossy",
      "protected" : false,
      "id_str" : "12073182",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3770383786\/ea292be438dca8e3b8e0e0cdbda0b40c_normal.jpeg",
      "id" : 12073182,
      "verified" : false
    }
  },
  "id" : 578542919784759296,
  "created_at" : "2015-03-19 13:05:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "wildatlanticway",
      "indices" : [ 29, 45 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578538018790879232",
  "text" : "What the best way to explore #wildatlanticway? We try to avoid driving",
  "id" : 578538018790879232,
  "created_at" : "2015-03-19 12:46:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Lian",
      "screen_name" : "davidlian",
      "indices" : [ 3, 13 ],
      "id_str" : "2000251",
      "id" : 2000251
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PR",
      "indices" : [ 23, 26 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578537099722371072",
  "text" : "RT @davidlian: Being a #PR person means you often have to look at a bad situation and see the best in it.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "PR",
        "indices" : [ 8, 11 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578535768869613570",
    "text" : "Being a #PR person means you often have to look at a bad situation and see the best in it.",
    "id" : 578535768869613570,
    "created_at" : "2015-03-19 12:37:30 +0000",
    "user" : {
      "name" : "David Lian",
      "screen_name" : "davidlian",
      "protected" : true,
      "id_str" : "2000251",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991462850383118336\/1vdB6gT3_normal.jpg",
      "id" : 2000251,
      "verified" : false
    }
  },
  "id" : 578537099722371072,
  "created_at" : "2015-03-19 12:42:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Lee",
      "screen_name" : "AdrianLeeSA",
      "indices" : [ 3, 15 ],
      "id_str" : "33936903",
      "id" : 33936903
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tech",
      "indices" : [ 116, 121 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578536929144283136",
  "text" : "RT @AdrianLeeSA: Mmm, yummy. \"Intel, Google announce partnership with Tag Heuer for Android Wear-powered smartwatch #tech http:\/\/t.co\/5hOTQ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tech",
        "indices" : [ 99, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 105, 127 ],
        "url" : "http:\/\/t.co\/5hOTQHeUMn",
        "expanded_url" : "http:\/\/buff.ly\/1BZDTxJ",
        "display_url" : "buff.ly\/1BZDTxJ"
      } ]
    },
    "geo" : { },
    "id_str" : "578536353828347904",
    "text" : "Mmm, yummy. \"Intel, Google announce partnership with Tag Heuer for Android Wear-powered smartwatch #tech http:\/\/t.co\/5hOTQHeUMn\"",
    "id" : 578536353828347904,
    "created_at" : "2015-03-19 12:39:50 +0000",
    "user" : {
      "name" : "Adrian Lee",
      "screen_name" : "AdrianLeeSA",
      "protected" : false,
      "id_str" : "33936903",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/807449488793038848\/rQrUoExA_normal.jpg",
      "id" : 33936903,
      "verified" : false
    }
  },
  "id" : 578536929144283136,
  "created_at" : "2015-03-19 12:42:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melanie",
      "screen_name" : "tompino",
      "indices" : [ 3, 11 ],
      "id_str" : "41588131",
      "id" : 41588131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578533441161031680",
  "text" : "RT @tompino: 'People come because they're attracted to the mission.' 'Retention, not attraction, of digital talent is top of list now' @tco\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tanya Cordrey",
        "screen_name" : "tcordrey",
        "indices" : [ 122, 131 ],
        "id_str" : "16110539",
        "id" : 16110539
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GdnCMS",
        "indices" : [ 132, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578533102399664128",
    "text" : "'People come because they're attracted to the mission.' 'Retention, not attraction, of digital talent is top of list now' @tcordrey #GdnCMS",
    "id" : 578533102399664128,
    "created_at" : "2015-03-19 12:26:54 +0000",
    "user" : {
      "name" : "Melanie",
      "screen_name" : "tompino",
      "protected" : false,
      "id_str" : "41588131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/900043690240094209\/gbOAQQm__normal.jpg",
      "id" : 41588131,
      "verified" : false
    }
  },
  "id" : 578533441161031680,
  "created_at" : "2015-03-19 12:28:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http:\/\/t.co\/w8Bbq0NiDV",
      "expanded_url" : "http:\/\/bit.ly\/1bhgPR2",
      "display_url" : "bit.ly\/1bhgPR2"
    } ]
  },
  "geo" : { },
  "id_str" : "578531068887855104",
  "text" : "\"Metrics are story told in numbers. Is the best way to keep close tab on your business.\" http:\/\/t.co\/w8Bbq0NiDV",
  "id" : 578531068887855104,
  "created_at" : "2015-03-19 12:18:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aldi Ireland",
      "screen_name" : "Aldi_Ireland",
      "indices" : [ 3, 16 ],
      "id_str" : "2377180886",
      "id" : 2377180886
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578526574275887104",
  "text" : "RT @Aldi_Ireland: To celebrate that it\u2019s only 3 days until the weekend, lets giveaway a \u20AC100 voucher! Retweet for your chance to win! http:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Aldi_Ireland\/status\/578247081971748864\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/XnxqKKZMuF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAZOKGuUgAAT7CG.png",
        "id_str" : "578235537674240000",
        "id" : 578235537674240000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAZOKGuUgAAT7CG.png",
        "sizes" : [ {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XnxqKKZMuF"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578247081971748864",
    "text" : "To celebrate that it\u2019s only 3 days until the weekend, lets giveaway a \u20AC100 voucher! Retweet for your chance to win! http:\/\/t.co\/XnxqKKZMuF",
    "id" : 578247081971748864,
    "created_at" : "2015-03-18 17:30:22 +0000",
    "user" : {
      "name" : "Aldi Ireland",
      "screen_name" : "Aldi_Ireland",
      "protected" : false,
      "id_str" : "2377180886",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/902885452356583429\/z7AFHw46_normal.jpg",
      "id" : 2377180886,
      "verified" : true
    }
  },
  "id" : 578526574275887104,
  "created_at" : "2015-03-19 12:00:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dean Brandon",
      "screen_name" : "deanwbrandon",
      "indices" : [ 3, 16 ],
      "id_str" : "79664369",
      "id" : 79664369
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578523643833131008",
  "text" : "RT @deanwbrandon: The difference between commitment and technique: We spend way too much time teaching people technique. Teaching\u2026 http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/MQnCF0Va1H",
        "expanded_url" : "http:\/\/bit.ly\/1F80hHw",
        "display_url" : "bit.ly\/1F80hHw"
      } ]
    },
    "geo" : { },
    "id_str" : "578512047685562368",
    "text" : "The difference between commitment and technique: We spend way too much time teaching people technique. Teaching\u2026 http:\/\/t.co\/MQnCF0Va1H",
    "id" : 578512047685562368,
    "created_at" : "2015-03-19 11:03:15 +0000",
    "user" : {
      "name" : "Dean Brandon",
      "screen_name" : "deanwbrandon",
      "protected" : false,
      "id_str" : "79664369",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2707141072\/93fc7f53626e1077b2d7ce777adc18d2_normal.jpeg",
      "id" : 79664369,
      "verified" : false
    }
  },
  "id" : 578523643833131008,
  "created_at" : "2015-03-19 11:49:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SBP events team",
      "screen_name" : "sbpostevents",
      "indices" : [ 3, 16 ],
      "id_str" : "3201947782",
      "id" : 3201947782
    }, {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 19, 32 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "30pc",
      "indices" : [ 53, 58 ]
    }, {
      "text" : "women",
      "indices" : [ 68, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/Y8y1DNqvnS",
      "expanded_url" : "http:\/\/swipesummit.com",
      "display_url" : "swipesummit.com"
    } ]
  },
  "geo" : { },
  "id_str" : "578522081450668032",
  "text" : "RT @sbpostevents: .@DigiWomenIRL in response to your #30pc quota of #women speakers at http:\/\/t.co\/Y8y1DNqvnS. Meryl said it best,YES! http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DigiWomen",
        "screen_name" : "DigiWomenIRL",
        "indices" : [ 1, 14 ],
        "id_str" : "1221157964",
        "id" : 1221157964
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sbpostevents\/status\/576700918810439680\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/QmtLVtqN0f",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CADabeYUQAEs2t5.png",
        "id_str" : "576700917849800705",
        "id" : 576700917849800705,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CADabeYUQAEs2t5.png",
        "sizes" : [ {
          "h" : 210,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QmtLVtqN0f"
      } ],
      "hashtags" : [ {
        "text" : "30pc",
        "indices" : [ 35, 40 ]
      }, {
        "text" : "women",
        "indices" : [ 50, 56 ]
      } ],
      "urls" : [ {
        "indices" : [ 69, 91 ],
        "url" : "http:\/\/t.co\/Y8y1DNqvnS",
        "expanded_url" : "http:\/\/swipesummit.com",
        "display_url" : "swipesummit.com"
      } ]
    },
    "geo" : { },
    "id_str" : "576700918810439680",
    "text" : ".@DigiWomenIRL in response to your #30pc quota of #women speakers at http:\/\/t.co\/Y8y1DNqvnS. Meryl said it best,YES! http:\/\/t.co\/QmtLVtqN0f",
    "id" : 576700918810439680,
    "created_at" : "2015-03-14 11:06:28 +0000",
    "user" : {
      "name" : "Swipe Summit, 4 Feb",
      "screen_name" : "swipe360",
      "protected" : false,
      "id_str" : "2915792285",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/597182421390483458\/oFjI883w_normal.png",
      "id" : 2915792285,
      "verified" : false
    }
  },
  "id" : 578522081450668032,
  "created_at" : "2015-03-19 11:43:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Womersley",
      "screen_name" : "LisaWomersley",
      "indices" : [ 0, 14 ],
      "id_str" : "789148770869080064",
      "id" : 789148770869080064
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "578520787600216064",
  "geo" : { },
  "id_str" : "578521428368195584",
  "in_reply_to_user_id" : 15773853,
  "text" : "@lisawomersley I suppose you are not going to use Uber in the future. :)",
  "id" : 578521428368195584,
  "in_reply_to_status_id" : 578520787600216064,
  "created_at" : "2015-03-19 11:40:31 +0000",
  "in_reply_to_screen_name" : "LisaCollinsSG",
  "in_reply_to_user_id_str" : "15773853",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ivan Mazour",
      "screen_name" : "IvanMazour",
      "indices" : [ 3, 14 ],
      "id_str" : "115016715",
      "id" : 115016715
    }, {
      "name" : "Be My Eyes",
      "screen_name" : "BeMyEyes",
      "indices" : [ 126, 135 ],
      "id_str" : "1278698569",
      "id" : 1278698569
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/MSVRHhaoe9",
      "expanded_url" : "http:\/\/www.bemyeyes.org\/",
      "display_url" : "bemyeyes.org"
    } ]
  },
  "geo" : { },
  "id_str" : "578520567160180736",
  "text" : "RT @IvanMazour: This is incredible - how often does an app do something this positive and valuable.. http:\/\/t.co\/MSVRHhaoe9 - @BeMyEyes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Be My Eyes",
        "screen_name" : "BeMyEyes",
        "indices" : [ 110, 119 ],
        "id_str" : "1278698569",
        "id" : 1278698569
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/MSVRHhaoe9",
        "expanded_url" : "http:\/\/www.bemyeyes.org\/",
        "display_url" : "bemyeyes.org"
      } ]
    },
    "geo" : { },
    "id_str" : "578520372959657984",
    "text" : "This is incredible - how often does an app do something this positive and valuable.. http:\/\/t.co\/MSVRHhaoe9 - @BeMyEyes",
    "id" : 578520372959657984,
    "created_at" : "2015-03-19 11:36:19 +0000",
    "user" : {
      "name" : "Ivan Mazour",
      "screen_name" : "IvanMazour",
      "protected" : false,
      "id_str" : "115016715",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000751753157\/608a03e46f1ba6c4a53fc59a2fd8a397_normal.jpeg",
      "id" : 115016715,
      "verified" : false
    }
  },
  "id" : 578520567160180736,
  "created_at" : "2015-03-19 11:37:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Foolproof",
      "screen_name" : "Foolproof_UX",
      "indices" : [ 0, 13 ],
      "id_str" : "97477339",
      "id" : 97477339
    }, {
      "name" : "Fatimah",
      "screen_name" : "FatimahRadiom",
      "indices" : [ 14, 28 ],
      "id_str" : "438406879",
      "id" : 438406879
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "578472275420278784",
  "geo" : { },
  "id_str" : "578516739568361472",
  "in_reply_to_user_id" : 97477339,
  "text" : "@Foolproof_UX @FatimahRadiom Any idea what the type of goods and services they purchase? Thanks",
  "id" : 578516739568361472,
  "in_reply_to_status_id" : 578472275420278784,
  "created_at" : "2015-03-19 11:21:53 +0000",
  "in_reply_to_screen_name" : "Foolproof_UX",
  "in_reply_to_user_id_str" : "97477339",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jill Krasny",
      "screen_name" : "jillkrasny",
      "indices" : [ 42, 53 ],
      "id_str" : "29011684",
      "id" : 29011684
    }, {
      "name" : "Inc.",
      "screen_name" : "Inc",
      "indices" : [ 81, 85 ],
      "id_str" : "16896485",
      "id" : 16896485
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http:\/\/t.co\/2N98Bz0LYh",
      "expanded_url" : "http:\/\/www.inc.com\/jill-krasny\/stop-doing-these-things-with-your-contact-forms.html",
      "display_url" : "inc.com\/jill-krasny\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578509518956589056",
  "text" : "4 things every online form must get right @jillkrasny http:\/\/t.co\/2N98Bz0LYh via @Inc",
  "id" : 578509518956589056,
  "created_at" : "2015-03-19 10:53:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caixin Global",
      "screen_name" : "caixin",
      "indices" : [ 3, 10 ],
      "id_str" : "104146203",
      "id" : 104146203
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 113, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578504001165107200",
  "text" : "RT @caixin: Kissinger: \"He is one of the men who has created a society and we'll continue to be inspired by him\" #LeeKuanYew http:\/\/t.co\/9g\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/caixin\/status\/578416404212244480\/photo\/1",
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/9gVKSGAmi6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAbyp3CUIAACsK9.jpg",
        "id_str" : "578416403125903360",
        "id" : 578416403125903360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAbyp3CUIAACsK9.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 839
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 839
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 839
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9gVKSGAmi6"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 101, 112 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578416404212244480",
    "text" : "Kissinger: \"He is one of the men who has created a society and we'll continue to be inspired by him\" #LeeKuanYew http:\/\/t.co\/9gVKSGAmi6",
    "id" : 578416404212244480,
    "created_at" : "2015-03-19 04:43:11 +0000",
    "user" : {
      "name" : "Caixin Global",
      "screen_name" : "caixin",
      "protected" : false,
      "id_str" : "104146203",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/804562326045229056\/imp2Cit8_normal.jpg",
      "id" : 104146203,
      "verified" : true
    }
  },
  "id" : 578504001165107200,
  "created_at" : "2015-03-19 10:31:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caixin Global",
      "screen_name" : "caixin",
      "indices" : [ 3, 10 ],
      "id_str" : "104146203",
      "id" : 104146203
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578476408520986624",
  "text" : "RT @caixin: Kissinger: \"My view has been Ukraine should be in neither camp (NATO or Russian), that it should be a meeting place and not an \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578464406784233472",
    "text" : "Kissinger: \"My view has been Ukraine should be in neither camp (NATO or Russian), that it should be a meeting place and not an outpost\"",
    "id" : 578464406784233472,
    "created_at" : "2015-03-19 07:53:56 +0000",
    "user" : {
      "name" : "Caixin Global",
      "screen_name" : "caixin",
      "protected" : false,
      "id_str" : "104146203",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/804562326045229056\/imp2Cit8_normal.jpg",
      "id" : 104146203,
      "verified" : true
    }
  },
  "id" : 578476408520986624,
  "created_at" : "2015-03-19 08:41:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Late Debate",
      "screen_name" : "LatedebateRTE",
      "indices" : [ 3, 17 ],
      "id_str" : "106446904",
      "id" : 106446904
    }, {
      "name" : "Cormac Lucey",
      "screen_name" : "CormacLucey",
      "indices" : [ 119, 131 ],
      "id_str" : "185840964",
      "id" : 185840964
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578346912165720064",
  "text" : "RT @LatedebateRTE: One year bankruptcy will allow people a fresh start more quickly ..but it  won't cause a revolution @CormacLucey  #rtela\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Cormac Lucey",
        "screen_name" : "CormacLucey",
        "indices" : [ 100, 112 ],
        "id_str" : "185840964",
        "id" : 185840964
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rtelatedebate",
        "indices" : [ 114, 128 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578323156387131393",
    "text" : "One year bankruptcy will allow people a fresh start more quickly ..but it  won't cause a revolution @CormacLucey  #rtelatedebate",
    "id" : 578323156387131393,
    "created_at" : "2015-03-18 22:32:39 +0000",
    "user" : {
      "name" : "Late Debate",
      "screen_name" : "LatedebateRTE",
      "protected" : false,
      "id_str" : "106446904",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/477213990888955904\/OxmdkVNY_normal.jpeg",
      "id" : 106446904,
      "verified" : true
    }
  },
  "id" : 578346912165720064,
  "created_at" : "2015-03-19 00:07:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pdscott\/status\/578329236357951488\/photo\/1",
      "indices" : [ 31, 53 ],
      "url" : "http:\/\/t.co\/dPdVkCDFK6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAajXz9WQAAELWH.jpg",
      "id_str" : "578329231643525120",
      "id" : 578329231643525120,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAajXz9WQAAELWH.jpg",
      "sizes" : [ {
        "h" : 455,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 685,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 685,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 685,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dPdVkCDFK6"
    } ],
    "hashtags" : [ {
      "text" : "Dublin",
      "indices" : [ 23, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578346262669328385",
  "text" : "RT @pdscott: Waka waka #Dublin http:\/\/t.co\/dPdVkCDFK6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pdscott\/status\/578329236357951488\/photo\/1",
        "indices" : [ 18, 40 ],
        "url" : "http:\/\/t.co\/dPdVkCDFK6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAajXz9WQAAELWH.jpg",
        "id_str" : "578329231643525120",
        "id" : 578329231643525120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAajXz9WQAAELWH.jpg",
        "sizes" : [ {
          "h" : 455,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dPdVkCDFK6"
      } ],
      "hashtags" : [ {
        "text" : "Dublin",
        "indices" : [ 10, 17 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578329236357951488",
    "text" : "Waka waka #Dublin http:\/\/t.co\/dPdVkCDFK6",
    "id" : 578329236357951488,
    "created_at" : "2015-03-18 22:56:49 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 578346262669328385,
  "created_at" : "2015-03-19 00:04:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http:\/\/t.co\/LtohVRpsaI",
      "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/health\/expat-health\/8831080\/Expat-guide-to-Singapore-health-care.html",
      "display_url" : "telegraph.co.uk\/news\/health\/ex\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578341090765369345",
  "text" : "Guide to Singapore health care system. http:\/\/t.co\/LtohVRpsaI",
  "id" : 578341090765369345,
  "created_at" : "2015-03-18 23:43:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/578278906052141056\/photo\/1",
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/6IlUbR6hiD",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAZ1mbNWMAAYokC.png",
      "id_str" : "578278905162903552",
      "id" : 578278905162903552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAZ1mbNWMAAYokC.png",
      "sizes" : [ {
        "h" : 284,
        "resize" : "fit",
        "w" : 575
      }, {
        "h" : 284,
        "resize" : "fit",
        "w" : 575
      }, {
        "h" : 284,
        "resize" : "fit",
        "w" : 575
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 284,
        "resize" : "fit",
        "w" : 575
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6IlUbR6hiD"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578278906052141056",
  "text" : "On my timeline, a Twitter ads...really a woman with unbutton blouse to sell your ware?! http:\/\/t.co\/6IlUbR6hiD",
  "id" : 578278906052141056,
  "created_at" : "2015-03-18 19:36:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "indices" : [ 3, 13 ],
      "id_str" : "17524108",
      "id" : 17524108
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ericknopf\/status\/578267171651997696\/photo\/1",
      "indices" : [ 65, 87 ],
      "url" : "http:\/\/t.co\/XMqHckowpA",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAZq7b_WQAEGnAV.png",
      "id_str" : "578267171521970177",
      "id" : 578267171521970177,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAZq7b_WQAEGnAV.png",
      "sizes" : [ {
        "h" : 504,
        "resize" : "fit",
        "w" : 799
      }, {
        "h" : 429,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 504,
        "resize" : "fit",
        "w" : 799
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 504,
        "resize" : "fit",
        "w" : 799
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XMqHckowpA"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http:\/\/t.co\/uV0pgGeBSM",
      "expanded_url" : "http:\/\/bit.ly\/1EDLAf7",
      "display_url" : "bit.ly\/1EDLAf7"
    } ]
  },
  "geo" : { },
  "id_str" : "578268861042204672",
  "text" : "RT @ericknopf: 10 Inspiring UX Portfolios http:\/\/t.co\/uV0pgGeBSM http:\/\/t.co\/XMqHckowpA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ericknopf\/status\/578267171651997696\/photo\/1",
        "indices" : [ 50, 72 ],
        "url" : "http:\/\/t.co\/XMqHckowpA",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAZq7b_WQAEGnAV.png",
        "id_str" : "578267171521970177",
        "id" : 578267171521970177,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAZq7b_WQAEGnAV.png",
        "sizes" : [ {
          "h" : 504,
          "resize" : "fit",
          "w" : 799
        }, {
          "h" : 429,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 504,
          "resize" : "fit",
          "w" : 799
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 504,
          "resize" : "fit",
          "w" : 799
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XMqHckowpA"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 27, 49 ],
        "url" : "http:\/\/t.co\/uV0pgGeBSM",
        "expanded_url" : "http:\/\/bit.ly\/1EDLAf7",
        "display_url" : "bit.ly\/1EDLAf7"
      } ]
    },
    "geo" : { },
    "id_str" : "578267171651997696",
    "text" : "10 Inspiring UX Portfolios http:\/\/t.co\/uV0pgGeBSM http:\/\/t.co\/XMqHckowpA",
    "id" : 578267171651997696,
    "created_at" : "2015-03-18 18:50:12 +0000",
    "user" : {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "protected" : false,
      "id_str" : "17524108",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460579737018970113\/3x_hi1Zc_normal.jpeg",
      "id" : 17524108,
      "verified" : false
    }
  },
  "id" : 578268861042204672,
  "created_at" : "2015-03-18 18:56:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/7H6T1iZK88",
      "expanded_url" : "https:\/\/support.google.com\/analytics\/answer\/3191589?hl=en",
      "display_url" : "support.google.com\/analytics\/answ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578234767130411009",
  "text" : "Adding business data to Google Analytics data. https:\/\/t.co\/7H6T1iZK88",
  "id" : 578234767130411009,
  "created_at" : "2015-03-18 16:41:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/8oeiIb3Lae",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-india-31937270",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578228669765799937",
  "text" : "\"My daughter will not die if I take her to school. But she will surely die if she does not study,\" http:\/\/t.co\/8oeiIb3Lae",
  "id" : 578228669765799937,
  "created_at" : "2015-03-18 16:17:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578224446261075969",
  "text" : "Twitter pestering me to \"experience the quarter-finals of the Cricket World Cup.\"",
  "id" : 578224446261075969,
  "created_at" : "2015-03-18 16:00:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/m96J2avhOe",
      "expanded_url" : "https:\/\/twitter.com\/zainabkakal\/status\/578094032070422529",
      "display_url" : "twitter.com\/zainabkakal\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578223242944589824",
  "text" : "No matter how you look at it, as a species we really do mix up our priorities in life. https:\/\/t.co\/m96J2avhOe",
  "id" : 578223242944589824,
  "created_at" : "2015-03-18 15:55:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC News \u4E2D\u6587",
      "screen_name" : "bbcchinese",
      "indices" : [ 3, 14 ],
      "id_str" : "791197",
      "id" : 791197
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/bbcchinese\/status\/578123226997391360\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/FKpCihdJYf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAXoAsDUsAACJlR.jpg",
      "id_str" : "578123225709719552",
      "id" : 578123225709719552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAXoAsDUsAACJlR.jpg",
      "sizes" : [ {
        "h" : 549,
        "resize" : "fit",
        "w" : 976
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 549,
        "resize" : "fit",
        "w" : 976
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 549,
        "resize" : "fit",
        "w" : 976
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FKpCihdJYf"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578220303987404800",
  "text" : "RT @bbcchinese: Spectacular northern lights in Cumbria, northern England. \u4ECA\u5E74\u6781\u5149\u6D3B\u52A8\u5F3A\u70C8\uFF0C\u4E0D\u7528\u53BB\u632A\u5A01\u6216\u51B0\u5C9B\uFF0C\u6628\u591C\u82F1\u683C\u5170\u5317\u90E8\u770B\u5230\u7EDA\u70C2\u7684\u6781\u5149\u3002 http:\/\/t.co\/FKpCihdJYf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bbcchinese\/status\/578123226997391360\/photo\/1",
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/FKpCihdJYf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAXoAsDUsAACJlR.jpg",
        "id_str" : "578123225709719552",
        "id" : 578123225709719552,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAXoAsDUsAACJlR.jpg",
        "sizes" : [ {
          "h" : 549,
          "resize" : "fit",
          "w" : 976
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 549,
          "resize" : "fit",
          "w" : 976
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 549,
          "resize" : "fit",
          "w" : 976
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FKpCihdJYf"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578123226997391360",
    "text" : "Spectacular northern lights in Cumbria, northern England. \u4ECA\u5E74\u6781\u5149\u6D3B\u52A8\u5F3A\u70C8\uFF0C\u4E0D\u7528\u53BB\u632A\u5A01\u6216\u51B0\u5C9B\uFF0C\u6628\u591C\u82F1\u683C\u5170\u5317\u90E8\u770B\u5230\u7EDA\u70C2\u7684\u6781\u5149\u3002 http:\/\/t.co\/FKpCihdJYf",
    "id" : 578123226997391360,
    "created_at" : "2015-03-18 09:18:12 +0000",
    "user" : {
      "name" : "BBC News \u4E2D\u6587",
      "screen_name" : "bbcchinese",
      "protected" : false,
      "id_str" : "791197",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004638591308435457\/tiacF_Yg_normal.jpg",
      "id" : 791197,
      "verified" : true
    }
  },
  "id" : 578220303987404800,
  "created_at" : "2015-03-18 15:43:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/lutkAb0wk0",
      "expanded_url" : "https:\/\/www.flickr.com\/photos\/ssyap\/favorites",
      "display_url" : "flickr.com\/photos\/ssyap\/f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578218756691853312",
  "text" : "Few of my fav photos on Flickr. https:\/\/t.co\/lutkAb0wk0",
  "id" : 578218756691853312,
  "created_at" : "2015-03-18 15:37:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "missbossy",
      "screen_name" : "missbossy",
      "indices" : [ 3, 13 ],
      "id_str" : "12073182",
      "id" : 12073182
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/missbossy\/status\/578217260742905856\/photo\/1",
      "indices" : [ 82, 104 ],
      "url" : "http:\/\/t.co\/Ios0Wzqbci",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAY9hBLUgAA0CsU.jpg",
      "id_str" : "578217239624581120",
      "id" : 578217239624581120,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAY9hBLUgAA0CsU.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Ios0Wzqbci"
    } ],
    "hashtags" : [ {
      "text" : "lky",
      "indices" : [ 38, 42 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578217709562900481",
  "text" : "RT @missbossy: Still nothing official #lky Getting kicked out. Visiting hrs over. http:\/\/t.co\/Ios0Wzqbci",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/missbossy\/status\/578217260742905856\/photo\/1",
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/Ios0Wzqbci",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAY9hBLUgAA0CsU.jpg",
        "id_str" : "578217239624581120",
        "id" : 578217239624581120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAY9hBLUgAA0CsU.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ios0Wzqbci"
      } ],
      "hashtags" : [ {
        "text" : "lky",
        "indices" : [ 23, 27 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578217260742905856",
    "text" : "Still nothing official #lky Getting kicked out. Visiting hrs over. http:\/\/t.co\/Ios0Wzqbci",
    "id" : 578217260742905856,
    "created_at" : "2015-03-18 15:31:52 +0000",
    "user" : {
      "name" : "missbossy",
      "screen_name" : "missbossy",
      "protected" : false,
      "id_str" : "12073182",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3770383786\/ea292be438dca8e3b8e0e0cdbda0b40c_normal.jpeg",
      "id" : 12073182,
      "verified" : false
    }
  },
  "id" : 578217709562900481,
  "created_at" : "2015-03-18 15:33:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Evil Tofu",
      "screen_name" : "eviltofu",
      "indices" : [ 0, 9 ],
      "id_str" : "1730901",
      "id" : 1730901
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "578215056577462272",
  "geo" : { },
  "id_str" : "578216177903804416",
  "in_reply_to_user_id" : 1730901,
  "text" : "@eviltofu I more concern about when my next project is coming.",
  "id" : 578216177903804416,
  "in_reply_to_status_id" : 578215056577462272,
  "created_at" : "2015-03-18 15:27:34 +0000",
  "in_reply_to_screen_name" : "eviltofu",
  "in_reply_to_user_id_str" : "1730901",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CCTV NEWS",
      "screen_name" : "cctvnews",
      "indices" : [ 44, 53 ],
      "id_str" : "810730525421867008",
      "id" : 810730525421867008
    }, {
      "name" : "CNN Breaking News",
      "screen_name" : "cnnbrk",
      "indices" : [ 60, 67 ],
      "id_str" : "428333",
      "id" : 428333
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LeeKuanYew",
      "indices" : [ 93, 104 ]
    }, {
      "text" : "LKY",
      "indices" : [ 129, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578215822633656320",
  "text" : "RT @firdianshah1: BREAKING &amp; EXCLUSIVE: @cctvnews joins @cnnbrk in misreporting death of #LeeKuanYew. (Photo: Reshmi Prakas) #LKY http:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "CCTV NEWS",
        "screen_name" : "cctvnews",
        "indices" : [ 26, 35 ],
        "id_str" : "810730525421867008",
        "id" : 810730525421867008
      }, {
        "name" : "CNN Breaking News",
        "screen_name" : "cnnbrk",
        "indices" : [ 42, 49 ],
        "id_str" : "428333",
        "id" : 428333
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/firdianshah1\/status\/578198369132118016\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/AobC46KnoY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAYsWgJUcAA_fEM.jpg",
        "id_str" : "578198367261454336",
        "id" : 578198367261454336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAYsWgJUcAA_fEM.jpg",
        "sizes" : [ {
          "h" : 593,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 593,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 562,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 593,
          "resize" : "fit",
          "w" : 717
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AobC46KnoY"
      } ],
      "hashtags" : [ {
        "text" : "LeeKuanYew",
        "indices" : [ 75, 86 ]
      }, {
        "text" : "LKY",
        "indices" : [ 111, 115 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578198369132118016",
    "text" : "BREAKING &amp; EXCLUSIVE: @cctvnews joins @cnnbrk in misreporting death of #LeeKuanYew. (Photo: Reshmi Prakas) #LKY http:\/\/t.co\/AobC46KnoY",
    "id" : 578198369132118016,
    "created_at" : "2015-03-18 14:16:48 +0000",
    "user" : {
      "name" : "Kyle Malinda-White",
      "screen_name" : "kylemalinda",
      "protected" : false,
      "id_str" : "393088142",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/724592908335116288\/2adSaIN5_normal.jpg",
      "id" : 393088142,
      "verified" : false
    }
  },
  "id" : 578215822633656320,
  "created_at" : "2015-03-18 15:26:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578149744830201856",
  "text" : "Since using Wordpress, I have stop using FTP until today.",
  "id" : 578149744830201856,
  "created_at" : "2015-03-18 11:03:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times News",
      "screen_name" : "IrishTimesNews",
      "indices" : [ 0, 15 ],
      "id_str" : "1182148069",
      "id" : 1182148069
    }, {
      "name" : "Shane McCarthy",
      "screen_name" : "TechShane",
      "indices" : [ 16, 26 ],
      "id_str" : "294289478",
      "id" : 294289478
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "578133055426039809",
  "geo" : { },
  "id_str" : "578144281535377408",
  "in_reply_to_user_id" : 1182148069,
  "text" : "@IrishTimesNews @TechShane Can see the irony here. From the sector that contribute to the mess to a party that going to mess thing up.",
  "id" : 578144281535377408,
  "in_reply_to_status_id" : 578133055426039809,
  "created_at" : "2015-03-18 10:41:52 +0000",
  "in_reply_to_screen_name" : "IrishTimesNews",
  "in_reply_to_user_id_str" : "1182148069",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 143 ],
      "url" : "https:\/\/t.co\/JaYshfAUts",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/full-life-good-death-034639880.html",
      "display_url" : "sg.news.yahoo.com\/full-life-good\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578139996357545984",
  "text" : "\"The man has had a good long run, a full life &amp; whatever his detractors may say, he took us to this point in time.\" https:\/\/t.co\/JaYshfAUts",
  "id" : 578139996357545984,
  "created_at" : "2015-03-18 10:24:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times News",
      "screen_name" : "IrishTimesNews",
      "indices" : [ 3, 18 ],
      "id_str" : "1182148069",
      "id" : 1182148069
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IrishTimesNews\/status\/578133055426039809\/photo\/1",
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/a2thwpUCYQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAXw814VIAER2F9.jpg",
      "id_str" : "578133055233138689",
      "id" : 578133055233138689,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAXw814VIAER2F9.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 540,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 540,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 540,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/a2thwpUCYQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/EaREfQccOv",
      "expanded_url" : "http:\/\/iti.ms\/1F2jbzt",
      "display_url" : "iti.ms\/1F2jbzt"
    } ]
  },
  "geo" : { },
  "id_str" : "578138377792446464",
  "text" : "RT @IrishTimesNews: Rise of Sinn F\u00E9in represents main threat to growth, says economist http:\/\/t.co\/EaREfQccOv http:\/\/t.co\/a2thwpUCYQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishTimesNews\/status\/578133055426039809\/photo\/1",
        "indices" : [ 90, 112 ],
        "url" : "http:\/\/t.co\/a2thwpUCYQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAXw814VIAER2F9.jpg",
        "id_str" : "578133055233138689",
        "id" : 578133055233138689,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAXw814VIAER2F9.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/a2thwpUCYQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/EaREfQccOv",
        "expanded_url" : "http:\/\/iti.ms\/1F2jbzt",
        "display_url" : "iti.ms\/1F2jbzt"
      } ]
    },
    "geo" : { },
    "id_str" : "578133055426039809",
    "text" : "Rise of Sinn F\u00E9in represents main threat to growth, says economist http:\/\/t.co\/EaREfQccOv http:\/\/t.co\/a2thwpUCYQ",
    "id" : 578133055426039809,
    "created_at" : "2015-03-18 09:57:16 +0000",
    "user" : {
      "name" : "Irish Times News",
      "screen_name" : "IrishTimesNews",
      "protected" : false,
      "id_str" : "1182148069",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3357059449\/42a39201cbf368a9d853fd6a0b0165fa_normal.png",
      "id" : 1182148069,
      "verified" : true
    }
  },
  "id" : 578138377792446464,
  "created_at" : "2015-03-18 10:18:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Juliani",
      "screen_name" : "yanijulie",
      "indices" : [ 3, 13 ],
      "id_str" : "16978926",
      "id" : 16978926
    }, {
      "name" : "Harishini",
      "screen_name" : "hareshene",
      "indices" : [ 18, 28 ],
      "id_str" : "202526795",
      "id" : 202526795
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578137964141760512",
  "text" : "RT @yanijulie: RT @hareshene Whatever our political views may be now, we cannot deny Mr Lee Kuan Yew's phenomenal contributions to our nati\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Harishini",
        "screen_name" : "hareshene",
        "indices" : [ 3, 13 ],
        "id_str" : "202526795",
        "id" : 202526795
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "578130286233309184",
    "geo" : { },
    "id_str" : "578130956168536064",
    "in_reply_to_user_id" : 202526795,
    "text" : "RT @hareshene Whatever our political views may be now, we cannot deny Mr Lee Kuan Yew's phenomenal contributions to our nation.",
    "id" : 578130956168536064,
    "in_reply_to_status_id" : 578130286233309184,
    "created_at" : "2015-03-18 09:48:55 +0000",
    "in_reply_to_screen_name" : "hareshene",
    "in_reply_to_user_id_str" : "202526795",
    "user" : {
      "name" : "Juliani",
      "screen_name" : "yanijulie",
      "protected" : false,
      "id_str" : "16978926",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/611555772510527488\/dKS8h2os_normal.jpg",
      "id" : 16978926,
      "verified" : false
    }
  },
  "id" : 578137964141760512,
  "created_at" : "2015-03-18 10:16:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/578137705055404032\/photo\/1",
      "indices" : [ 34, 56 ],
      "url" : "http:\/\/t.co\/gYKt5NvKjo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAX1Lc4WsAA5MMm.png",
      "id_str" : "578137704266903552",
      "id" : 578137704266903552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAX1Lc4WsAA5MMm.png",
      "sizes" : [ {
        "h" : 289,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 289,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 289,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 289,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gYKt5NvKjo"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578137705055404032",
  "text" : "Trending on Singapore Twitter.... http:\/\/t.co\/gYKt5NvKjo",
  "id" : 578137705055404032,
  "created_at" : "2015-03-18 10:15:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 0, 9 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "578133321391169537",
  "geo" : { },
  "id_str" : "578137109892108288",
  "in_reply_to_user_id" : 10410242,
  "text" : "@enormous Our first parade we went there early and manage to secure front row view but weather is chilling to wait for parade to start",
  "id" : 578137109892108288,
  "in_reply_to_status_id" : 578133321391169537,
  "created_at" : "2015-03-18 10:13:22 +0000",
  "in_reply_to_screen_name" : "enormous",
  "in_reply_to_user_id_str" : "10410242",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578136704210583553",
  "text" : "what are the skills you need to dabble with API? Thanks",
  "id" : 578136704210583553,
  "created_at" : "2015-03-18 10:11:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa domican",
      "screen_name" : "lisamareedom",
      "indices" : [ 3, 16 ],
      "id_str" : "18174738",
      "id" : 18174738
    }, {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 94, 105 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http:\/\/t.co\/0oSx0yFlHM",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/scheme-aims-to-create-jobs-for-those-with-autism-1.2838#.VQgRF113U2M.twitter",
      "display_url" : "irishtimes.com\/news\/scheme-ai\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578104360409808896",
  "text" : "RT @lisamareedom: Scheme aims to create jobs for those with autism http:\/\/t.co\/0oSx0yFlHM via @IrishTimes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Irish Times",
        "screen_name" : "IrishTimes",
        "indices" : [ 76, 87 ],
        "id_str" : "15084853",
        "id" : 15084853
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 71 ],
        "url" : "http:\/\/t.co\/0oSx0yFlHM",
        "expanded_url" : "http:\/\/www.irishtimes.com\/news\/scheme-aims-to-create-jobs-for-those-with-autism-1.2838#.VQgRF113U2M.twitter",
        "display_url" : "irishtimes.com\/news\/scheme-ai\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577795148198887424",
    "text" : "Scheme aims to create jobs for those with autism http:\/\/t.co\/0oSx0yFlHM via @IrishTimes",
    "id" : 577795148198887424,
    "created_at" : "2015-03-17 11:34:32 +0000",
    "user" : {
      "name" : "lisa domican",
      "screen_name" : "lisamareedom",
      "protected" : false,
      "id_str" : "18174738",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039583920780705792\/oMVyRkr6_normal.jpg",
      "id" : 18174738,
      "verified" : false
    }
  },
  "id" : 578104360409808896,
  "created_at" : "2015-03-18 08:03:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AspireAsia",
      "screen_name" : "AspireAsia",
      "indices" : [ 3, 14 ],
      "id_str" : "318582782",
      "id" : 318582782
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 16, 26 ]
    }, {
      "text" : "safestcity",
      "indices" : [ 45, 56 ]
    }, {
      "text" : "world",
      "indices" : [ 64, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/qi94Ws20vr",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/20-safest-cities-in-the-world-182247656.html?linkId=12959596",
      "display_url" : "sg.news.yahoo.com\/20-safest-citi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "578091424790380544",
  "text" : "RT @AspireAsia: #Singapore ranked as the 2nd #safestcity in the #world - https:\/\/t.co\/qi94Ws20vr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 0, 10 ]
      }, {
        "text" : "safestcity",
        "indices" : [ 29, 40 ]
      }, {
        "text" : "world",
        "indices" : [ 48, 54 ]
      } ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/qi94Ws20vr",
        "expanded_url" : "https:\/\/sg.news.yahoo.com\/20-safest-cities-in-the-world-182247656.html?linkId=12959596",
        "display_url" : "sg.news.yahoo.com\/20-safest-citi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "578033636320026624",
    "text" : "#Singapore ranked as the 2nd #safestcity in the #world - https:\/\/t.co\/qi94Ws20vr",
    "id" : 578033636320026624,
    "created_at" : "2015-03-18 03:22:12 +0000",
    "user" : {
      "name" : "AspireAsia",
      "screen_name" : "AspireAsia",
      "protected" : false,
      "id_str" : "318582782",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/702103952951611392\/BRT3ji8O_normal.png",
      "id" : 318582782,
      "verified" : false
    }
  },
  "id" : 578091424790380544,
  "created_at" : "2015-03-18 07:11:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Travis Teo",
      "screen_name" : "scenz",
      "indices" : [ 3, 9 ],
      "id_str" : "24850339",
      "id" : 24850339
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/scenz\/status\/578045161772425216\/photo\/1",
      "indices" : [ 107, 129 ],
      "url" : "http:\/\/t.co\/slAwphoTGa",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAWg_k8U0AAz_gy.jpg",
      "id_str" : "578045141295878144",
      "id" : 578045141295878144,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAWg_k8U0AAz_gy.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/slAwphoTGa"
    } ],
    "hashtags" : [ {
      "text" : "saphyperconnect",
      "indices" : [ 85, 101 ]
    }, {
      "text" : "iot",
      "indices" : [ 102, 106 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "578091054643056640",
  "text" : "RT @scenz: Google acquisition of NEST is the Netscape moment for Internet of things. #saphyperconnect #iot http:\/\/t.co\/slAwphoTGa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/scenz\/status\/578045161772425216\/photo\/1",
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/slAwphoTGa",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAWg_k8U0AAz_gy.jpg",
        "id_str" : "578045141295878144",
        "id" : 578045141295878144,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAWg_k8U0AAz_gy.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/slAwphoTGa"
      } ],
      "hashtags" : [ {
        "text" : "saphyperconnect",
        "indices" : [ 74, 90 ]
      }, {
        "text" : "iot",
        "indices" : [ 91, 95 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "578045161772425216",
    "text" : "Google acquisition of NEST is the Netscape moment for Internet of things. #saphyperconnect #iot http:\/\/t.co\/slAwphoTGa",
    "id" : 578045161772425216,
    "created_at" : "2015-03-18 04:08:00 +0000",
    "user" : {
      "name" : "Travis Teo",
      "screen_name" : "scenz",
      "protected" : false,
      "id_str" : "24850339",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992574774051196929\/rS9h-HJD_normal.jpg",
      "id" : 24850339,
      "verified" : false
    }
  },
  "id" : 578091054643056640,
  "created_at" : "2015-03-18 07:10:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "indices" : [ 3, 16 ],
      "id_str" : "19698834",
      "id" : 19698834
    }, {
      "name" : "OCBC Bank",
      "screen_name" : "OCBCBank",
      "indices" : [ 19, 28 ],
      "id_str" : "166793353",
      "id" : 166793353
    }, {
      "name" : "DBS Bank",
      "screen_name" : "dbsbank",
      "indices" : [ 38, 46 ],
      "id_str" : "95310018",
      "id" : 95310018
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "socialmedia",
      "indices" : [ 62, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 83, 105 ],
      "url" : "http:\/\/t.co\/MJAJgolDRQ",
      "expanded_url" : "http:\/\/nyv.me\/l\/4gga",
      "display_url" : "nyv.me\/l\/4gga"
    } ]
  },
  "geo" : { },
  "id_str" : "578090845754142720",
  "text" : "RT @MarketingEds: .@OCBCBank Bank and @dbsbank in a spat over #socialmedia success http:\/\/t.co\/MJAJgolDRQ. But are fans and followers the b\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "OCBC Bank",
        "screen_name" : "OCBCBank",
        "indices" : [ 1, 10 ],
        "id_str" : "166793353",
        "id" : 166793353
      }, {
        "name" : "DBS Bank",
        "screen_name" : "dbsbank",
        "indices" : [ 20, 28 ],
        "id_str" : "95310018",
        "id" : 95310018
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "socialmedia",
        "indices" : [ 44, 56 ]
      } ],
      "urls" : [ {
        "indices" : [ 65, 87 ],
        "url" : "http:\/\/t.co\/MJAJgolDRQ",
        "expanded_url" : "http:\/\/nyv.me\/l\/4gga",
        "display_url" : "nyv.me\/l\/4gga"
      } ]
    },
    "geo" : { },
    "id_str" : "578060157705326593",
    "text" : ".@OCBCBank Bank and @dbsbank in a spat over #socialmedia success http:\/\/t.co\/MJAJgolDRQ. But are fans and followers the best gauge of it?",
    "id" : 578060157705326593,
    "created_at" : "2015-03-18 05:07:36 +0000",
    "user" : {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "protected" : false,
      "id_str" : "19698834",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948003366164033536\/_ojmapns_normal.jpg",
      "id" : 19698834,
      "verified" : false
    }
  },
  "id" : 578090845754142720,
  "created_at" : "2015-03-18 07:09:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577971311860928512",
  "text" : "Successful small nations positive narratives always go by these lines : \"punch above its weight\" and \"tiny but great nation\"",
  "id" : 577971311860928512,
  "created_at" : "2015-03-17 23:14:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "shopper culture",
      "screen_name" : "ShopperCulture",
      "indices" : [ 3, 18 ],
      "id_str" : "18819429",
      "id" : 18819429
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ShopperSummit",
      "indices" : [ 109, 123 ]
    }, {
      "text" : "TIGshopper",
      "indices" : [ 124, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577969793837445120",
  "text" : "RT @ShopperCulture: \"Ask yourself, why do you need real time data if you're not making real time decisions?\" #ShopperSummit #TIGshopper",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ShopperSummit",
        "indices" : [ 89, 103 ]
      }, {
        "text" : "TIGshopper",
        "indices" : [ 104, 115 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577927150994608129",
    "text" : "\"Ask yourself, why do you need real time data if you're not making real time decisions?\" #ShopperSummit #TIGshopper",
    "id" : 577927150994608129,
    "created_at" : "2015-03-17 20:19:04 +0000",
    "user" : {
      "name" : "shopper culture",
      "screen_name" : "ShopperCulture",
      "protected" : false,
      "id_str" : "18819429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/546080545998794752\/J4cSl1_h_normal.png",
      "id" : 18819429,
      "verified" : false
    }
  },
  "id" : 577969793837445120,
  "created_at" : "2015-03-17 23:08:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat O'Mahony",
      "screen_name" : "patomahony1",
      "indices" : [ 3, 15 ],
      "id_str" : "103558465",
      "id" : 103558465
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jeremyclarkson",
      "indices" : [ 44, 59 ]
    }, {
      "text" : "DirtyRottenScoundrels",
      "indices" : [ 74, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http:\/\/t.co\/mIBRSaOmgr",
      "expanded_url" : "http:\/\/www.patomahony.ie\/2015\/03\/dirty-rotten-scoundrels\/",
      "display_url" : "patomahony.ie\/2015\/03\/dirty-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577968426372739072",
  "text" : "RT @patomahony1: When presenters go bad: re #jeremyclarkson some tales of #DirtyRottenScoundrels I've worked w\/ http:\/\/t.co\/mIBRSaOmgr http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/janetter.net\/\" rel=\"nofollow\"\u003EJanetter\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/patomahony1\/status\/577533489974784000\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/45nTfNgslX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAPPpjAWQAEsdmF.jpg",
        "id_str" : "577533489911840769",
        "id" : 577533489911840769,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAPPpjAWQAEsdmF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 276,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 276,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 276,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 276,
          "resize" : "fit",
          "w" : 460
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/45nTfNgslX"
      } ],
      "hashtags" : [ {
        "text" : "jeremyclarkson",
        "indices" : [ 27, 42 ]
      }, {
        "text" : "DirtyRottenScoundrels",
        "indices" : [ 57, 79 ]
      } ],
      "urls" : [ {
        "indices" : [ 95, 117 ],
        "url" : "http:\/\/t.co\/mIBRSaOmgr",
        "expanded_url" : "http:\/\/www.patomahony.ie\/2015\/03\/dirty-rotten-scoundrels\/",
        "display_url" : "patomahony.ie\/2015\/03\/dirty-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577781400037904384",
    "text" : "When presenters go bad: re #jeremyclarkson some tales of #DirtyRottenScoundrels I've worked w\/ http:\/\/t.co\/mIBRSaOmgr http:\/\/t.co\/45nTfNgslX",
    "id" : 577781400037904384,
    "created_at" : "2015-03-17 10:39:55 +0000",
    "user" : {
      "name" : "Pat O'Mahony",
      "screen_name" : "patomahony1",
      "protected" : false,
      "id_str" : "103558465",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/995370152488259586\/yGrn6cSW_normal.jpg",
      "id" : 103558465,
      "verified" : true
    }
  },
  "id" : 577968426372739072,
  "created_at" : "2015-03-17 23:03:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TIME",
      "screen_name" : "TIME",
      "indices" : [ 3, 8 ],
      "id_str" : "14293310",
      "id" : 14293310
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http:\/\/t.co\/HmanNxj5GN",
      "expanded_url" : "http:\/\/ti.me\/1EmFWZy",
      "display_url" : "ti.me\/1EmFWZy"
    } ]
  },
  "geo" : { },
  "id_str" : "577898022186647552",
  "text" : "RT @TIME: How St. Patrick\u2019s Day became the most global national holiday http:\/\/t.co\/HmanNxj5GN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 84 ],
        "url" : "http:\/\/t.co\/HmanNxj5GN",
        "expanded_url" : "http:\/\/ti.me\/1EmFWZy",
        "display_url" : "ti.me\/1EmFWZy"
      } ]
    },
    "geo" : { },
    "id_str" : "577825474753232897",
    "text" : "How St. Patrick\u2019s Day became the most global national holiday http:\/\/t.co\/HmanNxj5GN",
    "id" : 577825474753232897,
    "created_at" : "2015-03-17 13:35:03 +0000",
    "user" : {
      "name" : "TIME",
      "screen_name" : "TIME",
      "protected" : false,
      "id_str" : "14293310",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1700796190\/Picture_24_normal.png",
      "id" : 14293310,
      "verified" : true
    }
  },
  "id" : 577898022186647552,
  "created_at" : "2015-03-17 18:23:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577894017389760512",
  "text" : "Everyone already has an e-mail list. It called Linkedin. Do keep sharing Ideas, Questions, Articles, and Links BEFORE you email them!",
  "id" : 577894017389760512,
  "created_at" : "2015-03-17 18:07:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Panic Dots",
      "screen_name" : "panicdots",
      "indices" : [ 3, 13 ],
      "id_str" : "20281026",
      "id" : 20281026
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/PanicDots\/status\/577831906101825536\/photo\/1",
      "indices" : [ 55, 77 ],
      "url" : "http:\/\/t.co\/sia8py73EB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CATfDpPWQAAuKtK.png",
      "id_str" : "577831905913094144",
      "id" : 577831905913094144,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CATfDpPWQAAuKtK.png",
      "sizes" : [ {
        "h" : 479,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 636
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sia8py73EB"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577837489613856768",
  "text" : "RT @PanicDots: Hong Kong is asking the tough questions http:\/\/t.co\/sia8py73EB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PanicDots\/status\/577831906101825536\/photo\/1",
        "indices" : [ 40, 62 ],
        "url" : "http:\/\/t.co\/sia8py73EB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CATfDpPWQAAuKtK.png",
        "id_str" : "577831905913094144",
        "id" : 577831905913094144,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CATfDpPWQAAuKtK.png",
        "sizes" : [ {
          "h" : 479,
          "resize" : "fit",
          "w" : 636
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 636
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 636
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 636
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sia8py73EB"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577831906101825536",
    "text" : "Hong Kong is asking the tough questions http:\/\/t.co\/sia8py73EB",
    "id" : 577831906101825536,
    "created_at" : "2015-03-17 14:00:36 +0000",
    "user" : {
      "name" : "Panic Dots",
      "screen_name" : "panicdots",
      "protected" : false,
      "id_str" : "20281026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/919797450046500864\/iDjyCoDy_normal.jpg",
      "id" : 20281026,
      "verified" : false
    }
  },
  "id" : 577837489613856768,
  "created_at" : "2015-03-17 14:22:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Irish For \uD83E\uDD64\u26BD\uFE0F\uD83C\uDFC3\uD83C\uDFFB\u200D\u2642\uFE0F",
      "screen_name" : "theirishfor",
      "indices" : [ 3, 15 ],
      "id_str" : "2978345031",
      "id" : 2978345031
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 92, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 69, 91 ],
      "url" : "http:\/\/t.co\/Yy9pM2G6jm",
      "expanded_url" : "http:\/\/goo.gl\/nF17Ex",
      "display_url" : "goo.gl\/nF17Ex"
    } ]
  },
  "geo" : { },
  "id_str" : "577798717509611520",
  "text" : "RT @theirishfor: Some Irish words that found their way into English: http:\/\/t.co\/Yy9pM2G6jm #StPatricksDay",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 75, 89 ]
      } ],
      "urls" : [ {
        "indices" : [ 52, 74 ],
        "url" : "http:\/\/t.co\/Yy9pM2G6jm",
        "expanded_url" : "http:\/\/goo.gl\/nF17Ex",
        "display_url" : "goo.gl\/nF17Ex"
      } ]
    },
    "geo" : { },
    "id_str" : "577795300447817728",
    "text" : "Some Irish words that found their way into English: http:\/\/t.co\/Yy9pM2G6jm #StPatricksDay",
    "id" : 577795300447817728,
    "created_at" : "2015-03-17 11:35:09 +0000",
    "user" : {
      "name" : "The Irish For \uD83E\uDD64\u26BD\uFE0F\uD83C\uDFC3\uD83C\uDFFB\u200D\u2642\uFE0F",
      "screen_name" : "theirishfor",
      "protected" : false,
      "id_str" : "2978345031",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033112789211127808\/CzyXxf1e_normal.jpg",
      "id" : 2978345031,
      "verified" : false
    }
  },
  "id" : 577798717509611520,
  "created_at" : "2015-03-17 11:48:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Carter",
      "screen_name" : "withoutdoing",
      "indices" : [ 3, 16 ],
      "id_str" : "435020266",
      "id" : 435020266
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577798498453712896",
  "text" : "RT @withoutdoing: Today's Chinese: \u80BE6 (sh\u00E8n li\u00F9) \u201CKidney 6\" - slang for an iPhone 6 emphasizing how expensive it is (costs a kidney, figura\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577795508279885824",
    "text" : "Today's Chinese: \u80BE6 (sh\u00E8n li\u00F9) \u201CKidney 6\" - slang for an iPhone 6 emphasizing how expensive it is (costs a kidney, figuratively speaking)",
    "id" : 577795508279885824,
    "created_at" : "2015-03-17 11:35:58 +0000",
    "user" : {
      "name" : "Liz Carter",
      "screen_name" : "withoutdoing",
      "protected" : false,
      "id_str" : "435020266",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/600731240812032001\/Z4afrP4k_normal.jpg",
      "id" : 435020266,
      "verified" : true
    }
  },
  "id" : 577798498453712896,
  "created_at" : "2015-03-17 11:47:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Anderson \uD83D\uDE2C\uD83D\uDC4D",
      "screen_name" : "supergingerdave",
      "indices" : [ 3, 19 ],
      "id_str" : "14383171",
      "id" : 14383171
    }, {
      "name" : "chris horn",
      "screen_name" : "chrisjhorn",
      "indices" : [ 21, 32 ],
      "id_str" : "24028945",
      "id" : 24028945
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 33, 39 ],
      "id_str" : "9465632",
      "id" : 9465632
    }, {
      "name" : "SensiPass",
      "screen_name" : "Sensipass",
      "indices" : [ 64, 74 ],
      "id_str" : "242956030",
      "id" : 242956030
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ireland",
      "indices" : [ 122, 130 ]
    }, {
      "text" : "winning",
      "indices" : [ 131, 139 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577769228721385472",
  "text" : "RT @supergingerdave: @chrisjhorn @mryap he doesn't realise that @Sensipass are already doing this, right here in Ireland! #ireland #winning",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "chris horn",
        "screen_name" : "chrisjhorn",
        "indices" : [ 0, 11 ],
        "id_str" : "24028945",
        "id" : 24028945
      }, {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 12, 18 ],
        "id_str" : "9465632",
        "id" : 9465632
      }, {
        "name" : "SensiPass",
        "screen_name" : "Sensipass",
        "indices" : [ 43, 53 ],
        "id_str" : "242956030",
        "id" : 242956030
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ireland",
        "indices" : [ 101, 109 ]
      }, {
        "text" : "winning",
        "indices" : [ 110, 118 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "577751519946088448",
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 53.64878482634308, -6.671788355329037 ]
    },
    "id_str" : "577756998999138304",
    "in_reply_to_user_id" : 24028945,
    "text" : "@chrisjhorn @mryap he doesn't realise that @Sensipass are already doing this, right here in Ireland! #ireland #winning",
    "id" : 577756998999138304,
    "in_reply_to_status_id" : 577751519946088448,
    "created_at" : "2015-03-17 09:02:57 +0000",
    "in_reply_to_screen_name" : "chrisjhorn",
    "in_reply_to_user_id_str" : "24028945",
    "user" : {
      "name" : "Dave Anderson \uD83D\uDE2C\uD83D\uDC4D",
      "screen_name" : "supergingerdave",
      "protected" : false,
      "id_str" : "14383171",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972558202704465922\/uaO3fFrt_normal.jpg",
      "id" : 14383171,
      "verified" : false
    }
  },
  "id" : 577769228721385472,
  "created_at" : "2015-03-17 09:51:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "noah kagan",
      "screen_name" : "noahkagan",
      "indices" : [ 0, 10 ],
      "id_str" : "13737",
      "id" : 13737
    }, {
      "name" : "Meerkat",
      "screen_name" : "AppMeerkat",
      "indices" : [ 11, 22 ],
      "id_str" : "708741916204994560",
      "id" : 708741916204994560
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "577664596967755776",
  "geo" : { },
  "id_str" : "577753357038804992",
  "in_reply_to_user_id" : 13737,
  "text" : "@noahkagan @AppMeerkat I saw a tapeworm.",
  "id" : 577753357038804992,
  "in_reply_to_status_id" : 577664596967755776,
  "created_at" : "2015-03-17 08:48:29 +0000",
  "in_reply_to_screen_name" : "noahkagan",
  "in_reply_to_user_id_str" : "13737",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Linda Yueh",
      "screen_name" : "lindayueh",
      "indices" : [ 3, 13 ],
      "id_str" : "350958562",
      "id" : 350958562
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577751968342532097",
  "text" : "RT @lindayueh: In 1990, 36% of the world\u2019s population lived in abject poverty. By 2010 this was down to 18%. End poverty by 2030?  https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/yDrbdPEx8r",
        "expanded_url" : "https:\/\/www.economist.com\/news\/finance-and-economics\/21645220-goal-ending-poverty-2030-worthy-increasingly-out-reach-povertys?fsrc=scn\/fb\/te\/pe\/ed\/povertyslongfarewell",
        "display_url" : "economist.com\/news\/finance-a\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577735329144274944",
    "text" : "In 1990, 36% of the world\u2019s population lived in abject poverty. By 2010 this was down to 18%. End poverty by 2030?  https:\/\/t.co\/yDrbdPEx8r",
    "id" : 577735329144274944,
    "created_at" : "2015-03-17 07:36:50 +0000",
    "user" : {
      "name" : "Linda Yueh",
      "screen_name" : "lindayueh",
      "protected" : false,
      "id_str" : "350958562",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3643565732\/613f9dec02cf8db979917d661ef64f3a_normal.jpeg",
      "id" : 350958562,
      "verified" : true
    }
  },
  "id" : 577751968342532097,
  "created_at" : "2015-03-17 08:42:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 103 ],
      "url" : "http:\/\/t.co\/trtIpaiLOv",
      "expanded_url" : "http:\/\/www.popsugar.com\/tech\/Eric-Schmidt-Interrupts-Megan-Smith-SXSW-37093103",
      "display_url" : "popsugar.com\/tech\/Eric-Schm\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577748987349991424",
  "text" : "RT @klillington: Oh Snap! Google Exec Just Got Brilliantly Called Out by a Woman http:\/\/t.co\/trtIpaiLOv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http:\/\/t.co\/trtIpaiLOv",
        "expanded_url" : "http:\/\/www.popsugar.com\/tech\/Eric-Schmidt-Interrupts-Megan-Smith-SXSW-37093103",
        "display_url" : "popsugar.com\/tech\/Eric-Schm\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577702938342092800",
    "text" : "Oh Snap! Google Exec Just Got Brilliantly Called Out by a Woman http:\/\/t.co\/trtIpaiLOv",
    "id" : 577702938342092800,
    "created_at" : "2015-03-17 05:28:08 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 577748987349991424,
  "created_at" : "2015-03-17 08:31:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mamoon Hamid",
      "screen_name" : "mamoonha",
      "indices" : [ 3, 12 ],
      "id_str" : "191098143",
      "id" : 191098143
    }, {
      "name" : "Bobby Pinero",
      "screen_name" : "bobbypinero",
      "indices" : [ 65, 77 ],
      "id_str" : "1209289981",
      "id" : 1209289981
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 125 ],
      "url" : "http:\/\/t.co\/YQwpMy7J1Z",
      "expanded_url" : "http:\/\/blog.intercom.io\/saas-metrics-for-fundraising\/",
      "display_url" : "blog.intercom.io\/saas-metrics-f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577748098254028801",
  "text" : "RT @mamoonha: Everyone in SaaS should read and save this post by @bobbypinero. Metrics for Fundraising http:\/\/t.co\/YQwpMy7J1Z",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bobby Pinero",
        "screen_name" : "bobbypinero",
        "indices" : [ 51, 63 ],
        "id_str" : "1209289981",
        "id" : 1209289981
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 111 ],
        "url" : "http:\/\/t.co\/YQwpMy7J1Z",
        "expanded_url" : "http:\/\/blog.intercom.io\/saas-metrics-for-fundraising\/",
        "display_url" : "blog.intercom.io\/saas-metrics-f\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577724389581520896",
    "text" : "Everyone in SaaS should read and save this post by @bobbypinero. Metrics for Fundraising http:\/\/t.co\/YQwpMy7J1Z",
    "id" : 577724389581520896,
    "created_at" : "2015-03-17 06:53:22 +0000",
    "user" : {
      "name" : "Mamoon Hamid",
      "screen_name" : "mamoonha",
      "protected" : false,
      "id_str" : "191098143",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037588915849293824\/RZFYXFEU_normal.jpg",
      "id" : 191098143,
      "verified" : true
    }
  },
  "id" : 577748098254028801,
  "created_at" : "2015-03-17 08:27:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elaine (fierce shining light) Larkin",
      "screen_name" : "elainelarkin",
      "indices" : [ 3, 16 ],
      "id_str" : "15452449",
      "id" : 15452449
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SaintPatricksDay",
      "indices" : [ 62, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577746969382264832",
  "text" : "RT @elainelarkin: The three essential emoji you must use this #SaintPatricksDay \uD83D\uDC0D\uD83C\uDF40\uD83D\uDC9A",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SaintPatricksDay",
        "indices" : [ 44, 61 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577746835470684160",
    "text" : "The three essential emoji you must use this #SaintPatricksDay \uD83D\uDC0D\uD83C\uDF40\uD83D\uDC9A",
    "id" : 577746835470684160,
    "created_at" : "2015-03-17 08:22:34 +0000",
    "user" : {
      "name" : "Elaine (fierce shining light) Larkin",
      "screen_name" : "elainelarkin",
      "protected" : false,
      "id_str" : "15452449",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039984575567613952\/fsQrS8gm_normal.jpg",
      "id" : 15452449,
      "verified" : false
    }
  },
  "id" : 577746969382264832,
  "created_at" : "2015-03-17 08:23:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melinda Gates",
      "screen_name" : "melindagates",
      "indices" : [ 3, 16 ],
      "id_str" : "161801527",
      "id" : 161801527
    }, {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 97, 110 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/qfxIyjHYWW",
      "expanded_url" : "http:\/\/m-gat.es\/1O0Atjl",
      "display_url" : "m-gat.es\/1O0Atjl"
    } ]
  },
  "geo" : { },
  "id_str" : "577745283557298176",
  "text" : "RT @melindagates: Which are the most progressive countries for working women? Check out the data @TheEconomist http:\/\/t.co\/qfxIyjHYWW http:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Economist",
        "screen_name" : "TheEconomist",
        "indices" : [ 79, 92 ],
        "id_str" : "5988062",
        "id" : 5988062
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/melindagates\/status\/577727341352366081\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/Hag7gZiXa9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAR_9LCWEAAJ2c0.png",
        "id_str" : "577727341121638400",
        "id" : 577727341121638400,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAR_9LCWEAAJ2c0.png",
        "sizes" : [ {
          "h" : 351,
          "resize" : "fit",
          "w" : 658
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 658
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 658
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 658
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Hag7gZiXa9"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/qfxIyjHYWW",
        "expanded_url" : "http:\/\/m-gat.es\/1O0Atjl",
        "display_url" : "m-gat.es\/1O0Atjl"
      } ]
    },
    "geo" : { },
    "id_str" : "577727341352366081",
    "text" : "Which are the most progressive countries for working women? Check out the data @TheEconomist http:\/\/t.co\/qfxIyjHYWW http:\/\/t.co\/Hag7gZiXa9",
    "id" : 577727341352366081,
    "created_at" : "2015-03-17 07:05:06 +0000",
    "user" : {
      "name" : "Melinda Gates",
      "screen_name" : "melindagates",
      "protected" : false,
      "id_str" : "161801527",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/576426383989080065\/HoFESJ2k_normal.jpeg",
      "id" : 161801527,
      "verified" : true
    }
  },
  "id" : 577745283557298176,
  "created_at" : "2015-03-17 08:16:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jerome",
      "screen_name" : "JeromeR",
      "indices" : [ 3, 11 ],
      "id_str" : "6630162",
      "id" : 6630162
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577642629514571776",
  "text" : "RT @JeromeR: Received an email notice: a relative died. The device appended \"Sent from my [branded device]\" because it's relevant and usefu\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "UX",
        "indices" : [ 137, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577642263901147136",
    "text" : "Received an email notice: a relative died. The device appended \"Sent from my [branded device]\" because it's relevant and useful to know. #UX",
    "id" : 577642263901147136,
    "created_at" : "2015-03-17 01:27:02 +0000",
    "user" : {
      "name" : "Jerome",
      "screen_name" : "JeromeR",
      "protected" : false,
      "id_str" : "6630162",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/872435779347374081\/pbzYnrJw_normal.jpg",
      "id" : 6630162,
      "verified" : false
    }
  },
  "id" : 577642629514571776,
  "created_at" : "2015-03-17 01:28:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http:\/\/t.co\/f7ZI1yfCgM",
      "expanded_url" : "http:\/\/bloom.bg\/1CjRYGv",
      "display_url" : "bloom.bg\/1CjRYGv"
    } ]
  },
  "geo" : { },
  "id_str" : "577642022443552768",
  "text" : "Germans Tired of Greek Demands Want Country to Exit Euro http:\/\/t.co\/f7ZI1yfCgM",
  "id" : 577642022443552768,
  "created_at" : "2015-03-17 01:26:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sam Bishop",
      "screen_name" : "Samue1",
      "indices" : [ 3, 10 ],
      "id_str" : "19818376",
      "id" : 19818376
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/RIt8iRcTCY",
      "expanded_url" : "http:\/\/www.happenings.ie\/blog\/refocus-from-chaos-to-culture\/VQFxaSMAACBBfpee",
      "display_url" : "happenings.ie\/blog\/refocus-f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577641007577214976",
  "text" : "RT @Samue1: \"THIS is the Ireland we want to celebrate\" Why we're running an alcohol-free Paddy's Day festival: http:\/\/t.co\/RIt8iRcTCY #Padd\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "PaddysDayUnlocked",
        "indices" : [ 122, 140 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/RIt8iRcTCY",
        "expanded_url" : "http:\/\/www.happenings.ie\/blog\/refocus-from-chaos-to-culture\/VQFxaSMAACBBfpee",
        "display_url" : "happenings.ie\/blog\/refocus-f\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "576370654552977408",
    "text" : "\"THIS is the Ireland we want to celebrate\" Why we're running an alcohol-free Paddy's Day festival: http:\/\/t.co\/RIt8iRcTCY #PaddysDayUnlocked",
    "id" : 576370654552977408,
    "created_at" : "2015-03-13 13:14:07 +0000",
    "user" : {
      "name" : "Sam Bishop",
      "screen_name" : "Samue1",
      "protected" : false,
      "id_str" : "19818376",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/999355956981985280\/j66-dr5y_normal.jpg",
      "id" : 19818376,
      "verified" : false
    }
  },
  "id" : 577641007577214976,
  "created_at" : "2015-03-17 01:22:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tam\u00E1s L\u00E9b",
      "screen_name" : "tamasleb",
      "indices" : [ 3, 12 ],
      "id_str" : "2846461850",
      "id" : 2846461850
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/tamasleb\/status\/575561703867441152\/photo\/1",
      "indices" : [ 51, 73 ],
      "url" : "http:\/\/t.co\/0awj1p1ZGW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_zOUZ7XEAAc0wc.jpg",
      "id_str" : "575561702349148160",
      "id" : 575561702349148160,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_zOUZ7XEAAc0wc.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 366,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 366,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 366,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 366,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0awj1p1ZGW"
    } ],
    "hashtags" : [ {
      "text" : "UX",
      "indices" : [ 37, 40 ]
    }, {
      "text" : "uxdesign",
      "indices" : [ 41, 50 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577609985930240000",
  "text" : "RT @tamasleb: Age verification WIN!  #UX #uxdesign http:\/\/t.co\/0awj1p1ZGW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tamasleb\/status\/575561703867441152\/photo\/1",
        "indices" : [ 37, 59 ],
        "url" : "http:\/\/t.co\/0awj1p1ZGW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_zOUZ7XEAAc0wc.jpg",
        "id_str" : "575561702349148160",
        "id" : 575561702349148160,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_zOUZ7XEAAc0wc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 366,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 366,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 366,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 366,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0awj1p1ZGW"
      } ],
      "hashtags" : [ {
        "text" : "UX",
        "indices" : [ 23, 26 ]
      }, {
        "text" : "uxdesign",
        "indices" : [ 27, 36 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "575561703867441152",
    "text" : "Age verification WIN!  #UX #uxdesign http:\/\/t.co\/0awj1p1ZGW",
    "id" : 575561703867441152,
    "created_at" : "2015-03-11 07:39:38 +0000",
    "user" : {
      "name" : "Tam\u00E1s L\u00E9b",
      "screen_name" : "tamasleb",
      "protected" : false,
      "id_str" : "2846461850",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/706078872731852800\/Qs78te_9_normal.jpg",
      "id" : 2846461850,
      "verified" : false
    }
  },
  "id" : 577609985930240000,
  "created_at" : "2015-03-16 23:18:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/aheAqTxSIj",
      "expanded_url" : "https:\/\/support.google.com\/youtube\/answer\/6165415",
      "display_url" : "support.google.com\/youtube\/answer\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577606232300720128",
  "text" : "YouTube introduced 6 types of Card &amp; performance reporting is available in YouTube Analytics. https:\/\/t.co\/aheAqTxSIj",
  "id" : 577606232300720128,
  "created_at" : "2015-03-16 23:03:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edward Harrison",
      "screen_name" : "edwardnh",
      "indices" : [ 3, 12 ],
      "id_str" : "16400258",
      "id" : 16400258
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/bbHfn9fe4U",
      "expanded_url" : "http:\/\/www.theguardian.com\/money\/2015\/mar\/16\/ireland-tops-global-house-price-index",
      "display_url" : "theguardian.com\/money\/2015\/mar\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577569902074195969",
  "text" : "RT @edwardnh: House prices: Ireland's gains most in the world (GDP now projected up 4.8% in 2015). http:\/\/t.co\/bbHfn9fe4U http:\/\/t.co\/cfF7S\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/edwardnh\/status\/577543079856586752\/photo\/1",
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/cfF7SE25oC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAPYK-oVAAAp51D.png",
        "id_str" : "577542860355993600",
        "id" : 577542860355993600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAPYK-oVAAAp51D.png",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 547
        }, {
          "h" : 795,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 795,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 795,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cfF7SE25oC"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/bbHfn9fe4U",
        "expanded_url" : "http:\/\/www.theguardian.com\/money\/2015\/mar\/16\/ireland-tops-global-house-price-index",
        "display_url" : "theguardian.com\/money\/2015\/mar\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577543079856586752",
    "text" : "House prices: Ireland's gains most in the world (GDP now projected up 4.8% in 2015). http:\/\/t.co\/bbHfn9fe4U http:\/\/t.co\/cfF7SE25oC",
    "id" : 577543079856586752,
    "created_at" : "2015-03-16 18:52:55 +0000",
    "user" : {
      "name" : "Edward Harrison",
      "screen_name" : "edwardnh",
      "protected" : false,
      "id_str" : "16400258",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/859167555327582208\/CBa9eKMp_normal.jpg",
      "id" : 16400258,
      "verified" : true
    }
  },
  "id" : 577569902074195969,
  "created_at" : "2015-03-16 20:39:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 0, 12 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "Irish Chamber Commerce SG",
      "screen_name" : "IrishChambSing",
      "indices" : [ 13, 28 ],
      "id_str" : "2825780695",
      "id" : 2825780695
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "577056139927392256",
  "geo" : { },
  "id_str" : "577521526435635200",
  "in_reply_to_user_id" : 979910827,
  "text" : "@GeoffreyIRL @IrishChambSing What brings Minister Frances Fitzgerald to Singapore?",
  "id" : 577521526435635200,
  "in_reply_to_status_id" : 577056139927392256,
  "created_at" : "2015-03-16 17:27:16 +0000",
  "in_reply_to_screen_name" : "GeoffreyIRL",
  "in_reply_to_user_id_str" : "979910827",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startup",
      "indices" : [ 26, 34 ]
    }, {
      "text" : "getoptimise",
      "indices" : [ 103, 115 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 58 ],
      "url" : "http:\/\/t.co\/tYGSDo72An",
      "expanded_url" : "http:\/\/namefka.com\/",
      "display_url" : "namefka.com"
    } ]
  },
  "geo" : { },
  "id_str" : "577520393939996673",
  "text" : "Do you need $50K for your #startup? http:\/\/t.co\/tYGSDo72An If you win, hire me to be your web analyst. #getoptimise",
  "id" : 577520393939996673,
  "created_at" : "2015-03-16 17:22:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 113, 122 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 24, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577519321158717440",
  "text" : "Dear Tourists coming to #Ireland for St Patrick's Day : It's tradition to let the kids in front at parade. HT to @enormous",
  "id" : 577519321158717440,
  "created_at" : "2015-03-16 17:18:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577515588312014849",
  "text" : "Quick survey: Ask a shopper for postal code &amp; only show what's available, or let them browse products they might not be able to buy?",
  "id" : 577515588312014849,
  "created_at" : "2015-03-16 17:03:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/577498189999259648\/photo\/1",
      "indices" : [ 20, 42 ],
      "url" : "http:\/\/t.co\/CVsj9ehe0D",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAOvixhWYAAlGrw.png",
      "id_str" : "577498189177184256",
      "id" : 577498189177184256,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAOvixhWYAAlGrw.png",
      "sizes" : [ {
        "h" : 512,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 512,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 512,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/CVsj9ehe0D"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577498189999259648",
  "text" : "Thought of the Day. http:\/\/t.co\/CVsj9ehe0D",
  "id" : 577498189999259648,
  "created_at" : "2015-03-16 15:54:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Theo Lynn",
      "screen_name" : "theolynn",
      "indices" : [ 3, 12 ],
      "id_str" : "22193590",
      "id" : 22193590
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/theolynn\/status\/577474560422981632\/photo\/1",
      "indices" : [ 96, 118 ],
      "url" : "http:\/\/t.co\/wx1jSB0ju9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAOaDDiWYAENYxo.jpg",
      "id_str" : "577474554513219585",
      "id" : 577474554513219585,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAOaDDiWYAENYxo.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 592,
        "resize" : "fit",
        "w" : 592
      }, {
        "h" : 592,
        "resize" : "fit",
        "w" : 592
      }, {
        "h" : 592,
        "resize" : "fit",
        "w" : 592
      }, {
        "h" : 592,
        "resize" : "fit",
        "w" : 592
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wx1jSB0ju9"
    } ],
    "hashtags" : [ {
      "text" : "TLC",
      "indices" : [ 41, 45 ]
    }, {
      "text" : "snickers",
      "indices" : [ 46, 55 ]
    }, {
      "text" : "jeremyclarkson",
      "indices" : [ 56, 71 ]
    }, {
      "text" : "Clarksongate",
      "indices" : [ 72, 85 ]
    }, {
      "text" : "clarkson",
      "indices" : [ 86, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577481023212163072",
  "text" : "RT @theolynn: Opportunistic marketing or #TLC #snickers #jeremyclarkson #Clarksongate #clarkson http:\/\/t.co\/wx1jSB0ju9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/theolynn\/status\/577474560422981632\/photo\/1",
        "indices" : [ 82, 104 ],
        "url" : "http:\/\/t.co\/wx1jSB0ju9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAOaDDiWYAENYxo.jpg",
        "id_str" : "577474554513219585",
        "id" : 577474554513219585,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAOaDDiWYAENYxo.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 592
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 592
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 592
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 592
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wx1jSB0ju9"
      } ],
      "hashtags" : [ {
        "text" : "TLC",
        "indices" : [ 27, 31 ]
      }, {
        "text" : "snickers",
        "indices" : [ 32, 41 ]
      }, {
        "text" : "jeremyclarkson",
        "indices" : [ 42, 57 ]
      }, {
        "text" : "Clarksongate",
        "indices" : [ 58, 71 ]
      }, {
        "text" : "clarkson",
        "indices" : [ 72, 81 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577474560422981632",
    "text" : "Opportunistic marketing or #TLC #snickers #jeremyclarkson #Clarksongate #clarkson http:\/\/t.co\/wx1jSB0ju9",
    "id" : 577474560422981632,
    "created_at" : "2015-03-16 14:20:38 +0000",
    "user" : {
      "name" : "Theo Lynn",
      "screen_name" : "theolynn",
      "protected" : false,
      "id_str" : "22193590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/623594385150709760\/vqt1kYEu_normal.jpg",
      "id" : 22193590,
      "verified" : false
    }
  },
  "id" : 577481023212163072,
  "created_at" : "2015-03-16 14:46:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577461261358784512",
  "text" : "RT @yapphenghui: Routledge are offering free access to the 25 most read articles from all of the Arts &amp; Humanities subject areas. http:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/ryMPKeQHOD",
        "expanded_url" : "http:\/\/explore.tandfonline.com\/page\/ah\/most-read-2015",
        "display_url" : "explore.tandfonline.com\/page\/ah\/most-r\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577459972977496067",
    "text" : "Routledge are offering free access to the 25 most read articles from all of the Arts &amp; Humanities subject areas. http:\/\/t.co\/ryMPKeQHOD",
    "id" : 577459972977496067,
    "created_at" : "2015-03-16 13:22:40 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 577461261358784512,
  "created_at" : "2015-03-16 13:27:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "williamxu\u5A01\u5EC9\u9000\u5C14",
      "screen_name" : "mynamexu",
      "indices" : [ 3, 12 ],
      "id_str" : "49693637",
      "id" : 49693637
    }, {
      "name" : "FMN \u81EA\u66F2\u65B0\u95FB",
      "screen_name" : "freemoren",
      "indices" : [ 30, 40 ],
      "id_str" : "9027902",
      "id" : 9027902
    }, {
      "name" : "\u7EBD\u7EA6\u65F6\u62A5\u4E2D\u6587\u7F51",
      "screen_name" : "nytchinese",
      "indices" : [ 47, 58 ],
      "id_str" : "620632841",
      "id" : 620632841
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/nytchinese\/status\/577307779850899456\/photo\/1",
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/2dcBK0pXh9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAMCPKGVAAAh7-O.jpg",
      "id_str" : "577307636665745408",
      "id" : 577307636665745408,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAMCPKGVAAAh7-O.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 433,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 433,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 433,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 433,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2dcBK0pXh9"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http:\/\/t.co\/UNjwFK73vY",
      "expanded_url" : "http:\/\/cn.nytimes.com\/china\/20150312\/c12dalailama\/",
      "display_url" : "cn.nytimes.com\/china\/20150312\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577458602014863360",
  "text" : "RT @mynamexu: \u8FD9\u6F2B\u753B\u9887\u6709\u4E01\u806A\u8001\u5148\u751F\u7684\u9057\u97F5\u3002RT@freemoren  \u8F6C RT @nytchinese: \u3010\u6F2B\u753B\uFF1A\u4E2D\u56FD\u548C\u8FBE\u8D56\u5587\u561B\u3011 http:\/\/t.co\/UNjwFK73vY \u2026 http:\/\/t.co\/2dcBK0pXh9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "FMN \u81EA\u66F2\u65B0\u95FB",
        "screen_name" : "freemoren",
        "indices" : [ 16, 26 ],
        "id_str" : "9027902",
        "id" : 9027902
      }, {
        "name" : "\u7EBD\u7EA6\u65F6\u62A5\u4E2D\u6587\u7F51",
        "screen_name" : "nytchinese",
        "indices" : [ 33, 44 ],
        "id_str" : "620632841",
        "id" : 620632841
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nytchinese\/status\/577307779850899456\/photo\/1",
        "indices" : [ 84, 106 ],
        "url" : "http:\/\/t.co\/2dcBK0pXh9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAMCPKGVAAAh7-O.jpg",
        "id_str" : "577307636665745408",
        "id" : 577307636665745408,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAMCPKGVAAAh7-O.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 433,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 433,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 433,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 433,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2dcBK0pXh9"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 81 ],
        "url" : "http:\/\/t.co\/UNjwFK73vY",
        "expanded_url" : "http:\/\/cn.nytimes.com\/china\/20150312\/c12dalailama\/",
        "display_url" : "cn.nytimes.com\/china\/20150312\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "577451391838351360",
    "text" : "\u8FD9\u6F2B\u753B\u9887\u6709\u4E01\u806A\u8001\u5148\u751F\u7684\u9057\u97F5\u3002RT@freemoren  \u8F6C RT @nytchinese: \u3010\u6F2B\u753B\uFF1A\u4E2D\u56FD\u548C\u8FBE\u8D56\u5587\u561B\u3011 http:\/\/t.co\/UNjwFK73vY \u2026 http:\/\/t.co\/2dcBK0pXh9",
    "id" : 577451391838351360,
    "created_at" : "2015-03-16 12:48:34 +0000",
    "user" : {
      "name" : "williamxu\u5A01\u5EC9\u9000\u5C14",
      "screen_name" : "mynamexu",
      "protected" : false,
      "id_str" : "49693637",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2355536613\/phpNEfiQr_normal",
      "id" : 49693637,
      "verified" : false
    }
  },
  "id" : 577458602014863360,
  "created_at" : "2015-03-16 13:17:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577455681453821953",
  "text" : "Don't brag about your website has responsive design feature in your email newsletter. This should be a given.",
  "id" : 577455681453821953,
  "created_at" : "2015-03-16 13:05:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577453190339276800",
  "text" : "For someone who has no achievement in professional life, a quick refund by a SaaS provider is the highlight moment on Monday before lunch.",
  "id" : 577453190339276800,
  "created_at" : "2015-03-16 12:55:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SG50YouWish",
      "indices" : [ 104, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577451767308382209",
  "text" : "I hope govt liberal immigration policy is going to guarantee Singapore a victory at the 2026 World Cup. #SG50YouWish",
  "id" : 577451767308382209,
  "created_at" : "2015-03-16 12:50:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/6eyPGiaNpm",
      "expanded_url" : "http:\/\/www.wired.com\/2015\/03\/jojakim-cortis-adrian-sonderegger-iconen\/",
      "display_url" : "wired.com\/2015\/03\/jojaki\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577449714972540928",
  "text" : "Some of the most famous photographs in history, have been recreated in miniature model form by two photographers. http:\/\/t.co\/6eyPGiaNpm",
  "id" : 577449714972540928,
  "created_at" : "2015-03-16 12:41:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/ZhLexhOywG",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/577438064089391104",
      "display_url" : "twitter.com\/mryap\/status\/5\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "577440282892976129",
  "text" : "If there is a any indication, I think I might need to work till I die...https:\/\/t.co\/ZhLexhOywG",
  "id" : 577440282892976129,
  "created_at" : "2015-03-16 12:04:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GSElevator",
      "screen_name" : "GSElevator",
      "indices" : [ 3, 14 ],
      "id_str" : "352947624",
      "id" : 352947624
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577439343247298560",
  "text" : "RT @GSElevator: \"If women really get paid 20% less for the same work, wouldn't a shareholder conscious company just hire only women?\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "527460771647614976",
    "text" : "\"If women really get paid 20% less for the same work, wouldn't a shareholder conscious company just hire only women?\"",
    "id" : 527460771647614976,
    "created_at" : "2014-10-29 14:03:42 +0000",
    "user" : {
      "name" : "GSElevator",
      "screen_name" : "GSElevator",
      "protected" : false,
      "id_str" : "352947624",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1594623967\/LloydBlankfeinLookingSkeptical_normal.jpg",
      "id" : 352947624,
      "verified" : false
    }
  },
  "id" : 577439343247298560,
  "created_at" : "2015-03-16 12:00:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 40 ],
      "url" : "http:\/\/t.co\/hyhtigmwpw",
      "expanded_url" : "http:\/\/bond11plus.co.uk",
      "display_url" : "bond11plus.co.uk"
    } ]
  },
  "geo" : { },
  "id_str" : "577438064089391104",
  "text" : "Like the revamped http:\/\/t.co\/hyhtigmwpw website. I know it school holiday but my kid is my retirement plan.",
  "id" : 577438064089391104,
  "created_at" : "2015-03-16 11:55:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stephen Conmy",
      "screen_name" : "digital_times",
      "indices" : [ 3, 17 ],
      "id_str" : "84589508",
      "id" : 84589508
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/Yb9qzovyp9",
      "expanded_url" : "http:\/\/shrd.by\/9VzaEn",
      "display_url" : "shrd.by\/9VzaEn"
    } ]
  },
  "geo" : { },
  "id_str" : "577428570357809152",
  "text" : "RT @digital_times: Ireland\u2019s largest ever peer-to-peer loan has just happened http:\/\/t.co\/Yb9qzovyp9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.sharedby.co\" rel=\"nofollow\"\u003ESharedBy\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 81 ],
        "url" : "http:\/\/t.co\/Yb9qzovyp9",
        "expanded_url" : "http:\/\/shrd.by\/9VzaEn",
        "display_url" : "shrd.by\/9VzaEn"
      } ]
    },
    "geo" : { },
    "id_str" : "577427636017852416",
    "text" : "Ireland\u2019s largest ever peer-to-peer loan has just happened http:\/\/t.co\/Yb9qzovyp9",
    "id" : 577427636017852416,
    "created_at" : "2015-03-16 11:14:11 +0000",
    "user" : {
      "name" : "Stephen Conmy",
      "screen_name" : "digital_times",
      "protected" : false,
      "id_str" : "84589508",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014139098750210048\/e9AfAk4i_normal.jpg",
      "id" : 84589508,
      "verified" : false
    }
  },
  "id" : 577428570357809152,
  "created_at" : "2015-03-16 11:17:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/577428440422432769\/photo\/1",
      "indices" : [ 37, 59 ],
      "url" : "http:\/\/t.co\/dlDjCO74al",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CANwGzbW4AAwHKK.jpg",
      "id_str" : "577428439420035072",
      "id" : 577428439420035072,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CANwGzbW4AAwHKK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1944,
        "resize" : "fit",
        "w" : 2592
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dlDjCO74al"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577428440422432769",
  "text" : "Tucking in to some local ethnic food http:\/\/t.co\/dlDjCO74al",
  "id" : 577428440422432769,
  "created_at" : "2015-03-16 11:17:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Business Support Helpline",
      "screen_name" : "businessgov",
      "indices" : [ 3, 15 ],
      "id_str" : "19707318",
      "id" : 19707318
    }, {
      "name" : "Growth Vouchers",
      "screen_name" : "growthvouchers",
      "indices" : [ 44, 59 ],
      "id_str" : "1926387211",
      "id" : 1926387211
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "business",
      "indices" : [ 69, 78 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577421433997037568",
  "text" : "RT @businessgov: Not long left to apply for @growthvouchers for your #business. Up to \u00A32k towards strategic advice. Apply today! http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Growth Vouchers",
        "screen_name" : "growthvouchers",
        "indices" : [ 27, 42 ],
        "id_str" : "1926387211",
        "id" : 1926387211
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/businessgov\/status\/577156105320599553\/photo\/1",
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/oZtWpGZluI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_-xScPVAAAu9qA.jpg",
        "id_str" : "576374207702958080",
        "id" : 576374207702958080,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_-xScPVAAAu9qA.jpg",
        "sizes" : [ {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oZtWpGZluI"
      } ],
      "hashtags" : [ {
        "text" : "business",
        "indices" : [ 52, 61 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577156105320599553",
    "text" : "Not long left to apply for @growthvouchers for your #business. Up to \u00A32k towards strategic advice. Apply today! http:\/\/t.co\/oZtWpGZluI",
    "id" : 577156105320599553,
    "created_at" : "2015-03-15 17:15:13 +0000",
    "user" : {
      "name" : "Business Support Helpline",
      "screen_name" : "businessgov",
      "protected" : false,
      "id_str" : "19707318",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/831799801121488897\/_a-pP05c_normal.jpg",
      "id" : 19707318,
      "verified" : true
    }
  },
  "id" : 577421433997037568,
  "created_at" : "2015-03-16 10:49:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Unsuck It",
      "screen_name" : "unsuckit",
      "indices" : [ 103, 112 ],
      "id_str" : "146899007",
      "id" : 146899007
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/yZ88cKEqyt",
      "expanded_url" : "http:\/\/unsuck-it.com\/?p=101",
      "display_url" : "unsuck-it.com\/?p=101"
    } ]
  },
  "geo" : { },
  "id_str" : "577418231385866240",
  "text" : "If people have Ninja in their Linkedin profile, this is what it really means. : http:\/\/t.co\/yZ88cKEqyt @unsuckit",
  "id" : 577418231385866240,
  "created_at" : "2015-03-16 10:36:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577413498977824768",
  "text" : "Hit a roadblock at work. Instead of pushing on, taking a break hopefully to goes back fresh.",
  "id" : 577413498977824768,
  "created_at" : "2015-03-16 10:18:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Powell",
      "screen_name" : "ShootTokyo",
      "indices" : [ 3, 14 ],
      "id_str" : "205967864",
      "id" : 205967864
    }, {
      "name" : "The Wall Street Journal",
      "screen_name" : "WSJ",
      "indices" : [ 120, 124 ],
      "id_str" : "3108351",
      "id" : 3108351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/7KpvTKlqIB",
      "expanded_url" : "http:\/\/on.wsj.com\/1L966bg",
      "display_url" : "on.wsj.com\/1L966bg"
    } ]
  },
  "geo" : { },
  "id_str" : "577164978236559360",
  "text" : "RT @ShootTokyo: Leica has struggled with the death of film, but now it has a lucrative niche http:\/\/t.co\/7KpvTKlqIB via @WSJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Wall Street Journal",
        "screen_name" : "WSJ",
        "indices" : [ 104, 108 ],
        "id_str" : "3108351",
        "id" : 3108351
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 99 ],
        "url" : "http:\/\/t.co\/7KpvTKlqIB",
        "expanded_url" : "http:\/\/on.wsj.com\/1L966bg",
        "display_url" : "on.wsj.com\/1L966bg"
      } ]
    },
    "geo" : { },
    "id_str" : "576935120172498944",
    "text" : "Leica has struggled with the death of film, but now it has a lucrative niche http:\/\/t.co\/7KpvTKlqIB via @WSJ",
    "id" : 576935120172498944,
    "created_at" : "2015-03-15 02:37:06 +0000",
    "user" : {
      "name" : "Dave Powell",
      "screen_name" : "ShootTokyo",
      "protected" : false,
      "id_str" : "205967864",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784721760516083712\/LCvcEjmf_normal.jpg",
      "id" : 205967864,
      "verified" : true
    }
  },
  "id" : 577164978236559360,
  "created_at" : "2015-03-15 17:50:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft Singapore",
      "screen_name" : "Microsoft_SG",
      "indices" : [ 3, 16 ],
      "id_str" : "229311774",
      "id" : 229311774
    }, {
      "name" : "Michelle Simmons",
      "screen_name" : "mwsimm",
      "indices" : [ 95, 102 ],
      "id_str" : "27080560",
      "id" : 27080560
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MakeItHappen",
      "indices" : [ 81, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 125 ],
      "url" : "http:\/\/t.co\/JkfRvbO4Xl",
      "expanded_url" : "http:\/\/spr.ly\/6015268d",
      "display_url" : "spr.ly\/6015268d"
    } ]
  },
  "geo" : { },
  "id_str" : "577119569606795265",
  "text" : "RT @Microsoft_SG: Michelle Simmons: Women are critical to encouraging innovation #MakeItHappen @mwsimm http:\/\/t.co\/JkfRvbO4Xl http:\/\/t.co\/v\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Michelle Simmons",
        "screen_name" : "mwsimm",
        "indices" : [ 77, 84 ],
        "id_str" : "27080560",
        "id" : 27080560
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Microsoft_SG\/status\/576279264783417344\/photo\/1",
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/vkot1MXQ2n",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_9a8BpWMAA7hI3.png",
        "id_str" : "576279264607219712",
        "id" : 576279264607219712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_9a8BpWMAA7hI3.png",
        "sizes" : [ {
          "h" : 495,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 329,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 495,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 495,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vkot1MXQ2n"
      } ],
      "hashtags" : [ {
        "text" : "MakeItHappen",
        "indices" : [ 63, 76 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/JkfRvbO4Xl",
        "expanded_url" : "http:\/\/spr.ly\/6015268d",
        "display_url" : "spr.ly\/6015268d"
      } ]
    },
    "geo" : { },
    "id_str" : "576279264783417344",
    "text" : "Michelle Simmons: Women are critical to encouraging innovation #MakeItHappen @mwsimm http:\/\/t.co\/JkfRvbO4Xl http:\/\/t.co\/vkot1MXQ2n",
    "id" : 576279264783417344,
    "created_at" : "2015-03-13 07:10:58 +0000",
    "user" : {
      "name" : "Microsoft Singapore",
      "screen_name" : "Microsoft_SG",
      "protected" : false,
      "id_str" : "229311774",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875540682764505088\/agBHzBu__normal.jpg",
      "id" : 229311774,
      "verified" : true
    }
  },
  "id" : 577119569606795265,
  "created_at" : "2015-03-15 14:50:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Erin Khoo",
      "screen_name" : "erinkhoo",
      "indices" : [ 3, 12 ],
      "id_str" : "18742611",
      "id" : 18742611
    }, {
      "name" : "TNW",
      "screen_name" : "TheNextWeb",
      "indices" : [ 105, 116 ],
      "id_str" : "10876852",
      "id" : 10876852
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/5yWpKeVIfO",
      "expanded_url" : "http:\/\/tnw.to\/g4q18",
      "display_url" : "tnw.to\/g4q18"
    } ]
  },
  "geo" : { },
  "id_str" : "577086573453398016",
  "text" : "RT @erinkhoo: Why European startups shouldn\u2019t fundraise in the Silicon Valley http:\/\/t.co\/5yWpKeVIfO via @thenextweb or should they?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TNW",
        "screen_name" : "TheNextWeb",
        "indices" : [ 91, 102 ],
        "id_str" : "10876852",
        "id" : 10876852
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http:\/\/t.co\/5yWpKeVIfO",
        "expanded_url" : "http:\/\/tnw.to\/g4q18",
        "display_url" : "tnw.to\/g4q18"
      } ]
    },
    "geo" : { },
    "id_str" : "577086291772383233",
    "text" : "Why European startups shouldn\u2019t fundraise in the Silicon Valley http:\/\/t.co\/5yWpKeVIfO via @thenextweb or should they?",
    "id" : 577086291772383233,
    "created_at" : "2015-03-15 12:37:48 +0000",
    "user" : {
      "name" : "Erin Khoo",
      "screen_name" : "erinkhoo",
      "protected" : false,
      "id_str" : "18742611",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/491984039809531904\/u0SZLZFm_normal.jpeg",
      "id" : 18742611,
      "verified" : false
    }
  },
  "id" : 577086573453398016,
  "created_at" : "2015-03-15 12:38:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/577057393411944449\/photo\/1",
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/IpdFAcK2Dc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAIepEfUgAAc0gl.jpg",
      "id_str" : "577057393185423360",
      "id" : 577057393185423360,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAIepEfUgAAc0gl.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IpdFAcK2Dc"
    } ],
    "hashtags" : [ {
      "text" : "GoGreen4PatricksDay",
      "indices" : [ 64, 84 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "577072939251675137",
  "text" : "RT @GeoffreyIRL: Great turnout for today's parade in Singapore! #GoGreen4PatricksDay http:\/\/t.co\/IpdFAcK2Dc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/577057393411944449\/photo\/1",
        "indices" : [ 68, 90 ],
        "url" : "http:\/\/t.co\/IpdFAcK2Dc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAIepEfUgAAc0gl.jpg",
        "id_str" : "577057393185423360",
        "id" : 577057393185423360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAIepEfUgAAc0gl.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IpdFAcK2Dc"
      } ],
      "hashtags" : [ {
        "text" : "GoGreen4PatricksDay",
        "indices" : [ 47, 67 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "577057393411944449",
    "text" : "Great turnout for today's parade in Singapore! #GoGreen4PatricksDay http:\/\/t.co\/IpdFAcK2Dc",
    "id" : 577057393411944449,
    "created_at" : "2015-03-15 10:42:58 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 577072939251675137,
  "created_at" : "2015-03-15 11:44:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 125, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/Q2LS6svYhR",
      "expanded_url" : "https:\/\/www.closedloop.com\/make-your-metrics-meaningful-3-must-have-rules-for-any-report#more-42",
      "display_url" : "closedloop.com\/make-your-metr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576801009198071808",
  "text" : "Incorporate contextualization, annotation and label to add meaning and value to your metrics report. https:\/\/t.co\/Q2LS6svYhR #getoptimise",
  "id" : 576801009198071808,
  "created_at" : "2015-03-14 17:44:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 3, 12 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 14, 20 ],
      "id_str" : "9465632",
      "id" : 9465632
    }, {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 21, 31 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 32, 39 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 40, 48 ],
      "id_str" : "227274783",
      "id" : 227274783
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hottopic",
      "indices" : [ 69, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http:\/\/t.co\/M5hcmkZ2dj",
      "expanded_url" : "http:\/\/m.thelocal.de\/20140617\/are-you-an-expat-or-immigrant-in-germany",
      "display_url" : "m.thelocal.de\/20140617\/are-y\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576784135500476416",
  "text" : "RT @Fondacey: @mryap @starduest @sweden @Capesgo Similar discussion. #hottopic http:\/\/t.co\/M5hcmkZ2dj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      }, {
        "name" : "check bio",
        "screen_name" : "starduest",
        "indices" : [ 7, 17 ],
        "id_str" : "824059172731830272",
        "id" : 824059172731830272
      }, {
        "name" : "@sweden \/ Rebecka",
        "screen_name" : "sweden",
        "indices" : [ 18, 25 ],
        "id_str" : "19663706",
        "id" : 19663706
      }, {
        "name" : "Carlos A. Perdomo",
        "screen_name" : "Capesgo",
        "indices" : [ 26, 34 ],
        "id_str" : "227274783",
        "id" : 227274783
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "hottopic",
        "indices" : [ 55, 64 ]
      } ],
      "urls" : [ {
        "indices" : [ 65, 87 ],
        "url" : "http:\/\/t.co\/M5hcmkZ2dj",
        "expanded_url" : "http:\/\/m.thelocal.de\/20140617\/are-you-an-expat-or-immigrant-in-germany",
        "display_url" : "m.thelocal.de\/20140617\/are-y\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "576679807410376704",
    "geo" : { },
    "id_str" : "576752952137900032",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap @starduest @sweden @Capesgo Similar discussion. #hottopic http:\/\/t.co\/M5hcmkZ2dj",
    "id" : 576752952137900032,
    "in_reply_to_status_id" : 576679807410376704,
    "created_at" : "2015-03-14 14:33:13 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "protected" : false,
      "id_str" : "19338425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3157014212\/d431af4ae6cd671dfa76a92f7ceceee4_normal.jpeg",
      "id" : 19338425,
      "verified" : false
    }
  },
  "id" : 576784135500476416,
  "created_at" : "2015-03-14 16:37:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Harvey's Point",
      "screen_name" : "HarveysPoint",
      "indices" : [ 3, 16 ],
      "id_str" : "44926937",
      "id" : 44926937
    }, {
      "name" : "Harvey's Point",
      "screen_name" : "HarveysPoint",
      "indices" : [ 81, 94 ],
      "id_str" : "44926937",
      "id" : 44926937
    }, {
      "name" : "Donegal Tourism",
      "screen_name" : "govisitdonegal",
      "indices" : [ 95, 110 ],
      "id_str" : "1111743175",
      "id" : 1111743175
    }, {
      "name" : "Wild Atlantic Way",
      "screen_name" : "wildatlanticway",
      "indices" : [ 111, 127 ],
      "id_str" : "525580086",
      "id" : 525580086
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576695946320461825",
  "text" : "RT @HarveysPoint: *\u201CGood Morning from Sunny Lough Eske! What a beautiful morning @HarveysPoint @govisitdonegal @wildatlanticway http:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Harvey's Point",
        "screen_name" : "HarveysPoint",
        "indices" : [ 63, 76 ],
        "id_str" : "44926937",
        "id" : 44926937
      }, {
        "name" : "Donegal Tourism",
        "screen_name" : "govisitdonegal",
        "indices" : [ 77, 92 ],
        "id_str" : "1111743175",
        "id" : 1111743175
      }, {
        "name" : "Wild Atlantic Way",
        "screen_name" : "wildatlanticway",
        "indices" : [ 93, 109 ],
        "id_str" : "525580086",
        "id" : 525580086
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SheilaRuss80\/status\/576339231561408512\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/ORlB6PHaU7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_-RegyU8AABgS4.jpg",
        "id_str" : "576339230709837824",
        "id" : 576339230709837824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_-RegyU8AABgS4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ORlB6PHaU7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "576342487477972992",
    "text" : "*\u201CGood Morning from Sunny Lough Eske! What a beautiful morning @HarveysPoint @govisitdonegal @wildatlanticway http:\/\/t.co\/ORlB6PHaU7\u201D",
    "id" : 576342487477972992,
    "created_at" : "2015-03-13 11:22:11 +0000",
    "user" : {
      "name" : "Harvey's Point",
      "screen_name" : "HarveysPoint",
      "protected" : false,
      "id_str" : "44926937",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788432414246854656\/DLYUR02a_normal.jpg",
      "id" : 44926937,
      "verified" : true
    }
  },
  "id" : 576695946320461825,
  "created_at" : "2015-03-14 10:46:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576687762075680768",
  "text" : "@bharatidalela Are you at the measurecamp today?",
  "id" : 576687762075680768,
  "created_at" : "2015-03-14 10:14:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicolas Malo",
      "screen_name" : "nicolasmalo",
      "indices" : [ 3, 15 ],
      "id_str" : "19016916",
      "id" : 19016916
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/nicolasmalo\/status\/576687046284193793\/photo\/1",
      "indices" : [ 47, 69 ],
      "url" : "http:\/\/t.co\/YfzSkj0AZK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CADNzuLWsAA175l.jpg",
      "id_str" : "576687040756101120",
      "id" : 576687040756101120,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CADNzuLWsAA175l.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YfzSkj0AZK"
    } ],
    "hashtags" : [ {
      "text" : "measurecamp",
      "indices" : [ 34, 46 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576687496014225408",
  "text" : "RT @nicolasmalo: Swear jar swears #measurecamp http:\/\/t.co\/YfzSkj0AZK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nicolasmalo\/status\/576687046284193793\/photo\/1",
        "indices" : [ 30, 52 ],
        "url" : "http:\/\/t.co\/YfzSkj0AZK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CADNzuLWsAA175l.jpg",
        "id_str" : "576687040756101120",
        "id" : 576687040756101120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CADNzuLWsAA175l.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YfzSkj0AZK"
      } ],
      "hashtags" : [ {
        "text" : "measurecamp",
        "indices" : [ 17, 29 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "576687046284193793",
    "text" : "Swear jar swears #measurecamp http:\/\/t.co\/YfzSkj0AZK",
    "id" : 576687046284193793,
    "created_at" : "2015-03-14 10:11:20 +0000",
    "user" : {
      "name" : "Nicolas Malo",
      "screen_name" : "nicolasmalo",
      "protected" : false,
      "id_str" : "19016916",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422638203376390144\/VBHQe-Ca_normal.png",
      "id" : 19016916,
      "verified" : false
    }
  },
  "id" : 576687496014225408,
  "created_at" : "2015-03-14 10:13:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Moritz Heininger",
      "screen_name" : "MoritzHeininger",
      "indices" : [ 3, 19 ],
      "id_str" : "2768223248",
      "id" : 2768223248
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576687248617394176",
  "text" : "RT @MoritzHeininger: What are the three ways to train your brain to be happy? And why do pessimists make great lawyers? http:\/\/t.co\/Z6b1cnq\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TIME Ideas",
        "screen_name" : "TIMEIdeas",
        "indices" : [ 126, 136 ],
        "id_str" : "386742828",
        "id" : 386742828
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/Z6b1cnqMED",
        "expanded_url" : "http:\/\/ti.me\/1BUM0HW",
        "display_url" : "ti.me\/1BUM0HW"
      } ]
    },
    "geo" : { },
    "id_str" : "576686831883952128",
    "text" : "What are the three ways to train your brain to be happy? And why do pessimists make great lawyers? http:\/\/t.co\/Z6b1cnqMED via @TIMEIdeas",
    "id" : 576686831883952128,
    "created_at" : "2015-03-14 10:10:29 +0000",
    "user" : {
      "name" : "Moritz Heininger",
      "screen_name" : "MoritzHeininger",
      "protected" : false,
      "id_str" : "2768223248",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/523694108921978880\/vL5fD7cU_normal.jpeg",
      "id" : 2768223248,
      "verified" : false
    }
  },
  "id" : 576687248617394176,
  "created_at" : "2015-03-14 10:12:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 0, 9 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 10, 20 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 72 ],
      "url" : "http:\/\/t.co\/7nFojjczsL",
      "expanded_url" : "http:\/\/www.theguardian.com\/global-development-professionals-network\/2015\/mar\/13\/white-people-expats-immigrants-migration?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/global-develop\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "574895662237614080",
  "geo" : { },
  "id_str" : "576679807410376704",
  "in_reply_to_user_id" : 19338425,
  "text" : "@Fondacey @starduest @sweden @Capesgo Here you go http:\/\/t.co\/7nFojjczsL",
  "id" : 576679807410376704,
  "in_reply_to_status_id" : 574895662237614080,
  "created_at" : "2015-03-14 09:42:34 +0000",
  "in_reply_to_screen_name" : "Fondacey",
  "in_reply_to_user_id_str" : "19338425",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DW \u4E2D\u6587- \u5FB7\u56FD\u4E4B\u58F0",
      "screen_name" : "dw_chinese",
      "indices" : [ 3, 14 ],
      "id_str" : "143810986",
      "id" : 143810986
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/8tcLoLstsT",
      "expanded_url" : "http:\/\/bit.ly\/18my2H0",
      "display_url" : "bit.ly\/18my2H0"
    } ]
  },
  "geo" : { },
  "id_str" : "576677974759596032",
  "text" : "RT @dw_chinese: \u5E0C\u814A\uFF1A\u903C\u6211\u9000\u6B27\u5143 \u5FB7\u56FD\u4F1A\u201C\u7206\u70B8\u201D: \u5728\u6B27\u76DF\u4E0E\u5E0C\u814A\u7684\u5173\u7CFB\u4E2D\uFF0C\u5E0C\u814A\u56FD\u9632\u90E8\u957F\u5361\u95E8\u8BFA\u65AF\u7EDD\u5BF9\u662F\u5531\u201C\u9ED1\u8138\u201D\u7684\u89D2\u8272\u3002\u5728\u653E\u51FA\u5411\u5FB7\u56FD\u8F93\u9001\u96BE\u6C11\u7684\u5A01\u80C1\u540E\uFF0C\u73B0\u5728\u53C8\u4EAE\u51FA\u4E86\u201C\u4E00\u635F\u4FF1\u635F\u201D\u7684\u62DB\u5F0F\u3002 http:\/\/t.co\/8tcLoLstsT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterfeed.com\" rel=\"nofollow\"\u003Etwitterfeed\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/8tcLoLstsT",
        "expanded_url" : "http:\/\/bit.ly\/18my2H0",
        "display_url" : "bit.ly\/18my2H0"
      } ]
    },
    "geo" : { },
    "id_str" : "576653095666499584",
    "text" : "\u5E0C\u814A\uFF1A\u903C\u6211\u9000\u6B27\u5143 \u5FB7\u56FD\u4F1A\u201C\u7206\u70B8\u201D: \u5728\u6B27\u76DF\u4E0E\u5E0C\u814A\u7684\u5173\u7CFB\u4E2D\uFF0C\u5E0C\u814A\u56FD\u9632\u90E8\u957F\u5361\u95E8\u8BFA\u65AF\u7EDD\u5BF9\u662F\u5531\u201C\u9ED1\u8138\u201D\u7684\u89D2\u8272\u3002\u5728\u653E\u51FA\u5411\u5FB7\u56FD\u8F93\u9001\u96BE\u6C11\u7684\u5A01\u80C1\u540E\uFF0C\u73B0\u5728\u53C8\u4EAE\u51FA\u4E86\u201C\u4E00\u635F\u4FF1\u635F\u201D\u7684\u62DB\u5F0F\u3002 http:\/\/t.co\/8tcLoLstsT",
    "id" : 576653095666499584,
    "created_at" : "2015-03-14 07:56:26 +0000",
    "user" : {
      "name" : "DW \u4E2D\u6587- \u5FB7\u56FD\u4E4B\u58F0",
      "screen_name" : "dw_chinese",
      "protected" : false,
      "id_str" : "143810986",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/905053498173214721\/3PZLD_5D_normal.jpg",
      "id" : 143810986,
      "verified" : true
    }
  },
  "id" : 576677974759596032,
  "created_at" : "2015-03-14 09:35:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conor Goodman",
      "screen_name" : "conorgoodman",
      "indices" : [ 3, 16 ],
      "id_str" : "211999934",
      "id" : 211999934
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/conorgoodman\/status\/576671506295574528\/photo\/1",
      "indices" : [ 116, 138 ],
      "url" : "http:\/\/t.co\/r3g9M9Hkrq",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CAC_rfnWUAAke4A.jpg",
      "id_str" : "576671506245242880",
      "id" : 576671506245242880,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAC_rfnWUAAke4A.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/r3g9M9Hkrq"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/2z0fdrsNhy",
      "expanded_url" : "http:\/\/www.irishtimes.com\/life-and-style\/people\/emma-ray-25-i-m-a-recession-anomaly-25-self-employed-and-engaged-1.2138657",
      "display_url" : "irishtimes.com\/life-and-style\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576671934219423745",
  "text" : "RT @conorgoodman: At 25, Emma Ray is a recession anomaly: self-employed, engaged, mortgaged. http:\/\/t.co\/2z0fdrsNhy http:\/\/t.co\/r3g9M9Hkrq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/conorgoodman\/status\/576671506295574528\/photo\/1",
        "indices" : [ 98, 120 ],
        "url" : "http:\/\/t.co\/r3g9M9Hkrq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CAC_rfnWUAAke4A.jpg",
        "id_str" : "576671506245242880",
        "id" : 576671506245242880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CAC_rfnWUAAke4A.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/r3g9M9Hkrq"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 97 ],
        "url" : "http:\/\/t.co\/2z0fdrsNhy",
        "expanded_url" : "http:\/\/www.irishtimes.com\/life-and-style\/people\/emma-ray-25-i-m-a-recession-anomaly-25-self-employed-and-engaged-1.2138657",
        "display_url" : "irishtimes.com\/life-and-style\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "576671506295574528",
    "text" : "At 25, Emma Ray is a recession anomaly: self-employed, engaged, mortgaged. http:\/\/t.co\/2z0fdrsNhy http:\/\/t.co\/r3g9M9Hkrq",
    "id" : 576671506295574528,
    "created_at" : "2015-03-14 09:09:35 +0000",
    "user" : {
      "name" : "Conor Goodman",
      "screen_name" : "conorgoodman",
      "protected" : false,
      "id_str" : "211999934",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459077273438916608\/ohPR1tsm_normal.jpeg",
      "id" : 211999934,
      "verified" : true
    }
  },
  "id" : 576671934219423745,
  "created_at" : "2015-03-14 09:11:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alison Austin",
      "screen_name" : "alicenwondrlnd",
      "indices" : [ 3, 18 ],
      "id_str" : "13690622",
      "id" : 13690622
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rwd",
      "indices" : [ 121, 125 ]
    } ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/OvWBdFXK18",
      "expanded_url" : "http:\/\/responsiveconf.com\/",
      "display_url" : "responsiveconf.com"
    } ]
  },
  "geo" : { },
  "id_str" : "576671724877545472",
  "text" : "RT @alicenwondrlnd: Grab 'em while they're hot &gt; Tickets for Responsive Day Out now available: http:\/\/t.co\/OvWBdFXK18 #rwd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rwd",
        "indices" : [ 101, 105 ]
      } ],
      "urls" : [ {
        "indices" : [ 78, 100 ],
        "url" : "http:\/\/t.co\/OvWBdFXK18",
        "expanded_url" : "http:\/\/responsiveconf.com\/",
        "display_url" : "responsiveconf.com"
      } ]
    },
    "geo" : { },
    "id_str" : "576288385410674688",
    "text" : "Grab 'em while they're hot &gt; Tickets for Responsive Day Out now available: http:\/\/t.co\/OvWBdFXK18 #rwd",
    "id" : 576288385410674688,
    "created_at" : "2015-03-13 07:47:12 +0000",
    "user" : {
      "name" : "Alison Austin",
      "screen_name" : "alicenwondrlnd",
      "protected" : false,
      "id_str" : "13690622",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/592018505152765952\/Yn2xByzg_normal.png",
      "id" : 13690622,
      "verified" : false
    }
  },
  "id" : 576671724877545472,
  "created_at" : "2015-03-14 09:10:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Doctor Who Official",
      "screen_name" : "bbcdoctorwho",
      "indices" : [ 3, 16 ],
      "id_str" : "237670274",
      "id" : 237670274
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/bbcdoctorwho\/status\/576654033055834112\/photo\/1",
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/njDUFGKaYD",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CACvyaYW8AAF_jg.jpg",
      "id_str" : "576654032913231872",
      "id" : 576654032913231872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CACvyaYW8AAF_jg.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/njDUFGKaYD"
    } ],
    "hashtags" : [ {
      "text" : "PiDay",
      "indices" : [ 59, 65 ]
    }, {
      "text" : "DoctorWho",
      "indices" : [ 66, 76 ]
    }, {
      "text" : "TARDIS",
      "indices" : [ 77, 84 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576671319779110912",
  "text" : "RT @bbcdoctorwho: The Doctor\u2019s left a pie out in honour of #PiDay #DoctorWho #TARDIS http:\/\/t.co\/njDUFGKaYD",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bbcdoctorwho\/status\/576654033055834112\/photo\/1",
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/njDUFGKaYD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CACvyaYW8AAF_jg.jpg",
        "id_str" : "576654032913231872",
        "id" : 576654032913231872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CACvyaYW8AAF_jg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/njDUFGKaYD"
      } ],
      "hashtags" : [ {
        "text" : "PiDay",
        "indices" : [ 41, 47 ]
      }, {
        "text" : "DoctorWho",
        "indices" : [ 48, 58 ]
      }, {
        "text" : "TARDIS",
        "indices" : [ 59, 66 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "576654033055834112",
    "text" : "The Doctor\u2019s left a pie out in honour of #PiDay #DoctorWho #TARDIS http:\/\/t.co\/njDUFGKaYD",
    "id" : 576654033055834112,
    "created_at" : "2015-03-14 08:00:09 +0000",
    "user" : {
      "name" : "Doctor Who Official",
      "screen_name" : "bbcdoctorwho",
      "protected" : false,
      "id_str" : "237670274",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966039857398153217\/NMS4vIGH_normal.jpg",
      "id" : 237670274,
      "verified" : true
    }
  },
  "id" : 576671319779110912,
  "created_at" : "2015-03-14 09:08:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pivot Dublin",
      "screen_name" : "PivotDublin",
      "indices" : [ 3, 15 ],
      "id_str" : "195004961",
      "id" : 195004961
    }, {
      "name" : "Typography Ireland",
      "screen_name" : "typographyIRL",
      "indices" : [ 86, 100 ],
      "id_str" : "776178216",
      "id" : 776178216
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/hTUgjbgG9P",
      "expanded_url" : "http:\/\/bit.ly\/1L1l2rW",
      "display_url" : "bit.ly\/1L1l2rW"
    } ]
  },
  "geo" : { },
  "id_str" : "576671295934447616",
  "text" : "RT @PivotDublin: Free Typography Ireland Seminar with Annie Atkins &amp; James Keller @typographyIRL http:\/\/t.co\/hTUgjbgG9P http:\/\/t.co\/JBvX945\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Typography Ireland",
        "screen_name" : "typographyIRL",
        "indices" : [ 69, 83 ],
        "id_str" : "776178216",
        "id" : 776178216
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PivotDublin\/status\/575990423153471488\/photo\/1",
        "indices" : [ 107, 129 ],
        "url" : "http:\/\/t.co\/JBvX945M1c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_5UPLKWwAAMar7.jpg",
        "id_str" : "575990422020997120",
        "id" : 575990422020997120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_5UPLKWwAAMar7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 372,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JBvX945M1c"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 106 ],
        "url" : "http:\/\/t.co\/hTUgjbgG9P",
        "expanded_url" : "http:\/\/bit.ly\/1L1l2rW",
        "display_url" : "bit.ly\/1L1l2rW"
      } ]
    },
    "geo" : { },
    "id_str" : "575990423153471488",
    "text" : "Free Typography Ireland Seminar with Annie Atkins &amp; James Keller @typographyIRL http:\/\/t.co\/hTUgjbgG9P http:\/\/t.co\/JBvX945M1c",
    "id" : 575990423153471488,
    "created_at" : "2015-03-12 12:03:12 +0000",
    "user" : {
      "name" : "Pivot Dublin",
      "screen_name" : "PivotDublin",
      "protected" : false,
      "id_str" : "195004961",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/453871624740020224\/aoxNal8i_normal.png",
      "id" : 195004961,
      "verified" : false
    }
  },
  "id" : 576671295934447616,
  "created_at" : "2015-03-14 09:08:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Warren Whitlock",
      "screen_name" : "WarrenWhitlock",
      "indices" : [ 0, 15 ],
      "id_str" : "15081182",
      "id" : 15081182
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "576660605723123712",
  "geo" : { },
  "id_str" : "576667999047938049",
  "in_reply_to_user_id" : 15081182,
  "text" : "@WarrenWhitlock That a dead giveaway. But I suspect there are some who are so gullible that they fall for it.",
  "id" : 576667999047938049,
  "in_reply_to_status_id" : 576660605723123712,
  "created_at" : "2015-03-14 08:55:39 +0000",
  "in_reply_to_screen_name" : "WarrenWhitlock",
  "in_reply_to_user_id_str" : "15081182",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/t8Ve6nOVbV",
      "expanded_url" : "https:\/\/www.flickr.com\/groups\/fiscus_effect\/pool\/",
      "display_url" : "flickr.com\/groups\/fiscus_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576533902640214016",
  "text" : "I am a fan of Jim Fiscus distinct post process technique that I created a Flickr group https:\/\/t.co\/t8Ve6nOVbV",
  "id" : 576533902640214016,
  "created_at" : "2015-03-14 00:02:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 50 ],
      "url" : "http:\/\/t.co\/UbMwK28pzT",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/environment\/dublin-city-council-seeks-views-of-public-on-dealing-with-litter-problems-1.2137827",
      "display_url" : "irishtimes.com\/news\/environme\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576532758698336257",
  "text" : "It a matter of enforcement. http:\/\/t.co\/UbMwK28pzT",
  "id" : 576532758698336257,
  "created_at" : "2015-03-13 23:58:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 119 ],
      "url" : "http:\/\/t.co\/H9hFCjMeTP",
      "expanded_url" : "http:\/\/stocklandmartelblog.com\/2015\/03\/13\/jim-fiscus-penny-dreadful-video\/",
      "display_url" : "stocklandmartelblog.com\/2015\/03\/13\/jim\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576532424215134208",
  "text" : "Jim shooting on set in Dublin. Is he bringing his distinct post process technique to the screen? http:\/\/t.co\/H9hFCjMeTP",
  "id" : 576532424215134208,
  "created_at" : "2015-03-13 23:56:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Neil Patel",
      "screen_name" : "neilpatel",
      "indices" : [ 0, 10 ],
      "id_str" : "1322691",
      "id" : 1322691
    }, {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "indices" : [ 25, 37 ],
      "id_str" : "10504042",
      "id" : 10504042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/obSzoqSMv5",
      "expanded_url" : "http:\/\/www.slideshare.net\/cxpartners\/chinese-web-design-patterns-how-and-why-theyre-different",
      "display_url" : "slideshare.net\/cxpartners\/chi\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "576520883470209024",
  "geo" : { },
  "id_str" : "576527916676325376",
  "in_reply_to_user_id" : 1322691,
  "text" : "@neilpatel checkout also @ChuiSquared on Web Design in China http:\/\/t.co\/obSzoqSMv5",
  "id" : 576527916676325376,
  "in_reply_to_status_id" : 576520883470209024,
  "created_at" : "2015-03-13 23:39:01 +0000",
  "in_reply_to_screen_name" : "neilpatel",
  "in_reply_to_user_id_str" : "1322691",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 101, 110 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/HOeWec0WPk",
      "expanded_url" : "https:\/\/wordpress.org\/plugins\/recipe-card\/",
      "display_url" : "wordpress.org\/plugins\/recipe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576518488405221376",
  "text" : "Recipe Card plugin that adds a recipe template to your WordPress website. https:\/\/t.co\/HOeWec0WPk cc @enormous",
  "id" : 576518488405221376,
  "created_at" : "2015-03-13 23:01:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576430816806457344",
  "text" : "On TV. What do you think of her? \"She has a nice set of teeth and hair\" You would have thought there is cattle trading involve.",
  "id" : 576430816806457344,
  "created_at" : "2015-03-13 17:13:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/oqWGyfJER5",
      "expanded_url" : "http:\/\/www.paulallen.com\/Interests\/Musashi",
      "display_url" : "paulallen.com\/Interests\/Musa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576430173068898304",
  "text" : "Microsoft co-founder found the remains of Japanese battleship sunken at the bottom of sea in the Philippines. http:\/\/t.co\/oqWGyfJER5",
  "id" : 576430173068898304,
  "created_at" : "2015-03-13 17:10:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ethan Zuckerman",
      "screen_name" : "EthanZ",
      "indices" : [ 3, 10 ],
      "id_str" : "1051171",
      "id" : 1051171
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/8bOd7MnZfe",
      "expanded_url" : "http:\/\/globalvoicesonline.org\/2015\/03\/12\/japan-disposable-workers-internet-cafe-refugees-documentary\/",
      "display_url" : "globalvoicesonline.org\/2015\/03\/12\/jap\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576397160016658433",
  "text" : "RT @EthanZ: A step above homelessness in Japan: living in internet caf\u00E9s. Fascinating documentary: http:\/\/t.co\/8bOd7MnZfe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 109 ],
        "url" : "http:\/\/t.co\/8bOd7MnZfe",
        "expanded_url" : "http:\/\/globalvoicesonline.org\/2015\/03\/12\/japan-disposable-workers-internet-cafe-refugees-documentary\/",
        "display_url" : "globalvoicesonline.org\/2015\/03\/12\/jap\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "576394283189723136",
    "text" : "A step above homelessness in Japan: living in internet caf\u00E9s. Fascinating documentary: http:\/\/t.co\/8bOd7MnZfe",
    "id" : 576394283189723136,
    "created_at" : "2015-03-13 14:48:00 +0000",
    "user" : {
      "name" : "Ethan Zuckerman",
      "screen_name" : "EthanZ",
      "protected" : false,
      "id_str" : "1051171",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659351758704136193\/5jUqQnv0_normal.jpg",
      "id" : 1051171,
      "verified" : false
    }
  },
  "id" : 576397160016658433,
  "created_at" : "2015-03-13 14:59:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "30pc",
      "indices" : [ 29, 34 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http:\/\/t.co\/I8G6o4US78",
      "expanded_url" : "http:\/\/ow.ly\/KheGw",
      "display_url" : "ow.ly\/KheGw"
    } ]
  },
  "geo" : { },
  "id_str" : "576397069524553728",
  "text" : "RT @DigiWomenIRL: Please use #30pc if you are sick of mostly male speaker panels at conferences. C\u2019mon everyone http:\/\/t.co\/I8G6o4US78",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "30pc",
        "indices" : [ 11, 16 ]
      } ],
      "urls" : [ {
        "indices" : [ 94, 116 ],
        "url" : "http:\/\/t.co\/I8G6o4US78",
        "expanded_url" : "http:\/\/ow.ly\/KheGw",
        "display_url" : "ow.ly\/KheGw"
      } ]
    },
    "geo" : { },
    "id_str" : "576396075944288257",
    "text" : "Please use #30pc if you are sick of mostly male speaker panels at conferences. C\u2019mon everyone http:\/\/t.co\/I8G6o4US78",
    "id" : 576396075944288257,
    "created_at" : "2015-03-13 14:55:08 +0000",
    "user" : {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "protected" : false,
      "id_str" : "1221157964",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527003323623145473\/C2yyI2ob_normal.png",
      "id" : 1221157964,
      "verified" : false
    }
  },
  "id" : 576397069524553728,
  "created_at" : "2015-03-13 14:59:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576387943566282752",
  "text" : "According to Google, You have just over a month to get your site mobile friendly or it will start effecting your search engine ranking.",
  "id" : 576387943566282752,
  "created_at" : "2015-03-13 14:22:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 59 ],
      "url" : "http:\/\/t.co\/KlA89gb9Ba",
      "expanded_url" : "http:\/\/www.urbandictionary.com\/define.php?term=Renua",
      "display_url" : "urbandictionary.com\/define.php?ter\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576387720777453568",
  "text" : "Renua according to Urban Dictionary. http:\/\/t.co\/KlA89gb9Ba",
  "id" : 576387720777453568,
  "created_at" : "2015-03-13 14:21:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 0, 9 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "576378902211330048",
  "geo" : { },
  "id_str" : "576381620556603392",
  "in_reply_to_user_id" : 10410242,
  "text" : "@enormous Brilliant day today.",
  "id" : 576381620556603392,
  "in_reply_to_status_id" : 576378902211330048,
  "created_at" : "2015-03-13 13:57:41 +0000",
  "in_reply_to_screen_name" : "enormous",
  "in_reply_to_user_id_str" : "10410242",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576379809200226304",
  "text" : "Political party seek to reboot the country what about people mindset? That need a reboot too.",
  "id" : 576379809200226304,
  "created_at" : "2015-03-13 13:50:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "576372498624856064",
  "geo" : { },
  "id_str" : "576379281514864641",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest it helps that the apartment is properly insulated. :)",
  "id" : 576379281514864641,
  "in_reply_to_status_id" : 576372498624856064,
  "created_at" : "2015-03-13 13:48:23 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576348056578338816",
  "text" : "To wean people off social welfare by locking them up? Wow. Food, Accommodation, healthcare and security taken care of I suppose.",
  "id" : 576348056578338816,
  "created_at" : "2015-03-13 11:44:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576346294073073664",
  "text" : "Survived the winter without using the heater! Woohoo!",
  "id" : 576346294073073664,
  "created_at" : "2015-03-13 11:37:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zafar Anjum",
      "screen_name" : "zafaranjum",
      "indices" : [ 3, 14 ],
      "id_str" : "28495071",
      "id" : 28495071
    }, {
      "name" : "IDA - Deactivated",
      "screen_name" : "ida_singapore",
      "indices" : [ 76, 90 ],
      "id_str" : "3145679317",
      "id" : 3145679317
    }, {
      "name" : "Computerworld Sg",
      "screen_name" : "ComputerworldSg",
      "indices" : [ 91, 107 ],
      "id_str" : "263539392",
      "id" : 263539392
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sg",
      "indices" : [ 72, 75 ]
    }, {
      "text" : "SXSW",
      "indices" : [ 108, 113 ]
    } ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/To5sM9ZVfw",
      "expanded_url" : "http:\/\/www.computerworld.com.sg\/resource\/industries\/13-singapore-tech-startups-to-be-showcased-at-sxsw-2015\/",
      "display_url" : "computerworld.com.sg\/resource\/indus\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576345792899858432",
  "text" : "RT @zafaranjum: 13 Singapore tech startups to be showcased at SXSW 2015 #sg @IDA_SINGAPORE @ComputerworldSg #SXSW http:\/\/t.co\/To5sM9ZVfw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "IDA - Deactivated",
        "screen_name" : "ida_singapore",
        "indices" : [ 60, 74 ],
        "id_str" : "3145679317",
        "id" : 3145679317
      }, {
        "name" : "Computerworld Sg",
        "screen_name" : "ComputerworldSg",
        "indices" : [ 75, 91 ],
        "id_str" : "263539392",
        "id" : 263539392
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "sg",
        "indices" : [ 56, 59 ]
      }, {
        "text" : "SXSW",
        "indices" : [ 92, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 98, 120 ],
        "url" : "http:\/\/t.co\/To5sM9ZVfw",
        "expanded_url" : "http:\/\/www.computerworld.com.sg\/resource\/industries\/13-singapore-tech-startups-to-be-showcased-at-sxsw-2015\/",
        "display_url" : "computerworld.com.sg\/resource\/indus\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "576331260773056512",
    "text" : "13 Singapore tech startups to be showcased at SXSW 2015 #sg @IDA_SINGAPORE @ComputerworldSg #SXSW http:\/\/t.co\/To5sM9ZVfw",
    "id" : 576331260773056512,
    "created_at" : "2015-03-13 10:37:34 +0000",
    "user" : {
      "name" : "Zafar Anjum",
      "screen_name" : "zafaranjum",
      "protected" : false,
      "id_str" : "28495071",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1019786762271350784\/8oevKsgr_normal.jpg",
      "id" : 28495071,
      "verified" : false
    }
  },
  "id" : 576345792899858432,
  "created_at" : "2015-03-13 11:35:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    }, {
      "name" : "SingaporePoliceForce",
      "screen_name" : "SingaporePolice",
      "indices" : [ 30, 46 ],
      "id_str" : "38140836",
      "id" : 38140836
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/576334042989096960\/photo\/1",
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/DgSVbVjDRJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_-MvUqU8AAdXAo.jpg",
      "id_str" : "576334021954695168",
      "id" : 576334021954695168,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_-MvUqU8AAdXAo.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 427,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 427,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 427,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 427,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DgSVbVjDRJ"
    } ],
    "hashtags" : [ {
      "text" : "CardboardConstable",
      "indices" : [ 55, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/OSWVqLRLZT",
      "expanded_url" : "http:\/\/cna.asia\/1L63Dyk",
      "display_url" : "cna.asia\/1L63Dyk"
    } ]
  },
  "geo" : { },
  "id_str" : "576336107375312896",
  "text" : "RT @ChannelNewsAsia: Meet the @SingaporePolice Force's #CardboardConstable in real life http:\/\/t.co\/OSWVqLRLZT http:\/\/t.co\/DgSVbVjDRJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SingaporePoliceForce",
        "screen_name" : "SingaporePolice",
        "indices" : [ 9, 25 ],
        "id_str" : "38140836",
        "id" : 38140836
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/576334042989096960\/photo\/1",
        "indices" : [ 90, 112 ],
        "url" : "http:\/\/t.co\/DgSVbVjDRJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_-MvUqU8AAdXAo.jpg",
        "id_str" : "576334021954695168",
        "id" : 576334021954695168,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_-MvUqU8AAdXAo.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 427,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 427,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 427,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 427,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DgSVbVjDRJ"
      } ],
      "hashtags" : [ {
        "text" : "CardboardConstable",
        "indices" : [ 34, 53 ]
      } ],
      "urls" : [ {
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/OSWVqLRLZT",
        "expanded_url" : "http:\/\/cna.asia\/1L63Dyk",
        "display_url" : "cna.asia\/1L63Dyk"
      } ]
    },
    "geo" : { },
    "id_str" : "576334042989096960",
    "text" : "Meet the @SingaporePolice Force's #CardboardConstable in real life http:\/\/t.co\/OSWVqLRLZT http:\/\/t.co\/DgSVbVjDRJ",
    "id" : 576334042989096960,
    "created_at" : "2015-03-13 10:48:38 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 576336107375312896,
  "created_at" : "2015-03-13 10:56:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "576325536890400768",
  "geo" : { },
  "id_str" : "576329164657164288",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose all recorded for training purposes and review. \"Customers start singing along when they have been on hold for so long\"",
  "id" : 576329164657164288,
  "in_reply_to_status_id" : 576325536890400768,
  "created_at" : "2015-03-13 10:29:15 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jake Vig",
      "screen_name" : "Jake_Vig",
      "indices" : [ 3, 12 ],
      "id_str" : "181532763",
      "id" : 181532763
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576164188948660224",
  "text" : "RT @Jake_Vig: I wish the government would go through my e-mails, because I'm never going to.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "575310373508812801",
    "text" : "I wish the government would go through my e-mails, because I'm never going to.",
    "id" : 575310373508812801,
    "created_at" : "2015-03-10 15:00:56 +0000",
    "user" : {
      "name" : "Jake Vig",
      "screen_name" : "Jake_Vig",
      "protected" : false,
      "id_str" : "181532763",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009432334955941889\/neLs7-tq_normal.jpg",
      "id" : 181532763,
      "verified" : false
    }
  },
  "id" : 576164188948660224,
  "created_at" : "2015-03-12 23:33:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dublinese \u6708\u5149",
      "screen_name" : "rachel_violin",
      "indices" : [ 0, 14 ],
      "id_str" : "116755889",
      "id" : 116755889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 118 ],
      "url" : "http:\/\/t.co\/FXfqHmDVb7",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/Lor_mee",
      "display_url" : "en.wikipedia.org\/wiki\/Lor_mee"
    } ]
  },
  "in_reply_to_status_id_str" : "576121558877499392",
  "geo" : { },
  "id_str" : "576149190537056256",
  "in_reply_to_user_id" : 116755889,
  "text" : "@rachel_violin It braised mushroom with black soy sauce together with mock meat. Our take on \u9E75\u9EB5 http:\/\/t.co\/FXfqHmDVb7",
  "id" : 576149190537056256,
  "in_reply_to_status_id" : 576121558877499392,
  "created_at" : "2015-03-12 22:34:06 +0000",
  "in_reply_to_screen_name" : "rachel_violin",
  "in_reply_to_user_id_str" : "116755889",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http:\/\/t.co\/h4DZvgIbVg",
      "expanded_url" : "http:\/\/www.writersua.com\/articles\/quickref\/index.html",
      "display_url" : "writersua.com\/articles\/quick\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "576148128363126784",
  "text" : "How to write short and sweet documentation http:\/\/t.co\/h4DZvgIbVg",
  "id" : 576148128363126784,
  "created_at" : "2015-03-12 22:29:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/576092110279020544\/photo\/1",
      "indices" : [ 34, 56 ],
      "url" : "http:\/\/t.co\/afU4CqY166",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_6wtjeWUAAHhjS.jpg",
      "id_str" : "576092099013136384",
      "id" : 576092099013136384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_6wtjeWUAAHhjS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/afU4CqY166"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576092110279020544",
  "text" : "Breakfast 8 hours ago. Happy man. http:\/\/t.co\/afU4CqY166",
  "id" : 576092110279020544,
  "created_at" : "2015-03-12 18:47:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter O'Neill",
      "screen_name" : "peter_oneill",
      "indices" : [ 0, 13 ],
      "id_str" : "16502577",
      "id" : 16502577
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "576087922060992513",
  "geo" : { },
  "id_str" : "576090227581849600",
  "in_reply_to_user_id" : 16502577,
  "text" : "@peter_oneill @bharatidalela I not coming this Saturday due to other commitment.",
  "id" : 576090227581849600,
  "in_reply_to_status_id" : 576087922060992513,
  "created_at" : "2015-03-12 18:39:48 +0000",
  "in_reply_to_screen_name" : "peter_oneill",
  "in_reply_to_user_id_str" : "16502577",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/576078382284300288\/photo\/1",
      "indices" : [ 28, 50 ],
      "url" : "http:\/\/t.co\/Ad5oQZ9PAN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_6kPFKWcAAOGH9.png",
      "id_str" : "576078381340585984",
      "id" : 576078381340585984,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_6kPFKWcAAOGH9.png",
      "sizes" : [ {
        "h" : 468,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 489,
        "resize" : "fit",
        "w" : 711
      }, {
        "h" : 489,
        "resize" : "fit",
        "w" : 711
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 489,
        "resize" : "fit",
        "w" : 711
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Ad5oQZ9PAN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576078382284300288",
  "text" : "&lt;cough&gt;&lt;\/cough&gt; http:\/\/t.co\/Ad5oQZ9PAN",
  "id" : 576078382284300288,
  "created_at" : "2015-03-12 17:52:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter O'Neill",
      "screen_name" : "peter_oneill",
      "indices" : [ 15, 28 ],
      "id_str" : "16502577",
      "id" : 16502577
    }, {
      "name" : "yeh-hoh-SHU-ah\uD83E\uDD47",
      "screen_name" : "AnalyticsNinja",
      "indices" : [ 29, 44 ],
      "id_str" : "41089626",
      "id" : 41089626
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576073025570877440",
  "text" : "@bharatidalela @peter_oneill @AnalyticsNinja Thanks I will find out more.",
  "id" : 576073025570877440,
  "created_at" : "2015-03-12 17:31:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "FORTUNE",
      "screen_name" : "FortuneMagazine",
      "indices" : [ 3, 19 ],
      "id_str" : "25053299",
      "id" : 25053299
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/FortuneMagazine\/status\/576048254388944896\/photo\/1",
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/hjyfQlJo9c",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_6I1dTWEAAf7dL.jpg",
      "id_str" : "576048254330212352",
      "id" : 576048254330212352,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_6I1dTWEAAf7dL.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 485,
        "resize" : "fit",
        "w" : 840
      }, {
        "h" : 485,
        "resize" : "fit",
        "w" : 840
      }, {
        "h" : 485,
        "resize" : "fit",
        "w" : 840
      }, {
        "h" : 393,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hjyfQlJo9c"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 91 ],
      "url" : "http:\/\/t.co\/04HkT7U2AH",
      "expanded_url" : "http:\/\/for.tn\/1MvFVsG",
      "display_url" : "for.tn\/1MvFVsG"
    } ]
  },
  "geo" : { },
  "id_str" : "576048837493686272",
  "text" : "RT @FortuneMagazine: Why Xiaomi terrifies the rest of the tech world http:\/\/t.co\/04HkT7U2AH http:\/\/t.co\/hjyfQlJo9c",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FortuneMagazine\/status\/576048254388944896\/photo\/1",
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/hjyfQlJo9c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_6I1dTWEAAf7dL.jpg",
        "id_str" : "576048254330212352",
        "id" : 576048254330212352,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_6I1dTWEAAf7dL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 840
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 840
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 840
        }, {
          "h" : 393,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hjyfQlJo9c"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 70 ],
        "url" : "http:\/\/t.co\/04HkT7U2AH",
        "expanded_url" : "http:\/\/for.tn\/1MvFVsG",
        "display_url" : "for.tn\/1MvFVsG"
      } ]
    },
    "geo" : { },
    "id_str" : "576048254388944896",
    "text" : "Why Xiaomi terrifies the rest of the tech world http:\/\/t.co\/04HkT7U2AH http:\/\/t.co\/hjyfQlJo9c",
    "id" : 576048254388944896,
    "created_at" : "2015-03-12 15:53:00 +0000",
    "user" : {
      "name" : "FORTUNE",
      "screen_name" : "FortuneMagazine",
      "protected" : false,
      "id_str" : "25053299",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875382047216467972\/3119VjuE_normal.jpg",
      "id" : 25053299,
      "verified" : true
    }
  },
  "id" : 576048837493686272,
  "created_at" : "2015-03-12 15:55:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 3, 12 ],
      "id_str" : "1291131",
      "id" : 1291131
    }, {
      "name" : "The Business Post",
      "screen_name" : "sundaybusiness",
      "indices" : [ 45, 60 ],
      "id_str" : "17208934",
      "id" : 17208934
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http:\/\/t.co\/ml1lGqZnlf",
      "expanded_url" : "http:\/\/www.brightspark-consulting.com\/30\/",
      "display_url" : "brightspark-consulting.com\/30\/"
    } ]
  },
  "geo" : { },
  "id_str" : "576029892258754561",
  "text" : "RT @maryrose: We call on event organisers of @sundaybusiness to increase the number of female speakers. #30% http:\/\/t.co\/ml1lGqZnlf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Business Post",
        "screen_name" : "sundaybusiness",
        "indices" : [ 31, 46 ],
        "id_str" : "17208934",
        "id" : 17208934
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 117 ],
        "url" : "http:\/\/t.co\/ml1lGqZnlf",
        "expanded_url" : "http:\/\/www.brightspark-consulting.com\/30\/",
        "display_url" : "brightspark-consulting.com\/30\/"
      } ]
    },
    "geo" : { },
    "id_str" : "576029606358188032",
    "text" : "We call on event organisers of @sundaybusiness to increase the number of female speakers. #30% http:\/\/t.co\/ml1lGqZnlf",
    "id" : 576029606358188032,
    "created_at" : "2015-03-12 14:38:54 +0000",
    "user" : {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "protected" : false,
      "id_str" : "1291131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034068926370594816\/eS9-sUNe_normal.jpg",
      "id" : 1291131,
      "verified" : false
    }
  },
  "id" : 576029892258754561,
  "created_at" : "2015-03-12 14:40:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "576029300400504832",
  "text" : "Found that some website owner like to use these words \"Just a quick job\" (in the hope of getting a low price?)",
  "id" : 576029300400504832,
  "created_at" : "2015-03-12 14:37:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC Press Office",
      "screen_name" : "bbcpress",
      "indices" : [ 3, 12 ],
      "id_str" : "34918353",
      "id" : 34918353
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BBCMakeitDigital",
      "indices" : [ 14, 31 ]
    }, {
      "text" : "BBCMicro",
      "indices" : [ 87, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http:\/\/t.co\/MixlfabOo3",
      "expanded_url" : "http:\/\/bbc.in\/1Fe30Oo",
      "display_url" : "bbc.in\/1Fe30Oo"
    } ]
  },
  "geo" : { },
  "id_str" : "575978864398200832",
  "text" : "RT @bbcpress: #BBCMakeitDigital will give 1m coding devices to every yr 7 child in UK. #BBCMicro for today http:\/\/t.co\/MixlfabOo3 http:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bbcpress\/status\/575969411884023808\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/fH9mmVdN4r",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_5BIN3WAAApS9l.jpg",
        "id_str" : "575969411766550528",
        "id" : 575969411766550528,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_5BIN3WAAApS9l.jpg",
        "sizes" : [ {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fH9mmVdN4r"
      } ],
      "hashtags" : [ {
        "text" : "BBCMakeitDigital",
        "indices" : [ 0, 17 ]
      }, {
        "text" : "BBCMicro",
        "indices" : [ 73, 82 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/MixlfabOo3",
        "expanded_url" : "http:\/\/bbc.in\/1Fe30Oo",
        "display_url" : "bbc.in\/1Fe30Oo"
      } ]
    },
    "geo" : { },
    "id_str" : "575969411884023808",
    "text" : "#BBCMakeitDigital will give 1m coding devices to every yr 7 child in UK. #BBCMicro for today http:\/\/t.co\/MixlfabOo3 http:\/\/t.co\/fH9mmVdN4r",
    "id" : 575969411884023808,
    "created_at" : "2015-03-12 10:39:43 +0000",
    "user" : {
      "name" : "BBC Press Office",
      "screen_name" : "bbcpress",
      "protected" : false,
      "id_str" : "34918353",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/883285902247723010\/QuzjYov5_normal.jpg",
      "id" : 34918353,
      "verified" : true
    }
  },
  "id" : 575978864398200832,
  "created_at" : "2015-03-12 11:17:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 28, 40 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575822343253147648",
  "text" : "Is the hashtag the new URL? #getoptimise",
  "id" : 575822343253147648,
  "created_at" : "2015-03-12 00:55:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 7, 17 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/cKmsDqVwvL",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/575298181732392961",
      "display_url" : "twitter.com\/mryap\/status\/5\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "575820586045341697",
  "text" : "Thanks @starduest for retweeting this. https:\/\/t.co\/cKmsDqVwvL",
  "id" : 575820586045341697,
  "created_at" : "2015-03-12 00:48:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "834055856",
      "id" : 834055856
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Expat",
      "indices" : [ 29, 35 ]
    } ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http:\/\/t.co\/whKfpMXogd",
      "expanded_url" : "http:\/\/tinyurl.com\/nlr6q96",
      "display_url" : "tinyurl.com\/nlr6q96"
    } ]
  },
  "geo" : { },
  "id_str" : "575820473671507968",
  "text" : "RT @expatsblog: NEW Featured #Expat - Singaporean Expat Living in Northern Ireland - Interview with Joanne http:\/\/t.co\/whKfpMXogd  Thanks @\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.expatsblog.com\" rel=\"nofollow\"\u003EExpatsBlog.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "check bio",
        "screen_name" : "starduest",
        "indices" : [ 122, 132 ],
        "id_str" : "824059172731830272",
        "id" : 824059172731830272
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Expat",
        "indices" : [ 13, 19 ]
      }, {
        "text" : "expats",
        "indices" : [ 133, 140 ]
      } ],
      "urls" : [ {
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/whKfpMXogd",
        "expanded_url" : "http:\/\/tinyurl.com\/nlr6q96",
        "display_url" : "tinyurl.com\/nlr6q96"
      } ]
    },
    "geo" : { },
    "id_str" : "575583162664906752",
    "text" : "NEW Featured #Expat - Singaporean Expat Living in Northern Ireland - Interview with Joanne http:\/\/t.co\/whKfpMXogd  Thanks @starduest #expats",
    "id" : 575583162664906752,
    "created_at" : "2015-03-11 09:04:54 +0000",
    "user" : {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "protected" : false,
      "id_str" : "834055856",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2627803659\/logo_normal.png",
      "id" : 834055856,
      "verified" : false
    }
  },
  "id" : 575820473671507968,
  "created_at" : "2015-03-12 00:47:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/flickr.com\/services\/twitter\/\" rel=\"nofollow\"\u003EFlickr\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/d4c9taFVTh",
      "expanded_url" : "https:\/\/flic.kr\/p\/qCQqpk",
      "display_url" : "flic.kr\/p\/qCQqpk"
    } ]
  },
  "geo" : { },
  "id_str" : "575814260988706816",
  "text" : "Singapore Suntec City, known as Asia's vertical Silicon Valley https:\/\/t.co\/d4c9taFVTh",
  "id" : 575814260988706816,
  "created_at" : "2015-03-12 00:23:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Womersley",
      "screen_name" : "LisaWomersley",
      "indices" : [ 3, 17 ],
      "id_str" : "789148770869080064",
      "id" : 789148770869080064
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/NHriNUebeZ",
      "expanded_url" : "http:\/\/watson-um-demo.mybluemix.net\/",
      "display_url" : "watson-um-demo.mybluemix.net"
    } ]
  },
  "geo" : { },
  "id_str" : "575717535145984000",
  "text" : "RT @lisawomersley: Watson can analyse your personality traits based on a 100 word sample.  http:\/\/t.co\/NHriNUebeZ (mine was scarily accurat\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 94 ],
        "url" : "http:\/\/t.co\/NHriNUebeZ",
        "expanded_url" : "http:\/\/watson-um-demo.mybluemix.net\/",
        "display_url" : "watson-um-demo.mybluemix.net"
      } ]
    },
    "geo" : { },
    "id_str" : "575472551511486464",
    "text" : "Watson can analyse your personality traits based on a 100 word sample.  http:\/\/t.co\/NHriNUebeZ (mine was scarily accurate)",
    "id" : 575472551511486464,
    "created_at" : "2015-03-11 01:45:22 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 575717535145984000,
  "created_at" : "2015-03-11 17:58:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa O Brien",
      "screen_name" : "liobrien",
      "indices" : [ 3, 12 ],
      "id_str" : "46666884",
      "id" : 46666884
    }, {
      "name" : "Edelman Digital",
      "screen_name" : "EdelmanDigital",
      "indices" : [ 27, 42 ],
      "id_str" : "14704992",
      "id" : 14704992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575630401194061824",
  "text" : "RT @liobrien: Love Brian's @EdelmanDigital analogy - treat social like going to a party. When conversation dies down know a game you can pl\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Edelman Digital",
        "screen_name" : "EdelmanDigital",
        "indices" : [ 13, 28 ],
        "id_str" : "14704992",
        "id" : 14704992
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DMXDublin",
        "indices" : [ 129, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "575628011057385473",
    "text" : "Love Brian's @EdelmanDigital analogy - treat social like going to a party. When conversation dies down know a game you can play. #DMXDublin",
    "id" : 575628011057385473,
    "created_at" : "2015-03-11 12:03:07 +0000",
    "user" : {
      "name" : "Lisa O Brien",
      "screen_name" : "liobrien",
      "protected" : false,
      "id_str" : "46666884",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1036041868423966721\/XjqTFCzr_normal.jpg",
      "id" : 46666884,
      "verified" : false
    }
  },
  "id" : 575630401194061824,
  "created_at" : "2015-03-11 12:12:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/gjn0d9MY1C",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/cops-investigated-sleeping-job-070719438.html",
      "display_url" : "sg.news.yahoo.com\/cops-investiga\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "575619542862446592",
  "text" : "maybe one should keep a look out while other take a cat nap https:\/\/t.co\/gjn0d9MY1C Just Saying.",
  "id" : 575619542862446592,
  "created_at" : "2015-03-11 11:29:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DMX Dublin",
      "screen_name" : "DMXDublin",
      "indices" : [ 3, 13 ],
      "id_str" : "957894468",
      "id" : 957894468
    }, {
      "name" : "Dr Dave Chaffey",
      "screen_name" : "DaveChaffey",
      "indices" : [ 16, 28 ],
      "id_str" : "16134616",
      "id" : 16134616
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575611186680590336",
  "text" : "RT @DMXDublin: .@DaveChaffey asks how many of you have integrated your digital into your overall marketing strategy. Not too many hands go \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dr Dave Chaffey",
        "screen_name" : "DaveChaffey",
        "indices" : [ 1, 13 ],
        "id_str" : "16134616",
        "id" : 16134616
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DMXDublin",
        "indices" : [ 127, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "575607674739552256",
    "text" : ".@DaveChaffey asks how many of you have integrated your digital into your overall marketing strategy. Not too many hands go up #DMXDublin",
    "id" : 575607674739552256,
    "created_at" : "2015-03-11 10:42:18 +0000",
    "user" : {
      "name" : "DMX Dublin",
      "screen_name" : "DMXDublin",
      "protected" : false,
      "id_str" : "957894468",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948496226395226112\/0YMbnAiz_normal.jpg",
      "id" : 957894468,
      "verified" : false
    }
  },
  "id" : 575611186680590336,
  "created_at" : "2015-03-11 10:56:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "indices" : [ 0, 13 ],
      "id_str" : "854642300",
      "id" : 854642300
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "575304490359873536",
  "geo" : { },
  "id_str" : "575305604660617216",
  "in_reply_to_user_id" : 854642300,
  "text" : "@3thoughtbbls You imagine trying to update this type of site on a CMS!",
  "id" : 575305604660617216,
  "in_reply_to_status_id" : 575304490359873536,
  "created_at" : "2015-03-10 14:41:59 +0000",
  "in_reply_to_screen_name" : "3thoughtbbls",
  "in_reply_to_user_id_str" : "854642300",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon te Brinke",
      "screen_name" : "gramercypark",
      "indices" : [ 3, 16 ],
      "id_str" : "15915031",
      "id" : 15915031
    }, {
      "name" : "Brian Solis",
      "screen_name" : "briansolis",
      "indices" : [ 114, 125 ],
      "id_str" : "11489",
      "id" : 11489
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SocialMedia",
      "indices" : [ 19, 31 ]
    }, {
      "text" : "SoMe",
      "indices" : [ 126, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575304028613124096",
  "text" : "RT @gramercypark: '#SocialMedia is less about technology and more about anthropology, sociology and ethnography.\u2019 @briansolis #SoMe http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Brian Solis",
        "screen_name" : "briansolis",
        "indices" : [ 96, 107 ],
        "id_str" : "11489",
        "id" : 11489
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gramercypark\/status\/572281128599490560\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/fyM4sR3oGL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_EmpyyUsAA5RUw.jpg",
        "id_str" : "572281127102099456",
        "id" : 572281127102099456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_EmpyyUsAA5RUw.jpg",
        "sizes" : [ {
          "h" : 370,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 370,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fyM4sR3oGL"
      } ],
      "hashtags" : [ {
        "text" : "SocialMedia",
        "indices" : [ 1, 13 ]
      }, {
        "text" : "SoMe",
        "indices" : [ 108, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572281128599490560",
    "text" : "'#SocialMedia is less about technology and more about anthropology, sociology and ethnography.\u2019 @briansolis #SoMe http:\/\/t.co\/fyM4sR3oGL",
    "id" : 572281128599490560,
    "created_at" : "2015-03-02 06:23:48 +0000",
    "user" : {
      "name" : "Simon te Brinke",
      "screen_name" : "gramercypark",
      "protected" : false,
      "id_str" : "15915031",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2341993131\/sjm1aiqeqcf422brydv9_normal.jpeg",
      "id" : 15915031,
      "verified" : false
    }
  },
  "id" : 575304028613124096,
  "created_at" : "2015-03-10 14:35:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 110, 122 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/Jl2s8Q5nKc",
      "expanded_url" : "http:\/\/www.getoptimise.com\/how-to-track-single-page-website-with-google-analytics\/",
      "display_url" : "getoptimise.com\/how-to-track-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "575298181732392961",
  "text" : "I give up basking in the sunshine to blog this. Please read and comment it. Thank You. http:\/\/t.co\/Jl2s8Q5nKc #getoptimise",
  "id" : 575298181732392961,
  "created_at" : "2015-03-10 14:12:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575296484490493954",
  "text" : "Good weather is a much sought-after event here. Thing I took for granted back home.",
  "id" : 575296484490493954,
  "created_at" : "2015-03-10 14:05:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575289537783545857",
  "text" : "2. Hopefully leads to fame &amp; fortune.",
  "id" : 575289537783545857,
  "created_at" : "2015-03-10 13:38:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575289420330459136",
  "text" : "1. Lovely weather outside in Dublin. Staying in to write a blogpost so my website smell nice for Google Search engine bot to pick up.",
  "id" : 575289420330459136,
  "created_at" : "2015-03-10 13:37:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ann Tran",
      "screen_name" : "AnnTran_",
      "indices" : [ 3, 12 ],
      "id_str" : "23135992",
      "id" : 23135992
    }, {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "indices" : [ 109, 113 ],
      "id_str" : "5120691",
      "id" : 5120691
    }, {
      "name" : "Steve Akins",
      "screen_name" : "SteveAkinsSEO",
      "indices" : [ 114, 128 ],
      "id_str" : "22539801",
      "id" : 22539801
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/574056918114787328\/photo\/1",
      "indices" : [ 82, 104 ],
      "url" : "http:\/\/t.co\/ys3fCiiyFc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_d1ubnWQAAlQW6.jpg",
      "id_str" : "574056918060253184",
      "id" : 574056918060253184,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_d1ubnWQAAlQW6.jpg",
      "sizes" : [ {
        "h" : 330,
        "resize" : "fit",
        "w" : 628
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 330,
        "resize" : "fit",
        "w" : 628
      }, {
        "h" : 330,
        "resize" : "fit",
        "w" : 628
      }, {
        "h" : 330,
        "resize" : "fit",
        "w" : 628
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ys3fCiiyFc"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http:\/\/t.co\/9bpJsOMqb6",
      "expanded_url" : "http:\/\/wef.ch\/1GsEpUW",
      "display_url" : "wef.ch\/1GsEpUW"
    } ]
  },
  "geo" : { },
  "id_str" : "575214404574253057",
  "text" : "RT @AnnTran_: What is the cost of living around the world? http:\/\/t.co\/9bpJsOMqb6 http:\/\/t.co\/ys3fCiiyFc via @wef @SteveAkinsSEO http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "World Economic Forum",
        "screen_name" : "wef",
        "indices" : [ 95, 99 ],
        "id_str" : "5120691",
        "id" : 5120691
      }, {
        "name" : "Steve Akins",
        "screen_name" : "SteveAkinsSEO",
        "indices" : [ 100, 114 ],
        "id_str" : "22539801",
        "id" : 22539801
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AnnTran_\/status\/574191971591397376\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/XEgqHZ1GKA",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_fwjX7WQAAZ58e.jpg",
        "id_str" : "574191968022052864",
        "id" : 574191968022052864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_fwjX7WQAAZ58e.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 418
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 630
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 630
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 630
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XEgqHZ1GKA"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/574056918114787328\/photo\/1",
        "indices" : [ 68, 90 ],
        "url" : "http:\/\/t.co\/ys3fCiiyFc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_d1ubnWQAAlQW6.jpg",
        "id_str" : "574056918060253184",
        "id" : 574056918060253184,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_d1ubnWQAAlQW6.jpg",
        "sizes" : [ {
          "h" : 330,
          "resize" : "fit",
          "w" : 628
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 330,
          "resize" : "fit",
          "w" : 628
        }, {
          "h" : 330,
          "resize" : "fit",
          "w" : 628
        }, {
          "h" : 330,
          "resize" : "fit",
          "w" : 628
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ys3fCiiyFc"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 45, 67 ],
        "url" : "http:\/\/t.co\/9bpJsOMqb6",
        "expanded_url" : "http:\/\/wef.ch\/1GsEpUW",
        "display_url" : "wef.ch\/1GsEpUW"
      } ]
    },
    "geo" : { },
    "id_str" : "574191971591397376",
    "text" : "What is the cost of living around the world? http:\/\/t.co\/9bpJsOMqb6 http:\/\/t.co\/ys3fCiiyFc via @wef @SteveAkinsSEO http:\/\/t.co\/XEgqHZ1GKA",
    "id" : 574191971591397376,
    "created_at" : "2015-03-07 12:56:48 +0000",
    "user" : {
      "name" : "Ann Tran",
      "screen_name" : "AnnTran_",
      "protected" : false,
      "id_str" : "23135992",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/653008365170388992\/NS1ZVs6o_normal.jpg",
      "id" : 23135992,
      "verified" : true
    }
  },
  "id" : 575214404574253057,
  "created_at" : "2015-03-10 08:39:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/575212570812018688\/photo\/1",
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/IaxHHD07Ym",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_uQx40WwAANdF7.jpg",
      "id_str" : "575212564159840256",
      "id" : 575212564159840256,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_uQx40WwAANdF7.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IaxHHD07Ym"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575212570812018688",
  "text" : "Like what the school is doing. Friends for Life Programs. Say no to negativety. http:\/\/t.co\/IaxHHD07Ym",
  "id" : 575212570812018688,
  "created_at" : "2015-03-10 08:32:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "indices" : [ 3, 11 ],
      "id_str" : "14213600",
      "id" : 14213600
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575204886519939072",
  "text" : "RT @paulmah: Xiaomi is now the world's most valuable tech startup at US$45B, and is the largest smartphone maker in China for 2014",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "575179609626406913",
    "text" : "Xiaomi is now the world's most valuable tech startup at US$45B, and is the largest smartphone maker in China for 2014",
    "id" : 575179609626406913,
    "created_at" : "2015-03-10 06:21:19 +0000",
    "user" : {
      "name" : "Paul Mah",
      "screen_name" : "paulmah",
      "protected" : false,
      "id_str" : "14213600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882186001157832705\/gOEbAj77_normal.jpg",
      "id" : 14213600,
      "verified" : true
    }
  },
  "id" : 575204886519939072,
  "created_at" : "2015-03-10 08:01:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575204064058851328",
  "text" : "@avalanchelynn same in Ireland too on 15 March. That means going to call my mum twice...",
  "id" : 575204064058851328,
  "created_at" : "2015-03-10 07:58:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Focus Taiwan",
      "screen_name" : "Focus_Taiwan",
      "indices" : [ 3, 16 ],
      "id_str" : "60514666",
      "id" : 60514666
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Focus_Taiwan\/status\/575196668376698881\/photo\/1",
      "indices" : [ 103, 125 ],
      "url" : "http:\/\/t.co\/tZs0p9s3S7",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_uCUjUUcAAbyDf.jpg",
      "id_str" : "575196667009331200",
      "id" : 575196667009331200,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_uCUjUUcAAbyDf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 329,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 329,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 329,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 329,
        "resize" : "fit",
        "w" : 500
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tZs0p9s3S7"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/CL3mTThoUt",
      "expanded_url" : "http:\/\/focustaiwan.tw\/news\/aipl\/201503090039.aspx",
      "display_url" : "focustaiwan.tw\/news\/aipl\/2015\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "575203625099788288",
  "text" : "RT @Focus_Taiwan: Taiwan to mark allied victory in World War II with activities\nhttp:\/\/t.co\/CL3mTThoUt http:\/\/t.co\/tZs0p9s3S7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Focus_Taiwan\/status\/575196668376698881\/photo\/1",
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/tZs0p9s3S7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_uCUjUUcAAbyDf.jpg",
        "id_str" : "575196667009331200",
        "id" : 575196667009331200,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_uCUjUUcAAbyDf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 329,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 329,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 329,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 329,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tZs0p9s3S7"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 84 ],
        "url" : "http:\/\/t.co\/CL3mTThoUt",
        "expanded_url" : "http:\/\/focustaiwan.tw\/news\/aipl\/201503090039.aspx",
        "display_url" : "focustaiwan.tw\/news\/aipl\/2015\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "575196668376698881",
    "text" : "Taiwan to mark allied victory in World War II with activities\nhttp:\/\/t.co\/CL3mTThoUt http:\/\/t.co\/tZs0p9s3S7",
    "id" : 575196668376698881,
    "created_at" : "2015-03-10 07:29:07 +0000",
    "user" : {
      "name" : "Focus Taiwan",
      "screen_name" : "Focus_Taiwan",
      "protected" : false,
      "id_str" : "60514666",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/458797317127032832\/E1ww1CDH_normal.jpeg",
      "id" : 60514666,
      "verified" : false
    }
  },
  "id" : 575203625099788288,
  "created_at" : "2015-03-10 07:56:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "FiveThirtyEight",
      "screen_name" : "FiveThirtyEight",
      "indices" : [ 3, 19 ],
      "id_str" : "2303751216",
      "id" : 2303751216
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GE2015",
      "indices" : [ 98, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 128 ],
      "url" : "http:\/\/t.co\/7Tc2Xpkuz7",
      "expanded_url" : "http:\/\/53eig.ht\/1AaxsDx",
      "display_url" : "53eig.ht\/1AaxsDx"
    } ]
  },
  "geo" : { },
  "id_str" : "575095654936092672",
  "text" : "RT @FiveThirtyEight: We're covering this year's UK elections. Here's everything you need to know. #GE2015 http:\/\/t.co\/7Tc2Xpkuz7 http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FiveThirtyEight\/status\/575091314812788736\/photo\/1",
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/0yp3FHebSw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_sCVmbVAAAJF7o.jpg",
        "id_str" : "575055947535548416",
        "id" : 575055947535548416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_sCVmbVAAAJF7o.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 458,
          "resize" : "fit",
          "w" : 610
        }, {
          "h" : 458,
          "resize" : "fit",
          "w" : 610
        }, {
          "h" : 458,
          "resize" : "fit",
          "w" : 610
        }, {
          "h" : 458,
          "resize" : "fit",
          "w" : 610
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0yp3FHebSw"
      } ],
      "hashtags" : [ {
        "text" : "GE2015",
        "indices" : [ 77, 84 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/7Tc2Xpkuz7",
        "expanded_url" : "http:\/\/53eig.ht\/1AaxsDx",
        "display_url" : "53eig.ht\/1AaxsDx"
      } ]
    },
    "geo" : { },
    "id_str" : "575091314812788736",
    "text" : "We're covering this year's UK elections. Here's everything you need to know. #GE2015 http:\/\/t.co\/7Tc2Xpkuz7 http:\/\/t.co\/0yp3FHebSw",
    "id" : 575091314812788736,
    "created_at" : "2015-03-10 00:30:28 +0000",
    "user" : {
      "name" : "FiveThirtyEight",
      "screen_name" : "FiveThirtyEight",
      "protected" : false,
      "id_str" : "2303751216",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875426588061573121\/lpQG3W6i_normal.jpg",
      "id" : 2303751216,
      "verified" : true
    }
  },
  "id" : 575095654936092672,
  "created_at" : "2015-03-10 00:47:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575093940497612801",
  "text" : "\u201CWe think we want information when really we want\nknowledge.\u201D\n\u2014Nate Silver, from The Signal and the Noise",
  "id" : 575093940497612801,
  "created_at" : "2015-03-10 00:40:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tex_n_Singapore",
      "screen_name" : "Tex_n_Singapore",
      "indices" : [ 3, 19 ],
      "id_str" : "2651067600",
      "id" : 2651067600
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575088438845833216",
  "text" : "RT @Tex_n_Singapore: Zheng Guang Wanton Noodles \u2013 They spent 50 years serving excellent wanton noodles at Hin Hollywood canteen http:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Tex_n_Singapore\/status\/575087345164247040\/photo\/1",
        "indices" : [ 107, 129 ],
        "url" : "http:\/\/t.co\/nHo58TfOQI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_se5LQWEAEWQHz.jpg",
        "id_str" : "575087345042591745",
        "id" : 575087345042591745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_se5LQWEAEWQHz.jpg",
        "sizes" : [ {
          "h" : 739,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 491,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 739,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 739,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nHo58TfOQI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "575087345164247040",
    "text" : "Zheng Guang Wanton Noodles \u2013 They spent 50 years serving excellent wanton noodles at Hin Hollywood canteen http:\/\/t.co\/nHo58TfOQI",
    "id" : 575087345164247040,
    "created_at" : "2015-03-10 00:14:42 +0000",
    "user" : {
      "name" : "Tex_n_Singapore",
      "screen_name" : "Tex_n_Singapore",
      "protected" : false,
      "id_str" : "2651067600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/489405892517830657\/5_NmWxaN_normal.jpeg",
      "id" : 2651067600,
      "verified" : false
    }
  },
  "id" : 575088438845833216,
  "created_at" : "2015-03-10 00:19:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TheBattle",
      "indices" : [ 75, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575087594536636417",
  "text" : "There is this lady who runs a successful export-oriented business in Silgo #TheBattle",
  "id" : 575087594536636417,
  "created_at" : "2015-03-10 00:15:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575080038304989184",
  "text" : "what about turning some part of rural Ireland into filming location?",
  "id" : 575080038304989184,
  "created_at" : "2015-03-09 23:45:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/575077327408873472\/photo\/1",
      "indices" : [ 21, 43 ],
      "url" : "http:\/\/t.co\/omiV1t4blp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_sVyBcXAAATCAN.jpg",
      "id_str" : "575077326544896000",
      "id" : 575077326544896000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_sVyBcXAAATCAN.jpg",
      "sizes" : [ {
        "h" : 144,
        "resize" : "fit",
        "w" : 611
      }, {
        "h" : 144,
        "resize" : "fit",
        "w" : 611
      }, {
        "h" : 144,
        "resize" : "fit",
        "w" : 611
      }, {
        "h" : 144,
        "resize" : "fit",
        "w" : 611
      }, {
        "h" : 144,
        "resize" : "crop",
        "w" : 144
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/omiV1t4blp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575077327408873472",
  "text" : "What the story here? http:\/\/t.co\/omiV1t4blp",
  "id" : 575077327408873472,
  "created_at" : "2015-03-09 23:34:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u2729 Rachel Khoo \u2729",
      "screen_name" : "rkhooks",
      "indices" : [ 3, 11 ],
      "id_str" : "19883422",
      "id" : 19883422
    }, {
      "name" : "BBC Two",
      "screen_name" : "BBCTwo",
      "indices" : [ 20, 27 ],
      "id_str" : "1586183960",
      "id" : 1586183960
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/rkhooks\/status\/575034631310831616\/photo\/1",
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/evydfN1ESZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_V00HTVAAAaVYS.jpg",
      "id_str" : "573492966221742080",
      "id" : 573492966221742080,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_V00HTVAAAaVYS.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/evydfN1ESZ"
    } ],
    "hashtags" : [ {
      "text" : "Malaysia",
      "indices" : [ 54, 63 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/e1OOM4saV2",
      "expanded_url" : "http:\/\/ow.ly\/JIA6z",
      "display_url" : "ow.ly\/JIA6z"
    } ]
  },
  "geo" : { },
  "id_str" : "575044925416468480",
  "text" : "RT @rkhooks: I'm on @BBCTwo in 15 mins! Don't miss my #Malaysia adventure on A Cook Abroad http:\/\/t.co\/e1OOM4saV2 http:\/\/t.co\/evydfN1ESZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC Two",
        "screen_name" : "BBCTwo",
        "indices" : [ 7, 14 ],
        "id_str" : "1586183960",
        "id" : 1586183960
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rkhooks\/status\/575034631310831616\/photo\/1",
        "indices" : [ 101, 123 ],
        "url" : "http:\/\/t.co\/evydfN1ESZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_V00HTVAAAaVYS.jpg",
        "id_str" : "573492966221742080",
        "id" : 573492966221742080,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_V00HTVAAAaVYS.jpg",
        "sizes" : [ {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/evydfN1ESZ"
      } ],
      "hashtags" : [ {
        "text" : "Malaysia",
        "indices" : [ 41, 50 ]
      } ],
      "urls" : [ {
        "indices" : [ 78, 100 ],
        "url" : "http:\/\/t.co\/e1OOM4saV2",
        "expanded_url" : "http:\/\/ow.ly\/JIA6z",
        "display_url" : "ow.ly\/JIA6z"
      } ]
    },
    "geo" : { },
    "id_str" : "575034631310831616",
    "text" : "I'm on @BBCTwo in 15 mins! Don't miss my #Malaysia adventure on A Cook Abroad http:\/\/t.co\/e1OOM4saV2 http:\/\/t.co\/evydfN1ESZ",
    "id" : 575034631310831616,
    "created_at" : "2015-03-09 20:45:14 +0000",
    "user" : {
      "name" : "\u2729 Rachel Khoo \u2729",
      "screen_name" : "rkhooks",
      "protected" : false,
      "id_str" : "19883422",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659366236548108288\/G9h0MWn0_normal.jpg",
      "id" : 19883422,
      "verified" : true
    }
  },
  "id" : 575044925416468480,
  "created_at" : "2015-03-09 21:26:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brian Ibbott",
      "screen_name" : "Coverville",
      "indices" : [ 3, 14 ],
      "id_str" : "776031",
      "id" : 776031
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Coverville\/status\/574980753278287873\/photo\/1",
      "indices" : [ 53, 75 ],
      "url" : "http:\/\/t.co\/HXIGjCClpN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_q98rqUgAAKvfl.jpg",
      "id_str" : "574980752653189120",
      "id" : 574980752653189120,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_q98rqUgAAKvfl.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 354,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 354,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 354,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 354,
        "resize" : "fit",
        "w" : 636
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HXIGjCClpN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "575012032413564928",
  "text" : "RT @Coverville: Don\u2019t think we didn\u2019t notice, Apple\u2026 http:\/\/t.co\/HXIGjCClpN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Coverville\/status\/574980753278287873\/photo\/1",
        "indices" : [ 37, 59 ],
        "url" : "http:\/\/t.co\/HXIGjCClpN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_q98rqUgAAKvfl.jpg",
        "id_str" : "574980752653189120",
        "id" : 574980752653189120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_q98rqUgAAKvfl.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 354,
          "resize" : "fit",
          "w" : 636
        }, {
          "h" : 354,
          "resize" : "fit",
          "w" : 636
        }, {
          "h" : 354,
          "resize" : "fit",
          "w" : 636
        }, {
          "h" : 354,
          "resize" : "fit",
          "w" : 636
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HXIGjCClpN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574980753278287873",
    "text" : "Don\u2019t think we didn\u2019t notice, Apple\u2026 http:\/\/t.co\/HXIGjCClpN",
    "id" : 574980753278287873,
    "created_at" : "2015-03-09 17:11:08 +0000",
    "user" : {
      "name" : "Brian Ibbott",
      "screen_name" : "Coverville",
      "protected" : false,
      "id_str" : "776031",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459471383668330496\/Z5BVqC3Y_normal.jpeg",
      "id" : 776031,
      "verified" : true
    }
  },
  "id" : 575012032413564928,
  "created_at" : "2015-03-09 19:15:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Panzarino",
      "screen_name" : "panzer",
      "indices" : [ 3, 10 ],
      "id_str" : "19312115",
      "id" : 19312115
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574996623547363329",
  "text" : "RT @panzer: First woman on an Apple stage in forever: Christy Turlington.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574994185691881472",
    "text" : "First woman on an Apple stage in forever: Christy Turlington.",
    "id" : 574994185691881472,
    "created_at" : "2015-03-09 18:04:31 +0000",
    "user" : {
      "name" : "Matthew Panzarino",
      "screen_name" : "panzer",
      "protected" : false,
      "id_str" : "19312115",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972174840017793024\/EBlzynRx_normal.jpg",
      "id" : 19312115,
      "verified" : true
    }
  },
  "id" : 574996623547363329,
  "created_at" : "2015-03-09 18:14:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 3, 14 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574984917743984640",
  "text" : "RT @davegantly: Liking Apple's research kit idea, but I reckon all our health\/med data should be collected and available, anonymously, to r\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574984749162348546",
    "text" : "Liking Apple's research kit idea, but I reckon all our health\/med data should be collected and available, anonymously, to researchers, etc.",
    "id" : 574984749162348546,
    "created_at" : "2015-03-09 17:27:01 +0000",
    "user" : {
      "name" : "David",
      "screen_name" : "davegantly",
      "protected" : false,
      "id_str" : "16738907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001158218519863296\/rY-yu2r7_normal.jpg",
      "id" : 16738907,
      "verified" : false
    }
  },
  "id" : 574984917743984640,
  "created_at" : "2015-03-09 17:27:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574983027723485186",
  "text" : "I remember I have an iPad that means I can watch the streaming video!",
  "id" : 574983027723485186,
  "created_at" : "2015-03-09 17:20:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574981689295925250",
  "text" : "@bharatidalela I realized if you are on Window there is no way to download the latest version to watch the streaming video.",
  "id" : 574981689295925250,
  "created_at" : "2015-03-09 17:14:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kathleen Wynne",
      "screen_name" : "Kathleen_Wynne",
      "indices" : [ 3, 18 ],
      "id_str" : "193725962",
      "id" : 193725962
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WhoWillYouHelp",
      "indices" : [ 99, 114 ]
    } ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/QvjW1yFYlG",
      "expanded_url" : "https:\/\/amp.twimg.com\/v\/e3533b92-82ef-4734-a384-6b2b85a0e905",
      "display_url" : "amp.twimg.com\/v\/e3533b92-82e\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "574970490529386496",
  "text" : "RT @Kathleen_Wynne: We will not condone, normalize or minimize harmful sexually violent behaviour. #WhoWillYouHelp https:\/\/t.co\/QvjW1yFYlG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WhoWillYouHelp",
        "indices" : [ 79, 94 ]
      } ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/QvjW1yFYlG",
        "expanded_url" : "https:\/\/amp.twimg.com\/v\/e3533b92-82ef-4734-a384-6b2b85a0e905",
        "display_url" : "amp.twimg.com\/v\/e3533b92-82e\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "574961582175879168",
    "text" : "We will not condone, normalize or minimize harmful sexually violent behaviour. #WhoWillYouHelp https:\/\/t.co\/QvjW1yFYlG",
    "id" : 574961582175879168,
    "created_at" : "2015-03-09 15:54:58 +0000",
    "user" : {
      "name" : "Kathleen Wynne",
      "screen_name" : "Kathleen_Wynne",
      "protected" : false,
      "id_str" : "193725962",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/994681033604780033\/St3RFpEF_normal.jpg",
      "id" : 193725962,
      "verified" : true
    }
  },
  "id" : 574970490529386496,
  "created_at" : "2015-03-09 16:30:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http:\/\/t.co\/2cWO04GXl8",
      "expanded_url" : "http:\/\/www.getoptimise.com\/selfemployed\/?page_id=5",
      "display_url" : "getoptimise.com\/selfemployed\/?\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "574910810348253185",
  "text" : "Tell your story of self-employment in Ireland http:\/\/t.co\/2cWO04GXl8 No sign up require.",
  "id" : 574910810348253185,
  "created_at" : "2015-03-09 12:33:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574909630003998720",
  "text" : "Should not have mess with Chrome sync. Now I lost all bookmarks and history.",
  "id" : 574909630003998720,
  "created_at" : "2015-03-09 12:28:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "indices" : [ 3, 15 ],
      "id_str" : "14479046",
      "id" : 14479046
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574906474973036545",
  "text" : "RT @daniel_kane: If EU push ahead with plans for a joint army where does that leave Ireland in terms on EU membership and neutrality? #EUar\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "EUarmy",
        "indices" : [ 117, 124 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574875840825131008",
    "text" : "If EU push ahead with plans for a joint army where does that leave Ireland in terms on EU membership and neutrality? #EUarmy",
    "id" : 574875840825131008,
    "created_at" : "2015-03-09 10:14:15 +0000",
    "user" : {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "protected" : false,
      "id_str" : "14479046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959158926339137536\/dxkfCxt4_normal.jpg",
      "id" : 14479046,
      "verified" : false
    }
  },
  "id" : 574906474973036545,
  "created_at" : "2015-03-09 12:15:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Doha News",
      "screen_name" : "dohanews",
      "indices" : [ 3, 12 ],
      "id_str" : "23123824",
      "id" : 23123824
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/dohanews\/status\/574889375223279617\/photo\/1",
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/bBOvOBW4rx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_pq05yUYAELsrA.png",
      "id_str" : "574889359540772865",
      "id" : 574889359540772865,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_pq05yUYAELsrA.png",
      "sizes" : [ {
        "h" : 379,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 430,
        "resize" : "fit",
        "w" : 771
      }, {
        "h" : 430,
        "resize" : "fit",
        "w" : 771
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 430,
        "resize" : "fit",
        "w" : 771
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bBOvOBW4rx"
    } ],
    "hashtags" : [ {
      "text" : "Doha",
      "indices" : [ 42, 47 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/XSbAQ4ndXz",
      "expanded_url" : "http:\/\/dohane.ws\/1CUad6D",
      "display_url" : "dohane.ws\/1CUad6D"
    } ]
  },
  "geo" : { },
  "id_str" : "574905903822028800",
  "text" : "RT @dohanews: Former Nepali ambassador to #Doha urges investigation into expat deaths. http:\/\/t.co\/XSbAQ4ndXz http:\/\/t.co\/bBOvOBW4rx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dohanews\/status\/574889375223279617\/photo\/1",
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/bBOvOBW4rx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_pq05yUYAELsrA.png",
        "id_str" : "574889359540772865",
        "id" : 574889359540772865,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_pq05yUYAELsrA.png",
        "sizes" : [ {
          "h" : 379,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 430,
          "resize" : "fit",
          "w" : 771
        }, {
          "h" : 430,
          "resize" : "fit",
          "w" : 771
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 430,
          "resize" : "fit",
          "w" : 771
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bBOvOBW4rx"
      } ],
      "hashtags" : [ {
        "text" : "Doha",
        "indices" : [ 28, 33 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/XSbAQ4ndXz",
        "expanded_url" : "http:\/\/dohane.ws\/1CUad6D",
        "display_url" : "dohane.ws\/1CUad6D"
      } ]
    },
    "geo" : { },
    "id_str" : "574889375223279617",
    "text" : "Former Nepali ambassador to #Doha urges investigation into expat deaths. http:\/\/t.co\/XSbAQ4ndXz http:\/\/t.co\/bBOvOBW4rx",
    "id" : 574889375223279617,
    "created_at" : "2015-03-09 11:08:02 +0000",
    "user" : {
      "name" : "Doha News",
      "screen_name" : "dohanews",
      "protected" : false,
      "id_str" : "23123824",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/790832152778772480\/rp8BozXv_normal.jpg",
      "id" : 23123824,
      "verified" : true
    }
  },
  "id" : 574905903822028800,
  "created_at" : "2015-03-09 12:13:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 0, 9 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 10, 20 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    }, {
      "name" : "WSJ Expat",
      "screen_name" : "WSJexpat",
      "indices" : [ 38, 47 ],
      "id_str" : "28171205",
      "id" : 28171205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/RUKqDkxKHs",
      "expanded_url" : "http:\/\/graphics.wsj.com\/documents\/expatquiz\/",
      "display_url" : "graphics.wsj.com\/documents\/expa\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "574900432163766272",
  "geo" : { },
  "id_str" : "574901267425812480",
  "in_reply_to_user_id" : 19338425,
  "text" : "@Fondacey @starduest @sweden @Capesgo @WSJexpat There you go http:\/\/t.co\/RUKqDkxKHs",
  "id" : 574901267425812480,
  "in_reply_to_status_id" : 574900432163766272,
  "created_at" : "2015-03-09 11:55:17 +0000",
  "in_reply_to_screen_name" : "Fondacey",
  "in_reply_to_user_id_str" : "19338425",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 11, 20 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    }, {
      "name" : "WSJ Expat",
      "screen_name" : "WSJexpat",
      "indices" : [ 38, 47 ],
      "id_str" : "28171205",
      "id" : 28171205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574899105970917376",
  "geo" : { },
  "id_str" : "574900152361746432",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest @Fondacey @sweden @Capesgo @WSJexpat maybe cramped cabin is the shipping container with a middle class veneer.",
  "id" : 574900152361746432,
  "in_reply_to_status_id" : 574899105970917376,
  "created_at" : "2015-03-09 11:50:52 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steve Leonard",
      "screen_name" : "steveleonardSG",
      "indices" : [ 0, 15 ],
      "id_str" : "2574271958",
      "id" : 2574271958
    }, {
      "name" : "IDA - Deactivated",
      "screen_name" : "ida_singapore",
      "indices" : [ 16, 30 ],
      "id_str" : "3145679317",
      "id" : 3145679317
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574086164740624384",
  "geo" : { },
  "id_str" : "574899690409431042",
  "in_reply_to_user_id" : 2574271958,
  "text" : "@steveleonardSG @IDA_SINGAPORE Looking forward to see these in HDB multi-story car park.",
  "id" : 574899690409431042,
  "in_reply_to_status_id" : 574086164740624384,
  "created_at" : "2015-03-09 11:49:01 +0000",
  "in_reply_to_screen_name" : "steveleonardSG",
  "in_reply_to_user_id_str" : "2574271958",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 11, 20 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    }, {
      "name" : "WSJ Expat",
      "screen_name" : "WSJexpat",
      "indices" : [ 38, 47 ],
      "id_str" : "28171205",
      "id" : 28171205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574898014076145665",
  "geo" : { },
  "id_str" : "574898858423091200",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest @Fondacey @sweden @Capesgo @WSJexpat The quiz make me realise how lucky I am. I came here on British Airway and I complain.",
  "id" : 574898858423091200,
  "in_reply_to_status_id" : 574898014076145665,
  "created_at" : "2015-03-09 11:45:43 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 11, 20 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    }, {
      "name" : "WSJ Expat",
      "screen_name" : "WSJexpat",
      "indices" : [ 38, 47 ],
      "id_str" : "28171205",
      "id" : 28171205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574898014076145665",
  "geo" : { },
  "id_str" : "574898562582052864",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest @Fondacey @sweden @Capesgo @WSJexpat I find it amusing. The quizz also ask do you comes by shipping container.",
  "id" : 574898562582052864,
  "in_reply_to_status_id" : 574898014076145665,
  "created_at" : "2015-03-09 11:44:33 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 11, 20 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    }, {
      "name" : "WSJ Expat",
      "screen_name" : "WSJexpat",
      "indices" : [ 52, 61 ],
      "id_str" : "28171205",
      "id" : 28171205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574896772570546176",
  "geo" : { },
  "id_str" : "574897518573654016",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest @Fondacey @sweden @Capesgo Few weeks ago @WSJexpat in a quizz define migrants as those use prepay phone &amp; learn English.",
  "id" : 574897518573654016,
  "in_reply_to_status_id" : 574896772570546176,
  "created_at" : "2015-03-09 11:40:24 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 11, 20 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    }, {
      "name" : "WSJ Expat",
      "screen_name" : "WSJexpat",
      "indices" : [ 55, 64 ],
      "id_str" : "28171205",
      "id" : 28171205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574896141860499456",
  "geo" : { },
  "id_str" : "574896483742449665",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest @Fondacey @sweden @Capesgo that also can be @WSJexpat definition of expats.",
  "id" : 574896483742449665,
  "in_reply_to_status_id" : 574896141860499456,
  "created_at" : "2015-03-09 11:36:17 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Charlotte Baker",
      "screen_name" : "CharlotteNBaker",
      "indices" : [ 3, 19 ],
      "id_str" : "303469649",
      "id" : 303469649
    }, {
      "name" : "Cathy Jaquiss-Ollier",
      "screen_name" : "CathyJaquiss",
      "indices" : [ 71, 84 ],
      "id_str" : "297481552",
      "id" : 297481552
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/b8ljbbKr2y",
      "expanded_url" : "http:\/\/www.independent.co.uk\/news\/world\/europe\/german-couple-starts-website-to-match-asylum-seekers-with-potential-housemates-10093113.html",
      "display_url" : "independent.co.uk\/news\/world\/eur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "574894117001560064",
  "text" : "RT @CharlotteNBaker: This is so cool! A better way to welcome refugees @CathyJaquiss http:\/\/t.co\/b8ljbbKr2y",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Cathy Jaquiss-Ollier",
        "screen_name" : "CathyJaquiss",
        "indices" : [ 50, 63 ],
        "id_str" : "297481552",
        "id" : 297481552
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http:\/\/t.co\/b8ljbbKr2y",
        "expanded_url" : "http:\/\/www.independent.co.uk\/news\/world\/europe\/german-couple-starts-website-to-match-asylum-seekers-with-potential-housemates-10093113.html",
        "display_url" : "independent.co.uk\/news\/world\/eur\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "574891801343094784",
    "text" : "This is so cool! A better way to welcome refugees @CathyJaquiss http:\/\/t.co\/b8ljbbKr2y",
    "id" : 574891801343094784,
    "created_at" : "2015-03-09 11:17:41 +0000",
    "user" : {
      "name" : "Charlotte Baker",
      "screen_name" : "CharlotteNBaker",
      "protected" : false,
      "id_str" : "303469649",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/927231232408203264\/vvH9KD-U_normal.jpg",
      "id" : 303469649,
      "verified" : false
    }
  },
  "id" : 574894117001560064,
  "created_at" : "2015-03-09 11:26:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 0, 9 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 10, 20 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 21, 28 ],
      "id_str" : "19663706",
      "id" : 19663706
    }, {
      "name" : "Carlos A. Perdomo",
      "screen_name" : "Capesgo",
      "indices" : [ 29, 37 ],
      "id_str" : "227274783",
      "id" : 227274783
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574890830147751936",
  "geo" : { },
  "id_str" : "574893752059346944",
  "in_reply_to_user_id" : 19338425,
  "text" : "@Fondacey @starduest @sweden @Capesgo What about? Expat is a westerner and migrant are not a westerner",
  "id" : 574893752059346944,
  "in_reply_to_status_id" : 574890830147751936,
  "created_at" : "2015-03-09 11:25:26 +0000",
  "in_reply_to_screen_name" : "Fondacey",
  "in_reply_to_user_id_str" : "19338425",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "indices" : [ 3, 12 ],
      "id_str" : "19338425",
      "id" : 19338425
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 14, 21 ],
      "id_str" : "19663706",
      "id" : 19663706
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574893093482336258",
  "text" : "RT @Fondacey: @sweden An expat most often not an migrant &amp; technically a migrant is never an expat, though may self-identifiy if from an i-\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "@sweden \/ Rebecka",
        "screen_name" : "sweden",
        "indices" : [ 0, 7 ],
        "id_str" : "19663706",
        "id" : 19663706
      }, {
        "name" : "Carlos A. Perdomo",
        "screen_name" : "Capesgo",
        "indices" : [ 135, 143 ],
        "id_str" : "227274783",
        "id" : 227274783
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "574889311805444096",
    "geo" : { },
    "id_str" : "574890830147751936",
    "in_reply_to_user_id" : 19663706,
    "text" : "@sweden An expat most often not an migrant &amp; technically a migrant is never an expat, though may self-identifiy if from an i-land. @Capesgo",
    "id" : 574890830147751936,
    "in_reply_to_status_id" : 574889311805444096,
    "created_at" : "2015-03-09 11:13:49 +0000",
    "in_reply_to_screen_name" : "sweden",
    "in_reply_to_user_id_str" : "19663706",
    "user" : {
      "name" : "E. Dacey-Fondelius",
      "screen_name" : "Fondacey",
      "protected" : false,
      "id_str" : "19338425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3157014212\/d431af4ae6cd671dfa76a92f7ceceee4_normal.jpeg",
      "id" : 19338425,
      "verified" : false
    }
  },
  "id" : 574893093482336258,
  "created_at" : "2015-03-09 11:22:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574879889473601536",
  "text" : "\"We have 50,000 registered users.\"  Do they still use the service? Are they the right people?",
  "id" : 574879889473601536,
  "created_at" : "2015-03-09 10:30:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNET",
      "screen_name" : "CNET",
      "indices" : [ 3, 8 ],
      "id_str" : "30261067",
      "id" : 30261067
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/CNET\/status\/574676070055682048\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/sI1wmGtFMH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_mo10WXAAEOIKG.jpg",
      "id_str" : "574676070005407745",
      "id" : 574676070005407745,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_mo10WXAAEOIKG.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 770
      }, {
        "h" : 253,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 770
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 770
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sI1wmGtFMH"
    } ],
    "hashtags" : [ {
      "text" : "MWC15",
      "indices" : [ 101, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/R2muMvzkDU",
      "expanded_url" : "http:\/\/cnet.co\/1MbOzMW",
      "display_url" : "cnet.co\/1MbOzMW"
    } ]
  },
  "geo" : { },
  "id_str" : "574687933820727298",
  "text" : "RT @CNET: How 5G will push a supercharged network to your phone, home and car http:\/\/t.co\/R2muMvzkDU #MWC15 http:\/\/t.co\/sI1wmGtFMH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CNET\/status\/574676070055682048\/photo\/1",
        "indices" : [ 98, 120 ],
        "url" : "http:\/\/t.co\/sI1wmGtFMH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_mo10WXAAEOIKG.jpg",
        "id_str" : "574676070005407745",
        "id" : 574676070005407745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_mo10WXAAEOIKG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 287,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 253,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 287,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 287,
          "resize" : "fit",
          "w" : 770
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sI1wmGtFMH"
      } ],
      "hashtags" : [ {
        "text" : "MWC15",
        "indices" : [ 91, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 68, 90 ],
        "url" : "http:\/\/t.co\/R2muMvzkDU",
        "expanded_url" : "http:\/\/cnet.co\/1MbOzMW",
        "display_url" : "cnet.co\/1MbOzMW"
      } ]
    },
    "geo" : { },
    "id_str" : "574676070055682048",
    "text" : "How 5G will push a supercharged network to your phone, home and car http:\/\/t.co\/R2muMvzkDU #MWC15 http:\/\/t.co\/sI1wmGtFMH",
    "id" : 574676070055682048,
    "created_at" : "2015-03-08 21:00:26 +0000",
    "user" : {
      "name" : "CNET",
      "screen_name" : "CNET",
      "protected" : false,
      "id_str" : "30261067",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963998359001317376\/scuoOV5m_normal.jpg",
      "id" : 30261067,
      "verified" : true
    }
  },
  "id" : 574687933820727298,
  "created_at" : "2015-03-08 21:47:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574672193432477696",
  "text" : "Now you can fly from Dublin to Asia with Finnair via Helsinki.",
  "id" : 574672193432477696,
  "created_at" : "2015-03-08 20:45:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574671087667146755",
  "text" : "On this day, So quiet on my FB feed.  No storming of the Bastille and no call to arms.",
  "id" : 574671087667146755,
  "created_at" : "2015-03-08 20:40:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ravi Simhambhatla",
      "screen_name" : "ravioli67",
      "indices" : [ 3, 13 ],
      "id_str" : "25138148",
      "id" : 25138148
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ravioli67\/status\/574562029333315584\/photo\/1",
      "indices" : [ 66, 88 ],
      "url" : "http:\/\/t.co\/xDyJSjXZbP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_lBHdVWYAApmiA.jpg",
      "id_str" : "574562023855513600",
      "id" : 574562023855513600,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_lBHdVWYAApmiA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xDyJSjXZbP"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574562177367085056",
  "text" : "RT @ravioli67: Beautiful Ireland. View from the Howth Cliff walk. http:\/\/t.co\/xDyJSjXZbP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ravioli67\/status\/574562029333315584\/photo\/1",
        "indices" : [ 51, 73 ],
        "url" : "http:\/\/t.co\/xDyJSjXZbP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_lBHdVWYAApmiA.jpg",
        "id_str" : "574562023855513600",
        "id" : 574562023855513600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_lBHdVWYAApmiA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xDyJSjXZbP"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574562029333315584",
    "text" : "Beautiful Ireland. View from the Howth Cliff walk. http:\/\/t.co\/xDyJSjXZbP",
    "id" : 574562029333315584,
    "created_at" : "2015-03-08 13:27:17 +0000",
    "user" : {
      "name" : "Ravi Simhambhatla",
      "screen_name" : "ravioli67",
      "protected" : false,
      "id_str" : "25138148",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784809795190525953\/NKtGQlHw_normal.jpg",
      "id" : 25138148,
      "verified" : false
    }
  },
  "id" : 574562177367085056,
  "created_at" : "2015-03-08 13:27:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victoria Scott",
      "screen_name" : "Toryscott",
      "indices" : [ 3, 13 ],
      "id_str" : "60876742",
      "id" : 60876742
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574543863869763584",
  "text" : "RT @Toryscott: And on International Women's Day - \"Written permission to leave compound now required for some Qatar workers.\"  http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/mTUVcoPR1y",
        "expanded_url" : "http:\/\/dohanews.co\/written-permission-to-leave-compound-now-required-for-some-qatar-workers\/#disqus_thread",
        "display_url" : "dohanews.co\/written-permis\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "574543277031993344",
    "text" : "And on International Women's Day - \"Written permission to leave compound now required for some Qatar workers.\"  http:\/\/t.co\/mTUVcoPR1y",
    "id" : 574543277031993344,
    "created_at" : "2015-03-08 12:12:46 +0000",
    "user" : {
      "name" : "Victoria Scott",
      "screen_name" : "Toryscott",
      "protected" : false,
      "id_str" : "60876742",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005053368845393921\/60swwYy6_normal.jpg",
      "id" : 60876742,
      "verified" : true
    }
  },
  "id" : 574543863869763584,
  "created_at" : "2015-03-08 12:15:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/574526462897823746\/photo\/1",
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/lxMZRsF81y",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_kgxfxXEAAJ8Xh.png",
      "id_str" : "574526462180659200",
      "id" : 574526462180659200,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_kgxfxXEAAJ8Xh.png",
      "sizes" : [ {
        "h" : 154,
        "resize" : "fit",
        "w" : 651
      }, {
        "h" : 154,
        "resize" : "fit",
        "w" : 651
      }, {
        "h" : 154,
        "resize" : "fit",
        "w" : 651
      }, {
        "h" : 154,
        "resize" : "fit",
        "w" : 651
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lxMZRsF81y"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574526462897823746",
  "text" : "One more click to access someone website on Linkedin profile. It under Contact Info. http:\/\/t.co\/lxMZRsF81y",
  "id" : 574526462897823746,
  "created_at" : "2015-03-08 11:05:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 118 ],
      "url" : "http:\/\/t.co\/roTE0jpCpm",
      "expanded_url" : "http:\/\/chronicle.com\/article\/The-Benefits-of-No-Tech-Note\/228089\/",
      "display_url" : "chronicle.com\/article\/The-Be\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "574513226253230080",
  "text" : "A year after banning students from taking notes on laptops, a professor reports on the results. http:\/\/t.co\/roTE0jpCpm",
  "id" : 574513226253230080,
  "created_at" : "2015-03-08 10:13:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http:\/\/t.co\/II865sqHcp",
      "expanded_url" : "http:\/\/www.todayonline.com\/www.todayonline.com\/more-sporeans-overseas-brain-drain-concerns-dissipate-0",
      "display_url" : "todayonline.com\/www.todayonlin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "574511493275860992",
  "text" : "More S\u2019poreans overseas, but brain drain concerns dissipate http:\/\/t.co\/II865sqHcp",
  "id" : 574511493275860992,
  "created_at" : "2015-03-08 10:06:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/574511201884966913\/photo\/1",
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/KyMsIcVPvl",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_kS5LyWIAEmsf5.png",
      "id_str" : "574511201092247553",
      "id" : 574511201092247553,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_kS5LyWIAEmsf5.png",
      "sizes" : [ {
        "h" : 414,
        "resize" : "fit",
        "w" : 522
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 522
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 522
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 522
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KyMsIcVPvl"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574511201884966913",
  "text" : "It is possible to build a visitors personas base on the type of ISP they use? http:\/\/t.co\/KyMsIcVPvl",
  "id" : 574511201884966913,
  "created_at" : "2015-03-08 10:05:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeffrey Paine",
      "screen_name" : "jpaine",
      "indices" : [ 3, 10 ],
      "id_str" : "9650622",
      "id" : 9650622
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/CExDaCJG6k",
      "expanded_url" : "http:\/\/bit.ly\/1EZ8Enp",
      "display_url" : "bit.ly\/1EZ8Enp"
    } ]
  },
  "geo" : { },
  "id_str" : "574508661017591808",
  "text" : "RT @jpaine: Asia\u2019s tech industry is about to enter a golden age http:\/\/t.co\/CExDaCJG6k",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 52, 74 ],
        "url" : "http:\/\/t.co\/CExDaCJG6k",
        "expanded_url" : "http:\/\/bit.ly\/1EZ8Enp",
        "display_url" : "bit.ly\/1EZ8Enp"
      } ]
    },
    "geo" : { },
    "id_str" : "574495045673484289",
    "text" : "Asia\u2019s tech industry is about to enter a golden age http:\/\/t.co\/CExDaCJG6k",
    "id" : 574495045673484289,
    "created_at" : "2015-03-08 09:01:07 +0000",
    "user" : {
      "name" : "Jeffrey Paine",
      "screen_name" : "jpaine",
      "protected" : false,
      "id_str" : "9650622",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/546667795962793985\/r8aVQ0Na_normal.jpeg",
      "id" : 9650622,
      "verified" : false
    }
  },
  "id" : 574508661017591808,
  "created_at" : "2015-03-08 09:55:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Irish For \uD83E\uDD64\u26BD\uFE0F\uD83C\uDFC3\uD83C\uDFFB\u200D\u2642\uFE0F",
      "screen_name" : "theirishfor",
      "indices" : [ 3, 15 ],
      "id_str" : "2978345031",
      "id" : 2978345031
    }, {
      "name" : "Damien Owens",
      "screen_name" : "OwensDamien",
      "indices" : [ 24, 36 ],
      "id_str" : "67725191",
      "id" : 67725191
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574505993637330944",
  "text" : "RT @theirishfor: Ha! RT @OwensDamien The name Dublin comes from the Gaelic \u2018dubh linn\u2019, meaning \u2018stag venue\u2019.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\" rel=\"nofollow\"\u003EEchofon for Android PRO\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Damien Owens",
        "screen_name" : "OwensDamien",
        "indices" : [ 7, 19 ],
        "id_str" : "67725191",
        "id" : 67725191
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574504648184315904",
    "text" : "Ha! RT @OwensDamien The name Dublin comes from the Gaelic \u2018dubh linn\u2019, meaning \u2018stag venue\u2019.",
    "id" : 574504648184315904,
    "created_at" : "2015-03-08 09:39:16 +0000",
    "user" : {
      "name" : "The Irish For \uD83E\uDD64\u26BD\uFE0F\uD83C\uDFC3\uD83C\uDFFB\u200D\u2642\uFE0F",
      "screen_name" : "theirishfor",
      "protected" : false,
      "id_str" : "2978345031",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033112789211127808\/CzyXxf1e_normal.jpg",
      "id" : 2978345031,
      "verified" : false
    }
  },
  "id" : 574505993637330944,
  "created_at" : "2015-03-08 09:44:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Open Minds",
      "screen_name" : "MindsOpened",
      "indices" : [ 3, 15 ],
      "id_str" : "1870056282",
      "id" : 1870056282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "574505432863096832",
  "text" : "RT @MindsOpened: If we want to be accepted as we are, we have to be willing to accept others as they are.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.kucukbeyin.com\/\" rel=\"nofollow\"\u003Efahdjfasdfajsdlkfjalsj\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "574494773580595200",
    "text" : "If we want to be accepted as we are, we have to be willing to accept others as they are.",
    "id" : 574494773580595200,
    "created_at" : "2015-03-08 09:00:02 +0000",
    "user" : {
      "name" : "Open Minds",
      "screen_name" : "MindsOpened",
      "protected" : false,
      "id_str" : "1870056282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/473116416615280641\/_NLFmh-w_normal.jpeg",
      "id" : 1870056282,
      "verified" : false
    }
  },
  "id" : 574505432863096832,
  "created_at" : "2015-03-08 09:42:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 0, 8 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "574177492082253825",
  "geo" : { },
  "id_str" : "574182806915579904",
  "in_reply_to_user_id" : 19360077,
  "text" : "@pdscott sometimes I have problem trying to find where I put my notes.",
  "id" : 574182806915579904,
  "in_reply_to_status_id" : 574177492082253825,
  "created_at" : "2015-03-07 12:20:23 +0000",
  "in_reply_to_screen_name" : "pdscott",
  "in_reply_to_user_id_str" : "19360077",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/pcFJcFiZ4G",
      "expanded_url" : "https:\/\/www.facebook.com\/video.php?v=1419888568058329",
      "display_url" : "facebook.com\/video.php?v=14\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "574013717492408320",
  "text" : "Guy get cocky pre-fight and.... https:\/\/t.co\/pcFJcFiZ4G",
  "id" : 574013717492408320,
  "created_at" : "2015-03-07 01:08:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http:\/\/t.co\/KlEGblwwi4",
      "expanded_url" : "http:\/\/theworkingcapitol.com\/en#tour",
      "display_url" : "theworkingcapitol.com\/en#tour"
    } ]
  },
  "geo" : { },
  "id_str" : "574005375512526848",
  "text" : "New co-working place in Singapore. http:\/\/t.co\/KlEGblwwi4 Accessible by Tanjong Pagar and Outram MRT station",
  "id" : 574005375512526848,
  "created_at" : "2015-03-07 00:35:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http:\/\/t.co\/3YXq2ryZnk",
      "expanded_url" : "http:\/\/www.getoptimise.com\/book-review-of-remote\/",
      "display_url" : "getoptimise.com\/book-review-of\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "573999556213497856",
  "text" : "My review on 37signals founder book \"Remote\"  http:\/\/t.co\/3YXq2ryZnk",
  "id" : 573999556213497856,
  "created_at" : "2015-03-07 00:12:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573990564644978688",
  "text" : "Was informed by neighbour NBA game tonight (GMT timezone).",
  "id" : 573990564644978688,
  "created_at" : "2015-03-06 23:36:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573979357913444353",
  "text" : "RT @patphelan: New LinkedIn update is busy :-( I really don't care if Mary changed her profile picture or Rick is 4 years in this job tbh",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "573969584342044674",
    "text" : "New LinkedIn update is busy :-( I really don't care if Mary changed her profile picture or Rick is 4 years in this job tbh",
    "id" : 573969584342044674,
    "created_at" : "2015-03-06 22:13:07 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 573979357913444353,
  "created_at" : "2015-03-06 22:51:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/573935534873141248\/photo\/1",
      "indices" : [ 8, 30 ],
      "url" : "http:\/\/t.co\/gLS5KeXmgm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_cHUsfWEAE1b-w.jpg",
      "id_str" : "573935529634435073",
      "id" : 573935529634435073,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_cHUsfWEAE1b-w.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gLS5KeXmgm"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573935534873141248",
  "text" : "Dinner. http:\/\/t.co\/gLS5KeXmgm",
  "id" : 573935534873141248,
  "created_at" : "2015-03-06 19:57:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573905200362164224",
  "text" : "Friday and weekend. Getting gear up for Monday.",
  "id" : 573905200362164224,
  "created_at" : "2015-03-06 17:57:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 56 ],
      "url" : "http:\/\/t.co\/2cWO04GXl8",
      "expanded_url" : "http:\/\/www.getoptimise.com\/selfemployed\/?page_id=5",
      "display_url" : "getoptimise.com\/selfemployed\/?\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "573892284862849027",
  "text" : "Share your self-employment story. http:\/\/t.co\/2cWO04GXl8 No registration require.",
  "id" : 573892284862849027,
  "created_at" : "2015-03-06 17:05:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "smecommunity",
      "indices" : [ 55, 68 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573876094283223040",
  "text" : "Which Time tracking &amp; invoice software do you use? #smecommunity",
  "id" : 573876094283223040,
  "created_at" : "2015-03-06 16:01:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startup",
      "indices" : [ 12, 20 ]
    }, {
      "text" : "futurehealth",
      "indices" : [ 119, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573790007120494593",
  "text" : "Wish to see #startup whose business model is to bring generic prescription medicines in Ireland without us going to NI #futurehealth",
  "id" : 573790007120494593,
  "created_at" : "2015-03-06 10:19:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/573789784205824000\/photo\/1",
      "indices" : [ 50, 72 ],
      "url" : "http:\/\/t.co\/qZL4U1d6FL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_aCw-DXAAAvNQl.jpg",
      "id_str" : "573789780338737152",
      "id" : 573789780338737152,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_aCw-DXAAAvNQl.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2592,
        "resize" : "fit",
        "w" : 1944
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qZL4U1d6FL"
    } ],
    "hashtags" : [ {
      "text" : "futurehealth",
      "indices" : [ 36, 49 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573789784205824000",
  "text" : "This 1 tablet set me back at EUR 10.#futurehealth http:\/\/t.co\/qZL4U1d6FL",
  "id" : 573789784205824000,
  "created_at" : "2015-03-06 10:18:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tim Wu",
      "screen_name" : "Hyperconnection",
      "indices" : [ 3, 19 ],
      "id_str" : "140509231",
      "id" : 140509231
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573756472431132672",
  "text" : "RT @Hyperconnection: Super cute - People In Australia Have Started Taking Selfies With Quokkas, And The Results Are Brilliant.:  http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/oZREH24YWH",
        "expanded_url" : "http:\/\/pulptastic.com\/quokkas-selfie\/",
        "display_url" : "pulptastic.com\/quokkas-selfie\/"
      } ]
    },
    "geo" : { },
    "id_str" : "573750502925602816",
    "text" : "Super cute - People In Australia Have Started Taking Selfies With Quokkas, And The Results Are Brilliant.:  http:\/\/t.co\/oZREH24YWH",
    "id" : 573750502925602816,
    "created_at" : "2015-03-06 07:42:34 +0000",
    "user" : {
      "name" : "Tim Wu",
      "screen_name" : "Hyperconnection",
      "protected" : false,
      "id_str" : "140509231",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/552747578270310400\/B4vzMjHZ_normal.jpeg",
      "id" : 140509231,
      "verified" : false
    }
  },
  "id" : 573756472431132672,
  "created_at" : "2015-03-06 08:06:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Forrester",
      "screen_name" : "forrester",
      "indices" : [ 3, 13 ],
      "id_str" : "7712452",
      "id" : 7712452
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/forrester\/status\/573551544462884865\/photo\/1",
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/t6366QfVM8",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_WqFxJU8AAg02l.png",
      "id_str" : "573551543628132352",
      "id" : 573551543628132352,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_WqFxJU8AAg02l.png",
      "sizes" : [ {
        "h" : 481,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 576
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/t6366QfVM8"
    } ],
    "hashtags" : [ {
      "text" : "healthinsurance",
      "indices" : [ 56, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/ONHFHyyScg",
      "expanded_url" : "http:\/\/forr.com\/1DQbqHf",
      "display_url" : "forr.com\/1DQbqHf"
    } ]
  },
  "geo" : { },
  "id_str" : "573552030473658368",
  "text" : "RT @forrester: Website, customer service calls dominate #healthinsurance path to purchase. http:\/\/t.co\/ONHFHyyScg http:\/\/t.co\/t6366QfVM8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/forrester\/status\/573551544462884865\/photo\/1",
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/t6366QfVM8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_WqFxJU8AAg02l.png",
        "id_str" : "573551543628132352",
        "id" : 573551543628132352,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_WqFxJU8AAg02l.png",
        "sizes" : [ {
          "h" : 481,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 576
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/t6366QfVM8"
      } ],
      "hashtags" : [ {
        "text" : "healthinsurance",
        "indices" : [ 41, 57 ]
      } ],
      "urls" : [ {
        "indices" : [ 76, 98 ],
        "url" : "http:\/\/t.co\/ONHFHyyScg",
        "expanded_url" : "http:\/\/forr.com\/1DQbqHf",
        "display_url" : "forr.com\/1DQbqHf"
      } ]
    },
    "geo" : { },
    "id_str" : "573551544462884865",
    "text" : "Website, customer service calls dominate #healthinsurance path to purchase. http:\/\/t.co\/ONHFHyyScg http:\/\/t.co\/t6366QfVM8",
    "id" : 573551544462884865,
    "created_at" : "2015-03-05 18:31:58 +0000",
    "user" : {
      "name" : "Forrester",
      "screen_name" : "forrester",
      "protected" : false,
      "id_str" : "7712452",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/557536783252996097\/2mkZ4an0_normal.png",
      "id" : 7712452,
      "verified" : true
    }
  },
  "id" : 573552030473658368,
  "created_at" : "2015-03-05 18:33:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573514146593042433",
  "text" : "Headline: \"Elder spent over 84 hours at A&amp;E\". Would it help to alleviate the situation if there is a dedicate hospital for them?",
  "id" : 573514146593042433,
  "created_at" : "2015-03-05 16:03:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/573462200679014400\/photo\/1",
      "indices" : [ 97, 119 ],
      "url" : "http:\/\/t.co\/mncwntxpJj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_VY1R_WQAAi-AP.jpg",
      "id_str" : "573462199945019392",
      "id" : 573462199945019392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_VY1R_WQAAi-AP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 745,
        "resize" : "fit",
        "w" : 521
      }, {
        "h" : 745,
        "resize" : "fit",
        "w" : 521
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 476
      }, {
        "h" : 745,
        "resize" : "fit",
        "w" : 521
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mncwntxpJj"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573462200679014400",
  "text" : "Personalisation in action. If you are a returning visitors, the content will change accordingly. http:\/\/t.co\/mncwntxpJj",
  "id" : 573462200679014400,
  "created_at" : "2015-03-05 12:36:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Loreto Bello Gude",
      "screen_name" : "loretobgude",
      "indices" : [ 3, 15 ],
      "id_str" : "77040973",
      "id" : 77040973
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/ILM1ig2G9W",
      "expanded_url" : "http:\/\/zite.to\/1EPYhQF",
      "display_url" : "zite.to\/1EPYhQF"
    } ]
  },
  "geo" : { },
  "id_str" : "573460750209052672",
  "text" : "RT @loretobgude: If you speak Mandarin, your brain is different http:\/\/t.co\/ILM1ig2G9W",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 69 ],
        "url" : "http:\/\/t.co\/ILM1ig2G9W",
        "expanded_url" : "http:\/\/zite.to\/1EPYhQF",
        "display_url" : "zite.to\/1EPYhQF"
      } ]
    },
    "geo" : { },
    "id_str" : "570570656565354496",
    "text" : "If you speak Mandarin, your brain is different http:\/\/t.co\/ILM1ig2G9W",
    "id" : 570570656565354496,
    "created_at" : "2015-02-25 13:06:59 +0000",
    "user" : {
      "name" : "Loreto Bello Gude",
      "screen_name" : "loretobgude",
      "protected" : false,
      "id_str" : "77040973",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000330286181\/1ede53f9e760259a4a66734219397e9a_normal.jpeg",
      "id" : 77040973,
      "verified" : false
    }
  },
  "id" : 573460750209052672,
  "created_at" : "2015-03-05 12:31:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 75, 87 ]
    } ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http:\/\/t.co\/1vaSKgSPpY",
      "expanded_url" : "http:\/\/www.getoptimise.com\/",
      "display_url" : "getoptimise.com"
    } ]
  },
  "geo" : { },
  "id_str" : "573456823346413568",
  "text" : "Treemaps reports a new feature in Google Analytics. http:\/\/t.co\/1vaSKgSPpY #getoptimise",
  "id" : 573456823346413568,
  "created_at" : "2015-03-05 12:15:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/8kEoUCpLvY",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/german-vandals-ordered-jailed-caned-singapore-052050381.html",
      "display_url" : "sg.news.yahoo.com\/german-vandals\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "573398466354917376",
  "text" : "9 months in prison and 3 strokes of the cane for vandalism. https:\/\/t.co\/8kEoUCpLvY &lt;That the way it should be.",
  "id" : 573398466354917376,
  "created_at" : "2015-03-05 08:23:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573220882870640640",
  "text" : "Happily applied firmware to the wireless router end up corrupting the existing setup and took 2 hours to fix that.",
  "id" : 573220882870640640,
  "created_at" : "2015-03-04 20:38:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Stanley",
      "screen_name" : "markstanley",
      "indices" : [ 3, 15 ],
      "id_str" : "14132298",
      "id" : 14132298
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "573034568464396289",
  "text" : "RT @markstanley: Looking forward to speaking on a panel today in Dublins Mansion House on the topic 'Purpose beyond Task' as part of Nation\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 53.36257611629634, -6.242611489925416 ]
    },
    "id_str" : "573030659905613824",
    "text" : "Looking forward to speaking on a panel today in Dublins Mansion House on the topic 'Purpose beyond Task' as part of National Employment Week",
    "id" : 573030659905613824,
    "created_at" : "2015-03-04 08:02:10 +0000",
    "user" : {
      "name" : "Mark Stanley",
      "screen_name" : "markstanley",
      "protected" : false,
      "id_str" : "14132298",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/749953968957251585\/KSsphMJ__normal.jpg",
      "id" : 14132298,
      "verified" : false
    }
  },
  "id" : 573034568464396289,
  "created_at" : "2015-03-04 08:17:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http:\/\/t.co\/iHMD6UYi54",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/agribusiness-and-food\/hong-kong-bans-importation-of-raw-oysters-from-donegal-1.2123290#.VPY7-XtzvS8.twitter",
      "display_url" : "irishtimes.com\/business\/agrib\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572893089813155840",
  "text" : "Irish raw oysters banned by Hong Kong authority http:\/\/t.co\/iHMD6UYi54",
  "id" : 572893089813155840,
  "created_at" : "2015-03-03 22:55:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 111, 122 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http:\/\/t.co\/TcQjS4Vmc2",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/lessons-for-state-in-singapore-s-scientific-research-investment-1.2120342#.VPYy1qXTczc.twitter",
      "display_url" : "irishtimes.com\/business\/lesso\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572886660205158400",
  "text" : "RT @GeoffreyIRL: Lessons for Ireland in Singapore\u2019s scientific research investment  http:\/\/t.co\/TcQjS4Vmc2 via @IrishTimes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Irish Times",
        "screen_name" : "IrishTimes",
        "indices" : [ 94, 105 ],
        "id_str" : "15084853",
        "id" : 15084853
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/TcQjS4Vmc2",
        "expanded_url" : "http:\/\/www.irishtimes.com\/business\/lessons-for-state-in-singapore-s-scientific-research-investment-1.2120342#.VPYy1qXTczc.twitter",
        "display_url" : "irishtimes.com\/business\/lesso\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "572884307368259584",
    "text" : "Lessons for Ireland in Singapore\u2019s scientific research investment  http:\/\/t.co\/TcQjS4Vmc2 via @IrishTimes",
    "id" : 572884307368259584,
    "created_at" : "2015-03-03 22:20:37 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 572886660205158400,
  "created_at" : "2015-03-03 22:29:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TalkingSoc",
      "indices" : [ 126, 137 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572884975957250049",
  "text" : "RT @DigiWomenIRL: a5) I don't think all female tech conferences are the answer either. I want a balance at the one conference #TalkingSoc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TalkingSoc",
        "indices" : [ 108, 119 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572877853362012160",
    "text" : "a5) I don't think all female tech conferences are the answer either. I want a balance at the one conference #TalkingSoc",
    "id" : 572877853362012160,
    "created_at" : "2015-03-03 21:54:58 +0000",
    "user" : {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "protected" : false,
      "id_str" : "1221157964",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527003323623145473\/C2yyI2ob_normal.png",
      "id" : 1221157964,
      "verified" : false
    }
  },
  "id" : 572884975957250049,
  "created_at" : "2015-03-03 22:23:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rachel",
      "screen_name" : "rachelrmk",
      "indices" : [ 3, 13 ],
      "id_str" : "257844106",
      "id" : 257844106
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http:\/\/t.co\/0wgBPzUqLG",
      "expanded_url" : "http:\/\/www.mymodernmet.com\/profiles\/blogs\/apple-iphone-6-best-photography",
      "display_url" : "mymodernmet.com\/profiles\/blogs\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572884809917341697",
  "text" : "RT @rachelrmk: Apple's new advertising campaign showcases beautiful iPhone 6 photography they've sourced online http:\/\/t.co\/0wgBPzUqLG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 97, 119 ],
        "url" : "http:\/\/t.co\/0wgBPzUqLG",
        "expanded_url" : "http:\/\/www.mymodernmet.com\/profiles\/blogs\/apple-iphone-6-best-photography",
        "display_url" : "mymodernmet.com\/profiles\/blogs\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "572877935876579328",
    "text" : "Apple's new advertising campaign showcases beautiful iPhone 6 photography they've sourced online http:\/\/t.co\/0wgBPzUqLG",
    "id" : 572877935876579328,
    "created_at" : "2015-03-03 21:55:18 +0000",
    "user" : {
      "name" : "Rachel",
      "screen_name" : "rachelrmk",
      "protected" : false,
      "id_str" : "257844106",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469623404010938368\/InsRVH-Q_normal.jpeg",
      "id" : 257844106,
      "verified" : false
    }
  },
  "id" : 572884809917341697,
  "created_at" : "2015-03-03 22:22:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rebekah Iliff",
      "screen_name" : "rebekahiliff",
      "indices" : [ 41, 54 ],
      "id_str" : "75951347",
      "id" : 75951347
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "social",
      "indices" : [ 29, 36 ]
    }, {
      "text" : "PR",
      "indices" : [ 56, 59 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572884312997175296",
  "text" : "RT @GoGallivanting: Same for #social! MT @rebekahiliff: #PR w\/o measurement is like playing soccer w\/o goalposts. Just a bunch of men runni\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rebekah Iliff",
        "screen_name" : "rebekahiliff",
        "indices" : [ 21, 34 ],
        "id_str" : "75951347",
        "id" : 75951347
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "social",
        "indices" : [ 9, 16 ]
      }, {
        "text" : "PR",
        "indices" : [ 36, 39 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572879548859088901",
    "text" : "Same for #social! MT @rebekahiliff: #PR w\/o measurement is like playing soccer w\/o goalposts. Just a bunch of men running around in shorts.",
    "id" : 572879548859088901,
    "created_at" : "2015-03-03 22:01:42 +0000",
    "user" : {
      "name" : "Jennifer Kolbuc",
      "screen_name" : "JenniferKolbuc",
      "protected" : false,
      "id_str" : "17629488",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/970370674244988928\/4-ngiezh_normal.jpg",
      "id" : 17629488,
      "verified" : false
    }
  },
  "id" : 572884312997175296,
  "created_at" : "2015-03-03 22:20:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "genderbalance",
      "indices" : [ 90, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572884040937824257",
  "text" : "RT @DigiWomenIRL: One last thing event organisers need to get less defensive when lack of #genderbalance is highlight and ask what can they\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "genderbalance",
        "indices" : [ 72, 86 ]
      }, {
        "text" : "TalkingSoc",
        "indices" : [ 125, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572880335928573952",
    "text" : "One last thing event organisers need to get less defensive when lack of #genderbalance is highlight and ask what can they do #TalkingSoc",
    "id" : 572880335928573952,
    "created_at" : "2015-03-03 22:04:50 +0000",
    "user" : {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "protected" : false,
      "id_str" : "1221157964",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527003323623145473\/C2yyI2ob_normal.png",
      "id" : 1221157964,
      "verified" : false
    }
  },
  "id" : 572884040937824257,
  "created_at" : "2015-03-03 22:19:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lorna Sixsmith",
      "screen_name" : "WriteOnTrack_L",
      "indices" : [ 0, 15 ],
      "id_str" : "377288024",
      "id" : 377288024
    }, {
      "name" : "Clare",
      "screen_name" : "TheTBRpile",
      "indices" : [ 16, 27 ],
      "id_str" : "2718995780",
      "id" : 2718995780
    }, {
      "name" : "Jan Carroll",
      "screen_name" : "ThePlanJan",
      "indices" : [ 28, 39 ],
      "id_str" : "859688444",
      "id" : 859688444
    }, {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 40, 53 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "572871132342181888",
  "geo" : { },
  "id_str" : "572882925814853633",
  "in_reply_to_user_id" : 377288024,
  "text" : "@WriteOnTrack_L @TheTBRpile @ThePlanJan @DigiWomenIRL I attended last year event and I agreed with Lorna observation.",
  "id" : 572882925814853633,
  "in_reply_to_status_id" : 572871132342181888,
  "created_at" : "2015-03-03 22:15:07 +0000",
  "in_reply_to_screen_name" : "WriteOnTrack_L",
  "in_reply_to_user_id_str" : "377288024",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "weaselpecker",
      "indices" : [ 89, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 104, 126 ],
      "url" : "http:\/\/t.co\/3WjIKeyYWW",
      "expanded_url" : "http:\/\/str.sg\/oXX",
      "display_url" : "str.sg\/oXX"
    } ]
  },
  "geo" : { },
  "id_str" : "572784232050900992",
  "text" : "RT @STcom: Incredible photo of weasel riding on woodpecker's back spawns hilarious memes #weaselpecker  http:\/\/t.co\/3WjIKeyYWW http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/572770337001111552\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/Rjt8AJ8jHZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_LjkSiUoAAh0_N.jpg",
        "id_str" : "572770315220066304",
        "id" : 572770315220066304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_LjkSiUoAAh0_N.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 581
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 581
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 581
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 581
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Rjt8AJ8jHZ"
      } ],
      "hashtags" : [ {
        "text" : "weaselpecker",
        "indices" : [ 78, 91 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/3WjIKeyYWW",
        "expanded_url" : "http:\/\/str.sg\/oXX",
        "display_url" : "str.sg\/oXX"
      } ]
    },
    "geo" : { },
    "id_str" : "572770337001111552",
    "text" : "Incredible photo of weasel riding on woodpecker's back spawns hilarious memes #weaselpecker  http:\/\/t.co\/3WjIKeyYWW http:\/\/t.co\/Rjt8AJ8jHZ",
    "id" : 572770337001111552,
    "created_at" : "2015-03-03 14:47:44 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 572784232050900992,
  "created_at" : "2015-03-03 15:42:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "datapine",
      "screen_name" : "datapine",
      "indices" : [ 3, 12 ],
      "id_str" : "1446344904",
      "id" : 1446344904
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dataviz",
      "indices" : [ 110, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/S89jNm7Uip",
      "expanded_url" : "http:\/\/buff.ly\/1wOrOFS",
      "display_url" : "buff.ly\/1wOrOFS"
    } ]
  },
  "geo" : { },
  "id_str" : "572764627655913472",
  "text" : "RT @datapine: We\u2019re easily fooled by data visualizations. Here's how to spot the B.S . http:\/\/t.co\/S89jNm7Uip #dataviz http:\/\/t.co\/1i99PPF4\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/datapine\/status\/572731927377149952\/photo\/1",
        "indices" : [ 105, 127 ],
        "url" : "http:\/\/t.co\/1i99PPF4eB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_LAp0gXAAAu6sw.jpg",
        "id_str" : "572731927331012608",
        "id" : 572731927331012608,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_LAp0gXAAAu6sw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 384,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 384,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 384,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 384,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1i99PPF4eB"
      } ],
      "hashtags" : [ {
        "text" : "dataviz",
        "indices" : [ 96, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/S89jNm7Uip",
        "expanded_url" : "http:\/\/buff.ly\/1wOrOFS",
        "display_url" : "buff.ly\/1wOrOFS"
      } ]
    },
    "geo" : { },
    "id_str" : "572731927377149952",
    "text" : "We\u2019re easily fooled by data visualizations. Here's how to spot the B.S . http:\/\/t.co\/S89jNm7Uip #dataviz http:\/\/t.co\/1i99PPF4eB",
    "id" : 572731927377149952,
    "created_at" : "2015-03-03 12:15:06 +0000",
    "user" : {
      "name" : "datapine",
      "screen_name" : "datapine",
      "protected" : false,
      "id_str" : "1446344904",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3703174210\/217a65791863500bdb475542e3c11eb2_normal.jpeg",
      "id" : 1446344904,
      "verified" : false
    }
  },
  "id" : 572764627655913472,
  "created_at" : "2015-03-03 14:25:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/572738532642578433\/photo\/1",
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/2eL0mXDVWK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_LGk0pW8AAlOzl.jpg",
      "id_str" : "572738438539177984",
      "id" : 572738438539177984,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_LGk0pW8AAlOzl.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2eL0mXDVWK"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/572738532642578433\/photo\/1",
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/2eL0mXDVWK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_LGpWKWwAEUxZM.jpg",
      "id_str" : "572738516255424513",
      "id" : 572738516255424513,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_LGpWKWwAEUxZM.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2eL0mXDVWK"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572738532642578433",
  "text" : "At a local library. Book from the founders of 37 signals. http:\/\/t.co\/2eL0mXDVWK",
  "id" : 572738532642578433,
  "created_at" : "2015-03-03 12:41:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dublinese \u6708\u5149",
      "screen_name" : "rachel_violin",
      "indices" : [ 0, 14 ],
      "id_str" : "116755889",
      "id" : 116755889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "572540061843116032",
  "geo" : { },
  "id_str" : "572713511115218944",
  "in_reply_to_user_id" : 116755889,
  "text" : "@rachel_violin No.2 take the biscuit",
  "id" : 572713511115218944,
  "in_reply_to_status_id" : 572540061843116032,
  "created_at" : "2015-03-03 11:01:56 +0000",
  "in_reply_to_screen_name" : "rachel_violin",
  "in_reply_to_user_id_str" : "116755889",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http:\/\/t.co\/76sfrn38Yc",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-31689124?OCID=fbasia",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572708579637141504",
  "text" : "IT the cost of car ownership that pushed Singapore up the league table as the world's most expensive city. http:\/\/t.co\/76sfrn38Yc",
  "id" : 572708579637141504,
  "created_at" : "2015-03-03 10:42:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tex_n_Singapore",
      "screen_name" : "Tex_n_Singapore",
      "indices" : [ 3, 19 ],
      "id_str" : "2651067600",
      "id" : 2651067600
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 93 ],
      "url" : "http:\/\/t.co\/WnbVrBWtxj",
      "expanded_url" : "http:\/\/ift.tt\/1vXekMO",
      "display_url" : "ift.tt\/1vXekMO"
    } ]
  },
  "geo" : { },
  "id_str" : "572700621444386816",
  "text" : "RT @Tex_n_Singapore: Delhi rapist blames victim for being out at night http:\/\/t.co\/WnbVrBWtxj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 72 ],
        "url" : "http:\/\/t.co\/WnbVrBWtxj",
        "expanded_url" : "http:\/\/ift.tt\/1vXekMO",
        "display_url" : "ift.tt\/1vXekMO"
      } ]
    },
    "geo" : { },
    "id_str" : "572699703927820288",
    "text" : "Delhi rapist blames victim for being out at night http:\/\/t.co\/WnbVrBWtxj",
    "id" : 572699703927820288,
    "created_at" : "2015-03-03 10:07:04 +0000",
    "user" : {
      "name" : "Tex_n_Singapore",
      "screen_name" : "Tex_n_Singapore",
      "protected" : false,
      "id_str" : "2651067600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/489405892517830657\/5_NmWxaN_normal.jpeg",
      "id" : 2651067600,
      "verified" : false
    }
  },
  "id" : 572700621444386816,
  "created_at" : "2015-03-03 10:10:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572696217538461696",
  "text" : "Irish historical drama Viking is now showing on Singapore Cable TV channel.",
  "id" : 572696217538461696,
  "created_at" : "2015-03-03 09:53:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 62 ],
      "url" : "http:\/\/t.co\/TguHvZta01",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b054f7qp",
      "display_url" : "bbc.co.uk\/programmes\/b05\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572540510860140545",
  "text" : "Olympus fraud scandal on BBC TV tonight http:\/\/t.co\/TguHvZta01 is more interesting than the climate change data story.",
  "id" : 572540510860140545,
  "created_at" : "2015-03-02 23:34:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hetan Shah",
      "screen_name" : "HetanShah",
      "indices" : [ 3, 13 ],
      "id_str" : "123220773",
      "id" : 123220773
    }, {
      "name" : "Royal Statistical Society",
      "screen_name" : "RoyalStatSoc",
      "indices" : [ 93, 106 ],
      "id_str" : "124176132",
      "id" : 124176132
    }, {
      "name" : "David Spiegelhalter",
      "screen_name" : "d_spiegel",
      "indices" : [ 114, 124 ],
      "id_str" : "76956597",
      "id" : 76956597
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "climatechange",
      "indices" : [ 49, 63 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572506755562901504",
  "text" : "RT @HetanShah: Want to understand the numbers of #climatechange? Watch 9pm BBC4 tonight with @RoyalStatSoc fellow @d_spiegel - http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Royal Statistical Society",
        "screen_name" : "RoyalStatSoc",
        "indices" : [ 78, 91 ],
        "id_str" : "124176132",
        "id" : 124176132
      }, {
        "name" : "David Spiegelhalter",
        "screen_name" : "d_spiegel",
        "indices" : [ 99, 109 ],
        "id_str" : "76956597",
        "id" : 76956597
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "climatechange",
        "indices" : [ 34, 48 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/Xm0p5K1kav",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/p02jsdrk",
        "display_url" : "bbc.co.uk\/programmes\/p02\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "572342600734253056",
    "text" : "Want to understand the numbers of #climatechange? Watch 9pm BBC4 tonight with @RoyalStatSoc fellow @d_spiegel - http:\/\/t.co\/Xm0p5K1kav",
    "id" : 572342600734253056,
    "created_at" : "2015-03-02 10:28:04 +0000",
    "user" : {
      "name" : "Hetan Shah",
      "screen_name" : "HetanShah",
      "protected" : false,
      "id_str" : "123220773",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/575963450045677569\/mY4H0cfL_normal.jpeg",
      "id" : 123220773,
      "verified" : false
    }
  },
  "id" : 572506755562901504,
  "created_at" : "2015-03-02 21:20:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Newsweek",
      "screen_name" : "Newsweek",
      "indices" : [ 3, 12 ],
      "id_str" : "2884771",
      "id" : 2884771
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NewsweekRewind",
      "indices" : [ 107, 122 ]
    } ],
    "urls" : [ {
      "indices" : [ 83, 105 ],
      "url" : "http:\/\/t.co\/ap8vtRnpIZ",
      "expanded_url" : "http:\/\/bit.ly\/1E0iK6P",
      "display_url" : "bit.ly\/1E0iK6P"
    } ]
  },
  "geo" : { },
  "id_str" : "572478260090675200",
  "text" : "RT @Newsweek: 20 years ago this week, Newsweek published the wrongest column ever. http:\/\/t.co\/ap8vtRnpIZ  #NewsweekRewind http:\/\/t.co\/8LHA\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Newsweek\/status\/571768557358358528\/photo\/1",
        "indices" : [ 109, 131 ],
        "url" : "http:\/\/t.co\/8LHAKpw4rE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B-9UeTKVEAAWJjA.png",
        "id_str" : "571768557215748096",
        "id" : 571768557215748096,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B-9UeTKVEAAWJjA.png",
        "sizes" : [ {
          "h" : 312,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 990
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 990
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 990
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8LHAKpw4rE"
      } ],
      "hashtags" : [ {
        "text" : "NewsweekRewind",
        "indices" : [ 93, 108 ]
      } ],
      "urls" : [ {
        "indices" : [ 69, 91 ],
        "url" : "http:\/\/t.co\/ap8vtRnpIZ",
        "expanded_url" : "http:\/\/bit.ly\/1E0iK6P",
        "display_url" : "bit.ly\/1E0iK6P"
      } ]
    },
    "geo" : { },
    "id_str" : "571768557358358528",
    "text" : "20 years ago this week, Newsweek published the wrongest column ever. http:\/\/t.co\/ap8vtRnpIZ  #NewsweekRewind http:\/\/t.co\/8LHAKpw4rE",
    "id" : 571768557358358528,
    "created_at" : "2015-02-28 20:27:01 +0000",
    "user" : {
      "name" : "Newsweek",
      "screen_name" : "Newsweek",
      "protected" : false,
      "id_str" : "2884771",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/741603495929905152\/di0NxkFa_normal.jpg",
      "id" : 2884771,
      "verified" : true
    }
  },
  "id" : 572478260090675200,
  "created_at" : "2015-03-02 19:27:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cosgrove Gaynard Solicitors",
      "screen_name" : "CosgroveGaynard",
      "indices" : [ 3, 19 ],
      "id_str" : "712072753",
      "id" : 712072753
    }, {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "indices" : [ 99, 113 ],
      "id_str" : "150246405",
      "id" : 150246405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/WWsu4aydwv",
      "expanded_url" : "http:\/\/jrnl.ie\/1960351",
      "display_url" : "jrnl.ie\/1960351"
    } ]
  },
  "geo" : { },
  "id_str" : "572473451648307200",
  "text" : "RT @CosgroveGaynard: Protecting yourself with pepper spray is illegal and that\u2019s not changing (via @thejournal_ie) http:\/\/t.co\/WWsu4aydwv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TheJournal.ie",
        "screen_name" : "thejournal_ie",
        "indices" : [ 78, 92 ],
        "id_str" : "150246405",
        "id" : 150246405
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 116 ],
        "url" : "http:\/\/t.co\/WWsu4aydwv",
        "expanded_url" : "http:\/\/jrnl.ie\/1960351",
        "display_url" : "jrnl.ie\/1960351"
      } ]
    },
    "geo" : { },
    "id_str" : "572462265053675521",
    "text" : "Protecting yourself with pepper spray is illegal and that\u2019s not changing (via @thejournal_ie) http:\/\/t.co\/WWsu4aydwv",
    "id" : 572462265053675521,
    "created_at" : "2015-03-02 18:23:34 +0000",
    "user" : {
      "name" : "Cosgrove Gaynard Solicitors",
      "screen_name" : "CosgroveGaynard",
      "protected" : false,
      "id_str" : "712072753",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000082815379\/a1326700247b71fb5c9edbf1a186fde0_normal.jpeg",
      "id" : 712072753,
      "verified" : false
    }
  },
  "id" : 572473451648307200,
  "created_at" : "2015-03-02 19:08:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "See @ruta_io",
      "screen_name" : "rutadanyte",
      "indices" : [ 3, 14 ],
      "id_str" : "775291101280399360",
      "id" : 775291101280399360
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "g",
      "indices" : [ 89, 91 ]
    }, {
      "text" : "socialmedia",
      "indices" : [ 93, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 66, 88 ],
      "url" : "http:\/\/t.co\/EUriGw67Uq",
      "expanded_url" : "http:\/\/bit.ly\/17L48vB",
      "display_url" : "bit.ly\/17L48vB"
    } ]
  },
  "geo" : { },
  "id_str" : "572471105249779712",
  "text" : "RT @rutadanyte: Google+ officially splits into Photos and Streams http:\/\/t.co\/EUriGw67Uq #g+ #socialmedia",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "g",
        "indices" : [ 73, 75 ]
      }, {
        "text" : "socialmedia",
        "indices" : [ 77, 89 ]
      } ],
      "urls" : [ {
        "indices" : [ 50, 72 ],
        "url" : "http:\/\/t.co\/EUriGw67Uq",
        "expanded_url" : "http:\/\/bit.ly\/17L48vB",
        "display_url" : "bit.ly\/17L48vB"
      } ]
    },
    "geo" : { },
    "id_str" : "572470995849764864",
    "text" : "Google+ officially splits into Photos and Streams http:\/\/t.co\/EUriGw67Uq #g+ #socialmedia",
    "id" : 572470995849764864,
    "created_at" : "2015-03-02 18:58:16 +0000",
    "user" : {
      "name" : "Ruta",
      "screen_name" : "ruta_io",
      "protected" : false,
      "id_str" : "249794996",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/976495387145105408\/f3GWeuOg_normal.jpg",
      "id" : 249794996,
      "verified" : false
    }
  },
  "id" : 572471105249779712,
  "created_at" : "2015-03-02 18:58:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572445833867735040",
  "text" : "I have 4 recruiters that approach me on this position that was advertised on Linkedin 2 months ago.",
  "id" : 572445833867735040,
  "created_at" : "2015-03-02 17:18:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Magee",
      "screen_name" : "ton_magee",
      "indices" : [ 3, 13 ],
      "id_str" : "429210913",
      "id" : 429210913
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 128 ],
      "url" : "http:\/\/t.co\/IUdlJ8l1as",
      "expanded_url" : "http:\/\/goo.gl\/6Kdxuy",
      "display_url" : "goo.gl\/6Kdxuy"
    } ]
  },
  "geo" : { },
  "id_str" : "572438720223289344",
  "text" : "RT @ton_magee: Fantastic opportunity join Argos Digital Transformation, get in touch if you're interested http:\/\/t.co\/IUdlJ8l1as",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/IUdlJ8l1as",
        "expanded_url" : "http:\/\/goo.gl\/6Kdxuy",
        "display_url" : "goo.gl\/6Kdxuy"
      } ]
    },
    "geo" : { },
    "id_str" : "571805259007455232",
    "text" : "Fantastic opportunity join Argos Digital Transformation, get in touch if you're interested http:\/\/t.co\/IUdlJ8l1as",
    "id" : 571805259007455232,
    "created_at" : "2015-02-28 22:52:52 +0000",
    "user" : {
      "name" : "Anthony Magee",
      "screen_name" : "ton_magee",
      "protected" : false,
      "id_str" : "429210913",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/984916900600725506\/Ukax-w_0_normal.jpg",
      "id" : 429210913,
      "verified" : false
    }
  },
  "id" : 572438720223289344,
  "created_at" : "2015-03-02 16:50:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/kNyDFfMpWE",
      "expanded_url" : "https:\/\/support.google.com\/analytics\/answer\/2731565?hl=en",
      "display_url" : "support.google.com\/analytics\/answ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572422464132780032",
  "text" : "\"sessions don\u2019t convert, users do.\" Let discuss https:\/\/t.co\/kNyDFfMpWE",
  "id" : 572422464132780032,
  "created_at" : "2015-03-02 15:45:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dominic Coyle",
      "screen_name" : "ITdominiccoyle",
      "indices" : [ 3, 18 ],
      "id_str" : "380945140",
      "id" : 380945140
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ITdominiccoyle\/status\/572383899801808896\/photo\/1",
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/QyCkA2HMqf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_GEH8xXEAA6W2U.jpg",
      "id_str" : "572383899759874048",
      "id" : 572383899759874048,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_GEH8xXEAA6W2U.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QyCkA2HMqf"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/JJh1vrUqpi",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/media-and-marketing\/rebekah-brooks-set-to-run-dublin-based-storyful-1.2122888",
      "display_url" : "irishtimes.com\/business\/media\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572384191922495489",
  "text" : "RT @ITdominiccoyle: Rebekah Brooks set to run Dublin-based Storyful\nvia The Irish Times\nhttp:\/\/t.co\/JJh1vrUqpi http:\/\/t.co\/QyCkA2HMqf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ITdominiccoyle\/status\/572383899801808896\/photo\/1",
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/QyCkA2HMqf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_GEH8xXEAA6W2U.jpg",
        "id_str" : "572383899759874048",
        "id" : 572383899759874048,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_GEH8xXEAA6W2U.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QyCkA2HMqf"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 90 ],
        "url" : "http:\/\/t.co\/JJh1vrUqpi",
        "expanded_url" : "http:\/\/www.irishtimes.com\/business\/media-and-marketing\/rebekah-brooks-set-to-run-dublin-based-storyful-1.2122888",
        "display_url" : "irishtimes.com\/business\/media\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "572383899801808896",
    "text" : "Rebekah Brooks set to run Dublin-based Storyful\nvia The Irish Times\nhttp:\/\/t.co\/JJh1vrUqpi http:\/\/t.co\/QyCkA2HMqf",
    "id" : 572383899801808896,
    "created_at" : "2015-03-02 13:12:10 +0000",
    "user" : {
      "name" : "Dominic Coyle",
      "screen_name" : "ITdominiccoyle",
      "protected" : false,
      "id_str" : "380945140",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1562408149\/byline_pic-_2008_1638708_normal.jpg",
      "id" : 380945140,
      "verified" : true
    }
  },
  "id" : 572384191922495489,
  "created_at" : "2015-03-02 13:13:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cindy Gallop",
      "screen_name" : "cindygallop",
      "indices" : [ 3, 15 ],
      "id_str" : "8622212",
      "id" : 8622212
    }, {
      "name" : "SiliconRepublic",
      "screen_name" : "siliconrepublic",
      "indices" : [ 101, 117 ],
      "id_str" : "14385329",
      "id" : 14385329
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572384042886307841",
  "text" : "RT @cindygallop: Hello Dublin! Attend a tech conference w 20 women speakers 3 men! Register, join me @siliconrepublic http:\/\/t.co\/e2sRvDIe3\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SiliconRepublic",
        "screen_name" : "siliconrepublic",
        "indices" : [ 84, 100 ],
        "id_str" : "14385329",
        "id" : 14385329
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "inspirefest2015",
        "indices" : [ 124, 140 ]
      } ],
      "urls" : [ {
        "indices" : [ 101, 123 ],
        "url" : "http:\/\/t.co\/e2sRvDIe37",
        "expanded_url" : "http:\/\/inspirefest2015.com\/",
        "display_url" : "inspirefest2015.com"
      } ]
    },
    "geo" : { },
    "id_str" : "569885112646680576",
    "text" : "Hello Dublin! Attend a tech conference w 20 women speakers 3 men! Register, join me @siliconrepublic http:\/\/t.co\/e2sRvDIe37 #inspirefest2015",
    "id" : 569885112646680576,
    "created_at" : "2015-02-23 15:42:53 +0000",
    "user" : {
      "name" : "Cindy Gallop",
      "screen_name" : "cindygallop",
      "protected" : false,
      "id_str" : "8622212",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/969834322596384768\/WXumpDpK_normal.jpg",
      "id" : 8622212,
      "verified" : true
    }
  },
  "id" : 572384042886307841,
  "created_at" : "2015-03-02 13:12:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "why vo",
      "screen_name" : "why_vo",
      "indices" : [ 12, 19 ],
      "id_str" : "725367012533264384",
      "id" : 725367012533264384
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "572373530286796800",
  "geo" : { },
  "id_str" : "572374181481979905",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG @why_vo  feminism is about equality",
  "id" : 572374181481979905,
  "in_reply_to_status_id" : 572373530286796800,
  "created_at" : "2015-03-02 12:33:33 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pc Clean",
      "screen_name" : "PcCleanComputer",
      "indices" : [ 3, 19 ],
      "id_str" : "117805943",
      "id" : 117805943
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572373906633457664",
  "text" : "RT @PcCleanComputer: O2 customers to get 4G from June as Three rebrand completes: It\u2019s the end of an era for 1.5 million O2 custome... http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterfeed.com\" rel=\"nofollow\"\u003Etwitterfeed\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/SsJsatBNeb",
        "expanded_url" : "http:\/\/bit.ly\/1E9IhKK",
        "display_url" : "bit.ly\/1E9IhKK"
      } ]
    },
    "geo" : { },
    "id_str" : "572372826369363969",
    "text" : "O2 customers to get 4G from June as Three rebrand completes: It\u2019s the end of an era for 1.5 million O2 custome... http:\/\/t.co\/SsJsatBNeb",
    "id" : 572372826369363969,
    "created_at" : "2015-03-02 12:28:10 +0000",
    "user" : {
      "name" : "Pc Clean",
      "screen_name" : "PcCleanComputer",
      "protected" : false,
      "id_str" : "117805943",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2928854925\/b6944b18e418df93f7befbdbc22db8eb_normal.jpeg",
      "id" : 117805943,
      "verified" : false
    }
  },
  "id" : 572373906633457664,
  "created_at" : "2015-03-02 12:32:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 0, 12 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "572367734861922304",
  "geo" : { },
  "id_str" : "572370109739749377",
  "in_reply_to_user_id" : 18721044,
  "text" : "@klillington Their interface really needs improvement.",
  "id" : 572370109739749377,
  "in_reply_to_status_id" : 572367734861922304,
  "created_at" : "2015-03-02 12:17:22 +0000",
  "in_reply_to_screen_name" : "klillington",
  "in_reply_to_user_id_str" : "18721044",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Fleisher",
      "screen_name" : "lisafleisher",
      "indices" : [ 3, 16 ],
      "id_str" : "15119529",
      "id" : 15119529
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/lisafleisher\/status\/572355346787934211\/photo\/1",
      "indices" : [ 47, 69 ],
      "url" : "http:\/\/t.co\/Yqwx3Jxuy1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_FqJWlW8AEIqtT.jpg",
      "id_str" : "572355336566403073",
      "id" : 572355336566403073,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_FqJWlW8AEIqtT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 674,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 674,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 674,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 448,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Yqwx3Jxuy1"
    } ],
    "hashtags" : [ {
      "text" : "MWC15",
      "indices" : [ 34, 40 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572357721409908736",
  "text" : "RT @lisafleisher: My \u20AC17 lunch at #MWC15. Ffs. http:\/\/t.co\/Yqwx3Jxuy1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lisafleisher\/status\/572355346787934211\/photo\/1",
        "indices" : [ 29, 51 ],
        "url" : "http:\/\/t.co\/Yqwx3Jxuy1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_FqJWlW8AEIqtT.jpg",
        "id_str" : "572355336566403073",
        "id" : 572355336566403073,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_FqJWlW8AEIqtT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 448,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Yqwx3Jxuy1"
      } ],
      "hashtags" : [ {
        "text" : "MWC15",
        "indices" : [ 16, 22 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572355346787934211",
    "text" : "My \u20AC17 lunch at #MWC15. Ffs. http:\/\/t.co\/Yqwx3Jxuy1",
    "id" : 572355346787934211,
    "created_at" : "2015-03-02 11:18:43 +0000",
    "user" : {
      "name" : "Lisa Fleisher",
      "screen_name" : "lisafleisher",
      "protected" : false,
      "id_str" : "15119529",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023995166544277505\/4sXKW2kb_normal.jpg",
      "id" : 15119529,
      "verified" : true
    }
  },
  "id" : 572357721409908736,
  "created_at" : "2015-03-02 11:28:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "India Today",
      "screen_name" : "IndiaToday",
      "indices" : [ 3, 14 ],
      "id_str" : "19897138",
      "id" : 19897138
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IndiaToday\/status\/572329053761507328\/photo\/1",
      "indices" : [ 81, 103 ],
      "url" : "http:\/\/t.co\/MFKskxl6Rx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_FSPX6U0AA3hMK.jpg",
      "id_str" : "572329051722928128",
      "id" : 572329051722928128,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_FSPX6U0AA3hMK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 650
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 650
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 650
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 650
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MFKskxl6Rx"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/hbax9z6Pig",
      "expanded_url" : "http:\/\/indiatoday.intoday.in\/story\/pm-modi-takes-lunch-in-parliament-canteen\/1\/421816.html",
      "display_url" : "indiatoday.intoday.in\/story\/pm-modi-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572346275384967168",
  "text" : "RT @IndiaToday: PM Modi takes lunch in Parliament canteen http:\/\/t.co\/hbax9z6Pig http:\/\/t.co\/MFKskxl6Rx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IndiaToday\/status\/572329053761507328\/photo\/1",
        "indices" : [ 65, 87 ],
        "url" : "http:\/\/t.co\/MFKskxl6Rx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_FSPX6U0AA3hMK.jpg",
        "id_str" : "572329051722928128",
        "id" : 572329051722928128,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_FSPX6U0AA3hMK.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 650
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 650
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 650
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 650
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MFKskxl6Rx"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 42, 64 ],
        "url" : "http:\/\/t.co\/hbax9z6Pig",
        "expanded_url" : "http:\/\/indiatoday.intoday.in\/story\/pm-modi-takes-lunch-in-parliament-canteen\/1\/421816.html",
        "display_url" : "indiatoday.intoday.in\/story\/pm-modi-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "572329053761507328",
    "text" : "PM Modi takes lunch in Parliament canteen http:\/\/t.co\/hbax9z6Pig http:\/\/t.co\/MFKskxl6Rx",
    "id" : 572329053761507328,
    "created_at" : "2015-03-02 09:34:14 +0000",
    "user" : {
      "name" : "India Today",
      "screen_name" : "IndiaToday",
      "protected" : false,
      "id_str" : "19897138",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/787569535880531968\/3FtQQHyA_normal.jpg",
      "id" : 19897138,
      "verified" : true
    }
  },
  "id" : 572346275384967168,
  "created_at" : "2015-03-02 10:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572311420232179712",
  "text" : "To those getting thier A level result, I hope you get into the college, polytechnic your parents have in mind. They are always right.",
  "id" : 572311420232179712,
  "created_at" : "2015-03-02 08:24:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoodLuck",
      "indices" : [ 109, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/X23z2pHnNk",
      "expanded_url" : "http:\/\/cna.asia\/1vQ7clf",
      "display_url" : "cna.asia\/1vQ7clf"
    } ]
  },
  "geo" : { },
  "id_str" : "572310638812377088",
  "text" : "RT @ChannelNewsAsia: The 2014 'A'-Level results are being released to candidates now http:\/\/t.co\/X23z2pHnNk  #GoodLuck http:\/\/t.co\/RnS2gp1q\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/572283508867010560\/photo\/1",
        "indices" : [ 98, 120 ],
        "url" : "http:\/\/t.co\/RnS2gp1q11",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_Eou3zUwAIdgL6.jpg",
        "id_str" : "572283413371076610",
        "id" : 572283413371076610,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_Eou3zUwAIdgL6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1494,
          "resize" : "fit",
          "w" : 2656
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RnS2gp1q11"
      } ],
      "hashtags" : [ {
        "text" : "GoodLuck",
        "indices" : [ 88, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http:\/\/t.co\/X23z2pHnNk",
        "expanded_url" : "http:\/\/cna.asia\/1vQ7clf",
        "display_url" : "cna.asia\/1vQ7clf"
      } ]
    },
    "geo" : { },
    "id_str" : "572283508867010560",
    "text" : "The 2014 'A'-Level results are being released to candidates now http:\/\/t.co\/X23z2pHnNk  #GoodLuck http:\/\/t.co\/RnS2gp1q11",
    "id" : 572283508867010560,
    "created_at" : "2015-03-02 06:33:15 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 572310638812377088,
  "created_at" : "2015-03-02 08:21:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572310445027147776",
  "text" : "It Monday. Bring it on!",
  "id" : 572310445027147776,
  "created_at" : "2015-03-02 08:20:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 140 ],
      "url" : "http:\/\/t.co\/jKGUze11BV",
      "expanded_url" : "http:\/\/www.rackspace.com\/content\/2015\/02\/20\/one-image-to-rule-them-all-size-specs-to-work-across-social-media\/",
      "display_url" : "rackspace.com\/content\/2015\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "572143502911537154",
  "text" : "Post to FB, Twitter, Google+ &amp; LinkedIn? 1600 by 800 pixels with 160-pixel padding left &amp; right margins works http:\/\/t.co\/jKGUze11BV",
  "id" : 572143502911537154,
  "created_at" : "2015-03-01 21:16:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "See @ruta_io",
      "screen_name" : "rutadanyte",
      "indices" : [ 0, 11 ],
      "id_str" : "775291101280399360",
      "id" : 775291101280399360
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "572136590446866433",
  "geo" : { },
  "id_str" : "572136876779421696",
  "in_reply_to_user_id" : 249794996,
  "text" : "@rutadanyte Thanks. I will try that.",
  "id" : 572136876779421696,
  "in_reply_to_status_id" : 572136590446866433,
  "created_at" : "2015-03-01 20:50:35 +0000",
  "in_reply_to_screen_name" : "ruta_io",
  "in_reply_to_user_id_str" : "249794996",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "See @ruta_io",
      "screen_name" : "rutadanyte",
      "indices" : [ 3, 14 ],
      "id_str" : "775291101280399360",
      "id" : 775291101280399360
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 16, 22 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572136652430299138",
  "text" : "RT @rutadanyte: @mryap why not Evernote + Trello through Zappier?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "572127104302764032",
    "geo" : { },
    "id_str" : "572136590446866433",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap why not Evernote + Trello through Zappier?",
    "id" : 572136590446866433,
    "in_reply_to_status_id" : 572127104302764032,
    "created_at" : "2015-03-01 20:49:27 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Ruta",
      "screen_name" : "ruta_io",
      "protected" : false,
      "id_str" : "249794996",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/976495387145105408\/f3GWeuOg_normal.jpg",
      "id" : 249794996,
      "verified" : false
    }
  },
  "id" : 572136652430299138,
  "created_at" : "2015-03-01 20:49:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http:\/\/t.co\/sMQG2gjFoK",
      "expanded_url" : "http:\/\/www.kanbanote.com\/",
      "display_url" : "kanbanote.com"
    } ]
  },
  "geo" : { },
  "id_str" : "572127104302764032",
  "text" : "Turn your Evernote into Kanban Boards. http:\/\/t.co\/sMQG2gjFoK",
  "id" : 572127104302764032,
  "created_at" : "2015-03-01 20:11:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Meng Lye, Liu",
      "screen_name" : "menglye",
      "indices" : [ 3, 11 ],
      "id_str" : "140078516",
      "id" : 140078516
    }, {
      "name" : "Samsung Malaysia",
      "screen_name" : "SamsungMalaysia",
      "indices" : [ 109, 125 ],
      "id_str" : "456289652",
      "id" : 456289652
    }, {
      "name" : "Samsung Singapore",
      "screen_name" : "SamsungSG",
      "indices" : [ 126, 136 ],
      "id_str" : "16463201",
      "id" : 16463201
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Unpacked",
      "indices" : [ 69, 78 ]
    }, {
      "text" : "TheNextGalaxy",
      "indices" : [ 79, 93 ]
    }, {
      "text" : "SamsungMobile",
      "indices" : [ 94, 108 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http:\/\/t.co\/3Luck17exs",
      "expanded_url" : "http:\/\/bit.ly\/unpacked2015",
      "display_url" : "bit.ly\/unpacked2015"
    } ]
  },
  "geo" : { },
  "id_str" : "572092122096148481",
  "text" : "RT @menglye: It's time. Find out What's Next. http:\/\/t.co\/3Luck17exs #Unpacked #TheNextGalaxy #SamsungMobile @SamsungMalaysia @SamsungSg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Samsung Malaysia",
        "screen_name" : "SamsungMalaysia",
        "indices" : [ 96, 112 ],
        "id_str" : "456289652",
        "id" : 456289652
      }, {
        "name" : "Samsung Singapore",
        "screen_name" : "SamsungSG",
        "indices" : [ 113, 123 ],
        "id_str" : "16463201",
        "id" : 16463201
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Unpacked",
        "indices" : [ 56, 65 ]
      }, {
        "text" : "TheNextGalaxy",
        "indices" : [ 66, 80 ]
      }, {
        "text" : "SamsungMobile",
        "indices" : [ 81, 95 ]
      } ],
      "urls" : [ {
        "indices" : [ 33, 55 ],
        "url" : "http:\/\/t.co\/3Luck17exs",
        "expanded_url" : "http:\/\/bit.ly\/unpacked2015",
        "display_url" : "bit.ly\/unpacked2015"
      } ]
    },
    "geo" : { },
    "id_str" : "572089303263666176",
    "text" : "It's time. Find out What's Next. http:\/\/t.co\/3Luck17exs #Unpacked #TheNextGalaxy #SamsungMobile @SamsungMalaysia @SamsungSg",
    "id" : 572089303263666176,
    "created_at" : "2015-03-01 17:41:33 +0000",
    "user" : {
      "name" : "Meng Lye, Liu",
      "screen_name" : "menglye",
      "protected" : false,
      "id_str" : "140078516",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/511314413161156608\/ZJbhFuGd_normal.jpeg",
      "id" : 140078516,
      "verified" : false
    }
  },
  "id" : 572092122096148481,
  "created_at" : "2015-03-01 17:52:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "V",
      "screen_name" : "ThatFabulist",
      "indices" : [ 3, 16 ],
      "id_str" : "51500165",
      "id" : 51500165
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572061452938444800",
  "text" : "RT @ThatFabulist: Tried the local police station who suggested that if I'm being harassed on the streets, I should go home.\nPure gold, that\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "572049394914091008",
    "geo" : { },
    "id_str" : "572049699982581761",
    "in_reply_to_user_id" : 51500165,
    "text" : "Tried the local police station who suggested that if I'm being harassed on the streets, I should go home.\nPure gold, that one.",
    "id" : 572049699982581761,
    "in_reply_to_status_id" : 572049394914091008,
    "created_at" : "2015-03-01 15:04:11 +0000",
    "in_reply_to_screen_name" : "ThatFabulist",
    "in_reply_to_user_id_str" : "51500165",
    "user" : {
      "name" : "V",
      "screen_name" : "ThatFabulist",
      "protected" : false,
      "id_str" : "51500165",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005747301900431360\/C4hn4x1Y_normal.jpg",
      "id" : 51500165,
      "verified" : false
    }
  },
  "id" : 572061452938444800,
  "created_at" : "2015-03-01 15:50:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572056867691732992",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG  Any unusual SG50 related promotion you come across?",
  "id" : 572056867691732992,
  "created_at" : "2015-03-01 15:32:40 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572045981564461056",
  "text" : "what colour are these jersey in the changing room?",
  "id" : 572045981564461056,
  "created_at" : "2015-03-01 14:49:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Cruddas",
      "screen_name" : "sarahcruddas",
      "indices" : [ 3, 16 ],
      "id_str" : "19384258",
      "id" : 19384258
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "spaceX",
      "indices" : [ 41, 48 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572032605769609216",
  "text" : "RT @sarahcruddas: Space fans, there is a #spaceX launch tonight. No barge landing attempt, but still amazing to think how many launches thi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "spaceX",
        "indices" : [ 23, 30 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572029826783174657",
    "text" : "Space fans, there is a #spaceX launch tonight. No barge landing attempt, but still amazing to think how many launches this company is doing",
    "id" : 572029826783174657,
    "created_at" : "2015-03-01 13:45:13 +0000",
    "user" : {
      "name" : "Sarah Cruddas",
      "screen_name" : "sarahcruddas",
      "protected" : false,
      "id_str" : "19384258",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/855206612415516672\/0ClJVsI9_normal.jpg",
      "id" : 19384258,
      "verified" : true
    }
  },
  "id" : 572032605769609216,
  "created_at" : "2015-03-01 13:56:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 3, 14 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/davegantly\/status\/572022976301932545\/photo\/1",
      "indices" : [ 60, 82 ],
      "url" : "http:\/\/t.co\/ryKtoCmJXj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B_A73XnXIAAe7MG.jpg",
      "id_str" : "572022975094005760",
      "id" : 572022975094005760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_A73XnXIAAe7MG.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 576
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ryKtoCmJXj"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572027037134823424",
  "text" : "RT @davegantly: Springtime in Ireland. You have to love it. http:\/\/t.co\/ryKtoCmJXj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/davegantly\/status\/572022976301932545\/photo\/1",
        "indices" : [ 44, 66 ],
        "url" : "http:\/\/t.co\/ryKtoCmJXj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B_A73XnXIAAe7MG.jpg",
        "id_str" : "572022975094005760",
        "id" : 572022975094005760,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B_A73XnXIAAe7MG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 576
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ryKtoCmJXj"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "572022976301932545",
    "text" : "Springtime in Ireland. You have to love it. http:\/\/t.co\/ryKtoCmJXj",
    "id" : 572022976301932545,
    "created_at" : "2015-03-01 13:17:59 +0000",
    "user" : {
      "name" : "David",
      "screen_name" : "davegantly",
      "protected" : false,
      "id_str" : "16738907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001158218519863296\/rY-yu2r7_normal.jpg",
      "id" : 16738907,
      "verified" : false
    }
  },
  "id" : 572027037134823424,
  "created_at" : "2015-03-01 13:34:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "572022967418400768",
  "text" : "In their minds, Parents with toddler going to : Should I stay home to watch Ruby or bring them to make snowman.",
  "id" : 572022967418400768,
  "created_at" : "2015-03-01 13:17:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Walker",
      "screen_name" : "iHiD",
      "indices" : [ 3, 8 ],
      "id_str" : "15120222",
      "id" : 15120222
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/iHiD\/status\/569642630189064192\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/qxbM6iuX6q",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/B-fG8oCIAAARQNO.jpg",
      "id_str" : "569642622726438912",
      "id" : 569642622726438912,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B-fG8oCIAAARQNO.jpg",
      "sizes" : [ {
        "h" : 586,
        "resize" : "fit",
        "w" : 881
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 586,
        "resize" : "fit",
        "w" : 881
      }, {
        "h" : 452,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 586,
        "resize" : "fit",
        "w" : 881
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qxbM6iuX6q"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/ZvvlPasBXr",
      "expanded_url" : "https:\/\/www.hackerearth.com\/women-hackathon-2015\/",
      "display_url" : "hackerearth.com\/women-hackatho\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "571966485771517952",
  "text" : "RT @iHiD: Help spread the word about the International Women's Day Online Hackathon https:\/\/t.co\/ZvvlPasBXr http:\/\/t.co\/qxbM6iuX6q",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iHiD\/status\/569642630189064192\/photo\/1",
        "indices" : [ 98, 120 ],
        "url" : "http:\/\/t.co\/qxbM6iuX6q",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/B-fG8oCIAAARQNO.jpg",
        "id_str" : "569642622726438912",
        "id" : 569642622726438912,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/B-fG8oCIAAARQNO.jpg",
        "sizes" : [ {
          "h" : 586,
          "resize" : "fit",
          "w" : 881
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 586,
          "resize" : "fit",
          "w" : 881
        }, {
          "h" : 452,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 586,
          "resize" : "fit",
          "w" : 881
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qxbM6iuX6q"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/ZvvlPasBXr",
        "expanded_url" : "https:\/\/www.hackerearth.com\/women-hackathon-2015\/",
        "display_url" : "hackerearth.com\/women-hackatho\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "569642630189064192",
    "text" : "Help spread the word about the International Women's Day Online Hackathon https:\/\/t.co\/ZvvlPasBXr http:\/\/t.co\/qxbM6iuX6q",
    "id" : 569642630189064192,
    "created_at" : "2015-02-22 23:39:21 +0000",
    "user" : {
      "name" : "Jeremy Walker",
      "screen_name" : "iHiD",
      "protected" : false,
      "id_str" : "15120222",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947582697790156800\/jSaB2fR-_normal.jpg",
      "id" : 15120222,
      "verified" : false
    }
  },
  "id" : 571966485771517952,
  "created_at" : "2015-03-01 09:33:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]