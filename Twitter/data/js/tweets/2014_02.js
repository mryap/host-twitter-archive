Grailbird.data.tweets_2014_02 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/VWSQLAZTbm",
      "expanded_url" : "http:\/\/andrewchen.co\/2009\/06\/11\/benefit-driven-metrics-measure-the-lives-you-save-not-the-life-preservers-you-sell\/?utm_source=GrowthHackers&utm_medium=Anuj+Adhiya&utm_campaign=Anuj+Adhiya",
      "display_url" : "andrewchen.co\/2009\/06\/11\/ben\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "439485674128179201",
  "text" : "Metrics should align with your value proposition\u2014framed in terms of your customers\u2019 needs. http:\/\/t.co\/VWSQLAZTbm",
  "id" : 439485674128179201,
  "created_at" : "2014-02-28 19:42:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 98 ],
      "url" : "http:\/\/t.co\/pNeQ8GKrTu",
      "expanded_url" : "http:\/\/cna.asia\/1gGSz7i",
      "display_url" : "cna.asia\/1gGSz7i"
    } ]
  },
  "geo" : { },
  "id_str" : "439379992921645057",
  "text" : "Drought forces water rationing on millions of Malaysians - Channel NewsAsia http:\/\/t.co\/pNeQ8GKrTu",
  "id" : 439379992921645057,
  "created_at" : "2014-02-28 12:42:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NationalConcertHall",
      "screen_name" : "NCH_Music",
      "indices" : [ 3, 13 ],
      "id_str" : "23615801",
      "id" : 23615801
    }, {
      "name" : "RT\u00C9ConcertOrchestra",
      "screen_name" : "rte_co",
      "indices" : [ 76, 83 ],
      "id_str" : "247302284",
      "id" : 247302284
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "439339987872792577",
  "text" : "RT @NCH_Music: Tickets for The Lord of the Rings composer Howard Shore with @rte_co on Saturday 26 April are now on sale. https:\/\/t.co\/tf2y\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RT\u00C9ConcertOrchestra",
        "screen_name" : "rte_co",
        "indices" : [ 61, 68 ],
        "id_str" : "247302284",
        "id" : 247302284
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/tf2yc11vL5",
        "expanded_url" : "https:\/\/www.nch.ie\/Online\/HowardShore26Apr14",
        "display_url" : "nch.ie\/Online\/HowardS\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "439049606240813057",
    "text" : "Tickets for The Lord of the Rings composer Howard Shore with @rte_co on Saturday 26 April are now on sale. https:\/\/t.co\/tf2yc11vL5",
    "id" : 439049606240813057,
    "created_at" : "2014-02-27 14:49:17 +0000",
    "user" : {
      "name" : "NationalConcertHall",
      "screen_name" : "NCH_Music",
      "protected" : false,
      "id_str" : "23615801",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006839564248535040\/UBaZn3S6_normal.jpg",
      "id" : 23615801,
      "verified" : false
    }
  },
  "id" : 439339987872792577,
  "created_at" : "2014-02-28 10:03:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "439339797614985216",
  "text" : "\u4E00\u4E2A\u5E74\u8F7B\u4EBA\u95EE\u4E00\u4E2A\u5F97\u9053\u7684\u8001\u8005\uFF1A\u201C\u667A\u6167\u54EA\u91CC\u6765\uFF1F\u201D\uFF0C\u667A\u8005\u8BF4\uFF1A\u201C\u7CBE\u786E\u7684\u5224\u65AD\u529B\u201D\u3002\u5E74\u8F7B\u4EBA\u53C8\u95EE\uFF1A\u201C\u7CBE\u786E\u7684\u5224\u65AD\u529B\u54EA\u91CC\u6765\uFF1F\u201D\uFF0C\u667A\u8005\u8BF4\uFF1A\u201C\u7ECF\u9A8C\u201D\u3002\u5E74\u8F7B\u4EBA\u518D\u95EE\uFF1A\u201C\u7ECF\u9A8C\u54EA\u91CC\u6765\uFF1F\u201D\uFF0C\u667A\u8005\u8BF4\uFF1A\u201C\u9519\u8BEF\u7684\u5224\u65AD\u201D",
  "id" : 439339797614985216,
  "created_at" : "2014-02-28 10:02:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Associated Press",
      "screen_name" : "AP",
      "indices" : [ 3, 6 ],
      "id_str" : "51241574",
      "id" : 51241574
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/BuFYedUIgy",
      "expanded_url" : "http:\/\/apne.ws\/1hEChwy",
      "display_url" : "apne.ws\/1hEChwy"
    } ]
  },
  "geo" : { },
  "id_str" : "439182823032098816",
  "text" : "RT @AP: Mayors of NYC and Boston plan to boycott St. Patrick's Day parades to protest exclusion of gay groups: http:\/\/t.co\/BuFYedUIgy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 125 ],
        "url" : "http:\/\/t.co\/BuFYedUIgy",
        "expanded_url" : "http:\/\/apne.ws\/1hEChwy",
        "display_url" : "apne.ws\/1hEChwy"
      } ]
    },
    "geo" : { },
    "id_str" : "439174618587619328",
    "text" : "Mayors of NYC and Boston plan to boycott St. Patrick's Day parades to protest exclusion of gay groups: http:\/\/t.co\/BuFYedUIgy",
    "id" : 439174618587619328,
    "created_at" : "2014-02-27 23:06:03 +0000",
    "user" : {
      "name" : "The Associated Press",
      "screen_name" : "AP",
      "protected" : false,
      "id_str" : "51241574",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/461964160838803457\/8z9FImcv_normal.png",
      "id" : 51241574,
      "verified" : true
    }
  },
  "id" : 439182823032098816,
  "created_at" : "2014-02-27 23:38:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/yFmKmelTZZ",
      "expanded_url" : "https:\/\/mobile.twitter.com\/ederoiste\/status\/439170548195926016?screen_name=ederoiste",
      "display_url" : "mobile.twitter.com\/ederoiste\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "439172690717315072",
  "text" : "So Irish citizens paid both way? https:\/\/t.co\/yFmKmelTZZ",
  "id" : 439172690717315072,
  "created_at" : "2014-02-27 22:58:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http:\/\/t.co\/XBoi3P1va7",
      "expanded_url" : "http:\/\/uk.news.yahoo.com\/nato-commander-plays-down-tension-russia-over-ukraine-172755388.html#BqUaaTu",
      "display_url" : "uk.news.yahoo.com\/nato-commander\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "439105629555523584",
  "text" : "Do you know that NATO stands for No Action Talks Only? http:\/\/t.co\/XBoi3P1va7",
  "id" : 439105629555523584,
  "created_at" : "2014-02-27 18:31:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cecilia Liao",
      "screen_name" : "cecilialiao",
      "indices" : [ 3, 15 ],
      "id_str" : "152014438",
      "id" : 152014438
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dataviz",
      "indices" : [ 43, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/tErTRC86hV",
      "expanded_url" : "http:\/\/ow.ly\/u3hkE",
      "display_url" : "ow.ly\/u3hkE"
    } ]
  },
  "geo" : { },
  "id_str" : "438990816217141248",
  "text" : "RT @cecilialiao: Flying from US to Europe? #dataviz shows you should buy 53 days in advance for cheapest fare http:\/\/t.co\/tErTRC86hV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dataviz",
        "indices" : [ 26, 34 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/tErTRC86hV",
        "expanded_url" : "http:\/\/ow.ly\/u3hkE",
        "display_url" : "ow.ly\/u3hkE"
      } ]
    },
    "geo" : { },
    "id_str" : "438981856470790144",
    "text" : "Flying from US to Europe? #dataviz shows you should buy 53 days in advance for cheapest fare http:\/\/t.co\/tErTRC86hV",
    "id" : 438981856470790144,
    "created_at" : "2014-02-27 10:20:05 +0000",
    "user" : {
      "name" : "Cecilia Liao",
      "screen_name" : "cecilialiao",
      "protected" : false,
      "id_str" : "152014438",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/819551679053963264\/KH47slQg_normal.jpg",
      "id" : 152014438,
      "verified" : false
    }
  },
  "id" : 438990816217141248,
  "created_at" : "2014-02-27 10:55:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http:\/\/t.co\/qZ4313yZyb",
      "expanded_url" : "http:\/\/www.startupshk.com\/li-ka-shing-adds-synthetic-egg-startup-portfolio-investing-us-23m-silicon-valley-based-hampton-creek\/?utm_content=bufferd2c2f&utm_medium=social&utm_source=plus.google.com&utm_campaign=buffer#more-6245",
      "display_url" : "startupshk.com\/li-ka-shing-ad\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "438940814975455232",
  "text" : "Li Ka-Shing invest US 23 million in synthetic egg. http:\/\/t.co\/qZ4313yZyb",
  "id" : 438940814975455232,
  "created_at" : "2014-02-27 07:37:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 91 ],
      "url" : "http:\/\/t.co\/3LjXezSSci",
      "expanded_url" : "http:\/\/www.amateurphotographer.co.uk\/photo-news\/540427\/compact-system-camera-sales-crash-special-report-with-video",
      "display_url" : "amateurphotographer.co.uk\/photo-news\/540\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "438786120894386177",
  "text" : "Do people really care if they buy a compact system camera or a DSLR? http:\/\/t.co\/3LjXezSSci",
  "id" : 438786120894386177,
  "created_at" : "2014-02-26 21:22:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Dave Chaffey",
      "screen_name" : "DaveChaffey",
      "indices" : [ 0, 12 ],
      "id_str" : "16134616",
      "id" : 16134616
    }, {
      "name" : "Hera Hussain \u062D\u0631\u0627",
      "screen_name" : "herahussain",
      "indices" : [ 13, 25 ],
      "id_str" : "59307246",
      "id" : 59307246
    }, {
      "name" : "Katy Howell",
      "screen_name" : "katyhowell",
      "indices" : [ 26, 37 ],
      "id_str" : "2569471",
      "id" : 2569471
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "393334158900363264",
  "geo" : { },
  "id_str" : "438371063383486465",
  "in_reply_to_user_id" : 16134616,
  "text" : "@DaveChaffey @herahussain @katyhowell Any chance where I can access this presentation slide? Thank You.",
  "id" : 438371063383486465,
  "in_reply_to_status_id" : 393334158900363264,
  "created_at" : "2014-02-25 17:53:00 +0000",
  "in_reply_to_screen_name" : "DaveChaffey",
  "in_reply_to_user_id_str" : "16134616",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Downtown Josh Brown",
      "screen_name" : "ReformedBroker",
      "indices" : [ 3, 18 ],
      "id_str" : "22522178",
      "id" : 22522178
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "438364498416926721",
  "text" : "RT @ReformedBroker: I can\u2019t believe an imaginary currency backed by nothing on an exchange run by no one is melting down.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "438341922143612928",
    "text" : "I can\u2019t believe an imaginary currency backed by nothing on an exchange run by no one is melting down.",
    "id" : 438341922143612928,
    "created_at" : "2014-02-25 15:57:12 +0000",
    "user" : {
      "name" : "Downtown Josh Brown",
      "screen_name" : "ReformedBroker",
      "protected" : false,
      "id_str" : "22522178",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3510014701\/6dfc231a5c790c6595fe1bd135a92013_normal.jpeg",
      "id" : 22522178,
      "verified" : true
    }
  },
  "id" : 438364498416926721,
  "created_at" : "2014-02-25 17:26:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 85 ],
      "url" : "http:\/\/t.co\/IP1Zt7UvBl",
      "expanded_url" : "http:\/\/gu.com\/p\/3n3tb\/tw",
      "display_url" : "gu.com\/p\/3n3tb\/tw"
    } ]
  },
  "geo" : { },
  "id_str" : "438339325151830016",
  "text" : "EU focus on mobile roaming charges is 'outdated' says EE chief http:\/\/t.co\/IP1Zt7UvBl",
  "id" : 438339325151830016,
  "created_at" : "2014-02-25 15:46:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/BG2vKSAPur",
      "expanded_url" : "http:\/\/www.google.com\/hostednews\/afp\/article\/ALeqM5h3u4CmvwCBwA9zDARlaKyXq8eTTA?docId=c22fc028-f7f5-4e26-aa11-7226f2b9fc55",
      "display_url" : "google.com\/hostednews\/afp\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "438322948353851392",
  "text" : "Divided Thailand faces warnings of civil war http:\/\/t.co\/BG2vKSAPur",
  "id" : 438322948353851392,
  "created_at" : "2014-02-25 14:41:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/AtfO3V1N3i",
      "expanded_url" : "http:\/\/econsultancy.com\/blog\/7860-monitoring-purchase-latency-in-google-analytics",
      "display_url" : "econsultancy.com\/blog\/7860-moni\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "438279233228791808",
  "text" : "Research: an existing customer is worth more than a new customer, so you need to monitor and hold on to them.\nhttp:\/\/t.co\/AtfO3V1N3i",
  "id" : 438279233228791808,
  "created_at" : "2014-02-25 11:48:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http:\/\/t.co\/ffQ8kM6E1y",
      "expanded_url" : "http:\/\/www.pinterest.com\/pin\/150518812515973757\/",
      "display_url" : "pinterest.com\/pin\/1505188125\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "438262608391860224",
  "text" : "Getting to grips with Google analytics? Start off with what to measure. http:\/\/t.co\/ffQ8kM6E1y",
  "id" : 438262608391860224,
  "created_at" : "2014-02-25 10:42:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 92 ],
      "url" : "http:\/\/t.co\/7HF3S4p38H",
      "expanded_url" : "http:\/\/i1266.photobucket.com\/albums\/jj524\/picrumors\/picrumors001\/Bildschirmfoto2014-02-07um184923_zps7e88a424.png",
      "display_url" : "i1266.photobucket.com\/albums\/jj524\/p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "438106928225726466",
  "text" : "I cringed whenever I came across this type of powerpoint presentation http:\/\/t.co\/7HF3S4p38H",
  "id" : 438106928225726466,
  "created_at" : "2014-02-25 00:23:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BehaviouralDesignLab",
      "screen_name" : "behdeslab",
      "indices" : [ 3, 13 ],
      "id_str" : "1287181285",
      "id" : 1287181285
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "horizon",
      "indices" : [ 18, 26 ]
    }, {
      "text" : "behaviouraleconomics",
      "indices" : [ 86, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "438087021157179392",
  "text" : "RT @behdeslab: If #horizon this evening has wet your appetite, our library is full of #behaviouraleconomics inspiration http:\/\/t.co\/1zLk7M6\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "horizon",
        "indices" : [ 3, 11 ]
      }, {
        "text" : "behaviouraleconomics",
        "indices" : [ 71, 92 ]
      } ],
      "urls" : [ {
        "indices" : [ 105, 127 ],
        "url" : "http:\/\/t.co\/1zLk7M66hZ",
        "expanded_url" : "http:\/\/www.behaviouraldesignlab.org\/library\/",
        "display_url" : "behaviouraldesignlab.org\/library\/"
      } ]
    },
    "geo" : { },
    "id_str" : "438078460452691968",
    "text" : "If #horizon this evening has wet your appetite, our library is full of #behaviouraleconomics inspiration http:\/\/t.co\/1zLk7M66hZ",
    "id" : 438078460452691968,
    "created_at" : "2014-02-24 22:30:18 +0000",
    "user" : {
      "name" : "BehaviouralDesignLab",
      "screen_name" : "behdeslab",
      "protected" : false,
      "id_str" : "1287181285",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3634816703\/d25d09087c37ac35e217ea22cb9db8a2_normal.png",
      "id" : 1287181285,
      "verified" : false
    }
  },
  "id" : 438087021157179392,
  "created_at" : "2014-02-24 23:04:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "834055856",
      "id" : 834055856
    }, {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "indices" : [ 116, 127 ],
      "id_str" : "834055856",
      "id" : 834055856
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Expat",
      "indices" : [ 104, 110 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 103 ],
      "url" : "http:\/\/t.co\/0A3GOag9rJ",
      "expanded_url" : "http:\/\/tinyurl.com\/qbvhg25",
      "display_url" : "tinyurl.com\/qbvhg25"
    } ]
  },
  "geo" : { },
  "id_str" : "438016793215791104",
  "text" : "RT @expatsblog: Dublin ranks 34th in latest global expat living standards survey http:\/\/t.co\/0A3GOag9rJ #Expat News @expatsblog",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.expatsblog.com\" rel=\"nofollow\"\u003EExpatsBlog.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Expats Blog",
        "screen_name" : "expatsblog",
        "indices" : [ 100, 111 ],
        "id_str" : "834055856",
        "id" : 834055856
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Expat",
        "indices" : [ 88, 94 ]
      } ],
      "urls" : [ {
        "indices" : [ 65, 87 ],
        "url" : "http:\/\/t.co\/0A3GOag9rJ",
        "expanded_url" : "http:\/\/tinyurl.com\/qbvhg25",
        "display_url" : "tinyurl.com\/qbvhg25"
      } ]
    },
    "geo" : { },
    "id_str" : "438010682030948352",
    "text" : "Dublin ranks 34th in latest global expat living standards survey http:\/\/t.co\/0A3GOag9rJ #Expat News @expatsblog",
    "id" : 438010682030948352,
    "created_at" : "2014-02-24 18:00:59 +0000",
    "user" : {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "protected" : false,
      "id_str" : "834055856",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2627803659\/logo_normal.png",
      "id" : 834055856,
      "verified" : false
    }
  },
  "id" : 438016793215791104,
  "created_at" : "2014-02-24 18:25:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "438016410422218752",
  "text" : "Really that the revamped Yahoo homepage?",
  "id" : 438016410422218752,
  "created_at" : "2014-02-24 18:23:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/MCNeBvbDZD",
      "expanded_url" : "http:\/\/checklist.com\/link\/dAIAtxgGb26TfuAO",
      "display_url" : "checklist.com\/link\/dAIAtxgGb\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "437960603332661248",
  "text" : "Google Analytics Setup Checklist What else would you add? http:\/\/t.co\/MCNeBvbDZD",
  "id" : 437960603332661248,
  "created_at" : "2014-02-24 14:41:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 87 ],
      "url" : "http:\/\/t.co\/RdA1N76rQw",
      "expanded_url" : "http:\/\/www.gachecker.com\/",
      "display_url" : "gachecker.com"
    } ]
  },
  "geo" : { },
  "id_str" : "437956044086444032",
  "text" : "Verify that your Google Analytics tags are present on every page http:\/\/t.co\/RdA1N76rQw",
  "id" : 437956044086444032,
  "created_at" : "2014-02-24 14:23:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 58 ],
      "url" : "http:\/\/t.co\/SNqS8UwUbr",
      "expanded_url" : "http:\/\/blog.ubersnap.com\/post\/74248810821\/16-photo-festivals-in-asia-you-should-check-out-in-2014",
      "display_url" : "blog.ubersnap.com\/post\/742488108\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "437921389849243648",
  "text" : "Top photo festivals in Asia in 2014 http:\/\/t.co\/SNqS8UwUbr",
  "id" : 437921389849243648,
  "created_at" : "2014-02-24 12:06:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elzabeth",
      "screen_name" : "elegsabiff",
      "indices" : [ 3, 14 ],
      "id_str" : "18649438",
      "id" : 18649438
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "437601400986812416",
  "text" : "RT @elegsabiff: Teach a man to fish and he'll eat for life. Give him someone else's fish and he'll vote for you.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "437598038782377984",
    "text" : "Teach a man to fish and he'll eat for life. Give him someone else's fish and he'll vote for you.",
    "id" : 437598038782377984,
    "created_at" : "2014-02-23 14:41:17 +0000",
    "user" : {
      "name" : "Elzabeth",
      "screen_name" : "elegsabiff",
      "protected" : false,
      "id_str" : "18649438",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1016633757934276614\/6xmqyZT__normal.jpg",
      "id" : 18649438,
      "verified" : false
    }
  },
  "id" : 437601400986812416,
  "created_at" : "2014-02-23 14:54:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Designed 4 You",
      "screen_name" : "lpdesigned4you",
      "indices" : [ 0, 15 ],
      "id_str" : "24039179",
      "id" : 24039179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "437596044134973440",
  "geo" : { },
  "id_str" : "437596764044918785",
  "in_reply_to_user_id" : 24039179,
  "text" : "@lpdesigned4you Thank you!",
  "id" : 437596764044918785,
  "in_reply_to_status_id" : 437596044134973440,
  "created_at" : "2014-02-23 14:36:13 +0000",
  "in_reply_to_screen_name" : "lpdesigned4you",
  "in_reply_to_user_id_str" : "24039179",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 0, 14 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "437593917932834816",
  "text" : "#irishbizparty Where can I get drawer slide roller bearings in Dublin? Thank you",
  "id" : 437593917932834816,
  "created_at" : "2014-02-23 14:24:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "YouAreWelcome",
      "indices" : [ 118, 132 ]
    } ],
    "urls" : [ {
      "indices" : [ 4, 26 ],
      "url" : "http:\/\/t.co\/JGNJcYjho5",
      "expanded_url" : "http:\/\/gaconfig.com\/google-analytics-url-builder\/",
      "display_url" : "gaconfig.com\/google-analyti\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "437591371075973120",
  "text" : "Use http:\/\/t.co\/JGNJcYjho5 to create unique URL for your marketing campaign so they can be track in Google Analytics. #YouAreWelcome",
  "id" : 437591371075973120,
  "created_at" : "2014-02-23 14:14:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "437536574306525184",
  "geo" : { },
  "id_str" : "437564761673183232",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose I think people in Ireland value work life balance.  If you got an email at 9 pm, most probably it from me. :)",
  "id" : 437564761673183232,
  "in_reply_to_status_id" : 437536574306525184,
  "created_at" : "2014-02-23 12:29:03 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "437534693903200256",
  "geo" : { },
  "id_str" : "437536409050963968",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose Pretty spot on where I comes from. I have once got a call from HR at 9pm.",
  "id" : 437536409050963968,
  "in_reply_to_status_id" : 437534693903200256,
  "created_at" : "2014-02-23 10:36:23 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Designed 4 You",
      "screen_name" : "lpdesigned4you",
      "indices" : [ 0, 15 ],
      "id_str" : "24039179",
      "id" : 24039179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "436252385078685696",
  "geo" : { },
  "id_str" : "437307919559049217",
  "in_reply_to_user_id" : 24039179,
  "text" : "@lpdesigned4you I will tweet about tips to help you get to grips with Google Analytics",
  "id" : 437307919559049217,
  "in_reply_to_status_id" : 436252385078685696,
  "created_at" : "2014-02-22 19:28:27 +0000",
  "in_reply_to_screen_name" : "lpdesigned4you",
  "in_reply_to_user_id_str" : "24039179",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/xHFcwKMXCf",
      "expanded_url" : "http:\/\/www.ometria.com\/content\/ultimate-customer-segmentation",
      "display_url" : "ometria.com\/content\/ultima\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "437303498389352449",
  "text" : "Data in aggregate is pretty much useless. When you segment, you begin to get great insights. http:\/\/t.co\/xHFcwKMXCf",
  "id" : 437303498389352449,
  "created_at" : "2014-02-22 19:10:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/OE6F4Iaevy",
      "expanded_url" : "http:\/\/collectably.com",
      "display_url" : "collectably.com"
    } ]
  },
  "geo" : { },
  "id_str" : "437292150930571264",
  "text" : "Need to tame those browser tab that is spawning all over? Checkout tab.bz and http:\/\/t.co\/OE6F4Iaevy",
  "id" : 437292150930571264,
  "created_at" : "2014-02-22 18:25:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC World Service",
      "screen_name" : "bbcworldservice",
      "indices" : [ 3, 19 ],
      "id_str" : "786764",
      "id" : 786764
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 128 ],
      "url" : "http:\/\/t.co\/2DfUeiq3Fo",
      "expanded_url" : "http:\/\/bbc.in\/1l53sFu",
      "display_url" : "bbc.in\/1l53sFu"
    } ]
  },
  "geo" : { },
  "id_str" : "437247220631756802",
  "text" : "RT @bbcworldservice: How an Israeli and a Palestinian woman became firm friends at a cancer support group http:\/\/t.co\/2DfUeiq3Fo http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bbcworldservice\/status\/437210949976862720\/photo\/1",
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/5qETHqqz0Q",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BhFJCt0IcAAZrPX.jpg",
        "id_str" : "437210949838467072",
        "id" : 437210949838467072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BhFJCt0IcAAZrPX.jpg",
        "sizes" : [ {
          "h" : 351,
          "resize" : "fit",
          "w" : 624
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 624
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 624
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 624
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5qETHqqz0Q"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 85, 107 ],
        "url" : "http:\/\/t.co\/2DfUeiq3Fo",
        "expanded_url" : "http:\/\/bbc.in\/1l53sFu",
        "display_url" : "bbc.in\/1l53sFu"
      } ]
    },
    "geo" : { },
    "id_str" : "437210949976862720",
    "text" : "How an Israeli and a Palestinian woman became firm friends at a cancer support group http:\/\/t.co\/2DfUeiq3Fo http:\/\/t.co\/5qETHqqz0Q",
    "id" : 437210949976862720,
    "created_at" : "2014-02-22 13:03:08 +0000",
    "user" : {
      "name" : "BBC World Service",
      "screen_name" : "bbcworldservice",
      "protected" : false,
      "id_str" : "786764",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004638958091894784\/JtNS_gjE_normal.jpg",
      "id" : 786764,
      "verified" : true
    }
  },
  "id" : 437247220631756802,
  "created_at" : "2014-02-22 15:27:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "indices" : [ 0, 8 ],
      "id_str" : "26425084",
      "id" : 26425084
    }, {
      "name" : "Uber KL",
      "screen_name" : "Uber_KL",
      "indices" : [ 9, 17 ],
      "id_str" : "1851826346",
      "id" : 1851826346
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "436881630230097920",
  "geo" : { },
  "id_str" : "436897415615242240",
  "in_reply_to_user_id" : 26425084,
  "text" : "@oiwoods @Uber_KL Looking forward to use the service when I travel to KL.",
  "id" : 436897415615242240,
  "in_reply_to_status_id" : 436881630230097920,
  "created_at" : "2014-02-21 16:17:15 +0000",
  "in_reply_to_screen_name" : "oiwoods",
  "in_reply_to_user_id_str" : "26425084",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deloitte Singapore",
      "screen_name" : "DeloitteSG",
      "indices" : [ 3, 14 ],
      "id_str" : "1497469477",
      "id" : 1497469477
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SGBudget",
      "indices" : [ 16, 25 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "436838039315578880",
  "text" : "RT @DeloitteSG: #SGBudget Taxes on liquor, betting, smoking: did they just take the \u2018Sin\u2019 out of \u2018Singapore\u2019?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SGBudget",
        "indices" : [ 0, 9 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "436825598405529600",
    "text" : "#SGBudget Taxes on liquor, betting, smoking: did they just take the \u2018Sin\u2019 out of \u2018Singapore\u2019?",
    "id" : 436825598405529600,
    "created_at" : "2014-02-21 11:31:53 +0000",
    "user" : {
      "name" : "Deloitte Singapore",
      "screen_name" : "DeloitteSG",
      "protected" : false,
      "id_str" : "1497469477",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/944139451713732608\/vI9lRvj6_normal.jpg",
      "id" : 1497469477,
      "verified" : false
    }
  },
  "id" : 436838039315578880,
  "created_at" : "2014-02-21 12:21:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "indices" : [ 3, 11 ],
      "id_str" : "26425084",
      "id" : 26425084
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SGbudget",
      "indices" : [ 124, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "436837848449564672",
  "text" : "RT @oiwoods: Wow, Singapore raising \"excise duty rate of all liquor types by 25%\" - no more nights out at Pangaea for me... #SGbudget cc. @\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jeremy Grant",
        "screen_name" : "TradingJeremy",
        "indices" : [ 125, 139 ],
        "id_str" : "47456979",
        "id" : 47456979
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SGbudget",
        "indices" : [ 111, 120 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "436792507439931393",
    "text" : "Wow, Singapore raising \"excise duty rate of all liquor types by 25%\" - no more nights out at Pangaea for me... #SGbudget cc. @TradingJeremy",
    "id" : 436792507439931393,
    "created_at" : "2014-02-21 09:20:23 +0000",
    "user" : {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "protected" : false,
      "id_str" : "26425084",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914900128707764225\/Vmx_JkpE_normal.jpg",
      "id" : 26425084,
      "verified" : false
    }
  },
  "id" : 436837848449564672,
  "created_at" : "2014-02-21 12:20:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 0, 16 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/9kiQvNlI1T",
      "expanded_url" : "http:\/\/www.change.org\/en-CA\/petitions\/international-skating-union-isu-open-investigation-into-judging-decisions-of-women-s-figure-skating-and-demand-rejudgement-at-the-sochi-olympics",
      "display_url" : "change.org\/en-CA\/petition\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "436719032616878081",
  "geo" : { },
  "id_str" : "436831358816907264",
  "in_reply_to_user_id" : 14340736,
  "text" : "@interactivemark You can ask her to sign the petition (which appear on my Twitter timeline) http:\/\/t.co\/9kiQvNlI1T :)",
  "id" : 436831358816907264,
  "in_reply_to_status_id" : 436719032616878081,
  "created_at" : "2014-02-21 11:54:46 +0000",
  "in_reply_to_screen_name" : "interactivemark",
  "in_reply_to_user_id_str" : "14340736",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http:\/\/t.co\/FgSst2eRLx",
      "expanded_url" : "http:\/\/www.smh.com.au\/sport\/winter-olympics\/sochi-2014-controversy-as-russian-adelina-sotnikova-upsets-korean-favourite-kim-yuna-to-snatch-figure-skating-gold-medal-20140221-334z4.html",
      "display_url" : "smh.com.au\/sport\/winter-o\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "436763878324588544",
  "text" : "Controversy as Russian Adelina Sotnikova upsets Korean favourite Kim Yu-Na to snatch figure skating gold medal  http:\/\/t.co\/FgSst2eRLx",
  "id" : 436763878324588544,
  "created_at" : "2014-02-21 07:26:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "indices" : [ 3, 11 ],
      "id_str" : "26425084",
      "id" : 26425084
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "436758444868378624",
  "text" : "RT @oiwoods: \"Learn Chinese, Spanish, Portuguese and Russian.\" WPP CEO Martin Sorrel's advice on how to get into advertising (via @RobinHic\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Robin Hicks",
        "screen_name" : "RobinHicks_",
        "indices" : [ 117, 129 ],
        "id_str" : "563443018",
        "id" : 563443018
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "436724596771799041",
    "text" : "\"Learn Chinese, Spanish, Portuguese and Russian.\" WPP CEO Martin Sorrel's advice on how to get into advertising (via @RobinHicks_)",
    "id" : 436724596771799041,
    "created_at" : "2014-02-21 04:50:32 +0000",
    "user" : {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "protected" : false,
      "id_str" : "26425084",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914900128707764225\/Vmx_JkpE_normal.jpg",
      "id" : 26425084,
      "verified" : false
    }
  },
  "id" : 436758444868378624,
  "created_at" : "2014-02-21 07:05:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ECCV",
      "screen_name" : "ethnicvic",
      "indices" : [ 3, 13 ],
      "id_str" : "246077495",
      "id" : 246077495
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/4nd6vCnS4H",
      "expanded_url" : "http:\/\/goo.gl\/aX77xj",
      "display_url" : "goo.gl\/aX77xj"
    } ]
  },
  "geo" : { },
  "id_str" : "436652048487313408",
  "text" : "RT @ethnicvic: South Australian Senator Penny Wong reveals she had to confront overt racism in her formative years http:\/\/t.co\/4nd6vCnS4H",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 100, 122 ],
        "url" : "http:\/\/t.co\/4nd6vCnS4H",
        "expanded_url" : "http:\/\/goo.gl\/aX77xj",
        "display_url" : "goo.gl\/aX77xj"
      } ]
    },
    "geo" : { },
    "id_str" : "436640179252912128",
    "text" : "South Australian Senator Penny Wong reveals she had to confront overt racism in her formative years http:\/\/t.co\/4nd6vCnS4H",
    "id" : 436640179252912128,
    "created_at" : "2014-02-20 23:15:05 +0000",
    "user" : {
      "name" : "ECCV",
      "screen_name" : "ethnicvic",
      "protected" : false,
      "id_str" : "246077495",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/496431501790023680\/TYQ6xvc2_normal.jpeg",
      "id" : 246077495,
      "verified" : false
    }
  },
  "id" : 436652048487313408,
  "created_at" : "2014-02-21 00:02:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Royal Statistical Society",
      "screen_name" : "RoyalStatSoc",
      "indices" : [ 3, 16 ],
      "id_str" : "124176132",
      "id" : 124176132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http:\/\/t.co\/hlAS68fTpv",
      "expanded_url" : "http:\/\/www.statslife.org.uk\/news\/1256-british-library-hosts-data-visualisation-exhibition#.UwTeFvzyElY.twitter",
      "display_url" : "statslife.org.uk\/news\/1256-brit\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "436572153946050561",
  "text" : "RT @RoyalStatSoc: British Library to host data visualisation exhibition http:\/\/t.co\/hlAS68fTpv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 76 ],
        "url" : "http:\/\/t.co\/hlAS68fTpv",
        "expanded_url" : "http:\/\/www.statslife.org.uk\/news\/1256-british-library-hosts-data-visualisation-exhibition#.UwTeFvzyElY.twitter",
        "display_url" : "statslife.org.uk\/news\/1256-brit\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "436178096052396032",
    "text" : "British Library to host data visualisation exhibition http:\/\/t.co\/hlAS68fTpv",
    "id" : 436178096052396032,
    "created_at" : "2014-02-19 16:38:56 +0000",
    "user" : {
      "name" : "Royal Statistical Society",
      "screen_name" : "RoyalStatSoc",
      "protected" : false,
      "id_str" : "124176132",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/458914803294027777\/4KcRK8L5_normal.jpeg",
      "id" : 124176132,
      "verified" : false
    }
  },
  "id" : 436572153946050561,
  "created_at" : "2014-02-20 18:44:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 74 ],
      "url" : "http:\/\/t.co\/XALXlKQAeA",
      "expanded_url" : "http:\/\/sg.news.yahoo.com\/singapore-diplomat-jailed-over-false-claims-pineapple-tarts-132136769.html",
      "display_url" : "sg.news.yahoo.com\/singapore-dipl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "436570046660218880",
  "text" : "Diplomat jailed over for pineapple tarts &amp; wine http:\/\/t.co\/XALXlKQAeA",
  "id" : 436570046660218880,
  "created_at" : "2014-02-20 18:36:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ann Donnelly",
      "screen_name" : "ann_donnelly",
      "indices" : [ 3, 16 ],
      "id_str" : "17484033",
      "id" : 17484033
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "436539826393718785",
  "text" : "RT @ann_donnelly: Anyone else tired of all this social media sh!t?! Bored, waiting for the next 'big thing' to come along? RT if you are wi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "436531909464977408",
    "text" : "Anyone else tired of all this social media sh!t?! Bored, waiting for the next 'big thing' to come along? RT if you are with me!",
    "id" : 436531909464977408,
    "created_at" : "2014-02-20 16:04:52 +0000",
    "user" : {
      "name" : "Ann Donnelly",
      "screen_name" : "ann_donnelly",
      "protected" : false,
      "id_str" : "17484033",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033138108928348160\/u35SDSYz_normal.jpg",
      "id" : 17484033,
      "verified" : false
    }
  },
  "id" : 436539826393718785,
  "created_at" : "2014-02-20 16:36:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/436504109990940672\/photo\/1",
      "indices" : [ 52, 74 ],
      "url" : "http:\/\/t.co\/Gb4bKBCJES",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bg7GLONCEAAa8ih.png",
      "id_str" : "436504109995134976",
      "id" : 436504109995134976,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bg7GLONCEAAa8ih.png",
      "sizes" : [ {
        "h" : 289,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 435,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 435,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 435,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Gb4bKBCJES"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "436502122751082496",
  "geo" : { },
  "id_str" : "436504109990940672",
  "in_reply_to_user_id" : 9465632,
  "text" : "@BridieDees Here the Google Analytics Social Report http:\/\/t.co\/Gb4bKBCJES",
  "id" : 436504109990940672,
  "in_reply_to_status_id" : 436502122751082496,
  "created_at" : "2014-02-20 14:14:24 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "436502122751082496",
  "text" : "@BridieDees You can set up Google analytics's Social Report to monitor your Twitter performance.",
  "id" : 436502122751082496,
  "created_at" : "2014-02-20 14:06:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/rDRyRAqWcJ",
      "expanded_url" : "http:\/\/mothership.sg\/2014\/02\/woman-couldnt-jump-out-of-plane-said-yes-to-marriage-proposal-on-board-sia-flight\/",
      "display_url" : "mothership.sg\/2014\/02\/woman-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "436446279133327360",
  "text" : "Poor Thing. Woman couldn\u2019t jump out of plane, said yes to marriage proposal on board flight http:\/\/t.co\/rDRyRAqWcJ",
  "id" : 436446279133327360,
  "created_at" : "2014-02-20 10:24:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 0, 14 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "436250747555303424",
  "text" : "#irishbizparty good evening.  I help business make sense of web analytics data so they can do more online.",
  "id" : 436250747555303424,
  "created_at" : "2014-02-19 21:27:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Engine Digital",
      "screen_name" : "EngineDigital",
      "indices" : [ 3, 17 ],
      "id_str" : "20071894",
      "id" : 20071894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/7mqsK8xTKL",
      "expanded_url" : "http:\/\/fb.me\/365b7oZ5E",
      "display_url" : "fb.me\/365b7oZ5E"
    } ]
  },
  "geo" : { },
  "id_str" : "435877530005176320",
  "text" : "RT @EngineDigital: Great, 'Hipster' is now a web strategy... http:\/\/t.co\/7mqsK8xTKL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 42, 64 ],
        "url" : "http:\/\/t.co\/7mqsK8xTKL",
        "expanded_url" : "http:\/\/fb.me\/365b7oZ5E",
        "display_url" : "fb.me\/365b7oZ5E"
      } ]
    },
    "geo" : { },
    "id_str" : "435875821941575681",
    "text" : "Great, 'Hipster' is now a web strategy... http:\/\/t.co\/7mqsK8xTKL",
    "id" : 435875821941575681,
    "created_at" : "2014-02-18 20:37:48 +0000",
    "user" : {
      "name" : "Engine Digital",
      "screen_name" : "EngineDigital",
      "protected" : false,
      "id_str" : "20071894",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/957655756676907008\/j8Kdnaj9_normal.jpg",
      "id" : 20071894,
      "verified" : false
    }
  },
  "id" : 435877530005176320,
  "created_at" : "2014-02-18 20:44:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Changi Airport",
      "screen_name" : "FansofChangi",
      "indices" : [ 0, 13 ],
      "id_str" : "365857264",
      "id" : 365857264
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "435769974989131776",
  "geo" : { },
  "id_str" : "435782508349648896",
  "in_reply_to_user_id" : 65268724,
  "text" : "@FansofChangi Have your organisation considering promoting her ?",
  "id" : 435782508349648896,
  "in_reply_to_status_id" : 435769974989131776,
  "created_at" : "2014-02-18 14:27:01 +0000",
  "in_reply_to_screen_name" : "ChangiAirport",
  "in_reply_to_user_id_str" : "65268724",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http:\/\/t.co\/BSCA9p1hCP",
      "expanded_url" : "http:\/\/hi.co",
      "display_url" : "hi.co"
    } ]
  },
  "geo" : { },
  "id_str" : "435728979022979072",
  "text" : "I have 5 http:\/\/t.co\/BSCA9p1hCP invitations to give. Please contact me.",
  "id" : 435728979022979072,
  "created_at" : "2014-02-18 10:54:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Venetia Rainey",
      "screen_name" : "venetiarainey",
      "indices" : [ 3, 17 ],
      "id_str" : "181121575",
      "id" : 181121575
    }, {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "indices" : [ 50, 59 ],
      "id_str" : "742143",
      "id" : 742143
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "435727448337289216",
  "text" : "RT @venetiarainey: Ahh, context, there you are MT @BBCWorld Separated but not alone. Syrian child refugee whose photo hit a nerve online ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC News (World)",
        "screen_name" : "BBCWorld",
        "indices" : [ 31, 40 ],
        "id_str" : "742143",
        "id" : 742143
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BBCWorld\/status\/435706219831906305\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/qAqxbPwd3l",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Bgvwf5RCUAA_EHY.jpg",
        "id_str" : "435706219710271488",
        "id" : 435706219710271488,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bgvwf5RCUAA_EHY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 348,
          "resize" : "fit",
          "w" : 616
        }, {
          "h" : 348,
          "resize" : "fit",
          "w" : 616
        }, {
          "h" : 348,
          "resize" : "fit",
          "w" : 616
        }, {
          "h" : 348,
          "resize" : "fit",
          "w" : 616
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qAqxbPwd3l"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "435707231401959424",
    "text" : "Ahh, context, there you are MT @BBCWorld Separated but not alone. Syrian child refugee whose photo hit a nerve online http:\/\/t.co\/qAqxbPwd3l",
    "id" : 435707231401959424,
    "created_at" : "2014-02-18 09:27:53 +0000",
    "user" : {
      "name" : "Venetia Rainey",
      "screen_name" : "venetiarainey",
      "protected" : false,
      "id_str" : "181121575",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/785172631947444224\/jG3yhR0Y_normal.jpg",
      "id" : 181121575,
      "verified" : false
    }
  },
  "id" : 435727448337289216,
  "created_at" : "2014-02-18 10:48:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 54 ],
      "url" : "http:\/\/t.co\/9MpOZaegZo",
      "expanded_url" : "http:\/\/www.icij.org\/blog\/2013\/03\/fact-checking-social-media",
      "display_url" : "icij.org\/blog\/2013\/03\/f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435716403149017088",
  "text" : "HOW TO FACT-CHECK SOCIAL MEDIA? http:\/\/t.co\/9MpOZaegZo",
  "id" : 435716403149017088,
  "created_at" : "2014-02-18 10:04:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LensCulture",
      "screen_name" : "lensculture",
      "indices" : [ 3, 15 ],
      "id_str" : "23179472",
      "id" : 23179472
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/kbMILPGmjN",
      "expanded_url" : "http:\/\/bit.ly\/LCpreci",
      "display_url" : "bit.ly\/LCpreci"
    } ]
  },
  "geo" : { },
  "id_str" : "435714798282899456",
  "text" : "RT @lensculture: LOOK: Some animals are \"manufactured\" as food, while others are pampered as pets http:\/\/t.co\/kbMILPGmjN Jan Van IJken http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lensculture\/status\/435698781472821248\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/sfcLIcARMT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Bgvpu7tCEAAlM-b.jpg",
        "id_str" : "435698781481209856",
        "id" : 435698781481209856,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bgvpu7tCEAAlM-b.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 434,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sfcLIcARMT"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 103 ],
        "url" : "http:\/\/t.co\/kbMILPGmjN",
        "expanded_url" : "http:\/\/bit.ly\/LCpreci",
        "display_url" : "bit.ly\/LCpreci"
      } ]
    },
    "geo" : { },
    "id_str" : "435698781472821248",
    "text" : "LOOK: Some animals are \"manufactured\" as food, while others are pampered as pets http:\/\/t.co\/kbMILPGmjN Jan Van IJken http:\/\/t.co\/sfcLIcARMT",
    "id" : 435698781472821248,
    "created_at" : "2014-02-18 08:54:19 +0000",
    "user" : {
      "name" : "LensCulture",
      "screen_name" : "lensculture",
      "protected" : false,
      "id_str" : "23179472",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877172804625608705\/hZX_mk4a_normal.jpg",
      "id" : 23179472,
      "verified" : false
    }
  },
  "id" : 435714798282899456,
  "created_at" : "2014-02-18 09:57:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 121 ],
      "url" : "http:\/\/t.co\/Mhveo8Q5fM",
      "expanded_url" : "http:\/\/www.slideshare.net\/wearesocialsg\/social-digital-mobile-in-europe",
      "display_url" : "slideshare.net\/wearesocialsg\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435685724390572032",
  "text" : "Data, statistics, facts &amp; behavioural indicators to understand the online ecosystem in Europe. http:\/\/t.co\/Mhveo8Q5fM",
  "id" : 435685724390572032,
  "created_at" : "2014-02-18 08:02:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 141 ],
      "url" : "http:\/\/t.co\/RMCd5yUPg6",
      "expanded_url" : "http:\/\/www.nytimes.com\/2014\/02\/16\/technology\/intels-sharp-eyed-social-scientist.html?ref=todayspaper&_r=0",
      "display_url" : "nytimes.com\/2014\/02\/16\/tec\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435680849166487552",
  "text" : "Intel anthropologist, traveled around the world, examining, logging &amp; photographing the contents of people\u2019s cars. http:\/\/t.co\/RMCd5yUPg6",
  "id" : 435680849166487552,
  "created_at" : "2014-02-18 07:43:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/8QBJtrnaOM",
      "expanded_url" : "http:\/\/explore.noodle.org\/post\/76982293225\/i-dont-know-what-it-means-to-be-happy-pleasure",
      "display_url" : "explore.noodle.org\/post\/769822932\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435552586515906560",
  "text" : "The late Philip Seymour Hoffman on happiness http:\/\/t.co\/8QBJtrnaOM",
  "id" : 435552586515906560,
  "created_at" : "2014-02-17 23:13:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0421\u0442\u0435\u043F\u0430\u043D\u0447\u0443\u043A \u041F\u0430\u0432\u0435\u043B",
      "screen_name" : "BI_GettingThere",
      "indices" : [ 98, 114 ],
      "id_str" : "2833370901",
      "id" : 2833370901
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 93 ],
      "url" : "http:\/\/t.co\/O3k3oUHuFl",
      "expanded_url" : "http:\/\/www.businessinsider.com\/singapore-changi-airport-photos-2014-2",
      "display_url" : "businessinsider.com\/singapore-chan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435458056013369344",
  "text" : "We Visited Singapore's Outrageous Airport, And Were Totally Blown Away http:\/\/t.co\/O3k3oUHuFl via @bi_gettingthere",
  "id" : 435458056013369344,
  "created_at" : "2014-02-17 16:57:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/PYAJXdYc1A",
      "expanded_url" : "https:\/\/fullfact.org\/factchecks\/nhs_waiting_lists_numbers_patients_rising-29507",
      "display_url" : "fullfact.org\/factchecks\/nhs\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435457607545782272",
  "text" : "Guardian reported an increase in the no. of patients on waiting lists, but is this a fair comparison? https:\/\/t.co\/PYAJXdYc1A",
  "id" : 435457607545782272,
  "created_at" : "2014-02-17 16:55:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "435449124670808064",
  "text" : "enjoying \u51AC\u5929\u91CC\u7684\u4E00\u628A\u706B on Youtube. RIP",
  "id" : 435449124670808064,
  "created_at" : "2014-02-17 16:22:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marc Burrows \u26A1\uFE0F",
      "screen_name" : "20thcenturymarc",
      "indices" : [ 3, 19 ],
      "id_str" : "17866309",
      "id" : 17866309
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "435406806119366656",
  "text" : "RT @20thcenturymarc: Dear Express: Helen Mirren's dad was a Russian immigrant, Cowell's grandmother was polish &amp; Cliff was born in India ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/suttonnick\/status\/435176927079305216\/photo\/1",
        "indices" : [ 120, 142 ],
        "url" : "http:\/\/t.co\/UmLhEjgpwe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BgoPG_wCcAAhxTb.jpg",
        "id_str" : "435176926861225984",
        "id" : 435176926861225984,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BgoPG_wCcAAhxTb.jpg",
        "sizes" : [ {
          "h" : 1529,
          "resize" : "fit",
          "w" : 1195
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 531
        }, {
          "h" : 1529,
          "resize" : "fit",
          "w" : 1195
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 938
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UmLhEjgpwe"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "435362714807197696",
    "text" : "Dear Express: Helen Mirren's dad was a Russian immigrant, Cowell's grandmother was polish &amp; Cliff was born in India http:\/\/t.co\/UmLhEjgpwe",
    "id" : 435362714807197696,
    "created_at" : "2014-02-17 10:38:54 +0000",
    "user" : {
      "name" : "Marc Burrows \u26A1\uFE0F",
      "screen_name" : "20thcenturymarc",
      "protected" : false,
      "id_str" : "17866309",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/888341113135824896\/2jEskyVD_normal.jpg",
      "id" : 17866309,
      "verified" : false
    }
  },
  "id" : 435406806119366656,
  "created_at" : "2014-02-17 13:34:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/iXClWCZQRc",
      "expanded_url" : "https:\/\/twitter.com\/BBCSporf\/status\/435386749900963842",
      "display_url" : "twitter.com\/BBCSporf\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435405731060133888",
  "text" : "Sunshine! please read the user guide https:\/\/t.co\/iXClWCZQRc",
  "id" : 435405731060133888,
  "created_at" : "2014-02-17 13:29:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/IZDAaRlpxS",
      "expanded_url" : "https:\/\/twitter.com\/barryhand\/status\/435400893773905920",
      "display_url" : "twitter.com\/barryhand\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435402181001232384",
  "text" : "Let \"...shake them down for a few quid\". https:\/\/t.co\/IZDAaRlpxS",
  "id" : 435402181001232384,
  "created_at" : "2014-02-17 13:15:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shelley Bowdler-Olagbaiye",
      "screen_name" : "shelleybowdler",
      "indices" : [ 3, 18 ],
      "id_str" : "1140029412",
      "id" : 1140029412
    }, {
      "name" : "Forbes Tech",
      "screen_name" : "ForbesTech",
      "indices" : [ 116, 127 ],
      "id_str" : "14885549",
      "id" : 14885549
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Instagram",
      "indices" : [ 54, 64 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "435387932384718848",
  "text" : "RT @shelleybowdler: Visual commerce = conversion. Why #Instagram is a powerful tool for omni-channel retailers, via @ForbesTech: http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Forbes Tech",
        "screen_name" : "ForbesTech",
        "indices" : [ 96, 107 ],
        "id_str" : "14885549",
        "id" : 14885549
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Instagram",
        "indices" : [ 34, 44 ]
      } ],
      "urls" : [ {
        "indices" : [ 109, 131 ],
        "url" : "http:\/\/t.co\/EJ7MbUIRxA",
        "expanded_url" : "http:\/\/onforb.es\/1dCx9cY",
        "display_url" : "onforb.es\/1dCx9cY"
      } ]
    },
    "geo" : { },
    "id_str" : "435384455851552769",
    "text" : "Visual commerce = conversion. Why #Instagram is a powerful tool for omni-channel retailers, via @ForbesTech: http:\/\/t.co\/EJ7MbUIRxA",
    "id" : 435384455851552769,
    "created_at" : "2014-02-17 12:05:17 +0000",
    "user" : {
      "name" : "Shelley Bowdler-Olagbaiye",
      "screen_name" : "shelleybowdler",
      "protected" : false,
      "id_str" : "1140029412",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039157241688084482\/6MwkEFC2_normal.jpg",
      "id" : 1140029412,
      "verified" : false
    }
  },
  "id" : 435387932384718848,
  "created_at" : "2014-02-17 12:19:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http:\/\/t.co\/hzmpxqxxNm",
      "expanded_url" : "http:\/\/www.npr.org\/2014\/02\/14\/276574225\/harried-mom-becomes-dynamic-woman-in-these-stock-images?utm_content=socialflow&utm_campaign=nprfacebook&utm_source=npr&utm_medium=facebook",
      "display_url" : "npr.org\/2014\/02\/14\/276\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435036760524226560",
  "text" : "RT @MylinhCheung: Harried mom becomes dynamic woman in these stock images http:\/\/t.co\/hzmpxqxxNm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 78 ],
        "url" : "http:\/\/t.co\/hzmpxqxxNm",
        "expanded_url" : "http:\/\/www.npr.org\/2014\/02\/14\/276574225\/harried-mom-becomes-dynamic-woman-in-these-stock-images?utm_content=socialflow&utm_campaign=nprfacebook&utm_source=npr&utm_medium=facebook",
        "display_url" : "npr.org\/2014\/02\/14\/276\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "434514288103813120",
    "text" : "Harried mom becomes dynamic woman in these stock images http:\/\/t.co\/hzmpxqxxNm",
    "id" : 434514288103813120,
    "created_at" : "2014-02-15 02:27:33 +0000",
    "user" : {
      "name" : "Mylinh Lee",
      "screen_name" : "Mylinh_Lee",
      "protected" : false,
      "id_str" : "14672537",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/672267345658679296\/uTkcbMHM_normal.jpg",
      "id" : 14672537,
      "verified" : false
    }
  },
  "id" : 435036760524226560,
  "created_at" : "2014-02-16 13:03:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/nOqOHKEsMH",
      "expanded_url" : "http:\/\/www.techinasia.com\/wechat-21-million-taxi-rides-booked\/?utm_content=buffer48bd6&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer",
      "display_url" : "techinasia.com\/wechat-21-mill\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "435035688614891521",
  "text" : "21 million taxi rides have been booked on WeChat (popular messaging app in China) in the past month. http:\/\/t.co\/nOqOHKEsMH",
  "id" : 435035688614891521,
  "created_at" : "2014-02-16 12:59:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http:\/\/t.co\/QbkNvK2A1B",
      "expanded_url" : "http:\/\/fw.to\/I0GC7yj",
      "display_url" : "fw.to\/I0GC7yj"
    } ]
  },
  "geo" : { },
  "id_str" : "435019714062860288",
  "text" : "Have Sweden\u2019s permissive parents given birth to a generation of monsters? http:\/\/t.co\/QbkNvK2A1B",
  "id" : 435019714062860288,
  "created_at" : "2014-02-16 11:55:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore Airshow",
      "screen_name" : "SGAirshow",
      "indices" : [ 3, 13 ],
      "id_str" : "215931999",
      "id" : 215931999
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SGAirshow",
      "indices" : [ 15, 25 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434977261893480448",
  "text" : "RT @SGAirshow: #SGAirshow 2014 has come to an end! We thank you for all your support, and hope to see you again in 2016!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SGAirshow",
        "indices" : [ 0, 10 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "434976668675895296",
    "text" : "#SGAirshow 2014 has come to an end! We thank you for all your support, and hope to see you again in 2016!",
    "id" : 434976668675895296,
    "created_at" : "2014-02-16 09:04:53 +0000",
    "user" : {
      "name" : "Singapore Airshow",
      "screen_name" : "SGAirshow",
      "protected" : false,
      "id_str" : "215931999",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2609109533\/image_normal.jpg",
      "id" : 215931999,
      "verified" : false
    }
  },
  "id" : 434977261893480448,
  "created_at" : "2014-02-16 09:07:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434880411739496450",
  "text" : "So many places to establish your online outlet...overwhelmed.",
  "id" : 434880411739496450,
  "created_at" : "2014-02-16 02:42:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LinkedIn",
      "screen_name" : "LinkedIn",
      "indices" : [ 69, 78 ],
      "id_str" : "13058772",
      "id" : 13058772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434873902217441280",
  "text" : "You still cannot attached an image to Linkedin Group discussions! CC @LinkedIn",
  "id" : 434873902217441280,
  "created_at" : "2014-02-16 02:16:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SGAirshow",
      "indices" : [ 69, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/JNch7pnlBo",
      "expanded_url" : "https:\/\/www.flickr.com\/photos\/jordan_tan\/12521831213\/",
      "display_url" : "flickr.com\/photos\/jordan_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "434833482590605312",
  "text" : "Weapon payload on a Singapore Air force F15 https:\/\/t.co\/JNch7pnlBo  #SGAirshow",
  "id" : 434833482590605312,
  "created_at" : "2014-02-15 23:35:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 0, 10 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "434806987193593857",
  "geo" : { },
  "id_str" : "434810720472297472",
  "in_reply_to_user_id" : 10737,
  "text" : "@patphelan You are not alone.",
  "id" : 434810720472297472,
  "in_reply_to_status_id" : 434806987193593857,
  "created_at" : "2014-02-15 22:05:28 +0000",
  "in_reply_to_screen_name" : "patphelan",
  "in_reply_to_user_id_str" : "10737",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Jarvis",
      "screen_name" : "jeremyjarvis",
      "indices" : [ 3, 16 ],
      "id_str" : "12111842",
      "id" : 12111842
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jeremyjarvis\/status\/428848527226437632\/photo\/1",
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/HypLL3Cnye",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BfOTdpkIUAA0UXA.jpg",
      "id_str" : "428848527113211904",
      "id" : 428848527113211904,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfOTdpkIUAA0UXA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1023
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1023
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1023
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 679
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HypLL3Cnye"
    } ],
    "hashtags" : [ {
      "text" : "monkigras",
      "indices" : [ 82, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434808294776270848",
  "text" : "RT @jeremyjarvis: \"A data scientist is a statistician who lives in San Fransisco\" #monkigras http:\/\/t.co\/HypLL3Cnye",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jeremyjarvis\/status\/428848527226437632\/photo\/1",
        "indices" : [ 75, 97 ],
        "url" : "http:\/\/t.co\/HypLL3Cnye",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BfOTdpkIUAA0UXA.jpg",
        "id_str" : "428848527113211904",
        "id" : 428848527113211904,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfOTdpkIUAA0UXA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1023
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1023
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1023
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 679
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HypLL3Cnye"
      } ],
      "hashtags" : [ {
        "text" : "monkigras",
        "indices" : [ 64, 74 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "428848527226437632",
    "text" : "\"A data scientist is a statistician who lives in San Fransisco\" #monkigras http:\/\/t.co\/HypLL3Cnye",
    "id" : 428848527226437632,
    "created_at" : "2014-01-30 11:13:51 +0000",
    "user" : {
      "name" : "Jeremy Jarvis",
      "screen_name" : "jeremyjarvis",
      "protected" : false,
      "id_str" : "12111842",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/95172444\/Me_small_bw_normal.jpg",
      "id" : 12111842,
      "verified" : false
    }
  },
  "id" : 434808294776270848,
  "created_at" : "2014-02-15 21:55:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Airbus",
      "screen_name" : "Airbus",
      "indices" : [ 3, 10 ],
      "id_str" : "15425377",
      "id" : 15425377
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SGAirshow",
      "indices" : [ 123, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434803820858982402",
  "text" : "RT @Airbus: During the Singapore Airshow we received 83 firm orders worth almost US$15 billion and options for 30 aircraft #SGAirshow",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SGAirshow",
        "indices" : [ 111, 121 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "434340582790414337",
    "text" : "During the Singapore Airshow we received 83 firm orders worth almost US$15 billion and options for 30 aircraft #SGAirshow",
    "id" : 434340582790414337,
    "created_at" : "2014-02-14 14:57:19 +0000",
    "user" : {
      "name" : "Airbus",
      "screen_name" : "Airbus",
      "protected" : false,
      "id_str" : "15425377",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007497242264076293\/XLNMqJRx_normal.jpg",
      "id" : 15425377,
      "verified" : true
    }
  },
  "id" : 434803820858982402,
  "created_at" : "2014-02-15 21:38:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/zRIcNZzH0U",
      "expanded_url" : "https:\/\/twitter.com\/gavreilly\/status\/434727408068222977",
      "display_url" : "twitter.com\/gavreilly\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "434741179112882177",
  "text" : "How much is this exercise going to cost the taxpayer? https:\/\/t.co\/zRIcNZzH0U",
  "id" : 434741179112882177,
  "created_at" : "2014-02-15 17:29:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434736456045563905",
  "text" : "The new google map need some time to get use to.",
  "id" : 434736456045563905,
  "created_at" : "2014-02-15 17:10:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonah Kessel",
      "screen_name" : "jonah_kessel",
      "indices" : [ 3, 16 ],
      "id_str" : "20946110",
      "id" : 20946110
    }, {
      "name" : "World Press Photo",
      "screen_name" : "WorldPressPhoto",
      "indices" : [ 35, 51 ],
      "id_str" : "191750596",
      "id" : 191750596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434625890015014912",
  "text" : "RT @jonah_kessel: \"More than 9% of @WorldPressPhoto finalists\u2019 images disqualified because of removing information in post-processing\" http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "World Press Photo",
        "screen_name" : "WorldPressPhoto",
        "indices" : [ 17, 33 ],
        "id_str" : "191750596",
        "id" : 191750596
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/DXm3Dzaqm8",
        "expanded_url" : "http:\/\/buff.ly\/1nAYTzN",
        "display_url" : "buff.ly\/1nAYTzN"
      } ]
    },
    "geo" : { },
    "id_str" : "434624294178811904",
    "text" : "\"More than 9% of @WorldPressPhoto finalists\u2019 images disqualified because of removing information in post-processing\" http:\/\/t.co\/DXm3Dzaqm8",
    "id" : 434624294178811904,
    "created_at" : "2014-02-15 09:44:41 +0000",
    "user" : {
      "name" : "Jonah Kessel",
      "screen_name" : "jonah_kessel",
      "protected" : false,
      "id_str" : "20946110",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/562644586749624320\/--vY61VJ_normal.jpeg",
      "id" : 20946110,
      "verified" : true
    }
  },
  "id" : 434625890015014912,
  "created_at" : "2014-02-15 09:51:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/yza8sCeTMQ",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/Battle_of_Singapore",
      "display_url" : "en.wikipedia.org\/wiki\/Battle_of\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "434622566095802368",
  "text" : "Churchill called the fall of Singapore the \"worst disaster\" &amp; \"largest capitulation\" in British military. http:\/\/t.co\/yza8sCeTMQ",
  "id" : 434622566095802368,
  "created_at" : "2014-02-15 09:37:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434620955696975872",
  "text" : "Sorry for my last tweet on Abraham Lincoln quote. I should modify a bit and include women.",
  "id" : 434620955696975872,
  "created_at" : "2014-02-15 09:31:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434620654092951552",
  "text" : "\"Nearly all men can stand adversity, but if you want to test a man's character, give him power.\" - Abraham Lincoln",
  "id" : 434620654092951552,
  "created_at" : "2014-02-15 09:30:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JamesCorbett",
      "screen_name" : "eirepreneur",
      "indices" : [ 0, 12 ],
      "id_str" : "9066422",
      "id" : 9066422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "434616675686948864",
  "geo" : { },
  "id_str" : "434617314353217536",
  "in_reply_to_user_id" : 10142,
  "text" : "@eirepreneur beside this t-junction, I think there is a hotel also.",
  "id" : 434617314353217536,
  "in_reply_to_status_id" : 434616675686948864,
  "created_at" : "2014-02-15 09:16:57 +0000",
  "in_reply_to_screen_name" : "JamesCorbett",
  "in_reply_to_user_id_str" : "10142",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JamesCorbett",
      "screen_name" : "eirepreneur",
      "indices" : [ 0, 12 ],
      "id_str" : "9066422",
      "id" : 9066422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "434064445027807232",
  "geo" : { },
  "id_str" : "434615904463093761",
  "in_reply_to_user_id" : 10142,
  "text" : "@eirepreneur Thank you. Now I know the name of the place.",
  "id" : 434615904463093761,
  "in_reply_to_status_id" : 434064445027807232,
  "created_at" : "2014-02-15 09:11:21 +0000",
  "in_reply_to_screen_name" : "JamesCorbett",
  "in_reply_to_user_id_str" : "10142",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mishal Husain",
      "screen_name" : "MishalHusainBBC",
      "indices" : [ 3, 19 ],
      "id_str" : "74265849",
      "id" : 74265849
    }, {
      "name" : "BBC Radio 4 Today",
      "screen_name" : "BBCr4today",
      "indices" : [ 83, 94 ],
      "id_str" : "8170292",
      "id" : 8170292
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434612757045211136",
  "text" : "RT @MishalHusainBBC: To back up our attempt to talk visual effects on the radio:\nRT@BBCr4today: WATCH: How animation was used for Gravity h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC Radio 4 Today",
        "screen_name" : "BBCr4today",
        "indices" : [ 62, 73 ],
        "id_str" : "8170292",
        "id" : 8170292
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/XImQrEs2Ck",
        "expanded_url" : "http:\/\/bbc.in\/1g6b6tm",
        "display_url" : "bbc.in\/1g6b6tm"
      } ]
    },
    "in_reply_to_status_id_str" : "434609749665910784",
    "geo" : { },
    "id_str" : "434611033240784896",
    "in_reply_to_user_id" : 8170292,
    "text" : "To back up our attempt to talk visual effects on the radio:\nRT@BBCr4today: WATCH: How animation was used for Gravity http:\/\/t.co\/XImQrEs2Ck",
    "id" : 434611033240784896,
    "in_reply_to_status_id" : 434609749665910784,
    "created_at" : "2014-02-15 08:51:59 +0000",
    "in_reply_to_screen_name" : "BBCr4today",
    "in_reply_to_user_id_str" : "8170292",
    "user" : {
      "name" : "Mishal Husain",
      "screen_name" : "MishalHusainBBC",
      "protected" : false,
      "id_str" : "74265849",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1641915046\/mishal_normal.jpg",
      "id" : 74265849,
      "verified" : true
    }
  },
  "id" : 434612757045211136,
  "created_at" : "2014-02-15 08:58:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel 4 News",
      "screen_name" : "Channel4News",
      "indices" : [ 3, 16 ],
      "id_str" : "14569869",
      "id" : 14569869
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "c4news",
      "indices" : [ 94, 101 ]
    } ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/UJjTZwrnBV",
      "expanded_url" : "http:\/\/www.channel4.com\/news\/floods-british-government-turned-down-dutch-help",
      "display_url" : "channel4.com\/news\/floods-br\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "434612284619763712",
  "text" : "RT @Channel4News: The British government turned down some Dutch emergency help on the floods, #c4news can reveal: http:\/\/t.co\/UJjTZwrnBV #f\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "c4news",
        "indices" : [ 76, 83 ]
      }, {
        "text" : "floods",
        "indices" : [ 119, 126 ]
      } ],
      "urls" : [ {
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/UJjTZwrnBV",
        "expanded_url" : "http:\/\/www.channel4.com\/news\/floods-british-government-turned-down-dutch-help",
        "display_url" : "channel4.com\/news\/floods-br\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "434382779149582336",
    "text" : "The British government turned down some Dutch emergency help on the floods, #c4news can reveal: http:\/\/t.co\/UJjTZwrnBV #floods",
    "id" : 434382779149582336,
    "created_at" : "2014-02-14 17:44:59 +0000",
    "user" : {
      "name" : "Channel 4 News",
      "screen_name" : "Channel4News",
      "protected" : false,
      "id_str" : "14569869",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875404558528393216\/cTknVhwm_normal.jpg",
      "id" : 14569869,
      "verified" : true
    }
  },
  "id" : 434612284619763712,
  "created_at" : "2014-02-15 08:56:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434611027381346304",
  "text" : "people complain Ryanair again. If you can afford, why aren't you taking aer lingus then? (For the record, poor people like me took Ryanair)",
  "id" : 434611027381346304,
  "created_at" : "2014-02-15 08:51:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u041F\u0451\u0442\u0440 \u041C\u0430\u0440\u0442\u044B\u043D\u043E\u0432",
      "screen_name" : "natlmuseum_sg",
      "indices" : [ 3, 17 ],
      "id_str" : "2325264697",
      "id" : 2325264697
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434607839580282880",
  "text" : "RT @natlmuseum_sg: Just heard the sirens sounding, here's to the 72nd anniversary of the fall of Singapore and 30 years of Total Defence.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "434539367361564674",
    "text" : "Just heard the sirens sounding, here's to the 72nd anniversary of the fall of Singapore and 30 years of Total Defence.",
    "id" : 434539367361564674,
    "created_at" : "2014-02-15 04:07:13 +0000",
    "user" : {
      "name" : "National Museum",
      "screen_name" : "natmuseum_sg",
      "protected" : false,
      "id_str" : "191701740",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998405998023880706\/7_dvfVbc_normal.jpg",
      "id" : 191701740,
      "verified" : false
    }
  },
  "id" : 434607839580282880,
  "created_at" : "2014-02-15 08:39:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Evil Tofu",
      "screen_name" : "eviltofu",
      "indices" : [ 0, 9 ],
      "id_str" : "1730901",
      "id" : 1730901
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "434572066977619968",
  "geo" : { },
  "id_str" : "434607569211228160",
  "in_reply_to_user_id" : 1730901,
  "text" : "@eviltofu Pick it up and eat. It more tasty when the sausage come in contact with the floor.",
  "id" : 434607569211228160,
  "in_reply_to_status_id" : 434572066977619968,
  "created_at" : "2014-02-15 08:38:13 +0000",
  "in_reply_to_screen_name" : "eviltofu",
  "in_reply_to_user_id_str" : "1730901",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Jarrett",
      "screen_name" : "cjforms",
      "indices" : [ 3, 11 ],
      "id_str" : "144605504",
      "id" : 144605504
    }, {
      "name" : "Whitney Quesenbery",
      "screen_name" : "whitneyq",
      "indices" : [ 130, 139 ],
      "id_str" : "16949684",
      "id" : 16949684
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "uxgov",
      "indices" : [ 118, 124 ]
    } ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http:\/\/t.co\/0M2wiqU0rF",
      "expanded_url" : "http:\/\/Howto.gov",
      "display_url" : "Howto.gov"
    }, {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/wycWkberKj",
      "expanded_url" : "http:\/\/1.usa.gov\/1lOuVJF",
      "display_url" : "1.usa.gov\/1lOuVJF"
    } ]
  },
  "geo" : { },
  "id_str" : "434372184514248704",
  "text" : "RT @cjforms: http:\/\/t.co\/0M2wiqU0rF gallery of before-and-after results from usability testing:http:\/\/t.co\/wycWkberKj #uxgov (via @whitneyq)",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Whitney Quesenbery",
        "screen_name" : "whitneyq",
        "indices" : [ 117, 126 ],
        "id_str" : "16949684",
        "id" : 16949684
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "uxgov",
        "indices" : [ 105, 111 ]
      } ],
      "urls" : [ {
        "indices" : [ 0, 22 ],
        "url" : "http:\/\/t.co\/0M2wiqU0rF",
        "expanded_url" : "http:\/\/Howto.gov",
        "display_url" : "Howto.gov"
      }, {
        "indices" : [ 82, 104 ],
        "url" : "http:\/\/t.co\/wycWkberKj",
        "expanded_url" : "http:\/\/1.usa.gov\/1lOuVJF",
        "display_url" : "1.usa.gov\/1lOuVJF"
      } ]
    },
    "geo" : { },
    "id_str" : "434360885050478593",
    "text" : "http:\/\/t.co\/0M2wiqU0rF gallery of before-and-after results from usability testing:http:\/\/t.co\/wycWkberKj #uxgov (via @whitneyq)",
    "id" : 434360885050478593,
    "created_at" : "2014-02-14 16:17:59 +0000",
    "user" : {
      "name" : "Caroline Jarrett",
      "screen_name" : "cjforms",
      "protected" : false,
      "id_str" : "144605504",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528613343381032960\/aFnqmqcK_normal.jpeg",
      "id" : 144605504,
      "verified" : false
    }
  },
  "id" : 434372184514248704,
  "created_at" : "2014-02-14 17:02:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mason Hayes & Curran",
      "screen_name" : "MHCLawyers",
      "indices" : [ 3, 14 ],
      "id_str" : "330404048",
      "id" : 330404048
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434370070698295296",
  "text" : "RT @MHCLawyers: A judgment was made this week that the hyperlinking of content is not copyright infringement. Read more here: http:\/\/t.co\/b\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/buNJk0vouw",
        "expanded_url" : "http:\/\/buff.ly\/1bvlw8U",
        "display_url" : "buff.ly\/1bvlw8U"
      } ]
    },
    "geo" : { },
    "id_str" : "434362530467950592",
    "text" : "A judgment was made this week that the hyperlinking of content is not copyright infringement. Read more here: http:\/\/t.co\/buNJk0vouw",
    "id" : 434362530467950592,
    "created_at" : "2014-02-14 16:24:32 +0000",
    "user" : {
      "name" : "Mason Hayes & Curran",
      "screen_name" : "MHCLawyers",
      "protected" : false,
      "id_str" : "330404048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1563621571\/mhc_main_logo_normal.gif",
      "id" : 330404048,
      "verified" : false
    }
  },
  "id" : 434370070698295296,
  "created_at" : "2014-02-14 16:54:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Findery",
      "screen_name" : "findery",
      "indices" : [ 115, 123 ],
      "id_str" : "479840400",
      "id" : 479840400
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/Z9AG8evfWa",
      "expanded_url" : "https:\/\/findery.com\/openplaques\/notes\/lafcadio-hearn-brown-plaque-in-dublin",
      "display_url" : "findery.com\/openplaques\/no\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "434369891332681728",
  "text" : "Lafcadio Hearn, famous for his writings on Japan lived in Dublin during his childhood. https:\/\/t.co\/Z9AG8evfWa via @findery",
  "id" : 434369891332681728,
  "created_at" : "2014-02-14 16:53:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434333024407138305",
  "text" : "M&amp;S Ireland have this Dine-In Menu for EUR 28 including a bottle of wine.",
  "id" : 434333024407138305,
  "created_at" : "2014-02-14 14:27:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/5r2pZfTT5Y",
      "expanded_url" : "https:\/\/www.google.com\/maps\/views\/view\/streetview\/business-highlights\/emirates-a380-view-1\/XxG0IKbwvOAAAAQIt3wkPw?gl=sg&heading=34&pitch=67&fovy=75",
      "display_url" : "google.com\/maps\/views\/vie\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "434264211925897216",
  "text" : "Explore the luxurious Emirates A380 via Google Street View! https:\/\/t.co\/5r2pZfTT5Y",
  "id" : 434264211925897216,
  "created_at" : "2014-02-14 09:53:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http:\/\/t.co\/w2imQ5klgF",
      "expanded_url" : "http:\/\/www.singaporeairshow.com\/",
      "display_url" : "singaporeairshow.com"
    } ]
  },
  "geo" : { },
  "id_str" : "434263719602708481",
  "text" : "As an aviation geek, this is one thing I miss back home. http:\/\/t.co\/w2imQ5klgF",
  "id" : 434263719602708481,
  "created_at" : "2014-02-14 09:51:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kevin Lim (PhD)",
      "screen_name" : "brainopera",
      "indices" : [ 3, 14 ],
      "id_str" : "22823",
      "id" : 22823
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/sfpol09FzA",
      "expanded_url" : "http:\/\/ift.tt\/1g1yHvd",
      "display_url" : "ift.tt\/1g1yHvd"
    } ]
  },
  "geo" : { },
  "id_str" : "434089940260233216",
  "text" : "RT @brainopera: The Narrative Clip changes everything. Where I once developed a networkable backpack whi... http:\/\/t.co\/sfpol09FzA http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/brainopera\/status\/433987892596453376\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/7HeUjYUmIC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BgXVsFdIAAATqtm.jpg",
        "id_str" : "433987892466417664",
        "id" : 433987892466417664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BgXVsFdIAAATqtm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/7HeUjYUmIC"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/sfpol09FzA",
        "expanded_url" : "http:\/\/ift.tt\/1g1yHvd",
        "display_url" : "ift.tt\/1g1yHvd"
      } ]
    },
    "geo" : { },
    "id_str" : "433987892596453376",
    "text" : "The Narrative Clip changes everything. Where I once developed a networkable backpack whi... http:\/\/t.co\/sfpol09FzA http:\/\/t.co\/7HeUjYUmIC",
    "id" : 433987892596453376,
    "created_at" : "2014-02-13 15:35:51 +0000",
    "user" : {
      "name" : "Kevin Lim (PhD)",
      "screen_name" : "brainopera",
      "protected" : false,
      "id_str" : "22823",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/820467861936369664\/gAir1s2g_normal.jpg",
      "id" : 22823,
      "verified" : false
    }
  },
  "id" : 434089940260233216,
  "created_at" : "2014-02-13 22:21:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "434027709077798912",
  "text" : "Any figure skater executing their move to the tune of Let it Go (Frozen)?",
  "id" : 434027709077798912,
  "created_at" : "2014-02-13 18:14:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http:\/\/t.co\/ElkXeubzaq",
      "expanded_url" : "http:\/\/gistflow.com\/",
      "display_url" : "gistflow.com"
    } ]
  },
  "geo" : { },
  "id_str" : "434024613106810880",
  "text" : "A blog platform just for developer http:\/\/t.co\/ElkXeubzaq",
  "id" : 434024613106810880,
  "created_at" : "2014-02-13 18:01:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 105 ],
      "url" : "http:\/\/t.co\/mQRSC5Iw8e",
      "expanded_url" : "http:\/\/www.wired.com\/autopia\/2014\/02\/renault-kwid\/?cid=co18194054&utm_campaign=Feed%3A%20wiredautopia%20%28Wired%3A%20Blog%20-%20Autopia%29&utm_medium=feed&utm_source=feedburner#slide-id-153971",
      "display_url" : "wired.com\/autopia\/2014\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "433998777133711360",
  "text" : "This one is good to seek out feral kids down the road who throw stone at your car. http:\/\/t.co\/mQRSC5Iw8e",
  "id" : 433998777133711360,
  "created_at" : "2014-02-13 16:19:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433973312440369153",
  "text" : "The pic on the left of that ad is a sweet glutinous rice ball brewed in a soup eaten on the 15th day of Lunar New Year.",
  "id" : 433973312440369153,
  "created_at" : "2014-02-13 14:37:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/UQsNrmWIkW",
      "expanded_url" : "https:\/\/twitter.com\/krislc\/status\/433969089405407232",
      "display_url" : "twitter.com\/krislc\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "433972626080268288",
  "text" : "Fri is 15th day of the lunar new year (Yuanxiao Festival) or so-called Chinese Valentines day...The alleged Durex ad https:\/\/t.co\/UQsNrmWIkW",
  "id" : 433972626080268288,
  "created_at" : "2014-02-13 14:35:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433968218512707584",
  "text" : "Thanks. I no longer do wordpress website anymore. But I can advice.",
  "id" : 433968218512707584,
  "created_at" : "2014-02-13 14:17:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "smecommunity",
      "indices" : [ 79, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433962883769040896",
  "text" : "When did you last look at your google analytics data and make sense out of it? #smecommunity I can help.",
  "id" : 433962883769040896,
  "created_at" : "2014-02-13 13:56:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "smecommunity",
      "indices" : [ 93, 106 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433896892582199296",
  "text" : "For self-employed people, which Linkedin features is suitable? Group or Company Page? Thanks #smecommunity",
  "id" : 433896892582199296,
  "created_at" : "2014-02-13 09:34:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "D12",
      "indices" : [ 91, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433889166351286273",
  "text" : "Over night that storm has transform THE Superquinn store to Supervalu in my neighbourhood. #D12",
  "id" : 433889166351286273,
  "created_at" : "2014-02-13 09:03:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/avquFzkNCN",
      "expanded_url" : "http:\/\/www.foreignpolicy.com\/articles\/2014\/02\/11\/an_offer_they_cant_refuse",
      "display_url" : "foreignpolicy.com\/articles\/2014\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "433642397126623232",
  "text" : "RT @yapphenghui: An Offer They Can't Refuse  http:\/\/t.co\/avquFzkNCN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 50 ],
        "url" : "http:\/\/t.co\/avquFzkNCN",
        "expanded_url" : "http:\/\/www.foreignpolicy.com\/articles\/2014\/02\/11\/an_offer_they_cant_refuse",
        "display_url" : "foreignpolicy.com\/articles\/2014\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "433639837824860161",
    "text" : "An Offer They Can't Refuse  http:\/\/t.co\/avquFzkNCN",
    "id" : 433639837824860161,
    "created_at" : "2014-02-12 16:32:48 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 433642397126623232,
  "created_at" : "2014-02-12 16:42:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Miranda Green",
      "screen_name" : "greenmiranda",
      "indices" : [ 3, 16 ],
      "id_str" : "146366133",
      "id" : 146366133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433641703200010240",
  "text" : "RT @greenmiranda: Idea: why not rename the British Museum the Museum of World History? Then the Greeks, Egyptians etc cld have common owner\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "433632291298893824",
    "text" : "Idea: why not rename the British Museum the Museum of World History? Then the Greeks, Egyptians etc cld have common ownership of artefacts.",
    "id" : 433632291298893824,
    "created_at" : "2014-02-12 16:02:49 +0000",
    "user" : {
      "name" : "Miranda Green",
      "screen_name" : "greenmiranda",
      "protected" : false,
      "id_str" : "146366133",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014277861644128257\/k8Gii2IL_normal.jpg",
      "id" : 146366133,
      "verified" : false
    }
  },
  "id" : 433641703200010240,
  "created_at" : "2014-02-12 16:40:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stephen Holmes",
      "screen_name" : "nonsequitir",
      "indices" : [ 0, 12 ],
      "id_str" : "14266781",
      "id" : 14266781
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "433636887631515648",
  "geo" : { },
  "id_str" : "433638191720329216",
  "in_reply_to_user_id" : 14266781,
  "text" : "@nonsequitir At least you have your last wine...",
  "id" : 433638191720329216,
  "in_reply_to_status_id" : 433636887631515648,
  "created_at" : "2014-02-12 16:26:16 +0000",
  "in_reply_to_screen_name" : "nonsequitir",
  "in_reply_to_user_id_str" : "14266781",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    }, {
      "name" : "\u7F8E\u56FD\u4E4B\u97F3\u4E2D\u6587\u7F51",
      "screen_name" : "VOAChinese",
      "indices" : [ 23, 34 ],
      "id_str" : "8149482",
      "id" : 8149482
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433632294604398592",
  "text" : "RT @yapphenghui: \u592A\u5165\u6232\u7684\u60B2\u5287@VOAChinese: \u738B\u90C1\u7426\u5357\u4EAC\u63D0\u201C\u4E2D\u534E\u6C11\u56FD\u201D\uFF0C\u4E00\u65C1\u62CD\u624B\u6C11\u4F17\u88AB\u62C9\u8D70: \u53F0\u6E7E\u884C\u653F\u9662\u5927\u9646\u59D4\u5458\u4F1A\u4E3B\u4EFB\u59D4\u5458\u738B\u90C1\u74262\u670812\u65E5\u5728\u5357\u4EAC\u4E2D\u5C71\u9675\u81F4\u796D\u5B59\u4E2D\u5C71\uFF0C\u4ED6\u5728\u8C08\u8BDD\u4E2D\u63D0\u5230\u201D\u4E2D\u534E\u6C11\u56FD\u201D\uFF0C\u4E00\u65C1\u56F4\u89C2\u7684\u4E2D\u56FD\u5927\u9646\u6C11\u4F17\u62CD\u624B\u53EB\u597D\uFF0C\u9A6C\u4E0A\u906D\u5230\u4FDD\u5168\u62C9\u8D70\u3002 http:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\u7F8E\u56FD\u4E4B\u97F3\u4E2D\u6587\u7F51",
        "screen_name" : "VOAChinese",
        "indices" : [ 6, 17 ],
        "id_str" : "8149482",
        "id" : 8149482
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/jsMNwRAZII",
        "expanded_url" : "http:\/\/bit.ly\/1gth0Fy",
        "display_url" : "bit.ly\/1gth0Fy"
      } ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 1.2898639, 103.8289727 ]
    },
    "id_str" : "433631166294290433",
    "text" : "\u592A\u5165\u6232\u7684\u60B2\u5287@VOAChinese: \u738B\u90C1\u7426\u5357\u4EAC\u63D0\u201C\u4E2D\u534E\u6C11\u56FD\u201D\uFF0C\u4E00\u65C1\u62CD\u624B\u6C11\u4F17\u88AB\u62C9\u8D70: \u53F0\u6E7E\u884C\u653F\u9662\u5927\u9646\u59D4\u5458\u4F1A\u4E3B\u4EFB\u59D4\u5458\u738B\u90C1\u74262\u670812\u65E5\u5728\u5357\u4EAC\u4E2D\u5C71\u9675\u81F4\u796D\u5B59\u4E2D\u5C71\uFF0C\u4ED6\u5728\u8C08\u8BDD\u4E2D\u63D0\u5230\u201D\u4E2D\u534E\u6C11\u56FD\u201D\uFF0C\u4E00\u65C1\u56F4\u89C2\u7684\u4E2D\u56FD\u5927\u9646\u6C11\u4F17\u62CD\u624B\u53EB\u597D\uFF0C\u9A6C\u4E0A\u906D\u5230\u4FDD\u5168\u62C9\u8D70\u3002 http:\/\/t.co\/jsMNwRAZII",
    "id" : 433631166294290433,
    "created_at" : "2014-02-12 15:58:21 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 433632294604398592,
  "created_at" : "2014-02-12 16:02:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 127 ],
      "url" : "http:\/\/t.co\/eFWTfbDs6V",
      "expanded_url" : "http:\/\/www.redcross.ie\/news\/appeals\/ireland-floods-appeal\/",
      "display_url" : "redcross.ie\/news\/appeals\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "433631777320488961",
  "text" : "Storms and high tides have swamped homes around Ireland. You can help families who have lost everything. http:\/\/t.co\/eFWTfbDs6V",
  "id" : 433631777320488961,
  "created_at" : "2014-02-12 16:00:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433627297929646080",
  "text" : "With this kind of weather, bet the dublin city public swimming pool is empty...",
  "id" : 433627297929646080,
  "created_at" : "2014-02-12 15:42:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 104 ],
      "url" : "http:\/\/t.co\/ylF2ADZtsz",
      "expanded_url" : "http:\/\/lifehacker.com\/5902988\/use-springpad-as-your-new-personal-assistant-get-organized-save-money-and-have-fun-being-productive",
      "display_url" : "lifehacker.com\/5902988\/use-sp\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "433601755427774465",
  "text" : "\"The new Springpad is a little like Pinterest\u2014if Pinterest were insanely useful!\" http:\/\/t.co\/ylF2ADZtsz",
  "id" : 433601755427774465,
  "created_at" : "2014-02-12 14:01:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433594270562258944",
  "text" : "I spoke too soon.",
  "id" : 433594270562258944,
  "created_at" : "2014-02-12 13:31:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433592451178057730",
  "text" : "It dry now over in Dublin...",
  "id" : 433592451178057730,
  "created_at" : "2014-02-12 13:24:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Blog Awards 2018",
      "screen_name" : "BlogAwardsIE",
      "indices" : [ 0, 13 ],
      "id_str" : "534385230",
      "id" : 534385230
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "433563954284335104",
  "geo" : { },
  "id_str" : "433565190584410112",
  "in_reply_to_user_id" : 534385230,
  "text" : "@BlogAwardsIE Thanks. I be participating for this year :)",
  "id" : 433565190584410112,
  "in_reply_to_status_id" : 433563954284335104,
  "created_at" : "2014-02-12 11:36:11 +0000",
  "in_reply_to_screen_name" : "BlogAwardsIE",
  "in_reply_to_user_id_str" : "534385230",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Blog Awards 2018",
      "screen_name" : "BlogAwardsIE",
      "indices" : [ 0, 13 ],
      "id_str" : "534385230",
      "id" : 534385230
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "433561841311416320",
  "geo" : { },
  "id_str" : "433563793264627713",
  "in_reply_to_user_id" : 534385230,
  "text" : "@BlogAwardsIE Will there be new category introduce?",
  "id" : 433563793264627713,
  "in_reply_to_status_id" : 433561841311416320,
  "created_at" : "2014-02-12 11:30:38 +0000",
  "in_reply_to_screen_name" : "BlogAwardsIE",
  "in_reply_to_user_id_str" : "534385230",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Prosperity",
      "screen_name" : "happinessworks",
      "indices" : [ 3, 18 ],
      "id_str" : "24672931",
      "id" : 24672931
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433553240391966720",
  "text" : "RT @happinessworks: Are you a UX Consultant with 2-5 years' experience on large client solutions? We have THE role for you! have a look: ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/GwqLZblzxg",
        "expanded_url" : "http:\/\/www.prosperity.ie\/jobs\/930675187_ux_consultant_mobile.html",
        "display_url" : "prosperity.ie\/jobs\/930675187\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "433537219475697664",
    "text" : "Are you a UX Consultant with 2-5 years' experience on large client solutions? We have THE role for you! have a look: http:\/\/t.co\/GwqLZblzxg",
    "id" : 433537219475697664,
    "created_at" : "2014-02-12 09:45:02 +0000",
    "user" : {
      "name" : "Prosperity",
      "screen_name" : "happinessworks",
      "protected" : false,
      "id_str" : "24672931",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1501610309\/Twitter_avatar_normal.jpg",
      "id" : 24672931,
      "verified" : false
    }
  },
  "id" : 433553240391966720,
  "created_at" : "2014-02-12 10:48:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433411746070401024",
  "text" : "Revisited Springpad again. It better than Pinterest as you can use it to plan, organise &amp; share ideas.",
  "id" : 433411746070401024,
  "created_at" : "2014-02-12 01:26:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Apptimize",
      "screen_name" : "apptimizeAB",
      "indices" : [ 3, 15 ],
      "id_str" : "1529746926",
      "id" : 1529746926
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/r52BkJPMVp",
      "expanded_url" : "http:\/\/ow.ly\/twV9J",
      "display_url" : "ow.ly\/twV9J"
    } ]
  },
  "geo" : { },
  "id_str" : "433391087861133312",
  "text" : "RT @apptimizeAB: Whoa, Twitter is the 3rd major social network to publicize its A\/B testing in the last few weeks http:\/\/t.co\/r52BkJPMVp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 97, 119 ],
        "url" : "http:\/\/t.co\/r52BkJPMVp",
        "expanded_url" : "http:\/\/ow.ly\/twV9J",
        "display_url" : "ow.ly\/twV9J"
      } ]
    },
    "geo" : { },
    "id_str" : "433390032238678017",
    "text" : "Whoa, Twitter is the 3rd major social network to publicize its A\/B testing in the last few weeks http:\/\/t.co\/r52BkJPMVp",
    "id" : 433390032238678017,
    "created_at" : "2014-02-12 00:00:10 +0000",
    "user" : {
      "name" : "Apptimize",
      "screen_name" : "apptimizeAB",
      "protected" : false,
      "id_str" : "1529746926",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/443857061730721792\/PSg52cwo_normal.jpeg",
      "id" : 1529746926,
      "verified" : false
    }
  },
  "id" : 433391087861133312,
  "created_at" : "2014-02-12 00:04:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433389203607388162",
  "text" : "I am using pinterest, tumblr, getpocket, evernote, mammothhq to collect, organise and share online material. What do u use?",
  "id" : 433389203607388162,
  "created_at" : "2014-02-11 23:56:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433388425526255616",
  "text" : "What is your recommended tools to collect, organize and share your online material?",
  "id" : 433388425526255616,
  "created_at" : "2014-02-11 23:53:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Business Insider",
      "screen_name" : "businessinsider",
      "indices" : [ 3, 19 ],
      "id_str" : "20562637",
      "id" : 20562637
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/7MUodRksbe",
      "expanded_url" : "http:\/\/read.bi\/MIu4NW",
      "display_url" : "read.bi\/MIu4NW"
    } ]
  },
  "geo" : { },
  "id_str" : "433318941705109504",
  "text" : "RT @businessinsider: 10 things you didn't know about Satya Nadella, Microsoft's new CEO http:\/\/t.co\/7MUodRksbe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.businessinsider.com\" rel=\"nofollow\"\u003EBusiness Insider\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 89 ],
        "url" : "http:\/\/t.co\/7MUodRksbe",
        "expanded_url" : "http:\/\/read.bi\/MIu4NW",
        "display_url" : "read.bi\/MIu4NW"
      } ]
    },
    "geo" : { },
    "id_str" : "433287731993919488",
    "text" : "10 things you didn't know about Satya Nadella, Microsoft's new CEO http:\/\/t.co\/7MUodRksbe",
    "id" : 433287731993919488,
    "created_at" : "2014-02-11 17:13:40 +0000",
    "user" : {
      "name" : "Business Insider",
      "screen_name" : "businessinsider",
      "protected" : false,
      "id_str" : "20562637",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/887662979902304257\/azSzxYkB_normal.jpg",
      "id" : 20562637,
      "verified" : true
    }
  },
  "id" : 433318941705109504,
  "created_at" : "2014-02-11 19:17:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 113 ],
      "url" : "http:\/\/t.co\/LWkKvyvjID",
      "expanded_url" : "http:\/\/lnkd.in\/buK6p9N",
      "display_url" : "lnkd.in\/buK6p9N"
    } ]
  },
  "geo" : { },
  "id_str" : "433304631771688960",
  "text" : "RT @damacfad: Singapore: Historic mission boldly goes where Stormont has never gone before http:\/\/t.co\/LWkKvyvjID",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 99 ],
        "url" : "http:\/\/t.co\/LWkKvyvjID",
        "expanded_url" : "http:\/\/lnkd.in\/buK6p9N",
        "display_url" : "lnkd.in\/buK6p9N"
      } ]
    },
    "geo" : { },
    "id_str" : "433259292402012160",
    "text" : "Singapore: Historic mission boldly goes where Stormont has never gone before http:\/\/t.co\/LWkKvyvjID",
    "id" : 433259292402012160,
    "created_at" : "2014-02-11 15:20:39 +0000",
    "user" : {
      "name" : "D\u00E9agl\u00E1n MacPh\u00E1id\u00EDn",
      "screen_name" : "declanmacfadden",
      "protected" : false,
      "id_str" : "19288130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684614991492743168\/Rup-iIUi_normal.jpg",
      "id" : 19288130,
      "verified" : false
    }
  },
  "id" : 433304631771688960,
  "created_at" : "2014-02-11 18:20:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "laura kidd",
      "screen_name" : "kidd_laura",
      "indices" : [ 0, 11 ],
      "id_str" : "343965205",
      "id" : 343965205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "433045026570907649",
  "geo" : { },
  "id_str" : "433304320876883969",
  "in_reply_to_user_id" : 343965205,
  "text" : "@kidd_laura No worry. You might need to change your password.",
  "id" : 433304320876883969,
  "in_reply_to_status_id" : 433045026570907649,
  "created_at" : "2014-02-11 18:19:35 +0000",
  "in_reply_to_screen_name" : "kidd_laura",
  "in_reply_to_user_id_str" : "343965205",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/s0Ho65RRAI",
      "expanded_url" : "http:\/\/bit.ly\/M8qlsZ",
      "display_url" : "bit.ly\/M8qlsZ"
    } ]
  },
  "geo" : { },
  "id_str" : "433303706512408579",
  "text" : "RT @Suptderrick: This lady is someone's Mum Gran or neighbour  If you know something do something about it  http:\/\/t.co\/s0Ho65RRAI  http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CrimestoppersUK\/status\/433243492983119873\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/C4v5Hc8PfV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BgMwqVVCIAAevrd.jpg",
        "id_str" : "433243492995702784",
        "id" : 433243492995702784,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BgMwqVVCIAAevrd.jpg",
        "sizes" : [ {
          "h" : 416,
          "resize" : "fit",
          "w" : 618
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 416,
          "resize" : "fit",
          "w" : 618
        }, {
          "h" : 416,
          "resize" : "fit",
          "w" : 618
        }, {
          "h" : 416,
          "resize" : "fit",
          "w" : 618
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/C4v5Hc8PfV"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/s0Ho65RRAI",
        "expanded_url" : "http:\/\/bit.ly\/M8qlsZ",
        "display_url" : "bit.ly\/M8qlsZ"
      } ]
    },
    "geo" : { },
    "id_str" : "433293626974535680",
    "text" : "This lady is someone's Mum Gran or neighbour  If you know something do something about it  http:\/\/t.co\/s0Ho65RRAI  http:\/\/t.co\/C4v5Hc8PfV",
    "id" : 433293626974535680,
    "created_at" : "2014-02-11 17:37:05 +0000",
    "user" : {
      "name" : "Jane Derrick",
      "screen_name" : "ChSuptDerrick",
      "protected" : false,
      "id_str" : "298039294",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/951611083520991232\/wiLB3sEf_normal.jpg",
      "id" : 298039294,
      "verified" : false
    }
  },
  "id" : 433303706512408579,
  "created_at" : "2014-02-11 18:17:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433201525314187264",
  "text" : "This is handy. Do a screenshot and the file was saved automatically to my Dropbox folder. Thank You.",
  "id" : 433201525314187264,
  "created_at" : "2014-02-11 11:31:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eddie Du",
      "screen_name" : "Edourdoo",
      "indices" : [ 3, 12 ],
      "id_str" : "20044440",
      "id" : 20044440
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/HuffPostUK\/status\/433187411846115328\/photo\/1",
      "indices" : [ 84, 106 ],
      "url" : "http:\/\/t.co\/toUhs3b8uX",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BgL9p-BCQAAFnEF.jpg",
      "id_str" : "433187411644792832",
      "id" : 433187411644792832,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BgL9p-BCQAAFnEF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/toUhs3b8uX"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/zUvf3YC1Ti",
      "expanded_url" : "http:\/\/huff.to\/1gp1SZG",
      "display_url" : "huff.to\/1gp1SZG"
    } ]
  },
  "geo" : { },
  "id_str" : "433192724385529856",
  "text" : "RT @Edourdoo: \u79C0\u5170\u00B7\u9093\u6CE2\u513F\u53BB\u4E16\uFF0C\u4EAB\u5E7485\u5C81\uFF0CShirley Temple has died aged 85 http:\/\/t.co\/zUvf3YC1Ti http:\/\/t.co\/toUhs3b8uX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.ttj720.com\/index.php\" rel=\"nofollow\"\u003E\u63A8\u571F\u673A(\u5899\u5185\u76F4\u63A5\u70B9\u51FB)\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/HuffPostUK\/status\/433187411846115328\/photo\/1",
        "indices" : [ 70, 92 ],
        "url" : "http:\/\/t.co\/toUhs3b8uX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BgL9p-BCQAAFnEF.jpg",
        "id_str" : "433187411644792832",
        "id" : 433187411644792832,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BgL9p-BCQAAFnEF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/toUhs3b8uX"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 69 ],
        "url" : "http:\/\/t.co\/zUvf3YC1Ti",
        "expanded_url" : "http:\/\/huff.to\/1gp1SZG",
        "display_url" : "huff.to\/1gp1SZG"
      } ]
    },
    "geo" : { },
    "id_str" : "433187931872698368",
    "text" : "\u79C0\u5170\u00B7\u9093\u6CE2\u513F\u53BB\u4E16\uFF0C\u4EAB\u5E7485\u5C81\uFF0CShirley Temple has died aged 85 http:\/\/t.co\/zUvf3YC1Ti http:\/\/t.co\/toUhs3b8uX",
    "id" : 433187931872698368,
    "created_at" : "2014-02-11 10:37:05 +0000",
    "user" : {
      "name" : "Eddie Du",
      "screen_name" : "Edourdoo",
      "protected" : false,
      "id_str" : "20044440",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000669568751\/56adc18070e25792ea98d90fad615383_normal.jpeg",
      "id" : 20044440,
      "verified" : false
    }
  },
  "id" : 433192724385529856,
  "created_at" : "2014-02-11 10:56:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "indices" : [ 3, 15 ],
      "id_str" : "14479046",
      "id" : 14479046
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gsoc",
      "indices" : [ 84, 89 ]
    }, {
      "text" : "gsocgate",
      "indices" : [ 90, 99 ]
    }, {
      "text" : "rtept",
      "indices" : [ 100, 106 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "433013206035210240",
  "text" : "RT @daniel_kane: Surely security auditing should be routine for gov depts\/agencies. #gsoc #gsocgate #rtept",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "gsoc",
        "indices" : [ 67, 72 ]
      }, {
        "text" : "gsocgate",
        "indices" : [ 73, 82 ]
      }, {
        "text" : "rtept",
        "indices" : [ 83, 89 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "433012025099571200",
    "text" : "Surely security auditing should be routine for gov depts\/agencies. #gsoc #gsocgate #rtept",
    "id" : 433012025099571200,
    "created_at" : "2014-02-10 22:58:06 +0000",
    "user" : {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "protected" : false,
      "id_str" : "14479046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959158926339137536\/dxkfCxt4_normal.jpg",
      "id" : 14479046,
      "verified" : false
    }
  },
  "id" : 433013206035210240,
  "created_at" : "2014-02-10 23:02:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 70 ],
      "url" : "http:\/\/t.co\/ksYbnsB6uv",
      "expanded_url" : "http:\/\/www.brenontheroad.com\/singapore-airport-a-disneyland-for-backpackers\/",
      "display_url" : "brenontheroad.com\/singapore-airp\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "432945374760677376",
  "text" : "Singapore Airport: A Disneyland for backpackers http:\/\/t.co\/ksYbnsB6uv",
  "id" : 432945374760677376,
  "created_at" : "2014-02-10 18:33:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremiah Owyang",
      "screen_name" : "jowyang",
      "indices" : [ 0, 8 ],
      "id_str" : "79543",
      "id" : 79543
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http:\/\/t.co\/evwAO4yfZ4",
      "expanded_url" : "http:\/\/www.pub.gov.sg\/water\/newater\/newatertech\/Pages\/default.aspx",
      "display_url" : "pub.gov.sg\/water\/newater\/\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "432508451474788352",
  "geo" : { },
  "id_str" : "432835886623891458",
  "in_reply_to_user_id" : 79543,
  "text" : "@jowyang Using technology to help water efficiency http:\/\/t.co\/evwAO4yfZ4",
  "id" : 432835886623891458,
  "in_reply_to_status_id" : 432508451474788352,
  "created_at" : "2014-02-10 11:18:11 +0000",
  "in_reply_to_screen_name" : "jowyang",
  "in_reply_to_user_id_str" : "79543",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Healy",
      "screen_name" : "jonathanhealy",
      "indices" : [ 3, 17 ],
      "id_str" : "21304205",
      "id" : 21304205
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IrishSmallBiz",
      "indices" : [ 23, 37 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432834643382267904",
  "text" : "RT @jonathanhealy: Any #IrishSmallBiz news from jobs to startups; email smallbusiness@newstalk.ie",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IrishSmallBiz",
        "indices" : [ 4, 18 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "432820798387023872",
    "text" : "Any #IrishSmallBiz news from jobs to startups; email smallbusiness@newstalk.ie",
    "id" : 432820798387023872,
    "created_at" : "2014-02-10 10:18:14 +0000",
    "user" : {
      "name" : "Jonathan Healy",
      "screen_name" : "jonathanhealy",
      "protected" : false,
      "id_str" : "21304205",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/979798752323100673\/MyUJMmJA_normal.jpg",
      "id" : 21304205,
      "verified" : true
    }
  },
  "id" : 432834643382267904,
  "created_at" : "2014-02-10 11:13:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Mesut",
      "screen_name" : "jasonmesut",
      "indices" : [ 3, 14 ],
      "id_str" : "719373",
      "id" : 719373
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd14",
      "indices" : [ 113, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432832792725307392",
  "text" : "RT @jasonmesut: Check out my Bridging the Physical-Digital Divide presentation from this Saturday in Amsterdam \u2014\u00A0#ixd14 http:\/\/t.co\/d8J7iEE\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ixd14",
        "indices" : [ 97, 103 ]
      } ],
      "urls" : [ {
        "indices" : [ 104, 126 ],
        "url" : "http:\/\/t.co\/d8J7iEENn6",
        "expanded_url" : "http:\/\/lnkd.in\/dEni3eV",
        "display_url" : "lnkd.in\/dEni3eV"
      } ]
    },
    "geo" : { },
    "id_str" : "432819859702153216",
    "text" : "Check out my Bridging the Physical-Digital Divide presentation from this Saturday in Amsterdam \u2014\u00A0#ixd14 http:\/\/t.co\/d8J7iEENn6",
    "id" : 432819859702153216,
    "created_at" : "2014-02-10 10:14:30 +0000",
    "user" : {
      "name" : "Jason Mesut",
      "screen_name" : "jasonmesut",
      "protected" : false,
      "id_str" : "719373",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/85698789\/jasonHead_normal.jpg",
      "id" : 719373,
      "verified" : false
    }
  },
  "id" : 432832792725307392,
  "created_at" : "2014-02-10 11:05:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432829215302365184",
  "text" : "It would be good PR if they donate RTE EUR 85K payout to a charity. Just Staying.",
  "id" : 432829215302365184,
  "created_at" : "2014-02-10 10:51:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 81 ],
      "url" : "http:\/\/t.co\/UskLZ6sXxO",
      "expanded_url" : "http:\/\/adage.com\/article\/creativity-pick-of-the-day\/ajax-app-clean-social-media-profiles\/291173\/",
      "display_url" : "adage.com\/article\/creati\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "432826947463176193",
  "text" : "Ajax App Will Clean Up Your Facebook Page and Twitter Feed http:\/\/t.co\/UskLZ6sXxO",
  "id" : 432826947463176193,
  "created_at" : "2014-02-10 10:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "failbetter",
      "indices" : [ 36, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432812602519863297",
  "text" : "We should also allow politicians to #failbetter. Equally opportunity for all.",
  "id" : 432812602519863297,
  "created_at" : "2014-02-10 09:45:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "indices" : [ 3, 19 ],
      "id_str" : "326869253",
      "id" : 326869253
    }, {
      "name" : "Roz Kelly",
      "screen_name" : "RozsaraKelly",
      "indices" : [ 86, 99 ],
      "id_str" : "16964481",
      "id" : 16964481
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432810641817362432",
  "text" : "RT @Tweetinggoddess: My Daughter has Diabetes type 1  and she is doing this walk with @RozsaraKelly my sister. Donations welcome http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Roz Kelly",
        "screen_name" : "RozsaraKelly",
        "indices" : [ 65, 78 ],
        "id_str" : "16964481",
        "id" : 16964481
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/cXxm06927o",
        "expanded_url" : "http:\/\/www.mycharity.ie\/event\/roz_kellys_event\/",
        "display_url" : "mycharity.ie\/event\/roz_kell\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "432810186688835584",
    "text" : "My Daughter has Diabetes type 1  and she is doing this walk with @RozsaraKelly my sister. Donations welcome http:\/\/t.co\/cXxm06927o",
    "id" : 432810186688835584,
    "created_at" : "2014-02-10 09:36:04 +0000",
    "user" : {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "protected" : false,
      "id_str" : "326869253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946430925667229696\/wSG185FK_normal.jpg",
      "id" : 326869253,
      "verified" : true
    }
  },
  "id" : 432810641817362432,
  "created_at" : "2014-02-10 09:37:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432533899260866560",
  "text" : "Watching ice skating - an American skater doing an interpretation of the River Dance.",
  "id" : 432533899260866560,
  "created_at" : "2014-02-09 15:18:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432533067451097088",
  "text" : "RT @Clearpreso: One reason why local businesses closing in our towns: complete lack of innovation\/ even the most basic online presence.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "432527257781813248",
    "text" : "One reason why local businesses closing in our towns: complete lack of innovation\/ even the most basic online presence.",
    "id" : 432527257781813248,
    "created_at" : "2014-02-09 14:51:48 +0000",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 432533067451097088,
  "created_at" : "2014-02-09 15:14:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "migrantography",
      "screen_name" : "migrantography",
      "indices" : [ 3, 18 ],
      "id_str" : "1345952557",
      "id" : 1345952557
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 31, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "432532806812848129",
  "text" : "RT @migrantography: \"Things in #Singapore are very expensive. I still try to save money so I can send home around $800\/month.\" http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.tumblr.com\/\" rel=\"nofollow\"\u003ETumblr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 11, 21 ]
      } ],
      "urls" : [ {
        "indices" : [ 107, 129 ],
        "url" : "http:\/\/t.co\/FQlj21vQut",
        "expanded_url" : "http:\/\/tmblr.co\/Zc0y5u16pdYOi",
        "display_url" : "tmblr.co\/Zc0y5u16pdYOi"
      } ]
    },
    "geo" : { },
    "id_str" : "432242887976566784",
    "text" : "\"Things in #Singapore are very expensive. I still try to save money so I can send home around $800\/month.\" http:\/\/t.co\/FQlj21vQut",
    "id" : 432242887976566784,
    "created_at" : "2014-02-08 20:01:49 +0000",
    "user" : {
      "name" : "migrantography",
      "screen_name" : "migrantography",
      "protected" : false,
      "id_str" : "1345952557",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000437583590\/f039ac9ceb545e22fdfa4c2d81fa7404_normal.jpeg",
      "id" : 1345952557,
      "verified" : false
    }
  },
  "id" : 432532806812848129,
  "created_at" : "2014-02-09 15:13:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http:\/\/t.co\/kmn6EG9QCu",
      "expanded_url" : "http:\/\/www.pinterest.com\/khoi\/card-user-interfaces\/",
      "display_url" : "pinterest.com\/khoi\/card-user\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "431668106272854016",
  "text" : "A collection of Card-based User Interface. http:\/\/t.co\/kmn6EG9QCu",
  "id" : 431668106272854016,
  "created_at" : "2014-02-07 05:57:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "431664696467279872",
  "text" : "Good morning!",
  "id" : 431664696467279872,
  "created_at" : "2014-02-07 05:44:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 55 ],
      "url" : "http:\/\/t.co\/7JNnlgZ1yZ",
      "expanded_url" : "http:\/\/www.valuebasket.fr\/",
      "display_url" : "valuebasket.fr"
    } ]
  },
  "geo" : { },
  "id_str" : "431500810610950144",
  "text" : "Has anyone order from this site? http:\/\/t.co\/7JNnlgZ1yZ It is reliable? Thanks",
  "id" : 431500810610950144,
  "created_at" : "2014-02-06 18:53:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryanair",
      "screen_name" : "Ryanair",
      "indices" : [ 15, 23 ],
      "id_str" : "1542862735",
      "id" : 1542862735
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "431412048811925504",
  "geo" : { },
  "id_str" : "431412815044165632",
  "in_reply_to_user_id" : 327329868,
  "text" : "@ReubenRyanair @Ryanair Have the invites been sent out for Saturdays open day for selected candidates? Thanks",
  "id" : 431412815044165632,
  "in_reply_to_status_id" : 431412048811925504,
  "created_at" : "2014-02-06 13:03:25 +0000",
  "in_reply_to_screen_name" : "Reuben_May",
  "in_reply_to_user_id_str" : "327329868",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryanair",
      "screen_name" : "Ryanair",
      "indices" : [ 32, 40 ],
      "id_str" : "1542862735",
      "id" : 1542862735
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "digitalmarketing",
      "indices" : [ 61, 78 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "431412517500641280",
  "text" : "RT @ReubenRyanair: I will be on @ryanair today talking about #digitalmarketing between 4-5pm. If you have any questions send them in:) #ask\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ryanair",
        "screen_name" : "Ryanair",
        "indices" : [ 13, 21 ],
        "id_str" : "1542862735",
        "id" : 1542862735
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "digitalmarketing",
        "indices" : [ 42, 59 ]
      }, {
        "text" : "askryanair",
        "indices" : [ 116, 127 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "431412048811925504",
    "text" : "I will be on @ryanair today talking about #digitalmarketing between 4-5pm. If you have any questions send them in:) #askryanair",
    "id" : 431412048811925504,
    "created_at" : "2014-02-06 13:00:22 +0000",
    "user" : {
      "name" : "Reuben May",
      "screen_name" : "Reuben_May",
      "protected" : false,
      "id_str" : "327329868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978966814007971840\/surQiXsB_normal.jpg",
      "id" : 327329868,
      "verified" : false
    }
  },
  "id" : 431412517500641280,
  "created_at" : "2014-02-06 13:02:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JoshgunAtakhanov",
      "screen_name" : "JoshgunA",
      "indices" : [ 0, 9 ],
      "id_str" : "808548594164834304",
      "id" : 808548594164834304
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "431409972711137280",
  "text" : "@joshguna A comedian used to say something like :\" I like Jesus but I do not like his fan club\"",
  "id" : 431409972711137280,
  "created_at" : "2014-02-06 12:52:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "431407046185861120",
  "text" : "An online live chat \"We remain at your disposal for any questions.\" Disposal?",
  "id" : 431407046185861120,
  "created_at" : "2014-02-06 12:40:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/lPvS3u5mms",
      "expanded_url" : "http:\/\/clearpreso.com\/2014\/02\/06\/why-you-should-travel-from-chicago-to-san-francisco-by-train\/",
      "display_url" : "clearpreso.com\/2014\/02\/06\/why\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "431386212323323904",
  "text" : "RT @Clearpreso: Blogged: Why you should take the train from Chicago to SF next time you're out there on biz http:\/\/t.co\/lPvS3u5mms \u2026 http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Clearpreso\/status\/431228860173000704\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/lxA85aLUPQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BfwIXRRCIAAKyjn.png",
        "id_str" : "431228860185583616",
        "id" : 431228860185583616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfwIXRRCIAAKyjn.png",
        "sizes" : [ {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 885,
          "resize" : "fit",
          "w" : 1771
        }, {
          "h" : 885,
          "resize" : "fit",
          "w" : 1771
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/lxA85aLUPQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/lPvS3u5mms",
        "expanded_url" : "http:\/\/clearpreso.com\/2014\/02\/06\/why-you-should-travel-from-chicago-to-san-francisco-by-train\/",
        "display_url" : "clearpreso.com\/2014\/02\/06\/why\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "431365329063645184",
    "text" : "Blogged: Why you should take the train from Chicago to SF next time you're out there on biz http:\/\/t.co\/lPvS3u5mms \u2026 http:\/\/t.co\/lxA85aLUPQ",
    "id" : 431365329063645184,
    "created_at" : "2014-02-06 09:54:43 +0000",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 431386212323323904,
  "created_at" : "2014-02-06 11:17:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "431026374875156480",
  "text" : "anyone done a swiss railway trip before? What the best plan for budget traveler? Thks",
  "id" : 431026374875156480,
  "created_at" : "2014-02-05 11:27:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elena Millan",
      "screen_name" : "eMillanO",
      "indices" : [ 3, 12 ],
      "id_str" : "22264317",
      "id" : 22264317
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/Kn1sb9Xg4t",
      "expanded_url" : "http:\/\/fplus.me\/p\/5fvQ",
      "display_url" : "fplus.me\/p\/5fvQ"
    } ]
  },
  "geo" : { },
  "id_str" : "431021002626457600",
  "text" : "RT @eMillanO: The How-To Guide to Responsive Email Design. 47% of email opens happen on mobile devices h... http:\/\/t.co\/Kn1sb9Xg4t http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/friendsplus.me\" rel=\"nofollow\"\u003EFriends Me\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/eMillanO\/status\/431020522609340416\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/2zWVGh9IEc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BftK4a4IMAEvYgA.png",
        "id_str" : "431020522491883521",
        "id" : 431020522491883521,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BftK4a4IMAEvYgA.png",
        "sizes" : [ {
          "h" : 362,
          "resize" : "fit",
          "w" : 690
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 690
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 690
        }, {
          "h" : 357,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2zWVGh9IEc"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 116 ],
        "url" : "http:\/\/t.co\/Kn1sb9Xg4t",
        "expanded_url" : "http:\/\/fplus.me\/p\/5fvQ",
        "display_url" : "fplus.me\/p\/5fvQ"
      } ]
    },
    "geo" : { },
    "id_str" : "431020522609340416",
    "text" : "The How-To Guide to Responsive Email Design. 47% of email opens happen on mobile devices h... http:\/\/t.co\/Kn1sb9Xg4t http:\/\/t.co\/2zWVGh9IEc",
    "id" : 431020522609340416,
    "created_at" : "2014-02-05 11:04:35 +0000",
    "user" : {
      "name" : "Elena Millan",
      "screen_name" : "eMillanO",
      "protected" : false,
      "id_str" : "22264317",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930780295904813056\/xnjtooaw_normal.jpg",
      "id" : 22264317,
      "verified" : false
    }
  },
  "id" : 431021002626457600,
  "created_at" : "2014-02-05 11:06:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "430990787091107840",
  "geo" : { },
  "id_str" : "431018809772634112",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose hi I am not going. Hope my retweet can help you to get the cheaper group bundle. cheers.",
  "id" : 431018809772634112,
  "in_reply_to_status_id" : 430990787091107840,
  "created_at" : "2014-02-05 10:57:46 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "431013172607455232",
  "geo" : { },
  "id_str" : "431018021654495232",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney  Maybe biz can sell coupon for their goods &amp; services online. When flood recede, customers can redeem their purchases.",
  "id" : 431018021654495232,
  "in_reply_to_status_id" : 431013172607455232,
  "created_at" : "2014-02-05 10:54:38 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 3, 10 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Forbes\/status\/430741706469023744\/photo\/1",
      "indices" : [ 83, 105 ],
      "url" : "http:\/\/t.co\/eEphQcoUZd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BfpNTLxIgAAZ3MK.png",
      "id_str" : "430741706339024896",
      "id" : 430741706339024896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfpNTLxIgAAZ3MK.png",
      "sizes" : [ {
        "h" : 668,
        "resize" : "fit",
        "w" : 850
      }, {
        "h" : 668,
        "resize" : "fit",
        "w" : 850
      }, {
        "h" : 668,
        "resize" : "fit",
        "w" : 850
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 534,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eEphQcoUZd"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http:\/\/t.co\/AK54venJsN",
      "expanded_url" : "http:\/\/onforb.es\/1eOOThz",
      "display_url" : "onforb.es\/1eOOThz"
    } ]
  },
  "geo" : { },
  "id_str" : "430751502190534659",
  "text" : "RT @Forbes: Why Singapore's bear market may have just begun http:\/\/t.co\/AK54venJsN http:\/\/t.co\/eEphQcoUZd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Forbes\/status\/430741706469023744\/photo\/1",
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/eEphQcoUZd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BfpNTLxIgAAZ3MK.png",
        "id_str" : "430741706339024896",
        "id" : 430741706339024896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfpNTLxIgAAZ3MK.png",
        "sizes" : [ {
          "h" : 668,
          "resize" : "fit",
          "w" : 850
        }, {
          "h" : 668,
          "resize" : "fit",
          "w" : 850
        }, {
          "h" : 668,
          "resize" : "fit",
          "w" : 850
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 534,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eEphQcoUZd"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 70 ],
        "url" : "http:\/\/t.co\/AK54venJsN",
        "expanded_url" : "http:\/\/onforb.es\/1eOOThz",
        "display_url" : "onforb.es\/1eOOThz"
      } ]
    },
    "geo" : { },
    "id_str" : "430741706469023744",
    "text" : "Why Singapore's bear market may have just begun http:\/\/t.co\/AK54venJsN http:\/\/t.co\/eEphQcoUZd",
    "id" : 430741706469023744,
    "created_at" : "2014-02-04 16:36:40 +0000",
    "user" : {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "protected" : false,
      "id_str" : "91478624",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017039596083974149\/6AUhxLpr_normal.jpg",
      "id" : 91478624,
      "verified" : true
    }
  },
  "id" : 430751502190534659,
  "created_at" : "2014-02-04 17:15:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 3, 12 ],
      "id_str" : "1291131",
      "id" : 1291131
    }, {
      "name" : "Heidi Jermyn",
      "screen_name" : "heidipj",
      "indices" : [ 74, 82 ],
      "id_str" : "1251431",
      "id" : 1251431
    }, {
      "name" : "OFFSET",
      "screen_name" : "weloveoffset",
      "indices" : [ 121, 134 ],
      "id_str" : "52010943",
      "id" : 52010943
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430751145997635584",
  "text" : "RT @maryrose: Is there anyone out there who\u2019d like to join up with me and @heidipj  and score an even bigger discount on @weloveoffset tix?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Heidi Jermyn",
        "screen_name" : "heidipj",
        "indices" : [ 60, 68 ],
        "id_str" : "1251431",
        "id" : 1251431
      }, {
        "name" : "OFFSET",
        "screen_name" : "weloveoffset",
        "indices" : [ 107, 120 ],
        "id_str" : "52010943",
        "id" : 52010943
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "430748006024224768",
    "text" : "Is there anyone out there who\u2019d like to join up with me and @heidipj  and score an even bigger discount on @weloveoffset tix?",
    "id" : 430748006024224768,
    "created_at" : "2014-02-04 17:01:42 +0000",
    "user" : {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "protected" : false,
      "id_str" : "1291131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034068926370594816\/eS9-sUNe_normal.jpg",
      "id" : 1291131,
      "verified" : false
    }
  },
  "id" : 430751145997635584,
  "created_at" : "2014-02-04 17:14:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Wong",
      "screen_name" : "armchairdude",
      "indices" : [ 3, 16 ],
      "id_str" : "14551192",
      "id" : 14551192
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SatyaNadella",
      "indices" : [ 33, 46 ]
    }, {
      "text" : "MSFT",
      "indices" : [ 47, 52 ]
    }, {
      "text" : "CEO",
      "indices" : [ 53, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/Sa3dYdqZCr",
      "expanded_url" : "http:\/\/www.techmeme.com\/140204\/p12",
      "display_url" : "techmeme.com\/140204\/p12"
    } ]
  },
  "geo" : { },
  "id_str" : "430708664530640896",
  "text" : "RT @armchairdude: It's official! #SatyaNadella #MSFT #CEO http:\/\/t.co\/Sa3dYdqZCr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SatyaNadella",
        "indices" : [ 15, 28 ]
      }, {
        "text" : "MSFT",
        "indices" : [ 29, 34 ]
      }, {
        "text" : "CEO",
        "indices" : [ 35, 39 ]
      } ],
      "urls" : [ {
        "indices" : [ 40, 62 ],
        "url" : "http:\/\/t.co\/Sa3dYdqZCr",
        "expanded_url" : "http:\/\/www.techmeme.com\/140204\/p12",
        "display_url" : "techmeme.com\/140204\/p12"
      } ]
    },
    "geo" : { },
    "id_str" : "430705950853328896",
    "text" : "It's official! #SatyaNadella #MSFT #CEO http:\/\/t.co\/Sa3dYdqZCr",
    "id" : 430705950853328896,
    "created_at" : "2014-02-04 14:14:35 +0000",
    "user" : {
      "name" : "Jonathan Wong",
      "screen_name" : "armchairdude",
      "protected" : false,
      "id_str" : "14551192",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/70023742\/avatar_photo3_normal.jpg",
      "id" : 14551192,
      "verified" : false
    }
  },
  "id" : 430708664530640896,
  "created_at" : "2014-02-04 14:25:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/4qOzPEbnpq",
      "expanded_url" : "https:\/\/unroll.me\/awards",
      "display_url" : "unroll.me\/awards"
    } ]
  },
  "geo" : { },
  "id_str" : "430674186856435712",
  "text" : "\"Most Unsubscribed\" The place where email marketer weep https:\/\/t.co\/4qOzPEbnpq",
  "id" : 430674186856435712,
  "created_at" : "2014-02-04 12:08:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nick Barber",
      "screen_name" : "efsb",
      "indices" : [ 3, 8 ],
      "id_str" : "19938015",
      "id" : 19938015
    }, {
      "name" : "Olympus",
      "screen_name" : "getolympus",
      "indices" : [ 10, 21 ],
      "id_str" : "18994003",
      "id" : 18994003
    }, {
      "name" : "OlympusUK",
      "screen_name" : "OlympusUK",
      "indices" : [ 22, 32 ],
      "id_str" : "568399773",
      "id" : 568399773
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 128 ],
      "url" : "http:\/\/t.co\/ofoUAEkZn2",
      "expanded_url" : "http:\/\/www.flickr.com\/photos\/efsb\/sets\/72157640454943826\/",
      "display_url" : "flickr.com\/photos\/efsb\/se\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "430666741740740608",
  "text" : "RT @efsb: @getolympus @OlympusUK The majority of these (my pics) were taken with the glorious Stylus 1 :) http:\/\/t.co\/ofoUAEkZn2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Olympus",
        "screen_name" : "getolympus",
        "indices" : [ 0, 11 ],
        "id_str" : "18994003",
        "id" : 18994003
      }, {
        "name" : "OlympusUK",
        "screen_name" : "OlympusUK",
        "indices" : [ 12, 22 ],
        "id_str" : "568399773",
        "id" : 568399773
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/ofoUAEkZn2",
        "expanded_url" : "http:\/\/www.flickr.com\/photos\/efsb\/sets\/72157640454943826\/",
        "display_url" : "flickr.com\/photos\/efsb\/se\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "430380513077915648",
    "in_reply_to_user_id" : 18994003,
    "text" : "@getolympus @OlympusUK The majority of these (my pics) were taken with the glorious Stylus 1 :) http:\/\/t.co\/ofoUAEkZn2",
    "id" : 430380513077915648,
    "created_at" : "2014-02-03 16:41:25 +0000",
    "in_reply_to_screen_name" : "getolympus",
    "in_reply_to_user_id_str" : "18994003",
    "user" : {
      "name" : "Nick Barber",
      "screen_name" : "efsb",
      "protected" : false,
      "id_str" : "19938015",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/723855412647366656\/dPXQ9gcG_normal.jpg",
      "id" : 19938015,
      "verified" : false
    }
  },
  "id" : 430666741740740608,
  "created_at" : "2014-02-04 11:38:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Herbert Hood",
      "screen_name" : "SonoranHanbok",
      "indices" : [ 3, 17 ],
      "id_str" : "786238259164614656",
      "id" : 786238259164614656
    }, {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "indices" : [ 126, 137 ],
      "id_str" : "834055856",
      "id" : 834055856
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430661420116893696",
  "text" : "RT @SonoranHanbok: On expat attitudes: \"Be prepared to adapt to another culture (&amp;) try not to look at the negatives\" via @expatsblog http:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Expats Blog",
        "screen_name" : "expatsblog",
        "indices" : [ 107, 118 ],
        "id_str" : "834055856",
        "id" : 834055856
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 141 ],
        "url" : "http:\/\/t.co\/3zxYXdulmD",
        "expanded_url" : "http:\/\/www.expatsblog.com\/articles\/1679\/irish-expat-living-in-jordan-interview-michelle",
        "display_url" : "expatsblog.com\/articles\/1679\/\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "430446386161528832",
    "geo" : { },
    "id_str" : "430450057565855744",
    "in_reply_to_user_id" : 834055856,
    "text" : "On expat attitudes: \"Be prepared to adapt to another culture (&amp;) try not to look at the negatives\" via @expatsblog http:\/\/t.co\/3zxYXdulmD",
    "id" : 430450057565855744,
    "in_reply_to_status_id" : 430446386161528832,
    "created_at" : "2014-02-03 21:17:45 +0000",
    "in_reply_to_screen_name" : "expatsblog",
    "in_reply_to_user_id_str" : "834055856",
    "user" : {
      "name" : "Melissa Hahn",
      "screen_name" : "CulturalMelissa",
      "protected" : false,
      "id_str" : "149184380",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/903712416793452544\/fPYyDBqI_normal.jpg",
      "id" : 149184380,
      "verified" : false
    }
  },
  "id" : 430661420116893696,
  "created_at" : "2014-02-04 11:17:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430509061440692224",
  "text" : "\u602A\u4E0D\u5F97\u4EBA\u4EBA\u6E34\u671B\u88AB\u7231\uFF0C\u90A3\u79CD\u611F\u89C9\u7684\u786E\u5E78\u798F\uFF0C\u5FC3\u4E2D\u5145\u5B9E\u5F97\u9F13\u9F13\uFF0C\u50CF\u6781\u5C0F\u7684\u65F6\u5019\uFF0C\u81EA\u5E7C\u7A1A\u56ED\u8BFE\u5BA4\u653E\u5B66\u51FA\u6765\uFF0C\u77E5\u9053\u5988\u5988\u4E00\u5B9A\u5728\u95E8\u5916\u7B49\uFF0C\u95E8\u4E00\u6253\u5F00\uFF0C\u4FBF\u98DE\u5954\u51FA\u6765\uFF1A\u201C\u5988\u5988\uFF01\u201D\u6251\u5230\u6BCD\u4EB2\u6000\u4E2D\uFF0C\u90A3\u4FBF\u662F\u88AB\u7231\u7684\u611F\u89C9\u3002\u4E00\u751F\u4E2D\u53EF\u9047\u4E0D\u53EF\u6C42\uFF0C\u4F46\u662F\uFF0C\u7EC8\u4E8E\u5F97\u5230\uFF0C\u6CEA\u76C8\u4E8E\u776B\u3002(\u4EA6\u8212)",
  "id" : 430509061440692224,
  "created_at" : "2014-02-04 01:12:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Barrow in Thailand \uD83C\uDDF9\uD83C\uDDED \uD83C\uDDEC\uD83C\uDDE7",
      "screen_name" : "RichardBarrow",
      "indices" : [ 3, 17 ],
      "id_str" : "72888855",
      "id" : 72888855
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RichardBarrow\/status\/430372363017158657\/photo\/1",
      "indices" : [ 112, 134 ],
      "url" : "http:\/\/t.co\/G9C78fWqXq",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bfj9YkDIYAAoq7b.jpg",
      "id_str" : "430372362849378304",
      "id" : 430372362849378304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bfj9YkDIYAAoq7b.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 426,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 426,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 426,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 426,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/G9C78fWqXq"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 111 ],
      "url" : "http:\/\/t.co\/PA1poXpmKp",
      "expanded_url" : "http:\/\/www.bangkokpost.com\/news\/local\/393010\/handsome-japanese-reporter-overshadows-thai-politics",
      "display_url" : "bangkokpost.com\/news\/local\/393\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "430481513663438848",
  "text" : "RT @RichardBarrow: [Bangkok Post] 'Handsome' Japanese reporter overshadows Thai politics http:\/\/t.co\/PA1poXpmKp http:\/\/t.co\/G9C78fWqXq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RichardBarrow\/status\/430372363017158657\/photo\/1",
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/G9C78fWqXq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Bfj9YkDIYAAoq7b.jpg",
        "id_str" : "430372362849378304",
        "id" : 430372362849378304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bfj9YkDIYAAoq7b.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/G9C78fWqXq"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 92 ],
        "url" : "http:\/\/t.co\/PA1poXpmKp",
        "expanded_url" : "http:\/\/www.bangkokpost.com\/news\/local\/393010\/handsome-japanese-reporter-overshadows-thai-politics",
        "display_url" : "bangkokpost.com\/news\/local\/393\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "430372363017158657",
    "text" : "[Bangkok Post] 'Handsome' Japanese reporter overshadows Thai politics http:\/\/t.co\/PA1poXpmKp http:\/\/t.co\/G9C78fWqXq",
    "id" : 430372363017158657,
    "created_at" : "2014-02-03 16:09:01 +0000",
    "user" : {
      "name" : "Richard Barrow in Thailand \uD83C\uDDF9\uD83C\uDDED \uD83C\uDDEC\uD83C\uDDE7",
      "screen_name" : "RichardBarrow",
      "protected" : false,
      "id_str" : "72888855",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039280223995351041\/jsEqE7NL_normal.jpg",
      "id" : 72888855,
      "verified" : false
    }
  },
  "id" : 430481513663438848,
  "created_at" : "2014-02-03 23:22:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Sheehan",
      "screen_name" : "risteard69",
      "indices" : [ 3, 14 ],
      "id_str" : "294872539",
      "id" : 294872539
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430480000966725632",
  "text" : "RT @risteard69: Facebook and twitter always take the blame for these phenomenon but individuals need to fuckin cop on and take responsibili\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rtept",
        "indices" : [ 126, 132 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "430477416545677312",
    "text" : "Facebook and twitter always take the blame for these phenomenon but individuals need to fuckin cop on and take responsibility #rtept",
    "id" : 430477416545677312,
    "created_at" : "2014-02-03 23:06:28 +0000",
    "user" : {
      "name" : "Richard Sheehan",
      "screen_name" : "risteard69",
      "protected" : false,
      "id_str" : "294872539",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/920590812265832450\/Ws2jV9q__normal.jpg",
      "id" : 294872539,
      "verified" : false
    }
  },
  "id" : 430480000966725632,
  "created_at" : "2014-02-03 23:16:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore Airshow",
      "screen_name" : "SGAirshow",
      "indices" : [ 3, 13 ],
      "id_str" : "215931999",
      "id" : 215931999
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RSAFBlackKnights",
      "indices" : [ 84, 101 ]
    } ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/BU6VvSuzvs",
      "expanded_url" : "http:\/\/on.fb.me\/1aJi342",
      "display_url" : "on.fb.me\/1aJi342"
    } ]
  },
  "geo" : { },
  "id_str" : "430433030868848641",
  "text" : "RT @SGAirshow: Meet the capable &amp; professional Flight Line Crew maintaining the #RSAFBlackKnights's aircraft! http:\/\/t.co\/BU6VvSuzvs http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SGAirshow\/status\/423634174570811392\/photo\/1",
        "indices" : [ 122, 144 ],
        "url" : "http:\/\/t.co\/UsrFoaynT3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BeENCYrCUAALspJ.jpg",
        "id_str" : "423634174583394304",
        "id" : 423634174583394304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BeENCYrCUAALspJ.jpg",
        "sizes" : [ {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UsrFoaynT3"
      } ],
      "hashtags" : [ {
        "text" : "RSAFBlackKnights",
        "indices" : [ 69, 86 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/BU6VvSuzvs",
        "expanded_url" : "http:\/\/on.fb.me\/1aJi342",
        "display_url" : "on.fb.me\/1aJi342"
      } ]
    },
    "geo" : { },
    "id_str" : "423634174570811392",
    "text" : "Meet the capable &amp; professional Flight Line Crew maintaining the #RSAFBlackKnights's aircraft! http:\/\/t.co\/BU6VvSuzvs http:\/\/t.co\/UsrFoaynT3",
    "id" : 423634174570811392,
    "created_at" : "2014-01-16 01:53:52 +0000",
    "user" : {
      "name" : "Singapore Airshow",
      "screen_name" : "SGAirshow",
      "protected" : false,
      "id_str" : "215931999",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2609109533\/image_normal.jpg",
      "id" : 215931999,
      "verified" : false
    }
  },
  "id" : 430433030868848641,
  "created_at" : "2014-02-03 20:10:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430397918931996672",
  "text" : "Logged in and welcome by a new twitter look.",
  "id" : 430397918931996672,
  "created_at" : "2014-02-03 17:50:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Buckley \u50A8\u767E\u4EAE",
      "screen_name" : "ChuBailiang",
      "indices" : [ 3, 15 ],
      "id_str" : "19383099",
      "id" : 19383099
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http:\/\/t.co\/AYCjv7QOhn",
      "expanded_url" : "http:\/\/goo.gl\/lpMCLX",
      "display_url" : "goo.gl\/lpMCLX"
    } ]
  },
  "geo" : { },
  "id_str" : "430343569615101952",
  "text" : "RT @ChuBailiang: Very funny. Chinese people share pix of families enduring CCTV Spring Festival spectacular: http:\/\/t.co\/AYCjv7QOhn http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChuBailiang\/status\/430142695038652416\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/Cl9tQE9p7x",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BfgsgI1CEAAKM2l.jpg",
        "id_str" : "430142695051235328",
        "id" : 430142695051235328,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfgsgI1CEAAKM2l.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 625,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 625,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 625,
          "resize" : "fit",
          "w" : 717
        }, {
          "h" : 593,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Cl9tQE9p7x"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/AYCjv7QOhn",
        "expanded_url" : "http:\/\/goo.gl\/lpMCLX",
        "display_url" : "goo.gl\/lpMCLX"
      } ]
    },
    "geo" : { },
    "id_str" : "430142695038652416",
    "text" : "Very funny. Chinese people share pix of families enduring CCTV Spring Festival spectacular: http:\/\/t.co\/AYCjv7QOhn http:\/\/t.co\/Cl9tQE9p7x",
    "id" : 430142695038652416,
    "created_at" : "2014-02-03 00:56:24 +0000",
    "user" : {
      "name" : "Chris Buckley \u50A8\u767E\u4EAE",
      "screen_name" : "ChuBailiang",
      "protected" : false,
      "id_str" : "19383099",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3067768957\/8d3137ea8d8b5ee21641d56a2f504391_normal.jpeg",
      "id" : 19383099,
      "verified" : true
    }
  },
  "id" : 430343569615101952,
  "created_at" : "2014-02-03 14:14:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "indices" : [ 0, 16 ],
      "id_str" : "326869253",
      "id" : 326869253
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/430331748707086338\/photo\/1",
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/KO2CBx0l1R",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BfjYcgnCMAAsGEg.jpg",
      "id_str" : "430331748715474944",
      "id" : 430331748715474944,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfjYcgnCMAAsGEg.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 488,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 488,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 488,
        "resize" : "fit",
        "w" : 577
      }, {
        "h" : 488,
        "resize" : "fit",
        "w" : 577
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KO2CBx0l1R"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "430330089281028096",
  "geo" : { },
  "id_str" : "430331748707086338",
  "in_reply_to_user_id" : 326869253,
  "text" : "@Tweetinggoddess Google Drive has the option to let you download the doc as PDF http:\/\/t.co\/KO2CBx0l1R",
  "id" : 430331748707086338,
  "in_reply_to_status_id" : 430330089281028096,
  "created_at" : "2014-02-03 13:27:38 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "indices" : [ 0, 16 ],
      "id_str" : "326869253",
      "id" : 326869253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "430324676674347009",
  "geo" : { },
  "id_str" : "430329677652037632",
  "in_reply_to_user_id" : 326869253,
  "text" : "@Tweetinggoddess try saving as PDF in your Word Doc software. Better than as jpeg",
  "id" : 430329677652037632,
  "in_reply_to_status_id" : 430324676674347009,
  "created_at" : "2014-02-03 13:19:24 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/Ire80xTZQo",
      "expanded_url" : "http:\/\/www.youtube.com\/watch?v=L5LHALGus5Q",
      "display_url" : "youtube.com\/watch?v=L5LHAL\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "430325381275467777",
  "text" : "My fav scene by Philip Seymour Hoffman in The Ides of March. http:\/\/t.co\/Ire80xTZQo",
  "id" : 430325381275467777,
  "created_at" : "2014-02-03 13:02:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 3, 13 ],
      "id_str" : "18849187",
      "id" : 18849187
    }, {
      "name" : "GrabOne.ie",
      "screen_name" : "GrabOneIE",
      "indices" : [ 84, 94 ],
      "id_str" : "254204531",
      "id" : 254204531
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/YBZWqG4U7v",
      "expanded_url" : "https:\/\/www.linkedin.com\/jobs2\/view\/10804468",
      "display_url" : "linkedin.com\/jobs2\/view\/108\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "430323714815983616",
  "text" : "RT @barryhand: Amazing opportunity to take over from me as the Marketing Manager at @GrabOneIE https:\/\/t.co\/YBZWqG4U7v",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GrabOne.ie",
        "screen_name" : "GrabOneIE",
        "indices" : [ 69, 79 ],
        "id_str" : "254204531",
        "id" : 254204531
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/YBZWqG4U7v",
        "expanded_url" : "https:\/\/www.linkedin.com\/jobs2\/view\/10804468",
        "display_url" : "linkedin.com\/jobs2\/view\/108\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "430301092698533888",
    "text" : "Amazing opportunity to take over from me as the Marketing Manager at @GrabOneIE https:\/\/t.co\/YBZWqG4U7v",
    "id" : 430301092698533888,
    "created_at" : "2014-02-03 11:25:49 +0000",
    "user" : {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "protected" : false,
      "id_str" : "18849187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614532770300329984\/xJWWAtSu_normal.jpg",
      "id" : 18849187,
      "verified" : false
    }
  },
  "id" : 430323714815983616,
  "created_at" : "2014-02-03 12:55:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 0, 10 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "430315806589988864",
  "geo" : { },
  "id_str" : "430323698386481152",
  "in_reply_to_user_id" : 18849187,
  "text" : "@barryhand All the best in your future undertaking!",
  "id" : 430323698386481152,
  "in_reply_to_status_id" : 430315806589988864,
  "created_at" : "2014-02-03 12:55:39 +0000",
  "in_reply_to_screen_name" : "barryhand",
  "in_reply_to_user_id_str" : "18849187",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 57 ],
      "url" : "http:\/\/t.co\/zCIQ6tLNvk",
      "expanded_url" : "http:\/\/i.imgur.com\/SDnL9Tu.jpg",
      "display_url" : "i.imgur.com\/SDnL9Tu.jpg"
    } ]
  },
  "geo" : { },
  "id_str" : "430238462478790656",
  "text" : "corporate gift from a funeral home http:\/\/t.co\/zCIQ6tLNvk",
  "id" : 430238462478790656,
  "created_at" : "2014-02-03 07:16:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/WWKMyEg0S6",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/instituteforjustice\/2014\/01\/29\/california-legalized-selling-food-made-at-home-and-created-over-a-thousand-local-businesses\/",
      "display_url" : "forbes.com\/sites\/institut\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "430128241501032448",
  "text" : "Loosening regulations created over one thousand new local businesses in the first year. http:\/\/t.co\/WWKMyEg0S6",
  "id" : 430128241501032448,
  "created_at" : "2014-02-02 23:58:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kristopher B. Jones",
      "screen_name" : "krisjonescom",
      "indices" : [ 3, 16 ],
      "id_str" : "9082652",
      "id" : 9082652
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430066066073481217",
  "text" : "RT @krisjonescom: Very sad to hear about the passing of Philip Seymour Hoffman. He is one of my all-time favorites.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "430064694242447360",
    "text" : "Very sad to hear about the passing of Philip Seymour Hoffman. He is one of my all-time favorites.",
    "id" : 430064694242447360,
    "created_at" : "2014-02-02 19:46:27 +0000",
    "user" : {
      "name" : "Kristopher B. Jones",
      "screen_name" : "krisjonescom",
      "protected" : false,
      "id_str" : "9082652",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012891375325646848\/erRmWSDd_normal.jpg",
      "id" : 9082652,
      "verified" : true
    }
  },
  "id" : 430066066073481217,
  "created_at" : "2014-02-02 19:51:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Rentoul",
      "screen_name" : "JohnRentoul",
      "indices" : [ 3, 15 ],
      "id_str" : "14085096",
      "id" : 14085096
    }, {
      "name" : "Jake Goretzki \uD83C\uDDEA\uD83C\uDDFA\uD83C\uDDE7\uD83C\uDDEA\uD83C\uDDE7\uD83C\uDDE6\uD83C\uDDEE\uD83C\uDDF1",
      "screen_name" : "jakegoretzki",
      "indices" : [ 113, 126 ],
      "id_str" : "80169652",
      "id" : 80169652
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jakegoretzki\/status\/429033353086369792\/photo\/1",
      "indices" : [ 77, 99 ],
      "url" : "http:\/\/t.co\/AAPXWgOk5r",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BfQ7j7_CAAAbikE.jpg",
      "id_str" : "429033353090564096",
      "id" : 429033353090564096,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfQ7j7_CAAAbikE.jpg",
      "sizes" : [ {
        "h" : 965,
        "resize" : "fit",
        "w" : 1365
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 965,
        "resize" : "fit",
        "w" : 1365
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 848,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/AAPXWgOk5r"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "430060861831798784",
  "text" : "RT @JohnRentoul: Cartoon of the Week: \"If The Guardian covered Middle-earth\" http:\/\/t.co\/AAPXWgOk5r Brilliant by @jakegoretzki https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jake Goretzki \uD83C\uDDEA\uD83C\uDDFA\uD83C\uDDE7\uD83C\uDDEA\uD83C\uDDE7\uD83C\uDDE6\uD83C\uDDEE\uD83C\uDDF1",
        "screen_name" : "jakegoretzki",
        "indices" : [ 96, 109 ],
        "id_str" : "80169652",
        "id" : 80169652
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jakegoretzki\/status\/429033353086369792\/photo\/1",
        "indices" : [ 60, 82 ],
        "url" : "http:\/\/t.co\/AAPXWgOk5r",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/BfQ7j7_CAAAbikE.jpg",
        "id_str" : "429033353090564096",
        "id" : 429033353090564096,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BfQ7j7_CAAAbikE.jpg",
        "sizes" : [ {
          "h" : 965,
          "resize" : "fit",
          "w" : 1365
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 965,
          "resize" : "fit",
          "w" : 1365
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 848,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AAPXWgOk5r"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/pgclpADemh",
        "expanded_url" : "https:\/\/twitter.com\/jakegoretzki\/status\/429033353086369792",
        "display_url" : "twitter.com\/jakegoretzki\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "430033330902282240",
    "text" : "Cartoon of the Week: \"If The Guardian covered Middle-earth\" http:\/\/t.co\/AAPXWgOk5r Brilliant by @jakegoretzki https:\/\/t.co\/pgclpADemh",
    "id" : 430033330902282240,
    "created_at" : "2014-02-02 17:41:50 +0000",
    "user" : {
      "name" : "John Rentoul",
      "screen_name" : "JohnRentoul",
      "protected" : false,
      "id_str" : "14085096",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686469551119269888\/q9ToXXT7_normal.jpg",
      "id" : 14085096,
      "verified" : true
    }
  },
  "id" : 430060861831798784,
  "created_at" : "2014-02-02 19:31:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CNY2014",
      "indices" : [ 60, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 58 ],
      "url" : "http:\/\/t.co\/NV1vXgsQrN",
      "expanded_url" : "http:\/\/mryap.tumblr.com\/post\/75306435298\/mrs-invited-some-colleagues-on-the-second-day-of",
      "display_url" : "mryap.tumblr.com\/post\/753064352\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "429767815340384256",
  "text" : "A greeting card from a young guest. http:\/\/t.co\/NV1vXgsQrN  #CNY2014",
  "id" : 429767815340384256,
  "created_at" : "2014-02-02 00:06:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "15908631",
      "id" : 15908631
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "satnightshow",
      "indices" : [ 56, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "429765823822303232",
  "text" : "RT @gavinsblog: My Twitter feed does a collective \"WTF\" #satnightshow",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "satnightshow",
        "indices" : [ 40, 53 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "429751701159944193",
    "text" : "My Twitter feed does a collective \"WTF\" #satnightshow",
    "id" : 429751701159944193,
    "created_at" : "2014-02-01 23:02:44 +0000",
    "user" : {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "protected" : false,
      "id_str" : "15908631",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894331872716754945\/0KfaOf4I_normal.jpg",
      "id" : 15908631,
      "verified" : true
    }
  },
  "id" : 429765823822303232,
  "created_at" : "2014-02-01 23:58:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dublinese \u6708\u5149",
      "screen_name" : "rachel_violin",
      "indices" : [ 0, 14 ],
      "id_str" : "116755889",
      "id" : 116755889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "428877983928307712",
  "geo" : { },
  "id_str" : "429644126783418369",
  "in_reply_to_user_id" : 116755889,
  "text" : "@rachel_violin \u65B0\u5E74\u5FEB\u4E50\uFF01",
  "id" : 429644126783418369,
  "in_reply_to_status_id" : 428877983928307712,
  "created_at" : "2014-02-01 15:55:16 +0000",
  "in_reply_to_screen_name" : "rachel_violin",
  "in_reply_to_user_id_str" : "116755889",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]