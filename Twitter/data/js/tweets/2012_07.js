Grailbird.data.tweets_2012_07 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230398007990968320",
  "text" : "A Study by found that Men were 25% less likely to purchase Clearasil skin products when a Facebook Like or Tweet button was present.",
  "id" : 230398007990968320,
  "created_at" : "2012-07-31 20:22:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joshua Kay",
      "screen_name" : "js_kay",
      "indices" : [ 3, 10 ],
      "id_str" : "364815125",
      "id" : 364815125
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "london2012",
      "indices" : [ 104, 115 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230336305345728514",
  "text" : "RT @js_kay: When Zara Phillips is involved in a coin toss, does she call \"grandma\" instead of \"tails\"?\n\n#london2012",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "london2012",
        "indices" : [ 92, 103 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "230252701269753856",
    "text" : "When Zara Phillips is involved in a coin toss, does she call \"grandma\" instead of \"tails\"?\n\n#london2012",
    "id" : 230252701269753856,
    "created_at" : "2012-07-31 10:44:56 +0000",
    "user" : {
      "name" : "Joshua Kay",
      "screen_name" : "js_kay",
      "protected" : false,
      "id_str" : "364815125",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030054078301143040\/Mz3wAgoR_normal.jpg",
      "id" : 364815125,
      "verified" : false
    }
  },
  "id" : 230336305345728514,
  "created_at" : "2012-07-31 16:17:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 13, 21 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http:\/\/t.co\/NOGUJi9r",
      "expanded_url" : "http:\/\/www.guardian.co.uk\/small-business-network\/2012\/jul\/31\/best-practice-exchange-best-practice-winning-new-business?newsfeed=true",
      "display_url" : "guardian.co.uk\/small-business\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230336117508026368",
  "text" : "PR people in #Ireland you should read this http:\/\/t.co\/NOGUJi9r &lt;&lt; P.S. : Hire Me",
  "id" : 230336117508026368,
  "created_at" : "2012-07-31 16:16:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "indices" : [ 3, 19 ],
      "id_str" : "51263711",
      "id" : 51263711
    }, {
      "name" : "PR Agency One",
      "screen_name" : "PRAgencyOne",
      "indices" : [ 45, 57 ],
      "id_str" : "350152748",
      "id" : 350152748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230335888821993472",
  "text" : "RT @googleanalytics: Check out how PR Agency @pragencyone has grown by using Google Analytics to track lead generation &amp; show ROI: h ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "PR Agency One",
        "screen_name" : "PRAgencyOne",
        "indices" : [ 24, 36 ],
        "id_str" : "350152748",
        "id" : 350152748
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 134 ],
        "url" : "http:\/\/t.co\/ZV0G68zc",
        "expanded_url" : "http:\/\/goo.gl\/Mdlmi",
        "display_url" : "goo.gl\/Mdlmi"
      } ]
    },
    "geo" : { },
    "id_str" : "230334810655162370",
    "text" : "Check out how PR Agency @pragencyone has grown by using Google Analytics to track lead generation &amp; show ROI: http:\/\/t.co\/ZV0G68zc",
    "id" : 230334810655162370,
    "created_at" : "2012-07-31 16:11:12 +0000",
    "user" : {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "protected" : false,
      "id_str" : "51263711",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021848775885651968\/cU74ahCn_normal.jpg",
      "id" : 51263711,
      "verified" : true
    }
  },
  "id" : 230335888821993472,
  "created_at" : "2012-07-31 16:15:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http:\/\/t.co\/rf9JY8aY",
      "expanded_url" : "http:\/\/knowledge.wharton.upenn.edu\/article.cfm?articleid=3027",
      "display_url" : "knowledge.wharton.upenn.edu\/article.cfm?ar\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230335588484653058",
  "text" : "Not finding [employees], don't call it a skills gap; don't call it a skills mismatch - Wharton management professor http:\/\/t.co\/rf9JY8aY",
  "id" : 230335588484653058,
  "created_at" : "2012-07-31 16:14:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 67, 80 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 81, 88 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 66 ],
      "url" : "http:\/\/t.co\/SR7cIBVi",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/quickerbettertech\/2012\/07\/23\/does-your-gas-station-have-a-mobile-site-too\/",
      "display_url" : "forbes.com\/sites\/quickerb\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230333959630893056",
  "text" : "Forbes: Businesses Should be Mobile-Friendly. http:\/\/t.co\/SR7cIBVi #SMEcommunity #irebiz",
  "id" : 230333959630893056,
  "created_at" : "2012-07-31 16:07:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http:\/\/t.co\/CneVcP7S",
      "expanded_url" : "http:\/\/sg.news.yahoo.com\/hong-kong-parents-fight-china-brainwashing-064048174.html",
      "display_url" : "sg.news.yahoo.com\/hong-kong-pare\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230331030442229761",
  "text" : "Hong Kong parents fight China communist party 'brainwashing' http:\/\/t.co\/CneVcP7S",
  "id" : 230331030442229761,
  "created_at" : "2012-07-31 15:56:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230329485722013696",
  "text" : "Singapore Table tennis advances to semi-final.",
  "id" : 230329485722013696,
  "created_at" : "2012-07-31 15:50:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230326339075469312",
  "text" : "Dinner prepared - pasta with Swedish ball. Now watching women's team gymnastics final. My fav event.",
  "id" : 230326339075469312,
  "created_at" : "2012-07-31 15:37:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230282723347021824",
  "text" : "Half an hr ago on TV, I saw Ireland is leading in the woman sailing.",
  "id" : 230282723347021824,
  "created_at" : "2012-07-31 12:44:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 0, 11 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "230273233029562369",
  "geo" : { },
  "id_str" : "230274341227618304",
  "in_reply_to_user_id" : 16738907,
  "text" : "@davegantly Agreed.",
  "id" : 230274341227618304,
  "in_reply_to_status_id" : 230273233029562369,
  "created_at" : "2012-07-31 12:10:55 +0000",
  "in_reply_to_screen_name" : "davegantly",
  "in_reply_to_user_id_str" : "16738907",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http:\/\/t.co\/HtjXmnfc",
      "expanded_url" : "http:\/\/news.insing.com\/tabloid\/joseph-schooling-olympics-drama\/id-985a3f00",
      "display_url" : "news.insing.com\/tabloid\/joseph\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230272257220542464",
  "text" : "There must be something so special about TVR sports equipment &amp; gear. http:\/\/t.co\/HtjXmnfc",
  "id" : 230272257220542464,
  "created_at" : "2012-07-31 12:02:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 0, 11 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "230271065543290880",
  "geo" : { },
  "id_str" : "230271911140139008",
  "in_reply_to_user_id" : 16738907,
  "text" : "@davegantly Thanks. The news I read is on that tweet about letting his old man down which is not as malicious as \"find and drown\" him.",
  "id" : 230271911140139008,
  "in_reply_to_status_id" : 230271065543290880,
  "created_at" : "2012-07-31 12:01:16 +0000",
  "in_reply_to_screen_name" : "davegantly",
  "in_reply_to_user_id_str" : "16738907",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 88 ],
      "url" : "http:\/\/t.co\/48tO60f3",
      "expanded_url" : "http:\/\/news.insing.com\/tabloid\/slammed-giving-free-meals-poor\/id-4c5a3f00",
      "display_url" : "news.insing.com\/tabloid\/slamme\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230270380181426177",
  "text" : "It a disgrace of some Singaporeans to slam other people for giving. http:\/\/t.co\/48tO60f3",
  "id" : 230270380181426177,
  "created_at" : "2012-07-31 11:55:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Cousins",
      "screen_name" : "xcelbusiness",
      "indices" : [ 3, 16 ],
      "id_str" : "2410788877",
      "id" : 2410788877
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 103 ],
      "url" : "http:\/\/t.co\/3zNRFlIv",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/magazine-19045837",
      "display_url" : "bbc.co.uk\/news\/magazine-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230205902895075328",
  "text" : "RT @xcelbusiness: Why the fax thrives in Japan &lt; so it's not just Irish Revenue http:\/\/t.co\/3zNRFlIv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/bbc-news\/id364147881?mt=8&uo=4\" rel=\"nofollow\"\u003EBBC News on iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 65, 85 ],
        "url" : "http:\/\/t.co\/3zNRFlIv",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/magazine-19045837",
        "display_url" : "bbc.co.uk\/news\/magazine-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "230200372218716160",
    "text" : "Why the fax thrives in Japan &lt; so it's not just Irish Revenue http:\/\/t.co\/3zNRFlIv",
    "id" : 230200372218716160,
    "created_at" : "2012-07-31 07:17:00 +0000",
    "user" : {
      "name" : "Helen Cousins",
      "screen_name" : "_helencousins",
      "protected" : false,
      "id_str" : "119546373",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/602560856488955905\/DQt4Hz6D_normal.jpg",
      "id" : 119546373,
      "verified" : false
    }
  },
  "id" : 230205902895075328,
  "created_at" : "2012-07-31 07:38:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Iarnr\u00F3d \u00C9ireann",
      "screen_name" : "IrishRail",
      "indices" : [ 3, 13 ],
      "id_str" : "15115986",
      "id" : 15115986
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "daysout",
      "indices" : [ 35, 43 ]
    } ],
    "urls" : [ {
      "indices" : [ 113, 133 ],
      "url" : "http:\/\/t.co\/cm2db05C",
      "expanded_url" : "http:\/\/www.irishrail.ie\/blog_post.jsp?blogID=1&a=320",
      "display_url" : "irishrail.ie\/blog_post.jsp?\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230205309753376770",
  "text" : "RT @IrishRail: Tell us your family #daysout destination idea on our blog &amp; enter draw for family rail ticket http:\/\/t.co\/cm2db05C",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "daysout",
        "indices" : [ 20, 28 ]
      } ],
      "urls" : [ {
        "indices" : [ 98, 118 ],
        "url" : "http:\/\/t.co\/cm2db05C",
        "expanded_url" : "http:\/\/www.irishrail.ie\/blog_post.jsp?blogID=1&a=320",
        "display_url" : "irishrail.ie\/blog_post.jsp?\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "230204384854831104",
    "text" : "Tell us your family #daysout destination idea on our blog &amp; enter draw for family rail ticket http:\/\/t.co\/cm2db05C",
    "id" : 230204384854831104,
    "created_at" : "2012-07-31 07:32:56 +0000",
    "user" : {
      "name" : "Iarnr\u00F3d \u00C9ireann",
      "screen_name" : "IrishRail",
      "protected" : false,
      "id_str" : "15115986",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002515342281846784\/mcPgLtBl_normal.jpg",
      "id" : 15115986,
      "verified" : true
    }
  },
  "id" : 230205309753376770,
  "created_at" : "2012-07-31 07:36:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC News (UK)",
      "screen_name" : "BBCNews",
      "indices" : [ 3, 11 ],
      "id_str" : "612473",
      "id" : 612473
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http:\/\/t.co\/dUF5z16x",
      "expanded_url" : "http:\/\/bbc.in\/PgfjvE",
      "display_url" : "bbc.in\/PgfjvE"
    } ]
  },
  "geo" : { },
  "id_str" : "230166287819616256",
  "text" : "RT @BBCNews: Taiwan cuts growth forecast http:\/\/t.co\/dUF5z16x",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.bbc.co.uk\/news\/\" rel=\"nofollow\"\u003EBBC News\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 48 ],
        "url" : "http:\/\/t.co\/dUF5z16x",
        "expanded_url" : "http:\/\/bbc.in\/PgfjvE",
        "display_url" : "bbc.in\/PgfjvE"
      } ]
    },
    "geo" : { },
    "id_str" : "230118613468725248",
    "text" : "Taiwan cuts growth forecast http:\/\/t.co\/dUF5z16x",
    "id" : 230118613468725248,
    "created_at" : "2012-07-31 01:52:07 +0000",
    "user" : {
      "name" : "BBC News (UK)",
      "screen_name" : "BBCNews",
      "protected" : false,
      "id_str" : "612473",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875702547016802304\/9TC6qsAT_normal.jpg",
      "id" : 612473,
      "verified" : true
    }
  },
  "id" : 230166287819616256,
  "created_at" : "2012-07-31 05:01:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore Embassy US",
      "screen_name" : "SingaporeEmbDC",
      "indices" : [ 3, 18 ],
      "id_str" : "215060963",
      "id" : 215060963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230047573157740544",
  "text" : "RT @SingaporeEmbDC: Story of Singapore's efforts to achieve water self-sufficiency @BloombergNews http:\/\/t.co\/sU9QN8dX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 98 ],
        "url" : "http:\/\/t.co\/sU9QN8dX",
        "expanded_url" : "http:\/\/bloom.bg\/QpZ5qG",
        "display_url" : "bloom.bg\/QpZ5qG"
      } ]
    },
    "geo" : { },
    "id_str" : "230040385135718401",
    "text" : "Story of Singapore's efforts to achieve water self-sufficiency @BloombergNews http:\/\/t.co\/sU9QN8dX",
    "id" : 230040385135718401,
    "created_at" : "2012-07-30 20:41:16 +0000",
    "user" : {
      "name" : "Singapore Embassy US",
      "screen_name" : "SingaporeEmbDC",
      "protected" : false,
      "id_str" : "215060963",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/769108637839134720\/VbTBpM63_normal.jpg",
      "id" : 215060963,
      "verified" : true
    }
  },
  "id" : 230047573157740544,
  "created_at" : "2012-07-30 21:09:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PetaPixel",
      "screen_name" : "petapixel",
      "indices" : [ 1, 11 ],
      "id_str" : "37971731",
      "id" : 37971731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 111 ],
      "url" : "http:\/\/t.co\/wop34c9Q",
      "expanded_url" : "http:\/\/j.mp\/ODnLbV",
      "display_url" : "j.mp\/ODnLbV"
    } ]
  },
  "geo" : { },
  "id_str" : "230041284948160512",
  "text" : "\u201C@petapixel: Olympic opening ceremony participant captures inside view with hidden camera: http:\/\/t.co\/wop34c9Q\u201D &lt;&lt; blocked by IOC",
  "id" : 230041284948160512,
  "created_at" : "2012-07-30 20:44:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 3, 12 ],
      "id_str" : "19456433",
      "id" : 19456433
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "london2012",
      "indices" : [ 81, 92 ]
    }, {
      "text" : "olympics",
      "indices" : [ 93, 102 ]
    }, {
      "text" : "teamGB",
      "indices" : [ 103, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "230007158513737728",
  "text" : "RT @jonboyes: Bronze still an amazing result in first ever team final. Be proud. #london2012 #olympics #teamGB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "london2012",
        "indices" : [ 67, 78 ]
      }, {
        "text" : "olympics",
        "indices" : [ 79, 88 ]
      }, {
        "text" : "teamGB",
        "indices" : [ 89, 96 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "230003307391885313",
    "text" : "Bronze still an amazing result in first ever team final. Be proud. #london2012 #olympics #teamGB",
    "id" : 230003307391885313,
    "created_at" : "2012-07-30 18:13:56 +0000",
    "user" : {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "protected" : false,
      "id_str" : "19456433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3386817686\/4b1b7303154ea92ea270a5afec16da70_normal.jpeg",
      "id" : 19456433,
      "verified" : false
    }
  },
  "id" : 230007158513737728,
  "created_at" : "2012-07-30 18:29:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 52 ],
      "url" : "https:\/\/t.co\/LuFTwJ9H",
      "expanded_url" : "https:\/\/twitter.com\/alexlynch_13\/status\/230006461290385408",
      "display_url" : "twitter.com\/alexlynch_13\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "230007112208633856",
  "text" : "That a racist remark... \"J8ps\" https:\/\/t.co\/LuFTwJ9H",
  "id" : 230007112208633856,
  "created_at" : "2012-07-30 18:29:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mr Miyagi",
      "screen_name" : "miyagi",
      "indices" : [ 3, 10 ],
      "id_str" : "1543661",
      "id" : 1543661
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "olympics",
      "indices" : [ 102, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 132 ],
      "url" : "http:\/\/t.co\/ErCmQ7JS",
      "expanded_url" : "http:\/\/miy.ag\/LYPsfF",
      "display_url" : "miy.ag\/LYPsfF"
    } ]
  },
  "geo" : { },
  "id_str" : "229995858639351809",
  "text" : "RT @miyagi: SG swimmer fails because \"goggle and swim cap not approved\". Come on SG sports officials! #olympics http:\/\/t.co\/ErCmQ7JS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "olympics",
        "indices" : [ 90, 99 ]
      } ],
      "urls" : [ {
        "indices" : [ 100, 120 ],
        "url" : "http:\/\/t.co\/ErCmQ7JS",
        "expanded_url" : "http:\/\/miy.ag\/LYPsfF",
        "display_url" : "miy.ag\/LYPsfF"
      } ]
    },
    "geo" : { },
    "id_str" : "229960948511809537",
    "text" : "SG swimmer fails because \"goggle and swim cap not approved\". Come on SG sports officials! #olympics http:\/\/t.co\/ErCmQ7JS",
    "id" : 229960948511809537,
    "created_at" : "2012-07-30 15:25:37 +0000",
    "user" : {
      "name" : "Mr Miyagi",
      "screen_name" : "miyagi",
      "protected" : false,
      "id_str" : "1543661",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/881149400562761733\/eQxe-Smp_normal.jpg",
      "id" : 1543661,
      "verified" : false
    }
  },
  "id" : 229995858639351809,
  "created_at" : "2012-07-30 17:44:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229994026353426432",
  "text" : "Spotted dragon motifs on China's men gymnast sport attire.",
  "id" : 229994026353426432,
  "created_at" : "2012-07-30 17:37:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http:\/\/t.co\/viwjXg5G",
      "expanded_url" : "http:\/\/www.eurovisionsports.tv\/london2012\/",
      "display_url" : "eurovisionsports.tv\/london2012\/"
    } ]
  },
  "geo" : { },
  "id_str" : "229862234963185664",
  "text" : "RT @lisaocarroll: If you want to follow non-UK sports man\/woman save this link: http:\/\/t.co\/viwjXg5G\nlive streams from 40 stations acros ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 82 ],
        "url" : "http:\/\/t.co\/viwjXg5G",
        "expanded_url" : "http:\/\/www.eurovisionsports.tv\/london2012\/",
        "display_url" : "eurovisionsports.tv\/london2012\/"
      } ]
    },
    "geo" : { },
    "id_str" : "229860777820049408",
    "text" : "If you want to follow non-UK sports man\/woman save this link: http:\/\/t.co\/viwjXg5G\nlive streams from 40 stations across europe+ 12 raw feeds",
    "id" : 229860777820049408,
    "created_at" : "2012-07-30 08:47:34 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 229862234963185664,
  "created_at" : "2012-07-30 08:53:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "piggyback",
      "indices" : [ 105, 115 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229654550469029888",
  "text" : "Hire me to fine tune your site so you can have more Sales, Leads &amp; Conversions. No Results, No fees. #piggyback",
  "id" : 229654550469029888,
  "created_at" : "2012-07-29 19:08:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Piggyback",
      "indices" : [ 83, 93 ]
    }, {
      "text" : "bizitalk",
      "indices" : [ 94, 103 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229646080726429696",
  "text" : "I help you make sense of web analytics data so you can serve your customer better. #Piggyback #bizitalk",
  "id" : 229646080726429696,
  "created_at" : "2012-07-29 18:34:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Boston.com",
      "screen_name" : "BostonDotCom",
      "indices" : [ 59, 72 ],
      "id_str" : "14602259",
      "id" : 14602259
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http:\/\/t.co\/ULcpPkKj",
      "expanded_url" : "http:\/\/www.boston.com\/bigpicture\/2012\/07\/olympics_2012_opening_ceremoni.html",
      "display_url" : "boston.com\/bigpicture\/201\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "229593859791941632",
  "text" : "Olympics 2012: Opening ceremonies http:\/\/t.co\/ULcpPkKj via @BostonDotCom The Big Picture",
  "id" : 229593859791941632,
  "created_at" : "2012-07-29 15:06:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tony Wang",
      "screen_name" : "TonyW",
      "indices" : [ 3, 9 ],
      "id_str" : "85993428",
      "id" : 85993428
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Olympic",
      "indices" : [ 46, 54 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229591811776188416",
  "text" : "RT @TonyW: C'mon folks, lets fill those empty #Olympic seats. If you're a corporate with no-shows, get tweeting to give those seats away ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Olympic",
        "indices" : [ 35, 43 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "229583190434648065",
    "text" : "C'mon folks, lets fill those empty #Olympic seats. If you're a corporate with no-shows, get tweeting to give those seats away to eager fans.",
    "id" : 229583190434648065,
    "created_at" : "2012-07-29 14:24:32 +0000",
    "user" : {
      "name" : "Tony Wang",
      "screen_name" : "TonyW",
      "protected" : false,
      "id_str" : "85993428",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/493986035919904768\/EaXKgH_D_normal.jpeg",
      "id" : 85993428,
      "verified" : false
    }
  },
  "id" : 229591811776188416,
  "created_at" : "2012-07-29 14:58:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 3, 19 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229589466459803648",
  "text" : "RT @funkygoddessirl: Folks I need some sales today so I can buy POS.  If you know any Mums or Lone Fathers whose daughter is 9-14 pls RT ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 136 ],
        "url" : "http:\/\/t.co\/PV2GRhCl",
        "expanded_url" : "http:\/\/www.funkygoddess.ie\/gift-boxes\/welcome-to-womanhood\/",
        "display_url" : "funkygoddess.ie\/gift-boxes\/wel\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "229584779488661505",
    "text" : "Folks I need some sales today so I can buy POS.  If you know any Mums or Lone Fathers whose daughter is 9-14 pls RT http:\/\/t.co\/PV2GRhCl",
    "id" : 229584779488661505,
    "created_at" : "2012-07-29 14:30:51 +0000",
    "user" : {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "protected" : false,
      "id_str" : "326869253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946430925667229696\/wSG185FK_normal.jpg",
      "id" : 326869253,
      "verified" : true
    }
  },
  "id" : 229589466459803648,
  "created_at" : "2012-07-29 14:49:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229488870184607744",
  "text" : "\"Without website analytics you will always be in the realm of subjectivity.\"",
  "id" : 229488870184607744,
  "created_at" : "2012-07-29 08:09:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229488014479474691",
  "text" : "Wondering should I tell the recruiter: \"Oh you are hiring on behalf of Company X cos I saw the recruitment ad on its own website.\"",
  "id" : 229488014479474691,
  "created_at" : "2012-07-29 08:06:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229486841995337729",
  "text" : "Working on a proposal for a potential client on a Sunday morning striving to be brief, comprehensive and persuasive. Yes I am bragging.",
  "id" : 229486841995337729,
  "created_at" : "2012-07-29 08:01:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229485081473982464",
  "text" : "I know you trying to drive more twitter traffic to your blog but can you try not to tweet the same link again? THKS",
  "id" : 229485081473982464,
  "created_at" : "2012-07-29 07:54:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229484410389549056",
  "text" : "When you hit the big 40, sugar no longer goes with your coffee.",
  "id" : 229484410389549056,
  "created_at" : "2012-07-29 07:52:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 81, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 92, 112 ],
      "url" : "http:\/\/t.co\/w16eerC8",
      "expanded_url" : "http:\/\/www.tremeritus.com\/2012\/07\/29\/7-pap-town-councils-to-raise-fees-but-wp-town-council-wont\/",
      "display_url" : "tremeritus.com\/2012\/07\/29\/7-p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "229328995093721089",
  "text" : "I have \"5 years to repent\" as my apartment is under ruling party's town council. #Singapore http:\/\/t.co\/w16eerC8",
  "id" : 229328995093721089,
  "created_at" : "2012-07-28 21:34:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "229325994761285632",
  "geo" : { },
  "id_str" : "229326393350160384",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl If you in Dublin, we can meetup for tea.",
  "id" : 229326393350160384,
  "in_reply_to_status_id" : 229325994761285632,
  "created_at" : "2012-07-28 21:24:07 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "229229276451438592",
  "geo" : { },
  "id_str" : "229256498683985921",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl Olympic on TV?",
  "id" : 229256498683985921,
  "in_reply_to_status_id" : 229229276451438592,
  "created_at" : "2012-07-28 16:46:23 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gutter Bookshop",
      "screen_name" : "gutterbookshop",
      "indices" : [ 3, 18 ],
      "id_str" : "91151543",
      "id" : 91151543
    }, {
      "name" : "PhotoIreland.org",
      "screen_name" : "PhotoIreland",
      "indices" : [ 47, 60 ],
      "id_str" : "19349325",
      "id" : 19349325
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229201036357812225",
  "text" : "RT @gutterbookshop: It's the final weekend for @PhotoIreland Festival - get out there and view some lovely pictures!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "PhotoIreland.org",
        "screen_name" : "PhotoIreland",
        "indices" : [ 27, 40 ],
        "id_str" : "19349325",
        "id" : 19349325
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "229193993555173376",
    "text" : "It's the final weekend for @PhotoIreland Festival - get out there and view some lovely pictures!",
    "id" : 229193993555173376,
    "created_at" : "2012-07-28 12:38:00 +0000",
    "user" : {
      "name" : "Gutter Bookshop",
      "screen_name" : "gutterbookshop",
      "protected" : false,
      "id_str" : "91151543",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006920218034278401\/9tJSl2m5_normal.jpg",
      "id" : 91151543,
      "verified" : false
    }
  },
  "id" : 229201036357812225,
  "created_at" : "2012-07-28 13:06:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sian Phillips - Proofreader & Copy Editor",
      "screen_name" : "_Sians",
      "indices" : [ 3, 10 ],
      "id_str" : "18809102",
      "id" : 18809102
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "olympicceremony",
      "indices" : [ 69, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "229000325187653632",
  "text" : "RT @_Sians: Love the fact they all passed it on to the younger ones  #olympicceremony",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "olympicceremony",
        "indices" : [ 57, 73 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228997369105113088",
    "text" : "Love the fact they all passed it on to the younger ones  #olympicceremony",
    "id" : 228997369105113088,
    "created_at" : "2012-07-27 23:36:42 +0000",
    "user" : {
      "name" : "Sian Phillips - Proofreader & Copy Editor",
      "screen_name" : "_Sians",
      "protected" : false,
      "id_str" : "18809102",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/556466406317772802\/3bYVarGn_normal.jpeg",
      "id" : 18809102,
      "verified" : false
    }
  },
  "id" : 229000325187653632,
  "created_at" : "2012-07-27 23:48:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228991701828644866",
  "geo" : { },
  "id_str" : "228992577804840960",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl haha :)",
  "id" : 228992577804840960,
  "in_reply_to_status_id" : 228991701828644866,
  "created_at" : "2012-07-27 23:17:39 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228992355590619136",
  "text" : "I like the Queen\/Bond duet and Mr Bean at the Opening Ceremony. NHS?",
  "id" : 228992355590619136,
  "created_at" : "2012-07-27 23:16:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 3, 14 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Olympics2012",
      "indices" : [ 91, 104 ]
    }, {
      "text" : "Burberry",
      "indices" : [ 105, 114 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228991436002058241",
  "text" : "RT @davegantly: I was right about the chavs coming out towards the end. Gold collars? No!! #Olympics2012 #Burberry",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Olympics2012",
        "indices" : [ 75, 88 ]
      }, {
        "text" : "Burberry",
        "indices" : [ 89, 98 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228987545478914050",
    "text" : "I was right about the chavs coming out towards the end. Gold collars? No!! #Olympics2012 #Burberry",
    "id" : 228987545478914050,
    "created_at" : "2012-07-27 22:57:39 +0000",
    "user" : {
      "name" : "David",
      "screen_name" : "davegantly",
      "protected" : false,
      "id_str" : "16738907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001158218519863296\/rY-yu2r7_normal.jpg",
      "id" : 16738907,
      "verified" : false
    }
  },
  "id" : 228991436002058241,
  "created_at" : "2012-07-27 23:13:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228987833883455488",
  "geo" : { },
  "id_str" : "228991243579977730",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl exhausting for lady of her age.",
  "id" : 228991243579977730,
  "in_reply_to_status_id" : 228987833883455488,
  "created_at" : "2012-07-27 23:12:21 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anthony Mc Guinness",
      "screen_name" : "anthonymcg",
      "indices" : [ 3, 14 ],
      "id_str" : "5902592",
      "id" : 5902592
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "olympicceremony",
      "indices" : [ 64, 80 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228990907725271040",
  "text" : "RT @anthonymcg: It was nice of Jedward to design the GB outfits #olympicceremony",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "olympicceremony",
        "indices" : [ 48, 64 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228987765818265600",
    "text" : "It was nice of Jedward to design the GB outfits #olympicceremony",
    "id" : 228987765818265600,
    "created_at" : "2012-07-27 22:58:32 +0000",
    "user" : {
      "name" : "Anthony Mc Guinness",
      "screen_name" : "anthonymcg",
      "protected" : false,
      "id_str" : "5902592",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040903561645830145\/Dy_6louO_normal.jpg",
      "id" : 5902592,
      "verified" : false
    }
  },
  "id" : 228990907725271040,
  "created_at" : "2012-07-27 23:11:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Beatrice Whelan",
      "screen_name" : "beatricewhelan",
      "indices" : [ 0, 15 ],
      "id_str" : "201285346",
      "id" : 201285346
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228986344448344064",
  "geo" : { },
  "id_str" : "228986651320401922",
  "in_reply_to_user_id" : 201285346,
  "text" : "@beatricewhelan it is heavy?",
  "id" : 228986651320401922,
  "in_reply_to_status_id" : 228986344448344064,
  "created_at" : "2012-07-27 22:54:06 +0000",
  "in_reply_to_screen_name" : "beatricewhelan",
  "in_reply_to_user_id_str" : "201285346",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sheila at BWC",
      "screen_name" : "BlackWtrCastle",
      "indices" : [ 3, 18 ],
      "id_str" : "249765637",
      "id" : 249765637
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "openingceremony",
      "indices" : [ 20, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228986393819484160",
  "text" : "RT @BlackWtrCastle: #openingceremony Struck by athletes intent on recording the moment rather than experiencing it - live the moment!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "openingceremony",
        "indices" : [ 0, 16 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228985969418858496",
    "text" : "#openingceremony Struck by athletes intent on recording the moment rather than experiencing it - live the moment!",
    "id" : 228985969418858496,
    "created_at" : "2012-07-27 22:51:24 +0000",
    "user" : {
      "name" : "Sheila at BWC",
      "screen_name" : "BlackWtrCastle",
      "protected" : false,
      "id_str" : "249765637",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009425384545415170\/OZfH18mF_normal.jpg",
      "id" : 249765637,
      "verified" : false
    }
  },
  "id" : 228986393819484160,
  "created_at" : "2012-07-27 22:53:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Pearce",
      "screen_name" : "Pearcesport",
      "indices" : [ 3, 15 ],
      "id_str" : "26514893",
      "id" : 26514893
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228986089992499200",
  "text" : "RT @Pearcesport: Some spectators inside Opening Ceremony are using athletes' parade for a very British refreshment break http:\/\/t.co\/2b8 ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Pearcesport\/status\/228978184329244673\/photo\/1",
        "indices" : [ 104, 124 ],
        "url" : "http:\/\/t.co\/2b8L7VSC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Ay1-cgwCMAAVuon.jpg",
        "id_str" : "228978184358604800",
        "id" : 228978184358604800,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ay1-cgwCMAAVuon.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2b8L7VSC"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228978184329244673",
    "text" : "Some spectators inside Opening Ceremony are using athletes' parade for a very British refreshment break http:\/\/t.co\/2b8L7VSC",
    "id" : 228978184329244673,
    "created_at" : "2012-07-27 22:20:28 +0000",
    "user" : {
      "name" : "James Pearce",
      "screen_name" : "Pearcesport",
      "protected" : false,
      "id_str" : "26514893",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1803717208\/jamespearce4_normal.jpg",
      "id" : 26514893,
      "verified" : true
    }
  },
  "id" : 228986089992499200,
  "created_at" : "2012-07-27 22:51:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228985969666318336",
  "text" : "RT @paddyinpoland: black panther hats for America! wicked",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228985621962694657",
    "text" : "black panther hats for America! wicked",
    "id" : 228985621962694657,
    "created_at" : "2012-07-27 22:50:01 +0000",
    "user" : {
      "name" : "Spud Murphy\uD83C\uDF99\u26BD\uD83C\uDFAE\uD83D\uDCF8",
      "screen_name" : "Spudcast",
      "protected" : false,
      "id_str" : "95065106",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/970277472968368138\/5pPqGfWD_normal.jpg",
      "id" : 95065106,
      "verified" : false
    }
  },
  "id" : 228985969666318336,
  "created_at" : "2012-07-27 22:51:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228985781774065664",
  "text" : "I thought the French just trooped in....",
  "id" : 228985781774065664,
  "created_at" : "2012-07-27 22:50:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "tania branigan",
      "screen_name" : "taniabranigan",
      "indices" : [ 3, 17 ],
      "id_str" : "56967720",
      "id" : 56967720
    }, {
      "name" : "Mark Stephens",
      "screen_name" : "MarksLarks",
      "indices" : [ 22, 33 ],
      "id_str" : "91585149",
      "id" : 91585149
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "2012OlympicCeremony",
      "indices" : [ 100, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228985575397527552",
  "text" : "RT @taniabranigan: RT @markslarks: This would be so much shorter if the Soviet Union still existed. #2012OlympicCeremony",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Mark Stephens",
        "screen_name" : "MarksLarks",
        "indices" : [ 3, 14 ],
        "id_str" : "91585149",
        "id" : 91585149
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "2012OlympicCeremony",
        "indices" : [ 81, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228985038199476224",
    "text" : "RT @markslarks: This would be so much shorter if the Soviet Union still existed. #2012OlympicCeremony",
    "id" : 228985038199476224,
    "created_at" : "2012-07-27 22:47:42 +0000",
    "user" : {
      "name" : "tania branigan",
      "screen_name" : "taniabranigan",
      "protected" : false,
      "id_str" : "56967720",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/337692612\/profile_normal.jpg",
      "id" : 56967720,
      "verified" : false
    }
  },
  "id" : 228985575397527552,
  "created_at" : "2012-07-27 22:49:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228983798174461952",
  "text" : "Chinese Taipei - Republic of China.",
  "id" : 228983798174461952,
  "created_at" : "2012-07-27 22:42:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Neil Humphreys",
      "screen_name" : "NeilHumphreys",
      "indices" : [ 3, 17 ],
      "id_str" : "14843506",
      "id" : 14843506
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "openingceremony",
      "indices" : [ 32, 48 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228983085511892992",
  "text" : "RT @NeilHumphreys: Go Singapore #openingceremony",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "openingceremony",
        "indices" : [ 13, 29 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228982893786042368",
    "text" : "Go Singapore #openingceremony",
    "id" : 228982893786042368,
    "created_at" : "2012-07-27 22:39:10 +0000",
    "user" : {
      "name" : "Neil Humphreys",
      "screen_name" : "NeilHumphreys",
      "protected" : false,
      "id_str" : "14843506",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/920280113463271426\/DOv8jZ6O_normal.jpg",
      "id" : 14843506,
      "verified" : false
    }
  },
  "id" : 228983085511892992,
  "created_at" : "2012-07-27 22:39:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "tania branigan",
      "screen_name" : "taniabranigan",
      "indices" : [ 3, 17 ],
      "id_str" : "56967720",
      "id" : 56967720
    }, {
      "name" : "Jim Miller",
      "screen_name" : "DSLRinformer",
      "indices" : [ 22, 35 ],
      "id_str" : "3606013219",
      "id" : 3606013219
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 116 ],
      "url" : "http:\/\/t.co\/vv9c7dMy",
      "expanded_url" : "http:\/\/www.guardian.co.uk\/sport\/2012\/jul\/27\/london-olympics-2012-smartphone",
      "display_url" : "guardian.co.uk\/sport\/2012\/jul\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "228982620950765568",
  "text" : "RT @taniabranigan: RT @dslrinformer: My guardian photo blog of the Olympics shot with my IPhone http:\/\/t.co\/vv9c7dMy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jim Miller",
        "screen_name" : "DSLRinformer",
        "indices" : [ 3, 16 ],
        "id_str" : "3606013219",
        "id" : 3606013219
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 97 ],
        "url" : "http:\/\/t.co\/vv9c7dMy",
        "expanded_url" : "http:\/\/www.guardian.co.uk\/sport\/2012\/jul\/27\/london-olympics-2012-smartphone",
        "display_url" : "guardian.co.uk\/sport\/2012\/jul\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "228982223121039360",
    "text" : "RT @dslrinformer: My guardian photo blog of the Olympics shot with my IPhone http:\/\/t.co\/vv9c7dMy",
    "id" : 228982223121039360,
    "created_at" : "2012-07-27 22:36:30 +0000",
    "user" : {
      "name" : "tania branigan",
      "screen_name" : "taniabranigan",
      "protected" : false,
      "id_str" : "56967720",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/337692612\/profile_normal.jpg",
      "id" : 56967720,
      "verified" : false
    }
  },
  "id" : 228982620950765568,
  "created_at" : "2012-07-27 22:38:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conor Wilson",
      "screen_name" : "ConorWilson",
      "indices" : [ 3, 15 ],
      "id_str" : "22508221",
      "id" : 22508221
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228981132727840768",
  "text" : "RT @ConorWilson: I love the Heads of State waving like mad at their national teams. \n\nThey're like Mams and Dads sitting in the stands.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228980363731533825",
    "text" : "I love the Heads of State waving like mad at their national teams. \n\nThey're like Mams and Dads sitting in the stands.",
    "id" : 228980363731533825,
    "created_at" : "2012-07-27 22:29:07 +0000",
    "user" : {
      "name" : "Conor Wilson",
      "screen_name" : "ConorWilson",
      "protected" : false,
      "id_str" : "22508221",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/870281755508568065\/XN2yA9i9_normal.jpg",
      "id" : 22508221,
      "verified" : true
    }
  },
  "id" : 228981132727840768,
  "created_at" : "2012-07-27 22:32:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sian Phillips - Proofreader & Copy Editor",
      "screen_name" : "_Sians",
      "indices" : [ 3, 10 ],
      "id_str" : "18809102",
      "id" : 18809102
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228980842398113792",
  "text" : "RT @_Sians: David Beckham has come right around the UK and heading across the Irish sea now. He's nicked the Olympic flame for Ireland :)",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228980614269923328",
    "text" : "David Beckham has come right around the UK and heading across the Irish sea now. He's nicked the Olympic flame for Ireland :)",
    "id" : 228980614269923328,
    "created_at" : "2012-07-27 22:30:07 +0000",
    "user" : {
      "name" : "Sian Phillips - Proofreader & Copy Editor",
      "screen_name" : "_Sians",
      "protected" : false,
      "id_str" : "18809102",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/556466406317772802\/3bYVarGn_normal.jpeg",
      "id" : 18809102,
      "verified" : false
    }
  },
  "id" : 228980842398113792,
  "created_at" : "2012-07-27 22:31:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228980463715377154",
  "geo" : { },
  "id_str" : "228980675221540865",
  "in_reply_to_user_id" : 95065106,
  "text" : "@paddyinpoland oh yes baby!",
  "id" : 228980675221540865,
  "in_reply_to_status_id" : 228980463715377154,
  "created_at" : "2012-07-27 22:30:21 +0000",
  "in_reply_to_screen_name" : "Spudcast",
  "in_reply_to_user_id_str" : "95065106",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228980368357879808",
  "text" : "\u201C@paddyinpoland: Paraguay! fuck in hell, wow!\u201D u referring to the lady in red? :)",
  "id" : 228980368357879808,
  "created_at" : "2012-07-27 22:29:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The IvyGram",
      "screen_name" : "ivyjosiah",
      "indices" : [ 3, 13 ],
      "id_str" : "38389899",
      "id" : 38389899
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "London2012",
      "indices" : [ 108, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228979733688377345",
  "text" : "RT @ivyjosiah: Pandelela Rinong, Malaysian diver and our flag-bearer is Malaysia's FIRST female flag bearer.#London2012",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "London2012",
        "indices" : [ 93, 104 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228978056843370497",
    "text" : "Pandelela Rinong, Malaysian diver and our flag-bearer is Malaysia's FIRST female flag bearer.#London2012",
    "id" : 228978056843370497,
    "created_at" : "2012-07-27 22:19:57 +0000",
    "user" : {
      "name" : "The IvyGram",
      "screen_name" : "ivyjosiah",
      "protected" : false,
      "id_str" : "38389899",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/920306434394296320\/DzJeio5h_normal.jpg",
      "id" : 38389899,
      "verified" : false
    }
  },
  "id" : 228979733688377345,
  "created_at" : "2012-07-27 22:26:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Twitter UK",
      "screen_name" : "TwitterUK",
      "indices" : [ 3, 13 ],
      "id_str" : "277761722",
      "id" : 277761722
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 122 ],
      "url" : "http:\/\/t.co\/UxZLqAa8",
      "expanded_url" : "https:\/\/blog.uk.twitter.com\/",
      "display_url" : "blog.uk.twitter.com"
    } ]
  },
  "geo" : { },
  "id_str" : "228978759355744256",
  "text" : "RT @TwitterUK: There are now more Tweets in a single day than during the entire 2008 Beijing Games... http:\/\/t.co\/UxZLqAa8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 107 ],
        "url" : "http:\/\/t.co\/UxZLqAa8",
        "expanded_url" : "https:\/\/blog.uk.twitter.com\/",
        "display_url" : "blog.uk.twitter.com"
      } ]
    },
    "geo" : { },
    "id_str" : "228892006045343744",
    "text" : "There are now more Tweets in a single day than during the entire 2008 Beijing Games... http:\/\/t.co\/UxZLqAa8",
    "id" : 228892006045343744,
    "created_at" : "2012-07-27 16:38:01 +0000",
    "user" : {
      "name" : "Twitter UK",
      "screen_name" : "TwitterUK",
      "protected" : false,
      "id_str" : "277761722",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/941622204827078657\/gGPNg67y_normal.jpg",
      "id" : 277761722,
      "verified" : true
    }
  },
  "id" : 228978759355744256,
  "created_at" : "2012-07-27 22:22:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228976903606919168",
  "text" : "Hello ?! Why keep focusing on Team GB. Mrs wait so long for Malaysia team.",
  "id" : 228976903606919168,
  "created_at" : "2012-07-27 22:15:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228974645104234496",
  "text" : "Cheers for Ireland.",
  "id" : 228974645104234496,
  "created_at" : "2012-07-27 22:06:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Olympics",
      "indices" : [ 101, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228973649695236097",
  "text" : "RT @SimonPRepublic: Did anyone see the irony of North Korea coming out with the Ds for 'Democratic'? #Olympics",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Olympics",
        "indices" : [ 81, 90 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228973123817590784",
    "text" : "Did anyone see the irony of North Korea coming out with the Ds for 'Democratic'? #Olympics",
    "id" : 228973123817590784,
    "created_at" : "2012-07-27 22:00:21 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 228973649695236097,
  "created_at" : "2012-07-27 22:02:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228973403258904576",
  "text" : "Son, u can take a cat nap now. Singapore still long way to go.",
  "id" : 228973403258904576,
  "created_at" : "2012-07-27 22:01:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228972291508297729",
  "text" : "Now that our EU overlord.",
  "id" : 228972291508297729,
  "created_at" : "2012-07-27 21:57:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Annette Priest",
      "screen_name" : "AnnettePriest",
      "indices" : [ 3, 17 ],
      "id_str" : "885321",
      "id" : 885321
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228971297688932353",
  "text" : "RT @AnnettePriest: China seems very gracious waving UK flags as well as their own. Also, gold piping on the jackets gets my approval.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228968829382950913",
    "text" : "China seems very gracious waving UK flags as well as their own. Also, gold piping on the jackets gets my approval.",
    "id" : 228968829382950913,
    "created_at" : "2012-07-27 21:43:17 +0000",
    "user" : {
      "name" : "Annette Priest",
      "screen_name" : "AnnettePriest",
      "protected" : false,
      "id_str" : "885321",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972296102249558016\/ZGX56Wjj_normal.jpg",
      "id" : 885321,
      "verified" : false
    }
  },
  "id" : 228971297688932353,
  "created_at" : "2012-07-27 21:53:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228970488666415104",
  "text" : "Denmark ladies are glorious...",
  "id" : 228970488666415104,
  "created_at" : "2012-07-27 21:49:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228968988972048384",
  "text" : "Colombia flag bearer is so pretty.",
  "id" : 228968988972048384,
  "created_at" : "2012-07-27 21:43:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228954864087072769",
  "text" : "Next expect NHS to be in the pic...for the rest of the world they will scratching their head.",
  "id" : 228954864087072769,
  "created_at" : "2012-07-27 20:47:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228954327014854658",
  "text" : "RT @topgold: I never expected to see a Queen Bond moment.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228952269738102785",
    "text" : "I never expected to see a Queen Bond moment.",
    "id" : 228952269738102785,
    "created_at" : "2012-07-27 20:37:29 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 228954327014854658,
  "created_at" : "2012-07-27 20:45:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "olympic",
      "indices" : [ 27, 35 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228929344368693248",
  "text" : "Animal Farm? Middle-earth? #olympic",
  "id" : 228929344368693248,
  "created_at" : "2012-07-27 19:06:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http:\/\/t.co\/zGUs5QUW",
      "expanded_url" : "http:\/\/www.facebook.com\/media\/set\/?set=a.339746716107444.76541.334274156654700&type=3",
      "display_url" : "facebook.com\/media\/set\/?set\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "228893696052051968",
  "text" : "Singapore (old) Currency note is hugely influenced by Fengshui.  http:\/\/t.co\/zGUs5QUW",
  "id" : 228893696052051968,
  "created_at" : "2012-07-27 16:44:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 68 ],
      "url" : "http:\/\/t.co\/FUSZlOvi",
      "expanded_url" : "http:\/\/nyti.ms\/QlXVfV",
      "display_url" : "nyti.ms\/QlXVfV"
    } ]
  },
  "geo" : { },
  "id_str" : "228892158017536000",
  "text" : "In Singapore, Vitriol Against Chinese Newcomers http:\/\/t.co\/FUSZlOvi &lt;&lt; Spore govt, immigration should be controlled just like Australia.",
  "id" : 228892158017536000,
  "created_at" : "2012-07-27 16:38:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Roy Cheong",
      "screen_name" : "roycheong1",
      "indices" : [ 3, 14 ],
      "id_str" : "20221897",
      "id" : 20221897
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 38, 48 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228710295596580865",
  "text" : "RT @roycheong1: Who spill the beans ? #Singapore",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 22, 32 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228703599591571456",
    "text" : "Who spill the beans ? #Singapore",
    "id" : 228703599591571456,
    "created_at" : "2012-07-27 04:09:21 +0000",
    "user" : {
      "name" : "Roy Cheong",
      "screen_name" : "roycheong1",
      "protected" : false,
      "id_str" : "20221897",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/419286998671585280\/KQ6tQJqP_normal.jpeg",
      "id" : 20221897,
      "verified" : false
    }
  },
  "id" : 228710295596580865,
  "created_at" : "2012-07-27 04:35:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "A Blog About History",
      "screen_name" : "historytweeter",
      "indices" : [ 3, 18 ],
      "id_str" : "37118832",
      "id" : 37118832
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "history",
      "indices" : [ 20, 28 ]
    } ],
    "urls" : [ {
      "indices" : [ 78, 98 ],
      "url" : "http:\/\/t.co\/v6JOcH8g",
      "expanded_url" : "http:\/\/goo.gl\/fb\/2by0z",
      "display_url" : "goo.gl\/fb\/2by0z"
    } ]
  },
  "geo" : { },
  "id_str" : "228702866595004416",
  "text" : "RT @historytweeter: #history 1,800-year-old General\u2019s tomb excavated in China http:\/\/t.co\/v6JOcH8g",
  "id" : 228702866595004416,
  "created_at" : "2012-07-27 04:06:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228696087383392256",
  "text" : "Good morning. 4:45am. Waking up at this time is a sign of getting old.",
  "id" : 228696087383392256,
  "created_at" : "2012-07-27 03:39:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.yelp.com\/\" rel=\"nofollow\"\u003EYelp\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Yelp",
      "indices" : [ 14, 19 ]
    } ],
    "urls" : [ {
      "indices" : [ 119, 139 ],
      "url" : "http:\/\/t.co\/babwWzTJ",
      "expanded_url" : "http:\/\/bit.ly\/N6YLp4",
      "display_url" : "bit.ly\/N6YLp4"
    } ]
  },
  "geo" : { },
  "id_str" : "228695116632686593",
  "text" : "KC Peaches on #Yelp: Disappointed. No one came and clean the table when new customers take over the table. We don't g\u2026 http:\/\/t.co\/babwWzTJ",
  "id" : 228695116632686593,
  "created_at" : "2012-07-27 03:35:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228692190535237632",
  "text" : "\u4EBA\u5230\u4E2D\u5E74\uFF0C\u559D\u8336\u5DF2\u4E0D\u52A0\u7CD6\u4E86\u3002",
  "id" : 228692190535237632,
  "created_at" : "2012-07-27 03:24:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kathryn Presner",
      "screen_name" : "zoonini",
      "indices" : [ 3, 11 ],
      "id_str" : "16671466",
      "id" : 16671466
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228607811079254016",
  "text" : "RT @zoonini I can't believe people still send out HTML email newsletters that consist solely of a single image.",
  "id" : 228607811079254016,
  "created_at" : "2012-07-26 21:48:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 0, 14 ],
      "id_str" : "80549724",
      "id" : 80549724
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228600253971050497",
  "in_reply_to_user_id" : 80549724,
  "text" : "@dublinbusnews By any chance, your 46a bus driver came across a sport cap today? Thks in advance.",
  "id" : 228600253971050497,
  "created_at" : "2012-07-26 21:18:42 +0000",
  "in_reply_to_screen_name" : "dublinbusnews",
  "in_reply_to_user_id_str" : "80549724",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http:\/\/t.co\/7iTXUZbN",
      "expanded_url" : "http:\/\/www.brayairdisplay.com\/",
      "display_url" : "brayairdisplay.com"
    } ]
  },
  "geo" : { },
  "id_str" : "228599520303386624",
  "text" : "First ever aerial proposal at an Irish air show. http:\/\/t.co\/7iTXUZbN",
  "id" : 228599520303386624,
  "created_at" : "2012-07-26 21:15:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brendan Hughes",
      "screen_name" : "brendanhughes",
      "indices" : [ 3, 17 ],
      "id_str" : "11754792",
      "id" : 11754792
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 92 ],
      "url" : "http:\/\/t.co\/i2hCCHpu",
      "expanded_url" : "http:\/\/mashable.com\/2012\/07\/26\/madonna-paris-show\/",
      "display_url" : "mashable.com\/2012\/07\/26\/mad\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "228597590378307585",
  "text" : "RT @brendanhughes: Madonna's Paris Show being Live Streamed NOW YouTube http:\/\/t.co\/i2hCCHpu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 73 ],
        "url" : "http:\/\/t.co\/i2hCCHpu",
        "expanded_url" : "http:\/\/mashable.com\/2012\/07\/26\/madonna-paris-show\/",
        "display_url" : "mashable.com\/2012\/07\/26\/mad\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "228597255161139201",
    "text" : "Madonna's Paris Show being Live Streamed NOW YouTube http:\/\/t.co\/i2hCCHpu",
    "id" : 228597255161139201,
    "created_at" : "2012-07-26 21:06:47 +0000",
    "user" : {
      "name" : "Brendan Hughes",
      "screen_name" : "brendanhughes",
      "protected" : false,
      "id_str" : "11754792",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460854132824211456\/t2pxlCih_normal.jpeg",
      "id" : 11754792,
      "verified" : false
    }
  },
  "id" : 228597590378307585,
  "created_at" : "2012-07-26 21:08:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228594704458067968",
  "text" : "one more month of school summer break...counting down.",
  "id" : 228594704458067968,
  "created_at" : "2012-07-26 20:56:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kathryn Presner",
      "screen_name" : "zoonini",
      "indices" : [ 3, 11 ],
      "id_str" : "16671466",
      "id" : 16671466
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228591273081176064",
  "text" : "RT @zoonini: Can anyone suggest a free, easy, screencast tool for Mac 10.6?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228587556290306048",
    "text" : "Can anyone suggest a free, easy, screencast tool for Mac 10.6?",
    "id" : 228587556290306048,
    "created_at" : "2012-07-26 20:28:15 +0000",
    "user" : {
      "name" : "Kathryn Presner",
      "screen_name" : "zoonini",
      "protected" : false,
      "id_str" : "16671466",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2209749690\/KathrynPresner_0757_web-square_normal.jpg",
      "id" : 16671466,
      "verified" : false
    }
  },
  "id" : 228591273081176064,
  "created_at" : "2012-07-26 20:43:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 0, 10 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228404103917731840",
  "geo" : { },
  "id_str" : "228581960048590849",
  "in_reply_to_user_id" : 10737,
  "text" : "@patphelan Heard so much story about that Redline. IN fact, I took Dublin bus to phoenix park today instead of Luas.",
  "id" : 228581960048590849,
  "in_reply_to_status_id" : 228404103917731840,
  "created_at" : "2012-07-26 20:06:00 +0000",
  "in_reply_to_screen_name" : "patphelan",
  "in_reply_to_user_id_str" : "10737",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228429560121790465",
  "geo" : { },
  "id_str" : "228431249394503680",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl Good morning. Same here in Dublin. Scorching Hot",
  "id" : 228431249394503680,
  "in_reply_to_status_id" : 228429560121790465,
  "created_at" : "2012-07-26 10:07:08 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228409021026140160",
  "geo" : { },
  "id_str" : "228411005695315968",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney still shopping around.",
  "id" : 228411005695315968,
  "in_reply_to_status_id" : 228409021026140160,
  "created_at" : "2012-07-26 08:46:42 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228409976090161152",
  "text" : "I no longer look for unlimited GB of space when shopping for hosting.",
  "id" : 228409976090161152,
  "created_at" : "2012-07-26 08:42:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228407511236112384",
  "geo" : { },
  "id_str" : "228407984752062467",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney Good Morning to you!",
  "id" : 228407984752062467,
  "in_reply_to_status_id" : 228407511236112384,
  "created_at" : "2012-07-26 08:34:41 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kellie Fullmer",
      "screen_name" : "kellfu2",
      "indices" : [ 39, 47 ],
      "id_str" : "364458100",
      "id" : 364458100
    }, {
      "name" : "Lonely Planet",
      "screen_name" : "lonelyplanet",
      "indices" : [ 49, 62 ],
      "id_str" : "15066760",
      "id" : 15066760
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 75, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http:\/\/t.co\/wQaGYlxU",
      "expanded_url" : "https:\/\/twitter.com\/kellfu2\/status\/228317491171041280\/photo\/1",
      "display_url" : "pic.twitter.com\/wQaGYlxU"
    } ]
  },
  "geo" : { },
  "id_str" : "228354874113413121",
  "text" : "FYI. They don't exist in Singapore. RT @kellfu2: @lonelyplanet Rice Fields #Singapore http:\/\/t.co\/wQaGYlxU",
  "id" : 228354874113413121,
  "created_at" : "2012-07-26 05:03:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228249849198424066",
  "geo" : { },
  "id_str" : "228259869306781696",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney Thks a million. I will think about it",
  "id" : 228259869306781696,
  "in_reply_to_status_id" : 228249849198424066,
  "created_at" : "2012-07-25 22:46:08 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Niamh O'Reilly",
      "screen_name" : "nurserydublin",
      "indices" : [ 3, 17 ],
      "id_str" : "249793310",
      "id" : 249793310
    }, {
      "name" : "Tus Nua Designs",
      "screen_name" : "TusNuaDesigns",
      "indices" : [ 73, 87 ],
      "id_str" : "1372338342",
      "id" : 1372338342
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 55, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228258653654876162",
  "text" : "RT @nurserydublin I have an app idea. Can anyone help? #irishbizparty CC @TusNuaDesigns",
  "id" : 228258653654876162,
  "created_at" : "2012-07-25 22:41:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 90, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228249460612952064",
  "text" : "Want to switch web hosting. If you can offer less than 119 USD per year. I will consider. #irishbizparty",
  "id" : 228249460612952064,
  "created_at" : "2012-07-25 22:04:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 124, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228247884594827264",
  "text" : "Turn visitors into paying customers by testing &amp; improving your site. No result, No Fee. Interested? Msg me for detail. #irishbizparty",
  "id" : 228247884594827264,
  "created_at" : "2012-07-25 21:58:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 73, 87 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228246431855681536",
  "text" : "I help you make sense of Web Analytics data so you can sell more online. #irishbizparty",
  "id" : 228246431855681536,
  "created_at" : "2012-07-25 21:52:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 66, 80 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228229508090716160",
  "text" : "Think I might stick with existing site and domain name for tonite #irishbizparty. The site building template is awful.",
  "id" : 228229508090716160,
  "created_at" : "2012-07-25 20:45:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228207040063995904",
  "geo" : { },
  "id_str" : "228225448931368960",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl Website and domain name ready!",
  "id" : 228225448931368960,
  "in_reply_to_status_id" : 228207040063995904,
  "created_at" : "2012-07-25 20:29:21 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228216024548392960",
  "text" : "\"Nameserver updates on this domains are prohibited by the Terms of Service for its first year of registration.\" Damn!",
  "id" : 228216024548392960,
  "created_at" : "2012-07-25 19:51:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizpart",
      "indices" : [ 82, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228210037024235521",
  "text" : "Have to read bedtime story for kid and sort out the template design before 9:30pm #irishbizpart",
  "id" : 228210037024235521,
  "created_at" : "2012-07-25 19:28:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 33, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228209677572399104",
  "text" : "Hosting and domain name done for #irishbizparty but the free template leave much to be desire.",
  "id" : 228209677572399104,
  "created_at" : "2012-07-25 19:26:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 28, 42 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228204662224990209",
  "text" : "Can I get ready for tonite  #irishbizparty 9:30pm with a domain name and a new website?",
  "id" : 228204662224990209,
  "created_at" : "2012-07-25 19:06:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 78, 91 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 92, 99 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228160125028945923",
  "text" : "Do you know which pages do people view before reaching the registration form? #SMEcommunity #irebiz",
  "id" : 228160125028945923,
  "created_at" : "2012-07-25 16:09:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Tai",
      "screen_name" : "jasontaicc",
      "indices" : [ 0, 11 ],
      "id_str" : "142606482",
      "id" : 142606482
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228148762072190977",
  "geo" : { },
  "id_str" : "228151434456211458",
  "in_reply_to_user_id" : 142606482,
  "text" : "@jasontaicc They did create a lot of buzz. Some will consider MBS for their next holiday. (But not that photo booth that link to FB)  :)",
  "id" : 228151434456211458,
  "in_reply_to_status_id" : 228148762072190977,
  "created_at" : "2012-07-25 15:35:15 +0000",
  "in_reply_to_screen_name" : "jasontaicc",
  "in_reply_to_user_id_str" : "142606482",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bryan O'Brien",
      "screen_name" : "BryanJOBrien",
      "indices" : [ 3, 16 ],
      "id_str" : "352478969",
      "id" : 352478969
    }, {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 18, 29 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228150268427124736",
  "text" : "RT @BryanJOBrien: @IrishTimes colleague has just had two Nikon D4 bodies, 2 lenses &amp; flash stolen if offered any or hear anything pl ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Irish Times",
        "screen_name" : "IrishTimes",
        "indices" : [ 0, 11 ],
        "id_str" : "15084853",
        "id" : 15084853
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228143910613049345",
    "in_reply_to_user_id" : 15084853,
    "text" : "@IrishTimes colleague has just had two Nikon D4 bodies, 2 lenses &amp; flash stolen if offered any or hear anything please contact me. Please rt",
    "id" : 228143910613049345,
    "created_at" : "2012-07-25 15:05:21 +0000",
    "in_reply_to_screen_name" : "IrishTimes",
    "in_reply_to_user_id_str" : "15084853",
    "user" : {
      "name" : "Bryan O'Brien",
      "screen_name" : "BryanJOBrien",
      "protected" : false,
      "id_str" : "352478969",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/888444194880204800\/WhtJFDIx_normal.jpg",
      "id" : 352478969,
      "verified" : true
    }
  },
  "id" : 228150268427124736,
  "created_at" : "2012-07-25 15:30:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mishal Husain",
      "screen_name" : "MishalHusainBBC",
      "indices" : [ 3, 19 ],
      "id_str" : "74265849",
      "id" : 74265849
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228144767190585344",
  "text" : "RT @MishalHusainBBC: Highly recommended: new BBC Olympics app. Incl wonderful moments from Olympics past",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228143888794275840",
    "text" : "Highly recommended: new BBC Olympics app. Incl wonderful moments from Olympics past",
    "id" : 228143888794275840,
    "created_at" : "2012-07-25 15:05:16 +0000",
    "user" : {
      "name" : "Mishal Husain",
      "screen_name" : "MishalHusainBBC",
      "protected" : false,
      "id_str" : "74265849",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1641915046\/mishal_normal.jpg",
      "id" : 74265849,
      "verified" : true
    }
  },
  "id" : 228144767190585344,
  "created_at" : "2012-07-25 15:08:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hiroko Tabuchi",
      "screen_name" : "HirokoTabuchi",
      "indices" : [ 3, 17 ],
      "id_str" : "8442372",
      "id" : 8442372
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228132163692920834",
  "text" : "RT @HirokoTabuchi: Are Japanese world's biggest train nerds? Gigantic poster announcing new photobook of 5000 stations\/depots across Jap ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ECamera on iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/HirokoTabuchi\/status\/228130710177865728\/photo\/1",
        "indices" : [ 120, 140 ],
        "url" : "http:\/\/t.co\/hlYN0VwN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Ayp7rBcCYAArh4Z.jpg",
        "id_str" : "228130710186254336",
        "id" : 228130710186254336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ayp7rBcCYAArh4Z.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 816
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 816
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 816
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hlYN0VwN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "228130710177865728",
    "text" : "Are Japanese world's biggest train nerds? Gigantic poster announcing new photobook of 5000 stations\/depots across Japan http:\/\/t.co\/hlYN0VwN",
    "id" : 228130710177865728,
    "created_at" : "2012-07-25 14:12:55 +0000",
    "user" : {
      "name" : "Hiroko Tabuchi",
      "screen_name" : "HirokoTabuchi",
      "protected" : false,
      "id_str" : "8442372",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3653996497\/4e0a4cdc66345410396a3956455984da_normal.jpeg",
      "id" : 8442372,
      "verified" : true
    }
  },
  "id" : 228132163692920834,
  "created_at" : "2012-07-25 14:18:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 109, 122 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 123, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228131117608366080",
  "text" : "Do you know the busiest period of relevant traffic to your website and what leads to this increase in visit? #SMEcommunity #irebiz",
  "id" : 228131117608366080,
  "created_at" : "2012-07-25 14:14:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http:\/\/t.co\/GEZB7tAT",
      "expanded_url" : "http:\/\/bit.ly\/QDCGF9",
      "display_url" : "bit.ly\/QDCGF9"
    } ]
  },
  "geo" : { },
  "id_str" : "228122013947023360",
  "text" : "The most expensive cities to have an office: http:\/\/t.co\/GEZB7tAT",
  "id" : 228122013947023360,
  "created_at" : "2012-07-25 13:38:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Grafton Media",
      "screen_name" : "GraftonMedia",
      "indices" : [ 3, 16 ],
      "id_str" : "374067886",
      "id" : 374067886
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "digital",
      "indices" : [ 114, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "228121167276437505",
  "text" : "RT @GraftonMedia: 64% of senior decision makers don\u2019t see the internet as top channel for driving overseas trade  #digital http:\/\/t.co\/w ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "digital",
        "indices" : [ 96, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 105, 125 ],
        "url" : "http:\/\/t.co\/woeQ2wQt",
        "expanded_url" : "http:\/\/bit.ly\/OhVszm",
        "display_url" : "bit.ly\/OhVszm"
      } ]
    },
    "geo" : { },
    "id_str" : "228118047599239168",
    "text" : "64% of senior decision makers don\u2019t see the internet as top channel for driving overseas trade  #digital http:\/\/t.co\/woeQ2wQt",
    "id" : 228118047599239168,
    "created_at" : "2012-07-25 13:22:35 +0000",
    "user" : {
      "name" : "Grafton Media",
      "screen_name" : "GraftonMedia",
      "protected" : false,
      "id_str" : "374067886",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2728688712\/614ba5014a92cbecf31c20f07b8e57c1_normal.png",
      "id" : 374067886,
      "verified" : false
    }
  },
  "id" : 228121167276437505,
  "created_at" : "2012-07-25 13:34:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MBSman",
      "indices" : [ 30, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http:\/\/t.co\/0o8IeTwN",
      "expanded_url" : "http:\/\/www.dedrickkoh.com\/2012\/06\/22\/the-social-pavilion-at-the-shoppes-at-marina-bay-sands-bad-on-so-many-levels\/",
      "display_url" : "dedrickkoh.com\/2012\/06\/22\/the\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "228119546123399169",
  "text" : "Now that where this so called #MBSman came about. http:\/\/t.co\/0o8IeTwN",
  "id" : 228119546123399169,
  "created_at" : "2012-07-25 13:28:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "measure",
      "indices" : [ 117, 125 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 116 ],
      "url" : "http:\/\/t.co\/XwZjXFL2",
      "expanded_url" : "http:\/\/adage.com\/article\/digitalnext\/facebook-s-metrics-changing-game-marketers\/236218\/",
      "display_url" : "adage.com\/article\/digita\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "228093495607435264",
  "text" : "Facebook introduces changes in conversion, engagement metrics and goodbye to  outdated metrics. http:\/\/t.co\/XwZjXFL2 #measure",
  "id" : 228093495607435264,
  "created_at" : "2012-07-25 11:45:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http:\/\/t.co\/QJCCuSVY",
      "expanded_url" : "http:\/\/www.facebook.com\/photo.php?fbid=244981842288642&set=a.212366955550131.51708.100003304835646&type=1&theater",
      "display_url" : "facebook.com\/photo.php?fbid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "228092028179845121",
  "text" : "Thank to the training, he survived the 4-storey fall. http:\/\/t.co\/QJCCuSVY",
  "id" : 228092028179845121,
  "created_at" : "2012-07-25 11:39:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amanda",
      "screen_name" : "Hamperlady",
      "indices" : [ 0, 11 ],
      "id_str" : "19398524",
      "id" : 19398524
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "228025993800056834",
  "geo" : { },
  "id_str" : "228031386051825664",
  "in_reply_to_user_id" : 19398524,
  "text" : "@Hamperlady Good Morning!",
  "id" : 228031386051825664,
  "in_reply_to_status_id" : 228025993800056834,
  "created_at" : "2012-07-25 07:38:13 +0000",
  "in_reply_to_screen_name" : "Hamperlady",
  "in_reply_to_user_id_str" : "19398524",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227890501192404992",
  "text" : "Hire me to turn visitors into paying customers by testing &amp; improving your site. Only Pay for improved result. Interested? Msg me.",
  "id" : 227890501192404992,
  "created_at" : "2012-07-24 22:18:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227865667527929856",
  "text" : "Do you know the Conversion Rate by the Top Five Referring URLs for your website?",
  "id" : 227865667527929856,
  "created_at" : "2012-07-24 20:39:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore",
      "screen_name" : "mySingapore",
      "indices" : [ 3, 15 ],
      "id_str" : "18099511",
      "id" : 18099511
    }, {
      "name" : "The Telegraph",
      "screen_name" : "Telegraph",
      "indices" : [ 85, 95 ],
      "id_str" : "16343974",
      "id" : 16343974
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http:\/\/t.co\/twODKTuh",
      "expanded_url" : "http:\/\/soc.li\/sA7o5bN",
      "display_url" : "soc.li\/sA7o5bN"
    } ]
  },
  "geo" : { },
  "id_str" : "227856202426630144",
  "text" : "RT @mySingapore: Singapore tightens rules for expat workers http:\/\/t.co\/twODKTuh via @Telegraph",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Telegraph",
        "screen_name" : "Telegraph",
        "indices" : [ 68, 78 ],
        "id_str" : "16343974",
        "id" : 16343974
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 43, 63 ],
        "url" : "http:\/\/t.co\/twODKTuh",
        "expanded_url" : "http:\/\/soc.li\/sA7o5bN",
        "display_url" : "soc.li\/sA7o5bN"
      } ]
    },
    "geo" : { },
    "id_str" : "227855788876656640",
    "text" : "Singapore tightens rules for expat workers http:\/\/t.co\/twODKTuh via @Telegraph",
    "id" : 227855788876656640,
    "created_at" : "2012-07-24 20:00:28 +0000",
    "user" : {
      "name" : "Singapore",
      "screen_name" : "mySingapore",
      "protected" : false,
      "id_str" : "18099511",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/132370352\/396x297_normal.jpg",
      "id" : 18099511,
      "verified" : false
    }
  },
  "id" : 227856202426630144,
  "created_at" : "2012-07-24 20:02:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 91 ],
      "url" : "https:\/\/t.co\/8RYnVv5S",
      "expanded_url" : "https:\/\/attendee.gotowebinar.com\/register\/1908544721878215936",
      "display_url" : "attendee.gotowebinar.com\/register\/19085\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "227854731194818561",
  "text" : "FREE - How to Get Better Insights in Less Time With Google Analytics. https:\/\/t.co\/8RYnVv5S",
  "id" : 227854731194818561,
  "created_at" : "2012-07-24 19:56:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227633490097745920",
  "text" : "\"you must pay a fee of \u20AC500 when you are granted Long Term Residency.(in Ireland)\" Ouch!",
  "id" : 227633490097745920,
  "created_at" : "2012-07-24 05:17:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227632732249935872",
  "text" : "Cup of pipping hot coffee to start the day. Woke up at 5am!",
  "id" : 227632732249935872,
  "created_at" : "2012-07-24 05:14:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227520234666483713",
  "text" : "Tracking comments in Google Analytics  lets you see what drives people to comment on your blog.",
  "id" : 227520234666483713,
  "created_at" : "2012-07-23 21:47:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 117 ],
      "url" : "https:\/\/t.co\/iafQIA08",
      "expanded_url" : "https:\/\/www.google.com\/analytics\/siteopt\/siteopt\/help\/calculator.html",
      "display_url" : "google.com\/analytics\/site\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "227516444563361792",
  "text" : "Calculator to help you estimate how long it will take to run a test for improving your website. https:\/\/t.co\/iafQIA08",
  "id" : 227516444563361792,
  "created_at" : "2012-07-23 21:32:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "227509389567868928",
  "geo" : { },
  "id_str" : "227511339277512704",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl maybe you can try personal account instead of biz account?",
  "id" : 227511339277512704,
  "in_reply_to_status_id" : 227509389567868928,
  "created_at" : "2012-07-23 21:11:44 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "227509389567868928",
  "geo" : { },
  "id_str" : "227510786778611713",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl What about register on ebay,com? When I started, they  do not need a VAT number. I not sure about the address part.",
  "id" : 227510786778611713,
  "in_reply_to_status_id" : 227509389567868928,
  "created_at" : "2012-07-23 21:09:33 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227509106527846400",
  "text" : "RT @SimonPRepublic: Loving the documentary about RTE. Can't believe it was basically run by the Catholic church. S'pose it's still run b ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TV50",
        "indices" : [ 132, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "227508545850073090",
    "text" : "Loving the documentary about RTE. Can't believe it was basically run by the Catholic church. S'pose it's still run by an ex-priest. #TV50",
    "id" : 227508545850073090,
    "created_at" : "2012-07-23 21:00:38 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 227509106527846400,
  "created_at" : "2012-07-23 21:02:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http:\/\/t.co\/jGqlWD2M",
      "expanded_url" : "http:\/\/myworld.ebay.com.sg\/mryap\/",
      "display_url" : "myworld.ebay.com.sg\/mryap\/"
    } ]
  },
  "in_reply_to_status_id_str" : "227494196532105217",
  "geo" : { },
  "id_str" : "227507168524529664",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl I used to dabble on ebay as a seller. http:\/\/t.co\/jGqlWD2M",
  "id" : 227507168524529664,
  "in_reply_to_status_id" : 227494196532105217,
  "created_at" : "2012-07-23 20:55:10 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 3, 12 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227497164094464001",
  "text" : "RT @maryrose: Can anyone recommend a good removalist? Northside Dublin.   Pls RT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "227423301687988224",
    "text" : "Can anyone recommend a good removalist? Northside Dublin.   Pls RT",
    "id" : 227423301687988224,
    "created_at" : "2012-07-23 15:21:55 +0000",
    "user" : {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "protected" : false,
      "id_str" : "1291131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034068926370594816\/eS9-sUNe_normal.jpg",
      "id" : 1291131,
      "verified" : false
    }
  },
  "id" : 227497164094464001,
  "created_at" : "2012-07-23 20:15:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Grehan",
      "screen_name" : "mikegrehan",
      "indices" : [ 45, 56 ],
      "id_str" : "10218832",
      "id" : 10218832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 140 ],
      "url" : "http:\/\/t.co\/hmmHbw63",
      "expanded_url" : "http:\/\/googlemobileads.blogspot.com\/2012\/07\/mobile-websites-vs-responsive-design.html?spref=tw",
      "display_url" : "googlemobileads.blogspot.com\/2012\/07\/mobile\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "227493486142840832",
  "text" : "Which one is better for your mobile site? RT @mikegrehan: Google Mobile Ads Blog: Mobile Websites vs Responsive Design: http:\/\/t.co\/hmmHbw63",
  "id" : 227493486142840832,
  "created_at" : "2012-07-23 20:00:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sky News",
      "screen_name" : "SkyNews",
      "indices" : [ 3, 11 ],
      "id_str" : "7587032",
      "id" : 7587032
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http:\/\/t.co\/pYYBz2Wk",
      "expanded_url" : "http:\/\/bit.ly\/OLCk9f",
      "display_url" : "bit.ly\/OLCk9f"
    } ]
  },
  "geo" : { },
  "id_str" : "227491269184741376",
  "text" : "RT @SkyNews: India: Boss Beaten And Burned Alive By Workers http:\/\/t.co\/pYYBz2Wk",
  "id" : 227491269184741376,
  "created_at" : "2012-07-23 19:51:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227475805092081664",
  "text" : "Lack of time and resources in web analytics usage? I can help.",
  "id" : 227475805092081664,
  "created_at" : "2012-07-23 18:50:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227455528652115968",
  "text" : "Today weather make me feel like I am in Singapore....",
  "id" : 227455528652115968,
  "created_at" : "2012-07-23 17:29:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bray Air Display",
      "screen_name" : "BrayAirShow",
      "indices" : [ 94, 106 ],
      "id_str" : "564475504",
      "id" : 564475504
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RandomKindness",
      "indices" : [ 107, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227452779055157248",
  "text" : "Thanks to a mother leaving early give her children ticket to the Bouncy World for my child at @BrayAirShow #RandomKindness",
  "id" : 227452779055157248,
  "created_at" : "2012-07-23 17:19:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Olympics",
      "indices" : [ 129, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227449499575009280",
  "text" : "ON FB group, Singaporeans in UK complained Singapore flag not displayed at a certain street. C'mon get a life. FFS Singaporeans. #Olympics.",
  "id" : 227449499575009280,
  "created_at" : "2012-07-23 17:06:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227445599895691264",
  "text" : "Today is the start of a new beginning. Feeling scary...but helps to kick start a lot of thing.",
  "id" : 227445599895691264,
  "created_at" : "2012-07-23 16:50:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tony Smith",
      "screen_name" : "TonyNewsCamera",
      "indices" : [ 3, 18 ],
      "id_str" : "242366566",
      "id" : 242366566
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "notsurprised",
      "indices" : [ 69, 82 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227435105700937728",
  "text" : "RT @TonyNewsCamera: So Prince Harry is going to the beach volleyball #notsurprised",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "notsurprised",
        "indices" : [ 49, 62 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "227434310981001216",
    "text" : "So Prince Harry is going to the beach volleyball #notsurprised",
    "id" : 227434310981001216,
    "created_at" : "2012-07-23 16:05:39 +0000",
    "user" : {
      "name" : "Tony Smith",
      "screen_name" : "TonyNewsCamera",
      "protected" : false,
      "id_str" : "242366566",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2259947469\/IMG_1129_cropped_and_smaller_normal.jpg",
      "id" : 242366566,
      "verified" : false
    }
  },
  "id" : 227435105700937728,
  "created_at" : "2012-07-23 16:08:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227428666110517249",
  "text" : "Got goosebumps now writing a cover-letter.",
  "id" : 227428666110517249,
  "created_at" : "2012-07-23 15:43:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "227428233040236544",
  "text" : "Writing a cover-letter for employment is like writing love letter - you want to convince the recipient to show you are the one &amp; only.",
  "id" : 227428233040236544,
  "created_at" : "2012-07-23 15:41:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http:\/\/t.co\/w7ie7rbM",
      "expanded_url" : "http:\/\/news.yahoo.com\/aurora-suspect-james-holmess-familys-painful-emotional-hurdles-223115000--abc-news-health.html",
      "display_url" : "news.yahoo.com\/aurora-suspect\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "227419654480601088",
  "text" : "Let spare a thought for the suspect's family http:\/\/t.co\/w7ie7rbM",
  "id" : 227419654480601088,
  "created_at" : "2012-07-23 15:07:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 74, 87 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 88, 95 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 73 ],
      "url" : "http:\/\/t.co\/ZwxNUPGp",
      "expanded_url" : "http:\/\/www.seo.com\/blog\/10-things-you-must-do-to-prepare-your-business-for-seo\/#ixzz21L2Tw8Xg",
      "display_url" : "seo.com\/blog\/10-things\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "226957837086113792",
  "text" : "\"Be Ready To Give Up Full Analytics Access\" for SEO  http:\/\/t.co\/ZwxNUPGp #SMEcommunity #irebiz",
  "id" : 226957837086113792,
  "created_at" : "2012-07-22 08:32:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226956757421928449",
  "text" : "Will say \"hi\" when I see you at the Bray Air Show Display this afternoon.",
  "id" : 226956757421928449,
  "created_at" : "2012-07-22 08:28:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Beatrice Whelan",
      "screen_name" : "beatricewhelan",
      "indices" : [ 0, 15 ],
      "id_str" : "201285346",
      "id" : 201285346
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "226955997887987712",
  "geo" : { },
  "id_str" : "226956266046627840",
  "in_reply_to_user_id" : 201285346,
  "text" : "@beatricewhelan Something to celebrate about. But why it end in tears?",
  "id" : 226956266046627840,
  "in_reply_to_status_id" : 226955997887987712,
  "created_at" : "2012-07-22 08:26:05 +0000",
  "in_reply_to_screen_name" : "beatricewhelan",
  "in_reply_to_user_id_str" : "201285346",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 95, 108 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 109, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226956170169024512",
  "text" : "Want More Sales, Leads &amp; Conversions for your site? No Results, No fees. DM me your email. #SMEcommunity #irebiz",
  "id" : 226956170169024512,
  "created_at" : "2012-07-22 08:25:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Beatrice Whelan",
      "screen_name" : "beatricewhelan",
      "indices" : [ 13, 28 ],
      "id_str" : "201285346",
      "id" : 201285346
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "226954309068279808",
  "geo" : { },
  "id_str" : "226954543181729793",
  "in_reply_to_user_id" : 201285346,
  "text" : "Good morning @beatricewhelan :) trying to be cheeky here",
  "id" : 226954543181729793,
  "in_reply_to_status_id" : 226954309068279808,
  "created_at" : "2012-07-22 08:19:14 +0000",
  "in_reply_to_screen_name" : "beatricewhelan",
  "in_reply_to_user_id_str" : "201285346",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226953709232472064",
  "text" : "Looking for a few good man to organise National Men Enterprise Week. C'mon brother let storm the bastille",
  "id" : 226953709232472064,
  "created_at" : "2012-07-22 08:15:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IsACultureThing",
      "indices" : [ 61, 77 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226953121811804160",
  "text" : "Received a chq from client and dutifully handover to Missus. #IsACultureThing",
  "id" : 226953121811804160,
  "created_at" : "2012-07-22 08:13:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Jenkins",
      "screen_name" : "L8ESJ",
      "indices" : [ 3, 9 ],
      "id_str" : "133758702",
      "id" : 133758702
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226706938119979009",
  "text" : "RT @L8ESJ: To keep the whole G4S Fiasco rolling, Soldier returns to his Land Rover to find this. At least G4S are doing their bit. http: ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/blackberry.com\/twitter\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u00AE\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/L8ESJ\/status\/226591621385424896\/photo\/1",
        "indices" : [ 120, 140 ],
        "url" : "http:\/\/t.co\/4LWzCN96",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/AyUD4Q4CEAEBOjd.jpg",
        "id_str" : "226591621389619201",
        "id" : 226591621389619201,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AyUD4Q4CEAEBOjd.jpg",
        "sizes" : [ {
          "h" : 640,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/4LWzCN96"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "226591621385424896",
    "text" : "To keep the whole G4S Fiasco rolling, Soldier returns to his Land Rover to find this. At least G4S are doing their bit. http:\/\/t.co\/4LWzCN96",
    "id" : 226591621385424896,
    "created_at" : "2012-07-21 08:17:07 +0000",
    "user" : {
      "name" : "Lee Jenkins",
      "screen_name" : "L8ESJ",
      "protected" : false,
      "id_str" : "133758702",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2515377244\/Lee_20Jenkins_normal.jpg",
      "id" : 133758702,
      "verified" : false
    }
  },
  "id" : 226706938119979009,
  "created_at" : "2012-07-21 15:55:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "maryanne hobbs",
      "screen_name" : "maryannehobbs",
      "indices" : [ 3, 17 ],
      "id_str" : "20612033",
      "id" : 20612033
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226706090614738944",
  "text" : "RT @maryannehobbs: a Somalian taxi driver said to me.. \"don't curse the rain.. in my country every drop is like a diamond falling from t ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "perspective",
        "indices" : [ 125, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "226214855017897984",
    "text" : "a Somalian taxi driver said to me.. \"don't curse the rain.. in my country every drop is like a diamond falling from the sky\" #perspective",
    "id" : 226214855017897984,
    "created_at" : "2012-07-20 07:19:58 +0000",
    "user" : {
      "name" : "maryanne hobbs",
      "screen_name" : "maryannehobbs",
      "protected" : false,
      "id_str" : "20612033",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040898835982282752\/Ipl_uA5r_normal.jpg",
      "id" : 20612033,
      "verified" : true
    }
  },
  "id" : 226706090614738944,
  "created_at" : "2012-07-21 15:51:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Grafton Media",
      "screen_name" : "GraftonMedia",
      "indices" : [ 0, 13 ],
      "id_str" : "374067886",
      "id" : 374067886
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "226659269146189824",
  "geo" : { },
  "id_str" : "226688583715667968",
  "in_reply_to_user_id" : 374067886,
  "text" : "@GraftonMedia Thks. Just follow you.",
  "id" : 226688583715667968,
  "in_reply_to_status_id" : 226659269146189824,
  "created_at" : "2012-07-21 14:42:24 +0000",
  "in_reply_to_screen_name" : "GraftonMedia",
  "in_reply_to_user_id_str" : "374067886",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ahmed Zahid",
      "screen_name" : "ahmedzahid",
      "indices" : [ 74, 85 ],
      "id_str" : "15330518",
      "id" : 15330518
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http:\/\/t.co\/9P1zAq9z",
      "expanded_url" : "http:\/\/www.photoplusmag.com\/2012\/07\/21\/how-to-shoot-photos-from-airplane-windows\/",
      "display_url" : "photoplusmag.com\/2012\/07\/21\/how\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "226589509410758656",
  "text" : "How to shoot photos from airplane windows\nhttp:\/\/t.co\/9P1zAq9z  Link from @ahmedzahid",
  "id" : 226589509410758656,
  "created_at" : "2012-07-21 08:08:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 96, 109 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 110, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226589006870216704",
  "text" : "Want More Sales, Leads &amp; Conversions for your site? No Results, No fees. DM me your email.  #SMEcommunity #irebiz",
  "id" : 226589006870216704,
  "created_at" : "2012-07-21 08:06:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "indices" : [ 3, 19 ],
      "id_str" : "50343375",
      "id" : 50343375
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226562032521064448",
  "text" : "RT @superfinefeline: Lenovo CEO distributes $3 million personal bonus to production-line workers, other junior-level employees http:\/\/t. ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Verge",
        "screen_name" : "verge",
        "indices" : [ 131, 137 ],
        "id_str" : "275686563",
        "id" : 275686563
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 126 ],
        "url" : "http:\/\/t.co\/F00LG6LF",
        "expanded_url" : "http:\/\/www.theverge.com\/2012\/7\/20\/3171590\/lenovo-ceo-worker-3-million-bonus",
        "display_url" : "theverge.com\/2012\/7\/20\/3171\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "226501893470437376",
    "text" : "Lenovo CEO distributes $3 million personal bonus to production-line workers, other junior-level employees http:\/\/t.co\/F00LG6LF via @verge",
    "id" : 226501893470437376,
    "created_at" : "2012-07-21 02:20:34 +0000",
    "user" : {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "protected" : false,
      "id_str" : "50343375",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978593788175306752\/Z467RtZM_normal.jpg",
      "id" : 50343375,
      "verified" : false
    }
  },
  "id" : 226562032521064448,
  "created_at" : "2012-07-21 06:19:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226427989523582976",
  "text" : "\"The cause is good. The method is wrong.\" A quote in movie.",
  "id" : 226427989523582976,
  "created_at" : "2012-07-20 21:26:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Womersley",
      "screen_name" : "LisaWomersley",
      "indices" : [ 3, 17 ],
      "id_str" : "789148770869080064",
      "id" : 789148770869080064
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "measure",
      "indices" : [ 122, 130 ]
    } ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http:\/\/t.co\/hB1zzoKg",
      "expanded_url" : "http:\/\/fuckyeahanalytics.tumblr.com\/",
      "display_url" : "fuckyeahanalytics.tumblr.com"
    } ]
  },
  "geo" : { },
  "id_str" : "226425651840831488",
  "text" : "RT @lisawomersley: Just discovered http:\/\/t.co\/hB1zzoKg I think I might start using this pic in all future presentations. #measure http: ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lisawomersley\/status\/224051610509250560\/photo\/1",
        "indices" : [ 112, 132 ],
        "url" : "http:\/\/t.co\/zTRt90HQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Axv9wLACEAAezVu.jpg",
        "id_str" : "224051610513444864",
        "id" : 224051610513444864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Axv9wLACEAAezVu.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 351,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zTRt90HQ"
      } ],
      "hashtags" : [ {
        "text" : "measure",
        "indices" : [ 103, 111 ]
      } ],
      "urls" : [ {
        "indices" : [ 16, 36 ],
        "url" : "http:\/\/t.co\/hB1zzoKg",
        "expanded_url" : "http:\/\/fuckyeahanalytics.tumblr.com\/",
        "display_url" : "fuckyeahanalytics.tumblr.com"
      } ]
    },
    "geo" : { },
    "id_str" : "224051610509250560",
    "text" : "Just discovered http:\/\/t.co\/hB1zzoKg I think I might start using this pic in all future presentations. #measure http:\/\/t.co\/zTRt90HQ",
    "id" : 224051610509250560,
    "created_at" : "2012-07-14 08:04:01 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 226425651840831488,
  "created_at" : "2012-07-20 21:17:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 41 ],
      "url" : "http:\/\/t.co\/CDOWCVxN",
      "expanded_url" : "http:\/\/pinterest.com\/pin\/133911788890826717\/",
      "display_url" : "pinterest.com\/pin\/1339117888\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "226423814756646913",
  "text" : "Illusion and Reality http:\/\/t.co\/CDOWCVxN",
  "id" : 226423814756646913,
  "created_at" : "2012-07-20 21:10:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 103 ],
      "url" : "http:\/\/t.co\/P1GyeYh5",
      "expanded_url" : "http:\/\/www.nusparkmarketing.com\/2012\/07\/implementing-a-content-experiment-from-google-analytics-with-a-wordpress-website-conversion-testing\/",
      "display_url" : "nusparkmarketing.com\/2012\/07\/implem\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "226324248950616064",
  "text" : "Using Google Analytics Content Experiment to A\/B test Wordpress page (not WP post) http:\/\/t.co\/P1GyeYh5",
  "id" : 226324248950616064,
  "created_at" : "2012-07-20 14:34:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getmeasure",
      "indices" : [ 113, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226211227951591424",
  "text" : "A visitor who pays with credit card vs one who pays with PayPal? Does one spend more, on average, than the other?#getmeasure",
  "id" : 226211227951591424,
  "created_at" : "2012-07-20 07:05:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "226199290228404224",
  "text" : "Look forward to the airdisplay in Bray this Sunday.",
  "id" : 226199290228404224,
  "created_at" : "2012-07-20 06:18:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reg Saddler",
      "screen_name" : "zaibatsu",
      "indices" : [ 3, 12 ],
      "id_str" : "8071902",
      "id" : 8071902
    }, {
      "name" : "TweetSmarter",
      "screen_name" : "TweetSmarter",
      "indices" : [ 109, 122 ],
      "id_str" : "15947185",
      "id" : 15947185
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tips",
      "indices" : [ 123, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 105 ],
      "url" : "http:\/\/t.co\/1Jtms03j",
      "expanded_url" : "http:\/\/bit.ly\/NHnt4S",
      "display_url" : "bit.ly\/NHnt4S"
    } ]
  },
  "geo" : { },
  "id_str" : "226188230138146816",
  "text" : "RT @zaibatsu: A Complete Guide To Avoiding Batman\u2014or any kind of\u2014Spoilers On Twitter http:\/\/t.co\/1Jtms03j RT @tweetsmarter #tips",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TweetSmarter",
        "screen_name" : "TweetSmarter",
        "indices" : [ 95, 108 ],
        "id_str" : "15947185",
        "id" : 15947185
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tips",
        "indices" : [ 109, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 71, 91 ],
        "url" : "http:\/\/t.co\/1Jtms03j",
        "expanded_url" : "http:\/\/bit.ly\/NHnt4S",
        "display_url" : "bit.ly\/NHnt4S"
      } ]
    },
    "geo" : { },
    "id_str" : "226164880493924352",
    "text" : "A Complete Guide To Avoiding Batman\u2014or any kind of\u2014Spoilers On Twitter http:\/\/t.co\/1Jtms03j RT @tweetsmarter #tips",
    "id" : 226164880493924352,
    "created_at" : "2012-07-20 04:01:24 +0000",
    "user" : {
      "name" : "Reg Saddler",
      "screen_name" : "zaibatsu",
      "protected" : false,
      "id_str" : "8071902",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1765976458\/1111_normal.jpg",
      "id" : 8071902,
      "verified" : false
    }
  },
  "id" : 226188230138146816,
  "created_at" : "2012-07-20 05:34:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore",
      "screen_name" : "mySingapore",
      "indices" : [ 3, 15 ],
      "id_str" : "18099511",
      "id" : 18099511
    }, {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "indices" : [ 97, 105 ],
      "id_str" : "115624161",
      "id" : 115624161
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 106, 116 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 92 ],
      "url" : "http:\/\/t.co\/rJED2CqJ",
      "expanded_url" : "http:\/\/sg.news.yahoo.com\/sdp-criticises-yale-nus-college-ban-on-partisan-politics.html",
      "display_url" : "sg.news.yahoo.com\/sdp-criticises\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "226054729124433920",
  "text" : "RT @mySingapore: Yale-NUS slammed for ban on political protests, groups http:\/\/t.co\/rJED2CqJ via @yahoosg #Singapore",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yahoo Singapore",
        "screen_name" : "YahooSG",
        "indices" : [ 80, 88 ],
        "id_str" : "115624161",
        "id" : 115624161
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 89, 99 ]
      } ],
      "urls" : [ {
        "indices" : [ 55, 75 ],
        "url" : "http:\/\/t.co\/rJED2CqJ",
        "expanded_url" : "http:\/\/sg.news.yahoo.com\/sdp-criticises-yale-nus-college-ban-on-partisan-politics.html",
        "display_url" : "sg.news.yahoo.com\/sdp-criticises\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "226021231340703744",
    "text" : "Yale-NUS slammed for ban on political protests, groups http:\/\/t.co\/rJED2CqJ via @yahoosg #Singapore",
    "id" : 226021231340703744,
    "created_at" : "2012-07-19 18:30:35 +0000",
    "user" : {
      "name" : "Singapore",
      "screen_name" : "mySingapore",
      "protected" : false,
      "id_str" : "18099511",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/132370352\/396x297_normal.jpg",
      "id" : 18099511,
      "verified" : false
    }
  },
  "id" : 226054729124433920,
  "created_at" : "2012-07-19 20:43:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225837419981844480",
  "text" : "Just starting out on this. No prior record to show and that why I am offering no results no fees. You can't have your cake and eat it.",
  "id" : 225837419981844480,
  "created_at" : "2012-07-19 06:20:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225832578328977408",
  "text" : "\u4F60\u6253\u7684\u5982\u610F\u7B97\u76D8\u771F\u54CD\uFF01\u4FBF\u5B9C\u79DF\u4F60\u5C4B\u5B50\uFF0C\u4F60\u60F3\u5206\u79DF\u51FA\u53BB\uFF1F\uFF01",
  "id" : 225832578328977408,
  "created_at" : "2012-07-19 06:00:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u6D88\u6781",
      "screen_name" : "cxiaoji",
      "indices" : [ 3, 11 ],
      "id_str" : "45635727",
      "id" : 45635727
    }, {
      "name" : "Melody",
      "screen_name" : "pulse_me",
      "indices" : [ 47, 56 ],
      "id_str" : "852708830450528256",
      "id" : 852708830450528256
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225676385442725888",
  "text" : "RT @cxiaoji: \u8FD9\u4EF6\u4E8B\u6211\u65E9\u5C31\u505A\u5230\u4E86\uFF0C\u628AChrome\u7684\u5FEB\u6377\u65B9\u5F0F\u56FE\u6807\u6539\u6210IE\u5373\u53EF\u3002RT @Pulse_me: \u5FC5\u987B\u5F3A\u5236\u8BA9\u6211\u7238\u7528chrome",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/hotot.org\" rel=\"nofollow\"\u003EHotot for Chrome\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Melody",
        "screen_name" : "pulse_me",
        "indices" : [ 34, 43 ],
        "id_str" : "852708830450528256",
        "id" : 852708830450528256
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225648567405121536",
    "text" : "\u8FD9\u4EF6\u4E8B\u6211\u65E9\u5C31\u505A\u5230\u4E86\uFF0C\u628AChrome\u7684\u5FEB\u6377\u65B9\u5F0F\u56FE\u6807\u6539\u6210IE\u5373\u53EF\u3002RT @Pulse_me: \u5FC5\u987B\u5F3A\u5236\u8BA9\u6211\u7238\u7528chrome",
    "id" : 225648567405121536,
    "created_at" : "2012-07-18 17:49:45 +0000",
    "user" : {
      "name" : "\u6D88\u6781",
      "screen_name" : "cxiaoji",
      "protected" : false,
      "id_str" : "45635727",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2663756711\/7060581286b8b5c1beb81c6cce5b03e4_normal.png",
      "id" : 45635727,
      "verified" : false
    }
  },
  "id" : 225676385442725888,
  "created_at" : "2012-07-18 19:40:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joe Drumgoole",
      "screen_name" : "jdrumgoole",
      "indices" : [ 3, 14 ],
      "id_str" : "66613",
      "id" : 66613
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225675514814291969",
  "text" : "RT @jdrumgoole: Not sure I I'd want the resuscitation of Yahoo as my first CEO job",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225656217526231040",
    "text" : "Not sure I I'd want the resuscitation of Yahoo as my first CEO job",
    "id" : 225656217526231040,
    "created_at" : "2012-07-18 18:20:09 +0000",
    "user" : {
      "name" : "Joe Drumgoole",
      "screen_name" : "jdrumgoole",
      "protected" : false,
      "id_str" : "66613",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/93409180\/joe1_normal.jpg",
      "id" : 66613,
      "verified" : false
    }
  },
  "id" : 225675514814291969,
  "created_at" : "2012-07-18 19:36:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tus Nua Designs",
      "screen_name" : "TusNuaDesigns",
      "indices" : [ 3, 17 ],
      "id_str" : "1372338342",
      "id" : 1372338342
    }, {
      "name" : "Kerry Biofuels",
      "screen_name" : "KerryBiofuels",
      "indices" : [ 22, 36 ],
      "id_str" : "243773785",
      "id" : 243773785
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 122, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225631571380674562",
  "text" : "RT @TusNuaDesigns: RT @KerryBiofuels: Our VAN HAS BEEN STOLEN this afternoon. White Ford Transit refrigerated 07-LH-5820. #SMEcommunity  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Kerry Biofuels",
        "screen_name" : "KerryBiofuels",
        "indices" : [ 3, 17 ],
        "id_str" : "243773785",
        "id" : 243773785
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SMEcommunity",
        "indices" : [ 103, 116 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225234781502324740",
    "text" : "RT @KerryBiofuels: Our VAN HAS BEEN STOLEN this afternoon. White Ford Transit refrigerated 07-LH-5820. #SMEcommunity spread the word",
    "id" : 225234781502324740,
    "created_at" : "2012-07-17 14:25:31 +0000",
    "user" : {
      "name" : "Debi  Harper",
      "screen_name" : "DebiHarper1",
      "protected" : false,
      "id_str" : "132181648",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/994588866114998272\/8xd8XNlU_normal.jpg",
      "id" : 132181648,
      "verified" : false
    }
  },
  "id" : 225631571380674562,
  "created_at" : "2012-07-18 16:42:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andy Kirk",
      "screen_name" : "visualisingdata",
      "indices" : [ 3, 19 ],
      "id_str" : "137712607",
      "id" : 137712607
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225612320192217088",
  "text" : "RT @visualisingdata: People of Manchester, Dublin, London, Berlin and Paris I'm coming your way with my data viz training in next few mo ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 120, 140 ],
        "url" : "http:\/\/t.co\/LzkM4HhQ",
        "expanded_url" : "http:\/\/bit.ly\/O8YANM",
        "display_url" : "bit.ly\/O8YANM"
      } ]
    },
    "geo" : { },
    "id_str" : "225479733129908224",
    "text" : "People of Manchester, Dublin, London, Berlin and Paris I'm coming your way with my data viz training in next few months http:\/\/t.co\/LzkM4HhQ",
    "id" : 225479733129908224,
    "created_at" : "2012-07-18 06:38:52 +0000",
    "user" : {
      "name" : "Andy Kirk",
      "screen_name" : "visualisingdata",
      "protected" : false,
      "id_str" : "137712607",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1000851383363493893\/fHt8A__Y_normal.jpg",
      "id" : 137712607,
      "verified" : false
    }
  },
  "id" : 225612320192217088,
  "created_at" : "2012-07-18 15:25:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ron Shevlin",
      "screen_name" : "rshevlin",
      "indices" : [ 3, 12 ],
      "id_str" : "9385512",
      "id" : 9385512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225611187885322241",
  "text" : "RT @rshevlin: If elected president in November, I will require ppl to get a license before they can publish an infographic",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225569834757406722",
    "text" : "If elected president in November, I will require ppl to get a license before they can publish an infographic",
    "id" : 225569834757406722,
    "created_at" : "2012-07-18 12:36:54 +0000",
    "user" : {
      "name" : "Ron Shevlin",
      "screen_name" : "rshevlin",
      "protected" : false,
      "id_str" : "9385512",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966844480492994560\/a6-R47ZS_normal.jpg",
      "id" : 9385512,
      "verified" : false
    }
  },
  "id" : 225611187885322241,
  "created_at" : "2012-07-18 15:21:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225607518573957121",
  "text" : "\"Deploy our app and get your free shirt to join the movement.\" &lt;&lt; A free shirt? You got to do better than that...",
  "id" : 225607518573957121,
  "created_at" : "2012-07-18 15:06:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "225580860848619520",
  "geo" : { },
  "id_str" : "225606092590284802",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney most probably on union mandated tea break.",
  "id" : 225606092590284802,
  "in_reply_to_status_id" : 225580860848619520,
  "created_at" : "2012-07-18 15:00:58 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dara \u00D3 Briain",
      "screen_name" : "daraobriain",
      "indices" : [ 3, 15 ],
      "id_str" : "44874400",
      "id" : 44874400
    }, {
      "name" : "Rack and Rule",
      "screen_name" : "BakerMicky",
      "indices" : [ 78, 89 ],
      "id_str" : "492044414",
      "id" : 492044414
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225605633456611329",
  "text" : "RT @daraobriain: People will be needed to man the doors at the Frisbee Arena. @BakerMicky: \nYeah, a G4S 6 week contract!!!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rack and Rule",
        "screen_name" : "BakerMicky",
        "indices" : [ 61, 72 ],
        "id_str" : "492044414",
        "id" : 492044414
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225593240789852161",
    "text" : "People will be needed to man the doors at the Frisbee Arena. @BakerMicky: \nYeah, a G4S 6 week contract!!!",
    "id" : 225593240789852161,
    "created_at" : "2012-07-18 14:09:54 +0000",
    "user" : {
      "name" : "Dara \u00D3 Briain",
      "screen_name" : "daraobriain",
      "protected" : false,
      "id_str" : "44874400",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/251011476\/dobrian08_normal.jpg",
      "id" : 44874400,
      "verified" : false
    }
  },
  "id" : 225605633456611329,
  "created_at" : "2012-07-18 14:59:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "net magazine",
      "screen_name" : "netmag",
      "indices" : [ 3, 10 ],
      "id_str" : "17648193",
      "id" : 17648193
    }, {
      "name" : "Trent Walton",
      "screen_name" : "TrentWalton",
      "indices" : [ 80, 92 ],
      "id_str" : "14477161",
      "id" : 14477161
    }, {
      "name" : "Rachel \uD83D\uDC1D",
      "screen_name" : "MissRachilli",
      "indices" : [ 94, 107 ],
      "id_str" : "20010180",
      "id" : 20010180
    }, {
      "name" : "Chris Coyier",
      "screen_name" : "chriscoyier",
      "indices" : [ 109, 121 ],
      "id_str" : "793830",
      "id" : 793830
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225604837973295105",
  "text" : "RT @netmag: Big question: should we drop social media buttons? With comments by @trentwalton, @missrachilli, @chriscoyier etc: http:\/\/t. ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Trent Walton",
        "screen_name" : "TrentWalton",
        "indices" : [ 68, 80 ],
        "id_str" : "14477161",
        "id" : 14477161
      }, {
        "name" : "Rachel \uD83D\uDC1D",
        "screen_name" : "MissRachilli",
        "indices" : [ 82, 95 ],
        "id_str" : "20010180",
        "id" : 20010180
      }, {
        "name" : "Chris Coyier",
        "screen_name" : "chriscoyier",
        "indices" : [ 97, 109 ],
        "id_str" : "793830",
        "id" : 793830
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 135 ],
        "url" : "http:\/\/t.co\/x2iuRDDZ",
        "expanded_url" : "http:\/\/netm.ag\/OFK2Si",
        "display_url" : "netm.ag\/OFK2Si"
      } ]
    },
    "geo" : { },
    "id_str" : "225596812143243264",
    "text" : "Big question: should we drop social media buttons? With comments by @trentwalton, @missrachilli, @chriscoyier etc: http:\/\/t.co\/x2iuRDDZ",
    "id" : 225596812143243264,
    "created_at" : "2012-07-18 14:24:06 +0000",
    "user" : {
      "name" : "net magazine",
      "screen_name" : "netmag",
      "protected" : false,
      "id_str" : "17648193",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000697523305\/d556bef753c6ac3933e9d5bf9f147418_normal.jpeg",
      "id" : 17648193,
      "verified" : false
    }
  },
  "id" : 225604837973295105,
  "created_at" : "2012-07-18 14:55:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 60, 73 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 74, 81 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225573704967524352",
  "text" : "Do you have a way of measuring the success of your website? #SMEcommunity #irebiz",
  "id" : 225573704967524352,
  "created_at" : "2012-07-18 12:52:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 67, 80 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 81, 88 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225572531401273344",
  "text" : "When was the last time your website had a professional evaluation? #SMEcommunity #irebiz",
  "id" : 225572531401273344,
  "created_at" : "2012-07-18 12:47:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http:\/\/t.co\/IlwV2cAn",
      "expanded_url" : "http:\/\/www.imagineallthewater.eu\/EN",
      "display_url" : "imagineallthewater.eu\/EN"
    } ]
  },
  "geo" : { },
  "id_str" : "225566018578288640",
  "text" : "Dive in and discover the hidden side of your water consumption. http:\/\/t.co\/IlwV2cAn",
  "id" : 225566018578288640,
  "created_at" : "2012-07-18 12:21:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http:\/\/t.co\/05tlSsdw",
      "expanded_url" : "http:\/\/econsultancy.com\/us\/blog\/8186-people-and-processes-hold-key-to-conversion-rate-optimization-study",
      "display_url" : "econsultancy.com\/us\/blog\/8186-p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "225565372521259008",
  "text" : "Companies who were happy with their conversion rates did on average 40% more tests. http:\/\/t.co\/05tlSsdw",
  "id" : 225565372521259008,
  "created_at" : "2012-07-18 12:19:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 53 ],
      "url" : "http:\/\/t.co\/j75CbgMi",
      "expanded_url" : "http:\/\/www.facebookmonthlydownload.com\/july\/uk\/d\/newsletter\/index.html",
      "display_url" : "facebookmonthlydownload.com\/july\/uk\/d\/news\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "225525960517226496",
  "text" : "Facebook Download for Marketers. http:\/\/t.co\/j75CbgMi",
  "id" : 225525960517226496,
  "created_at" : "2012-07-18 09:42:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 132 ],
      "url" : "http:\/\/t.co\/4mIcoCfX",
      "expanded_url" : "http:\/\/econsultancy.com\/uk\/blog\/8196-how-to-optimise-headlines-using-the-65-character-rule",
      "display_url" : "econsultancy.com\/uk\/blog\/8196-h\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "225314498662248449",
  "text" : "Max. 65 characters for headlines is a good idea for search,news aggregators, social media channels &amp; email. http:\/\/t.co\/4mIcoCfX",
  "id" : 225314498662248449,
  "created_at" : "2012-07-17 19:42:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 0, 13 ]
    }, {
      "text" : "Irebiz",
      "indices" : [ 14, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225282174994219008",
  "text" : "#SMEcommunity #Irebiz. Hire me to fine tune your site so you can sell more online. No results, no fee.",
  "id" : 225282174994219008,
  "created_at" : "2012-07-17 17:33:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Cousins",
      "screen_name" : "xcelbusiness",
      "indices" : [ 0, 13 ],
      "id_str" : "2410788877",
      "id" : 2410788877
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "225273726235721729",
  "geo" : { },
  "id_str" : "225281533198610432",
  "in_reply_to_user_id" : 119546373,
  "text" : "@xcelbusiness I should thank you for alerting us this on this bug.",
  "id" : 225281533198610432,
  "in_reply_to_status_id" : 225273726235721729,
  "created_at" : "2012-07-17 17:31:17 +0000",
  "in_reply_to_screen_name" : "_helencousins",
  "in_reply_to_user_id_str" : "119546373",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 100, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225276420316860417",
  "text" : "It is possible to cycle from CCK to NTU via Park Connector? IS there a specified pathway to follow? #Singapore Thks",
  "id" : 225276420316860417,
  "created_at" : "2012-07-17 17:10:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Turner",
      "screen_name" : "jamesasterisk",
      "indices" : [ 3, 17 ],
      "id_str" : "2294098392",
      "id" : 2294098392
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UX",
      "indices" : [ 137, 140 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225203813102002177",
  "text" : "RT @JamesAsterisk: Looking for a UX'er\/Agency to partner with in France to so some user interviews in September. Recommendations anyone? #UX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "UX",
        "indices" : [ 118, 121 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "225188257938415617",
    "text" : "Looking for a UX'er\/Agency to partner with in France to so some user interviews in September. Recommendations anyone? #UX",
    "id" : 225188257938415617,
    "created_at" : "2012-07-17 11:20:39 +0000",
    "user" : {
      "name" : "James Turner",
      "screen_name" : "jamesturnerux",
      "protected" : false,
      "id_str" : "19864220",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/627912796408315904\/kmAyljZS_normal.jpg",
      "id" : 19864220,
      "verified" : false
    }
  },
  "id" : 225203813102002177,
  "created_at" : "2012-07-17 12:22:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "indices" : [ 0, 9 ],
      "id_str" : "875971",
      "id" : 875971
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "225114388753956864",
  "geo" : { },
  "id_str" : "225134310053773312",
  "in_reply_to_user_id" : 875971,
  "text" : "@Rubenerd Which teleco network?",
  "id" : 225134310053773312,
  "in_reply_to_status_id" : 225114388753956864,
  "created_at" : "2012-07-17 07:46:16 +0000",
  "in_reply_to_screen_name" : "Rubenerd",
  "in_reply_to_user_id_str" : "875971",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Cousins",
      "screen_name" : "xcelbusiness",
      "indices" : [ 3, 16 ],
      "id_str" : "2410788877",
      "id" : 2410788877
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225133431493885953",
  "text" : "RT @xcelbusiness: Skype bug hits private messages &lt; Er, copies IM to other contact. Be careful  with IM until bug fixed :) http:\/\/t.c ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/bbc-news\/id364147881?mt=8&uo=4\" rel=\"nofollow\"\u003EBBC News on iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 128 ],
        "url" : "http:\/\/t.co\/b0ZTZj9e",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/technology-18863423",
        "display_url" : "bbc.co.uk\/news\/technolog\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "225128850080796672",
    "text" : "Skype bug hits private messages &lt; Er, copies IM to other contact. Be careful  with IM until bug fixed :) http:\/\/t.co\/b0ZTZj9e",
    "id" : 225128850080796672,
    "created_at" : "2012-07-17 07:24:35 +0000",
    "user" : {
      "name" : "Helen Cousins",
      "screen_name" : "_helencousins",
      "protected" : false,
      "id_str" : "119546373",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/602560856488955905\/DQt4Hz6D_normal.jpg",
      "id" : 119546373,
      "verified" : false
    }
  },
  "id" : 225133431493885953,
  "created_at" : "2012-07-17 07:42:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fifty Sheds of Grey",
      "screen_name" : "50ShedsofGrey",
      "indices" : [ 3, 17 ],
      "id_str" : "797009297876123652",
      "id" : 797009297876123652
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225133296202424321",
  "text" : "RT @50ShedsofGrey: 'Punish me,' she cried desperately, 'Make me suffer like only a real man can!' 'Very well,' I replied, leaving the to ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224461696926879746",
    "text" : "'Punish me,' she cried desperately, 'Make me suffer like only a real man can!' 'Very well,' I replied, leaving the toilet seat up.",
    "id" : 224461696926879746,
    "created_at" : "2012-07-15 11:13:33 +0000",
    "user" : {
      "name" : "Paul Rigby",
      "screen_name" : "PaulRigbywrites",
      "protected" : false,
      "id_str" : "614449931",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1019199975274811392\/cZbXKjkx_normal.jpg",
      "id" : 614449931,
      "verified" : false
    }
  },
  "id" : 225133296202424321,
  "created_at" : "2012-07-17 07:42:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fifty Sheds of Grey",
      "screen_name" : "50ShedsofGrey",
      "indices" : [ 3, 17 ],
      "id_str" : "797009297876123652",
      "id" : 797009297876123652
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "225129518753529856",
  "text" : "RT @50ShedsofGrey: She gazed up at me wide-eyed from the shed floor and bit her lip seductively. Unfortunately it was her top lip so she ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224580686818975744",
    "text" : "She gazed up at me wide-eyed from the shed floor and bit her lip seductively. Unfortunately it was her top lip so she looked like a piranha.",
    "id" : 224580686818975744,
    "created_at" : "2012-07-15 19:06:22 +0000",
    "user" : {
      "name" : "Paul Rigby",
      "screen_name" : "PaulRigbywrites",
      "protected" : false,
      "id_str" : "614449931",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1019199975274811392\/cZbXKjkx_normal.jpg",
      "id" : 614449931,
      "verified" : false
    }
  },
  "id" : 225129518753529856,
  "created_at" : "2012-07-17 07:27:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nansen Malin",
      "screen_name" : "nansen",
      "indices" : [ 3, 10 ],
      "id_str" : "14389132",
      "id" : 14389132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224928590565883904",
  "text" : "RT @nansen: RIP Steven Covey.  7 Habits author.  He gave us so much!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224928011890331650",
    "text" : "RIP Steven Covey.  7 Habits author.  He gave us so much!",
    "id" : 224928011890331650,
    "created_at" : "2012-07-16 18:06:31 +0000",
    "user" : {
      "name" : "Nansen Malin",
      "screen_name" : "nansen",
      "protected" : false,
      "id_str" : "14389132",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797651345524199424\/hhYFpIXH_normal.jpg",
      "id" : 14389132,
      "verified" : false
    }
  },
  "id" : 224928590565883904,
  "created_at" : "2012-07-16 18:08:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TV",
      "indices" : [ 82, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224927953228791808",
  "text" : "Babies at the office? Don't think I can give 100% focus to my work in the office. #TV",
  "id" : 224927953228791808,
  "created_at" : "2012-07-16 18:06:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224877438474260481",
  "text" : "Hi Mother nature, Can I have the sunshine in the morning? Late afternoon (3pm) is all yours. Your can rain and pour to your heart content.",
  "id" : 224877438474260481,
  "created_at" : "2012-07-16 14:45:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sean Nicholls",
      "screen_name" : "sean_nicholls",
      "indices" : [ 0, 14 ],
      "id_str" : "955116647488028672",
      "id" : 955116647488028672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224876698573869057",
  "text" : "@sean_nicholls I recommend Hacking Work: Breaking Stupid Rules for Smart Results",
  "id" : 224876698573869057,
  "created_at" : "2012-07-16 14:42:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SyncWebApp",
      "screen_name" : "syncwebapp",
      "indices" : [ 3, 14 ],
      "id_str" : "631013614",
      "id" : 631013614
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224868801773178880",
  "text" : "RT @syncwebapp: Need to create PDF documents from your form submissions and email them to yourself or your customers? I can help",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224811495534505984",
    "text" : "Need to create PDF documents from your form submissions and email them to yourself or your customers? I can help",
    "id" : 224811495534505984,
    "created_at" : "2012-07-16 10:23:31 +0000",
    "user" : {
      "name" : "SyncWebApp",
      "screen_name" : "syncwebapp",
      "protected" : false,
      "id_str" : "631013614",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2389024672\/33gg0xyp2lvfazp3bve8_normal.jpeg",
      "id" : 631013614,
      "verified" : false
    }
  },
  "id" : 224868801773178880,
  "created_at" : "2012-07-16 14:11:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "berniceklaassen",
      "screen_name" : "berniceklaassen",
      "indices" : [ 3, 19 ],
      "id_str" : "17632827",
      "id" : 17632827
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http:\/\/t.co\/uytcjwFp",
      "expanded_url" : "http:\/\/www.independent.co.uk\/news\/uk\/home-news\/britain-flooded-with-brand-police-to-protect-sponsors-7945436.html",
      "display_url" : "independent.co.uk\/news\/uk\/home-n\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "224829261196955648",
  "text" : "RT @berniceklaassen: Britain flooded with 'brand police' to protect sponsors -  http:\/\/t.co\/uytcjwFp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 79 ],
        "url" : "http:\/\/t.co\/uytcjwFp",
        "expanded_url" : "http:\/\/www.independent.co.uk\/news\/uk\/home-news\/britain-flooded-with-brand-police-to-protect-sponsors-7945436.html",
        "display_url" : "independent.co.uk\/news\/uk\/home-n\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "224803574499119104",
    "text" : "Britain flooded with 'brand police' to protect sponsors -  http:\/\/t.co\/uytcjwFp",
    "id" : 224803574499119104,
    "created_at" : "2012-07-16 09:52:03 +0000",
    "user" : {
      "name" : "berniceklaassen",
      "screen_name" : "berniceklaassen",
      "protected" : false,
      "id_str" : "17632827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/590952560\/Bernicek_normal.jpg",
      "id" : 17632827,
      "verified" : false
    }
  },
  "id" : 224829261196955648,
  "created_at" : "2012-07-16 11:34:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224827120826859523",
  "text" : "When I say \"so many proposal\" , it just means a handful of proposal ;)",
  "id" : 224827120826859523,
  "created_at" : "2012-07-16 11:25:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224826873010593794",
  "text" : "Submit so many proposal over the last week. I think I could use that for the basis of  a new blog post.",
  "id" : 224826873010593794,
  "created_at" : "2012-07-16 11:24:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Butlers Chocolates",
      "screen_name" : "ButlersChocs",
      "indices" : [ 3, 16 ],
      "id_str" : "115040168",
      "id" : 115040168
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224826217063387137",
  "text" : "RT @ButlersChocs: \"Chocolate is nature's way of making up for Mondays\" so we have 3 boxes of Butlers Chocolates up for grabs this dreary ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224812619700256768",
    "text" : "\"Chocolate is nature's way of making up for Mondays\" so we have 3 boxes of Butlers Chocolates up for grabs this dreary Monday! RT to enter!",
    "id" : 224812619700256768,
    "created_at" : "2012-07-16 10:28:00 +0000",
    "user" : {
      "name" : "Butlers Chocolates",
      "screen_name" : "ButlersChocs",
      "protected" : false,
      "id_str" : "115040168",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039181649945272321\/DpTP-9OT_normal.jpg",
      "id" : 115040168,
      "verified" : true
    }
  },
  "id" : 224826217063387137,
  "created_at" : "2012-07-16 11:22:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Veronica Walsh",
      "screen_name" : "VCurrentAffairs",
      "indices" : [ 3, 19 ],
      "id_str" : "40254038",
      "id" : 40254038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224813519990833152",
  "text" : "RT @VCurrentAffairs: If your children are on your passport, check with the country you're going to and check their regs. Many require th ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224811127522074624",
    "text" : "If your children are on your passport, check with the country you're going to and check their regs. Many require they have their own now.",
    "id" : 224811127522074624,
    "created_at" : "2012-07-16 10:22:04 +0000",
    "user" : {
      "name" : "Veronica Walsh",
      "screen_name" : "VCurrentAffairs",
      "protected" : false,
      "id_str" : "40254038",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/707950814476816384\/5upWTLhl_normal.jpg",
      "id" : 40254038,
      "verified" : false
    }
  },
  "id" : 224813519990833152,
  "created_at" : "2012-07-16 10:31:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philippe Legrain",
      "screen_name" : "plegrain",
      "indices" : [ 3, 12 ],
      "id_str" : "118680576",
      "id" : 118680576
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http:\/\/t.co\/1YjzGEcD",
      "expanded_url" : "http:\/\/www.ft.com\/cms\/s\/0\/44b7771c-cd15-11e1-92c1-00144feabdc0.html",
      "display_url" : "ft.com\/cms\/s\/0\/44b777\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "224791175859679232",
  "text" : "RT @plegrain: UK manufacturers can't find skilled engineers 2 meet rising orders http:\/\/t.co\/1YjzGEcD training takes time, so migrants r ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 87 ],
        "url" : "http:\/\/t.co\/1YjzGEcD",
        "expanded_url" : "http:\/\/www.ft.com\/cms\/s\/0\/44b7771c-cd15-11e1-92c1-00144feabdc0.html",
        "display_url" : "ft.com\/cms\/s\/0\/44b777\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "224773146966900736",
    "text" : "UK manufacturers can't find skilled engineers 2 meet rising orders http:\/\/t.co\/1YjzGEcD training takes time, so migrants r obvious solution",
    "id" : 224773146966900736,
    "created_at" : "2012-07-16 07:51:08 +0000",
    "user" : {
      "name" : "Philippe Legrain",
      "screen_name" : "plegrain",
      "protected" : false,
      "id_str" : "118680576",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882952723901149184\/NWu-Czta_normal.jpg",
      "id" : 118680576,
      "verified" : false
    }
  },
  "id" : 224791175859679232,
  "created_at" : "2012-07-16 09:02:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http:\/\/t.co\/10DAGJLy",
      "expanded_url" : "http:\/\/news.xin.msn.com\/en\/singapore\/singapore-navy-takes-part-in-rimpac-off-coast-of-hawaii",
      "display_url" : "news.xin.msn.com\/en\/singapore\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "224538851870375936",
  "text" : "I love the Commanding Officer chair. http:\/\/t.co\/10DAGJLy",
  "id" : 224538851870375936,
  "created_at" : "2012-07-15 16:20:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul O'Mahony (Cork)",
      "screen_name" : "Omaniblog",
      "indices" : [ 0, 10 ],
      "id_str" : "17655702",
      "id" : 17655702
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "stylesofwakingIreland",
      "indices" : [ 37, 59 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "224419532494737408",
  "geo" : { },
  "id_str" : "224533912205004800",
  "in_reply_to_user_id" : 17655702,
  "text" : "@Omaniblog Thanks for the mention on #stylesofwakingIreland",
  "id" : 224533912205004800,
  "in_reply_to_status_id" : 224419532494737408,
  "created_at" : "2012-07-15 16:00:30 +0000",
  "in_reply_to_screen_name" : "Omaniblog",
  "in_reply_to_user_id_str" : "17655702",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http:\/\/t.co\/apjfKxgD",
      "expanded_url" : "http:\/\/www.teamsky.com\/article\/0,27290,17546_5886373,00.html",
      "display_url" : "teamsky.com\/article\/0,2729\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "224533654200795137",
  "text" : "You should checkout Sky Team's Volvo bus http:\/\/t.co\/apjfKxgD",
  "id" : 224533654200795137,
  "created_at" : "2012-07-15 15:59:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 91, 104 ]
    }, {
      "text" : "irebiz",
      "indices" : [ 105, 112 ]
    } ],
    "urls" : [ {
      "indices" : [ 70, 90 ],
      "url" : "http:\/\/t.co\/doFg9RhP",
      "expanded_url" : "http:\/\/www.fastcoexist.com\/node\/1680155",
      "display_url" : "fastcoexist.com\/node\/1680155"
    } ]
  },
  "geo" : { },
  "id_str" : "224437444983013376",
  "text" : "Can The Rise of Micro-Entrepreneurs Force Companies To Be More Human? http:\/\/t.co\/doFg9RhP #SMEcommunity #irebiz",
  "id" : 224437444983013376,
  "created_at" : "2012-07-15 09:37:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224432202543415296",
  "text" : "RT @patphelan: Two things I detest on twitter, \"my week on twitter\" and \"the \"username\" daily is out\" absolutely pointless",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224419017606168576",
    "text" : "Two things I detest on twitter, \"my week on twitter\" and \"the \"username\" daily is out\" absolutely pointless",
    "id" : 224419017606168576,
    "created_at" : "2012-07-15 08:23:57 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 224432202543415296,
  "created_at" : "2012-07-15 09:16:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224351644878508032",
  "text" : "Good morning Dublin! 5am local time.",
  "id" : 224351644878508032,
  "created_at" : "2012-07-15 03:56:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeffrey Paine",
      "screen_name" : "jpaine",
      "indices" : [ 3, 10 ],
      "id_str" : "9650622",
      "id" : 9650622
    }, {
      "name" : "Aaron Levie",
      "screen_name" : "levie",
      "indices" : [ 13, 19 ],
      "id_str" : "914061",
      "id" : 914061
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224350987228422144",
  "text" : "RT @jpaine: \"@levie: Competition is good. You tend to run faster when you're being chased.\" V true",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Aaron Levie",
        "screen_name" : "levie",
        "indices" : [ 1, 7 ],
        "id_str" : "914061",
        "id" : 914061
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "224306869798322176",
    "text" : "\"@levie: Competition is good. You tend to run faster when you're being chased.\" V true",
    "id" : 224306869798322176,
    "created_at" : "2012-07-15 00:58:19 +0000",
    "user" : {
      "name" : "Jeffrey Paine",
      "screen_name" : "jpaine",
      "protected" : false,
      "id_str" : "9650622",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/546667795962793985\/r8aVQ0Na_normal.jpeg",
      "id" : 9650622,
      "verified" : false
    }
  },
  "id" : 224350987228422144,
  "created_at" : "2012-07-15 03:53:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Niall McKeown",
      "screen_name" : "niallmckeown",
      "indices" : [ 3, 16 ],
      "id_str" : "19062464",
      "id" : 19062464
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http:\/\/t.co\/9vioqGsG",
      "expanded_url" : "http:\/\/bit.ly\/N8Uh1G",
      "display_url" : "bit.ly\/N8Uh1G"
    } ]
  },
  "geo" : { },
  "id_str" : "224221691839127554",
  "text" : "RT @niallmckeown: Blog: There are 5 Types of Digital Marketers. Which kind are you? http:\/\/t.co\/9vioqGsG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 86 ],
        "url" : "http:\/\/t.co\/9vioqGsG",
        "expanded_url" : "http:\/\/bit.ly\/N8Uh1G",
        "display_url" : "bit.ly\/N8Uh1G"
      } ]
    },
    "geo" : { },
    "id_str" : "215453317608902656",
    "text" : "Blog: There are 5 Types of Digital Marketers. Which kind are you? http:\/\/t.co\/9vioqGsG",
    "id" : 215453317608902656,
    "created_at" : "2012-06-20 14:37:28 +0000",
    "user" : {
      "name" : "Niall McKeown",
      "screen_name" : "niallmckeown",
      "protected" : false,
      "id_str" : "19062464",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1143316324\/niall_normal.png",
      "id" : 19062464,
      "verified" : false
    }
  },
  "id" : 224221691839127554,
  "created_at" : "2012-07-14 19:19:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tim Wu",
      "screen_name" : "Hyperconnection",
      "indices" : [ 3, 19 ],
      "id_str" : "140509231",
      "id" : 140509231
    }, {
      "name" : "Tech in Asia",
      "screen_name" : "techinasia",
      "indices" : [ 112, 123 ],
      "id_str" : "44078873",
      "id" : 44078873
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 107 ],
      "url" : "http:\/\/t.co\/ntJg6prP",
      "expanded_url" : "http:\/\/www.techinasia.com\/asia-one-billion-internet-users\/",
      "display_url" : "techinasia.com\/asia-one-billi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "224220742689095680",
  "text" : "RT @Hyperconnection: Asia Now Has 1 Billion Web Users, And This is What They Do Online http:\/\/t.co\/ntJg6prP via @Techinasia",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tech in Asia",
        "screen_name" : "techinasia",
        "indices" : [ 91, 102 ],
        "id_str" : "44078873",
        "id" : 44078873
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 86 ],
        "url" : "http:\/\/t.co\/ntJg6prP",
        "expanded_url" : "http:\/\/www.techinasia.com\/asia-one-billion-internet-users\/",
        "display_url" : "techinasia.com\/asia-one-billi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "224217808207228928",
    "text" : "Asia Now Has 1 Billion Web Users, And This is What They Do Online http:\/\/t.co\/ntJg6prP via @Techinasia",
    "id" : 224217808207228928,
    "created_at" : "2012-07-14 19:04:25 +0000",
    "user" : {
      "name" : "Tim Wu",
      "screen_name" : "Hyperconnection",
      "protected" : false,
      "id_str" : "140509231",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/552747578270310400\/B4vzMjHZ_normal.jpeg",
      "id" : 140509231,
      "verified" : false
    }
  },
  "id" : 224220742689095680,
  "created_at" : "2012-07-14 19:16:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "#Carl T Griffith \uD83D\uDE0E",
      "screen_name" : "CarlGriffith",
      "indices" : [ 3, 16 ],
      "id_str" : "5417662",
      "id" : 5417662
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 134 ],
      "url" : "http:\/\/t.co\/6rRXzFMG",
      "expanded_url" : "http:\/\/tinyurl.com\/7a9tna9",
      "display_url" : "tinyurl.com\/7a9tna9"
    } ]
  },
  "geo" : { },
  "id_str" : "224217756545978368",
  "text" : "MT @CarlGriffith: Epic Fail: Barclays' Facebook Debacle Highlighted the Chasm Between the Bank and Its Customers: http:\/\/t.co\/6rRXzFMG",
  "id" : 224217756545978368,
  "created_at" : "2012-07-14 19:04:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/224207200275537920\/photo\/1",
      "indices" : [ 31, 51 ],
      "url" : "http:\/\/t.co\/9JzsraCs",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AxyLQsDCIAE7YP-.jpg",
      "id_str" : "224207200279732225",
      "id" : 224207200279732225,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AxyLQsDCIAE7YP-.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9JzsraCs"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224207200275537920",
  "text" : "From a PCB to a flashing badge http:\/\/t.co\/9JzsraCs",
  "id" : 224207200275537920,
  "created_at" : "2012-07-14 18:22:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/224206689044402176\/photo\/1",
      "indices" : [ 73, 93 ],
      "url" : "http:\/\/t.co\/TN2RLUDf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AxyKy7kCEAAT2Gq.jpg",
      "id_str" : "224206689048596480",
      "id" : 224206689048596480,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AxyKy7kCEAAT2Gq.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TN2RLUDf"
    } ],
    "hashtags" : [ {
      "text" : "dmmf",
      "indices" : [ 67, 72 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224206689044402176",
  "text" : "Son learn to assemble Printed Circuit Board using a supplied kit.  #dmmf http:\/\/t.co\/TN2RLUDf",
  "id" : 224206689044402176,
  "created_at" : "2012-07-14 18:20:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224083662960988160",
  "text" : "Heading to Dublin Mini Maker Faire with 8 yo. hope to get him inspire to make thing.",
  "id" : 224083662960988160,
  "created_at" : "2012-07-14 10:11:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "224082840491524096",
  "text" : "@joeljoshuagoh what happen?",
  "id" : 224082840491524096,
  "created_at" : "2012-07-14 10:08:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "indices" : [ 3, 15 ],
      "id_str" : "14107081",
      "id" : 14107081
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 105 ],
      "url" : "http:\/\/t.co\/LylPVTwp",
      "expanded_url" : "http:\/\/asia.cnet.com\/one-in-three-singaporeans-would-rather-lose-a-windfall-than-a-phone-62217813.htm?src=twt_share",
      "display_url" : "asia.cnet.com\/one-in-three-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "224082122355392512",
  "text" : "RT @geekonomics: One in three Singaporeans would rather lose a windfall than a phone http:\/\/t.co\/LylPVTwp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 88 ],
        "url" : "http:\/\/t.co\/LylPVTwp",
        "expanded_url" : "http:\/\/asia.cnet.com\/one-in-three-singaporeans-would-rather-lose-a-windfall-than-a-phone-62217813.htm?src=twt_share",
        "display_url" : "asia.cnet.com\/one-in-three-s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "224068603199234048",
    "text" : "One in three Singaporeans would rather lose a windfall than a phone http:\/\/t.co\/LylPVTwp",
    "id" : 224068603199234048,
    "created_at" : "2012-07-14 09:11:32 +0000",
    "user" : {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "protected" : false,
      "id_str" : "14107081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815459302941093888\/HFWgUA6V_normal.jpg",
      "id" : 14107081,
      "verified" : false
    }
  },
  "id" : 224082122355392512,
  "created_at" : "2012-07-14 10:05:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bray Air Display",
      "screen_name" : "BrayAirShow",
      "indices" : [ 25, 37 ],
      "id_str" : "564475504",
      "id" : 564475504
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "summer4kid",
      "indices" : [ 38, 49 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223916521901129728",
  "text" : "Next Sunday 22 July 2012 @brayairshow #summer4kid",
  "id" : 223916521901129728,
  "created_at" : "2012-07-13 23:07:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SimonNRicketts",
      "screen_name" : "SimonNRicketts",
      "indices" : [ 3, 18 ],
      "id_str" : "18991263",
      "id" : 18991263
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223915140087693312",
  "text" : "RT @SimonNRicketts: The Olympic rings would make a great Venn Diagram. \"People who give a shit, People with tickets, People who have inf ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "223877923885690880",
    "text" : "The Olympic rings would make a great Venn Diagram. \"People who give a shit, People with tickets, People who have infringed copyright\" etc",
    "id" : 223877923885690880,
    "created_at" : "2012-07-13 20:33:51 +0000",
    "user" : {
      "name" : "SimonNRicketts",
      "screen_name" : "SimonNRicketts",
      "protected" : false,
      "id_str" : "18991263",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000759409678\/0f9477b787db017cfb2c0c4aac78cbf3_normal.jpeg",
      "id" : 18991263,
      "verified" : false
    }
  },
  "id" : 223915140087693312,
  "created_at" : "2012-07-13 23:01:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223812793122566145",
  "text" : "Undeterred I try again on that Lego Club form. It works!. The form did not recognise Ireland postal code. You have to put 4 instead of D4.",
  "id" : 223812793122566145,
  "created_at" : "2012-07-13 16:15:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/223811003815698432\/photo\/1",
      "indices" : [ 108, 128 ],
      "url" : "http:\/\/t.co\/6691Xs59",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Axsi7BPCIAAokIC.png",
      "id_str" : "223811003824087040",
      "id" : 223811003824087040,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Axsi7BPCIAAokIC.png",
      "sizes" : [ {
        "h" : 622,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 622,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 622,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 622,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6691Xs59"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223811003815698432",
  "text" : "Dear Lego Club, Your form show \"Please enter valid information.\" but never show us which info is not valid. http:\/\/t.co\/6691Xs59",
  "id" : 223811003815698432,
  "created_at" : "2012-07-13 16:07:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Cousins",
      "screen_name" : "xcelbusiness",
      "indices" : [ 0, 13 ],
      "id_str" : "2410788877",
      "id" : 2410788877
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "223772226502139905",
  "geo" : { },
  "id_str" : "223772901051084800",
  "in_reply_to_user_id" : 119546373,
  "text" : "@xcelbusiness Ticket for closing ceremony is till available when we check last evening but it about 650 GBP. ;P",
  "id" : 223772901051084800,
  "in_reply_to_status_id" : 223772226502139905,
  "created_at" : "2012-07-13 13:36:31 +0000",
  "in_reply_to_screen_name" : "_helencousins",
  "in_reply_to_user_id_str" : "119546373",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "InternetMeMe",
      "indices" : [ 61, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223771873631154176",
  "text" : "\"Justice not done\", claims Taoiseach. John Terry not guilty. #InternetMeMe",
  "id" : 223771873631154176,
  "created_at" : "2012-07-13 13:32:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223768838293041152",
  "text" : "\"SINGAPORE GLOBAL (SG) REACHES 10,000!!! Now one of the Biggest Groups for SGP in Linkedin\" Lot of spammer on board &amp; you waving your flag?",
  "id" : 223768838293041152,
  "created_at" : "2012-07-13 13:20:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "thisisnotlazyweb",
      "indices" : [ 97, 114 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223723603970109441",
  "text" : "Anything to do or visit at Westport? (I can google but I prefer the interaction on social media) #thisisnotlazyweb Thk You.",
  "id" : 223723603970109441,
  "created_at" : "2012-07-13 10:20:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223673606222266368",
  "text" : "Try linking Google Analytics code to Google Checkout\/Google Wallet on Joomla before? Google official documentation does not really help.",
  "id" : 223673606222266368,
  "created_at" : "2012-07-13 07:01:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223482091353026560",
  "text" : "RT @SimonPRepublic: Audacity experts out there: trying to start a new edit of an interview &amp; all the edit options are greyed out &am ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "223479968468967425",
    "text" : "Audacity experts out there: trying to start a new edit of an interview &amp; all the edit options are greyed out &amp; no shortcuts work. Any ideas?",
    "id" : 223479968468967425,
    "created_at" : "2012-07-12 18:12:31 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 223482091353026560,
  "created_at" : "2012-07-12 18:20:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223176104096186368",
  "text" : "Security code text message at this hour is triggering alarm bell on Missus. LOL",
  "id" : 223176104096186368,
  "created_at" : "2012-07-11 22:05:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 75, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 50 ],
      "url" : "http:\/\/t.co\/uSWQ2m1v",
      "expanded_url" : "http:\/\/www.facebook.com\/GetMeasure",
      "display_url" : "facebook.com\/GetMeasure"
    } ]
  },
  "geo" : { },
  "id_str" : "223173152405061632",
  "text" : "My Facebook page \"GetMeasure\" http:\/\/t.co\/uSWQ2m1v need some company after #irishbizparty ends. See you there.",
  "id" : 223173152405061632,
  "created_at" : "2012-07-11 21:53:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 80, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223170832808161280",
  "text" : "I help small biz make sense of Web Analytics data so they can sell more online. #irishbizparty",
  "id" : 223170832808161280,
  "created_at" : "2012-07-11 21:44:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Encouragxcellns",
      "screen_name" : "encouragxcellns",
      "indices" : [ 0, 16 ],
      "id_str" : "64884948",
      "id" : 64884948
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "223166405913219073",
  "geo" : { },
  "id_str" : "223167188272553984",
  "in_reply_to_user_id" : 64884948,
  "text" : "@encouragxcellns hello there. Hope this msg finds you well.",
  "id" : 223167188272553984,
  "in_reply_to_status_id" : 223166405913219073,
  "created_at" : "2012-07-11 21:29:38 +0000",
  "in_reply_to_screen_name" : "encouragxcellns",
  "in_reply_to_user_id_str" : "64884948",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Maker",
      "screen_name" : "DublinMaker",
      "indices" : [ 3, 15 ],
      "id_str" : "484902159",
      "id" : 484902159
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223166075729223682",
  "text" : "RT @DublinMaker: Three days to Dublin Mini Maker Faire.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "223165026150789120",
    "text" : "Three days to Dublin Mini Maker Faire.",
    "id" : 223165026150789120,
    "created_at" : "2012-07-11 21:21:03 +0000",
    "user" : {
      "name" : "Dublin Maker",
      "screen_name" : "DublinMaker",
      "protected" : false,
      "id_str" : "484902159",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/480718625351802880\/zLCCOuCT_normal.png",
      "id" : 484902159,
      "verified" : false
    }
  },
  "id" : 223166075729223682,
  "created_at" : "2012-07-11 21:25:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 125, 139 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223165493303975936",
  "text" : "The best organisations are using Web Analytics to find customers, understand their online behaviour &amp; serve them better. #irishbizparty",
  "id" : 223165493303975936,
  "created_at" : "2012-07-11 21:22:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 6, 20 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223150237466902528",
  "text" : "Isn't #irishbizparty starts at 9.30 pm?",
  "id" : 223150237466902528,
  "created_at" : "2012-07-11 20:22:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223149744036388864",
  "text" : "\u4E00\u5207\u6709\u70BA\u6CD5, \u5982\u5922\u5E7B\u6CE1\u5F71, \u5982\u9732\u4EA6\u5982\u96FB, \u61C9\u4F5C\u5982\u662F\u89C0.",
  "id" : 223149744036388864,
  "created_at" : "2012-07-11 20:20:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "223130988904062976",
  "text" : "Lunch time at Nyonya Malaysian Restaurant along Dame street and most of the customers came in with Groupon voucher.",
  "id" : 223130988904062976,
  "created_at" : "2012-07-11 19:05:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "I Need a Job Norwich",
      "screen_name" : "Needajobnorwich",
      "indices" : [ 3, 19 ],
      "id_str" : "622158335",
      "id" : 622158335
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222785940056317954",
  "text" : "RT @Needajobnorwich: If you're reading this please press RT, will take under a second, It will mean nothing to you but may just may get  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "221870673411321856",
    "text" : "If you're reading this please press RT, will take under a second, It will mean nothing to you but may just may get me a Job.,Thank you",
    "id" : 221870673411321856,
    "created_at" : "2012-07-08 07:37:45 +0000",
    "user" : {
      "name" : "I Need a Job Norwich",
      "screen_name" : "Needajobnorwich",
      "protected" : false,
      "id_str" : "622158335",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2390344876\/ijkevmlgiotzjitd04sj_normal.jpeg",
      "id" : 622158335,
      "verified" : false
    }
  },
  "id" : 222785940056317954,
  "created_at" : "2012-07-10 20:14:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SyncWebApp",
      "screen_name" : "syncwebapp",
      "indices" : [ 3, 14 ],
      "id_str" : "631013614",
      "id" : 631013614
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222689981829885954",
  "text" : "RT @syncwebapp: Add PayPal customers automatically to Aweber an email news letter service for small biz . I can set up for you. http:\/\/t ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/syncwebapp\/status\/222683412404764672\/photo\/1",
        "indices" : [ 112, 132 ],
        "url" : "http:\/\/t.co\/euTgaj2f",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/AxchYj4CAAEEu5T.jpg",
        "id_str" : "222683412408958977",
        "id" : 222683412408958977,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AxchYj4CAAEEu5T.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 184,
          "resize" : "fit",
          "w" : 536
        }, {
          "h" : 184,
          "resize" : "fit",
          "w" : 536
        }, {
          "h" : 184,
          "resize" : "fit",
          "w" : 536
        }, {
          "h" : 184,
          "resize" : "fit",
          "w" : 536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/euTgaj2f"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "222683412404764672",
    "text" : "Add PayPal customers automatically to Aweber an email news letter service for small biz . I can set up for you. http:\/\/t.co\/euTgaj2f",
    "id" : 222683412404764672,
    "created_at" : "2012-07-10 13:27:18 +0000",
    "user" : {
      "name" : "SyncWebApp",
      "screen_name" : "syncwebapp",
      "protected" : false,
      "id_str" : "631013614",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2389024672\/33gg0xyp2lvfazp3bve8_normal.jpeg",
      "id" : 631013614,
      "verified" : false
    }
  },
  "id" : 222689981829885954,
  "created_at" : "2012-07-10 13:53:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aysha Ridzuan",
      "screen_name" : "ayshardzn",
      "indices" : [ 3, 13 ],
      "id_str" : "52719612",
      "id" : 52719612
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222634444417282050",
  "text" : "RT @ayshardzn: What you don't see with your eyes, don't invent with your mouth.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "222633930682150913",
    "text" : "What you don't see with your eyes, don't invent with your mouth.",
    "id" : 222633930682150913,
    "created_at" : "2012-07-10 10:10:40 +0000",
    "user" : {
      "name" : "Aysha Ridzuan",
      "screen_name" : "ayshardzn",
      "protected" : false,
      "id_str" : "52719612",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1036059517648232448\/LJcap7Tm_normal.jpg",
      "id" : 52719612,
      "verified" : true
    }
  },
  "id" : 222634444417282050,
  "created_at" : "2012-07-10 10:12:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222427354583146498",
  "text" : "Small biz owner lost his 4 mths biz to riot he built from his 10 years savings.",
  "id" : 222427354583146498,
  "created_at" : "2012-07-09 20:29:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http:\/\/t.co\/codJaqwO",
      "expanded_url" : "http:\/\/sueunerman.com\/2012\/07\/the-marketing-truth-deficit-is-greater-than-i-thought\/",
      "display_url" : "sueunerman.com\/2012\/07\/the-ma\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "222333860120244224",
  "text" : "challenge for agencies 66% of the public don't trust advertising. http:\/\/t.co\/codJaqwO",
  "id" : 222333860120244224,
  "created_at" : "2012-07-09 14:18:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 0, 12 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "222270545360723969",
  "geo" : { },
  "id_str" : "222276179443458048",
  "in_reply_to_user_id" : 33462545,
  "text" : "@yapphenghui \u53F6\u516C\u90A3\u4E2A\u56FE\u7247404 \u5462",
  "id" : 222276179443458048,
  "in_reply_to_status_id" : 222270545360723969,
  "created_at" : "2012-07-09 10:29:05 +0000",
  "in_reply_to_screen_name" : "yapphenghui",
  "in_reply_to_user_id_str" : "33462545",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Design Sojourn",
      "screen_name" : "designsojourn",
      "indices" : [ 3, 17 ],
      "id_str" : "3029921",
      "id" : 3029921
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "service",
      "indices" : [ 90, 98 ]
    }, {
      "text" : "design",
      "indices" : [ 99, 106 ]
    }, {
      "text" : "innovation",
      "indices" : [ 107, 118 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222275508208017409",
  "text" : "RT @designsojourn: All resturants should train their service staff to take decent photos. #service #design #innovation.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "service",
        "indices" : [ 71, 79 ]
      }, {
        "text" : "design",
        "indices" : [ 80, 87 ]
      }, {
        "text" : "innovation",
        "indices" : [ 88, 99 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "222271752770818051",
    "text" : "All resturants should train their service staff to take decent photos. #service #design #innovation.",
    "id" : 222271752770818051,
    "created_at" : "2012-07-09 10:11:30 +0000",
    "user" : {
      "name" : "Design Sojourn",
      "screen_name" : "designsojourn",
      "protected" : false,
      "id_str" : "3029921",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/911468983345324033\/55akIGM2_normal.jpg",
      "id" : 3029921,
      "verified" : false
    }
  },
  "id" : 222275508208017409,
  "created_at" : "2012-07-09 10:26:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "singaporeshadesofgrey",
      "indices" : [ 29, 51 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222225377857257473",
  "text" : "MRT train jerking off again. #singaporeshadesofgrey",
  "id" : 222225377857257473,
  "created_at" : "2012-07-09 07:07:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222223608309747712",
  "text" : "RT @mrbrown: \"Let's ballot for a BTO flat,\" he whispered.\n\nHer toes curled, as thoughts of the 30-year loan pleasured her senses.\n\n#sing ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "singaporeshadesofgrey",
        "indices" : [ 118, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "222205846111793153",
    "text" : "\"Let's ballot for a BTO flat,\" he whispered.\n\nHer toes curled, as thoughts of the 30-year loan pleasured her senses.\n\n#singaporeshadesofgrey",
    "id" : 222205846111793153,
    "created_at" : "2012-07-09 05:49:36 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 222223608309747712,
  "created_at" : "2012-07-09 07:00:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/0MFBr7Yf",
      "expanded_url" : "http:\/\/www.delighta.com\/",
      "display_url" : "delighta.com"
    } ]
  },
  "geo" : { },
  "id_str" : "222223118935134209",
  "text" : "Access Google services using SMS. No app require.  http:\/\/t.co\/0MFBr7Yf",
  "id" : 222223118935134209,
  "created_at" : "2012-07-09 06:58:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 123, 143 ],
      "url" : "http:\/\/t.co\/Umt0qyIr",
      "expanded_url" : "http:\/\/citiwire.net\/columns\/a-nation-of-public-housing\/",
      "display_url" : "citiwire.net\/columns\/a-nati\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "222222050738176000",
  "text" : "In Singapore, the landlord is most likely the govt. also property developer &amp; mortgages provider - so far I am happy.  http:\/\/t.co\/Umt0qyIr",
  "id" : 222222050738176000,
  "created_at" : "2012-07-09 06:54:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "indices" : [ 3, 13 ],
      "id_str" : "17524108",
      "id" : 17524108
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http:\/\/t.co\/1KotqtMp",
      "expanded_url" : "http:\/\/bit.ly\/M4VZqb",
      "display_url" : "bit.ly\/M4VZqb"
    } ]
  },
  "geo" : { },
  "id_str" : "222067383265796097",
  "text" : "RT @ericknopf: Good deal: Groupon opens first physical store http:\/\/t.co\/1KotqtMp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 66 ],
        "url" : "http:\/\/t.co\/1KotqtMp",
        "expanded_url" : "http:\/\/bit.ly\/M4VZqb",
        "display_url" : "bit.ly\/M4VZqb"
      } ]
    },
    "geo" : { },
    "id_str" : "222029046257692672",
    "text" : "Good deal: Groupon opens first physical store http:\/\/t.co\/1KotqtMp",
    "id" : 222029046257692672,
    "created_at" : "2012-07-08 18:07:04 +0000",
    "user" : {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "protected" : false,
      "id_str" : "17524108",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460579737018970113\/3x_hi1Zc_normal.jpeg",
      "id" : 17524108,
      "verified" : false
    }
  },
  "id" : 222067383265796097,
  "created_at" : "2012-07-08 20:39:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore News",
      "screen_name" : "SGnews",
      "indices" : [ 58, 65 ],
      "id_str" : "15731526",
      "id" : 15731526
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 133 ],
      "url" : "http:\/\/t.co\/gX5WILeK",
      "expanded_url" : "http:\/\/bit.ly\/PBC6Ze",
      "display_url" : "bit.ly\/PBC6Ze"
    } ]
  },
  "geo" : { },
  "id_str" : "222066493993660416",
  "text" : "Good news for Bombardier in N.Ireland if order is place? \u201C@SGnews: AirAsia in talks to buy Bombardier 160-seater http:\/\/t.co\/gX5WILeK\u201D",
  "id" : 222066493993660416,
  "created_at" : "2012-07-08 20:35:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "222033242629476352",
  "text" : "Oh'cmon FB, Just want to read the news article from the link. I do not want to install another app just to do that!",
  "id" : 222033242629476352,
  "created_at" : "2012-07-08 18:23:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 3, 19 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 121 ],
      "url" : "http:\/\/t.co\/BR35078L",
      "expanded_url" : "http:\/\/www.funkygoddess.ie\/",
      "display_url" : "funkygoddess.ie"
    } ]
  },
  "geo" : { },
  "id_str" : "221944446466535425",
  "text" : "RT @funkygoddessirl: Funky Goddess...Helping Girls and Parents everywhere to make life a bit easier! http:\/\/t.co\/BR35078L",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 100 ],
        "url" : "http:\/\/t.co\/BR35078L",
        "expanded_url" : "http:\/\/www.funkygoddess.ie\/",
        "display_url" : "funkygoddess.ie"
      } ]
    },
    "geo" : { },
    "id_str" : "221896425221529600",
    "text" : "Funky Goddess...Helping Girls and Parents everywhere to make life a bit easier! http:\/\/t.co\/BR35078L",
    "id" : 221896425221529600,
    "created_at" : "2012-07-08 09:20:05 +0000",
    "user" : {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "protected" : false,
      "id_str" : "326869253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946430925667229696\/wSG185FK_normal.jpg",
      "id" : 326869253,
      "verified" : true
    }
  },
  "id" : 221944446466535425,
  "created_at" : "2012-07-08 12:30:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http:\/\/t.co\/iLIuVyu5",
      "expanded_url" : "http:\/\/buff.ly\/PrIVv5",
      "display_url" : "buff.ly\/PrIVv5"
    } ]
  },
  "geo" : { },
  "id_str" : "221943452840103936",
  "text" : "RT @patphelan: 10 Ways Honesty Makes You More Money http:\/\/t.co\/iLIuVyu5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 37, 57 ],
        "url" : "http:\/\/t.co\/iLIuVyu5",
        "expanded_url" : "http:\/\/buff.ly\/PrIVv5",
        "display_url" : "buff.ly\/PrIVv5"
      } ]
    },
    "geo" : { },
    "id_str" : "221921857526439936",
    "text" : "10 Ways Honesty Makes You More Money http:\/\/t.co\/iLIuVyu5",
    "id" : 221921857526439936,
    "created_at" : "2012-07-08 11:01:08 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 221943452840103936,
  "created_at" : "2012-07-08 12:26:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aussie China Stories",
      "screen_name" : "AussieChinese",
      "indices" : [ 3, 17 ],
      "id_str" : "497886235",
      "id" : 497886235
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221942121664491520",
  "text" : "RT @AussieChinese: 6 Photos from a French Photographer Showing China in 2050 with Foreigners as Migrant Workers \u6CD5\u56FD\u4EBA\u62CD\u201C\u6D0B\u6C11\u5DE5\u5728\u4E2D\u56FD\u201D... http:\/\/t ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterfeed.com\" rel=\"nofollow\"\u003Etwitterfeed\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "china",
        "indices" : [ 130, 136 ]
      } ],
      "urls" : [ {
        "indices" : [ 109, 129 ],
        "url" : "http:\/\/t.co\/fpsAbPqt",
        "expanded_url" : "http:\/\/bit.ly\/NyO8NC",
        "display_url" : "bit.ly\/NyO8NC"
      } ]
    },
    "geo" : { },
    "id_str" : "221939415767977984",
    "text" : "6 Photos from a French Photographer Showing China in 2050 with Foreigners as Migrant Workers \u6CD5\u56FD\u4EBA\u62CD\u201C\u6D0B\u6C11\u5DE5\u5728\u4E2D\u56FD\u201D... http:\/\/t.co\/fpsAbPqt #china",
    "id" : 221939415767977984,
    "created_at" : "2012-07-08 12:10:54 +0000",
    "user" : {
      "name" : "Aussie China Stories",
      "screen_name" : "AussieChinese",
      "protected" : false,
      "id_str" : "497886235",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1840766321\/Twitter_China_Stories888_normal.jpg",
      "id" : 497886235,
      "verified" : false
    }
  },
  "id" : 221942121664491520,
  "created_at" : "2012-07-08 12:21:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221941857712750594",
  "text" : "@avalanchelynn Let put that into law. Will run my campaign on this platform. Look forward to your $ support. You can PayPal me.",
  "id" : 221941857712750594,
  "created_at" : "2012-07-08 12:20:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http:\/\/t.co\/JwlX2Grl",
      "expanded_url" : "http:\/\/m.clickz.com\/clickz\/column\/2188027\/little-google-analytics-matter",
      "display_url" : "m.clickz.com\/clickz\/column\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "221896563922968576",
  "text" : "Google Analytics may be free to \"get\" but not necessarily free to \"get right.\" http:\/\/t.co\/JwlX2Grl",
  "id" : 221896563922968576,
  "created_at" : "2012-07-08 09:20:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221895303387807744",
  "text" : "@avalanchelynn same over here!",
  "id" : 221895303387807744,
  "created_at" : "2012-07-08 09:15:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lonely Planet",
      "screen_name" : "lonelyplanet",
      "indices" : [ 3, 16 ],
      "id_str" : "15066760",
      "id" : 15066760
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 125 ],
      "url" : "http:\/\/t.co\/qisZGBLe",
      "expanded_url" : "http:\/\/ow.ly\/bXFXM",
      "display_url" : "ow.ly\/bXFXM"
    } ]
  },
  "geo" : { },
  "id_str" : "221894724179595264",
  "text" : "RT @lonelyplanet: Send us a great picture for our Twitter page &amp; win a LP photography book. T&amp;Cs http:\/\/t.co\/qisZGBLe 1pic p per ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "landscapes",
        "indices" : [ 130, 141 ]
      } ],
      "urls" : [ {
        "indices" : [ 87, 107 ],
        "url" : "http:\/\/t.co\/qisZGBLe",
        "expanded_url" : "http:\/\/ow.ly\/bXFXM",
        "display_url" : "ow.ly\/bXFXM"
      } ]
    },
    "geo" : { },
    "id_str" : "221232975944232960",
    "text" : "Send us a great picture for our Twitter page &amp; win a LP photography book. T&amp;Cs http:\/\/t.co\/qisZGBLe 1pic p person. Tag w\/ #landscapes",
    "id" : 221232975944232960,
    "created_at" : "2012-07-06 13:23:46 +0000",
    "user" : {
      "name" : "Lonely Planet",
      "screen_name" : "lonelyplanet",
      "protected" : false,
      "id_str" : "15066760",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659349744532246528\/oJDWTI75_normal.png",
      "id" : 15066760,
      "verified" : true
    }
  },
  "id" : 221894724179595264,
  "created_at" : "2012-07-08 09:13:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http:\/\/t.co\/pfMYLEKL",
      "expanded_url" : "http:\/\/j.mp\/L1sz67",
      "display_url" : "j.mp\/L1sz67"
    } ]
  },
  "geo" : { },
  "id_str" : "221887017947115520",
  "text" : "Why your business needs web analytics - Enterprise Ireland (2005) http:\/\/t.co\/pfMYLEKL",
  "id" : 221887017947115520,
  "created_at" : "2012-07-08 08:42:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 3, 19 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "youknowitssunnyinireland",
      "indices" : [ 21, 46 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221677349727510528",
  "text" : "RT @funkygoddessirl: #youknowitssunnyinireland when.....no one is on twitter!! lol any more?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "youknowitssunnyinireland",
        "indices" : [ 0, 25 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "221591689855381505",
    "text" : "#youknowitssunnyinireland when.....no one is on twitter!! lol any more?",
    "id" : 221591689855381505,
    "created_at" : "2012-07-07 13:09:10 +0000",
    "user" : {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "protected" : false,
      "id_str" : "326869253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946430925667229696\/wSG185FK_normal.jpg",
      "id" : 326869253,
      "verified" : true
    }
  },
  "id" : 221677349727510528,
  "created_at" : "2012-07-07 18:49:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 92 ],
      "url" : "http:\/\/t.co\/yQ92yfeL",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/The_Host_(2006_film)",
      "display_url" : "en.wikipedia.org\/wiki\/The_Host_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "221410585693257728",
  "text" : "LOL. First time watching The Host, find it funny even it a horror show. http:\/\/t.co\/yQ92yfeL",
  "id" : 221410585693257728,
  "created_at" : "2012-07-07 01:09:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221407005674446848",
  "text" : "Korean film \"The Host\" is showing on Film 4 now on 2am Sat morning...",
  "id" : 221407005674446848,
  "created_at" : "2012-07-07 00:55:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Camile Thai",
      "screen_name" : "CamileThai",
      "indices" : [ 3, 14 ],
      "id_str" : "223834162",
      "id" : 223834162
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221280341376253952",
  "text" : "RT @CamileThai: You can always eat vegetarian at Camile. Almost all our dishes can be made as a vegetarian option, where we substitute m ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "221279880472576000",
    "text" : "You can always eat vegetarian at Camile. Almost all our dishes can be made as a vegetarian option, where we substitute meat for tofu.",
    "id" : 221279880472576000,
    "created_at" : "2012-07-06 16:30:09 +0000",
    "user" : {
      "name" : "Camile Thai",
      "screen_name" : "CamileThai",
      "protected" : false,
      "id_str" : "223834162",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1029363342480158721\/wpNrTxAR_normal.jpg",
      "id" : 223834162,
      "verified" : false
    }
  },
  "id" : 221280341376253952,
  "created_at" : "2012-07-06 16:31:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221278085826347008",
  "text" : "Poor user experience. Download PDF form to sign up for Summer children swimming course &amp; travel to the premise to make payment.",
  "id" : 221278085826347008,
  "created_at" : "2012-07-06 16:23:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Cousins",
      "screen_name" : "xcelbusiness",
      "indices" : [ 6, 19 ],
      "id_str" : "2410788877",
      "id" : 2410788877
    }, {
      "name" : "Flor McCarthy",
      "screen_name" : "FlorMcCarthy",
      "indices" : [ 20, 33 ],
      "id_str" : "47958004",
      "id" : 47958004
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 135 ],
      "url" : "http:\/\/t.co\/YztKf1eW",
      "expanded_url" : "http:\/\/lockerz.com\/s\/222717770",
      "display_url" : "lockerz.com\/s\/222717770"
    } ]
  },
  "geo" : { },
  "id_str" : "221243936134541312",
  "text" : "+1 RT @xcelbusiness @FlorMcCarthy Now this is what I call an imaginative response from a flood affected hair salon http:\/\/t.co\/YztKf1eW\u201D",
  "id" : 221243936134541312,
  "created_at" : "2012-07-06 14:07:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/221194510733033472\/photo\/1",
      "indices" : [ 65, 85 ],
      "url" : "http:\/\/t.co\/lyhT8oRK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AxHXPE7CQAE_M8Z.png",
      "id_str" : "221194510737227777",
      "id" : 221194510737227777,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AxHXPE7CQAE_M8Z.png",
      "sizes" : [ {
        "h" : 104,
        "resize" : "fit",
        "w" : 273
      }, {
        "h" : 104,
        "resize" : "crop",
        "w" : 104
      }, {
        "h" : 104,
        "resize" : "fit",
        "w" : 273
      }, {
        "h" : 104,
        "resize" : "fit",
        "w" : 273
      }, {
        "h" : 104,
        "resize" : "fit",
        "w" : 273
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lyhT8oRK"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http:\/\/t.co\/reiyBt85",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/Numbers_in_Chinese_culture#Eight",
      "display_url" : "en.wikipedia.org\/wiki\/Numbers_i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "221194510733033472",
  "text" : "An auspicious number (888) here  Reference: http:\/\/t.co\/reiyBt85 http:\/\/t.co\/lyhT8oRK",
  "id" : 221194510733033472,
  "created_at" : "2012-07-06 10:50:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 114 ],
      "url" : "http:\/\/t.co\/vnq8Ulst",
      "expanded_url" : "http:\/\/www.facebook.com\/note.php?note_id=228217223889603",
      "display_url" : "facebook.com\/note.php?note_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "221192044893323265",
  "text" : "\"If you don't see 'Invite Friends' then you are still viewing the Facebook Page as the Page'  http:\/\/t.co\/vnq8Ulst",
  "id" : 221192044893323265,
  "created_at" : "2012-07-06 10:41:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221191852324438016",
  "text" : "@HelmyRooney Marketing people should have some culture sense esp they are in South East Asia :)",
  "id" : 221191852324438016,
  "created_at" : "2012-07-06 10:40:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221189908864315393",
  "text" : "@HelmyRooney I would not associate sales of alcoholic beverages during Ramadhan period unless I am wrong here.",
  "id" : 221189908864315393,
  "created_at" : "2012-07-06 10:32:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sean Hennessy",
      "screen_name" : "LEDPowerhouse",
      "indices" : [ 5, 19 ],
      "id_str" : "301411728",
      "id" : 301411728
    }, {
      "name" : "Mandy O'Rorke",
      "screen_name" : "babybaymarket",
      "indices" : [ 20, 34 ],
      "id_str" : "569306991",
      "id" : 569306991
    }, {
      "name" : "rent and hire",
      "screen_name" : "rentandhire",
      "indices" : [ 35, 47 ],
      "id_str" : "746755827210518528",
      "id" : 746755827210518528
    }, {
      "name" : "Encouragxcellns",
      "screen_name" : "encouragxcellns",
      "indices" : [ 61, 77 ],
      "id_str" : "64884948",
      "id" : 64884948
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 90, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221187336371834882",
  "text" : "Thks @LEDPowerhouse @babybaymarket @Rentandhire @biznireland @encouragxcellns  for the RT #irishbizparty",
  "id" : 221187336371834882,
  "created_at" : "2012-07-06 10:22:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "To be decided",
      "screen_name" : "mcwilliamph",
      "indices" : [ 5, 17 ],
      "id_str" : "22757695",
      "id" : 22757695
    }, {
      "name" : "ciaranreidsafetyltd",
      "screen_name" : "ciaranreidsafe",
      "indices" : [ 18, 33 ],
      "id_str" : "441640550",
      "id" : 441640550
    }, {
      "name" : "Conor Kenny",
      "screen_name" : "conor_kenny",
      "indices" : [ 34, 46 ],
      "id_str" : "1573994420",
      "id" : 1573994420
    }, {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 47, 63 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 75, 89 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221187114379915264",
  "text" : "Thks @mcwilliamph @ciaranreidsafe @Conor_Kenny @funkygoddessirl for the RT #irishbizparty",
  "id" : 221187114379915264,
  "created_at" : "2012-07-06 10:21:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 103 ],
      "url" : "http:\/\/t.co\/GTccq40I",
      "expanded_url" : "http:\/\/venturevillage.eu\/startup-hipster",
      "display_url" : "venturevillage.eu\/startup-hipster"
    } ]
  },
  "geo" : { },
  "id_str" : "221185955732787203",
  "text" : "One sign of a startup Hipster  - using the world \"social\" at least 15 times a day. http:\/\/t.co\/GTccq40I",
  "id" : 221185955732787203,
  "created_at" : "2012-07-06 10:16:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.dropbox.com\" rel=\"nofollow\"\u003EDropbox\u00A0\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 79, 92 ]
    }, {
      "text" : "Irebiz",
      "indices" : [ 93, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 121 ],
      "url" : "http:\/\/t.co\/4neoi36I",
      "expanded_url" : "http:\/\/db.tt\/Z2Id22Zj",
      "display_url" : "db.tt\/Z2Id22Zj"
    } ]
  },
  "geo" : { },
  "id_str" : "221173887965204480",
  "text" : "[Sorry for the broken link] Best of Web Analytics articles from Clickz in PDF  #SMEcommunity #Irebiz http:\/\/t.co\/4neoi36I",
  "id" : 221173887965204480,
  "created_at" : "2012-07-06 09:28:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "indices" : [ 0, 12 ],
      "id_str" : "15191926",
      "id" : 15191926
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "221151221531160577",
  "geo" : { },
  "id_str" : "221166255233699842",
  "in_reply_to_user_id" : 15191926,
  "text" : "@clementchio Thks",
  "id" : 221166255233699842,
  "in_reply_to_status_id" : 221151221531160577,
  "created_at" : "2012-07-06 08:58:38 +0000",
  "in_reply_to_screen_name" : "clementchio",
  "in_reply_to_user_id_str" : "15191926",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "221150705765986304",
  "text" : "Is Twitter having hiccups? People I had  followed is being unfollowed",
  "id" : 221150705765986304,
  "created_at" : "2012-07-06 07:56:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeffrey Paine",
      "screen_name" : "jpaine",
      "indices" : [ 3, 10 ],
      "id_str" : "9650622",
      "id" : 9650622
    }, {
      "name" : "The Verge",
      "screen_name" : "verge",
      "indices" : [ 27, 33 ],
      "id_str" : "275686563",
      "id" : 275686563
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 120 ],
      "url" : "http:\/\/t.co\/HkbAWpDc",
      "expanded_url" : "http:\/\/vrge.co\/Nb0qh0",
      "display_url" : "vrge.co\/Nb0qh0"
    } ]
  },
  "geo" : { },
  "id_str" : "220993103258071040",
  "text" : "RT @jpaine: interesting RT @verge: Facebook invests in underwater fiber optic cable project in Asia http:\/\/t.co\/HkbAWpDc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Verge",
        "screen_name" : "verge",
        "indices" : [ 15, 21 ],
        "id_str" : "275686563",
        "id" : 275686563
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 108 ],
        "url" : "http:\/\/t.co\/HkbAWpDc",
        "expanded_url" : "http:\/\/vrge.co\/Nb0qh0",
        "display_url" : "vrge.co\/Nb0qh0"
      } ]
    },
    "geo" : { },
    "id_str" : "220988060341518339",
    "text" : "interesting RT @verge: Facebook invests in underwater fiber optic cable project in Asia http:\/\/t.co\/HkbAWpDc",
    "id" : 220988060341518339,
    "created_at" : "2012-07-05 21:10:34 +0000",
    "user" : {
      "name" : "Jeffrey Paine",
      "screen_name" : "jpaine",
      "protected" : false,
      "id_str" : "9650622",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/546667795962793985\/r8aVQ0Na_normal.jpeg",
      "id" : 9650622,
      "verified" : false
    }
  },
  "id" : 220993103258071040,
  "created_at" : "2012-07-05 21:30:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 83 ],
      "url" : "http:\/\/t.co\/LDoFCB5h",
      "expanded_url" : "http:\/\/support.microsoft.com\/kb\/260563",
      "display_url" : "support.microsoft.com\/kb\/260563"
    } ]
  },
  "geo" : { },
  "id_str" : "220873324065079296",
  "text" : "Just introduce my 8yo to create his first .log file on Window. http:\/\/t.co\/LDoFCB5h",
  "id" : 220873324065079296,
  "created_at" : "2012-07-05 13:34:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    }, {
      "name" : "KAGUYA",
      "screen_name" : "Cute_Honey_",
      "indices" : [ 25, 37 ],
      "id_str" : "3067927176",
      "id" : 3067927176
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220511160439554048",
  "geo" : { },
  "id_str" : "220868583016763392",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney I like what @Cute_Honey_ has done for her site with regards to the EU cookies law. (IMO, the cookies law is stupid)",
  "id" : 220868583016763392,
  "in_reply_to_status_id" : 220511160439554048,
  "created_at" : "2012-07-05 13:15:48 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220748955489812481",
  "text" : "Facebook (Friends, Family), Twitter (Open to all), Linkedin (business), Google + (sounds of cricket)",
  "id" : 220748955489812481,
  "created_at" : "2012-07-05 05:20:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laurel Papworth",
      "screen_name" : "SilkCharm",
      "indices" : [ 0, 10 ],
      "id_str" : "3652891",
      "id" : 3652891
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220742726071750657",
  "in_reply_to_user_id" : 3652891,
  "text" : "@SilkCharm what does this profile ad revenue do for the user?",
  "id" : 220742726071750657,
  "created_at" : "2012-07-05 04:55:41 +0000",
  "in_reply_to_screen_name" : "SilkCharm",
  "in_reply_to_user_id_str" : "3652891",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Irishbizparty",
      "indices" : [ 114, 128 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220652472417533952",
  "text" : "Many Thanks to those who retweet, favorite my tweets and follow me. Hope to catch up with you outside of Twitter. #Irishbizparty",
  "id" : 220652472417533952,
  "created_at" : "2012-07-04 22:57:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laurel Papworth",
      "screen_name" : "SilkCharm",
      "indices" : [ 3, 13 ],
      "id_str" : "3652891",
      "id" : 3652891
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 15, 21 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220651841883607045",
  "text" : "RT @SilkCharm: @mryap I really disagree. Anyone who underestimates Google's play in social doesn't understand how badly they need Profil ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "220651148498046976",
    "geo" : { },
    "id_str" : "220651752507183105",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap I really disagree. Anyone who underestimates Google's play in social doesn't understand how badly they need Profile ad revenue.",
    "id" : 220651752507183105,
    "in_reply_to_status_id" : 220651148498046976,
    "created_at" : "2012-07-04 22:54:11 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Laurel Papworth",
      "screen_name" : "SilkCharm",
      "protected" : false,
      "id_str" : "3652891",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/831015242675085314\/HzRrXy1O_normal.jpg",
      "id" : 3652891,
      "verified" : false
    }
  },
  "id" : 220651841883607045,
  "created_at" : "2012-07-04 22:54:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laurel Papworth",
      "screen_name" : "SilkCharm",
      "indices" : [ 0, 10 ],
      "id_str" : "3652891",
      "id" : 3652891
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220649782761361410",
  "geo" : { },
  "id_str" : "220651148498046976",
  "in_reply_to_user_id" : 3652891,
  "text" : "@SilkCharm Some said Google Plus is like Dublin new Terminal 2 airport. Looks nice, nobody there except the staffs.",
  "id" : 220651148498046976,
  "in_reply_to_status_id" : 220649782761361410,
  "created_at" : "2012-07-04 22:51:47 +0000",
  "in_reply_to_screen_name" : "SilkCharm",
  "in_reply_to_user_id_str" : "3652891",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220645553850171393",
  "text" : "On my timeline, a lot of people is singing praise of Andrea Leadsom.",
  "id" : 220645553850171393,
  "created_at" : "2012-07-04 22:29:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http:\/\/t.co\/nEeujVaO",
      "expanded_url" : "http:\/\/www.reuters.com\/article\/2012\/07\/04\/temasek-review-idUSL3E8I412L20120704?feedType=RSS&feedName=basicMaterialsSector",
      "display_url" : "reuters.com\/article\/2012\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220644194115534848",
  "text" : "Singapore state investor has limited exposure to European assets. http:\/\/t.co\/nEeujVaO",
  "id" : 220644194115534848,
  "created_at" : "2012-07-04 22:24:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 5, 19 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220641841710764032",
  "text" : "This #irishbizparty is introvert (i.e. me) Woodstock",
  "id" : 220641841710764032,
  "created_at" : "2012-07-04 22:14:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 7, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220640580588417024",
  "text" : "Is the #irishbizparty over? I just getting started....",
  "id" : 220640580588417024,
  "created_at" : "2012-07-04 22:09:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 102 ],
      "url" : "http:\/\/t.co\/GnrF8JnW",
      "expanded_url" : "http:\/\/cdn.thenextweb.com\/wp-content\/blogs.dir\/1\/files\/2012\/06\/Screen-Shot-2012-06-28-at-11.43.56-AM.jpg",
      "display_url" : "cdn.thenextweb.com\/wp-content\/blo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220640388011143169",
  "text" : "RT @patphelan: Love this Screen-Shot-2012-06-28-at-11.43.56-AM.jpg 374\u00D7369 pixels http:\/\/t.co\/GnrF8JnW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 87 ],
        "url" : "http:\/\/t.co\/GnrF8JnW",
        "expanded_url" : "http:\/\/cdn.thenextweb.com\/wp-content\/blogs.dir\/1\/files\/2012\/06\/Screen-Shot-2012-06-28-at-11.43.56-AM.jpg",
        "display_url" : "cdn.thenextweb.com\/wp-content\/blo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "220640025057034240",
    "text" : "Love this Screen-Shot-2012-06-28-at-11.43.56-AM.jpg 374\u00D7369 pixels http:\/\/t.co\/GnrF8JnW",
    "id" : 220640025057034240,
    "created_at" : "2012-07-04 22:07:35 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 220640388011143169,
  "created_at" : "2012-07-04 22:09:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 79, 93 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220639429814009856",
  "text" : "Thinking about doing biz in Southeast Asia?and not sure where to start? Ask me #irishbizparty",
  "id" : 220639429814009856,
  "created_at" : "2012-07-04 22:05:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "evidence cloud",
      "screen_name" : "evidencecloud",
      "indices" : [ 3, 17 ],
      "id_str" : "488682592",
      "id" : 488682592
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 19, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220637501910564864",
  "text" : "RT @evidencecloud: #irishbizparty is the bar still serving ??? greetings all from Ireland's newest web resource for High definition cctv ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "irishbizparty",
        "indices" : [ 0, 14 ]
      }, {
        "text" : "useenvr",
        "indices" : [ 132, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "220634478668816385",
    "text" : "#irishbizparty is the bar still serving ??? greetings all from Ireland's newest web resource for High definition cctv cloud storage #useenvr",
    "id" : 220634478668816385,
    "created_at" : "2012-07-04 21:45:33 +0000",
    "user" : {
      "name" : "evidence cloud",
      "screen_name" : "evidencecloud",
      "protected" : false,
      "id_str" : "488682592",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1817878652\/Evidence_cloud_normal.jpg",
      "id" : 488682592,
      "verified" : false
    }
  },
  "id" : 220637501910564864,
  "created_at" : "2012-07-04 21:57:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Funky Goddess Irl",
      "screen_name" : "FunkyGoddessIrl",
      "indices" : [ 0, 16 ],
      "id_str" : "1257888804",
      "id" : 1257888804
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220636448448184320",
  "geo" : { },
  "id_str" : "220637142546784256",
  "in_reply_to_user_id" : 326869253,
  "text" : "@funkygoddessirl Thank you. Great to hear that you love tea.",
  "id" : 220637142546784256,
  "in_reply_to_status_id" : 220636448448184320,
  "created_at" : "2012-07-04 21:56:08 +0000",
  "in_reply_to_screen_name" : "Tweetinggoddess",
  "in_reply_to_user_id_str" : "326869253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 41, 55 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220635829373108227",
  "text" : "Ok who is the brilliant mind behind this #irishbizparty? I going to buy you a beer.",
  "id" : 220635829373108227,
  "created_at" : "2012-07-04 21:50:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220635330703925249",
  "text" : "What with all those sirens going on in Drimnagh?",
  "id" : 220635330703925249,
  "created_at" : "2012-07-04 21:48:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 84, 98 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220635108036714496",
  "text" : "Need help to use Google Analytics to calculate customer lifetime value? I can help. #irishbizparty",
  "id" : 220635108036714496,
  "created_at" : "2012-07-04 21:48:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 72 ],
      "url" : "http:\/\/t.co\/MP7RAY0q",
      "expanded_url" : "http:\/\/bit.ly\/MVmEkJ",
      "display_url" : "bit.ly\/MVmEkJ"
    } ]
  },
  "geo" : { },
  "id_str" : "220632320724566016",
  "text" : "RT @enormous: Need a washing machine? Item Details: http:\/\/t.co\/MP7RAY0q",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 38, 58 ],
        "url" : "http:\/\/t.co\/MP7RAY0q",
        "expanded_url" : "http:\/\/bit.ly\/MVmEkJ",
        "display_url" : "bit.ly\/MVmEkJ"
      } ]
    },
    "geo" : { },
    "id_str" : "220631507046383616",
    "text" : "Need a washing machine? Item Details: http:\/\/t.co\/MP7RAY0q",
    "id" : 220631507046383616,
    "created_at" : "2012-07-04 21:33:45 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 220632320724566016,
  "created_at" : "2012-07-04 21:36:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http:\/\/t.co\/GTgL6YEH",
      "expanded_url" : "http:\/\/bit.ly\/MVmJ7P",
      "display_url" : "bit.ly\/MVmJ7P"
    } ]
  },
  "geo" : { },
  "id_str" : "220632072937680899",
  "text" : "RT @enormous: Anyone want a piano? http:\/\/t.co\/GTgL6YEH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 21, 41 ],
        "url" : "http:\/\/t.co\/GTgL6YEH",
        "expanded_url" : "http:\/\/bit.ly\/MVmJ7P",
        "display_url" : "bit.ly\/MVmJ7P"
      } ]
    },
    "geo" : { },
    "id_str" : "220631677683236864",
    "text" : "Anyone want a piano? http:\/\/t.co\/GTgL6YEH",
    "id" : 220631677683236864,
    "created_at" : "2012-07-04 21:34:25 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 220632072937680899,
  "created_at" : "2012-07-04 21:35:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 85, 99 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220630081201115136",
  "text" : "I help SME make sense of Web Analytics data so they can serve their customer better. #irishbizparty",
  "id" : 220630081201115136,
  "created_at" : "2012-07-04 21:28:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elish Bul-Godley",
      "screen_name" : "ElishBulGodley",
      "indices" : [ 0, 15 ],
      "id_str" : "311397063",
      "id" : 311397063
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220622162288062465",
  "geo" : { },
  "id_str" : "220623751484669952",
  "in_reply_to_user_id" : 311397063,
  "text" : "@ElishBulGodley What it is about redline? thks",
  "id" : 220623751484669952,
  "in_reply_to_status_id" : 220622162288062465,
  "created_at" : "2012-07-04 21:02:56 +0000",
  "in_reply_to_screen_name" : "ElishBulGodley",
  "in_reply_to_user_id_str" : "311397063",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eddie McGuinness",
      "screen_name" : "eddiemcguinness",
      "indices" : [ 3, 19 ],
      "id_str" : "370114100",
      "id" : 370114100
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Broadband",
      "indices" : [ 73, 83 ]
    }, {
      "text" : "UPC",
      "indices" : [ 84, 88 ]
    } ],
    "urls" : [ {
      "indices" : [ 89, 109 ],
      "url" : "http:\/\/t.co\/HuXPyfat",
      "expanded_url" : "http:\/\/www.siliconrepublic.com\/business\/item\/28126-irelands-lost-12bn-inter",
      "display_url" : "siliconrepublic.com\/business\/item\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220599639311794177",
  "text" : "RT @eddiemcguinness: Ireland\u2019s lost \u2018\u20AC12bn internet economy\u2019 opportunity #Broadband #UPC http:\/\/t.co\/HuXPyfat",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Broadband",
        "indices" : [ 52, 62 ]
      }, {
        "text" : "UPC",
        "indices" : [ 63, 67 ]
      } ],
      "urls" : [ {
        "indices" : [ 68, 88 ],
        "url" : "http:\/\/t.co\/HuXPyfat",
        "expanded_url" : "http:\/\/www.siliconrepublic.com\/business\/item\/28126-irelands-lost-12bn-inter",
        "display_url" : "siliconrepublic.com\/business\/item\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "220588989130883072",
    "text" : "Ireland\u2019s lost \u2018\u20AC12bn internet economy\u2019 opportunity #Broadband #UPC http:\/\/t.co\/HuXPyfat",
    "id" : 220588989130883072,
    "created_at" : "2012-07-04 18:44:48 +0000",
    "user" : {
      "name" : "Eddie McGuinness",
      "screen_name" : "eddiemcguinness",
      "protected" : false,
      "id_str" : "370114100",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000544543972\/7f59db4aab97fd53eaf8374b3a8359c5_normal.jpeg",
      "id" : 370114100,
      "verified" : false
    }
  },
  "id" : 220599639311794177,
  "created_at" : "2012-07-04 19:27:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http:\/\/t.co\/l7l9J7uU",
      "expanded_url" : "http:\/\/sg.finance.yahoo.com\/news\/malaysian-team-buy-battersea-power-171441708.html",
      "display_url" : "sg.finance.yahoo.com\/news\/malaysian\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220597914202935296",
  "text" : "Malaysian team buy Battersea power station site. http:\/\/t.co\/l7l9J7uU",
  "id" : 220597914202935296,
  "created_at" : "2012-07-04 19:20:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220552483574001666",
  "text" : "4. No impact for me as I can still get project thru the internet. Income earned is ALL mine.",
  "id" : 220552483574001666,
  "created_at" : "2012-07-04 16:19:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220546720679997440",
  "text" : "3. If I got \u20AC300,000 laying around, I would have go for semi-retirements in Thailand where cost of living is low.",
  "id" : 220546720679997440,
  "created_at" : "2012-07-04 15:56:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220546307553640448",
  "text" : "2. \"You must have not less than \u20AC300,000 of your own money available to make an investment into business in Ireland\" hmmm...",
  "id" : 220546307553640448,
  "created_at" : "2012-07-04 15:55:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220546111558000640",
  "text" : "1. \"Non-EEA Nationals who wish to pursue a business activity must first obtain Business Permission.\" OK",
  "id" : 220546111558000640,
  "created_at" : "2012-07-04 15:54:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220538656216203265",
  "text" : "\"A copy of the certificate of registration for each business name must be exhibited in a conspicuous position.\" &lt; As a tailsman?",
  "id" : 220538656216203265,
  "created_at" : "2012-07-04 15:24:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dubchat",
      "indices" : [ 25, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220526818967564288",
  "text" : "RT @enormous: Don't miss #dubchat this evening hosted by the bould @PaddyComyn on the subject \"Getting Around Dublin\" - time to get your ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dubchat",
        "indices" : [ 11, 19 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "220525457974628352",
    "text" : "Don't miss #dubchat this evening hosted by the bould @PaddyComyn on the subject \"Getting Around Dublin\" - time to get your rant on! :)",
    "id" : 220525457974628352,
    "created_at" : "2012-07-04 14:32:21 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 220526818967564288,
  "created_at" : "2012-07-04 14:37:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tus Nua Designs",
      "screen_name" : "TusNuaDesigns",
      "indices" : [ 14, 28 ],
      "id_str" : "1372338342",
      "id" : 1372338342
    }, {
      "name" : "TheMarketingShop.ie",
      "screen_name" : "marketingdebbie",
      "indices" : [ 29, 45 ],
      "id_str" : "114487731",
      "id" : 114487731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220522746885570560",
  "text" : "@stuartmeharg @TusNuaDesigns @marketingdebbie Thanks a million.",
  "id" : 220522746885570560,
  "created_at" : "2012-07-04 14:21:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Ariely",
      "screen_name" : "danariely",
      "indices" : [ 3, 13 ],
      "id_str" : "17997789",
      "id" : 17997789
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220518291918168064",
  "text" : "RT @danariely: Bankers just don't feel bad anymore when they F&amp;%# their customers. For them it is like downloading illegal content.  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 121, 141 ],
        "url" : "http:\/\/t.co\/pfAJb1mN",
        "expanded_url" : "http:\/\/nyti.ms\/LQNKYi",
        "display_url" : "nyti.ms\/LQNKYi"
      } ]
    },
    "geo" : { },
    "id_str" : "220518095368888320",
    "text" : "Bankers just don't feel bad anymore when they F&amp;%# their customers. For them it is like downloading illegal content. http:\/\/t.co\/pfAJb1mN",
    "id" : 220518095368888320,
    "created_at" : "2012-07-04 14:03:05 +0000",
    "user" : {
      "name" : "Dan Ariely",
      "screen_name" : "danariely",
      "protected" : false,
      "id_str" : "17997789",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3079991122\/885ba5efe97fcfd916001b8374d0d75c_normal.jpeg",
      "id" : 17997789,
      "verified" : true
    }
  },
  "id" : 220518291918168064,
  "created_at" : "2012-07-04 14:03:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220518131813203968",
  "text" : "Is Facebook down?",
  "id" : 220518131813203968,
  "created_at" : "2012-07-04 14:03:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheMarketingShop.ie",
      "screen_name" : "marketingdebbie",
      "indices" : [ 0, 16 ],
      "id_str" : "114487731",
      "id" : 114487731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220512358622179328",
  "geo" : { },
  "id_str" : "220514633071603713",
  "in_reply_to_user_id" : 114487731,
  "text" : "@marketingdebbie Thks. What about a personal bank account?",
  "id" : 220514633071603713,
  "in_reply_to_status_id" : 220512358622179328,
  "created_at" : "2012-07-04 13:49:20 +0000",
  "in_reply_to_screen_name" : "marketingdebbie",
  "in_reply_to_user_id_str" : "114487731",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 79, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220509126336315393",
  "text" : "It is a legal requirement to have a biz bank account if you are a sole trader? #SMEcommunity Thks",
  "id" : 220509126336315393,
  "created_at" : "2012-07-04 13:27:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220492193637470208",
  "text" : "It takes 5 days for domain transfer to be completed...it just 1 and 0 right and what taking so long?",
  "id" : 220492193637470208,
  "created_at" : "2012-07-04 12:20:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 90 ],
      "url" : "http:\/\/t.co\/zcBQ5ps8",
      "expanded_url" : "http:\/\/www.thejournal.ie\/photoireland-festival-powerful-images-explore-migration-and-diaspora-507080-Jul2012",
      "display_url" : "thejournal.ie\/photoireland-f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220320497966133250",
  "text" : "PhotoIreland Festival: Powerful images explore migration and diaspora http:\/\/t.co\/zcBQ5ps8",
  "id" : 220320497966133250,
  "created_at" : "2012-07-04 00:57:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "indices" : [ 3, 13 ],
      "id_str" : "17524108",
      "id" : 17524108
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 93 ],
      "url" : "http:\/\/t.co\/Awb2eQpC",
      "expanded_url" : "http:\/\/bit.ly\/N7b2ds",
      "display_url" : "bit.ly\/N7b2ds"
    } ]
  },
  "geo" : { },
  "id_str" : "220286989293993985",
  "text" : "RT @ericknopf: Mozilla Shows Off Junior, a Simple Browser Built for iPad http:\/\/t.co\/Awb2eQpC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 78 ],
        "url" : "http:\/\/t.co\/Awb2eQpC",
        "expanded_url" : "http:\/\/bit.ly\/N7b2ds",
        "display_url" : "bit.ly\/N7b2ds"
      } ]
    },
    "geo" : { },
    "id_str" : "220283293042618370",
    "text" : "Mozilla Shows Off Junior, a Simple Browser Built for iPad http:\/\/t.co\/Awb2eQpC",
    "id" : 220283293042618370,
    "created_at" : "2012-07-03 22:30:04 +0000",
    "user" : {
      "name" : "Eric Knopf",
      "screen_name" : "ericknopf",
      "protected" : false,
      "id_str" : "17524108",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460579737018970113\/3x_hi1Zc_normal.jpeg",
      "id" : 17524108,
      "verified" : false
    }
  },
  "id" : 220286989293993985,
  "created_at" : "2012-07-03 22:44:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mullens for Yes",
      "screen_name" : "deirdremullen",
      "indices" : [ 3, 17 ],
      "id_str" : "106248025",
      "id" : 106248025
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220276724724805635",
  "text" : "RT @deirdremullen: Another thing Centra needs to do, apart the whole Children's Allowance shambles today, is teach their staff to spell! ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/deirdremullen\/status\/220274278296330240\/photo\/1",
        "indices" : [ 118, 138 ],
        "url" : "http:\/\/t.co\/OlUlKjdL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Aw6SSf9CIAEQ7_J.jpg",
        "id_str" : "220274278300524545",
        "id" : 220274278300524545,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Aw6SSf9CIAEQ7_J.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 453
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/OlUlKjdL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "220274278296330240",
    "text" : "Another thing Centra needs to do, apart the whole Children's Allowance shambles today, is teach their staff to spell! http:\/\/t.co\/OlUlKjdL",
    "id" : 220274278296330240,
    "created_at" : "2012-07-03 21:54:15 +0000",
    "user" : {
      "name" : "Mullens for Yes",
      "screen_name" : "deirdremullen",
      "protected" : false,
      "id_str" : "106248025",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/983720292508811266\/swBZAY_R_normal.jpg",
      "id" : 106248025,
      "verified" : false
    }
  },
  "id" : 220276724724805635,
  "created_at" : "2012-07-03 22:03:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220260561206775808",
  "text" : "Would love to hear from someone here who chose to leave to work &amp; stay overseas cos is cool to do so.",
  "id" : 220260561206775808,
  "created_at" : "2012-07-03 20:59:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 54 ],
      "url" : "http:\/\/t.co\/FksaNml8",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b01kg7ky",
      "display_url" : "bbc.co.uk\/programmes\/b01\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220248737199554560",
  "text" : "Ireland, Lost and Leaving on BBC3 http:\/\/t.co\/FksaNml8",
  "id" : 220248737199554560,
  "created_at" : "2012-07-03 20:12:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anne Marie O'Connor",
      "screen_name" : "Kitsgirl1",
      "indices" : [ 3, 13 ],
      "id_str" : "20962402",
      "id" : 20962402
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishshadesofgrey",
      "indices" : [ 115, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220224599856644096",
  "text" : "RT @Kitsgirl1: 'Give it to me, give it to me', he roared aggressively. Some days Mary hated working at Ulster Bank #irishshadesofgrey",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "irishshadesofgrey",
        "indices" : [ 100, 118 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "220169348600434688",
    "text" : "'Give it to me, give it to me', he roared aggressively. Some days Mary hated working at Ulster Bank #irishshadesofgrey",
    "id" : 220169348600434688,
    "created_at" : "2012-07-03 14:57:17 +0000",
    "user" : {
      "name" : "Anne Marie O'Connor",
      "screen_name" : "Kitsgirl1",
      "protected" : false,
      "id_str" : "20962402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/603268952177942528\/JcFGtK-n_normal.jpg",
      "id" : 20962402,
      "verified" : false
    }
  },
  "id" : 220224599856644096,
  "created_at" : "2012-07-03 18:36:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Se\u00E1n Kearns",
      "screen_name" : "kearnsey100",
      "indices" : [ 3, 15 ],
      "id_str" : "255536654",
      "id" : 255536654
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishshadesofgrey",
      "indices" : [ 111, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220224018765185024",
  "text" : "RT @kearnsey100: She looked into his eyes and said 'Take me, take me'......he said 'Im sorry, the bus is full' #irishshadesofgrey",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "irishshadesofgrey",
        "indices" : [ 94, 112 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "220203269815930880",
    "text" : "She looked into his eyes and said 'Take me, take me'......he said 'Im sorry, the bus is full' #irishshadesofgrey",
    "id" : 220203269815930880,
    "created_at" : "2012-07-03 17:12:05 +0000",
    "user" : {
      "name" : "Se\u00E1n Kearns",
      "screen_name" : "kearnsey100",
      "protected" : false,
      "id_str" : "255536654",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/555028469575602176\/bipZNFRi_normal.jpeg",
      "id" : 255536654,
      "verified" : false
    }
  },
  "id" : 220224018765185024,
  "created_at" : "2012-07-03 18:34:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Niamh Redmond",
      "screen_name" : "nredmond",
      "indices" : [ 3, 12 ],
      "id_str" : "8724262",
      "id" : 8724262
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 127 ],
      "url" : "http:\/\/t.co\/7FATyijb",
      "expanded_url" : "http:\/\/lnkd.in\/-uyu6W",
      "display_url" : "lnkd.in\/-uyu6W"
    } ]
  },
  "geo" : { },
  "id_str" : "220223432313421824",
  "text" : "RT @nredmond: Tr\u00F3caire are looking for an Online Manager\/ Coordinator in the Dublin\/Kildare area, Ireland: http:\/\/t.co\/7FATyijb *Message ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 113 ],
        "url" : "http:\/\/t.co\/7FATyijb",
        "expanded_url" : "http:\/\/lnkd.in\/-uyu6W",
        "display_url" : "lnkd.in\/-uyu6W"
      } ]
    },
    "geo" : { },
    "id_str" : "220209490493050880",
    "text" : "Tr\u00F3caire are looking for an Online Manager\/ Coordinator in the Dublin\/Kildare area, Ireland: http:\/\/t.co\/7FATyijb *Message me for an intro\u2026",
    "id" : 220209490493050880,
    "created_at" : "2012-07-03 17:36:48 +0000",
    "user" : {
      "name" : "Niamh Redmond",
      "screen_name" : "nredmond",
      "protected" : false,
      "id_str" : "8724262",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/985669828382896128\/KnzyBdVG_normal.jpg",
      "id" : 8724262,
      "verified" : false
    }
  },
  "id" : 220223432313421824,
  "created_at" : "2012-07-03 18:32:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 84, 97 ]
    }, {
      "text" : "Irebiz",
      "indices" : [ 98, 105 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220200103275802624",
  "text" : "Need help to use Google Analytics to calculate customer lifetime value? I can help. #SMEcommunity #Irebiz",
  "id" : 220200103275802624,
  "created_at" : "2012-07-03 16:59:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220190763588390913",
  "text" : "For the first half of 2012, I meet more people than whole of 2011.",
  "id" : 220190763588390913,
  "created_at" : "2012-07-03 16:22:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RaboDirect",
      "screen_name" : "RaboDirectIE",
      "indices" : [ 3, 16 ],
      "id_str" : "38183040",
      "id" : 38183040
    }, {
      "name" : "Tina Leonard",
      "screen_name" : "TinaLConsumer",
      "indices" : [ 99, 113 ],
      "id_str" : "385541825",
      "id" : 385541825
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "savingisbelieving",
      "indices" : [ 114, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220171911911587840",
  "text" : "RT @RaboDirectIE: Don\u2019t spend more than you have to. Here\u2019s 6 smart saving tips for your hols from @TinaLconsumer #savingisbelieving htt ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tina Leonard",
        "screen_name" : "TinaLConsumer",
        "indices" : [ 81, 95 ],
        "id_str" : "385541825",
        "id" : 385541825
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "savingisbelieving",
        "indices" : [ 96, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 135 ],
        "url" : "http:\/\/t.co\/ZIXESwoM",
        "expanded_url" : "http:\/\/ow.ly\/bYSBB",
        "display_url" : "ow.ly\/bYSBB"
      } ]
    },
    "geo" : { },
    "id_str" : "220136151388921856",
    "text" : "Don\u2019t spend more than you have to. Here\u2019s 6 smart saving tips for your hols from @TinaLconsumer #savingisbelieving http:\/\/t.co\/ZIXESwoM",
    "id" : 220136151388921856,
    "created_at" : "2012-07-03 12:45:23 +0000",
    "user" : {
      "name" : "RaboDirect",
      "screen_name" : "RaboDirectIE",
      "protected" : false,
      "id_str" : "38183040",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/623422164050513920\/uSfGWKCl_normal.png",
      "id" : 38183040,
      "verified" : true
    }
  },
  "id" : 220171911911587840,
  "created_at" : "2012-07-03 15:07:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mustafa khalili",
      "screen_name" : "muskhalili",
      "indices" : [ 3, 14 ],
      "id_str" : "181523152",
      "id" : 181523152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220135613922414592",
  "text" : "RT @muskhalili: Japanese visitors to Scotland told be wary of calling locals English and steer clear of council estates\nhttp:\/\/t.co\/ncmq ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Guardian",
        "screen_name" : "guardian",
        "indices" : [ 129, 138 ],
        "id_str" : "87818409",
        "id" : 87818409
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 104, 124 ],
        "url" : "http:\/\/t.co\/ncmqb7Ni",
        "expanded_url" : "http:\/\/gu.com\/p\/38mea",
        "display_url" : "gu.com\/p\/38mea"
      } ]
    },
    "geo" : { },
    "id_str" : "220130259343904768",
    "text" : "Japanese visitors to Scotland told be wary of calling locals English and steer clear of council estates\nhttp:\/\/t.co\/ncmqb7Ni\nvia @guardian",
    "id" : 220130259343904768,
    "created_at" : "2012-07-03 12:21:58 +0000",
    "user" : {
      "name" : "mustafa khalili",
      "screen_name" : "muskhalili",
      "protected" : false,
      "id_str" : "181523152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/584380877100187649\/1iI3VBJL_normal.jpg",
      "id" : 181523152,
      "verified" : true
    }
  },
  "id" : 220135613922414592,
  "created_at" : "2012-07-03 12:43:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 0, 8 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "220104068465569793",
  "geo" : { },
  "id_str" : "220104775549714432",
  "in_reply_to_user_id" : 574253,
  "text" : "@mrbrown At the same subscription rates with the cap from 12 to 2GB?",
  "id" : 220104775549714432,
  "in_reply_to_status_id" : 220104068465569793,
  "created_at" : "2012-07-03 10:40:42 +0000",
  "in_reply_to_screen_name" : "mrbrown",
  "in_reply_to_user_id_str" : "574253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220074996620476417",
  "text" : "Think you can do better? Go easy on the politicians sometimes it ok to fail. Just move on and not repeat the same mistakes again.",
  "id" : 220074996620476417,
  "created_at" : "2012-07-03 08:42:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 118, 131 ]
    }, {
      "text" : "Irebiz",
      "indices" : [ 132, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http:\/\/t.co\/RL6vmLWD",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/patrickspenner\/2012\/07\/02\/marketers-have-it-wrong-forget-engagement-consumers-want-simplicity\/",
      "display_url" : "forbes.com\/sites\/patricks\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220072499222818816",
  "text" : "Forget about engagement, Things that are easy to mentally process tend to be more well received. http:\/\/t.co\/RL6vmLWD #SMEcommunity #Irebiz",
  "id" : 220072499222818816,
  "created_at" : "2012-07-03 08:32:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http:\/\/t.co\/RL6vmLWD",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/patrickspenner\/2012\/07\/02\/marketers-have-it-wrong-forget-engagement-consumers-want-simplicity\/",
      "display_url" : "forbes.com\/sites\/patricks\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "220070547227295745",
  "text" : "Marketer forget about \"engagement\" http:\/\/t.co\/RL6vmLWD",
  "id" : 220070547227295745,
  "created_at" : "2012-07-03 08:24:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Cooke",
      "screen_name" : "A_Cooke_",
      "indices" : [ 3, 12 ],
      "id_str" : "3062426607",
      "id" : 3062426607
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http:\/\/t.co\/qH6pmk1r",
      "expanded_url" : "http:\/\/bit.ly\/M2xvsc",
      "display_url" : "bit.ly\/M2xvsc"
    } ]
  },
  "geo" : { },
  "id_str" : "220068678924910592",
  "text" : "RT @A_Cooke_: Maxus are looking for a Digital Business Director in Japan \u5916\u8CC7\u7CFB\u30E1\u30C7\u30A3\u30A2\u5927\u624B\u4F1A\u793E\u304C\u30DE\u30FC\u30B1\u30C6\u30A3\u30F3\u30B0\u90E8\u9577\u3092\u52DF\u96C6\u3057\u3066\u3044\u307E\u3059\u306E\u3067\u3001\u6C17\u697D\u306BTWEET\u3057\u3066\u306Dhttp:\/\/t.co\/qH6pmk1r ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "\u30DE\u30FC\u30B1\u30C6\u30A3\u30F3\u30B0",
        "indices" : [ 124, 132 ]
      }, {
        "text" : "\u8EE2\u8077",
        "indices" : [ 133, 136 ]
      }, {
        "text" : "\u6C42\u4EBA",
        "indices" : [ 137, 140 ]
      } ],
      "urls" : [ {
        "indices" : [ 102, 122 ],
        "url" : "http:\/\/t.co\/qH6pmk1r",
        "expanded_url" : "http:\/\/bit.ly\/M2xvsc",
        "display_url" : "bit.ly\/M2xvsc"
      } ]
    },
    "geo" : { },
    "id_str" : "220068191756496897",
    "text" : "Maxus are looking for a Digital Business Director in Japan \u5916\u8CC7\u7CFB\u30E1\u30C7\u30A3\u30A2\u5927\u624B\u4F1A\u793E\u304C\u30DE\u30FC\u30B1\u30C6\u30A3\u30F3\u30B0\u90E8\u9577\u3092\u52DF\u96C6\u3057\u3066\u3044\u307E\u3059\u306E\u3067\u3001\u6C17\u697D\u306BTWEET\u3057\u3066\u306Dhttp:\/\/t.co\/qH6pmk1r  #\u30DE\u30FC\u30B1\u30C6\u30A3\u30F3\u30B0 #\u8EE2\u8077\u3000#\u6C42\u4EBA",
    "id" : 220068191756496897,
    "created_at" : "2012-07-03 08:15:20 +0000",
    "user" : {
      "name" : "Adam Cooke",
      "screen_name" : "MusubiGroup",
      "protected" : false,
      "id_str" : "223424879",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849576191652540416\/jjqmG64w_normal.jpg",
      "id" : 223424879,
      "verified" : false
    }
  },
  "id" : 220068678924910592,
  "created_at" : "2012-07-03 08:17:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "220068102442987520",
  "text" : "Seems that coffee in the morning is driving me to embrace the toilet bowl...",
  "id" : 220068102442987520,
  "created_at" : "2012-07-03 08:14:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219876671182999552",
  "text" : "Any domain name registrar you can recommend?",
  "id" : 219876671182999552,
  "created_at" : "2012-07-02 19:34:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 52, 65 ]
    } ],
    "urls" : [ {
      "indices" : [ 31, 51 ],
      "url" : "http:\/\/t.co\/2ZPzU5xt",
      "expanded_url" : "http:\/\/www.practicalecommerce.com\/articles\/2374-6-Reasons-to-Use-URL-Redirects",
      "display_url" : "practicalecommerce.com\/articles\/2374-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "219816390981464065",
  "text" : "6 Reasons to Use URL Redirects http:\/\/t.co\/2ZPzU5xt #SMEcommunity",
  "id" : 219816390981464065,
  "created_at" : "2012-07-02 15:34:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219764076795019265",
  "text" : "One way to make money in Singapore is set up &amp; run a charity. No \"foreign talent\" needed.",
  "id" : 219764076795019265,
  "created_at" : "2012-07-02 12:06:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/219758061793984512\/photo\/1",
      "indices" : [ 70, 90 ],
      "url" : "http:\/\/t.co\/tEK3lmF1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Awy8yvoCQAA684G.png",
      "id_str" : "219758061798178816",
      "id" : 219758061798178816,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Awy8yvoCQAA684G.png",
      "sizes" : [ {
        "h" : 437,
        "resize" : "fit",
        "w" : 336
      }, {
        "h" : 437,
        "resize" : "fit",
        "w" : 336
      }, {
        "h" : 437,
        "resize" : "fit",
        "w" : 336
      }, {
        "h" : 437,
        "resize" : "fit",
        "w" : 336
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tEK3lmF1"
    } ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 48, 61 ]
    }, {
      "text" : "Irebiz",
      "indices" : [ 62, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219758061793984512",
  "text" : "Cover for my \"book\" Like it? Please RT. Thanks. #SMEcommunity #Irebiz http:\/\/t.co\/tEK3lmF1",
  "id" : 219758061793984512,
  "created_at" : "2012-07-02 11:42:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "bizachievers",
      "screen_name" : "smallbc",
      "indices" : [ 3, 11 ],
      "id_str" : "852116383810957313",
      "id" : 852116383810957313
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/aXCXUbIF",
      "expanded_url" : "http:\/\/fb.me\/1d4pBugX5",
      "display_url" : "fb.me\/1d4pBugX5"
    } ]
  },
  "geo" : { },
  "id_str" : "219741753006227457",
  "text" : "RT @SmallBC: Why Asia really matters to Irish SMEs http:\/\/t.co\/aXCXUbIF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 38, 58 ],
        "url" : "http:\/\/t.co\/aXCXUbIF",
        "expanded_url" : "http:\/\/fb.me\/1d4pBugX5",
        "display_url" : "fb.me\/1d4pBugX5"
      } ]
    },
    "geo" : { },
    "id_str" : "219739331726815232",
    "text" : "Why Asia really matters to Irish SMEs http:\/\/t.co\/aXCXUbIF",
    "id" : 219739331726815232,
    "created_at" : "2012-07-02 10:28:33 +0000",
    "user" : {
      "name" : "Business Achievers",
      "screen_name" : "bizachievers",
      "protected" : false,
      "id_str" : "39465286",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/895212459836964864\/3sAhbm30_normal.jpg",
      "id" : 39465286,
      "verified" : false
    }
  },
  "id" : 219741753006227457,
  "created_at" : "2012-07-02 10:38:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219464499558752256",
  "text" : "Any activity for kids in summer where parents can drop them off just for a few hrs?",
  "id" : 219464499558752256,
  "created_at" : "2012-07-01 16:16:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Light House Cinema",
      "screen_name" : "LightHouseD7",
      "indices" : [ 3, 16 ],
      "id_str" : "65338464",
      "id" : 65338464
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219434666984345600",
  "text" : "RT @LightHouseD7: Today's your last chance to win Point Break tickets.  Photoshop Sad Keanu in Dublin and tweet it at us. Get Keanu here ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 139 ],
        "url" : "http:\/\/t.co\/YSqHCU7o",
        "expanded_url" : "http:\/\/bit.ly\/OFPWIh",
        "display_url" : "bit.ly\/OFPWIh"
      } ]
    },
    "geo" : { },
    "id_str" : "219422140947120128",
    "text" : "Today's your last chance to win Point Break tickets.  Photoshop Sad Keanu in Dublin and tweet it at us. Get Keanu here http:\/\/t.co\/YSqHCU7o",
    "id" : 219422140947120128,
    "created_at" : "2012-07-01 13:28:09 +0000",
    "user" : {
      "name" : "Light House Cinema",
      "screen_name" : "LightHouseD7",
      "protected" : false,
      "id_str" : "65338464",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/498783859165507584\/dFcAFHDB_normal.jpeg",
      "id" : 65338464,
      "verified" : false
    }
  },
  "id" : 219434666984345600,
  "created_at" : "2012-07-01 14:17:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "@jdrive",
      "screen_name" : "jdrive",
      "indices" : [ 3, 10 ],
      "id_str" : "15137329",
      "id" : 15137329
    }, {
      "name" : "SFGate",
      "screen_name" : "SFGate",
      "indices" : [ 89, 96 ],
      "id_str" : "36511031",
      "id" : 36511031
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http:\/\/t.co\/bJSb7YdM",
      "expanded_url" : "http:\/\/www.sfgate.com\/business\/article\/Manufacturer-Quirky-finds-China-not-always-3675966.php?cmpid=twitter",
      "display_url" : "sfgate.com\/business\/artic\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "219358888561098752",
  "text" : "RT @jdrive: Manufacturer Quirky finds China not always cheapest http:\/\/t.co\/bJSb7YdM via @SFGate",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SFGate",
        "screen_name" : "SFGate",
        "indices" : [ 77, 84 ],
        "id_str" : "36511031",
        "id" : 36511031
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 52, 72 ],
        "url" : "http:\/\/t.co\/bJSb7YdM",
        "expanded_url" : "http:\/\/www.sfgate.com\/business\/article\/Manufacturer-Quirky-finds-China-not-always-3675966.php?cmpid=twitter",
        "display_url" : "sfgate.com\/business\/artic\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "219187552287072257",
    "text" : "Manufacturer Quirky finds China not always cheapest http:\/\/t.co\/bJSb7YdM via @SFGate",
    "id" : 219187552287072257,
    "created_at" : "2012-06-30 21:55:59 +0000",
    "user" : {
      "name" : "@jdrive",
      "screen_name" : "jdrive",
      "protected" : false,
      "id_str" : "15137329",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914007073679134720\/JYcl2zf__normal.jpg",
      "id" : 15137329,
      "verified" : false
    }
  },
  "id" : 219358888561098752,
  "created_at" : "2012-07-01 09:16:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219323210561363969",
  "text" : "Qn: It is a good idea to introduce related idea on the same landingpage?",
  "id" : 219323210561363969,
  "created_at" : "2012-07-01 06:55:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 70 ],
      "url" : "http:\/\/t.co\/4rxN8vxA",
      "expanded_url" : "http:\/\/wp.smashingmagazine.com\/2012\/06\/28\/create-responsive-mobile-first-wordpress-theme\/",
      "display_url" : "wp.smashingmagazine.com\/2012\/06\/28\/cre\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "219301776925528064",
  "text" : "Create A Responsive, Mobile-First WordPress Theme\nhttp:\/\/t.co\/4rxN8vxA",
  "id" : 219301776925528064,
  "created_at" : "2012-07-01 05:29:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "beki",
      "screen_name" : "smocktofrock",
      "indices" : [ 3, 16 ],
      "id_str" : "989466717087764481",
      "id" : 989466717087764481
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Manchester",
      "indices" : [ 55, 66 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "219291643621277698",
  "text" : "RT @smocktofrock: Finally one guy manages to walk down #Manchester Market Street without getting hounded by promoters (Funny) http:\/\/t.c ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Manchester",
        "indices" : [ 37, 48 ]
      } ],
      "urls" : [ {
        "indices" : [ 108, 128 ],
        "url" : "http:\/\/t.co\/UnM0oAqL",
        "expanded_url" : "http:\/\/newsmanc.co.uk\/2012\/02\/08\/news-market-street-gauntlet-finally-completed\/",
        "display_url" : "newsmanc.co.uk\/2012\/02\/08\/new\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "219123818554339328",
    "text" : "Finally one guy manages to walk down #Manchester Market Street without getting hounded by promoters (Funny) http:\/\/t.co\/UnM0oAqL",
    "id" : 219123818554339328,
    "created_at" : "2012-06-30 17:42:44 +0000",
    "user" : {
      "name" : "Beki Smith",
      "screen_name" : "SlurpSocial",
      "protected" : false,
      "id_str" : "98222717",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996007468944842752\/MWOxDUrH_normal.jpg",
      "id" : 98222717,
      "verified" : false
    }
  },
  "id" : 219291643621277698,
  "created_at" : "2012-07-01 04:49:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]