Grailbird.data.tweets_2016_11 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "804097215274897415",
  "text" : "RT @pdscott: Amazon\u2019s image recognition AI can identify your dog's breed - and Ireland is one of the few places that can test it https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/aFwqrMzGYN",
        "expanded_url" : "http:\/\/www.theverge.com\/2016\/11\/30\/13799582\/amazon-rekognition-machine-learning-image-processing",
        "display_url" : "theverge.com\/2016\/11\/30\/137\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "804095119481851904",
    "text" : "Amazon\u2019s image recognition AI can identify your dog's breed - and Ireland is one of the few places that can test it https:\/\/t.co\/aFwqrMzGYN?",
    "id" : 804095119481851904,
    "created_at" : "2016-11-30 22:49:50 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 804097215274897415,
  "created_at" : "2016-11-30 22:58:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "804095839924879361",
  "text" : "Learnt on the grapevine that nurses are in demand that there plan to hire foreigners again cos local don't stay in the job long enough.",
  "id" : 804095839924879361,
  "created_at" : "2016-11-30 22:52:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sebastian Ruder",
      "screen_name" : "seb_ruder",
      "indices" : [ 3, 13 ],
      "id_str" : "2785337469",
      "id" : 2785337469
    }, {
      "name" : "TensorFlow",
      "screen_name" : "TensorFlow",
      "indices" : [ 119, 130 ],
      "id_str" : "254107028",
      "id" : 254107028
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "804065535893897216",
  "text" : "RT @seb_ruder: Tensorboard can now visualize embeddings. Inspecting your model just got a lot more visually appealing! @tensorflow https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TensorFlow",
        "screen_name" : "TensorFlow",
        "indices" : [ 104, 115 ],
        "id_str" : "254107028",
        "id" : 254107028
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/GAgKWYCgvr",
        "expanded_url" : "https:\/\/www.tensorflow.org\/versions\/master\/how_tos\/embedding_viz\/index.html#tensorboard-embedding-visualization",
        "display_url" : "tensorflow.org\/versions\/maste\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "803729474235691009",
    "text" : "Tensorboard can now visualize embeddings. Inspecting your model just got a lot more visually appealing! @tensorflow https:\/\/t.co\/GAgKWYCgvr",
    "id" : 803729474235691009,
    "created_at" : "2016-11-29 22:36:53 +0000",
    "user" : {
      "name" : "Sebastian Ruder",
      "screen_name" : "seb_ruder",
      "protected" : false,
      "id_str" : "2785337469",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/662962892057219072\/LkbhMXhF_normal.png",
      "id" : 2785337469,
      "verified" : false
    }
  },
  "id" : 804065535893897216,
  "created_at" : "2016-11-30 20:52:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Amm",
      "screen_name" : "changeagents_",
      "indices" : [ 3, 17 ],
      "id_str" : "91827895",
      "id" : 91827895
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/changeagents_\/status\/803983856709804033\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/z2LGhUBylO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CyhTDmYXcAIg9zY.jpg",
      "id_str" : "803983854793093122",
      "id" : 803983854793093122,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyhTDmYXcAIg9zY.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 570,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 588,
        "resize" : "fit",
        "w" : 702
      }, {
        "h" : 588,
        "resize" : "fit",
        "w" : 702
      }, {
        "h" : 588,
        "resize" : "fit",
        "w" : 702
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/z2LGhUBylO"
    } ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 28, 36 ]
    }, {
      "text" : "generosity",
      "indices" : [ 54, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "804006563174379522",
  "text" : "RT @changeagents_: How does #Ireland rank in terms of #generosity? Maybe not as well as we might think.. https:\/\/t.co\/z2LGhUBylO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/changeagents_\/status\/803983856709804033\/photo\/1",
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/z2LGhUBylO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CyhTDmYXcAIg9zY.jpg",
        "id_str" : "803983854793093122",
        "id" : 803983854793093122,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyhTDmYXcAIg9zY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 570,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 588,
          "resize" : "fit",
          "w" : 702
        }, {
          "h" : 588,
          "resize" : "fit",
          "w" : 702
        }, {
          "h" : 588,
          "resize" : "fit",
          "w" : 702
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/z2LGhUBylO"
      } ],
      "hashtags" : [ {
        "text" : "Ireland",
        "indices" : [ 9, 17 ]
      }, {
        "text" : "generosity",
        "indices" : [ 35, 46 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "803983856709804033",
    "text" : "How does #Ireland rank in terms of #generosity? Maybe not as well as we might think.. https:\/\/t.co\/z2LGhUBylO",
    "id" : 803983856709804033,
    "created_at" : "2016-11-30 15:27:43 +0000",
    "user" : {
      "name" : "Jonathan Amm",
      "screen_name" : "changeagents_",
      "protected" : false,
      "id_str" : "91827895",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/718044359325847552\/DsDLDnKF_normal.jpg",
      "id" : 91827895,
      "verified" : false
    }
  },
  "id" : 804006563174379522,
  "created_at" : "2016-11-30 16:57:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/o63aGpvDSM",
      "expanded_url" : "https:\/\/twitter.com\/IrelandUnlocked\/status\/804000528111910912",
      "display_url" : "twitter.com\/IrelandUnlocke\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "804006197217161218",
  "text" : "Thinking how many lives are destroyed or created because of these drinks. https:\/\/t.co\/o63aGpvDSM",
  "id" : 804006197217161218,
  "created_at" : "2016-11-30 16:56:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tim Fish",
      "screen_name" : "sweeneygov",
      "indices" : [ 3, 14 ],
      "id_str" : "850805922",
      "id" : 850805922
    }, {
      "name" : "Financial Times",
      "screen_name" : "FT",
      "indices" : [ 25, 28 ],
      "id_str" : "18949452",
      "id" : 18949452
    }, {
      "name" : "Gordon Arthur",
      "screen_name" : "combatpaparazzi",
      "indices" : [ 105, 121 ],
      "id_str" : "2955776982",
      "id" : 2955776982
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/W01ynOohzD",
      "expanded_url" : "https:\/\/www.ft.com\/content\/1a0fbcd0-b2e5-11e6-9c37-5787335499a0",
      "display_url" : "ft.com\/content\/1a0fbc\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "803951896855777284",
  "text" : "RT @sweeneygov: Story in @FT China seizing Singapore Terrex vehicles https:\/\/t.co\/W01ynOohzD or read the @combatpaparazzi story here https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Financial Times",
        "screen_name" : "FT",
        "indices" : [ 9, 12 ],
        "id_str" : "18949452",
        "id" : 18949452
      }, {
        "name" : "Gordon Arthur",
        "screen_name" : "combatpaparazzi",
        "indices" : [ 89, 105 ],
        "id_str" : "2955776982",
        "id" : 2955776982
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sweeneygov\/status\/803887392457420800\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/PANnOyC6bx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cyf7TsPXcAImwG6.jpg",
        "id_str" : "803887374220619778",
        "id" : 803887374220619778,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cyf7TsPXcAImwG6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 420,
          "resize" : "fit",
          "w" : 630
        }, {
          "h" : 420,
          "resize" : "fit",
          "w" : 630
        }, {
          "h" : 420,
          "resize" : "fit",
          "w" : 630
        }, {
          "h" : 420,
          "resize" : "fit",
          "w" : 630
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PANnOyC6bx"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/W01ynOohzD",
        "expanded_url" : "https:\/\/www.ft.com\/content\/1a0fbcd0-b2e5-11e6-9c37-5787335499a0",
        "display_url" : "ft.com\/content\/1a0fbc\u2026"
      }, {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/SmBuVKtzkV",
        "expanded_url" : "https:\/\/www.shephardmedia.com\/news\/defence-notes\/terrexgate-how-defence-and-politics-intersect\/",
        "display_url" : "shephardmedia.com\/news\/defence-n\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "803887392457420800",
    "text" : "Story in @FT China seizing Singapore Terrex vehicles https:\/\/t.co\/W01ynOohzD or read the @combatpaparazzi story here https:\/\/t.co\/SmBuVKtzkV https:\/\/t.co\/PANnOyC6bx",
    "id" : 803887392457420800,
    "created_at" : "2016-11-30 09:04:24 +0000",
    "user" : {
      "name" : "Tim Fish",
      "screen_name" : "sweeneygov",
      "protected" : false,
      "id_str" : "850805922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/555314471854280706\/B83gmAuf_normal.jpeg",
      "id" : 850805922,
      "verified" : false
    }
  },
  "id" : 803951896855777284,
  "created_at" : "2016-11-30 13:20:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "indices" : [ 3, 12 ],
      "id_str" : "875971",
      "id" : 875971
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/QpfBlbBedZ",
      "expanded_url" : "https:\/\/twitter.com\/sphasiaone\/status\/803807169267331076",
      "display_url" : "twitter.com\/sphasiaone\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "803869907830079488",
  "text" : "RT @Rubenerd: And incredible pressure to go with it. Knew more than a few locals who were close to suicidal : (. https:\/\/t.co\/QpfBlbBedZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/QpfBlbBedZ",
        "expanded_url" : "https:\/\/twitter.com\/sphasiaone\/status\/803807169267331076",
        "display_url" : "twitter.com\/sphasiaone\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "803807345994178560",
    "text" : "And incredible pressure to go with it. Knew more than a few locals who were close to suicidal : (. https:\/\/t.co\/QpfBlbBedZ",
    "id" : 803807345994178560,
    "created_at" : "2016-11-30 03:46:19 +0000",
    "user" : {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "protected" : false,
      "id_str" : "875971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011136325364301827\/hmrtp1V1_normal.jpg",
      "id" : 875971,
      "verified" : false
    }
  },
  "id" : 803869907830079488,
  "created_at" : "2016-11-30 07:54:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 0, 10 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "803190317960822784",
  "geo" : { },
  "id_str" : "803653411732230144",
  "in_reply_to_user_id" : 18849187,
  "text" : "@barryhand Thanks. I decided to install own copy of MS Office 2007.",
  "id" : 803653411732230144,
  "in_reply_to_status_id" : 803190317960822784,
  "created_at" : "2016-11-29 17:34:39 +0000",
  "in_reply_to_screen_name" : "barryhand",
  "in_reply_to_user_id_str" : "18849187",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/rj1xSubqf1",
      "expanded_url" : "https:\/\/twitter.com\/BeckyLehmann\/status\/803622096458813442",
      "display_url" : "twitter.com\/BeckyLehmann\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "803650238594445312",
  "text" : "cc @Clearpreso https:\/\/t.co\/rj1xSubqf1",
  "id" : 803650238594445312,
  "created_at" : "2016-11-29 17:22:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vicky Harp",
      "screen_name" : "vickyharp",
      "indices" : [ 3, 13 ],
      "id_str" : "254222101",
      "id" : 254222101
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803649041905041412",
  "text" : "RT @vickyharp: [User requests feature already in product]\nJunior dev: \"lol dumb user\"\nStaff dev: \"Closed - fixed\"\nSenior dev: &lt;opens usabil\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "736228835046219779",
    "text" : "[User requests feature already in product]\nJunior dev: \"lol dumb user\"\nStaff dev: \"Closed - fixed\"\nSenior dev: &lt;opens usability bug&gt;",
    "id" : 736228835046219779,
    "created_at" : "2016-05-27 16:13:27 +0000",
    "user" : {
      "name" : "Vicky Harp",
      "screen_name" : "vickyharp",
      "protected" : false,
      "id_str" : "254222101",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/986074272782151681\/jchV09XE_normal.jpg",
      "id" : 254222101,
      "verified" : false
    }
  },
  "id" : 803649041905041412,
  "created_at" : "2016-11-29 17:17:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MasterClass",
      "screen_name" : "masterclass",
      "indices" : [ 3, 15 ],
      "id_str" : "3108033410",
      "id" : 3108033410
    }, {
      "name" : "Rebecca Gallegos",
      "screen_name" : "RealHansZimmer",
      "indices" : [ 40, 55 ],
      "id_str" : "846094303063654400",
      "id" : 846094303063654400
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803592996134469632",
  "text" : "RT @masterclass: Now you can learn from @RealHansZimmer, the man behind the music that made Sherlock Holmes, The Joker, and Jack Sparrow! h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rebecca Gallegos",
        "screen_name" : "RealHansZimmer",
        "indices" : [ 23, 38 ],
        "id_str" : "846094303063654400",
        "id" : 846094303063654400
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/masterclass\/status\/799723619601371137\/video\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/y5XrsbOhrv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxkwEqYUAAAu4Dr.jpg",
        "id_str" : "799722766987501569",
        "id" : 799722766987501569,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxkwEqYUAAAu4Dr.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y5XrsbOhrv"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "799723619601371137",
    "text" : "Now you can learn from @RealHansZimmer, the man behind the music that made Sherlock Holmes, The Joker, and Jack Sparrow! https:\/\/t.co\/y5XrsbOhrv",
    "id" : 799723619601371137,
    "created_at" : "2016-11-18 21:19:03 +0000",
    "user" : {
      "name" : "MasterClass",
      "screen_name" : "masterclass",
      "protected" : false,
      "id_str" : "3108033410",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875482347658895361\/OWbmmcOY_normal.jpg",
      "id" : 3108033410,
      "verified" : false
    }
  },
  "id" : 803592996134469632,
  "created_at" : "2016-11-29 13:34:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Harvard Biz Review",
      "screen_name" : "HarvardBiz",
      "indices" : [ 3, 14 ],
      "id_str" : "14800270",
      "id" : 14800270
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803549390250065920",
  "text" : "RT @HarvardBiz: Efforts to cure cancer will be hampered if the scientific community does not change how patient data is shared https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/aDZ01IdfJr",
        "expanded_url" : "http:\/\/s.hbr.org\/2gpxfKq",
        "display_url" : "s.hbr.org\/2gpxfKq"
      } ]
    },
    "geo" : { },
    "id_str" : "803381481796149249",
    "text" : "Efforts to cure cancer will be hampered if the scientific community does not change how patient data is shared https:\/\/t.co\/aDZ01IdfJr",
    "id" : 803381481796149249,
    "created_at" : "2016-11-28 23:34:06 +0000",
    "user" : {
      "name" : "Harvard Biz Review",
      "screen_name" : "HarvardBiz",
      "protected" : false,
      "id_str" : "14800270",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/890310669739991040\/wXqj8htb_normal.jpg",
      "id" : 14800270,
      "verified" : true
    }
  },
  "id" : 803549390250065920,
  "created_at" : "2016-11-29 10:41:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deirdre Lee",
      "screen_name" : "deirdrelee",
      "indices" : [ 3, 14 ],
      "id_str" : "21078662",
      "id" : 21078662
    }, {
      "name" : "Derilinx",
      "screen_name" : "derilinx",
      "indices" : [ 62, 71 ],
      "id_str" : "2241015385",
      "id" : 2241015385
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803545186668314624",
  "text" : "RT @deirdrelee: Interested in joining a growing data startup? @Derilinx is hiring a Business Development Executive \/ Account Manager\nhttps:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Derilinx",
        "screen_name" : "derilinx",
        "indices" : [ 46, 55 ],
        "id_str" : "2241015385",
        "id" : 2241015385
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/8G5GMSaCl9",
        "expanded_url" : "http:\/\/jobview.monster.ie\/v2\/job\/View?JobID=176755895",
        "display_url" : "jobview.monster.ie\/v2\/job\/View?Jo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "803540164240871424",
    "text" : "Interested in joining a growing data startup? @Derilinx is hiring a Business Development Executive \/ Account Manager\nhttps:\/\/t.co\/8G5GMSaCl9",
    "id" : 803540164240871424,
    "created_at" : "2016-11-29 10:04:38 +0000",
    "user" : {
      "name" : "Deirdre Lee",
      "screen_name" : "deirdrelee",
      "protected" : false,
      "id_str" : "21078662",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/723143954917634048\/X0Uc0skg_normal.jpg",
      "id" : 21078662,
      "verified" : false
    }
  },
  "id" : 803545186668314624,
  "created_at" : "2016-11-29 10:24:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/4bgWS4WmeH",
      "expanded_url" : "https:\/\/twitter.com\/sergeysus\/status\/803520025114058752",
      "display_url" : "twitter.com\/sergeysus\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "803532554221551616",
  "text" : "I spat my morning coffee on this one. https:\/\/t.co\/4bgWS4WmeH",
  "id" : 803532554221551616,
  "created_at" : "2016-11-29 09:34:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Asia Matters",
      "screen_name" : "asiamatters_biz",
      "indices" : [ 3, 19 ],
      "id_str" : "491708429",
      "id" : 491708429
    }, {
      "name" : "Ibec",
      "screen_name" : "ibec_irl",
      "indices" : [ 61, 70 ],
      "id_str" : "161242354",
      "id" : 161242354
    }, {
      "name" : "Enterprise Ireland",
      "screen_name" : "Entirl",
      "indices" : [ 129, 136 ],
      "id_str" : "60575311",
      "id" : 60575311
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803531569919066113",
  "text" : "RT @asiamatters_biz: Nicola Sturgeon First Minister Scotland @ibec_irl promotes Ireland Scotland business partnership postBrexit @Entirl ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ibec",
        "screen_name" : "ibec_irl",
        "indices" : [ 40, 49 ],
        "id_str" : "161242354",
        "id" : 161242354
      }, {
        "name" : "Enterprise Ireland",
        "screen_name" : "Entirl",
        "indices" : [ 108, 115 ],
        "id_str" : "60575311",
        "id" : 60575311
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/asiamatters_biz\/status\/803523737647714304\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/fGc13N9PrW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cyawj_SWgAIjRbX.jpg",
        "id_str" : "803523715862528002",
        "id" : 803523715862528002,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cyawj_SWgAIjRbX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fGc13N9PrW"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "803523737647714304",
    "text" : "Nicola Sturgeon First Minister Scotland @ibec_irl promotes Ireland Scotland business partnership postBrexit @Entirl https:\/\/t.co\/fGc13N9PrW",
    "id" : 803523737647714304,
    "created_at" : "2016-11-29 08:59:22 +0000",
    "user" : {
      "name" : "Asia Matters",
      "screen_name" : "asiamatters_biz",
      "protected" : false,
      "id_str" : "491708429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/775714250392305664\/OoHdgZNl_normal.jpg",
      "id" : 491708429,
      "verified" : false
    }
  },
  "id" : 803531569919066113,
  "created_at" : "2016-11-29 09:30:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Margaret Molloy",
      "screen_name" : "MargaretMolloy",
      "indices" : [ 3, 18 ],
      "id_str" : "22328001",
      "id" : 22328001
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startup",
      "indices" : [ 46, 54 ]
    }, {
      "text" : "Stripe",
      "indices" : [ 84, 91 ]
    }, {
      "text" : "brand",
      "indices" : [ 110, 116 ]
    }, {
      "text" : "CyberMonday",
      "indices" : [ 117, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803499569459957760",
  "text" : "RT @MargaretMolloy: The most valuable fintech #startup in the US has Irish origins: #Stripe is now worth $9bn #brand #CyberMonday https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MargaretMolloy\/status\/803263770717986816\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/w5irxRq4SD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CyXD60gWIAEXZXe.jpg",
        "id_str" : "803263523849641985",
        "id" : 803263523849641985,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyXD60gWIAEXZXe.jpg",
        "sizes" : [ {
          "h" : 321,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 321,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 321,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 321,
          "resize" : "fit",
          "w" : 599
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/w5irxRq4SD"
      } ],
      "hashtags" : [ {
        "text" : "startup",
        "indices" : [ 26, 34 ]
      }, {
        "text" : "Stripe",
        "indices" : [ 64, 71 ]
      }, {
        "text" : "brand",
        "indices" : [ 90, 96 ]
      }, {
        "text" : "CyberMonday",
        "indices" : [ 97, 109 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "803263770717986816",
    "text" : "The most valuable fintech #startup in the US has Irish origins: #Stripe is now worth $9bn #brand #CyberMonday https:\/\/t.co\/w5irxRq4SD",
    "id" : 803263770717986816,
    "created_at" : "2016-11-28 15:46:21 +0000",
    "user" : {
      "name" : "Margaret Molloy",
      "screen_name" : "MargaretMolloy",
      "protected" : false,
      "id_str" : "22328001",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/667002859272040448\/7jNWhjdM_normal.jpg",
      "id" : 22328001,
      "verified" : true
    }
  },
  "id" : 803499569459957760,
  "created_at" : "2016-11-29 07:23:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 94, 102 ],
      "id_str" : "1652541",
      "id" : 1652541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/W0sYsAgbXk",
      "expanded_url" : "http:\/\/reut.rs\/2gdegoQ",
      "display_url" : "reut.rs\/2gdegoQ"
    } ]
  },
  "geo" : { },
  "id_str" : "803495292091990016",
  "text" : "China paper says Singapore troop carriers should be 'melted down' https:\/\/t.co\/W0sYsAgbXk via @Reuters",
  "id" : 803495292091990016,
  "created_at" : "2016-11-29 07:06:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/m1toqYxq5D",
      "expanded_url" : "http:\/\/www.timeout.com\/london\/blog\/tell-us-your-favourite-singaporean-places-in-london-112816",
      "display_url" : "timeout.com\/london\/blog\/te\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "803494946917466112",
  "text" : "There are about 10,000 Singaporeans in London. In Ireland, there are 100 plus https:\/\/t.co\/m1toqYxq5D",
  "id" : 803494946917466112,
  "created_at" : "2016-11-29 07:04:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Lorica \u7F57\u745E\u5361",
      "screen_name" : "bigdata",
      "indices" : [ 3, 11 ],
      "id_str" : "18318677",
      "id" : 18318677
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "stratahadoop",
      "indices" : [ 13, 26 ]
    }, {
      "text" : "smartnation",
      "indices" : [ 78, 90 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803492530553757696",
  "text" : "RT @bigdata: #stratahadoop \uD83C\uDDF8\uD83C\uDDEC starts in less than a week, see you there soon! #smartnation use the Discount code bigdata20 https:\/\/t.co\/fYy\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "stratahadoop",
        "indices" : [ 0, 13 ]
      }, {
        "text" : "smartnation",
        "indices" : [ 65, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/fYycWjI19N",
        "expanded_url" : "http:\/\/conferences.oreilly.com\/strata\/hadoop-big-data-sg",
        "display_url" : "conferences.oreilly.com\/strata\/hadoop-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "803490452808855553",
    "text" : "#stratahadoop \uD83C\uDDF8\uD83C\uDDEC starts in less than a week, see you there soon! #smartnation use the Discount code bigdata20 https:\/\/t.co\/fYycWjI19N",
    "id" : 803490452808855553,
    "created_at" : "2016-11-29 06:47:06 +0000",
    "user" : {
      "name" : "Ben Lorica \u7F57\u745E\u5361",
      "screen_name" : "bigdata",
      "protected" : false,
      "id_str" : "18318677",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/235298259\/bigdata_logo_center3_normal.jpg",
      "id" : 18318677,
      "verified" : false
    }
  },
  "id" : 803492530553757696,
  "created_at" : "2016-11-29 06:55:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803303450951958528",
  "text" : "Hi folks, do you keep a record or journal of the decision you made?",
  "id" : 803303450951958528,
  "created_at" : "2016-11-28 18:24:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803297653274583043",
  "text" : "Someone is doing a presentation on \"Feature Selection for Twitter Sentiment Analysis\" at this evening Machine Learning Dublin meetup.",
  "id" : 803297653274583043,
  "created_at" : "2016-11-28 18:00:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Unroll.Me",
      "screen_name" : "Unrollme",
      "indices" : [ 0, 9 ],
      "id_str" : "339769044",
      "id" : 339769044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/HeNFlblyNo",
      "expanded_url" : "http:\/\/unrollmail.com",
      "display_url" : "unrollmail.com"
    } ]
  },
  "geo" : { },
  "id_str" : "803285111915642880",
  "in_reply_to_user_id" : 339769044,
  "text" : "@Unrollme I came across a dodgy email that has your brand look and feel but the link is https:\/\/t.co\/HeNFlblyNo not unroll.me",
  "id" : 803285111915642880,
  "created_at" : "2016-11-28 17:11:09 +0000",
  "in_reply_to_screen_name" : "Unrollme",
  "in_reply_to_user_id_str" : "339769044",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803252176122478593",
  "text" : "Privatisation seems to be a dirty word....",
  "id" : 803252176122478593,
  "created_at" : "2016-11-28 15:00:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "803187489787805701",
  "text" : "Need to review a Word Doc but there is no MS Office install on your PC? Upload to Dropbox to view it.",
  "id" : 803187489787805701,
  "created_at" : "2016-11-28 10:43:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Rivers",
      "screen_name" : "danriversitv",
      "indices" : [ 3, 16 ],
      "id_str" : "25468225",
      "id" : 25468225
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/g6vJIhqltX",
      "expanded_url" : "https:\/\/twitter.com\/alabedbana\/status\/802870627686301697",
      "display_url" : "twitter.com\/alabedbana\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "803003046150238208",
  "text" : "RT @danriversitv: Gut-wrenching tweet from East Aleppo as Govt forces close in. https:\/\/t.co\/g6vJIhqltX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/g6vJIhqltX",
        "expanded_url" : "https:\/\/twitter.com\/alabedbana\/status\/802870627686301697",
        "display_url" : "twitter.com\/alabedbana\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "802967645939187713",
    "text" : "Gut-wrenching tweet from East Aleppo as Govt forces close in. https:\/\/t.co\/g6vJIhqltX",
    "id" : 802967645939187713,
    "created_at" : "2016-11-27 20:09:39 +0000",
    "user" : {
      "name" : "Dan Rivers",
      "screen_name" : "danriversitv",
      "protected" : false,
      "id_str" : "25468225",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/687364565148037121\/ZpCyLD4S_normal.jpg",
      "id" : 25468225,
      "verified" : true
    }
  },
  "id" : 803003046150238208,
  "created_at" : "2016-11-27 22:30:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/JkHtPhDh6h",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/20140409134155-10626173-how-percolate-does-onboarding?_mSplash=1",
      "display_url" : "linkedin.com\/pulse\/20140409\u2026"
    }, {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/bjttHq4qTC",
      "expanded_url" : "http:\/\/www.adweek.com\/adfreak\/check-out-amazing-welcome-kit-ogilvy-office-gives-each-new-hire-164196",
      "display_url" : "adweek.com\/adfreak\/check-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "802952901312344064",
  "text" : "How some organisation offer special welcome kit to get new hire on board. https:\/\/t.co\/JkHtPhDh6h | https:\/\/t.co\/bjttHq4qTC",
  "id" : 802952901312344064,
  "created_at" : "2016-11-27 19:11:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Murphy",
      "screen_name" : "DrMarkMurphy",
      "indices" : [ 3, 16 ],
      "id_str" : "865173241",
      "id" : 865173241
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802929115808591872",
  "text" : "RT @DrMarkMurphy: Health-lobby groups need to realise HSE can only advocate 'cost-effective' treatments. \n\nIssue is the pharma price.  http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/JsadJ7YVVd",
        "expanded_url" : "https:\/\/twitter.com\/thejournal_ie\/status\/802898924214374404",
        "display_url" : "twitter.com\/thejournal_ie\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "802901116761604096",
    "text" : "Health-lobby groups need to realise HSE can only advocate 'cost-effective' treatments. \n\nIssue is the pharma price.  https:\/\/t.co\/JsadJ7YVVd",
    "id" : 802901116761604096,
    "created_at" : "2016-11-27 15:45:18 +0000",
    "user" : {
      "name" : "Mark Murphy",
      "screen_name" : "DrMarkMurphy",
      "protected" : false,
      "id_str" : "865173241",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032193399112323072\/mmGJjO0T_normal.jpg",
      "id" : 865173241,
      "verified" : false
    }
  },
  "id" : 802929115808591872,
  "created_at" : "2016-11-27 17:36:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emma McArdle",
      "screen_name" : "McArdlePhoto",
      "indices" : [ 0, 13 ],
      "id_str" : "4354408581",
      "id" : 4354408581
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "802896988006215680",
  "geo" : { },
  "id_str" : "802928715239854080",
  "in_reply_to_user_id" : 4354408581,
  "text" : "@McArdlePhoto image on the right.",
  "id" : 802928715239854080,
  "in_reply_to_status_id" : 802896988006215680,
  "created_at" : "2016-11-27 17:34:58 +0000",
  "in_reply_to_screen_name" : "McArdlePhoto",
  "in_reply_to_user_id_str" : "4354408581",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jennifer O'Connell",
      "screen_name" : "jenoconnell",
      "indices" : [ 3, 15 ],
      "id_str" : "37694171",
      "id" : 37694171
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802833639088291840",
  "text" : "RT @jenoconnell: The average house has 300,000 things. 244 moving boxes later, that seems modest. My column on me vs. stuff  https:\/\/t.co\/A\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/A00MvMQzRv",
        "expanded_url" : "http:\/\/www.irishtimes.com\/life-and-style\/people\/why-do-we-spend-years-accumulating-useless-things-1.2870943",
        "display_url" : "irishtimes.com\/life-and-style\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "802825156695564288",
    "text" : "The average house has 300,000 things. 244 moving boxes later, that seems modest. My column on me vs. stuff  https:\/\/t.co\/A00MvMQzRv",
    "id" : 802825156695564288,
    "created_at" : "2016-11-27 10:43:27 +0000",
    "user" : {
      "name" : "Jennifer O'Connell",
      "screen_name" : "jenoconnell",
      "protected" : false,
      "id_str" : "37694171",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/955861069125312512\/5X6CrtZd_normal.jpg",
      "id" : 37694171,
      "verified" : true
    }
  },
  "id" : 802833639088291840,
  "created_at" : "2016-11-27 11:17:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 129, 152 ],
      "url" : "https:\/\/t.co\/oim5ptXUjn",
      "expanded_url" : "https:\/\/twitter.com\/sparkcbc\/status\/800142633846931456",
      "display_url" : "twitter.com\/sparkcbc\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "802826197327548416",
  "text" : "\"Using a ML algorithm, Chillwall, over time, learns the interests of its users and predicts events they might find interesting.\" https:\/\/t.co\/oim5ptXUjn",
  "id" : 802826197327548416,
  "created_at" : "2016-11-27 10:47:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/4XGvSCTMIw",
      "expanded_url" : "https:\/\/twitter.com\/danielhfrank\/status\/801133342250962944",
      "display_url" : "twitter.com\/danielhfrank\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "802822619644837888",
  "text" : "Every business that deal with data should do reproducible research as part of their work flow. https:\/\/t.co\/4XGvSCTMIw",
  "id" : 802822619644837888,
  "created_at" : "2016-11-27 10:33:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Collins",
      "screen_name" : "LJCollins_",
      "indices" : [ 3, 14 ],
      "id_str" : "861836838071226368",
      "id" : 861836838071226368
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/SCcjYb8djr",
      "expanded_url" : "https:\/\/www.theguardian.com\/commentisfree\/2015\/mar\/25\/treating-soil-like-dirt-fatal-mistake-human-life",
      "display_url" : "theguardian.com\/commentisfree\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "802810790092926977",
  "text" : "RT @LJCollins_: Cheery Saturday reading: we have 60 years of harvests lefts https:\/\/t.co\/SCcjYb8djr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/SCcjYb8djr",
        "expanded_url" : "https:\/\/www.theguardian.com\/commentisfree\/2015\/mar\/25\/treating-soil-like-dirt-fatal-mistake-human-life",
        "display_url" : "theguardian.com\/commentisfree\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "802399776587149312",
    "text" : "Cheery Saturday reading: we have 60 years of harvests lefts https:\/\/t.co\/SCcjYb8djr",
    "id" : 802399776587149312,
    "created_at" : "2016-11-26 06:33:09 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 802810790092926977,
  "created_at" : "2016-11-27 09:46:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802648325505695745",
  "text" : "RT @SarcasmMother: Religion is just an open source program where everyone claims their build is the official release.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "770255856889950214",
    "text" : "Religion is just an open source program where everyone claims their build is the official release.",
    "id" : 770255856889950214,
    "created_at" : "2016-08-29 13:44:41 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 802648325505695745,
  "created_at" : "2016-11-26 23:00:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802643408032563200",
  "text" : "Want to sell my vacuum cleaner, it was just gathering dust.",
  "id" : 802643408032563200,
  "created_at" : "2016-11-26 22:41:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sergey Sus",
      "screen_name" : "sergeysus",
      "indices" : [ 3, 13 ],
      "id_str" : "88265548",
      "id" : 88265548
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/dEwzfeuKhc",
      "expanded_url" : "http:\/\/buff.ly\/2gEiac3",
      "display_url" : "buff.ly\/2gEiac3"
    } ]
  },
  "geo" : { },
  "id_str" : "802631079295381509",
  "text" : "RT @sergeysus: Must See: Explore Time\u2019s Interactive \u201C100 Most Influential Images of All Time\u201D Online Project https:\/\/t.co\/dEwzfeuKhc https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sergeysus\/status\/802619944705794049\/photo\/1",
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/bmHK5SgItv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CyN6ldCXcAAa_cM.jpg",
        "id_str" : "802619942470381568",
        "id" : 802619942470381568,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyN6ldCXcAAa_cM.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 332,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 332,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 332,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 332,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bmHK5SgItv"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/dEwzfeuKhc",
        "expanded_url" : "http:\/\/buff.ly\/2gEiac3",
        "display_url" : "buff.ly\/2gEiac3"
      } ]
    },
    "geo" : { },
    "id_str" : "802619944705794049",
    "text" : "Must See: Explore Time\u2019s Interactive \u201C100 Most Influential Images of All Time\u201D Online Project https:\/\/t.co\/dEwzfeuKhc https:\/\/t.co\/bmHK5SgItv",
    "id" : 802619944705794049,
    "created_at" : "2016-11-26 21:08:01 +0000",
    "user" : {
      "name" : "Sergey Sus",
      "screen_name" : "sergeysus",
      "protected" : false,
      "id_str" : "88265548",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1498740695\/self_normal.jpg",
      "id" : 88265548,
      "verified" : false
    }
  },
  "id" : 802631079295381509,
  "created_at" : "2016-11-26 21:52:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "P\u23E9M\u00E9ance\uD83D\uDCA6",
      "screen_name" : "pmeance",
      "indices" : [ 3, 11 ],
      "id_str" : "29747712",
      "id" : 29747712
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "chefdescadrille",
      "indices" : [ 32, 48 ]
    } ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/oLLmnB4t2o",
      "expanded_url" : "https:\/\/twitter.com\/tomipa06\/status\/802603777429737472",
      "display_url" : "twitter.com\/tomipa06\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "802630816475942912",
  "text" : "RT @pmeance: H\u00E9 h\u00E9, magnifique! #chefdescadrille https:\/\/t.co\/oLLmnB4t2o",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "chefdescadrille",
        "indices" : [ 19, 35 ]
      } ],
      "urls" : [ {
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/oLLmnB4t2o",
        "expanded_url" : "https:\/\/twitter.com\/tomipa06\/status\/802603777429737472",
        "display_url" : "twitter.com\/tomipa06\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "802629831187775488",
    "text" : "H\u00E9 h\u00E9, magnifique! #chefdescadrille https:\/\/t.co\/oLLmnB4t2o",
    "id" : 802629831187775488,
    "created_at" : "2016-11-26 21:47:18 +0000",
    "user" : {
      "name" : "P\u23E9M\u00E9ance\uD83D\uDCA6",
      "screen_name" : "pmeance",
      "protected" : false,
      "id_str" : "29747712",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/419514267662950400\/NVYI-oIo_normal.jpeg",
      "id" : 29747712,
      "verified" : false
    }
  },
  "id" : 802630816475942912,
  "created_at" : "2016-11-26 21:51:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cause We're Canadian",
      "screen_name" : "MadelnCanada",
      "indices" : [ 3, 16 ],
      "id_str" : "445138206",
      "id" : 445138206
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/SBelliveauCTV\/status\/802089760516874240\/video\/1",
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/MkMPUoDW8m",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/802089546020069376\/pu\/img\/3R_e4xrmBOmxrDx8.jpg",
      "id_str" : "802089546020069376",
      "id" : 802089546020069376,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/802089546020069376\/pu\/img\/3R_e4xrmBOmxrDx8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MkMPUoDW8m"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802571274379821057",
  "text" : "RT @MadelnCanada: How Canada does door crasher Black Friday sales. This is so Canadian it hurts lmao https:\/\/t.co\/MkMPUoDW8m",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SBelliveauCTV\/status\/802089760516874240\/video\/1",
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/MkMPUoDW8m",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/802089546020069376\/pu\/img\/3R_e4xrmBOmxrDx8.jpg",
        "id_str" : "802089546020069376",
        "id" : 802089546020069376,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/802089546020069376\/pu\/img\/3R_e4xrmBOmxrDx8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MkMPUoDW8m"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "802218968773722112",
    "text" : "How Canada does door crasher Black Friday sales. This is so Canadian it hurts lmao https:\/\/t.co\/MkMPUoDW8m",
    "id" : 802218968773722112,
    "created_at" : "2016-11-25 18:34:41 +0000",
    "user" : {
      "name" : "Cause We're Canadian",
      "screen_name" : "MadelnCanada",
      "protected" : false,
      "id_str" : "445138206",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3113992990\/d6554617784208c38de83446a7ba1ce4_normal.jpeg",
      "id" : 445138206,
      "verified" : false
    }
  },
  "id" : 802571274379821057,
  "created_at" : "2016-11-26 17:54:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Data Science Fact",
      "screen_name" : "DataSciFact",
      "indices" : [ 3, 15 ],
      "id_str" : "220139885",
      "id" : 220139885
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802565655493046272",
  "text" : "RT @DataSciFact: 'A Bayesian is one who, vaguely expecting a horse, and catching a glimpse of a donkey, strongly believes he has seen a mul\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "802229272555515904",
    "text" : "'A Bayesian is one who, vaguely expecting a horse, and catching a glimpse of a donkey, strongly believes he has seen a mule.'",
    "id" : 802229272555515904,
    "created_at" : "2016-11-25 19:15:37 +0000",
    "user" : {
      "name" : "Data Science Fact",
      "screen_name" : "DataSciFact",
      "protected" : false,
      "id_str" : "220139885",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/871521787300855809\/sMpYCCGn_normal.jpg",
      "id" : 220139885,
      "verified" : false
    }
  },
  "id" : 802565655493046272,
  "created_at" : "2016-11-26 17:32:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jacamo",
      "screen_name" : "Jacamo",
      "indices" : [ 3, 10 ],
      "id_str" : "14275264",
      "id" : 14275264
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BlackFriday",
      "indices" : [ 18, 30 ]
    }, {
      "text" : "WIN",
      "indices" : [ 31, 35 ]
    }, {
      "text" : "FreebieFriday",
      "indices" : [ 110, 124 ]
    }, {
      "text" : "PS4BlackFriday",
      "indices" : [ 125, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/mwrHRgJsUt",
      "expanded_url" : "http:\/\/buff.ly\/2ggt5UC",
      "display_url" : "buff.ly\/2ggt5UC"
    } ]
  },
  "geo" : { },
  "id_str" : "802227367221612548",
  "text" : "RT @Jacamo: Happy #BlackFriday #WIN a PS4 Bundle! \uD83D\uDC4D Followers, RT to win! (T&amp;C's https:\/\/t.co\/mwrHRgJsUt) #FreebieFriday #PS4BlackFriday ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Jacamo\/status\/802074613073244160\/photo\/1",
        "indices" : [ 129, 152 ],
        "url" : "https:\/\/t.co\/W3GPWJYn2U",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CyGKm9tUQAAEYdQ.jpg",
        "id_str" : "802074610652954624",
        "id" : 802074610652954624,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyGKm9tUQAAEYdQ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 473,
          "resize" : "fit",
          "w" : 945
        }, {
          "h" : 473,
          "resize" : "fit",
          "w" : 945
        }, {
          "h" : 473,
          "resize" : "fit",
          "w" : 945
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/W3GPWJYn2U"
      } ],
      "hashtags" : [ {
        "text" : "BlackFriday",
        "indices" : [ 6, 18 ]
      }, {
        "text" : "WIN",
        "indices" : [ 19, 23 ]
      }, {
        "text" : "FreebieFriday",
        "indices" : [ 98, 112 ]
      }, {
        "text" : "PS4BlackFriday",
        "indices" : [ 113, 128 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/mwrHRgJsUt",
        "expanded_url" : "http:\/\/buff.ly\/2ggt5UC",
        "display_url" : "buff.ly\/2ggt5UC"
      } ]
    },
    "geo" : { },
    "id_str" : "802074613073244160",
    "text" : "Happy #BlackFriday #WIN a PS4 Bundle! \uD83D\uDC4D Followers, RT to win! (T&amp;C's https:\/\/t.co\/mwrHRgJsUt) #FreebieFriday #PS4BlackFriday https:\/\/t.co\/W3GPWJYn2U",
    "id" : 802074613073244160,
    "created_at" : "2016-11-25 09:01:04 +0000",
    "user" : {
      "name" : "Jacamo",
      "screen_name" : "Jacamo",
      "protected" : false,
      "id_str" : "14275264",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/806166444136402944\/qVfw_s7s_normal.jpg",
      "id" : 14275264,
      "verified" : true
    }
  },
  "id" : 802227367221612548,
  "created_at" : "2016-11-25 19:08:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802200486220800000",
  "text" : "As an avgeek, I choose plane type I going to fly right after budget consideration. Looking forward travelling the Boeing 787 in 2017.",
  "id" : 802200486220800000,
  "created_at" : "2016-11-25 17:21:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kickstarter",
      "screen_name" : "kickstarter",
      "indices" : [ 67, 79 ],
      "id_str" : "16186995",
      "id" : 16186995
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/XD8QSLwwZn",
      "expanded_url" : "http:\/\/kck.st\/2fd94xy",
      "display_url" : "kck.st\/2fd94xy"
    } ]
  },
  "geo" : { },
  "id_str" : "802157224982446080",
  "text" : "World's first wallet that sorts your cash in a cinch is popular on @Kickstarter! https:\/\/t.co\/XD8QSLwwZn",
  "id" : 802157224982446080,
  "created_at" : "2016-11-25 14:29:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "indices" : [ 3, 16 ],
      "id_str" : "2300614526",
      "id" : 2300614526
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802153053340233728",
  "text" : "RT @pachamaltese: If you have an unused PS3 that you are willing to give for free\/at a low price write me. It can help me to create a \"supe\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "801891992460554240",
    "text" : "If you have an unused PS3 that you are willing to give for free\/at a low price write me. It can help me to create a \"supercomputer\"",
    "id" : 801891992460554240,
    "created_at" : "2016-11-24 20:55:24 +0000",
    "user" : {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "protected" : false,
      "id_str" : "2300614526",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/955616100645068800\/nexQrTFV_normal.jpg",
      "id" : 2300614526,
      "verified" : false
    }
  },
  "id" : 802153053340233728,
  "created_at" : "2016-11-25 14:12:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/8PgYPeb0hQ",
      "expanded_url" : "https:\/\/thenmpr.com\/",
      "display_url" : "thenmpr.com"
    } ]
  },
  "geo" : { },
  "id_str" : "802152683327066112",
  "text" : "IS there something similar to https:\/\/t.co\/8PgYPeb0hQ in Ireland? Thanks",
  "id" : 802152683327066112,
  "created_at" : "2016-11-25 14:11:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "802054024484814848",
  "text" : "RT @yapphenghui: Great initiative \nAdults reveal PSLE scores &amp; what they\u2019re doing now as encouragement for primary school leavers https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/mG7eku56JD",
        "expanded_url" : "http:\/\/mothership.sg\/2016\/11\/adults-reveal-psle-scores-what-theyre-doing-now-as-encouragement-for-primary-school-leavers\/",
        "display_url" : "mothership.sg\/2016\/11\/adults\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "801988581149446144",
    "text" : "Great initiative \nAdults reveal PSLE scores &amp; what they\u2019re doing now as encouragement for primary school leavers https:\/\/t.co\/mG7eku56JD",
    "id" : 801988581149446144,
    "created_at" : "2016-11-25 03:19:12 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 802054024484814848,
  "created_at" : "2016-11-25 07:39:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/RGgGh3yBvC",
      "expanded_url" : "http:\/\/thedataist.com\/the-biggest-problem-in-data-science-and-how-to-fix-it\/",
      "display_url" : "thedataist.com\/the-biggest-pr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801889386686246916",
  "text" : "Biggest problem is how to \"merge multiple disparate data sources and types quickly and easily\" https:\/\/t.co\/RGgGh3yBvC",
  "id" : 801889386686246916,
  "created_at" : "2016-11-24 20:45:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Robinson",
      "screen_name" : "drob",
      "indices" : [ 3, 8 ],
      "id_str" : "46245868",
      "id" : 46245868
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/2LPgJil3s1",
      "expanded_url" : "https:\/\/twitter.com\/andrestaltz\/status\/801798428363751424",
      "display_url" : "twitter.com\/andrestaltz\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801887108268957700",
  "text" : "RT @drob: This animation would have saved me so much pain in college https:\/\/t.co\/2LPgJil3s1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/2LPgJil3s1",
        "expanded_url" : "https:\/\/twitter.com\/andrestaltz\/status\/801798428363751424",
        "display_url" : "twitter.com\/andrestaltz\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "801876079799574528",
    "text" : "This animation would have saved me so much pain in college https:\/\/t.co\/2LPgJil3s1",
    "id" : 801876079799574528,
    "created_at" : "2016-11-24 19:52:10 +0000",
    "user" : {
      "name" : "David Robinson",
      "screen_name" : "drob",
      "protected" : false,
      "id_str" : "46245868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876529727284039680\/dfvG_Dy4_normal.jpg",
      "id" : 46245868,
      "verified" : false
    }
  },
  "id" : 801887108268957700,
  "created_at" : "2016-11-24 20:35:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Heidi Shey",
      "screen_name" : "heidishey",
      "indices" : [ 3, 13 ],
      "id_str" : "61813035",
      "id" : 61813035
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/EqC3p0xE1j",
      "expanded_url" : "https:\/\/twitter.com\/rik_ferguson\/status\/801396842185555968",
      "display_url" : "twitter.com\/rik_ferguson\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801886603341860864",
  "text" : "RT @heidishey: Detailed, actionable guide. https:\/\/t.co\/EqC3p0xE1j",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 51 ],
        "url" : "https:\/\/t.co\/EqC3p0xE1j",
        "expanded_url" : "https:\/\/twitter.com\/rik_ferguson\/status\/801396842185555968",
        "display_url" : "twitter.com\/rik_ferguson\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "801452797182914564",
    "text" : "Detailed, actionable guide. https:\/\/t.co\/EqC3p0xE1j",
    "id" : 801452797182914564,
    "created_at" : "2016-11-23 15:50:11 +0000",
    "user" : {
      "name" : "Heidi Shey",
      "screen_name" : "heidishey",
      "protected" : false,
      "id_str" : "61813035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1745929847\/me_normal.jpg",
      "id" : 61813035,
      "verified" : false
    }
  },
  "id" : 801886603341860864,
  "created_at" : "2016-11-24 20:33:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801880381838557184",
  "text" : "Some shoppers still find time to chit chat and engage in small talk with the cashier when there is a long queue behind the till...",
  "id" : 801880381838557184,
  "created_at" : "2016-11-24 20:09:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Max Roser",
      "screen_name" : "MaxCRoser",
      "indices" : [ 3, 13 ],
      "id_str" : "610659001",
      "id" : 610659001
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/W3jYzkv76R",
      "expanded_url" : "http:\/\/OurWorldInData.org",
      "display_url" : "OurWorldInData.org"
    } ]
  },
  "geo" : { },
  "id_str" : "801855356158808064",
  "text" : "RT @MaxCRoser: In every country people are overestimating the share of the foreign-born population.\n\n(from https:\/\/t.co\/W3jYzkv76R) https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MaxCRoser\/status\/801584526266875914\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/MtT0No3Ue3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cx_MgazXAAAC9SO.jpg",
        "id_str" : "801584116017856512",
        "id" : 801584116017856512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cx_MgazXAAAC9SO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2100,
          "resize" : "fit",
          "w" : 3000
        }, {
          "h" : 840,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 476,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1434,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MtT0No3Ue3"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/W3jYzkv76R",
        "expanded_url" : "http:\/\/OurWorldInData.org",
        "display_url" : "OurWorldInData.org"
      } ]
    },
    "geo" : { },
    "id_str" : "801584526266875914",
    "text" : "In every country people are overestimating the share of the foreign-born population.\n\n(from https:\/\/t.co\/W3jYzkv76R) https:\/\/t.co\/MtT0No3Ue3",
    "id" : 801584526266875914,
    "created_at" : "2016-11-24 00:33:38 +0000",
    "user" : {
      "name" : "Max Roser",
      "screen_name" : "MaxCRoser",
      "protected" : false,
      "id_str" : "610659001",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991373737571348481\/Y1zcLTFQ_normal.jpg",
      "id" : 610659001,
      "verified" : true
    }
  },
  "id" : 801855356158808064,
  "created_at" : "2016-11-24 18:29:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801854481344368640",
  "text" : "If you can't afford,  you shouldn't live in city centres",
  "id" : 801854481344368640,
  "created_at" : "2016-11-24 18:26:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/rATrh8dNLG",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/288fa9a230b69428dede05d1bbb681ac",
      "display_url" : "gist.github.com\/mryap\/288fa9a2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801841604965371904",
  "text" : "How do I go about solving this &gt; Permission denied: '\/usr\/local\/lib\/python2.7\/dist-packages\/bs4' ? https:\/\/t.co\/rATrh8dNLG Thanks",
  "id" : 801841604965371904,
  "created_at" : "2016-11-24 17:35:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/Jme1oKd5Md",
      "expanded_url" : "http:\/\/stackoverflow.com\/a\/30146007\/6087933?stw=2",
      "display_url" : "stackoverflow.com\/a\/30146007\/608\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801832425647063040",
  "text" : "This solve my problem of installing beautifulsoup on Jupyter notebook https:\/\/t.co\/Jme1oKd5Md",
  "id" : 801832425647063040,
  "created_at" : "2016-11-24 16:58:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GWPF",
      "screen_name" : "thegwpfcom",
      "indices" : [ 3, 14 ],
      "id_str" : "2784228956",
      "id" : 2784228956
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/OSmZm9z76y",
      "expanded_url" : "https:\/\/www.theguardian.com\/environment\/2016\/nov\/24\/protected-forests-in-europe-felled-to-meet-eu-renewable-targets-report?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/environment\/20\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801812306174873600",
  "text" : "RT @thegwpfcom: Green madness: Protected forests in Europe felled to meet EU renewable targets \u2013 report https:\/\/t.co\/OSmZm9z76y",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/OSmZm9z76y",
        "expanded_url" : "https:\/\/www.theguardian.com\/environment\/2016\/nov\/24\/protected-forests-in-europe-felled-to-meet-eu-renewable-targets-report?CMP=share_btn_tw",
        "display_url" : "theguardian.com\/environment\/20\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "801796325117411329",
    "text" : "Green madness: Protected forests in Europe felled to meet EU renewable targets \u2013 report https:\/\/t.co\/OSmZm9z76y",
    "id" : 801796325117411329,
    "created_at" : "2016-11-24 14:35:15 +0000",
    "user" : {
      "name" : "GWPF",
      "screen_name" : "thegwpfcom",
      "protected" : false,
      "id_str" : "2784228956",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506456246795264001\/-nX89U5Y_normal.jpeg",
      "id" : 2784228956,
      "verified" : false
    }
  },
  "id" : 801812306174873600,
  "created_at" : "2016-11-24 15:38:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Yeo \u6768\u542F\u94ED",
      "screen_name" : "TheBaseLeg",
      "indices" : [ 3, 14 ],
      "id_str" : "231384721",
      "id" : 231384721
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheBaseLeg\/status\/801751739124678656\/photo\/1",
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/HDqYDLO3NR",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CyBk8mlUcAABuVE.jpg",
      "id_str" : "801751725983952896",
      "id" : 801751725983952896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyBk8mlUcAABuVE.jpg",
      "sizes" : [ {
        "h" : 132,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 399,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 234,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 399,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HDqYDLO3NR"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801755379713581057",
  "text" : "RT @TheBaseLeg: Singapore Ministry of Defence issues statement about its Terrex ICVs being detained in Hong Kong https:\/\/t.co\/HDqYDLO3NR",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheBaseLeg\/status\/801751739124678656\/photo\/1",
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/HDqYDLO3NR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CyBk8mlUcAABuVE.jpg",
        "id_str" : "801751725983952896",
        "id" : 801751725983952896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CyBk8mlUcAABuVE.jpg",
        "sizes" : [ {
          "h" : 132,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 399,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 234,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 399,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HDqYDLO3NR"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "801751739124678656",
    "text" : "Singapore Ministry of Defence issues statement about its Terrex ICVs being detained in Hong Kong https:\/\/t.co\/HDqYDLO3NR",
    "id" : 801751739124678656,
    "created_at" : "2016-11-24 11:38:05 +0000",
    "user" : {
      "name" : "Mike Yeo \u6768\u542F\u94ED",
      "screen_name" : "TheBaseLeg",
      "protected" : false,
      "id_str" : "231384721",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1688802649\/profile_normal.jpg",
      "id" : 231384721,
      "verified" : false
    }
  },
  "id" : 801755379713581057,
  "created_at" : "2016-11-24 11:52:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 138, 161 ],
      "url" : "https:\/\/t.co\/GRXVobfy6m",
      "expanded_url" : "https:\/\/twitter.com\/sweeneygov\/status\/801728676391952384",
      "display_url" : "twitter.com\/sweeneygov\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801752653357338625",
  "text" : "\u201CStarlight Program\u201D being known to Beijing govt for over a decade &amp; being given tact approval. Reported in the media. Nothing secret. https:\/\/t.co\/GRXVobfy6m",
  "id" : 801752653357338625,
  "created_at" : "2016-11-24 11:41:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 139, 162 ],
      "url" : "https:\/\/t.co\/fnZLNUWaLv",
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/801730144515653633",
      "display_url" : "twitter.com\/mrbrown\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801745221948211200",
  "text" : "Likely to be Singapore Technologies developed Terrex Infantry Carrier Vehicle with Irish powertrains &amp; independent suspension systems. https:\/\/t.co\/fnZLNUWaLv",
  "id" : 801745221948211200,
  "created_at" : "2016-11-24 11:12:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/yzkTwxa99e",
      "expanded_url" : "http:\/\/www.population.sg\/articles\/the-changing-faces-of-singapore-overseas-singaporean-families",
      "display_url" : "population.sg\/articles\/the-c\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801741534433542144",
  "text" : "The changing faces of Singapore: Overseas Singaporean Families https:\/\/t.co\/yzkTwxa99e",
  "id" : 801741534433542144,
  "created_at" : "2016-11-24 10:57:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 0, 12 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "800415248259936256",
  "geo" : { },
  "id_str" : "801556453408591895",
  "in_reply_to_user_id" : 19793936,
  "text" : "@gianniponzi shot from a drone?",
  "id" : 801556453408591895,
  "in_reply_to_status_id" : 800415248259936256,
  "created_at" : "2016-11-23 22:42:05 +0000",
  "in_reply_to_screen_name" : "gianniponzi",
  "in_reply_to_user_id_str" : "19793936",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Blackdog",
      "screen_name" : "BDogCreative",
      "indices" : [ 3, 16 ],
      "id_str" : "389980644",
      "id" : 389980644
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/49NrLbMoCT",
      "expanded_url" : "https:\/\/shar.es\/18mSwN",
      "display_url" : "shar.es\/18mSwN"
    } ]
  },
  "geo" : { },
  "id_str" : "801503165745078273",
  "text" : "RT @BDogCreative: ...the man in the street goes to the shops and buys stuff, he doesn\u2019t enter a retail ecosystem... https:\/\/t.co\/49NrLbMoCT\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "ShareThis",
        "screen_name" : "ShareThis",
        "indices" : [ 126, 136 ],
        "id_str" : "14116807",
        "id" : 14116807
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/49NrLbMoCT",
        "expanded_url" : "https:\/\/shar.es\/18mSwN",
        "display_url" : "shar.es\/18mSwN"
      } ]
    },
    "geo" : { },
    "id_str" : "801363695565737985",
    "text" : "...the man in the street goes to the shops and buys stuff, he doesn\u2019t enter a retail ecosystem... https:\/\/t.co\/49NrLbMoCT via @sharethis",
    "id" : 801363695565737985,
    "created_at" : "2016-11-23 09:56:08 +0000",
    "user" : {
      "name" : "Blackdog",
      "screen_name" : "BDogCreative",
      "protected" : false,
      "id_str" : "389980644",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/786125334995501056\/eYw9r89Y_normal.jpg",
      "id" : 389980644,
      "verified" : false
    }
  },
  "id" : 801503165745078273,
  "created_at" : "2016-11-23 19:10:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9 Business",
      "screen_name" : "RTEbusiness",
      "indices" : [ 3, 15 ],
      "id_str" : "8973142",
      "id" : 8973142
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801425152185696256",
  "text" : "RT @RTEbusiness: The Central Bank is to remove the \u20AC220,000 cap on mortgage lending for first-time buyers who have a deposit of 10%.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "801411677073932288",
    "text" : "The Central Bank is to remove the \u20AC220,000 cap on mortgage lending for first-time buyers who have a deposit of 10%.",
    "id" : 801411677073932288,
    "created_at" : "2016-11-23 13:06:47 +0000",
    "user" : {
      "name" : "RT\u00C9 Business",
      "screen_name" : "RTEbusiness",
      "protected" : false,
      "id_str" : "8973142",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/535034524099293184\/rPdfsuhS_normal.png",
      "id" : 8973142,
      "verified" : true
    }
  },
  "id" : 801425152185696256,
  "created_at" : "2016-11-23 14:00:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrick Collison",
      "screen_name" : "patrickc",
      "indices" : [ 3, 12 ],
      "id_str" : "4939401",
      "id" : 4939401
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/patrickc\/status\/801144552107208704\/photo\/1",
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/HZh3IPS4SI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cx48tKcUQAA9iI-.jpg",
      "id_str" : "801144530313428992",
      "id" : 801144530313428992,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cx48tKcUQAA9iI-.jpg",
      "sizes" : [ {
        "h" : 1446,
        "resize" : "fit",
        "w" : 1241
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 584
      }, {
        "h" : 1446,
        "resize" : "fit",
        "w" : 1241
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1030
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HZh3IPS4SI"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/LnY51tpjTC",
      "expanded_url" : "http:\/\/stripe.com\/blog\/reproducible-research",
      "display_url" : "stripe.com\/blog\/reproduci\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801393475317067776",
  "text" : "RT @patrickc: Reproducible data science: https:\/\/t.co\/LnY51tpjTC https:\/\/t.co\/HZh3IPS4SI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/patrickc\/status\/801144552107208704\/photo\/1",
        "indices" : [ 51, 74 ],
        "url" : "https:\/\/t.co\/HZh3IPS4SI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cx48tKcUQAA9iI-.jpg",
        "id_str" : "801144530313428992",
        "id" : 801144530313428992,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cx48tKcUQAA9iI-.jpg",
        "sizes" : [ {
          "h" : 1446,
          "resize" : "fit",
          "w" : 1241
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 584
        }, {
          "h" : 1446,
          "resize" : "fit",
          "w" : 1241
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1030
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HZh3IPS4SI"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 27, 50 ],
        "url" : "https:\/\/t.co\/LnY51tpjTC",
        "expanded_url" : "http:\/\/stripe.com\/blog\/reproducible-research",
        "display_url" : "stripe.com\/blog\/reproduci\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "801144552107208704",
    "text" : "Reproducible data science: https:\/\/t.co\/LnY51tpjTC https:\/\/t.co\/HZh3IPS4SI",
    "id" : 801144552107208704,
    "created_at" : "2016-11-22 19:25:20 +0000",
    "user" : {
      "name" : "Patrick Collison",
      "screen_name" : "patrickc",
      "protected" : false,
      "id_str" : "4939401",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/825622525342199809\/_iAaSUQf_normal.jpg",
      "id" : 4939401,
      "verified" : true
    }
  },
  "id" : 801393475317067776,
  "created_at" : "2016-11-23 11:54:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/ixfiFkxJU9",
      "expanded_url" : "https:\/\/twitter.com\/oconnellbrian\/status\/801349920196296704",
      "display_url" : "twitter.com\/oconnellbrian\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801372550727106560",
  "text" : "Can't she stay with her parents? https:\/\/t.co\/ixfiFkxJU9",
  "id" : 801372550727106560,
  "created_at" : "2016-11-23 10:31:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/YapQ8Wr61n",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/799737293183459328",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801371998773530624",
  "text" : "By interacting with the app, you essentially help Google to train the machine to learn leading to better outcome. https:\/\/t.co\/YapQ8Wr61n",
  "id" : 801371998773530624,
  "created_at" : "2016-11-23 10:29:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Front Line (Retd)",
      "screen_name" : "GalliganChristy",
      "indices" : [ 3, 19 ],
      "id_str" : "773380357",
      "id" : 773380357
    }, {
      "name" : "Brian O'Connell",
      "screen_name" : "oconnellbrian",
      "indices" : [ 21, 35 ],
      "id_str" : "108597502",
      "id" : 108597502
    }, {
      "name" : "Today Sean O'Rourke",
      "screen_name" : "TodaySOR",
      "indices" : [ 36, 45 ],
      "id_str" : "242702809",
      "id" : 242702809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801370815929401344",
  "text" : "RT @GalliganChristy: @oconnellbrian @TodaySOR This is not PC. But why be a mother knowing you don't have a home for your child?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Brian O'Connell",
        "screen_name" : "oconnellbrian",
        "indices" : [ 0, 14 ],
        "id_str" : "108597502",
        "id" : 108597502
      }, {
        "name" : "Today Sean O'Rourke",
        "screen_name" : "TodaySOR",
        "indices" : [ 15, 24 ],
        "id_str" : "242702809",
        "id" : 242702809
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "801349920196296704",
    "geo" : { },
    "id_str" : "801362403405561860",
    "in_reply_to_user_id" : 108597502,
    "text" : "@oconnellbrian @TodaySOR This is not PC. But why be a mother knowing you don't have a home for your child?",
    "id" : 801362403405561860,
    "in_reply_to_status_id" : 801349920196296704,
    "created_at" : "2016-11-23 09:51:00 +0000",
    "in_reply_to_screen_name" : "oconnellbrian",
    "in_reply_to_user_id_str" : "108597502",
    "user" : {
      "name" : "Front Line (Retd)",
      "screen_name" : "GalliganChristy",
      "protected" : false,
      "id_str" : "773380357",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/703690916889038849\/-8XO3ZeR_normal.jpg",
      "id" : 773380357,
      "verified" : false
    }
  },
  "id" : 801370815929401344,
  "created_at" : "2016-11-23 10:24:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801368575764295681",
  "text" : "Back home with personal income tax rate of 22%, you can put aside a 20% down payment for an apartment.",
  "id" : 801368575764295681,
  "created_at" : "2016-11-23 10:15:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    }, {
      "name" : "RT\u00C9 Radio 1",
      "screen_name" : "RTERadio1",
      "indices" : [ 84, 94 ],
      "id_str" : "57336315",
      "id" : 57336315
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801367022424702976",
  "text" : "RT @EimearMcCormack: How on earth can you save \u20AC20k a year with the rental crisis?? @RTERadio1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RT\u00C9 Radio 1",
        "screen_name" : "RTERadio1",
        "indices" : [ 63, 73 ],
        "id_str" : "57336315",
        "id" : 57336315
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "801366883236769792",
    "text" : "How on earth can you save \u20AC20k a year with the rental crisis?? @RTERadio1",
    "id" : 801366883236769792,
    "created_at" : "2016-11-23 10:08:48 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 801367022424702976,
  "created_at" : "2016-11-23 10:09:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/RBKSrp1KIH",
      "expanded_url" : "https:\/\/blog.google\/products\/g-suite\/totally-rebuilt-sites-customer-tested\/",
      "display_url" : "blog.google\/products\/g-sui\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801322281532293121",
  "text" : "The new version of Google Site, a drag-and-drop website builder goes live https:\/\/t.co\/RBKSrp1KIH",
  "id" : 801322281532293121,
  "created_at" : "2016-11-23 07:11:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bloomberg",
      "screen_name" : "business",
      "indices" : [ 3, 12 ],
      "id_str" : "34713362",
      "id" : 34713362
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/jjbRz4gqLr",
      "expanded_url" : "http:\/\/bloom.bg\/2fnEd0J",
      "display_url" : "bloom.bg\/2fnEd0J"
    } ]
  },
  "geo" : { },
  "id_str" : "801319019378864132",
  "text" : "RT @business: This $100 billion Chinese-made city near Singapore \"scares the hell out of everybody\" https:\/\/t.co\/jjbRz4gqLr https:\/\/t.co\/zq\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/business\/status\/801286503834025984\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/zq3zHUjG1h",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cx6905hWgAEdqE2.jpg",
        "id_str" : "801286500210081793",
        "id" : 801286500210081793,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cx6905hWgAEdqE2.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1467,
          "resize" : "fit",
          "w" : 2200
        }, {
          "h" : 1366,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zq3zHUjG1h"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/jjbRz4gqLr",
        "expanded_url" : "http:\/\/bloom.bg\/2fnEd0J",
        "display_url" : "bloom.bg\/2fnEd0J"
      } ]
    },
    "geo" : { },
    "id_str" : "801286503834025984",
    "text" : "This $100 billion Chinese-made city near Singapore \"scares the hell out of everybody\" https:\/\/t.co\/jjbRz4gqLr https:\/\/t.co\/zq3zHUjG1h",
    "id" : 801286503834025984,
    "created_at" : "2016-11-23 04:49:24 +0000",
    "user" : {
      "name" : "Bloomberg",
      "screen_name" : "business",
      "protected" : false,
      "id_str" : "34713362",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991818020233404416\/alrBF_dr_normal.jpg",
      "id" : 34713362,
      "verified" : true
    }
  },
  "id" : 801319019378864132,
  "created_at" : "2016-11-23 06:58:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "wrapped in plastic",
      "screen_name" : "twinpeaksgiant",
      "indices" : [ 3, 18 ],
      "id_str" : "140562983",
      "id" : 140562983
    }, {
      "name" : "Photos of Dublin",
      "screen_name" : "PhotosOfDublin",
      "indices" : [ 20, 35 ],
      "id_str" : "1943866482",
      "id" : 1943866482
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/twinpeaksgiant\/status\/799214771627167744\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/cE3D9xfmrZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CxdhlefXAAANdPN.jpg",
      "id_str" : "799214755349200896",
      "id" : 799214755349200896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxdhlefXAAANdPN.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cE3D9xfmrZ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801152215566073857",
  "text" : "RT @twinpeaksgiant: @PhotosOfDublin so wet in dublin morning that this woman dressed her kids in bin bags https:\/\/t.co\/cE3D9xfmrZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Photos of Dublin",
        "screen_name" : "PhotosOfDublin",
        "indices" : [ 0, 15 ],
        "id_str" : "1943866482",
        "id" : 1943866482
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/twinpeaksgiant\/status\/799214771627167744\/photo\/1",
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/cE3D9xfmrZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxdhlefXAAANdPN.jpg",
        "id_str" : "799214755349200896",
        "id" : 799214755349200896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxdhlefXAAANdPN.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cE3D9xfmrZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "799214771627167744",
    "in_reply_to_user_id" : 1943866482,
    "text" : "@PhotosOfDublin so wet in dublin morning that this woman dressed her kids in bin bags https:\/\/t.co\/cE3D9xfmrZ",
    "id" : 799214771627167744,
    "created_at" : "2016-11-17 11:37:04 +0000",
    "in_reply_to_screen_name" : "PhotosOfDublin",
    "in_reply_to_user_id_str" : "1943866482",
    "user" : {
      "name" : "wrapped in plastic",
      "screen_name" : "twinpeaksgiant",
      "protected" : false,
      "id_str" : "140562983",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/548062831619239938\/9zq0tk-Y_normal.jpeg",
      "id" : 140562983,
      "verified" : false
    }
  },
  "id" : 801152215566073857,
  "created_at" : "2016-11-22 19:55:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/uB5FRnj3wn",
      "expanded_url" : "https:\/\/twitter.com\/stephenkinsella\/status\/800685610176385024",
      "display_url" : "twitter.com\/stephenkinsell\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801143754602270720",
  "text" : "\"...when public sector pay goes up, it has to be paid for using taxes, and this is true forever.\" https:\/\/t.co\/uB5FRnj3wn",
  "id" : 801143754602270720,
  "created_at" : "2016-11-22 19:22:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bojan Pancevski",
      "screen_name" : "bopanc",
      "indices" : [ 3, 10 ],
      "id_str" : "207441433",
      "id" : 207441433
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/bopanc\/status\/801120409911492610\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/LTnzhISyOZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cx4mZV5XgAA4BW3.jpg",
      "id_str" : "801120000534872064",
      "id" : 801120000534872064,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cx4mZV5XgAA4BW3.jpg",
      "sizes" : [ {
        "h" : 1071,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 607,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1828,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1828,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LTnzhISyOZ"
    } ],
    "hashtags" : [ {
      "text" : "Belgium",
      "indices" : [ 12, 20 ]
    }, {
      "text" : "Japan",
      "indices" : [ 24, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801142503713816576",
  "text" : "RT @bopanc: #Belgium vs #Japan. Roundabout Schuman best left unmentioned https:\/\/t.co\/LTnzhISyOZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bopanc\/status\/801120409911492610\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/LTnzhISyOZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cx4mZV5XgAA4BW3.jpg",
        "id_str" : "801120000534872064",
        "id" : 801120000534872064,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cx4mZV5XgAA4BW3.jpg",
        "sizes" : [ {
          "h" : 1071,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 607,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1828,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1828,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LTnzhISyOZ"
      } ],
      "hashtags" : [ {
        "text" : "Belgium",
        "indices" : [ 0, 8 ]
      }, {
        "text" : "Japan",
        "indices" : [ 12, 18 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "801120409911492610",
    "text" : "#Belgium vs #Japan. Roundabout Schuman best left unmentioned https:\/\/t.co\/LTnzhISyOZ",
    "id" : 801120409911492610,
    "created_at" : "2016-11-22 17:49:24 +0000",
    "user" : {
      "name" : "Bojan Pancevski",
      "screen_name" : "bopanc",
      "protected" : false,
      "id_str" : "207441433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/652120923567427584\/cyqqRoDA_normal.jpg",
      "id" : 207441433,
      "verified" : true
    }
  },
  "id" : 801142503713816576,
  "created_at" : "2016-11-22 19:17:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801132106831241216",
  "text" : "Having to produce an ID to avail to free healthcare is consider heavy handed wor...",
  "id" : 801132106831241216,
  "created_at" : "2016-11-22 18:35:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801089919116603392",
  "text" : "RT @yapphenghui: Humans are a plague on the Earth that need to be controlled by limiting population growth, Sir David Attenborough.\nhttps:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/6MXZg0yQQ8",
        "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/earth\/earthnews\/9815862\/Humans-are-plague-on-Earth-Attenborough.html",
        "display_url" : "telegraph.co.uk\/news\/earth\/ear\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "801070323814670336",
    "text" : "Humans are a plague on the Earth that need to be controlled by limiting population growth, Sir David Attenborough.\nhttps:\/\/t.co\/6MXZg0yQQ8",
    "id" : 801070323814670336,
    "created_at" : "2016-11-22 14:30:23 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 801089919116603392,
  "created_at" : "2016-11-22 15:48:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenData",
      "indices" : [ 98, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/TNxm1DCcm9",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/578fdd5fa74a28d278a3c4722ce21f3a",
      "display_url" : "gist.github.com\/mryap\/578fdd5f\u2026"
    }, {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/Sgfyp8RzEQ",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/801067466998874112",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801080924481818624",
  "text" : "Here how to use R to access those pesky PX file and to covert to CSV file https:\/\/t.co\/TNxm1DCcm9 #OpenData https:\/\/t.co\/Sgfyp8RzEQ",
  "id" : 801080924481818624,
  "created_at" : "2016-11-22 15:12:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801067466998874112",
  "text" : "Open Data not really that \"open\" if is in JSON and PX format.",
  "id" : 801067466998874112,
  "created_at" : "2016-11-22 14:19:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 3, 15 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801059733411164160",
  "text" : "RT @gianniponzi: Some shops in Wexford still close from 1-2pm. When people are on lunch. And it's the only time they have to do things. Tha\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "801058409047097346",
    "text" : "Some shops in Wexford still close from 1-2pm. When people are on lunch. And it's the only time they have to do things. That's mad Ted",
    "id" : 801058409047097346,
    "created_at" : "2016-11-22 13:43:02 +0000",
    "user" : {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "protected" : false,
      "id_str" : "19793936",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034160433949761536\/GdlrLPNt_normal.jpg",
      "id" : 19793936,
      "verified" : false
    }
  },
  "id" : 801059733411164160,
  "created_at" : "2016-11-22 13:48:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/qBq06kx23d",
      "expanded_url" : "http:\/\/azureplatform.azurewebsites.net\/en-us\/",
      "display_url" : "azureplatform.azurewebsites.net\/en-us\/"
    } ]
  },
  "geo" : { },
  "id_str" : "801059597993857024",
  "text" : "Interactive overview of services available on Microsoft Azure. Click on a service to learn about it. https:\/\/t.co\/qBq06kx23d",
  "id" : 801059597993857024,
  "created_at" : "2016-11-22 13:47:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/MkGE9si1tV",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/singaporeans-give-generously-to-help-pay-vietnamese-premature-babys-medical-bill-094850993.html",
      "display_url" : "sg.news.yahoo.com\/singaporeans-g\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "801058502475026433",
  "text" : "In Singapore, 11 days bills for a baby is SGD 268,000 (EUR 177,195.77) https:\/\/t.co\/MkGE9si1tV",
  "id" : 801058502475026433,
  "created_at" : "2016-11-22 13:43:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "801048165147410432",
  "text" : "I got \u20AC120 in free Google AdWords credit to be used. Contact me if you want to find out more.",
  "id" : 801048165147410432,
  "created_at" : "2016-11-22 13:02:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "800843206946656256",
  "text" : "Do you use SQL with R?",
  "id" : 800843206946656256,
  "created_at" : "2016-11-21 23:27:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/VE0K1sKSTk",
      "expanded_url" : "https:\/\/twitter.com\/RLadiesLondon\/status\/800774803942608898",
      "display_url" : "twitter.com\/RLadiesLondon\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "800840926717427712",
  "text" : "Some of these universities still use SPSS... https:\/\/t.co\/VE0K1sKSTk",
  "id" : 800840926717427712,
  "created_at" : "2016-11-21 23:18:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/QEjiEn69M0",
      "expanded_url" : "http:\/\/uk.businessinsider.com\/how-people-find-black-friday-deals-chart-2016-11",
      "display_url" : "uk.businessinsider.com\/how-people-fin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "800811143682027520",
  "text" : "These are the most popular ways people seek out Black Friday deals https:\/\/t.co\/QEjiEn69M0",
  "id" : 800811143682027520,
  "created_at" : "2016-11-21 21:20:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rome2rio",
      "screen_name" : "rome2rio",
      "indices" : [ 3, 12 ],
      "id_str" : "206580889",
      "id" : 206580889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "800726179145715712",
  "text" : "RT @rome2rio: Chris Forsyth delves into the architectural wonder of the world's transit systems in this beautiful photo essay. \nhttps:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/6Y4njppJDQ",
        "expanded_url" : "http:\/\/www.wallpaper.com\/art\/chris-forsyth-captures-europes-overlooked-underground-spaces",
        "display_url" : "wallpaper.com\/art\/chris-fors\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "800701688579493888",
    "text" : "Chris Forsyth delves into the architectural wonder of the world's transit systems in this beautiful photo essay. \nhttps:\/\/t.co\/6Y4njppJDQ",
    "id" : 800701688579493888,
    "created_at" : "2016-11-21 14:05:33 +0000",
    "user" : {
      "name" : "Rome2rio",
      "screen_name" : "rome2rio",
      "protected" : false,
      "id_str" : "206580889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998425486765146113\/RcriqZkf_normal.jpg",
      "id" : 206580889,
      "verified" : false
    }
  },
  "id" : 800726179145715712,
  "created_at" : "2016-11-21 15:42:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "800693028587470850",
  "text" : "RT @ronanlyons: Time to renew the car insurance. To avail of the same service in December as in November, I will need to pay 80% more. EIGH\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ronanlyons\/status\/800644615082676224\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/D3PpjeC20l",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/Cxx1-cVWgAEUMpi.jpg",
        "id_str" : "800644549383192577",
        "id" : 800644549383192577,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/Cxx1-cVWgAEUMpi.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 320,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 320,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 320,
          "resize" : "fit",
          "w" : 340
        }, {
          "h" : 320,
          "resize" : "fit",
          "w" : 340
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/D3PpjeC20l"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "800644615082676224",
    "text" : "Time to renew the car insurance. To avail of the same service in December as in November, I will need to pay 80% more. EIGHTY PERCENT! https:\/\/t.co\/D3PpjeC20l",
    "id" : 800644615082676224,
    "created_at" : "2016-11-21 10:18:46 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 800693028587470850,
  "created_at" : "2016-11-21 13:31:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emma McArdle",
      "screen_name" : "McArdlePhoto",
      "indices" : [ 3, 16 ],
      "id_str" : "4354408581",
      "id" : 4354408581
    }, {
      "name" : "Picture Ireland",
      "screen_name" : "PictureIreland",
      "indices" : [ 102, 117 ],
      "id_str" : "2190623134",
      "id" : 2190623134
    }, {
      "name" : "Barra Best",
      "screen_name" : "barrabest",
      "indices" : [ 118, 128 ],
      "id_str" : "228874251",
      "id" : 228874251
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Down",
      "indices" : [ 77, 82 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 93, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "800454950987976704",
  "text" : "RT @McArdlePhoto: Going to miss the autumn colours! Tollymore Forest, County #Down, Northern #Ireland @PictureIreland @barrabest https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Picture Ireland",
        "screen_name" : "PictureIreland",
        "indices" : [ 84, 99 ],
        "id_str" : "2190623134",
        "id" : 2190623134
      }, {
        "name" : "Barra Best",
        "screen_name" : "barrabest",
        "indices" : [ 100, 110 ],
        "id_str" : "228874251",
        "id" : 228874251
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/McArdlePhoto\/status\/800387580676214784\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/6e2cAJSqP5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxuMOz5W8AEM19F.jpg",
        "id_str" : "800387544865239041",
        "id" : 800387544865239041,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxuMOz5W8AEM19F.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6e2cAJSqP5"
      } ],
      "hashtags" : [ {
        "text" : "Down",
        "indices" : [ 59, 64 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 75, 83 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "800387580676214784",
    "text" : "Going to miss the autumn colours! Tollymore Forest, County #Down, Northern #Ireland @PictureIreland @barrabest https:\/\/t.co\/6e2cAJSqP5",
    "id" : 800387580676214784,
    "created_at" : "2016-11-20 17:17:24 +0000",
    "user" : {
      "name" : "Emma McArdle",
      "screen_name" : "McArdlePhoto",
      "protected" : false,
      "id_str" : "4354408581",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038477296137633796\/TJchp_P-_normal.jpg",
      "id" : 4354408581,
      "verified" : false
    }
  },
  "id" : 800454950987976704,
  "created_at" : "2016-11-20 21:45:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richie Oakley #andacyclist",
      "screen_name" : "roakleyIRL",
      "indices" : [ 3, 14 ],
      "id_str" : "116435072",
      "id" : 116435072
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "800349700377444353",
  "text" : "RT @roakleyIRL: Private sector and self-employed workers must wonder why their own issues are not as breathlessly reported on as public sec\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "798454232080908288",
    "text" : "Private sector and self-employed workers must wonder why their own issues are not as breathlessly reported on as public sector on RTE?",
    "id" : 798454232080908288,
    "created_at" : "2016-11-15 09:14:58 +0000",
    "user" : {
      "name" : "Richie Oakley #andacyclist",
      "screen_name" : "roakleyIRL",
      "protected" : false,
      "id_str" : "116435072",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/616688329266958338\/PFN_9tDo_normal.jpg",
      "id" : 116435072,
      "verified" : true
    }
  },
  "id" : 800349700377444353,
  "created_at" : "2016-11-20 14:46:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Rent Controller",
      "screen_name" : "RentController",
      "indices" : [ 3, 18 ],
      "id_str" : "4100470341",
      "id" : 4100470341
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/cCgpVBBsUS",
      "expanded_url" : "http:\/\/www.independent.ie\/business\/personal-finance\/property-mortgages\/a-housing-crisis-while-165000-houses-lie-vacant-35171636.html",
      "display_url" : "independent.ie\/business\/perso\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "800334119511199744",
  "text" : "RT @RentController: https:\/\/t.co\/cCgpVBBsUS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/cCgpVBBsUS",
        "expanded_url" : "http:\/\/www.independent.ie\/business\/personal-finance\/property-mortgages\/a-housing-crisis-while-165000-houses-lie-vacant-35171636.html",
        "display_url" : "independent.ie\/business\/perso\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "800277156794286080",
    "text" : "https:\/\/t.co\/cCgpVBBsUS",
    "id" : 800277156794286080,
    "created_at" : "2016-11-20 09:58:37 +0000",
    "user" : {
      "name" : "The Rent Controller",
      "screen_name" : "RentController",
      "protected" : false,
      "id_str" : "4100470341",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/663481026094698496\/OvypFW9I_normal.jpg",
      "id" : 4100470341,
      "verified" : false
    }
  },
  "id" : 800334119511199744,
  "created_at" : "2016-11-20 13:44:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "the painter flynn",
      "screen_name" : "thepainterflynn",
      "indices" : [ 3, 19 ],
      "id_str" : "58199261",
      "id" : 58199261
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/thepainterflynn\/status\/800327083494936577\/photo\/1",
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/BbnHbT4DBz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CxtVOq5W8AAwib5.jpg",
      "id_str" : "800327069309792256",
      "id" : 800327069309792256,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxtVOq5W8AAwib5.jpg",
      "sizes" : [ {
        "h" : 273,
        "resize" : "fit",
        "w" : 365
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 273,
        "resize" : "fit",
        "w" : 365
      }, {
        "h" : 273,
        "resize" : "fit",
        "w" : 365
      }, {
        "h" : 273,
        "resize" : "fit",
        "w" : 365
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BbnHbT4DBz"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "800333330264850432",
  "text" : "RT @thepainterflynn: Today in 1985  Microsoft Windows 1.0 is released. https:\/\/t.co\/BbnHbT4DBz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/thepainterflynn\/status\/800327083494936577\/photo\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/BbnHbT4DBz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxtVOq5W8AAwib5.jpg",
        "id_str" : "800327069309792256",
        "id" : 800327069309792256,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxtVOq5W8AAwib5.jpg",
        "sizes" : [ {
          "h" : 273,
          "resize" : "fit",
          "w" : 365
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 365
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 365
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 365
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BbnHbT4DBz"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "800327083494936577",
    "text" : "Today in 1985  Microsoft Windows 1.0 is released. https:\/\/t.co\/BbnHbT4DBz",
    "id" : 800327083494936577,
    "created_at" : "2016-11-20 13:17:00 +0000",
    "user" : {
      "name" : "the painter flynn",
      "screen_name" : "thepainterflynn",
      "protected" : false,
      "id_str" : "58199261",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/482159227616518144\/xLJHpiug_normal.jpeg",
      "id" : 58199261,
      "verified" : false
    }
  },
  "id" : 800333330264850432,
  "created_at" : "2016-11-20 13:41:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/LY86BG8rHZ",
      "expanded_url" : "https:\/\/twitter.com\/RoisinPrizeman\/status\/800248606003433472",
      "display_url" : "twitter.com\/RoisinPrizeman\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "800269239320006656",
  "text" : "Will this drive up rental in Dublin? https:\/\/t.co\/LY86BG8rHZ",
  "id" : 800269239320006656,
  "created_at" : "2016-11-20 09:27:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Treasa Lynch",
      "screen_name" : "treasa",
      "indices" : [ 3, 10 ],
      "id_str" : "14746983",
      "id" : 14746983
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/1tRk5nk05v",
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/800260664564609025",
      "display_url" : "twitter.com\/wef\/status\/800\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "800268725286998016",
  "text" : "RT @treasa: Ireland is not on this list. We need to be. https:\/\/t.co\/1tRk5nk05v",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/1tRk5nk05v",
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/800260664564609025",
        "display_url" : "twitter.com\/wef\/status\/800\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "800260805858037760",
    "text" : "Ireland is not on this list. We need to be. https:\/\/t.co\/1tRk5nk05v",
    "id" : 800260805858037760,
    "created_at" : "2016-11-20 08:53:38 +0000",
    "user" : {
      "name" : "Treasa Lynch",
      "screen_name" : "treasa",
      "protected" : false,
      "id_str" : "14746983",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/712342342674620417\/u6e44GHd_normal.jpg",
      "id" : 14746983,
      "verified" : false
    }
  },
  "id" : 800268725286998016,
  "created_at" : "2016-11-20 09:25:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Claire Isabel \u2122 \u2606 \uD83D\uDC28",
      "screen_name" : "claireisabel",
      "indices" : [ 3, 16 ],
      "id_str" : "24966334",
      "id" : 24966334
    }, {
      "name" : "Cond\u00E9 Nast Traveler",
      "screen_name" : "CNTraveler",
      "indices" : [ 85, 96 ],
      "id_str" : "17219108",
      "id" : 17219108
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/Yg5qBwmVbU",
      "expanded_url" : "http:\/\/www.cntraveler.com\/stories\/2016-04-07\/best-stationery-stores-in-tokyo-japan?mbid=social_twitter",
      "display_url" : "cntraveler.com\/stories\/2016-0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799955137624154112",
  "text" : "RT @claireisabel: The 10 Best Stationery Stores in Tokyo https:\/\/t.co\/Yg5qBwmVbU via @CNTraveler Just as well I am not in Japan. I love Sta\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Cond\u00E9 Nast Traveler",
        "screen_name" : "CNTraveler",
        "indices" : [ 67, 78 ],
        "id_str" : "17219108",
        "id" : 17219108
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/Yg5qBwmVbU",
        "expanded_url" : "http:\/\/www.cntraveler.com\/stories\/2016-04-07\/best-stationery-stores-in-tokyo-japan?mbid=social_twitter",
        "display_url" : "cntraveler.com\/stories\/2016-0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "720238389283258369",
    "text" : "The 10 Best Stationery Stores in Tokyo https:\/\/t.co\/Yg5qBwmVbU via @CNTraveler Just as well I am not in Japan. I love Stationery supplies.",
    "id" : 720238389283258369,
    "created_at" : "2016-04-13 13:13:08 +0000",
    "user" : {
      "name" : "Claire Isabel \u2122 \u2606 \uD83D\uDC28",
      "screen_name" : "claireisabel",
      "protected" : false,
      "id_str" : "24966334",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/589497779388489728\/cuzYa-1c_normal.jpg",
      "id" : 24966334,
      "verified" : false
    }
  },
  "id" : 799955137624154112,
  "created_at" : "2016-11-19 12:39:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/TGCDQTc4qg",
      "expanded_url" : "https:\/\/www.facebook.com\/groups\/minimalistbulletjournals",
      "display_url" : "facebook.com\/groups\/minimal\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799951994307878912",
  "text" : "People are spending more time setting up then actually using the journal? https:\/\/t.co\/TGCDQTc4qg",
  "id" : 799951994307878912,
  "created_at" : "2016-11-19 12:26:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/DosU8495W2",
      "expanded_url" : "http:\/\/datascientistworkbench.com",
      "display_url" : "datascientistworkbench.com"
    }, {
      "indices" : [ 118, 141 ],
      "url" : "https:\/\/t.co\/D7rK8Nfv1O",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/799245077759434754",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799950982025199616",
  "text" : "Still not done with this &amp; it over a week. Not installing anything. Running data tool at https:\/\/t.co\/DosU8495W2  https:\/\/t.co\/D7rK8Nfv1O",
  "id" : 799950982025199616,
  "created_at" : "2016-11-19 12:22:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/YxqoSXRCnV",
      "expanded_url" : "https:\/\/twitter.com\/datagovsg\/status\/799469042465546240",
      "display_url" : "twitter.com\/datagovsg\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799917533352566784",
  "text" : "The less \"posh\" looking food-stall back home are usually the most delicious. https:\/\/t.co\/YxqoSXRCnV",
  "id" : 799917533352566784,
  "created_at" : "2016-11-19 10:09:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MIT Technology Review",
      "screen_name" : "techreview",
      "indices" : [ 3, 14 ],
      "id_str" : "15808647",
      "id" : 15808647
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "799882201357807616",
  "text" : "RT @techreview: Google has released some seemingly silly but actually very useful games to help make AI less mysterious. https:\/\/t.co\/4jFi7\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/4jFi7RCAfh",
        "expanded_url" : "http:\/\/trib.al\/DWtrEWA",
        "display_url" : "trib.al\/DWtrEWA"
      } ]
    },
    "geo" : { },
    "id_str" : "799737293183459328",
    "text" : "Google has released some seemingly silly but actually very useful games to help make AI less mysterious. https:\/\/t.co\/4jFi7RCAfh",
    "id" : 799737293183459328,
    "created_at" : "2016-11-18 22:13:23 +0000",
    "user" : {
      "name" : "MIT Technology Review",
      "screen_name" : "techreview",
      "protected" : false,
      "id_str" : "15808647",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875400785294532608\/feOza3es_normal.jpg",
      "id" : 15808647,
      "verified" : true
    }
  },
  "id" : 799882201357807616,
  "created_at" : "2016-11-19 07:49:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nature is Scary",
      "screen_name" : "NatureisScary",
      "indices" : [ 3, 17 ],
      "id_str" : "2458781599",
      "id" : 2458781599
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/NatureisScary\/status\/799811186921500672\/photo\/1",
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/dYfzz4ugG4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CxmACOWXAAAOnsd.jpg",
      "id_str" : "799811184534945792",
      "id" : 799811184534945792,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxmACOWXAAAOnsd.jpg",
      "sizes" : [ {
        "h" : 643,
        "resize" : "fit",
        "w" : 964
      }, {
        "h" : 643,
        "resize" : "fit",
        "w" : 964
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 643,
        "resize" : "fit",
        "w" : 964
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dYfzz4ugG4"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "799881948260945920",
  "text" : "RT @NatureisScary: A job whale done https:\/\/t.co\/dYfzz4ugG4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/NatureisScary\/status\/799811186921500672\/photo\/1",
        "indices" : [ 17, 40 ],
        "url" : "https:\/\/t.co\/dYfzz4ugG4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxmACOWXAAAOnsd.jpg",
        "id_str" : "799811184534945792",
        "id" : 799811184534945792,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxmACOWXAAAOnsd.jpg",
        "sizes" : [ {
          "h" : 643,
          "resize" : "fit",
          "w" : 964
        }, {
          "h" : 643,
          "resize" : "fit",
          "w" : 964
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 643,
          "resize" : "fit",
          "w" : 964
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dYfzz4ugG4"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "799811186921500672",
    "text" : "A job whale done https:\/\/t.co\/dYfzz4ugG4",
    "id" : 799811186921500672,
    "created_at" : "2016-11-19 03:07:01 +0000",
    "user" : {
      "name" : "Nature is Scary",
      "screen_name" : "NatureisScary",
      "protected" : false,
      "id_str" : "2458781599",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/774789870673731584\/f0pfhpvR_normal.jpg",
      "id" : 2458781599,
      "verified" : false
    }
  },
  "id" : 799881948260945920,
  "created_at" : "2016-11-19 07:48:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startups",
      "indices" : [ 55, 64 ]
    }, {
      "text" : "DigitalGov",
      "indices" : [ 120, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "799881749534769152",
  "text" : "RT @GovTechSG: Singapore one of several govts to apply #startups' principles of innovation to transform public services #DigitalGov https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "startups",
        "indices" : [ 40, 49 ]
      }, {
        "text" : "DigitalGov",
        "indices" : [ 105, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/0dv4vgqsGI",
        "expanded_url" : "https:\/\/www.theguardian.com\/public-leaders-network\/2016\/nov\/17\/public-bodies-copy-silicon-valley-tech-digital-services?CMP=share_btn_fb",
        "display_url" : "theguardian.com\/public-leaders\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "799859166076542976",
    "text" : "Singapore one of several govts to apply #startups' principles of innovation to transform public services #DigitalGov https:\/\/t.co\/0dv4vgqsGI",
    "id" : 799859166076542976,
    "created_at" : "2016-11-19 06:17:40 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 799881749534769152,
  "created_at" : "2016-11-19 07:47:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoogleAlerts",
      "indices" : [ 83, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/ueICPevQ6U",
      "expanded_url" : "http:\/\/goo.gl\/alerts\/Z4ufs",
      "display_url" : "goo.gl\/alerts\/Z4ufs"
    } ]
  },
  "geo" : { },
  "id_str" : "799632233418149888",
  "text" : "Singapore is beating the rest of Asia on one key measure - https:\/\/t.co\/ueICPevQ6U #GoogleAlerts",
  "id" : 799632233418149888,
  "created_at" : "2016-11-18 15:15:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DW News",
      "screen_name" : "dwnews",
      "indices" : [ 3, 10 ],
      "id_str" : "6134882",
      "id" : 6134882
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/dwnews\/status\/799352159947001856\/photo\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/8LhfAKhkes",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CxfUlBvXAAA9NOs.jpg",
      "id_str" : "799341191468875776",
      "id" : 799341191468875776,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxfUlBvXAAA9NOs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 940
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 940
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 940
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8LhfAKhkes"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/qfph6gVYwx",
      "expanded_url" : "http:\/\/dw.com\/p\/2SrQM?maca=en-tco-dw",
      "display_url" : "dw.com\/p\/2SrQM?maca=e\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799408715535450112",
  "text" : "RT @dwnews: Merkel calls for loosening of 'restrictive' German data protection laws https:\/\/t.co\/qfph6gVYwx https:\/\/t.co\/8LhfAKhkes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dwnews\/status\/799352159947001856\/photo\/1",
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/8LhfAKhkes",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxfUlBvXAAA9NOs.jpg",
        "id_str" : "799341191468875776",
        "id" : 799341191468875776,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxfUlBvXAAA9NOs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 940
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 940
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 940
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8LhfAKhkes"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/qfph6gVYwx",
        "expanded_url" : "http:\/\/dw.com\/p\/2SrQM?maca=en-tco-dw",
        "display_url" : "dw.com\/p\/2SrQM?maca=e\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "799352159947001856",
    "text" : "Merkel calls for loosening of 'restrictive' German data protection laws https:\/\/t.co\/qfph6gVYwx https:\/\/t.co\/8LhfAKhkes",
    "id" : 799352159947001856,
    "created_at" : "2016-11-17 20:43:00 +0000",
    "user" : {
      "name" : "DW News",
      "screen_name" : "dwnews",
      "protected" : false,
      "id_str" : "6134882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/900261211509489666\/-1Fu5hU8_normal.jpg",
      "id" : 6134882,
      "verified" : true
    }
  },
  "id" : 799408715535450112,
  "created_at" : "2016-11-18 00:27:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CloudControl",
      "indices" : [ 53, 66 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "799395500441079812",
  "text" : "No idea who the presenter. Going to google the name. #CloudControl",
  "id" : 799395500441079812,
  "created_at" : "2016-11-17 23:35:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/axIVXL5KoM",
      "expanded_url" : "https:\/\/blog.docker.com\/2016\/03\/containers-are-not-vms\/",
      "display_url" : "blog.docker.com\/2016\/03\/contai\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799319607031070720",
  "text" : "You might hear this term \"Docker\" among Data people. Here is a good house metaphor that explain Docker. : https:\/\/t.co\/axIVXL5KoM",
  "id" : 799319607031070720,
  "created_at" : "2016-11-17 18:33:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DW News",
      "screen_name" : "dwnews",
      "indices" : [ 3, 10 ],
      "id_str" : "6134882",
      "id" : 6134882
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Merkel",
      "indices" : [ 12, 19 ]
    }, {
      "text" : "Trump",
      "indices" : [ 44, 50 ]
    }, {
      "text" : "ObamaBerlin",
      "indices" : [ 87, 99 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "799298410830401536",
  "text" : "RT @dwnews: #Merkel: I aproach the incoming #Trump administration with \"an open mind.\" #ObamaBerlin",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Merkel",
        "indices" : [ 0, 7 ]
      }, {
        "text" : "Trump",
        "indices" : [ 32, 38 ]
      }, {
        "text" : "ObamaBerlin",
        "indices" : [ 75, 87 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "799295804276174848",
    "geo" : { },
    "id_str" : "799296202147643393",
    "in_reply_to_user_id" : 6134882,
    "text" : "#Merkel: I aproach the incoming #Trump administration with \"an open mind.\" #ObamaBerlin",
    "id" : 799296202147643393,
    "in_reply_to_status_id" : 799295804276174848,
    "created_at" : "2016-11-17 17:00:39 +0000",
    "in_reply_to_screen_name" : "dwnews",
    "in_reply_to_user_id_str" : "6134882",
    "user" : {
      "name" : "DW News",
      "screen_name" : "dwnews",
      "protected" : false,
      "id_str" : "6134882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/900261211509489666\/-1Fu5hU8_normal.jpg",
      "id" : 6134882,
      "verified" : true
    }
  },
  "id" : 799298410830401536,
  "created_at" : "2016-11-17 17:09:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 137, 160 ],
      "url" : "https:\/\/t.co\/u1uNqSU3Tb",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/798636325100322817",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799245077759434754",
  "text" : "Connect only the new SSD, boot from Windows 7 DVD, at Windows Setup screen, SHIFT+F10 to use diskpart to make SSD bootable. Any caveats? https:\/\/t.co\/u1uNqSU3Tb",
  "id" : 799245077759434754,
  "created_at" : "2016-11-17 13:37:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/3SqECBc6H3",
      "expanded_url" : "https:\/\/blog.asana.com\/2016\/11\/introducing-boards\/",
      "display_url" : "blog.asana.com\/2016\/11\/introd\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "799234571132751872",
  "text" : "Asana introduce Kanban style board https:\/\/t.co\/3SqECBc6H3",
  "id" : 799234571132751872,
  "created_at" : "2016-11-17 12:55:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adam Bienkov",
      "screen_name" : "AdamBienkov",
      "indices" : [ 3, 15 ],
      "id_str" : "14476016",
      "id" : 14476016
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "799224431654666240",
  "text" : "RT @AdamBienkov: Immigrants: Attacked for not working, attacked for working. Attacked when unemployment goes up, attacked when unemployment\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AdamBienkov\/status\/799027741584670721\/photo\/1",
        "indices" : [ 134, 157 ],
        "url" : "https:\/\/t.co\/8upx4EgnBF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cxa3d8rXAAAmju5.jpg",
        "id_str" : "799027709036920832",
        "id" : 799027709036920832,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cxa3d8rXAAAmju5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1855,
          "resize" : "fit",
          "w" : 673
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 247
        }, {
          "h" : 1855,
          "resize" : "fit",
          "w" : 673
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 435
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8upx4EgnBF"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "799027741584670721",
    "text" : "Immigrants: Attacked for not working, attacked for working. Attacked when unemployment goes up, attacked when unemployment goes down. https:\/\/t.co\/8upx4EgnBF",
    "id" : 799027741584670721,
    "created_at" : "2016-11-16 23:13:53 +0000",
    "user" : {
      "name" : "Adam Bienkov",
      "screen_name" : "AdamBienkov",
      "protected" : false,
      "id_str" : "14476016",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989158759569281024\/GDZ5sVPc_normal.jpg",
      "id" : 14476016,
      "verified" : true
    }
  },
  "id" : 799224431654666240,
  "created_at" : "2016-11-17 12:15:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hannah Dreier",
      "screen_name" : "hannahdreier",
      "indices" : [ 3, 16 ],
      "id_str" : "92854623",
      "id" : 92854623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798990070543826944",
  "text" : "RT @hannahdreier: Woman arrested in Venezuela last week for strangling her newborn baby to death. She said she had no way to support a chil\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "798915750916399104",
    "text" : "Woman arrested in Venezuela last week for strangling her newborn baby to death. She said she had no way to support a child amid the crisis",
    "id" : 798915750916399104,
    "created_at" : "2016-11-16 15:48:52 +0000",
    "user" : {
      "name" : "Hannah Dreier",
      "screen_name" : "hannahdreier",
      "protected" : false,
      "id_str" : "92854623",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/878991384006377472\/sHTP7lS7_normal.jpg",
      "id" : 92854623,
      "verified" : true
    }
  },
  "id" : 798990070543826944,
  "created_at" : "2016-11-16 20:44:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/hu9xfPnlkz",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BM4LunqhJ9U\/",
      "display_url" : "instagram.com\/p\/BM4LunqhJ9U\/"
    } ]
  },
  "geo" : { },
  "id_str" : "798926845307592705",
  "text" : "From garden city to city in a garden https:\/\/t.co\/hu9xfPnlkz",
  "id" : 798926845307592705,
  "created_at" : "2016-11-16 16:32:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Gogglebox",
      "indices" : [ 88, 98 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798662272721686529",
  "text" : "She (Hillary) should email to congratulate him on election victory on a private server. #Gogglebox LoL",
  "id" : 798662272721686529,
  "created_at" : "2016-11-15 23:01:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Gogglebox",
      "indices" : [ 25, 35 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798661336292990976",
  "text" : "Her leg is every where.  #Gogglebox",
  "id" : 798661336292990976,
  "created_at" : "2016-11-15 22:57:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Next Stage",
      "screen_name" : "Nextstagenow",
      "indices" : [ 3, 16 ],
      "id_str" : "2248626686",
      "id" : 2248626686
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Nextstagenow\/status\/794138183051149312\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/pzoTLa45sz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwVYd5FXcAA7o0J.jpg",
      "id_str" : "794138179863539712",
      "id" : 794138179863539712,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwVYd5FXcAA7o0J.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1127,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1127,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 639,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1127,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/pzoTLa45sz"
    } ],
    "hashtags" : [ {
      "text" : "smartphone",
      "indices" : [ 67, 78 ]
    }, {
      "text" : "brain",
      "indices" : [ 79, 85 ]
    }, {
      "text" : "body",
      "indices" : [ 86, 91 ]
    }, {
      "text" : "health",
      "indices" : [ 92, 99 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798648684992925696",
  "text" : "RT @Nextstagenow: How smartphone light affects your brain and body\n#smartphone #brain #body #health https:\/\/t.co\/pzoTLa45sz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Nextstagenow\/status\/794138183051149312\/photo\/1",
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/pzoTLa45sz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwVYd5FXcAA7o0J.jpg",
        "id_str" : "794138179863539712",
        "id" : 794138179863539712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwVYd5FXcAA7o0J.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1127,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1127,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 639,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1127,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/pzoTLa45sz"
      } ],
      "hashtags" : [ {
        "text" : "smartphone",
        "indices" : [ 49, 60 ]
      }, {
        "text" : "brain",
        "indices" : [ 61, 67 ]
      }, {
        "text" : "body",
        "indices" : [ 68, 73 ]
      }, {
        "text" : "health",
        "indices" : [ 74, 81 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "794138183051149312",
    "text" : "How smartphone light affects your brain and body\n#smartphone #brain #body #health https:\/\/t.co\/pzoTLa45sz",
    "id" : 794138183051149312,
    "created_at" : "2016-11-03 11:24:31 +0000",
    "user" : {
      "name" : "Next Stage",
      "screen_name" : "Nextstagenow",
      "protected" : false,
      "id_str" : "2248626686",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/412641811014766592\/0vv2mgBz_normal.png",
      "id" : 2248626686,
      "verified" : false
    }
  },
  "id" : 798648684992925696,
  "created_at" : "2016-11-15 22:07:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/Cz9jBOkNsx",
      "expanded_url" : "http:\/\/blogs.gartner.com\/martin-kihn\/data-management-platform\/",
      "display_url" : "blogs.gartner.com\/martin-kihn\/da\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "798648475906809856",
  "text" : "What Does a Data Management Platform Do, Anyway?https:\/\/t.co\/Cz9jBOkNsx",
  "id" : 798648475906809856,
  "created_at" : "2016-11-15 22:06:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798641840861958144",
  "text" : "So an organiser is offering a free pair of STANCE\u2122 socks when 3 or more coworkers or friends register and attend a summit...",
  "id" : 798641840861958144,
  "created_at" : "2016-11-15 21:40:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Miranda Whiting",
      "screen_name" : "MirandaWhiting",
      "indices" : [ 3, 18 ],
      "id_str" : "2332033299",
      "id" : 2332033299
    }, {
      "name" : "Tom Cox",
      "screen_name" : "cox_tom",
      "indices" : [ 99, 107 ],
      "id_str" : "35288455",
      "id" : 35288455
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/3rgahou8UZ",
      "expanded_url" : "https:\/\/www.gov.uk\/government\/news\/armitage-pet-care-flea-and-tick-drops-for-dogs-and-armitage-flea-drops-for-cats-and-kittens-product-defect-alert",
      "display_url" : "gov.uk\/government\/new\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "798639037980483584",
  "text" : "RT @MirandaWhiting: Please retweet: recall of flea drop treatment for cats\nhttps:\/\/t.co\/3rgahou8UZ @cox_tom",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tom Cox",
        "screen_name" : "cox_tom",
        "indices" : [ 79, 87 ],
        "id_str" : "35288455",
        "id" : 35288455
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/3rgahou8UZ",
        "expanded_url" : "https:\/\/www.gov.uk\/government\/news\/armitage-pet-care-flea-and-tick-drops-for-dogs-and-armitage-flea-drops-for-cats-and-kittens-product-defect-alert",
        "display_url" : "gov.uk\/government\/new\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "798610808997494785",
    "text" : "Please retweet: recall of flea drop treatment for cats\nhttps:\/\/t.co\/3rgahou8UZ @cox_tom",
    "id" : 798610808997494785,
    "created_at" : "2016-11-15 19:37:08 +0000",
    "user" : {
      "name" : "Miranda Whiting",
      "screen_name" : "MirandaWhiting",
      "protected" : false,
      "id_str" : "2332033299",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876449801625382912\/qucDQzGS_normal.jpg",
      "id" : 2332033299,
      "verified" : false
    }
  },
  "id" : 798639037980483584,
  "created_at" : "2016-11-15 21:29:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798636325100322817",
  "text" : "'Cloned' successfully from a HDD to SSD. Swapped in the SSD and get an 'operating system not found' error. Any advice? Thanks",
  "id" : 798636325100322817,
  "created_at" : "2016-11-15 21:18:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Theresa Reidy",
      "screen_name" : "theresareidy",
      "indices" : [ 3, 16 ],
      "id_str" : "126035531",
      "id" : 126035531
    }, {
      "name" : "The Business Post",
      "screen_name" : "sundaybusiness",
      "indices" : [ 79, 94 ],
      "id_str" : "17208934",
      "id" : 17208934
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/GOPrS4001K",
      "expanded_url" : "https:\/\/www.businesspost.ie\/opinion\/pay-restoration-unfair-demand-370279?utm_source=twitter&utm_campaign=article&utm_medium=web",
      "display_url" : "businesspost.ie\/opinion\/pay-re\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "798482538276802560",
  "text" : "RT @theresareidy: Pay restoration an unfair demand https:\/\/t.co\/GOPrS4001K via @sundaybusiness",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Business Post",
        "screen_name" : "sundaybusiness",
        "indices" : [ 61, 76 ],
        "id_str" : "17208934",
        "id" : 17208934
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 33, 56 ],
        "url" : "https:\/\/t.co\/GOPrS4001K",
        "expanded_url" : "https:\/\/www.businesspost.ie\/opinion\/pay-restoration-unfair-demand-370279?utm_source=twitter&utm_campaign=article&utm_medium=web",
        "display_url" : "businesspost.ie\/opinion\/pay-re\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "798476672710406144",
    "text" : "Pay restoration an unfair demand https:\/\/t.co\/GOPrS4001K via @sundaybusiness",
    "id" : 798476672710406144,
    "created_at" : "2016-11-15 10:44:08 +0000",
    "user" : {
      "name" : "Theresa Reidy",
      "screen_name" : "theresareidy",
      "protected" : false,
      "id_str" : "126035531",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/898661567549591553\/m1jOHLOK_normal.jpg",
      "id" : 126035531,
      "verified" : false
    }
  },
  "id" : 798482538276802560,
  "created_at" : "2016-11-15 11:07:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798481778428231680",
  "text" : "Should I upgrade from Windows 8.1 to Windows 10?",
  "id" : 798481778428231680,
  "created_at" : "2016-11-15 11:04:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/5DRtdgwz2e",
      "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/asiapacific\/japan-street-swallowed-by-giant-hole-reopens-in-a-week\/3290088.html#.WCrVCkpKnPg.twitter",
      "display_url" : "channelnewsasia.com\/news\/asiapacif\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "798467351196946432",
  "text" : "RT @mrbrown: Japan street swallowed by giant hole reopens in a week https:\/\/t.co\/5DRtdgwz2e Because Japan.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/5DRtdgwz2e",
        "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/asiapacific\/japan-street-swallowed-by-giant-hole-reopens-in-a-week\/3290088.html#.WCrVCkpKnPg.twitter",
        "display_url" : "channelnewsasia.com\/news\/asiapacif\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "798457478824402945",
    "text" : "Japan street swallowed by giant hole reopens in a week https:\/\/t.co\/5DRtdgwz2e Because Japan.",
    "id" : 798457478824402945,
    "created_at" : "2016-11-15 09:27:52 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 798467351196946432,
  "created_at" : "2016-11-15 10:07:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798338550462906368",
  "text" : "Someone's job title spotted on Linkedin \"Head of Happiness\"",
  "id" : 798338550462906368,
  "created_at" : "2016-11-15 01:35:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryanair",
      "screen_name" : "Ryanair",
      "indices" : [ 3, 11 ],
      "id_str" : "1542862735",
      "id" : 1542862735
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WinWednesday",
      "indices" : [ 26, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/2lnfmuQnEP",
      "expanded_url" : "http:\/\/bit.ly\/2f19LL9",
      "display_url" : "bit.ly\/2f19LL9"
    } ]
  },
  "geo" : { },
  "id_str" : "798310807679418368",
  "text" : "RT @Ryanair: It\u2019s Ryanair #WinWednesday! WIN the entire range. Simply like &amp;  RT to enter. See prizes here: https:\/\/t.co\/2lnfmuQnEP #Ryanai\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Ryanair\/status\/793848076301639680\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/Lnz7zG5xgx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwRQlhkXEAE1F8t.jpg",
        "id_str" : "793848039920308225",
        "id" : 793848039920308225,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwRQlhkXEAE1F8t.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 356,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Lnz7zG5xgx"
      } ],
      "hashtags" : [ {
        "text" : "WinWednesday",
        "indices" : [ 13, 26 ]
      }, {
        "text" : "RyanairInflight",
        "indices" : [ 123, 139 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/2lnfmuQnEP",
        "expanded_url" : "http:\/\/bit.ly\/2f19LL9",
        "display_url" : "bit.ly\/2f19LL9"
      } ]
    },
    "geo" : { },
    "id_str" : "793848076301639680",
    "text" : "It\u2019s Ryanair #WinWednesday! WIN the entire range. Simply like &amp;  RT to enter. See prizes here: https:\/\/t.co\/2lnfmuQnEP #RyanairInflight https:\/\/t.co\/Lnz7zG5xgx",
    "id" : 793848076301639680,
    "created_at" : "2016-11-02 16:11:45 +0000",
    "user" : {
      "name" : "Ryanair",
      "screen_name" : "Ryanair",
      "protected" : false,
      "id_str" : "1542862735",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/920680994788773888\/c5Of1Yvl_normal.jpg",
      "id" : 1542862735,
      "verified" : true
    }
  },
  "id" : 798310807679418368,
  "created_at" : "2016-11-14 23:45:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/qUwJ0BEnOA",
      "expanded_url" : "https:\/\/twitter.com\/jimkennedy\/status\/798117228985974785",
      "display_url" : "twitter.com\/jimkennedy\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "798153686019141632",
  "text" : "RT @klillington: Yes\u2026 same for Americans and Canadians here in Ireland. We are \u2018expats\u2019. https:\/\/t.co\/qUwJ0BEnOA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/qUwJ0BEnOA",
        "expanded_url" : "https:\/\/twitter.com\/jimkennedy\/status\/798117228985974785",
        "display_url" : "twitter.com\/jimkennedy\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "798121560662036480",
    "text" : "Yes\u2026 same for Americans and Canadians here in Ireland. We are \u2018expats\u2019. https:\/\/t.co\/qUwJ0BEnOA",
    "id" : 798121560662036480,
    "created_at" : "2016-11-14 11:13:03 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 798153686019141632,
  "created_at" : "2016-11-14 13:20:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/yT5pvlmHxP",
      "expanded_url" : "http:\/\/www.laptopmag.com\/articles\/ssd-upgrade-tutorial",
      "display_url" : "laptopmag.com\/articles\/ssd-u\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "798147292998107136",
  "text" : "Currently, I working on this https:\/\/t.co\/yT5pvlmHxP and moving video files are killing the fun.",
  "id" : 798147292998107136,
  "created_at" : "2016-11-14 12:55:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Harris TD",
      "screen_name" : "SimonHarrisTD",
      "indices" : [ 3, 17 ],
      "id_str" : "21117425",
      "id" : 21117425
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "798146051504893953",
  "text" : "RT @SimonHarrisTD: So delighted to see this in one of our supermarkets. Hope many more follow. Brilliant initiative - promoting inclusivity\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SimonHarrisTD\/status\/797844453738299392\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/Zj1hTwKiNb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxKDSvpWgAAvqB2.jpg",
        "id_str" : "797844442048790528",
        "id" : 797844442048790528,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxKDSvpWgAAvqB2.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Zj1hTwKiNb"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797844453738299392",
    "text" : "So delighted to see this in one of our supermarkets. Hope many more follow. Brilliant initiative - promoting inclusivity &amp; understanding https:\/\/t.co\/Zj1hTwKiNb",
    "id" : 797844453738299392,
    "created_at" : "2016-11-13 16:51:55 +0000",
    "user" : {
      "name" : "Simon Harris TD",
      "screen_name" : "SimonHarrisTD",
      "protected" : false,
      "id_str" : "21117425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978019128349900800\/Zaev06hX_normal.jpg",
      "id" : 21117425,
      "verified" : true
    }
  },
  "id" : 798146051504893953,
  "created_at" : "2016-11-14 12:50:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "indices" : [ 3, 17 ],
      "id_str" : "14157788",
      "id" : 14157788
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/iheartbeijing\/status\/797950888274984960\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/YMspqJMAOH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CxLkGnuXUAADFvX.jpg",
      "id_str" : "797950886391795712",
      "id" : 797950886391795712,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxLkGnuXUAADFvX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YMspqJMAOH"
    } ],
    "hashtags" : [ {
      "text" : "deer",
      "indices" : [ 64, 69 ]
    }, {
      "text" : "Michigan",
      "indices" : [ 70, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797956012988764164",
  "text" : "RT @iheartbeijing: So it looks like hunting season opened today #deer #Michigan https:\/\/t.co\/YMspqJMAOH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iheartbeijing\/status\/797950888274984960\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/YMspqJMAOH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxLkGnuXUAADFvX.jpg",
        "id_str" : "797950886391795712",
        "id" : 797950886391795712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxLkGnuXUAADFvX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YMspqJMAOH"
      } ],
      "hashtags" : [ {
        "text" : "deer",
        "indices" : [ 45, 50 ]
      }, {
        "text" : "Michigan",
        "indices" : [ 51, 60 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797950888274984960",
    "text" : "So it looks like hunting season opened today #deer #Michigan https:\/\/t.co\/YMspqJMAOH",
    "id" : 797950888274984960,
    "created_at" : "2016-11-13 23:54:51 +0000",
    "user" : {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "protected" : false,
      "id_str" : "14157788",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469468312292712448\/kKTKW0ON_normal.jpeg",
      "id" : 14157788,
      "verified" : false
    }
  },
  "id" : 797956012988764164,
  "created_at" : "2016-11-14 00:15:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/HwprJ3CBsN",
      "expanded_url" : "http:\/\/shr.gs\/Ym9UQfj",
      "display_url" : "shr.gs\/Ym9UQfj"
    } ]
  },
  "geo" : { },
  "id_str" : "797940298282364928",
  "text" : "The Web hit that was Lisbon shows Ireland what might have been https:\/\/t.co\/HwprJ3CBsN",
  "id" : 797940298282364928,
  "created_at" : "2016-11-13 23:12:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC Two",
      "screen_name" : "BBCTwo",
      "indices" : [ 3, 10 ],
      "id_str" : "1586183960",
      "id" : 1586183960
    }, {
      "name" : "BBC iPlayer",
      "screen_name" : "BBCiPlayer",
      "indices" : [ 98, 109 ],
      "id_str" : "17793580",
      "id" : 17793580
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BlackPM",
      "indices" : [ 81, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/b1JljiOlnd",
      "expanded_url" : "http:\/\/bbc.in\/2f6w6GC",
      "display_url" : "bbc.in\/2f6w6GC"
    } ]
  },
  "geo" : { },
  "id_str" : "797931848546402304",
  "text" : "RT @BBCTwo: Against all the odds, will Britain ever have a black Prime Minister? #BlackPM, now on @BBCiPlayer: https:\/\/t.co\/b1JljiOlnd #Bla\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC iPlayer",
        "screen_name" : "BBCiPlayer",
        "indices" : [ 86, 97 ],
        "id_str" : "17793580",
        "id" : 17793580
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BBCTwo\/status\/797923756811243521\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/t7o713fzEV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxK3MkRWgAA45WP.jpg",
        "id_str" : "797901510520766464",
        "id" : 797901510520766464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxK3MkRWgAA45WP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 384,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 440,
          "resize" : "fit",
          "w" : 780
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/t7o713fzEV"
      } ],
      "hashtags" : [ {
        "text" : "BlackPM",
        "indices" : [ 69, 77 ]
      }, {
        "text" : "BlackAndBritish",
        "indices" : [ 123, 139 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/b1JljiOlnd",
        "expanded_url" : "http:\/\/bbc.in\/2f6w6GC",
        "display_url" : "bbc.in\/2f6w6GC"
      } ]
    },
    "geo" : { },
    "id_str" : "797923756811243521",
    "text" : "Against all the odds, will Britain ever have a black Prime Minister? #BlackPM, now on @BBCiPlayer: https:\/\/t.co\/b1JljiOlnd #BlackAndBritish https:\/\/t.co\/t7o713fzEV",
    "id" : 797923756811243521,
    "created_at" : "2016-11-13 22:07:02 +0000",
    "user" : {
      "name" : "BBC Two",
      "screen_name" : "BBCTwo",
      "protected" : false,
      "id_str" : "1586183960",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998184813185056768\/rwlaj3Jx_normal.jpg",
      "id" : 1586183960,
      "verified" : true
    }
  },
  "id" : 797931848546402304,
  "created_at" : "2016-11-13 22:39:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rohan",
      "screen_name" : "RoAlleyn",
      "indices" : [ 3, 12 ],
      "id_str" : "27487993",
      "id" : 27487993
    }, {
      "name" : "BBC Two",
      "screen_name" : "BBCTwo",
      "indices" : [ 26, 33 ],
      "id_str" : "1586183960",
      "id" : 1586183960
    }, {
      "name" : "David Harewood",
      "screen_name" : "DavidHarewood",
      "indices" : [ 38, 52 ],
      "id_str" : "379286641",
      "id" : 379286641
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797931700000956416",
  "text" : "RT @RoAlleyn: Hats off to @BBCTwo and @DavidHarewood for highlighting these inequalities that most black ppl live and know to a wide audien\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC Two",
        "screen_name" : "BBCTwo",
        "indices" : [ 12, 19 ],
        "id_str" : "1586183960",
        "id" : 1586183960
      }, {
        "name" : "David Harewood",
        "screen_name" : "DavidHarewood",
        "indices" : [ 24, 38 ],
        "id_str" : "379286641",
        "id" : 379286641
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BlackPM",
        "indices" : [ 128, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797914925976821760",
    "text" : "Hats off to @BBCTwo and @DavidHarewood for highlighting these inequalities that most black ppl live and know to a wide audience #BlackPM",
    "id" : 797914925976821760,
    "created_at" : "2016-11-13 21:31:57 +0000",
    "user" : {
      "name" : "Rohan",
      "screen_name" : "RoAlleyn",
      "protected" : false,
      "id_str" : "27487993",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/929753324148154369\/XBQOfYGS_normal.jpg",
      "id" : 27487993,
      "verified" : false
    }
  },
  "id" : 797931700000956416,
  "created_at" : "2016-11-13 22:38:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    }, {
      "name" : "Independent.ie",
      "screen_name" : "Independent_ie",
      "indices" : [ 69, 84 ],
      "id_str" : "91334232",
      "id" : 91334232
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797818574844858372",
  "text" : "RT @ronanlyons: Following the latest grim rental figures, in today's @Independent_ie I discuss the main problem: construction costs in Irel\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Independent.ie",
        "screen_name" : "Independent_ie",
        "indices" : [ 53, 68 ],
        "id_str" : "91334232",
        "id" : 91334232
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/2kfnvS2b0Q",
        "expanded_url" : "https:\/\/twitter.com\/BRegsBlog\/status\/797760650642673665",
        "display_url" : "twitter.com\/BRegsBlog\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "797765866200989696",
    "text" : "Following the latest grim rental figures, in today's @Independent_ie I discuss the main problem: construction costs in Ireland vs elsewhere. https:\/\/t.co\/2kfnvS2b0Q",
    "id" : 797765866200989696,
    "created_at" : "2016-11-13 11:39:38 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 797818574844858372,
  "created_at" : "2016-11-13 15:09:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alibaba Group",
      "screen_name" : "AlibabaGroup",
      "indices" : [ 3, 16 ],
      "id_str" : "2694564780",
      "id" : 2694564780
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AlibabaGroup\/status\/797092395200512001\/photo\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/1iqZ9fo2fK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw_XTqCUoAALQAQ.jpg",
      "id_str" : "797092391769382912",
      "id" : 797092391769382912,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw_XTqCUoAALQAQ.jpg",
      "sizes" : [ {
        "h" : 360,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1iqZ9fo2fK"
    } ],
    "hashtags" : [ {
      "text" : "Double11",
      "indices" : [ 18, 27 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/uwB4ITTVAe",
      "expanded_url" : "http:\/\/ow.ly\/9e0S30655MG",
      "display_url" : "ow.ly\/9e0S30655MG"
    } ]
  },
  "geo" : { },
  "id_str" : "797800385763217409",
  "text" : "RT @AlibabaGroup: #Double11 https:\/\/t.co\/uwB4ITTVAe https:\/\/t.co\/1iqZ9fo2fK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AlibabaGroup\/status\/797092395200512001\/photo\/1",
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/1iqZ9fo2fK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw_XTqCUoAALQAQ.jpg",
        "id_str" : "797092391769382912",
        "id" : 797092391769382912,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw_XTqCUoAALQAQ.jpg",
        "sizes" : [ {
          "h" : 360,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1iqZ9fo2fK"
      } ],
      "hashtags" : [ {
        "text" : "Double11",
        "indices" : [ 0, 9 ]
      } ],
      "urls" : [ {
        "indices" : [ 10, 33 ],
        "url" : "https:\/\/t.co\/uwB4ITTVAe",
        "expanded_url" : "http:\/\/ow.ly\/9e0S30655MG",
        "display_url" : "ow.ly\/9e0S30655MG"
      } ]
    },
    "geo" : { },
    "id_str" : "797092395200512001",
    "text" : "#Double11 https:\/\/t.co\/uwB4ITTVAe https:\/\/t.co\/1iqZ9fo2fK",
    "id" : 797092395200512001,
    "created_at" : "2016-11-11 15:03:30 +0000",
    "user" : {
      "name" : "Alibaba Group",
      "screen_name" : "AlibabaGroup",
      "protected" : false,
      "id_str" : "2694564780",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922921690459283456\/rwVj6I1R_normal.jpg",
      "id" : 2694564780,
      "verified" : true
    }
  },
  "id" : 797800385763217409,
  "created_at" : "2016-11-13 13:56:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/0wGcuLDcqt",
      "expanded_url" : "http:\/\/str.sg\/4gHt",
      "display_url" : "str.sg\/4gHt"
    } ]
  },
  "geo" : { },
  "id_str" : "797799101668720640",
  "text" : "RT @STcom: JUST IN: First tsunami wave has arrived in NZ's South island after 7.8-magnitude quake https:\/\/t.co\/0wGcuLDcqt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 110 ],
        "url" : "https:\/\/t.co\/0wGcuLDcqt",
        "expanded_url" : "http:\/\/str.sg\/4gHt",
        "display_url" : "str.sg\/4gHt"
      } ]
    },
    "geo" : { },
    "id_str" : "797786722037690368",
    "text" : "JUST IN: First tsunami wave has arrived in NZ's South island after 7.8-magnitude quake https:\/\/t.co\/0wGcuLDcqt",
    "id" : 797786722037690368,
    "created_at" : "2016-11-13 13:02:31 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 797799101668720640,
  "created_at" : "2016-11-13 13:51:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "D\u00E9agl\u00E1n MacPh\u00E1id\u00EDn",
      "screen_name" : "declanmacfadden",
      "indices" : [ 3, 19 ],
      "id_str" : "19288130",
      "id" : 19288130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/uiIJBXOTyT",
      "expanded_url" : "https:\/\/twitter.com\/irish_graduates\/status\/797676464569057280",
      "display_url" : "twitter.com\/irish_graduate\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797742166701383680",
  "text" : "RT @declanmacfadden: Wow! https:\/\/t.co\/uiIJBXOTyT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 5, 28 ],
        "url" : "https:\/\/t.co\/uiIJBXOTyT",
        "expanded_url" : "https:\/\/twitter.com\/irish_graduates\/status\/797676464569057280",
        "display_url" : "twitter.com\/irish_graduate\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "797709225875173376",
    "text" : "Wow! https:\/\/t.co\/uiIJBXOTyT",
    "id" : 797709225875173376,
    "created_at" : "2016-11-13 07:54:34 +0000",
    "user" : {
      "name" : "D\u00E9agl\u00E1n MacPh\u00E1id\u00EDn",
      "screen_name" : "declanmacfadden",
      "protected" : false,
      "id_str" : "19288130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684614991492743168\/Rup-iIUi_normal.jpg",
      "id" : 19288130,
      "verified" : false
    }
  },
  "id" : 797742166701383680,
  "created_at" : "2016-11-13 10:05:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Catanese",
      "screen_name" : "davecatanese",
      "indices" : [ 3, 16 ],
      "id_str" : "15901113",
      "id" : 15901113
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797740637714083840",
  "text" : "RT @davecatanese: Literally just watched a couple break up over Trump vs Clinton at DC bar. She voted for Hillz, said bf didn't get her vie\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797647147768434688",
    "text" : "Literally just watched a couple break up over Trump vs Clinton at DC bar. She voted for Hillz, said bf didn't get her views. Guy walked out",
    "id" : 797647147768434688,
    "created_at" : "2016-11-13 03:47:54 +0000",
    "user" : {
      "name" : "David Catanese",
      "screen_name" : "davecatanese",
      "protected" : false,
      "id_str" : "15901113",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026338648143392768\/Xgj0XLPM_normal.jpg",
      "id" : 15901113,
      "verified" : true
    }
  },
  "id" : 797740637714083840,
  "created_at" : "2016-11-13 09:59:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "indices" : [ 3, 11 ],
      "id_str" : "137327916",
      "id" : 137327916
    }, {
      "name" : "Enterprise Ireland",
      "screen_name" : "Entirl",
      "indices" : [ 63, 70 ],
      "id_str" : "60575311",
      "id" : 60575311
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GECinD8\/status\/796730892756676608\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/4bW4h1EPEb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwWUj0LWIAAGA1R.jpg",
      "id_str" : "794204252323323904",
      "id" : 794204252323323904,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwWUj0LWIAAGA1R.jpg",
      "sizes" : [ {
        "h" : 326,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 982,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1766,
        "resize" : "fit",
        "w" : 3682
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4bW4h1EPEb"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/YxPrbmGUVO",
      "expanded_url" : "https:\/\/www.enterprise-ireland.com\/en\/Events\/OurEvents\/Exploring-Exporting\/Exploring-Exporting-Workshop-6-December-2016-Meath.html",
      "display_url" : "enterprise-ireland.com\/en\/Events\/OurE\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797539950057156609",
  "text" : "RT @GECinD8: Exploring Exporting Workshop on December 6th with @Entirl\n\nRead more: https:\/\/t.co\/YxPrbmGUVO https:\/\/t.co\/4bW4h1EPEb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Enterprise Ireland",
        "screen_name" : "Entirl",
        "indices" : [ 50, 57 ],
        "id_str" : "60575311",
        "id" : 60575311
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GECinD8\/status\/796730892756676608\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/4bW4h1EPEb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwWUj0LWIAAGA1R.jpg",
        "id_str" : "794204252323323904",
        "id" : 794204252323323904,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwWUj0LWIAAGA1R.jpg",
        "sizes" : [ {
          "h" : 326,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 982,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1766,
          "resize" : "fit",
          "w" : 3682
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/4bW4h1EPEb"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/YxPrbmGUVO",
        "expanded_url" : "https:\/\/www.enterprise-ireland.com\/en\/Events\/OurEvents\/Exploring-Exporting\/Exploring-Exporting-Workshop-6-December-2016-Meath.html",
        "display_url" : "enterprise-ireland.com\/en\/Events\/OurE\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796730892756676608",
    "text" : "Exploring Exporting Workshop on December 6th with @Entirl\n\nRead more: https:\/\/t.co\/YxPrbmGUVO https:\/\/t.co\/4bW4h1EPEb",
    "id" : 796730892756676608,
    "created_at" : "2016-11-10 15:07:02 +0000",
    "user" : {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "protected" : false,
      "id_str" : "137327916",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014899460806234119\/K16b7185_normal.jpg",
      "id" : 137327916,
      "verified" : false
    }
  },
  "id" : 797539950057156609,
  "created_at" : "2016-11-12 20:41:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797538495141183488",
  "text" : "If you can\u2019t describe what you are doing as a process, you don\u2019t know what you\u2019re doing.\n\n\u2014W. Edwards Deming",
  "id" : 797538495141183488,
  "created_at" : "2016-11-12 20:36:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon",
      "screen_name" : "51monfarrow",
      "indices" : [ 3, 15 ],
      "id_str" : "314740869",
      "id" : 314740869
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/51monfarrow\/status\/797395660337246208\/photo\/1",
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/KEaoM6DQlw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CxDrH68XEAAAwWv.jpg",
      "id_str" : "797395655358615552",
      "id" : 797395655358615552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxDrH68XEAAAwWv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 475,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 475,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 475,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 475,
        "resize" : "fit",
        "w" : 480
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KEaoM6DQlw"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797527917047717888",
  "text" : "RT @51monfarrow: Code reuse. https:\/\/t.co\/KEaoM6DQlw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/51monfarrow\/status\/797395660337246208\/photo\/1",
        "indices" : [ 12, 35 ],
        "url" : "https:\/\/t.co\/KEaoM6DQlw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CxDrH68XEAAAwWv.jpg",
        "id_str" : "797395655358615552",
        "id" : 797395655358615552,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CxDrH68XEAAAwWv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 475,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 475,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 475,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 475,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KEaoM6DQlw"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797395660337246208",
    "text" : "Code reuse. https:\/\/t.co\/KEaoM6DQlw",
    "id" : 797395660337246208,
    "created_at" : "2016-11-12 11:08:34 +0000",
    "user" : {
      "name" : "Simon",
      "screen_name" : "51monfarrow",
      "protected" : false,
      "id_str" : "314740869",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1390279440\/MOSTLY-MONKEY-square_normal.jpg",
      "id" : 314740869,
      "verified" : false
    }
  },
  "id" : 797527917047717888,
  "created_at" : "2016-11-12 19:54:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Foo \/ \u80E1",
      "screen_name" : "AgentFoo",
      "indices" : [ 3, 12 ],
      "id_str" : "14530209",
      "id" : 14530209
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797520089033674752",
  "text" : "RT @AgentFoo: After Brexit, harassment spiked. This is going to happen in America. Here's a guide to defusing harassment. https:\/\/t.co\/E1GR\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/E1GRVXUFRL",
        "expanded_url" : "http:\/\/fb.me\/Vfasob2Q",
        "display_url" : "fb.me\/Vfasob2Q"
      } ]
    },
    "geo" : { },
    "id_str" : "796317430381428736",
    "text" : "After Brexit, harassment spiked. This is going to happen in America. Here's a guide to defusing harassment. https:\/\/t.co\/E1GRVXUFRL",
    "id" : 796317430381428736,
    "created_at" : "2016-11-09 11:44:04 +0000",
    "user" : {
      "name" : "Foo \/ \u80E1",
      "screen_name" : "AgentFoo",
      "protected" : false,
      "id_str" : "14530209",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/834795180729434112\/3gpa3bby_normal.jpg",
      "id" : 14530209,
      "verified" : false
    }
  },
  "id" : 797520089033674752,
  "created_at" : "2016-11-12 19:23:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "indices" : [ 3, 15 ],
      "id_str" : "14479046",
      "id" : 14479046
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NotMyPresident",
      "indices" : [ 49, 64 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797505141993836544",
  "text" : "RT @daniel_kane: I honestly don't understand the #NotMyPresident protests. What is it you want? An end to democracy? You had a free electio\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "NotMyPresident",
        "indices" : [ 32, 47 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797372195412594689",
    "text" : "I honestly don't understand the #NotMyPresident protests. What is it you want? An end to democracy? You had a free election, Trump won.",
    "id" : 797372195412594689,
    "created_at" : "2016-11-12 09:35:20 +0000",
    "user" : {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "protected" : false,
      "id_str" : "14479046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959158926339137536\/dxkfCxt4_normal.jpg",
      "id" : 14479046,
      "verified" : false
    }
  },
  "id" : 797505141993836544,
  "created_at" : "2016-11-12 18:23:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "indices" : [ 3, 16 ],
      "id_str" : "20192882",
      "id" : 20192882
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797504803320590336",
  "text" : "RT @mikewhitmore: World population living in extreme poverty went from 94% of in 1820, down to 9.6% of the world population in 2015. https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/ZmaxDLDuYm",
        "expanded_url" : "https:\/\/ourworldindata.org\/grapher\/world-population-in-extreme-poverty-absolute?stackMode=relative",
        "display_url" : "ourworldindata.org\/grapher\/world-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "797436347573211136",
    "text" : "World population living in extreme poverty went from 94% of in 1820, down to 9.6% of the world population in 2015. https:\/\/t.co\/ZmaxDLDuYm",
    "id" : 797436347573211136,
    "created_at" : "2016-11-12 13:50:15 +0000",
    "user" : {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "protected" : false,
      "id_str" : "20192882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/814556566179352577\/J3-2QBfu_normal.jpg",
      "id" : 20192882,
      "verified" : false
    }
  },
  "id" : 797504803320590336,
  "created_at" : "2016-11-12 18:22:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tony Robinson",
      "screen_name" : "Tony_Robinson",
      "indices" : [ 3, 17 ],
      "id_str" : "2483329075",
      "id" : 2483329075
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797444027440365568",
  "text" : "RT @Tony_Robinson: When we filmed the final scene, we didn't know the impact it would have. So proud it's now taught in schools #RememberTh\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Tony_Robinson\/status\/797110611134005248\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/Ec3j0pmkUI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw_nOcqWgAE7ptL.jpg",
        "id_str" : "797109894465880065",
        "id" : 797109894465880065,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw_nOcqWgAE7ptL.jpg",
        "sizes" : [ {
          "h" : 388,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 388,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 388,
          "resize" : "fit",
          "w" : 620
        }, {
          "h" : 388,
          "resize" : "fit",
          "w" : 620
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ec3j0pmkUI"
      } ],
      "hashtags" : [ {
        "text" : "RememberThem",
        "indices" : [ 109, 122 ]
      }, {
        "text" : "ArmisticeDay",
        "indices" : [ 123, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797110611134005248",
    "text" : "When we filmed the final scene, we didn't know the impact it would have. So proud it's now taught in schools #RememberThem #ArmisticeDay https:\/\/t.co\/Ec3j0pmkUI",
    "id" : 797110611134005248,
    "created_at" : "2016-11-11 16:15:53 +0000",
    "user" : {
      "name" : "Tony Robinson",
      "screen_name" : "Tony_Robinson",
      "protected" : false,
      "id_str" : "2483329075",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/760753714529497088\/jgfephfY_normal.jpg",
      "id" : 2483329075,
      "verified" : true
    }
  },
  "id" : 797444027440365568,
  "created_at" : "2016-11-12 14:20:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "michiyo ishida",
      "screen_name" : "MichiyoCNA",
      "indices" : [ 3, 14 ],
      "id_str" : "2576264888",
      "id" : 2576264888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797443824297590784",
  "text" : "RT @MichiyoCNA: Too many cases of 80 plus drivers running over pedestrians, not only hurting them, but killing them. Today an 83 year old k\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "797370240849981440",
    "text" : "Too many cases of 80 plus drivers running over pedestrians, not only hurting them, but killing them. Today an 83 year old killed a couple",
    "id" : 797370240849981440,
    "created_at" : "2016-11-12 09:27:34 +0000",
    "user" : {
      "name" : "michiyo ishida",
      "screen_name" : "MichiyoCNA",
      "protected" : false,
      "id_str" : "2576264888",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/732518202882674690\/jFHZERUX_normal.jpg",
      "id" : 2576264888,
      "verified" : false
    }
  },
  "id" : 797443824297590784,
  "created_at" : "2016-11-12 14:19:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797211700936183809",
  "text" : "I should NEVER change BIOS settings now I got \"Operating system not found?\" after rebooting.",
  "id" : 797211700936183809,
  "created_at" : "2016-11-11 22:57:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797202785330692096",
  "text" : "I miss using Windows 7...",
  "id" : 797202785330692096,
  "created_at" : "2016-11-11 22:22:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/4GWwVZ4CMN",
      "expanded_url" : "http:\/\/www.kdd.org\/exploration_files\/V14-02-05-Amatriain.pdf",
      "display_url" : "kdd.org\/exploration_fi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797201864223817728",
  "text" : "A primer on using data mining &amp; machine learning for an effective recommendation system https:\/\/t.co\/4GWwVZ4CMN",
  "id" : 797201864223817728,
  "created_at" : "2016-11-11 22:18:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/butjwAUQbe",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BMrpQrNBhgQ\/",
      "display_url" : "instagram.com\/p\/BMrpQrNBhgQ\/"
    } ]
  },
  "geo" : { },
  "id_str" : "797162647569371136",
  "text" : "Swapped in a SSD and after booting up, I got this... https:\/\/t.co\/butjwAUQbe",
  "id" : 797162647569371136,
  "created_at" : "2016-11-11 19:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Goldfish.ie \uD83D\uDC21\uD83D\uDCF2\u260E\uFE0F\uD83D\uDCDE\uD83D\uDCBB\uD83D\uDDA5\uD83D\uDCE0\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "goldfishtelecom",
      "indices" : [ 3, 19 ],
      "id_str" : "156587336",
      "id" : 156587336
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797102919589969920",
  "text" : "RT @goldfishtelecom: Competition time: we're giving away a Samsung Galaxy Tab A to one of our followers. Follow us &amp; RT to enter :)\nhttps:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/goldfishtelecom\/status\/797102814510153728\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/2drmAnJiHv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw_ggN_WEAAwmGE.jpg",
        "id_str" : "797102503183650816",
        "id" : 797102503183650816,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw_ggN_WEAAwmGE.jpg",
        "sizes" : [ {
          "h" : 600,
          "resize" : "fit",
          "w" : 608
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 608
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 608
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 608
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2drmAnJiHv"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/lO7ofxy0FI",
        "expanded_url" : "http:\/\/blog.goldfish.ie\/2016\/11\/11\/win-a-samsung-galaxy-tab-a\/",
        "display_url" : "blog.goldfish.ie\/2016\/11\/11\/win\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "797102814510153728",
    "text" : "Competition time: we're giving away a Samsung Galaxy Tab A to one of our followers. Follow us &amp; RT to enter :)\nhttps:\/\/t.co\/lO7ofxy0FI https:\/\/t.co\/2drmAnJiHv",
    "id" : 797102814510153728,
    "created_at" : "2016-11-11 15:44:55 +0000",
    "user" : {
      "name" : "Goldfish.ie \uD83D\uDC21\uD83D\uDCF2\u260E\uFE0F\uD83D\uDCDE\uD83D\uDCBB\uD83D\uDDA5\uD83D\uDCE0\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "goldfishtelecom",
      "protected" : false,
      "id_str" : "156587336",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/819545373073141760\/-yo_sD5s_normal.jpg",
      "id" : 156587336,
      "verified" : false
    }
  },
  "id" : 797102919589969920,
  "created_at" : "2016-11-11 15:45:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/Ce0XfPHxaK",
      "expanded_url" : "https:\/\/www.facebook.com\/TheRSAF\/videos\/vb.236424069799458\/1006909766084214\/?type=3&theater",
      "display_url" : "facebook.com\/TheRSAF\/videos\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797082638347620352",
  "text" : "What it takes to turn a road into a runway https:\/\/t.co\/Ce0XfPHxaK",
  "id" : 797082638347620352,
  "created_at" : "2016-11-11 14:24:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/fL6fTkD6X8",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/taiwan-set-legalize-same-sex-marriages-first-asia-063713594.html?soc_src=social-sh&soc_trk=tw",
      "display_url" : "sg.news.yahoo.com\/taiwan-set-leg\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797081557504786432",
  "text" : "Taiwan set to legalize same-sex marriages, a first in Asia https:\/\/t.co\/fL6fTkD6X8",
  "id" : 797081557504786432,
  "created_at" : "2016-11-11 14:20:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mollie Goodfellow",
      "screen_name" : "hansmollman",
      "indices" : [ 3, 15 ],
      "id_str" : "86101978",
      "id" : 86101978
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/hansmollman\/status\/796723991499116545\/photo\/1",
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/sJIejb1rEN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw6IN7dWgAAjTjZ.jpg",
      "id_str" : "796723956971634688",
      "id" : 796723956971634688,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw6IN7dWgAAjTjZ.jpg",
      "sizes" : [ {
        "h" : 391,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 460,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 460,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 460,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sJIejb1rEN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "797062168374034433",
  "text" : "RT @hansmollman: Biden: ...so, when Trump walks in, you duck and I sock him wit-\nObama: No, Joe https:\/\/t.co\/sJIejb1rEN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/hansmollman\/status\/796723991499116545\/photo\/1",
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/sJIejb1rEN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw6IN7dWgAAjTjZ.jpg",
        "id_str" : "796723956971634688",
        "id" : 796723956971634688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw6IN7dWgAAjTjZ.jpg",
        "sizes" : [ {
          "h" : 391,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sJIejb1rEN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "796695644744404992",
    "geo" : { },
    "id_str" : "796723991499116545",
    "in_reply_to_user_id" : 86101978,
    "text" : "Biden: ...so, when Trump walks in, you duck and I sock him wit-\nObama: No, Joe https:\/\/t.co\/sJIejb1rEN",
    "id" : 796723991499116545,
    "in_reply_to_status_id" : 796695644744404992,
    "created_at" : "2016-11-10 14:39:36 +0000",
    "in_reply_to_screen_name" : "hansmollman",
    "in_reply_to_user_id_str" : "86101978",
    "user" : {
      "name" : "Mollie Goodfellow",
      "screen_name" : "hansmollman",
      "protected" : false,
      "id_str" : "86101978",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031334841957138433\/1S5f9PDb_normal.jpg",
      "id" : 86101978,
      "verified" : true
    }
  },
  "id" : 797062168374034433,
  "created_at" : "2016-11-11 13:03:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/fVGqbNs4d2",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/787358366792294404",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    }, {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/CNCI3Q3Xhv",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/797058271555256321",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797060683670097920",
  "text" : "The apartments pics https:\/\/t.co\/fVGqbNs4d2 https:\/\/t.co\/CNCI3Q3Xhv",
  "id" : 797060683670097920,
  "created_at" : "2016-11-11 12:57:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/oCPUxNbsml",
      "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/797057624395173888",
      "display_url" : "twitter.com\/EimearMcCormac\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "797058271555256321",
  "text" : "Well...its D4 area but it still bonkers.... https:\/\/t.co\/oCPUxNbsml",
  "id" : 797058271555256321,
  "created_at" : "2016-11-11 12:47:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Yakobovitch",
      "screen_name" : "davidyako",
      "indices" : [ 3, 13 ],
      "id_str" : "31603627",
      "id" : 31603627
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BadElectionDay",
      "indices" : [ 15, 30 ]
    }, {
      "text" : "DataScience",
      "indices" : [ 54, 66 ]
    }, {
      "text" : "USelection",
      "indices" : [ 67, 78 ]
    }, {
      "text" : "Bigdata",
      "indices" : [ 79, 87 ]
    }, {
      "text" : "MachineLearning",
      "indices" : [ 89, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/LSHRUFdMgZ",
      "expanded_url" : "http:\/\/buff.ly\/2fo8mP1",
      "display_url" : "buff.ly\/2fo8mP1"
    } ]
  },
  "geo" : { },
  "id_str" : "797055757283889152",
  "text" : "RT @davidyako: #BadElectionDay Forecasts Deal Blow to #DataScience #USelection #Bigdata  #MachineLearning\nhttps:\/\/t.co\/LSHRUFdMgZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BadElectionDay",
        "indices" : [ 0, 15 ]
      }, {
        "text" : "DataScience",
        "indices" : [ 39, 51 ]
      }, {
        "text" : "USelection",
        "indices" : [ 52, 63 ]
      }, {
        "text" : "Bigdata",
        "indices" : [ 64, 72 ]
      }, {
        "text" : "MachineLearning",
        "indices" : [ 74, 90 ]
      } ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/LSHRUFdMgZ",
        "expanded_url" : "http:\/\/buff.ly\/2fo8mP1",
        "display_url" : "buff.ly\/2fo8mP1"
      } ]
    },
    "geo" : { },
    "id_str" : "797055045120512000",
    "text" : "#BadElectionDay Forecasts Deal Blow to #DataScience #USelection #Bigdata  #MachineLearning\nhttps:\/\/t.co\/LSHRUFdMgZ",
    "id" : 797055045120512000,
    "created_at" : "2016-11-11 12:35:05 +0000",
    "user" : {
      "name" : "David Yakobovitch",
      "screen_name" : "davidyako",
      "protected" : false,
      "id_str" : "31603627",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/883500362962944000\/KCe4hKoP_normal.jpg",
      "id" : 31603627,
      "verified" : false
    }
  },
  "id" : 797055757283889152,
  "created_at" : "2016-11-11 12:37:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shane O'Farrell",
      "screen_name" : "shanefofarrell",
      "indices" : [ 3, 18 ],
      "id_str" : "268464821",
      "id" : 268464821
    }, {
      "name" : "Iarnr\u00F3d \u00C9ireann",
      "screen_name" : "IrishRail",
      "indices" : [ 86, 96 ],
      "id_str" : "15115986",
      "id" : 15115986
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796985455564746752",
  "text" : "RT @shanefofarrell: Beautiful early Friday morning sunrise over malahide estuary from @IrishRail Donabate train. No filter here. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Iarnr\u00F3d \u00C9ireann",
        "screen_name" : "IrishRail",
        "indices" : [ 66, 76 ],
        "id_str" : "15115986",
        "id" : 15115986
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/shanefofarrell\/status\/796976872630390784\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/4GArG3rwEy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw9uOhDWgAATQPI.jpg",
        "id_str" : "796976854737518592",
        "id" : 796976854737518592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw9uOhDWgAATQPI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/4GArG3rwEy"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796976872630390784",
    "text" : "Beautiful early Friday morning sunrise over malahide estuary from @IrishRail Donabate train. No filter here. https:\/\/t.co\/4GArG3rwEy",
    "id" : 796976872630390784,
    "created_at" : "2016-11-11 07:24:28 +0000",
    "user" : {
      "name" : "Shane O'Farrell",
      "screen_name" : "shanefofarrell",
      "protected" : false,
      "id_str" : "268464821",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1507840304\/image_normal.jpg",
      "id" : 268464821,
      "verified" : false
    }
  },
  "id" : 796985455564746752,
  "created_at" : "2016-11-11 07:58:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Asia Matters",
      "screen_name" : "asiamatters_biz",
      "indices" : [ 3, 19 ],
      "id_str" : "491708429",
      "id" : 491708429
    }, {
      "name" : "APIBF",
      "screen_name" : "APIBF2012",
      "indices" : [ 83, 93 ],
      "id_str" : "455229230",
      "id" : 455229230
    }, {
      "name" : "CIT",
      "screen_name" : "CIT_ie",
      "indices" : [ 94, 101 ],
      "id_str" : "189653715",
      "id" : 189653715
    }, {
      "name" : "Embassy of Ireland",
      "screen_name" : "IrlEmbChina",
      "indices" : [ 102, 114 ],
      "id_str" : "3372956596",
      "id" : 3372956596
    }, {
      "name" : "Alibaba Group",
      "screen_name" : "AlibabaGroup",
      "indices" : [ 115, 128 ],
      "id_str" : "2694564780",
      "id" : 2694564780
    }, {
      "name" : "Enterprise Ireland",
      "screen_name" : "Entirl",
      "indices" : [ 129, 136 ],
      "id_str" : "60575311",
      "id" : 60575311
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796982548937244672",
  "text" : "RT @asiamatters_biz: Singles Day: Alibaba reports record-breaking start - BBC News @APIBF2012 @CIT_ie @IrlEmbChina @AlibabaGroup @Entirl  h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "APIBF",
        "screen_name" : "APIBF2012",
        "indices" : [ 62, 72 ],
        "id_str" : "455229230",
        "id" : 455229230
      }, {
        "name" : "CIT",
        "screen_name" : "CIT_ie",
        "indices" : [ 73, 80 ],
        "id_str" : "189653715",
        "id" : 189653715
      }, {
        "name" : "Embassy of Ireland",
        "screen_name" : "IrlEmbChina",
        "indices" : [ 81, 93 ],
        "id_str" : "3372956596",
        "id" : 3372956596
      }, {
        "name" : "Alibaba Group",
        "screen_name" : "AlibabaGroup",
        "indices" : [ 94, 107 ],
        "id_str" : "2694564780",
        "id" : 2694564780
      }, {
        "name" : "Enterprise Ireland",
        "screen_name" : "Entirl",
        "indices" : [ 108, 115 ],
        "id_str" : "60575311",
        "id" : 60575311
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/ZhfT7GqSI4",
        "expanded_url" : "http:\/\/www.bbc.com\/news\/37946470",
        "display_url" : "bbc.com\/news\/37946470"
      } ]
    },
    "geo" : { },
    "id_str" : "796978725715869697",
    "text" : "Singles Day: Alibaba reports record-breaking start - BBC News @APIBF2012 @CIT_ie @IrlEmbChina @AlibabaGroup @Entirl  https:\/\/t.co\/ZhfT7GqSI4",
    "id" : 796978725715869697,
    "created_at" : "2016-11-11 07:31:50 +0000",
    "user" : {
      "name" : "Asia Matters",
      "screen_name" : "asiamatters_biz",
      "protected" : false,
      "id_str" : "491708429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/775714250392305664\/OoHdgZNl_normal.jpg",
      "id" : 491708429,
      "verified" : false
    }
  },
  "id" : 796982548937244672,
  "created_at" : "2016-11-11 07:47:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796982412307795968",
  "text" : "Bias-variance tradeoff is statistical fact of life.",
  "id" : 796982412307795968,
  "created_at" : "2016-11-11 07:46:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796974869606965248",
  "text" : "How do I find people that I follow on Linkedin but with who I am not connected?",
  "id" : 796974869606965248,
  "created_at" : "2016-11-11 07:16:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796950758151294976",
  "text" : "RT @yapphenghui: What kind of political movement is predicated on openly disdaining the very people it is advocating for? \nhttps:\/\/t.co\/T3d\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Peggy Miles",
        "screen_name" : "vox",
        "indices" : [ 134, 138 ],
        "id_str" : "1351341",
        "id" : 1351341
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/T3dNxaN7jQ",
        "expanded_url" : "http:\/\/www.vox.com\/2016\/4\/21\/11451378\/smug-american-liberalism?utm_campaign=vox&utm_content=feature%3Afixed&utm_medium=social&utm_source=twitter",
        "display_url" : "vox.com\/2016\/4\/21\/1145\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796932082496720896",
    "text" : "What kind of political movement is predicated on openly disdaining the very people it is advocating for? \nhttps:\/\/t.co\/T3dNxaN7jQ via @vox",
    "id" : 796932082496720896,
    "created_at" : "2016-11-11 04:26:29 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 796950758151294976,
  "created_at" : "2016-11-11 05:40:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Pyers",
      "screen_name" : "Gobiguy",
      "indices" : [ 3, 11 ],
      "id_str" : "14959069",
      "id" : 14959069
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/PMLEzyl7Hb",
      "expanded_url" : "http:\/\/fxn.ws\/2eWws2J",
      "display_url" : "fxn.ws\/2eWws2J"
    } ]
  },
  "geo" : { },
  "id_str" : "796949885081763840",
  "text" : "RT @Gobiguy: This clown missed his constitutional class in school.  Boss tells pro-Trump employees to resign |  https:\/\/t.co\/PMLEzyl7Hb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/PMLEzyl7Hb",
        "expanded_url" : "http:\/\/fxn.ws\/2eWws2J",
        "display_url" : "fxn.ws\/2eWws2J"
      } ]
    },
    "geo" : { },
    "id_str" : "796867962812497920",
    "text" : "This clown missed his constitutional class in school.  Boss tells pro-Trump employees to resign |  https:\/\/t.co\/PMLEzyl7Hb",
    "id" : 796867962812497920,
    "created_at" : "2016-11-11 00:11:42 +0000",
    "user" : {
      "name" : "James Pyers",
      "screen_name" : "Gobiguy",
      "protected" : false,
      "id_str" : "14959069",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/736082244792553473\/sQEt0hol_normal.jpg",
      "id" : 14959069,
      "verified" : false
    }
  },
  "id" : 796949885081763840,
  "created_at" : "2016-11-11 05:37:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/fwUTetvhtQ",
      "expanded_url" : "http:\/\/reut.rs\/2fFKzZY",
      "display_url" : "reut.rs\/2fFKzZY"
    } ]
  },
  "geo" : { },
  "id_str" : "796833544073084929",
  "text" : "Bird flu hits Europe, France raises checks to counter virus https:\/\/t.co\/fwUTetvhtQ",
  "id" : 796833544073084929,
  "created_at" : "2016-11-10 21:54:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "indices" : [ 3, 11 ],
      "id_str" : "15019743",
      "id" : 15019743
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/h5pmT4j3qU",
      "expanded_url" : "https:\/\/www.cnet.com\/news\/google-to-eu-youre-trying-to-wreck-androids-openness\/",
      "display_url" : "cnet.com\/news\/google-to\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796827211265347584",
  "text" : "RT @2bscene: https:\/\/t.co\/h5pmT4j3qU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/h5pmT4j3qU",
        "expanded_url" : "https:\/\/www.cnet.com\/news\/google-to-eu-youre-trying-to-wreck-androids-openness\/",
        "display_url" : "cnet.com\/news\/google-to\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796817436372856832",
    "text" : "https:\/\/t.co\/h5pmT4j3qU",
    "id" : 796817436372856832,
    "created_at" : "2016-11-10 20:50:55 +0000",
    "user" : {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "protected" : false,
      "id_str" : "15019743",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/779435193312608256\/NJW57jaX_normal.jpg",
      "id" : 15019743,
      "verified" : false
    }
  },
  "id" : 796827211265347584,
  "created_at" : "2016-11-10 21:29:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Theresa Fallon",
      "screen_name" : "TheresaAFallon",
      "indices" : [ 3, 18 ],
      "id_str" : "2407734505",
      "id" : 2407734505
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796714211825090560",
  "text" : "RT @TheresaAFallon: Election of Chinese official 2 head Interpol sparked concern about China\u2019s campaign 2 pursue dissidents around globe ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/6Smc9ywXgf",
        "expanded_url" : "https:\/\/www.theguardian.com\/world\/2016\/nov\/10\/new-interpol-head-is-chinese-former-head-of-paramilitary-police-force?CMP=share_btn_tw",
        "display_url" : "theguardian.com\/world\/2016\/nov\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796705407242797057",
    "text" : "Election of Chinese official 2 head Interpol sparked concern about China\u2019s campaign 2 pursue dissidents around globe https:\/\/t.co\/6Smc9ywXgf",
    "id" : 796705407242797057,
    "created_at" : "2016-11-10 13:25:45 +0000",
    "user" : {
      "name" : "Theresa Fallon",
      "screen_name" : "TheresaAFallon",
      "protected" : false,
      "id_str" : "2407734505",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/649602371648000000\/TR22zWWv_normal.png",
      "id" : 2407734505,
      "verified" : false
    }
  },
  "id" : 796714211825090560,
  "created_at" : "2016-11-10 14:00:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Debra Ruh",
      "screen_name" : "debraruh",
      "indices" : [ 3, 12 ],
      "id_str" : "17905508",
      "id" : 17905508
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/JaAQlMYGhF",
      "expanded_url" : "http:\/\/en.unesco.org\/news\/unesco-works-meet-huge-education-needs-displaced-students-iraq",
      "display_url" : "en.unesco.org\/news\/unesco-wo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796713819242369024",
  "text" : "RT @debraruh: UNESCO works to meet huge education needs of displaced students in Iraq https:\/\/t.co\/JaAQlMYGhF via s_cochet",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/JaAQlMYGhF",
        "expanded_url" : "http:\/\/en.unesco.org\/news\/unesco-works-meet-huge-education-needs-displaced-students-iraq",
        "display_url" : "en.unesco.org\/news\/unesco-wo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796712470836559872",
    "text" : "UNESCO works to meet huge education needs of displaced students in Iraq https:\/\/t.co\/JaAQlMYGhF via s_cochet",
    "id" : 796712470836559872,
    "created_at" : "2016-11-10 13:53:49 +0000",
    "user" : {
      "name" : "Debra Ruh",
      "screen_name" : "debraruh",
      "protected" : false,
      "id_str" : "17905508",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/860520946431545344\/YKEgsJ9H_normal.jpg",
      "id" : 17905508,
      "verified" : false
    }
  },
  "id" : 796713819242369024,
  "created_at" : "2016-11-10 13:59:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/vLpsgk4kSa",
      "expanded_url" : "http:\/\/str.sg\/4g8W",
      "display_url" : "str.sg\/4g8W"
    } ]
  },
  "geo" : { },
  "id_str" : "796712150458834944",
  "text" : "RT @STcom: Samsung washing machine catches fire in HDB flat, forcing mother and daughter to flee https:\/\/t.co\/vLpsgk4kSa https:\/\/t.co\/yIKIM\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/796656228986277888\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/yIKIMqIzLX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw5KjqiUsAELo7I.jpg",
        "id_str" : "796656160665284609",
        "id" : 796656160665284609,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw5KjqiUsAELo7I.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1456,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 484,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1960,
          "resize" : "fit",
          "w" : 2756
        }, {
          "h" : 853,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yIKIMqIzLX"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/796656228986277888\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/yIKIMqIzLX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cw5KkFYUAAAIodu.jpg",
        "id_str" : "796656167871053824",
        "id" : 796656167871053824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cw5KkFYUAAAIodu.jpg",
        "sizes" : [ {
          "h" : 1115,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1115,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1115,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 457
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yIKIMqIzLX"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/vLpsgk4kSa",
        "expanded_url" : "http:\/\/str.sg\/4g8W",
        "display_url" : "str.sg\/4g8W"
      } ]
    },
    "geo" : { },
    "id_str" : "796656228986277888",
    "text" : "Samsung washing machine catches fire in HDB flat, forcing mother and daughter to flee https:\/\/t.co\/vLpsgk4kSa https:\/\/t.co\/yIKIMqIzLX",
    "id" : 796656228986277888,
    "created_at" : "2016-11-10 10:10:20 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 796712150458834944,
  "created_at" : "2016-11-10 13:52:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/0il4VPi0pC",
      "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/796683604822736897",
      "display_url" : "twitter.com\/lisaocarroll\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796689295033692160",
  "text" : "So there is no place for vegetarian? https:\/\/t.co\/0il4VPi0pC",
  "id" : 796689295033692160,
  "created_at" : "2016-11-10 12:21:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/dENQnDeFXp",
      "expanded_url" : "http:\/\/str.sg\/4g7w",
      "display_url" : "str.sg\/4g7w"
    } ]
  },
  "geo" : { },
  "id_str" : "796666288852660224",
  "text" : "Google moves to new office to house fast growing team of engineers in Singapore https:\/\/t.co\/dENQnDeFXp",
  "id" : 796666288852660224,
  "created_at" : "2016-11-10 10:50:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 3, 12 ],
      "id_str" : "19456433",
      "id" : 19456433
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TrumpProtest",
      "indices" : [ 127, 140 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796656885164376064",
  "text" : "RT @jonboyes: How did we get to a point where so many fail to grasp one of the basic tenets of democracy: Sometimes, you lose. #TrumpProtest",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TrumpProtest",
        "indices" : [ 113, 126 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796624403786244097",
    "text" : "How did we get to a point where so many fail to grasp one of the basic tenets of democracy: Sometimes, you lose. #TrumpProtest",
    "id" : 796624403786244097,
    "created_at" : "2016-11-10 08:03:53 +0000",
    "user" : {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "protected" : false,
      "id_str" : "19456433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3386817686\/4b1b7303154ea92ea270a5afec16da70_normal.jpeg",
      "id" : 19456433,
      "verified" : false
    }
  },
  "id" : 796656885164376064,
  "created_at" : "2016-11-10 10:12:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "indices" : [ 3, 13 ],
      "id_str" : "27819449",
      "id" : 27819449
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "data",
      "indices" : [ 37, 42 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/nFYLFNVByE",
      "expanded_url" : "http:\/\/micfarris.us\/2fcM3xS",
      "display_url" : "micfarris.us\/2fcM3xS"
    } ]
  },
  "geo" : { },
  "id_str" : "796618700937904128",
  "text" : "RT @MicFarris: Why biotech is wooing #data scientists from tech companies https:\/\/t.co\/nFYLFNVByE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "data",
        "indices" : [ 22, 27 ]
      } ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/nFYLFNVByE",
        "expanded_url" : "http:\/\/micfarris.us\/2fcM3xS",
        "display_url" : "micfarris.us\/2fcM3xS"
      } ]
    },
    "geo" : { },
    "id_str" : "796352909936103424",
    "text" : "Why biotech is wooing #data scientists from tech companies https:\/\/t.co\/nFYLFNVByE",
    "id" : 796352909936103424,
    "created_at" : "2016-11-09 14:05:03 +0000",
    "user" : {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "protected" : false,
      "id_str" : "27819449",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3055549457\/94b094a6e85db296aa251b3a775d7439_normal.jpeg",
      "id" : 27819449,
      "verified" : false
    }
  },
  "id" : 796618700937904128,
  "created_at" : "2016-11-10 07:41:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Medha Basu",
      "screen_name" : "medha_basu",
      "indices" : [ 3, 14 ],
      "id_str" : "1127293040",
      "id" : 1127293040
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796611645107826688",
  "text" : "RT @medha_basu: Singapore is the first public service in the world to use Facebook for Work. Entire civil service to use by mid 2017 https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/DCYHhVrqU3",
        "expanded_url" : "https:\/\/govinsider.asia\/connected-gov\/entire-singapore-civil-service-to-use-facebook-for-work-by-2017-peter-ong\/",
        "display_url" : "govinsider.asia\/connected-gov\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796554993214095360",
    "text" : "Singapore is the first public service in the world to use Facebook for Work. Entire civil service to use by mid 2017 https:\/\/t.co\/DCYHhVrqU3",
    "id" : 796554993214095360,
    "created_at" : "2016-11-10 03:28:04 +0000",
    "user" : {
      "name" : "Medha Basu",
      "screen_name" : "medha_basu",
      "protected" : false,
      "id_str" : "1127293040",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/995245822060064768\/reORIkly_normal.jpg",
      "id" : 1127293040,
      "verified" : false
    }
  },
  "id" : 796611645107826688,
  "created_at" : "2016-11-10 07:13:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/LKDYYeRXNd",
      "expanded_url" : "https:\/\/twitter.com\/JessicaDringman\/status\/796463553276301312",
      "display_url" : "twitter.com\/JessicaDringma\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796498874374520833",
  "text" : "\"how you can get more diversity into your news feeds and still be true to yourself.\" Applicable to offline too. https:\/\/t.co\/LKDYYeRXNd",
  "id" : 796498874374520833,
  "created_at" : "2016-11-09 23:45:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Heleo",
      "screen_name" : "heleoworld",
      "indices" : [ 3, 14 ],
      "id_str" : "3319533317",
      "id" : 3319533317
    }, {
      "name" : "Cass Sunstein",
      "screen_name" : "CassSunstein",
      "indices" : [ 68, 81 ],
      "id_str" : "991745802",
      "id" : 991745802
    }, {
      "name" : "Caroline Webb",
      "screen_name" : "Caroline_Webb_",
      "indices" : [ 82, 97 ],
      "id_str" : "381148093",
      "id" : 381148093
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/TMf1fgphRo",
      "expanded_url" : "http:\/\/buff.ly\/2fEeVvQ",
      "display_url" : "buff.ly\/2fEeVvQ"
    } ]
  },
  "geo" : { },
  "id_str" : "796496853802041344",
  "text" : "RT @heleoworld: Tiny tweaks can have huge effects in people's lives @CassSunstein @Caroline_Webb_ https:\/\/t.co\/TMf1fgphRo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Cass Sunstein",
        "screen_name" : "CassSunstein",
        "indices" : [ 52, 65 ],
        "id_str" : "991745802",
        "id" : 991745802
      }, {
        "name" : "Caroline Webb",
        "screen_name" : "Caroline_Webb_",
        "indices" : [ 66, 81 ],
        "id_str" : "381148093",
        "id" : 381148093
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/TMf1fgphRo",
        "expanded_url" : "http:\/\/buff.ly\/2fEeVvQ",
        "display_url" : "buff.ly\/2fEeVvQ"
      } ]
    },
    "geo" : { },
    "id_str" : "796461892185231360",
    "text" : "Tiny tweaks can have huge effects in people's lives @CassSunstein @Caroline_Webb_ https:\/\/t.co\/TMf1fgphRo",
    "id" : 796461892185231360,
    "created_at" : "2016-11-09 21:18:07 +0000",
    "user" : {
      "name" : "Heleo",
      "screen_name" : "heleoworld",
      "protected" : false,
      "id_str" : "3319533317",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/631199605326282752\/deLakUo9_normal.png",
      "id" : 3319533317,
      "verified" : false
    }
  },
  "id" : 796496853802041344,
  "created_at" : "2016-11-09 23:37:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/uKvmsjLcry",
      "expanded_url" : "http:\/\/reut.rs\/2fT8jOm",
      "display_url" : "reut.rs\/2fT8jOm"
    }, {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/91DAl4Htmm",
      "expanded_url" : "https:\/\/www.mindef.gov.sg\/imindef\/press_room\/details.html?name=08nov16_mq&date=2016-11-08#.WCOYWWqLTIU",
      "display_url" : "mindef.gov.sg\/imindef\/press_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796469063220400130",
  "text" : "Singapore to buy Airbus and Boeing helicopters https:\/\/t.co\/uKvmsjLcry | https:\/\/t.co\/91DAl4Htmm",
  "id" : 796469063220400130,
  "created_at" : "2016-11-09 21:46:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dee Rooney",
      "screen_name" : "Deezer76",
      "indices" : [ 3, 12 ],
      "id_str" : "24978144",
      "id" : 24978144
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796451425047941120",
  "text" : "RT @Deezer76: Please help!! Hit and run outside Terenure college at 8.23ish this morning the Garda aren't much help I need to find a witnes\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796396948404240384",
    "text" : "Please help!! Hit and run outside Terenure college at 8.23ish this morning the Garda aren't much help I need to find a witness please RT",
    "id" : 796396948404240384,
    "created_at" : "2016-11-09 17:00:03 +0000",
    "user" : {
      "name" : "Dee Rooney",
      "screen_name" : "Deezer76",
      "protected" : false,
      "id_str" : "24978144",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009904923692142594\/w4gao7rq_normal.jpg",
      "id" : 24978144,
      "verified" : false
    }
  },
  "id" : 796451425047941120,
  "created_at" : "2016-11-09 20:36:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796451206570921985",
  "text" : "RT @ronanlyons: The US electorate may seem to be sexist and racist - but at least it's not ageist. This is the oldest president ever electe\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796422312241332224",
    "text" : "The US electorate may seem to be sexist and racist - but at least it's not ageist. This is the oldest president ever elected!",
    "id" : 796422312241332224,
    "created_at" : "2016-11-09 18:40:50 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 796451206570921985,
  "created_at" : "2016-11-09 20:35:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mitch Joel",
      "screen_name" : "mitchjoel",
      "indices" : [ 3, 13 ],
      "id_str" : "792907",
      "id" : 792907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796341423951880194",
  "text" : "RT @mitchjoel: Booked a rental car. Now, all I see are ads for that company. Everywhere. Ugh. This is what will drive the use of ad blocker\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "720641702776684544",
    "text" : "Booked a rental car. Now, all I see are ads for that company. Everywhere. Ugh. This is what will drive the use of ad blockers.",
    "id" : 720641702776684544,
    "created_at" : "2016-04-14 15:55:45 +0000",
    "user" : {
      "name" : "Mitch Joel",
      "screen_name" : "mitchjoel",
      "protected" : false,
      "id_str" : "792907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/856809193986248704\/x_FRFBo-_normal.jpg",
      "id" : 792907,
      "verified" : true
    }
  },
  "id" : 796341423951880194,
  "created_at" : "2016-11-09 13:19:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/OHtTe5DmTm",
      "expanded_url" : "https:\/\/www.facebook.com\/lianhewanbao\/photos\/a.132423140160421.25332.125584034177665\/1166158023453589\/?type=3&theater",
      "display_url" : "facebook.com\/lianhewanbao\/p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796339017599983616",
  "text" : "The world we live in https:\/\/t.co\/OHtTe5DmTm",
  "id" : 796339017599983616,
  "created_at" : "2016-11-09 13:09:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 3, 19 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796338481547116545",
  "text" : "RT @interactivemark: Things I have lost this morning:\n20% from my 401k\nFaith in humanity\nWill to live",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796305234821791744",
    "text" : "Things I have lost this morning:\n20% from my 401k\nFaith in humanity\nWill to live",
    "id" : 796305234821791744,
    "created_at" : "2016-11-09 10:55:37 +0000",
    "user" : {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "protected" : false,
      "id_str" : "14340736",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974702103984791552\/eHtIZ2ef_normal.jpg",
      "id" : 14340736,
      "verified" : false
    }
  },
  "id" : 796338481547116545,
  "created_at" : "2016-11-09 13:07:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 118, 141 ],
      "url" : "https:\/\/t.co\/Oyd7UENpuC",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/692248382295949312",
      "display_url" : "twitter.com\/mryap\/status\/6\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796330798970470400",
  "text" : "Food for thought today for someone like me seeking a career using technology to learn from data to make prediction... https:\/\/t.co\/Oyd7UENpuC",
  "id" : 796330798970470400,
  "created_at" : "2016-11-09 12:37:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Visit Dublin",
      "screen_name" : "VisitDublin",
      "indices" : [ 3, 15 ],
      "id_str" : "22053827",
      "id" : 22053827
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "KillineyHill",
      "indices" : [ 40, 53 ]
    }, {
      "text" : "Dublin",
      "indices" : [ 86, 93 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796315204556619776",
  "text" : "RT @VisitDublin: Enjoying the view from #KillineyHill! Take a short trip and discover #Dublin's stunning coastline \uD83D\uDCF7 IG\/ jurkopagac https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.falcon.io\" rel=\"nofollow\"\u003EFalcon Social Media Management \u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/VisitDublin\/status\/778670485307592704\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/PdZBODqjMH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cs5krY_WIAAYEyB.jpg",
        "id_str" : "778670482186969088",
        "id" : 778670482186969088,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cs5krY_WIAAYEyB.jpg",
        "sizes" : [ {
          "h" : 800,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 544
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PdZBODqjMH"
      } ],
      "hashtags" : [ {
        "text" : "KillineyHill",
        "indices" : [ 23, 36 ]
      }, {
        "text" : "Dublin",
        "indices" : [ 69, 76 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "778670485307592704",
    "text" : "Enjoying the view from #KillineyHill! Take a short trip and discover #Dublin's stunning coastline \uD83D\uDCF7 IG\/ jurkopagac https:\/\/t.co\/PdZBODqjMH",
    "id" : 778670485307592704,
    "created_at" : "2016-09-21 19:01:25 +0000",
    "user" : {
      "name" : "Visit Dublin",
      "screen_name" : "VisitDublin",
      "protected" : false,
      "id_str" : "22053827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1016324465670676486\/p_3f6kRw_normal.jpg",
      "id" : 22053827,
      "verified" : true
    }
  },
  "id" : 796315204556619776,
  "created_at" : "2016-11-09 11:35:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/RMkQkfskpC",
      "expanded_url" : "http:\/\/www.nytimes.com\/2014\/08\/18\/technology\/for-big-data-scientists-hurdle-to-insights-is-janitor-work.html",
      "display_url" : "nytimes.com\/2014\/08\/18\/tec\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796311268009398272",
  "text" : "NYT gives short shrift to this important task by calling it \"janitorial work\". \nhttps:\/\/t.co\/RMkQkfskpC",
  "id" : 796311268009398272,
  "created_at" : "2016-11-09 11:19:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/hpvMgLpUYp",
      "expanded_url" : "https:\/\/twitter.com\/RentMyLens",
      "display_url" : "twitter.com\/RentMyLens"
    } ]
  },
  "geo" : { },
  "id_str" : "796305248658800641",
  "text" : "My DSLR is broken. I can't insert the SD card into the camera. I renting out the lens. https:\/\/t.co\/hpvMgLpUYp",
  "id" : 796305248658800641,
  "created_at" : "2016-11-09 10:55:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/g03U4RZVwJ",
      "expanded_url" : "https:\/\/moz.com\/blog\/sampling-in-google-analytics",
      "display_url" : "moz.com\/blog\/sampling-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796301632170586112",
  "text" : "A Guide to Sampling in Google Analytics https:\/\/t.co\/g03U4RZVwJ",
  "id" : 796301632170586112,
  "created_at" : "2016-11-09 10:41:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "85053026",
      "id" : 85053026
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BeWinterReady",
      "indices" : [ 102, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796293287195844608",
  "text" : "RT @gavindbrown: It is fantastic to see such a success campaign continuing to develop year after year\n#BeWinterReady via:https:\/\/t.co\/YhCeh\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "OEP",
        "screen_name" : "emergencyIE",
        "indices" : [ 128, 140 ],
        "id_str" : "2150681726",
        "id" : 2150681726
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BeWinterReady",
        "indices" : [ 85, 99 ]
      } ],
      "urls" : [ {
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/YhCehJKnlI",
        "expanded_url" : "http:\/\/winterready.ie\/Be-Winter-Ready.aspx",
        "display_url" : "winterready.ie\/Be-Winter-Read\u2026"
      }, {
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/kDjuZccF7i",
        "expanded_url" : "https:\/\/twitter.com\/emergencyIE\/status\/796263831831986176",
        "display_url" : "twitter.com\/emergencyIE\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796289721324412928",
    "text" : "It is fantastic to see such a success campaign continuing to develop year after year\n#BeWinterReady via:https:\/\/t.co\/YhCehJKnlI @emergencyIE https:\/\/t.co\/kDjuZccF7i",
    "id" : 796289721324412928,
    "created_at" : "2016-11-09 09:53:58 +0000",
    "user" : {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "protected" : false,
      "id_str" : "85053026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844192952914231296\/mLoaFZ86_normal.jpg",
      "id" : 85053026,
      "verified" : false
    }
  },
  "id" : 796293287195844608,
  "created_at" : "2016-11-09 10:08:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/2zvsvS9gtp",
      "expanded_url" : "https:\/\/twitter.com\/PrivacyMatters\/status\/795955416463278080",
      "display_url" : "twitter.com\/PrivacyMatters\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796291897253920772",
  "text" : "EU always like to do such thing. Doubt that tech firm is going to reveal their secret sauce without a contest. https:\/\/t.co\/2zvsvS9gtp",
  "id" : 796291897253920772,
  "created_at" : "2016-11-09 10:02:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Hsien Loong",
      "screen_name" : "leehsienloong",
      "indices" : [ 3, 17 ],
      "id_str" : "34568673",
      "id" : 34568673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796290479797190657",
  "text" : "RT @leehsienloong: Congratulations to President-Elect Donald Trump. SG will strive to keep working together with US. \u2013 LHL https:\/\/t.co\/sU6\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/sU6nCfISfN",
        "expanded_url" : "http:\/\/bit.ly\/2eCsxa6",
        "display_url" : "bit.ly\/2eCsxa6"
      } ]
    },
    "geo" : { },
    "id_str" : "796261626064211972",
    "text" : "Congratulations to President-Elect Donald Trump. SG will strive to keep working together with US. \u2013 LHL https:\/\/t.co\/sU6nCfISfN",
    "id" : 796261626064211972,
    "created_at" : "2016-11-09 08:02:20 +0000",
    "user" : {
      "name" : "Lee Hsien Loong",
      "screen_name" : "leehsienloong",
      "protected" : false,
      "id_str" : "34568673",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040783094607863808\/j9Pm25dJ_normal.jpg",
      "id" : 34568673,
      "verified" : true
    }
  },
  "id" : 796290479797190657,
  "created_at" : "2016-11-09 09:56:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/zXmA8SvhMt",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/retail-and-services\/amazon-to-open-parcel-locker-network-across-europe-1.2541056",
      "display_url" : "irishtimes.com\/business\/retai\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796287166078976000",
  "text" : "In other news, waiting for my delivery. Wish they have Amazon Locker in Ireland https:\/\/t.co\/zXmA8SvhMt",
  "id" : 796287166078976000,
  "created_at" : "2016-11-09 09:43:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796280284454985728",
  "text" : "RT @SarcasmMother: There is still hope for Hillary Clinton\n\nNelson Mandela became President after 27 years in prison.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796269946124795904",
    "text" : "There is still hope for Hillary Clinton\n\nNelson Mandela became President after 27 years in prison.",
    "id" : 796269946124795904,
    "created_at" : "2016-11-09 08:35:23 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 796280284454985728,
  "created_at" : "2016-11-09 09:16:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Till B\u00FCttner",
      "screen_name" : "oarsi",
      "indices" : [ 3, 9 ],
      "id_str" : "19332402",
      "id" : 19332402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796279961996972032",
  "text" : "RT @oarsi: Trumps Website uses Adobe DTM, Analytics and Target. Are they trying to get the best conversionrate by AB-Tests? https:\/\/t.co\/BR\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/BRO582Plw3",
        "expanded_url" : "https:\/\/twitter.com\/ejdyksen\/status\/796029149928390656",
        "display_url" : "twitter.com\/ejdyksen\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796094129734303744",
    "text" : "Trumps Website uses Adobe DTM, Analytics and Target. Are they trying to get the best conversionrate by AB-Tests? https:\/\/t.co\/BRO582Plw3",
    "id" : 796094129734303744,
    "created_at" : "2016-11-08 20:56:45 +0000",
    "user" : {
      "name" : "Till B\u00FCttner",
      "screen_name" : "oarsi",
      "protected" : false,
      "id_str" : "19332402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996508621080092672\/uQeCl0_q_normal.jpg",
      "id" : 19332402,
      "verified" : false
    }
  },
  "id" : 796279961996972032,
  "created_at" : "2016-11-09 09:15:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "norman smith",
      "screen_name" : "BBCNormanS",
      "indices" : [ 3, 14 ],
      "id_str" : "106118793",
      "id" : 106118793
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796260200466550784",
  "text" : "RT @BBCNormanS: Gosh. Trump goes for \"unity\" speech. Praising Hilary Clinton  and time to \"bind the wounds of division\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796259206181556224",
    "text" : "Gosh. Trump goes for \"unity\" speech. Praising Hilary Clinton  and time to \"bind the wounds of division\"",
    "id" : 796259206181556224,
    "created_at" : "2016-11-09 07:52:43 +0000",
    "user" : {
      "name" : "norman smith",
      "screen_name" : "BBCNormanS",
      "protected" : false,
      "id_str" : "106118793",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914472685106802688\/5LmcNI0p_normal.jpg",
      "id" : 106118793,
      "verified" : true
    }
  },
  "id" : 796260200466550784,
  "created_at" : "2016-11-09 07:56:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TIME",
      "screen_name" : "TIME",
      "indices" : [ 3, 8 ],
      "id_str" : "14293310",
      "id" : 14293310
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/JQbS9a2ULv",
      "expanded_url" : "http:\/\/ti.me\/2eCh7TK",
      "display_url" : "ti.me\/2eCh7TK"
    } ]
  },
  "geo" : { },
  "id_str" : "796254928637399040",
  "text" : "RT @TIME: Canada's immigration and citizenship website has crashed https:\/\/t.co\/JQbS9a2ULv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/JQbS9a2ULv",
        "expanded_url" : "http:\/\/ti.me\/2eCh7TK",
        "display_url" : "ti.me\/2eCh7TK"
      } ]
    },
    "geo" : { },
    "id_str" : "796249740539215872",
    "text" : "Canada's immigration and citizenship website has crashed https:\/\/t.co\/JQbS9a2ULv",
    "id" : 796249740539215872,
    "created_at" : "2016-11-09 07:15:06 +0000",
    "user" : {
      "name" : "TIME",
      "screen_name" : "TIME",
      "protected" : false,
      "id_str" : "14293310",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1700796190\/Picture_24_normal.png",
      "id" : 14293310,
      "verified" : true
    }
  },
  "id" : 796254928637399040,
  "created_at" : "2016-11-09 07:35:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Greg Pierce",
      "screen_name" : "agiletortoise",
      "indices" : [ 3, 17 ],
      "id_str" : "6152112",
      "id" : 6152112
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/agiletortoise\/status\/796134046325731329\/photo\/1",
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/6lbNpb5Yzp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwxvsd2XcAAuOw1.jpg",
      "id_str" : "796134043855319040",
      "id" : 796134043855319040,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwxvsd2XcAAuOw1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6lbNpb5Yzp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796145844051410944",
  "text" : "RT @agiletortoise: It's going to be a long and painful night for people who understand how charts should work. https:\/\/t.co\/6lbNpb5Yzp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/agiletortoise\/status\/796134046325731329\/photo\/1",
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/6lbNpb5Yzp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwxvsd2XcAAuOw1.jpg",
        "id_str" : "796134043855319040",
        "id" : 796134043855319040,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwxvsd2XcAAuOw1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6lbNpb5Yzp"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796134046325731329",
    "text" : "It's going to be a long and painful night for people who understand how charts should work. https:\/\/t.co\/6lbNpb5Yzp",
    "id" : 796134046325731329,
    "created_at" : "2016-11-08 23:35:22 +0000",
    "user" : {
      "name" : "Greg Pierce",
      "screen_name" : "agiletortoise",
      "protected" : false,
      "id_str" : "6152112",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/718519462656237568\/t8hpFYGa_normal.jpg",
      "id" : 6152112,
      "verified" : false
    }
  },
  "id" : 796145844051410944,
  "created_at" : "2016-11-09 00:22:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "indices" : [ 3, 11 ],
      "id_str" : "15019743",
      "id" : 15019743
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/IMSipl5uI3",
      "expanded_url" : "https:\/\/www.engadget.com\/2016\/11\/08\/web-of-trust-sold-browser-history\/",
      "display_url" : "engadget.com\/2016\/11\/08\/web\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796141465646858240",
  "text" : "RT @2bscene: Browser add-on caught selling identifiable web histories https:\/\/t.co\/IMSipl5uI3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/IMSipl5uI3",
        "expanded_url" : "https:\/\/www.engadget.com\/2016\/11\/08\/web-of-trust-sold-browser-history\/",
        "display_url" : "engadget.com\/2016\/11\/08\/web\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796114722554777600",
    "text" : "Browser add-on caught selling identifiable web histories https:\/\/t.co\/IMSipl5uI3",
    "id" : 796114722554777600,
    "created_at" : "2016-11-08 22:18:35 +0000",
    "user" : {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "protected" : false,
      "id_str" : "15019743",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/779435193312608256\/NJW57jaX_normal.jpg",
      "id" : 15019743,
      "verified" : false
    }
  },
  "id" : 796141465646858240,
  "created_at" : "2016-11-09 00:04:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ann O'Dea",
      "screen_name" : "AnnODeaSR",
      "indices" : [ 3, 13 ],
      "id_str" : "402099174",
      "id" : 402099174
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796140104599437317",
  "text" : "RT @AnnODeaSR: 89 year old woman about to spend  2nd night in corridor of A&amp;E. I love this country, but this is beyond upsetting. Why can't\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796132392897495040",
    "text" : "89 year old woman about to spend  2nd night in corridor of A&amp;E. I love this country, but this is beyond upsetting. Why can't we fix this!?",
    "id" : 796132392897495040,
    "created_at" : "2016-11-08 23:28:48 +0000",
    "user" : {
      "name" : "Ann O'Dea",
      "screen_name" : "AnnODeaSR",
      "protected" : false,
      "id_str" : "402099174",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011983281586409472\/_30Y0y7i_normal.jpg",
      "id" : 402099174,
      "verified" : true
    }
  },
  "id" : 796140104599437317,
  "created_at" : "2016-11-08 23:59:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wu-Tang Financial",
      "screen_name" : "Wu_Tang_Finance",
      "indices" : [ 3, 19 ],
      "id_str" : "553713584",
      "id" : 553713584
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/tCn4wanpf4",
      "expanded_url" : "https:\/\/twitter.com\/nigel_farage\/status\/795997944826699776",
      "display_url" : "twitter.com\/nigel_farage\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796127993114624000",
  "text" : "RT @Wu_Tang_Finance: BITCH WE LEFT OVER 240 YEARS AND 4 MONTHS AGO https:\/\/t.co\/tCn4wanpf4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/tCn4wanpf4",
        "expanded_url" : "https:\/\/twitter.com\/nigel_farage\/status\/795997944826699776",
        "display_url" : "twitter.com\/nigel_farage\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "796058951741833220",
    "text" : "BITCH WE LEFT OVER 240 YEARS AND 4 MONTHS AGO https:\/\/t.co\/tCn4wanpf4",
    "id" : 796058951741833220,
    "created_at" : "2016-11-08 18:36:58 +0000",
    "user" : {
      "name" : "Wu-Tang Financial",
      "screen_name" : "Wu_Tang_Finance",
      "protected" : false,
      "id_str" : "553713584",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/464244189589684224\/99xg62z1_normal.jpeg",
      "id" : 553713584,
      "verified" : false
    }
  },
  "id" : 796127993114624000,
  "created_at" : "2016-11-08 23:11:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sebastian Hejnowski",
      "screen_name" : "shejnowski",
      "indices" : [ 3, 14 ],
      "id_str" : "34900344",
      "id" : 34900344
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/shejnowski\/status\/795919326285021184\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/WuiQKSXFKJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwuryWdWEAAdPqH.jpg",
      "id_str" : "795918640671494144",
      "id" : 795918640671494144,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwuryWdWEAAdPqH.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WuiQKSXFKJ"
    } ],
    "hashtags" : [ {
      "text" : "WebSummit",
      "indices" : [ 54, 64 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796054503858589696",
  "text" : "RT @shejnowski: Lisbon metro system is unprepared for #WebSummit. Crowds flight for their ride https:\/\/t.co\/WuiQKSXFKJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/shejnowski\/status\/795919326285021184\/photo\/1",
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/WuiQKSXFKJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwuryWdWEAAdPqH.jpg",
        "id_str" : "795918640671494144",
        "id" : 795918640671494144,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwuryWdWEAAdPqH.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WuiQKSXFKJ"
      } ],
      "hashtags" : [ {
        "text" : "WebSummit",
        "indices" : [ 38, 48 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795919326285021184",
    "text" : "Lisbon metro system is unprepared for #WebSummit. Crowds flight for their ride https:\/\/t.co\/WuiQKSXFKJ",
    "id" : 795919326285021184,
    "created_at" : "2016-11-08 09:22:09 +0000",
    "user" : {
      "name" : "Sebastian Hejnowski",
      "screen_name" : "shejnowski",
      "protected" : false,
      "id_str" : "34900344",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/835778372743540736\/cO_dSQlf_normal.jpg",
      "id" : 34900344,
      "verified" : false
    }
  },
  "id" : 796054503858589696,
  "created_at" : "2016-11-08 18:19:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796052944726663168",
  "text" : "I don't get it. New teacher wants the same level pay as their older (more experienced) colleagues?",
  "id" : 796052944726663168,
  "created_at" : "2016-11-08 18:13:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Making Ireland Click",
      "screen_name" : "IrelandClicks",
      "indices" : [ 3, 17 ],
      "id_str" : "773178874855391232",
      "id" : 773178874855391232
    }, {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 33, 48 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796047006640832512",
  "text" : "RT @IrelandClicks: The path that @paulinesargent took to technology was grounded in solving real world problems. Read all about it https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Pauline Sargent",
        "screen_name" : "paulinesargent",
        "indices" : [ 14, 29 ],
        "id_str" : "22997944",
        "id" : 22997944
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrelandClicks\/status\/795993164700073984\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/3topKsrhdg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwvvkFlVQAAdEFL.jpg",
        "id_str" : "795993162414112768",
        "id" : 795993162414112768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwvvkFlVQAAdEFL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 661
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 661
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 661
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 661
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3topKsrhdg"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/emdTAjfZzF",
        "expanded_url" : "http:\/\/www.makingirelandclick.ie\/pauline-sargent-makingirelandclick\/",
        "display_url" : "makingirelandclick.ie\/pauline-sargen\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795993164700073984",
    "text" : "The path that @paulinesargent took to technology was grounded in solving real world problems. Read all about it https:\/\/t.co\/emdTAjfZzF https:\/\/t.co\/3topKsrhdg",
    "id" : 795993164700073984,
    "created_at" : "2016-11-08 14:15:33 +0000",
    "user" : {
      "name" : "Making Ireland Click",
      "screen_name" : "IrelandClicks",
      "protected" : false,
      "id_str" : "773178874855391232",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/785473566397652992\/TX9hQv1g_normal.jpg",
      "id" : 773178874855391232,
      "verified" : false
    }
  },
  "id" : 796047006640832512,
  "created_at" : "2016-11-08 17:49:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yosef Rapaport",
      "screen_name" : "YosefRapaport",
      "indices" : [ 3, 17 ],
      "id_str" : "329147510",
      "id" : 329147510
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796039229423894528",
  "text" : "RT @YosefRapaport: I'm  an Orthodox Jewish Immigrant My vote is private. Dedicated in honor of US CPT Khan, his devotion makes (religious)\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/YosefRapaport\/status\/796007917094891525\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/httaaUspeM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwv88mAWEAEmz9o.jpg",
        "id_str" : "796007877085368321",
        "id" : 796007877085368321,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwv88mAWEAEmz9o.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 489
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 736
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/httaaUspeM"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/YosefRapaport\/status\/796007917094891525\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/httaaUspeM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwv89u0XAAQX16V.jpg",
        "id_str" : "796007896630886404",
        "id" : 796007896630886404,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwv89u0XAAQX16V.jpg",
        "sizes" : [ {
          "h" : 591,
          "resize" : "fit",
          "w" : 719
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 591,
          "resize" : "fit",
          "w" : 719
        }, {
          "h" : 591,
          "resize" : "fit",
          "w" : 719
        }, {
          "h" : 559,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/httaaUspeM"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796007917094891525",
    "text" : "I'm  an Orthodox Jewish Immigrant My vote is private. Dedicated in honor of US CPT Khan, his devotion makes (religious) freedom possible https:\/\/t.co\/httaaUspeM",
    "id" : 796007917094891525,
    "created_at" : "2016-11-08 15:14:11 +0000",
    "user" : {
      "name" : "Yosef Rapaport",
      "screen_name" : "YosefRapaport",
      "protected" : false,
      "id_str" : "329147510",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014572134734196737\/6-lLlzbg_normal.jpg",
      "id" : 329147510,
      "verified" : false
    }
  },
  "id" : 796039229423894528,
  "created_at" : "2016-11-08 17:18:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/6zI1Y8PGo9",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=UfcAVejslrU",
      "display_url" : "youtube.com\/watch?v=UfcAVe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796035425852915712",
  "text" : "This might stir the soul and calm the nerve but my tabby feels otherwise https:\/\/t.co\/6zI1Y8PGo9",
  "id" : 796035425852915712,
  "created_at" : "2016-11-08 17:03:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "indices" : [ 3, 12 ],
      "id_str" : "11503282",
      "id" : 11503282
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "electionday",
      "indices" : [ 113, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796027733323223040",
  "text" : "RT @ladyxtel: Ok folks the contractions have started. In a couple of hours we will know if it's a boy or a girl. #electionday",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "electionday",
        "indices" : [ 99, 111 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "796027418804789248",
    "text" : "Ok folks the contractions have started. In a couple of hours we will know if it's a boy or a girl. #electionday",
    "id" : 796027418804789248,
    "created_at" : "2016-11-08 16:31:40 +0000",
    "user" : {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "protected" : false,
      "id_str" : "11503282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849872363671830528\/5iExzLy9_normal.jpg",
      "id" : 11503282,
      "verified" : true
    }
  },
  "id" : 796027733323223040,
  "created_at" : "2016-11-08 16:32:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MCI",
      "screen_name" : "SingaporeMCI",
      "indices" : [ 3, 16 ],
      "id_str" : "468159804",
      "id" : 468159804
    }, {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 54, 64 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "796026814430904320",
  "text" : "RT @SingaporeMCI: Here\u2019s how the data science team at @GovTechSG is helping put evidence into the hands of Singapore\u2019ss policy-makers.\nhttp\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dashboard.twitter.com\" rel=\"nofollow\"\u003ETwitter Business Experience\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GovTech (Singapore)",
        "screen_name" : "GovTechSG",
        "indices" : [ 36, 46 ],
        "id_str" : "123167035",
        "id" : 123167035
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/E5xWFFTAEQ",
        "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/singapore\/putting-evidence-into-the-hands-of-singapore-s-policy-makers\/3268118.html",
        "display_url" : "channelnewsasia.com\/news\/singapore\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795931117488603136",
    "text" : "Here\u2019s how the data science team at @GovTechSG is helping put evidence into the hands of Singapore\u2019ss policy-makers.\nhttps:\/\/t.co\/E5xWFFTAEQ",
    "id" : 795931117488603136,
    "created_at" : "2016-11-08 10:09:00 +0000",
    "user" : {
      "name" : "MCI",
      "screen_name" : "SingaporeMCI",
      "protected" : false,
      "id_str" : "468159804",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/767917313513304065\/pCrX-iyH_normal.jpg",
      "id" : 468159804,
      "verified" : true
    }
  },
  "id" : 796026814430904320,
  "created_at" : "2016-11-08 16:29:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Kavanagh",
      "screen_name" : "AdrianKavanagh",
      "indices" : [ 3, 18 ],
      "id_str" : "95161198",
      "id" : 95161198
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/JCbFIg03QN",
      "expanded_url" : "https:\/\/twitter.com\/AdrianKavanagh\/status\/795926265333186560",
      "display_url" : "twitter.com\/AdrianKavanagh\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "796001889452167168",
  "text" : "RT @AdrianKavanagh: How early US Presidential results might help you to have an early (earlier) night tonight... https:\/\/t.co\/JCbFIg03QN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/JCbFIg03QN",
        "expanded_url" : "https:\/\/twitter.com\/AdrianKavanagh\/status\/795926265333186560",
        "display_url" : "twitter.com\/AdrianKavanagh\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795960035360784384",
    "text" : "How early US Presidential results might help you to have an early (earlier) night tonight... https:\/\/t.co\/JCbFIg03QN",
    "id" : 795960035360784384,
    "created_at" : "2016-11-08 12:03:55 +0000",
    "user" : {
      "name" : "Adrian Kavanagh",
      "screen_name" : "AdrianKavanagh",
      "protected" : false,
      "id_str" : "95161198",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996321895640420352\/22Lhbm2J_normal.jpg",
      "id" : 95161198,
      "verified" : false
    }
  },
  "id" : 796001889452167168,
  "created_at" : "2016-11-08 14:50:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amazon Help",
      "screen_name" : "AmazonHelp",
      "indices" : [ 0, 11 ],
      "id_str" : "85741735",
      "id" : 85741735
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "795976532766457856",
  "geo" : { },
  "id_str" : "795980152807292929",
  "in_reply_to_user_id" : 85741735,
  "text" : "@AmazonHelp is there any email address I can write to?",
  "id" : 795980152807292929,
  "in_reply_to_status_id" : 795976532766457856,
  "created_at" : "2016-11-08 13:23:51 +0000",
  "in_reply_to_screen_name" : "AmazonHelp",
  "in_reply_to_user_id_str" : "85741735",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amazon Help",
      "screen_name" : "AmazonHelp",
      "indices" : [ 0, 11 ],
      "id_str" : "85741735",
      "id" : 85741735
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "795969997134761984",
  "geo" : { },
  "id_str" : "795970781025095680",
  "in_reply_to_user_id" : 85741735,
  "text" : "@AmazonHelp Thank You. The only email I got is a Dispatch Confirmation sent out on 7 November 2016 at 18:30",
  "id" : 795970781025095680,
  "in_reply_to_status_id" : 795969997134761984,
  "created_at" : "2016-11-08 12:46:37 +0000",
  "in_reply_to_screen_name" : "AmazonHelp",
  "in_reply_to_user_id_str" : "85741735",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amazon Help",
      "screen_name" : "AmazonHelp",
      "indices" : [ 24, 35 ],
      "id_str" : "85741735",
      "id" : 85741735
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/HXK3xONmYV",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/795930716789993472",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795966765817823232",
  "text" : "Are you able to advice? @AmazonHelp Thank You. https:\/\/t.co\/HXK3xONmYV",
  "id" : 795966765817823232,
  "created_at" : "2016-11-08 12:30:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795932293575610368",
  "text" : "RT @ronanlyons: There is no more urgent task facing Housing Minister than understanding why Irish building costs are so out of line with ot\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795929779899928576",
    "text" : "There is no more urgent task facing Housing Minister than understanding why Irish building costs are so out of line with other countries.",
    "id" : 795929779899928576,
    "created_at" : "2016-11-08 10:03:41 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 795932293575610368,
  "created_at" : "2016-11-08 10:13:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795930716789993472",
  "text" : "\"Your package has been taken off hold\" What does that mean? Thanks",
  "id" : 795930716789993472,
  "created_at" : "2016-11-08 10:07:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    }, {
      "name" : "Pat Kenny Newstalk",
      "screen_name" : "PatKennyNT",
      "indices" : [ 36, 47 ],
      "id_str" : "1640382823",
      "id" : 1640382823
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DaftReport",
      "indices" : [ 73, 84 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795926195661578240",
  "text" : "RT @ronanlyons: Just about to go on @PatKennyNT to talk about the latest #DaftReport, which shows rents - and rent inflation - at record hi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Pat Kenny Newstalk",
        "screen_name" : "PatKennyNT",
        "indices" : [ 20, 31 ],
        "id_str" : "1640382823",
        "id" : 1640382823
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DaftReport",
        "indices" : [ 57, 68 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795918367521734656",
    "text" : "Just about to go on @PatKennyNT to talk about the latest #DaftReport, which shows rents - and rent inflation - at record highs nationwide.",
    "id" : 795918367521734656,
    "created_at" : "2016-11-08 09:18:20 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 795926195661578240,
  "created_at" : "2016-11-08 09:49:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795916648259682304",
  "text" : "RT @Dotnetster: Still looking for a web application designer to work on our cool product. If your feeling like a small fish in a big pond,\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "794657186077097984",
    "text" : "Still looking for a web application designer to work on our cool product. If your feeling like a small fish in a big pond, come work with us",
    "id" : 794657186077097984,
    "created_at" : "2016-11-04 21:46:51 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 795916648259682304,
  "created_at" : "2016-11-08 09:11:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/a2qFkZD0ha",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/795585315390062592",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795906986898583552",
  "text" : "MMM is known as regressive analysis by Data scientist\/Statistician\/Data Analyst https:\/\/t.co\/a2qFkZD0ha",
  "id" : 795906986898583552,
  "created_at" : "2016-11-08 08:33:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/IXmL8OZMe7",
      "expanded_url" : "http:\/\/www.abs.gov.au\/websitedbs\/D3310114.nsf\/home\/Time+Series+Analysis:+The+Basics",
      "display_url" : "abs.gov.au\/websitedbs\/D33\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795904770972196864",
  "text" : "The Australian Bureau of Statistics has a good review the Basics of Time Series Analysis. https:\/\/t.co\/IXmL8OZMe7",
  "id" : 795904770972196864,
  "created_at" : "2016-11-08 08:24:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/h7jKc02lux",
      "expanded_url" : "http:\/\/www.straitstimes.com\/world\/united-states\/countries-like-spore-stealing-us-jobs-trump",
      "display_url" : "straitstimes.com\/world\/united-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795901128043008000",
  "text" : "https:\/\/t.co\/h7jKc02lux",
  "id" : 795901128043008000,
  "created_at" : "2016-11-08 08:09:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "karorynka",
      "screen_name" : "karorynka",
      "indices" : [ 3, 13 ],
      "id_str" : "36373786",
      "id" : 36373786
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LinkedIn",
      "indices" : [ 16, 25 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795771909728305152",
  "text" : "RT @karorynka: \"#LinkedIn salary\" is coming - might be a really useful tool, but why is it not available in Ireland at launch? :(  https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LinkedIn",
        "indices" : [ 1, 10 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/IVOm1XpKdu",
        "expanded_url" : "https:\/\/blog.linkedin.com\/2016\/11\/02\/introducing-linkedin-salary-unlock-your-earning-potential",
        "display_url" : "blog.linkedin.com\/2016\/11\/02\/int\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "794459398605258756",
    "text" : "\"#LinkedIn salary\" is coming - might be a really useful tool, but why is it not available in Ireland at launch? :(  https:\/\/t.co\/IVOm1XpKdu",
    "id" : 794459398605258756,
    "created_at" : "2016-11-04 08:40:55 +0000",
    "user" : {
      "name" : "karorynka",
      "screen_name" : "karorynka",
      "protected" : false,
      "id_str" : "36373786",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/710563402419150848\/rvrj9jrF_normal.jpg",
      "id" : 36373786,
      "verified" : false
    }
  },
  "id" : 795771909728305152,
  "created_at" : "2016-11-07 23:36:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "George Takei",
      "screen_name" : "GeorgeTakei",
      "indices" : [ 3, 15 ],
      "id_str" : "237845487",
      "id" : 237845487
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795765350268895232",
  "text" : "RT @GeorgeTakei: Stevie Wonder:  \"Voting for Trump is like asking me to drive.\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795757868037316608",
    "text" : "Stevie Wonder:  \"Voting for Trump is like asking me to drive.\"",
    "id" : 795757868037316608,
    "created_at" : "2016-11-07 22:40:34 +0000",
    "user" : {
      "name" : "George Takei",
      "screen_name" : "GeorgeTakei",
      "protected" : false,
      "id_str" : "237845487",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885170691846414339\/-08XzXKy_normal.jpg",
      "id" : 237845487,
      "verified" : true
    }
  },
  "id" : 795765350268895232,
  "created_at" : "2016-11-07 23:10:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dominant Cunningham",
      "screen_name" : "KCsixtyseven",
      "indices" : [ 3, 16 ],
      "id_str" : "205282903",
      "id" : 205282903
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795763436772884480",
  "text" : "RT @KCsixtyseven: Hard to get your head around the mentality that thinks there's a need to pin a poppy on Cookie Monster. https:\/\/t.co\/b67h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KCsixtyseven\/status\/795714459423690752\/photo\/1",
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/b67hemyrG6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwryDhSWgAEaI0k.jpg",
        "id_str" : "795714426473185281",
        "id" : 795714426473185281,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwryDhSWgAEaI0k.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 1136
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 1136
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 1136
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/b67hemyrG6"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795714459423690752",
    "text" : "Hard to get your head around the mentality that thinks there's a need to pin a poppy on Cookie Monster. https:\/\/t.co\/b67hemyrG6",
    "id" : 795714459423690752,
    "created_at" : "2016-11-07 19:48:05 +0000",
    "user" : {
      "name" : "Dominant Cunningham",
      "screen_name" : "KCsixtyseven",
      "protected" : false,
      "id_str" : "205282903",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/887651167207129089\/JFdrSHYD_normal.jpg",
      "id" : 205282903,
      "verified" : true
    }
  },
  "id" : 795763436772884480,
  "created_at" : "2016-11-07 23:02:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u270C\uD83C\uDFFD\uFE0F",
      "screen_name" : "Owen_1906",
      "indices" : [ 3, 13 ],
      "id_str" : "2916381417",
      "id" : 2916381417
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Owen_1906\/status\/794946964832002049\/photo\/1",
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/EvzaOTBIYM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwg4C9PWQAAG5_G.jpg",
      "id_str" : "794946957680721920",
      "id" : 794946957680721920,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwg4C9PWQAAG5_G.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 820,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 820,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 820,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 498
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EvzaOTBIYM"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795763351821361152",
  "text" : "RT @Owen_1906: Fella at Lidl got heavy confidence in that rose https:\/\/t.co\/EvzaOTBIYM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Owen_1906\/status\/794946964832002049\/photo\/1",
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/EvzaOTBIYM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwg4C9PWQAAG5_G.jpg",
        "id_str" : "794946957680721920",
        "id" : 794946957680721920,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwg4C9PWQAAG5_G.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 820,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 820,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 820,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 498
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EvzaOTBIYM"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "794946964832002049",
    "text" : "Fella at Lidl got heavy confidence in that rose https:\/\/t.co\/EvzaOTBIYM",
    "id" : 794946964832002049,
    "created_at" : "2016-11-05 16:58:20 +0000",
    "user" : {
      "name" : "\u270C\uD83C\uDFFD\uFE0F",
      "screen_name" : "Owen_1906",
      "protected" : false,
      "id_str" : "2916381417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1000717714682466305\/NkMJvIU8_normal.jpg",
      "id" : 2916381417,
      "verified" : false
    }
  },
  "id" : 795763351821361152,
  "created_at" : "2016-11-07 23:02:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "elva carri",
      "screen_name" : "elvacarri",
      "indices" : [ 3, 13 ],
      "id_str" : "242806816",
      "id" : 242806816
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/zBDtMfS5le",
      "expanded_url" : "https:\/\/twitter.com\/girlcrewhq\/status\/795649525620817920",
      "display_url" : "twitter.com\/girlcrewhq\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795730239649353728",
  "text" : "RT @elvacarri: If it's not too late go register \uD83D\uDE01 talking about how I started a tech business as a non-techie https:\/\/t.co\/zBDtMfS5le",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/zBDtMfS5le",
        "expanded_url" : "https:\/\/twitter.com\/girlcrewhq\/status\/795649525620817920",
        "display_url" : "twitter.com\/girlcrewhq\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795717818264199168",
    "text" : "If it's not too late go register \uD83D\uDE01 talking about how I started a tech business as a non-techie https:\/\/t.co\/zBDtMfS5le",
    "id" : 795717818264199168,
    "created_at" : "2016-11-07 20:01:26 +0000",
    "user" : {
      "name" : "elva carri",
      "screen_name" : "elvacarri",
      "protected" : false,
      "id_str" : "242806816",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/817371591088766976\/OrrxlEz0_normal.jpg",
      "id" : 242806816,
      "verified" : false
    }
  },
  "id" : 795730239649353728,
  "created_at" : "2016-11-07 20:50:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Popular Science",
      "screen_name" : "PopSci",
      "indices" : [ 3, 10 ],
      "id_str" : "19722699",
      "id" : 19722699
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/PopSci\/status\/795725035881250816\/photo\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/mmU30KqBEa",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwr7seeW8AA80NJ.jpg",
      "id_str" : "795725025697525760",
      "id" : 795725025697525760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwr7seeW8AA80NJ.jpg",
      "sizes" : [ {
        "h" : 338,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 338,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 338,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 338,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mmU30KqBEa"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/sW01RFFcrc",
      "expanded_url" : "http:\/\/bit.ly\/2eg6VFe",
      "display_url" : "bit.ly\/2eg6VFe"
    } ]
  },
  "geo" : { },
  "id_str" : "795730009155575808",
  "text" : "RT @PopSci: Japan Made The World\u2019s First-Ever Cat Street View https:\/\/t.co\/sW01RFFcrc https:\/\/t.co\/mmU30KqBEa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/trueanthem.com\/\" rel=\"nofollow\"\u003EtrueAnthem\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PopSci\/status\/795725035881250816\/photo\/1",
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/mmU30KqBEa",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwr7seeW8AA80NJ.jpg",
        "id_str" : "795725025697525760",
        "id" : 795725025697525760,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwr7seeW8AA80NJ.jpg",
        "sizes" : [ {
          "h" : 338,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mmU30KqBEa"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/sW01RFFcrc",
        "expanded_url" : "http:\/\/bit.ly\/2eg6VFe",
        "display_url" : "bit.ly\/2eg6VFe"
      } ]
    },
    "geo" : { },
    "id_str" : "795725035881250816",
    "text" : "Japan Made The World\u2019s First-Ever Cat Street View https:\/\/t.co\/sW01RFFcrc https:\/\/t.co\/mmU30KqBEa",
    "id" : 795725035881250816,
    "created_at" : "2016-11-07 20:30:07 +0000",
    "user" : {
      "name" : "Popular Science",
      "screen_name" : "PopSci",
      "protected" : false,
      "id_str" : "19722699",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1029084163780079616\/KL96wS4C_normal.jpg",
      "id" : 19722699,
      "verified" : true
    }
  },
  "id" : 795730009155575808,
  "created_at" : "2016-11-07 20:49:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Stinson",
      "screen_name" : "stinson",
      "indices" : [ 3, 11 ],
      "id_str" : "9730672",
      "id" : 9730672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/aqYtyybtvI",
      "expanded_url" : "https:\/\/twitter.com\/sonnybunch\/status\/795646270027296770",
      "display_url" : "twitter.com\/sonnybunch\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795729832080527360",
  "text" : "RT @stinson: Reminds me of the memes comparing McCain to Saul Tigh from Battlestar. https:\/\/t.co\/aqYtyybtvI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/aqYtyybtvI",
        "expanded_url" : "https:\/\/twitter.com\/sonnybunch\/status\/795646270027296770",
        "display_url" : "twitter.com\/sonnybunch\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795680316836888576",
    "text" : "Reminds me of the memes comparing McCain to Saul Tigh from Battlestar. https:\/\/t.co\/aqYtyybtvI",
    "id" : 795680316836888576,
    "created_at" : "2016-11-07 17:32:25 +0000",
    "user" : {
      "name" : "Matthew Stinson",
      "screen_name" : "stinson",
      "protected" : false,
      "id_str" : "9730672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006165032806125568\/EhnbyUJ1_normal.jpg",
      "id" : 9730672,
      "verified" : true
    }
  },
  "id" : 795729832080527360,
  "created_at" : "2016-11-07 20:49:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/795718607028310016\/photo\/1",
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/qnZjpFLQ77",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwr111EWgAAKW2y.jpg",
      "id_str" : "795718589311516672",
      "id" : 795718589311516672,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwr111EWgAAKW2y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 766,
        "resize" : "fit",
        "w" : 1186
      }, {
        "h" : 766,
        "resize" : "fit",
        "w" : 1186
      }, {
        "h" : 766,
        "resize" : "fit",
        "w" : 1186
      }, {
        "h" : 439,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qnZjpFLQ77"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795718607028310016",
  "text" : "My SSD is on its way. https:\/\/t.co\/qnZjpFLQ77",
  "id" : 795718607028310016,
  "created_at" : "2016-11-07 20:04:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "joaobeltrao",
      "screen_name" : "joaobeltrao",
      "indices" : [ 3, 15 ],
      "id_str" : "14214663",
      "id" : 14214663
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WebSummit",
      "indices" : [ 91, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795714955244896256",
  "text" : "RT @joaobeltrao: The doors to the venue closed with a lot of people outside. Over capacity #WebSummit.  People who paid a lot of money are\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WebSummit",
        "indices" : [ 74, 84 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795693303509426176",
    "text" : "The doors to the venue closed with a lot of people outside. Over capacity #WebSummit.  People who paid a lot of money are not allowed in.",
    "id" : 795693303509426176,
    "created_at" : "2016-11-07 18:24:01 +0000",
    "user" : {
      "name" : "joaobeltrao",
      "screen_name" : "joaobeltrao",
      "protected" : false,
      "id_str" : "14214663",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1540875679\/Joao_Beltrao_Caixa_Seguros_normal.jpg",
      "id" : 14214663,
      "verified" : false
    }
  },
  "id" : 795714955244896256,
  "created_at" : "2016-11-07 19:50:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/AVuWiWZnXf",
      "expanded_url" : "https:\/\/twitter.com\/adrianweckler\/status\/795698229010636802",
      "display_url" : "twitter.com\/adrianweckler\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795713623477878786",
  "text" : "The curse that follows from RDS Dublin to Portugal. https:\/\/t.co\/AVuWiWZnXf",
  "id" : 795713623477878786,
  "created_at" : "2016-11-07 19:44:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795712924442591232",
  "text" : "School newsletter assume us to be aware what is JMB. I got to google that...",
  "id" : 795712924442591232,
  "created_at" : "2016-11-07 19:41:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795710979954921473",
  "text" : "Going by the tweets, if teachers ready for work today but school gate locked? Need to read again the letter from school again.",
  "id" : 795710979954921473,
  "created_at" : "2016-11-07 19:34:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/g5GdkWV96B",
      "expanded_url" : "https:\/\/www.theguardian.com\/technology\/2016\/nov\/07\/hopping-amazon-dash-buttons-?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/technology\/201\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795701775466696704",
  "text" : "I tried to do my shopping with Amazon Dash buttons so you don't have to https:\/\/t.co\/g5GdkWV96B",
  "id" : 795701775466696704,
  "created_at" : "2016-11-07 18:57:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795670966261125120",
  "text" : "I have Facebook to thanks if not my friends will not be able to wish me.",
  "id" : 795670966261125120,
  "created_at" : "2016-11-07 16:55:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/lxNWGCiflA",
      "expanded_url" : "https:\/\/twitter.com\/ABC\/status\/795425768406917124",
      "display_url" : "twitter.com\/ABC\/status\/795\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795668115887636484",
  "text" : "Maybe he has no idea what Machine Learning can do? https:\/\/t.co\/lxNWGCiflA",
  "id" : 795668115887636484,
  "created_at" : "2016-11-07 16:43:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Nobel Prize",
      "screen_name" : "NobelPrize",
      "indices" : [ 3, 14 ],
      "id_str" : "16465385",
      "id" : 16465385
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795665440899002369",
  "text" : "RT @NobelPrize: Marie Curie, born 7 Nov 1867, still the only individual awarded with 2 Nobel Prizes in two different scientific fields, Phy\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/NobelPrize\/status\/795574799233908736\/video\/1",
        "indices" : [ 145, 168 ],
        "url" : "https:\/\/t.co\/04emtV9IrU",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/795574405795610624\/pu\/img\/6j-puC6GPqUpzI8B.jpg",
        "id_str" : "795574405795610624",
        "id" : 795574405795610624,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/795574405795610624\/pu\/img\/6j-puC6GPqUpzI8B.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/04emtV9IrU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795574799233908736",
    "text" : "Marie Curie, born 7 Nov 1867, still the only individual awarded with 2 Nobel Prizes in two different scientific fields, Physics &amp; Chemistry. https:\/\/t.co\/04emtV9IrU",
    "id" : 795574799233908736,
    "created_at" : "2016-11-07 10:33:07 +0000",
    "user" : {
      "name" : "The Nobel Prize",
      "screen_name" : "NobelPrize",
      "protected" : false,
      "id_str" : "16465385",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1029729528548139009\/fVPd0EwX_normal.jpg",
      "id" : 16465385,
      "verified" : true
    }
  },
  "id" : 795665440899002369,
  "created_at" : "2016-11-07 16:33:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/m3el1AevYS",
      "expanded_url" : "https:\/\/www.google.com\/analytics\/resources\/gat-mmm-view.html",
      "display_url" : "google.com\/analytics\/reso\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795585315390062592",
  "text" : "MMM tells you how to spend it by compares the impact of competition, demographics, economic, consumer trends https:\/\/t.co\/m3el1AevYS",
  "id" : 795585315390062592,
  "created_at" : "2016-11-07 11:14:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/uuASOwQym6",
      "expanded_url" : "http:\/\/cnet.co\/2fIVsPC",
      "display_url" : "cnet.co\/2fIVsPC"
    } ]
  },
  "geo" : { },
  "id_str" : "795583876399828992",
  "text" : "Remove distractions from websites you're just trying to read. Turn on Reader Mode for Chrome desktop versions https:\/\/t.co\/uuASOwQym6",
  "id" : 795583876399828992,
  "created_at" : "2016-11-07 11:09:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    }, {
      "name" : "The Register",
      "screen_name" : "TheRegister",
      "indices" : [ 105, 117 ],
      "id_str" : "78012548",
      "id" : 78012548
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/GZM2wGce6H",
      "expanded_url" : "http:\/\/www.theregister.co.uk\/2016\/11\/07\/uk_ai_ethics_board_to_launch\/",
      "display_url" : "theregister.co.uk\/2016\/11\/07\/uk_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795582170492444673",
  "text" : "RT @klillington: The Alan Turing Institute is launching a UK AI ethics board https:\/\/t.co\/GZM2wGce6H via @theregister",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Register",
        "screen_name" : "TheRegister",
        "indices" : [ 88, 100 ],
        "id_str" : "78012548",
        "id" : 78012548
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/GZM2wGce6H",
        "expanded_url" : "http:\/\/www.theregister.co.uk\/2016\/11\/07\/uk_ai_ethics_board_to_launch\/",
        "display_url" : "theregister.co.uk\/2016\/11\/07\/uk_\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795580772430258176",
    "text" : "The Alan Turing Institute is launching a UK AI ethics board https:\/\/t.co\/GZM2wGce6H via @theregister",
    "id" : 795580772430258176,
    "created_at" : "2016-11-07 10:56:51 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 795582170492444673,
  "created_at" : "2016-11-07 11:02:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kevin",
      "screen_name" : "Kevooo77",
      "indices" : [ 3, 12 ],
      "id_str" : "90716502",
      "id" : 90716502
    }, {
      "name" : "Pat Kenny Newstalk",
      "screen_name" : "PatKennyNT",
      "indices" : [ 26, 37 ],
      "id_str" : "1640382823",
      "id" : 1640382823
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Asti",
      "indices" : [ 14, 19 ]
    }, {
      "text" : "pknt",
      "indices" : [ 20, 25 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/tkQK1Zf9VS",
      "expanded_url" : "https:\/\/www.weforum.org\/agenda\/2016\/02\/which-countries-have-the-best-literacy-and-numeracy-rates\/",
      "display_url" : "weforum.org\/agenda\/2016\/02\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795565575552561152",
  "text" : "RT @Kevooo77: #Asti #pknt @PatKennyNT https:\/\/t.co\/tkQK1Zf9VS 6th worst literacy...3rd worst numeracy! horrific",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Pat Kenny Newstalk",
        "screen_name" : "PatKennyNT",
        "indices" : [ 12, 23 ],
        "id_str" : "1640382823",
        "id" : 1640382823
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Asti",
        "indices" : [ 0, 5 ]
      }, {
        "text" : "pknt",
        "indices" : [ 6, 11 ]
      } ],
      "urls" : [ {
        "indices" : [ 24, 47 ],
        "url" : "https:\/\/t.co\/tkQK1Zf9VS",
        "expanded_url" : "https:\/\/www.weforum.org\/agenda\/2016\/02\/which-countries-have-the-best-literacy-and-numeracy-rates\/",
        "display_url" : "weforum.org\/agenda\/2016\/02\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "795557754350039044",
    "text" : "#Asti #pknt @PatKennyNT https:\/\/t.co\/tkQK1Zf9VS 6th worst literacy...3rd worst numeracy! horrific",
    "id" : 795557754350039044,
    "created_at" : "2016-11-07 09:25:24 +0000",
    "user" : {
      "name" : "Kevin",
      "screen_name" : "Kevooo77",
      "protected" : false,
      "id_str" : "90716502",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876225295388463104\/fkKr41bO_normal.jpg",
      "id" : 90716502,
      "verified" : false
    }
  },
  "id" : 795565575552561152,
  "created_at" : "2016-11-07 09:56:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "mondaymotivation",
      "indices" : [ 12, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795565084936470528",
  "text" : "If you need #mondaymotivation to keep going, then you are in the wrong job or you need to change career direction?",
  "id" : 795565084936470528,
  "created_at" : "2016-11-07 09:54:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Suzanne Lucas",
      "screen_name" : "RealEvilHRLady",
      "indices" : [ 3, 18 ],
      "id_str" : "121728825",
      "id" : 121728825
    }, {
      "name" : "Suzanne Lucas",
      "screen_name" : "RealEvilHRLady",
      "indices" : [ 75, 90 ],
      "id_str" : "121728825",
      "id" : 121728825
    }, {
      "name" : "Inc.",
      "screen_name" : "Inc",
      "indices" : [ 119, 123 ],
      "id_str" : "16896485",
      "id" : 16896485
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/jQd4knYl4c",
      "expanded_url" : "http:\/\/www.inc.com\/suzanne-lucas\/how-to-get-a-tech-job-when-youre-really-old-like-35.html",
      "display_url" : "inc.com\/suzanne-lucas\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795409849534316544",
  "text" : "RT @RealEvilHRLady: How to Get a Tech Job When You're Really Old (Like 35) @realevilhrlady https:\/\/t.co\/jQd4knYl4c via @Inc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Suzanne Lucas",
        "screen_name" : "RealEvilHRLady",
        "indices" : [ 55, 70 ],
        "id_str" : "121728825",
        "id" : 121728825
      }, {
        "name" : "Inc.",
        "screen_name" : "Inc",
        "indices" : [ 99, 103 ],
        "id_str" : "16896485",
        "id" : 16896485
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/jQd4knYl4c",
        "expanded_url" : "http:\/\/www.inc.com\/suzanne-lucas\/how-to-get-a-tech-job-when-youre-really-old-like-35.html",
        "display_url" : "inc.com\/suzanne-lucas\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "778217565193637891",
    "text" : "How to Get a Tech Job When You're Really Old (Like 35) @realevilhrlady https:\/\/t.co\/jQd4knYl4c via @Inc",
    "id" : 778217565193637891,
    "created_at" : "2016-09-20 13:01:40 +0000",
    "user" : {
      "name" : "Suzanne Lucas",
      "screen_name" : "RealEvilHRLady",
      "protected" : false,
      "id_str" : "121728825",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/990879011415969792\/h5-1CUFO_normal.jpg",
      "id" : 121728825,
      "verified" : false
    }
  },
  "id" : 795409849534316544,
  "created_at" : "2016-11-06 23:37:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/3hdMybkOuL",
      "expanded_url" : "https:\/\/www.ncbi.nlm.nih.gov\/pubmed\/12610029",
      "display_url" : "ncbi.nlm.nih.gov\/pubmed\/12610029"
    } ]
  },
  "geo" : { },
  "id_str" : "795404465226612740",
  "text" : "Diabetes prediction score - Predictive Analytics application in the medical field. https:\/\/t.co\/3hdMybkOuL",
  "id" : 795404465226612740,
  "created_at" : "2016-11-06 23:16:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/viisv7LzGc",
      "expanded_url" : "http:\/\/lensculture.com\/y3BdR",
      "display_url" : "lensculture.com\/y3BdR"
    } ]
  },
  "geo" : { },
  "id_str" : "795394729773060096",
  "text" : "David Jay - The Unknown Soldier | LensCulture https:\/\/t.co\/viisv7LzGc",
  "id" : 795394729773060096,
  "created_at" : "2016-11-06 22:37:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gendergap16",
      "indices" : [ 95, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/fPjjWp6iOO",
      "expanded_url" : "http:\/\/wef.ch\/gendergap16",
      "display_url" : "wef.ch\/gendergap16"
    } ]
  },
  "geo" : { },
  "id_str" : "795371306489413636",
  "text" : "In Singapore, the average annual salary for a woman is $67,074.10 and for a man is $75,000.00. #gendergap16 https:\/\/t.co\/fPjjWp6iOO",
  "id" : 795371306489413636,
  "created_at" : "2016-11-06 21:04:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gendergap16",
      "indices" : [ 93, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/fPjjWp6iOO",
      "expanded_url" : "http:\/\/wef.ch\/gendergap16",
      "display_url" : "wef.ch\/gendergap16"
    } ]
  },
  "geo" : { },
  "id_str" : "795369248612896772",
  "text" : "In Ireland, the average annual salary for a woman is $36,686.91 and for a man is $62,144.47. #gendergap16 https:\/\/t.co\/fPjjWp6iOO",
  "id" : 795369248612896772,
  "created_at" : "2016-11-06 20:56:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cllr Ossian Smyth",
      "screen_name" : "smytho",
      "indices" : [ 3, 10 ],
      "id_str" : "116560657",
      "id" : 116560657
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795315856301522948",
  "text" : "RT @smytho: When choosing where to locate its new data centre, Apple ruled out Dublin for being too close to UK nuclear power stations. #ex\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/smytho\/status\/795228649612046336\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/dCyZKJJRig",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwk4J46XgAAd_Yc.jpg",
        "id_str" : "795228551754776576",
        "id" : 795228551754776576,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwk4J46XgAAd_Yc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 557
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 557
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 557
        }, {
          "h" : 603,
          "resize" : "fit",
          "w" : 557
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dCyZKJJRig"
      } ],
      "hashtags" : [ {
        "text" : "externality",
        "indices" : [ 124, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "795228649612046336",
    "text" : "When choosing where to locate its new data centre, Apple ruled out Dublin for being too close to UK nuclear power stations. #externality https:\/\/t.co\/dCyZKJJRig",
    "id" : 795228649612046336,
    "created_at" : "2016-11-06 11:37:39 +0000",
    "user" : {
      "name" : "Cllr Ossian Smyth",
      "screen_name" : "smytho",
      "protected" : false,
      "id_str" : "116560657",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009567049637662722\/ytIAvLDW_normal.jpg",
      "id" : 116560657,
      "verified" : false
    }
  },
  "id" : 795315856301522948,
  "created_at" : "2016-11-06 17:24:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/5HoBKddjwn",
      "expanded_url" : "http:\/\/www.unofficialgoogledatascience.com\/2016\/10\/practical-advice-for-analysis-of-large.html",
      "display_url" : "unofficialgoogledatascience.com\/2016\/10\/practi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795277272827752449",
  "text" : "Practical advice for analysis of large, complex data sets \nhttps:\/\/t.co\/5HoBKddjwn",
  "id" : 795277272827752449,
  "created_at" : "2016-11-06 14:50:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "indices" : [ 75, 85 ],
      "id_str" : "77901568",
      "id" : 77901568
    }, {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 86, 97 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/MnclK6QZni",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/10-reasons-why-you-should-attend-startup-weekend-dublin-duignan?trk=hp-feed-article-title-like",
      "display_url" : "linkedin.com\/pulse\/10-reaso\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "795275666472181760",
  "text" : "Startup Weekend Dublin back next week, sign up and get coach and mentor by @allentv4u @Clearpreso etc https:\/\/t.co\/MnclK6QZni",
  "id" : 795275666472181760,
  "created_at" : "2016-11-06 14:44:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795255722393174016",
  "text" : "November is upon us, which in the world of ecommerce means: Singles\u2019 Day \u5149\u68CD\u8282 (11.11 in China), Black Friday, and Cyber Monday",
  "id" : 795255722393174016,
  "created_at" : "2016-11-06 13:25:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alyson La",
      "screen_name" : "alysonlaaa",
      "indices" : [ 3, 14 ],
      "id_str" : "148927605",
      "id" : 148927605
    }, {
      "name" : "GitHub",
      "screen_name" : "github",
      "indices" : [ 55, 62 ],
      "id_str" : "13334762",
      "id" : 13334762
    }, {
      "name" : "Tableau Software",
      "screen_name" : "tableau",
      "indices" : [ 101, 109 ],
      "id_str" : "14792516",
      "id" : 14792516
    }, {
      "name" : "Looker",
      "screen_name" : "LookerData",
      "indices" : [ 110, 121 ],
      "id_str" : "1061101538",
      "id" : 1061101538
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BigQuery",
      "indices" : [ 85, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795253625534681088",
  "text" : "RT @alysonlaaa: Coming soon: Video all about analyzing @github's public data sets on #BigQuery using @tableau @LookerData and more. Thx to\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GitHub",
        "screen_name" : "github",
        "indices" : [ 39, 46 ],
        "id_str" : "13334762",
        "id" : 13334762
      }, {
        "name" : "Tableau Software",
        "screen_name" : "tableau",
        "indices" : [ 85, 93 ],
        "id_str" : "14792516",
        "id" : 14792516
      }, {
        "name" : "Looker",
        "screen_name" : "LookerData",
        "indices" : [ 94, 105 ],
        "id_str" : "1061101538",
        "id" : 1061101538
      }, {
        "name" : "Felipe Hoffa",
        "screen_name" : "felipehoffa",
        "indices" : [ 123, 135 ],
        "id_str" : "216735143",
        "id" : 216735143
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/alysonlaaa\/status\/793295467668877312\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/NH7KdMDDwm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwJaBHeUMAA65F6.jpg",
        "id_str" : "793295459603197952",
        "id" : 793295459603197952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwJaBHeUMAA65F6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NH7KdMDDwm"
      } ],
      "hashtags" : [ {
        "text" : "BigQuery",
        "indices" : [ 69, 78 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "793295467668877312",
    "text" : "Coming soon: Video all about analyzing @github's public data sets on #BigQuery using @tableau @LookerData and more. Thx to @felipehoffa \uD83D\uDCC8\u2728 https:\/\/t.co\/NH7KdMDDwm",
    "id" : 793295467668877312,
    "created_at" : "2016-11-01 03:35:52 +0000",
    "user" : {
      "name" : "Alyson La",
      "screen_name" : "alysonlaaa",
      "protected" : false,
      "id_str" : "148927605",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/573707548487909376\/WgKUw7r7_normal.jpeg",
      "id" : 148927605,
      "verified" : false
    }
  },
  "id" : 795253625534681088,
  "created_at" : "2016-11-06 13:16:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Leta Hong Fincher\u6D2A\u7406\u8FBE",
      "screen_name" : "LetaHong",
      "indices" : [ 3, 12 ],
      "id_str" : "292931596",
      "id" : 292931596
    }, {
      "name" : "#AAPIforHillary",
      "screen_name" : "AAPIforHillary",
      "indices" : [ 76, 91 ],
      "id_str" : "702296911265423361",
      "id" : 702296911265423361
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/LetaHong\/status\/784748355880677376\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/VdNGjlf5lj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CuP8dFfXEAIraxB.jpg",
      "id_str" : "784748336712716290",
      "id" : 784748336712716290,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CuP8dFfXEAIraxB.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VdNGjlf5lj"
    } ],
    "hashtags" : [ {
      "text" : "ImWithHer",
      "indices" : [ 92, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "795198988584710144",
  "text" : "RT @LetaHong: Asian American women for Hillary! Pennsylvania, here we come! @AAPIforHillary #ImWithHer https:\/\/t.co\/VdNGjlf5lj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "#AAPIforHillary",
        "screen_name" : "AAPIforHillary",
        "indices" : [ 62, 77 ],
        "id_str" : "702296911265423361",
        "id" : 702296911265423361
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LetaHong\/status\/784748355880677376\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/VdNGjlf5lj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CuP8dFfXEAIraxB.jpg",
        "id_str" : "784748336712716290",
        "id" : 784748336712716290,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CuP8dFfXEAIraxB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VdNGjlf5lj"
      } ],
      "hashtags" : [ {
        "text" : "ImWithHer",
        "indices" : [ 78, 88 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "784748355880677376",
    "text" : "Asian American women for Hillary! Pennsylvania, here we come! @AAPIforHillary #ImWithHer https:\/\/t.co\/VdNGjlf5lj",
    "id" : 784748355880677376,
    "created_at" : "2016-10-08 13:32:42 +0000",
    "user" : {
      "name" : "Leta Hong Fincher\u6D2A\u7406\u8FBE",
      "screen_name" : "LetaHong",
      "protected" : false,
      "id_str" : "292931596",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963268792590782465\/TlZIJjRE_normal.jpg",
      "id" : 292931596,
      "verified" : false
    }
  },
  "id" : 795198988584710144,
  "created_at" : "2016-11-06 09:39:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Azeem Azhar",
      "screen_name" : "azeem",
      "indices" : [ 3, 9 ],
      "id_str" : "7640782",
      "id" : 7640782
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/3sooQXrT2w",
      "expanded_url" : "http:\/\/azm.io\/2eFUeiW",
      "display_url" : "azm.io\/2eFUeiW"
    } ]
  },
  "geo" : { },
  "id_str" : "794931011037593600",
  "text" : "RT @azeem: Data mining reveals that Pokemon Go players increased their activity levels by 25 percent on average https:\/\/t.co\/3sooQXrT2w",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/3sooQXrT2w",
        "expanded_url" : "http:\/\/azm.io\/2eFUeiW",
        "display_url" : "azm.io\/2eFUeiW"
      } ]
    },
    "geo" : { },
    "id_str" : "790118820321058817",
    "text" : "Data mining reveals that Pokemon Go players increased their activity levels by 25 percent on average https:\/\/t.co\/3sooQXrT2w",
    "id" : 790118820321058817,
    "created_at" : "2016-10-23 09:13:01 +0000",
    "user" : {
      "name" : "Azeem Azhar",
      "screen_name" : "azeem",
      "protected" : false,
      "id_str" : "7640782",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/847767475169222656\/ZFJMki2F_normal.jpg",
      "id" : 7640782,
      "verified" : true
    }
  },
  "id" : 794931011037593600,
  "created_at" : "2016-11-05 15:54:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Francois Ajenstat",
      "screen_name" : "Ajenstat",
      "indices" : [ 3, 12 ],
      "id_str" : "149336325",
      "id" : 149336325
    }, {
      "name" : "Tableau Software",
      "screen_name" : "tableau",
      "indices" : [ 58, 66 ],
      "id_str" : "14792516",
      "id" : 14792516
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "python",
      "indices" : [ 28, 35 ]
    }, {
      "text" : "Data16",
      "indices" : [ 130, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/8zJQGMWNov",
      "expanded_url" : "http:\/\/www.tableau.com\/about\/blog\/2016\/11\/leverage-power-python-tableau-tabpy-62077",
      "display_url" : "tableau.com\/about\/blog\/201\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794926722265337856",
  "text" : "RT @Ajenstat: Yes! Finally, #python available directly in @tableau. Will be great for advanced analytics. https:\/\/t.co\/8zJQGMWNov #Data16",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tableau Software",
        "screen_name" : "tableau",
        "indices" : [ 44, 52 ],
        "id_str" : "14792516",
        "id" : 14792516
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "python",
        "indices" : [ 14, 21 ]
      }, {
        "text" : "Data16",
        "indices" : [ 116, 123 ]
      } ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/8zJQGMWNov",
        "expanded_url" : "http:\/\/www.tableau.com\/about\/blog\/2016\/11\/leverage-power-python-tableau-tabpy-62077",
        "display_url" : "tableau.com\/about\/blog\/201\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "794622596293660672",
    "text" : "Yes! Finally, #python available directly in @tableau. Will be great for advanced analytics. https:\/\/t.co\/8zJQGMWNov #Data16",
    "id" : 794622596293660672,
    "created_at" : "2016-11-04 19:29:24 +0000",
    "user" : {
      "name" : "Francois Ajenstat",
      "screen_name" : "Ajenstat",
      "protected" : false,
      "id_str" : "149336325",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/993617099493859328\/2TlAyYsm_normal.jpg",
      "id" : 149336325,
      "verified" : false
    }
  },
  "id" : 794926722265337856,
  "created_at" : "2016-11-05 15:37:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794920548983832576",
  "text" : "\"The alternative to thinking ahead would be to think backwards...and that's just remembering\" - Sheldon, The Big Bang Theory",
  "id" : 794920548983832576,
  "created_at" : "2016-11-05 15:13:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "indices" : [ 3, 17 ],
      "id_str" : "52052800",
      "id" : 52052800
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ireland",
      "indices" : [ 82, 90 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794915038071062532",
  "text" : "RT @DrUmarAlQadri: This was inevitable. His death means the end of the threat for #ireland from him. He ws the 1st Irish suicide bomber &amp; h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RT\u00C9",
        "screen_name" : "rte",
        "indices" : [ 138, 142 ],
        "id_str" : "1245699895",
        "id" : 1245699895
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ireland",
        "indices" : [ 63, 71 ]
      } ],
      "urls" : [ {
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/TQISNtOGnP",
        "expanded_url" : "https:\/\/twitter.com\/ajaltamimi\/status\/794664472317165569",
        "display_url" : "twitter.com\/ajaltamimi\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "794846845344292864",
    "text" : "This was inevitable. His death means the end of the threat for #ireland from him. He ws the 1st Irish suicide bomber &amp; hopefully last @rte https:\/\/t.co\/TQISNtOGnP",
    "id" : 794846845344292864,
    "created_at" : "2016-11-05 10:20:30 +0000",
    "user" : {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "protected" : false,
      "id_str" : "52052800",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026918392907919360\/mMi9i844_normal.jpg",
      "id" : 52052800,
      "verified" : false
    }
  },
  "id" : 794915038071062532,
  "created_at" : "2016-11-05 14:51:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/794879384587005952\/photo\/1",
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/34JG6DVHMx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwf6lcfXAAAoIbq.jpg",
      "id_str" : "794879380464009216",
      "id" : 794879380464009216,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwf6lcfXAAAoIbq.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 384,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/34JG6DVHMx"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794902101726031872",
  "text" : "RT @rshotton: The Ladybird view of meetings is, unfortunately, accurate\r\rFrom The Meeting https:\/\/t.co\/34JG6DVHMx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/794879384587005952\/photo\/1",
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/34JG6DVHMx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cwf6lcfXAAAoIbq.jpg",
        "id_str" : "794879380464009216",
        "id" : 794879380464009216,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cwf6lcfXAAAoIbq.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 384,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/34JG6DVHMx"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "794879384587005952",
    "text" : "The Ladybird view of meetings is, unfortunately, accurate\r\rFrom The Meeting https:\/\/t.co\/34JG6DVHMx",
    "id" : 794879384587005952,
    "created_at" : "2016-11-05 12:29:48 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 794902101726031872,
  "created_at" : "2016-11-05 14:00:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/gHwj5Kxcbj",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BMbL6R9hG5a\/",
      "display_url" : "instagram.com\/p\/BMbL6R9hG5a\/"
    } ]
  },
  "geo" : { },
  "id_str" : "794901734669828096",
  "text" : "Red Arrow in Singapore. https:\/\/t.co\/gHwj5Kxcbj",
  "id" : 794901734669828096,
  "created_at" : "2016-11-05 13:58:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/ecsG9uUSM4",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/794853913849511936",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794859782981242885",
  "text" : "\uD83D\uDE02 https:\/\/t.co\/ecsG9uUSM4",
  "id" : 794859782981242885,
  "created_at" : "2016-11-05 11:11:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/56RWdhVHrJ",
      "expanded_url" : "http:\/\/dlvr.it\/MbbJCJ",
      "display_url" : "dlvr.it\/MbbJCJ"
    } ]
  },
  "geo" : { },
  "id_str" : "794802937696579584",
  "text" : "RT @ChannelNewsAsia: Corolla, Toyota's car for the masses, turns 50 https:\/\/t.co\/56RWdhVHrJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/56RWdhVHrJ",
        "expanded_url" : "http:\/\/dlvr.it\/MbbJCJ",
        "display_url" : "dlvr.it\/MbbJCJ"
      } ]
    },
    "geo" : { },
    "id_str" : "794745572146040832",
    "text" : "Corolla, Toyota's car for the masses, turns 50 https:\/\/t.co\/56RWdhVHrJ",
    "id" : 794745572146040832,
    "created_at" : "2016-11-05 03:38:04 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 794802937696579584,
  "created_at" : "2016-11-05 07:26:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/lwMb5EjDjl",
      "expanded_url" : "https:\/\/twitter.com\/share_ireland\/status\/794490669477806081",
      "display_url" : "twitter.com\/share_ireland\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794599479185993728",
  "text" : "Excuse to add: Some feral kids throwing stones at me. https:\/\/t.co\/lwMb5EjDjl",
  "id" : 794599479185993728,
  "created_at" : "2016-11-04 17:57:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/H0EtJYwudI",
      "expanded_url" : "https:\/\/twitter.com\/historyinmoment\/status\/794544050862964738",
      "display_url" : "twitter.com\/historyinmomen\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794554419966775296",
  "text" : "I want names. Who your boss? https:\/\/t.co\/H0EtJYwudI",
  "id" : 794554419966775296,
  "created_at" : "2016-11-04 14:58:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ant O'Fearghail",
      "screen_name" : "aofarre",
      "indices" : [ 3, 11 ],
      "id_str" : "139702052",
      "id" : 139702052
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794512987063205888",
  "text" : "RT @aofarre: Ireland's Minister for Enterprise (pictured below driving home from work) says UK's Brexit negotiators don't know what they ar\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/LURigoC1i6",
        "expanded_url" : "https:\/\/twitter.com\/Boglawyer\/status\/794498097372135424",
        "display_url" : "twitter.com\/Boglawyer\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "794509701924917248",
    "text" : "Ireland's Minister for Enterprise (pictured below driving home from work) says UK's Brexit negotiators don't know what they are doing. https:\/\/t.co\/LURigoC1i6",
    "id" : 794509701924917248,
    "created_at" : "2016-11-04 12:00:48 +0000",
    "user" : {
      "name" : "Ant O'Fearghail",
      "screen_name" : "aofarre",
      "protected" : false,
      "id_str" : "139702052",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025583227443007488\/OYjDXGuU_normal.jpg",
      "id" : 139702052,
      "verified" : false
    }
  },
  "id" : 794512987063205888,
  "created_at" : "2016-11-04 12:13:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794511677437935616",
  "text" : "Learn that election day is not a national holiday in US.",
  "id" : 794511677437935616,
  "created_at" : "2016-11-04 12:08:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ashwin Mahesh",
      "screen_name" : "ashwinmahesh",
      "indices" : [ 3, 16 ],
      "id_str" : "376138251",
      "id" : 376138251
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794492243004583937",
  "text" : "RT @ashwinmahesh: Singapore realised that to work on policy effectively, the city has to hire data scientists, technologists. Feng Yuan Liu\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CitiesontheMove",
        "indices" : [ 122, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "794350939628916736",
    "text" : "Singapore realised that to work on policy effectively, the city has to hire data scientists, technologists. Feng Yuan Liu #CitiesontheMove",
    "id" : 794350939628916736,
    "created_at" : "2016-11-04 01:29:56 +0000",
    "user" : {
      "name" : "Ashwin Mahesh",
      "screen_name" : "ashwinmahesh",
      "protected" : false,
      "id_str" : "376138251",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/414083019184427008\/uWEXnj-w_normal.jpeg",
      "id" : 376138251,
      "verified" : false
    }
  },
  "id" : 794492243004583937,
  "created_at" : "2016-11-04 10:51:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Priscilla Nu\u00F1ez",
      "screen_name" : "solutions",
      "indices" : [ 3, 13 ],
      "id_str" : "10038722",
      "id" : 10038722
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/solutions\/status\/794489010286718976\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/j48Kt8JcVl",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwaXiZeWgAEHs-Y.jpg",
      "id_str" : "794489001487007745",
      "id" : 794489001487007745,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwaXiZeWgAEHs-Y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/j48Kt8JcVl"
    } ],
    "hashtags" : [ {
      "text" : "DataScience",
      "indices" : [ 34, 46 ]
    }, {
      "text" : "machinelearning",
      "indices" : [ 99, 115 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/jtKisuvg2u",
      "expanded_url" : "http:\/\/mobile.eweek.com\/cloud\/microsoft-offering-data-science-virtual-machine-test-drives.html",
      "display_url" : "mobile.eweek.com\/cloud\/microsof\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794491862249771008",
  "text" : "RT @solutions: Microsoft Offering #DataScience Virtual Machine Test Drives https:\/\/t.co\/jtKisuvg2u #machinelearning https:\/\/t.co\/j48Kt8JcVl",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/solutions\/status\/794489010286718976\/photo\/1",
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/j48Kt8JcVl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwaXiZeWgAEHs-Y.jpg",
        "id_str" : "794489001487007745",
        "id" : 794489001487007745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwaXiZeWgAEHs-Y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/j48Kt8JcVl"
      } ],
      "hashtags" : [ {
        "text" : "DataScience",
        "indices" : [ 19, 31 ]
      }, {
        "text" : "machinelearning",
        "indices" : [ 84, 100 ]
      } ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/jtKisuvg2u",
        "expanded_url" : "http:\/\/mobile.eweek.com\/cloud\/microsoft-offering-data-science-virtual-machine-test-drives.html",
        "display_url" : "mobile.eweek.com\/cloud\/microsof\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "794489010286718976",
    "text" : "Microsoft Offering #DataScience Virtual Machine Test Drives https:\/\/t.co\/jtKisuvg2u #machinelearning https:\/\/t.co\/j48Kt8JcVl",
    "id" : 794489010286718976,
    "created_at" : "2016-11-04 10:38:35 +0000",
    "user" : {
      "name" : "Priscilla Nu\u00F1ez",
      "screen_name" : "solutions",
      "protected" : false,
      "id_str" : "10038722",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038447654227849216\/YX3boOw-_normal.jpg",
      "id" : 10038722,
      "verified" : false
    }
  },
  "id" : 794491862249771008,
  "created_at" : "2016-11-04 10:49:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maya Goodfellow",
      "screen_name" : "MayaGoodfellow",
      "indices" : [ 3, 18 ],
      "id_str" : "71318028",
      "id" : 71318028
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794491173876465665",
  "text" : "RT @MayaGoodfellow: Sadly this is now pretty unsurprising: Gina Miller subjected to racist abuse after Brexit legal victory https:\/\/t.co\/bm\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/bmezfoaVMR",
        "expanded_url" : "http:\/\/www.independent.co.uk\/news\/uk\/home-news\/gina-miller-online-abuse-high-court-brexit-victory-article-50-a7396336.html",
        "display_url" : "independent.co.uk\/news\/uk\/home-n\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "794475480913182720",
    "text" : "Sadly this is now pretty unsurprising: Gina Miller subjected to racist abuse after Brexit legal victory https:\/\/t.co\/bmezfoaVMR",
    "id" : 794475480913182720,
    "created_at" : "2016-11-04 09:44:49 +0000",
    "user" : {
      "name" : "Maya Goodfellow",
      "screen_name" : "MayaGoodfellow",
      "protected" : false,
      "id_str" : "71318028",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/874343000272506886\/OVA6Mp84_normal.jpg",
      "id" : 71318028,
      "verified" : false
    }
  },
  "id" : 794491173876465665,
  "created_at" : "2016-11-04 10:47:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/rfhodEFBQz",
      "expanded_url" : "https:\/\/mryap.shinyapps.io\/learn_r-markdown\/",
      "display_url" : "mryap.shinyapps.io\/learn_r-markdo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794323659741679627",
  "text" : "Learn R-Markdown formatting to transform your plain text https:\/\/t.co\/rfhodEFBQz",
  "id" : 794323659741679627,
  "created_at" : "2016-11-03 23:41:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/794295540322357248\/photo\/1",
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/sFwsz2wTs2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwXnjdTXgAAFNIQ.jpg",
      "id_str" : "794295505648058368",
      "id" : 794295505648058368,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwXnjdTXgAAFNIQ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 384,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1360
      }, {
        "h" : 678,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1360
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sFwsz2wTs2"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794295540322357248",
  "text" : "Trying to turn my analyses into an interactive web applications. https:\/\/t.co\/sFwsz2wTs2",
  "id" : 794295540322357248,
  "created_at" : "2016-11-03 21:49:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 3, 12 ],
      "id_str" : "19456433",
      "id" : 19456433
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FENMUN",
      "indices" : [ 74, 81 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794275263400050689",
  "text" : "RT @jonboyes: Starting to think Mourinho is still employed by Chelsea.. \uD83D\uDE02 #FENMUN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "FENMUN",
        "indices" : [ 60, 67 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "794260780480888832",
    "text" : "Starting to think Mourinho is still employed by Chelsea.. \uD83D\uDE02 #FENMUN",
    "id" : 794260780480888832,
    "created_at" : "2016-11-03 19:31:41 +0000",
    "user" : {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "protected" : false,
      "id_str" : "19456433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3386817686\/4b1b7303154ea92ea270a5afec16da70_normal.jpeg",
      "id" : 19456433,
      "verified" : false
    }
  },
  "id" : 794275263400050689,
  "created_at" : "2016-11-03 20:29:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Benefacts",
      "screen_name" : "benefacts_ie",
      "indices" : [ 3, 16 ],
      "id_str" : "3254343526",
      "id" : 3254343526
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "nonprofit",
      "indices" : [ 45, 55 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794249371286142976",
  "text" : "RT @benefacts_ie: Our database covers 18,588 #nonprofit organisations in Ireland, by sector and location. Explore this data here: https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "nonprofit",
        "indices" : [ 27, 37 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/ba8xvZOQ3q",
        "expanded_url" : "https:\/\/benefacts.ie\/Explore",
        "display_url" : "benefacts.ie\/Explore"
      } ]
    },
    "geo" : { },
    "id_str" : "793397339297812480",
    "text" : "Our database covers 18,588 #nonprofit organisations in Ireland, by sector and location. Explore this data here: https:\/\/t.co\/ba8xvZOQ3q",
    "id" : 793397339297812480,
    "created_at" : "2016-11-01 10:20:40 +0000",
    "user" : {
      "name" : "Benefacts",
      "screen_name" : "benefacts_ie",
      "protected" : false,
      "id_str" : "3254343526",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/707239080338149376\/XGuW5QQr_normal.jpg",
      "id" : 3254343526,
      "verified" : false
    }
  },
  "id" : 794249371286142976,
  "created_at" : "2016-11-03 18:46:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/yRYinZ2hgp",
      "expanded_url" : "http:\/\/technical.ly\/baltimore\/2016\/11\/01\/johns-hopkins-genome-algorithm-wine\/",
      "display_url" : "technical.ly\/baltimore\/2016\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794248660292960256",
  "text" : "A new algorithm developed at Johns Hopkins could lead to better wine \uD83C\uDF77 https:\/\/t.co\/yRYinZ2hgp",
  "id" : 794248660292960256,
  "created_at" : "2016-11-03 18:43:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Collins",
      "screen_name" : "LJCollins_",
      "indices" : [ 3, 14 ],
      "id_str" : "861836838071226368",
      "id" : 861836838071226368
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/5w3Yke2TsQ",
      "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/793866825507364864",
      "display_url" : "twitter.com\/googleanalytic\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794246786315644929",
  "text" : "RT @LJCollins_: Thrilled to be included in this list of accomplished women! https:\/\/t.co\/5w3Yke2TsQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/5w3Yke2TsQ",
        "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/793866825507364864",
        "display_url" : "twitter.com\/googleanalytic\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "793974425301331969",
    "text" : "Thrilled to be included in this list of accomplished women! https:\/\/t.co\/5w3Yke2TsQ",
    "id" : 793974425301331969,
    "created_at" : "2016-11-03 00:33:48 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 794246786315644929,
  "created_at" : "2016-11-03 18:36:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794239106381389824",
  "text" : "TIL \"Triage\" comes from the French word trier meaning \"to sort, sift or classify\"",
  "id" : 794239106381389824,
  "created_at" : "2016-11-03 18:05:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andy Cotgreave",
      "screen_name" : "acotgreave",
      "indices" : [ 0, 11 ],
      "id_str" : "119704541",
      "id" : 119704541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "794219395920887808",
  "geo" : { },
  "id_str" : "794235909625544705",
  "in_reply_to_user_id" : 119704541,
  "text" : "@acotgreave Big Data Tsunami?",
  "id" : 794235909625544705,
  "in_reply_to_status_id" : 794219395920887808,
  "created_at" : "2016-11-03 17:52:51 +0000",
  "in_reply_to_screen_name" : "acotgreave",
  "in_reply_to_user_id_str" : "119704541",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/794235480867012608\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/Dqrph64Ngn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwWw1vDXcAAvWa1.jpg",
      "id_str" : "794235346510901248",
      "id" : 794235346510901248,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwWw1vDXcAAvWa1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Dqrph64Ngn"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794235480867012608",
  "text" : "I have no idea I am running a RStudio server locally on my computer.... https:\/\/t.co\/Dqrph64Ngn",
  "id" : 794235480867012608,
  "created_at" : "2016-11-03 17:51:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NCIRL",
      "screen_name" : "NCIRL",
      "indices" : [ 21, 27 ],
      "id_str" : "12319392",
      "id" : 12319392
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HDSDA",
      "indices" : [ 37, 43 ]
    }, {
      "text" : "digimarknci",
      "indices" : [ 62, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794234632740999168",
  "text" : "Some hashtag used by @NCIRL students #HDSDA (Data Analytics), #digimarknci (Digital Marketing)",
  "id" : 794234632740999168,
  "created_at" : "2016-11-03 17:47:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Newsweek",
      "screen_name" : "Newsweek",
      "indices" : [ 3, 12 ],
      "id_str" : "2884771",
      "id" : 2884771
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Newsweek\/status\/794212550242025473\/photo\/1",
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/LgRwhsIHp8",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwWcGqzXgAIPXe1.jpg",
      "id_str" : "794212547683647490",
      "id" : 794212547683647490,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwWcGqzXgAIPXe1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 261,
        "resize" : "fit",
        "w" : 391
      }, {
        "h" : 261,
        "resize" : "fit",
        "w" : 391
      }, {
        "h" : 261,
        "resize" : "fit",
        "w" : 391
      }, {
        "h" : 261,
        "resize" : "fit",
        "w" : 391
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LgRwhsIHp8"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/xsJbAPvYbA",
      "expanded_url" : "http:\/\/bit.ly\/2fiuL0G",
      "display_url" : "bit.ly\/2fiuL0G"
    } ]
  },
  "geo" : { },
  "id_str" : "794228228038754304",
  "text" : "RT @Newsweek: Nigeria\u2019s top blogger launches a Facebook alternative https:\/\/t.co\/xsJbAPvYbA https:\/\/t.co\/LgRwhsIHp8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Newsweek\/status\/794212550242025473\/photo\/1",
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/LgRwhsIHp8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwWcGqzXgAIPXe1.jpg",
        "id_str" : "794212547683647490",
        "id" : 794212547683647490,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwWcGqzXgAIPXe1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 261,
          "resize" : "fit",
          "w" : 391
        }, {
          "h" : 261,
          "resize" : "fit",
          "w" : 391
        }, {
          "h" : 261,
          "resize" : "fit",
          "w" : 391
        }, {
          "h" : 261,
          "resize" : "fit",
          "w" : 391
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LgRwhsIHp8"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/xsJbAPvYbA",
        "expanded_url" : "http:\/\/bit.ly\/2fiuL0G",
        "display_url" : "bit.ly\/2fiuL0G"
      } ]
    },
    "geo" : { },
    "id_str" : "794212550242025473",
    "text" : "Nigeria\u2019s top blogger launches a Facebook alternative https:\/\/t.co\/xsJbAPvYbA https:\/\/t.co\/LgRwhsIHp8",
    "id" : 794212550242025473,
    "created_at" : "2016-11-03 16:20:02 +0000",
    "user" : {
      "name" : "Newsweek",
      "screen_name" : "Newsweek",
      "protected" : false,
      "id_str" : "2884771",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/741603495929905152\/di0NxkFa_normal.jpg",
      "id" : 2884771,
      "verified" : true
    }
  },
  "id" : 794228228038754304,
  "created_at" : "2016-11-03 17:22:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794226150293831680",
  "text" : "By indicating to Gmail some emails in the spam folders are not spam, I telling machine (i.e. computer)  to learn from examples.",
  "id" : 794226150293831680,
  "created_at" : "2016-11-03 17:14:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SuperValu Ireland",
      "screen_name" : "SuperValuIRL",
      "indices" : [ 37, 50 ],
      "id_str" : "260784447",
      "id" : 260784447
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794191742266392577",
  "text" : "It is just me? Overall the staffs at @SuperValuIRL Walkinstown are not very helpful and friendly.",
  "id" : 794191742266392577,
  "created_at" : "2016-11-03 14:57:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/sfUoInZxl0",
      "expanded_url" : "https:\/\/twitter.com\/dlabanyi\/status\/793791729677852672",
      "display_url" : "twitter.com\/dlabanyi\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "794162946905620480",
  "text" : "News 24 hours ago.... https:\/\/t.co\/sfUoInZxl0",
  "id" : 794162946905620480,
  "created_at" : "2016-11-03 13:02:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ricardo Bion",
      "screen_name" : "ricardobion",
      "indices" : [ 3, 15 ],
      "id_str" : "150705545",
      "id" : 150705545
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 122, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "794159179598594052",
  "text" : "RT @ricardobion: Airbnb just open sourced its Knowledge Repository, a framework to share reproducible analyses written in #rstats https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ricardobion\/status\/793962225387991040\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/IbzhrNTdbb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwS4X85UUAAZJWY.jpg",
        "id_str" : "793962155947085824",
        "id" : 793962155947085824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwS4X85UUAAZJWY.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 643
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 846,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 846,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 846,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IbzhrNTdbb"
      } ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 105, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/Kl4MMx7YyN",
        "expanded_url" : "https:\/\/github.com\/airbnb\/knowledge-repo",
        "display_url" : "github.com\/airbnb\/knowled\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "793962225387991040",
    "text" : "Airbnb just open sourced its Knowledge Repository, a framework to share reproducible analyses written in #rstats https:\/\/t.co\/Kl4MMx7YyN https:\/\/t.co\/IbzhrNTdbb",
    "id" : 793962225387991040,
    "created_at" : "2016-11-02 23:45:20 +0000",
    "user" : {
      "name" : "Ricardo Bion",
      "screen_name" : "ricardobion",
      "protected" : false,
      "id_str" : "150705545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/775553552882118656\/ZgW0arvV_normal.jpg",
      "id" : 150705545,
      "verified" : false
    }
  },
  "id" : 794159179598594052,
  "created_at" : "2016-11-03 12:47:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christian Reilly",
      "screen_name" : "reillyusa",
      "indices" : [ 3, 13 ],
      "id_str" : "15128930",
      "id" : 15128930
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793897820642246656",
  "text" : "RT @reillyusa: What happened to Yammer?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "793874590221107200",
    "text" : "What happened to Yammer?",
    "id" : 793874590221107200,
    "created_at" : "2016-11-02 17:57:06 +0000",
    "user" : {
      "name" : "Christian Reilly",
      "screen_name" : "reillyusa",
      "protected" : false,
      "id_str" : "15128930",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1041332592308563969\/yb1Yx9sP_normal.jpg",
      "id" : 15128930,
      "verified" : false
    }
  },
  "id" : 793897820642246656,
  "created_at" : "2016-11-02 19:29:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jenn Kunz",
      "screen_name" : "Jenn_Kunz",
      "indices" : [ 3, 13 ],
      "id_str" : "236914694",
      "id" : 236914694
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793893785986068480",
  "text" : "RT @Jenn_Kunz: Is it just me or has the word \u201Crefactor\u201D come to mean \u201Cany improvement to code\u201D?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "793849811921350660",
    "text" : "Is it just me or has the word \u201Crefactor\u201D come to mean \u201Cany improvement to code\u201D?",
    "id" : 793849811921350660,
    "created_at" : "2016-11-02 16:18:38 +0000",
    "user" : {
      "name" : "Jenn Kunz",
      "screen_name" : "Jenn_Kunz",
      "protected" : false,
      "id_str" : "236914694",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/664532529458708480\/5ZcxgLDv_normal.png",
      "id" : 236914694,
      "verified" : false
    }
  },
  "id" : 793893785986068480,
  "created_at" : "2016-11-02 19:13:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tony Yates",
      "screen_name" : "t0nyyates",
      "indices" : [ 3, 13 ],
      "id_str" : "21510888",
      "id" : 21510888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/b20tw9cgDO",
      "expanded_url" : "https:\/\/twitter.com\/GreekGeordie\/status\/793487456305868801",
      "display_url" : "twitter.com\/GreekGeordie\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793885372254154753",
  "text" : "RT @t0nyyates: People wonder why there was a vote for Brexit. https:\/\/t.co\/b20tw9cgDO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/b20tw9cgDO",
        "expanded_url" : "https:\/\/twitter.com\/GreekGeordie\/status\/793487456305868801",
        "display_url" : "twitter.com\/GreekGeordie\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "793872738834407424",
    "text" : "People wonder why there was a vote for Brexit. https:\/\/t.co\/b20tw9cgDO",
    "id" : 793872738834407424,
    "created_at" : "2016-11-02 17:49:45 +0000",
    "user" : {
      "name" : "Tony Yates",
      "screen_name" : "t0nyyates",
      "protected" : false,
      "id_str" : "21510888",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/828646896398897154\/ydP4YECV_normal.jpg",
      "id" : 21510888,
      "verified" : false
    }
  },
  "id" : 793885372254154753,
  "created_at" : "2016-11-02 18:39:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "indices" : [ 3, 11 ],
      "id_str" : "13311832",
      "id" : 13311832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793880417459929089",
  "text" : "RT @dr_pete: Anyone have international SEO (especially localization issues) expertise in the UAE or general region?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "793875601480228864",
    "text" : "Anyone have international SEO (especially localization issues) expertise in the UAE or general region?",
    "id" : 793875601480228864,
    "created_at" : "2016-11-02 18:01:07 +0000",
    "user" : {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "protected" : false,
      "id_str" : "13311832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/542152616487038976\/-UUMowce_normal.jpeg",
      "id" : 13311832,
      "verified" : false
    }
  },
  "id" : 793880417459929089,
  "created_at" : "2016-11-02 18:20:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793880316922499074",
  "text" : "RT @ronanlyons: Enforced tenant rights &amp; significantly greater supply would solve these people's problems. Rent controls would hide them at\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 145, 168 ],
        "url" : "https:\/\/t.co\/YqXdRHhWVq",
        "expanded_url" : "https:\/\/twitter.com\/DublinTenants\/status\/793772142236147712",
        "display_url" : "twitter.com\/DublinTenants\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "793879843712724992",
    "text" : "Enforced tenant rights &amp; significantly greater supply would solve these people's problems. Rent controls would hide them at others' expense. https:\/\/t.co\/YqXdRHhWVq",
    "id" : 793879843712724992,
    "created_at" : "2016-11-02 18:17:58 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 793880316922499074,
  "created_at" : "2016-11-02 18:19:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/MMbZPzMRic",
      "expanded_url" : "http:\/\/www.nytimes.com\/2016\/11\/01\/world\/europe\/spooked-by-russia-tiny-estonia-trains-a-nation-of-insurgents.html?smid=fb-nytimes&smtyp=cur&_r=0",
      "display_url" : "nytimes.com\/2016\/11\/01\/wor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793872601500250113",
  "text" : "RT @yapphenghui: https:\/\/t.co\/MMbZPzMRic",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/MMbZPzMRic",
        "expanded_url" : "http:\/\/www.nytimes.com\/2016\/11\/01\/world\/europe\/spooked-by-russia-tiny-estonia-trains-a-nation-of-insurgents.html?smid=fb-nytimes&smtyp=cur&_r=0",
        "display_url" : "nytimes.com\/2016\/11\/01\/wor\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "793673303177043968",
    "text" : "https:\/\/t.co\/MMbZPzMRic",
    "id" : 793673303177043968,
    "created_at" : "2016-11-02 04:37:15 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 793872601500250113,
  "created_at" : "2016-11-02 17:49:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/SeqT5F7JeR",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/indonesian-women-killed-hong-kong-forgotten-home-080642115.html",
      "display_url" : "sg.news.yahoo.com\/indonesian-wom\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793871569814687744",
  "text" : "Indonesian women killed in Hong Kong are forgotten at home https:\/\/t.co\/SeqT5F7JeR",
  "id" : 793871569814687744,
  "created_at" : "2016-11-02 17:45:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793734499473432576",
  "text" : "RT @mrbrown: \"Singapore is leading the world in Smart City initiatives.\" -Neil Evans,  Director of IT &amp; Data centre, Huawei Australia.\n\nWOO\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "HID16",
        "indices" : [ 138, 144 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "793650340373549056",
    "text" : "\"Singapore is leading the world in Smart City initiatives.\" -Neil Evans,  Director of IT &amp; Data centre, Huawei Australia.\n\nWOOP WOOP! #HID16",
    "id" : 793650340373549056,
    "created_at" : "2016-11-02 03:06:01 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 793734499473432576,
  "created_at" : "2016-11-02 08:40:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    }, {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "indices" : [ 120, 128 ],
      "id_str" : "115624161",
      "id" : 115624161
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/Ps7JPnYvIr",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/xiaomi-lost-40bn-where-went-132852774.html?soc_src=social-sh&soc_trk=tw",
      "display_url" : "sg.news.yahoo.com\/xiaomi-lost-40\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793733119065071616",
  "text" : "RT @yapphenghui: How Xiaomi lost $40bn: Where it all went wrong for the 'Apple of the East' https:\/\/t.co\/Ps7JPnYvIr via @yahoosg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yahoo Singapore",
        "screen_name" : "YahooSG",
        "indices" : [ 103, 111 ],
        "id_str" : "115624161",
        "id" : 115624161
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/Ps7JPnYvIr",
        "expanded_url" : "https:\/\/sg.news.yahoo.com\/xiaomi-lost-40bn-where-went-132852774.html?soc_src=social-sh&soc_trk=tw",
        "display_url" : "sg.news.yahoo.com\/xiaomi-lost-40\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "793661864878092288",
    "text" : "How Xiaomi lost $40bn: Where it all went wrong for the 'Apple of the East' https:\/\/t.co\/Ps7JPnYvIr via @yahoosg",
    "id" : 793661864878092288,
    "created_at" : "2016-11-02 03:51:48 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 793733119065071616,
  "created_at" : "2016-11-02 08:34:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Teija Avila",
      "screen_name" : "TeijaRuo",
      "indices" : [ 3, 12 ],
      "id_str" : "3130789167",
      "id" : 3130789167
    }, {
      "name" : "IBM",
      "screen_name" : "IBM",
      "indices" : [ 110, 114 ],
      "id_str" : "18994444",
      "id" : 18994444
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TeijaRuo\/status\/793728181366886400\/photo\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/pBbIBWxVwJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CwPjkuQXEAEQLe0.jpg",
      "id_str" : "793728179378786305",
      "id" : 793728179378786305,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwPjkuQXEAEQLe0.jpg",
      "sizes" : [ {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/pBbIBWxVwJ"
    } ],
    "hashtags" : [ {
      "text" : "retailers",
      "indices" : [ 29, 39 ]
    }, {
      "text" : "algorithms",
      "indices" : [ 50, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/nZJ2IjwT02",
      "expanded_url" : "http:\/\/ow.ly\/nlr5305J3L9",
      "display_url" : "ow.ly\/nlr5305J3L9"
    } ]
  },
  "geo" : { },
  "id_str" : "793732775413252097",
  "text" : "RT @TeijaRuo: How aggressive #retailers are using #algorithms to beat the competition https:\/\/t.co\/nZJ2IjwT02 @IBM https:\/\/t.co\/pBbIBWxVwJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "IBM",
        "screen_name" : "IBM",
        "indices" : [ 96, 100 ],
        "id_str" : "18994444",
        "id" : 18994444
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TeijaRuo\/status\/793728181366886400\/photo\/1",
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/pBbIBWxVwJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwPjkuQXEAEQLe0.jpg",
        "id_str" : "793728179378786305",
        "id" : 793728179378786305,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwPjkuQXEAEQLe0.jpg",
        "sizes" : [ {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/pBbIBWxVwJ"
      } ],
      "hashtags" : [ {
        "text" : "retailers",
        "indices" : [ 15, 25 ]
      }, {
        "text" : "algorithms",
        "indices" : [ 36, 47 ]
      } ],
      "urls" : [ {
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/nZJ2IjwT02",
        "expanded_url" : "http:\/\/ow.ly\/nlr5305J3L9",
        "display_url" : "ow.ly\/nlr5305J3L9"
      } ]
    },
    "geo" : { },
    "id_str" : "793728181366886400",
    "text" : "How aggressive #retailers are using #algorithms to beat the competition https:\/\/t.co\/nZJ2IjwT02 @IBM https:\/\/t.co\/pBbIBWxVwJ",
    "id" : 793728181366886400,
    "created_at" : "2016-11-02 08:15:19 +0000",
    "user" : {
      "name" : "Teija Avila",
      "screen_name" : "TeijaRuo",
      "protected" : false,
      "id_str" : "3130789167",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/636786566048935936\/Blq5_Oko_normal.jpg",
      "id" : 3130789167,
      "verified" : false
    }
  },
  "id" : 793732775413252097,
  "created_at" : "2016-11-02 08:33:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GovTechSG",
      "indices" : [ 57, 67 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793721328931143680",
  "text" : "RT @GovTechSG: \"Build like LEGO, iterate as we go,\" says #GovTechSG CEO Jacqueline Poh at SITF CXO Breakfast Talk on this rainy morning. #S\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GovTechSG",
        "indices" : [ 42, 52 ]
      }, {
        "text" : "SITFCXO",
        "indices" : [ 122, 130 ]
      }, {
        "text" : "SITF",
        "indices" : [ 131, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "793628843646918656",
    "text" : "\"Build like LEGO, iterate as we go,\" says #GovTechSG CEO Jacqueline Poh at SITF CXO Breakfast Talk on this rainy morning. #SITFCXO #SITF",
    "id" : 793628843646918656,
    "created_at" : "2016-11-02 01:40:35 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 793721328931143680,
  "created_at" : "2016-11-02 07:48:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/tv6t5yAv8U",
      "expanded_url" : "http:\/\/betanews.com\/2016\/10\/30\/macbook-pro-2016-disappointment-pushes-some-apple-loyalists-to-ubuntu-linux\/",
      "display_url" : "betanews.com\/2016\/10\/30\/mac\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793573003758100480",
  "text" : "MacBook Pro (2016) disappointment pushes some Apple loyalists to Ubuntu Linux https:\/\/t.co\/tv6t5yAv8U",
  "id" : 793573003758100480,
  "created_at" : "2016-11-01 21:58:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zix",
      "screen_name" : "Zixxel",
      "indices" : [ 3, 10 ],
      "id_str" : "16127433",
      "id" : 16127433
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Zixxel\/status\/770940455110541312\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/sxew62XB6x",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrLuNa5WgAA66Yp.jpg",
      "id_str" : "770940400559357952",
      "id" : 770940400559357952,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrLuNa5WgAA66Yp.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 636,
        "resize" : "fit",
        "w" : 532
      }, {
        "h" : 636,
        "resize" : "fit",
        "w" : 532
      }, {
        "h" : 636,
        "resize" : "fit",
        "w" : 532
      }, {
        "h" : 636,
        "resize" : "fit",
        "w" : 532
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sxew62XB6x"
    } ],
    "hashtags" : [ {
      "text" : "csshumor",
      "indices" : [ 12, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793569670196592640",
  "text" : "RT @Zixxel: #csshumor .yacht \u007B\n   transform: rotate (90deg);\n   float: none;\n   vertical-align: bottom;\n\u007D https:\/\/t.co\/sxew62XB6x",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Zixxel\/status\/770940455110541312\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/sxew62XB6x",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrLuNa5WgAA66Yp.jpg",
        "id_str" : "770940400559357952",
        "id" : 770940400559357952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrLuNa5WgAA66Yp.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 636,
          "resize" : "fit",
          "w" : 532
        }, {
          "h" : 636,
          "resize" : "fit",
          "w" : 532
        }, {
          "h" : 636,
          "resize" : "fit",
          "w" : 532
        }, {
          "h" : 636,
          "resize" : "fit",
          "w" : 532
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sxew62XB6x"
      } ],
      "hashtags" : [ {
        "text" : "csshumor",
        "indices" : [ 0, 9 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "770940455110541312",
    "text" : "#csshumor .yacht \u007B\n   transform: rotate (90deg);\n   float: none;\n   vertical-align: bottom;\n\u007D https:\/\/t.co\/sxew62XB6x",
    "id" : 770940455110541312,
    "created_at" : "2016-08-31 11:05:02 +0000",
    "user" : {
      "name" : "Zix",
      "screen_name" : "Zixxel",
      "protected" : false,
      "id_str" : "16127433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/854396558284115969\/bAbwEEFa_normal.jpg",
      "id" : 16127433,
      "verified" : false
    }
  },
  "id" : 793569670196592640,
  "created_at" : "2016-11-01 21:45:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/fNTw6R0UCk",
      "expanded_url" : "http:\/\/www.jonzelner.net\/docker\/reproducibility\/2016\/06\/03\/docker\/",
      "display_url" : "jonzelner.net\/docker\/reprodu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793565383143858176",
  "text" : "It's not reproducible if it only runs on your laptop. https:\/\/t.co\/fNTw6R0UCk",
  "id" : 793565383143858176,
  "created_at" : "2016-11-01 21:28:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/dcDAsgqT8p",
      "expanded_url" : "http:\/\/www.todayonline.com\/singapore\/saf-looks-artificial-intelligence-gain-punch",
      "display_url" : "todayonline.com\/singapore\/saf-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793562116062142468",
  "text" : "Singapore is not the only country to leverage on AI to beef up its defence capabilities. https:\/\/t.co\/dcDAsgqT8p",
  "id" : 793562116062142468,
  "created_at" : "2016-11-01 21:15:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793505811444236288",
  "text" : "Any recommended SSD for a 3-years old laptop? The laptop hard drive sounds very loud and unusual when I boot up.",
  "id" : 793505811444236288,
  "created_at" : "2016-11-01 17:31:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/Ms5mhn4wBV",
      "expanded_url" : "https:\/\/twitter.com\/RentController\/status\/793162234721599489",
      "display_url" : "twitter.com\/RentController\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793503242919510021",
  "text" : "Take note Ireland. (Other things being equal) https:\/\/t.co\/Ms5mhn4wBV",
  "id" : 793503242919510021,
  "created_at" : "2016-11-01 17:21:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/RKA6YbRlfj",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BMRhOELhlWe\/",
      "display_url" : "instagram.com\/p\/BMRhOELhlWe\/"
    } ]
  },
  "geo" : { },
  "id_str" : "793485344301654016",
  "text" : "Books to read. https:\/\/t.co\/RKA6YbRlfj",
  "id" : 793485344301654016,
  "created_at" : "2016-11-01 16:10:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Rent Controller",
      "screen_name" : "RentController",
      "indices" : [ 3, 18 ],
      "id_str" : "4100470341",
      "id" : 4100470341
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/647yipkuKT",
      "expanded_url" : "http:\/\/www.bbc.com\/capital\/story\/20160517-this-is-one-city-where-youll-never-find-a-home",
      "display_url" : "bbc.com\/capital\/story\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "793418020727189505",
  "text" : "RT @RentController: This town's queue for rent-controlled housing is being considered by the Guinness World Records https:\/\/t.co\/647yipkuKT\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "katefaulkner",
        "screen_name" : "katefaulkner",
        "indices" : [ 120, 133 ],
        "id_str" : "18666521",
        "id" : 18666521
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/647yipkuKT",
        "expanded_url" : "http:\/\/www.bbc.com\/capital\/story\/20160517-this-is-one-city-where-youll-never-find-a-home",
        "display_url" : "bbc.com\/capital\/story\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "768319498227179520",
    "text" : "This town's queue for rent-controlled housing is being considered by the Guinness World Records https:\/\/t.co\/647yipkuKT @katefaulkner",
    "id" : 768319498227179520,
    "created_at" : "2016-08-24 05:30:17 +0000",
    "user" : {
      "name" : "The Rent Controller",
      "screen_name" : "RentController",
      "protected" : false,
      "id_str" : "4100470341",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/663481026094698496\/OvypFW9I_normal.jpg",
      "id" : 4100470341,
      "verified" : false
    }
  },
  "id" : 793418020727189505,
  "created_at" : "2016-11-01 11:42:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Lorica \u7F57\u745E\u5361",
      "screen_name" : "bigdata",
      "indices" : [ 3, 11 ],
      "id_str" : "18318677",
      "id" : 18318677
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StrataHadoop",
      "indices" : [ 13, 26 ]
    } ],
    "urls" : [ {
      "indices" : [ 120, 143 ],
      "url" : "https:\/\/t.co\/vdOFs28hvJ",
      "expanded_url" : "http:\/\/goo.gl\/3fSUa2",
      "display_url" : "goo.gl\/3fSUa2"
    } ]
  },
  "geo" : { },
  "id_str" : "793373779497984000",
  "text" : "RT @bigdata: #StrataHadoop Singapore\uD83C\uDDF8\uD83C\uDDEC  Sessions \u21E2 how some leading companies designed &amp; built their data platforms https:\/\/t.co\/vdOFs28hvJ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/bigdata\/status\/793302519883587585\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/58JUBhtADT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwJgXADUkAAzASI.jpg",
        "id_str" : "793302432637816832",
        "id" : 793302432637816832,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwJgXADUkAAzASI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 866,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 491,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1386
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1386
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/58JUBhtADT"
      } ],
      "hashtags" : [ {
        "text" : "StrataHadoop",
        "indices" : [ 0, 13 ]
      }, {
        "text" : "smartnation",
        "indices" : [ 131, 143 ]
      } ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/vdOFs28hvJ",
        "expanded_url" : "http:\/\/goo.gl\/3fSUa2",
        "display_url" : "goo.gl\/3fSUa2"
      } ]
    },
    "geo" : { },
    "id_str" : "793302519883587585",
    "text" : "#StrataHadoop Singapore\uD83C\uDDF8\uD83C\uDDEC  Sessions \u21E2 how some leading companies designed &amp; built their data platforms https:\/\/t.co\/vdOFs28hvJ #smartnation https:\/\/t.co\/58JUBhtADT",
    "id" : 793302519883587585,
    "created_at" : "2016-11-01 04:03:54 +0000",
    "user" : {
      "name" : "Ben Lorica \u7F57\u745E\u5361",
      "screen_name" : "bigdata",
      "protected" : false,
      "id_str" : "18318677",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/235298259\/bigdata_logo_center3_normal.jpg",
      "id" : 18318677,
      "verified" : false
    }
  },
  "id" : 793373779497984000,
  "created_at" : "2016-11-01 08:47:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "UN Refugee Agency",
      "screen_name" : "Refugees",
      "indices" : [ 3, 12 ],
      "id_str" : "14361155",
      "id" : 14361155
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "793373590565556224",
  "text" : "RT @Refugees: Refugees know best what they need. That's why they should have the power to decide how to manage their own budgets. https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Refugees\/status\/793320934623019008\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/TcpDKjiSB7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CwJxLyTWcAAXEc1.jpg",
        "id_str" : "793320931666063360",
        "id" : 793320931666063360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CwJxLyTWcAAXEc1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 947
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 947
        }, {
          "h" : 452,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 630,
          "resize" : "fit",
          "w" : 947
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/TcpDKjiSB7"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/WJcpQ9hbbu",
        "expanded_url" : "http:\/\/trib.al\/ONrdgdw",
        "display_url" : "trib.al\/ONrdgdw"
      } ]
    },
    "geo" : { },
    "id_str" : "793320934623019008",
    "text" : "Refugees know best what they need. That's why they should have the power to decide how to manage their own budgets. https:\/\/t.co\/WJcpQ9hbbu https:\/\/t.co\/TcpDKjiSB7",
    "id" : 793320934623019008,
    "created_at" : "2016-11-01 05:17:04 +0000",
    "user" : {
      "name" : "UN Refugee Agency",
      "screen_name" : "Refugees",
      "protected" : false,
      "id_str" : "14361155",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/486539514010013696\/nwAvlZ0v_normal.png",
      "id" : 14361155,
      "verified" : true
    }
  },
  "id" : 793373590565556224,
  "created_at" : "2016-11-01 08:46:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]