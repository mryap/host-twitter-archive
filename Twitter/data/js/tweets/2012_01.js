Grailbird.data.tweets_2012_01 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Martin Hawksey",
      "screen_name" : "mhawksey",
      "indices" : [ 76, 85 ],
      "id_str" : "13046992",
      "id" : 13046992
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 59, 65 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http:\/\/t.co\/Tbh4pFQG",
      "expanded_url" : "http:\/\/bit.ly\/y06zoR",
      "display_url" : "bit.ly\/y06zoR"
    } ]
  },
  "geo" : { },
  "id_str" : "164425961331359745",
  "text" : "Cobbled together http:\/\/t.co\/Tbh4pFQG to track & visualise #ixd12 Thanks to @mhawksey for the tool",
  "id" : 164425961331359745,
  "created_at" : "2012-01-31 19:12:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kindle",
      "screen_name" : "AmazonKindle",
      "indices" : [ 32, 45 ],
      "id_str" : "84249568",
      "id" : 84249568
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http:\/\/t.co\/XVHIpzh1",
      "expanded_url" : "http:\/\/amzn.to\/ygz9VH",
      "display_url" : "amzn.to\/ygz9VH"
    } ]
  },
  "geo" : { },
  "id_str" : "164384455958597632",
  "text" : "Bought: 'Designing Devices' via @amazonkindle offered FREE during the period of Interaction 12 Conference in Dublin http:\/\/t.co\/XVHIpzh1",
  "id" : 164384455958597632,
  "created_at" : "2012-01-31 16:28:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Binaebi Akah Calkins",
      "screen_name" : "siriomi",
      "indices" : [ 3, 11 ],
      "id_str" : "17125124",
      "id" : 17125124
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 19, 25 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 105 ],
      "url" : "http:\/\/t.co\/OPHWDslj",
      "expanded_url" : "http:\/\/siriomi.com\/p\/sketchnotes-field-guide\/",
      "display_url" : "siriomi.com\/p\/sketchnotes-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "164383095762595841",
  "text" : "RT @siriomi: Happy #ixd12! Do you have your copy of the Sketchnotes Field Guide yet? http:\/\/t.co\/OPHWDslj Free PDF for the length of the ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ixd12",
        "indices" : [ 6, 12 ]
      } ],
      "urls" : [ {
        "indices" : [ 72, 92 ],
        "url" : "http:\/\/t.co\/OPHWDslj",
        "expanded_url" : "http:\/\/siriomi.com\/p\/sketchnotes-field-guide\/",
        "display_url" : "siriomi.com\/p\/sketchnotes-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "164352081157828609",
    "text" : "Happy #ixd12! Do you have your copy of the Sketchnotes Field Guide yet? http:\/\/t.co\/OPHWDslj Free PDF for the length of the conference!",
    "id" : 164352081157828609,
    "created_at" : "2012-01-31 14:19:24 +0000",
    "user" : {
      "name" : "Binaebi Akah Calkins",
      "screen_name" : "siriomi",
      "protected" : false,
      "id_str" : "17125124",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/993607216790843392\/zrjCCkJr_normal.jpg",
      "id" : 17125124,
      "verified" : false
    }
  },
  "id" : 164383095762595841,
  "created_at" : "2012-01-31 16:22:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 49, 55 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http:\/\/t.co\/Tbh4pFQG",
      "expanded_url" : "http:\/\/bit.ly\/y06zoR",
      "display_url" : "bit.ly\/y06zoR"
    } ]
  },
  "geo" : { },
  "id_str" : "164352693597503491",
  "text" : "Interactive archive of twitter conversations for #ixd12 http:\/\/t.co\/Tbh4pFQG",
  "id" : 164352693597503491,
  "created_at" : "2012-01-31 14:21:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 73, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 107 ],
      "url" : "http:\/\/t.co\/PRUmPngN",
      "expanded_url" : "http:\/\/www.mckinseyquarterly.com\/Think_regionally_act_locally_Four_steps_to_reaching_the_Asian_consumer_2436",
      "display_url" : "mckinseyquarterly.com\/Think_regional\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "164335842721144833",
  "text" : "Think regionally, act locally: Four steps to reaching the Asian consumer #SMEcommunity http:\/\/t.co\/PRUmPngN",
  "id" : 164335842721144833,
  "created_at" : "2012-01-31 13:14:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 123, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164326663444897792",
  "text" : "Website must be: Findable (SEO, Ads) Trustable (Persuasion, Emotion, Trust) Usable (Usability) Measurable (Web anallytics) #SMEcommunity",
  "id" : 164326663444897792,
  "created_at" : "2012-01-31 12:38:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "abandonrate",
      "indices" : [ 126, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164321561195716608",
  "text" : "Am I the only one who added something to a shopping cart (just to check delivery charges) but didn't follow through purchase? #abandonrate",
  "id" : 164321561195716608,
  "created_at" : "2012-01-31 12:18:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GOOD",
      "screen_name" : "good",
      "indices" : [ 3, 8 ],
      "id_str" : "19621110",
      "id" : 19621110
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 102 ],
      "url" : "http:\/\/t.co\/Tru6gdGp",
      "expanded_url" : "http:\/\/su.pr\/28qfN7",
      "display_url" : "su.pr\/28qfN7"
    } ]
  },
  "geo" : { },
  "id_str" : "164290224149643264",
  "text" : "RT @GOOD: How do we prepare kids for jobs we can't imagine yet? Teach imagination http:\/\/t.co\/Tru6gdGp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.stumbleupon.com\/\" rel=\"nofollow\"\u003EStumbleUpon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 92 ],
        "url" : "http:\/\/t.co\/Tru6gdGp",
        "expanded_url" : "http:\/\/su.pr\/28qfN7",
        "display_url" : "su.pr\/28qfN7"
      } ]
    },
    "geo" : { },
    "id_str" : "164226991246163968",
    "text" : "How do we prepare kids for jobs we can't imagine yet? Teach imagination http:\/\/t.co\/Tru6gdGp",
    "id" : 164226991246163968,
    "created_at" : "2012-01-31 06:02:20 +0000",
    "user" : {
      "name" : "GOOD",
      "screen_name" : "good",
      "protected" : false,
      "id_str" : "19621110",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839881982574108672\/qN7BVlr4_normal.jpg",
      "id" : 19621110,
      "verified" : true
    }
  },
  "id" : 164290224149643264,
  "created_at" : "2012-01-31 10:13:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Saffer",
      "screen_name" : "odannyboy",
      "indices" : [ 3, 13 ],
      "id_str" : "3252",
      "id" : 3252
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IxD12",
      "indices" : [ 27, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164167896128430080",
  "text" : "RT @odannyboy: In honor of #IxD12 starting tomorrow and running until the end of the conference my Kindle ebook Designing Devices will b ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/sites.google.com\/site\/yorufukurou\/\" rel=\"nofollow\"\u003EYoruFukurou\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IxD12",
        "indices" : [ 12, 18 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "164141683028668418",
    "text" : "In honor of #IxD12 starting tomorrow and running until the end of the conference my Kindle ebook Designing Devices will be free to download!",
    "id" : 164141683028668418,
    "created_at" : "2012-01-31 00:23:21 +0000",
    "user" : {
      "name" : "Dan Saffer",
      "screen_name" : "odannyboy",
      "protected" : false,
      "id_str" : "3252",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1029076797915156482\/9QOgbx5Y_normal.jpg",
      "id" : 3252,
      "verified" : true
    }
  },
  "id" : 164167896128430080,
  "created_at" : "2012-01-31 02:07:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164167142651084800",
  "text" : "Shld not be up this late...mounting Skydrive on Window Explorer is not worth the time\/effort. Can only upload certain files type only.",
  "id" : 164167142651084800,
  "created_at" : "2012-01-31 02:04:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 87 ],
      "url" : "http:\/\/t.co\/mv51rfRk",
      "expanded_url" : "http:\/\/www.youtube.com\/watch?v=mEsnb3kUDAw&sns=fb",
      "display_url" : "youtube.com\/watch?v=mEsnb3\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "164102348736692226",
  "text" : "Surprise dance on Finnair Flight to celebrate India's Republic Day http:\/\/t.co\/mv51rfRk",
  "id" : 164102348736692226,
  "created_at" : "2012-01-30 21:47:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amanda",
      "screen_name" : "Hamperlady",
      "indices" : [ 0, 11 ],
      "id_str" : "19398524",
      "id" : 19398524
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http:\/\/t.co\/9bDzmvv8",
      "expanded_url" : "http:\/\/1.bp.blogspot.com\/-eKR5Rx1CUKE\/TvGVaymzWvI\/AAAAAAAAENA\/5jwqvYptqUo\/s1600\/puss-03.jpeg",
      "display_url" : "1.bp.blogspot.com\/-eKR5Rx1CUKE\/T\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "164080629821095936",
  "geo" : { },
  "id_str" : "164101569409859584",
  "in_reply_to_user_id" : 19398524,
  "text" : "@Hamperlady when they want something from you, they usually give you this look http:\/\/t.co\/9bDzmvv8",
  "id" : 164101569409859584,
  "in_reply_to_status_id" : 164080629821095936,
  "created_at" : "2012-01-30 21:43:57 +0000",
  "in_reply_to_screen_name" : "Hamperlady",
  "in_reply_to_user_id_str" : "19398524",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jodiemoule",
      "screen_name" : "jodiemoule",
      "indices" : [ 0, 11 ],
      "id_str" : "14318660",
      "id" : 14318660
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "164087240190992384",
  "geo" : { },
  "id_str" : "164099852844793856",
  "in_reply_to_user_id" : 14318660,
  "text" : "@jodiemoule oh...what happen? 2011 event was not on my radar",
  "id" : 164099852844793856,
  "in_reply_to_status_id" : 164087240190992384,
  "created_at" : "2012-01-30 21:37:08 +0000",
  "in_reply_to_screen_name" : "jodiemoule",
  "in_reply_to_user_id_str" : "14318660",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jodiemoule",
      "screen_name" : "jodiemoule",
      "indices" : [ 0, 11 ],
      "id_str" : "14318660",
      "id" : 14318660
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "164097055730581504",
  "geo" : { },
  "id_str" : "164097900685688832",
  "in_reply_to_user_id" : 14318660,
  "text" : "@jodiemoule I be there for the FREE workshop since I am based in Dublin. As freelancer, I do not have budgets for the conference :)",
  "id" : 164097900685688832,
  "in_reply_to_status_id" : 164097055730581504,
  "created_at" : "2012-01-30 21:29:22 +0000",
  "in_reply_to_screen_name" : "jodiemoule",
  "in_reply_to_user_id_str" : "14318660",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Catherine Shu",
      "screen_name" : "CatherineShu",
      "indices" : [ 3, 16 ],
      "id_str" : "7389732",
      "id" : 7389732
    }, {
      "name" : "Calvin Ho",
      "screen_name" : "calvinhyj",
      "indices" : [ 18, 28 ],
      "id_str" : "171987206",
      "id" : 171987206
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164097330671403008",
  "text" : "RT @CatherineShu: @calvinhyj Yeah, I'm surprised that it's a topic for \"debate.\" The process of acquiring a second language is rewarding ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Calvin Ho",
        "screen_name" : "calvinhyj",
        "indices" : [ 0, 10 ],
        "id_str" : "171987206",
        "id" : 171987206
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "164073721219399681",
    "in_reply_to_user_id" : 171987206,
    "text" : "@calvinhyj Yeah, I'm surprised that it's a topic for \"debate.\" The process of acquiring a second language is rewarding in and of itself.",
    "id" : 164073721219399681,
    "created_at" : "2012-01-30 19:53:18 +0000",
    "in_reply_to_screen_name" : "calvinhyj",
    "in_reply_to_user_id_str" : "171987206",
    "user" : {
      "name" : "Catherine Shu",
      "screen_name" : "CatherineShu",
      "protected" : false,
      "id_str" : "7389732",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885569689572614144\/OnNQWpPw_normal.jpg",
      "id" : 7389732,
      "verified" : true
    }
  },
  "id" : 164097330671403008,
  "created_at" : "2012-01-30 21:27:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jodiemoule",
      "screen_name" : "jodiemoule",
      "indices" : [ 0, 11 ],
      "id_str" : "14318660",
      "id" : 14318660
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 28, 34 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "164087240190992384",
  "geo" : { },
  "id_str" : "164096798523260928",
  "in_reply_to_user_id" : 14318660,
  "text" : "@jodiemoule The hash tag is #ixd12",
  "id" : 164096798523260928,
  "in_reply_to_status_id" : 164087240190992384,
  "created_at" : "2012-01-30 21:25:00 +0000",
  "in_reply_to_screen_name" : "jodiemoule",
  "in_reply_to_user_id_str" : "14318660",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "learn",
      "indices" : [ 27, 33 ]
    } ],
    "urls" : [ {
      "indices" : [ 35, 55 ],
      "url" : "http:\/\/t.co\/Z3d0W8WM",
      "expanded_url" : "http:\/\/blogs.forrester.com\/srividya_sridharan\/12-01-30-ci_fail_contextual_relevance_gone_bad",
      "display_url" : "blogs.forrester.com\/srividya_sridh\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "164027276588105729",
  "text" : "Contextual relevancy fail. #learn  http:\/\/t.co\/Z3d0W8WM",
  "id" : 164027276588105729,
  "created_at" : "2012-01-30 16:48:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "indices" : [ 0, 12 ],
      "id_str" : "10504042",
      "id" : 10504042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "163691330898366464",
  "geo" : { },
  "id_str" : "164025191926734851",
  "in_reply_to_user_id" : 10504042,
  "text" : "@ChuiSquared WP gives you more control over Tumblr. And I have the knowledge you can count on. :)",
  "id" : 164025191926734851,
  "in_reply_to_status_id" : 163691330898366464,
  "created_at" : "2012-01-30 16:40:27 +0000",
  "in_reply_to_screen_name" : "ChuiSquared",
  "in_reply_to_user_id_str" : "10504042",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164024620612206593",
  "text" : "\u90FD\u67CF\u6797\u8FD9\u51E0\u5929\u771F\u8981\u547D\uFF0C\u51B7\u98CE\u523A\u9AA8\uFF01",
  "id" : 164024620612206593,
  "created_at" : "2012-01-30 16:38:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DreamHost Status",
      "screen_name" : "dhstatus",
      "indices" : [ 3, 12 ],
      "id_str" : "8210302",
      "id" : 8210302
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "164023350266564611",
  "text" : "RT @dhstatus: Major Network Outage",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.dreamhoststatus.com\" rel=\"nofollow\"\u003EYOURLS: DH Status\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "164017288985587714",
    "text" : "Major Network Outage",
    "id" : 164017288985587714,
    "created_at" : "2012-01-30 16:09:03 +0000",
    "user" : {
      "name" : "DreamHost Status",
      "screen_name" : "dhstatus",
      "protected" : false,
      "id_str" : "8210302",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040733747497263104\/6L1CQWmY_normal.jpg",
      "id" : 8210302,
      "verified" : false
    }
  },
  "id" : 164023350266564611,
  "created_at" : "2012-01-30 16:33:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "indices" : [ 0, 12 ],
      "id_str" : "10504042",
      "id" : 10504042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "164019668519108609",
  "geo" : { },
  "id_str" : "164021866762219520",
  "in_reply_to_user_id" : 10504042,
  "text" : "@ChuiSquared I will! :)",
  "id" : 164021866762219520,
  "in_reply_to_status_id" : 164019668519108609,
  "created_at" : "2012-01-30 16:27:15 +0000",
  "in_reply_to_screen_name" : "ChuiSquared",
  "in_reply_to_user_id_str" : "10504042",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 37, 49 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 130 ],
      "url" : "http:\/\/t.co\/i4E4Bsrp",
      "expanded_url" : "http:\/\/sgpublicpolicy.wordpress.com\/2012\/01\/28\/happy-lunar-new-year\/",
      "display_url" : "sgpublicpolicy.wordpress.com\/2012\/01\/28\/hap\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "164019959821905920",
  "text" : "That a progress for Singapore media \u201C@yapphenghui: It was a memorable and bold moment in Singapore journalism http:\/\/t.co\/i4E4Bsrp\u201D",
  "id" : 164019959821905920,
  "created_at" : "2012-01-30 16:19:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nasdaq Webcasting",
      "screen_name" : "twebcasting",
      "indices" : [ 3, 15 ],
      "id_str" : "78100968",
      "id" : 78100968
    }, {
      "name" : "Nasdaq Webcasting",
      "screen_name" : "twebcasting",
      "indices" : [ 115, 127 ],
      "id_str" : "78100968",
      "id" : 78100968
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 91 ],
      "url" : "http:\/\/t.co\/7K6DjlqP",
      "expanded_url" : "http:\/\/bit.ly\/ywa26b",
      "display_url" : "bit.ly\/ywa26b"
    } ]
  },
  "geo" : { },
  "id_str" : "164019114074046464",
  "text" : "RT @twebcasting: Download the Video Best Practices Guide for Marketers http:\/\/t.co\/7K6DjlqP - new white paper from @twebcasting and @Mar ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Nasdaq Webcasting",
        "screen_name" : "twebcasting",
        "indices" : [ 98, 110 ],
        "id_str" : "78100968",
        "id" : 78100968
      }, {
        "name" : "Ann Handley",
        "screen_name" : "MarketingProfs",
        "indices" : [ 115, 130 ],
        "id_str" : "8596022",
        "id" : 8596022
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 74 ],
        "url" : "http:\/\/t.co\/7K6DjlqP",
        "expanded_url" : "http:\/\/bit.ly\/ywa26b",
        "display_url" : "bit.ly\/ywa26b"
      } ]
    },
    "geo" : { },
    "id_str" : "163998565969035264",
    "text" : "Download the Video Best Practices Guide for Marketers http:\/\/t.co\/7K6DjlqP - new white paper from @twebcasting and @MarketingProfs",
    "id" : 163998565969035264,
    "created_at" : "2012-01-30 14:54:39 +0000",
    "user" : {
      "name" : "Nasdaq Webcasting",
      "screen_name" : "twebcasting",
      "protected" : false,
      "id_str" : "78100968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/674602917446348806\/5_1xDbAy_normal.png",
      "id" : 78100968,
      "verified" : false
    }
  },
  "id" : 164019114074046464,
  "created_at" : "2012-01-30 16:16:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http:\/\/t.co\/BcvSkkLQ",
      "expanded_url" : "http:\/\/instagr.am\/p\/mM-ln\/",
      "display_url" : "instagr.am\/p\/mM-ln\/"
    } ]
  },
  "geo" : { },
  "id_str" : "163963027262160897",
  "text" : "Jaysu RT @paddyinpoland It's so cold in Poland my water froze when I was driving this morning :O http:\/\/t.co\/BcvSkkLQ",
  "id" : 163963027262160897,
  "created_at" : "2012-01-30 12:33:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163962541528203264",
  "text" : "I want to access Skydrive from my PC without going thru the web browser? Any tips? Thks",
  "id" : 163962541528203264,
  "created_at" : "2012-01-30 12:31:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Lian",
      "screen_name" : "davidlian",
      "indices" : [ 0, 10 ],
      "id_str" : "2000251",
      "id" : 2000251
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 57 ],
      "url" : "http:\/\/t.co\/HtwXmTXe",
      "expanded_url" : "http:\/\/pinterest.com\/yapss\/",
      "display_url" : "pinterest.com\/yapss\/"
    } ]
  },
  "in_reply_to_status_id_str" : "163900952296951808",
  "geo" : { },
  "id_str" : "163957411659530241",
  "in_reply_to_user_id" : 2000251,
  "text" : "@davidlian Here is my pinterest page http:\/\/t.co\/HtwXmTXe",
  "id" : 163957411659530241,
  "in_reply_to_status_id" : 163900952296951808,
  "created_at" : "2012-01-30 12:11:07 +0000",
  "in_reply_to_screen_name" : "davidlian",
  "in_reply_to_user_id_str" : "2000251",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Iarnr\u00F3d \u00C9ireann",
      "screen_name" : "IrishRail",
      "indices" : [ 3, 13 ],
      "id_str" : "15115986",
      "id" : 15115986
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163953296539779072",
  "text" : "RT @IrishRail: Great Railway Journeys w Ml Portillo on BBC2\/BBCHD starts its Ireland week tonight at 6.30pm with Bray-Dublin up first! h ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/ubersocial.com\" rel=\"nofollow\"\u003EUberSocial for BlackBerry\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 120, 140 ],
        "url" : "http:\/\/t.co\/mwUZmgyt",
        "expanded_url" : "http:\/\/bit.ly\/wFgl4g",
        "display_url" : "bit.ly\/wFgl4g"
      } ]
    },
    "geo" : { },
    "id_str" : "163946016763154433",
    "text" : "Great Railway Journeys w Ml Portillo on BBC2\/BBCHD starts its Ireland week tonight at 6.30pm with Bray-Dublin up first! http:\/\/t.co\/mwUZmgyt",
    "id" : 163946016763154433,
    "created_at" : "2012-01-30 11:25:51 +0000",
    "user" : {
      "name" : "Iarnr\u00F3d \u00C9ireann",
      "screen_name" : "IrishRail",
      "protected" : false,
      "id_str" : "15115986",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002515342281846784\/mcPgLtBl_normal.jpg",
      "id" : 15115986,
      "verified" : true
    }
  },
  "id" : 163953296539779072,
  "created_at" : "2012-01-30 11:54:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joshua Kaufman",
      "screen_name" : "jmk",
      "indices" : [ 3, 7 ],
      "id_str" : "12376",
      "id" : 12376
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 96, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163949006412726272",
  "text" : "RT @jmk: Countryside day trip recommendations from Dublin, Ireland (Specifically from DUB). Go! #ixd12",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ixd12",
        "indices" : [ 87, 93 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "161587143452524544",
    "text" : "Countryside day trip recommendations from Dublin, Ireland (Specifically from DUB). Go! #ixd12",
    "id" : 161587143452524544,
    "created_at" : "2012-01-23 23:12:31 +0000",
    "user" : {
      "name" : "Joshua Kaufman",
      "screen_name" : "jmk",
      "protected" : false,
      "id_str" : "12376",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/887669730508722177\/TJhg96uW_normal.jpg",
      "id" : 12376,
      "verified" : false
    }
  },
  "id" : 163949006412726272,
  "created_at" : "2012-01-30 11:37:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joshua Kaufman",
      "screen_name" : "jmk",
      "indices" : [ 0, 4 ],
      "id_str" : "12376",
      "id" : 12376
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 96, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 43 ],
      "url" : "http:\/\/t.co\/9yFpxAkI",
      "expanded_url" : "http:\/\/www.visitwicklow.ie\/",
      "display_url" : "visitwicklow.ie"
    }, {
      "indices" : [ 75, 95 ],
      "url" : "http:\/\/t.co\/E5QfM22y",
      "expanded_url" : "http:\/\/www.powerscourt.ie\/gardens",
      "display_url" : "powerscourt.ie\/gardens"
    } ]
  },
  "in_reply_to_status_id_str" : "161587143452524544",
  "geo" : { },
  "id_str" : "163948946576781312",
  "in_reply_to_user_id" : 12376,
  "text" : "@jmk Wicklow mountains http:\/\/t.co\/9yFpxAkI, Powerscourt House and Gardens http:\/\/t.co\/E5QfM22y #ixd12",
  "id" : 163948946576781312,
  "in_reply_to_status_id" : 161587143452524544,
  "created_at" : "2012-01-30 11:37:29 +0000",
  "in_reply_to_screen_name" : "jmk",
  "in_reply_to_user_id_str" : "12376",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 0, 16 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "162672614358253570",
  "geo" : { },
  "id_str" : "163948213232087041",
  "in_reply_to_user_id" : 14340736,
  "text" : "@interactivemark look forward to seeing you!",
  "id" : 163948213232087041,
  "in_reply_to_status_id" : 162672614358253570,
  "created_at" : "2012-01-30 11:34:34 +0000",
  "in_reply_to_screen_name" : "interactivemark",
  "in_reply_to_user_id_str" : "14340736",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishthings",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163947642513129474",
  "text" : "#irishthings Barry's Tea, Tayto chips",
  "id" : 163947642513129474,
  "created_at" : "2012-01-30 11:32:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mishal Husain",
      "screen_name" : "MishalHusainBBC",
      "indices" : [ 3, 19 ],
      "id_str" : "74265849",
      "id" : 74265849
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163946578653085697",
  "text" : "RT @MishalHusainBBC: Is there a doctor in the Twittersphere? Looking for someone to take my blood pressure live on air today after this: ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 136 ],
        "url" : "http:\/\/t.co\/AtMZzNmp",
        "expanded_url" : "http:\/\/bbc.in\/Aedllg",
        "display_url" : "bbc.in\/Aedllg"
      } ]
    },
    "geo" : { },
    "id_str" : "163944075542798336",
    "text" : "Is there a doctor in the Twittersphere? Looking for someone to take my blood pressure live on air today after this: http:\/\/t.co\/AtMZzNmp",
    "id" : 163944075542798336,
    "created_at" : "2012-01-30 11:18:08 +0000",
    "user" : {
      "name" : "Mishal Husain",
      "screen_name" : "MishalHusainBBC",
      "protected" : false,
      "id_str" : "74265849",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1641915046\/mishal_normal.jpg",
      "id" : 74265849,
      "verified" : true
    }
  },
  "id" : 163946578653085697,
  "created_at" : "2012-01-30 11:28:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Perkasa",
      "indices" : [ 33, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163897503601790976",
  "text" : "RT @mrbrown: Malay Supremacy NGO #Perkasa gives out white angpows for CNY in Malaysia. White angpows are given during funerals, you morons.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Perkasa",
        "indices" : [ 20, 28 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "163853309965369345",
    "text" : "Malay Supremacy NGO #Perkasa gives out white angpows for CNY in Malaysia. White angpows are given during funerals, you morons.",
    "id" : 163853309965369345,
    "created_at" : "2012-01-30 05:17:28 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 163897503601790976,
  "created_at" : "2012-01-30 08:13:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Irishthings",
      "indices" : [ 13, 25 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163778893290012672",
  "text" : "RT @topgold: #Irishthings \"How long are you staying?\" asks the Garda every time at immigration.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.tweetcaster.com\" rel=\"nofollow\"\u003ETweetCaster for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Irishthings",
        "indices" : [ 0, 12 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "163772710491668480",
    "text" : "#Irishthings \"How long are you staying?\" asks the Garda every time at immigration.",
    "id" : 163772710491668480,
    "created_at" : "2012-01-29 23:57:11 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 163778893290012672,
  "created_at" : "2012-01-30 00:21:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishthings",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163778665447038977",
  "text" : "#irishthings Saying Thanks to the bus driver when getting off at the bus stop.",
  "id" : 163778665447038977,
  "created_at" : "2012-01-30 00:20:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishthings",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163778196553203712",
  "text" : "#irishthings Where emigration is a shakespearean tragedy.",
  "id" : 163778196553203712,
  "created_at" : "2012-01-30 00:18:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishthings",
      "indices" : [ 0, 12 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163777303560716288",
  "text" : "#irishthings As long as you have 0.01% of Irish blood & you are famous, you are an Irish in the eyes of the media here.",
  "id" : 163777303560716288,
  "created_at" : "2012-01-30 00:15:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ixd12",
      "indices" : [ 30, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163775713286168576",
  "text" : "Where to eat in Dublin during #ixd12 ? My suggested list foursquare.com\/mryap\/list\/dublin",
  "id" : 163775713286168576,
  "created_at" : "2012-01-30 00:09:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Design Sojourn",
      "screen_name" : "designsojourn",
      "indices" : [ 0, 14 ],
      "id_str" : "3029921",
      "id" : 3029921
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "163662010540756992",
  "geo" : { },
  "id_str" : "163664640742653953",
  "in_reply_to_user_id" : 3029921,
  "text" : "@designsojourn what the font you used for the bold header?",
  "id" : 163664640742653953,
  "in_reply_to_status_id" : 163662010540756992,
  "created_at" : "2012-01-29 16:47:45 +0000",
  "in_reply_to_screen_name" : "designsojourn",
  "in_reply_to_user_id_str" : "3029921",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/fancy\/id407324335?mt=8&uo=4\" rel=\"nofollow\"\u003Efancy on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http:\/\/t.co\/zle6ce5t",
      "expanded_url" : "http:\/\/www.thefancy.com\/things\/285373867\/Free-Charging-Station-%40-Changi-Airport-Singapore",
      "display_url" : "thefancy.com\/things\/2853738\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "163624482852962305",
  "text" : "Wishes this is available in all airport! Free Charging Station. http:\/\/t.co\/zle6ce5t",
  "id" : 163624482852962305,
  "created_at" : "2012-01-29 14:08:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http:\/\/t.co\/lOcQp6OE",
      "expanded_url" : "http:\/\/www.businessinsider.com\/davos-sponsors-2012-1",
      "display_url" : "businessinsider.com\/davos-sponsors\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "163618674614353920",
  "text" : "RT @patphelan: and heres me thinking that Davos was about saving the planet :-) http:\/\/t.co\/lOcQp6OE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 65, 85 ],
        "url" : "http:\/\/t.co\/lOcQp6OE",
        "expanded_url" : "http:\/\/www.businessinsider.com\/davos-sponsors-2012-1",
        "display_url" : "businessinsider.com\/davos-sponsors\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "163605639371833345",
    "text" : "and heres me thinking that Davos was about saving the planet :-) http:\/\/t.co\/lOcQp6OE",
    "id" : 163605639371833345,
    "created_at" : "2012-01-29 12:53:18 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 163618674614353920,
  "created_at" : "2012-01-29 13:45:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Manchester News MEN",
      "screen_name" : "MENnewsdesk",
      "indices" : [ 3, 15 ],
      "id_str" : "20678866",
      "id" : 20678866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http:\/\/t.co\/0WglJiHv",
      "expanded_url" : "http:\/\/dlvr.it\/17vNqt",
      "display_url" : "dlvr.it\/17vNqt"
    } ]
  },
  "geo" : { },
  "id_str" : "163614735110119424",
  "text" : "RT @MENnewsdesk: Thousands set to celebrate Chinese New Year in Manchester http:\/\/t.co\/0WglJiHv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 78 ],
        "url" : "http:\/\/t.co\/0WglJiHv",
        "expanded_url" : "http:\/\/dlvr.it\/17vNqt",
        "display_url" : "dlvr.it\/17vNqt"
      } ]
    },
    "geo" : { },
    "id_str" : "163611785679732736",
    "text" : "Thousands set to celebrate Chinese New Year in Manchester http:\/\/t.co\/0WglJiHv",
    "id" : 163611785679732736,
    "created_at" : "2012-01-29 13:17:44 +0000",
    "user" : {
      "name" : "Manchester News MEN",
      "screen_name" : "MENnewsdesk",
      "protected" : false,
      "id_str" : "20678866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034761426441830400\/wlUHaUsZ_normal.jpg",
      "id" : 20678866,
      "verified" : true
    }
  },
  "id" : 163614735110119424,
  "created_at" : "2012-01-29 13:29:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163577333264027649",
  "text" : "Hope to meet some of you at the Interaction Conference 2012 Dublin. I am there for the FREE workshop on 1 Feb 2012.",
  "id" : 163577333264027649,
  "created_at" : "2012-01-29 11:00:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Indi Young",
      "screen_name" : "indiyoung",
      "indices" : [ 3, 13 ],
      "id_str" : "816841",
      "id" : 816841
    }, {
      "name" : "Jill Christ",
      "screen_name" : "jillchrist",
      "indices" : [ 18, 29 ],
      "id_str" : "14436418",
      "id" : 14436418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163572146612862976",
  "text" : "RT @indiyoung: RT @jillchrist: \"Building products based on preference research is like building a kitchen from a stack of magazine clipp ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twhirl.org\" rel=\"nofollow\"\u003ESeesmic twhirl\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jill Christ",
        "screen_name" : "jillchrist",
        "indices" : [ 3, 14 ],
        "id_str" : "14436418",
        "id" : 14436418
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "144861444603781120",
    "text" : "RT @jillchrist: \"Building products based on preference research is like building a kitchen from a stack of magazine clippings.\" Mental Model",
    "id" : 144861444603781120,
    "created_at" : "2011-12-08 19:30:34 +0000",
    "user" : {
      "name" : "Indi Young",
      "screen_name" : "indiyoung",
      "protected" : false,
      "id_str" : "816841",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/699722060994449408\/eSfWKrGz_normal.jpg",
      "id" : 816841,
      "verified" : false
    }
  },
  "id" : 163572146612862976,
  "created_at" : "2012-01-29 10:40:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 39, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 54, 74 ],
      "url" : "http:\/\/t.co\/WpfKzpaq",
      "expanded_url" : "http:\/\/www.fastcompany.com\/blog\/cliff-kuang\/design-innovation\/infographic-day-30-free-websites-online-promotion?partner=rss",
      "display_url" : "fastcompany.com\/blog\/cliff-kua\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "163566348427997184",
  "text" : "30 Free Web Sites for Online Promotion #SMEcommunity  http:\/\/t.co\/WpfKzpaq",
  "id" : 163566348427997184,
  "created_at" : "2012-01-29 10:17:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 97 ],
      "url" : "http:\/\/t.co\/gtWZSgtP",
      "expanded_url" : "http:\/\/wordpress.org\/extend\/plugins\/robots-meta\/",
      "display_url" : "wordpress.org\/extend\/plugins\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "163562132477181952",
  "text" : "Stop specific posts\/pages from being indexed by search engines in Wordpress  http:\/\/t.co\/gtWZSgtP",
  "id" : 163562132477181952,
  "created_at" : "2012-01-29 10:00:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Krynsky",
      "screen_name" : "krynsky",
      "indices" : [ 3, 11 ],
      "id_str" : "772681",
      "id" : 772681
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 107 ],
      "url" : "http:\/\/t.co\/CWMz10Xv",
      "expanded_url" : "http:\/\/j.mp\/yqkrmk",
      "display_url" : "j.mp\/yqkrmk"
    } ]
  },
  "geo" : { },
  "id_str" : "163555378771136513",
  "text" : "RT @krynsky: WTF?!? I thought ColdFusion died with MySpace - Adobe ColdFusion 9 family http:\/\/t.co\/CWMz10Xv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/bitly.com\" rel=\"nofollow\"\u003Ebitly bitlink\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 94 ],
        "url" : "http:\/\/t.co\/CWMz10Xv",
        "expanded_url" : "http:\/\/j.mp\/yqkrmk",
        "display_url" : "j.mp\/yqkrmk"
      } ]
    },
    "geo" : { },
    "id_str" : "163524699194802177",
    "text" : "WTF?!? I thought ColdFusion died with MySpace - Adobe ColdFusion 9 family http:\/\/t.co\/CWMz10Xv",
    "id" : 163524699194802177,
    "created_at" : "2012-01-29 07:31:41 +0000",
    "user" : {
      "name" : "Mark Krynsky",
      "screen_name" : "krynsky",
      "protected" : false,
      "id_str" : "772681",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964582164237991936\/MshbR7KM_normal.jpg",
      "id" : 772681,
      "verified" : false
    }
  },
  "id" : 163555378771136513,
  "created_at" : "2012-01-29 09:33:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "indices" : [ 3, 15 ],
      "id_str" : "15736190",
      "id" : 15736190
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 84 ],
      "url" : "http:\/\/t.co\/VVtM388J",
      "expanded_url" : "http:\/\/bit.ly\/DylTW",
      "display_url" : "bit.ly\/DylTW"
    } ]
  },
  "geo" : { },
  "id_str" : "163423773863837697",
  "text" : "RT @smashingmag: ITU Gaze Tracker: an open source eye tracker - http:\/\/t.co\/VVtM388J and an open gaze and movement analyzer - http:\/\/t.c ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 67 ],
        "url" : "http:\/\/t.co\/VVtM388J",
        "expanded_url" : "http:\/\/bit.ly\/DylTW",
        "display_url" : "bit.ly\/DylTW"
      }, {
        "indices" : [ 109, 129 ],
        "url" : "http:\/\/t.co\/dhls4dio",
        "expanded_url" : "http:\/\/www.ogama.net\/",
        "display_url" : "ogama.net"
      } ]
    },
    "geo" : { },
    "id_str" : "161099559584546816",
    "text" : "ITU Gaze Tracker: an open source eye tracker - http:\/\/t.co\/VVtM388J and an open gaze and movement analyzer - http:\/\/t.co\/dhls4dio",
    "id" : 161099559584546816,
    "created_at" : "2012-01-22 14:55:02 +0000",
    "user" : {
      "name" : "Smashing Magazine",
      "screen_name" : "smashingmag",
      "protected" : false,
      "id_str" : "15736190",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014194982499078145\/ScpR-ujV_normal.jpg",
      "id" : 15736190,
      "verified" : true
    }
  },
  "id" : 163423773863837697,
  "created_at" : "2012-01-29 00:50:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frank Prendergast",
      "screen_name" : "frankiep",
      "indices" : [ 0, 9 ],
      "id_str" : "12395472",
      "id" : 12395472
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "163338907856023553",
  "geo" : { },
  "id_str" : "163376041488957441",
  "in_reply_to_user_id" : 12395472,
  "text" : "@frankiep Twitter on iPad also can't allow one to copy text...I used the Mail Tweet function instead just to copy text.",
  "id" : 163376041488957441,
  "in_reply_to_status_id" : 163338907856023553,
  "created_at" : "2012-01-28 21:40:58 +0000",
  "in_reply_to_screen_name" : "frankiep",
  "in_reply_to_user_id_str" : "12395472",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/163288730617118720\/photo\/1",
      "indices" : [ 41, 61 ],
      "url" : "http:\/\/t.co\/cRKp6tXK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AkQePECCAAAGqS-.jpg",
      "id_str" : "163288730621313024",
      "id" : 163288730621313024,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AkQePECCAAAGqS-.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 482,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cRKp6tXK"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163288730617118720",
  "text" : "Do you aspire to join the film industry? http:\/\/t.co\/cRKp6tXK",
  "id" : 163288730617118720,
  "created_at" : "2012-01-28 15:54:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "A River Runs Stewart",
      "screen_name" : "irishstu",
      "indices" : [ 0, 9 ],
      "id_str" : "815463",
      "id" : 815463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 30 ],
      "url" : "http:\/\/t.co\/nOfqdAoB",
      "expanded_url" : "http:\/\/www.framewise.ie\/",
      "display_url" : "framewise.ie"
    } ]
  },
  "in_reply_to_status_id_str" : "163234206875516928",
  "geo" : { },
  "id_str" : "163287255811751936",
  "in_reply_to_user_id" : 815463,
  "text" : "@irishstu http:\/\/t.co\/nOfqdAoB at Crumlin, D12",
  "id" : 163287255811751936,
  "in_reply_to_status_id" : 163234206875516928,
  "created_at" : "2012-01-28 15:48:10 +0000",
  "in_reply_to_screen_name" : "irishstu",
  "in_reply_to_user_id_str" : "815463",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Xavier Lur",
      "screen_name" : "xavierlur",
      "indices" : [ 3, 13 ],
      "id_str" : "12589972",
      "id" : 12589972
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "youknowyouarefromSGwhen",
      "indices" : [ 15, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163285629415854080",
  "text" : "RT @xavierlur: #youknowyouarefromSGwhen In HDB void deck...Malay wedding on your right, Chinese funeral on your left, Indian provision s ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "youknowyouarefromSGwhen",
        "indices" : [ 0, 24 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "163246212445769729",
    "text" : "#youknowyouarefromSGwhen In HDB void deck...Malay wedding on your right, Chinese funeral on your left, Indian provision shop in the middle.",
    "id" : 163246212445769729,
    "created_at" : "2012-01-28 13:05:04 +0000",
    "user" : {
      "name" : "Xavier Lur",
      "screen_name" : "xavierlur",
      "protected" : false,
      "id_str" : "12589972",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028953650704482306\/tTH7hzPm_normal.jpg",
      "id" : 12589972,
      "verified" : false
    }
  },
  "id" : 163285629415854080,
  "created_at" : "2012-01-28 15:41:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tvtag.com\" rel=\"nofollow\"\u003Etvtag.com\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GetGlue (now Telfie)",
      "screen_name" : "GetGlue",
      "indices" : [ 107, 115 ],
      "id_str" : "1571579137",
      "id" : 1571579137
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NowYouSeeIt",
      "indices" : [ 116, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http:\/\/t.co\/7NqTvxWE",
      "expanded_url" : "http:\/\/bit.ly\/wrwlcX",
      "display_url" : "bit.ly\/wrwlcX"
    } ]
  },
  "geo" : { },
  "id_str" : "163278555097219073",
  "text" : "I'm reading Now You See It: Simple Visualization Techniques for Quantitative Analysis http:\/\/t.co\/7NqTvxWE @GetGlue #NowYouSeeIt",
  "id" : 163278555097219073,
  "created_at" : "2012-01-28 15:13:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Audrey Tan",
      "screen_name" : "audreytan",
      "indices" : [ 0, 10 ],
      "id_str" : "15770959",
      "id" : 15770959
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "163256328578539521",
  "geo" : { },
  "id_str" : "163271280219533312",
  "in_reply_to_user_id" : 15770959,
  "text" : "@audreytan actually I was trying to be cheeky. ;)",
  "id" : 163271280219533312,
  "in_reply_to_status_id" : 163256328578539521,
  "created_at" : "2012-01-28 14:44:41 +0000",
  "in_reply_to_screen_name" : "audreytan",
  "in_reply_to_user_id_str" : "15770959",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 118 ],
      "url" : "http:\/\/t.co\/U5cyRaqe",
      "expanded_url" : "http:\/\/www.kickstarter.com\/projects\/ninja\/ninja-blocks-connect-your-world-with-the-web",
      "display_url" : "kickstarter.com\/projects\/ninja\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "163185732113010688",
  "text" : "Connect your (physical) world with the web by creating Tasks that made up of Triggers and Actions http:\/\/t.co\/U5cyRaqe",
  "id" : 163185732113010688,
  "created_at" : "2012-01-28 09:04:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Audrey Tan",
      "screen_name" : "audreytan",
      "indices" : [ 1, 11 ],
      "id_str" : "15770959",
      "id" : 15770959
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "163041019460468737",
  "text" : "\u201C@audreytan: 3am madness. I can't sleep! What keeps u up at night?\u201D World peace",
  "id" : 163041019460468737,
  "created_at" : "2012-01-27 23:29:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samsung C&T",
      "screen_name" : "Samsungcnt",
      "indices" : [ 61, 72 ],
      "id_str" : "90988340",
      "id" : 90988340
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 45 ],
      "url" : "http:\/\/t.co\/9iPwfZSu",
      "expanded_url" : "http:\/\/twc2.org.sg\/2012\/01\/25\/how-low-can-a-salary-go\/#.TyMw7RqR5MQ.twitter",
      "display_url" : "twc2.org.sg\/2012\/01\/25\/how\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "163039070807801856",
  "text" : "How low can a salary go? http:\/\/t.co\/9iPwfZSu via @TWC2  FYI @samsungcnt",
  "id" : 163039070807801856,
  "created_at" : "2012-01-27 23:21:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Reil",
      "screen_name" : "jrdesign",
      "indices" : [ 3, 12 ],
      "id_str" : "11266652",
      "id" : 11266652
    }, {
      "name" : "Onextrapixel",
      "screen_name" : "onextrapixel",
      "indices" : [ 80, 93 ],
      "id_str" : "29734071",
      "id" : 29734071
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 75 ],
      "url" : "http:\/\/t.co\/8OdNzWvw",
      "expanded_url" : "http:\/\/www.onextrapixel.com\/2012\/01\/02\/design-best-practices-for-the-mobile-web\/",
      "display_url" : "onextrapixel.com\/2012\/01\/02\/des\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162940486800252928",
  "text" : "RT @jrdesign: Design Best Practices for the Mobile Web http:\/\/t.co\/8OdNzWvw via @onextrapixel",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Onextrapixel",
        "screen_name" : "onextrapixel",
        "indices" : [ 66, 79 ],
        "id_str" : "29734071",
        "id" : 29734071
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 41, 61 ],
        "url" : "http:\/\/t.co\/8OdNzWvw",
        "expanded_url" : "http:\/\/www.onextrapixel.com\/2012\/01\/02\/design-best-practices-for-the-mobile-web\/",
        "display_url" : "onextrapixel.com\/2012\/01\/02\/des\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "155746839369482240",
    "text" : "Design Best Practices for the Mobile Web http:\/\/t.co\/8OdNzWvw via @onextrapixel",
    "id" : 155746839369482240,
    "created_at" : "2012-01-07 20:25:14 +0000",
    "user" : {
      "name" : "Jon Reil",
      "screen_name" : "jrdesign",
      "protected" : false,
      "id_str" : "11266652",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/460817599014531072\/xO2vqc3K_normal.png",
      "id" : 11266652,
      "verified" : false
    }
  },
  "id" : 162940486800252928,
  "created_at" : "2012-01-27 16:50:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "berniceklaassen",
      "screen_name" : "berniceklaassen",
      "indices" : [ 3, 19 ],
      "id_str" : "17632827",
      "id" : 17632827
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http:\/\/t.co\/v8b8p1iA",
      "expanded_url" : "http:\/\/www.wfs.org\/content\/eight-things-know-about-asia-2012",
      "display_url" : "wfs.org\/content\/eight-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162936572470231040",
  "text" : "RT @berniceklaassen: Eight Things to Know About Asia in 2012 http:\/\/t.co\/v8b8p1iA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 40, 60 ],
        "url" : "http:\/\/t.co\/v8b8p1iA",
        "expanded_url" : "http:\/\/www.wfs.org\/content\/eight-things-know-about-asia-2012",
        "display_url" : "wfs.org\/content\/eight-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "162921816220893185",
    "text" : "Eight Things to Know About Asia in 2012 http:\/\/t.co\/v8b8p1iA",
    "id" : 162921816220893185,
    "created_at" : "2012-01-27 15:36:02 +0000",
    "user" : {
      "name" : "berniceklaassen",
      "screen_name" : "berniceklaassen",
      "protected" : false,
      "id_str" : "17632827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/590952560\/Bernicek_normal.jpg",
      "id" : 17632827,
      "verified" : false
    }
  },
  "id" : 162936572470231040,
  "created_at" : "2012-01-27 16:34:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vahe Habeshian",
      "screen_name" : "habesh",
      "indices" : [ 3, 10 ],
      "id_str" : "11141322",
      "id" : 11141322
    }, {
      "name" : "Ann Handley",
      "screen_name" : "MarketingProfs",
      "indices" : [ 111, 126 ],
      "id_str" : "8596022",
      "id" : 8596022
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http:\/\/t.co\/YGiWuP72",
      "expanded_url" : "http:\/\/www.marketingprofs.com\/articles\/2012\/6890\/three-keys-to-accelerating-marketing-measurement-and-a-fact-based-culture",
      "display_url" : "marketingprofs.com\/articles\/2012\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162934971445686274",
  "text" : "RT @habesh: Three Keys to Accelerating Marketing Measurement and a Fact-Based Culture http:\/\/t.co\/YGiWuP72 via @marketingprofs",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ann Handley",
        "screen_name" : "MarketingProfs",
        "indices" : [ 99, 114 ],
        "id_str" : "8596022",
        "id" : 8596022
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 94 ],
        "url" : "http:\/\/t.co\/YGiWuP72",
        "expanded_url" : "http:\/\/www.marketingprofs.com\/articles\/2012\/6890\/three-keys-to-accelerating-marketing-measurement-and-a-fact-based-culture",
        "display_url" : "marketingprofs.com\/articles\/2012\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "162924077982556161",
    "text" : "Three Keys to Accelerating Marketing Measurement and a Fact-Based Culture http:\/\/t.co\/YGiWuP72 via @marketingprofs",
    "id" : 162924077982556161,
    "created_at" : "2012-01-27 15:45:01 +0000",
    "user" : {
      "name" : "Vahe Habeshian",
      "screen_name" : "habesh",
      "protected" : false,
      "id_str" : "11141322",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206947546\/vahe-divvy-shirt-twitter_normal.JPG",
      "id" : 11141322,
      "verified" : false
    }
  },
  "id" : 162934971445686274,
  "created_at" : "2012-01-27 16:28:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http:\/\/t.co\/EiqJ4UEZ",
      "expanded_url" : "http:\/\/www.economist.com\/blogs\/graphicdetail\/2012\/01\/daily-chart-11",
      "display_url" : "economist.com\/blogs\/graphicd\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162890212802969600",
  "text" : "The \"Wiggle Room\" Index: Which emerging economies have the most monetary and fiscal firepower? Egypt, India, Poland http:\/\/t.co\/EiqJ4UEZ",
  "id" : 162890212802969600,
  "created_at" : "2012-01-27 13:30:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/bphSkksU",
      "expanded_url" : "http:\/\/wordpress.org\/extend\/plugins\/the-events-calendar\/",
      "display_url" : "wordpress.org\/extend\/plugins\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162889529676660737",
  "text" : "Event management calender for your WordPress site. http:\/\/t.co\/bphSkksU",
  "id" : 162889529676660737,
  "created_at" : "2012-01-27 13:27:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http:\/\/t.co\/znKQbFjR",
      "expanded_url" : "http:\/\/www.delicious.com\/redirect?url=http%3A\/\/download.techsmith.com\/morae\/docs\/customer-research\/3.2\/Remote-testing.pdf",
      "display_url" : "delicious.com\/redirect?url=h\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162884165723885568",
  "text" : "Are you using Morae for remote participant testing? Tips document (PDF) for reference http:\/\/t.co\/znKQbFjR",
  "id" : 162884165723885568,
  "created_at" : "2012-01-27 13:06:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http:\/\/t.co\/X5au52Bc",
      "expanded_url" : "http:\/\/www.hci-class.org\/",
      "display_url" : "hci-class.org"
    } ]
  },
  "geo" : { },
  "id_str" : "162873219777437696",
  "text" : "Study Human Computer Interaction with Stanford University. Class starts January 30th. http:\/\/t.co\/X5au52Bc It FREE",
  "id" : 162873219777437696,
  "created_at" : "2012-01-27 12:22:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bill Gross",
      "screen_name" : "Bill_Gross",
      "indices" : [ 3, 14 ],
      "id_str" : "47539748",
      "id" : 47539748
    }, {
      "name" : "Matthew Harrington",
      "screen_name" : "mj_harrington",
      "indices" : [ 19, 33 ],
      "id_str" : "24756640",
      "id" : 24756640
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162574045617991680",
  "text" : "RT @Bill_Gross: RT @mj_harrington: \"Over next several years world will need amount of food equal to all produced in last 10,000 years!\"  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Matthew Harrington",
        "screen_name" : "mj_harrington",
        "indices" : [ 3, 17 ],
        "id_str" : "24756640",
        "id" : 24756640
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WEF",
        "indices" : [ 127, 131 ]
      }, {
        "text" : "Davos",
        "indices" : [ 132, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "162541634603593730",
    "text" : "RT @mj_harrington: \"Over next several years world will need amount of food equal to all produced in last 10,000 years!\" Polman #WEF #Davos",
    "id" : 162541634603593730,
    "created_at" : "2012-01-26 14:25:20 +0000",
    "user" : {
      "name" : "Bill Gross",
      "screen_name" : "Bill_Gross",
      "protected" : false,
      "id_str" : "47539748",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/264971169\/Bill_Gross_Thumbnail_normal.jpg",
      "id" : 47539748,
      "verified" : true
    }
  },
  "id" : 162574045617991680,
  "created_at" : "2012-01-26 16:34:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stephen Donnelly",
      "screen_name" : "DonnellyStephen",
      "indices" : [ 3, 19 ],
      "id_str" : "237826875",
      "id" : 237826875
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 119 ],
      "url" : "http:\/\/t.co\/akKbb8N0",
      "expanded_url" : "http:\/\/bit.ly\/24YmPI",
      "display_url" : "bit.ly\/24YmPI"
    } ]
  },
  "geo" : { },
  "id_str" : "162540656991019009",
  "text" : "RT @DonnellyStephen: SOPA-Ireland to be debated in the D\u00E1il today at 3.42pm for 10 mins. Live link http:\/\/t.co\/akKbb8N0.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 98 ],
        "url" : "http:\/\/t.co\/akKbb8N0",
        "expanded_url" : "http:\/\/bit.ly\/24YmPI",
        "display_url" : "bit.ly\/24YmPI"
      } ]
    },
    "geo" : { },
    "id_str" : "162538088139194368",
    "text" : "SOPA-Ireland to be debated in the D\u00E1il today at 3.42pm for 10 mins. Live link http:\/\/t.co\/akKbb8N0.",
    "id" : 162538088139194368,
    "created_at" : "2012-01-26 14:11:14 +0000",
    "user" : {
      "name" : "Stephen Donnelly",
      "screen_name" : "DonnellyStephen",
      "protected" : false,
      "id_str" : "237826875",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/696299769078140929\/ybuSeetG_normal.jpg",
      "id" : 237826875,
      "verified" : false
    }
  },
  "id" : 162540656991019009,
  "created_at" : "2012-01-26 14:21:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162523342480478208",
  "text" : "People on my timeline is talking about Pinterest since the start of the new year",
  "id" : 162523342480478208,
  "created_at" : "2012-01-26 13:12:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tony Hirst",
      "screen_name" : "psychemedia",
      "indices" : [ 3, 15 ],
      "id_str" : "7129072",
      "id" : 7129072
    }, {
      "name" : "Martin Hawksey",
      "screen_name" : "mhawksey",
      "indices" : [ 123, 132 ],
      "id_str" : "13046992",
      "id" : 13046992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http:\/\/t.co\/OEdVrC6t",
      "expanded_url" : "http:\/\/bit.ly\/x5rguw",
      "display_url" : "bit.ly\/x5rguw"
    } ]
  },
  "geo" : { },
  "id_str" : "162522853906980865",
  "text" : "RT @psychemedia: Easy way of archiving hashtagged tweets automagically using google spreadsheets http:\/\/t.co\/OEdVrC6t \/via @mhawksey",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Martin Hawksey",
        "screen_name" : "mhawksey",
        "indices" : [ 106, 115 ],
        "id_str" : "13046992",
        "id" : 13046992
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 100 ],
        "url" : "http:\/\/t.co\/OEdVrC6t",
        "expanded_url" : "http:\/\/bit.ly\/x5rguw",
        "display_url" : "bit.ly\/x5rguw"
      } ]
    },
    "geo" : { },
    "id_str" : "162494477758513152",
    "text" : "Easy way of archiving hashtagged tweets automagically using google spreadsheets http:\/\/t.co\/OEdVrC6t \/via @mhawksey",
    "id" : 162494477758513152,
    "created_at" : "2012-01-26 11:17:57 +0000",
    "user" : {
      "name" : "Tony Hirst",
      "screen_name" : "psychemedia",
      "protected" : false,
      "id_str" : "7129072",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1195013164\/Picture_23_normal.png",
      "id" : 7129072,
      "verified" : false
    }
  },
  "id" : 162522853906980865,
  "created_at" : "2012-01-26 13:10:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162511908660969473",
  "text" : "Some Singaporeans residing in a north UK city laments that there is no CNY mood there. It like complaining no Halloween mood in Singapore.",
  "id" : 162511908660969473,
  "created_at" : "2012-01-26 12:27:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Expat Explorer",
      "screen_name" : "expatexplorer",
      "indices" : [ 31, 45 ],
      "id_str" : "27960823",
      "id" : 27960823
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "qotd",
      "indices" : [ 53, 58 ]
    }, {
      "text" : "expat",
      "indices" : [ 78, 84 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162507428502581248",
  "text" : "It a lifestyle choice for us. \u201C@expatexplorer: Expat #qotd: Did you become an #expat for financial gain?\u201D",
  "id" : 162507428502581248,
  "created_at" : "2012-01-26 12:09:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Jeffs",
      "screen_name" : "DeeJay800",
      "indices" : [ 3, 13 ],
      "id_str" : "206077420",
      "id" : 206077420
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162506695531175936",
  "text" : "RT @DeeJay800: My dental clinic just emailed me a link to their fanpage asking me to \"like\" them.. what is the world coming too!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "162496470967267328",
    "text" : "My dental clinic just emailed me a link to their fanpage asking me to \"like\" them.. what is the world coming too!",
    "id" : 162496470967267328,
    "created_at" : "2012-01-26 11:25:52 +0000",
    "user" : {
      "name" : "David Jeffs",
      "screen_name" : "DeeJay800",
      "protected" : false,
      "id_str" : "206077420",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1355490418\/dj_normal.jpg",
      "id" : 206077420,
      "verified" : false
    }
  },
  "id" : 162506695531175936,
  "created_at" : "2012-01-26 12:06:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Godwin Grech",
      "screen_name" : "godwingrech",
      "indices" : [ 3, 15 ],
      "id_str" : "49634378",
      "id" : 49634378
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162506556791996416",
  "text" : "RT @godwingrech: ACT Policing needs more officers for Tony Abbott protection duties. Interviews are being held tomorrow. Come early...be ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AusPol",
        "indices" : [ 133, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "162497978748256256",
    "text" : "ACT Policing needs more officers for Tony Abbott protection duties. Interviews are being held tomorrow. Come early...beat the crowd. #AusPol",
    "id" : 162497978748256256,
    "created_at" : "2012-01-26 11:31:51 +0000",
    "user" : {
      "name" : "Godwin Grech",
      "screen_name" : "godwingrech",
      "protected" : false,
      "id_str" : "49634378",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1377355317\/haha_normal.gif",
      "id" : 49634378,
      "verified" : false
    }
  },
  "id" : 162506556791996416,
  "created_at" : "2012-01-26 12:05:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "esther addley",
      "screen_name" : "estheraddley",
      "indices" : [ 3, 16 ],
      "id_str" : "19391595",
      "id" : 19391595
    }, {
      "name" : "Patrick Kingsley",
      "screen_name" : "PatrickKingsley",
      "indices" : [ 110, 126 ],
      "id_str" : "90464160",
      "id" : 90464160
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 109 ],
      "url" : "http:\/\/t.co\/XJgk3DuD",
      "expanded_url" : "http:\/\/bit.ly\/xoB8QL",
      "display_url" : "bit.ly\/xoB8QL"
    } ]
  },
  "geo" : { },
  "id_str" : "162506319948021760",
  "text" : "RT @estheraddley: Great interview with Martin McGuinness on his deep love of ... cricket http:\/\/t.co\/XJgk3DuD @PatrickKingsley",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Patrick Kingsley",
        "screen_name" : "PatrickKingsley",
        "indices" : [ 92, 108 ],
        "id_str" : "90464160",
        "id" : 90464160
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 91 ],
        "url" : "http:\/\/t.co\/XJgk3DuD",
        "expanded_url" : "http:\/\/bit.ly\/xoB8QL",
        "display_url" : "bit.ly\/xoB8QL"
      } ]
    },
    "geo" : { },
    "id_str" : "162499367100628992",
    "text" : "Great interview with Martin McGuinness on his deep love of ... cricket http:\/\/t.co\/XJgk3DuD @PatrickKingsley",
    "id" : 162499367100628992,
    "created_at" : "2012-01-26 11:37:22 +0000",
    "user" : {
      "name" : "esther addley",
      "screen_name" : "estheraddley",
      "protected" : false,
      "id_str" : "19391595",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/443714389456486400\/gGn1nChr_normal.jpeg",
      "id" : 19391595,
      "verified" : true
    }
  },
  "id" : 162506319948021760,
  "created_at" : "2012-01-26 12:05:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jim Maiella",
      "screen_name" : "jimmaiella",
      "indices" : [ 3, 14 ],
      "id_str" : "16124187",
      "id" : 16124187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162244650248318977",
  "text" : "RT @jimmaiella: Most valuable company in the world. No Twitter feed. No Facebook \"fan\" page. Hmm.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "161953244782919681",
    "text" : "Most valuable company in the world. No Twitter feed. No Facebook \"fan\" page. Hmm.",
    "id" : 161953244782919681,
    "created_at" : "2012-01-24 23:27:17 +0000",
    "user" : {
      "name" : "Jim Maiella",
      "screen_name" : "jimmaiella",
      "protected" : false,
      "id_str" : "16124187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/800015069119557632\/5P4Z1mHj_normal.jpg",
      "id" : 16124187,
      "verified" : true
    }
  },
  "id" : 162244650248318977,
  "created_at" : "2012-01-25 18:45:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Analysis Exchange",
      "screen_name" : "analysisxchange",
      "indices" : [ 26, 42 ],
      "id_str" : "83692200",
      "id" : 83692200
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162235459592912897",
  "text" : "IRL charity where are u? \u201C@analysisxchange: 9 open Analysis Exchange Projects...If you have time, please sign up!! bit.ly\/ae-open\u201D",
  "id" : 162235459592912897,
  "created_at" : "2012-01-25 18:08:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162234421741748224",
  "text" : "Guest No. 1 commented to Mrs: \"Your husband look younger than you.\" Ok one extra vege dumpling for Guest No. 1",
  "id" : 162234421741748224,
  "created_at" : "2012-01-25 18:04:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "indices" : [ 3, 16 ],
      "id_str" : "14826420",
      "id" : 14826420
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 132 ],
      "url" : "http:\/\/t.co\/6wzJr2g8",
      "expanded_url" : "http:\/\/bit.ly\/yGQf25",
      "display_url" : "bit.ly\/yGQf25"
    } ]
  },
  "geo" : { },
  "id_str" : "162205757851435011",
  "text" : "RT @iamkelvinlee: UAE to clear $693m debt of thousands of citizens - what a nice country to be a citizen in! =) http:\/\/t.co\/6wzJr2g8",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 114 ],
        "url" : "http:\/\/t.co\/6wzJr2g8",
        "expanded_url" : "http:\/\/bit.ly\/yGQf25",
        "display_url" : "bit.ly\/yGQf25"
      } ]
    },
    "geo" : { },
    "id_str" : "162200495971241984",
    "text" : "UAE to clear $693m debt of thousands of citizens - what a nice country to be a citizen in! =) http:\/\/t.co\/6wzJr2g8",
    "id" : 162200495971241984,
    "created_at" : "2012-01-25 15:49:46 +0000",
    "user" : {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "protected" : false,
      "id_str" : "14826420",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996378142682923010\/A8QOlqWt_normal.jpg",
      "id" : 14826420,
      "verified" : false
    }
  },
  "id" : 162205757851435011,
  "created_at" : "2012-01-25 16:10:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 126, 139 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "162130970403815424",
  "text" : "Website up & running? What next? \"89% of companies surveyed say that web analytics drive action\". - eCONSULTANCY \nSURVEY 2010 #SMEcommunity",
  "id" : 162130970403815424,
  "created_at" : "2012-01-25 11:13:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 32 ],
      "url" : "http:\/\/t.co\/brcGmlOe",
      "expanded_url" : "http:\/\/mrbrwn.co\/xDaNwI",
      "display_url" : "mrbrwn.co\/xDaNwI"
    } ]
  },
  "geo" : { },
  "id_str" : "162124978072985601",
  "text" : "RT @mrbrown http:\/\/t.co\/brcGmlOe  Real cost of driving a car in Singapore= $1,855 a month. Even taking a cab for a month= $968",
  "id" : 162124978072985601,
  "created_at" : "2012-01-25 10:49:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TrendWatching",
      "screen_name" : "trendwatching",
      "indices" : [ 84, 98 ],
      "id_str" : "17066838",
      "id" : 17066838
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "2012trends",
      "indices" : [ 47, 58 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 79 ],
      "url" : "http:\/\/t.co\/FRoe22d0",
      "expanded_url" : "http:\/\/trendwatching.com\/trends\/12trends2012\/",
      "display_url" : "trendwatching.com\/trends\/12trend\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "162110303851642880",
  "text" : "Check out 12 Crucial Consumer Trends for 2012, #2012trends http:\/\/t.co\/FRoe22d0 via @trendwatching",
  "id" : 162110303851642880,
  "created_at" : "2012-01-25 09:51:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/lntoark5",
      "expanded_url" : "http:\/\/vimeo.com\/34783883",
      "display_url" : "vimeo.com\/34783883"
    } ]
  },
  "geo" : { },
  "id_str" : "161936276164050944",
  "text" : "Breath taking video of jump from a 55-story resort http:\/\/t.co\/lntoark5",
  "id" : 161936276164050944,
  "created_at" : "2012-01-24 22:19:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "indices" : [ 3, 9 ],
      "id_str" : "1344951",
      "id" : 1344951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 131 ],
      "url" : "http:\/\/t.co\/XvQVoCnH",
      "expanded_url" : "http:\/\/bit.ly\/A45L3s",
      "display_url" : "bit.ly\/A45L3s"
    } ]
  },
  "geo" : { },
  "id_str" : "161887709625974785",
  "text" : "RT @wired: In case you missed it: Famous photographers pose with their most iconic images. An amazing gallery. http:\/\/t.co\/XvQVoCnH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 100, 120 ],
        "url" : "http:\/\/t.co\/XvQVoCnH",
        "expanded_url" : "http:\/\/bit.ly\/A45L3s",
        "display_url" : "bit.ly\/A45L3s"
      } ]
    },
    "geo" : { },
    "id_str" : "161859310639521792",
    "text" : "In case you missed it: Famous photographers pose with their most iconic images. An amazing gallery. http:\/\/t.co\/XvQVoCnH",
    "id" : 161859310639521792,
    "created_at" : "2012-01-24 17:14:01 +0000",
    "user" : {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "protected" : false,
      "id_str" : "1344951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/615598832726970372\/jsK-gBSt_normal.png",
      "id" : 1344951,
      "verified" : true
    }
  },
  "id" : 161887709625974785,
  "created_at" : "2012-01-24 19:06:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "n8t8sh8",
      "screen_name" : "n8t8sh8",
      "indices" : [ 3, 11 ],
      "id_str" : "13993942",
      "id" : 13993942
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "email",
      "indices" : [ 37, 43 ]
    }, {
      "text" : "spoofing",
      "indices" : [ 44, 53 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161879980639399937",
  "text" : "RT @n8t8sh8: Anyone know how to stop #email #spoofing? Have run Norton and malware but friends are still being sent scummy messages. Any ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.htc.com\" rel=\"nofollow\"\u003E  HTC Peep\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "email",
        "indices" : [ 24, 30 ]
      }, {
        "text" : "spoofing",
        "indices" : [ 31, 40 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "161867465746423808",
    "text" : "Anyone know how to stop #email #spoofing? Have run Norton and malware but friends are still being sent scummy messages. Any help gr8ly appre",
    "id" : 161867465746423808,
    "created_at" : "2012-01-24 17:46:25 +0000",
    "user" : {
      "name" : "n8t8sh8",
      "screen_name" : "n8t8sh8",
      "protected" : false,
      "id_str" : "13993942",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/504744478\/n654445925_1392422_7777_normal.jpg",
      "id" : 13993942,
      "verified" : false
    }
  },
  "id" : 161879980639399937,
  "created_at" : "2012-01-24 18:36:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emily Chang",
      "screen_name" : "emilychangtv",
      "indices" : [ 3, 16 ],
      "id_str" : "74130577",
      "id" : 74130577
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161879517818925057",
  "text" : "RT @emilychangtv: Facebook says it adds $20 billion to European economy and supports 232,000 jobs, according to Deloitte study, via @Blo ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "161872800355848192",
    "text" : "Facebook says it adds $20 billion to European economy and supports 232,000 jobs, according to Deloitte study, via @BloombergNews",
    "id" : 161872800355848192,
    "created_at" : "2012-01-24 18:07:37 +0000",
    "user" : {
      "name" : "Emily Chang",
      "screen_name" : "emilychangtv",
      "protected" : false,
      "id_str" : "74130577",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/872219385313861632\/_fPENJf5_normal.jpg",
      "id" : 74130577,
      "verified" : true
    }
  },
  "id" : 161879517818925057,
  "created_at" : "2012-01-24 18:34:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Remember H. Rosling",
      "screen_name" : "HansRosling",
      "indices" : [ 3, 15 ],
      "id_str" : "20280065",
      "id" : 20280065
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161800031047909376",
  "text" : "RT @HansRosling: In BBC newsnight around 11pm this evening I will try to predict the year when China reaches the same GDP\/capita as UK h ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 118, 138 ],
        "url" : "http:\/\/t.co\/MrcLUCTl",
        "expanded_url" : "http:\/\/news.bbc.co.uk\/2\/hi\/programmes\/newsnight",
        "display_url" : "news.bbc.co.uk\/2\/hi\/programme\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "161797208914661376",
    "text" : "In BBC newsnight around 11pm this evening I will try to predict the year when China reaches the same GDP\/capita as UK http:\/\/t.co\/MrcLUCTl",
    "id" : 161797208914661376,
    "created_at" : "2012-01-24 13:07:15 +0000",
    "user" : {
      "name" : "Remember H. Rosling",
      "screen_name" : "HansRosling",
      "protected" : false,
      "id_str" : "20280065",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/116939798\/Hans_Rosling_liten_normal.jpg",
      "id" : 20280065,
      "verified" : true
    }
  },
  "id" : 161800031047909376,
  "created_at" : "2012-01-24 13:18:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Guardian",
      "screen_name" : "guardian",
      "indices" : [ 100, 109 ],
      "id_str" : "87818409",
      "id" : 87818409
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http:\/\/t.co\/MCTNhvPF",
      "expanded_url" : "http:\/\/gu.com\/p\/35x5v\/tw",
      "display_url" : "gu.com\/p\/35x5v\/tw"
    } ]
  },
  "geo" : { },
  "id_str" : "161775764247814145",
  "text" : "Chinese professor calls Hong Kong residents 'dogs of British imperialists' http:\/\/t.co\/MCTNhvPF via @guardian",
  "id" : 161775764247814145,
  "created_at" : "2012-01-24 11:42:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 81 ],
      "url" : "http:\/\/t.co\/goGCZhye",
      "expanded_url" : "http:\/\/www.academia.edu\/",
      "display_url" : "academia.edu"
    } ]
  },
  "geo" : { },
  "id_str" : "161767907662118912",
  "text" : "Is this kinda of Linkedin\/Facebook for academics researcher? http:\/\/t.co\/goGCZhye",
  "id" : 161767907662118912,
  "created_at" : "2012-01-24 11:10:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "sarcasm",
      "screen_name" : "autocorrects",
      "indices" : [ 3, 16 ],
      "id_str" : "345811633",
      "id" : 345811633
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161767277908328450",
  "text" : "RT @autocorrects: \"Money can't buy happiness .\" Yeah, but money can buy food. And that's kinda like the same thing.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.exacttarget.com\/social\" rel=\"nofollow\"\u003ESocialEngage\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "161229409196572673",
    "text" : "\"Money can't buy happiness .\" Yeah, but money can buy food. And that's kinda like the same thing.",
    "id" : 161229409196572673,
    "created_at" : "2012-01-22 23:31:01 +0000",
    "user" : {
      "name" : "sarcasm",
      "screen_name" : "autocorrects",
      "protected" : false,
      "id_str" : "345811633",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/892950334510833664\/gE7wharM_normal.jpg",
      "id" : 345811633,
      "verified" : false
    }
  },
  "id" : 161767277908328450,
  "created_at" : "2012-01-24 11:08:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Crystal",
      "screen_name" : "BornInBeijing",
      "indices" : [ 3, 17 ],
      "id_str" : "19674275",
      "id" : 19674275
    }, {
      "name" : "The Guardian",
      "screen_name" : "guardian",
      "indices" : [ 88, 97 ],
      "id_str" : "87818409",
      "id" : 87818409
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 83 ],
      "url" : "http:\/\/t.co\/xE4DapKg",
      "expanded_url" : "http:\/\/gu.com\/p\/35v45\/tw",
      "display_url" : "gu.com\/p\/35v45\/tw"
    } ]
  },
  "geo" : { },
  "id_str" : "161512440901812224",
  "text" : "RT @BornInBeijing: Chinese new year celebrations \u2013 in pictures http:\/\/t.co\/xE4DapKg via @guardian",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Guardian",
        "screen_name" : "guardian",
        "indices" : [ 69, 78 ],
        "id_str" : "87818409",
        "id" : 87818409
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 64 ],
        "url" : "http:\/\/t.co\/xE4DapKg",
        "expanded_url" : "http:\/\/gu.com\/p\/35v45\/tw",
        "display_url" : "gu.com\/p\/35v45\/tw"
      } ]
    },
    "geo" : { },
    "id_str" : "161511749328183296",
    "text" : "Chinese new year celebrations \u2013 in pictures http:\/\/t.co\/xE4DapKg via @guardian",
    "id" : 161511749328183296,
    "created_at" : "2012-01-23 18:12:56 +0000",
    "user" : {
      "name" : "Crystal",
      "screen_name" : "BornInBeijing",
      "protected" : false,
      "id_str" : "19674275",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000788922078\/86d5262a32ca92a832366379fb051ce7_normal.jpeg",
      "id" : 19674275,
      "verified" : false
    }
  },
  "id" : 161512440901812224,
  "created_at" : "2012-01-23 18:15:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheMarketingShop.ie",
      "screen_name" : "marketingdebbie",
      "indices" : [ 3, 19 ],
      "id_str" : "114487731",
      "id" : 114487731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161469035903598593",
  "text" : "RT @marketingdebbie: Question, my understanding is that you are supposed to have reg biz no on yr website? Anybody know where I find the ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SMEcommunity",
        "indices" : [ 127, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "161467453493346305",
    "text" : "Question, my understanding is that you are supposed to have reg biz no on yr website? Anybody know where I find the legal info #SMEcommunity",
    "id" : 161467453493346305,
    "created_at" : "2012-01-23 15:16:55 +0000",
    "user" : {
      "name" : "TheMarketingShop.ie",
      "screen_name" : "marketingdebbie",
      "protected" : false,
      "id_str" : "114487731",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/985192724877008897\/IwhrV223_normal.jpg",
      "id" : 114487731,
      "verified" : false
    }
  },
  "id" : 161469035903598593,
  "created_at" : "2012-01-23 15:23:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 80 ],
      "url" : "http:\/\/t.co\/TOxeBXHO",
      "expanded_url" : "http:\/\/sg.news.yahoo.com\/chinese-indonesians-celebrate-once-forbidden-roots-090744139.html",
      "display_url" : "sg.news.yahoo.com\/chinese-indone\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "161431888114876416",
  "text" : "Chinese-Indonesians celebrate once-forbidden roots - Yahoo! http:\/\/t.co\/TOxeBXHO",
  "id" : 161431888114876416,
  "created_at" : "2012-01-23 12:55:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric",
      "screen_name" : "happyten",
      "indices" : [ 0, 9 ],
      "id_str" : "55843629",
      "id" : 55843629
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "161430178264907776",
  "geo" : { },
  "id_str" : "161431419195891712",
  "in_reply_to_user_id" : 55843629,
  "text" : "@happyten Gong Heng Fai Choy! No snow here in Dublin but it freezing this morning.",
  "id" : 161431419195891712,
  "in_reply_to_status_id" : 161430178264907776,
  "created_at" : "2012-01-23 12:53:44 +0000",
  "in_reply_to_screen_name" : "happyten",
  "in_reply_to_user_id_str" : "55843629",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u827E\u672A\u672A  Ai Weiwei",
      "screen_name" : "aiww",
      "indices" : [ 3, 8 ],
      "id_str" : "43654274",
      "id" : 43654274
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 52 ],
      "url" : "http:\/\/t.co\/x7JyIzzZ",
      "expanded_url" : "http:\/\/path.com\/p\/4ySPIj",
      "display_url" : "path.com\/p\/4ySPIj"
    } ]
  },
  "geo" : { },
  "id_str" : "161427538952982528",
  "text" : "RT @aiww: Just posted a photo \u2014 http:\/\/t.co\/x7JyIzzZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/path.com\/\" rel=\"nofollow\"\u003EPath\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 22, 42 ],
        "url" : "http:\/\/t.co\/x7JyIzzZ",
        "expanded_url" : "http:\/\/path.com\/p\/4ySPIj",
        "display_url" : "path.com\/p\/4ySPIj"
      } ]
    },
    "geo" : { },
    "id_str" : "161375945540907009",
    "text" : "Just posted a photo \u2014 http:\/\/t.co\/x7JyIzzZ",
    "id" : 161375945540907009,
    "created_at" : "2012-01-23 09:13:18 +0000",
    "user" : {
      "name" : "\u827E\u672A\u672A  Ai Weiwei",
      "screen_name" : "aiww",
      "protected" : false,
      "id_str" : "43654274",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2972716369\/e27a35486a2ec507063cb19c89e3ce82_normal.jpeg",
      "id" : 43654274,
      "verified" : true
    }
  },
  "id" : 161427538952982528,
  "created_at" : "2012-01-23 12:38:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161427414570905600",
  "text" : "Trivial fact: Famous people born in Dragon years: former US president Bill Clinton, actress Reese Witherspoon & artist Salvador Dali.",
  "id" : 161427414570905600,
  "created_at" : "2012-01-23 12:37:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161422235448520704",
  "text" : "It Skype sessions with family members from 3 continents during the festive seasons.",
  "id" : 161422235448520704,
  "created_at" : "2012-01-23 12:17:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "indices" : [ 0, 16 ],
      "id_str" : "50343375",
      "id" : 50343375
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "161157568448835584",
  "geo" : { },
  "id_str" : "161409741879394305",
  "in_reply_to_user_id" : 50343375,
  "text" : "@superfinefeline Happy Lunar New Year too!",
  "id" : 161409741879394305,
  "in_reply_to_status_id" : 161157568448835584,
  "created_at" : "2012-01-23 11:27:35 +0000",
  "in_reply_to_screen_name" : "superfinefeline",
  "in_reply_to_user_id_str" : "50343375",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161409083822440448",
  "text" : "\u67D0\u5927\u9F84\u5973\u9752\u5E74\u88AB\u8001\u7238\u50AC\u711D\u4E0D\u8010\u70E6\u5730\u8BF4\uFF1A\u60A8\u4EE5\u4E3A\u73B0\u5728\u5AC1\u4EBA\u5BB9\u6613\u554A\uFF1F\u7537\u4EBA\u90FD\u559C\u6B22\u7537\u4EBA\u4E86\u3002# \u6625\u8282\u5FEB\u4E50 ^_^",
  "id" : 161409083822440448,
  "created_at" : "2012-01-23 11:24:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 65, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 44, 64 ],
      "url" : "http:\/\/t.co\/bYEaF6lV",
      "expanded_url" : "http:\/\/mashable.com\/2012\/01\/04\/google-analytics-guide\/",
      "display_url" : "mashable.com\/2012\/01\/04\/goo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "161396414633615360",
  "text" : "How to Get the Most Out of Google Analytics http:\/\/t.co\/bYEaF6lV #SMEcommunity",
  "id" : 161396414633615360,
  "created_at" : "2012-01-23 10:34:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http:\/\/t.co\/o4KD7EDv",
      "expanded_url" : "http:\/\/lightroomsecrets.com\/2010\/05\/a-catalog-in-the-cloud\/",
      "display_url" : "lightroomsecrets.com\/2010\/05\/a-cata\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "161395555988291584",
  "text" : "Using Dropbox svc to syncronise the Lightroom catalogue between your computers! http:\/\/t.co\/o4KD7EDv",
  "id" : 161395555988291584,
  "created_at" : "2012-01-23 10:31:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161185399241322496",
  "text" : "On eve of CNY, kids are encourage  to stay up late. The longer they stay awake, the longer & healthy lives their parents will have.",
  "id" : 161185399241322496,
  "created_at" : "2012-01-22 20:36:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/161182499437948928\/photo\/1",
      "indices" : [ 29, 49 ],
      "url" : "http:\/\/t.co\/MXDxWuaN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AjyioRdCMAA7CDC.png",
      "id_str" : "161182499442143232",
      "id" : 161182499442143232,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AjyioRdCMAA7CDC.png",
      "sizes" : [ {
        "h" : 565,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 643,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 643,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 320,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MXDxWuaN"
    } ],
    "hashtags" : [ {
      "text" : "CNY",
      "indices" : [ 17, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161182499437948928",
  "text" : "Google Hong Kong #CNY doodle http:\/\/t.co\/MXDxWuaN",
  "id" : 161182499437948928,
  "created_at" : "2012-01-22 20:24:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/flickr.com\/services\/twitter\/\" rel=\"nofollow\"\u003EFlickr\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http:\/\/t.co\/LRJS8ApQ",
      "expanded_url" : "http:\/\/flic.kr\/p\/bgY48B",
      "display_url" : "flic.kr\/p\/bgY48B"
    } ]
  },
  "geo" : { },
  "id_str" : "161181904408809473",
  "text" : "Dublin Chinese New Year Carnival 22 Jan 2012 http:\/\/t.co\/LRJS8ApQ",
  "id" : 161181904408809473,
  "created_at" : "2012-01-22 20:22:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 76 ],
      "url" : "http:\/\/t.co\/SVVlFV3Z",
      "expanded_url" : "http:\/\/datadrivenjournalism.net\/news_and_analysis\/first_ever_data_journalism_awards_open_for_submissions",
      "display_url" : "datadrivenjournalism.net\/news_and_analy\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "161168376041967616",
  "text" : "First ever Data Journalism Awards open for submissions! http:\/\/t.co\/SVVlFV3Z",
  "id" : 161168376041967616,
  "created_at" : "2012-01-22 19:28:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161166169389281280",
  "text" : "\u606D\u795D\uFF02\u63A8\u7279\u53CB\uFF02\u5BCC\u8D35\u5E73\u5B89\uFF0C\u4E07\u4E8B\u5982\u610F\uFF0C\u4E00\u5E06\u98CE\u987A\uFF0C\u5927\u5409\u5927\u5229\u3002",
  "id" : 161166169389281280,
  "created_at" : "2012-01-22 19:19:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http:\/\/t.co\/BP6rvqZb",
      "expanded_url" : "http:\/\/www.cny.ie\/headlines\/4164C2AF84723C8EFD3461CBD7EADF2D",
      "display_url" : "cny.ie\/headlines\/4164\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "161039222915072000",
  "text" : "Asia Market Chinese New Year Carnival's Program  http:\/\/t.co\/BP6rvqZb",
  "id" : 161039222915072000,
  "created_at" : "2012-01-22 10:55:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EPhotos on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/161031023025586176\/photo\/1",
      "indices" : [ 34, 54 ],
      "url" : "http:\/\/t.co\/VElDP3zI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/AjwY3L2CAAARbMA.jpg",
      "id_str" : "161031023029780480",
      "id" : 161031023029780480,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/AjwY3L2CAAARbMA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 384,
        "resize" : "fit",
        "w" : 512
      }, {
        "h" : 384,
        "resize" : "fit",
        "w" : 512
      }, {
        "h" : 384,
        "resize" : "fit",
        "w" : 512
      }, {
        "h" : 384,
        "resize" : "fit",
        "w" : 512
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VElDP3zI"
    } ],
    "hashtags" : [ {
      "text" : "cny",
      "indices" : [ 29, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161031023025586176",
  "text" : "Lunar New Year deal  from EA #cny http:\/\/t.co\/VElDP3zI",
  "id" : 161031023025586176,
  "created_at" : "2012-01-22 10:22:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161028430203338753",
  "text" : "@avalanchelynn well with tax payer money. ;)",
  "id" : 161028430203338753,
  "created_at" : "2012-01-22 10:12:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CNY",
      "indices" : [ 88, 92 ]
    }, {
      "text" : "Dublin",
      "indices" : [ 93, 100 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "161024313443627008",
  "text" : "Anyone heading to Asia Market Chinese New Year Carnival at Meeting House Sq Temple Bar? #CNY #Dublin",
  "id" : 161024313443627008,
  "created_at" : "2012-01-22 09:56:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christopher Fahey",
      "screen_name" : "chrisfahey",
      "indices" : [ 3, 14 ],
      "id_str" : "974131",
      "id" : 974131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160863982620651521",
  "text" : "RT @chrisfahey: NYTimes vid illustrates America's jobs problem. Maybe the real problem is the all-male Playmobil infographix workforce?  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 120, 140 ],
        "url" : "http:\/\/t.co\/0lkRPJ5f",
        "expanded_url" : "http:\/\/www.nytimes.com\/interactive\/2012\/01\/20\/business\/the-iphone-economy.html",
        "display_url" : "nytimes.com\/interactive\/20\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "160852757069316096",
    "text" : "NYTimes vid illustrates America's jobs problem. Maybe the real problem is the all-male Playmobil infographix workforce? http:\/\/t.co\/0lkRPJ5f",
    "id" : 160852757069316096,
    "created_at" : "2012-01-21 22:34:20 +0000",
    "user" : {
      "name" : "Christopher Fahey",
      "screen_name" : "chrisfahey",
      "protected" : false,
      "id_str" : "974131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959409193211097088\/mC_H642I_normal.jpg",
      "id" : 974131,
      "verified" : false
    }
  },
  "id" : 160863982620651521,
  "created_at" : "2012-01-21 23:18:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Liau",
      "screen_name" : "benliau",
      "indices" : [ 0, 8 ],
      "id_str" : "17336534",
      "id" : 17336534
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "160837750545854464",
  "geo" : { },
  "id_str" : "160840983150469121",
  "in_reply_to_user_id" : 17336534,
  "text" : "@benliau oh...looks more like a Hand Plane to me :)",
  "id" : 160840983150469121,
  "in_reply_to_status_id" : 160837750545854464,
  "created_at" : "2012-01-21 21:47:33 +0000",
  "in_reply_to_screen_name" : "benliau",
  "in_reply_to_user_id_str" : "17336534",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Liau",
      "screen_name" : "benliau",
      "indices" : [ 0, 8 ],
      "id_str" : "17336534",
      "id" : 17336534
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "160831708109864961",
  "geo" : { },
  "id_str" : "160837230787698688",
  "in_reply_to_user_id" : 17336534,
  "text" : "@benliau looks more like a power tool to me.",
  "id" : 160837230787698688,
  "in_reply_to_status_id" : 160831708109864961,
  "created_at" : "2012-01-21 21:32:38 +0000",
  "in_reply_to_screen_name" : "benliau",
  "in_reply_to_user_id_str" : "17336534",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/160792196616949760\/photo\/1",
      "indices" : [ 44, 64 ],
      "url" : "http:\/\/t.co\/c1YClkP3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Ajs_pqJCEAE6zp_.jpg",
      "id_str" : "160792196621144065",
      "id" : 160792196621144065,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ajs_pqJCEAE6zp_.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/c1YClkP3"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160792196616949760",
  "text" : "My Facebook Timeline business cards is here http:\/\/t.co\/c1YClkP3",
  "id" : 160792196616949760,
  "created_at" : "2012-01-21 18:33:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elish Bul-Godley",
      "screen_name" : "ElishBulGodley",
      "indices" : [ 3, 18 ],
      "id_str" : "311397063",
      "id" : 311397063
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 136 ],
      "url" : "http:\/\/t.co\/TlHhc0Xz",
      "expanded_url" : "http:\/\/bloggertone.com\/global\/2012\/01\/06\/welcoming-the-year-of-the-dragon-business-tips-from-the-far-east\/",
      "display_url" : "bloggertone.com\/global\/2012\/01\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "160738215748304897",
  "text" : "RT @ElishBulGodley: Welcoming The Year Of The Dragon: Business Tips From The Far East | Internationalization blog | http:\/\/t.co\/TlHhc0Xz ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bloggertone",
        "screen_name" : "Bloggertone",
        "indices" : [ 121, 133 ],
        "id_str" : "2809742219",
        "id" : 2809742219
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 96, 116 ],
        "url" : "http:\/\/t.co\/TlHhc0Xz",
        "expanded_url" : "http:\/\/bloggertone.com\/global\/2012\/01\/06\/welcoming-the-year-of-the-dragon-business-tips-from-the-far-east\/",
        "display_url" : "bloggertone.com\/global\/2012\/01\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "160658047218356224",
    "text" : "Welcoming The Year Of The Dragon: Business Tips From The Far East | Internationalization blog | http:\/\/t.co\/TlHhc0Xz via @bloggertone",
    "id" : 160658047218356224,
    "created_at" : "2012-01-21 09:40:37 +0000",
    "user" : {
      "name" : "Elish Bul-Godley",
      "screen_name" : "ElishBulGodley",
      "protected" : false,
      "id_str" : "311397063",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/781120799998607361\/Zq4hT0b3_normal.jpg",
      "id" : 311397063,
      "verified" : false
    }
  },
  "id" : 160738215748304897,
  "created_at" : "2012-01-21 14:59:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "indices" : [ 0, 16 ],
      "id_str" : "50343375",
      "id" : 50343375
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "159698626736685056",
  "geo" : { },
  "id_str" : "160736567516868609",
  "in_reply_to_user_id" : 50343375,
  "text" : "@superfinefeline Do they reply back on using your images for their website?",
  "id" : 160736567516868609,
  "in_reply_to_status_id" : 159698626736685056,
  "created_at" : "2012-01-21 14:52:38 +0000",
  "in_reply_to_screen_name" : "superfinefeline",
  "in_reply_to_user_id_str" : "50343375",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CSIS Southeast Asia",
      "screen_name" : "SoutheastAsiaDC",
      "indices" : [ 3, 19 ],
      "id_str" : "90407341",
      "id" : 90407341
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 125 ],
      "url" : "http:\/\/t.co\/M0zjBqMi",
      "expanded_url" : "http:\/\/fb.me\/138J2flD3",
      "display_url" : "fb.me\/138J2flD3"
    } ]
  },
  "geo" : { },
  "id_str" : "160733045291364355",
  "text" : "RT @SoutheastAsiaDC: Southeast Asia is mostly in the RED, on Transparency International's corruption map http:\/\/t.co\/M0zjBqMi",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 104 ],
        "url" : "http:\/\/t.co\/M0zjBqMi",
        "expanded_url" : "http:\/\/fb.me\/138J2flD3",
        "display_url" : "fb.me\/138J2flD3"
      } ]
    },
    "geo" : { },
    "id_str" : "160728446773903360",
    "text" : "Southeast Asia is mostly in the RED, on Transparency International's corruption map http:\/\/t.co\/M0zjBqMi",
    "id" : 160728446773903360,
    "created_at" : "2012-01-21 14:20:22 +0000",
    "user" : {
      "name" : "CSIS Southeast Asia",
      "screen_name" : "SoutheastAsiaDC",
      "protected" : false,
      "id_str" : "90407341",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/717785626511171584\/OSJcER0U_normal.jpg",
      "id" : 90407341,
      "verified" : false
    }
  },
  "id" : 160733045291364355,
  "created_at" : "2012-01-21 14:38:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 37 ],
      "url" : "http:\/\/t.co\/v5BHpHzZ",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/File:Chilli_Padi_red.JPG",
      "display_url" : "en.wikipedia.org\/wiki\/File:Chil\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "160731995650015232",
  "text" : "I can't get this http:\/\/t.co\/v5BHpHzZ at both Asian store in Dublin. Damn!",
  "id" : 160731995650015232,
  "created_at" : "2012-01-21 14:34:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "#Carl T Griffith \uD83D\uDE0E",
      "screen_name" : "CarlGriffith",
      "indices" : [ 0, 13 ],
      "id_str" : "5417662",
      "id" : 5417662
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "160730305546813440",
  "geo" : { },
  "id_str" : "160731393649934336",
  "in_reply_to_user_id" : 5417662,
  "text" : "@CarlGriffith You got an auspicious number (No. 8) for your unit. :) Gong Xi Fai Chai!",
  "id" : 160731393649934336,
  "in_reply_to_status_id" : 160730305546813440,
  "created_at" : "2012-01-21 14:32:05 +0000",
  "in_reply_to_screen_name" : "CarlGriffith",
  "in_reply_to_user_id_str" : "5417662",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160730167780704256",
  "text" : "Even at 1130am, street looks empty today in Dublin",
  "id" : 160730167780704256,
  "created_at" : "2012-01-21 14:27:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/dSov2z2p",
      "expanded_url" : "http:\/\/now.periscopic.com\/2012\/01\/periscopic-featured-in-print-magazine\/",
      "display_url" : "now.periscopic.com\/2012\/01\/perisc\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "160638201151750145",
  "text" : "\"In general, data visualizations deal with facts.\" http:\/\/t.co\/dSov2z2p",
  "id" : 160638201151750145,
  "created_at" : "2012-01-21 08:21:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 128 ],
      "url" : "http:\/\/t.co\/yMNVtPnm",
      "expanded_url" : "http:\/\/url.ie\/e0i4",
      "display_url" : "url.ie\/e0i4"
    } ]
  },
  "geo" : { },
  "id_str" : "160635913444139008",
  "text" : "RT @topgold: If you're an average worker during Ireland's recession, you're headed straight for the bottom. http:\/\/t.co\/yMNVtPnm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dabr.co.uk\" rel=\"nofollow\"\u003EDabr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 115 ],
        "url" : "http:\/\/t.co\/yMNVtPnm",
        "expanded_url" : "http:\/\/url.ie\/e0i4",
        "display_url" : "url.ie\/e0i4"
      } ]
    },
    "geo" : { },
    "id_str" : "160592233094057985",
    "text" : "If you're an average worker during Ireland's recession, you're headed straight for the bottom. http:\/\/t.co\/yMNVtPnm",
    "id" : 160592233094057985,
    "created_at" : "2012-01-21 05:19:06 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 160635913444139008,
  "created_at" : "2012-01-21 08:12:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Manchester News MEN",
      "screen_name" : "MENnewsdesk",
      "indices" : [ 3, 15 ],
      "id_str" : "20678866",
      "id" : 20678866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 124 ],
      "url" : "http:\/\/t.co\/VZ2rdkFk",
      "expanded_url" : "http:\/\/dlvr.it\/16MdfN",
      "display_url" : "dlvr.it\/16MdfN"
    } ]
  },
  "geo" : { },
  "id_str" : "160634507911901185",
  "text" : "RT @MENnewsdesk: The 10-year-old girl from Rochdale who is a global TV star - as the voice of Peppa Pig http:\/\/t.co\/VZ2rdkFk",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 107 ],
        "url" : "http:\/\/t.co\/VZ2rdkFk",
        "expanded_url" : "http:\/\/dlvr.it\/16MdfN",
        "display_url" : "dlvr.it\/16MdfN"
      } ]
    },
    "geo" : { },
    "id_str" : "160616633101062144",
    "text" : "The 10-year-old girl from Rochdale who is a global TV star - as the voice of Peppa Pig http:\/\/t.co\/VZ2rdkFk",
    "id" : 160616633101062144,
    "created_at" : "2012-01-21 06:56:04 +0000",
    "user" : {
      "name" : "Manchester News MEN",
      "screen_name" : "MENnewsdesk",
      "protected" : false,
      "id_str" : "20678866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034761426441830400\/wlUHaUsZ_normal.jpg",
      "id" : 20678866,
      "verified" : true
    }
  },
  "id" : 160634507911901185,
  "created_at" : "2012-01-21 08:07:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kathryn Presner",
      "screen_name" : "zoonini",
      "indices" : [ 0, 8 ],
      "id_str" : "16671466",
      "id" : 16671466
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "160511159185833984",
  "geo" : { },
  "id_str" : "160517691529113600",
  "in_reply_to_user_id" : 16671466,
  "text" : "@zoonini Many Thanks on this! Cheers",
  "id" : 160517691529113600,
  "in_reply_to_status_id" : 160511159185833984,
  "created_at" : "2012-01-21 00:22:54 +0000",
  "in_reply_to_screen_name" : "zoonini",
  "in_reply_to_user_id_str" : "16671466",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160486460502450176",
  "text" : "1995's The Quick and the Dead on More 4 - Leonardo DiCaprio is just a teen",
  "id" : 160486460502450176,
  "created_at" : "2012-01-20 22:18:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elish Bul-Godley",
      "screen_name" : "ElishBulGodley",
      "indices" : [ 0, 15 ],
      "id_str" : "311397063",
      "id" : 311397063
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "160403368936882177",
  "geo" : { },
  "id_str" : "160438862001356800",
  "in_reply_to_user_id" : 311397063,
  "text" : "@ElishBulGodley Gong Xi Fa Chai to you too!",
  "id" : 160438862001356800,
  "in_reply_to_status_id" : 160403368936882177,
  "created_at" : "2012-01-20 19:09:40 +0000",
  "in_reply_to_screen_name" : "ElishBulGodley",
  "in_reply_to_user_id_str" : "311397063",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160437015551279106",
  "text" : "For us, coming to Ireland to work and stay is a \u2018free choice of lifestyle\u2019",
  "id" : 160437015551279106,
  "created_at" : "2012-01-20 19:02:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IRAS",
      "screen_name" : "IRAS_SG",
      "indices" : [ 0, 8 ],
      "id_str" : "119762742",
      "id" : 119762742
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160357404037496832",
  "in_reply_to_user_id" : 119762742,
  "text" : "@IRAS_SG I unable to log into My Tax Portal. It just keep on re-directing me back to My Tax Portal. Thks.",
  "id" : 160357404037496832,
  "created_at" : "2012-01-20 13:45:59 +0000",
  "in_reply_to_screen_name" : "IRAS_SG",
  "in_reply_to_user_id_str" : "119762742",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http:\/\/t.co\/5ot85u08",
      "expanded_url" : "http:\/\/bbc.in\/Am52dK",
      "display_url" : "bbc.in\/Am52dK"
    } ]
  },
  "geo" : { },
  "id_str" : "160326037866151936",
  "text" : "BBC News - Enter the dragons: A baby boom for Chinese across Asia http:\/\/t.co\/5ot85u08",
  "id" : 160326037866151936,
  "created_at" : "2012-01-20 11:41:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160325463682715648",
  "text" : "A french engineer on TV \"best chances of getting work rest with the socialist party\"...yeah right...",
  "id" : 160325463682715648,
  "created_at" : "2012-01-20 11:39:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Chinese NYF",
      "screen_name" : "DCNYF",
      "indices" : [ 3, 9 ],
      "id_str" : "236581659",
      "id" : 236581659
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160324898261188608",
  "text" : "RT @dcnyf: Beautiful exhibition of images of China by John Thomson (1837-1921) now open in Chester Beatty Library. This will... http:\/\/t ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 137 ],
        "url" : "http:\/\/t.co\/qfNbbyyN",
        "expanded_url" : "http:\/\/fb.me\/1o8Sn2HCp",
        "display_url" : "fb.me\/1o8Sn2HCp"
      } ]
    },
    "geo" : { },
    "id_str" : "137489341739057152",
    "text" : "Beautiful exhibition of images of China by John Thomson (1837-1921) now open in Chester Beatty Library. This will... http:\/\/t.co\/qfNbbyyN",
    "id" : 137489341739057152,
    "created_at" : "2011-11-18 11:16:28 +0000",
    "user" : {
      "name" : "Dublin Chinese NYF",
      "screen_name" : "DCNYF",
      "protected" : false,
      "id_str" : "236581659",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/938711063704436736\/Yxe-aBxC_normal.jpg",
      "id" : 236581659,
      "verified" : false
    }
  },
  "id" : 160324898261188608,
  "created_at" : "2012-01-20 11:36:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 71 ],
      "url" : "http:\/\/t.co\/phiOwduZ",
      "expanded_url" : "http:\/\/www.nytimes.com\/2009\/07\/19\/magazine\/19healthcare-t.html?_r=2&pagewanted=all",
      "display_url" : "nytimes.com\/2009\/07\/19\/mag\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "160321599311065088",
  "text" : "Argument for placing a dollar value on human life. http:\/\/t.co\/phiOwduZ",
  "id" : 160321599311065088,
  "created_at" : "2012-01-20 11:23:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GenerationEmigration",
      "screen_name" : "genemigration",
      "indices" : [ 3, 17 ],
      "id_str" : "786547679425802240",
      "id" : 786547679425802240
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160315702564225025",
  "text" : "RT @GenEmigration: \u2018I didn\u2019t have a mortgage, I was single, it was very easy for me to move\u2019 Caroline Bowler on expat life in Singapore  ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 137 ],
        "url" : "http:\/\/t.co\/kdL9hThK",
        "expanded_url" : "http:\/\/bit.ly\/yNplnQ",
        "display_url" : "bit.ly\/yNplnQ"
      } ]
    },
    "geo" : { },
    "id_str" : "160290342267133952",
    "text" : "\u2018I didn\u2019t have a mortgage, I was single, it was very easy for me to move\u2019 Caroline Bowler on expat life in Singapore http:\/\/t.co\/kdL9hThK",
    "id" : 160290342267133952,
    "created_at" : "2012-01-20 09:19:30 +0000",
    "user" : {
      "name" : "Irish Times Abroad",
      "screen_name" : "ITabroad",
      "protected" : false,
      "id_str" : "394027634",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/793579775403126798\/hAgGxOnq_normal.jpg",
      "id" : 394027634,
      "verified" : false
    }
  },
  "id" : 160315702564225025,
  "created_at" : "2012-01-20 11:00:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karl Norman",
      "screen_name" : "karlnorman",
      "indices" : [ 3, 14 ],
      "id_str" : "21749008",
      "id" : 21749008
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 96 ],
      "url" : "http:\/\/t.co\/UP0vuZyx",
      "expanded_url" : "http:\/\/bit.ly\/AzdjR1",
      "display_url" : "bit.ly\/AzdjR1"
    } ]
  },
  "geo" : { },
  "id_str" : "160312677338591232",
  "text" : "RT @karlnorman: How To Integrate Facebook, Twitter And Google+ In WordPress http:\/\/t.co\/UP0vuZyx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/socialcast.localvox.com\" rel=\"nofollow\"\u003ESocialCastLV\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 80 ],
        "url" : "http:\/\/t.co\/UP0vuZyx",
        "expanded_url" : "http:\/\/bit.ly\/AzdjR1",
        "display_url" : "bit.ly\/AzdjR1"
      } ]
    },
    "geo" : { },
    "id_str" : "160311407831822337",
    "text" : "How To Integrate Facebook, Twitter And Google+ In WordPress http:\/\/t.co\/UP0vuZyx",
    "id" : 160311407831822337,
    "created_at" : "2012-01-20 10:43:12 +0000",
    "user" : {
      "name" : "Karl Norman",
      "screen_name" : "karlnorman",
      "protected" : false,
      "id_str" : "21749008",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1535139495\/IMG_0159_normal.jpg",
      "id" : 21749008,
      "verified" : false
    }
  },
  "id" : 160312677338591232,
  "created_at" : "2012-01-20 10:48:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "iHeartThisApp",
      "screen_name" : "iHeartThisApp",
      "indices" : [ 3, 17 ],
      "id_str" : "280918201",
      "id" : 280918201
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 134 ],
      "url" : "http:\/\/t.co\/SOiroCQD",
      "expanded_url" : "http:\/\/ow.ly\/8yDEX",
      "display_url" : "ow.ly\/8yDEX"
    } ]
  },
  "geo" : { },
  "id_str" : "160041146666594304",
  "text" : "RT @iHeartThisApp: Hogworld: Gnarts Adventure is FREE!  Plus checkout the other Apps for Kids that are FREE\/Sale! http:\/\/t.co\/SOiroCQD",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 115 ],
        "url" : "http:\/\/t.co\/SOiroCQD",
        "expanded_url" : "http:\/\/ow.ly\/8yDEX",
        "display_url" : "ow.ly\/8yDEX"
      } ]
    },
    "geo" : { },
    "id_str" : "160028987802918912",
    "text" : "Hogworld: Gnarts Adventure is FREE!  Plus checkout the other Apps for Kids that are FREE\/Sale! http:\/\/t.co\/SOiroCQD",
    "id" : 160028987802918912,
    "created_at" : "2012-01-19 16:00:58 +0000",
    "user" : {
      "name" : "iHeartThisApp",
      "screen_name" : "iHeartThisApp",
      "protected" : false,
      "id_str" : "280918201",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1308783288\/TwitProf_normal.jpg",
      "id" : 280918201,
      "verified" : false
    }
  },
  "id" : 160041146666594304,
  "created_at" : "2012-01-19 16:49:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "OECD",
      "screen_name" : "OECD",
      "indices" : [ 3, 8 ],
      "id_str" : "25390350",
      "id" : 25390350
    }, {
      "name" : "OECD Social",
      "screen_name" : "OECD_Social",
      "indices" : [ 108, 120 ],
      "id_str" : "85526005",
      "id" : 85526005
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OECDhealth",
      "indices" : [ 121, 132 ]
    }, {
      "text" : "stats",
      "indices" : [ 133, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http:\/\/t.co\/uXygXjSr",
      "expanded_url" : "http:\/\/bit.ly\/x4B3XF",
      "display_url" : "bit.ly\/x4B3XF"
    } ]
  },
  "geo" : { },
  "id_str" : "160040442128384000",
  "text" : "RT @OECD: US & Greece do more MRI & CT exams per capita than any other OECD country http:\/\/t.co\/uXygXjSr RT @OECD_Social #OECDhealth #stats",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "OECD Social",
        "screen_name" : "OECD_Social",
        "indices" : [ 98, 110 ],
        "id_str" : "85526005",
        "id" : 85526005
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "OECDhealth",
        "indices" : [ 111, 122 ]
      }, {
        "text" : "stats",
        "indices" : [ 123, 129 ]
      } ],
      "urls" : [ {
        "indices" : [ 74, 94 ],
        "url" : "http:\/\/t.co\/uXygXjSr",
        "expanded_url" : "http:\/\/bit.ly\/x4B3XF",
        "display_url" : "bit.ly\/x4B3XF"
      } ]
    },
    "geo" : { },
    "id_str" : "160032531738132481",
    "text" : "US & Greece do more MRI & CT exams per capita than any other OECD country http:\/\/t.co\/uXygXjSr RT @OECD_Social #OECDhealth #stats",
    "id" : 160032531738132481,
    "created_at" : "2012-01-19 16:15:03 +0000",
    "user" : {
      "name" : "OECD",
      "screen_name" : "OECD",
      "protected" : false,
      "id_str" : "25390350",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2704512490\/f8fdec2f9125956c0f0f92decee4ac71_normal.png",
      "id" : 25390350,
      "verified" : true
    }
  },
  "id" : 160040442128384000,
  "created_at" : "2012-01-19 16:46:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Pepper",
      "screen_name" : "daveO2irl",
      "indices" : [ 3, 13 ],
      "id_str" : "208545874",
      "id" : 208545874
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "itunesu",
      "indices" : [ 117, 125 ]
    }, {
      "text" : "apple",
      "indices" : [ 126, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160032086772813824",
  "text" : "RT @daveO2irl: Can anyone see this working in Ireland? Who's going to pay for the devices would be my main concern.. #itunesu #apple",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "itunesu",
        "indices" : [ 102, 110 ]
      }, {
        "text" : "apple",
        "indices" : [ 111, 117 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "160030834844385280",
    "text" : "Can anyone see this working in Ireland? Who's going to pay for the devices would be my main concern.. #itunesu #apple",
    "id" : 160030834844385280,
    "created_at" : "2012-01-19 16:08:18 +0000",
    "user" : {
      "name" : "Dave Pepper",
      "screen_name" : "daveO2irl",
      "protected" : false,
      "id_str" : "208545874",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1154174311\/DP_normal.PNG",
      "id" : 208545874,
      "verified" : false
    }
  },
  "id" : 160032086772813824,
  "created_at" : "2012-01-19 16:13:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oliver Burkeman",
      "screen_name" : "oliverburkeman",
      "indices" : [ 3, 18 ],
      "id_str" : "112037009",
      "id" : 112037009
    }, {
      "name" : "@sweden \/ Rebecka",
      "screen_name" : "sweden",
      "indices" : [ 69, 76 ],
      "id_str" : "19663706",
      "id" : 19663706
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "160030803638751233",
  "text" : "RT @oliverburkeman: Brilliant: Swedish tourist board is handing over @sweden to a different Swede each week. This week, \"just your avera ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "@sweden \/ Rebecka",
        "screen_name" : "sweden",
        "indices" : [ 49, 56 ],
        "id_str" : "19663706",
        "id" : 19663706
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "160025582703362051",
    "text" : "Brilliant: Swedish tourist board is handing over @sweden to a different Swede each week. This week, \"just your average lesbian truck driver\"",
    "id" : 160025582703362051,
    "created_at" : "2012-01-19 15:47:26 +0000",
    "user" : {
      "name" : "Oliver Burkeman",
      "screen_name" : "oliverburkeman",
      "protected" : false,
      "id_str" : "112037009",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/809803556878483456\/wm-6DZST_normal.jpg",
      "id" : 112037009,
      "verified" : true
    }
  },
  "id" : 160030803638751233,
  "created_at" : "2012-01-19 16:08:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Chinese NYF",
      "screen_name" : "DCNYF",
      "indices" : [ 3, 9 ],
      "id_str" : "236581659",
      "id" : 236581659
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http:\/\/t.co\/mgXjQe38",
      "expanded_url" : "http:\/\/fb.me\/1gTiv6Cp5",
      "display_url" : "fb.me\/1gTiv6Cp5"
    } ]
  },
  "geo" : { },
  "id_str" : "159981614766620672",
  "text" : "RT @dcnyf: check out what's on for this DRAGON year festival!!!! http:\/\/t.co\/mgXjQe38",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 74 ],
        "url" : "http:\/\/t.co\/mgXjQe38",
        "expanded_url" : "http:\/\/fb.me\/1gTiv6Cp5",
        "display_url" : "fb.me\/1gTiv6Cp5"
      } ]
    },
    "geo" : { },
    "id_str" : "158266719968829441",
    "text" : "check out what's on for this DRAGON year festival!!!! http:\/\/t.co\/mgXjQe38",
    "id" : 158266719968829441,
    "created_at" : "2012-01-14 19:18:21 +0000",
    "user" : {
      "name" : "Dublin Chinese NYF",
      "screen_name" : "DCNYF",
      "protected" : false,
      "id_str" : "236581659",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/938711063704436736\/Yxe-aBxC_normal.jpg",
      "id" : 236581659,
      "verified" : false
    }
  },
  "id" : 159981614766620672,
  "created_at" : "2012-01-19 12:52:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159896853570269184",
  "text" : "Appreciate the importance of fire alarm!",
  "id" : 159896853570269184,
  "created_at" : "2012-01-19 07:15:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159889627057094656",
  "text" : "Fire at a unit in our apartment at 550am. Evacuated. Dublin Fire bridge put out the fire.",
  "id" : 159889627057094656,
  "created_at" : "2012-01-19 06:47:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Fugelsang",
      "screen_name" : "JohnFugelsang",
      "indices" : [ 3, 17 ],
      "id_str" : "33276161",
      "id" : 33276161
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159770706614288384",
  "text" : "RT @JohnFugelsang: I'm against Sopa but not Pipa cos she was the only thing that kept me awake in the Royal Wedding.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "159769766599475200",
    "text" : "I'm against Sopa but not Pipa cos she was the only thing that kept me awake in the Royal Wedding.",
    "id" : 159769766599475200,
    "created_at" : "2012-01-18 22:50:55 +0000",
    "user" : {
      "name" : "John Fugelsang",
      "screen_name" : "JohnFugelsang",
      "protected" : false,
      "id_str" : "33276161",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/618501788518342656\/ycqZZrVj_normal.jpg",
      "id" : 33276161,
      "verified" : true
    }
  },
  "id" : 159770706614288384,
  "created_at" : "2012-01-18 22:54:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159770121592778752",
  "text" : "Choc drink taste just better with a pinch of salt. Learn that from a tv chef.",
  "id" : 159770121592778752,
  "created_at" : "2012-01-18 22:52:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "indices" : [ 0, 16 ],
      "id_str" : "50343375",
      "id" : 50343375
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "159687960462237698",
  "geo" : { },
  "id_str" : "159698334997676033",
  "in_reply_to_user_id" : 50343375,
  "text" : "@superfinefeline Do a screenshot of the site that use the pic and email them. Ask them to stop OR better sent them an invoice for pic use.",
  "id" : 159698334997676033,
  "in_reply_to_status_id" : 159687960462237698,
  "created_at" : "2012-01-18 18:07:04 +0000",
  "in_reply_to_screen_name" : "superfinefeline",
  "in_reply_to_user_id_str" : "50343375",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheMarketingShop.ie",
      "screen_name" : "marketingdebbie",
      "indices" : [ 0, 16 ],
      "id_str" : "114487731",
      "id" : 114487731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "159668657310531584",
  "geo" : { },
  "id_str" : "159680479702941697",
  "in_reply_to_user_id" : 114487731,
  "text" : "@marketingdebbie I used them on some of my social media profile but not on my biz card.",
  "id" : 159680479702941697,
  "in_reply_to_status_id" : 159668657310531584,
  "created_at" : "2012-01-18 16:56:07 +0000",
  "in_reply_to_screen_name" : "marketingdebbie",
  "in_reply_to_user_id_str" : "114487731",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159678976858664960",
  "text" : "Yesterday, I can't help giggle when Ian Hislop appeared on  media ethics inquiry. Reminds me of the show Have I got news for you.",
  "id" : 159678976858664960,
  "created_at" : "2012-01-18 16:50:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 107 ],
      "url" : "http:\/\/t.co\/H95wEaWR",
      "expanded_url" : "http:\/\/www.independent.ie\/world-news\/europe\/costa-concordia-italians-buy-tshirts-with-get-back-on-board-for-s-sake-logo-2993279.html",
      "display_url" : "independent.ie\/world-news\/eur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "159675329164754944",
  "text" : "Costa Concordia: Italians buy t-shirts with 'Get back on board, for ----\u2019s sake!' logo http:\/\/t.co\/H95wEaWR",
  "id" : 159675329164754944,
  "created_at" : "2012-01-18 16:35:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159670077212471296",
  "text" : "@avalanchelynn oh yes over here consumer right to return goods is very strong in EU. (unlike Singapore)",
  "id" : 159670077212471296,
  "created_at" : "2012-01-18 16:14:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159666977521139712",
  "text" : "In the meantime, son is \"scared for life\" cos the return xmas card.",
  "id" : 159666977521139712,
  "created_at" : "2012-01-18 16:02:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159666739532144640",
  "text" : "Son's classmate return Xmas card cos classmate do not like the design. Need to wait till wiki blackout ends to find out about the culture.",
  "id" : 159666739532144640,
  "created_at" : "2012-01-18 16:01:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Kemp",
      "screen_name" : "eskimon",
      "indices" : [ 3, 11 ],
      "id_str" : "15177871",
      "id" : 15177871
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 128 ],
      "url" : "http:\/\/t.co\/fXV12CFg",
      "expanded_url" : "http:\/\/uniprogy.com\/",
      "display_url" : "uniprogy.com"
    } ]
  },
  "geo" : { },
  "id_str" : "159640461361950720",
  "text" : "RT @eskimon: Forget Groupon - there's now a service where you can make your own group buying site for $200: http:\/\/t.co\/fXV12CFg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 115 ],
        "url" : "http:\/\/t.co\/fXV12CFg",
        "expanded_url" : "http:\/\/uniprogy.com\/",
        "display_url" : "uniprogy.com"
      } ]
    },
    "geo" : { },
    "id_str" : "159636323412623360",
    "text" : "Forget Groupon - there's now a service where you can make your own group buying site for $200: http:\/\/t.co\/fXV12CFg",
    "id" : 159636323412623360,
    "created_at" : "2012-01-18 14:00:40 +0000",
    "user" : {
      "name" : "Simon Kemp",
      "screen_name" : "eskimon",
      "protected" : false,
      "id_str" : "15177871",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/673806239634169856\/ui-tRkJy_normal.jpg",
      "id" : 15177871,
      "verified" : true
    }
  },
  "id" : 159640461361950720,
  "created_at" : "2012-01-18 14:17:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clark MacLeod \uFF08\u514B\u62C9\u514B\uFF09",
      "screen_name" : "kelake",
      "indices" : [ 3, 10 ],
      "id_str" : "821846",
      "id" : 821846
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159608685721686016",
  "text" : "RT @kelake: Path is immeasurably better than Facebook or Google + but alas no one I know uses it.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "159604678299090944",
    "text" : "Path is immeasurably better than Facebook or Google + but alas no one I know uses it.",
    "id" : 159604678299090944,
    "created_at" : "2012-01-18 11:54:55 +0000",
    "user" : {
      "name" : "Clark MacLeod \uFF08\u514B\u62C9\u514B\uFF09",
      "screen_name" : "kelake",
      "protected" : false,
      "id_str" : "821846",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039671370785976321\/Q5NwY6oQ_normal.jpg",
      "id" : 821846,
      "verified" : false
    }
  },
  "id" : 159608685721686016,
  "created_at" : "2012-01-18 12:10:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http:\/\/t.co\/OeVuvk1J",
      "expanded_url" : "http:\/\/sg.news.yahoo.com\/guess-where-mahjong-is-gaining-a-foothold-.html",
      "display_url" : "sg.news.yahoo.com\/guess-where-ma\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "159600401740611588",
  "text" : "Surprise to learn that Chinese board game \"Mahjong\" is gaining foothold in Denmark. http:\/\/t.co\/OeVuvk1J",
  "id" : 159600401740611588,
  "created_at" : "2012-01-18 11:37:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Interaction19",
      "screen_name" : "ixdconf",
      "indices" : [ 19, 27 ],
      "id_str" : "21664207",
      "id" : 21664207
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159598251018952704",
  "text" : "Just signed up for @ixdconf FREE workshop at Convention Centre Dublin. Hope to see you there.",
  "id" : 159598251018952704,
  "created_at" : "2012-01-18 11:29:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Neasa",
      "screen_name" : "neasac",
      "indices" : [ 3, 10 ],
      "id_str" : "19526862",
      "id" : 19526862
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 93 ],
      "url" : "http:\/\/t.co\/RTAW04QS",
      "expanded_url" : "http:\/\/yfrog.com\/h435qbp",
      "display_url" : "yfrog.com\/h435qbp"
    } ]
  },
  "geo" : { },
  "id_str" : "159594277637332992",
  "text" : "RT @neasac: So do you fire the media agency or is it the paper's fault?  http:\/\/t.co\/RTAW04QS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 81 ],
        "url" : "http:\/\/t.co\/RTAW04QS",
        "expanded_url" : "http:\/\/yfrog.com\/h435qbp",
        "display_url" : "yfrog.com\/h435qbp"
      } ]
    },
    "geo" : { },
    "id_str" : "159591923173167104",
    "text" : "So do you fire the media agency or is it the paper's fault?  http:\/\/t.co\/RTAW04QS",
    "id" : 159591923173167104,
    "created_at" : "2012-01-18 11:04:14 +0000",
    "user" : {
      "name" : "Neasa",
      "screen_name" : "neasac",
      "protected" : false,
      "id_str" : "19526862",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/412240286283096064\/YMiZsA9h_normal.jpeg",
      "id" : 19526862,
      "verified" : false
    }
  },
  "id" : 159594277637332992,
  "created_at" : "2012-01-18 11:13:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Grafton Media",
      "screen_name" : "GraftonMedia",
      "indices" : [ 0, 13 ],
      "id_str" : "374067886",
      "id" : 374067886
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "159317031118843905",
  "geo" : { },
  "id_str" : "159349974864367616",
  "in_reply_to_user_id" : 374067886,
  "text" : "@GraftonMedia Checkout vikingdirect.ie They seem to have the lowest price for iPad 2 in IRL.",
  "id" : 159349974864367616,
  "in_reply_to_status_id" : 159317031118843905,
  "created_at" : "2012-01-17 19:02:49 +0000",
  "in_reply_to_screen_name" : "GraftonMedia",
  "in_reply_to_user_id_str" : "374067886",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Retwittings",
      "screen_name" : "retwittings",
      "indices" : [ 3, 15 ],
      "id_str" : "1391733006",
      "id" : 1391733006
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159278173958307841",
  "text" : "RT @Retwittings: To \"testify\" was based on men in the Roman court swearing to a statement made by swearing on their testicles.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "159276294956253187",
    "text" : "To \"testify\" was based on men in the Roman court swearing to a statement made by swearing on their testicles.",
    "id" : 159276294956253187,
    "created_at" : "2012-01-17 14:10:02 +0000",
    "user" : {
      "name" : "BriskHumor",
      "screen_name" : "BriskHumor",
      "protected" : false,
      "id_str" : "407454381",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/587187738094374913\/0tuVR1vA_normal.png",
      "id" : 407454381,
      "verified" : false
    }
  },
  "id" : 159278173958307841,
  "created_at" : "2012-01-17 14:17:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ori Sasson",
      "screen_name" : "orisasson",
      "indices" : [ 0, 10 ],
      "id_str" : "19797964",
      "id" : 19797964
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "159273807482339328",
  "geo" : { },
  "id_str" : "159276231135735808",
  "in_reply_to_user_id" : 19797964,
  "text" : "@orisasson Good to see you back on Twitter!",
  "id" : 159276231135735808,
  "in_reply_to_status_id" : 159273807482339328,
  "created_at" : "2012-01-17 14:09:47 +0000",
  "in_reply_to_screen_name" : "orisasson",
  "in_reply_to_user_id_str" : "19797964",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Callan Tham",
      "screen_name" : "ctham",
      "indices" : [ 0, 6 ],
      "id_str" : "23011555",
      "id" : 23011555
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "159274395985121280",
  "geo" : { },
  "id_str" : "159276005272469505",
  "in_reply_to_user_id" : 23011555,
  "text" : "@ctham The Swede spelt Geylang as Gaylang :)",
  "id" : 159276005272469505,
  "in_reply_to_status_id" : 159274395985121280,
  "created_at" : "2012-01-17 14:08:53 +0000",
  "in_reply_to_screen_name" : "ctham",
  "in_reply_to_user_id_str" : "23011555",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Callan Tham",
      "screen_name" : "ctham",
      "indices" : [ 3, 9 ],
      "id_str" : "23011555",
      "id" : 23011555
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 88 ],
      "url" : "http:\/\/t.co\/Jiutxn9a",
      "expanded_url" : "http:\/\/flpbd.it\/RG06",
      "display_url" : "flpbd.it\/RG06"
    } ]
  },
  "geo" : { },
  "id_str" : "159275421416955904",
  "text" : "RT @ctham: S'pore is \"globalization for early adopters\", says Swede http:\/\/t.co\/Jiutxn9a",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 77 ],
        "url" : "http:\/\/t.co\/Jiutxn9a",
        "expanded_url" : "http:\/\/flpbd.it\/RG06",
        "display_url" : "flpbd.it\/RG06"
      } ]
    },
    "geo" : { },
    "id_str" : "159274395985121280",
    "text" : "S'pore is \"globalization for early adopters\", says Swede http:\/\/t.co\/Jiutxn9a",
    "id" : 159274395985121280,
    "created_at" : "2012-01-17 14:02:29 +0000",
    "user" : {
      "name" : "Callan Tham",
      "screen_name" : "ctham",
      "protected" : false,
      "id_str" : "23011555",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2612846065\/zxfg4dpk8klhp6jgkpir_normal.jpeg",
      "id" : 23011555,
      "verified" : false
    }
  },
  "id" : 159275421416955904,
  "created_at" : "2012-01-17 14:06:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Australian",
      "screen_name" : "australian",
      "indices" : [ 3, 14 ],
      "id_str" : "9609632",
      "id" : 9609632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159187389720444928",
  "text" : "RT @australian: ANZ axes 130 jobs: ANZ Bank today kicked off a new round of job cuts amid tough domestic and global economic con... http ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitterfeed.com\" rel=\"nofollow\"\u003Etwitterfeed\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 136 ],
        "url" : "http:\/\/t.co\/TOAATESS",
        "expanded_url" : "http:\/\/bit.ly\/y3u8PA",
        "display_url" : "bit.ly\/y3u8PA"
      } ]
    },
    "geo" : { },
    "id_str" : "159148945350733824",
    "text" : "ANZ axes 130 jobs: ANZ Bank today kicked off a new round of job cuts amid tough domestic and global economic con... http:\/\/t.co\/TOAATESS",
    "id" : 159148945350733824,
    "created_at" : "2012-01-17 05:44:00 +0000",
    "user" : {
      "name" : "The Australian",
      "screen_name" : "australian",
      "protected" : false,
      "id_str" : "9609632",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/446890190703845376\/9WGYE1xK_normal.png",
      "id" : 9609632,
      "verified" : true
    }
  },
  "id" : 159187389720444928,
  "created_at" : "2012-01-17 08:16:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 63, 76 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "159183800813887488",
  "text" : "Looking for a cooker hood filter\/replacement part. Anyone from #SMEcommunity supplying? It for home kitchen. Thanks",
  "id" : 159183800813887488,
  "created_at" : "2012-01-17 08:02:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "indices" : [ 3, 15 ],
      "id_str" : "14107081",
      "id" : 14107081
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 95 ],
      "url" : "http:\/\/t.co\/i0tlsFv9",
      "expanded_url" : "http:\/\/zite.to\/wqLDXy",
      "display_url" : "zite.to\/wqLDXy"
    } ]
  },
  "geo" : { },
  "id_str" : "159171376169758721",
  "text" : "RT @geekonomics: The downside of cloud computing: 4 reasons to think\u00A0twice http:\/\/t.co\/i0tlsFv9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.zite.com\/\" rel=\"nofollow\"\u003EZite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 78 ],
        "url" : "http:\/\/t.co\/i0tlsFv9",
        "expanded_url" : "http:\/\/zite.to\/wqLDXy",
        "display_url" : "zite.to\/wqLDXy"
      } ]
    },
    "geo" : { },
    "id_str" : "159164743280623616",
    "text" : "The downside of cloud computing: 4 reasons to think\u00A0twice http:\/\/t.co\/i0tlsFv9",
    "id" : 159164743280623616,
    "created_at" : "2012-01-17 06:46:46 +0000",
    "user" : {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "protected" : false,
      "id_str" : "14107081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815459302941093888\/HFWgUA6V_normal.jpg",
      "id" : 14107081,
      "verified" : false
    }
  },
  "id" : 159171376169758721,
  "created_at" : "2012-01-17 07:13:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Willison",
      "screen_name" : "simonw",
      "indices" : [ 3, 10 ],
      "id_str" : "12497",
      "id" : 12497
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "158970183703207936",
  "text" : "RT @simonw: 31 people cancelled their Open Source Scotland tickets because the organiser said WordPress wasn't the best CMS :\/ http:\/\/t. ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 135 ],
        "url" : "http:\/\/t.co\/L9Z9KbOv",
        "expanded_url" : "http:\/\/www.wptavern.com\/some-orgnizations-and-wordpress-just-dont-mix#comment-17486",
        "display_url" : "wptavern.com\/some-orgnizati\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "158933130923872256",
    "text" : "31 people cancelled their Open Source Scotland tickets because the organiser said WordPress wasn't the best CMS :\/ http:\/\/t.co\/L9Z9KbOv",
    "id" : 158933130923872256,
    "created_at" : "2012-01-16 15:26:25 +0000",
    "user" : {
      "name" : "Simon Willison",
      "screen_name" : "simonw",
      "protected" : false,
      "id_str" : "12497",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000261649705\/be9cc55e64014e6d7663c50d7cb9fc75_normal.jpeg",
      "id" : 12497,
      "verified" : true
    }
  },
  "id" : 158970183703207936,
  "created_at" : "2012-01-16 17:53:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "job",
      "indices" : [ 107, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 86, 106 ],
      "url" : "http:\/\/t.co\/ObkWZo9T",
      "expanded_url" : "http:\/\/bull.hn\/l\/B56E\/6",
      "display_url" : "bull.hn\/l\/B56E\/6"
    } ]
  },
  "geo" : { },
  "id_str" : "158935848417632257",
  "text" : "RT @enormous: Are you a good fit for this job? Data Mining Analyst in Dublin, Ireland http:\/\/t.co\/ObkWZo9T #job",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/erased227602.com\" rel=\"nofollow\"\u003Eerased227602\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "job",
        "indices" : [ 93, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 72, 92 ],
        "url" : "http:\/\/t.co\/ObkWZo9T",
        "expanded_url" : "http:\/\/bull.hn\/l\/B56E\/6",
        "display_url" : "bull.hn\/l\/B56E\/6"
      } ]
    },
    "geo" : { },
    "id_str" : "158930580434722818",
    "text" : "Are you a good fit for this job? Data Mining Analyst in Dublin, Ireland http:\/\/t.co\/ObkWZo9T #job",
    "id" : 158930580434722818,
    "created_at" : "2012-01-16 15:16:17 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 158935848417632257,
  "created_at" : "2012-01-16 15:37:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "158876025856606208",
  "text" : "User get around the require name field by pressing the spacebar. How do you fix this?",
  "id" : 158876025856606208,
  "created_at" : "2012-01-16 11:39:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marina de Joslin",
      "screen_name" : "dejoslin",
      "indices" : [ 0, 9 ],
      "id_str" : "19356932",
      "id" : 19356932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "158867393198821376",
  "geo" : { },
  "id_str" : "158875564298608641",
  "in_reply_to_user_id" : 19356932,
  "text" : "@dejoslin congratulations!",
  "id" : 158875564298608641,
  "in_reply_to_status_id" : 158867393198821376,
  "created_at" : "2012-01-16 11:37:40 +0000",
  "in_reply_to_screen_name" : "dejoslin",
  "in_reply_to_user_id_str" : "19356932",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 62 ],
      "url" : "http:\/\/t.co\/Bx9tCSwS",
      "expanded_url" : "http:\/\/superweek.hu\/en\/",
      "display_url" : "superweek.hu\/en\/"
    } ]
  },
  "geo" : { },
  "id_str" : "158869564464185344",
  "text" : "I wish that this event is held in Ireland http:\/\/t.co\/Bx9tCSwS",
  "id" : 158869564464185344,
  "created_at" : "2012-01-16 11:13:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http:\/\/t.co\/yunwWF4p",
      "expanded_url" : "http:\/\/thenextweb.com\/apple\/2012\/01\/13\/mobile-operators-in-singapore-may-soon-sell-camera-less-iphone-4s-models\/",
      "display_url" : "thenextweb.com\/apple\/2012\/01\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "158809338239139840",
  "text" : "iPhone 4s without camera offered in Singapore  http:\/\/t.co\/yunwWF4p",
  "id" : 158809338239139840,
  "created_at" : "2012-01-16 07:14:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 94 ],
      "url" : "http:\/\/t.co\/Ii1tGDcp",
      "expanded_url" : "http:\/\/iwishthiswas.cc\/",
      "display_url" : "iwishthiswas.cc"
    } ]
  },
  "geo" : { },
  "id_str" : "158594134779768833",
  "text" : "Combining street art and urban planning, another way to collect feedback? http:\/\/t.co\/Ii1tGDcp",
  "id" : 158594134779768833,
  "created_at" : "2012-01-15 16:59:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 87 ],
      "url" : "http:\/\/t.co\/5ijMnQoD",
      "expanded_url" : "http:\/\/en.wikipedia.org\/wiki\/Bakkwa",
      "display_url" : "en.wikipedia.org\/wiki\/Bakkwa"
    } ]
  },
  "geo" : { },
  "id_str" : "158587214597586945",
  "text" : "For some of my twitter friend, Bakkwa is a Lunar New Year delicacy http:\/\/t.co\/5ijMnQoD",
  "id" : 158587214597586945,
  "created_at" : "2012-01-15 16:31:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ReplaceMovieTitlesWithBakKwa",
      "indices" : [ 16, 45 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "158582912567095296",
  "text" : "The BakKwa Lady #ReplaceMovieTitlesWithBakKwa",
  "id" : 158582912567095296,
  "created_at" : "2012-01-15 16:14:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Roy Cheong",
      "screen_name" : "roycheong1",
      "indices" : [ 0, 11 ],
      "id_str" : "20221897",
      "id" : 20221897
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "158346695606935552",
  "geo" : { },
  "id_str" : "158495484963590145",
  "in_reply_to_user_id" : 20221897,
  "text" : "@roycheong1 what about those traveling and need access to their fund?",
  "id" : 158495484963590145,
  "in_reply_to_status_id" : 158346695606935552,
  "created_at" : "2012-01-15 10:27:22 +0000",
  "in_reply_to_screen_name" : "roycheong1",
  "in_reply_to_user_id_str" : "20221897",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "158208856021925889",
  "text" : "Know your new user, return user, or even video user to your websites? Create a dashboard to highlight key Web Analytics metrics that matters",
  "id" : 158208856021925889,
  "created_at" : "2012-01-14 15:28:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Roy Cheong",
      "screen_name" : "roycheong1",
      "indices" : [ 0, 11 ],
      "id_str" : "20221897",
      "id" : 20221897
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Taiwan2012",
      "indices" : [ 56, 67 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "158119129306501120",
  "geo" : { },
  "id_str" : "158123080147927040",
  "in_reply_to_user_id" : 20221897,
  "text" : "@roycheong1 vote counting in Taiwan is really efficient #Taiwan2012",
  "id" : 158123080147927040,
  "in_reply_to_status_id" : 158119129306501120,
  "created_at" : "2012-01-14 09:47:34 +0000",
  "in_reply_to_screen_name" : "roycheong1",
  "in_reply_to_user_id_str" : "20221897",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 100 ],
      "url" : "http:\/\/t.co\/Y7cTYaMf",
      "expanded_url" : "http:\/\/amzn.to\/y4cyHn",
      "display_url" : "amzn.to\/y4cyHn"
    } ]
  },
  "geo" : { },
  "id_str" : "157892656012345344",
  "text" : "\"Law allows reasonable force defending home\" It time to do some Amazon shopping http:\/\/t.co\/Y7cTYaMf",
  "id" : 157892656012345344,
  "created_at" : "2012-01-13 18:31:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMECommunity",
      "indices" : [ 111, 124 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 110 ],
      "url" : "http:\/\/t.co\/mbAkPImX",
      "expanded_url" : "http:\/\/bit.ly\/zDGBAy",
      "display_url" : "bit.ly\/zDGBAy"
    } ]
  },
  "geo" : { },
  "id_str" : "157890991435681793",
  "text" : "Use this FREE tool to easily create the required tag for advanced Google Analytics setup. http:\/\/t.co\/mbAkPImX #SMECommunity",
  "id" : 157890991435681793,
  "created_at" : "2012-01-13 18:25:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Primary Position",
      "screen_name" : "primaryposition",
      "indices" : [ 3, 19 ],
      "id_str" : "17221943",
      "id" : 17221943
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 96 ],
      "url" : "http:\/\/t.co\/my3jTCxj",
      "expanded_url" : "http:\/\/www.youtube.com\/watch?feature=player_embedded&v=p4HYSsrlcq8",
      "display_url" : "youtube.com\/watch?feature=\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "157827160856465408",
  "text" : "RT @primaryposition \"Michael O'Leary - too bad we only produced one of him\" http:\/\/t.co\/my3jTCxj",
  "id" : 157827160856465408,
  "created_at" : "2012-01-13 14:11:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 122, 135 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 121 ],
      "url" : "http:\/\/t.co\/5VW1RQDa",
      "expanded_url" : "http:\/\/code.google.com\/apis\/analytics\/docs\/tracking\/gaTrackingSite.html#multipleDomains",
      "display_url" : "code.google.com\/apis\/analytics\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "157799015407300609",
  "text" : "Does your site spans multiple domains or subdomains? Use this guide to set up your Google Analytics. http:\/\/t.co\/5VW1RQDa #SMEcommunity",
  "id" : 157799015407300609,
  "created_at" : "2012-01-13 12:19:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joe Augustin",
      "screen_name" : "JoeAugustin",
      "indices" : [ 5, 17 ],
      "id_str" : "20788139",
      "id" : 20788139
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "157797142260498432",
  "text" : "lol \u201C@JoeAugustin: Auto spam.. PLEASE go and suck on the exhaust of a still running tractor! (Great, now I'll get spam about tractors).\u201D",
  "id" : 157797142260498432,
  "created_at" : "2012-01-13 12:12:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 120, 140 ],
      "url" : "http:\/\/t.co\/b9CeWuhn",
      "expanded_url" : "http:\/\/bit.ly\/xJ5EBe",
      "display_url" : "bit.ly\/xJ5EBe"
    } ]
  },
  "geo" : { },
  "id_str" : "157794054237716480",
  "text" : "Microsoft's motion-sensor Kinect came out of company's research lab in Beijing, away from company's day-to-day business http:\/\/t.co\/b9CeWuhn",
  "id" : 157794054237716480,
  "created_at" : "2012-01-13 12:00:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "indices" : [ 3, 16 ],
      "id_str" : "14826420",
      "id" : 14826420
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "157500494602969089",
  "text" : "RT @iamkelvinlee: Thief uses stolen iPad to take pictures of dog and friend, accidentally uploads to previous owner's iCloud.Doh!  http: ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 133 ],
        "url" : "http:\/\/t.co\/a9McMTzr",
        "expanded_url" : "http:\/\/yhoo.it\/xhU4tN",
        "display_url" : "yhoo.it\/xhU4tN"
      } ]
    },
    "geo" : { },
    "id_str" : "157483355166867458",
    "text" : "Thief uses stolen iPad to take pictures of dog and friend, accidentally uploads to previous owner's iCloud.Doh!  http:\/\/t.co\/a9McMTzr",
    "id" : 157483355166867458,
    "created_at" : "2012-01-12 15:25:32 +0000",
    "user" : {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "protected" : false,
      "id_str" : "14826420",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996378142682923010\/A8QOlqWt_normal.jpg",
      "id" : 14826420,
      "verified" : false
    }
  },
  "id" : 157500494602969089,
  "created_at" : "2012-01-12 16:33:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 75, 88 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 74 ],
      "url" : "http:\/\/t.co\/t6lL9k3",
      "expanded_url" : "http:\/\/bit.ly\/wzKVyz",
      "display_url" : "bit.ly\/wzKVyz"
    } ]
  },
  "geo" : { },
  "id_str" : "157453275837370368",
  "text" : "10-Point Check List to Setup Google Analytics Properly http:\/\/t.co\/t6lL9k3 #SMEcommunity",
  "id" : 157453275837370368,
  "created_at" : "2012-01-12 13:26:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aoife Rigney",
      "screen_name" : "aoiferigney",
      "indices" : [ 0, 12 ],
      "id_str" : "148496835",
      "id" : 148496835
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "157413530000310273",
  "geo" : { },
  "id_str" : "157442572959227904",
  "in_reply_to_user_id" : 148496835,
  "text" : "@aoiferigney You are welcome :)",
  "id" : 157442572959227904,
  "in_reply_to_status_id" : 157413530000310273,
  "created_at" : "2012-01-12 12:43:29 +0000",
  "in_reply_to_screen_name" : "aoiferigney",
  "in_reply_to_user_id_str" : "148496835",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "157438199243870210",
  "text" : "Do bank here offers safe deposit boxes? thks",
  "id" : 157438199243870210,
  "created_at" : "2012-01-12 12:26:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 3, 12 ],
      "id_str" : "19456433",
      "id" : 19456433
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 85 ],
      "url" : "http:\/\/t.co\/U3bGS2T8",
      "expanded_url" : "http:\/\/bbc.in\/ySeUiQ",
      "display_url" : "bbc.in\/ySeUiQ"
    } ]
  },
  "geo" : { },
  "id_str" : "157437622640320512",
  "text" : "RT @jonboyes: BBC News - Five ways the digital camera changed us http:\/\/t.co\/U3bGS2T8 - Hey BBC, you forgot \"getting stupid folk to send ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 51, 71 ],
        "url" : "http:\/\/t.co\/U3bGS2T8",
        "expanded_url" : "http:\/\/bbc.in\/ySeUiQ",
        "display_url" : "bbc.in\/ySeUiQ"
      } ]
    },
    "geo" : { },
    "id_str" : "157425175774760961",
    "text" : "BBC News - Five ways the digital camera changed us http:\/\/t.co\/U3bGS2T8 - Hey BBC, you forgot \"getting stupid folk to send us pics for free\"",
    "id" : 157425175774760961,
    "created_at" : "2012-01-12 11:34:21 +0000",
    "user" : {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "protected" : false,
      "id_str" : "19456433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3386817686\/4b1b7303154ea92ea270a5afec16da70_normal.jpeg",
      "id" : 19456433,
      "verified" : false
    }
  },
  "id" : 157437622640320512,
  "created_at" : "2012-01-12 12:23:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrick Culleton",
      "screen_name" : "cullydk",
      "indices" : [ 0, 8 ],
      "id_str" : "23039248",
      "id" : 23039248
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "157347669205987328",
  "geo" : { },
  "id_str" : "157374722777161728",
  "in_reply_to_user_id" : 23039248,
  "text" : "@cullydk Good morning!",
  "id" : 157374722777161728,
  "in_reply_to_status_id" : 157347669205987328,
  "created_at" : "2012-01-12 08:13:52 +0000",
  "in_reply_to_screen_name" : "cullydk",
  "in_reply_to_user_id_str" : "23039248",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ireland",
      "indices" : [ 66, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 76, 96 ],
      "url" : "http:\/\/t.co\/kfomEvEe",
      "expanded_url" : "http:\/\/www.webanalyticsdemystified.com\/ae\/index.asp",
      "display_url" : "webanalyticsdemystified.com\/ae\/index.asp"
    } ]
  },
  "geo" : { },
  "id_str" : "157229872073289728",
  "text" : "Free website analysis service for non-profits - Analysis Exchange #ireland  http:\/\/t.co\/kfomEvEe",
  "id" : 157229872073289728,
  "created_at" : "2012-01-11 22:38:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 0, 9 ],
      "id_str" : "19456433",
      "id" : 19456433
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "157172411056390145",
  "geo" : { },
  "id_str" : "157174380282122240",
  "in_reply_to_user_id" : 19456433,
  "text" : "@jonboyes I also got rated 'Capitalist roader'",
  "id" : 157174380282122240,
  "in_reply_to_status_id" : 157172411056390145,
  "created_at" : "2012-01-11 18:57:47 +0000",
  "in_reply_to_screen_name" : "jonboyes",
  "in_reply_to_user_id_str" : "19456433",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 87 ],
      "url" : "http:\/\/t.co\/WN7lygOM",
      "expanded_url" : "http:\/\/bbc.in\/xrnk7N",
      "display_url" : "bbc.in\/xrnk7N"
    }, {
      "indices" : [ 116, 136 ],
      "url" : "http:\/\/t.co\/npDATlnL",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/education-16493929?postId=111401304#comment_111401304",
      "display_url" : "bbc.co.uk\/news\/education\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "157143467343097857",
  "text" : "BBC News - School ICT to be replaced by computer science programme http:\/\/t.co\/WN7lygOM | I agree with this comment http:\/\/t.co\/npDATlnL",
  "id" : 157143467343097857,
  "created_at" : "2012-01-11 16:54:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yes thanks i am tryi",
      "screen_name" : "AmWorks",
      "indices" : [ 3, 11 ],
      "id_str" : "2160889140",
      "id" : 2160889140
    }, {
      "name" : "Yes thanks i am tryi",
      "screen_name" : "AmWorks",
      "indices" : [ 114, 122 ],
      "id_str" : "2160889140",
      "id" : 2160889140
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Jellyweek",
      "indices" : [ 103, 113 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "157070899890237441",
  "text" : "RT @amworks: Looking for a new way to work and meet interesting entrepreneurs & freelancers? Worldwide #Jellyweek @AMWorks - http:\/\/t.co ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yes thanks i am tryi",
        "screen_name" : "AmWorks",
        "indices" : [ 101, 109 ],
        "id_str" : "2160889140",
        "id" : 2160889140
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Jellyweek",
        "indices" : [ 90, 100 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 132 ],
        "url" : "http:\/\/t.co\/Smvqmxmh",
        "expanded_url" : "http:\/\/bit.ly\/ApI9lT",
        "display_url" : "bit.ly\/ApI9lT"
      } ]
    },
    "geo" : { },
    "id_str" : "157059432029761537",
    "text" : "Looking for a new way to work and meet interesting entrepreneurs & freelancers? Worldwide #Jellyweek @AMWorks - http:\/\/t.co\/Smvqmxmh",
    "id" : 157059432029761537,
    "created_at" : "2012-01-11 11:21:01 +0000",
    "user" : {
      "name" : "Alan Richardson",
      "screen_name" : "alanrichardsonI",
      "protected" : false,
      "id_str" : "72272437",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3682145738\/a9f8e6e015d6d022e34448f961c1ca95_normal.jpeg",
      "id" : 72272437,
      "verified" : false
    }
  },
  "id" : 157070899890237441,
  "created_at" : "2012-01-11 12:06:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "157009051459395584",
  "text" : "@joeljoshuagoh which is your fav place? (excluding Singapore)",
  "id" : 157009051459395584,
  "created_at" : "2012-01-11 08:00:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 4, 12 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 34 ],
      "url" : "http:\/\/t.co\/yccj0WBz",
      "expanded_url" : "http:\/\/mrbrwn.co\/wqc2U8",
      "display_url" : "mrbrwn.co\/wqc2U8"
    } ]
  },
  "geo" : { },
  "id_str" : "157008364763754498",
  "text" : "MT \u201C@mrbrown: http:\/\/t.co\/yccj0WBz \"The Straits Times has always been the gold standard in journalism.\"\n\n - Self praise is not praise.",
  "id" : 157008364763754498,
  "created_at" : "2012-01-11 07:58:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 3, 19 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156840225862189056",
  "text" : "RT @interactivemark: \u201CWhat do you mean an Indesign file doesn\u2019t work for a rich media banner? Just flash it.\u201D Effin creatives http:\/\/t.c ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 105, 125 ],
        "url" : "http:\/\/t.co\/wEuYxxRp",
        "expanded_url" : "http:\/\/fuckingcreatives.tumblr.com\/",
        "display_url" : "fuckingcreatives.tumblr.com"
      } ]
    },
    "geo" : { },
    "id_str" : "156838119587586048",
    "text" : "\u201CWhat do you mean an Indesign file doesn\u2019t work for a rich media banner? Just flash it.\u201D Effin creatives http:\/\/t.co\/wEuYxxRp",
    "id" : 156838119587586048,
    "created_at" : "2012-01-10 20:41:36 +0000",
    "user" : {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "protected" : false,
      "id_str" : "14340736",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974702103984791552\/eHtIZ2ef_normal.jpg",
      "id" : 14340736,
      "verified" : false
    }
  },
  "id" : 156840225862189056,
  "created_at" : "2012-01-10 20:49:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Interaction Lab",
      "screen_name" : "cinteractionlab",
      "indices" : [ 3, 19 ],
      "id_str" : "26516416",
      "id" : 26516416
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ux",
      "indices" : [ 87, 90 ]
    } ],
    "urls" : [ {
      "indices" : [ 66, 86 ],
      "url" : "http:\/\/t.co\/MItRGYFM",
      "expanded_url" : "http:\/\/bit.ly\/xJu6KZ",
      "display_url" : "bit.ly\/xJu6KZ"
    } ]
  },
  "geo" : { },
  "id_str" : "156697574470983680",
  "text" : "RT @cinteractionlab: Seven UX Best Practices of Community Design  http:\/\/t.co\/MItRGYFM #ux",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ux",
        "indices" : [ 66, 69 ]
      } ],
      "urls" : [ {
        "indices" : [ 45, 65 ],
        "url" : "http:\/\/t.co\/MItRGYFM",
        "expanded_url" : "http:\/\/bit.ly\/xJu6KZ",
        "display_url" : "bit.ly\/xJu6KZ"
      } ]
    },
    "geo" : { },
    "id_str" : "156695133537058816",
    "text" : "Seven UX Best Practices of Community Design  http:\/\/t.co\/MItRGYFM #ux",
    "id" : 156695133537058816,
    "created_at" : "2012-01-10 11:13:25 +0000",
    "user" : {
      "name" : "Interaction Lab",
      "screen_name" : "cinteractionlab",
      "protected" : false,
      "id_str" : "26516416",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/456465788921073667\/uU9ugPcv_normal.jpeg",
      "id" : 26516416,
      "verified" : false
    }
  },
  "id" : 156697574470983680,
  "created_at" : "2012-01-10 11:23:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156693210176036864",
  "text" : "Put my name down for a tea reception hosted by Singapore Consulate in Dublin - wonder they serve Teh si ping or not",
  "id" : 156693210176036864,
  "created_at" : "2012-01-10 11:05:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nino Leitner",
      "screen_name" : "NinoLeitner",
      "indices" : [ 3, 15 ],
      "id_str" : "23105968",
      "id" : 23105968
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156679848306286592",
  "text" : "RT @NinoLeitner: Looking for multicultural group of people, age ~ 17-25, for shoot in Vienna (1 day) next week, with budget :) email off ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "156366360610476032",
    "text" : "Looking for multicultural group of people, age ~ 17-25, for shoot in Vienna (1 day) next week, with budget :) email office[at]ninofilm.net",
    "id" : 156366360610476032,
    "created_at" : "2012-01-09 13:27:00 +0000",
    "user" : {
      "name" : "Nino Leitner",
      "screen_name" : "NinoLeitner",
      "protected" : false,
      "id_str" : "23105968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/616214352325136384\/H7BPrZzB_normal.jpg",
      "id" : 23105968,
      "verified" : false
    }
  },
  "id" : 156679848306286592,
  "created_at" : "2012-01-10 10:12:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yes4G",
      "screen_name" : "Yes4G",
      "indices" : [ 3, 9 ],
      "id_str" : "102576922",
      "id" : 102576922
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156674160037347328",
  "text" : "RT @Yes4G: Gong Xi \u201CFast\u201D Cai! from the fastest mobile internet with voice. The first 40 to RT this to get 5 exclusive Yes4G ang pow pac ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "156673563905097728",
    "text" : "Gong Xi \u201CFast\u201D Cai! from the fastest mobile internet with voice. The first 40 to RT this to get 5 exclusive Yes4G ang pow packets each",
    "id" : 156673563905097728,
    "created_at" : "2012-01-10 09:47:43 +0000",
    "user" : {
      "name" : "Yes4G",
      "screen_name" : "Yes4G",
      "protected" : false,
      "id_str" : "102576922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/934978173703413761\/7v4UJ2kE_normal.jpg",
      "id" : 102576922,
      "verified" : false
    }
  },
  "id" : 156674160037347328,
  "created_at" : "2012-01-10 09:50:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Crystal",
      "screen_name" : "BornInBeijing",
      "indices" : [ 3, 17 ],
      "id_str" : "19674275",
      "id" : 19674275
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156517175589486592",
  "text" : "RT @BornInBeijing: GOOGLE:\"I have everything!\" FACEBOOK: \"I know everybody!\" INTERNET: \"Without me, y'all are nothing.\" ELECTRICITY: \"Ke ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "156514904357089280",
    "text" : "GOOGLE:\"I have everything!\" FACEBOOK: \"I know everybody!\" INTERNET: \"Without me, y'all are nothing.\" ELECTRICITY: \"Keep talking, bitches.\"",
    "id" : 156514904357089280,
    "created_at" : "2012-01-09 23:17:15 +0000",
    "user" : {
      "name" : "Crystal",
      "screen_name" : "BornInBeijing",
      "protected" : false,
      "id_str" : "19674275",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000788922078\/86d5262a32ca92a832366379fb051ce7_normal.jpeg",
      "id" : 19674275,
      "verified" : false
    }
  },
  "id" : 156517175589486592,
  "created_at" : "2012-01-09 23:26:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tv",
      "indices" : [ 59, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156497447445741569",
  "text" : "Michael Mcintyre on Google Earth and modern technology LOL #tv",
  "id" : 156497447445741569,
  "created_at" : "2012-01-09 22:07:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hugo Dixon",
      "screen_name" : "Hugodixon",
      "indices" : [ 3, 13 ],
      "id_str" : "288909699",
      "id" : 288909699
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 104 ],
      "url" : "http:\/\/t.co\/LtLhkDU1",
      "expanded_url" : "http:\/\/reut.rs\/yzCnv1",
      "display_url" : "reut.rs\/yzCnv1"
    } ]
  },
  "geo" : { },
  "id_str" : "156493955012374528",
  "text" : "RT @Hugodixon: Enough austerity; it's time for reform. My latest column on the euro http:\/\/t.co\/LtLhkDU1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 89 ],
        "url" : "http:\/\/t.co\/LtLhkDU1",
        "expanded_url" : "http:\/\/reut.rs\/yzCnv1",
        "display_url" : "reut.rs\/yzCnv1"
      } ]
    },
    "geo" : { },
    "id_str" : "156331626882793472",
    "text" : "Enough austerity; it's time for reform. My latest column on the euro http:\/\/t.co\/LtLhkDU1",
    "id" : 156331626882793472,
    "created_at" : "2012-01-09 11:08:59 +0000",
    "user" : {
      "name" : "Hugo Dixon",
      "screen_name" : "Hugodixon",
      "protected" : false,
      "id_str" : "288909699",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/673148051268182016\/YanJ8g70_normal.jpg",
      "id" : 288909699,
      "verified" : false
    }
  },
  "id" : 156493955012374528,
  "created_at" : "2012-01-09 21:54:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TV",
      "indices" : [ 27, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156491830333161473",
  "text" : "love Live at the Apollo on #TV",
  "id" : 156491830333161473,
  "created_at" : "2012-01-09 21:45:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 126 ],
      "url" : "http:\/\/t.co\/HBMzdMpJ",
      "expanded_url" : "http:\/\/www.spiegel.de\/international\/world\/0,1518,808044,00.html",
      "display_url" : "spiegel.de\/international\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "156478396954718208",
  "text" : "Europe is socialist, bloated & threat to the global economy - message from the US presidential campaign.  http:\/\/t.co\/HBMzdMpJ",
  "id" : 156478396954718208,
  "created_at" : "2012-01-09 20:52:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 67 ],
      "url" : "http:\/\/t.co\/AKsSrw3P",
      "expanded_url" : "http:\/\/www.contentmarketinginstitute.com\/2012\/01\/new-breed-content-marketing-engineer\/",
      "display_url" : "contentmarketinginstitute.com\/2012\/01\/new-br\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "156428925650743296",
  "text" : "Content Marketing \u201CEngineer\u201D?! What a title... http:\/\/t.co\/AKsSrw3P",
  "id" : 156428925650743296,
  "created_at" : "2012-01-09 17:35:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156420374664458240",
  "text" : "RT @SimonPRepublic: Ha! - FG councillor removes wife's Nazi salute honeymoon picture from Facebook page - National News - Independent.ie ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 137 ],
        "url" : "http:\/\/t.co\/3muWTW6C",
        "expanded_url" : "http:\/\/shar.es\/WxF3p",
        "display_url" : "shar.es\/WxF3p"
      } ]
    },
    "geo" : { },
    "id_str" : "156419344295919616",
    "text" : "Ha! - FG councillor removes wife's Nazi salute honeymoon picture from Facebook page - National News - Independent.ie http:\/\/t.co\/3muWTW6C",
    "id" : 156419344295919616,
    "created_at" : "2012-01-09 16:57:32 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 156420374664458240,
  "created_at" : "2012-01-09 17:01:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156420196855320577",
  "text" : "RT @laokalaoka: \u521A\u770B\u5230\u7684\u4E00\u4E2A\u7B7E\u540D\uFF1A\u798F\u5982\u4E2D\u5357\u6D77\uFF0C\u5BFF\u6BD4\u516B\u5B9D\u5C71\u3002",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/i.fantui.net\" rel=\"nofollow\"\u003Ei\u996D\u63A8\u00B7\u7C89\u4E1D\u7684\u624B\u673A\u63A8\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "156389533909401602",
    "text" : "\u521A\u770B\u5230\u7684\u4E00\u4E2A\u7B7E\u540D\uFF1A\u798F\u5982\u4E2D\u5357\u6D77\uFF0C\u5BFF\u6BD4\u516B\u5B9D\u5C71\u3002",
    "id" : 156389533909401602,
    "created_at" : "2012-01-09 14:59:05 +0000",
    "user" : {
      "name" : "\u8001\u5361",
      "screen_name" : "laoka01",
      "protected" : false,
      "id_str" : "49960188",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/568562696140754944\/GM7J15T7_normal.jpeg",
      "id" : 49960188,
      "verified" : false
    }
  },
  "id" : 156420196855320577,
  "created_at" : "2012-01-09 17:00:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DCUBS",
      "screen_name" : "DCUBS",
      "indices" : [ 80, 86 ],
      "id_str" : "957926752885538817",
      "id" : 957926752885538817
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SMEcommunity",
      "indices" : [ 63, 76 ]
    } ],
    "urls" : [ {
      "indices" : [ 41, 61 ],
      "url" : "http:\/\/t.co\/feASeW7p",
      "expanded_url" : "http:\/\/bit.ly\/xCNw78",
      "display_url" : "bit.ly\/xCNw78"
    } ]
  },
  "geo" : { },
  "id_str" : "156372120388112384",
  "text" : "FREE social media tactical plan template http:\/\/t.co\/feASeW7p  #SMEcommunity HT @DCUBS",
  "id" : 156372120388112384,
  "created_at" : "2012-01-09 13:49:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156166982750846976",
  "text" : "Can anyone recommend coffee shop with free (working) wifi in Dublin? Thks",
  "id" : 156166982750846976,
  "created_at" : "2012-01-09 00:14:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 16, 22 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156163832170020864",
  "text" : "RT @Clearpreso: @mryap Was \"Managing Director\", I genuinely think next batch will say \"Chief Powerpoint-er\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "156161369547345920",
    "geo" : { },
    "id_str" : "156161941985959937",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap Was \"Managing Director\", I genuinely think next batch will say \"Chief Powerpoint-er\"",
    "id" : 156161941985959937,
    "in_reply_to_status_id" : 156161369547345920,
    "created_at" : "2012-01-08 23:54:42 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 156163832170020864,
  "created_at" : "2012-01-09 00:02:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "156161369547345920",
  "text" : "For self-employ people, what do you call yourself on the business card?",
  "id" : 156161369547345920,
  "created_at" : "2012-01-08 23:52:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gady Epstein",
      "screen_name" : "gadyepstein",
      "indices" : [ 3, 15 ],
      "id_str" : "24674124",
      "id" : 24674124
    }, {
      "name" : "Barbara Demick",
      "screen_name" : "BarbaraDemick",
      "indices" : [ 90, 104 ],
      "id_str" : "27571535",
      "id" : 27571535
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 125 ],
      "url" : "http:\/\/t.co\/bk2bfGnY",
      "expanded_url" : "http:\/\/www.latimes.com\/news\/nationworld\/world\/la-fg-china-corruption-on-wheels-20120108,0,4555295.story",
      "display_url" : "latimes.com\/news\/nationwor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "156084621858177024",
  "text" : "RT @gadyepstein: \"Corruption on wheels\" \u2014 Chinese government's Bentlleys, Maseratis... by @BarbaraDemick http:\/\/t.co\/bk2bfGnY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Barbara Demick",
        "screen_name" : "BarbaraDemick",
        "indices" : [ 73, 87 ],
        "id_str" : "27571535",
        "id" : 27571535
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 108 ],
        "url" : "http:\/\/t.co\/bk2bfGnY",
        "expanded_url" : "http:\/\/www.latimes.com\/news\/nationworld\/world\/la-fg-china-corruption-on-wheels-20120108,0,4555295.story",
        "display_url" : "latimes.com\/news\/nationwor\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "156045876224196608",
    "text" : "\"Corruption on wheels\" \u2014 Chinese government's Bentlleys, Maseratis... by @BarbaraDemick http:\/\/t.co\/bk2bfGnY",
    "id" : 156045876224196608,
    "created_at" : "2012-01-08 16:13:30 +0000",
    "user" : {
      "name" : "Gady Epstein",
      "screen_name" : "gadyepstein",
      "protected" : false,
      "id_str" : "24674124",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1234593929\/gadyepstein_headshot2_normal.jpg",
      "id" : 24674124,
      "verified" : true
    }
  },
  "id" : 156084621858177024,
  "created_at" : "2012-01-08 18:47:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philippe Legrain",
      "screen_name" : "plegrain",
      "indices" : [ 3, 12 ],
      "id_str" : "118680576",
      "id" : 118680576
    }, {
      "name" : "William Easterly",
      "screen_name" : "bill_easterly",
      "indices" : [ 121, 135 ],
      "id_str" : "35810531",
      "id" : 35810531
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 117 ],
      "url" : "http:\/\/t.co\/xN3jfGwf",
      "expanded_url" : "http:\/\/twitpic.com\/84gr00",
      "display_url" : "twitpic.com\/84gr00"
    } ]
  },
  "geo" : { },
  "id_str" : "156016770342334464",
  "text" : "RT @plegrain: In every country, people vastly overestimate the immigrant share of the population http:\/\/t.co\/xN3jfGwf HT @bill_easterly",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "William Easterly",
        "screen_name" : "bill_easterly",
        "indices" : [ 107, 121 ],
        "id_str" : "35810531",
        "id" : 35810531
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 103 ],
        "url" : "http:\/\/t.co\/xN3jfGwf",
        "expanded_url" : "http:\/\/twitpic.com\/84gr00",
        "display_url" : "twitpic.com\/84gr00"
      } ]
    },
    "geo" : { },
    "id_str" : "155977075415724033",
    "text" : "In every country, people vastly overestimate the immigrant share of the population http:\/\/t.co\/xN3jfGwf HT @bill_easterly",
    "id" : 155977075415724033,
    "created_at" : "2012-01-08 11:40:07 +0000",
    "user" : {
      "name" : "Philippe Legrain",
      "screen_name" : "plegrain",
      "protected" : false,
      "id_str" : "118680576",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882952723901149184\/NWu-Czta_normal.jpg",
      "id" : 118680576,
      "verified" : false
    }
  },
  "id" : 156016770342334464,
  "created_at" : "2012-01-08 14:17:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155694179673063425",
  "text" : "What the best sauce to go with lightly fried shredded white cabbages (not baggages!), carrot and onion? Thks",
  "id" : 155694179673063425,
  "created_at" : "2012-01-07 16:55:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155693643573891072",
  "text" : "Anyone here still on xmas\/new year break?",
  "id" : 155693643573891072,
  "created_at" : "2012-01-07 16:53:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155691023241191426",
  "text" : "\u4E00\u4F4D\u4FA8\u636E\u6D77\u5916\u591A\u5E74\u7684\u65B0\u52A0\u5761\u4EBA\u60F3\u643A\u5BB6\u4EBA\u56DE\u56FD\uFF0C\u88F9\u8DB3\u4E0D\u524D\u56E0\u4E3A\u5BF9\u5979\u800C\u8A00\u65B0\u52A0\u5761\u8FD1\u5E74\u6765\uFF0C\u5916\u52B3\u5267\u5897\u3002\u5979\u5FD8\u4E86\u5728\u6B27\u6D32\u5F53\u5730\u4EBA\u7684\u773C\u4E2D\uFF0C\u5979\u4E5F\u6630\u5916\u52B3\u3002",
  "id" : 155691023241191426,
  "created_at" : "2012-01-07 16:43:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Courtney Carver",
      "screen_name" : "bemorewithless",
      "indices" : [ 0, 15 ],
      "id_str" : "134207471",
      "id" : 134207471
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http:\/\/t.co\/6Wt1Rs6R",
      "expanded_url" : "http:\/\/fthrwght.com\/autofocus\/",
      "display_url" : "fthrwght.com\/autofocus\/"
    } ]
  },
  "in_reply_to_status_id_str" : "155404508468482048",
  "geo" : { },
  "id_str" : "155450759847227392",
  "in_reply_to_user_id" : 134207471,
  "text" : "@bemorewithless It AutoFocus 2.0 Wordpress Theme http:\/\/t.co\/6Wt1Rs6R",
  "id" : 155450759847227392,
  "in_reply_to_status_id" : 155404508468482048,
  "created_at" : "2012-01-07 00:48:43 +0000",
  "in_reply_to_screen_name" : "bemorewithless",
  "in_reply_to_user_id_str" : "134207471",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "indices" : [ 3, 19 ],
      "id_str" : "50343375",
      "id" : 50343375
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 102 ],
      "url" : "http:\/\/t.co\/sdbKoIwT",
      "expanded_url" : "http:\/\/econ.st\/rxjmLa",
      "display_url" : "econ.st\/rxjmLa"
    } ]
  },
  "geo" : { },
  "id_str" : "155353936696590336",
  "text" : "RT @superfinefeline: Singapore politics: Falling on their wallets | The Economist http:\/\/t.co\/sdbKoIwT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 81 ],
        "url" : "http:\/\/t.co\/sdbKoIwT",
        "expanded_url" : "http:\/\/econ.st\/rxjmLa",
        "display_url" : "econ.st\/rxjmLa"
      } ]
    },
    "geo" : { },
    "id_str" : "155344017473806336",
    "text" : "Singapore politics: Falling on their wallets | The Economist http:\/\/t.co\/sdbKoIwT",
    "id" : 155344017473806336,
    "created_at" : "2012-01-06 17:44:34 +0000",
    "user" : {
      "name" : "Karen Wee",
      "screen_name" : "superfinefeline",
      "protected" : false,
      "id_str" : "50343375",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978593788175306752\/Z467RtZM_normal.jpg",
      "id" : 50343375,
      "verified" : false
    }
  },
  "id" : 155353936696590336,
  "created_at" : "2012-01-06 18:23:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 91 ],
      "url" : "http:\/\/t.co\/YlhJjsAl",
      "expanded_url" : "http:\/\/www.straitstimes.com\/BreakingNews\/Singapore\/Story\/STIStory_751638.html",
      "display_url" : "straitstimes.com\/BreakingNews\/S\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "155348561108799488",
  "text" : "Epic fail!  Domain name not renew lead to inaccessible govt services.  http:\/\/t.co\/YlhJjsAl",
  "id" : 155348561108799488,
  "created_at" : "2012-01-06 18:02:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155265890013806593",
  "text" : "Looking for a driving instructor in Dublin. Any recommendation? Thks",
  "id" : 155265890013806593,
  "created_at" : "2012-01-06 12:34:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155264515695591425",
  "text" : "CEO stepped down after Singapore\u2019s worst subway disruptions on record.",
  "id" : 155264515695591425,
  "created_at" : "2012-01-06 12:28:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 91 ],
      "url" : "http:\/\/t.co\/oe0AxCeC",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b019873g",
      "display_url" : "bbc.co.uk\/programmes\/b01\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "155045061615292417",
  "text" : "We feel we are \"normal\" parent after watching this documentary on BBC2 http:\/\/t.co\/oe0AxCeC",
  "id" : 155045061615292417,
  "created_at" : "2012-01-05 21:56:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bbc2",
      "indices" : [ 64, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155042182087196672",
  "text" : "\"we don't have safety net...we have to work hard\" - a tiger mum #bbc2",
  "id" : 155042182087196672,
  "created_at" : "2012-01-05 21:45:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "155031399156551681",
  "text" : "Meet Britain's own Tiger Mum on BBC 2",
  "id" : 155031399156551681,
  "created_at" : "2012-01-05 21:02:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "indices" : [ 3, 15 ],
      "id_str" : "14107081",
      "id" : 14107081
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 108 ],
      "url" : "http:\/\/t.co\/uWXvEX2A",
      "expanded_url" : "http:\/\/zite.to\/A5W51w",
      "display_url" : "zite.to\/A5W51w"
    } ]
  },
  "geo" : { },
  "id_str" : "154708110861275136",
  "text" : "RT @geekonomics: Cost-cutting Nokia to move its Asia Pacific HQ from Singapore to China http:\/\/t.co\/uWXvEX2A",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.zite.com\/\" rel=\"nofollow\"\u003EZite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 91 ],
        "url" : "http:\/\/t.co\/uWXvEX2A",
        "expanded_url" : "http:\/\/zite.to\/A5W51w",
        "display_url" : "zite.to\/A5W51w"
      } ]
    },
    "geo" : { },
    "id_str" : "154706693073276928",
    "text" : "Cost-cutting Nokia to move its Asia Pacific HQ from Singapore to China http:\/\/t.co\/uWXvEX2A",
    "id" : 154706693073276928,
    "created_at" : "2012-01-04 23:32:04 +0000",
    "user" : {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "protected" : false,
      "id_str" : "14107081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815459302941093888\/HFWgUA6V_normal.jpg",
      "id" : 14107081,
      "verified" : false
    }
  },
  "id" : 154708110861275136,
  "created_at" : "2012-01-04 23:37:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dorothy Ryan",
      "screen_name" : "Dotwebs",
      "indices" : [ 3, 11 ],
      "id_str" : "16984046",
      "id" : 16984046
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 65 ],
      "url" : "http:\/\/t.co\/fyp2XVDd",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/technology-16408850",
      "display_url" : "bbc.co.uk\/news\/technolog\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "154689482346610688",
  "text" : "RT @Dotwebs: Farewell  IE6 it's been a blast http:\/\/t.co\/fyp2XVDd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 52 ],
        "url" : "http:\/\/t.co\/fyp2XVDd",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/technology-16408850",
        "display_url" : "bbc.co.uk\/news\/technolog\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "154673695942639616",
    "text" : "Farewell  IE6 it's been a blast http:\/\/t.co\/fyp2XVDd",
    "id" : 154673695942639616,
    "created_at" : "2012-01-04 21:20:57 +0000",
    "user" : {
      "name" : "Dorothy Ryan",
      "screen_name" : "Dotwebs",
      "protected" : false,
      "id_str" : "16984046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/754655766515253248\/e1Qkb51Y_normal.jpg",
      "id" : 16984046,
      "verified" : false
    }
  },
  "id" : 154689482346610688,
  "created_at" : "2012-01-04 22:23:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "154631566516174848",
  "text" : "Someone put Steve Job biography under \"Dealing with difficult people\" section.",
  "id" : 154631566516174848,
  "created_at" : "2012-01-04 18:33:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003ESafari on iOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 101 ],
      "url" : "http:\/\/t.co\/rOCEGzuH",
      "expanded_url" : "http:\/\/2012.photoireland.org\/intro\/",
      "display_url" : "2012.photoireland.org\/intro\/"
    } ]
  },
  "geo" : { },
  "id_str" : "154623784203198465",
  "text" : "Theme of 2012 PhotoIreland Festival : Migrations, Diaspora & Cultural Identity.  http:\/\/t.co\/rOCEGzuH",
  "id" : 154623784203198465,
  "created_at" : "2012-01-04 18:02:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Design Sojourn",
      "screen_name" : "designsojourn",
      "indices" : [ 3, 17 ],
      "id_str" : "3029921",
      "id" : 3029921
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 99 ],
      "url" : "http:\/\/t.co\/He4ErooO",
      "expanded_url" : "http:\/\/bit.ly\/wUM76s",
      "display_url" : "bit.ly\/wUM76s"
    } ]
  },
  "geo" : { },
  "id_str" : "154620543788785664",
  "text" : "RT @designsojourn: iPad App Teaches You How To Finally Master Your DSLR Camera http:\/\/t.co\/He4ErooO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.flipboard.com\" rel=\"nofollow\"\u003EFlipboard\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 80 ],
        "url" : "http:\/\/t.co\/He4ErooO",
        "expanded_url" : "http:\/\/bit.ly\/wUM76s",
        "display_url" : "bit.ly\/wUM76s"
      } ]
    },
    "geo" : { },
    "id_str" : "154613778896531456",
    "text" : "iPad App Teaches You How To Finally Master Your DSLR Camera http:\/\/t.co\/He4ErooO",
    "id" : 154613778896531456,
    "created_at" : "2012-01-04 17:22:52 +0000",
    "user" : {
      "name" : "Design Sojourn",
      "screen_name" : "designsojourn",
      "protected" : false,
      "id_str" : "3029921",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/911468983345324033\/55akIGM2_normal.jpg",
      "id" : 3029921,
      "verified" : false
    }
  },
  "id" : 154620543788785664,
  "created_at" : "2012-01-04 17:49:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "154512857004900352",
  "text" : "At Apple Store's Intro to iCloud workshop",
  "id" : 154512857004900352,
  "created_at" : "2012-01-04 10:41:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 3, 11 ],
      "id_str" : "1652541",
      "id" : 1652541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 92 ],
      "url" : "http:\/\/t.co\/YF1dwCEz",
      "expanded_url" : "http:\/\/reut.rs\/ADxzPg",
      "display_url" : "reut.rs\/ADxzPg"
    } ]
  },
  "geo" : { },
  "id_str" : "154463768519249921",
  "text" : "RT @Reuters: Singapore PM faces 36 pct pay cut, still world's best paid http:\/\/t.co\/YF1dwCEz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 79 ],
        "url" : "http:\/\/t.co\/YF1dwCEz",
        "expanded_url" : "http:\/\/reut.rs\/ADxzPg",
        "display_url" : "reut.rs\/ADxzPg"
      } ]
    },
    "geo" : { },
    "id_str" : "154458455246241792",
    "text" : "Singapore PM faces 36 pct pay cut, still world's best paid http:\/\/t.co\/YF1dwCEz",
    "id" : 154458455246241792,
    "created_at" : "2012-01-04 07:05:40 +0000",
    "user" : {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "protected" : false,
      "id_str" : "1652541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877554927932891136\/ZBEs235N_normal.jpg",
      "id" : 1652541,
      "verified" : true
    }
  },
  "id" : 154463768519249921,
  "created_at" : "2012-01-04 07:26:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "154275448661082112",
  "text" : "\u8001\u5A46\u53BB\u55E6\u51B0\u5927\u534A\u5929\uFF0C\u96BE\u5F97\u8033\u6735\u6DF8\u9759\u3002",
  "id" : 154275448661082112,
  "created_at" : "2012-01-03 18:58:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RupertMurdochPR",
      "screen_name" : "RupertMurdochPR",
      "indices" : [ 3, 19 ],
      "id_str" : "2541922537",
      "id" : 2541922537
    }, {
      "name" : "Wendi Deng",
      "screen_name" : "wendi_deng",
      "indices" : [ 97, 108 ],
      "id_str" : "456082417",
      "id" : 456082417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "154237291420848128",
  "text" : "RT @RupertMurdochPR: Twitter welcomed Rupert Murdoch by allowing someone to impersonate his wife @Wendi_Deng here for 3 days, with a 've ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Wendi Deng",
        "screen_name" : "wendi_deng",
        "indices" : [ 76, 87 ],
        "id_str" : "456082417",
        "id" : 456082417
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Oops",
        "indices" : [ 131, 136 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "154220359590219776",
    "text" : "Twitter welcomed Rupert Murdoch by allowing someone to impersonate his wife @Wendi_Deng here for 3 days, with a 'verified' account #Oops",
    "id" : 154220359590219776,
    "created_at" : "2012-01-03 15:19:33 +0000",
    "user" : {
      "name" : "Alt-Rupert",
      "screen_name" : "TheMurdochTimes",
      "protected" : false,
      "id_str" : "330378192",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/864761867226423297\/iNNuSHMd_normal.jpg",
      "id" : 330378192,
      "verified" : false
    }
  },
  "id" : 154237291420848128,
  "created_at" : "2012-01-03 16:26:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 88 ],
      "url" : "http:\/\/t.co\/JM7dUUs4",
      "expanded_url" : "http:\/\/tnw.to\/1CR3y",
      "display_url" : "tnw.to\/1CR3y"
    } ]
  },
  "geo" : { },
  "id_str" : "154209672004501505",
  "text" : "New law requires all restaurants in Malaysian city to provide Wi-Fi http:\/\/t.co\/JM7dUUs4 - Decent WC anytime over Wifi for me.",
  "id" : 154209672004501505,
  "created_at" : "2012-01-03 14:37:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernard Marr",
      "screen_name" : "BernardMarr",
      "indices" : [ 3, 15 ],
      "id_str" : "108286674",
      "id" : 108286674
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 69 ],
      "url" : "http:\/\/t.co\/5TYIZdyy",
      "expanded_url" : "http:\/\/ow.ly\/88j89",
      "display_url" : "ow.ly\/88j89"
    } ]
  },
  "geo" : { },
  "id_str" : "154207801537871872",
  "text" : "RT @BernardMarr: Preventing 'Analysis Paralysis' http:\/\/t.co\/5TYIZdyy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 52 ],
        "url" : "http:\/\/t.co\/5TYIZdyy",
        "expanded_url" : "http:\/\/ow.ly\/88j89",
        "display_url" : "ow.ly\/88j89"
      } ]
    },
    "geo" : { },
    "id_str" : "154185382395785216",
    "text" : "Preventing 'Analysis Paralysis' http:\/\/t.co\/5TYIZdyy",
    "id" : 154185382395785216,
    "created_at" : "2012-01-03 13:00:34 +0000",
    "user" : {
      "name" : "Bernard Marr",
      "screen_name" : "BernardMarr",
      "protected" : false,
      "id_str" : "108286674",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/801052732304097280\/_CHjX9Ke_normal.jpg",
      "id" : 108286674,
      "verified" : false
    }
  },
  "id" : 154207801537871872,
  "created_at" : "2012-01-03 14:29:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fred Caballero",
      "screen_name" : "fredchannel",
      "indices" : [ 4, 16 ],
      "id_str" : "16672823",
      "id" : 16672823
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 63 ],
      "url" : "http:\/\/t.co\/z6DOBKsa",
      "expanded_url" : "http:\/\/bit.ly\/tefPQc",
      "display_url" : "bit.ly\/tefPQc"
    } ]
  },
  "geo" : { },
  "id_str" : "154207468380098561",
  "text" : "+1 \u201C@fredchannel: R.I.P. Personal Branding http:\/\/t.co\/z6DOBKsa\u201D",
  "id" : 154207468380098561,
  "created_at" : "2012-01-03 14:28:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vicky Brock",
      "screen_name" : "brockvicky",
      "indices" : [ 3, 14 ],
      "id_str" : "14408201",
      "id" : 14408201
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cookies",
      "indices" : [ 49, 57 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "154204424984739840",
  "text" : "RT @brockvicky: What's frustrating about this EU #cookies argument is how cookies & behavioural targetting are seen as same thing: http: ...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "cookies",
        "indices" : [ 33, 41 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 135 ],
        "url" : "http:\/\/t.co\/Pvx518Tv",
        "expanded_url" : "http:\/\/bit.ly\/ujzCbl",
        "display_url" : "bit.ly\/ujzCbl"
      } ]
    },
    "geo" : { },
    "id_str" : "154194234352734208",
    "text" : "What's frustrating about this EU #cookies argument is how cookies & behavioural targetting are seen as same thing: http:\/\/t.co\/Pvx518Tv",
    "id" : 154194234352734208,
    "created_at" : "2012-01-03 13:35:44 +0000",
    "user" : {
      "name" : "Vicky Brock",
      "screen_name" : "brockvicky",
      "protected" : false,
      "id_str" : "14408201",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882591960766271488\/V1FLZf4l_normal.jpg",
      "id" : 14408201,
      "verified" : false
    }
  },
  "id" : 154204424984739840,
  "created_at" : "2012-01-03 14:16:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "153783306067001344",
  "text" : "Only when you are overseas, you will miss chinese new year song playing all over the Mall...",
  "id" : 153783306067001344,
  "created_at" : "2012-01-02 10:22:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "153594656494587906",
  "text" : "Harry Potter and Half Blood prince on all 4 UK channel at different time today.",
  "id" : 153594656494587906,
  "created_at" : "2012-01-01 21:53:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]