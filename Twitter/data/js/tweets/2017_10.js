Grailbird.data.tweets_2017_10 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925395837018083328",
  "text" : "Maybe I should build a Ubuntu-based VM on Windows then install &amp; run Docker there?",
  "id" : 925395837018083328,
  "created_at" : "2017-10-31 16:15:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "indices" : [ 3, 7 ],
      "id_str" : "380648579",
      "id" : 380648579
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/55dg3rn9P4",
      "expanded_url" : "http:\/\/u.afp.com\/4HSR",
      "display_url" : "u.afp.com\/4HSR"
    } ]
  },
  "geo" : { },
  "id_str" : "925394608493907969",
  "text" : "RT @AFP: More than '200 feared dead' as tunnel reportedly caves in at North Korea's nuclear test site https:\/\/t.co\/55dg3rn9P4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/55dg3rn9P4",
        "expanded_url" : "http:\/\/u.afp.com\/4HSR",
        "display_url" : "u.afp.com\/4HSR"
      } ]
    },
    "geo" : { },
    "id_str" : "925330299499708417",
    "text" : "More than '200 feared dead' as tunnel reportedly caves in at North Korea's nuclear test site https:\/\/t.co\/55dg3rn9P4",
    "id" : 925330299499708417,
    "created_at" : "2017-10-31 11:55:09 +0000",
    "user" : {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "protected" : false,
      "id_str" : "380648579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/697343883630481408\/c08JBfBB_normal.jpg",
      "id" : 380648579,
      "verified" : true
    }
  },
  "id" : 925394608493907969,
  "created_at" : "2017-10-31 16:10:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925394123242328064",
  "text" : "This morning at the supermarket, I overheard  \"Just don't open the door\" (this evening)",
  "id" : 925394123242328064,
  "created_at" : "2017-10-31 16:08:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Larry Kim",
      "screen_name" : "larrykim",
      "indices" : [ 3, 12 ],
      "id_str" : "17850785",
      "id" : 17850785
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925393647042023424",
  "text" : "RT @larrykim: in silicon valley failure is celebrated but usually from the safety of having achieved a later success. otherwise it's 2 stri\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "925392160979079170",
    "text" : "in silicon valley failure is celebrated but usually from the safety of having achieved a later success. otherwise it's 2 strikes you're out.",
    "id" : 925392160979079170,
    "created_at" : "2017-10-31 16:00:58 +0000",
    "user" : {
      "name" : "Larry Kim",
      "screen_name" : "larrykim",
      "protected" : false,
      "id_str" : "17850785",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/634035372733898752\/6aSBCDd9_normal.jpg",
      "id" : 17850785,
      "verified" : true
    }
  },
  "id" : 925393647042023424,
  "created_at" : "2017-10-31 16:06:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "indices" : [ 3, 13 ],
      "id_str" : "911413417",
      "id" : 911413417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925291075736137728",
  "text" : "RT @Dr_Draper: I'm old enough to remember when \"unicorns\" were just \"people with multiple skills\". Good times.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "925282871379574784",
    "text" : "I'm old enough to remember when \"unicorns\" were just \"people with multiple skills\". Good times.",
    "id" : 925282871379574784,
    "created_at" : "2017-10-31 08:46:42 +0000",
    "user" : {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "protected" : false,
      "id_str" : "911413417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2778032626\/f43efb67d690165e4c0003ca3fa2694b_normal.jpeg",
      "id" : 911413417,
      "verified" : false
    }
  },
  "id" : 925291075736137728,
  "created_at" : "2017-10-31 09:19:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/qZMaAO5Tso",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-41812909",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "925281065958346752",
  "text" : "RT @mrbrown: BBC: Singapore python tries to eat 'petrified' pet cockatoo https:\/\/t.co\/qZMaAO5Tso Good thing our Pudding lives in a high-ris\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/qZMaAO5Tso",
        "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-41812909",
        "display_url" : "bbc.com\/news\/world-asi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "925218254213210113",
    "text" : "BBC: Singapore python tries to eat 'petrified' pet cockatoo https:\/\/t.co\/qZMaAO5Tso Good thing our Pudding lives in a high-rise flat then.",
    "id" : 925218254213210113,
    "created_at" : "2017-10-31 04:29:56 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 925281065958346752,
  "created_at" : "2017-10-31 08:39:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Evil Tofu",
      "screen_name" : "eviltofu",
      "indices" : [ 3, 12 ],
      "id_str" : "1730901",
      "id" : 1730901
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/eviltofu\/status\/925223903282135040\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/dUVseb9nPN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNcOObEUEAAtNB3.jpg",
      "id_str" : "925223889394733056",
      "id" : 925223889394733056,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNcOObEUEAAtNB3.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dUVseb9nPN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925280516848447488",
  "text" : "RT @eviltofu: Train stopped before Tampines station. https:\/\/t.co\/dUVseb9nPN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/eviltofu\/status\/925223903282135040\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/dUVseb9nPN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNcOObEUEAAtNB3.jpg",
        "id_str" : "925223889394733056",
        "id" : 925223889394733056,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNcOObEUEAAtNB3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dUVseb9nPN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "925223903282135040",
    "text" : "Train stopped before Tampines station. https:\/\/t.co\/dUVseb9nPN",
    "id" : 925223903282135040,
    "created_at" : "2017-10-31 04:52:23 +0000",
    "user" : {
      "name" : "Evil Tofu",
      "screen_name" : "eviltofu",
      "protected" : false,
      "id_str" : "1730901",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/472334032592592896\/mEsxsh6f_normal.jpeg",
      "id" : 1730901,
      "verified" : false
    }
  },
  "id" : 925280516848447488,
  "created_at" : "2017-10-31 08:37:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marcus du Sautoy",
      "screen_name" : "MarcusduSautoy",
      "indices" : [ 3, 18 ],
      "id_str" : "22272053",
      "id" : 22272053
    }, {
      "name" : "BBC Four",
      "screen_name" : "BBCFOUR",
      "indices" : [ 109, 117 ],
      "id_str" : "3826680281",
      "id" : 3826680281
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925109426197094406",
  "text" : "RT @MarcusduSautoy: Hopefully an algorithm has already alerted you to the fact my programme on algorithms is @BBCFOUR tonight at 10:30 http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC Four",
        "screen_name" : "BBCFOUR",
        "indices" : [ 89, 97 ],
        "id_str" : "3826680281",
        "id" : 3826680281
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/Ibm6luhYJQ",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/p030s6b3",
        "display_url" : "bbc.co.uk\/programmes\/p03\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "925107073758715906",
    "text" : "Hopefully an algorithm has already alerted you to the fact my programme on algorithms is @BBCFOUR tonight at 10:30 https:\/\/t.co\/Ibm6luhYJQ",
    "id" : 925107073758715906,
    "created_at" : "2017-10-30 21:08:08 +0000",
    "user" : {
      "name" : "Marcus du Sautoy",
      "screen_name" : "MarcusduSautoy",
      "protected" : false,
      "id_str" : "22272053",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875435976847495168\/UejhOkd8_normal.jpg",
      "id" : 22272053,
      "verified" : false
    }
  },
  "id" : 925109426197094406,
  "created_at" : "2017-10-30 21:17:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "H L U",
      "screen_name" : "H_Lucke",
      "indices" : [ 3, 11 ],
      "id_str" : "135470429",
      "id" : 135470429
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Survols\/status\/914952269556322306\/video\/1",
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/uzDzCr9utj",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/914951645527724032\/pu\/img\/JQz95Nkds08SZz-f.jpg",
      "id_str" : "914951645527724032",
      "id" : 914951645527724032,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/914951645527724032\/pu\/img\/JQz95Nkds08SZz-f.jpg",
      "sizes" : [ {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uzDzCr9utj"
    } ],
    "hashtags" : [ {
      "text" : "aerodynamics",
      "indices" : [ 13, 26 ]
    }, {
      "text" : "avgeek",
      "indices" : [ 72, 79 ]
    }, {
      "text" : "aircraft",
      "indices" : [ 80, 89 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "925095093316538373",
  "text" : "RT @H_Lucke: #aerodynamics, not easy science... https:\/\/t.co\/uzDzCr9utj #avgeek #aircraft",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Survols\/status\/914952269556322306\/video\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/uzDzCr9utj",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/914951645527724032\/pu\/img\/JQz95Nkds08SZz-f.jpg",
        "id_str" : "914951645527724032",
        "id" : 914951645527724032,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/914951645527724032\/pu\/img\/JQz95Nkds08SZz-f.jpg",
        "sizes" : [ {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uzDzCr9utj"
      } ],
      "hashtags" : [ {
        "text" : "aerodynamics",
        "indices" : [ 0, 13 ]
      }, {
        "text" : "avgeek",
        "indices" : [ 59, 66 ]
      }, {
        "text" : "aircraft",
        "indices" : [ 67, 76 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "924403190082277376",
    "text" : "#aerodynamics, not easy science... https:\/\/t.co\/uzDzCr9utj #avgeek #aircraft",
    "id" : 924403190082277376,
    "created_at" : "2017-10-28 22:31:09 +0000",
    "user" : {
      "name" : "H L U",
      "screen_name" : "H_Lucke",
      "protected" : false,
      "id_str" : "135470429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/724191755306983424\/iwaQ4yrB_normal.jpg",
      "id" : 135470429,
      "verified" : false
    }
  },
  "id" : 925095093316538373,
  "created_at" : "2017-10-30 20:20:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/r0x8KhvplG",
      "expanded_url" : "http:\/\/po.st\/EWz2Yt",
      "display_url" : "po.st\/EWz2Yt"
    } ]
  },
  "geo" : { },
  "id_str" : "925039576418279424",
  "text" : "Electric trackless trains launched in China https:\/\/t.co\/r0x8KhvplG",
  "id" : 925039576418279424,
  "created_at" : "2017-10-30 16:39:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/tw379ncNWO",
      "expanded_url" : "https:\/\/twitter.com\/notetoshelf",
      "display_url" : "twitter.com\/notetoshelf"
    } ]
  },
  "in_reply_to_status_id_str" : "924995388444160001",
  "geo" : { },
  "id_str" : "924995653872300032",
  "in_reply_to_user_id" : 9465632,
  "text" : "Now I am using Twitter cos it can host image and I can access everywhere.  https:\/\/t.co\/tw379ncNWO",
  "id" : 924995653872300032,
  "in_reply_to_status_id" : 924995388444160001,
  "created_at" : "2017-10-30 13:45:24 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/Xf7Bl4L1K4",
      "expanded_url" : "https:\/\/support.microsoft.com\/en-us\/help\/260563\/how-to-use-notepad-to-create-a-log-file",
      "display_url" : "support.microsoft.com\/en-us\/help\/260\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "924995388444160001",
  "text" : "This the first trick my previous boss share with me https:\/\/t.co\/Xf7Bl4L1K4",
  "id" : 924995388444160001,
  "created_at" : "2017-10-30 13:44:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abortion Rights IE",
      "screen_name" : "freesafelegal",
      "indices" : [ 3, 17 ],
      "id_str" : "1096875997",
      "id" : 1096875997
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Savita",
      "indices" : [ 81, 88 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924936438352924672",
  "text" : "RT @freesafelegal: Yesterday all over Ireland, people gathered to pay tribute to #Savita on 5th anniv of her untimely death. we will never\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/freesafelegal\/status\/924735342380400640\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/nBtKplujVM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNVEUcEWAAEqViX.jpg",
        "id_str" : "924720416416989185",
        "id" : 924720416416989185,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNVEUcEWAAEqViX.jpg",
        "sizes" : [ {
          "h" : 960,
          "resize" : "fit",
          "w" : 642
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 455
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 642
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 642
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nBtKplujVM"
      } ],
      "hashtags" : [ {
        "text" : "Savita",
        "indices" : [ 62, 69 ]
      }, {
        "text" : "neveragain",
        "indices" : [ 128, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "924735342380400640",
    "text" : "Yesterday all over Ireland, people gathered to pay tribute to #Savita on 5th anniv of her untimely death. we will never forget. #neveragain https:\/\/t.co\/nBtKplujVM",
    "id" : 924735342380400640,
    "created_at" : "2017-10-29 20:31:01 +0000",
    "user" : {
      "name" : "Abortion Rights IE",
      "screen_name" : "freesafelegal",
      "protected" : false,
      "id_str" : "1096875997",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013839343872626688\/S0PvLQ6F_normal.jpg",
      "id" : 1096875997,
      "verified" : true
    }
  },
  "id" : 924936438352924672,
  "created_at" : "2017-10-30 09:50:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Telegraph",
      "screen_name" : "Telegraph",
      "indices" : [ 3, 13 ],
      "id_str" : "16343974",
      "id" : 16343974
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Telegraph\/status\/924327408509030400\/photo\/1",
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/oggeSRuPvb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNPct7MX4AA_SvL.jpg",
      "id_str" : "924325030082895872",
      "id" : 924325030082895872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNPct7MX4AA_SvL.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1367,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1806,
        "resize" : "fit",
        "w" : 2706
      }, {
        "h" : 801,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oggeSRuPvb"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924672849369059328",
  "text" : "RT @Telegraph: https:\/\/t.co\/oggeSRuPvb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Telegraph\/status\/924327408509030400\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/oggeSRuPvb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNPct7MX4AA_SvL.jpg",
        "id_str" : "924325030082895872",
        "id" : 924325030082895872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNPct7MX4AA_SvL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1367,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1806,
          "resize" : "fit",
          "w" : 2706
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oggeSRuPvb"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "924327408509030400",
    "text" : "https:\/\/t.co\/oggeSRuPvb",
    "id" : 924327408509030400,
    "created_at" : "2017-10-28 17:30:02 +0000",
    "user" : {
      "name" : "The Telegraph",
      "screen_name" : "Telegraph",
      "protected" : false,
      "id_str" : "16343974",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943090005723041792\/2cjxINlJ_normal.jpg",
      "id" : 16343974,
      "verified" : true
    }
  },
  "id" : 924672849369059328,
  "created_at" : "2017-10-29 16:22:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Katherine O'Keefe",
      "screen_name" : "okeefekat",
      "indices" : [ 3, 13 ],
      "id_str" : "2472741312",
      "id" : 2472741312
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924672382706618369",
  "text" : "RT @okeefekat: Saying \"Data is the new science\" is like saying \"Words are the new Literature\". The scientific method is important (and requ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/kZ8BSx9r6P",
        "expanded_url" : "https:\/\/twitter.com\/CompactBi\/status\/860464540013690880",
        "display_url" : "twitter.com\/CompactBi\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "860485149364023296",
    "text" : "Saying \"Data is the new science\" is like saying \"Words are the new Literature\". The scientific method is important (and requires data). https:\/\/t.co\/kZ8BSx9r6P",
    "id" : 860485149364023296,
    "created_at" : "2017-05-05 13:23:41 +0000",
    "user" : {
      "name" : "Dr. Katherine O'Keefe",
      "screen_name" : "okeefekat",
      "protected" : false,
      "id_str" : "2472741312",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040315080011907073\/G3QIxoau_normal.jpg",
      "id" : 2472741312,
      "verified" : false
    }
  },
  "id" : 924672382706618369,
  "created_at" : "2017-10-29 16:20:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/I8ppFXrX6S",
      "expanded_url" : "http:\/\/www.hbs.edu\/faculty\/Publication%20Files\/12-016_a7e4a5a2-03f9-490d-b093-8f951238dba2.pdf",
      "display_url" : "hbs.edu\/faculty\/Public\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "924638112931868672",
  "text" : "If a biz receives 1 more star in overall rating, it can generate a 5 to 9% increase in revenue according to a study https:\/\/t.co\/I8ppFXrX6S",
  "id" : 924638112931868672,
  "created_at" : "2017-10-29 14:04:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andreeeeeeeeaaaaaa!",
      "screen_name" : "brandalisms",
      "indices" : [ 0, 12 ],
      "id_str" : "249318820",
      "id" : 249318820
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "924243753958768640",
  "geo" : { },
  "id_str" : "924611702603354112",
  "in_reply_to_user_id" : 249318820,
  "text" : "@brandalisms A very interesting shaped ashtray. \uD83D\uDE01",
  "id" : 924611702603354112,
  "in_reply_to_status_id" : 924243753958768640,
  "created_at" : "2017-10-29 12:19:43 +0000",
  "in_reply_to_screen_name" : "brandalisms",
  "in_reply_to_user_id_str" : "249318820",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/924604190776807424\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/S4vsc2ZOLM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNTanElW4AE25Hw.jpg",
      "id_str" : "924604188297977857",
      "id" : 924604188297977857,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNTanElW4AE25Hw.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/S4vsc2ZOLM"
    } ],
    "hashtags" : [ {
      "text" : "Dublinmarathon",
      "indices" : [ 17, 32 ]
    } ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/CwO5uaXtfr",
      "expanded_url" : "http:\/\/ift.tt\/2idEw1W",
      "display_url" : "ift.tt\/2idEw1W"
    } ]
  },
  "geo" : { },
  "id_str" : "924604190776807424",
  "text" : "The professional #Dublinmarathon https:\/\/t.co\/CwO5uaXtfr https:\/\/t.co\/S4vsc2ZOLM",
  "id" : 924604190776807424,
  "created_at" : "2017-10-29 11:49:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DublinMarathon",
      "indices" : [ 30, 45 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924570630258855936",
  "text" : "All the best to those running #DublinMarathon this morning.",
  "id" : 924570630258855936,
  "created_at" : "2017-10-29 09:36:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cambridge University",
      "screen_name" : "Cambridge_Uni",
      "indices" : [ 3, 17 ],
      "id_str" : "33474655",
      "id" : 33474655
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StephenHawking",
      "indices" : [ 59, 74 ]
    }, {
      "text" : "openaccessweek",
      "indices" : [ 113, 128 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924359361752961029",
  "text" : "RT @Cambridge_Uni: Fancy a bit of light reading? We've put #StephenHawking's 1966 PhD thesis online to celebrate #openaccessweek https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "StephenHawking",
        "indices" : [ 40, 55 ]
      }, {
        "text" : "openaccessweek",
        "indices" : [ 94, 109 ]
      } ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/bakmB4kRtl",
        "expanded_url" : "http:\/\/ow.ly\/albb30g3GdL",
        "display_url" : "ow.ly\/albb30g3GdL"
      } ]
    },
    "geo" : { },
    "id_str" : "922380982442917888",
    "text" : "Fancy a bit of light reading? We've put #StephenHawking's 1966 PhD thesis online to celebrate #openaccessweek https:\/\/t.co\/bakmB4kRtl",
    "id" : 922380982442917888,
    "created_at" : "2017-10-23 08:35:37 +0000",
    "user" : {
      "name" : "Cambridge University",
      "screen_name" : "Cambridge_Uni",
      "protected" : false,
      "id_str" : "33474655",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875634893534904323\/Alz7fmFM_normal.jpg",
      "id" : 33474655,
      "verified" : true
    }
  },
  "id" : 924359361752961029,
  "created_at" : "2017-10-28 19:37:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Fire Brigade",
      "screen_name" : "DubFireBrigade",
      "indices" : [ 3, 18 ],
      "id_str" : "2798058029",
      "id" : 2798058029
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924355615836835840",
  "text" : "RT @DubFireBrigade: Our new ambulances are fitted with solar panels and re-generative braking to help recharge on board batteries and lower\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DubFireBrigade\/status\/922912992668409856\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/OhaRMUTBfa",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DM7YNRXX0AA_Z0B.jpg",
        "id_str" : "922912696168861696",
        "id" : 922912696168861696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM7YNRXX0AA_Z0B.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/OhaRMUTBfa"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/DubFireBrigade\/status\/922912992668409856\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/OhaRMUTBfa",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DM7YNr3X4AAx4Rb.jpg",
        "id_str" : "922912703282405376",
        "id" : 922912703282405376,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM7YNr3X4AAx4Rb.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/OhaRMUTBfa"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "922912992668409856",
    "text" : "Our new ambulances are fitted with solar panels and re-generative braking to help recharge on board batteries and lower CO2 emissions. https:\/\/t.co\/OhaRMUTBfa",
    "id" : 922912992668409856,
    "created_at" : "2017-10-24 19:49:39 +0000",
    "user" : {
      "name" : "Dublin Fire Brigade",
      "screen_name" : "DubFireBrigade",
      "protected" : false,
      "id_str" : "2798058029",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013704894518349824\/a65k7PEh_normal.jpg",
      "id" : 2798058029,
      "verified" : true
    }
  },
  "id" : 924355615836835840,
  "created_at" : "2017-10-28 19:22:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Independent",
      "screen_name" : "Independent",
      "indices" : [ 0, 12 ],
      "id_str" : "16973333",
      "id" : 16973333
    }, {
      "name" : "Renae Lucas-Hall \uD83D\uDCD6",
      "screen_name" : "RenaeLucasHall",
      "indices" : [ 13, 28 ],
      "id_str" : "629537782",
      "id" : 629537782
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "923918857814847489",
  "geo" : { },
  "id_str" : "924354976314478592",
  "in_reply_to_user_id" : 16973333,
  "text" : "@Independent @RenaeLucasHall Reminds me of muji store",
  "id" : 924354976314478592,
  "in_reply_to_status_id" : 923918857814847489,
  "created_at" : "2017-10-28 19:19:34 +0000",
  "in_reply_to_screen_name" : "Independent",
  "in_reply_to_user_id_str" : "16973333",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melissa",
      "screen_name" : "MelissaTorresO",
      "indices" : [ 3, 18 ],
      "id_str" : "134290838",
      "id" : 134290838
    }, {
      "name" : "\u2605 AKIRA THE DON \u2605",
      "screen_name" : "akirathedon",
      "indices" : [ 20, 32 ],
      "id_str" : "18225967",
      "id" : 18225967
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924353770590523393",
  "text" : "RT @MelissaTorresO: @akirathedon Sexy flying cockroach",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\u2605 AKIRA THE DON \u2605",
        "screen_name" : "akirathedon",
        "indices" : [ 0, 12 ],
        "id_str" : "18225967",
        "id" : 18225967
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "923417219437969408",
    "geo" : { },
    "id_str" : "924022639563194368",
    "in_reply_to_user_id" : 18225967,
    "text" : "@akirathedon Sexy flying cockroach",
    "id" : 924022639563194368,
    "in_reply_to_status_id" : 923417219437969408,
    "created_at" : "2017-10-27 21:18:59 +0000",
    "in_reply_to_screen_name" : "akirathedon",
    "in_reply_to_user_id_str" : "18225967",
    "user" : {
      "name" : "Melissa",
      "screen_name" : "MelissaTorresO",
      "protected" : false,
      "id_str" : "134290838",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035045352171286528\/Q0eCHroH_normal.jpg",
      "id" : 134290838,
      "verified" : false
    }
  },
  "id" : 924353770590523393,
  "created_at" : "2017-10-28 19:14:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/924135248773890048\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/UVeWf3DyXz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNMwGDfU8AAS493.jpg",
      "id_str" : "924135229115133952",
      "id" : 924135229115133952,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNMwGDfU8AAS493.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1367,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1367,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 801,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UVeWf3DyXz"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/924135248773890048\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/UVeWf3DyXz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNMwGDqVAAAiZMs.jpg",
      "id_str" : "924135229161275392",
      "id" : 924135229161275392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNMwGDqVAAAiZMs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1000
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UVeWf3DyXz"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/924135248773890048\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/UVeWf3DyXz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNMwGDjVQAAez1y.jpg",
      "id_str" : "924135229131931648",
      "id" : 924135229131931648,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNMwGDjVQAAez1y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1199,
        "resize" : "fit",
        "w" : 1600
      }, {
        "h" : 899,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1199,
        "resize" : "fit",
        "w" : 1600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UVeWf3DyXz"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/RdxrYUJVnM",
      "expanded_url" : "http:\/\/str.sg\/4FAn",
      "display_url" : "str.sg\/4FAn"
    } ]
  },
  "geo" : { },
  "id_str" : "924353209338159105",
  "text" : "RT @STcom: Inclusive play areas among new facilities at opening of Admiralty Park https:\/\/t.co\/RdxrYUJVnM https:\/\/t.co\/UVeWf3DyXz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/924135248773890048\/photo\/1",
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/UVeWf3DyXz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNMwGDfU8AAS493.jpg",
        "id_str" : "924135229115133952",
        "id" : 924135229115133952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNMwGDfU8AAS493.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1367,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1367,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UVeWf3DyXz"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/924135248773890048\/photo\/1",
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/UVeWf3DyXz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNMwGDqVAAAiZMs.jpg",
        "id_str" : "924135229161275392",
        "id" : 924135229161275392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNMwGDqVAAAiZMs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UVeWf3DyXz"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/924135248773890048\/photo\/1",
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/UVeWf3DyXz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNMwGDjVQAAez1y.jpg",
        "id_str" : "924135229131931648",
        "id" : 924135229131931648,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNMwGDjVQAAez1y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1199,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 899,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1199,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UVeWf3DyXz"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/RdxrYUJVnM",
        "expanded_url" : "http:\/\/str.sg\/4FAn",
        "display_url" : "str.sg\/4FAn"
      } ]
    },
    "geo" : { },
    "id_str" : "924135248773890048",
    "text" : "Inclusive play areas among new facilities at opening of Admiralty Park https:\/\/t.co\/RdxrYUJVnM https:\/\/t.co\/UVeWf3DyXz",
    "id" : 924135248773890048,
    "created_at" : "2017-10-28 04:46:27 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 924353209338159105,
  "created_at" : "2017-10-28 19:12:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allison Nau",
      "screen_name" : "AllisonMNau",
      "indices" : [ 3, 15 ],
      "id_str" : "2375231048",
      "id" : 2375231048
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cdo",
      "indices" : [ 66, 70 ]
    }, {
      "text" : "createvaluefromdata",
      "indices" : [ 71, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/kUQJytrehI",
      "expanded_url" : "https:\/\/www.cio.com.au\/article\/629103\/what-chief-data-officer-leader-who-creates-value-from-all-things-data\/",
      "display_url" : "cio.com.au\/article\/629103\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "924345095800967168",
  "text" : "RT @AllisonMNau: Best description of the role of a CDO I've seen. #cdo #createvaluefromdata https:\/\/t.co\/kUQJytrehI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "cdo",
        "indices" : [ 49, 53 ]
      }, {
        "text" : "createvaluefromdata",
        "indices" : [ 54, 74 ]
      } ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/kUQJytrehI",
        "expanded_url" : "https:\/\/www.cio.com.au\/article\/629103\/what-chief-data-officer-leader-who-creates-value-from-all-things-data\/",
        "display_url" : "cio.com.au\/article\/629103\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923948190361104384",
    "text" : "Best description of the role of a CDO I've seen. #cdo #createvaluefromdata https:\/\/t.co\/kUQJytrehI",
    "id" : 923948190361104384,
    "created_at" : "2017-10-27 16:23:09 +0000",
    "user" : {
      "name" : "Allison Nau",
      "screen_name" : "AllisonMNau",
      "protected" : false,
      "id_str" : "2375231048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/735531639024062464\/KMMQGJz9_normal.jpg",
      "id" : 2375231048,
      "verified" : false
    }
  },
  "id" : 924345095800967168,
  "created_at" : "2017-10-28 18:40:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/tw379ncNWO",
      "expanded_url" : "https:\/\/twitter.com\/notetoshelf",
      "display_url" : "twitter.com\/notetoshelf"
    } ]
  },
  "geo" : { },
  "id_str" : "924335955816927232",
  "text" : "To make my timeline more relevant here, I used another account https:\/\/t.co\/tw379ncNWO to tweet and comment as I go about doing my coding.",
  "id" : 924335955816927232,
  "created_at" : "2017-10-28 18:03:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Born Chinese",
      "screen_name" : "irishchinese",
      "indices" : [ 3, 16 ],
      "id_str" : "91134388",
      "id" : 91134388
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/3YLpky74vi",
      "expanded_url" : "https:\/\/www.theguardian.com\/world\/2017\/oct\/28\/asias-biggest-gay-pride-parade-tens-thousands-taipei-taiwan-marriage?CMP=Share_iOSApp_Other",
      "display_url" : "theguardian.com\/world\/2017\/oct\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "924277517061705728",
  "text" : "RT @irishchinese: Asia's biggest gay pride parade brings tens of thousands to Taipei https:\/\/t.co\/3YLpky74vi",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/3YLpky74vi",
        "expanded_url" : "https:\/\/www.theguardian.com\/world\/2017\/oct\/28\/asias-biggest-gay-pride-parade-tens-thousands-taipei-taiwan-marriage?CMP=Share_iOSApp_Other",
        "display_url" : "theguardian.com\/world\/2017\/oct\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "924239461021618176",
    "text" : "Asia's biggest gay pride parade brings tens of thousands to Taipei https:\/\/t.co\/3YLpky74vi",
    "id" : 924239461021618176,
    "created_at" : "2017-10-28 11:40:33 +0000",
    "user" : {
      "name" : "Irish Born Chinese",
      "screen_name" : "irishchinese",
      "protected" : false,
      "id_str" : "91134388",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1147247668\/ibc_normal.png",
      "id" : 91134388,
      "verified" : false
    }
  },
  "id" : 924277517061705728,
  "created_at" : "2017-10-28 14:11:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/924230222634987520\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/2vn5GZrZfM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNOGfTRXkAEP7B9.jpg",
      "id_str" : "924230220848271361",
      "id" : 924230220848271361,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNOGfTRXkAEP7B9.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2vn5GZrZfM"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/Iy0extpla7",
      "expanded_url" : "http:\/\/ift.tt\/2zdKhaJ",
      "display_url" : "ift.tt\/2zdKhaJ"
    } ]
  },
  "geo" : { },
  "id_str" : "924230222634987520",
  "text" : "Local news: man excited with the sighting of a USB charging point on Dublin bus. https:\/\/t.co\/Iy0extpla7 https:\/\/t.co\/2vn5GZrZfM",
  "id" : 924230222634987520,
  "created_at" : "2017-10-28 11:03:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/924219872036425728\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/MohZFecrp9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNN9E0KX4AAOqkk.jpg",
      "id_str" : "924219870216183808",
      "id" : 924219870216183808,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNN9E0KX4AAOqkk.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MohZFecrp9"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/wGdiDqzJ6G",
      "expanded_url" : "http:\/\/ift.tt\/2zX5aUQ",
      "display_url" : "ift.tt\/2zX5aUQ"
    } ]
  },
  "geo" : { },
  "id_str" : "924219872036425728",
  "text" : "Thankful that this survive the washing machine. https:\/\/t.co\/wGdiDqzJ6G https:\/\/t.co\/MohZFecrp9",
  "id" : 924219872036425728,
  "created_at" : "2017-10-28 10:22:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/j5EVQYG8Pl",
      "expanded_url" : "https:\/\/twitter.com\/GeneKerrigan\/status\/924189453475831808",
      "display_url" : "twitter.com\/GeneKerrigan\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "924218594405253120",
  "text" : "\"The line between the perverted and the ordinary is much, much thinner and more porous than we like to think.\" https:\/\/t.co\/j5EVQYG8Pl",
  "id" : 924218594405253120,
  "created_at" : "2017-10-28 10:17:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/VtRGXa0yWE",
      "expanded_url" : "https:\/\/liftr.me\/",
      "display_url" : "liftr.me"
    } ]
  },
  "geo" : { },
  "id_str" : "924204677943775232",
  "text" : "Share your Rmarkdown analysis including package names for reproducible reporting. https:\/\/t.co\/VtRGXa0yWE",
  "id" : 924204677943775232,
  "created_at" : "2017-10-28 09:22:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ro Khanna",
      "screen_name" : "RoKhanna",
      "indices" : [ 3, 12 ],
      "id_str" : "771152516",
      "id" : 771152516
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RoKhanna\/status\/923701871092441088\/photo\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/TlLYGezmv6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNGlrABUIAAr9RO.jpg",
      "id_str" : "923701556746133504",
      "id" : 923701556746133504,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNGlrABUIAAr9RO.jpg",
      "sizes" : [ {
        "h" : 467,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 813,
        "resize" : "fit",
        "w" : 1185
      }, {
        "h" : 813,
        "resize" : "fit",
        "w" : 1185
      }, {
        "h" : 813,
        "resize" : "fit",
        "w" : 1185
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TlLYGezmv6"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924184586963308544",
  "text" : "RT @RoKhanna: In Portugal, with no net neutrality, internet providers are starting to split the net into packages. https:\/\/t.co\/TlLYGezmv6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RoKhanna\/status\/923701871092441088\/photo\/1",
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/TlLYGezmv6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNGlrABUIAAr9RO.jpg",
        "id_str" : "923701556746133504",
        "id" : 923701556746133504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNGlrABUIAAr9RO.jpg",
        "sizes" : [ {
          "h" : 467,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 813,
          "resize" : "fit",
          "w" : 1185
        }, {
          "h" : 813,
          "resize" : "fit",
          "w" : 1185
        }, {
          "h" : 813,
          "resize" : "fit",
          "w" : 1185
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/TlLYGezmv6"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "923701871092441088",
    "text" : "In Portugal, with no net neutrality, internet providers are starting to split the net into packages. https:\/\/t.co\/TlLYGezmv6",
    "id" : 923701871092441088,
    "created_at" : "2017-10-27 00:04:22 +0000",
    "user" : {
      "name" : "Ro Khanna",
      "screen_name" : "RoKhanna",
      "protected" : false,
      "id_str" : "771152516",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/530907007746703360\/-mIRE-wq_normal.jpeg",
      "id" : 771152516,
      "verified" : true
    }
  },
  "id" : 924184586963308544,
  "created_at" : "2017-10-28 08:02:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GeekWire",
      "screen_name" : "geekwire",
      "indices" : [ 3, 12 ],
      "id_str" : "255784266",
      "id" : 255784266
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "924184425646166017",
  "text" : "RT @geekwire: Alibaba co-founder buys part of NBA team, SeatGeek and Facebook ink partnership, and more sports tech news from https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.geekwire.com\" rel=\"nofollow\"\u003EGeekWire\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/cSOJQ4YQvd",
        "expanded_url" : "https:\/\/www.geekwire.com\/2017\/alibaba-co-founder-buys-part-nba-team-seatgeek-facebook-ink-partnership-sports-tech-news-week\/",
        "display_url" : "geekwire.com\/2017\/alibaba-c\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "924028679272804353",
    "text" : "Alibaba co-founder buys part of NBA team, SeatGeek and Facebook ink partnership, and more sports tech news from https:\/\/t.co\/cSOJQ4YQvd",
    "id" : 924028679272804353,
    "created_at" : "2017-10-27 21:42:59 +0000",
    "user" : {
      "name" : "GeekWire",
      "screen_name" : "geekwire",
      "protected" : false,
      "id_str" : "255784266",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/454998478880403457\/BjKnFxtf_normal.png",
      "id" : 255784266,
      "verified" : true
    }
  },
  "id" : 924184425646166017,
  "created_at" : "2017-10-28 08:01:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Renae Lucas-Hall \uD83D\uDCD6",
      "screen_name" : "RenaeLucasHall",
      "indices" : [ 3, 18 ],
      "id_str" : "629537782",
      "id" : 629537782
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/IVKGpF0WTL",
      "expanded_url" : "http:\/\/www.japantimes.co.jp\/life\/2017\/07\/31\/language\/aware-japanese-workplace-no-nos\/#.WX77SoC5nnE.twitter",
      "display_url" : "japantimes.co.jp\/life\/2017\/07\/3\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "924171864494297088",
  "text" : "RT @RenaeLucasHall: Be aware of Japanese workplace no-nos | The Japan Times https:\/\/t.co\/IVKGpF0WTL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/IVKGpF0WTL",
        "expanded_url" : "http:\/\/www.japantimes.co.jp\/life\/2017\/07\/31\/language\/aware-japanese-workplace-no-nos\/#.WX77SoC5nnE.twitter",
        "display_url" : "japantimes.co.jp\/life\/2017\/07\/3\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "891956993371623424",
    "text" : "Be aware of Japanese workplace no-nos | The Japan Times https:\/\/t.co\/IVKGpF0WTL",
    "id" : 891956993371623424,
    "created_at" : "2017-07-31 09:41:33 +0000",
    "user" : {
      "name" : "Renae Lucas-Hall \uD83D\uDCD6",
      "screen_name" : "RenaeLucasHall",
      "protected" : false,
      "id_str" : "629537782",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/805743939957428224\/JMN045gA_normal.jpg",
      "id" : 629537782,
      "verified" : false
    }
  },
  "id" : 924171864494297088,
  "created_at" : "2017-10-28 07:11:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/vyM02xzAgT",
      "expanded_url" : "https:\/\/hub.docker.com\/r\/mryap\/minimal-notebook-r\/",
      "display_url" : "hub.docker.com\/r\/mryap\/minima\u2026"
    }, {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/q2U9RKZD68",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923963615132217345",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923984837312176130",
  "text" : "Clarifications: It not Conda R-essentials. Details here : https:\/\/t.co\/vyM02xzAgT https:\/\/t.co\/q2U9RKZD68",
  "id" : 923984837312176130,
  "created_at" : "2017-10-27 18:48:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Farbod Saraf",
      "screen_name" : "farbodsaraf",
      "indices" : [ 3, 15 ],
      "id_str" : "256900512",
      "id" : 256900512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923980006841253888",
  "text" : "RT @farbodsaraf: \"You likely have to get management approval for a $500 expense... but you can call a 1 hour meeting with 20 people and no\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "923353731948863489",
    "text" : "\"You likely have to get management approval for a $500 expense... but you can call a 1 hour meeting with 20 people and no one notices.\"",
    "id" : 923353731948863489,
    "created_at" : "2017-10-26 01:00:59 +0000",
    "user" : {
      "name" : "Farbod Saraf",
      "screen_name" : "farbodsaraf",
      "protected" : false,
      "id_str" : "256900512",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/854703172841025537\/0HaCebN__normal.jpg",
      "id" : 256900512,
      "verified" : false
    }
  },
  "id" : 923980006841253888,
  "created_at" : "2017-10-27 18:29:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/fC1oFaKL2D",
      "expanded_url" : "https:\/\/twitter.com\/dragonflystats\/status\/923557219484012544",
      "display_url" : "twitter.com\/dragonflystats\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923966225578852353",
  "text" : "This another one. https:\/\/t.co\/fC1oFaKL2D",
  "id" : 923966225578852353,
  "created_at" : "2017-10-27 17:34:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 66, 73 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/tzbfWf2wvf",
      "expanded_url" : "https:\/\/twitter.com\/jhollist\/status\/923301155685392384",
      "display_url" : "twitter.com\/jhollist\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923966007076642816",
  "text" : "This one takes the cake as my fav song. (if you are familiar with #rstats) https:\/\/t.co\/tzbfWf2wvf",
  "id" : 923966007076642816,
  "created_at" : "2017-10-27 17:33:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923963615132217345\/photo\/1",
      "indices" : [ 139, 162 ],
      "url" : "https:\/\/t.co\/80POOrqjTn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNKT-paXcAAr3vu.jpg",
      "id_str" : "923963578041987072",
      "id" : 923963578041987072,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNKT-paXcAAr3vu.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 992
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 992
      }, {
        "h" : 328,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 992
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/80POOrqjTn"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923963615132217345",
  "text" : "Building on the shoulders of \"giants\", does this look right? esp those \"Mounted from\" I just add R-essentials to jupyter\/minimal-notebook\/ https:\/\/t.co\/80POOrqjTn",
  "id" : 923963615132217345,
  "created_at" : "2017-10-27 17:24:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923923084486369280\/photo\/1",
      "indices" : [ 140, 163 ],
      "url" : "https:\/\/t.co\/8bzAgPnZFS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNJvJgDW4AAXd9e.jpg",
      "id_str" : "923923082577895424",
      "id" : 923923082577895424,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNJvJgDW4AAXd9e.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8bzAgPnZFS"
    } ],
    "hashtags" : [ {
      "text" : "ire",
      "indices" : [ 110, 114 ]
    } ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/RnGcy9Penp",
      "expanded_url" : "http:\/\/ift.tt\/2heAo20",
      "display_url" : "ift.tt\/2heAo20"
    } ]
  },
  "geo" : { },
  "id_str" : "923923084486369280",
  "text" : "A bank branch with Halloween decoration -Tracker mortgage facilitated by Central bank enter at your own risk. #ire\u2026 https:\/\/t.co\/RnGcy9Penp https:\/\/t.co\/8bzAgPnZFS",
  "id" : 923923084486369280,
  "created_at" : "2017-10-27 14:43:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "indices" : [ 3, 12 ],
      "id_str" : "742143",
      "id" : 742143
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/CHn0UnjM0s",
      "expanded_url" : "http:\/\/bbc.in\/2yQZU7u",
      "display_url" : "bbc.in\/2yQZU7u"
    } ]
  },
  "geo" : { },
  "id_str" : "923914270869516288",
  "text" : "RT @BBCWorld: Catalans declare independence from Spain https:\/\/t.co\/CHn0UnjM0s",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 41, 64 ],
        "url" : "https:\/\/t.co\/CHn0UnjM0s",
        "expanded_url" : "http:\/\/bbc.in\/2yQZU7u",
        "display_url" : "bbc.in\/2yQZU7u"
      } ]
    },
    "geo" : { },
    "id_str" : "923904970432438272",
    "text" : "Catalans declare independence from Spain https:\/\/t.co\/CHn0UnjM0s",
    "id" : 923904970432438272,
    "created_at" : "2017-10-27 13:31:24 +0000",
    "user" : {
      "name" : "BBC News (World)",
      "screen_name" : "BBCWorld",
      "protected" : false,
      "id_str" : "742143",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875702138680246273\/BfQLzf7G_normal.jpg",
      "id" : 742143,
      "verified" : true
    }
  },
  "id" : 923914270869516288,
  "created_at" : "2017-10-27 14:08:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923891479248080896",
  "text" : "That Warburton bread TV ads...\uD83D\uDE02",
  "id" : 923891479248080896,
  "created_at" : "2017-10-27 12:37:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/YzETTrSTxF",
      "expanded_url" : "https:\/\/twitter.com\/iryna_bur\/status\/923842625383395328",
      "display_url" : "twitter.com\/iryna_bur\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923857442164887552",
  "text" : "File under \"shit that start-up people utter\" https:\/\/t.co\/YzETTrSTxF",
  "id" : 923857442164887552,
  "created_at" : "2017-10-27 10:22:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "indices" : [ 3, 15 ],
      "id_str" : "10504042",
      "id" : 10504042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/6vM2EGObSz",
      "expanded_url" : "http:\/\/n.pr\/2y77efv",
      "display_url" : "n.pr\/2y77efv"
    } ]
  },
  "geo" : { },
  "id_str" : "923856078286311425",
  "text" : "RT @ChuiSquared: Most people in the UK don\u2019t like going to IKEA. It\u2019s a completely different story in China https:\/\/t.co\/6vM2EGObSz #intern\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "international",
        "indices" : [ 115, 129 ]
      }, {
        "text" : "culture",
        "indices" : [ 130, 138 ]
      } ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/6vM2EGObSz",
        "expanded_url" : "http:\/\/n.pr\/2y77efv",
        "display_url" : "n.pr\/2y77efv"
      } ]
    },
    "geo" : { },
    "id_str" : "923855390705668096",
    "text" : "Most people in the UK don\u2019t like going to IKEA. It\u2019s a completely different story in China https:\/\/t.co\/6vM2EGObSz #international #culture",
    "id" : 923855390705668096,
    "created_at" : "2017-10-27 10:14:24 +0000",
    "user" : {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "protected" : false,
      "id_str" : "10504042",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/892390265444552704\/Yg7EbMxO_normal.jpg",
      "id" : 10504042,
      "verified" : false
    }
  },
  "id" : 923856078286311425,
  "created_at" : "2017-10-27 10:17:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/7fiM4138dk",
      "expanded_url" : "https:\/\/www.forbes.com\/sites\/outofasia\/2017\/10\/27\/why-an-australian-space-entrepreneur-chose-tiny-singapore-to-build-a-rocket-factory\/#27e89b3198bc",
      "display_url" : "forbes.com\/sites\/outofasi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923850830205345792",
  "text" : "Why An Australian Space Entrepreneur Chose Tiny Singapore To Build A Rocket Factory https:\/\/t.co\/7fiM4138dk",
  "id" : 923850830205345792,
  "created_at" : "2017-10-27 09:56:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923833241194385408",
  "text" : "A business leader in SE Asia already quoted part of Xi speech on post on FB.",
  "id" : 923833241194385408,
  "created_at" : "2017-10-27 08:46:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Best Linux Blog In the Unixverse",
      "screen_name" : "nixcraft",
      "indices" : [ 3, 12 ],
      "id_str" : "17484680",
      "id" : 17484680
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/nixcraft\/status\/923779788384243712\/photo\/1",
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/bbeEvgpvsC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNHsvraVAAAICcC.jpg",
      "id_str" : "923779702438821888",
      "id" : 923779702438821888,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNHsvraVAAAICcC.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 519,
        "resize" : "fit",
        "w" : 1090
      }, {
        "h" : 519,
        "resize" : "fit",
        "w" : 1090
      }, {
        "h" : 519,
        "resize" : "fit",
        "w" : 1090
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bbeEvgpvsC"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923829473220415488",
  "text" : "RT @nixcraft: Working at porn hub ... lmao https:\/\/t.co\/bbeEvgpvsC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nixcraft\/status\/923779788384243712\/photo\/1",
        "indices" : [ 29, 52 ],
        "url" : "https:\/\/t.co\/bbeEvgpvsC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNHsvraVAAAICcC.jpg",
        "id_str" : "923779702438821888",
        "id" : 923779702438821888,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNHsvraVAAAICcC.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 519,
          "resize" : "fit",
          "w" : 1090
        }, {
          "h" : 519,
          "resize" : "fit",
          "w" : 1090
        }, {
          "h" : 519,
          "resize" : "fit",
          "w" : 1090
        }, {
          "h" : 324,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bbeEvgpvsC"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "923779788384243712",
    "text" : "Working at porn hub ... lmao https:\/\/t.co\/bbeEvgpvsC",
    "id" : 923779788384243712,
    "created_at" : "2017-10-27 05:13:59 +0000",
    "user" : {
      "name" : "The Best Linux Blog In the Unixverse",
      "screen_name" : "nixcraft",
      "protected" : false,
      "id_str" : "17484680",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/854775743552696324\/JPAU_2aP_normal.jpg",
      "id" : 17484680,
      "verified" : false
    }
  },
  "id" : 923829473220415488,
  "created_at" : "2017-10-27 08:31:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Arjun Pakrashi",
      "screen_name" : "phoxis",
      "indices" : [ 3, 10 ],
      "id_str" : "22647312",
      "id" : 22647312
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/phoxis\/status\/923613655954067457\/photo\/1",
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/c80eG5NVt3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNFVtpeWAAEzles.jpg",
      "id_str" : "923613641303261185",
      "id" : 923613641303261185,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNFVtpeWAAEzles.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/c80eG5NVt3"
    } ],
    "hashtags" : [ {
      "text" : "PyDataDublin",
      "indices" : [ 97, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923828623718731776",
  "text" : "RT @phoxis: Aniruddha Ghosh talking about Cyber Bullying Detection using Deep Neutral Network at #PyDataDublin https:\/\/t.co\/c80eG5NVt3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/phoxis\/status\/923613655954067457\/photo\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/c80eG5NVt3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNFVtpeWAAEzles.jpg",
        "id_str" : "923613641303261185",
        "id" : 923613641303261185,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNFVtpeWAAEzles.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/c80eG5NVt3"
      } ],
      "hashtags" : [ {
        "text" : "PyDataDublin",
        "indices" : [ 85, 98 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "923613655954067457",
    "text" : "Aniruddha Ghosh talking about Cyber Bullying Detection using Deep Neutral Network at #PyDataDublin https:\/\/t.co\/c80eG5NVt3",
    "id" : 923613655954067457,
    "created_at" : "2017-10-26 18:13:50 +0000",
    "user" : {
      "name" : "Arjun Pakrashi",
      "screen_name" : "phoxis",
      "protected" : false,
      "id_str" : "22647312",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/368077772\/kyungup2_normal.jpg",
      "id" : 22647312,
      "verified" : false
    }
  },
  "id" : 923828623718731776,
  "created_at" : "2017-10-27 08:28:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923828086369550336\/photo\/1",
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/eTt0eWcmci",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNIYv2bX0AAI8UX.jpg",
      "id_str" : "923828083907612672",
      "id" : 923828083907612672,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNIYv2bX0AAI8UX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eTt0eWcmci"
    } ],
    "hashtags" : [ {
      "text" : "dublin",
      "indices" : [ 28, 35 ]
    }, {
      "text" : "ireland",
      "indices" : [ 36, 44 ]
    } ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/nYFdNnrnwr",
      "expanded_url" : "http:\/\/ift.tt\/2iG6hnp",
      "display_url" : "ift.tt\/2iG6hnp"
    } ]
  },
  "geo" : { },
  "id_str" : "923828086369550336",
  "text" : "Good Morning. One hour ago. #dublin #ireland https:\/\/t.co\/nYFdNnrnwr https:\/\/t.co\/eTt0eWcmci",
  "id" : 923828086369550336,
  "created_at" : "2017-10-27 08:25:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Prof. Barry O'Sullivan, MRIA",
      "screen_name" : "BarryOSullivan",
      "indices" : [ 3, 18 ],
      "id_str" : "21306343",
      "id" : 21306343
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 131, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923806018198491136",
  "text" : "RT @BarryOSullivan: LeCun says \"recent advances in the field aren\u2019t taking us closer to super-intelligent machines\" which is true! #AI http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 111, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/7vFRBZ1Z6x",
        "expanded_url" : "https:\/\/www.theverge.com\/2017\/10\/26\/16552056\/a-intelligence-terminator-facebook-yann-lecun-interview?utm_campaign=theverge&utm_content=entry&utm_medium=social&utm_source=twitter",
        "display_url" : "theverge.com\/2017\/10\/26\/165\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923805325412765696",
    "text" : "LeCun says \"recent advances in the field aren\u2019t taking us closer to super-intelligent machines\" which is true! #AI https:\/\/t.co\/7vFRBZ1Z6x",
    "id" : 923805325412765696,
    "created_at" : "2017-10-27 06:55:27 +0000",
    "user" : {
      "name" : "Prof. Barry O'Sullivan, MRIA",
      "screen_name" : "BarryOSullivan",
      "protected" : false,
      "id_str" : "21306343",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001140233516273664\/iesSibD0_normal.jpg",
      "id" : 21306343,
      "verified" : false
    }
  },
  "id" : 923806018198491136,
  "created_at" : "2017-10-27 06:58:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathew Ingram",
      "screen_name" : "mathewi",
      "indices" : [ 3, 11 ],
      "id_str" : "824157",
      "id" : 824157
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/kepU5UuaRV",
      "expanded_url" : "https:\/\/twitter.com\/ShiraOvide\/status\/923661643338436609",
      "display_url" : "twitter.com\/ShiraOvide\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923668183722156032",
  "text" : "RT @mathewi: Something Amazon started as a side project to use up spare server space and processor time https:\/\/t.co\/kepU5UuaRV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/kepU5UuaRV",
        "expanded_url" : "https:\/\/twitter.com\/ShiraOvide\/status\/923661643338436609",
        "display_url" : "twitter.com\/ShiraOvide\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923664554629914624",
    "text" : "Something Amazon started as a side project to use up spare server space and processor time https:\/\/t.co\/kepU5UuaRV",
    "id" : 923664554629914624,
    "created_at" : "2017-10-26 21:36:05 +0000",
    "user" : {
      "name" : "Mathew Ingram",
      "screen_name" : "mathewi",
      "protected" : false,
      "id_str" : "824157",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/956610633369141248\/o0_-OLn__normal.jpg",
      "id" : 824157,
      "verified" : true
    }
  },
  "id" : 923668183722156032,
  "created_at" : "2017-10-26 21:50:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elaine Edwards",
      "screen_name" : "ElaineEdwardsIT",
      "indices" : [ 3, 19 ],
      "id_str" : "568092599",
      "id" : 568092599
    }, {
      "name" : "European Parliament",
      "screen_name" : "Europarl_EN",
      "indices" : [ 65, 77 ],
      "id_str" : "36329597",
      "id" : 36329597
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "eprivacy",
      "indices" : [ 86, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923652580353499136",
  "text" : "RT @ElaineEdwardsIT: Advertisers are, er, not happy with today's @Europarl_EN vote on #eprivacy regulation. No madam, they are not. https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "European Parliament",
        "screen_name" : "Europarl_EN",
        "indices" : [ 44, 56 ],
        "id_str" : "36329597",
        "id" : 36329597
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "eprivacy",
        "indices" : [ 65, 74 ]
      } ],
      "urls" : [ {
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/uwkp6osCYm",
        "expanded_url" : "https:\/\/www.iabeurope.eu\/policy\/iab-europe-press-release-european-parliament-positions-itself-against-the-ad-supported-internet\/",
        "display_url" : "iabeurope.eu\/policy\/iab-eur\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923647349632720896",
    "text" : "Advertisers are, er, not happy with today's @Europarl_EN vote on #eprivacy regulation. No madam, they are not. https:\/\/t.co\/uwkp6osCYm",
    "id" : 923647349632720896,
    "created_at" : "2017-10-26 20:27:43 +0000",
    "user" : {
      "name" : "Elaine Edwards",
      "screen_name" : "ElaineEdwardsIT",
      "protected" : false,
      "id_str" : "568092599",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/555911154061828096\/qM06WTGf_normal.jpeg",
      "id" : 568092599,
      "verified" : true
    }
  },
  "id" : 923652580353499136,
  "created_at" : "2017-10-26 20:48:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923616383862296582\/photo\/1",
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/PQrkYtOLsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNFYKN4WkAACN1K.jpg",
      "id_str" : "923616331135619072",
      "id" : 923616331135619072,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNFYKN4WkAACN1K.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PQrkYtOLsH"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923616383862296582",
  "text" : "\"Unfair advantage\" for those hard working students making use of down time so they can gain more computer game playing time. https:\/\/t.co\/PQrkYtOLsH",
  "id" : 923616383862296582,
  "created_at" : "2017-10-26 18:24:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/NyGzxCGobR",
      "expanded_url" : "https:\/\/twitter.com\/PrivacyMatters\/status\/923093979285073920",
      "display_url" : "twitter.com\/PrivacyMatters\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923611951116308480",
  "text" : "That grand. \uD83D\uDC4C https:\/\/t.co\/NyGzxCGobR",
  "id" : 923611951116308480,
  "created_at" : "2017-10-26 18:07:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/vNSLzPKCb6",
      "expanded_url" : "https:\/\/twitter.com\/beeonaposy\/status\/922565399987679233",
      "display_url" : "twitter.com\/beeonaposy\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923539843992489985",
  "text" : "'When we measure something in the real world, we never measure with exact accuracy and precision.' https:\/\/t.co\/vNSLzPKCb6",
  "id" : 923539843992489985,
  "created_at" : "2017-10-26 13:20:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "indices" : [ 0, 11 ],
      "id_str" : "166540104",
      "id" : 166540104
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "923222669373394944",
  "geo" : { },
  "id_str" : "923537101374787585",
  "in_reply_to_user_id" : 166540104,
  "text" : "@longhowlam Your Linkedin profile description are brilliant. Good play with words! \uD83D\uDE01",
  "id" : 923537101374787585,
  "in_reply_to_status_id" : 923222669373394944,
  "created_at" : "2017-10-26 13:09:38 +0000",
  "in_reply_to_screen_name" : "longhowlam",
  "in_reply_to_user_id_str" : "166540104",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/l1MreNumfC",
      "expanded_url" : "http:\/\/semphonic.blogs.com\/semangel\/2014\/01\/paid-owned-earned-and-measurement-frameworks-for-digital-media.html",
      "display_url" : "semphonic.blogs.com\/semangel\/2014\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923534510410657792",
  "text" : "Why Paid-Owned-Earned media isn\u2019t a great marketing framework for analytics and reporting? https:\/\/t.co\/l1MreNumfC",
  "id" : 923534510410657792,
  "created_at" : "2017-10-26 12:59:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923500605183807489\/photo\/1",
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/ENbeRZQ1ou",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DNDuqujX4AA_9xF.jpg",
      "id_str" : "923500341429264384",
      "id" : 923500341429264384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNDuqujX4AA_9xF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 224,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 224,
        "resize" : "fit",
        "w" : 681
      }, {
        "h" : 224,
        "resize" : "fit",
        "w" : 681
      }, {
        "h" : 224,
        "resize" : "fit",
        "w" : 681
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ENbeRZQ1ou"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923500605183807489",
  "text" : "Might be a big undertaking to get end-user to enable BIOS.... https:\/\/t.co\/ENbeRZQ1ou",
  "id" : 923500605183807489,
  "created_at" : "2017-10-26 10:44:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923497596055965696",
  "text" : "Some organisations wants your email in return for a PDF of info graphics which can be easily published on a blog.",
  "id" : 923497596055965696,
  "created_at" : "2017-10-26 10:32:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bambu",
      "screen_name" : "bambu_life",
      "indices" : [ 3, 14 ],
      "id_str" : "748338025566871552",
      "id" : 748338025566871552
    }, {
      "name" : "Bloomberg Technology",
      "screen_name" : "technology",
      "indices" : [ 100, 111 ],
      "id_str" : "21272440",
      "id" : 21272440
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/V0EmZ7QAtC",
      "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2017-10-25\/traditional-rivals-hong-kong-singapore-join-forces-for-fintech",
      "display_url" : "bloomberg.com\/news\/articles\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923492313489428480",
  "text" : "RT @bambu_life: Hong Kong and Singapore are joining forces over fintech https:\/\/t.co\/V0EmZ7QAtC via @technology",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bloomberg Technology",
        "screen_name" : "technology",
        "indices" : [ 84, 95 ],
        "id_str" : "21272440",
        "id" : 21272440
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/V0EmZ7QAtC",
        "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2017-10-25\/traditional-rivals-hong-kong-singapore-join-forces-for-fintech",
        "display_url" : "bloomberg.com\/news\/articles\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923467994201694208",
    "text" : "Hong Kong and Singapore are joining forces over fintech https:\/\/t.co\/V0EmZ7QAtC via @technology",
    "id" : 923467994201694208,
    "created_at" : "2017-10-26 08:35:01 +0000",
    "user" : {
      "name" : "Bambu",
      "screen_name" : "bambu_life",
      "protected" : false,
      "id_str" : "748338025566871552",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1018680646045036544\/nJsgZ9sf_normal.jpg",
      "id" : 748338025566871552,
      "verified" : false
    }
  },
  "id" : 923492313489428480,
  "created_at" : "2017-10-26 10:11:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/7NKaL6Aas9",
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/923352492301197312",
      "display_url" : "twitter.com\/mrbrown\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923491229874184192",
  "text" : "Except for bus and good vehicles. Feel sorry for those petrolhead... https:\/\/t.co\/7NKaL6Aas9",
  "id" : 923491229874184192,
  "created_at" : "2017-10-26 10:07:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/fHaN9tiW4b",
      "expanded_url" : "https:\/\/hub.docker.com\/r\/mryap\/",
      "display_url" : "hub.docker.com\/r\/mryap\/"
    } ]
  },
  "geo" : { },
  "id_str" : "923488998768726016",
  "text" : "My PC being \"hijacked\" for other use. Thanks to Docker, all my toolbox is available at on another PC. https:\/\/t.co\/fHaN9tiW4b",
  "id" : 923488998768726016,
  "created_at" : "2017-10-26 09:58:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deirdre Lee",
      "screen_name" : "deirdrelee",
      "indices" : [ 3, 14 ],
      "id_str" : "21078662",
      "id" : 21078662
    }, {
      "name" : "TechIreland",
      "screen_name" : "techireland",
      "indices" : [ 25, 37 ],
      "id_str" : "748550537528905728",
      "id" : 748550537528905728
    }, {
      "name" : "Derilinx",
      "screen_name" : "derilinx",
      "indices" : [ 106, 115 ],
      "id_str" : "2241015385",
      "id" : 2241015385
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FemaleFounders",
      "indices" : [ 63, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/CZ68qsMNKe",
      "expanded_url" : "https:\/\/welcome.techireland.org\/snapshots\/view\/brilliant-female-founders-bffs",
      "display_url" : "welcome.techireland.org\/snapshots\/view\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923486510221086722",
  "text" : "RT @deirdrelee: Kudos to @techireland for showcasing Ireland's #FemaleFounders. Delighted to be included! @derilinx https:\/\/t.co\/CZ68qsMNKe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TechIreland",
        "screen_name" : "techireland",
        "indices" : [ 9, 21 ],
        "id_str" : "748550537528905728",
        "id" : 748550537528905728
      }, {
        "name" : "Derilinx",
        "screen_name" : "derilinx",
        "indices" : [ 90, 99 ],
        "id_str" : "2241015385",
        "id" : 2241015385
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "FemaleFounders",
        "indices" : [ 47, 62 ]
      } ],
      "urls" : [ {
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/CZ68qsMNKe",
        "expanded_url" : "https:\/\/welcome.techireland.org\/snapshots\/view\/brilliant-female-founders-bffs",
        "display_url" : "welcome.techireland.org\/snapshots\/view\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923464764642283520",
    "text" : "Kudos to @techireland for showcasing Ireland's #FemaleFounders. Delighted to be included! @derilinx https:\/\/t.co\/CZ68qsMNKe",
    "id" : 923464764642283520,
    "created_at" : "2017-10-26 08:22:11 +0000",
    "user" : {
      "name" : "Deirdre Lee",
      "screen_name" : "deirdrelee",
      "protected" : false,
      "id_str" : "21078662",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/723143954917634048\/X0Uc0skg_normal.jpg",
      "id" : 21078662,
      "verified" : false
    }
  },
  "id" : 923486510221086722,
  "created_at" : "2017-10-26 09:48:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ranja Reda Kouba",
      "screen_name" : "ranjareda",
      "indices" : [ 3, 13 ],
      "id_str" : "284788118",
      "id" : 284788118
    }, {
      "name" : "Prof Alice Roberts",
      "screen_name" : "theAliceRoberts",
      "indices" : [ 119, 135 ],
      "id_str" : "260211154",
      "id" : 260211154
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923275579557842945",
  "text" : "RT @ranjareda: Why are fall leaves yellow in Europe &amp; red in North America?Evolution provides an intriguing answer @theAliceRoberts https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Prof Alice Roberts",
        "screen_name" : "theAliceRoberts",
        "indices" : [ 104, 120 ],
        "id_str" : "260211154",
        "id" : 260211154
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ranjareda\/status\/923243235492941831\/photo\/1",
        "indices" : [ 145, 168 ],
        "url" : "https:\/\/t.co\/J43aP9JLOH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DNAEtIdX4AE5FwI.jpg",
        "id_str" : "923243097022259201",
        "id" : 923243097022259201,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DNAEtIdX4AE5FwI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 366,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 645,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1102,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1270,
          "resize" : "fit",
          "w" : 2361
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J43aP9JLOH"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/3ZE98NHMZo",
        "expanded_url" : "https:\/\/www.theguardian.com\/environment\/2015\/nov\/22\/why-are-autumn-leaves-yellow-in-europe-red-in-north-america",
        "display_url" : "theguardian.com\/environment\/20\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "923243235492941831",
    "text" : "Why are fall leaves yellow in Europe &amp; red in North America?Evolution provides an intriguing answer @theAliceRoberts https:\/\/t.co\/3ZE98NHMZo https:\/\/t.co\/J43aP9JLOH",
    "id" : 923243235492941831,
    "created_at" : "2017-10-25 17:41:55 +0000",
    "user" : {
      "name" : "Ranja Reda Kouba",
      "screen_name" : "ranjareda",
      "protected" : false,
      "id_str" : "284788118",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922372932780818432\/xy-K7-_v_normal.jpg",
      "id" : 284788118,
      "verified" : false
    }
  },
  "id" : 923275579557842945,
  "created_at" : "2017-10-25 19:50:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923216233335607296\/photo\/1",
      "indices" : [ 16, 39 ],
      "url" : "https:\/\/t.co\/hHHS13S72h",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DM_sPNeW0AAQt8v.jpg",
      "id_str" : "923216194693419008",
      "id" : 923216194693419008,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM_sPNeW0AAQt8v.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 422,
        "resize" : "fit",
        "w" : 618
      }, {
        "h" : 422,
        "resize" : "fit",
        "w" : 618
      }, {
        "h" : 422,
        "resize" : "fit",
        "w" : 618
      }, {
        "h" : 422,
        "resize" : "fit",
        "w" : 618
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hHHS13S72h"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923216233335607296",
  "text" : "Love the story. https:\/\/t.co\/hHHS13S72h",
  "id" : 923216233335607296,
  "created_at" : "2017-10-25 15:54:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/922960480687218688\/photo\/1",
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/WLnCtbWAXL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DM8DpC4UIAAeCSt.jpg",
      "id_str" : "922960452316831744",
      "id" : 922960452316831744,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM8DpC4UIAAeCSt.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 733,
        "resize" : "fit",
        "w" : 1242
      }, {
        "h" : 733,
        "resize" : "fit",
        "w" : 1242
      }, {
        "h" : 708,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 401,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WLnCtbWAXL"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923141737756921857",
  "text" : "RT @mrbrown: Since when? https:\/\/t.co\/WLnCtbWAXL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/922960480687218688\/photo\/1",
        "indices" : [ 12, 35 ],
        "url" : "https:\/\/t.co\/WLnCtbWAXL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DM8DpC4UIAAeCSt.jpg",
        "id_str" : "922960452316831744",
        "id" : 922960452316831744,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM8DpC4UIAAeCSt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 733,
          "resize" : "fit",
          "w" : 1242
        }, {
          "h" : 733,
          "resize" : "fit",
          "w" : 1242
        }, {
          "h" : 708,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 401,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WLnCtbWAXL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "922960480687218688",
    "text" : "Since when? https:\/\/t.co\/WLnCtbWAXL",
    "id" : 922960480687218688,
    "created_at" : "2017-10-24 22:58:21 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 923141737756921857,
  "created_at" : "2017-10-25 10:58:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923139224903323648",
  "text" : "Most tutorial assume you're familiar with XYZ, have it configured, know exactly what you'd like to run by giving you just a one-liner code.",
  "id" : 923139224903323648,
  "created_at" : "2017-10-25 10:48:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923131005208276993\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/LbRLz8a9fc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DM-ewFOWkAAz2wj.jpg",
      "id_str" : "923130997507526656",
      "id" : 923130997507526656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM-ewFOWkAAz2wj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 978
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 978
      }, {
        "h" : 225,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 978
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LbRLz8a9fc"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923131005208276993",
  "text" : "This makes sense even 5 months later when I read the documentation again. https:\/\/t.co\/LbRLz8a9fc",
  "id" : 923131005208276993,
  "created_at" : "2017-10-25 10:15:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/923127231085535232\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/dn3q3vslEr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DM-bUMDWAAAEHB_.jpg",
      "id_str" : "923127219769180160",
      "id" : 923127219769180160,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM-bUMDWAAAEHB_.jpg",
      "sizes" : [ {
        "h" : 72,
        "resize" : "fit",
        "w" : 899
      }, {
        "h" : 72,
        "resize" : "fit",
        "w" : 899
      }, {
        "h" : 72,
        "resize" : "fit",
        "w" : 899
      }, {
        "h" : 72,
        "resize" : "crop",
        "w" : 72
      }, {
        "h" : 54,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dn3q3vslEr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "923127231085535232",
  "text" : "When such words are uttered, you know developers are talking among themselves. https:\/\/t.co\/dn3q3vslEr",
  "id" : 923127231085535232,
  "created_at" : "2017-10-25 10:00:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "air plus news",
      "screen_name" : "airplusnews",
      "indices" : [ 3, 15 ],
      "id_str" : "883253580991475713",
      "id" : 883253580991475713
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "avgeek",
      "indices" : [ 49, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/Dh6VXh922U",
      "expanded_url" : "https:\/\/twitter.com\/FABIANOVELAR2\/status\/922701085298626560\/video\/1",
      "display_url" : "twitter.com\/FABIANOVELAR2\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923100303624429569",
  "text" : "RT @airplusnews: On s\u2019amuse bien \u00E0 Rochester \uD83D\uDE1C\u2708\uFE0F #avgeek  https:\/\/t.co\/Dh6VXh922U",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "avgeek",
        "indices" : [ 32, 39 ]
      } ],
      "urls" : [ {
        "indices" : [ 41, 64 ],
        "url" : "https:\/\/t.co\/Dh6VXh922U",
        "expanded_url" : "https:\/\/twitter.com\/FABIANOVELAR2\/status\/922701085298626560\/video\/1",
        "display_url" : "twitter.com\/FABIANOVELAR2\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "922893336343666689",
    "text" : "On s\u2019amuse bien \u00E0 Rochester \uD83D\uDE1C\u2708\uFE0F #avgeek  https:\/\/t.co\/Dh6VXh922U",
    "id" : 922893336343666689,
    "created_at" : "2017-10-24 18:31:32 +0000",
    "user" : {
      "name" : "air plus news",
      "screen_name" : "airplusnews",
      "protected" : false,
      "id_str" : "883253580991475713",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013474926232244224\/jpf5L4jx_normal.jpg",
      "id" : 883253580991475713,
      "verified" : false
    }
  },
  "id" : 923100303624429569,
  "created_at" : "2017-10-25 08:13:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "harryshum",
      "screen_name" : "harryshum",
      "indices" : [ 3, 13 ],
      "id_str" : "15636998",
      "id" : 15636998
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/kXVywQsVzc",
      "expanded_url" : "https:\/\/twitter.com\/windowsblog\/status\/921360699862892544",
      "display_url" : "twitter.com\/windowsblog\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "923059467981344768",
  "text" : "RT @harryshum: Excited to partner with Harmon Kardon to take Cortana with me wherever I go! https:\/\/t.co\/kXVywQsVzc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/kXVywQsVzc",
        "expanded_url" : "https:\/\/twitter.com\/windowsblog\/status\/921360699862892544",
        "display_url" : "twitter.com\/windowsblog\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "921367888245637120",
    "text" : "Excited to partner with Harmon Kardon to take Cortana with me wherever I go! https:\/\/t.co\/kXVywQsVzc",
    "id" : 921367888245637120,
    "created_at" : "2017-10-20 13:29:57 +0000",
    "user" : {
      "name" : "harryshum",
      "screen_name" : "harryshum",
      "protected" : false,
      "id_str" : "15636998",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/473379244840480769\/Y4YeRso7_normal.png",
      "id" : 15636998,
      "verified" : true
    }
  },
  "id" : 923059467981344768,
  "created_at" : "2017-10-25 05:31:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tourism NI",
      "screen_name" : "NITouristBoard",
      "indices" : [ 3, 18 ],
      "id_str" : "65617825",
      "id" : 65617825
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BestinTravel",
      "indices" : [ 122, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922949207899688960",
  "text" : "RT @NITouristBoard: Lonely Planet declares Belfast and the Causeway Coast the Number One Region in world to visit in 2018 #BestinTravel htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/NITouristBoard\/status\/922814867656118272\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/8gOKkGkRlb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DM59ti_W4AAehYS.jpg",
        "id_str" : "922813195097661440",
        "id" : 922813195097661440,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM59ti_W4AAehYS.jpg",
        "sizes" : [ {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8gOKkGkRlb"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/NITouristBoard\/status\/922814867656118272\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/8gOKkGkRlb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DM59ti9X0AAhuZR.jpg",
        "id_str" : "922813195089334272",
        "id" : 922813195089334272,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM59ti9X0AAhuZR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8gOKkGkRlb"
      } ],
      "hashtags" : [ {
        "text" : "BestinTravel",
        "indices" : [ 102, 115 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/xbpXK97IqV",
        "expanded_url" : "https:\/\/tourismni.com\/media-centre\/News\/",
        "display_url" : "tourismni.com\/media-centre\/N\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "922814867656118272",
    "text" : "Lonely Planet declares Belfast and the Causeway Coast the Number One Region in world to visit in 2018 #BestinTravel https:\/\/t.co\/xbpXK97IqV https:\/\/t.co\/8gOKkGkRlb",
    "id" : 922814867656118272,
    "created_at" : "2017-10-24 13:19:44 +0000",
    "user" : {
      "name" : "Tourism NI",
      "screen_name" : "NITouristBoard",
      "protected" : false,
      "id_str" : "65617825",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3161261547\/27c23bdd007a89aac9dca076b6d3460f_normal.jpeg",
      "id" : 65617825,
      "verified" : false
    }
  },
  "id" : 922949207899688960,
  "created_at" : "2017-10-24 22:13:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rtept",
      "indices" : [ 123, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922930998190501889",
  "text" : "Miriam (the presenter) walk across as guest answering her question. Space between them is so wide you can drive a van thru #rtept",
  "id" : 922930998190501889,
  "created_at" : "2017-10-24 21:01:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/YL8FSweymc",
      "expanded_url" : "https:\/\/stackoverflow.com\/questions\/38118791\/can-t-delete-image-with-children",
      "display_url" : "stackoverflow.com\/questions\/3811\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922892280545054720",
  "text" : "\"Can\u2019t delete image with children\" I can assure you it nothing sinister https:\/\/t.co\/YL8FSweymc",
  "id" : 922892280545054720,
  "created_at" : "2017-10-24 18:27:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/Ipn1VUpZ45",
      "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6328584064597131264",
      "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922818879772127232",
  "text" : "You can consider container as the end product of a data analysis. https:\/\/t.co\/Ipn1VUpZ45",
  "id" : 922818879772127232,
  "created_at" : "2017-10-24 13:35:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/dCcOf9iAVU",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/922636816708259840",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922812191061012481",
  "text" : "It \"fun\" wading into this and asking myself: \"What the use-case?\" \"Can I make a business out of it?\" https:\/\/t.co\/dCcOf9iAVU",
  "id" : 922812191061012481,
  "created_at" : "2017-10-24 13:09:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dominant Puspure",
      "screen_name" : "josefoshea",
      "indices" : [ 3, 14 ],
      "id_str" : "249075294",
      "id" : 249075294
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922804771496525829",
  "text" : "RT @josefoshea: He's an Irish citizen - he was locked up under a cruel regieme. We should always look after our own . All u need to know #I\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IbrahimHalawa",
        "indices" : [ 121, 135 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "922795222429569025",
    "text" : "He's an Irish citizen - he was locked up under a cruel regieme. We should always look after our own . All u need to know #IbrahimHalawa",
    "id" : 922795222429569025,
    "created_at" : "2017-10-24 12:01:40 +0000",
    "user" : {
      "name" : "Dominant Puspure",
      "screen_name" : "josefoshea",
      "protected" : false,
      "id_str" : "249075294",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024005955720294402\/AzEG7ryA_normal.jpg",
      "id" : 249075294,
      "verified" : false
    }
  },
  "id" : 922804771496525829,
  "created_at" : "2017-10-24 12:39:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Pierotti",
      "screen_name" : "paul73p",
      "indices" : [ 3, 11 ],
      "id_str" : "298424747",
      "id" : 298424747
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922797925373218816",
  "text" : "RT @paul73p: Lee Hickin, IoT Business Lead, APAC , Amazon Web Services paints a picture of a tech enabled future for Singapore https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/QiqNrv2dFJ",
        "expanded_url" : "https:\/\/lnkd.in\/eMKj3WQ",
        "display_url" : "lnkd.in\/eMKj3WQ"
      } ]
    },
    "geo" : { },
    "id_str" : "922779706147180546",
    "text" : "Lee Hickin, IoT Business Lead, APAC , Amazon Web Services paints a picture of a tech enabled future for Singapore https:\/\/t.co\/QiqNrv2dFJ",
    "id" : 922779706147180546,
    "created_at" : "2017-10-24 11:00:01 +0000",
    "user" : {
      "name" : "Paul Pierotti",
      "screen_name" : "paul73p",
      "protected" : false,
      "id_str" : "298424747",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/462206785634304000\/gOFQRSAc_normal.jpeg",
      "id" : 298424747,
      "verified" : false
    }
  },
  "id" : 922797925373218816,
  "created_at" : "2017-10-24 12:12:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/XhlzhWIrBi",
      "expanded_url" : "http:\/\/po.st\/4xwCJO",
      "display_url" : "po.st\/4xwCJO"
    } ]
  },
  "geo" : { },
  "id_str" : "922771056846344193",
  "text" : "Sisters wake up at 4am to go to school in Singapore\nhttps:\/\/t.co\/XhlzhWIrBi",
  "id" : 922771056846344193,
  "created_at" : "2017-10-24 10:25:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922754460509790208",
  "text" : "Thanks to Google Search, I do not need to fork out \u20AC27.05 to contact a LinkedIn member I am not connected to via Linkedin InMail.",
  "id" : 922754460509790208,
  "created_at" : "2017-10-24 09:19:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922752474871721985",
  "text" : "I hope I do not need to make a call to Joe Duffey.",
  "id" : 922752474871721985,
  "created_at" : "2017-10-24 09:11:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 7, 18 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922752357506658304",
  "text" : "Thanks @Dotnetster for the retweet.",
  "id" : 922752357506658304,
  "created_at" : "2017-10-24 09:11:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "indices" : [ 0, 12 ],
      "id_str" : "21564577",
      "id" : 21564577
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/NUPbljg9PU",
      "expanded_url" : "https:\/\/www.myhome.ie\/rentals\/brochure\/the-marker-residence-grand-canal-dk-dublin-2\/3210420",
      "display_url" : "myhome.ie\/rentals\/brochu\u2026"
    }, {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/e8bQMc47BL",
      "expanded_url" : "https:\/\/www.linkedin.com\/company\/ires-fund-management\/",
      "display_url" : "linkedin.com\/company\/ires-f\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "922750857988452352",
  "geo" : { },
  "id_str" : "922752137096056832",
  "in_reply_to_user_id" : 21564577,
  "text" : "@chris_byrne Thanks. I manage to get an email from https:\/\/t.co\/NUPbljg9PU after getting hold a staff name from https:\/\/t.co\/e8bQMc47BL",
  "id" : 922752137096056832,
  "in_reply_to_status_id" : 922750857988452352,
  "created_at" : "2017-10-24 09:10:28 +0000",
  "in_reply_to_screen_name" : "chris_byrne",
  "in_reply_to_user_id_str" : "21564577",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922744666386321408",
  "text" : "Anyone on Twitter know anyone at https:\/www.iresreit.ie? Email them and no replied since 28 Sept 2017. Thanks.",
  "id" : 922744666386321408,
  "created_at" : "2017-10-24 08:40:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/vDzWNpzCW7",
      "expanded_url" : "https:\/\/hub.docker.com\/r\/mryap\/verse_gapminder\/",
      "display_url" : "hub.docker.com\/r\/mryap\/verse_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922636816708259840",
  "text" : "My \"Hello  World\" of Data Science in Containers. https:\/\/t.co\/vDzWNpzCW7",
  "id" : 922636816708259840,
  "created_at" : "2017-10-24 01:32:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore Airlines",
      "screen_name" : "SingaporeAir",
      "indices" : [ 0, 13 ],
      "id_str" : "253340062",
      "id" : 253340062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "922613235760664577",
  "geo" : { },
  "id_str" : "922616562028306432",
  "in_reply_to_user_id" : 253340062,
  "text" : "@SingaporeAir Are u referring to the US$13.8 billion deal between SIA and Boeing for 20 Boeing 777-9 model aircraft and 19 Boeing 787-10 Dreamliners?",
  "id" : 922616562028306432,
  "in_reply_to_status_id" : 922613235760664577,
  "created_at" : "2017-10-24 00:11:44 +0000",
  "in_reply_to_screen_name" : "SingaporeAir",
  "in_reply_to_user_id_str" : "253340062",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kevin Lim (PhD)",
      "screen_name" : "brainopera",
      "indices" : [ 3, 14 ],
      "id_str" : "22823",
      "id" : 22823
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "supreme",
      "indices" : [ 88, 96 ]
    }, {
      "text" : "lv",
      "indices" : [ 97, 100 ]
    }, {
      "text" : "louisvuitton",
      "indices" : [ 101, 114 ]
    }, {
      "text" : "lvtimecapsule",
      "indices" : [ 115, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922611396172447747",
  "text" : "RT @brainopera: $60,000 skateboard with its own traveling case. Dare you to grind this! #supreme #lv #louisvuitton #lvtimecapsule \u2026 https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/brainopera\/status\/922610184253353984\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/89QEDHKqZk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DM3FEp3UMAAwWS6.jpg",
        "id_str" : "922610182428831744",
        "id" : 922610182428831744,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DM3FEp3UMAAwWS6.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/89QEDHKqZk"
      } ],
      "hashtags" : [ {
        "text" : "supreme",
        "indices" : [ 72, 80 ]
      }, {
        "text" : "lv",
        "indices" : [ 81, 84 ]
      }, {
        "text" : "louisvuitton",
        "indices" : [ 85, 98 ]
      }, {
        "text" : "lvtimecapsule",
        "indices" : [ 99, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/JLoBA9Mruk",
        "expanded_url" : "http:\/\/ift.tt\/2zLceUb",
        "display_url" : "ift.tt\/2zLceUb"
      } ]
    },
    "geo" : { },
    "id_str" : "922610184253353984",
    "text" : "$60,000 skateboard with its own traveling case. Dare you to grind this! #supreme #lv #louisvuitton #lvtimecapsule \u2026 https:\/\/t.co\/JLoBA9Mruk https:\/\/t.co\/89QEDHKqZk",
    "id" : 922610184253353984,
    "created_at" : "2017-10-23 23:46:23 +0000",
    "user" : {
      "name" : "Kevin Lim (PhD)",
      "screen_name" : "brainopera",
      "protected" : false,
      "id_str" : "22823",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/820467861936369664\/gAir1s2g_normal.jpg",
      "id" : 22823,
      "verified" : false
    }
  },
  "id" : 922611396172447747,
  "created_at" : "2017-10-23 23:51:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/lh5mINl4l1",
      "expanded_url" : "https:\/\/www.eventbrite.ie\/e\/ai-data-science-big-data-open-house-tickets-37945807933",
      "display_url" : "eventbrite.ie\/e\/ai-data-scie\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922609559683944450",
  "text" : "Anyone on Twitter heading to this event ? https:\/\/t.co\/lh5mINl4l1",
  "id" : 922609559683944450,
  "created_at" : "2017-10-23 23:43:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/t2AeTvlO4U",
      "expanded_url" : "https:\/\/github.com\/docker\/kitematic\/issues\/3204",
      "display_url" : "github.com\/docker\/kitemat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922551967678894081",
  "text" : "I end up solving the problem myself...https:\/\/t.co\/t2AeTvlO4U",
  "id" : 922551967678894081,
  "created_at" : "2017-10-23 19:55:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 3, 11 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/R2nu0bq1ir",
      "expanded_url" : "https:\/\/en.m.wikipedia.org\/wiki\/List_of_national_mottos",
      "display_url" : "en.m.wikipedia.org\/wiki\/List_of_n\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922517976624041984",
  "text" : "RT @pdscott: Ireland has no official motto, apparently. We should design one\nhttps:\/\/t.co\/R2nu0bq1ir",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/R2nu0bq1ir",
        "expanded_url" : "https:\/\/en.m.wikipedia.org\/wiki\/List_of_national_mottos",
        "display_url" : "en.m.wikipedia.org\/wiki\/List_of_n\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "922515328453808134",
    "text" : "Ireland has no official motto, apparently. We should design one\nhttps:\/\/t.co\/R2nu0bq1ir",
    "id" : 922515328453808134,
    "created_at" : "2017-10-23 17:29:28 +0000",
    "user" : {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "protected" : false,
      "id_str" : "19360077",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850841737333485568\/qrEHiA5j_normal.jpg",
      "id" : 19360077,
      "verified" : false
    }
  },
  "id" : 922517976624041984,
  "created_at" : "2017-10-23 17:39:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/RKOHPruOx4",
      "expanded_url" : "http:\/\/sobersecurity.blogspot.ie\/2016\/03\/containers-are-like-sandwiches.html",
      "display_url" : "sobersecurity.blogspot.ie\/2016\/03\/contai\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922507846021013504",
  "text" : "The old adage still applies.  You shouldn\u2019t use software from untrusted sources.  https:\/\/t.co\/RKOHPruOx4",
  "id" : 922507846021013504,
  "created_at" : "2017-10-23 16:59:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenGovData",
      "indices" : [ 50, 62 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/2xLwNAhNEg",
      "expanded_url" : "http:\/\/bit.ly\/OpenGovDataStudy",
      "display_url" : "bit.ly\/OpenGovDataStu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922491491532132352",
  "text" : "RT @GovTechSG: Can global businesses benefit from #OpenGovData? Find out more from the global EIU survey report https:\/\/t.co\/2xLwNAhNEg htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GovTechSG\/status\/921179914975322112\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/XyBumddwjq",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DMiwHHHVwAAaggG.jpg",
        "id_str" : "921179760012673024",
        "id" : 921179760012673024,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DMiwHHHVwAAaggG.jpg",
        "sizes" : [ {
          "h" : 800,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XyBumddwjq"
      } ],
      "hashtags" : [ {
        "text" : "OpenGovData",
        "indices" : [ 35, 47 ]
      } ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/2xLwNAhNEg",
        "expanded_url" : "http:\/\/bit.ly\/OpenGovDataStudy",
        "display_url" : "bit.ly\/OpenGovDataStu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "921179914975322112",
    "text" : "Can global businesses benefit from #OpenGovData? Find out more from the global EIU survey report https:\/\/t.co\/2xLwNAhNEg https:\/\/t.co\/XyBumddwjq",
    "id" : 921179914975322112,
    "created_at" : "2017-10-20 01:03:01 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 922491491532132352,
  "created_at" : "2017-10-23 15:54:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/zfSWGJwKES",
      "expanded_url" : "http:\/\/bit.ly\/2z1iRF5",
      "display_url" : "bit.ly\/2z1iRF5"
    }, {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/N9aUPUwz7P",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918048862853652480",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922403578903396352",
  "text" : "Decided to make available the marketing plan I submitted for an interview few months ago. https:\/\/t.co\/zfSWGJwKES https:\/\/t.co\/N9aUPUwz7P",
  "id" : 922403578903396352,
  "created_at" : "2017-10-23 10:05:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 135, 158 ],
      "url" : "https:\/\/t.co\/tH9ujpcqnO",
      "expanded_url" : "https:\/\/twitter.com\/dempseam\/status\/922395571830378496",
      "display_url" : "twitter.com\/dempseam\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922401963458859008",
  "text" : "This is one thing I find when I first reached here. I thought I reached some town called O'Connell but was told this is the Capital... https:\/\/t.co\/tH9ujpcqnO",
  "id" : 922401963458859008,
  "created_at" : "2017-10-23 09:59:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "indices" : [ 3, 16 ],
      "id_str" : "768968",
      "id" : 768968
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/coolinsights\/status\/922079765565181953\/photo\/1",
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/lA6F1iu5NG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMviqPMXUAEFvmL.jpg",
      "id_str" : "922079763988172801",
      "id" : 922079763988172801,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMviqPMXUAEFvmL.jpg",
      "sizes" : [ {
        "h" : 719,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 719,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 719,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 473
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lA6F1iu5NG"
    } ],
    "hashtags" : [ {
      "text" : "contentmarketing",
      "indices" : [ 50, 67 ]
    } ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/vgmb397zPw",
      "expanded_url" : "http:\/\/coolerinsights.com\/2012\/01\/marketing-metaphoria-book-review\/",
      "display_url" : "coolerinsights.com\/2012\/01\/market\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922388191369482240",
  "text" : "RT @coolinsights: 7 ways to use deep metaphors in #contentmarketing https:\/\/t.co\/vgmb397zPw https:\/\/t.co\/lA6F1iu5NG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitshot.com\" rel=\"nofollow\"\u003ETwitshot.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/coolinsights\/status\/922079765565181953\/photo\/1",
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/lA6F1iu5NG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMviqPMXUAEFvmL.jpg",
        "id_str" : "922079763988172801",
        "id" : 922079763988172801,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMviqPMXUAEFvmL.jpg",
        "sizes" : [ {
          "h" : 719,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 719,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 719,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 473
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/lA6F1iu5NG"
      } ],
      "hashtags" : [ {
        "text" : "contentmarketing",
        "indices" : [ 32, 49 ]
      } ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/vgmb397zPw",
        "expanded_url" : "http:\/\/coolerinsights.com\/2012\/01\/marketing-metaphoria-book-review\/",
        "display_url" : "coolerinsights.com\/2012\/01\/market\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "922079765565181953",
    "text" : "7 ways to use deep metaphors in #contentmarketing https:\/\/t.co\/vgmb397zPw https:\/\/t.co\/lA6F1iu5NG",
    "id" : 922079765565181953,
    "created_at" : "2017-10-22 12:38:42 +0000",
    "user" : {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "protected" : false,
      "id_str" : "768968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665181228841046016\/dvZ4PiEU_normal.jpg",
      "id" : 768968,
      "verified" : false
    }
  },
  "id" : 922388191369482240,
  "created_at" : "2017-10-23 09:04:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/R3aPdDvGMU",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/922187272048562176",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922372603884564480",
  "text" : "Ability to work on any OS as well as in the cloud is often a good litmus test of Docker. https:\/\/t.co\/R3aPdDvGMU",
  "id" : 922372603884564480,
  "created_at" : "2017-10-23 08:02:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reductress",
      "screen_name" : "Reductress",
      "indices" : [ 3, 14 ],
      "id_str" : "1090026433",
      "id" : 1090026433
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Reductress\/status\/922148809337077761\/photo\/1",
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/72QwIbmDXp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMwhdBZXkAEAHuQ.jpg",
      "id_str" : "922148806178869249",
      "id" : 922148806178869249,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMwhdBZXkAEAHuQ.jpg",
      "sizes" : [ {
        "h" : 500,
        "resize" : "fit",
        "w" : 820
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 415,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 820
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 820
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/72QwIbmDXp"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/sTwVoFCr6k",
      "expanded_url" : "http:\/\/bit.ly\/2yE4Iwp",
      "display_url" : "bit.ly\/2yE4Iwp"
    } ]
  },
  "geo" : { },
  "id_str" : "922352332490371072",
  "text" : "RT @Reductress: White Friend More Intolerant of Gluten Than Racism: https:\/\/t.co\/sTwVoFCr6k https:\/\/t.co\/72QwIbmDXp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Reductress\/status\/922148809337077761\/photo\/1",
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/72QwIbmDXp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMwhdBZXkAEAHuQ.jpg",
        "id_str" : "922148806178869249",
        "id" : 922148806178869249,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMwhdBZXkAEAHuQ.jpg",
        "sizes" : [ {
          "h" : 500,
          "resize" : "fit",
          "w" : 820
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 415,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 820
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 820
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/72QwIbmDXp"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 52, 75 ],
        "url" : "https:\/\/t.co\/sTwVoFCr6k",
        "expanded_url" : "http:\/\/bit.ly\/2yE4Iwp",
        "display_url" : "bit.ly\/2yE4Iwp"
      } ]
    },
    "geo" : { },
    "id_str" : "922148809337077761",
    "text" : "White Friend More Intolerant of Gluten Than Racism: https:\/\/t.co\/sTwVoFCr6k https:\/\/t.co\/72QwIbmDXp",
    "id" : 922148809337077761,
    "created_at" : "2017-10-22 17:13:03 +0000",
    "user" : {
      "name" : "Reductress",
      "screen_name" : "Reductress",
      "protected" : false,
      "id_str" : "1090026433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/413509753185128448\/gvk-A4qp_normal.jpeg",
      "id" : 1090026433,
      "verified" : true
    }
  },
  "id" : 922352332490371072,
  "created_at" : "2017-10-23 06:41:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/922187272048562176\/photo\/1",
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/llMx6JXliw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMxAZoCW0AASuuv.jpg",
      "id_str" : "922182832692318208",
      "id" : 922182832692318208,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMxAZoCW0AASuuv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 323,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 649,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 649,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 570,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/llMx6JXliw"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922187272048562176",
  "text" : "My first push: Using command line to upload (push) a Jupyter notebook with Python, a programming language for data analysis. https:\/\/t.co\/llMx6JXliw",
  "id" : 922187272048562176,
  "created_at" : "2017-10-22 19:45:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E50",
      "screen_name" : "shimmertje8",
      "indices" : [ 3, 15 ],
      "id_str" : "237988530",
      "id" : 237988530
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/B8vhOH6V30",
      "expanded_url" : "https:\/\/mothership.sg\/2017\/10\/here-are-some-awkwardly-unnecessary-things-singaporeans-do-in-front-of-persons-with-disabilities\/",
      "display_url" : "mothership.sg\/2017\/10\/here-a\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922107820585168896",
  "text" : "RT @shimmertje8: Here are some awkwardly unnecessary things Singaporeans do in front of persons with disabilities https:\/\/t.co\/B8vhOH6V30",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/B8vhOH6V30",
        "expanded_url" : "https:\/\/mothership.sg\/2017\/10\/here-are-some-awkwardly-unnecessary-things-singaporeans-do-in-front-of-persons-with-disabilities\/",
        "display_url" : "mothership.sg\/2017\/10\/here-a\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "921890899327565824",
    "text" : "Here are some awkwardly unnecessary things Singaporeans do in front of persons with disabilities https:\/\/t.co\/B8vhOH6V30",
    "id" : 921890899327565824,
    "created_at" : "2017-10-22 00:08:12 +0000",
    "user" : {
      "name" : "\u4E50",
      "screen_name" : "shimmertje8",
      "protected" : false,
      "id_str" : "237988530",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992965034992058370\/Dh35z6Z3_normal.jpg",
      "id" : 237988530,
      "verified" : false
    }
  },
  "id" : 922107820585168896,
  "created_at" : "2017-10-22 14:30:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Webb",
      "screen_name" : "arobertwebb",
      "indices" : [ 3, 15 ],
      "id_str" : "25087909",
      "id" : 25087909
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922107579156836358",
  "text" : "RT @arobertwebb: Anyone who expects to feel safe in a driverless car has never owned a printer.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921461512383926273",
    "text" : "Anyone who expects to feel safe in a driverless car has never owned a printer.",
    "id" : 921461512383926273,
    "created_at" : "2017-10-20 19:41:59 +0000",
    "user" : {
      "name" : "Robert Webb",
      "screen_name" : "arobertwebb",
      "protected" : false,
      "id_str" : "25087909",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841045200831143936\/LDbmEo4t_normal.jpg",
      "id" : 25087909,
      "verified" : true
    }
  },
  "id" : 922107579156836358,
  "created_at" : "2017-10-22 14:29:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922106956894101505",
  "text" : "The Chinese translation of \"draw ing line in the sand\" is \u695A\u6CB3\u6C49\u754C\uFF1F",
  "id" : 922106956894101505,
  "created_at" : "2017-10-22 14:26:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stephanie Hurlburt",
      "screen_name" : "sehurlburt",
      "indices" : [ 3, 14 ],
      "id_str" : "2759769840",
      "id" : 2759769840
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922071295696793600",
  "text" : "RT @sehurlburt: \u201CWhy isn\u2019t someone using my software product or open source tool? It\u2019s good!\u201D\n\nA checklist for you:",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921921604140937216",
    "text" : "\u201CWhy isn\u2019t someone using my software product or open source tool? It\u2019s good!\u201D\n\nA checklist for you:",
    "id" : 921921604140937216,
    "created_at" : "2017-10-22 02:10:13 +0000",
    "user" : {
      "name" : "Stephanie Hurlburt",
      "screen_name" : "sehurlburt",
      "protected" : false,
      "id_str" : "2759769840",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/958787031051612160\/b22Sy_uC_normal.jpg",
      "id" : 2759769840,
      "verified" : false
    }
  },
  "id" : 922071295696793600,
  "created_at" : "2017-10-22 12:05:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "indices" : [ 3, 17 ],
      "id_str" : "14157788",
      "id" : 14157788
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/iheartbeijing\/status\/921994127130136576\/photo\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/OdKRWrKIvh",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMuUxX3WkAAL50_.jpg",
      "id_str" : "921994124668080128",
      "id" : 921994124668080128,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMuUxX3WkAAL50_.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OdKRWrKIvh"
    } ],
    "hashtags" : [ {
      "text" : "China",
      "indices" : [ 92, 98 ]
    }, {
      "text" : "condoms",
      "indices" : [ 99, 107 ]
    }, {
      "text" : "funny",
      "indices" : [ 108, 114 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922047076367814662",
  "text" : "RT @iheartbeijing: Certainly a different (tho not incorrect?) interpretation of \"oral care\" #China #condoms #funny https:\/\/t.co\/OdKRWrKIvh",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iheartbeijing\/status\/921994127130136576\/photo\/1",
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/OdKRWrKIvh",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMuUxX3WkAAL50_.jpg",
        "id_str" : "921994124668080128",
        "id" : 921994124668080128,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMuUxX3WkAAL50_.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/OdKRWrKIvh"
      } ],
      "hashtags" : [ {
        "text" : "China",
        "indices" : [ 73, 79 ]
      }, {
        "text" : "condoms",
        "indices" : [ 80, 88 ]
      }, {
        "text" : "funny",
        "indices" : [ 89, 95 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921994127130136576",
    "text" : "Certainly a different (tho not incorrect?) interpretation of \"oral care\" #China #condoms #funny https:\/\/t.co\/OdKRWrKIvh",
    "id" : 921994127130136576,
    "created_at" : "2017-10-22 06:58:24 +0000",
    "user" : {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "protected" : false,
      "id_str" : "14157788",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469468312292712448\/kKTKW0ON_normal.jpeg",
      "id" : 14157788,
      "verified" : false
    }
  },
  "id" : 922047076367814662,
  "created_at" : "2017-10-22 10:28:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff",
      "screen_name" : "5stringTex",
      "indices" : [ 3, 14 ],
      "id_str" : "2173906535",
      "id" : 2173906535
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/5stringTex\/status\/921856580932562944\/photo\/1",
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/Bbn0PbNjft",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMsXmNmVwAEPJzI.jpg",
      "id_str" : "921856493980336129",
      "id" : 921856493980336129,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMsXmNmVwAEPJzI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1002,
        "resize" : "fit",
        "w" : 564
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1002,
        "resize" : "fit",
        "w" : 564
      }, {
        "h" : 1002,
        "resize" : "fit",
        "w" : 564
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Bbn0PbNjft"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "922046658543800321",
  "text" : "RT @5stringTex: LOTR in the 21st century. https:\/\/t.co\/Bbn0PbNjft",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/5stringTex\/status\/921856580932562944\/photo\/1",
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/Bbn0PbNjft",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMsXmNmVwAEPJzI.jpg",
        "id_str" : "921856493980336129",
        "id" : 921856493980336129,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMsXmNmVwAEPJzI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1002,
          "resize" : "fit",
          "w" : 564
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1002,
          "resize" : "fit",
          "w" : 564
        }, {
          "h" : 1002,
          "resize" : "fit",
          "w" : 564
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Bbn0PbNjft"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921856580932562944",
    "text" : "LOTR in the 21st century. https:\/\/t.co\/Bbn0PbNjft",
    "id" : 921856580932562944,
    "created_at" : "2017-10-21 21:51:50 +0000",
    "user" : {
      "name" : "Jeff",
      "screen_name" : "5stringTex",
      "protected" : false,
      "id_str" : "2173906535",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1018643136875147264\/XdyYn0v__normal.jpg",
      "id" : 2173906535,
      "verified" : false
    }
  },
  "id" : 922046658543800321,
  "created_at" : "2017-10-22 10:27:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shannon Forrest",
      "screen_name" : "waittilitellya",
      "indices" : [ 3, 18 ],
      "id_str" : "275752356",
      "id" : 275752356
    }, {
      "name" : "Irish Food Board",
      "screen_name" : "Bordbia",
      "indices" : [ 106, 114 ],
      "id_str" : "48673814",
      "id" : 48673814
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "food",
      "indices" : [ 61, 66 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 97, 105 ]
    } ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/ZkBqJgaCFb",
      "expanded_url" : "https:\/\/twitter.com\/business\/status\/921939340892213249",
      "display_url" : "twitter.com\/business\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "922046187238240256",
  "text" : "RT @waittilitellya: Our incredible little island is the most #food secure nation in the world! \uD83C\uDF7D #Ireland @Bordbia https:\/\/t.co\/ZkBqJgaCFb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Food Board",
        "screen_name" : "Bordbia",
        "indices" : [ 86, 94 ],
        "id_str" : "48673814",
        "id" : 48673814
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "food",
        "indices" : [ 41, 46 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 77, 85 ]
      } ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/ZkBqJgaCFb",
        "expanded_url" : "https:\/\/twitter.com\/business\/status\/921939340892213249",
        "display_url" : "twitter.com\/business\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "922014618066870274",
    "text" : "Our incredible little island is the most #food secure nation in the world! \uD83C\uDF7D #Ireland @Bordbia https:\/\/t.co\/ZkBqJgaCFb",
    "id" : 922014618066870274,
    "created_at" : "2017-10-22 08:19:49 +0000",
    "user" : {
      "name" : "Shannon Forrest",
      "screen_name" : "waittilitellya",
      "protected" : false,
      "id_str" : "275752356",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947946881837039617\/a0drBLiz_normal.jpg",
      "id" : 275752356,
      "verified" : false
    }
  },
  "id" : 922046187238240256,
  "created_at" : "2017-10-22 10:25:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Iya Whiteley",
      "screen_name" : "IyaWhiteley",
      "indices" : [ 3, 15 ],
      "id_str" : "854705276179664896",
      "id" : 854705276179664896
    }, {
      "name" : "BBC Science and Environment News",
      "screen_name" : "BBCScienceNews",
      "indices" : [ 36, 51 ],
      "id_str" : "621573",
      "id" : 621573
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Science",
      "indices" : [ 17, 25 ]
    }, {
      "text" : "Space",
      "indices" : [ 85, 91 ]
    }, {
      "text" : "Universe",
      "indices" : [ 92, 101 ]
    } ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/TJ2RTQxHIk",
      "expanded_url" : "https:\/\/goo.gl\/C9QLjX",
      "display_url" : "goo.gl\/C9QLjX"
    } ]
  },
  "geo" : { },
  "id_str" : "922045665626214400",
  "text" : "RT @IyaWhiteley: #Science news from @BBCScienceNews. The place spacecraft go to die! #Space #Universe\nhttps:\/\/t.co\/TJ2RTQxHIk",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC Science and Environment News",
        "screen_name" : "BBCScienceNews",
        "indices" : [ 19, 34 ],
        "id_str" : "621573",
        "id" : 621573
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Science",
        "indices" : [ 0, 8 ]
      }, {
        "text" : "Space",
        "indices" : [ 68, 74 ]
      }, {
        "text" : "Universe",
        "indices" : [ 75, 84 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/TJ2RTQxHIk",
        "expanded_url" : "https:\/\/goo.gl\/C9QLjX",
        "display_url" : "goo.gl\/C9QLjX"
      } ]
    },
    "geo" : { },
    "id_str" : "922008956192649216",
    "text" : "#Science news from @BBCScienceNews. The place spacecraft go to die! #Space #Universe\nhttps:\/\/t.co\/TJ2RTQxHIk",
    "id" : 922008956192649216,
    "created_at" : "2017-10-22 07:57:19 +0000",
    "user" : {
      "name" : "Dr Iya Whiteley",
      "screen_name" : "IyaWhiteley",
      "protected" : false,
      "id_str" : "854705276179664896",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/872828182231228417\/8ee8wYpg_normal.jpg",
      "id" : 854705276179664896,
      "verified" : false
    }
  },
  "id" : 922045665626214400,
  "created_at" : "2017-10-22 10:23:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Financial Times",
      "screen_name" : "FT",
      "indices" : [ 86, 89 ],
      "id_str" : "18949452",
      "id" : 18949452
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/ip2lmjzFY1",
      "expanded_url" : "https:\/\/www.ft.com\/content\/14317238-b594-11e7-aa26-bb002965bce8",
      "display_url" : "ft.com\/content\/143172\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921883638081511424",
  "text" : "UK schools teach maths Singapore-style to boost standards https:\/\/t.co\/ip2lmjzFY1 via @FT",
  "id" : 921883638081511424,
  "created_at" : "2017-10-21 23:39:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/fBtdb854c8",
      "expanded_url" : "https:\/\/arxiv.org\/abs\/1410.0846",
      "display_url" : "arxiv.org\/abs\/1410.0846"
    } ]
  },
  "in_reply_to_status_id_str" : "921850073927086080",
  "geo" : { },
  "id_str" : "921850195045961728",
  "in_reply_to_user_id" : 9465632,
  "text" : "An introduction to Docker for reproducible research, with examples from the R environment https:\/\/t.co\/fBtdb854c8",
  "id" : 921850195045961728,
  "in_reply_to_status_id" : 921850073927086080,
  "created_at" : "2017-10-21 21:26:28 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921850073927086080",
  "text" : "So Docker can act like a virtualenv? Different computing setup\/environment on the same PC and serve as an reproducible research tools",
  "id" : 921850073927086080,
  "created_at" : "2017-10-21 21:25:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "indices" : [ 130, 143 ],
      "id_str" : "2300614526",
      "id" : 2300614526
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/MdOFMBoZF0",
      "expanded_url" : "http:\/\/pacha.hk\/2017-10-20_r_on_ubuntu_17_10.html",
      "display_url" : "pacha.hk\/2017-10-20_r_o\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921813375130243073",
  "text" : "Save yourself a headache &amp; valuable time by using this script to quickly Install R on Ubuntu 17.10 https:\/\/t.co\/MdOFMBoZF0 by @pachamaltese",
  "id" : 921813375130243073,
  "created_at" : "2017-10-21 19:00:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Sandra Collins",
      "screen_name" : "SandriCollins",
      "indices" : [ 3, 17 ],
      "id_str" : "902464315",
      "id" : 902464315
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921805636144390146",
  "text" : "RT @SandriCollins: I am really looking forward to this! The Irishman who broke the Nazi code - Documentary on One preview https:\/\/t.co\/J5Q2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RT\u00C9",
        "screen_name" : "rte",
        "indices" : [ 131, 135 ],
        "id_str" : "1245699895",
        "id" : 1245699895
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/J5Q2WnvRzi",
        "expanded_url" : "http:\/\/www.rte.ie\/culture\/2017\/1020\/913994-documentary-on-one-richard-hayes-nazi-codebreaker\/",
        "display_url" : "rte.ie\/culture\/2017\/1\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "921420127928713216",
    "text" : "I am really looking forward to this! The Irishman who broke the Nazi code - Documentary on One preview https:\/\/t.co\/J5Q2WnvRzi via @rte",
    "id" : 921420127928713216,
    "created_at" : "2017-10-20 16:57:32 +0000",
    "user" : {
      "name" : "Dr Sandra Collins",
      "screen_name" : "SandriCollins",
      "protected" : false,
      "id_str" : "902464315",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/603641316535365636\/SXmnFNKc_normal.jpg",
      "id" : 902464315,
      "verified" : false
    }
  },
  "id" : 921805636144390146,
  "created_at" : "2017-10-21 18:29:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "datajournalism",
      "indices" : [ 22, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/C3XqckA0g5",
      "expanded_url" : "https:\/\/github.com\/collections\/open-journalism",
      "display_url" : "github.com\/collections\/op\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921797334576615427",
  "text" : "Uses cases related to #datajournalism https:\/\/t.co\/C3XqckA0g5",
  "id" : 921797334576615427,
  "created_at" : "2017-10-21 17:56:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernard Leong",
      "screen_name" : "bernardleong",
      "indices" : [ 3, 16 ],
      "id_str" : "2193071",
      "id" : 2193071
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/aadHS6ftXq",
      "expanded_url" : "http:\/\/firstround.com\/review\/these-13-exercises-will-prepare-you-for-works-toughest-situations\/?utm_content=ShareEntry-side&utm_source=twitter&utm_medium=social#rt_u=1452148697_PMuBnH",
      "display_url" : "firstround.com\/review\/these-1\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921793507819446274",
  "text" : "RT @bernardleong: These 13 Exercises Will Prepare You for Work's Toughest Situations \u2013 https:\/\/t.co\/aadHS6ftXq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/aadHS6ftXq",
        "expanded_url" : "http:\/\/firstround.com\/review\/these-13-exercises-will-prepare-you-for-works-toughest-situations\/?utm_content=ShareEntry-side&utm_source=twitter&utm_medium=social#rt_u=1452148697_PMuBnH",
        "display_url" : "firstround.com\/review\/these-1\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920389875337019392",
    "text" : "These 13 Exercises Will Prepare You for Work's Toughest Situations \u2013 https:\/\/t.co\/aadHS6ftXq",
    "id" : 920389875337019392,
    "created_at" : "2017-10-17 20:43:40 +0000",
    "user" : {
      "name" : "Bernard Leong",
      "screen_name" : "bernardleong",
      "protected" : false,
      "id_str" : "2193071",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/750746934801944576\/wFn6x_6g_normal.jpg",
      "id" : 2193071,
      "verified" : true
    }
  },
  "id" : 921793507819446274,
  "created_at" : "2017-10-21 17:41:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frank Prendergast",
      "screen_name" : "frankiep",
      "indices" : [ 3, 12 ],
      "id_str" : "12395472",
      "id" : 12395472
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/acf1MDt78r",
      "expanded_url" : "https:\/\/www.boredpanda.com\/people-matching-artworks-stefan-draschan\/",
      "display_url" : "boredpanda.com\/people-matchin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921791507597877248",
  "text" : "RT @frankiep: This is just fantastic.\nhttps:\/\/t.co\/acf1MDt78r",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 24, 47 ],
        "url" : "https:\/\/t.co\/acf1MDt78r",
        "expanded_url" : "https:\/\/www.boredpanda.com\/people-matching-artworks-stefan-draschan\/",
        "display_url" : "boredpanda.com\/people-matchin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "921153554831806464",
    "text" : "This is just fantastic.\nhttps:\/\/t.co\/acf1MDt78r",
    "id" : 921153554831806464,
    "created_at" : "2017-10-19 23:18:16 +0000",
    "user" : {
      "name" : "Frank Prendergast",
      "screen_name" : "frankiep",
      "protected" : false,
      "id_str" : "12395472",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014861886154780672\/-vERdGD0_normal.jpg",
      "id" : 12395472,
      "verified" : false
    }
  },
  "id" : 921791507597877248,
  "created_at" : "2017-10-21 17:33:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Naveed",
      "screen_name" : "Seditious_medic",
      "indices" : [ 3, 19 ],
      "id_str" : "3018840016",
      "id" : 3018840016
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921783403153969153",
  "text" : "RT @Seditious_medic: So this is the waiting room of a dental clinic. Desperately hoping Gynaecologists don\u2019t copy this idea. https:\/\/t.co\/C\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Seditious_medic\/status\/921306398323077120\/photo\/1",
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/CylRda7hhI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMkjSCVXkAAPazk.jpg",
        "id_str" : "921306391545090048",
        "id" : 921306391545090048,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMkjSCVXkAAPazk.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CylRda7hhI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921306398323077120",
    "text" : "So this is the waiting room of a dental clinic. Desperately hoping Gynaecologists don\u2019t copy this idea. https:\/\/t.co\/CylRda7hhI",
    "id" : 921306398323077120,
    "created_at" : "2017-10-20 09:25:37 +0000",
    "user" : {
      "name" : "Naveed",
      "screen_name" : "Seditious_medic",
      "protected" : false,
      "id_str" : "3018840016",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033055796001681408\/IZ85P__f_normal.jpg",
      "id" : 3018840016,
      "verified" : false
    }
  },
  "id" : 921783403153969153,
  "created_at" : "2017-10-21 17:01:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    }, {
      "name" : "President of Ireland",
      "screen_name" : "PresidentIRL",
      "indices" : [ 97, 110 ],
      "id_str" : "569892832",
      "id" : 569892832
    }, {
      "name" : "TCD Economics",
      "screen_name" : "tcdeconomics",
      "indices" : [ 120, 133 ],
      "id_str" : "3046665274",
      "id" : 3046665274
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921716408471474177",
  "text" : "RT @ronanlyons: Hard not to take this personally when it's an attack on the people. I would love @PresidentIRL to visit @tcdeconomics or ot\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "President of Ireland",
        "screen_name" : "PresidentIRL",
        "indices" : [ 81, 94 ],
        "id_str" : "569892832",
        "id" : 569892832
      }, {
        "name" : "TCD Economics",
        "screen_name" : "tcdeconomics",
        "indices" : [ 104, 117 ],
        "id_str" : "3046665274",
        "id" : 3046665274
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 204, 227 ],
        "url" : "https:\/\/t.co\/dZ6Kw9NB8d",
        "expanded_url" : "https:\/\/twitter.com\/thetimesIE\/status\/921682489621532672",
        "display_url" : "twitter.com\/thetimesIE\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "921704069722501120",
    "text" : "Hard not to take this personally when it's an attack on the people. I would love @PresidentIRL to visit @tcdeconomics or other Depts and see what economics actually is - he's in for a big surprise. (1\/3) https:\/\/t.co\/dZ6Kw9NB8d",
    "id" : 921704069722501120,
    "created_at" : "2017-10-21 11:45:49 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 921716408471474177,
  "created_at" : "2017-10-21 12:34:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/921715551445176321\/photo\/1",
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/OXx27YNrJ3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMqXaLxXkAAMFVs.jpg",
      "id_str" : "921715549842935808",
      "id" : 921715549842935808,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMqXaLxXkAAMFVs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OXx27YNrJ3"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/eqsFRkEsJd",
      "expanded_url" : "http:\/\/ift.tt\/2xbXtIt",
      "display_url" : "ift.tt\/2xbXtIt"
    } ]
  },
  "geo" : { },
  "id_str" : "921715551445176321",
  "text" : "How do you find out about our Lemon Meringue pie and Red Velvet? In a toilet cubicle while.... https:\/\/t.co\/eqsFRkEsJd https:\/\/t.co\/OXx27YNrJ3",
  "id" : 921715551445176321,
  "created_at" : "2017-10-21 12:31:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Shower",
      "screen_name" : "SJSchauer",
      "indices" : [ 3, 13 ],
      "id_str" : "250887629",
      "id" : 250887629
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/yYZVyWYTLc",
      "expanded_url" : "https:\/\/twitter.com\/realdonaldtrump\/status\/331907383771148288",
      "display_url" : "twitter.com\/realdonaldtrum\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921513378811662336",
  "text" : "RT @SJSchauer: RT if you've worked with the opposite sex and not raped them https:\/\/t.co\/yYZVyWYTLc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/yYZVyWYTLc",
        "expanded_url" : "https:\/\/twitter.com\/realdonaldtrump\/status\/331907383771148288",
        "display_url" : "twitter.com\/realdonaldtrum\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920833392043806720",
    "text" : "RT if you've worked with the opposite sex and not raped them https:\/\/t.co\/yYZVyWYTLc",
    "id" : 920833392043806720,
    "created_at" : "2017-10-19 02:06:03 +0000",
    "user" : {
      "name" : "Sarah Shower",
      "screen_name" : "SJSchauer",
      "protected" : false,
      "id_str" : "250887629",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033800905047584768\/4NFHaDzv_normal.jpg",
      "id" : 250887629,
      "verified" : true
    }
  },
  "id" : 921513378811662336,
  "created_at" : "2017-10-20 23:08:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/RqjLPrXsDv",
      "expanded_url" : "http:\/\/m.independent.ie\/life\/travel\/world\/10-super-things-to-do-in-singapore-without-breaking-the-budget-36241694.html",
      "display_url" : "m.independent.ie\/life\/travel\/wo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921512997058744320",
  "text" : "https:\/\/t.co\/RqjLPrXsDv",
  "id" : 921512997058744320,
  "created_at" : "2017-10-20 23:06:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "indices" : [ 3, 11 ],
      "id_str" : "20697268",
      "id" : 20697268
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BBC4",
      "indices" : [ 113, 118 ]
    }, {
      "text" : "GreatAmericanAnthems",
      "indices" : [ 119, 140 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921509383409938433",
  "text" : "RT @KJoanne: 20 years later and I still don\u2019t have a fockin clue what are the lyrics to Smells Like Teen Spirit. #BBC4 #GreatAmericanAnthems",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BBC4",
        "indices" : [ 100, 105 ]
      }, {
        "text" : "GreatAmericanAnthems",
        "indices" : [ 106, 127 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921508928978071552",
    "text" : "20 years later and I still don\u2019t have a fockin clue what are the lyrics to Smells Like Teen Spirit. #BBC4 #GreatAmericanAnthems",
    "id" : 921508928978071552,
    "created_at" : "2017-10-20 22:50:24 +0000",
    "user" : {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "protected" : false,
      "id_str" : "20697268",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005549984522043392\/YDQyk6cL_normal.jpg",
      "id" : 20697268,
      "verified" : false
    }
  },
  "id" : 921509383409938433,
  "created_at" : "2017-10-20 22:52:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/921507625325522945\/photo\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/3IXu76GyVr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMnaTTEXUAAy9NX.jpg",
      "id_str" : "921507623844925440",
      "id" : 921507623844925440,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMnaTTEXUAAy9NX.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3IXu76GyVr"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/WbuYEYVcQj",
      "expanded_url" : "http:\/\/ift.tt\/2gVJvnX",
      "display_url" : "ift.tt\/2gVJvnX"
    } ]
  },
  "geo" : { },
  "id_str" : "921507625325522945",
  "text" : "She is on Graham Norton's show tonight. https:\/\/t.co\/WbuYEYVcQj https:\/\/t.co\/3IXu76GyVr",
  "id" : 921507625325522945,
  "created_at" : "2017-10-20 22:45:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921397819512061952",
  "text" : "What is the easiest way to deploy a machine learning model (say a regression) for production? Thanks",
  "id" : 921397819512061952,
  "created_at" : "2017-10-20 15:28:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/aPBPNf1QoQ",
      "expanded_url" : "http:\/\/blog.flickr.net\/2017\/10\/16\/unleash-your-creativity-with-new-blurb-photo-books\/",
      "display_url" : "blog.flickr.net\/2017\/10\/16\/unl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921397338383450112",
  "text" : "Flickr is shutting down their wall art offering. https:\/\/t.co\/aPBPNf1QoQ",
  "id" : 921397338383450112,
  "created_at" : "2017-10-20 15:26:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/nHmPtSO91i",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/921390435368611841",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "921392393215201282",
  "text" : "In the cloud (Azure notebook) the package that read the Excel file is not available (for R version 3.3.0) https:\/\/t.co\/nHmPtSO91i",
  "id" : 921392393215201282,
  "created_at" : "2017-10-20 15:07:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/921390435368611841\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/PerbBxJv7C",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMluopPWkAAP26X.jpg",
      "id_str" : "921389243318046720",
      "id" : 921389243318046720,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMluopPWkAAP26X.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 367,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 738,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 738,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 648,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PerbBxJv7C"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921390435368611841",
  "text" : "RStudio in Docker on my PC struggled to read a 22.6MB excel files (541910 rows and 8 columns). https:\/\/t.co\/PerbBxJv7C",
  "id" : 921390435368611841,
  "created_at" : "2017-10-20 14:59:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921386159128961024",
  "text" : "R struggled to read a 22.6MB excel files (541910 rows and 8 columns)",
  "id" : 921386159128961024,
  "created_at" : "2017-10-20 14:42:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ina O' Murchu",
      "screen_name" : "Ina",
      "indices" : [ 0, 4 ],
      "id_str" : "617623",
      "id" : 617623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "921362452650000384",
  "geo" : { },
  "id_str" : "921384508720599048",
  "in_reply_to_user_id" : 617623,
  "text" : "@Ina meanwhile Heavy rain spreading East across the country.",
  "id" : 921384508720599048,
  "in_reply_to_status_id" : 921362452650000384,
  "created_at" : "2017-10-20 14:36:00 +0000",
  "in_reply_to_screen_name" : "Ina",
  "in_reply_to_user_id_str" : "617623",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/NvrGkkKWUt",
      "expanded_url" : "http:\/\/cnet.co\/2yD50nk",
      "display_url" : "cnet.co\/2yD50nk"
    } ]
  },
  "geo" : { },
  "id_str" : "921350034939174912",
  "text" : "\uD83D\uDE02Will Ferrell bitingly mocks how much devices control us https:\/\/t.co\/NvrGkkKWUt",
  "id" : 921350034939174912,
  "created_at" : "2017-10-20 12:19:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/921323132673232896\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/4ryf0CdvhA",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMkye9HX4AAkdHi.jpg",
      "id_str" : "921323106156929024",
      "id" : 921323106156929024,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMkye9HX4AAkdHi.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4ryf0CdvhA"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921323132673232896",
  "text" : "Find ways to prove your business before you code. https:\/\/t.co\/4ryf0CdvhA",
  "id" : 921323132673232896,
  "created_at" : "2017-10-20 10:32:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921303783963054080",
  "text" : "For enduser, Docker offers software on demand without messing up their PC system. Am I right?",
  "id" : 921303783963054080,
  "created_at" : "2017-10-20 09:15:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 0, 12 ],
      "id_str" : "85053026",
      "id" : 85053026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/IgrAnPAEL6",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=vdHBsWXaHN8",
      "display_url" : "youtube.com\/watch?v=vdHBsW\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "921301828016529408",
  "geo" : { },
  "id_str" : "921303457759457280",
  "in_reply_to_user_id" : 85053026,
  "text" : "@gavindbrown reminds me of that scene in Topgun...https:\/\/t.co\/IgrAnPAEL6",
  "id" : 921303457759457280,
  "in_reply_to_status_id" : 921301828016529408,
  "created_at" : "2017-10-20 09:13:55 +0000",
  "in_reply_to_screen_name" : "gavindbrown",
  "in_reply_to_user_id_str" : "85053026",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Prof Jackie Carter",
      "screen_name" : "JackieCarter",
      "indices" : [ 3, 16 ],
      "id_str" : "19656274",
      "id" : 19656274
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "datascience",
      "indices" : [ 36, 48 ]
    }, {
      "text" : "gender",
      "indices" : [ 119, 126 ]
    }, {
      "text" : "equality",
      "indices" : [ 127, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921271829494468608",
  "text" : "RT @JackieCarter: All male panel at #datascience event I attended yesterday. Call them out. We can do better than this #gender #equality #M\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JackieCarter\/status\/921271562745139200\/photo\/1",
        "indices" : [ 127, 150 ],
        "url" : "https:\/\/t.co\/K1DQsjxQCe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMkDlYFXcAAI_qy.jpg",
        "id_str" : "921271539428978688",
        "id" : 921271539428978688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMkDlYFXcAAI_qy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/K1DQsjxQCe"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/JackieCarter\/status\/921271562745139200\/photo\/1",
        "indices" : [ 127, 150 ],
        "url" : "https:\/\/t.co\/K1DQsjxQCe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMkDlYJW0AYfvy7.jpg",
        "id_str" : "921271539445714950",
        "id" : 921271539445714950,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMkDlYJW0AYfvy7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 382
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/K1DQsjxQCe"
      } ],
      "hashtags" : [ {
        "text" : "datascience",
        "indices" : [ 18, 30 ]
      }, {
        "text" : "gender",
        "indices" : [ 101, 108 ]
      }, {
        "text" : "equality",
        "indices" : [ 109, 118 ]
      }, {
        "text" : "Manels",
        "indices" : [ 119, 126 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921271562745139200",
    "text" : "All male panel at #datascience event I attended yesterday. Call them out. We can do better than this #gender #equality #Manels https:\/\/t.co\/K1DQsjxQCe",
    "id" : 921271562745139200,
    "created_at" : "2017-10-20 07:07:11 +0000",
    "user" : {
      "name" : "Prof Jackie Carter",
      "screen_name" : "JackieCarter",
      "protected" : false,
      "id_str" : "19656274",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1345208779\/Jackie_5_normal.JPG",
      "id" : 19656274,
      "verified" : false
    }
  },
  "id" : 921271829494468608,
  "created_at" : "2017-10-20 07:08:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Melville",
      "screen_name" : "JamesMelville",
      "indices" : [ 3, 17 ],
      "id_str" : "20675681",
      "id" : 20675681
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921270727797600261",
  "text" : "RT @JamesMelville: What a brilliant gesture by The Cornish Catering Company in Truro. \nFree tea or coffee for those who work in NHS, fire s\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JamesMelville\/status\/920967060024381446\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/RpLWyun7oo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMfupB2XkAA8WH3.jpg",
        "id_str" : "920967037459075072",
        "id" : 920967037459075072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMfupB2XkAA8WH3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RpLWyun7oo"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "920967060024381446",
    "text" : "What a brilliant gesture by The Cornish Catering Company in Truro. \nFree tea or coffee for those who work in NHS, fire services and police. https:\/\/t.co\/RpLWyun7oo",
    "id" : 920967060024381446,
    "created_at" : "2017-10-19 10:57:12 +0000",
    "user" : {
      "name" : "James Melville",
      "screen_name" : "JamesMelville",
      "protected" : false,
      "id_str" : "20675681",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039527740356132865\/IGI4GgQ8_normal.jpg",
      "id" : 20675681,
      "verified" : false
    }
  },
  "id" : 921270727797600261,
  "created_at" : "2017-10-20 07:03:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Wanstrath",
      "screen_name" : "defunkt",
      "indices" : [ 3, 11 ],
      "id_str" : "713263",
      "id" : 713263
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921257827863318528",
  "text" : "RT @defunkt: Ten years ago we made the first commit to GitHub. Thank you for helping turn this little side project into what it is today.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "921158180280983552",
    "text" : "Ten years ago we made the first commit to GitHub. Thank you for helping turn this little side project into what it is today.",
    "id" : 921158180280983552,
    "created_at" : "2017-10-19 23:36:39 +0000",
    "user" : {
      "name" : "Chris Wanstrath",
      "screen_name" : "defunkt",
      "protected" : false,
      "id_str" : "713263",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/771512986427068417\/eix-QRXH_normal.jpg",
      "id" : 713263,
      "verified" : false
    }
  },
  "id" : 921257827863318528,
  "created_at" : "2017-10-20 06:12:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kitematic",
      "screen_name" : "kitematic",
      "indices" : [ 5, 15 ],
      "id_str" : "2735249833",
      "id" : 2735249833
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921153338925899777",
  "text" : "Dear @kitematic  You are a star. \uD83D\uDC4F\uD83D\uDE4C",
  "id" : 921153338925899777,
  "created_at" : "2017-10-19 23:17:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "921151852774862851",
  "text" : "Sometimes you can get to know a politician when his name trending on Twitter...",
  "id" : 921151852774862851,
  "created_at" : "2017-10-19 23:11:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/h5bhsJEIe2",
      "expanded_url" : "https:\/\/github.com\/mryap\/rtb",
      "display_url" : "github.com\/mryap\/rtb"
    }, {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/VIvs1aDCLF",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/751887114233647106",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920994137050107904",
  "text" : "Just uploaded the data files and codes that pre-process the data. https:\/\/t.co\/h5bhsJEIe2 https:\/\/t.co\/VIvs1aDCLF",
  "id" : 920994137050107904,
  "created_at" : "2017-10-19 12:44:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "OMG! UBUNTU!",
      "screen_name" : "omgubuntu",
      "indices" : [ 116, 126 ],
      "id_str" : "72915446",
      "id" : 72915446
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/Y9tqQn1rx9",
      "expanded_url" : "http:\/\/www.omgubuntu.co.uk\/2017\/10\/ubuntu-17-10-release-features",
      "display_url" : "omgubuntu.co.uk\/2017\/10\/ubuntu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920993225699201025",
  "text" : "RT @edwinksl: Ubuntu 17.10 Now Available to Download, This Is What's New - OMG! Ubuntu! https:\/\/t.co\/Y9tqQn1rx9 via @omgubuntu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "OMG! UBUNTU!",
        "screen_name" : "omgubuntu",
        "indices" : [ 102, 112 ],
        "id_str" : "72915446",
        "id" : 72915446
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/Y9tqQn1rx9",
        "expanded_url" : "http:\/\/www.omgubuntu.co.uk\/2017\/10\/ubuntu-17-10-release-features",
        "display_url" : "omgubuntu.co.uk\/2017\/10\/ubuntu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920965155369955328",
    "text" : "Ubuntu 17.10 Now Available to Download, This Is What's New - OMG! Ubuntu! https:\/\/t.co\/Y9tqQn1rx9 via @omgubuntu",
    "id" : 920965155369955328,
    "created_at" : "2017-10-19 10:49:38 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 920993225699201025,
  "created_at" : "2017-10-19 12:41:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CSAIL at MIT",
      "screen_name" : "MIT_CSAIL",
      "indices" : [ 3, 13 ],
      "id_str" : "82364810",
      "id" : 82364810
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "womenofnasa",
      "indices" : [ 127, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/d9nJ9TlllQ",
      "expanded_url" : "http:\/\/bit.ly\/2yxzSoR",
      "display_url" : "bit.ly\/2yxzSoR"
    } ]
  },
  "geo" : { },
  "id_str" : "920990885122330624",
  "text" : "RT @MIT_CSAIL: On Nov. 1 the MIT coder who put a man on the moon will be getting her own LEGO figure!  https:\/\/t.co\/d9nJ9TlllQ #womenofnasa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MIT_CSAIL\/status\/920692047450910720\/photo\/1",
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/zIuIDVl8Ph",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMbz51DXcAANDPt.jpg",
        "id_str" : "920691348663136256",
        "id" : 920691348663136256,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMbz51DXcAANDPt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 714,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 379,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 714,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 669,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zIuIDVl8Ph"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MIT_CSAIL\/status\/920692047450910720\/photo\/1",
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/zIuIDVl8Ph",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMb0OD9X0AAwnzZ.jpg",
        "id_str" : "920691696261910528",
        "id" : 920691696261910528,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMb0OD9X0AAwnzZ.jpg",
        "sizes" : [ {
          "h" : 1882,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1103,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1882,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 625,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zIuIDVl8Ph"
      } ],
      "hashtags" : [ {
        "text" : "womenofnasa",
        "indices" : [ 112, 124 ]
      } ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/d9nJ9TlllQ",
        "expanded_url" : "http:\/\/bit.ly\/2yxzSoR",
        "display_url" : "bit.ly\/2yxzSoR"
      } ]
    },
    "geo" : { },
    "id_str" : "920692047450910720",
    "text" : "On Nov. 1 the MIT coder who put a man on the moon will be getting her own LEGO figure!  https:\/\/t.co\/d9nJ9TlllQ #womenofnasa https:\/\/t.co\/zIuIDVl8Ph",
    "id" : 920692047450910720,
    "created_at" : "2017-10-18 16:44:24 +0000",
    "user" : {
      "name" : "CSAIL at MIT",
      "screen_name" : "MIT_CSAIL",
      "protected" : false,
      "id_str" : "82364810",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885505956272115712\/U81HpDxb_normal.jpg",
      "id" : 82364810,
      "verified" : true
    }
  },
  "id" : 920990885122330624,
  "created_at" : "2017-10-19 12:31:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/Sd5PgiWvkm",
      "expanded_url" : "https:\/\/twitter.com\/gavindbrown\/status\/920969195512369153",
      "display_url" : "twitter.com\/gavindbrown\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920969811194236929",
  "text" : "Need to tweet to inform my follower if I can during the brief period... https:\/\/t.co\/Sd5PgiWvkm",
  "id" : 920969811194236929,
  "created_at" : "2017-10-19 11:08:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/82vIWkPYNN",
      "expanded_url" : "https:\/\/www.w3.org\/DesignIssues\/LinkedData.html",
      "display_url" : "w3.org\/DesignIssues\/L\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920958883539116032",
  "text" : "Tim Berners Lee\u2019s four rules of Linked Data, the web of data. https:\/\/t.co\/82vIWkPYNN",
  "id" : 920958883539116032,
  "created_at" : "2017-10-19 10:24:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/tFjz9jXpkj",
      "expanded_url" : "https:\/\/www.kaggle.com\/questions-and-answers\/41452#post232862",
      "display_url" : "kaggle.com\/questions-and-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920958114853277697",
  "text" : "RT @getoptimise: My answer to Market basket Analysis use-case. https:\/\/t.co\/tFjz9jXpkj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/tFjz9jXpkj",
        "expanded_url" : "https:\/\/www.kaggle.com\/questions-and-answers\/41452#post232862",
        "display_url" : "kaggle.com\/questions-and-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920906345938915329",
    "text" : "My answer to Market basket Analysis use-case. https:\/\/t.co\/tFjz9jXpkj",
    "id" : 920906345938915329,
    "created_at" : "2017-10-19 06:55:57 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 920958114853277697,
  "created_at" : "2017-10-19 10:21:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PlayStation Ireland",
      "screen_name" : "PlayStationIE",
      "indices" : [ 0, 14 ],
      "id_str" : "525497174",
      "id" : 525497174
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "920294878722043905",
  "geo" : { },
  "id_str" : "920956748395053056",
  "in_reply_to_user_id" : 525497174,
  "text" : "@PlayStationIE Portugal",
  "id" : 920956748395053056,
  "in_reply_to_status_id" : 920294878722043905,
  "created_at" : "2017-10-19 10:16:14 +0000",
  "in_reply_to_screen_name" : "PlayStationIE",
  "in_reply_to_user_id_str" : "525497174",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/7vgpD7CuOi",
      "expanded_url" : "https:\/\/www.theguardian.com\/uk-news\/2017\/oct\/17\/wife-of-stroke-victim-who-needs-24hr-care-must-leave-uk-while-he-cares-for-children",
      "display_url" : "theguardian.com\/uk-news\/2017\/o\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920768943136165888",
  "text" : "Wife of stroke victim who needs 24hr care must leave UK while he cares for  children https:\/\/t.co\/7vgpD7CuOi",
  "id" : 920768943136165888,
  "created_at" : "2017-10-18 21:49:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "KEH Camera",
      "screen_name" : "KEHcamera",
      "indices" : [ 3, 13 ],
      "id_str" : "288888097",
      "id" : 288888097
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/8ohSkHZMOq",
      "expanded_url" : "https:\/\/www.thephoblographer.com\/2017\/10\/18\/definitive-guide-photographer-real-photographer\/",
      "display_url" : "thephoblographer.com\/2017\/10\/18\/def\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920752612496084993",
  "text" : "RT @KEHcamera: Are you truly a photographer if you don\u2019t do these things?\nJust a little hump day humor! \uD83E\uDD23\uD83D\uDCF8\nhttps:\/\/t.co\/8ohSkHZMOq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/8ohSkHZMOq",
        "expanded_url" : "https:\/\/www.thephoblographer.com\/2017\/10\/18\/definitive-guide-photographer-real-photographer\/",
        "display_url" : "thephoblographer.com\/2017\/10\/18\/def\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920711255962390531",
    "text" : "Are you truly a photographer if you don\u2019t do these things?\nJust a little hump day humor! \uD83E\uDD23\uD83D\uDCF8\nhttps:\/\/t.co\/8ohSkHZMOq",
    "id" : 920711255962390531,
    "created_at" : "2017-10-18 18:00:44 +0000",
    "user" : {
      "name" : "KEH Camera",
      "screen_name" : "KEHcamera",
      "protected" : false,
      "id_str" : "288888097",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1003673258053521408\/tk3pCzB4_normal.jpg",
      "id" : 288888097,
      "verified" : false
    }
  },
  "id" : 920752612496084993,
  "created_at" : "2017-10-18 20:45:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alison Begas",
      "screen_name" : "kamfinsa",
      "indices" : [ 3, 12 ],
      "id_str" : "115653249",
      "id" : 115653249
    }, {
      "name" : "Peter Boylan",
      "screen_name" : "drboylan",
      "indices" : [ 88, 97 ],
      "id_str" : "417318906",
      "id" : 417318906
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "8Committee",
      "indices" : [ 30, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920751406482710531",
  "text" : "RT @kamfinsa: Some members of #8Committee are as qualified to argue medical ethics with @drboylan as I would be to argue physics with Steph\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Peter Boylan",
        "screen_name" : "drboylan",
        "indices" : [ 74, 83 ],
        "id_str" : "417318906",
        "id" : 417318906
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "8Committee",
        "indices" : [ 16, 27 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "920653838872449026",
    "text" : "Some members of #8Committee are as qualified to argue medical ethics with @drboylan as I would be to argue physics with Stephen Hawking.",
    "id" : 920653838872449026,
    "created_at" : "2017-10-18 14:12:34 +0000",
    "user" : {
      "name" : "Alison Begas",
      "screen_name" : "kamfinsa",
      "protected" : false,
      "id_str" : "115653249",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012744548567715841\/-ZQxyzvk_normal.jpg",
      "id" : 115653249,
      "verified" : false
    }
  },
  "id" : 920751406482710531,
  "created_at" : "2017-10-18 20:40:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/bVLQBVmEyh",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b08c7tjr",
      "display_url" : "bbc.co.uk\/programmes\/b08\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920717097994473472",
  "text" : "So people in those days have lamb chop for breakfast. https:\/\/t.co\/bVLQBVmEyh",
  "id" : 920717097994473472,
  "created_at" : "2017-10-18 18:23:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "C A T",
      "screen_name" : "magicgoeshere",
      "indices" : [ 3, 17 ],
      "id_str" : "27460424",
      "id" : 27460424
    }, {
      "name" : "DublinLive",
      "screen_name" : "DublinLive",
      "indices" : [ 25, 36 ],
      "id_str" : "4555772415",
      "id" : 4555772415
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/magicgoeshere\/status\/920648050611310593\/photo\/1",
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/MV56WrTBg4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMbMhD8W4AAIBt6.jpg",
      "id_str" : "920648042210058240",
      "id" : 920648042210058240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMbMhD8W4AAIBt6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MV56WrTBg4"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920668562397777922",
  "text" : "RT @magicgoeshere: Here, @DublinLive, I fixed it for you: https:\/\/t.co\/MV56WrTBg4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DublinLive",
        "screen_name" : "DublinLive",
        "indices" : [ 6, 17 ],
        "id_str" : "4555772415",
        "id" : 4555772415
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/magicgoeshere\/status\/920648050611310593\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/MV56WrTBg4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMbMhD8W4AAIBt6.jpg",
        "id_str" : "920648042210058240",
        "id" : 920648042210058240,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMbMhD8W4AAIBt6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1136,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 1136,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 1136,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MV56WrTBg4"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "920648050611310593",
    "text" : "Here, @DublinLive, I fixed it for you: https:\/\/t.co\/MV56WrTBg4",
    "id" : 920648050611310593,
    "created_at" : "2017-10-18 13:49:34 +0000",
    "user" : {
      "name" : "C A T",
      "screen_name" : "magicgoeshere",
      "protected" : false,
      "id_str" : "27460424",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004287546925441024\/yzh250Iz_normal.jpg",
      "id" : 27460424,
      "verified" : false
    }
  },
  "id" : 920668562397777922,
  "created_at" : "2017-10-18 15:11:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ASA Science Policy",
      "screen_name" : "ASA_SciPol",
      "indices" : [ 3, 14 ],
      "id_str" : "413263746",
      "id" : 413263746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920633364884938754",
  "text" : "RT @ASA_SciPol: Uncertainty &amp; graphicacy: How shd statisticians\/journalists\/designers reveal uncertainty in graphics? https:\/\/t.co\/Bagjm5Ln\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Alberto Cairo",
        "screen_name" : "albertocairo",
        "indices" : [ 130, 143 ],
        "id_str" : "34255829",
        "id" : 34255829
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ASA_SciPol\/status\/920322364411449344\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/Aw6su8r3ub",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMWkT9VWAAENbMm.jpg",
        "id_str" : "920322361655689217",
        "id" : 920322361655689217,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMWkT9VWAAENbMm.jpg",
        "sizes" : [ {
          "h" : 380,
          "resize" : "fit",
          "w" : 821
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 821
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 821
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Aw6su8r3ub"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/Bagjm5LnrB",
        "expanded_url" : "http:\/\/powerfromstatistics.eu\/uploads\/originals\/PDF\/outlook-reports\/PfS_Uncertainty%20and%20graphicacy.pdf",
        "display_url" : "powerfromstatistics.eu\/uploads\/origin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920322364411449344",
    "text" : "Uncertainty &amp; graphicacy: How shd statisticians\/journalists\/designers reveal uncertainty in graphics? https:\/\/t.co\/Bagjm5LnrB @albertocairo https:\/\/t.co\/Aw6su8r3ub",
    "id" : 920322364411449344,
    "created_at" : "2017-10-17 16:15:25 +0000",
    "user" : {
      "name" : "ASA Science Policy",
      "screen_name" : "ASA_SciPol",
      "protected" : false,
      "id_str" : "413263746",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/682567128189407233\/itoUHxIZ_normal.jpg",
      "id" : 413263746,
      "verified" : false
    }
  },
  "id" : 920633364884938754,
  "created_at" : "2017-10-18 12:51:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/8uiRoYBH4r",
      "expanded_url" : "https:\/\/twitter.com\/paulodonoghue93\/status\/920245205596622848",
      "display_url" : "twitter.com\/paulodonoghue9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920631559652368384",
  "text" : "Suggestion: Issue them a bill when they face difficulties &amp; have to call emergency services. https:\/\/t.co\/8uiRoYBH4r",
  "id" : 920631559652368384,
  "created_at" : "2017-10-18 12:44:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Dooley",
      "screen_name" : "BenjaminDooley",
      "indices" : [ 3, 18 ],
      "id_str" : "305130750",
      "id" : 305130750
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920629876201312259",
  "text" : "RT @BenjaminDooley: So interesting that Han Chinese delegates wear Western business suits while minorities are all in \"traditional\" costume\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dissertationtopics",
        "indices" : [ 121, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "920463759704956928",
    "text" : "So interesting that Han Chinese delegates wear Western business suits while minorities are all in \"traditional\" costumes #dissertationtopics",
    "id" : 920463759704956928,
    "created_at" : "2017-10-18 01:37:16 +0000",
    "user" : {
      "name" : "Ben Dooley",
      "screen_name" : "BenjaminDooley",
      "protected" : false,
      "id_str" : "305130750",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1467678042\/2011-01_Hu_visit_normal.png",
      "id" : 305130750,
      "verified" : false
    }
  },
  "id" : 920629876201312259,
  "created_at" : "2017-10-18 12:37:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Khaya Dlanga",
      "screen_name" : "khayadlanga",
      "indices" : [ 3, 15 ],
      "id_str" : "15801023",
      "id" : 15801023
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920583710784835584",
  "text" : "RT @khayadlanga: Some heroes are never recognised. But he was not about recognition, he simply stood for what was right.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "919516407636905984",
    "geo" : { },
    "id_str" : "919516856700100608",
    "in_reply_to_user_id" : 15801023,
    "text" : "Some heroes are never recognised. But he was not about recognition, he simply stood for what was right.",
    "id" : 919516856700100608,
    "in_reply_to_status_id" : 919516407636905984,
    "created_at" : "2017-10-15 10:54:37 +0000",
    "in_reply_to_screen_name" : "khayadlanga",
    "in_reply_to_user_id_str" : "15801023",
    "user" : {
      "name" : "Khaya Dlanga",
      "screen_name" : "khayadlanga",
      "protected" : false,
      "id_str" : "15801023",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031016687309602817\/i6RPQ7Lz_normal.jpg",
      "id" : 15801023,
      "verified" : true
    }
  },
  "id" : 920583710784835584,
  "created_at" : "2017-10-18 09:33:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/KzkFEDjHlk",
      "expanded_url" : "http:\/\/str.sg\/4yTL",
      "display_url" : "str.sg\/4yTL"
    } ]
  },
  "geo" : { },
  "id_str" : "920579195432067072",
  "text" : "Police test multi-agency response with mock terror attack in Changi Airport https:\/\/t.co\/KzkFEDjHlk",
  "id" : 920579195432067072,
  "created_at" : "2017-10-18 09:15:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/r6tYc19PqY",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/your-name-tag-upside-down-larry-marine\/?trackingId=MSxOqK7exCiHMuef8%2BEquQ%3D%3D",
      "display_url" : "linkedin.com\/pulse\/your-nam\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920578370978701312",
  "text" : "Is Your Name Tag Upside Down? https:\/\/t.co\/r6tYc19PqY",
  "id" : 920578370978701312,
  "created_at" : "2017-10-18 09:12:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920575498840891393",
  "text" : "To all celebrating Deepavali may the festival of lights illuminate your life &amp; shower you with health, peace &amp;  prosperity.",
  "id" : 920575498840891393,
  "created_at" : "2017-10-18 09:01:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Hobby",
      "screen_name" : "strobist",
      "indices" : [ 3, 12 ],
      "id_str" : "14305530",
      "id" : 14305530
    }, {
      "name" : "Stephen Shankland",
      "screen_name" : "stshank",
      "indices" : [ 21, 29 ],
      "id_str" : "6595682",
      "id" : 6595682
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Pixel2",
      "indices" : [ 46, 53 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920320410696212481",
  "text" : "RT @strobist: CNET's @stshank's review of the #Pixel2 is also a great primer on the current state of computational photography. https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Stephen Shankland",
        "screen_name" : "stshank",
        "indices" : [ 7, 15 ],
        "id_str" : "6595682",
        "id" : 6595682
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/strobist\/status\/920307029767606272\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/uEWPTxupBs",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMWWV4KX4AA7fkG.jpg",
        "id_str" : "920307001464446976",
        "id" : 920307001464446976,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMWWV4KX4AA7fkG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 814,
          "resize" : "fit",
          "w" : 1079
        }, {
          "h" : 814,
          "resize" : "fit",
          "w" : 1079
        }, {
          "h" : 814,
          "resize" : "fit",
          "w" : 1079
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uEWPTxupBs"
      } ],
      "hashtags" : [ {
        "text" : "Pixel2",
        "indices" : [ 32, 39 ]
      } ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/d1VwAgfHXX",
        "expanded_url" : "https:\/\/www.cnet.com\/news\/how-googles-pixel-2-camera-outpaces-last-years-photo-tech\/",
        "display_url" : "cnet.com\/news\/how-googl\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "920307029767606272",
    "text" : "CNET's @stshank's review of the #Pixel2 is also a great primer on the current state of computational photography. https:\/\/t.co\/d1VwAgfHXX https:\/\/t.co\/uEWPTxupBs",
    "id" : 920307029767606272,
    "created_at" : "2017-10-17 15:14:29 +0000",
    "user" : {
      "name" : "David Hobby",
      "screen_name" : "strobist",
      "protected" : false,
      "id_str" : "14305530",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/477161583043289088\/fwHHk4L5_normal.jpeg",
      "id" : 14305530,
      "verified" : true
    }
  },
  "id" : 920320410696212481,
  "created_at" : "2017-10-17 16:07:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/4lftajJ5tN",
      "expanded_url" : "https:\/\/twitter.com\/search?q=%40mryap%20notebooks%20&src=typd&lang=en",
      "display_url" : "twitter.com\/search?q=%40mr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "920280956484890624",
  "text" : "My other computer is an Azure data centre. https:\/\/t.co\/4lftajJ5tN",
  "id" : 920280956484890624,
  "created_at" : "2017-10-17 13:30:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "indices" : [ 3, 13 ],
      "id_str" : "27819449",
      "id" : 27819449
    }, {
      "name" : "All Tech Considered",
      "screen_name" : "npralltech",
      "indices" : [ 94, 105 ],
      "id_str" : "40974333",
      "id" : 40974333
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ArtificialIntelligence",
      "indices" : [ 38, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/hxoRBOPOqB",
      "expanded_url" : "http:\/\/micfarris.us\/2kxFH0m",
      "display_url" : "micfarris.us\/2kxFH0m"
    } ]
  },
  "geo" : { },
  "id_str" : "920265693932806144",
  "text" : "RT @MicFarris: Lawmakers: Don't Gauge #ArtificialIntelligence By What You See In The Movies | @npralltech https:\/\/t.co\/hxoRBOPOqB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "All Tech Considered",
        "screen_name" : "npralltech",
        "indices" : [ 79, 90 ],
        "id_str" : "40974333",
        "id" : 40974333
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ArtificialIntelligence",
        "indices" : [ 23, 46 ]
      } ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/hxoRBOPOqB",
        "expanded_url" : "http:\/\/micfarris.us\/2kxFH0m",
        "display_url" : "micfarris.us\/2kxFH0m"
      } ]
    },
    "geo" : { },
    "id_str" : "920264413130653696",
    "text" : "Lawmakers: Don't Gauge #ArtificialIntelligence By What You See In The Movies | @npralltech https:\/\/t.co\/hxoRBOPOqB",
    "id" : 920264413130653696,
    "created_at" : "2017-10-17 12:25:08 +0000",
    "user" : {
      "name" : "Mic Farris",
      "screen_name" : "MicFarris",
      "protected" : false,
      "id_str" : "27819449",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3055549457\/94b094a6e85db296aa251b3a775d7439_normal.jpeg",
      "id" : 27819449,
      "verified" : false
    }
  },
  "id" : 920265693932806144,
  "created_at" : "2017-10-17 12:30:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "elliekisyombe@gmail.",
      "screen_name" : "elliekisyombe1",
      "indices" : [ 3, 18 ],
      "id_str" : "3348219707",
      "id" : 3348219707
    }, {
      "name" : "Al Jazeera News",
      "screen_name" : "AJENews",
      "indices" : [ 72, 80 ],
      "id_str" : "18424289",
      "id" : 18424289
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/U2v7j9eKjg",
      "expanded_url" : "http:\/\/aje.io\/uj6ku",
      "display_url" : "aje.io\/uj6ku"
    } ]
  },
  "geo" : { },
  "id_str" : "920259532307206145",
  "text" : "RT @elliekisyombe1: Double standards: 'Why aren't we all with Somalia?' @AJENews  https:\/\/t.co\/U2v7j9eKjg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Al Jazeera News",
        "screen_name" : "AJENews",
        "indices" : [ 52, 60 ],
        "id_str" : "18424289",
        "id" : 18424289
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/U2v7j9eKjg",
        "expanded_url" : "http:\/\/aje.io\/uj6ku",
        "display_url" : "aje.io\/uj6ku"
      } ]
    },
    "geo" : { },
    "id_str" : "920061827060551683",
    "text" : "Double standards: 'Why aren't we all with Somalia?' @AJENews  https:\/\/t.co\/U2v7j9eKjg",
    "id" : 920061827060551683,
    "created_at" : "2017-10-16 23:00:08 +0000",
    "user" : {
      "name" : "elliekisyombe@gmail.",
      "screen_name" : "elliekisyombe1",
      "protected" : false,
      "id_str" : "3348219707",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/965588173001764864\/-o7eyyCd_normal.jpg",
      "id" : 3348219707,
      "verified" : false
    }
  },
  "id" : 920259532307206145,
  "created_at" : "2017-10-17 12:05:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Catherine Drea",
      "screen_name" : "foxglovelane",
      "indices" : [ 3, 16 ],
      "id_str" : "38410778",
      "id" : 38410778
    }, {
      "name" : "Granary Cafe",
      "screen_name" : "GranaryCafe",
      "indices" : [ 35, 47 ],
      "id_str" : "3332323365",
      "id" : 3332323365
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/foxglovelane\/status\/920238357128663040\/photo\/1",
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/3iAo4CyaxU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMVX6ItXcAAX42C.jpg",
      "id_str" : "920238355148926976",
      "id" : 920238355148926976,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMVX6ItXcAAX42C.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3iAo4CyaxU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920244393252196352",
  "text" : "RT @foxglovelane: Our office today @GranaryCafe https:\/\/t.co\/3iAo4CyaxU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Granary Cafe",
        "screen_name" : "GranaryCafe",
        "indices" : [ 17, 29 ],
        "id_str" : "3332323365",
        "id" : 3332323365
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/foxglovelane\/status\/920238357128663040\/photo\/1",
        "indices" : [ 30, 53 ],
        "url" : "https:\/\/t.co\/3iAo4CyaxU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMVX6ItXcAAX42C.jpg",
        "id_str" : "920238355148926976",
        "id" : 920238355148926976,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMVX6ItXcAAX42C.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3iAo4CyaxU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "920238357128663040",
    "text" : "Our office today @GranaryCafe https:\/\/t.co\/3iAo4CyaxU",
    "id" : 920238357128663040,
    "created_at" : "2017-10-17 10:41:36 +0000",
    "user" : {
      "name" : "Catherine Drea",
      "screen_name" : "foxglovelane",
      "protected" : false,
      "id_str" : "38410778",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/612211626595495936\/0qfYlVLN_normal.jpg",
      "id" : 38410778,
      "verified" : false
    }
  },
  "id" : 920244393252196352,
  "created_at" : "2017-10-17 11:05:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Juliet Hof-Hu-How do you say Hougland?",
      "screen_name" : "j_houg",
      "indices" : [ 3, 10 ],
      "id_str" : "376618837",
      "id" : 376618837
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "920244092646379520",
  "text" : "RT @j_houg: Nothing will ever be as pure and sweet as my aunt thinking Oracle is a sailboat company.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "919927381581742080",
    "text" : "Nothing will ever be as pure and sweet as my aunt thinking Oracle is a sailboat company.",
    "id" : 919927381581742080,
    "created_at" : "2017-10-16 14:05:53 +0000",
    "user" : {
      "name" : "Juliet Hof-Hu-How do you say Hougland?",
      "screen_name" : "j_houg",
      "protected" : false,
      "id_str" : "376618837",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/925079372415029249\/rFS4sVpB_normal.jpg",
      "id" : 376618837,
      "verified" : false
    }
  },
  "id" : 920244092646379520,
  "created_at" : "2017-10-17 11:04:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Max Galka",
      "screen_name" : "galka_max",
      "indices" : [ 3, 13 ],
      "id_str" : "2184943214",
      "id" : 2184943214
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/galka_max\/status\/903269223543267328\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/cHnakoJVBx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DIkOZhzXUAAcDYr.jpg",
      "id_str" : "903269031997820928",
      "id" : 903269031997820928,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIkOZhzXUAAcDYr.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 685,
        "resize" : "fit",
        "w" : 1249
      }, {
        "h" : 658,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 373,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 685,
        "resize" : "fit",
        "w" : 1249
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cHnakoJVBx"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/kBzAz8s7PP",
      "expanded_url" : "https:\/\/www.bloomberg.com\/graphics\/2017-health-care-spending\/",
      "display_url" : "bloomberg.com\/graphics\/2017-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919977481989427201",
  "text" : "RT @galka_max: Health care spending vs. life expectancy https:\/\/t.co\/kBzAz8s7PP https:\/\/t.co\/cHnakoJVBx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/galka_max\/status\/903269223543267328\/photo\/1",
        "indices" : [ 65, 88 ],
        "url" : "https:\/\/t.co\/cHnakoJVBx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DIkOZhzXUAAcDYr.jpg",
        "id_str" : "903269031997820928",
        "id" : 903269031997820928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIkOZhzXUAAcDYr.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1249
        }, {
          "h" : 658,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 373,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1249
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cHnakoJVBx"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 41, 64 ],
        "url" : "https:\/\/t.co\/kBzAz8s7PP",
        "expanded_url" : "https:\/\/www.bloomberg.com\/graphics\/2017-health-care-spending\/",
        "display_url" : "bloomberg.com\/graphics\/2017-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "903269223543267328",
    "text" : "Health care spending vs. life expectancy https:\/\/t.co\/kBzAz8s7PP https:\/\/t.co\/cHnakoJVBx",
    "id" : 903269223543267328,
    "created_at" : "2017-08-31 14:52:19 +0000",
    "user" : {
      "name" : "Max Galka",
      "screen_name" : "galka_max",
      "protected" : false,
      "id_str" : "2184943214",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/509352358141124608\/xZm7IOv__normal.jpeg",
      "id" : 2184943214,
      "verified" : true
    }
  },
  "id" : 919977481989427201,
  "created_at" : "2017-10-16 17:24:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/Xx023z36xD",
      "expanded_url" : "https:\/\/medium.com\/station-f\/onboarding-is-the-new-conversion-ee4aec732f93",
      "display_url" : "medium.com\/station-f\/onbo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919922725199732738",
  "text" : "What\u2019s better? \u201C1,000,000 signups after 4 days or 100,000,000 active users after 16 months?https:\/\/t.co\/Xx023z36xD",
  "id" : 919922725199732738,
  "created_at" : "2017-10-16 13:47:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919880406790541312",
  "text" : "Most of the time, I clicked on ads out of curiosity.  AM I the target audience? Yes. Am I already to transact? No",
  "id" : 919880406790541312,
  "created_at" : "2017-10-16 10:59:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919857351544524800\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/mundDaBO7t",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMP9YrWXUAAMAYG.jpg",
      "id_str" : "919857349308928000",
      "id" : 919857349308928000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMP9YrWXUAAMAYG.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mundDaBO7t"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/Gf1l3WgOA4",
      "expanded_url" : "http:\/\/ift.tt\/2ieGId6",
      "display_url" : "ift.tt\/2ieGId6"
    } ]
  },
  "geo" : { },
  "id_str" : "919857351544524800",
  "text" : "Local supermarket unusually busy. Car park full. Long queues forming. Not on Monday morning. https:\/\/t.co\/Gf1l3WgOA4 https:\/\/t.co\/mundDaBO7t",
  "id" : 919857351544524800,
  "created_at" : "2017-10-16 09:27:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tessa Naran",
      "screen_name" : "Tessthepilot",
      "indices" : [ 3, 16 ],
      "id_str" : "387829768",
      "id" : 387829768
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919845622404009984",
  "text" : "RT @Tessthepilot: Please retweet, let's see if I can get some Airlines to notice and hire me \uD83D\uDE03\uD83D\uDC69\uD83C\uDFFD\u200D\u2708\uFE0F\n\nMonarch F\/O A320 family type rating, 1\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Tessthepilot\/status\/919496105523601408\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/82zy5YOJYc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMK00o3X0AESGa0.jpg",
        "id_str" : "919496090352865281",
        "id" : 919496090352865281,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMK00o3X0AESGa0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/82zy5YOJYc"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "919496105523601408",
    "text" : "Please retweet, let's see if I can get some Airlines to notice and hire me \uD83D\uDE03\uD83D\uDC69\uD83C\uDFFD\u200D\u2708\uFE0F\n\nMonarch F\/O A320 family type rating, 1000+ on type \uD83D\uDE03 https:\/\/t.co\/82zy5YOJYc",
    "id" : 919496105523601408,
    "created_at" : "2017-10-15 09:32:09 +0000",
    "user" : {
      "name" : "Tessa Naran",
      "screen_name" : "Tessthepilot",
      "protected" : false,
      "id_str" : "387829768",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943772357210099712\/fLHeoQjO_normal.jpg",
      "id" : 387829768,
      "verified" : false
    }
  },
  "id" : 919845622404009984,
  "created_at" : "2017-10-16 08:41:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/kXMxM80n0Q",
      "expanded_url" : "https:\/\/twitter.com\/gavindbrown\/status\/919834018249629696",
      "display_url" : "twitter.com\/gavindbrown\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919834384844369920",
  "text" : "No internet access for them.... https:\/\/t.co\/kXMxM80n0Q",
  "id" : 919834384844369920,
  "created_at" : "2017-10-16 07:56:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpheliaStorm",
      "indices" : [ 30, 43 ]
    }, {
      "text" : "Opheila",
      "indices" : [ 44, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/lqP2a1j6gT",
      "expanded_url" : "https:\/\/twitter.com\/sltlk\/status\/919818660319023104",
      "display_url" : "twitter.com\/sltlk\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919831716805607424",
  "text" : "RT @EmeraldDeLeeuw: So eery.. #OpheliaStorm #Opheila is it Halloween yet? https:\/\/t.co\/lqP2a1j6gT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "OpheliaStorm",
        "indices" : [ 10, 23 ]
      }, {
        "text" : "Opheila",
        "indices" : [ 24, 32 ]
      } ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/lqP2a1j6gT",
        "expanded_url" : "https:\/\/twitter.com\/sltlk\/status\/919818660319023104",
        "display_url" : "twitter.com\/sltlk\/status\/9\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "919830248723709953",
    "text" : "So eery.. #OpheliaStorm #Opheila is it Halloween yet? https:\/\/t.co\/lqP2a1j6gT",
    "id" : 919830248723709953,
    "created_at" : "2017-10-16 07:39:55 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 919831716805607424,
  "created_at" : "2017-10-16 07:45:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "indices" : [ 3, 12 ],
      "id_str" : "130745589",
      "id" : 130745589
    }, {
      "name" : "Silvio Savarese",
      "screen_name" : "silviocinguetta",
      "indices" : [ 59, 75 ],
      "id_str" : "2835683058",
      "id" : 2835683058
    }, {
      "name" : "Stanford Vision and Learning Lab",
      "screen_name" : "StanfordCVGL",
      "indices" : [ 76, 89 ],
      "id_str" : "2832190776",
      "id" : 2832190776
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 48, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/SUSdg9NNm1",
      "expanded_url" : "https:\/\/twitter.com\/stanfordcvgl\/status\/919280909538377728",
      "display_url" : "twitter.com\/stanfordcvgl\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919827834771853313",
  "text" : "RT @drfeifei: Amazing new dataset from Stanford #AI Lab by @silviocinguetta @StanfordCVGL https:\/\/t.co\/SUSdg9NNm1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Silvio Savarese",
        "screen_name" : "silviocinguetta",
        "indices" : [ 45, 61 ],
        "id_str" : "2835683058",
        "id" : 2835683058
      }, {
        "name" : "Stanford Vision and Learning Lab",
        "screen_name" : "StanfordCVGL",
        "indices" : [ 62, 75 ],
        "id_str" : "2832190776",
        "id" : 2832190776
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 34, 37 ]
      } ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/SUSdg9NNm1",
        "expanded_url" : "https:\/\/twitter.com\/stanfordcvgl\/status\/919280909538377728",
        "display_url" : "twitter.com\/stanfordcvgl\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "919307408945455104",
    "text" : "Amazing new dataset from Stanford #AI Lab by @silviocinguetta @StanfordCVGL https:\/\/t.co\/SUSdg9NNm1",
    "id" : 919307408945455104,
    "created_at" : "2017-10-14 21:02:20 +0000",
    "user" : {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "protected" : false,
      "id_str" : "130745589",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841385099799085056\/R1iX4QGX_normal.jpg",
      "id" : 130745589,
      "verified" : false
    }
  },
  "id" : 919827834771853313,
  "created_at" : "2017-10-16 07:30:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919827595889446912",
  "text" : "Little bird told me that Hospital staffs for the morning shift report to workplace last night in case they could not make it to work today.",
  "id" : 919827595889446912,
  "created_at" : "2017-10-16 07:29:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/Dp0vHj0Nfv",
      "expanded_url" : "https:\/\/www.stuff.co.nz\/travel\/destinations\/asia\/97000336\/inside-singapore-airlines-tough-training-school-for-flight-attendants",
      "display_url" : "stuff.co.nz\/travel\/destina\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919826649138581504",
  "text" : "Inside Singapore Airlines' tough training school for flight attendants   https:\/\/t.co\/Dp0vHj0Nfv",
  "id" : 919826649138581504,
  "created_at" : "2017-10-16 07:25:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Cousins",
      "screen_name" : "Kilmorecottage",
      "indices" : [ 3, 18 ],
      "id_str" : "123530433",
      "id" : 123530433
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Kilmorecottage\/status\/919823505436348419\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/HxWE4UGXrP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMPel3zX0AAtPtw.jpg",
      "id_str" : "919823491129659392",
      "id" : 919823491129659392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMPel3zX0AAtPtw.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HxWE4UGXrP"
    } ],
    "hashtags" : [ {
      "text" : "IrishMammy",
      "indices" : [ 46, 57 ]
    }, {
      "text" : "Ophelia",
      "indices" : [ 58, 66 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919825479548456960",
  "text" : "RT @Kilmorecottage: At least there'll be tea. #IrishMammy #Ophelia https:\/\/t.co\/HxWE4UGXrP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Kilmorecottage\/status\/919823505436348419\/photo\/1",
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/HxWE4UGXrP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMPel3zX0AAtPtw.jpg",
        "id_str" : "919823491129659392",
        "id" : 919823491129659392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMPel3zX0AAtPtw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HxWE4UGXrP"
      } ],
      "hashtags" : [ {
        "text" : "IrishMammy",
        "indices" : [ 26, 37 ]
      }, {
        "text" : "Ophelia",
        "indices" : [ 38, 46 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "919823505436348419",
    "text" : "At least there'll be tea. #IrishMammy #Ophelia https:\/\/t.co\/HxWE4UGXrP",
    "id" : 919823505436348419,
    "created_at" : "2017-10-16 07:13:07 +0000",
    "user" : {
      "name" : "Helen Cousins",
      "screen_name" : "Kilmorecottage",
      "protected" : false,
      "id_str" : "123530433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877953200908849153\/rWDWLArv_normal.jpg",
      "id" : 123530433,
      "verified" : false
    }
  },
  "id" : 919825479548456960,
  "created_at" : "2017-10-16 07:20:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WWN",
      "screen_name" : "WhispersNewsLTD",
      "indices" : [ 3, 19 ],
      "id_str" : "394416044",
      "id" : 394416044
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/WhispersNewsLTD\/status\/919688413737029633\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/oLYLMRgWkF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMNjvBsX4AEDk25.jpg",
      "id_str" : "919688408473264129",
      "id" : 919688408473264129,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMNjvBsX4AEDk25.jpg",
      "sizes" : [ {
        "h" : 159,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 159,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 159,
        "resize" : "fit",
        "w" : 318
      }, {
        "h" : 159,
        "resize" : "fit",
        "w" : 318
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oLYLMRgWkF"
    } ],
    "hashtags" : [ {
      "text" : "Ophelia",
      "indices" : [ 93, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919823110681120768",
  "text" : "RT @WhispersNewsLTD: Local man checking online wind map like he's some kind of meteorologist #Ophelia https:\/\/t.co\/oLYLMRgWkF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WhispersNewsLTD\/status\/919688413737029633\/photo\/1",
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/oLYLMRgWkF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMNjvBsX4AEDk25.jpg",
        "id_str" : "919688408473264129",
        "id" : 919688408473264129,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMNjvBsX4AEDk25.jpg",
        "sizes" : [ {
          "h" : 159,
          "resize" : "fit",
          "w" : 318
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 159,
          "resize" : "fit",
          "w" : 318
        }, {
          "h" : 159,
          "resize" : "fit",
          "w" : 318
        }, {
          "h" : 159,
          "resize" : "fit",
          "w" : 318
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oLYLMRgWkF"
      } ],
      "hashtags" : [ {
        "text" : "Ophelia",
        "indices" : [ 72, 80 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "919688413737029633",
    "text" : "Local man checking online wind map like he's some kind of meteorologist #Ophelia https:\/\/t.co\/oLYLMRgWkF",
    "id" : 919688413737029633,
    "created_at" : "2017-10-15 22:16:19 +0000",
    "user" : {
      "name" : "WWN",
      "screen_name" : "WhispersNewsLTD",
      "protected" : false,
      "id_str" : "394416044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033384876098117634\/sjJIZreJ_normal.jpg",
      "id" : 394416044,
      "verified" : false
    }
  },
  "id" : 919823110681120768,
  "created_at" : "2017-10-16 07:11:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/919817906673856512\/video\/1",
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/Ap5RoII8Ax",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/919817830031417344\/pu\/img\/1tsjCNx4LrBV1Rn0.jpg",
      "id_str" : "919817830031417344",
      "id" : 919817830031417344,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/919817830031417344\/pu\/img\/1tsjCNx4LrBV1Rn0.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 848
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 848
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 848
      }, {
        "h" : 385,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Ap5RoII8Ax"
    } ],
    "hashtags" : [ {
      "text" : "Ophelia",
      "indices" : [ 84, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919821331406970880",
  "text" : "RT @EimearMcCormack: Very eerie over Dublin right now. A club mate just posted this #Ophelia https:\/\/t.co\/Ap5RoII8Ax",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/919817906673856512\/video\/1",
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/Ap5RoII8Ax",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/919817830031417344\/pu\/img\/1tsjCNx4LrBV1Rn0.jpg",
        "id_str" : "919817830031417344",
        "id" : 919817830031417344,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/919817830031417344\/pu\/img\/1tsjCNx4LrBV1Rn0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 848
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 848
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 848
        }, {
          "h" : 385,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ap5RoII8Ax"
      } ],
      "hashtags" : [ {
        "text" : "Ophelia",
        "indices" : [ 63, 71 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "919817906673856512",
    "text" : "Very eerie over Dublin right now. A club mate just posted this #Ophelia https:\/\/t.co\/Ap5RoII8Ax",
    "id" : 919817906673856512,
    "created_at" : "2017-10-16 06:50:52 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 919821331406970880,
  "created_at" : "2017-10-16 07:04:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919816581227663360",
  "text" : "I still can make a dash to the local supermarket to buy milk for tea since I am in Dublin?",
  "id" : 919816581227663360,
  "created_at" : "2017-10-16 06:45:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 3, 17 ],
      "id_str" : "80549724",
      "id" : 80549724
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ophelia",
      "indices" : [ 19, 27 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919677344238927872",
  "text" : "RT @dublinbusnews: #Ophelia - We're considering the implications to our services tomorrow &amp; taking advice from Met \u00C9ireann. For updates htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Ophelia",
        "indices" : [ 0, 8 ]
      } ],
      "urls" : [ {
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/0uRvRD7axh",
        "expanded_url" : "http:\/\/bit.ly\/2yMj66C",
        "display_url" : "bit.ly\/2yMj66C"
      } ]
    },
    "geo" : { },
    "id_str" : "919673470962814976",
    "text" : "#Ophelia - We're considering the implications to our services tomorrow &amp; taking advice from Met \u00C9ireann. For updates https:\/\/t.co\/0uRvRD7axh",
    "id" : 919673470962814976,
    "created_at" : "2017-10-15 21:16:56 +0000",
    "user" : {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "protected" : false,
      "id_str" : "80549724",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037301110988566529\/8dar7Mpu_normal.jpg",
      "id" : 80549724,
      "verified" : true
    }
  },
  "id" : 919677344238927872,
  "created_at" : "2017-10-15 21:32:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/ymaVDQma0D",
      "expanded_url" : "https:\/\/twitter.com\/TarekFatah\/status\/919610094207361025",
      "display_url" : "twitter.com\/TarekFatah\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919628553368547329",
  "text" : "RT @ronanlyons: Unimaginable suffering in Mogadishu today. https:\/\/t.co\/ymaVDQma0D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 43, 66 ],
        "url" : "https:\/\/t.co\/ymaVDQma0D",
        "expanded_url" : "https:\/\/twitter.com\/TarekFatah\/status\/919610094207361025",
        "display_url" : "twitter.com\/TarekFatah\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "919612847595933697",
    "text" : "Unimaginable suffering in Mogadishu today. https:\/\/t.co\/ymaVDQma0D",
    "id" : 919612847595933697,
    "created_at" : "2017-10-15 17:16:03 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 919628553368547329,
  "created_at" : "2017-10-15 18:18:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Treasa Lynch",
      "screen_name" : "treasa",
      "indices" : [ 3, 10 ],
      "id_str" : "14746983",
      "id" : 14746983
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/treasa\/status\/919253135721459713\/photo\/1",
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/rDeOJnFO0z",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMHX0Y8W4AExWY0.jpg",
      "id_str" : "919253094009069569",
      "id" : 919253094009069569,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMHX0Y8W4AExWY0.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rDeOJnFO0z"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/treasa\/status\/919253135721459713\/photo\/1",
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/rDeOJnFO0z",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMHX1v0XkAAiUNQ.jpg",
      "id_str" : "919253117329444864",
      "id" : 919253117329444864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMHX1v0XkAAiUNQ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rDeOJnFO0z"
    } ],
    "hashtags" : [ {
      "text" : "HurricaneOphelia",
      "indices" : [ 52, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919627568113995778",
  "text" : "RT @treasa: I feel the need to compare and contrast #HurricaneOphelia US and Ireland... https:\/\/t.co\/rDeOJnFO0z",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/treasa\/status\/919253135721459713\/photo\/1",
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/rDeOJnFO0z",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMHX0Y8W4AExWY0.jpg",
        "id_str" : "919253094009069569",
        "id" : 919253094009069569,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMHX0Y8W4AExWY0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rDeOJnFO0z"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/treasa\/status\/919253135721459713\/photo\/1",
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/rDeOJnFO0z",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DMHX1v0XkAAiUNQ.jpg",
        "id_str" : "919253117329444864",
        "id" : 919253117329444864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMHX1v0XkAAiUNQ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rDeOJnFO0z"
      } ],
      "hashtags" : [ {
        "text" : "HurricaneOphelia",
        "indices" : [ 40, 57 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "919253135721459713",
    "text" : "I feel the need to compare and contrast #HurricaneOphelia US and Ireland... https:\/\/t.co\/rDeOJnFO0z",
    "id" : 919253135721459713,
    "created_at" : "2017-10-14 17:26:41 +0000",
    "user" : {
      "name" : "Treasa Lynch",
      "screen_name" : "treasa",
      "protected" : false,
      "id_str" : "14746983",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/712342342674620417\/u6e44GHd_normal.jpg",
      "id" : 14746983,
      "verified" : false
    }
  },
  "id" : 919627568113995778,
  "created_at" : "2017-10-15 18:14:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919602510758891521",
  "text" : "Someone seldom appear on FB turn up on their birthday to lap up those birthday wishes.",
  "id" : 919602510758891521,
  "created_at" : "2017-10-15 16:34:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919545126485155842\/photo\/1",
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/lgcAwzG82m",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMLhYurX0AAdzyx.jpg",
      "id_str" : "919545088900059136",
      "id" : 919545088900059136,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMLhYurX0AAdzyx.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lgcAwzG82m"
    } ],
    "hashtags" : [ {
      "text" : "OPENHOUSEDUBLIN",
      "indices" : [ 46, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919545126485155842",
  "text" : "Belvedere House. Lavishly decorated interior. #OPENHOUSEDUBLIN https:\/\/t.co\/lgcAwzG82m",
  "id" : 919545126485155842,
  "created_at" : "2017-10-15 12:46:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919520171827572737\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/olwBCdfTT1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMLKuP8XcAEPNzR.jpg",
      "id_str" : "919520169839521793",
      "id" : 919520169839521793,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMLKuP8XcAEPNzR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/olwBCdfTT1"
    } ],
    "hashtags" : [ {
      "text" : "openhousedublin",
      "indices" : [ 56, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/wY4HJiND2d",
      "expanded_url" : "http:\/\/ift.tt\/2xGCxZs",
      "display_url" : "ift.tt\/2xGCxZs"
    } ]
  },
  "geo" : { },
  "id_str" : "919520171827572737",
  "text" : "Machine Learning algorithms code in the toilet cubicle. #openhousedublin https:\/\/t.co\/wY4HJiND2d https:\/\/t.co\/olwBCdfTT1",
  "id" : 919520171827572737,
  "created_at" : "2017-10-15 11:07:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919509585538502657\/photo\/1",
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/yT11enLrbF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMLBGC8XkAEN7Hi.jpg",
      "id_str" : "919509583550451713",
      "id" : 919509583550451713,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMLBGC8XkAEN7Hi.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yT11enLrbF"
    } ],
    "hashtags" : [ {
      "text" : "openhousedublin",
      "indices" : [ 0, 16 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/1TcnSFS9Tq",
      "expanded_url" : "http:\/\/ift.tt\/2kPagyW",
      "display_url" : "ift.tt\/2kPagyW"
    } ]
  },
  "geo" : { },
  "id_str" : "919509585538502657",
  "text" : "#openhousedublin https:\/\/t.co\/1TcnSFS9Tq https:\/\/t.co\/yT11enLrbF",
  "id" : 919509585538502657,
  "created_at" : "2017-10-15 10:25:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919502649438896129\/photo\/1",
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/OYyanD1sC0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMK6yToXUAAlR9J.jpg",
      "id_str" : "919502647362801664",
      "id" : 919502647362801664,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMK6yToXUAAlR9J.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OYyanD1sC0"
    } ],
    "hashtags" : [ {
      "text" : "openhousedublin",
      "indices" : [ 0, 16 ]
    } ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/zU22YCUQy3",
      "expanded_url" : "http:\/\/ift.tt\/2zpla1s",
      "display_url" : "ift.tt\/2zpla1s"
    } ]
  },
  "geo" : { },
  "id_str" : "919502649438896129",
  "text" : "#openhousedublin https:\/\/t.co\/zU22YCUQy3 https:\/\/t.co\/OYyanD1sC0",
  "id" : 919502649438896129,
  "created_at" : "2017-10-15 09:58:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/B9pf86nDNm",
      "expanded_url" : "http:\/\/www.businessinsider.sg\/safest-cities-in-the-world-2017-10\/#.WeJPbUt9LLE.twitter",
      "display_url" : "businessinsider.sg\/safest-cities-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919260258467446785",
  "text" : "11 cities around the world where crime is low, hospitals are plentiful, and families can feel safe https:\/\/t.co\/B9pf86nDNm",
  "id" : 919260258467446785,
  "created_at" : "2017-10-14 17:54:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Asher Wolf",
      "screen_name" : "Asher_Wolf",
      "indices" : [ 3, 14 ],
      "id_str" : "15486485",
      "id" : 15486485
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/A1Ud3AqURa",
      "expanded_url" : "https:\/\/twitter.com\/ft\/status\/918607243502813184",
      "display_url" : "twitter.com\/ft\/status\/9186\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919214579007225856",
  "text" : "RT @Asher_Wolf: This is a hotel. You're building a hotel. https:\/\/t.co\/A1Ud3AqURa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/A1Ud3AqURa",
        "expanded_url" : "https:\/\/twitter.com\/ft\/status\/918607243502813184",
        "display_url" : "twitter.com\/ft\/status\/9186\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "918675857224245248",
    "text" : "This is a hotel. You're building a hotel. https:\/\/t.co\/A1Ud3AqURa",
    "id" : 918675857224245248,
    "created_at" : "2017-10-13 03:12:47 +0000",
    "user" : {
      "name" : "Asher Wolf",
      "screen_name" : "Asher_Wolf",
      "protected" : false,
      "id_str" : "15486485",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1036115872316715008\/lenppiB5_normal.jpg",
      "id" : 15486485,
      "verified" : true
    }
  },
  "id" : 919214579007225856,
  "created_at" : "2017-10-14 14:53:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "919205093504897024",
  "geo" : { },
  "id_str" : "919214340582051842",
  "in_reply_to_user_id" : 64289556,
  "text" : "@KapilMespil Self doubt",
  "id" : 919214340582051842,
  "in_reply_to_status_id" : 919205093504897024,
  "created_at" : "2017-10-14 14:52:31 +0000",
  "in_reply_to_screen_name" : "KapilKhannaIRL",
  "in_reply_to_user_id_str" : "64289556",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919160681999593473\/photo\/1",
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/QNhrASw8B2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMGDvcNXUAABi-1.jpg",
      "id_str" : "919160650009628672",
      "id" : 919160650009628672,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMGDvcNXUAABi-1.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QNhrASw8B2"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "919160681999593473",
  "text" : "Digital Influencer is a thing now. https:\/\/t.co\/QNhrASw8B2",
  "id" : 919160681999593473,
  "created_at" : "2017-10-14 11:19:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/919132062287876096\/photo\/1",
      "indices" : [ 140, 163 ],
      "url" : "https:\/\/t.co\/JQSGz7tyz0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMFpvR6XkAE0DRY.jpg",
      "id_str" : "919132059943276545",
      "id" : 919132059943276545,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMFpvR6XkAE0DRY.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/JQSGz7tyz0"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/PsyFxYpXGN",
      "expanded_url" : "http:\/\/ift.tt\/2g8kghw",
      "display_url" : "ift.tt\/2g8kghw"
    } ]
  },
  "geo" : { },
  "id_str" : "919132062287876096",
  "text" : "A gated allotment in D8. UNDER the allotment is an archaeological site (said to be a medieval castle) waiting to b\u2026 https:\/\/t.co\/PsyFxYpXGN https:\/\/t.co\/JQSGz7tyz0",
  "id" : 919132062287876096,
  "created_at" : "2017-10-14 09:25:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Atticus Goldfinch",
      "screen_name" : "AtticusGF",
      "indices" : [ 3, 13 ],
      "id_str" : "93269181",
      "id" : 93269181
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/NACw6AS05m",
      "expanded_url" : "https:\/\/twitter.com\/aldotcom\/status\/918891089653895168",
      "display_url" : "twitter.com\/aldotcom\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919130657867161600",
  "text" : "RT @AtticusGF: THATS THE POINT OF THE FUCKING BOOK https:\/\/t.co\/NACw6AS05m",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/NACw6AS05m",
        "expanded_url" : "https:\/\/twitter.com\/aldotcom\/status\/918891089653895168",
        "display_url" : "twitter.com\/aldotcom\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "918981747882070016",
    "text" : "THATS THE POINT OF THE FUCKING BOOK https:\/\/t.co\/NACw6AS05m",
    "id" : 918981747882070016,
    "created_at" : "2017-10-13 23:28:17 +0000",
    "user" : {
      "name" : "Atticus Goldfinch",
      "screen_name" : "AtticusGF",
      "protected" : false,
      "id_str" : "93269181",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966040496014475264\/WUXZMJvc_normal.jpg",
      "id" : 93269181,
      "verified" : false
    }
  },
  "id" : 919130657867161600,
  "created_at" : "2017-10-14 09:20:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OPENHOUSEDUBLIN",
      "indices" : [ 35, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/fGgVYKXNGo",
      "expanded_url" : "https:\/\/twitter.com\/libertiesdublin\/status\/919115675586711552",
      "display_url" : "twitter.com\/libertiesdubli\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "919130376924254208",
  "text" : "An illustration shown at yesterday #OPENHOUSEDUBLIN  by DCC staff at Dublin 8. https:\/\/t.co\/fGgVYKXNGo",
  "id" : 919130376924254208,
  "created_at" : "2017-10-14 09:18:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Moylan",
      "screen_name" : "JohnMoylan01",
      "indices" : [ 3, 16 ],
      "id_str" : "235973288",
      "id" : 235973288
    }, {
      "name" : "Great Ormond Street Hospital",
      "screen_name" : "GreatOrmondSt",
      "indices" : [ 44, 58 ],
      "id_str" : "786227731167981568",
      "id" : 786227731167981568
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918944277643882497",
  "text" : "RT @JohnMoylan01: Taking daughter home from @GreatOrmondSt - licensed driver refuses to take my money..says it\u2019s a London cabbie tradition.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Great Ormond Street Hospital",
        "screen_name" : "GreatOrmondSt",
        "indices" : [ 26, 40 ],
        "id_str" : "786227731167981568",
        "id" : 786227731167981568
      }, {
        "name" : "TfL Taxi & Private Hire",
        "screen_name" : "TfLTPH",
        "indices" : [ 130, 137 ],
        "id_str" : "538103919",
        "id" : 538103919
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JohnMoylan01\/status\/918491111592689664\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/KGzthYvJYI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DL8ixIFXcAAPFjV.jpg",
        "id_str" : "918491076385796096",
        "id" : 918491076385796096,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DL8ixIFXcAAPFjV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KGzthYvJYI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "918491111592689664",
    "text" : "Taking daughter home from @GreatOrmondSt - licensed driver refuses to take my money..says it\u2019s a London cabbie tradition. Amazing.@TfLTPH https:\/\/t.co\/KGzthYvJYI",
    "id" : 918491111592689664,
    "created_at" : "2017-10-12 14:58:40 +0000",
    "user" : {
      "name" : "John Moylan",
      "screen_name" : "JohnMoylan01",
      "protected" : false,
      "id_str" : "235973288",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3602532977\/06b8064753d872580b2856bc159bf302_normal.jpeg",
      "id" : 235973288,
      "verified" : false
    }
  },
  "id" : 918944277643882497,
  "created_at" : "2017-10-13 20:59:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Craig Tomlin",
      "screen_name" : "ctomlin",
      "indices" : [ 3, 11 ],
      "id_str" : "15395410",
      "id" : 15395410
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "VR",
      "indices" : [ 36, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/ZwzuaszMDG",
      "expanded_url" : "http:\/\/ow.ly\/T1ok30fQSkO",
      "display_url" : "ow.ly\/T1ok30fQSkO"
    } ]
  },
  "geo" : { },
  "id_str" : "918930316634546176",
  "text" : "RT @ctomlin: Chinese startup's '8K' #VR headset is surprisingly advanced [removing screen door effect] | Engadget https:\/\/t.co\/ZwzuaszMDG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "VR",
        "indices" : [ 23, 26 ]
      } ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/ZwzuaszMDG",
        "expanded_url" : "http:\/\/ow.ly\/T1ok30fQSkO",
        "display_url" : "ow.ly\/T1ok30fQSkO"
      } ]
    },
    "geo" : { },
    "id_str" : "918926865477394432",
    "text" : "Chinese startup's '8K' #VR headset is surprisingly advanced [removing screen door effect] | Engadget https:\/\/t.co\/ZwzuaszMDG",
    "id" : 918926865477394432,
    "created_at" : "2017-10-13 19:50:12 +0000",
    "user" : {
      "name" : "Craig Tomlin",
      "screen_name" : "ctomlin",
      "protected" : false,
      "id_str" : "15395410",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/597143283\/headshot2_normal.JPG",
      "id" : 15395410,
      "verified" : false
    }
  },
  "id" : 918930316634546176,
  "created_at" : "2017-10-13 20:03:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "I Am Devloper",
      "screen_name" : "iamdevloper",
      "indices" : [ 3, 15 ],
      "id_str" : "564919357",
      "id" : 564919357
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918780258895257601",
  "text" : "RT @iamdevloper: normal person lying in bed:\n\"did I lock the back door?\"\n\nprogrammer lying in bed:\n\"did I close the database connection?\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "918447210580336640",
    "text" : "normal person lying in bed:\n\"did I lock the back door?\"\n\nprogrammer lying in bed:\n\"did I close the database connection?\"",
    "id" : 918447210580336640,
    "created_at" : "2017-10-12 12:04:13 +0000",
    "user" : {
      "name" : "I Am Devloper",
      "screen_name" : "iamdevloper",
      "protected" : false,
      "id_str" : "564919357",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/477397164453527552\/uh2w1u1o_normal.jpeg",
      "id" : 564919357,
      "verified" : false
    }
  },
  "id" : 918780258895257601,
  "created_at" : "2017-10-13 10:07:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918754598474657792\/photo\/1",
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/wA6bXoOegX",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DMASb9xWAAA1nNp.jpg",
      "id_str" : "918754595630874624",
      "id" : 918754595630874624,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DMASb9xWAAA1nNp.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wA6bXoOegX"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/B7JEHnrEOv",
      "expanded_url" : "http:\/\/ift.tt\/2zl02JJ",
      "display_url" : "ift.tt\/2zl02JJ"
    } ]
  },
  "geo" : { },
  "id_str" : "918754598474657792",
  "text" : "Good Morning. TGIF. Can you smell Monday? https:\/\/t.co\/B7JEHnrEOv https:\/\/t.co\/wA6bXoOegX",
  "id" : 918754598474657792,
  "created_at" : "2017-10-13 08:25:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 128, 151 ],
      "url" : "https:\/\/t.co\/Mo5Lb4bdTF",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918600075017949187",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "918602108244516866",
  "text" : "When you share your data analysis with other project stakeholder,  avoid code-heavy notebook. A cleaner report goes a long way. https:\/\/t.co\/Mo5Lb4bdTF",
  "id" : 918602108244516866,
  "created_at" : "2017-10-12 22:19:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918600075017949187\/photo\/1",
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/1mUN6wOMls",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DL-FukHW4AA4kT6.jpg",
      "id_str" : "918599884021882880",
      "id" : 918599884021882880,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DL-FukHW4AA4kT6.jpg",
      "sizes" : [ {
        "h" : 52,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 62,
        "resize" : "fit",
        "w" : 810
      }, {
        "h" : 62,
        "resize" : "fit",
        "w" : 810
      }, {
        "h" : 62,
        "resize" : "fit",
        "w" : 810
      }, {
        "h" : 62,
        "resize" : "crop",
        "w" : 62
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1mUN6wOMls"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918600075017949187",
  "text" : "Hands down this is my favourite cell magic in Juypter notebooks. What yours? https:\/\/t.co\/1mUN6wOMls",
  "id" : 918600075017949187,
  "created_at" : "2017-10-12 22:11:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tara Mulholland",
      "screen_name" : "tara_mulholland",
      "indices" : [ 3, 19 ],
      "id_str" : "221689934",
      "id" : 221689934
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918487728769191937",
  "text" : "RT @tara_mulholland: Jigsaw has wrapped Oxford Circus station in a big pro-immigration ad campaign - people were taking pics of this text,\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tara_mulholland\/status\/918095260433240065\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/UvPDU7nmpL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DL26wthWAAAHNSY.jpg",
        "id_str" : "918095245069385728",
        "id" : 918095245069385728,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DL26wthWAAAHNSY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UvPDU7nmpL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "918095260433240065",
    "text" : "Jigsaw has wrapped Oxford Circus station in a big pro-immigration ad campaign - people were taking pics of this text, not the clothes photos https:\/\/t.co\/UvPDU7nmpL",
    "id" : 918095260433240065,
    "created_at" : "2017-10-11 12:45:42 +0000",
    "user" : {
      "name" : "Tara Mulholland",
      "screen_name" : "tara_mulholland",
      "protected" : false,
      "id_str" : "221689934",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/872811272626200576\/S4o6FLq2_normal.jpg",
      "id" : 221689934,
      "verified" : true
    }
  },
  "id" : 918487728769191937,
  "created_at" : "2017-10-12 14:45:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Noam Ross",
      "screen_name" : "noamross",
      "indices" : [ 0, 9 ],
      "id_str" : "97582853",
      "id" : 97582853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "918415354296258560",
  "geo" : { },
  "id_str" : "918421987671859200",
  "in_reply_to_user_id" : 97582853,
  "text" : "@noamross Thank You",
  "id" : 918421987671859200,
  "in_reply_to_status_id" : 918415354296258560,
  "created_at" : "2017-10-12 10:23:59 +0000",
  "in_reply_to_screen_name" : "noamross",
  "in_reply_to_user_id_str" : "97582853",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Virgin Media Ireland",
      "screen_name" : "VirginMediaIE",
      "indices" : [ 0, 14 ],
      "id_str" : "289909806",
      "id" : 289909806
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918417378769358848\/photo\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/zfQwW63BHo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DL7fuEZW4AAWpdD.jpg",
      "id_str" : "918417356577234944",
      "id" : 918417356577234944,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DL7fuEZW4AAWpdD.jpg",
      "sizes" : [ {
        "h" : 363,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 206,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 409,
        "resize" : "fit",
        "w" : 1353
      }, {
        "h" : 409,
        "resize" : "fit",
        "w" : 1353
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/zfQwW63BHo"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918417378769358848",
  "in_reply_to_user_id" : 289909806,
  "text" : "@VirginMediaIE I got this and I do not remember I set this up. Can you advice? Thanks https:\/\/t.co\/zfQwW63BHo",
  "id" : 918417378769358848,
  "created_at" : "2017-10-12 10:05:41 +0000",
  "in_reply_to_screen_name" : "VirginMediaIE",
  "in_reply_to_user_id_str" : "289909806",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918412889182130176\/photo\/1",
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/qnZOuEchRu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DL7bPtrWAAE4Evw.jpg",
      "id_str" : "918412437036072961",
      "id" : 918412437036072961,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DL7bPtrWAAE4Evw.jpg",
      "sizes" : [ {
        "h" : 85,
        "resize" : "fit",
        "w" : 128
      }, {
        "h" : 85,
        "resize" : "fit",
        "w" : 128
      }, {
        "h" : 85,
        "resize" : "fit",
        "w" : 128
      }, {
        "h" : 85,
        "resize" : "fit",
        "w" : 128
      }, {
        "h" : 85,
        "resize" : "crop",
        "w" : 85
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qnZOuEchRu"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918412889182130176",
  "text" : "A rose by any other name. Say \u201CDocker\u201D, and you quite possibly mean lightweight virtual machine using Linux Containers https:\/\/t.co\/qnZOuEchRu",
  "id" : 918412889182130176,
  "created_at" : "2017-10-12 09:47:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 20, 27 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/LXqO3YpuNf",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/917768725792743426",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "918404591024304128",
  "text" : "*Sounds of Cricket* #rstats https:\/\/t.co\/LXqO3YpuNf",
  "id" : 918404591024304128,
  "created_at" : "2017-10-12 09:14:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918397994160017408",
  "text" : "Passed by a creche and the smell of soup from the creche make me hungry on a cold morning.",
  "id" : 918397994160017408,
  "created_at" : "2017-10-12 08:48:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918362414604840961",
  "text" : "Cloudron.io, a cPanel-like application (as far as the user is concerned) for launching dockerised applications.",
  "id" : 918362414604840961,
  "created_at" : "2017-10-12 06:27:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "indices" : [ 3, 13 ],
      "id_str" : "77901568",
      "id" : 77901568
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PyConIE",
      "indices" : [ 62, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/9F3jTZljnA",
      "expanded_url" : "http:\/\/schedule.pycon.python.ie\/#9Nt4ZO36kC3nSsWS8B",
      "display_url" : "schedule.pycon.python.ie\/#9Nt4ZO36kC3nS\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "918123416041021441",
  "text" : "RT @allentv4u: Hope to see you at my workshop about Django at #PyConIE https:\/\/t.co\/9F3jTZljnA I will upload details about my workshop on G\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "PyConIE",
        "indices" : [ 47, 55 ]
      } ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/9F3jTZljnA",
        "expanded_url" : "http:\/\/schedule.pycon.python.ie\/#9Nt4ZO36kC3nSsWS8B",
        "display_url" : "schedule.pycon.python.ie\/#9Nt4ZO36kC3nS\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "918027166385954816",
    "text" : "Hope to see you at my workshop about Django at #PyConIE https:\/\/t.co\/9F3jTZljnA I will upload details about my workshop on Github soon.",
    "id" : 918027166385954816,
    "created_at" : "2017-10-11 08:15:07 +0000",
    "user" : {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "protected" : false,
      "id_str" : "77901568",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/787980171202686976\/QbSu0IKB_normal.jpg",
      "id" : 77901568,
      "verified" : false
    }
  },
  "id" : 918123416041021441,
  "created_at" : "2017-10-11 14:37:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Farrell",
      "screen_name" : "ResearchLisa",
      "indices" : [ 3, 16 ],
      "id_str" : "915591945124220929",
      "id" : 915591945124220929
    }, {
      "name" : "Hannah Fry",
      "screen_name" : "FryRsquared",
      "indices" : [ 103, 115 ],
      "id_str" : "273375532",
      "id" : 273375532
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AdaLovelaceDay",
      "indices" : [ 116, 131 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/LMrLVyttqe",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/p030s5bx",
      "display_url" : "bbc.co.uk\/programmes\/p03\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "918053194495856640",
  "text" : "RT @ResearchLisa: Oops, a day late but this documentary is well worth a watch: https:\/\/t.co\/LMrLVyttqe @FryRsquared #AdaLovelaceDay #womeni\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hannah Fry",
        "screen_name" : "FryRsquared",
        "indices" : [ 85, 97 ],
        "id_str" : "273375532",
        "id" : 273375532
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AdaLovelaceDay",
        "indices" : [ 98, 113 ]
      }, {
        "text" : "womeninSTEM",
        "indices" : [ 114, 126 ]
      } ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/LMrLVyttqe",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/p030s5bx",
        "display_url" : "bbc.co.uk\/programmes\/p03\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "918021730546995201",
    "text" : "Oops, a day late but this documentary is well worth a watch: https:\/\/t.co\/LMrLVyttqe @FryRsquared #AdaLovelaceDay #womeninSTEM",
    "id" : 918021730546995201,
    "created_at" : "2017-10-11 07:53:31 +0000",
    "user" : {
      "name" : "Lisa Farrell",
      "screen_name" : "ResearchLisa",
      "protected" : false,
      "id_str" : "915591945124220929",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/938407231787294720\/VRnk5KU0_normal.jpg",
      "id" : 915591945124220929,
      "verified" : false
    }
  },
  "id" : 918053194495856640,
  "created_at" : "2017-10-11 09:58:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 122, 145 ],
      "url" : "https:\/\/t.co\/N9aUPUwz7P",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/918048862853652480",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "918051169129426945",
  "text" : "I suppose those working in DevOps already read about this book and a free pair of socks as a novelty get their attention? https:\/\/t.co\/N9aUPUwz7P",
  "id" : 918051169129426945,
  "created_at" : "2017-10-11 09:50:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/jzzCc8o6sH",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/917734708821266433",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "918048862853652480",
  "text" : "Once I propose as part of my interview for a marketing job to give away book consider the Bible for DevOps. https:\/\/t.co\/jzzCc8o6sH",
  "id" : 918048862853652480,
  "created_at" : "2017-10-11 09:41:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Python Ireland",
      "screen_name" : "PythonIreland",
      "indices" : [ 3, 17 ],
      "id_str" : "21097510",
      "id" : 21097510
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Python",
      "indices" : [ 124, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "918003651016380416",
  "text" : "RT @PythonIreland: Retweet this for a chance to win 1 of 2 free tickets for PyConIE 2017 (closing date : Sat 14th Oct Noon) #Python https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Python",
        "indices" : [ 105, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/vnLMD4sb3q",
        "expanded_url" : "https:\/\/twitter.com\/PythonIreland\/status\/910501997941542912",
        "display_url" : "twitter.com\/PythonIreland\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "917852221659865088",
    "text" : "Retweet this for a chance to win 1 of 2 free tickets for PyConIE 2017 (closing date : Sat 14th Oct Noon) #Python https:\/\/t.co\/vnLMD4sb3q",
    "id" : 917852221659865088,
    "created_at" : "2017-10-10 20:39:57 +0000",
    "user" : {
      "name" : "Python Ireland",
      "screen_name" : "PythonIreland",
      "protected" : false,
      "id_str" : "21097510",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/907178042807775232\/z9oQEEMl_normal.jpg",
      "id" : 21097510,
      "verified" : false
    }
  },
  "id" : 918003651016380416,
  "created_at" : "2017-10-11 06:41:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917841136982601730",
  "text" : "RT @SarcasmMother: Holding down the power button on a computer until it turns off feels like you are strangling someone.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "917819930015277056",
    "text" : "Holding down the power button on a computer until it turns off feels like you are strangling someone.",
    "id" : 917819930015277056,
    "created_at" : "2017-10-10 18:31:38 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 917841136982601730,
  "created_at" : "2017-10-10 19:55:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paris Gourtsoyannis",
      "screen_name" : "thistlejohn",
      "indices" : [ 3, 15 ],
      "id_str" : "87291538",
      "id" : 87291538
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CatalanReferendum",
      "indices" : [ 117, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917806437065162757",
  "text" : "RT @thistlejohn: If the UK could hold a mutually respected referendum in Scotland, Puigdemont says, why can't Spain? #CatalanReferendum",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CatalanReferendum",
        "indices" : [ 100, 118 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "917804876431847424",
    "text" : "If the UK could hold a mutually respected referendum in Scotland, Puigdemont says, why can't Spain? #CatalanReferendum",
    "id" : 917804876431847424,
    "created_at" : "2017-10-10 17:31:49 +0000",
    "user" : {
      "name" : "Paris Gourtsoyannis",
      "screen_name" : "thistlejohn",
      "protected" : false,
      "id_str" : "87291538",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894108218942795776\/AQcY3Cl-_normal.jpg",
      "id" : 87291538,
      "verified" : false
    }
  },
  "id" : 917806437065162757,
  "created_at" : "2017-10-10 17:38:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/pMTNgSEG0l",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/916751795246059521",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "917769529635241990",
  "text" : "Sometimes parents need to be awared that their children has grown up - married, living in a different culture etc. https:\/\/t.co\/pMTNgSEG0l",
  "id" : 917769529635241990,
  "created_at" : "2017-10-10 15:11:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 71, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/ICCNxjEIIM",
      "expanded_url" : "https:\/\/pip.readthedocs.io\/en\/1.1\/requirements.html",
      "display_url" : "pip.readthedocs.io\/en\/1.1\/require\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "917768725792743426",
  "text" : "Is there something like requirements.txt for R?https:\/\/t.co\/ICCNxjEIIM #rstats",
  "id" : 917768725792743426,
  "created_at" : "2017-10-10 15:08:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917734708821266433",
  "text" : "I seen it all. Tech firm offer a free pair of socks when you stop by their booth on a tech conference.",
  "id" : 917734708821266433,
  "created_at" : "2017-10-10 12:52:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917703973930446848",
  "text" : "Did not expect this to happen with Microsoft. Giving access to a Jupyter Server and running it in Linux (Ubuntu) both open-source project",
  "id" : 917703973930446848,
  "created_at" : "2017-10-10 10:50:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/83ghPbgW6r",
      "expanded_url" : "http:\/\/adweek.it\/2fWCkLg",
      "display_url" : "adweek.it\/2fWCkLg"
    } ]
  },
  "geo" : { },
  "id_str" : "917693393257541634",
  "text" : "WPP Firms Lobbied for the NRA While Its Agencies Made Gun Control Ads, Records Show https:\/\/t.co\/83ghPbgW6r",
  "id" : 917693393257541634,
  "created_at" : "2017-10-10 10:08:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/917688659268329472\/photo\/1",
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/yQ7apKugDT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLxI9phXkAALYyV.jpg",
      "id_str" : "917688648031768576",
      "id" : 917688648031768576,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLxI9phXkAALYyV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yQ7apKugDT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917688659268329472",
  "text" : "Jayus.... https:\/\/t.co\/yQ7apKugDT",
  "id" : 917688659268329472,
  "created_at" : "2017-10-10 09:50:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/917680827571589120\/photo\/1",
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/LpIwjWJX0O",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLxB2V2XUAAfe2e.jpg",
      "id_str" : "917680825910644736",
      "id" : 917680825910644736,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLxB2V2XUAAfe2e.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LpIwjWJX0O"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/POG4o88Uxc",
      "expanded_url" : "http:\/\/ift.tt\/2xvnkzc",
      "display_url" : "ift.tt\/2xvnkzc"
    } ]
  },
  "geo" : { },
  "id_str" : "917680827571589120",
  "text" : "Each going for Euro 0.49 Halloween is coming https:\/\/t.co\/POG4o88Uxc https:\/\/t.co\/LpIwjWJX0O",
  "id" : 917680827571589120,
  "created_at" : "2017-10-10 09:18:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/GXomIL13zq",
      "expanded_url" : "https:\/\/notebooks.azure.com\/mryap\/libraries\/user-friendly-notebook",
      "display_url" : "notebooks.azure.com\/mryap\/librarie\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "917555892370931713",
  "text" : "Is there something like requirements.txt for R?  https:\/\/t.co\/GXomIL13zq",
  "id" : 917555892370931713,
  "created_at" : "2017-10-10 01:02:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "indices" : [ 3, 15 ],
      "id_str" : "3760488922",
      "id" : 3760488922
    }, {
      "name" : "BBC Newsnight",
      "screen_name" : "BBCNewsnight",
      "indices" : [ 17, 30 ],
      "id_str" : "20543416",
      "id" : 20543416
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/S5OLFPVude",
      "expanded_url" : "https:\/\/blogs.scientificamerican.com\/mind-guest-blog\/what-experts-wish-you-knew-about-false-memories\/",
      "display_url" : "blogs.scientificamerican.com\/mind-guest-blo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "917517184838062080",
  "text" : "RT @drjuliashaw: @BBCNewsnight And this one; \"What Experts Wish You Knew about False Memories\" https:\/\/t.co\/S5OLFPVude",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC Newsnight",
        "screen_name" : "BBCNewsnight",
        "indices" : [ 0, 13 ],
        "id_str" : "20543416",
        "id" : 20543416
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/S5OLFPVude",
        "expanded_url" : "https:\/\/blogs.scientificamerican.com\/mind-guest-blog\/what-experts-wish-you-knew-about-false-memories\/",
        "display_url" : "blogs.scientificamerican.com\/mind-guest-blo\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "917479016348749829",
    "geo" : { },
    "id_str" : "917480088513413120",
    "in_reply_to_user_id" : 3760488922,
    "text" : "@BBCNewsnight And this one; \"What Experts Wish You Knew about False Memories\" https:\/\/t.co\/S5OLFPVude",
    "id" : 917480088513413120,
    "in_reply_to_status_id" : 917479016348749829,
    "created_at" : "2017-10-09 20:01:13 +0000",
    "in_reply_to_screen_name" : "drjuliashaw",
    "in_reply_to_user_id_str" : "3760488922",
    "user" : {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "protected" : false,
      "id_str" : "3760488922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989936592591650817\/TRkR3cmy_normal.jpg",
      "id" : 3760488922,
      "verified" : true
    }
  },
  "id" : 917517184838062080,
  "created_at" : "2017-10-09 22:28:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vincent Champain",
      "screen_name" : "vchampain",
      "indices" : [ 3, 13 ],
      "id_str" : "23342797",
      "id" : 23342797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917358116035072000",
  "text" : "RT @vchampain: \"Libertarian paternalism is not an oxymoron\" (Richard Thaler, 2017 Economics Nobel)",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "917357183637032961",
    "text" : "\"Libertarian paternalism is not an oxymoron\" (Richard Thaler, 2017 Economics Nobel)",
    "id" : 917357183637032961,
    "created_at" : "2017-10-09 11:52:50 +0000",
    "user" : {
      "name" : "Vincent Champain",
      "screen_name" : "vchampain",
      "protected" : false,
      "id_str" : "23342797",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/777563228540919808\/UiBaGxFb_normal.jpg",
      "id" : 23342797,
      "verified" : true
    }
  },
  "id" : 917358116035072000,
  "created_at" : "2017-10-09 11:56:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917352641868582912",
  "text" : "Just test out this R code. df &lt;- read.csv(file.choose(), header=TRUE) It works on Jupyter (i.e. in a browser environment)",
  "id" : 917352641868582912,
  "created_at" : "2017-10-09 11:34:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 144, 167 ],
      "url" : "https:\/\/t.co\/Oa8rVo3nSF",
      "expanded_url" : "https:\/\/twitter.com\/qz\/status\/917326757916823552",
      "display_url" : "twitter.com\/qz\/status\/9173\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "917347120079925248",
  "text" : "His work explored how human psychology shaped economic decisions. He wrote \"nudge\" on improving decisions about Health, Wealth, &amp; Happiness https:\/\/t.co\/Oa8rVo3nSF",
  "id" : 917347120079925248,
  "created_at" : "2017-10-09 11:12:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "googlecloudonboard",
      "indices" : [ 18, 37 ]
    }, {
      "text" : "dublin",
      "indices" : [ 38, 45 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917315626418524160",
  "text" : "Suppose to attend #googlecloudonboard #dublin but choose to focus on the analytics and modelling side of data.",
  "id" : 917315626418524160,
  "created_at" : "2017-10-09 09:07:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Internet of Shit",
      "screen_name" : "internetofshit",
      "indices" : [ 3, 18 ],
      "id_str" : "3356531254",
      "id" : 3356531254
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/internetofshit\/status\/917152653557112833\/photo\/1",
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/1Iw5LkV8jc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLpheb4U8AAtEP-.jpg",
      "id_str" : "917152649631297536",
      "id" : 917152649631297536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLpheb4U8AAtEP-.jpg",
      "sizes" : [ {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1Iw5LkV8jc"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917314738081722368",
  "text" : "RT @internetofshit: destination: wrong code https:\/\/t.co\/1Iw5LkV8jc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/internetofshit\/status\/917152653557112833\/photo\/1",
        "indices" : [ 24, 47 ],
        "url" : "https:\/\/t.co\/1Iw5LkV8jc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLpheb4U8AAtEP-.jpg",
        "id_str" : "917152649631297536",
        "id" : 917152649631297536,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLpheb4U8AAtEP-.jpg",
        "sizes" : [ {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1Iw5LkV8jc"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "917152653557112833",
    "text" : "destination: wrong code https:\/\/t.co\/1Iw5LkV8jc",
    "id" : 917152653557112833,
    "created_at" : "2017-10-08 22:20:07 +0000",
    "user" : {
      "name" : "Internet of Shit",
      "screen_name" : "internetofshit",
      "protected" : false,
      "id_str" : "3356531254",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/616895706150797312\/ol4PeiHz_normal.png",
      "id" : 3356531254,
      "verified" : false
    }
  },
  "id" : 917314738081722368,
  "created_at" : "2017-10-09 09:04:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/917233960509304832\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/LLmF3PMKd4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLqrbKXVwAAEmSW.jpg",
      "id_str" : "917233957250383872",
      "id" : 917233957250383872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLqrbKXVwAAEmSW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 363,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LLmF3PMKd4"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/piLQh62ob6",
      "expanded_url" : "https:\/\/cna.asia\/2gnjBJr",
      "display_url" : "cna.asia\/2gnjBJr"
    } ]
  },
  "geo" : { },
  "id_str" : "917273551253790720",
  "text" : "RT @ChannelNewsAsia: Nokia 3310 3G to go on sale in Singapore for S$99 https:\/\/t.co\/piLQh62ob6 https:\/\/t.co\/LLmF3PMKd4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/917233960509304832\/photo\/1",
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/LLmF3PMKd4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLqrbKXVwAAEmSW.jpg",
        "id_str" : "917233957250383872",
        "id" : 917233957250383872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLqrbKXVwAAEmSW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LLmF3PMKd4"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/piLQh62ob6",
        "expanded_url" : "https:\/\/cna.asia\/2gnjBJr",
        "display_url" : "cna.asia\/2gnjBJr"
      } ]
    },
    "geo" : { },
    "id_str" : "917233960509304832",
    "text" : "Nokia 3310 3G to go on sale in Singapore for S$99 https:\/\/t.co\/piLQh62ob6 https:\/\/t.co\/LLmF3PMKd4",
    "id" : 917233960509304832,
    "created_at" : "2017-10-09 03:43:12 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 917273551253790720,
  "created_at" : "2017-10-09 06:20:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/Q3SNXdGZKo",
      "expanded_url" : "http:\/\/edition.cnn.com\/style\/article\/worlds-best-school-designs\/index.html?gallery=%2F%2Fi2.cdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F160908142452-school-design-4.jpg",
      "display_url" : "edition.cnn.com\/style\/article\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "917034411111182336",
  "text" : "The curved design of the Learning Hub at Nanyang Technology University reminds me of those Dim Sum baskets https:\/\/t.co\/Q3SNXdGZKo",
  "id" : 917034411111182336,
  "created_at" : "2017-10-08 14:30:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ProperFood.ie",
      "screen_name" : "ProperFood_ie",
      "indices" : [ 3, 17 ],
      "id_str" : "2279127949",
      "id" : 2279127949
    }, {
      "name" : "Rachel Allen",
      "screen_name" : "rachelallen1",
      "indices" : [ 53, 66 ],
      "id_str" : "134608396",
      "id" : 134608396
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "917031634339356672",
  "text" : "RT @ProperFood_ie: COMPETITION KLAXON: Win a copy of @rachelallen1 's new book by replying with your all time favourite cake. RTs appreciat\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rachel Allen",
        "screen_name" : "rachelallen1",
        "indices" : [ 34, 47 ],
        "id_str" : "134608396",
        "id" : 134608396
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ProperFood_ie\/status\/915619981471223810\/photo\/1",
        "indices" : [ 124, 147 ],
        "url" : "https:\/\/t.co\/eUeVS7VPa5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLTu9avWsAAfjFX.jpg",
        "id_str" : "915619363180425216",
        "id" : 915619363180425216,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLTu9avWsAAfjFX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 255,
          "resize" : "fit",
          "w" : 198
        }, {
          "h" : 255,
          "resize" : "fit",
          "w" : 198
        }, {
          "h" : 255,
          "resize" : "fit",
          "w" : 198
        }, {
          "h" : 255,
          "resize" : "fit",
          "w" : 198
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eUeVS7VPa5"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "915619981471223810",
    "text" : "COMPETITION KLAXON: Win a copy of @rachelallen1 's new book by replying with your all time favourite cake. RTs appreciated! https:\/\/t.co\/eUeVS7VPa5",
    "id" : 915619981471223810,
    "created_at" : "2017-10-04 16:49:49 +0000",
    "user" : {
      "name" : "ProperFood.ie",
      "screen_name" : "ProperFood_ie",
      "protected" : false,
      "id_str" : "2279127949",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943598974715523072\/Gz73sG4N_normal.jpg",
      "id" : 2279127949,
      "verified" : false
    }
  },
  "id" : 917031634339356672,
  "created_at" : "2017-10-08 14:19:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ProperFood.ie",
      "screen_name" : "ProperFood_ie",
      "indices" : [ 0, 14 ],
      "id_str" : "2279127949",
      "id" : 2279127949
    }, {
      "name" : "Rachel Allen",
      "screen_name" : "rachelallen1",
      "indices" : [ 15, 28 ],
      "id_str" : "134608396",
      "id" : 134608396
    }, {
      "name" : "HarperCollinsUK",
      "screen_name" : "HarperCollinsUK",
      "indices" : [ 29, 45 ],
      "id_str" : "40843590",
      "id" : 40843590
    }, {
      "name" : "HarperCollins",
      "screen_name" : "HarperCollins",
      "indices" : [ 46, 60 ],
      "id_str" : "46696817",
      "id" : 46696817
    }, {
      "name" : "Rory Carrick",
      "screen_name" : "EatDrinkRunFun",
      "indices" : [ 61, 76 ],
      "id_str" : "135944259",
      "id" : 135944259
    }, {
      "name" : "IBA",
      "screen_name" : "IrishBloggerA",
      "indices" : [ 77, 91 ],
      "id_str" : "3017590552",
      "id" : 3017590552
    }, {
      "name" : "BLOGGERCONF",
      "screen_name" : "BloggerConf",
      "indices" : [ 92, 104 ],
      "id_str" : "3267816495",
      "id" : 3267816495
    }, {
      "name" : "Irish Food Bloggers",
      "screen_name" : "IrishFoodies",
      "indices" : [ 105, 118 ],
      "id_str" : "200485740",
      "id" : 200485740
    }, {
      "name" : "DublinByMouth: Niamh",
      "screen_name" : "DublinByMouth",
      "indices" : [ 119, 133 ],
      "id_str" : "469213264",
      "id" : 469213264
    }, {
      "name" : "Melanie May",
      "screen_name" : "_melaniemay",
      "indices" : [ 134, 146 ],
      "id_str" : "280004509",
      "id" : 280004509
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/917031598909923328\/photo\/1",
      "indices" : [ 165, 188 ],
      "url" : "https:\/\/t.co\/by8DjsjUGk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLnzWsuX4AEt6wW.jpg",
      "id_str" : "917031570434940929",
      "id" : 917031570434940929,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLnzWsuX4AEt6wW.jpg",
      "sizes" : [ {
        "h" : 1920,
        "resize" : "fit",
        "w" : 2560
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/by8DjsjUGk"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "915619981471223810",
  "geo" : { },
  "id_str" : "917031598909923328",
  "in_reply_to_user_id" : 2279127949,
  "text" : "@ProperFood_ie @rachelallen1 @HarperCollinsUK @HarperCollins @EatDrinkRunFun @IrishBloggerA @BloggerConf @IrishFoodies @DublinByMouth @_melaniemay Sticky Lemon Cake https:\/\/t.co\/by8DjsjUGk",
  "id" : 917031598909923328,
  "in_reply_to_status_id" : 915619981471223810,
  "created_at" : "2017-10-08 14:19:05 +0000",
  "in_reply_to_screen_name" : "ProperFood_ie",
  "in_reply_to_user_id_str" : "2279127949",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/ySpWlUK59a",
      "expanded_url" : "http:\/\/londonist.com\/london\/transport\/london-cutaways",
      "display_url" : "londonist.com\/london\/transpo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916970063361662977",
  "text" : "RT @edwinksl: London's hidden tunnels revealed in amazing cutaways https:\/\/t.co\/ySpWlUK59a",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/ySpWlUK59a",
        "expanded_url" : "http:\/\/londonist.com\/london\/transport\/london-cutaways",
        "display_url" : "londonist.com\/london\/transpo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "916969293719506945",
    "text" : "London's hidden tunnels revealed in amazing cutaways https:\/\/t.co\/ySpWlUK59a",
    "id" : 916969293719506945,
    "created_at" : "2017-10-08 10:11:30 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 916970063361662977,
  "created_at" : "2017-10-08 10:14:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/916968490531205120\/photo\/1",
      "indices" : [ 140, 163 ],
      "url" : "https:\/\/t.co\/4MRiaHTptw",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLm599_X4AAVVPo.jpg",
      "id_str" : "916968473410134016",
      "id" : 916968473410134016,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLm599_X4AAVVPo.jpg",
      "sizes" : [ {
        "h" : 562,
        "resize" : "fit",
        "w" : 645
      }, {
        "h" : 562,
        "resize" : "fit",
        "w" : 645
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 562,
        "resize" : "fit",
        "w" : 645
      }, {
        "h" : 562,
        "resize" : "fit",
        "w" : 645
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4MRiaHTptw"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/P6LUITMs1X",
      "expanded_url" : "http:\/\/github.com",
      "display_url" : "github.com"
    } ]
  },
  "geo" : { },
  "id_str" : "916968490531205120",
  "text" : "TIL that megering local repository (a local folder on your PC) and the remote repository (https:\/\/t.co\/P6LUITMs1X) is unallowed by default. https:\/\/t.co\/4MRiaHTptw",
  "id" : 916968490531205120,
  "created_at" : "2017-10-08 10:08:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/vSFJeMhMGr",
      "expanded_url" : "https:\/\/blog.compass.co\/find-out-if-you-have-a-viable-ecommerce-business-before-its-too-late\/",
      "display_url" : "blog.compass.co\/find-out-if-yo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916947614729465857",
  "text" : "6 metrics you can check if you have a viable business so call \"Product\/Market Fit\" for startup people https:\/\/t.co\/vSFJeMhMGr",
  "id" : 916947614729465857,
  "created_at" : "2017-10-08 08:45:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/Ptn8rrgZ9c",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/464c5d8ff38c3355c116b88ea241b815",
      "display_url" : "gist.github.com\/mryap\/464c5d8f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916803470430228480",
  "text" : "How I organize GitHub repositories https:\/\/t.co\/Ptn8rrgZ9c",
  "id" : 916803470430228480,
  "created_at" : "2017-10-07 23:12:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "disruption",
      "indices" : [ 69, 80 ]
    }, {
      "text" : "innovation",
      "indices" : [ 85, 96 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916801604531212288",
  "text" : "RT @EmeraldDeLeeuw: \"360\" and \"holistic\" solutions needs to go where #disruption and #innovation went to die. Fix problem, provide value an\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "disruption",
        "indices" : [ 49, 60 ]
      }, {
        "text" : "innovation",
        "indices" : [ 65, 76 ]
      }, {
        "text" : "jargon",
        "indices" : [ 131, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "916797573603708928",
    "text" : "\"360\" and \"holistic\" solutions needs to go where #disruption and #innovation went to die. Fix problem, provide value and be human. #jargon",
    "id" : 916797573603708928,
    "created_at" : "2017-10-07 22:49:09 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 916801604531212288,
  "created_at" : "2017-10-07 23:05:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/9oQw776EI9",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/912276346340089857",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916784993262866432",
  "text" : "Hire me to do this for you. Privacy ensure as I be working on your PC in front of you. https:\/\/t.co\/9oQw776EI9",
  "id" : 916784993262866432,
  "created_at" : "2017-10-07 21:59:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916751795246059521",
  "text" : "Just realised after several weeks that an elder members of the family is Mansplaining and I am the receiving end.  FFS",
  "id" : 916751795246059521,
  "created_at" : "2017-10-07 19:47:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LEGO",
      "screen_name" : "LEGO_Group",
      "indices" : [ 3, 14 ],
      "id_str" : "295325513",
      "id" : 295325513
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LEGONYCC",
      "indices" : [ 59, 68 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916700935077945344",
  "text" : "RT @LEGO_Group: Retweet for your chance to win this year's #LEGONYCC exclusive. LEGO BrickHeadz Boba Fett and Han Solo in Carbonite! \uD83D\uDE0D\uD83D\uDC4F @St\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/manage.involver.com\" rel=\"nofollow\"\u003EOracle Engage\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Star Wars",
        "screen_name" : "starwars",
        "indices" : [ 120, 129 ],
        "id_str" : "20106852",
        "id" : 20106852
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LEGO_Group\/status\/916664481534693376\/photo\/1",
        "indices" : [ 130, 153 ],
        "url" : "https:\/\/t.co\/FK9KtcccA1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLilfErUEAEktiA.jpg",
        "id_str" : "916664477419835393",
        "id" : 916664477419835393,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLilfErUEAEktiA.jpg",
        "sizes" : [ {
          "h" : 456,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1340,
          "resize" : "fit",
          "w" : 2000
        }, {
          "h" : 804,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1340,
          "resize" : "fit",
          "w" : 2000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FK9KtcccA1"
      } ],
      "hashtags" : [ {
        "text" : "LEGONYCC",
        "indices" : [ 43, 52 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "916664481534693376",
    "text" : "Retweet for your chance to win this year's #LEGONYCC exclusive. LEGO BrickHeadz Boba Fett and Han Solo in Carbonite! \uD83D\uDE0D\uD83D\uDC4F @StarWars https:\/\/t.co\/FK9KtcccA1",
    "id" : 916664481534693376,
    "created_at" : "2017-10-07 14:00:17 +0000",
    "user" : {
      "name" : "LEGO",
      "screen_name" : "LEGO_Group",
      "protected" : false,
      "id_str" : "295325513",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/940608855125487618\/-W2tTgtk_normal.jpg",
      "id" : 295325513,
      "verified" : true
    }
  },
  "id" : 916700935077945344,
  "created_at" : "2017-10-07 16:25:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "disqus",
      "indices" : [ 48, 55 ]
    }, {
      "text" : "eudatap",
      "indices" : [ 57, 65 ]
    }, {
      "text" : "infosec",
      "indices" : [ 66, 74 ]
    }, {
      "text" : "gdpr",
      "indices" : [ 75, 80 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/Hyl438icY5",
      "expanded_url" : "https:\/\/twitter.com\/katebevan\/status\/916689337714081798",
      "display_url" : "twitter.com\/katebevan\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916700467215896577",
  "text" : "RT @EmeraldDeLeeuw: Another one bites the dust. #disqus  #eudatap #infosec #gdpr https:\/\/t.co\/Hyl438icY5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "disqus",
        "indices" : [ 28, 35 ]
      }, {
        "text" : "eudatap",
        "indices" : [ 37, 45 ]
      }, {
        "text" : "infosec",
        "indices" : [ 46, 54 ]
      }, {
        "text" : "gdpr",
        "indices" : [ 55, 60 ]
      } ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/Hyl438icY5",
        "expanded_url" : "https:\/\/twitter.com\/katebevan\/status\/916689337714081798",
        "display_url" : "twitter.com\/katebevan\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "916692235898572800",
    "text" : "Another one bites the dust. #disqus  #eudatap #infosec #gdpr https:\/\/t.co\/Hyl438icY5",
    "id" : 916692235898572800,
    "created_at" : "2017-10-07 15:50:35 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 916700467215896577,
  "created_at" : "2017-10-07 16:23:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PlayersXpo",
      "screen_name" : "PlayersXpo",
      "indices" : [ 0, 11 ],
      "id_str" : "869908066799693826",
      "id" : 869908066799693826
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/916681035798269952\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/tRx5uWiQAi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLi0hDmXcAAVkcO.jpg",
      "id_str" : "916681004164804608",
      "id" : 916681004164804608,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLi0hDmXcAAVkcO.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 492,
        "resize" : "fit",
        "w" : 807
      }, {
        "h" : 492,
        "resize" : "fit",
        "w" : 807
      }, {
        "h" : 492,
        "resize" : "fit",
        "w" : 807
      }, {
        "h" : 415,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tRx5uWiQAi"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "916671882694397952",
  "geo" : { },
  "id_str" : "916681035798269952",
  "in_reply_to_user_id" : 869908066799693826,
  "text" : "@PlayersXpo Thank You. It not in the Spam folder.  I have emailed support yesterday immediately. https:\/\/t.co\/tRx5uWiQAi",
  "id" : 916681035798269952,
  "in_reply_to_status_id" : 916671882694397952,
  "created_at" : "2017-10-07 15:06:04 +0000",
  "in_reply_to_screen_name" : "PlayersXpo",
  "in_reply_to_user_id_str" : "869908066799693826",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Titus Brown",
      "screen_name" : "ctitusbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "26616462",
      "id" : 26616462
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/fhFN7TOw0L",
      "expanded_url" : "https:\/\/twitter.com\/gvwilson\/status\/915976897368084481",
      "display_url" : "twitter.com\/gvwilson\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916667975129845760",
  "text" : "RT @ctitusbrown: 2017, everyone. https:\/\/t.co\/fhFN7TOw0L",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 39 ],
        "url" : "https:\/\/t.co\/fhFN7TOw0L",
        "expanded_url" : "https:\/\/twitter.com\/gvwilson\/status\/915976897368084481",
        "display_url" : "twitter.com\/gvwilson\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "915982388144033793",
    "text" : "2017, everyone. https:\/\/t.co\/fhFN7TOw0L",
    "id" : 915982388144033793,
    "created_at" : "2017-10-05 16:49:54 +0000",
    "user" : {
      "name" : "Titus Brown",
      "screen_name" : "ctitusbrown",
      "protected" : false,
      "id_str" : "26616462",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/662714429742514176\/bwLg2tBG_normal.jpg",
      "id" : 26616462,
      "verified" : false
    }
  },
  "id" : 916667975129845760,
  "created_at" : "2017-10-07 14:14:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/OAuApFl4vB",
      "expanded_url" : "http:\/\/econ.st\/2fLAvk8",
      "display_url" : "econ.st\/2fLAvk8"
    } ]
  },
  "geo" : { },
  "id_str" : "916667730916462593",
  "text" : "RT @TheEconomist: Though it may be unpopular in some countries, open borders lead to better science https:\/\/t.co\/OAuApFl4vB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/OAuApFl4vB",
        "expanded_url" : "http:\/\/econ.st\/2fLAvk8",
        "display_url" : "econ.st\/2fLAvk8"
      } ]
    },
    "geo" : { },
    "id_str" : "916550901053173760",
    "text" : "Though it may be unpopular in some countries, open borders lead to better science https:\/\/t.co\/OAuApFl4vB",
    "id" : 916550901053173760,
    "created_at" : "2017-10-07 06:28:58 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 916667730916462593,
  "created_at" : "2017-10-07 14:13:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/y3oQA2ceON",
      "expanded_url" : "https:\/\/twitter.com\/JOEdotie\/status\/916305297773342721",
      "display_url" : "twitter.com\/JOEdotie\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916597349232848896",
  "text" : "The island of Ireland remains one of the world's\u00A0lowest occupiers of skyscrapers and tall buildings. https:\/\/t.co\/y3oQA2ceON",
  "id" : 916597349232848896,
  "created_at" : "2017-10-07 09:33:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "chris horn",
      "screen_name" : "chrisjhorn",
      "indices" : [ 3, 14 ],
      "id_str" : "24028945",
      "id" : 24028945
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916393122338267136",
  "text" : "RT @chrisjhorn: Skilled Emirates pilots landing an A380 at Dusseldorf yesterday, in strong and gusty cross wind: watch rudder!!.. https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/DuSH3CJpIV",
        "expanded_url" : "https:\/\/www.youtube.com\/watch?v=roS6oFjCDhc",
        "display_url" : "youtube.com\/watch?v=roS6oF\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "916383202486489088",
    "text" : "Skilled Emirates pilots landing an A380 at Dusseldorf yesterday, in strong and gusty cross wind: watch rudder!!.. https:\/\/t.co\/DuSH3CJpIV",
    "id" : 916383202486489088,
    "created_at" : "2017-10-06 19:22:35 +0000",
    "user" : {
      "name" : "chris horn",
      "screen_name" : "chrisjhorn",
      "protected" : false,
      "id_str" : "24028945",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/765951619175424000\/50XyH-AD_normal.jpg",
      "id" : 24028945,
      "verified" : false
    }
  },
  "id" : 916393122338267136,
  "created_at" : "2017-10-06 20:02:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83C\uDF31 Matthew Martin \u24CB",
      "screen_name" : "vegdevops",
      "indices" : [ 3, 13 ],
      "id_str" : "3892439001",
      "id" : 3892439001
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/FRNNJgvv7K",
      "expanded_url" : "https:\/\/twitter.com\/i\/moments\/916342969799708672",
      "display_url" : "twitter.com\/i\/moments\/9163\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916375069387091968",
  "text" : "RT @vegdevops: \u26A1\uFE0F \u201CYou can go meatless at McDonalds\u201D (In Finland. For 1 Month)\n\nhttps:\/\/t.co\/FRNNJgvv7K",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 65, 88 ],
        "url" : "https:\/\/t.co\/FRNNJgvv7K",
        "expanded_url" : "https:\/\/twitter.com\/i\/moments\/916342969799708672",
        "display_url" : "twitter.com\/i\/moments\/9163\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "916372315281281024",
    "text" : "\u26A1\uFE0F \u201CYou can go meatless at McDonalds\u201D (In Finland. For 1 Month)\n\nhttps:\/\/t.co\/FRNNJgvv7K",
    "id" : 916372315281281024,
    "created_at" : "2017-10-06 18:39:19 +0000",
    "user" : {
      "name" : "\uD83C\uDF31 Matthew Martin \u24CB",
      "screen_name" : "vegdevops",
      "protected" : false,
      "id_str" : "3892439001",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/651834414020841474\/5Ril8C0__normal.jpg",
      "id" : 3892439001,
      "verified" : false
    }
  },
  "id" : 916375069387091968,
  "created_at" : "2017-10-06 18:50:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PlayersXpo",
      "screen_name" : "PlayersXpo",
      "indices" : [ 0, 11 ],
      "id_str" : "869908066799693826",
      "id" : 869908066799693826
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916342684427673601",
  "in_reply_to_user_id" : 869908066799693826,
  "text" : "@PlayersXpo Make payment for tickets in the morn, There is no booking reference or email confirmation. Are you able to advice? Thanks",
  "id" : 916342684427673601,
  "created_at" : "2017-10-06 16:41:35 +0000",
  "in_reply_to_screen_name" : "PlayersXpo",
  "in_reply_to_user_id_str" : "869908066799693826",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916342188103159808",
  "text" : "Remember AIM was used for internal communication in my previous job.  You could say that before there is Slack, there is AIM.",
  "id" : 916342188103159808,
  "created_at" : "2017-10-06 16:39:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916265298382196737",
  "text" : "Docker + Jupyter = \"engineering problem\" solved Now it business and data modelling problem.",
  "id" : 916265298382196737,
  "created_at" : "2017-10-06 11:34:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/5Jx7NhdIUh",
      "expanded_url" : "http:\/\/www.msn.com\/en-ie\/news\/world\/las-vegas-shooter-stephen-paddock-fired-at-airport-fuel-tanks-during-attack\/ar-AAsYj7m?li=BBr5KbJ&ocid=mailsignout",
      "display_url" : "msn.com\/en-ie\/news\/wor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916261818804432896",
  "text" : "And that association want to legalise armour piercing bullet...https:\/\/t.co\/5Jx7NhdIUh",
  "id" : 916261818804432896,
  "created_at" : "2017-10-06 11:20:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/qbo2s2qgkU",
      "expanded_url" : "http:\/\/www.independent.ie\/irish-news\/news\/families-left-fuming-as-hundreds-turned-away-from-overbooked-gamercon-dublin-35543590.html",
      "display_url" : "independent.ie\/irish-news\/new\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916244836004519936",
  "text" : "Spooked by this news https:\/\/t.co\/qbo2s2qgkU Not sure whether to attend Oct's PlayersXpo 2017. Aware they are different organiser.",
  "id" : 916244836004519936,
  "created_at" : "2017-10-06 10:12:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "gal-dem",
      "screen_name" : "galdemzine",
      "indices" : [ 3, 14 ],
      "id_str" : "3302989984",
      "id" : 3302989984
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/4U7PsRcPFP",
      "expanded_url" : "http:\/\/www.gal-dem.com\/13946-2\/",
      "display_url" : "gal-dem.com\/13946-2\/"
    } ]
  },
  "geo" : { },
  "id_str" : "916236963702890496",
  "text" : "RT @galdemzine: We should be alarmed about Dublin\u2019s upcoming immigration detention centre https:\/\/t.co\/4U7PsRcPFP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.gal-dem.com\" rel=\"nofollow\"\u003Egal-dem\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/4U7PsRcPFP",
        "expanded_url" : "http:\/\/www.gal-dem.com\/13946-2\/",
        "display_url" : "gal-dem.com\/13946-2\/"
      } ]
    },
    "geo" : { },
    "id_str" : "914761983878205440",
    "text" : "We should be alarmed about Dublin\u2019s upcoming immigration detention centre https:\/\/t.co\/4U7PsRcPFP",
    "id" : 914761983878205440,
    "created_at" : "2017-10-02 08:00:27 +0000",
    "user" : {
      "name" : "gal-dem",
      "screen_name" : "galdemzine",
      "protected" : false,
      "id_str" : "3302989984",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/780118316534927360\/hVwlI8G6_normal.jpg",
      "id" : 3302989984,
      "verified" : true
    }
  },
  "id" : 916236963702890496,
  "created_at" : "2017-10-06 09:41:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "indices" : [ 3, 17 ],
      "id_str" : "3686936548",
      "id" : 3686936548
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Jupyter4learningNteaching",
      "indices" : [ 94, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "916101385032171520",
  "text" : "RT @iworkwithdata: You can embedded YouTube instructional video playlist in Jupyter notebook  #Jupyter4learningNteaching https:\/\/t.co\/dmavm\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iworkwithdata\/status\/916101175568715777\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/dmavm7Mikg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLalKHGX4AAD_D3.jpg",
        "id_str" : "916101167339528192",
        "id" : 916101167339528192,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLalKHGX4AAD_D3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1343
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 1343
        }, {
          "h" : 347,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dmavm7Mikg"
      } ],
      "hashtags" : [ {
        "text" : "Jupyter4learningNteaching",
        "indices" : [ 75, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "916101175568715777",
    "text" : "You can embedded YouTube instructional video playlist in Jupyter notebook  #Jupyter4learningNteaching https:\/\/t.co\/dmavm7Mikg",
    "id" : 916101175568715777,
    "created_at" : "2017-10-06 00:41:55 +0000",
    "user" : {
      "name" : "HDSDA",
      "screen_name" : "iworkwithdata",
      "protected" : true,
      "id_str" : "3686936548",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647779320430202880\/04Kghk7g_normal.jpg",
      "id" : 3686936548,
      "verified" : false
    }
  },
  "id" : 916101385032171520,
  "created_at" : "2017-10-06 00:42:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/IjaNduIB1D",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/916025538946457600",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916056720841084928",
  "text" : "You have resort to using R and Python to ferret out those data instead of downloadable CSV file open by Excel https:\/\/t.co\/IjaNduIB1D",
  "id" : 916056720841084928,
  "created_at" : "2017-10-05 21:45:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "opendata",
      "indices" : [ 128, 137 ]
    } ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/fxrc1emeqr",
      "expanded_url" : "https:\/\/data.gov.ie\/data",
      "display_url" : "data.gov.ie\/data"
    } ]
  },
  "geo" : { },
  "id_str" : "916025538946457600",
  "text" : "A cursory glance at https:\/\/t.co\/fxrc1emeqr  220 datasets are available in a proprietary close format (e.g., PX instead of CSV) #opendata",
  "id" : 916025538946457600,
  "created_at" : "2017-10-05 19:41:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/FNjqtRGDDe",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/av\/world-us-canada-41434644\/conservatives-flee-too-liberal-california-for-texas",
      "display_url" : "bbc.com\/news\/av\/world-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "916015468254781441",
  "text" : "Conservatives flee 'too liberal' California for Texas https:\/\/t.co\/FNjqtRGDDe",
  "id" : 916015468254781441,
  "created_at" : "2017-10-05 19:01:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Olivia Messer \uD83D\uDC80",
      "screen_name" : "OliviaMesser",
      "indices" : [ 3, 16 ],
      "id_str" : "293065451",
      "id" : 293065451
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/OliviaMesser\/status\/915905427816222720\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/RAnr8zML2j",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLXzEPRUQAUllCj.jpg",
      "id_str" : "915905353383952389",
      "id" : 915905353383952389,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLXzEPRUQAUllCj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 569,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 616,
        "resize" : "fit",
        "w" : 736
      }, {
        "h" : 616,
        "resize" : "fit",
        "w" : 736
      }, {
        "h" : 616,
        "resize" : "fit",
        "w" : 736
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RAnr8zML2j"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915997333292187648",
  "text" : "RT @OliviaMesser: This week's political cartoons are already......intense. https:\/\/t.co\/RAnr8zML2j",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/OliviaMesser\/status\/915905427816222720\/photo\/1",
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/RAnr8zML2j",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLXzEPRUQAUllCj.jpg",
        "id_str" : "915905353383952389",
        "id" : 915905353383952389,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLXzEPRUQAUllCj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 569,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 616,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 616,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 616,
          "resize" : "fit",
          "w" : 736
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RAnr8zML2j"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "915905427816222720",
    "text" : "This week's political cartoons are already......intense. https:\/\/t.co\/RAnr8zML2j",
    "id" : 915905427816222720,
    "created_at" : "2017-10-05 11:44:05 +0000",
    "user" : {
      "name" : "Olivia Messer \uD83D\uDC80",
      "screen_name" : "OliviaMesser",
      "protected" : false,
      "id_str" : "293065451",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/952337230126776320\/NA07ErYP_normal.jpg",
      "id" : 293065451,
      "verified" : true
    }
  },
  "id" : 915997333292187648,
  "created_at" : "2017-10-05 17:49:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "daiyi! \u2728 (chris)",
      "screen_name" : "daiyitastic",
      "indices" : [ 3, 15 ],
      "id_str" : "6758842",
      "id" : 6758842
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "show",
      "indices" : [ 136, 141 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915955552102096896",
  "text" : "RT @daiyitastic: if you're frequently in &amp; out of wifi, you can enable \"show saved copy button\" to see cached pages! chrome:\/\/flags\/#show-s\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/daiyitastic\/status\/901406860170993665\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/iitl4Eaknk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DIJv7-fXcAAoU6H.jpg",
        "id_str" : "901405951605043200",
        "id" : 901405951605043200,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIJv7-fXcAAoU6H.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 420
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 420
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 420
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 420
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iitl4Eaknk"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/daiyitastic\/status\/901406860170993665\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/iitl4Eaknk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DIJwudNXsAA9aiX.jpg",
        "id_str" : "901406818844520448",
        "id" : 901406818844520448,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DIJwudNXsAA9aiX.jpg",
        "sizes" : [ {
          "h" : 602,
          "resize" : "fit",
          "w" : 747
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 602,
          "resize" : "fit",
          "w" : 747
        }, {
          "h" : 548,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 602,
          "resize" : "fit",
          "w" : 747
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iitl4Eaknk"
      } ],
      "hashtags" : [ {
        "text" : "show",
        "indices" : [ 119, 124 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "901406860170993665",
    "text" : "if you're frequently in &amp; out of wifi, you can enable \"show saved copy button\" to see cached pages! chrome:\/\/flags\/#show-saved-copy https:\/\/t.co\/iitl4Eaknk",
    "id" : 901406860170993665,
    "created_at" : "2017-08-26 11:31:57 +0000",
    "user" : {
      "name" : "daiyi! \u2728 (chris)",
      "screen_name" : "daiyitastic",
      "protected" : false,
      "id_str" : "6758842",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/965908401799684096\/KEiN8dMO_normal.jpg",
      "id" : 6758842,
      "verified" : false
    }
  },
  "id" : 915955552102096896,
  "created_at" : "2017-10-05 15:03:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/Vchof7UUVM",
      "expanded_url" : "http:\/\/thefinder.com.sg\/singapore-expat-life\/living-singapore\/dear-world-heres-what-life-without-guns-singapore-expat-andrea",
      "display_url" : "thefinder.com.sg\/singapore-expa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915951094492262400",
  "text" : "I thought the penalty of having firearms back home is capital punishment. https:\/\/t.co\/Vchof7UUVM",
  "id" : 915951094492262400,
  "created_at" : "2017-10-05 14:45:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "(((Sam Shenton))) \uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08",
      "screen_name" : "SamDoesPolitics",
      "indices" : [ 3, 19 ],
      "id_str" : "843249580351348737",
      "id" : 843249580351348737
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Jamin2g\/status\/915545773885947904\/video\/1",
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/ch1hspfI1W",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915545565320146944\/pu\/img\/jHERd1AEBvmu4Sce.jpg",
      "id_str" : "915545565320146944",
      "id" : 915545565320146944,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915545565320146944\/pu\/img\/jHERd1AEBvmu4Sce.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ch1hspfI1W"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915949303163703296",
  "text" : "RT @SamDoesPolitics: If any of us find a partner like Philip May, we\u2019ll have done rather okay https:\/\/t.co\/ch1hspfI1W",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Jamin2g\/status\/915545773885947904\/video\/1",
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/ch1hspfI1W",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915545565320146944\/pu\/img\/jHERd1AEBvmu4Sce.jpg",
        "id_str" : "915545565320146944",
        "id" : 915545565320146944,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915545565320146944\/pu\/img\/jHERd1AEBvmu4Sce.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ch1hspfI1W"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "915552971706060800",
    "text" : "If any of us find a partner like Philip May, we\u2019ll have done rather okay https:\/\/t.co\/ch1hspfI1W",
    "id" : 915552971706060800,
    "created_at" : "2017-10-04 12:23:33 +0000",
    "user" : {
      "name" : "(((Sam Shenton))) \uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08",
      "screen_name" : "SamDoesPolitics",
      "protected" : false,
      "id_str" : "843249580351348737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1041464975884206080\/wORjZ9yL_normal.jpg",
      "id" : 843249580351348737,
      "verified" : false
    }
  },
  "id" : 915949303163703296,
  "created_at" : "2017-10-05 14:38:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/6cNE8s4JeG",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/915910151470796801",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915939660110684160",
  "text" : "I have to test the hypothesis that when it pours instead of cycling, people will travel in vehicle. https:\/\/t.co\/6cNE8s4JeG",
  "id" : 915939660110684160,
  "created_at" : "2017-10-05 14:00:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/915936955099238401\/photo\/1",
      "indices" : [ 129, 152 ],
      "url" : "https:\/\/t.co\/S9oDERZlK3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLYPy55XoAAm5il.jpg",
      "id_str" : "915936941425795072",
      "id" : 915936941425795072,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLYPy55XoAAm5il.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 634,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 659,
        "resize" : "fit",
        "w" : 1248
      }, {
        "h" : 359,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 659,
        "resize" : "fit",
        "w" : 1248
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/S9oDERZlK3"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915936955099238401",
  "text" : "In Machine Learning terms, I need to \"teach\" Spotify algorithms to \"learn\" from my activities (listen to more tracks on Spotify) https:\/\/t.co\/S9oDERZlK3",
  "id" : 915936955099238401,
  "created_at" : "2017-10-05 13:49:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/4NMGHcpdmH",
      "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/839381362603851776",
      "display_url" : "twitter.com\/getoptimise\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915927203510128646",
  "text" : "Machine learning is great with a lot of diverse data. https:\/\/t.co\/4NMGHcpdmH",
  "id" : 915927203510128646,
  "created_at" : "2017-10-05 13:10:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/KY86LknWnF",
      "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6321689992930430976\/",
      "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915924651011866626",
  "text" : "ML in marketing. https:\/\/t.co\/KY86LknWnF",
  "id" : 915924651011866626,
  "created_at" : "2017-10-05 13:00:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "indices" : [ 3, 7 ],
      "id_str" : "380648579",
      "id" : 380648579
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AFP\/status\/915913738896551939\/video\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/w8Al7GbIOX",
      "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/915889411761610753\/img\/qlwE0pZY2cSXJz-6.jpg",
      "id_str" : "915889411761610753",
      "id" : 915889411761610753,
      "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/915889411761610753\/img\/qlwE0pZY2cSXJz-6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/w8Al7GbIOX"
    } ],
    "hashtags" : [ {
      "text" : "JokoWidodo",
      "indices" : [ 74, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915919936383590400",
  "text" : "RT @AFP: Walk this way: Indonesia traffic jam forces president out of car #JokoWidodo https:\/\/t.co\/w8Al7GbIOX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AFP\/status\/915913738896551939\/video\/1",
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/w8Al7GbIOX",
        "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/915889411761610753\/img\/qlwE0pZY2cSXJz-6.jpg",
        "id_str" : "915889411761610753",
        "id" : 915889411761610753,
        "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/915889411761610753\/img\/qlwE0pZY2cSXJz-6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/w8Al7GbIOX"
      } ],
      "hashtags" : [ {
        "text" : "JokoWidodo",
        "indices" : [ 65, 76 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "915913738896551939",
    "text" : "Walk this way: Indonesia traffic jam forces president out of car #JokoWidodo https:\/\/t.co\/w8Al7GbIOX",
    "id" : 915913738896551939,
    "created_at" : "2017-10-05 12:17:06 +0000",
    "user" : {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "protected" : false,
      "id_str" : "380648579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/697343883630481408\/c08JBfBB_normal.jpg",
      "id" : 380648579,
      "verified" : true
    }
  },
  "id" : 915919936383590400,
  "created_at" : "2017-10-05 12:41:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915910151470796801",
  "text" : "Looking for open data out there to account for car use in Ireland on a given period?",
  "id" : 915910151470796801,
  "created_at" : "2017-10-05 12:02:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sony Pictures SG",
      "screen_name" : "SonyPicturesSG",
      "indices" : [ 3, 18 ],
      "id_str" : "185997882",
      "id" : 185997882
    }, {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 20, 28 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BladeRunner2049",
      "indices" : [ 47, 63 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915852773912190976",
  "text" : "RT @SonyPicturesSG: @mrbrown gives his take on #BladeRunner2049.\n\nBlade Runner 2049 is in cinemas now. Get your tickets here: https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "mrbrown",
        "screen_name" : "mrbrown",
        "indices" : [ 0, 8 ],
        "id_str" : "574253",
        "id" : 574253
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SonyPicturesSG\/status\/915812771895365633\/photo\/1",
        "indices" : [ 130, 153 ],
        "url" : "https:\/\/t.co\/k3xRBCrmJJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLWe3IjXUAEjzpi.jpg",
        "id_str" : "915812769265569793",
        "id" : 915812769265569793,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLWe3IjXUAEjzpi.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/k3xRBCrmJJ"
      } ],
      "hashtags" : [ {
        "text" : "BladeRunner2049",
        "indices" : [ 27, 43 ]
      } ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/ow8m4a1Ik7",
        "expanded_url" : "https:\/\/buff.ly\/2fTc8op",
        "display_url" : "buff.ly\/2fTc8op"
      } ]
    },
    "geo" : { },
    "id_str" : "915812771895365633",
    "in_reply_to_user_id" : 574253,
    "text" : "@mrbrown gives his take on #BladeRunner2049.\n\nBlade Runner 2049 is in cinemas now. Get your tickets here: https:\/\/t.co\/ow8m4a1Ik7 https:\/\/t.co\/k3xRBCrmJJ",
    "id" : 915812771895365633,
    "created_at" : "2017-10-05 05:35:54 +0000",
    "in_reply_to_screen_name" : "mrbrown",
    "in_reply_to_user_id_str" : "574253",
    "user" : {
      "name" : "Sony Pictures SG",
      "screen_name" : "SonyPicturesSG",
      "protected" : false,
      "id_str" : "185997882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1707256385\/cts_normal.jpg",
      "id" : 185997882,
      "verified" : false
    }
  },
  "id" : 915852773912190976,
  "created_at" : "2017-10-05 08:14:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Colledge",
      "screen_name" : "MikeDColledge",
      "indices" : [ 3, 17 ],
      "id_str" : "415072889",
      "id" : 415072889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915844282283642880",
  "text" : "RT @MikeDColledge: Everyone says data has great value. Eventually the people who create the data will want a share of the value. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/7o88h7T72J",
        "expanded_url" : "https:\/\/twitter.com\/ipsoscanada\/status\/915564704499683329",
        "display_url" : "twitter.com\/ipsoscanada\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "915734516739792896",
    "text" : "Everyone says data has great value. Eventually the people who create the data will want a share of the value. https:\/\/t.co\/7o88h7T72J",
    "id" : 915734516739792896,
    "created_at" : "2017-10-05 00:24:56 +0000",
    "user" : {
      "name" : "Mike Colledge",
      "screen_name" : "MikeDColledge",
      "protected" : false,
      "id_str" : "415072889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/942781799159144448\/wK7UMTZs_normal.jpg",
      "id" : 415072889,
      "verified" : false
    }
  },
  "id" : 915844282283642880,
  "created_at" : "2017-10-05 07:41:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/QLEyEFZ151",
      "expanded_url" : "http:\/\/mailchi.mp\/e942340a93ae\/the-tech-event-you-dont-want-to-miss-this-saturday-get-your-tickets-to-404?e=9ab5a7aa14",
      "display_url" : "mailchi.mp\/e942340a93ae\/t\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915831408744116224",
  "text" : "I have discount code for the ticket to this event. https:\/\/t.co\/QLEyEFZ151 DM me.",
  "id" : 915831408744116224,
  "created_at" : "2017-10-05 06:49:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ROSIE",
      "screen_name" : "Rosie",
      "indices" : [ 3, 9 ],
      "id_str" : "25203361",
      "id" : 25203361
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/Z22pPK4PI2",
      "expanded_url" : "https:\/\/nyti.ms\/2rA4yT4",
      "display_url" : "nyti.ms\/2rA4yT4"
    } ]
  },
  "geo" : { },
  "id_str" : "915703239185768448",
  "text" : "RT @Rosie: After 37 Die in Attack at Manila Resort, Questions Mount https:\/\/t.co\/Z22pPK4PI2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/Z22pPK4PI2",
        "expanded_url" : "https:\/\/nyti.ms\/2rA4yT4",
        "display_url" : "nyti.ms\/2rA4yT4"
      } ]
    },
    "geo" : { },
    "id_str" : "915643153662668800",
    "text" : "After 37 Die in Attack at Manila Resort, Questions Mount https:\/\/t.co\/Z22pPK4PI2",
    "id" : 915643153662668800,
    "created_at" : "2017-10-04 18:21:54 +0000",
    "user" : {
      "name" : "ROSIE",
      "screen_name" : "Rosie",
      "protected" : false,
      "id_str" : "25203361",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039204692155396098\/MVTIr3SA_normal.jpg",
      "id" : 25203361,
      "verified" : true
    }
  },
  "id" : 915703239185768448,
  "created_at" : "2017-10-04 22:20:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Meadows",
      "screen_name" : "robotech_master",
      "indices" : [ 3, 19 ],
      "id_str" : "14558776",
      "id" : 14558776
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "autoplay",
      "indices" : [ 70, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915647612447002624",
  "text" : "RT @robotech_master: If you use Google Chrome, go to \n\nchrome:\/\/flags\/#autoplay-policy\n\nand set it to \"Document user activation is required\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "autoplay",
        "indices" : [ 49, 58 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "915106702923583488",
    "text" : "If you use Google Chrome, go to \n\nchrome:\/\/flags\/#autoplay-policy\n\nand set it to \"Document user activation is required.\"\n\nBoom: no more auto-playing videos.\n\nYou're welcome.",
    "id" : 915106702923583488,
    "created_at" : "2017-10-03 06:50:14 +0000",
    "user" : {
      "name" : "Chris Meadows",
      "screen_name" : "robotech_master",
      "protected" : false,
      "id_str" : "14558776",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/316771690\/toonrmicon_normal.jpg",
      "id" : 14558776,
      "verified" : false
    }
  },
  "id" : 915647612447002624,
  "created_at" : "2017-10-04 18:39:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "indices" : [ 0, 13 ],
      "id_str" : "205451951",
      "id" : 205451951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "915566179237924866",
  "geo" : { },
  "id_str" : "915568514618068994",
  "in_reply_to_user_id" : 205451951,
  "text" : "@TheChrisBray Looks like an Iron to me. \uD83D\uDE01",
  "id" : 915568514618068994,
  "in_reply_to_status_id" : 915566179237924866,
  "created_at" : "2017-10-04 13:25:18 +0000",
  "in_reply_to_screen_name" : "TheChrisBray",
  "in_reply_to_user_id_str" : "205451951",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/BVqoH5SQFY",
      "expanded_url" : "https:\/\/e27.co\/ocbc-customers-will-soon-able-transfer-funds-check-bank-accounts-via-siri-20171004\/",
      "display_url" : "e27.co\/ocbc-customers\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915521427343859712",
  "text" : "HELLO Siri, Please transfer Euro 1000 to Yap of @getoptimise  https:\/\/t.co\/BVqoH5SQFY",
  "id" : 915521427343859712,
  "created_at" : "2017-10-04 10:18:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915497515667066881",
  "text" : "RT @Dotnetster: @0x4d4147 Check out https:\/\/t.co\/ufdkg3YbpO. You can search your likes by username, keyword and tweet date. You might find\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 20, 43 ],
        "url" : "https:\/\/t.co\/ufdkg3YbpO",
        "expanded_url" : "https:\/\/favourites.io",
        "display_url" : "favourites.io"
      } ]
    },
    "in_reply_to_status_id_str" : "915279710095060992",
    "geo" : { },
    "id_str" : "915495059985981440",
    "in_reply_to_user_id" : 1003673466,
    "text" : "@0x4d4147 Check out https:\/\/t.co\/ufdkg3YbpO. You can search your likes by username, keyword and tweet date. You might find it useful :)",
    "id" : 915495059985981440,
    "in_reply_to_status_id" : 915279710095060992,
    "created_at" : "2017-10-04 08:33:26 +0000",
    "in_reply_to_screen_name" : "mariosubspace",
    "in_reply_to_user_id_str" : "1003673466",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 915497515667066881,
  "created_at" : "2017-10-04 08:43:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/20N0jM3hkp",
      "expanded_url" : "https:\/\/twitter.com\/1followernodad\/status\/915423849432227840",
      "display_url" : "twitter.com\/1followernodad\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915497248011833345",
  "text" : "\uD83D\uDE02 https:\/\/t.co\/20N0jM3hkp",
  "id" : 915497248011833345,
  "created_at" : "2017-10-04 08:42:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915460873984110593",
  "text" : "Dear marketing people, Good job on your video ads that autoplay.",
  "id" : 915460873984110593,
  "created_at" : "2017-10-04 06:17:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/4Zr6bRXPKU",
      "expanded_url" : "http:\/\/cnet.co\/2hIhqnh",
      "display_url" : "cnet.co\/2hIhqnh"
    } ]
  },
  "geo" : { },
  "id_str" : "915459922996981760",
  "text" : "Xiaomi shipped over 10 million phones in September https:\/\/t.co\/4Zr6bRXPKU",
  "id" : 915459922996981760,
  "created_at" : "2017-10-04 06:13:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AB\u30EF\u30A4\u30A4\u3082\u3075\u3082\u3075\u52D5\u7269\u3010\u8D85\u53B3\u9078\u3011",
      "screen_name" : "kawaani_movie",
      "indices" : [ 3, 17 ],
      "id_str" : "846993307850936321",
      "id" : 846993307850936321
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/kawaani_movie\/status\/915128128003026945\/video\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/S0NYs0mhqo",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915128090606497793\/pu\/img\/bnH_mqfoEWmuhQ29.jpg",
      "id_str" : "915128090606497793",
      "id" : 915128090606497793,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915128090606497793\/pu\/img\/bnH_mqfoEWmuhQ29.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 360
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 360
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 360
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 360
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/S0NYs0mhqo"
    } ],
    "hashtags" : [ {
      "text" : "\u52D5\u7269",
      "indices" : [ 49, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915316876913373189",
  "text" : "RT @kawaani_movie: \u3053\u308C\u3053\u305D\n\u307F\u3093\u306A\u304C\u6C42\u3081\u3066\u305F\u732B\u306E\u59FF\u3067\u306F\n\u306A\u3044\u3067\u3057\u3087\u3046\u304B...\n#\u52D5\u7269\n. https:\/\/t.co\/S0NYs0mhqo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.yahoo.co.jp\/\" rel=\"nofollow\"\u003Esydayhacejz\u306EKEY\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/kawaani_movie\/status\/915128128003026945\/video\/1",
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/S0NYs0mhqo",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915128090606497793\/pu\/img\/bnH_mqfoEWmuhQ29.jpg",
        "id_str" : "915128090606497793",
        "id" : 915128090606497793,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/915128090606497793\/pu\/img\/bnH_mqfoEWmuhQ29.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 360
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 360
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 360
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 360
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/S0NYs0mhqo"
      } ],
      "hashtags" : [ {
        "text" : "\u52D5\u7269",
        "indices" : [ 30, 33 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "915128128003026945",
    "text" : "\u3053\u308C\u3053\u305D\n\u307F\u3093\u306A\u304C\u6C42\u3081\u3066\u305F\u732B\u306E\u59FF\u3067\u306F\n\u306A\u3044\u3067\u3057\u3087\u3046\u304B...\n#\u52D5\u7269\n. https:\/\/t.co\/S0NYs0mhqo",
    "id" : 915128128003026945,
    "created_at" : "2017-10-03 08:15:22 +0000",
    "user" : {
      "name" : "\u30AB\u30EF\u30A4\u30A4\u3082\u3075\u3082\u3075\u52D5\u7269\u3010\u8D85\u53B3\u9078\u3011",
      "screen_name" : "kawaani_movie",
      "protected" : false,
      "id_str" : "846993307850936321",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/886879858831147008\/J9Rq6UQv_normal.jpg",
      "id" : 846993307850936321,
      "verified" : false
    }
  },
  "id" : 915316876913373189,
  "created_at" : "2017-10-03 20:45:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915259125482876929",
  "text" : "Try this: look at a cohort of customers and plot what they do over time, they will generally purchase less as time goes on.",
  "id" : 915259125482876929,
  "created_at" : "2017-10-03 16:55:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/zW4dscXOuR",
      "expanded_url" : "https:\/\/www.technologyreview.com\/s\/531676\/algorithm-awareness\/",
      "display_url" : "technologyreview.com\/s\/531676\/algor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915239769323577344",
  "text" : "When logic behind an AI system is presented clearly, users can trust it and its decisions. https:\/\/t.co\/zW4dscXOuR",
  "id" : 915239769323577344,
  "created_at" : "2017-10-03 15:38:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "johnstcn",
      "screen_name" : "johnstcn",
      "indices" : [ 7, 16 ],
      "id_str" : "1549050667",
      "id" : 1549050667
    }, {
      "name" : "Concepture GmbH",
      "screen_name" : "ConceptureSC",
      "indices" : [ 17, 30 ],
      "id_str" : "51441084",
      "id" : 51441084
    }, {
      "name" : "Lee J. O'Riordan",
      "screen_name" : "mlxd",
      "indices" : [ 43, 48 ],
      "id_str" : "543599076",
      "id" : 543599076
    }, {
      "name" : "\u262D amy potayto \u262D |, ||,||, |_",
      "screen_name" : "apotayto",
      "indices" : [ 49, 58 ],
      "id_str" : "23091202",
      "id" : 23091202
    }, {
      "name" : "PyData Dublin",
      "screen_name" : "PyDataDublin",
      "indices" : [ 59, 72 ],
      "id_str" : "751790806290292737",
      "id" : 751790806290292737
    }, {
      "name" : "John Fitzpatrick",
      "screen_name" : "JFTAXI",
      "indices" : [ 73, 80 ],
      "id_str" : "21079924",
      "id" : 21079924
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/dFf7CRE3hf",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/914099372044046336",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915238452647612416",
  "text" : "Thanks @johnstcn @ConceptureSC @mariapokes @mlxd @apotayto @PyDataDublin @JFTAXI for re-tweeting this https:\/\/t.co\/dFf7CRE3hf",
  "id" : 915238452647612416,
  "created_at" : "2017-10-03 15:33:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/915229300294594562\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/96OLxIoOVK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLOMFE5XcAAJddi.jpg",
      "id_str" : "915229168127930368",
      "id" : 915229168127930368,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLOMFE5XcAAJddi.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/96OLxIoOVK"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915229300294594562",
  "text" : "Done with setting up those data science tool. Time to answer question such as: https:\/\/t.co\/96OLxIoOVK",
  "id" : 915229300294594562,
  "created_at" : "2017-10-03 14:57:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Brown",
      "screen_name" : "dave_brown24",
      "indices" : [ 3, 16 ],
      "id_str" : "2848493051",
      "id" : 2848493051
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915185128200179714",
  "text" : "RT @dave_brown24: Today's casualty count in Las Vegas roughly matches the U.S. dead and wounded from the second battle of Fallujah, Iraq, i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 128, 151 ],
        "url" : "https:\/\/t.co\/Wo4XteCD7E",
        "expanded_url" : "https:\/\/twitter.com\/20committee\/status\/914835924671975424",
        "display_url" : "twitter.com\/20committee\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "914837320888717313",
    "text" : "Today's casualty count in Las Vegas roughly matches the U.S. dead and wounded from the second battle of Fallujah, Iraq, in 2004 https:\/\/t.co\/Wo4XteCD7E",
    "id" : 914837320888717313,
    "created_at" : "2017-10-02 12:59:48 +0000",
    "user" : {
      "name" : "Dave Brown",
      "screen_name" : "dave_brown24",
      "protected" : false,
      "id_str" : "2848493051",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/536977216467247104\/tI41EO1R_normal.jpeg",
      "id" : 2848493051,
      "verified" : true
    }
  },
  "id" : 915185128200179714,
  "created_at" : "2017-10-03 12:01:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/915180045173391361\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/hmCPi5OX13",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLNej6oW0AESWHl.jpg",
      "id_str" : "915179120413298689",
      "id" : 915179120413298689,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLNej6oW0AESWHl.jpg",
      "sizes" : [ {
        "h" : 404,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1175
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1175
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1175
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hmCPi5OX13"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/gWyrqfSq18",
      "expanded_url" : "https:\/\/gist.github.com\/mryap\/a39efd4396c01fd7bf63be548a479d1a",
      "display_url" : "gist.github.com\/mryap\/a39efd43\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "915180045173391361",
  "text" : "I just installed Jupyter Lab via Jupyter Notebook. https:\/\/t.co\/gWyrqfSq18 https:\/\/t.co\/hmCPi5OX13",
  "id" : 915180045173391361,
  "created_at" : "2017-10-03 11:41:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "indices" : [ 3, 16 ],
      "id_str" : "205451951",
      "id" : 205451951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/8FURUvNzCC",
      "expanded_url" : "http:\/\/on.inc.com\/2hBryhA",
      "display_url" : "on.inc.com\/2hBryhA"
    } ]
  },
  "geo" : { },
  "id_str" : "915167930459021312",
  "text" : "RT @TheChrisBray: Want to do wonders for your creativity? Get out and see the world. https:\/\/t.co\/8FURUvNzCC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.everyonesocial.com\" rel=\"nofollow\"\u003EEveryoneSocial\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/8FURUvNzCC",
        "expanded_url" : "http:\/\/on.inc.com\/2hBryhA",
        "display_url" : "on.inc.com\/2hBryhA"
      } ]
    },
    "geo" : { },
    "id_str" : "915165970901209088",
    "text" : "Want to do wonders for your creativity? Get out and see the world. https:\/\/t.co\/8FURUvNzCC",
    "id" : 915165970901209088,
    "created_at" : "2017-10-03 10:45:45 +0000",
    "user" : {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "protected" : false,
      "id_str" : "205451951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/910574324540674048\/PkMoaUAB_normal.jpg",
      "id" : 205451951,
      "verified" : false
    }
  },
  "id" : 915167930459021312,
  "created_at" : "2017-10-03 10:53:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915125576825999360",
  "text" : "RT @mrbrown: \u201CIn China, what I have done by offering the money to Mr Loy is part of our culture\" You can take this culture home. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/WT3fQSXtLK",
        "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/singapore\/mother-of-ex-national-table-tennis-player-li-hu-on-trial-for-9271136",
        "display_url" : "channelnewsasia.com\/news\/singapore\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "915113266862366721",
    "text" : "\u201CIn China, what I have done by offering the money to Mr Loy is part of our culture\" You can take this culture home. https:\/\/t.co\/WT3fQSXtLK",
    "id" : 915113266862366721,
    "created_at" : "2017-10-03 07:16:19 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 915125576825999360,
  "created_at" : "2017-10-03 08:05:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915113067305988096",
  "text" : "#Ireland edu institution, are you exploring the use of  Jupyter notebooks for learning and teaching ? Available for hire.",
  "id" : 915113067305988096,
  "created_at" : "2017-10-03 07:15:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "YALiberty",
      "screen_name" : "YALiberty",
      "indices" : [ 3, 13 ],
      "id_str" : "17642330",
      "id" : 17642330
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/YALiberty\/status\/914520540429324288\/photo\/1",
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/bQBbUmIEdv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLEHlZyXoAAU-x-.jpg",
      "id_str" : "914520538491625472",
      "id" : 914520538491625472,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLEHlZyXoAAU-x-.jpg",
      "sizes" : [ {
        "h" : 604,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 604,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 604,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 401,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bQBbUmIEdv"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "915102764518318080",
  "text" : "RT @YALiberty: https:\/\/t.co\/bQBbUmIEdv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/YALiberty\/status\/914520540429324288\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/bQBbUmIEdv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLEHlZyXoAAU-x-.jpg",
        "id_str" : "914520538491625472",
        "id" : 914520538491625472,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLEHlZyXoAAU-x-.jpg",
        "sizes" : [ {
          "h" : 604,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 604,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 604,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 401,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bQBbUmIEdv"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "914520540429324288",
    "text" : "https:\/\/t.co\/bQBbUmIEdv",
    "id" : 914520540429324288,
    "created_at" : "2017-10-01 16:01:02 +0000",
    "user" : {
      "name" : "YALiberty",
      "screen_name" : "YALiberty",
      "protected" : false,
      "id_str" : "17642330",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/462235491845148672\/S6BsRotm_normal.jpeg",
      "id" : 17642330,
      "verified" : true
    }
  },
  "id" : 915102764518318080,
  "created_at" : "2017-10-03 06:34:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "T. J. Clark",
      "screen_name" : "theTeedge",
      "indices" : [ 3, 13 ],
      "id_str" : "620796820",
      "id" : 620796820
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914988105404383232",
  "text" : "RT @theTeedge: I have seen this headline three times in my life; Virginia Tech, Orlando, and now Las Vegas.\n\nI am only 22 years-old. https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/UU3GJJw8MD",
        "expanded_url" : "https:\/\/twitter.com\/ap\/status\/914805873024933888",
        "display_url" : "twitter.com\/ap\/status\/9148\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "914808746576109569",
    "text" : "I have seen this headline three times in my life; Virginia Tech, Orlando, and now Las Vegas.\n\nI am only 22 years-old. https:\/\/t.co\/UU3GJJw8MD",
    "id" : 914808746576109569,
    "created_at" : "2017-10-02 11:06:16 +0000",
    "user" : {
      "name" : "T. J. Clark",
      "screen_name" : "theTeedge",
      "protected" : false,
      "id_str" : "620796820",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028309950391611392\/9pylYlPY_normal.jpg",
      "id" : 620796820,
      "verified" : false
    }
  },
  "id" : 914988105404383232,
  "created_at" : "2017-10-02 22:58:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/6mg8QPiRcl",
      "expanded_url" : "https:\/\/twitter.com\/hadleywickham\/status\/914140589565841410",
      "display_url" : "twitter.com\/hadleywickham\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "914899380175540226",
  "text" : "Data Science people must be comfortable typing command line on those Terminal https:\/\/t.co\/6mg8QPiRcl",
  "id" : 914899380175540226,
  "created_at" : "2017-10-02 17:06:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/914897251130052609\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/HFaMw5U8uJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLJeDvaXUAAbw3E.jpg",
      "id_str" : "914897092669296640",
      "id" : 914897092669296640,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLJeDvaXUAAbw3E.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 651,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 1364
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 1364
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HFaMw5U8uJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914897251130052609",
  "text" : "Best thing about Notebooks Azure is that you can execute shell script in a Terminal via a browser. https:\/\/t.co\/HFaMw5U8uJ",
  "id" : 914897251130052609,
  "created_at" : "2017-10-02 16:57:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914882443613802496",
  "text" : "\u6B7B\u4EA1\u4EBA\u6570\u4E0A\u5347\u2026\u2026",
  "id" : 914882443613802496,
  "created_at" : "2017-10-02 15:59:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 0, 11 ],
      "id_str" : "18189672",
      "id" : 18189672
    }, {
      "name" : "Dermot O'Leary",
      "screen_name" : "GBSEconomist",
      "indices" : [ 12, 25 ],
      "id_str" : "1220894071",
      "id" : 1220894071
    }, {
      "name" : "\u00D3rla Hegarty",
      "screen_name" : "Orla_Hegarty",
      "indices" : [ 26, 39 ],
      "id_str" : "1543321040",
      "id" : 1543321040
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "914773040730398721",
  "geo" : { },
  "id_str" : "914821003599106048",
  "in_reply_to_user_id" : 18189672,
  "text" : "@ronanlyons @GBSEconomist @Orla_Hegarty Which is a more reliable proxy? BER certs or electricity connections?",
  "id" : 914821003599106048,
  "in_reply_to_status_id" : 914773040730398721,
  "created_at" : "2017-10-02 11:54:58 +0000",
  "in_reply_to_screen_name" : "ronanlyons",
  "in_reply_to_user_id_str" : "18189672",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 125, 139 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/e0yoixxVc6",
      "expanded_url" : "https:\/\/www.irishtimes.com\/business\/economy\/housebuilding-rate-is-well-below-official-figure-1.3240432#.WdInut-mQ3Q.twitter",
      "display_url" : "irishtimes.com\/business\/econo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "914820026393341954",
  "text" : "It be helpful if both parties release their data and calculation on Github for others to review. https:\/\/t.co\/e0yoixxVc6 via @IrishTimesBiz",
  "id" : 914820026393341954,
  "created_at" : "2017-10-02 11:51:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/m6j3ZN5gwA",
      "expanded_url" : "https:\/\/twitter.com\/interactivemark\/status\/914813192089620481",
      "display_url" : "twitter.com\/interactivemar\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "914813699969470464",
  "text" : "I am not the only one who thought the wording \"warmest condolences\" is not right. https:\/\/t.co\/m6j3ZN5gwA",
  "id" : 914813699969470464,
  "created_at" : "2017-10-02 11:25:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914798685220896768",
  "text" : "Where are the so call good guys with gun?",
  "id" : 914798685220896768,
  "created_at" : "2017-10-02 10:26:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dubstrike",
      "screen_name" : "dubstrike",
      "indices" : [ 3, 13 ],
      "id_str" : "915303103821258752",
      "id" : 915303103821258752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/PkDbHYlM1L",
      "expanded_url" : "http:\/\/www.opensource.apple.com",
      "display_url" : "opensource.apple.com"
    } ]
  },
  "geo" : { },
  "id_str" : "914793341929304065",
  "text" : "RT @dubstrike: Apple just open sourced every iOS and macOS kernel to date. What the hell.\n\nhttps:\/\/t.co\/PkDbHYlM1L",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/PkDbHYlM1L",
        "expanded_url" : "http:\/\/www.opensource.apple.com",
        "display_url" : "opensource.apple.com"
      } ]
    },
    "geo" : { },
    "id_str" : "913965740620963841",
    "text" : "Apple just open sourced every iOS and macOS kernel to date. What the hell.\n\nhttps:\/\/t.co\/PkDbHYlM1L",
    "id" : 913965740620963841,
    "created_at" : "2017-09-30 03:16:27 +0000",
    "user" : {
      "name" : "Wojtek",
      "screen_name" : "pugson",
      "protected" : false,
      "id_str" : "313527601",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/834049142515187713\/cOtVTgLm_normal.jpg",
      "id" : 313527601,
      "verified" : false
    }
  },
  "id" : 914793341929304065,
  "created_at" : "2017-10-02 10:05:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Azure Notebooks",
      "screen_name" : "AzureNotebooks",
      "indices" : [ 3, 18 ],
      "id_str" : "739884961793249280",
      "id" : 739884961793249280
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/wv8EGE23s1",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/914115777640452100",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "914472734607986688",
  "text" : "hi @AzureNotebooks Are you able to advice? Thanks https:\/\/t.co\/wv8EGE23s1",
  "id" : 914472734607986688,
  "created_at" : "2017-10-01 12:51:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/914446977571663872\/photo\/1",
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/wpCfdDnAxl",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLDErbOW4AAnBR9.jpg",
      "id_str" : "914446974677606400",
      "id" : 914446974677606400,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLDErbOW4AAnBR9.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wpCfdDnAxl"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/N29UKclqf4",
      "expanded_url" : "http:\/\/ift.tt\/2fBoFcF",
      "display_url" : "ift.tt\/2fBoFcF"
    } ]
  },
  "geo" : { },
  "id_str" : "914446977571663872",
  "text" : "On Channel 4, it about Mid-Autumn Festival (\u4E2D\u79CB\u8282\uFF09 food https:\/\/t.co\/N29UKclqf4 https:\/\/t.co\/wpCfdDnAxl",
  "id" : 914446977571663872,
  "created_at" : "2017-10-01 11:08:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "indices" : [ 3, 16 ],
      "id_str" : "854642300",
      "id" : 854642300
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/3thoughtbbls\/status\/914438655678255104\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/vfWVrLCquu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DLC9GVnWkAI0RwD.jpg",
      "id_str" : "914438640935276546",
      "id" : 914438640935276546,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLC9GVnWkAI0RwD.jpg",
      "sizes" : [ {
        "h" : 826,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 484,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 826,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vfWVrLCquu"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "914444783040565249",
  "text" : "RT @3thoughtbbls: Seriously - who writes this copy? What does it actually mean to real people? https:\/\/t.co\/vfWVrLCquu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/3thoughtbbls\/status\/914438655678255104\/photo\/1",
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/vfWVrLCquu",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DLC9GVnWkAI0RwD.jpg",
        "id_str" : "914438640935276546",
        "id" : 914438640935276546,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DLC9GVnWkAI0RwD.jpg",
        "sizes" : [ {
          "h" : 826,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 484,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 826,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vfWVrLCquu"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "914438655678255104",
    "text" : "Seriously - who writes this copy? What does it actually mean to real people? https:\/\/t.co\/vfWVrLCquu",
    "id" : 914438655678255104,
    "created_at" : "2017-10-01 10:35:39 +0000",
    "user" : {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "protected" : false,
      "id_str" : "854642300",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459287079227101184\/Yb2pWHi8_normal.jpeg",
      "id" : 854642300,
      "verified" : false
    }
  },
  "id" : 914444783040565249,
  "created_at" : "2017-10-01 11:00:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]