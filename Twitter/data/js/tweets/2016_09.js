Grailbird.data.tweets_2016_09 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "indices" : [ 3, 11 ],
      "id_str" : "41025492",
      "id" : 41025492
    }, {
      "name" : "McKinsey & Company",
      "screen_name" : "McKinsey",
      "indices" : [ 31, 40 ],
      "id_str" : "34042766",
      "id" : 34042766
    }, {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 71, 81 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781973908614475776",
  "text" : "RT @marklcl: Good article from @McKinsey validating what's building at @GovTechSG Hive The new tech talent you need to succeed https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "McKinsey & Company",
        "screen_name" : "McKinsey",
        "indices" : [ 18, 27 ],
        "id_str" : "34042766",
        "id" : 34042766
      }, {
        "name" : "GovTech (Singapore)",
        "screen_name" : "GovTechSG",
        "indices" : [ 58, 68 ],
        "id_str" : "123167035",
        "id" : 123167035
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/tdhSMPVhh3",
        "expanded_url" : "http:\/\/www.mckinsey.com\/business-functions\/digital-mckinsey\/our-insights\/the-new-tech-talent-you-need-to-succeed-in-digital",
        "display_url" : "mckinsey.com\/business-funct\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "781959385551482881",
    "text" : "Good article from @McKinsey validating what's building at @GovTechSG Hive The new tech talent you need to succeed https:\/\/t.co\/tdhSMPVhh3",
    "id" : 781959385551482881,
    "created_at" : "2016-09-30 20:50:20 +0000",
    "user" : {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "protected" : false,
      "id_str" : "41025492",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684550252083298306\/DmeNwyFV_normal.jpg",
      "id" : 41025492,
      "verified" : false
    }
  },
  "id" : 781973908614475776,
  "created_at" : "2016-09-30 21:48:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conor Duke",
      "screen_name" : "conr",
      "indices" : [ 3, 8 ],
      "id_str" : "6295332",
      "id" : 6295332
    }, {
      "name" : "Matt Slack",
      "screen_name" : "slack",
      "indices" : [ 37, 43 ],
      "id_str" : "8922",
      "id" : 8922
    }, {
      "name" : "saya kamide",
      "screen_name" : "statsbot",
      "indices" : [ 44, 53 ],
      "id_str" : "112552042",
      "id" : 112552042
    }, {
      "name" : "Wardani Rukeni",
      "screen_name" : "looker",
      "indices" : [ 54, 61 ],
      "id_str" : "7159912",
      "id" : 7159912
    }, {
      "name" : "Predict Conference",
      "screen_name" : "predictconf",
      "indices" : [ 124, 136 ],
      "id_str" : "2991904109",
      "id" : 2991904109
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ConversationalUI",
      "indices" : [ 96, 113 ]
    }, {
      "text" : "AI",
      "indices" : [ 120, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781972137766744066",
  "text" : "RT @conr: Discussing how to leverage @Slack @statsbot @Looker  to improve decision times using  #ConversationalUI &amp; #AI @predictConf next W\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Matt Slack",
        "screen_name" : "slack",
        "indices" : [ 27, 33 ],
        "id_str" : "8922",
        "id" : 8922
      }, {
        "name" : "saya kamide",
        "screen_name" : "statsbot",
        "indices" : [ 34, 43 ],
        "id_str" : "112552042",
        "id" : 112552042
      }, {
        "name" : "Wardani Rukeni",
        "screen_name" : "looker",
        "indices" : [ 44, 51 ],
        "id_str" : "7159912",
        "id" : 7159912
      }, {
        "name" : "Predict Conference",
        "screen_name" : "predictconf",
        "indices" : [ 114, 126 ],
        "id_str" : "2991904109",
        "id" : 2991904109
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ConversationalUI",
        "indices" : [ 86, 103 ]
      }, {
        "text" : "AI",
        "indices" : [ 110, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "781869976470847488",
    "text" : "Discussing how to leverage @Slack @statsbot @Looker  to improve decision times using  #ConversationalUI &amp; #AI @predictConf next Wednesday.",
    "id" : 781869976470847488,
    "created_at" : "2016-09-30 14:55:03 +0000",
    "user" : {
      "name" : "Conor Duke",
      "screen_name" : "conr",
      "protected" : false,
      "id_str" : "6295332",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/918362659250196480\/xhTqz3Bd_normal.jpg",
      "id" : 6295332,
      "verified" : false
    }
  },
  "id" : 781972137766744066,
  "created_at" : "2016-09-30 21:41:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/781912941905969152\/photo\/1",
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/rd9rs63fsJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtnppmxXYAI-qr1.jpg",
      "id_str" : "781912911317000194",
      "id" : 781912911317000194,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtnppmxXYAI-qr1.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rd9rs63fsJ"
    } ],
    "hashtags" : [ {
      "text" : "TCDProbe",
      "indices" : [ 21, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781912941905969152",
  "text" : "Thesis in 3 minutes. #TCDProbe https:\/\/t.co\/rd9rs63fsJ",
  "id" : 781912941905969152,
  "created_at" : "2016-09-30 17:45:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/781910146599583744\/photo\/1",
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/PqHMom4XKN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Ctnm-dQWIAAnBpA.jpg",
      "id_str" : "781909971004956672",
      "id" : 781909971004956672,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Ctnm-dQWIAAnBpA.jpg",
      "sizes" : [ {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PqHMom4XKN"
    } ],
    "hashtags" : [ {
      "text" : "tcdprobe",
      "indices" : [ 18, 27 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781910146599583744",
  "text" : "Building robot at #tcdprobe. Do checkout RoboSlam website https:\/\/t.co\/PqHMom4XKN",
  "id" : 781910146599583744,
  "created_at" : "2016-09-30 17:34:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Predict Conference",
      "screen_name" : "predictconf",
      "indices" : [ 111, 123 ],
      "id_str" : "2991904109",
      "id" : 2991904109
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781852065962360832",
  "text" : "Got my 1st job as a webmaster after attending an event. Hope to get a career in data analytics after attending @predictconf",
  "id" : 781852065962360832,
  "created_at" : "2016-09-30 13:43:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/Sm13zM0bKc",
      "expanded_url" : "https:\/\/twitter.com\/klillington\/status\/781850346062839808",
      "display_url" : "twitter.com\/klillington\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781850967570546688",
  "text" : "Tagged a British journalist instead of an Irish TD https:\/\/t.co\/Sm13zM0bKc",
  "id" : 781850967570546688,
  "created_at" : "2016-09-30 13:39:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "repeal",
      "indices" : [ 17, 24 ]
    } ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/ohfqiyEZgo",
      "expanded_url" : "https:\/\/twitter.com\/Harryslaststand\/status\/781829734506168321",
      "display_url" : "twitter.com\/Harryslaststan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781835163273728000",
  "text" : "RT @klillington: #repeal... https:\/\/t.co\/ohfqiyEZgo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "repeal",
        "indices" : [ 0, 7 ]
      } ],
      "urls" : [ {
        "indices" : [ 11, 34 ],
        "url" : "https:\/\/t.co\/ohfqiyEZgo",
        "expanded_url" : "https:\/\/twitter.com\/Harryslaststand\/status\/781829734506168321",
        "display_url" : "twitter.com\/Harryslaststan\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "781830656540155904",
    "text" : "#repeal... https:\/\/t.co\/ohfqiyEZgo",
    "id" : 781830656540155904,
    "created_at" : "2016-09-30 12:18:48 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 781835163273728000,
  "created_at" : "2016-09-30 12:36:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tibbr",
      "indices" : [ 84, 90 ]
    } ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/4nciE6f0C7",
      "expanded_url" : "http:\/\/www.tibbr.com\/",
      "display_url" : "tibbr.com"
    } ]
  },
  "geo" : { },
  "id_str" : "781834666173198336",
  "text" : "Using several different document sharing tools? Organise them all in one place with #tibbr: https:\/\/t.co\/4nciE6f0C7",
  "id" : 781834666173198336,
  "created_at" : "2016-09-30 12:34:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "harishpillay",
      "screen_name" : "harishpillay",
      "indices" : [ 3, 16 ],
      "id_str" : "12486322",
      "id" : 12486322
    }, {
      "name" : "GovTech SG",
      "screen_name" : "IDAsg",
      "indices" : [ 99, 105 ],
      "id_str" : "707512234658824192",
      "id" : 707512234658824192
    }, {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "indices" : [ 106, 114 ],
      "id_str" : "41025492",
      "id" : 41025492
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781831857327448064",
  "text" : "RT @harishpillay: Congratulations GovTech.sg on winning the Red Hat Innovation Award for cloud. Cc @IDAsg @marklcl govt open source ftw. ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GovTech SG",
        "screen_name" : "IDAsg",
        "indices" : [ 81, 87 ],
        "id_str" : "707512234658824192",
        "id" : 707512234658824192
      }, {
        "name" : "Mark Lim Choon Lye",
        "screen_name" : "marklcl",
        "indices" : [ 88, 96 ],
        "id_str" : "41025492",
        "id" : 41025492
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/harishpillay\/status\/781321421096038400\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/sSL8UKvAQ2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtfPqDuUMAAYibw.jpg",
        "id_str" : "781321381833093120",
        "id" : 781321381833093120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtfPqDuUMAAYibw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sSL8UKvAQ2"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/harishpillay\/status\/781321421096038400\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/sSL8UKvAQ2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtfPrSRVIAEht2S.jpg",
        "id_str" : "781321402917920769",
        "id" : 781321402917920769,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtfPrSRVIAEht2S.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sSL8UKvAQ2"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "781321421096038400",
    "text" : "Congratulations GovTech.sg on winning the Red Hat Innovation Award for cloud. Cc @IDAsg @marklcl govt open source ftw. https:\/\/t.co\/sSL8UKvAQ2",
    "id" : 781321421096038400,
    "created_at" : "2016-09-29 02:35:17 +0000",
    "user" : {
      "name" : "harishpillay",
      "screen_name" : "harishpillay",
      "protected" : false,
      "id_str" : "12486322",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/99978402\/HarishPillaycloseupshot_normal.jpg",
      "id" : 12486322,
      "verified" : false
    }
  },
  "id" : 781831857327448064,
  "created_at" : "2016-09-30 12:23:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ange Murphy",
      "screen_name" : "Ange_Murphy",
      "indices" : [ 3, 15 ],
      "id_str" : "1144965726",
      "id" : 1144965726
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781584147743727617",
  "text" : "RT @Ange_Murphy: Born 1966. Conceived of rape. Forced adoption. Mother spent 50yrs in  Magdalene Laundries until death. I am that child. #k\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "knowyourrepealers",
        "indices" : [ 120, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "781202604244295680",
    "text" : "Born 1966. Conceived of rape. Forced adoption. Mother spent 50yrs in  Magdalene Laundries until death. I am that child. #knowyourrepealers",
    "id" : 781202604244295680,
    "created_at" : "2016-09-28 18:43:09 +0000",
    "user" : {
      "name" : "Ange Murphy",
      "screen_name" : "Ange_Murphy",
      "protected" : false,
      "id_str" : "1144965726",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3201627169\/bdfbb31319abbd5181c0081925a5943f_normal.png",
      "id" : 1144965726,
      "verified" : false
    }
  },
  "id" : 781584147743727617,
  "created_at" : "2016-09-29 19:59:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/JtwuPbYFhD",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Ingelfinger_rule",
      "display_url" : "en.wikipedia.org\/wiki\/Ingelfing\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781581963635326977",
  "text" : "Don't don't Ingelfinger yourself https:\/\/t.co\/JtwuPbYFhD",
  "id" : 781581963635326977,
  "created_at" : "2016-09-29 19:50:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781575038122266625",
  "text" : "I know what to give to my neighbour this Xmas. Boots Super Absorbent foot powder.",
  "id" : 781575038122266625,
  "created_at" : "2016-09-29 19:23:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "indices" : [ 15, 28 ],
      "id_str" : "205451951",
      "id" : 205451951
    }, {
      "name" : "TravelAdam",
      "screen_name" : "traveladam",
      "indices" : [ 29, 40 ],
      "id_str" : "108250898",
      "id" : 108250898
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781574376084893696",
  "text" : "@bharatidalela @TheChrisBray @traveladam If you can't bring it &amp; show on your next interview, I would not consider a published author.",
  "id" : 781574376084893696,
  "created_at" : "2016-09-29 19:20:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/Dj3AEWEw8M",
      "expanded_url" : "http:\/\/marketingland.com\/google-analytics-free-optimize-session-quality-score-more-193096",
      "display_url" : "marketingland.com\/google-analyti\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781572563533230082",
  "text" : "Google Analytics introduce Machine Learning to its new metric - session quality score https:\/\/t.co\/Dj3AEWEw8M",
  "id" : 781572563533230082,
  "created_at" : "2016-09-29 19:13:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cathy Power",
      "screen_name" : "cathypower",
      "indices" : [ 3, 14 ],
      "id_str" : "22839715",
      "id" : 22839715
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cathypower\/status\/779686891214045184\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/gJzUifIrWU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtIBFLTWYAEAlfE.jpg",
      "id_str" : "779686873933438977",
      "id" : 779686873933438977,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtIBFLTWYAEAlfE.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 614,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 435,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 614,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 614,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gJzUifIrWU"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/cathypower\/status\/779686891214045184\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/gJzUifIrWU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtIBFpdWYAEmVtC.jpg",
      "id_str" : "779686882028445697",
      "id" : 779686882028445697,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtIBFpdWYAEmVtC.jpg",
      "sizes" : [ {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gJzUifIrWU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781571041286033409",
  "text" : "RT @cathypower: We fought the 8th amendment then and we're still fighting for reproductive rights. https:\/\/t.co\/gJzUifIrWU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cathypower\/status\/779686891214045184\/photo\/1",
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/gJzUifIrWU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtIBFLTWYAEAlfE.jpg",
        "id_str" : "779686873933438977",
        "id" : 779686873933438977,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtIBFLTWYAEAlfE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 614,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 435,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 614,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 614,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gJzUifIrWU"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/cathypower\/status\/779686891214045184\/photo\/1",
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/gJzUifIrWU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtIBFpdWYAEmVtC.jpg",
        "id_str" : "779686882028445697",
        "id" : 779686882028445697,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtIBFpdWYAEmVtC.jpg",
        "sizes" : [ {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gJzUifIrWU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "779686891214045184",
    "text" : "We fought the 8th amendment then and we're still fighting for reproductive rights. https:\/\/t.co\/gJzUifIrWU",
    "id" : 779686891214045184,
    "created_at" : "2016-09-24 14:20:15 +0000",
    "user" : {
      "name" : "Cathy Power",
      "screen_name" : "cathypower",
      "protected" : false,
      "id_str" : "22839715",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011547899308728325\/77tdtwEA_normal.jpg",
      "id" : 22839715,
      "verified" : false
    }
  },
  "id" : 781571041286033409,
  "created_at" : "2016-09-29 19:07:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mei Lee",
      "screen_name" : "HiMeiLee",
      "indices" : [ 3, 12 ],
      "id_str" : "15008100",
      "id" : 15008100
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "digitaltransformation",
      "indices" : [ 77, 99 ]
    }, {
      "text" : "innovation",
      "indices" : [ 100, 111 ]
    }, {
      "text" : "changeisgood",
      "indices" : [ 112, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781570556374159364",
  "text" : "RT @HiMeiLee: If your goal comes with resistance, you're on the right track. #digitaltransformation #innovation #changeisgood",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "digitaltransformation",
        "indices" : [ 63, 85 ]
      }, {
        "text" : "innovation",
        "indices" : [ 86, 97 ]
      }, {
        "text" : "changeisgood",
        "indices" : [ 98, 111 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "781081497977532416",
    "text" : "If your goal comes with resistance, you're on the right track. #digitaltransformation #innovation #changeisgood",
    "id" : 781081497977532416,
    "created_at" : "2016-09-28 10:41:55 +0000",
    "user" : {
      "name" : "Mei Lee",
      "screen_name" : "HiMeiLee",
      "protected" : false,
      "id_str" : "15008100",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3305300946\/eeaf1464f2f241afe7bf6118d7a840e0_normal.jpeg",
      "id" : 15008100,
      "verified" : false
    }
  },
  "id" : 781570556374159364,
  "created_at" : "2016-09-29 19:05:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781562815081488384",
  "text" : "Apparently writing a book on 75 + KPI means you are a \"big data business influencer\"",
  "id" : 781562815081488384,
  "created_at" : "2016-09-29 18:34:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PETA UK",
      "screen_name" : "PETAUK",
      "indices" : [ 3, 10 ],
      "id_str" : "357584312",
      "id" : 357584312
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoVegan",
      "indices" : [ 130, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/dRQJdHm1oe",
      "expanded_url" : "http:\/\/www.aol.co.uk\/news\/2016\/09\/29\/mary-robinson-suggests-going-vegan-to-reduce-carbon-footprint\/",
      "display_url" : "aol.co.uk\/news\/2016\/09\/2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781561057689145349",
  "text" : "RT @PETAUK: Former President of Ireland Mary Robinson suggests going vegan to reduce carbon footprint! https:\/\/t.co\/dRQJdHm1oe \uD83D\uDC9A\uD83C\uDF31 #GoVegan",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GoVegan",
        "indices" : [ 118, 126 ]
      } ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/dRQJdHm1oe",
        "expanded_url" : "http:\/\/www.aol.co.uk\/news\/2016\/09\/29\/mary-robinson-suggests-going-vegan-to-reduce-carbon-footprint\/",
        "display_url" : "aol.co.uk\/news\/2016\/09\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "781457308190662657",
    "text" : "Former President of Ireland Mary Robinson suggests going vegan to reduce carbon footprint! https:\/\/t.co\/dRQJdHm1oe \uD83D\uDC9A\uD83C\uDF31 #GoVegan",
    "id" : 781457308190662657,
    "created_at" : "2016-09-29 11:35:15 +0000",
    "user" : {
      "name" : "PETA UK",
      "screen_name" : "PETAUK",
      "protected" : false,
      "id_str" : "357584312",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875772234920587265\/aT3n2_qi_normal.jpg",
      "id" : 357584312,
      "verified" : true
    }
  },
  "id" : 781561057689145349,
  "created_at" : "2016-09-29 18:27:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781512830340177920",
  "text" : "Dear School administrator, not every parents drive. Dublin bus schedule is every 20 mins kid need to come back shower &amp; have lunch first.",
  "id" : 781512830340177920,
  "created_at" : "2016-09-29 15:15:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Loreto Bello Gude",
      "screen_name" : "loretobgude",
      "indices" : [ 3, 15 ],
      "id_str" : "77040973",
      "id" : 77040973
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/loretobgude\/status\/781172296715231232\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/bFYZme0PUJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtdIDkYWgAA-pLj.jpg",
      "id_str" : "781172286514692096",
      "id" : 781172286514692096,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtdIDkYWgAA-pLj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 338,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 442,
        "resize" : "fit",
        "w" : 889
      }, {
        "h" : 442,
        "resize" : "fit",
        "w" : 889
      }, {
        "h" : 442,
        "resize" : "fit",
        "w" : 889
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bFYZme0PUJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/sFef5Hqd4K",
      "expanded_url" : "http:\/\/flip.it\/1Gt5gF",
      "display_url" : "flip.it\/1Gt5gF"
    } ]
  },
  "geo" : { },
  "id_str" : "781218747537354752",
  "text" : "RT @loretobgude: World\u2019s first typhoon turbine could power all of Japan for 50 years\n\nhttps:\/\/t.co\/sFef5Hqd4K https:\/\/t.co\/bFYZme0PUJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/loretobgude\/status\/781172296715231232\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/bFYZme0PUJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtdIDkYWgAA-pLj.jpg",
        "id_str" : "781172286514692096",
        "id" : 781172286514692096,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtdIDkYWgAA-pLj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 442,
          "resize" : "fit",
          "w" : 889
        }, {
          "h" : 442,
          "resize" : "fit",
          "w" : 889
        }, {
          "h" : 442,
          "resize" : "fit",
          "w" : 889
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bFYZme0PUJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/sFef5Hqd4K",
        "expanded_url" : "http:\/\/flip.it\/1Gt5gF",
        "display_url" : "flip.it\/1Gt5gF"
      } ]
    },
    "geo" : { },
    "id_str" : "781172296715231232",
    "text" : "World\u2019s first typhoon turbine could power all of Japan for 50 years\n\nhttps:\/\/t.co\/sFef5Hqd4K https:\/\/t.co\/bFYZme0PUJ",
    "id" : 781172296715231232,
    "created_at" : "2016-09-28 16:42:43 +0000",
    "user" : {
      "name" : "Loreto Bello Gude",
      "screen_name" : "loretobgude",
      "protected" : false,
      "id_str" : "77040973",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000330286181\/1ede53f9e760259a4a66734219397e9a_normal.jpeg",
      "id" : 77040973,
      "verified" : false
    }
  },
  "id" : 781218747537354752,
  "created_at" : "2016-09-28 19:47:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/781170169766940672\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/thhYeo8L1k",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtdGIOjXgAAqmsI.jpg",
      "id_str" : "781170167531405312",
      "id" : 781170167531405312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtdGIOjXgAAqmsI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 335,
        "resize" : "fit",
        "w" : 595
      }, {
        "h" : 335,
        "resize" : "fit",
        "w" : 595
      }, {
        "h" : 335,
        "resize" : "fit",
        "w" : 595
      }, {
        "h" : 335,
        "resize" : "fit",
        "w" : 595
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/thhYeo8L1k"
    } ],
    "hashtags" : [ {
      "text" : "econarchive",
      "indices" : [ 66, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/45UORpEymk",
      "expanded_url" : "http:\/\/econ.st\/2d4Cjnp",
      "display_url" : "econ.st\/2d4Cjnp"
    } ]
  },
  "geo" : { },
  "id_str" : "781217733308448768",
  "text" : "RT @TheEconomist: How Lego became the world's hottest toy company #econarchive https:\/\/t.co\/45UORpEymk https:\/\/t.co\/thhYeo8L1k",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/781170169766940672\/photo\/1",
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/thhYeo8L1k",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtdGIOjXgAAqmsI.jpg",
        "id_str" : "781170167531405312",
        "id" : 781170167531405312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtdGIOjXgAAqmsI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        }, {
          "h" : 335,
          "resize" : "fit",
          "w" : 595
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/thhYeo8L1k"
      } ],
      "hashtags" : [ {
        "text" : "econarchive",
        "indices" : [ 48, 60 ]
      } ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/45UORpEymk",
        "expanded_url" : "http:\/\/econ.st\/2d4Cjnp",
        "display_url" : "econ.st\/2d4Cjnp"
      } ]
    },
    "geo" : { },
    "id_str" : "781170169766940672",
    "text" : "How Lego became the world's hottest toy company #econarchive https:\/\/t.co\/45UORpEymk https:\/\/t.co\/thhYeo8L1k",
    "id" : 781170169766940672,
    "created_at" : "2016-09-28 16:34:16 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 781217733308448768,
  "created_at" : "2016-09-28 19:43:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/bKZbeYoYSr",
      "expanded_url" : "http:\/\/settingrecordstraight.blogspot.com\/2015\/03\/irish-forgotten-white-slaves.html?spref=tw",
      "display_url" : "settingrecordstraight.blogspot.com\/2015\/03\/irish-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781214076017967104",
  "text" : "IRISH:  THE FORGOTTEN WHITE SLAVES https:\/\/t.co\/bKZbeYoYSr",
  "id" : 781214076017967104,
  "created_at" : "2016-09-28 19:28:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/781164466612367360\/photo\/1",
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/IKTF9B03md",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtdA7tmWAAAjh6b.jpg",
      "id_str" : "781164454968950784",
      "id" : 781164454968950784,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtdA7tmWAAAjh6b.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IKTF9B03md"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781164466612367360",
  "text" : "Looking forward to this https:\/\/t.co\/IKTF9B03md",
  "id" : 781164466612367360,
  "created_at" : "2016-09-28 16:11:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/7zCOsqQcJB",
      "expanded_url" : "http:\/\/choosealicense.com\/",
      "display_url" : "choosealicense.com"
    } ]
  },
  "geo" : { },
  "id_str" : "781094951010594816",
  "text" : "Which open source license? https:\/\/t.co\/7zCOsqQcJB",
  "id" : 781094951010594816,
  "created_at" : "2016-09-28 11:35:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/ICq8Uri9OH",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/alexkonrad\/2016\/09\/26\/microsofts-partners-with-adobe-and-renault-nissan-to-boost-azure\/#1eeba7004ae5",
      "display_url" : "forbes.com\/sites\/alexkonr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781088919551107073",
  "text" : "Microsoft's New Partnerships With Adobe  https:\/\/t.co\/ICq8Uri9OH",
  "id" : 781088919551107073,
  "created_at" : "2016-09-28 11:11:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/o4YSRh2r0A",
      "expanded_url" : "https:\/\/moz.com\/blog\/googles-future-is-in-the-cards",
      "display_url" : "moz.com\/blog\/googles-f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "781082598550413312",
  "text" : "Google's Future is in the Cards https:\/\/t.co\/o4YSRh2r0A",
  "id" : 781082598550413312,
  "created_at" : "2016-09-28 10:46:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781073611616620544",
  "text" : "Someone just use this analogy \"cloud hosting is like taking an Uber where you need it\"",
  "id" : 781073611616620544,
  "created_at" : "2016-09-28 10:10:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/781038753762738180\/photo\/1",
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/wWieJfTMSU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtbOlwqWcAA9xLc.jpg",
      "id_str" : "781038733508374528",
      "id" : 781038733508374528,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtbOlwqWcAA9xLc.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wWieJfTMSU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "781038753762738180",
  "text" : "From today's mailbox https:\/\/t.co\/wWieJfTMSU",
  "id" : 781038753762738180,
  "created_at" : "2016-09-28 07:52:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780927571441639424",
  "text" : "RT @SarcasmMother: We live in a world where trained cops can panic &amp; act on impulse, but untrained civilians must remain calm with a gun in\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "780771394611281920",
    "text" : "We live in a world where trained cops can panic &amp; act on impulse, but untrained civilians must remain calm with a gun in their face.",
    "id" : 780771394611281920,
    "created_at" : "2016-09-27 14:09:41 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 780927571441639424,
  "created_at" : "2016-09-28 00:30:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "indices" : [ 3, 11 ],
      "id_str" : "15019743",
      "id" : 15019743
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780854612194693121",
  "text" : "RT @2bscene: Seriously, on what planet does it make sense to reward people who fled during the hard times, rather than those who stuck it o\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "780846378398445568",
    "text" : "Seriously, on what planet does it make sense to reward people who fled during the hard times, rather than those who stuck it out?",
    "id" : 780846378398445568,
    "created_at" : "2016-09-27 19:07:38 +0000",
    "user" : {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "protected" : false,
      "id_str" : "15019743",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/779435193312608256\/NJW57jaX_normal.jpg",
      "id" : 15019743,
      "verified" : false
    }
  },
  "id" : 780854612194693121,
  "created_at" : "2016-09-27 19:40:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Singapore Airlines",
      "screen_name" : "SingaporeAir",
      "indices" : [ 0, 13 ],
      "id_str" : "253340062",
      "id" : 253340062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "780678367247032320",
  "geo" : { },
  "id_str" : "780807996054114304",
  "in_reply_to_user_id" : 253340062,
  "text" : "@SingaporeAir Along the aisle as I tend to make more frequent trip to the WC",
  "id" : 780807996054114304,
  "in_reply_to_status_id" : 780678367247032320,
  "created_at" : "2016-09-27 16:35:07 +0000",
  "in_reply_to_screen_name" : "SingaporeAir",
  "in_reply_to_user_id_str" : "253340062",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/qmHy61VeOS",
      "expanded_url" : "https:\/\/www.facebook.com\/hkveggie2015\/photos\/?tab=album&album_id=1522708078059024",
      "display_url" : "facebook.com\/hkveggie2015\/p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780777850194522112",
  "text" : "60\u9053\u5BB6\u5E38\u7D20\u83DC\u98DF\u8B5C https:\/\/t.co\/qmHy61VeOS",
  "id" : 780777850194522112,
  "created_at" : "2016-09-27 14:35:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780717676905758720",
  "text" : "I love organising things. I moving my data analysis code from Dropbox to GitHubGist.",
  "id" : 780717676905758720,
  "created_at" : "2016-09-27 10:36:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Unroll.Me",
      "screen_name" : "Unrollme",
      "indices" : [ 86, 95 ],
      "id_str" : "339769044",
      "id" : 339769044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/bMIwen1wW4",
      "expanded_url" : "https:\/\/unroll.me\/?utm_campaign=share-twitter-re",
      "display_url" : "unroll.me\/?utm_campaign=\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780694027528237056",
  "text" : "Unsubscribe from unwanted emails and combine the rest into a single daily digest with @Unrollme https:\/\/t.co\/bMIwen1wW4",
  "id" : 780694027528237056,
  "created_at" : "2016-09-27 09:02:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shelagh Fogarty",
      "screen_name" : "ShelaghFogarty",
      "indices" : [ 3, 18 ],
      "id_str" : "96092282",
      "id" : 96092282
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SaudiArabia",
      "indices" : [ 124, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780692246865776640",
  "text" : "RT @ShelaghFogarty: Never forget British women couldn't get a mortgage without spouse or father co signing until mid 1970s. #SaudiArabia ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SaudiArabia",
        "indices" : [ 104, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/oXTa9acobK",
        "expanded_url" : "https:\/\/twitter.com\/lilo11\/status\/780688722392190977",
        "display_url" : "twitter.com\/lilo11\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "780689596271263744",
    "text" : "Never forget British women couldn't get a mortgage without spouse or father co signing until mid 1970s. #SaudiArabia https:\/\/t.co\/oXTa9acobK",
    "id" : 780689596271263744,
    "created_at" : "2016-09-27 08:44:38 +0000",
    "user" : {
      "name" : "Shelagh Fogarty",
      "screen_name" : "ShelaghFogarty",
      "protected" : false,
      "id_str" : "96092282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665499719108575232\/jlNWZtOg_normal.jpg",
      "id" : 96092282,
      "verified" : true
    }
  },
  "id" : 780692246865776640,
  "created_at" : "2016-09-27 08:55:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 3, 19 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780652965262663680",
  "text" : "RT @interactivemark: That was like a 3rd grader debating a Rhodes scholar. How can people believe that he's a suitable Presidential candida\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "debatenight",
        "indices" : [ 122, 134 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "780599163583098885",
    "text" : "That was like a 3rd grader debating a Rhodes scholar. How can people believe that he's a suitable Presidential candidate? #debatenight",
    "id" : 780599163583098885,
    "created_at" : "2016-09-27 02:45:18 +0000",
    "user" : {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "protected" : false,
      "id_str" : "14340736",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974702103984791552\/eHtIZ2ef_normal.jpg",
      "id" : 14340736,
      "verified" : false
    }
  },
  "id" : 780652965262663680,
  "created_at" : "2016-09-27 06:19:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kevin O'Brien",
      "screen_name" : "dragonflystats",
      "indices" : [ 3, 18 ],
      "id_str" : "1291259790",
      "id" : 1291259790
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/3U8TYnAvCa",
      "expanded_url" : "http:\/\/on.wsj.com\/2dmGppN",
      "display_url" : "on.wsj.com\/2dmGppN"
    } ]
  },
  "geo" : { },
  "id_str" : "780512738539896832",
  "text" : "RT @dragonflystats: need this in Ireland -  too much stuff is turning \"academic\" https:\/\/t.co\/3U8TYnAvCa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/3U8TYnAvCa",
        "expanded_url" : "http:\/\/on.wsj.com\/2dmGppN",
        "display_url" : "on.wsj.com\/2dmGppN"
      } ]
    },
    "geo" : { },
    "id_str" : "780495489842020352",
    "text" : "need this in Ireland -  too much stuff is turning \"academic\" https:\/\/t.co\/3U8TYnAvCa",
    "id" : 780495489842020352,
    "created_at" : "2016-09-26 19:53:20 +0000",
    "user" : {
      "name" : "Kevin O'Brien",
      "screen_name" : "dragonflystats",
      "protected" : false,
      "id_str" : "1291259790",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3427713318\/840e4aa0a6dca9263a0507fbfe4c4074_normal.jpeg",
      "id" : 1291259790,
      "verified" : false
    }
  },
  "id" : 780512738539896832,
  "created_at" : "2016-09-26 21:01:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jens Lapinski",
      "screen_name" : "jenslapinski",
      "indices" : [ 3, 16 ],
      "id_str" : "12179742",
      "id" : 12179742
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780493045049462784",
  "text" : "RT @jenslapinski: 5\/ Healthcare coverage is excellent. I have high quality doctors and hospitals. All with standard public insurance.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "780484250885824513",
    "geo" : { },
    "id_str" : "780484519489134592",
    "in_reply_to_user_id" : 12179742,
    "text" : "5\/ Healthcare coverage is excellent. I have high quality doctors and hospitals. All with standard public insurance.",
    "id" : 780484519489134592,
    "in_reply_to_status_id" : 780484250885824513,
    "created_at" : "2016-09-26 19:09:44 +0000",
    "in_reply_to_screen_name" : "jenslapinski",
    "in_reply_to_user_id_str" : "12179742",
    "user" : {
      "name" : "Jens Lapinski",
      "screen_name" : "jenslapinski",
      "protected" : false,
      "id_str" : "12179742",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/884318686751883264\/QRWVe_E-_normal.jpg",
      "id" : 12179742,
      "verified" : false
    }
  },
  "id" : 780493045049462784,
  "created_at" : "2016-09-26 19:43:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexandra Fulford",
      "screen_name" : "pharmaguapa",
      "indices" : [ 3, 15 ],
      "id_str" : "106681538",
      "id" : 106681538
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780492473466519552",
  "text" : "RT @pharmaguapa: Earn 3 free tickets to Collision worth $700 each by referring 3 women in tech. Limited to first 2,016 tickets. https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/RDL8oClmYy",
        "expanded_url" : "http:\/\/wit.collisionconf.com\/?kid=BNRKS",
        "display_url" : "wit.collisionconf.com\/?kid=BNRKS"
      } ]
    },
    "geo" : { },
    "id_str" : "780473034574626818",
    "text" : "Earn 3 free tickets to Collision worth $700 each by referring 3 women in tech. Limited to first 2,016 tickets. https:\/\/t.co\/RDL8oClmYy",
    "id" : 780473034574626818,
    "created_at" : "2016-09-26 18:24:06 +0000",
    "user" : {
      "name" : "Alexandra Fulford",
      "screen_name" : "pharmaguapa",
      "protected" : false,
      "id_str" : "106681538",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/643504180\/red2_normal.JPG",
      "id" : 106681538,
      "verified" : false
    }
  },
  "id" : 780492473466519552,
  "created_at" : "2016-09-26 19:41:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jens Lapinski",
      "screen_name" : "jenslapinski",
      "indices" : [ 3, 16 ],
      "id_str" : "12179742",
      "id" : 12179742
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780492381556764674",
  "text" : "RT @jenslapinski: 1\/ Quick rant on how awesome Berlin is as a place to live. For the average developer salary, you can live like a king.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "780482025077805064",
    "text" : "1\/ Quick rant on how awesome Berlin is as a place to live. For the average developer salary, you can live like a king.",
    "id" : 780482025077805064,
    "created_at" : "2016-09-26 18:59:50 +0000",
    "user" : {
      "name" : "Jens Lapinski",
      "screen_name" : "jenslapinski",
      "protected" : false,
      "id_str" : "12179742",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/884318686751883264\/QRWVe_E-_normal.jpg",
      "id" : 12179742,
      "verified" : false
    }
  },
  "id" : 780492381556764674,
  "created_at" : "2016-09-26 19:40:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Waldo Jaquith",
      "screen_name" : "waldojaquith",
      "indices" : [ 3, 16 ],
      "id_str" : "206283535",
      "id" : 206283535
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/qJvY1zWjpa",
      "expanded_url" : "https:\/\/opensource.org\/history",
      "display_url" : "opensource.org\/history"
    } ]
  },
  "geo" : { },
  "id_str" : "780491807234859008",
  "text" : "RT @waldojaquith: This is Christine Peterson. She coined the phrase \"open source\" on February 3, 1998. https:\/\/t.co\/qJvY1zWjpa https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/waldojaquith\/status\/780399532282609664\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/cAjRP8coeL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtSJIp9WAAAU0pH.jpg",
        "id_str" : "780399417237045248",
        "id" : 780399417237045248,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtSJIp9WAAAU0pH.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 243,
          "resize" : "fit",
          "w" : 185
        }, {
          "h" : 243,
          "resize" : "fit",
          "w" : 185
        }, {
          "h" : 243,
          "resize" : "fit",
          "w" : 185
        }, {
          "h" : 243,
          "resize" : "fit",
          "w" : 185
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cAjRP8coeL"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/qJvY1zWjpa",
        "expanded_url" : "https:\/\/opensource.org\/history",
        "display_url" : "opensource.org\/history"
      } ]
    },
    "geo" : { },
    "id_str" : "780399532282609664",
    "text" : "This is Christine Peterson. She coined the phrase \"open source\" on February 3, 1998. https:\/\/t.co\/qJvY1zWjpa https:\/\/t.co\/cAjRP8coeL",
    "id" : 780399532282609664,
    "created_at" : "2016-09-26 13:32:02 +0000",
    "user" : {
      "name" : "Waldo Jaquith",
      "screen_name" : "waldojaquith",
      "protected" : false,
      "id_str" : "206283535",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459705033282420736\/lrr8ZvPe_normal.png",
      "id" : 206283535,
      "verified" : false
    }
  },
  "id" : 780491807234859008,
  "created_at" : "2016-09-26 19:38:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780471065650860032",
  "text" : "TIL a new word \"subvention\". Back home we just call it subsidy.",
  "id" : 780471065650860032,
  "created_at" : "2016-09-26 18:16:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/InOK31Psqx",
      "expanded_url" : "https:\/\/cloudplatform.googleblog.com\/2016\/09\/global-historical-daily-weather-data-now-available-in-BigQuery.html",
      "display_url" : "cloudplatform.googleblog.com\/2016\/09\/global\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780464636139372544",
  "text" : "If you analyze data to make better business decisions, weather should be one of your inputs. https:\/\/t.co\/InOK31Psqx",
  "id" : 780464636139372544,
  "created_at" : "2016-09-26 17:50:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/X5rvYPOJxP",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/779906355746398208",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780456625719087104",
  "text" : "\"You know you\u2019ve made it as a mall when the mecca of capitalism tries to claim you as their own.\" https:\/\/t.co\/X5rvYPOJxP",
  "id" : 780456625719087104,
  "created_at" : "2016-09-26 17:18:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/JvKRF7JifA",
      "expanded_url" : "http:\/\/singaporedaily.net\/2016\/09\/19\/ang-moh-lived-singapore-long-enough-see-merits-flaws\/",
      "display_url" : "singaporedaily.net\/2016\/09\/19\/ang\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780456084293156864",
  "text" : "RT @yapphenghui: This ang moh has lived in Singapore long enough to see its merits and flaws https:\/\/t.co\/JvKRF7JifA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/JvKRF7JifA",
        "expanded_url" : "http:\/\/singaporedaily.net\/2016\/09\/19\/ang-moh-lived-singapore-long-enough-see-merits-flaws\/",
        "display_url" : "singaporedaily.net\/2016\/09\/19\/ang\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "780018800959684608",
    "text" : "This ang moh has lived in Singapore long enough to see its merits and flaws https:\/\/t.co\/JvKRF7JifA",
    "id" : 780018800959684608,
    "created_at" : "2016-09-25 12:19:08 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 780456084293156864,
  "created_at" : "2016-09-26 17:16:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780453803137130498",
  "text" : "No bus strike on Tuesday and Wednesday. I am happy.",
  "id" : 780453803137130498,
  "created_at" : "2016-09-26 17:07:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "@TheVJworld",
      "screen_name" : "thevjworld",
      "indices" : [ 3, 14 ],
      "id_str" : "3987936869",
      "id" : 3987936869
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Trump",
      "indices" : [ 67, 73 ]
    }, {
      "text" : "Brexit",
      "indices" : [ 78, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/eBKavOovOS",
      "expanded_url" : "https:\/\/medium.com\/@theonlytoby\/history-tells-us-what-will-happen-next-with-brexit-trump-a3fefd154714#.7d2xlhanq",
      "display_url" : "medium.com\/@theonlytoby\/h\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780448921546059777",
  "text" : "RT @thevjworld: This is about the BEST thing I have read so far on #Trump and #Brexit\n\nhttps:\/\/t.co\/eBKavOovOS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Trump",
        "indices" : [ 51, 57 ]
      }, {
        "text" : "Brexit",
        "indices" : [ 62, 69 ]
      } ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/eBKavOovOS",
        "expanded_url" : "https:\/\/medium.com\/@theonlytoby\/history-tells-us-what-will-happen-next-with-brexit-trump-a3fefd154714#.7d2xlhanq",
        "display_url" : "medium.com\/@theonlytoby\/h\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "780385481141944320",
    "text" : "This is about the BEST thing I have read so far on #Trump and #Brexit\n\nhttps:\/\/t.co\/eBKavOovOS",
    "id" : 780385481141944320,
    "created_at" : "2016-09-26 12:36:12 +0000",
    "user" : {
      "name" : "@TheVJworld",
      "screen_name" : "thevjworld",
      "protected" : false,
      "id_str" : "3987936869",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/655772938621292544\/NA2LzFlr_normal.jpg",
      "id" : 3987936869,
      "verified" : false
    }
  },
  "id" : 780448921546059777,
  "created_at" : "2016-09-26 16:48:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "indices" : [ 3, 13 ],
      "id_str" : "77901568",
      "id" : 77901568
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/at7l5JfKmi",
      "expanded_url" : "https:\/\/twitter.com\/EntAnon\/status\/780376879735926784",
      "display_url" : "twitter.com\/EntAnon\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780446103296671744",
  "text" : "RT @allentv4u: Happy &amp; humbled to be featured in this list! https:\/\/t.co\/at7l5JfKmi",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 49, 72 ],
        "url" : "https:\/\/t.co\/at7l5JfKmi",
        "expanded_url" : "https:\/\/twitter.com\/EntAnon\/status\/780376879735926784",
        "display_url" : "twitter.com\/EntAnon\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "780437085199147008",
    "text" : "Happy &amp; humbled to be featured in this list! https:\/\/t.co\/at7l5JfKmi",
    "id" : 780437085199147008,
    "created_at" : "2016-09-26 16:01:15 +0000",
    "user" : {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "protected" : false,
      "id_str" : "77901568",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/787980171202686976\/QbSu0IKB_normal.jpg",
      "id" : 77901568,
      "verified" : false
    }
  },
  "id" : 780446103296671744,
  "created_at" : "2016-09-26 16:37:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kmandel",
      "screen_name" : "kmandel",
      "indices" : [ 3, 11 ],
      "id_str" : "14721687",
      "id" : 14721687
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/H4q89uPeDT",
      "expanded_url" : "http:\/\/www.wsj.com\/articles\/rolling-stone-magazine-sells-49-stake-to-singapores-bandlab-technologies-1474812182",
      "display_url" : "wsj.com\/articles\/rolli\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780436622072578053",
  "text" : "RT @kmandel: Rolling Stone Magazine Sells 49% Stake to Singapore\u2019s BandLab Technologies - WSJ https:\/\/t.co\/H4q89uPeDT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/H4q89uPeDT",
        "expanded_url" : "http:\/\/www.wsj.com\/articles\/rolling-stone-magazine-sells-49-stake-to-singapores-bandlab-technologies-1474812182",
        "display_url" : "wsj.com\/articles\/rolli\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "780051921918832641",
    "text" : "Rolling Stone Magazine Sells 49% Stake to Singapore\u2019s BandLab Technologies - WSJ https:\/\/t.co\/H4q89uPeDT",
    "id" : 780051921918832641,
    "created_at" : "2016-09-25 14:30:45 +0000",
    "user" : {
      "name" : "kmandel",
      "screen_name" : "kmandel",
      "protected" : false,
      "id_str" : "14721687",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/562516601111457793\/5mnbHL3__normal.jpeg",
      "id" : 14721687,
      "verified" : false
    }
  },
  "id" : 780436622072578053,
  "created_at" : "2016-09-26 15:59:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "indices" : [ 3, 12 ],
      "id_str" : "11503282",
      "id" : 11503282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/Ix13pxPn7X",
      "expanded_url" : "http:\/\/on.recode.net\/2dt2xz0",
      "display_url" : "on.recode.net\/2dt2xz0"
    } ]
  },
  "geo" : { },
  "id_str" : "780432848503398400",
  "text" : "RT @ladyxtel: The going rate for autonomous car engineers is $10m each https:\/\/t.co\/Ix13pxPn7X",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/Ix13pxPn7X",
        "expanded_url" : "http:\/\/on.recode.net\/2dt2xz0",
        "display_url" : "on.recode.net\/2dt2xz0"
      } ]
    },
    "geo" : { },
    "id_str" : "780424692867928065",
    "text" : "The going rate for autonomous car engineers is $10m each https:\/\/t.co\/Ix13pxPn7X",
    "id" : 780424692867928065,
    "created_at" : "2016-09-26 15:12:01 +0000",
    "user" : {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "protected" : false,
      "id_str" : "11503282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849872363671830528\/5iExzLy9_normal.jpg",
      "id" : 11503282,
      "verified" : true
    }
  },
  "id" : 780432848503398400,
  "created_at" : "2016-09-26 15:44:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clare County Council",
      "screen_name" : "ClareCoCo",
      "indices" : [ 3, 13 ],
      "id_str" : "304492668",
      "id" : 304492668
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780426259453079552",
  "text" : "RT @ClareCoCo: Ennis has been named Ireland's Tidiest Urban Centre in the 2016 SuperValu  TidyTowns National Awards. Congratulations to eve\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ClareCoCo\/status\/780378202233536512\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/67KUi7EgW6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtR1nMcW8AEEec7.jpg",
        "id_str" : "780377951657455617",
        "id" : 780377951657455617,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtR1nMcW8AEEec7.jpg",
        "sizes" : [ {
          "h" : 473,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 835,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1425,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 2280,
          "resize" : "fit",
          "w" : 3276
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/67KUi7EgW6"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/ClareCoCo\/status\/780378202233536512\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/67KUi7EgW6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtR1nMbXEAAoT4T.jpg",
        "id_str" : "780377951653269504",
        "id" : 780377951653269504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtR1nMbXEAAoT4T.jpg",
        "sizes" : [ {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 2400
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/67KUi7EgW6"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "780378202233536512",
    "text" : "Ennis has been named Ireland's Tidiest Urban Centre in the 2016 SuperValu  TidyTowns National Awards. Congratulations to everybody involved https:\/\/t.co\/67KUi7EgW6",
    "id" : 780378202233536512,
    "created_at" : "2016-09-26 12:07:16 +0000",
    "user" : {
      "name" : "Clare County Council",
      "screen_name" : "ClareCoCo",
      "protected" : false,
      "id_str" : "304492668",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/623812336525176832\/q5Ur9JNM_normal.png",
      "id" : 304492668,
      "verified" : true
    }
  },
  "id" : 780426259453079552,
  "created_at" : "2016-09-26 15:18:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/h5bhsJEIe2",
      "expanded_url" : "https:\/\/github.com\/mryap\/rtb",
      "display_url" : "github.com\/mryap\/rtb"
    }, {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/ZuFhxnOfKg",
      "expanded_url" : "https:\/\/twitter.com\/ronanlyons\/status\/780347194540974080",
      "display_url" : "twitter.com\/ronanlyons\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780348152029278208",
  "text" : "With this new index, I going to re-work my final project https:\/\/t.co\/h5bhsJEIe2 https:\/\/t.co\/ZuFhxnOfKg",
  "id" : 780348152029278208,
  "created_at" : "2016-09-26 10:07:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "FORTUNE",
      "screen_name" : "FortuneMagazine",
      "indices" : [ 28, 44 ],
      "id_str" : "25053299",
      "id" : 25053299
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/hT54wB4AFz",
      "expanded_url" : "http:\/\/for.tn\/22wvZun?xid=for_tw_sh",
      "display_url" : "for.tn\/22wvZun?xid=fo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780346634781728768",
  "text" : "My Year in Startup Hell via @FortuneMagazine https:\/\/t.co\/hT54wB4AFz",
  "id" : 780346634781728768,
  "created_at" : "2016-09-26 10:01:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WordCamp Belfast",
      "screen_name" : "WCBelfast",
      "indices" : [ 0, 10 ],
      "id_str" : "4833420611",
      "id" : 4833420611
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/LkZI9yrUOl",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/technology\/wp-engine-to-create-100-jobs-in-limerick-1.2805609",
      "display_url" : "irishtimes.com\/business\/techn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780344286550302720",
  "in_reply_to_user_id" : 4833420611,
  "text" : "@WCBelfast might get them to sponsor NI first WordPress event? https:\/\/t.co\/LkZI9yrUOl",
  "id" : 780344286550302720,
  "created_at" : "2016-09-26 09:52:30 +0000",
  "in_reply_to_screen_name" : "WCBelfast",
  "in_reply_to_user_id_str" : "4833420611",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/AjRslGx0tH",
      "expanded_url" : "https:\/\/twitter.com\/ingridmileyRTE\/status\/780048222630899712",
      "display_url" : "twitter.com\/ingridmileyRTE\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780343531009347584",
  "text" : "Keeping finger cross for Tues\/Wed suspension as I need the transport. However, they can strike on 1st Oct. https:\/\/t.co\/AjRslGx0tH",
  "id" : 780343531009347584,
  "created_at" : "2016-09-26 09:49:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780341050758074368",
  "text" : "Anyone has problem posting updates on Linkedin today?",
  "id" : 780341050758074368,
  "created_at" : "2016-09-26 09:39:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 121, 144 ],
      "url" : "https:\/\/t.co\/hlamMzibSx",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/most-singaporeans-would-choose-tharman-1523976433713206.html",
      "display_url" : "sg.news.yahoo.com\/most-singapore\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780317326688256001",
  "text" : "He was favoured across different age groups &amp; ethnicities, as well as socio-economic status, according to the survey https:\/\/t.co\/hlamMzibSx",
  "id" : 780317326688256001,
  "created_at" : "2016-09-26 08:05:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/WLZnt72dGm",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/780257902829314050",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "780302162630938624",
  "text" : "A  USD 400,000 fish bowl over your head. https:\/\/t.co\/WLZnt72dGm",
  "id" : 780302162630938624,
  "created_at" : "2016-09-26 07:05:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780301646161141760",
  "text" : "Good morning. Guess where am I as I tweet this?",
  "id" : 780301646161141760,
  "created_at" : "2016-09-26 07:03:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 3, 15 ],
      "id_str" : "41085467",
      "id" : 41085467
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/780270450806919168\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/3xe1gVziWK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtQPU2wUMAATBK3.jpg",
      "id_str" : "780265486411902976",
      "id" : 780265486411902976,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtQPU2wUMAATBK3.jpg",
      "sizes" : [ {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1745
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 421,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 743,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1745
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3xe1gVziWK"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/PTg1aeYscE",
      "expanded_url" : "http:\/\/bit.ly\/2d2nK4F",
      "display_url" : "bit.ly\/2d2nK4F"
    } ]
  },
  "geo" : { },
  "id_str" : "780286534683594752",
  "text" : "RT @TODAYonline: Coming soon: Nursing rooms at bus interchanges, MRT stations https:\/\/t.co\/PTg1aeYscE https:\/\/t.co\/3xe1gVziWK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/780270450806919168\/photo\/1",
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/3xe1gVziWK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtQPU2wUMAATBK3.jpg",
        "id_str" : "780265486411902976",
        "id" : 780265486411902976,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtQPU2wUMAATBK3.jpg",
        "sizes" : [ {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1745
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 743,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1745
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3xe1gVziWK"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/PTg1aeYscE",
        "expanded_url" : "http:\/\/bit.ly\/2d2nK4F",
        "display_url" : "bit.ly\/2d2nK4F"
      } ]
    },
    "geo" : { },
    "id_str" : "780270450806919168",
    "text" : "Coming soon: Nursing rooms at bus interchanges, MRT stations https:\/\/t.co\/PTg1aeYscE https:\/\/t.co\/3xe1gVziWK",
    "id" : 780270450806919168,
    "created_at" : "2016-09-26 04:59:06 +0000",
    "user" : {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "protected" : false,
      "id_str" : "41085467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/606854638801846272\/pi_RUbVK_normal.png",
      "id" : 41085467,
      "verified" : true
    }
  },
  "id" : 780286534683594752,
  "created_at" : "2016-09-26 06:03:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geraldine Herbert",
      "screen_name" : "GerHerbert1",
      "indices" : [ 3, 15 ],
      "id_str" : "462083773",
      "id" : 462083773
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "780150234928275456",
  "text" : "RT @GerHerbert1: Man residing on Co Kildare border forced to source car insurance in Germany due to Ireland's high prices \nhttps:\/\/t.co\/O1A\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/O1ATMD9gXA",
        "expanded_url" : "http:\/\/www.kildarenow.com\/news\/man-residing-in-co-kildare-border-forced-to-source-car-insurance-in-germany-due-to-irelands-high-prices\/97941",
        "display_url" : "kildarenow.com\/news\/man-resid\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "780127660974505984",
    "text" : "Man residing on Co Kildare border forced to source car insurance in Germany due to Ireland's high prices \nhttps:\/\/t.co\/O1ATMD9gXA",
    "id" : 780127660974505984,
    "created_at" : "2016-09-25 19:31:43 +0000",
    "user" : {
      "name" : "Geraldine Herbert",
      "screen_name" : "GerHerbert1",
      "protected" : false,
      "id_str" : "462083773",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/492655680470282242\/Du5bcunu_normal.jpeg",
      "id" : 462083773,
      "verified" : false
    }
  },
  "id" : 780150234928275456,
  "created_at" : "2016-09-25 21:01:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mathieu A.",
      "screen_name" : "zoontek",
      "indices" : [ 3, 11 ],
      "id_str" : "15810479",
      "id" : 15810479
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/s1wGqdFDDn",
      "expanded_url" : "http:\/\/www.cyclingweekly.co.uk\/news\/latest-news\/bike-manufacturer-reduces-delivery-damage-70-per-cent-printing-tv-box-285180",
      "display_url" : "cyclingweekly.co.uk\/news\/latest-ne\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "779967985536606208",
  "text" : "RT @Zoontek: Bike manufacturer sees huge reduction in delivery damage by printing TV on the box https:\/\/t.co\/s1wGqdFDDn",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/s1wGqdFDDn",
        "expanded_url" : "http:\/\/www.cyclingweekly.co.uk\/news\/latest-news\/bike-manufacturer-reduces-delivery-damage-70-per-cent-printing-tv-box-285180",
        "display_url" : "cyclingweekly.co.uk\/news\/latest-ne\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "779794851508391936",
    "text" : "Bike manufacturer sees huge reduction in delivery damage by printing TV on the box https:\/\/t.co\/s1wGqdFDDn",
    "id" : 779794851508391936,
    "created_at" : "2016-09-24 21:29:15 +0000",
    "user" : {
      "name" : "Mathieu A.",
      "screen_name" : "zoontek",
      "protected" : false,
      "id_str" : "15810479",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/911610006956593153\/AoXEQmvy_normal.jpg",
      "id" : 15810479,
      "verified" : false
    }
  },
  "id" : 779967985536606208,
  "created_at" : "2016-09-25 08:57:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 3, 12 ],
      "id_str" : "136690988",
      "id" : 136690988
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "edtech",
      "indices" : [ 126, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/rhEtgNDTjb",
      "expanded_url" : "http:\/\/fetchcourses.ie\/",
      "display_url" : "fetchcourses.ie"
    } ]
  },
  "geo" : { },
  "id_str" : "779727665293041664",
  "text" : "RT @ailieirv: Pleased that there is a new website outlining Further Education &amp; Training options. https:\/\/t.co\/rhEtgNDTjb #edtech",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "edtech",
        "indices" : [ 112, 119 ]
      } ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/rhEtgNDTjb",
        "expanded_url" : "http:\/\/fetchcourses.ie\/",
        "display_url" : "fetchcourses.ie"
      } ]
    },
    "geo" : { },
    "id_str" : "779033022528000000",
    "text" : "Pleased that there is a new website outlining Further Education &amp; Training options. https:\/\/t.co\/rhEtgNDTjb #edtech",
    "id" : 779033022528000000,
    "created_at" : "2016-09-22 19:02:00 +0000",
    "user" : {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "protected" : false,
      "id_str" : "136690988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528642221956796416\/WDp_wLcX_normal.jpeg",
      "id" : 136690988,
      "verified" : false
    }
  },
  "id" : 779727665293041664,
  "created_at" : "2016-09-24 17:02:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/CQCeWlfY19",
      "expanded_url" : "http:\/\/nicolas.kruchten.com\/content\/2015\/09\/jupyter_pivottablejs\/",
      "display_url" : "nicolas.kruchten.com\/content\/2015\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "779724781168058372",
  "text" : "Added Pivot Tables and Charts, in Jupyter\/IPython Notebook\nhttps:\/\/t.co\/CQCeWlfY19",
  "id" : 779724781168058372,
  "created_at" : "2016-09-24 16:50:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 3, 9 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779712439399149568",
  "text" : "RT @mryap: Have you follow people on Twitter that you don\u2019t like because of the need to hear different view points?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "778522695361294336",
    "text" : "Have you follow people on Twitter that you don\u2019t like because of the need to hear different view points?",
    "id" : 778522695361294336,
    "created_at" : "2016-09-21 09:14:09 +0000",
    "user" : {
      "name" : "hello!",
      "screen_name" : "mryap",
      "protected" : false,
      "id_str" : "9465632",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
      "id" : 9465632,
      "verified" : false
    }
  },
  "id" : 779712439399149568,
  "created_at" : "2016-09-24 16:01:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 3, 9 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779712266891587584",
  "text" : "RT @mryap: I'm currently seeking for a role in data analytics in Dublin, contract or permanent. Please get in touch if you have any leads.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776395486894891008",
    "text" : "I'm currently seeking for a role in data analytics in Dublin, contract or permanent. Please get in touch if you have any leads. Thanks.",
    "id" : 776395486894891008,
    "created_at" : "2016-09-15 12:21:23 +0000",
    "user" : {
      "name" : "hello!",
      "screen_name" : "mryap",
      "protected" : false,
      "id_str" : "9465632",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
      "id" : 9465632,
      "verified" : false
    }
  },
  "id" : 779712266891587584,
  "created_at" : "2016-09-24 16:01:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Graham Linehan",
      "screen_name" : "Glinner",
      "indices" : [ 3, 11 ],
      "id_str" : "7076492",
      "id" : 7076492
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "marchforchoice",
      "indices" : [ 67, 82 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779660634871005184",
  "text" : "RT @Glinner: Hey, world, please do all you can to signal boost the #marchforchoice today. Irish women need your help.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/onloft.com\/tweetlogix\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "marchforchoice",
        "indices" : [ 54, 69 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "779606967329382400",
    "text" : "Hey, world, please do all you can to signal boost the #marchforchoice today. Irish women need your help.",
    "id" : 779606967329382400,
    "created_at" : "2016-09-24 09:02:40 +0000",
    "user" : {
      "name" : "Graham Linehan",
      "screen_name" : "Glinner",
      "protected" : false,
      "id_str" : "7076492",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/433543395974205440\/e1vZ6x9u_normal.jpeg",
      "id" : 7076492,
      "verified" : true
    }
  },
  "id" : 779660634871005184,
  "created_at" : "2016-09-24 12:35:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Louise O' Neill",
      "screen_name" : "oneilllo",
      "indices" : [ 3, 12 ],
      "id_str" : "25263082",
      "id" : 25263082
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MarchForChoice",
      "indices" : [ 96, 111 ]
    }, {
      "text" : "RepealThe8th",
      "indices" : [ 112, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779660599731126272",
  "text" : "RT @oneilllo: Let's not allow rain or bus strikes to stop us marching today. It's too important #MarchForChoice #RepealThe8th https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "MarchForChoice",
        "indices" : [ 82, 97 ]
      }, {
        "text" : "RepealThe8th",
        "indices" : [ 98, 111 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/VtLCeQfE4Q",
        "expanded_url" : "https:\/\/twitter.com\/criticalredpen\/status\/779584258407034880",
        "display_url" : "twitter.com\/criticalredpen\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "779608290539995136",
    "text" : "Let's not allow rain or bus strikes to stop us marching today. It's too important #MarchForChoice #RepealThe8th https:\/\/t.co\/VtLCeQfE4Q",
    "id" : 779608290539995136,
    "created_at" : "2016-09-24 09:07:55 +0000",
    "user" : {
      "name" : "Louise O' Neill",
      "screen_name" : "oneilllo",
      "protected" : false,
      "id_str" : "25263082",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/999918644338483200\/SUhg3b1E_normal.jpg",
      "id" : 25263082,
      "verified" : true
    }
  },
  "id" : 779660599731126272,
  "created_at" : "2016-09-24 12:35:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779425028924436480",
  "text" : "\u201C\u5343\u91D1\u5C0F\u59D0\u201D \u53EA\u503C\u5343\u91D1\uFF1F",
  "id" : 779425028924436480,
  "created_at" : "2016-09-23 20:59:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "indices" : [ 3, 15 ],
      "id_str" : "10504042",
      "id" : 10504042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779407403947290624",
  "text" : "RT @ChuiSquared: Pleased to be selected - BBC Design Research roster on International Research. Look forward to working with the team https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/oixf5fhG9u",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/blogs\/internet\/entries\/bb7472e1-13be-4d61-a532-e2ecc7122bb7",
        "display_url" : "bbc.co.uk\/blogs\/internet\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "779333778368040960",
    "text" : "Pleased to be selected - BBC Design Research roster on International Research. Look forward to working with the team https:\/\/t.co\/oixf5fhG9u",
    "id" : 779333778368040960,
    "created_at" : "2016-09-23 14:57:06 +0000",
    "user" : {
      "name" : "Chui Chui Tan",
      "screen_name" : "ChuiSquared",
      "protected" : false,
      "id_str" : "10504042",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/892390265444552704\/Yg7EbMxO_normal.jpg",
      "id" : 10504042,
      "verified" : false
    }
  },
  "id" : 779407403947290624,
  "created_at" : "2016-09-23 19:49:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779407160400805889",
  "text" : "Algorithms could save \"insert your industry sector here\"",
  "id" : 779407160400805889,
  "created_at" : "2016-09-23 19:48:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779389438568914944",
  "text" : "RT @lisaocarroll: FT reporting that Google and Salesforce consider Twitter bid. Hegemony",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "779363139804598274",
    "text" : "FT reporting that Google and Salesforce consider Twitter bid. Hegemony",
    "id" : 779363139804598274,
    "created_at" : "2016-09-23 16:53:47 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 779389438568914944,
  "created_at" : "2016-09-23 18:38:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Claudia Hammond",
      "screen_name" : "claudiahammond",
      "indices" : [ 8, 23 ],
      "id_str" : "130849382",
      "id" : 130849382
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/779353217826189312\/photo\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/bXkIslagRN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CtDRm6tW8AERGv_.jpg",
      "id_str" : "779353202059833345",
      "id" : 779353202059833345,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtDRm6tW8AERGv_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bXkIslagRN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779353217826189312",
  "text" : "Reading @claudiahammond fascinating book about \u20AC\u00A5\u00A3$ https:\/\/t.co\/bXkIslagRN",
  "id" : 779353217826189312,
  "created_at" : "2016-09-23 16:14:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SamsungNote2",
      "indices" : [ 43, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/qBZp0GAUji",
      "expanded_url" : "http:\/\/bit.ly\/2dn6pBS",
      "display_url" : "bit.ly\/2dn6pBS"
    } ]
  },
  "geo" : { },
  "id_str" : "779345242558459904",
  "text" : "RT @ChannelNewsAsia: ANOTHER SMOKING NOTE: #SamsungNote2 sparks alarm on flight from Singapore to Chennai https:\/\/t.co\/qBZp0GAUji https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/779342579141705729\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/mt33pAOdr7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CtDH47eUsAA-DyW.jpg",
        "id_str" : "779342516386574336",
        "id" : 779342516386574336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CtDH47eUsAA-DyW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mt33pAOdr7"
      } ],
      "hashtags" : [ {
        "text" : "SamsungNote2",
        "indices" : [ 22, 35 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/qBZp0GAUji",
        "expanded_url" : "http:\/\/bit.ly\/2dn6pBS",
        "display_url" : "bit.ly\/2dn6pBS"
      } ]
    },
    "geo" : { },
    "id_str" : "779342579141705729",
    "text" : "ANOTHER SMOKING NOTE: #SamsungNote2 sparks alarm on flight from Singapore to Chennai https:\/\/t.co\/qBZp0GAUji https:\/\/t.co\/mt33pAOdr7",
    "id" : 779342579141705729,
    "created_at" : "2016-09-23 15:32:05 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 779345242558459904,
  "created_at" : "2016-09-23 15:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/XnJHWZ2ioL",
      "expanded_url" : "https:\/\/www.fastcompany.com\/3030063\/why-the-r-programming-language-is-good-for-business",
      "display_url" : "fastcompany.com\/3030063\/why-th\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "779240522888142848",
  "text" : "Tina Amirtha explains why the R Programming language is good for business. https:\/\/t.co\/XnJHWZ2ioL",
  "id" : 779240522888142848,
  "created_at" : "2016-09-23 08:46:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sean Hyland",
      "screen_name" : "EndTheEU",
      "indices" : [ 3, 12 ],
      "id_str" : "70150157",
      "id" : 70150157
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DublinBusstrike",
      "indices" : [ 95, 111 ]
    }, {
      "text" : "DublinBus",
      "indices" : [ 112, 122 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/A4Ld4W5fiv",
      "expanded_url" : "http:\/\/fortune.com\/2016\/04\/20\/singapore-driverless-cars\/",
      "display_url" : "fortune.com\/2016\/04\/20\/sin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "779239157746982913",
  "text" : "RT @EndTheEU: Singapore to get driverless buses \/ taxis by end of 2016 https:\/\/t.co\/A4Ld4W5fiv #DublinBusstrike #DublinBus",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DublinBusstrike",
        "indices" : [ 81, 97 ]
      }, {
        "text" : "DublinBus",
        "indices" : [ 98, 108 ]
      } ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/A4Ld4W5fiv",
        "expanded_url" : "http:\/\/fortune.com\/2016\/04\/20\/singapore-driverless-cars\/",
        "display_url" : "fortune.com\/2016\/04\/20\/sin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "774351011897106433",
    "text" : "Singapore to get driverless buses \/ taxis by end of 2016 https:\/\/t.co\/A4Ld4W5fiv #DublinBusstrike #DublinBus",
    "id" : 774351011897106433,
    "created_at" : "2016-09-09 20:57:22 +0000",
    "user" : {
      "name" : "Sean Hyland",
      "screen_name" : "EndTheEU",
      "protected" : false,
      "id_str" : "70150157",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/484008023501717505\/ViSb6Bnt_normal.jpeg",
      "id" : 70150157,
      "verified" : false
    }
  },
  "id" : 779239157746982913,
  "created_at" : "2016-09-23 08:41:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 3, 14 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IrishTimes\/status\/776463881912934400\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/zKVENLGXii",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CsaNyFJXEAAsUal.jpg",
      "id_str" : "776463877282402304",
      "id" : 776463877282402304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CsaNyFJXEAAsUal.jpg",
      "sizes" : [ {
        "h" : 270,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 270,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 270,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 270,
        "resize" : "fit",
        "w" : 480
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/zKVENLGXii"
    } ],
    "hashtags" : [ {
      "text" : "DublinBusStrike",
      "indices" : [ 76, 92 ]
    } ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/mZI573JlTB",
      "expanded_url" : "http:\/\/on.irishtimes.com\/jrSXEH6",
      "display_url" : "on.irishtimes.com\/jrSXEH6"
    } ]
  },
  "geo" : { },
  "id_str" : "779237989645942784",
  "text" : "RT @IrishTimes: Dublin faces 13 more days of bus strikes (and traffic jams) #DublinBusStrike https:\/\/t.co\/mZI573JlTB https:\/\/t.co\/zKVENLGXii",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishTimes\/status\/776463881912934400\/photo\/1",
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/zKVENLGXii",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CsaNyFJXEAAsUal.jpg",
        "id_str" : "776463877282402304",
        "id" : 776463877282402304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CsaNyFJXEAAsUal.jpg",
        "sizes" : [ {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zKVENLGXii"
      } ],
      "hashtags" : [ {
        "text" : "DublinBusStrike",
        "indices" : [ 60, 76 ]
      } ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/mZI573JlTB",
        "expanded_url" : "http:\/\/on.irishtimes.com\/jrSXEH6",
        "display_url" : "on.irishtimes.com\/jrSXEH6"
      } ]
    },
    "geo" : { },
    "id_str" : "776463881912934400",
    "text" : "Dublin faces 13 more days of bus strikes (and traffic jams) #DublinBusStrike https:\/\/t.co\/mZI573JlTB https:\/\/t.co\/zKVENLGXii",
    "id" : 776463881912934400,
    "created_at" : "2016-09-15 16:53:10 +0000",
    "user" : {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "protected" : false,
      "id_str" : "15084853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2944517635\/5492cf569e6b0b1a9326a7d388009b60_normal.png",
      "id" : 15084853,
      "verified" : true
    }
  },
  "id" : 779237989645942784,
  "created_at" : "2016-09-23 08:36:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DublinBusstrike",
      "indices" : [ 0, 16 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779237216430219264",
  "text" : "#DublinBusstrike The only time where you had to support local business.",
  "id" : 779237216430219264,
  "created_at" : "2016-09-23 08:33:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Una Mullally",
      "screen_name" : "UnaMullally",
      "indices" : [ 3, 15 ],
      "id_str" : "20980592",
      "id" : 20980592
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779236689952829440",
  "text" : "RT @UnaMullally: If you trust women, if you believe us to be equal, if you want women to access abortion in Ireland, please march with us.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "marchforchoice",
        "indices" : [ 122, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "779076048566771712",
    "text" : "If you trust women, if you believe us to be equal, if you want women to access abortion in Ireland, please march with us. #marchforchoice",
    "id" : 779076048566771712,
    "created_at" : "2016-09-22 21:52:59 +0000",
    "user" : {
      "name" : "Una Mullally",
      "screen_name" : "UnaMullally",
      "protected" : false,
      "id_str" : "20980592",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/862980687154094080\/C1wDAI9w_normal.jpg",
      "id" : 20980592,
      "verified" : true
    }
  },
  "id" : 779236689952829440,
  "created_at" : "2016-09-23 08:31:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "779233877340590080",
  "text" : "RT @AgentTonic: I don't know man.\nLike if you are selling prawn noodles, but you have more lard than prawn, then just sell Lard Noodles lor?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "779133850395877376",
    "text" : "I don't know man.\nLike if you are selling prawn noodles, but you have more lard than prawn, then just sell Lard Noodles lor?",
    "id" : 779133850395877376,
    "created_at" : "2016-09-23 01:42:40 +0000",
    "user" : {
      "name" : "Palace Maid (Latrine in-charge)",
      "screen_name" : "ToxicConsort",
      "protected" : false,
      "id_str" : "2559422864",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034779759719280642\/CIzDbVMX_normal.jpg",
      "id" : 2559422864,
      "verified" : false
    }
  },
  "id" : 779233877340590080,
  "created_at" : "2016-09-23 08:20:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778921825514512384",
  "text" : "At a work place spotted someone browsing a bicycle website. Could be due to 13 days strike in October.",
  "id" : 778921825514512384,
  "created_at" : "2016-09-22 11:40:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC News (UK)",
      "screen_name" : "BBCNews",
      "indices" : [ 120, 128 ],
      "id_str" : "612473",
      "id" : 612473
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GBBO",
      "indices" : [ 60, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778885752151220224",
  "text" : "RT @offdutyBL: First Bradgelina or whatever it's called now #GBBO nonsense served up to me as breaking news alerts from @BBCNews Very poor\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BBC News (UK)",
        "screen_name" : "BBCNews",
        "indices" : [ 105, 113 ],
        "id_str" : "612473",
        "id" : 612473
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GBBO",
        "indices" : [ 45, 50 ]
      }, {
        "text" : "notnews",
        "indices" : [ 132, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "778884179169775616",
    "text" : "First Bradgelina or whatever it's called now #GBBO nonsense served up to me as breaking news alerts from @BBCNews Very poor service #notnews",
    "id" : 778884179169775616,
    "created_at" : "2016-09-22 09:10:33 +0000",
    "user" : {
      "name" : "Bill",
      "screen_name" : "offdutybilll",
      "protected" : false,
      "id_str" : "1384135308",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998108069593726976\/NqHlNKPO_normal.jpg",
      "id" : 1384135308,
      "verified" : false
    }
  },
  "id" : 778885752151220224,
  "created_at" : "2016-09-22 09:16:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/LCnCNJnhhk",
      "expanded_url" : "https:\/\/www.farnamstreetblog.com\/2016\/09\/lee-kuan-yew-rule\/",
      "display_url" : "farnamstreetblog.com\/2016\/09\/lee-ku\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778870115093872640",
  "text" : "So Plato, Aristotle, Socrates, I am not guided by them...I am interested in what works...  https:\/\/t.co\/LCnCNJnhhk",
  "id" : 778870115093872640,
  "created_at" : "2016-09-22 08:14:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/kvsBvzuzGR",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/776395486894891008",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778845852769091584",
  "text" : "Please do \"Reach Out\" to me. I can be contacted. emailed. phoned.\nAnd yes I use US English so I'm  a \"Folk\" too. Cheers mate https:\/\/t.co\/kvsBvzuzGR",
  "id" : 778845852769091584,
  "created_at" : "2016-09-22 06:38:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hannah Dreier",
      "screen_name" : "hannahdreier",
      "indices" : [ 3, 16 ],
      "id_str" : "92854623",
      "id" : 92854623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778647023864217600",
  "text" : "RT @hannahdreier: Cutest dimension of Venezuela crisis: Newborn babies placed in cardboard boxes amid shortages at rural hospitals https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/hannahdreier\/status\/778609733846564864\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/XNyotrkgY9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cs4sIiiWgAA55zb.jpg",
        "id_str" : "778608310803136512",
        "id" : 778608310803136512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cs4sIiiWgAA55zb.jpg",
        "sizes" : [ {
          "h" : 352,
          "resize" : "fit",
          "w" : 634
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 634
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 634
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 634
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XNyotrkgY9"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "778609733846564864",
    "text" : "Cutest dimension of Venezuela crisis: Newborn babies placed in cardboard boxes amid shortages at rural hospitals https:\/\/t.co\/XNyotrkgY9",
    "id" : 778609733846564864,
    "created_at" : "2016-09-21 15:00:01 +0000",
    "user" : {
      "name" : "Hannah Dreier",
      "screen_name" : "hannahdreier",
      "protected" : false,
      "id_str" : "92854623",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/878991384006377472\/sHTP7lS7_normal.jpg",
      "id" : 92854623,
      "verified" : true
    }
  },
  "id" : 778647023864217600,
  "created_at" : "2016-09-21 17:28:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778646434820284416",
  "text" : "RT @yapphenghui: Angry PRC couple protest on Beijing airport runway to stop plane after they arrive too late to  board https:\/\/t.co\/40iz99t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Daily News",
        "screen_name" : "SCMP_News",
        "indices" : [ 130, 140 ],
        "id_str" : "1017004900218126338",
        "id" : 1017004900218126338
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/40iz99tLAK",
        "expanded_url" : "http:\/\/www.scmp.com\/news\/china\/society\/article\/2020568\/angry-chinese-couple-airport-runway-protest-halt-flight-after?utm_source=&utm_medium=&utm_campaign=SCMPSocialNewsfeed",
        "display_url" : "scmp.com\/news\/china\/soc\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "778628899664072704",
    "text" : "Angry PRC couple protest on Beijing airport runway to stop plane after they arrive too late to  board https:\/\/t.co\/40iz99tLAK via @SCMP_News",
    "id" : 778628899664072704,
    "created_at" : "2016-09-21 16:16:10 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 778646434820284416,
  "created_at" : "2016-09-21 17:25:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Department of Finance",
      "screen_name" : "IRLDeptFinance",
      "indices" : [ 3, 18 ],
      "id_str" : "1310401350",
      "id" : 1310401350
    }, {
      "name" : "Eoghan Murphy",
      "screen_name" : "MurphyEoghan",
      "indices" : [ 24, 37 ],
      "id_str" : "109986944",
      "id" : 109986944
    }, {
      "name" : "MOFsg",
      "screen_name" : "MOFsg",
      "indices" : [ 41, 47 ],
      "id_str" : "76578396",
      "id" : 76578396
    }, {
      "name" : "Indranee Rajah",
      "screen_name" : "indraneerajah",
      "indices" : [ 52, 66 ],
      "id_str" : "4024513332",
      "id" : 4024513332
    }, {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 118, 134 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778618405964447745",
  "text" : "RT @IRLDeptFinance: Min @MurphyEoghan w\/ @MOFsg Min @indraneerajah discussing domestic &amp; int'l economic matters - @IrlEmbSingapore https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Eoghan Murphy",
        "screen_name" : "MurphyEoghan",
        "indices" : [ 4, 17 ],
        "id_str" : "109986944",
        "id" : 109986944
      }, {
        "name" : "MOFsg",
        "screen_name" : "MOFsg",
        "indices" : [ 21, 27 ],
        "id_str" : "76578396",
        "id" : 76578396
      }, {
        "name" : "Indranee Rajah",
        "screen_name" : "indraneerajah",
        "indices" : [ 32, 46 ],
        "id_str" : "4024513332",
        "id" : 4024513332
      }, {
        "name" : "Irish Emb Singapore",
        "screen_name" : "IrlEmbSingapore",
        "indices" : [ 98, 114 ],
        "id_str" : "1721598590",
        "id" : 1721598590
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IRLDeptFinance\/status\/778430041273294849\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/I49THdbyaU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cs2J84XWYAAGBhc.jpg",
        "id_str" : "778429989620441088",
        "id" : 778429989620441088,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cs2J84XWYAAGBhc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 920,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1570,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1570,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 521,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/I49THdbyaU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "778430041273294849",
    "text" : "Min @MurphyEoghan w\/ @MOFsg Min @indraneerajah discussing domestic &amp; int'l economic matters - @IrlEmbSingapore https:\/\/t.co\/I49THdbyaU",
    "id" : 778430041273294849,
    "created_at" : "2016-09-21 03:05:59 +0000",
    "user" : {
      "name" : "Department of Finance",
      "screen_name" : "IRLDeptFinance",
      "protected" : false,
      "id_str" : "1310401350",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989149328672509952\/1wtz2avm_normal.jpg",
      "id" : 1310401350,
      "verified" : true
    }
  },
  "id" : 778618405964447745,
  "created_at" : "2016-09-21 15:34:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778522695361294336",
  "text" : "Have you follow people on Twitter that you don\u2019t like because of the need to hear different view points?",
  "id" : 778522695361294336,
  "created_at" : "2016-09-21 09:14:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u718A\u732B\u9972\u517B\u5458",
      "screen_name" : "lnineone",
      "indices" : [ 3, 12 ],
      "id_str" : "26000810",
      "id" : 26000810
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/27SCC\/status\/775924385911681024\/photo\/1",
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/p6Y2YDuTWO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsSjF8mVMAAOHtl.jpg",
      "id_str" : "775924358376140800",
      "id" : 775924358376140800,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsSjF8mVMAAOHtl.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 592,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/p6Y2YDuTWO"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778521106940559360",
  "text" : "RT @lnineone: \u54C8\u54C8 https:\/\/t.co\/p6Y2YDuTWO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/wxt2005.org\/\" rel=\"nofollow\"\u003E\u9992\u5934\u5361\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/27SCC\/status\/775924385911681024\/photo\/1",
        "indices" : [ 3, 26 ],
        "url" : "https:\/\/t.co\/p6Y2YDuTWO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsSjF8mVMAAOHtl.jpg",
        "id_str" : "775924358376140800",
        "id" : 775924358376140800,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsSjF8mVMAAOHtl.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 696,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 696,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 696,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/p6Y2YDuTWO"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775927113182384128",
    "text" : "\u54C8\u54C8 https:\/\/t.co\/p6Y2YDuTWO",
    "id" : 775927113182384128,
    "created_at" : "2016-09-14 05:20:14 +0000",
    "user" : {
      "name" : "\u718A\u732B\u9972\u517B\u5458",
      "screen_name" : "lnineone",
      "protected" : false,
      "id_str" : "26000810",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000411168289\/f0b221305abd53b36cd7e2a6b365f288_normal.jpeg",
      "id" : 26000810,
      "verified" : false
    }
  },
  "id" : 778521106940559360,
  "created_at" : "2016-09-21 09:07:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u7ED9\u6211\u4E00\u74F6\u51B0\u53EF\u4E50",
      "screen_name" : "izhujialiang",
      "indices" : [ 3, 16 ],
      "id_str" : "1013320460",
      "id" : 1013320460
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778521053773570048",
  "text" : "RT @izhujialiang: \u7B2C\u4E00\u6B21\u5403\u7F8E\u5FC3\u6708\u997C\uFF0C\u7B97\u4E86\u4E0B\u5E73\u5747\u4E00\u53E3\u8981\u4E09\u5341\u5757\u94B1\uFF0C\u5413\u5F97\u6211\u591A\u56BC\u4E24\u53E3......",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776368448033161216",
    "text" : "\u7B2C\u4E00\u6B21\u5403\u7F8E\u5FC3\u6708\u997C\uFF0C\u7B97\u4E86\u4E0B\u5E73\u5747\u4E00\u53E3\u8981\u4E09\u5341\u5757\u94B1\uFF0C\u5413\u5F97\u6211\u591A\u56BC\u4E24\u53E3......",
    "id" : 776368448033161216,
    "created_at" : "2016-09-15 10:33:56 +0000",
    "user" : {
      "name" : "\u7ED9\u6211\u4E00\u74F6\u51B0\u53EF\u4E50",
      "screen_name" : "izhujialiang",
      "protected" : false,
      "id_str" : "1013320460",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947731714805084160\/PyXo58B5_normal.jpg",
      "id" : 1013320460,
      "verified" : false
    }
  },
  "id" : 778521053773570048,
  "created_at" : "2016-09-21 09:07:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 24, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778520430109925377",
  "text" : "Are you using eircodes? #Ireland",
  "id" : 778520430109925377,
  "created_at" : "2016-09-21 09:05:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 0, 11 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "778515817122058240",
  "geo" : { },
  "id_str" : "778519641782095872",
  "in_reply_to_user_id" : 4747144932,
  "text" : "@CaroBowler I try to use it. When I received letter from SGP, they put down my ericode.",
  "id" : 778519641782095872,
  "in_reply_to_status_id" : 778515817122058240,
  "created_at" : "2016-09-21 09:02:01 +0000",
  "in_reply_to_screen_name" : "CaroBowler",
  "in_reply_to_user_id_str" : "4747144932",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/emq7GvG3B8",
      "expanded_url" : "https:\/\/www.whoismytd.com",
      "display_url" : "whoismytd.com"
    }, {
      "indices" : [ 134, 157 ],
      "url" : "https:\/\/t.co\/rWd0iVHIER",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/778503175645044736",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778505126847848448",
  "text" : "For Ireland, you need a full address. https:\/\/t.co\/emq7GvG3B8 Would be brilliant if the system can handle Ericodes (Ireland postcode) https:\/\/t.co\/rWd0iVHIER",
  "id" : 778505126847848448,
  "created_at" : "2016-09-21 08:04:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/wUiowm79Vo",
      "expanded_url" : "https:\/\/www.parliament.gov.sg\/whos-my-mp#",
      "display_url" : "parliament.gov.sg\/whos-my-mp#"
    }, {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/SMRESOJH9V",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/778498859165179905",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778503175645044736",
  "text" : "This comes in handy. https:\/\/t.co\/wUiowm79Vo Just pop-in your postcode. https:\/\/t.co\/SMRESOJH9V",
  "id" : 778503175645044736,
  "created_at" : "2016-09-21 07:56:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "vinoaj",
      "screen_name" : "vinoaj",
      "indices" : [ 3, 10 ],
      "id_str" : "14503314",
      "id" : 14503314
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/zZJFRNpUV9",
      "expanded_url" : "http:\/\/vnjv.co\/2clDfWv",
      "display_url" : "vnjv.co\/2clDfWv"
    } ]
  },
  "geo" : { },
  "id_str" : "778500804147867648",
  "text" : "RT @vinoaj: Opera\u2019s desktop browser with a built-in VPN is now available to all https:\/\/t.co\/zZJFRNpUV9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 91 ],
        "url" : "https:\/\/t.co\/zZJFRNpUV9",
        "expanded_url" : "http:\/\/vnjv.co\/2clDfWv",
        "display_url" : "vnjv.co\/2clDfWv"
      } ]
    },
    "geo" : { },
    "id_str" : "778203306665644032",
    "text" : "Opera\u2019s desktop browser with a built-in VPN is now available to all https:\/\/t.co\/zZJFRNpUV9",
    "id" : 778203306665644032,
    "created_at" : "2016-09-20 12:05:01 +0000",
    "user" : {
      "name" : "vinoaj",
      "screen_name" : "vinoaj",
      "protected" : false,
      "id_str" : "14503314",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1192346527\/Vinoaj_Vijeyakumaar_-_Headshot_-_Medium_normal.jpg",
      "id" : 14503314,
      "verified" : false
    }
  },
  "id" : 778500804147867648,
  "created_at" : "2016-09-21 07:47:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech SG",
      "screen_name" : "IDAsg",
      "indices" : [ 3, 9 ],
      "id_str" : "707512234658824192",
      "id" : 707512234658824192
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "programming",
      "indices" : [ 127, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/wyW6hgyiQc",
      "expanded_url" : "https:\/\/gbuy.gds-gov.tech\/",
      "display_url" : "gbuy.gds-gov.tech"
    } ]
  },
  "geo" : { },
  "id_str" : "778500498689261568",
  "text" : "RT @IDAsg: Programmers- do you wish to bid for microassignments from the Singapore Government? More at https:\/\/t.co\/wyW6hgyiQc #programming\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "programming",
        "indices" : [ 116, 128 ]
      }, {
        "text" : "coding",
        "indices" : [ 129, 136 ]
      } ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/wyW6hgyiQc",
        "expanded_url" : "https:\/\/gbuy.gds-gov.tech\/",
        "display_url" : "gbuy.gds-gov.tech"
      }, {
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/DUSuYuefru",
        "expanded_url" : "https:\/\/twitter.com\/linscorpz\/status\/778454285788909569",
        "display_url" : "twitter.com\/linscorpz\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "778493865502269445",
    "text" : "Programmers- do you wish to bid for microassignments from the Singapore Government? More at https:\/\/t.co\/wyW6hgyiQc #programming #coding https:\/\/t.co\/DUSuYuefru",
    "id" : 778493865502269445,
    "created_at" : "2016-09-21 07:19:35 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 778500498689261568,
  "created_at" : "2016-09-21 07:45:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    }, {
      "name" : "ABC News",
      "screen_name" : "abcnews",
      "indices" : [ 121, 129 ],
      "id_str" : "2768501",
      "id" : 2768501
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/9PBjt2vPpi",
      "expanded_url" : "http:\/\/www.abc.net.au\/news\/2016-09-19\/singapore-military-exercise-fires-up-regional-economy\/7853160?pfmredir=sm",
      "display_url" : "abc.net.au\/news\/2016-09-1\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778498996918779904",
  "text" : "RT @yapphenghui: Singapore military exercise fires up regional economy in central Queensland https:\/\/t.co\/9PBjt2vPpi via @abcnews",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "ABC News",
        "screen_name" : "abcnews",
        "indices" : [ 104, 112 ],
        "id_str" : "2768501",
        "id" : 2768501
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/9PBjt2vPpi",
        "expanded_url" : "http:\/\/www.abc.net.au\/news\/2016-09-19\/singapore-military-exercise-fires-up-regional-economy\/7853160?pfmredir=sm",
        "display_url" : "abc.net.au\/news\/2016-09-1\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "778456554492178432",
    "text" : "Singapore military exercise fires up regional economy in central Queensland https:\/\/t.co\/9PBjt2vPpi via @abcnews",
    "id" : 778456554492178432,
    "created_at" : "2016-09-21 04:51:20 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 778498996918779904,
  "created_at" : "2016-09-21 07:39:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778498859165179905",
  "text" : "Re-tweet this if you do not know whose your elected representative in the area you living.",
  "id" : 778498859165179905,
  "created_at" : "2016-09-21 07:39:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Beeston",
      "screen_name" : "beeston",
      "indices" : [ 3, 11 ],
      "id_str" : "6112042",
      "id" : 6112042
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/7TwbhnU9cY",
      "expanded_url" : "https:\/\/twitter.com\/guardian\/status\/778488496730308608",
      "display_url" : "twitter.com\/guardian\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778498136218165248",
  "text" : "RT @beeston: 21st century version of 'the dog ate my homework' https:\/\/t.co\/7TwbhnU9cY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/7TwbhnU9cY",
        "expanded_url" : "https:\/\/twitter.com\/guardian\/status\/778488496730308608",
        "display_url" : "twitter.com\/guardian\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "778493948176326656",
    "text" : "21st century version of 'the dog ate my homework' https:\/\/t.co\/7TwbhnU9cY",
    "id" : 778493948176326656,
    "created_at" : "2016-09-21 07:19:55 +0000",
    "user" : {
      "name" : "Jonathan Beeston",
      "screen_name" : "beeston",
      "protected" : false,
      "id_str" : "6112042",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/746342830612807680\/iIzxHmyB_normal.jpg",
      "id" : 6112042,
      "verified" : false
    }
  },
  "id" : 778498136218165248,
  "created_at" : "2016-09-21 07:36:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Saoire O' Brien",
      "screen_name" : "saoireobrien",
      "indices" : [ 3, 16 ],
      "id_str" : "37536288",
      "id" : 37536288
    }, {
      "name" : "Marketing Land",
      "screen_name" : "Marketingland",
      "indices" : [ 122, 136 ],
      "id_str" : "12553672",
      "id" : 12553672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/1qSEMSfK9j",
      "expanded_url" : "http:\/\/mklnd.com\/2cGzfOh",
      "display_url" : "mklnd.com\/2cGzfOh"
    } ]
  },
  "geo" : { },
  "id_str" : "778497944446205952",
  "text" : "RT @saoireobrien: Facebook's new retail ads show if a product's available at a physical store https:\/\/t.co\/1qSEMSfK9j via @marketingland",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Marketing Land",
        "screen_name" : "Marketingland",
        "indices" : [ 104, 118 ],
        "id_str" : "12553672",
        "id" : 12553672
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/1qSEMSfK9j",
        "expanded_url" : "http:\/\/mklnd.com\/2cGzfOh",
        "display_url" : "mklnd.com\/2cGzfOh"
      } ]
    },
    "geo" : { },
    "id_str" : "778496714093260800",
    "text" : "Facebook's new retail ads show if a product's available at a physical store https:\/\/t.co\/1qSEMSfK9j via @marketingland",
    "id" : 778496714093260800,
    "created_at" : "2016-09-21 07:30:55 +0000",
    "user" : {
      "name" : "Saoire O' Brien",
      "screen_name" : "saoireobrien",
      "protected" : false,
      "id_str" : "37536288",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/883348780212527104\/fPutZCeM_normal.jpg",
      "id" : 37536288,
      "verified" : false
    }
  },
  "id" : 778497944446205952,
  "created_at" : "2016-09-21 07:35:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/Et2Vnol5Xw",
      "expanded_url" : "http:\/\/www.msn.com\/en-ie\/news\/world\/unarmed-black-man-with-his-hands-in-the-air-tasered-and-shot-dead-by-police\/ar-BBwnRWC?li=BBr5KbJ&ocid=mailsignoutmd",
      "display_url" : "msn.com\/en-ie\/news\/wor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778353734325788672",
  "text" : "Story of the day should be this https:\/\/t.co\/Et2Vnol5Xw",
  "id" : 778353734325788672,
  "created_at" : "2016-09-20 22:02:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/J2MM7SG8Wm",
      "expanded_url" : "https:\/\/shar.es\/1xsERW",
      "display_url" : "shar.es\/1xsERW"
    } ]
  },
  "geo" : { },
  "id_str" : "778347524222554112",
  "text" : "Girl, 6, drowns in hotel pool while dad looks at phone https:\/\/t.co\/J2MM7SG8Wm",
  "id" : 778347524222554112,
  "created_at" : "2016-09-20 21:38:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/RQkDNzH0Rw",
      "expanded_url" : "https:\/\/www.cmpb.gov.sg\/cmpb\/parents-guide\/what-you-need-to-do\/exit-permit-and-bond\/",
      "display_url" : "cmpb.gov.sg\/cmpb\/parents-g\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "778320666982580226",
  "text" : "If we got idle cash of SGD 75000 (EUR49229.29), we will not be leaving home and work here. https:\/\/t.co\/RQkDNzH0Rw",
  "id" : 778320666982580226,
  "created_at" : "2016-09-20 19:51:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "George Soros",
      "screen_name" : "georgesoros",
      "indices" : [ 3, 15 ],
      "id_str" : "23125257",
      "id" : 23125257
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/KtYb2xyQe4",
      "expanded_url" : "http:\/\/trib.al\/V7TyyUV",
      "display_url" : "trib.al\/V7TyyUV"
    } ]
  },
  "geo" : { },
  "id_str" : "778316395285184512",
  "text" : "RT @georgesoros: Why I'm investing $500 million in migrants: https:\/\/t.co\/KtYb2xyQe4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/KtYb2xyQe4",
        "expanded_url" : "http:\/\/trib.al\/V7TyyUV",
        "display_url" : "trib.al\/V7TyyUV"
      } ]
    },
    "geo" : { },
    "id_str" : "778237678387138561",
    "text" : "Why I'm investing $500 million in migrants: https:\/\/t.co\/KtYb2xyQe4",
    "id" : 778237678387138561,
    "created_at" : "2016-09-20 14:21:36 +0000",
    "user" : {
      "name" : "George Soros",
      "screen_name" : "georgesoros",
      "protected" : false,
      "id_str" : "23125257",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2576794304\/3tt77o59jouq6b2ti7e5_normal.jpeg",
      "id" : 23125257,
      "verified" : true
    }
  },
  "id" : 778316395285184512,
  "created_at" : "2016-09-20 19:34:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/778121486292946944\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/xqYN7j8rku",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsxxK9GWEAAr08N.jpg",
      "id_str" : "778121268642058240",
      "id" : 778121268642058240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsxxK9GWEAAr08N.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 309,
        "resize" : "fit",
        "w" : 375
      }, {
        "h" : 309,
        "resize" : "fit",
        "w" : 375
      }, {
        "h" : 309,
        "resize" : "fit",
        "w" : 375
      }, {
        "h" : 309,
        "resize" : "fit",
        "w" : 375
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xqYN7j8rku"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "778121486292946944",
  "text" : "https:\/\/t.co\/xqYN7j8rku",
  "id" : 778121486292946944,
  "created_at" : "2016-09-20 06:39:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 31 ],
      "url" : "https:\/\/t.co\/OS0saDqhVW",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/777927929867083776",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777976315655909376",
  "text" : "No..... https:\/\/t.co\/OS0saDqhVW",
  "id" : 777976315655909376,
  "created_at" : "2016-09-19 21:03:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victoria Lynden",
      "screen_name" : "VictoriaLynden",
      "indices" : [ 3, 18 ],
      "id_str" : "190317345",
      "id" : 190317345
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "immigrant",
      "indices" : [ 28, 38 ]
    }, {
      "text" : "entrepreneur",
      "indices" : [ 120, 133 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/xffSNfNAXo",
      "expanded_url" : "http:\/\/cnb.cx\/2aareNV",
      "display_url" : "cnb.cx\/2aareNV"
    } ]
  },
  "geo" : { },
  "id_str" : "777930087123550208",
  "text" : "RT @VictoriaLynden: How one #immigrant started a $78 million company from his parents' basement https:\/\/t.co\/xffSNfNAXo #entrepreneur",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "immigrant",
        "indices" : [ 8, 18 ]
      }, {
        "text" : "entrepreneur",
        "indices" : [ 100, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/xffSNfNAXo",
        "expanded_url" : "http:\/\/cnb.cx\/2aareNV",
        "display_url" : "cnb.cx\/2aareNV"
      } ]
    },
    "geo" : { },
    "id_str" : "777928183551258624",
    "text" : "How one #immigrant started a $78 million company from his parents' basement https:\/\/t.co\/xffSNfNAXo #entrepreneur",
    "id" : 777928183551258624,
    "created_at" : "2016-09-19 17:51:46 +0000",
    "user" : {
      "name" : "Victoria Lynden",
      "screen_name" : "VictoriaLynden",
      "protected" : false,
      "id_str" : "190317345",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/598151044770574337\/MJQeXuh__normal.jpg",
      "id" : 190317345,
      "verified" : false
    }
  },
  "id" : 777930087123550208,
  "created_at" : "2016-09-19 17:59:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/YAg4HZsXN2",
      "expanded_url" : "https:\/\/sg.style.yahoo.com\/sound-music-actress-charmian-carr-dies-73-201229309.html",
      "display_url" : "sg.style.yahoo.com\/sound-music-ac\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777807384475471873",
  "text" : "'Sound of Music' actress Charmian Carr dies at 73 https:\/\/t.co\/YAg4HZsXN2",
  "id" : 777807384475471873,
  "created_at" : "2016-09-19 09:51:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Predict Conference",
      "screen_name" : "predictconf",
      "indices" : [ 30, 42 ],
      "id_str" : "2991904109",
      "id" : 2991904109
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777797548165754881",
  "text" : "Need to sort out transport to @predictconf due to industrial action by Dublin bus driver on 5th Oct. No one can predict it.",
  "id" : 777797548165754881,
  "created_at" : "2016-09-19 09:12:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "harishpillay",
      "screen_name" : "harishpillay",
      "indices" : [ 3, 16 ],
      "id_str" : "12486322",
      "id" : 12486322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777796525820932096",
  "text" : "RT @harishpillay: Did not know that Timor Leste uses US$",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777755789578514433",
    "text" : "Did not know that Timor Leste uses US$",
    "id" : 777755789578514433,
    "created_at" : "2016-09-19 06:26:44 +0000",
    "user" : {
      "name" : "harishpillay",
      "screen_name" : "harishpillay",
      "protected" : false,
      "id_str" : "12486322",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/99978402\/HarishPillaycloseupshot_normal.jpg",
      "id" : 12486322,
      "verified" : false
    }
  },
  "id" : 777796525820932096,
  "created_at" : "2016-09-19 09:08:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "melissa mannion",
      "screen_name" : "lostinskylines",
      "indices" : [ 3, 18 ],
      "id_str" : "16669529",
      "id" : 16669529
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777796426352975872",
  "text" : "RT @lostinskylines: Very odd listening to the legend of the Mayo curse on BBC Radio 4 this morning. Good to hear them covering the importan\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777761526883184640",
    "text" : "Very odd listening to the legend of the Mayo curse on BBC Radio 4 this morning. Good to hear them covering the important things in Ireland \uD83D\uDE10",
    "id" : 777761526883184640,
    "created_at" : "2016-09-19 06:49:32 +0000",
    "user" : {
      "name" : "melissa mannion",
      "screen_name" : "lostinskylines",
      "protected" : false,
      "id_str" : "16669529",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024787513192398854\/BF_TxH_2_normal.jpg",
      "id" : 16669529,
      "verified" : false
    }
  },
  "id" : 777796426352975872,
  "created_at" : "2016-09-19 09:08:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Drayton Bird",
      "screen_name" : "DraytonBird",
      "indices" : [ 3, 15 ],
      "id_str" : "16580016",
      "id" : 16580016
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777796061570146304",
  "text" : "RT @DraytonBird: Words to ban: Innovative, disruptive, leverage, leadership, align, empower, millennial, proactive, paradigm, compliant, pa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776849793998057472",
    "text" : "Words to ban: Innovative, disruptive, leverage, leadership, align, empower, millennial, proactive, paradigm, compliant, passionate, agile.",
    "id" : 776849793998057472,
    "created_at" : "2016-09-16 18:26:38 +0000",
    "user" : {
      "name" : "Drayton Bird",
      "screen_name" : "DraytonBird",
      "protected" : false,
      "id_str" : "16580016",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/627642547\/Drayton_Bird_120x120_normal.jpg",
      "id" : 16580016,
      "verified" : false
    }
  },
  "id" : 777796061570146304,
  "created_at" : "2016-09-19 09:06:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 3, 15 ],
      "id_str" : "19793936",
      "id" : 19793936
    }, {
      "name" : "Ireland \/ Bridget",
      "screen_name" : "ireland",
      "indices" : [ 74, 82 ],
      "id_str" : "1143551",
      "id" : 1143551
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777795754773667840",
  "text" : "RT @gianniponzi: Oh no :-(  Lets hope someone can help track it down \/\/cc @ireland Not the way you want an adventure to start :-\/ https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ireland \/ Bridget",
        "screen_name" : "ireland",
        "indices" : [ 57, 65 ],
        "id_str" : "1143551",
        "id" : 1143551
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/frZQpn7F8i",
        "expanded_url" : "https:\/\/twitter.com\/Taraustralis\/status\/777771514724769792",
        "display_url" : "twitter.com\/Taraustralis\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "777783388384133120",
    "text" : "Oh no :-(  Lets hope someone can help track it down \/\/cc @ireland Not the way you want an adventure to start :-\/ https:\/\/t.co\/frZQpn7F8i",
    "id" : 777783388384133120,
    "created_at" : "2016-09-19 08:16:24 +0000",
    "user" : {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "protected" : false,
      "id_str" : "19793936",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034160433949761536\/GdlrLPNt_normal.jpg",
      "id" : 19793936,
      "verified" : false
    }
  },
  "id" : 777795754773667840,
  "created_at" : "2016-09-19 09:05:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777795188769058816",
  "text" : "Anyone want to meet up for coffee this morning?",
  "id" : 777795188769058816,
  "created_at" : "2016-09-19 09:03:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Classic FM",
      "screen_name" : "ClassicFM",
      "indices" : [ 70, 80 ],
      "id_str" : "64714144",
      "id" : 64714144
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/bMOh0t475Q",
      "expanded_url" : "http:\/\/www.classicfm.com\/music-news\/videos\/oldest-song-melody\/",
      "display_url" : "classicfm.com\/music-news\/vid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777660361113759752",
  "text" : "This is the oldest melody in existence \u2013 and it\u2019s utterly enchanting  @classicfm https:\/\/t.co\/bMOh0t475Q",
  "id" : 777660361113759752,
  "created_at" : "2016-09-19 00:07:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/B54BxXv1QG",
      "expanded_url" : "https:\/\/www.eventbrite.co.uk\/e\/in-conversation-with-professor-andre-geim-tickets-27349985543",
      "display_url" : "eventbrite.co.uk\/e\/in-conversat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777620979786911745",
  "text" : "Check out \"In Conversation with Professor Andre Geim\" https:\/\/t.co\/B54BxXv1QG",
  "id" : 777620979786911745,
  "created_at" : "2016-09-18 21:31:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/777609527025950720\/photo\/1",
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/WDD8IriS1y",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsqfkjAXgAQGy49.jpg",
      "id_str" : "777609335895719940",
      "id" : 777609335895719940,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsqfkjAXgAQGy49.jpg",
      "sizes" : [ {
        "h" : 16,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 23,
        "resize" : "fit",
        "w" : 989
      }, {
        "h" : 23,
        "resize" : "crop",
        "w" : 23
      }, {
        "h" : 23,
        "resize" : "fit",
        "w" : 989
      }, {
        "h" : 23,
        "resize" : "fit",
        "w" : 989
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WDD8IriS1y"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777609527025950720",
  "text" : "GitHub is my Men's sheds. https:\/\/t.co\/WDD8IriS1y",
  "id" : 777609527025950720,
  "created_at" : "2016-09-18 20:45:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Wiseman",
      "screen_name" : "RichardWiseman",
      "indices" : [ 3, 18 ],
      "id_str" : "19512493",
      "id" : 19512493
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777608722113818630",
  "text" : "RT @RichardWiseman: Idea: children currently assessed on how hard they try &amp; achievement. How about adding how much they help other childre\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777498357254201348",
    "text" : "Idea: children currently assessed on how hard they try &amp; achievement. How about adding how much they help other children?",
    "id" : 777498357254201348,
    "created_at" : "2016-09-18 13:23:48 +0000",
    "user" : {
      "name" : "Richard Wiseman",
      "screen_name" : "RichardWiseman",
      "protected" : false,
      "id_str" : "19512493",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3225534671\/ef59c3b397d28dacb74034b47d8cd9e2_normal.jpeg",
      "id" : 19512493,
      "verified" : true
    }
  },
  "id" : 777608722113818630,
  "created_at" : "2016-09-18 20:42:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777551369939120128",
  "text" : "RT @SarcasmMother: Funerals are like family reunions, just minus one person.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773137011473584128",
    "text" : "Funerals are like family reunions, just minus one person.",
    "id" : 773137011473584128,
    "created_at" : "2016-09-06 12:33:22 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 777551369939120128,
  "created_at" : "2016-09-18 16:54:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "indices" : [ 3, 17 ],
      "id_str" : "4824205889",
      "id" : 4824205889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777549770898432000",
  "text" : "RT @SarcasmMother: One day an iPhone is going to explode, and Samsung users are going to be like, \u201CSamsung has had this feature for years\u201D.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777141363817275392",
    "text" : "One day an iPhone is going to explode, and Samsung users are going to be like, \u201CSamsung has had this feature for years\u201D.",
    "id" : 777141363817275392,
    "created_at" : "2016-09-17 13:45:14 +0000",
    "user" : {
      "name" : "Mother Of Sarcasm",
      "screen_name" : "SarcasmMother",
      "protected" : false,
      "id_str" : "4824205889",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737649673486274561\/wQV7CQ-R_normal.jpg",
      "id" : 4824205889,
      "verified" : false
    }
  },
  "id" : 777549770898432000,
  "created_at" : "2016-09-18 16:48:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amanda Webb \uD83D\uDC69\u200D\uD83D\uDCBB",
      "screen_name" : "Spiderworking",
      "indices" : [ 3, 17 ],
      "id_str" : "59495184",
      "id" : 59495184
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/lhODIDluDe",
      "expanded_url" : "http:\/\/www.spiderworking.com\/blog\/2012\/11\/26\/add-twitter-cards-to-your-wordpress-org-blog-with-yoast-cool-tool\/",
      "display_url" : "spiderworking.com\/blog\/2012\/11\/2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777532898199080960",
  "text" : "RT @Spiderworking: How To Get Thumbnail Images To Show On Twitter Links - Add Twitter Cards https:\/\/t.co\/lhODIDluDe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/coschedule.com\" rel=\"nofollow\"\u003ECoSchedule\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/lhODIDluDe",
        "expanded_url" : "http:\/\/www.spiderworking.com\/blog\/2012\/11\/26\/add-twitter-cards-to-your-wordpress-org-blog-with-yoast-cool-tool\/",
        "display_url" : "spiderworking.com\/blog\/2012\/11\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776853567869181953",
    "text" : "How To Get Thumbnail Images To Show On Twitter Links - Add Twitter Cards https:\/\/t.co\/lhODIDluDe",
    "id" : 776853567869181953,
    "created_at" : "2016-09-16 18:41:38 +0000",
    "user" : {
      "name" : "Amanda Webb \uD83D\uDC69\u200D\uD83D\uDCBB",
      "screen_name" : "Spiderworking",
      "protected" : false,
      "id_str" : "59495184",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/906213043549425665\/hr8ep3ra_normal.jpg",
      "id" : 59495184,
      "verified" : false
    }
  },
  "id" : 777532898199080960,
  "created_at" : "2016-09-18 15:41:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conan O'Brien",
      "screen_name" : "ConanOBrien",
      "indices" : [ 3, 15 ],
      "id_str" : "115485051",
      "id" : 115485051
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777532408509980673",
  "text" : "RT @ConanOBrien: Right before I die, if my life flashes before my eyes, I hope there aren't 30 second ads before each section.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776821809282965505",
    "text" : "Right before I die, if my life flashes before my eyes, I hope there aren't 30 second ads before each section.",
    "id" : 776821809282965505,
    "created_at" : "2016-09-16 16:35:26 +0000",
    "user" : {
      "name" : "Conan O'Brien",
      "screen_name" : "ConanOBrien",
      "protected" : false,
      "id_str" : "115485051",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/730612231021322240\/Rl0_QYhL_normal.jpg",
      "id" : 115485051,
      "verified" : true
    }
  },
  "id" : 777532408509980673,
  "created_at" : "2016-09-18 15:39:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DatSci Awards",
      "screen_name" : "DatSciAwards",
      "indices" : [ 3, 16 ],
      "id_str" : "713066829288177664",
      "id" : 713066829288177664
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DatSciAwards",
      "indices" : [ 46, 59 ]
    }, {
      "text" : "Data",
      "indices" : [ 117, 122 ]
    }, {
      "text" : "DataScience",
      "indices" : [ 123, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777532145929715713",
  "text" : "RT @DatSciAwards: Want to win a ticket to the #DatSciAwards? Follow &amp; Retweet by Tuesday to be in with a chance! #Data #DataScience https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hubspot.com\/\" rel=\"nofollow\"\u003EHubSpot\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DatSciAwards\/status\/777522574020468736\/photo\/1",
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/Y1mo2CZwye",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CspQqN_WIAEzsb5.jpg",
        "id_str" : "777522571914911745",
        "id" : 777522571914911745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CspQqN_WIAEzsb5.jpg",
        "sizes" : [ {
          "h" : 274,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 483,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 589,
          "resize" : "fit",
          "w" : 1462
        }, {
          "h" : 589,
          "resize" : "fit",
          "w" : 1462
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Y1mo2CZwye"
      } ],
      "hashtags" : [ {
        "text" : "DatSciAwards",
        "indices" : [ 28, 41 ]
      }, {
        "text" : "Data",
        "indices" : [ 99, 104 ]
      }, {
        "text" : "DataScience",
        "indices" : [ 105, 117 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777522574020468736",
    "text" : "Want to win a ticket to the #DatSciAwards? Follow &amp; Retweet by Tuesday to be in with a chance! #Data #DataScience https:\/\/t.co\/Y1mo2CZwye",
    "id" : 777522574020468736,
    "created_at" : "2016-09-18 15:00:01 +0000",
    "user" : {
      "name" : "DatSci Awards",
      "screen_name" : "DatSciAwards",
      "protected" : false,
      "id_str" : "713066829288177664",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966261120095932417\/Kh6SqlKH_normal.jpg",
      "id" : 713066829288177664,
      "verified" : false
    }
  },
  "id" : 777532145929715713,
  "created_at" : "2016-09-18 15:38:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777531908007886848",
  "text" : "Anyone in Dublin not watching the match now?",
  "id" : 777531908007886848,
  "created_at" : "2016-09-18 15:37:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/Eb2E6bmmmW",
      "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/776121907066339328",
      "display_url" : "twitter.com\/ThePracticalDe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777482100823957504",
  "text" : "Checkout the Kanban board-like project management features on this gif https:\/\/t.co\/Eb2E6bmmmW",
  "id" : 777482100823957504,
  "created_at" : "2016-09-18 12:19:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/Rpr4GsisvP",
      "expanded_url" : "http:\/\/petapixel.com\/2016\/09\/14\/20-composition-techniques-will-improve-photos\/",
      "display_url" : "petapixel.com\/2016\/09\/14\/20-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777447623343022080",
  "text" : "20 Composition Techniques That Will Improve Your Photos https:\/\/t.co\/Rpr4GsisvP",
  "id" : 777447623343022080,
  "created_at" : "2016-09-18 10:02:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Koenfucius",
      "screen_name" : "koenfucius",
      "indices" : [ 3, 14 ],
      "id_str" : "22464371",
      "id" : 22464371
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/ynME2RO13W",
      "expanded_url" : "http:\/\/econ.st\/2ctNAjj",
      "display_url" : "econ.st\/2ctNAjj"
    } ]
  },
  "geo" : { },
  "id_str" : "777409741060440064",
  "text" : "RT @koenfucius: \"Historically the public has been keener to fetishise the NHS than pay for it\" https:\/\/t.co\/ynME2RO13W https:\/\/t.co\/mubtAcK\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/koenfucius\/status\/774987663795490817\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/mubtAcKoYZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsFPLB9WEAAmdLI.jpg",
        "id_str" : "774987661807325184",
        "id" : 774987661807325184,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsFPLB9WEAAmdLI.jpg",
        "sizes" : [ {
          "h" : 598,
          "resize" : "fit",
          "w" : 580
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 580
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 580
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 580
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mubtAcKoYZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/ynME2RO13W",
        "expanded_url" : "http:\/\/econ.st\/2ctNAjj",
        "display_url" : "econ.st\/2ctNAjj"
      } ]
    },
    "geo" : { },
    "id_str" : "774987663795490817",
    "text" : "\"Historically the public has been keener to fetishise the NHS than pay for it\" https:\/\/t.co\/ynME2RO13W https:\/\/t.co\/mubtAcKoYZ",
    "id" : 774987663795490817,
    "created_at" : "2016-09-11 15:07:12 +0000",
    "user" : {
      "name" : "Koenfucius",
      "screen_name" : "koenfucius",
      "protected" : false,
      "id_str" : "22464371",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/553494434315522048\/z0a9Z6Xx_normal.jpeg",
      "id" : 22464371,
      "verified" : false
    }
  },
  "id" : 777409741060440064,
  "created_at" : "2016-09-18 07:31:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AJ Joshi \u24CB",
      "screen_name" : "AJ",
      "indices" : [ 3, 6 ],
      "id_str" : "73956951",
      "id" : 73956951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/ck6gby8gga",
      "expanded_url" : "https:\/\/twitter.com\/dlewis89\/status\/776612073287487489\/video\/1",
      "display_url" : "twitter.com\/dlewis89\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777402981172019200",
  "text" : "RT @AJ: The new iPhone 7 advert is out https:\/\/t.co\/ck6gby8gga",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 31, 54 ],
        "url" : "https:\/\/t.co\/ck6gby8gga",
        "expanded_url" : "https:\/\/twitter.com\/dlewis89\/status\/776612073287487489\/video\/1",
        "display_url" : "twitter.com\/dlewis89\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776684576076857344",
    "text" : "The new iPhone 7 advert is out https:\/\/t.co\/ck6gby8gga",
    "id" : 776684576076857344,
    "created_at" : "2016-09-16 07:30:07 +0000",
    "user" : {
      "name" : "AJ Joshi \u24CB",
      "screen_name" : "AJ",
      "protected" : false,
      "id_str" : "73956951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012797956007833601\/_BJxZrSG_normal.jpg",
      "id" : 73956951,
      "verified" : true
    }
  },
  "id" : 777402981172019200,
  "created_at" : "2016-09-18 07:04:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Weiner",
      "screen_name" : "jeffweiner",
      "indices" : [ 3, 14 ],
      "id_str" : "20348377",
      "id" : 20348377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777390834958368768",
  "text" : "RT @jeffweiner: Met with Singaporean gov't to talk skills and jobs. Rarely see systems thinking &amp; planning precision at this scale, public\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776709203314429953",
    "text" : "Met with Singaporean gov't to talk skills and jobs. Rarely see systems thinking &amp; planning precision at this scale, public or private sector",
    "id" : 776709203314429953,
    "created_at" : "2016-09-16 09:07:59 +0000",
    "user" : {
      "name" : "Jeff Weiner",
      "screen_name" : "jeffweiner",
      "protected" : false,
      "id_str" : "20348377",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/500480798886084611\/P5XUPlTJ_normal.jpeg",
      "id" : 20348377,
      "verified" : true
    }
  },
  "id" : 777390834958368768,
  "created_at" : "2016-09-18 06:16:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777274544310607872",
  "text" : "What is your preferred Python environment?",
  "id" : 777274544310607872,
  "created_at" : "2016-09-17 22:34:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/yoYMpWQ1iZ",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/777241831566368768",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777273736944812033",
  "text" : "Few hours later...setting up with Command Line is not something I would attempt again. https:\/\/t.co\/yoYMpWQ1iZ",
  "id" : 777273736944812033,
  "created_at" : "2016-09-17 22:31:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/777241831566368768\/photo\/1",
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/52PgG0I9y5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CslQWdLWEAE7pHn.jpg",
      "id_str" : "777240757417676801",
      "id" : 777240757417676801,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CslQWdLWEAE7pHn.jpg",
      "sizes" : [ {
        "h" : 343,
        "resize" : "fit",
        "w" : 677
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 343,
        "resize" : "fit",
        "w" : 677
      }, {
        "h" : 343,
        "resize" : "fit",
        "w" : 677
      }, {
        "h" : 343,
        "resize" : "fit",
        "w" : 677
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/52PgG0I9y5"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777241831566368768",
  "text" : "setup a machine learning environment with Command-Line is my idea of a relaxing evening weekend https:\/\/t.co\/52PgG0I9y5",
  "id" : 777241831566368768,
  "created_at" : "2016-09-17 20:24:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777226887747735553",
  "text" : "@bharatidalela not here in Dublin especially towards the evening.",
  "id" : 777226887747735553,
  "created_at" : "2016-09-17 19:25:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 3, 12 ],
      "id_str" : "136690988",
      "id" : 136690988
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "gaa",
      "indices" : [ 61, 65 ]
    }, {
      "text" : "towinjustonce",
      "indices" : [ 113, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777212454078181376",
  "text" : "RT @ailieirv: I need two tickets for a very sad real genuine #gaa fan. I have nothing to offer but my gratitude. #towinjustonce https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ailieirv\/status\/777203671054618624\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/IfzN3N8O17",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/CskuBMqWgAAhtlr.jpg",
        "id_str" : "777203008811728896",
        "id" : 777203008811728896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/CskuBMqWgAAhtlr.jpg",
        "sizes" : [ {
          "h" : 286,
          "resize" : "fit",
          "w" : 498
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 286,
          "resize" : "fit",
          "w" : 498
        }, {
          "h" : 286,
          "resize" : "fit",
          "w" : 498
        }, {
          "h" : 286,
          "resize" : "fit",
          "w" : 498
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IfzN3N8O17"
      } ],
      "hashtags" : [ {
        "text" : "gaa",
        "indices" : [ 47, 51 ]
      }, {
        "text" : "towinjustonce",
        "indices" : [ 99, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777203671054618624",
    "text" : "I need two tickets for a very sad real genuine #gaa fan. I have nothing to offer but my gratitude. #towinjustonce https:\/\/t.co\/IfzN3N8O17",
    "id" : 777203671054618624,
    "created_at" : "2016-09-17 17:52:49 +0000",
    "user" : {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "protected" : false,
      "id_str" : "136690988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528642221956796416\/WDp_wLcX_normal.jpeg",
      "id" : 136690988,
      "verified" : false
    }
  },
  "id" : 777212454078181376,
  "created_at" : "2016-09-17 18:27:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777194738189500416",
  "text" : "Hot water supply being restored after 3 days of outage!",
  "id" : 777194738189500416,
  "created_at" : "2016-09-17 17:17:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/777191982611460096\/photo\/1",
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/fogrZePzHe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cskj_PcXgAAxmYl.jpg",
      "id_str" : "777191980082364416",
      "id" : 777191980082364416,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cskj_PcXgAAxmYl.jpg",
      "sizes" : [ {
        "h" : 668,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 444,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 668,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 668,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/fogrZePzHe"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777193742587625472",
  "text" : "RT @rshotton: Danger that many mobile designers overestimate users knowledge of symbols\r\rFrom Head in the Cloud https:\/\/t.co\/fogrZePzHe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/777191982611460096\/photo\/1",
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/fogrZePzHe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cskj_PcXgAAxmYl.jpg",
        "id_str" : "777191980082364416",
        "id" : 777191980082364416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cskj_PcXgAAxmYl.jpg",
        "sizes" : [ {
          "h" : 668,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 444,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 668,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 668,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fogrZePzHe"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "777191982611460096",
    "text" : "Danger that many mobile designers overestimate users knowledge of symbols\r\rFrom Head in the Cloud https:\/\/t.co\/fogrZePzHe",
    "id" : 777191982611460096,
    "created_at" : "2016-09-17 17:06:22 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 777193742587625472,
  "created_at" : "2016-09-17 17:13:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777089313796399104",
  "text" : "Thought I could run a regression model with Python before I dash off to run an errand...",
  "id" : 777089313796399104,
  "created_at" : "2016-09-17 10:18:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/WvaNOHdPgm",
      "expanded_url" : "https:\/\/twitter.com\/SimonNRicketts\/status\/776771057588006912",
      "display_url" : "twitter.com\/SimonNRicketts\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777088682096484352",
  "text" : "\uD83D\uDE02 https:\/\/t.co\/WvaNOHdPgm",
  "id" : 777088682096484352,
  "created_at" : "2016-09-17 10:15:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/Xvm0Cbb4rS",
      "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/777057505579065344",
      "display_url" : "twitter.com\/lisaocarroll\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "777062391007830016",
  "text" : "The authority should have the power to confiscate the phone on the spot. https:\/\/t.co\/Xvm0Cbb4rS",
  "id" : 777062391007830016,
  "created_at" : "2016-09-17 08:31:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Saba Hamedy",
      "screen_name" : "saba_h",
      "indices" : [ 3, 10 ],
      "id_str" : "181419468",
      "id" : 181419468
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "777042531423031296",
  "text" : "RT @saba_h: Pilot gave a Samsung warning on my flight.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776637065454718976",
    "text" : "Pilot gave a Samsung warning on my flight.",
    "id" : 776637065454718976,
    "created_at" : "2016-09-16 04:21:20 +0000",
    "user" : {
      "name" : "Saba Hamedy",
      "screen_name" : "saba_h",
      "protected" : false,
      "id_str" : "181419468",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040076080479002624\/cx7gGO1P_normal.jpg",
      "id" : 181419468,
      "verified" : true
    }
  },
  "id" : 777042531423031296,
  "created_at" : "2016-09-17 07:12:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ST Sports Desk",
      "screen_name" : "STsportsdesk",
      "indices" : [ 3, 16 ],
      "id_str" : "308968000",
      "id" : 308968000
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/STsportsdesk\/status\/776990674402938880\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/pDsa4B2g7A",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cshs4I8UAAAgcb_.jpg",
      "id_str" : "776990647450337280",
      "id" : 776990647450337280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cshs4I8UAAAgcb_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1956,
        "resize" : "fit",
        "w" : 3000
      }, {
        "h" : 443,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1335,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 782,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/pDsa4B2g7A"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/4CEuUb9E76",
      "expanded_url" : "http:\/\/str.sg\/4GAQ",
      "display_url" : "str.sg\/4GAQ"
    } ]
  },
  "geo" : { },
  "id_str" : "777031954235592705",
  "text" : "RT @STsportsdesk: Singapore to be known as SGP, instead of SIN, at sporting events https:\/\/t.co\/4CEuUb9E76 https:\/\/t.co\/pDsa4B2g7A",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STsportsdesk\/status\/776990674402938880\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/pDsa4B2g7A",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cshs4I8UAAAgcb_.jpg",
        "id_str" : "776990647450337280",
        "id" : 776990647450337280,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cshs4I8UAAAgcb_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1956,
          "resize" : "fit",
          "w" : 3000
        }, {
          "h" : 443,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1335,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 782,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/pDsa4B2g7A"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 65, 88 ],
        "url" : "https:\/\/t.co\/4CEuUb9E76",
        "expanded_url" : "http:\/\/str.sg\/4GAQ",
        "display_url" : "str.sg\/4GAQ"
      } ]
    },
    "geo" : { },
    "id_str" : "776990674402938880",
    "text" : "Singapore to be known as SGP, instead of SIN, at sporting events https:\/\/t.co\/4CEuUb9E76 https:\/\/t.co\/pDsa4B2g7A",
    "id" : 776990674402938880,
    "created_at" : "2016-09-17 03:46:27 +0000",
    "user" : {
      "name" : "ST Sports Desk",
      "screen_name" : "STsportsdesk",
      "protected" : false,
      "id_str" : "308968000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/891352876332720128\/jRjrmFjC_normal.jpg",
      "id" : 308968000,
      "verified" : false
    }
  },
  "id" : 777031954235592705,
  "created_at" : "2016-09-17 06:30:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776919353988116485",
  "text" : "Some here are outraged over Irish lambs died of suffocation to Singapore. I do hope they are all vegetarian?",
  "id" : 776919353988116485,
  "created_at" : "2016-09-16 23:03:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/8lFC7zGTA7",
      "expanded_url" : "http:\/\/www.todayonline.com\/sports\/im-racing-equality-not-money-yip-pin-xiu",
      "display_url" : "todayonline.com\/sports\/im-raci\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776915292047941632",
  "text" : "I\u2019m racing for equality, not money: Yip Pin Xiu https:\/\/t.co\/8lFC7zGTA7",
  "id" : 776915292047941632,
  "created_at" : "2016-09-16 22:46:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/8YMqsMtDkc",
      "expanded_url" : "http:\/\/str.sg\/4prj",
      "display_url" : "str.sg\/4prj"
    } ]
  },
  "geo" : { },
  "id_str" : "776914628135755776",
  "text" : "Malaysian Paralympic gold medallists to get same cash reward as Olympians https:\/\/t.co\/8YMqsMtDkc",
  "id" : 776914628135755776,
  "created_at" : "2016-09-16 22:44:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patricia Heckmann",
      "screen_name" : "PHeckmann47",
      "indices" : [ 3, 15 ],
      "id_str" : "4872636197",
      "id" : 4872636197
    }, {
      "name" : "Richard Corbridge",
      "screen_name" : "R1chardatron",
      "indices" : [ 17, 30 ],
      "id_str" : "162317442",
      "id" : 162317442
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Healthandcare",
      "indices" : [ 91, 105 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776912877974384644",
  "text" : "RT @PHeckmann47: @R1chardatron  NHS having 3 events for patients to have their say. on how #Healthandcare data is used\rhttps:\/\/t.co\/jzEKN9M\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Richard Corbridge",
        "screen_name" : "R1chardatron",
        "indices" : [ 0, 13 ],
        "id_str" : "162317442",
        "id" : 162317442
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Healthandcare",
        "indices" : [ 74, 88 ]
      } ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/jzEKN9MTaq",
        "expanded_url" : "http:\/\/ow.ly\/yEpw304etAS",
        "display_url" : "ow.ly\/yEpw304etAS"
      } ]
    },
    "geo" : { },
    "id_str" : "776885509612249088",
    "in_reply_to_user_id" : 162317442,
    "text" : "@R1chardatron  NHS having 3 events for patients to have their say. on how #Healthandcare data is used\rhttps:\/\/t.co\/jzEKN9MTaq",
    "id" : 776885509612249088,
    "created_at" : "2016-09-16 20:48:33 +0000",
    "in_reply_to_screen_name" : "R1chardatron",
    "in_reply_to_user_id_str" : "162317442",
    "user" : {
      "name" : "Patricia Heckmann",
      "screen_name" : "PHeckmann47",
      "protected" : false,
      "id_str" : "4872636197",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/719211596954877952\/2fqDh5D-_normal.jpg",
      "id" : 4872636197,
      "verified" : false
    }
  },
  "id" : 776912877974384644,
  "created_at" : "2016-09-16 22:37:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/776873800264740865\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/KOS2gu2nFd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsgClwQXEAAWmfd.jpg",
      "id_str" : "776873783353348096",
      "id" : 776873783353348096,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsgClwQXEAAWmfd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 1044
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 1044
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 1044
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KOS2gu2nFd"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776873800264740865",
  "text" : "I had tried this while in Pairs this summer. I would not recommend at all. https:\/\/t.co\/KOS2gu2nFd",
  "id" : 776873800264740865,
  "created_at" : "2016-09-16 20:02:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/A1TUkghyD3",
      "expanded_url" : "https:\/\/cloud.google.com\/blog\/big-data\/2016\/08\/how-a-japanese-cucumber-farmer-is-using-deep-learning-and-tensorflow",
      "display_url" : "cloud.google.com\/blog\/big-data\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776869219296219136",
  "text" : "How Makoto help his parents at the family cucumber farm explore machine learning for sorting cucumbers. https:\/\/t.co\/A1TUkghyD3",
  "id" : 776869219296219136,
  "created_at" : "2016-09-16 19:43:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/GiTQ12RfMP",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-37358326",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776815492455686145",
  "text" : "Singapore Paralympians immortalised as Lego figurines https:\/\/t.co\/GiTQ12RfMP",
  "id" : 776815492455686145,
  "created_at" : "2016-09-16 16:10:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776737030554386432",
  "text" : "\"Hero Garda rescues woman from river using only his utility belt\" Isn't that jobs of the Garda to render assistance in the first place?",
  "id" : 776737030554386432,
  "created_at" : "2016-09-16 10:58:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Frontline Energy",
      "screen_name" : "FrontlineEnerg",
      "indices" : [ 0, 15 ],
      "id_str" : "260166169",
      "id" : 260166169
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776735197287112704",
  "in_reply_to_user_id" : 260166169,
  "text" : "@FrontlineEnerg There is no Hot Water Supply since 15th Sept 2016 afternoon. Please advice ASAP. Thanks",
  "id" : 776735197287112704,
  "created_at" : "2016-09-16 10:51:16 +0000",
  "in_reply_to_screen_name" : "FrontlineEnerg",
  "in_reply_to_user_id_str" : "260166169",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jo-fai (Joe) Chow",
      "screen_name" : "matlabulous",
      "indices" : [ 3, 15 ],
      "id_str" : "296664658",
      "id" : 296664658
    }, {
      "name" : "H2O.ai",
      "screen_name" : "h2oai",
      "indices" : [ 41, 47 ],
      "id_str" : "481867656",
      "id" : 481867656
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Paris",
      "indices" : [ 20, 26 ]
    }, {
      "text" : "DeepWater",
      "indices" : [ 48, 58 ]
    }, {
      "text" : "Spark",
      "indices" : [ 66, 72 ]
    }, {
      "text" : "MachineLearning",
      "indices" : [ 88, 104 ]
    }, {
      "text" : "meetup",
      "indices" : [ 105, 112 ]
    } ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/4FRHw0uH2z",
      "expanded_url" : "https:\/\/www.meetup.com\/Paris-Machine-learning-applications-group\/events\/233874278\/",
      "display_url" : "meetup.com\/Paris-Machine-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776734997969530880",
  "text" : "RT @matlabulous: In #Paris? Come and see @h2oai #DeepWater (&amp; #Spark) demos at this #MachineLearning #meetup https:\/\/t.co\/4FRHw0uH2z https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "H2O.ai",
        "screen_name" : "h2oai",
        "indices" : [ 24, 30 ],
        "id_str" : "481867656",
        "id" : 481867656
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Paris",
        "indices" : [ 3, 9 ]
      }, {
        "text" : "DeepWater",
        "indices" : [ 31, 41 ]
      }, {
        "text" : "Spark",
        "indices" : [ 49, 55 ]
      }, {
        "text" : "MachineLearning",
        "indices" : [ 71, 87 ]
      }, {
        "text" : "meetup",
        "indices" : [ 88, 95 ]
      } ],
      "urls" : [ {
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/4FRHw0uH2z",
        "expanded_url" : "https:\/\/www.meetup.com\/Paris-Machine-learning-applications-group\/events\/233874278\/",
        "display_url" : "meetup.com\/Paris-Machine-\u2026"
      }, {
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/lJBJHU2SBo",
        "expanded_url" : "https:\/\/twitter.com\/ArnoCandel\/status\/776579768464453633",
        "display_url" : "twitter.com\/ArnoCandel\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776734277891452928",
    "text" : "In #Paris? Come and see @h2oai #DeepWater (&amp; #Spark) demos at this #MachineLearning #meetup https:\/\/t.co\/4FRHw0uH2z https:\/\/t.co\/lJBJHU2SBo",
    "id" : 776734277891452928,
    "created_at" : "2016-09-16 10:47:37 +0000",
    "user" : {
      "name" : "Jo-fai (Joe) Chow",
      "screen_name" : "matlabulous",
      "protected" : false,
      "id_str" : "296664658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021046533360406528\/19NnYn60_normal.jpg",
      "id" : 296664658,
      "verified" : false
    }
  },
  "id" : 776734997969530880,
  "created_at" : "2016-09-16 10:50:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776721523877023746",
  "text" : "After filling up a RPEL form, it helps me to focus what to put in my CV.",
  "id" : 776721523877023746,
  "created_at" : "2016-09-16 09:56:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hazel Chu",
      "screen_name" : "hazechu",
      "indices" : [ 3, 11 ],
      "id_str" : "895595520",
      "id" : 895595520
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/SR7HrvBSGs",
      "expanded_url" : "https:\/\/twitter.com\/Diageo_News\/status\/771629758840868864",
      "display_url" : "twitter.com\/Diageo_News\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776720870706479104",
  "text" : "RT @hazechu: For those of you who wonder what I do and what my desk looks like (see told you it has a cow...) https:\/\/t.co\/SR7HrvBSGs",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/SR7HrvBSGs",
        "expanded_url" : "https:\/\/twitter.com\/Diageo_News\/status\/771629758840868864",
        "display_url" : "twitter.com\/Diageo_News\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776720067727855616",
    "text" : "For those of you who wonder what I do and what my desk looks like (see told you it has a cow...) https:\/\/t.co\/SR7HrvBSGs",
    "id" : 776720067727855616,
    "created_at" : "2016-09-16 09:51:09 +0000",
    "user" : {
      "name" : "Hazel Chu",
      "screen_name" : "hazechu",
      "protected" : false,
      "id_str" : "895595520",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/987968602648334336\/ri_CIcQz_normal.jpg",
      "id" : 895595520,
      "verified" : false
    }
  },
  "id" : 776720870706479104,
  "created_at" : "2016-09-16 09:54:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776718859197882368",
  "text" : "I learnt that in a cat caf\u00E9 you can see the cat but you can't touch it. Just like strip club?",
  "id" : 776718859197882368,
  "created_at" : "2016-09-16 09:46:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u00F3is\u00EDn Prizeman",
      "screen_name" : "RoisinPrizeman",
      "indices" : [ 3, 18 ],
      "id_str" : "3012925383",
      "id" : 3012925383
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/UGoVmroSxW",
      "expanded_url" : "http:\/\/globalnews.ca\/video\/2220460\/dragon-boating-for-women-with-breast-cancer",
      "display_url" : "globalnews.ca\/video\/2220460\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776718031506538498",
  "text" : "RT @RoisinPrizeman: Dragon Boating for women with breast cancer https:\/\/t.co\/UGoVmroSxW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/UGoVmroSxW",
        "expanded_url" : "http:\/\/globalnews.ca\/video\/2220460\/dragon-boating-for-women-with-breast-cancer",
        "display_url" : "globalnews.ca\/video\/2220460\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776716804764921856",
    "text" : "Dragon Boating for women with breast cancer https:\/\/t.co\/UGoVmroSxW",
    "id" : 776716804764921856,
    "created_at" : "2016-09-16 09:38:11 +0000",
    "user" : {
      "name" : "R\u00F3is\u00EDn Prizeman",
      "screen_name" : "RoisinPrizeman",
      "protected" : false,
      "id_str" : "3012925383",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/786492421622403072\/iLiCp7q6_normal.jpg",
      "id" : 3012925383,
      "verified" : false
    }
  },
  "id" : 776718031506538498,
  "created_at" : "2016-09-16 09:43:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/Ffk9IUKanP",
      "expanded_url" : "http:\/\/reut.rs\/2cHi40s",
      "display_url" : "reut.rs\/2cHi40s"
    } ]
  },
  "geo" : { },
  "id_str" : "776674310165499904",
  "text" : "Malaysia confirms debris found in Tanzania is from MH370 https:\/\/t.co\/Ffk9IUKanP",
  "id" : 776674310165499904,
  "created_at" : "2016-09-16 06:49:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776673586501263362",
  "text" : "Just a thought. Ireland you can tax car ownership to fund public transport.",
  "id" : 776673586501263362,
  "created_at" : "2016-09-16 06:46:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "indices" : [ 3, 11 ],
      "id_str" : "41025492",
      "id" : 41025492
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/DCXB0yciCJ",
      "expanded_url" : "https:\/\/twitter.com\/leehsienloong\/status\/776626397447987200",
      "display_url" : "twitter.com\/leehsienloong\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776671103569764353",
  "text" : "RT @marklcl: Awesome! So proud of her! https:\/\/t.co\/DCXB0yciCJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/DCXB0yciCJ",
        "expanded_url" : "https:\/\/twitter.com\/leehsienloong\/status\/776626397447987200",
        "display_url" : "twitter.com\/leehsienloong\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776658365690687488",
    "text" : "Awesome! So proud of her! https:\/\/t.co\/DCXB0yciCJ",
    "id" : 776658365690687488,
    "created_at" : "2016-09-16 05:45:58 +0000",
    "user" : {
      "name" : "Mark Lim Choon Lye",
      "screen_name" : "marklcl",
      "protected" : false,
      "id_str" : "41025492",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684550252083298306\/DmeNwyFV_normal.jpg",
      "id" : 41025492,
      "verified" : false
    }
  },
  "id" : 776671103569764353,
  "created_at" : "2016-09-16 06:36:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SiliconRepublic",
      "screen_name" : "siliconrepublic",
      "indices" : [ 90, 106 ],
      "id_str" : "14385329",
      "id" : 14385329
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/zjlf5f62QR",
      "expanded_url" : "https:\/\/www.siliconrepublic.com\/companies\/insight-centre-for-data-analytics-masterclass",
      "display_url" : "siliconrepublic.com\/companies\/insi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776546964133511168",
  "text" : "Free data \u2018masterclass\u2019 courses delivered to Irish businesses https:\/\/t.co\/zjlf5f62QR via @siliconrepublic",
  "id" : 776546964133511168,
  "created_at" : "2016-09-15 22:23:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776541311826530305",
  "text" : "ceteris paribus, search for bicycle spike given the industrial action has escalated to 13 days?",
  "id" : 776541311826530305,
  "created_at" : "2016-09-15 22:00:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Niamh \u2764\uFE0F",
      "screen_name" : "niamh_ly",
      "indices" : [ 3, 12 ],
      "id_str" : "902196143035207680",
      "id" : 902196143035207680
    }, {
      "name" : "Hailo",
      "screen_name" : "Hailo",
      "indices" : [ 14, 20 ],
      "id_str" : "1009499550",
      "id" : 1009499550
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "busstrike",
      "indices" : [ 101, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/6WlTnXVHH4",
      "expanded_url" : "http:\/\/buff.ly\/2bWMlIQ",
      "display_url" : "buff.ly\/2bWMlIQ"
    } ]
  },
  "geo" : { },
  "id_str" : "776540486970568709",
  "text" : "RT @niamh_ly: @Hailo hops on Dublin Bus strike woes with 50pc off-peak fares https:\/\/t.co\/6WlTnXVHH4 #busstrike",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hailo",
        "screen_name" : "Hailo",
        "indices" : [ 0, 6 ],
        "id_str" : "1009499550",
        "id" : 1009499550
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "busstrike",
        "indices" : [ 87, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/6WlTnXVHH4",
        "expanded_url" : "http:\/\/buff.ly\/2bWMlIQ",
        "display_url" : "buff.ly\/2bWMlIQ"
      } ]
    },
    "geo" : { },
    "id_str" : "776498322035048452",
    "in_reply_to_user_id" : 1009499550,
    "text" : "@Hailo hops on Dublin Bus strike woes with 50pc off-peak fares https:\/\/t.co\/6WlTnXVHH4 #busstrike",
    "id" : 776498322035048452,
    "created_at" : "2016-09-15 19:10:01 +0000",
    "in_reply_to_screen_name" : "Hailo",
    "in_reply_to_user_id_str" : "1009499550",
    "user" : {
      "name" : "Niamh Lynch",
      "screen_name" : "clockwork_blog",
      "protected" : false,
      "id_str" : "59750339",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001837006861885441\/sO_ZgO-W_normal.jpg",
      "id" : 59750339,
      "verified" : false
    }
  },
  "id" : 776540486970568709,
  "created_at" : "2016-09-15 21:57:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jobfairy",
      "indices" : [ 82, 91 ]
    }, {
      "text" : "DataScience",
      "indices" : [ 92, 104 ]
    }, {
      "text" : "rstats",
      "indices" : [ 105, 112 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/QqKx4TSTjL",
      "expanded_url" : "https:\/\/app.box.com\/s\/nn87oebwilga9zdk2re9rj8zazqgzyp9",
      "display_url" : "app.box.com\/s\/nn87oebwilga\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776536664676167680",
  "text" : "Machine Learning Software Engineer opportunity in Dublin https:\/\/t.co\/QqKx4TSTjL  #jobfairy #DataScience #rstats",
  "id" : 776536664676167680,
  "created_at" : "2016-09-15 21:42:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 3, 14 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/Wr1kIOHics",
      "expanded_url" : "https:\/\/twitter.com\/jeanyoon\/status\/776415726097670146",
      "display_url" : "twitter.com\/jeanyoon\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776532335441375232",
  "text" : "RT @CaroBowler: Between the property stashes &amp; private banking, how do the wealthy of ASEAN view Singapore?  https:\/\/t.co\/Wr1kIOHics",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/Wr1kIOHics",
        "expanded_url" : "https:\/\/twitter.com\/jeanyoon\/status\/776415726097670146",
        "display_url" : "twitter.com\/jeanyoon\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776425774886694913",
    "text" : "Between the property stashes &amp; private banking, how do the wealthy of ASEAN view Singapore?  https:\/\/t.co\/Wr1kIOHics",
    "id" : 776425774886694913,
    "created_at" : "2016-09-15 14:21:44 +0000",
    "user" : {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "protected" : false,
      "id_str" : "4747144932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686824073884643329\/U-3rp4zx_normal.jpg",
      "id" : 4747144932,
      "verified" : false
    }
  },
  "id" : 776532335441375232,
  "created_at" : "2016-09-15 21:25:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776531918825357313",
  "text" : "Is that the same TD that behind the 'Fornication row' few years back? That a new word for me. I got to look up a dictionary.",
  "id" : 776531918825357313,
  "created_at" : "2016-09-15 21:23:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 3, 17 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776497818810912768",
  "text" : "RT @ladystormhold: The UK's Department for International Trade is advertising globally for a Perm Sec. Y'know, if anyone's interested.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776415935167078400",
    "text" : "The UK's Department for International Trade is advertising globally for a Perm Sec. Y'know, if anyone's interested.",
    "id" : 776415935167078400,
    "created_at" : "2016-09-15 13:42:38 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 776497818810912768,
  "created_at" : "2016-09-15 19:08:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Meabh N\u00EDPhriondrag\u00E1s",
      "screen_name" : "meabhp",
      "indices" : [ 3, 10 ],
      "id_str" : "18996732",
      "id" : 18996732
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776497654784139264",
  "text" : "RT @meabhp: Listening to great presentation from @ShinyShella on eHealth at #nmpdconf16. Msg of the day \uD83D\uDE91 https:\/\/t.co\/nAP3yZBTyP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/meabhp\/status\/776429212521598976\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/nAP3yZBTyP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsZuPtAXEAExu3g.jpg",
        "id_str" : "776429201826189313",
        "id" : 776429201826189313,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsZuPtAXEAExu3g.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 857,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 863,
          "resize" : "fit",
          "w" : 1208
        }, {
          "h" : 486,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 863,
          "resize" : "fit",
          "w" : 1208
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nAP3yZBTyP"
      } ],
      "hashtags" : [ {
        "text" : "nmpdconf16",
        "indices" : [ 64, 75 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "776429212521598976",
    "text" : "Listening to great presentation from @ShinyShella on eHealth at #nmpdconf16. Msg of the day \uD83D\uDE91 https:\/\/t.co\/nAP3yZBTyP",
    "id" : 776429212521598976,
    "created_at" : "2016-09-15 14:35:24 +0000",
    "user" : {
      "name" : "Meabh N\u00EDPhriondrag\u00E1s",
      "screen_name" : "meabhp",
      "protected" : false,
      "id_str" : "18996732",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/984555909312602114\/9IAo-C1U_normal.jpg",
      "id" : 18996732,
      "verified" : false
    }
  },
  "id" : 776497654784139264,
  "created_at" : "2016-09-15 19:07:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776395486894891008",
  "text" : "I'm currently seeking for a role in data analytics in Dublin, contract or permanent. Please get in touch if you have any leads. Thanks.",
  "id" : 776395486894891008,
  "created_at" : "2016-09-15 12:21:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776379688419749888",
  "text" : "What the different between RPEL and CPD log? Thanks",
  "id" : 776379688419749888,
  "created_at" : "2016-09-15 11:18:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Curbed",
      "screen_name" : "Curbed",
      "indices" : [ 79, 86 ],
      "id_str" : "173996982",
      "id" : 173996982
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/RXdhQbjUcR",
      "expanded_url" : "http:\/\/www.curbed.com\/2016\/8\/31\/12691516\/self-driving-bus-vehicles-finland-helsinki-transportation?utm_campaign=curbed&utm_content=entry&utm_medium=social&utm_source=twitter",
      "display_url" : "curbed.com\/2016\/8\/31\/1269\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776374521951649792",
  "text" : "Self-driving buses are now on the road in Helsinki https:\/\/t.co\/RXdhQbjUcR via @Curbed",
  "id" : 776374521951649792,
  "created_at" : "2016-09-15 10:58:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/HDMe9Th2KJ",
      "expanded_url" : "http:\/\/finance.yahoo.com\/news\/microsoft-just-edged-facebook-proved-215358972.html?soc_src=social-sh&soc_trk=tw",
      "display_url" : "finance.yahoo.com\/news\/microsoft\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776364403709054976",
  "text" : "\"GitHub \u2014 the so-called \"Facebook for programmers,\" &lt; Really?  https:\/\/t.co\/HDMe9Th2KJ",
  "id" : 776364403709054976,
  "created_at" : "2016-09-15 10:17:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "k-leb k",
      "screen_name" : "dcatdemon",
      "indices" : [ 3, 13 ],
      "id_str" : "130048954",
      "id" : 130048954
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/kdPduE93EI",
      "expanded_url" : "https:\/\/twitter.com\/jennyteo\/status\/776248482155769856",
      "display_url" : "twitter.com\/jennyteo\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "776301389823827968",
  "text" : "RT @dcatdemon: Defendants of the Sun https:\/\/t.co\/kdPduE93EI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 22, 45 ],
        "url" : "https:\/\/t.co\/kdPduE93EI",
        "expanded_url" : "https:\/\/twitter.com\/jennyteo\/status\/776248482155769856",
        "display_url" : "twitter.com\/jennyteo\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "776293130517811200",
    "text" : "Defendants of the Sun https:\/\/t.co\/kdPduE93EI",
    "id" : 776293130517811200,
    "created_at" : "2016-09-15 05:34:39 +0000",
    "user" : {
      "name" : "k-leb k",
      "screen_name" : "dcatdemon",
      "protected" : false,
      "id_str" : "130048954",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/847358168338280448\/NuxLpO5t_normal.jpg",
      "id" : 130048954,
      "verified" : false
    }
  },
  "id" : 776301389823827968,
  "created_at" : "2016-09-15 06:07:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776300934985056256",
  "text" : "Solidarity with those who need bus to go to work, hospital and school.",
  "id" : 776300934985056256,
  "created_at" : "2016-09-15 06:05:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "M\u0103d\u0103lina Ciobanu",
      "screen_name" : "madalinacrc",
      "indices" : [ 3, 15 ],
      "id_str" : "103074200",
      "id" : 103074200
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/madalinacrc\/status\/706849181072232448\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/mtrcALoo0C",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc87kGwW8AAgjRT.jpg",
      "id_str" : "706849157995229184",
      "id" : 706849157995229184,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc87kGwW8AAgjRT.jpg",
      "sizes" : [ {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mtrcALoo0C"
    } ],
    "hashtags" : [ {
      "text" : "DMS16",
      "indices" : [ 66, 72 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776116990356426752",
  "text" : "RT @madalinacrc: How The Telegraph thinks of its digital audience #DMS16 https:\/\/t.co\/mtrcALoo0C",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/madalinacrc\/status\/706849181072232448\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/mtrcALoo0C",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cc87kGwW8AAgjRT.jpg",
        "id_str" : "706849157995229184",
        "id" : 706849157995229184,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cc87kGwW8AAgjRT.jpg",
        "sizes" : [ {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mtrcALoo0C"
      } ],
      "hashtags" : [ {
        "text" : "DMS16",
        "indices" : [ 49, 55 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "706849181072232448",
    "text" : "How The Telegraph thinks of its digital audience #DMS16 https:\/\/t.co\/mtrcALoo0C",
    "id" : 706849181072232448,
    "created_at" : "2016-03-07 14:29:12 +0000",
    "user" : {
      "name" : "M\u0103d\u0103lina Ciobanu",
      "screen_name" : "madalinacrc",
      "protected" : false,
      "id_str" : "103074200",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035453453122846720\/XGOgNPxA_normal.jpg",
      "id" : 103074200,
      "verified" : false
    }
  },
  "id" : 776116990356426752,
  "created_at" : "2016-09-14 17:54:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0625\u062D\u0633\u0627\u0646",
      "screen_name" : "I7sannFt",
      "indices" : [ 3, 12 ],
      "id_str" : "2396674785",
      "id" : 2396674785
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Mosjn\/status\/775657899171479552\/video\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/fHNRe8wEGr",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/775657824663719936\/pu\/img\/p4ZPc0bLANpNEqd0.jpg",
      "id_str" : "775657824663719936",
      "id" : 775657824663719936,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/775657824663719936\/pu\/img\/p4ZPc0bLANpNEqd0.jpg",
      "sizes" : [ {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 400
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/fHNRe8wEGr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776106493590601729",
  "text" : "RT @I7sannFt: I've been living life all wrong https:\/\/t.co\/fHNRe8wEGr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Mosjn\/status\/775657899171479552\/video\/1",
        "indices" : [ 32, 55 ],
        "url" : "https:\/\/t.co\/fHNRe8wEGr",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/775657824663719936\/pu\/img\/p4ZPc0bLANpNEqd0.jpg",
        "id_str" : "775657824663719936",
        "id" : 775657824663719936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/775657824663719936\/pu\/img\/p4ZPc0bLANpNEqd0.jpg",
        "sizes" : [ {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 400
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fHNRe8wEGr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775805555151470593",
    "text" : "I've been living life all wrong https:\/\/t.co\/fHNRe8wEGr",
    "id" : 775805555151470593,
    "created_at" : "2016-09-13 21:17:12 +0000",
    "user" : {
      "name" : "\u0625\u062D\u0633\u0627\u0646",
      "screen_name" : "I7sannFt",
      "protected" : false,
      "id_str" : "2396674785",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1008084468341575682\/atveP6sU_normal.jpg",
      "id" : 2396674785,
      "verified" : false
    }
  },
  "id" : 776106493590601729,
  "created_at" : "2016-09-14 17:13:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "776029436978266112",
  "text" : "It is true that compared to FB or Twitter, there is no analytics on Instagram?",
  "id" : 776029436978266112,
  "created_at" : "2016-09-14 12:06:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    }, {
      "name" : "Singapore Airlines",
      "screen_name" : "SingaporeAir",
      "indices" : [ 22, 35 ],
      "id_str" : "253340062",
      "id" : 253340062
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Airbus",
      "indices" : [ 78, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/LXhIgECM0M",
      "expanded_url" : "http:\/\/bit.ly\/2csctbY",
      "display_url" : "bit.ly\/2csctbY"
    } ]
  },
  "geo" : { },
  "id_str" : "776010229272154112",
  "text" : "RT @ChannelNewsAsia: .@singaporeair won't be extending the lease on its first #Airbus A380, which expires in 2017 https:\/\/t.co\/LXhIgECM0M h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Singapore Airlines",
        "screen_name" : "SingaporeAir",
        "indices" : [ 1, 14 ],
        "id_str" : "253340062",
        "id" : 253340062
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/775978923041492992\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/CemTAmD20c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsTTZ6JVUAEoLlF.jpg",
        "id_str" : "775977477873160193",
        "id" : 775977477873160193,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsTTZ6JVUAEoLlF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CemTAmD20c"
      } ],
      "hashtags" : [ {
        "text" : "Airbus",
        "indices" : [ 57, 64 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/LXhIgECM0M",
        "expanded_url" : "http:\/\/bit.ly\/2csctbY",
        "display_url" : "bit.ly\/2csctbY"
      } ]
    },
    "geo" : { },
    "id_str" : "775978923041492992",
    "text" : ".@singaporeair won't be extending the lease on its first #Airbus A380, which expires in 2017 https:\/\/t.co\/LXhIgECM0M https:\/\/t.co\/CemTAmD20c",
    "id" : 775978923041492992,
    "created_at" : "2016-09-14 08:46:06 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 776010229272154112,
  "created_at" : "2016-09-14 10:50:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775998129518895104",
  "text" : "Wrong timing to be in college library. Now, there is an orientation and I the only one using the computer.",
  "id" : 775998129518895104,
  "created_at" : "2016-09-14 10:02:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hong Kong",
      "screen_name" : "discoverhk",
      "indices" : [ 3, 14 ],
      "id_str" : "143819549",
      "id" : 143819549
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MidAutumn",
      "indices" : [ 81, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/eUY0V2ZJo3",
      "expanded_url" : "http:\/\/bit.ly\/2cJeiAP",
      "display_url" : "bit.ly\/2cJeiAP"
    } ]
  },
  "geo" : { },
  "id_str" : "775995442190512128",
  "text" : "RT @discoverhk: Hong Kong may be ever-changing, but traditional festivities like #MidAutumn lanterns thrive. https:\/\/t.co\/eUY0V2ZJo3 https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/discoverhk\/status\/775994551085346816\/video\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/ZvJXIczOE5",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/775994158200688640\/pu\/img\/k1LqBwZ_waDol3Eg.jpg",
        "id_str" : "775994158200688640",
        "id" : 775994158200688640,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/775994158200688640\/pu\/img\/k1LqBwZ_waDol3Eg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ZvJXIczOE5"
      } ],
      "hashtags" : [ {
        "text" : "MidAutumn",
        "indices" : [ 65, 75 ]
      } ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/eUY0V2ZJo3",
        "expanded_url" : "http:\/\/bit.ly\/2cJeiAP",
        "display_url" : "bit.ly\/2cJeiAP"
      } ]
    },
    "geo" : { },
    "id_str" : "775994551085346816",
    "text" : "Hong Kong may be ever-changing, but traditional festivities like #MidAutumn lanterns thrive. https:\/\/t.co\/eUY0V2ZJo3 https:\/\/t.co\/ZvJXIczOE5",
    "id" : 775994551085346816,
    "created_at" : "2016-09-14 09:48:12 +0000",
    "user" : {
      "name" : "Hong Kong",
      "screen_name" : "discoverhk",
      "protected" : false,
      "id_str" : "143819549",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031765688820498432\/wuL3j-nz_normal.jpg",
      "id" : 143819549,
      "verified" : true
    }
  },
  "id" : 775995442190512128,
  "created_at" : "2016-09-14 09:51:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/z7cABdh39n",
      "expanded_url" : "https:\/\/twitter.com\/alice_data\/status\/775974602229051392",
      "display_url" : "twitter.com\/alice_data\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "775977218463924224",
  "text" : "Bringing Jupyter notebook function in RStudio https:\/\/t.co\/z7cABdh39n",
  "id" : 775977218463924224,
  "created_at" : "2016-09-14 08:39:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dam\u00EFe\u00F1 M\u00FAlley \u00B8",
      "screen_name" : "damienmulley",
      "indices" : [ 3, 16 ],
      "id_str" : "193753",
      "id" : 193753
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775975640872943616",
  "text" : "RT @damienmulley: Ulta-orthadox Jewish men aren't meant to work, the women earn while they study. Nice gig til your benefits are cut https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/Cpd4gBUBl7",
        "expanded_url" : "http:\/\/www.economist.com\/news\/middle-east-and-africa\/21656207-israel-cannot-afford-keep-paying-ultra-orthodox-men-shun-employment-eat",
        "display_url" : "economist.com\/news\/middle-ea\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "775827936758886400",
    "text" : "Ulta-orthadox Jewish men aren't meant to work, the women earn while they study. Nice gig til your benefits are cut https:\/\/t.co\/Cpd4gBUBl7",
    "id" : 775827936758886400,
    "created_at" : "2016-09-13 22:46:08 +0000",
    "user" : {
      "name" : "Dam\u00EFe\u00F1 M\u00FAlley \u00B8",
      "screen_name" : "damienmulley",
      "protected" : false,
      "id_str" : "193753",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004142705511944193\/y_8rA5rW_normal.jpg",
      "id" : 193753,
      "verified" : false
    }
  },
  "id" : 775975640872943616,
  "created_at" : "2016-09-14 08:33:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alice Data",
      "screen_name" : "alice_data",
      "indices" : [ 3, 14 ],
      "id_str" : "474823395",
      "id" : 474823395
    }, {
      "name" : "RStudio",
      "screen_name" : "rstudio",
      "indices" : [ 26, 34 ],
      "id_str" : "235261861",
      "id" : 235261861
    }, {
      "name" : "Joe Cheng",
      "screen_name" : "jcheng",
      "indices" : [ 48, 55 ],
      "id_str" : "5849202",
      "id" : 5849202
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/alice_data\/status\/775971096130686976\/photo\/1",
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/rnTbLjNXoS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsTNmDeWIAEm3-0.jpg",
      "id_str" : "775971089465876481",
      "id" : 775971089465876481,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsTNmDeWIAEm3-0.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rnTbLjNXoS"
    } ],
    "hashtags" : [ {
      "text" : "EARL2016",
      "indices" : [ 56, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775973519222005760",
  "text" : "RT @alice_data: What does @rstudio do? Intro by @jcheng #EARL2016 https:\/\/t.co\/rnTbLjNXoS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RStudio",
        "screen_name" : "rstudio",
        "indices" : [ 10, 18 ],
        "id_str" : "235261861",
        "id" : 235261861
      }, {
        "name" : "Joe Cheng",
        "screen_name" : "jcheng",
        "indices" : [ 32, 39 ],
        "id_str" : "5849202",
        "id" : 5849202
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/alice_data\/status\/775971096130686976\/photo\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/rnTbLjNXoS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsTNmDeWIAEm3-0.jpg",
        "id_str" : "775971089465876481",
        "id" : 775971089465876481,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsTNmDeWIAEm3-0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rnTbLjNXoS"
      } ],
      "hashtags" : [ {
        "text" : "EARL2016",
        "indices" : [ 40, 49 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775971096130686976",
    "text" : "What does @rstudio do? Intro by @jcheng #EARL2016 https:\/\/t.co\/rnTbLjNXoS",
    "id" : 775971096130686976,
    "created_at" : "2016-09-14 08:15:00 +0000",
    "user" : {
      "name" : "Alice Data",
      "screen_name" : "alice_data",
      "protected" : false,
      "id_str" : "474823395",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991719412976758784\/7Zt3vqGX_normal.jpg",
      "id" : 474823395,
      "verified" : false
    }
  },
  "id" : 775973519222005760,
  "created_at" : "2016-09-14 08:24:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 3, 15 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/gianniponzi\/status\/775966189055008768\/photo\/1",
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/SG3QZjYxdW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsTJG-KWIAA5vhJ.jpg",
      "id_str" : "775966157417357312",
      "id" : 775966157417357312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsTJG-KWIAA5vhJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1142,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1142,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 669,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 379,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/SG3QZjYxdW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775972644554412032",
  "text" : "RT @gianniponzi: Because no one else in the world drinks....or maybe people \"lose their grip\" more easily here https:\/\/t.co\/SG3QZjYxdW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gianniponzi\/status\/775966189055008768\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/SG3QZjYxdW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsTJG-KWIAA5vhJ.jpg",
        "id_str" : "775966157417357312",
        "id" : 775966157417357312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsTJG-KWIAA5vhJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1142,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1142,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 669,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 379,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/SG3QZjYxdW"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775966189055008768",
    "text" : "Because no one else in the world drinks....or maybe people \"lose their grip\" more easily here https:\/\/t.co\/SG3QZjYxdW",
    "id" : 775966189055008768,
    "created_at" : "2016-09-14 07:55:30 +0000",
    "user" : {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "protected" : false,
      "id_str" : "19793936",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034160433949761536\/GdlrLPNt_normal.jpg",
      "id" : 19793936,
      "verified" : false
    }
  },
  "id" : 775972644554412032,
  "created_at" : "2016-09-14 08:21:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/775972452707012608\/photo\/1",
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/MMivln7x1j",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsTO0m_WgAAz-uR.jpg",
      "id_str" : "775972439029350400",
      "id" : 775972439029350400,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsTO0m_WgAAz-uR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MMivln7x1j"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775972452707012608",
  "text" : "2 weeks in a row. Can`t wait for self drive bus https:\/\/t.co\/MMivln7x1j",
  "id" : 775972452707012608,
  "created_at" : "2016-09-14 08:20:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dam\u00EFe\u00F1 M\u00FAlley \u00B8",
      "screen_name" : "damienmulley",
      "indices" : [ 3, 16 ],
      "id_str" : "193753",
      "id" : 193753
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775948899643195392",
  "text" : "RT @damienmulley: Wish the media would stop making out DrinkAware is some kind of health agency instead of the PR wing of Diageo and Heinek\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775943833397846017",
    "text" : "Wish the media would stop making out DrinkAware is some kind of health agency instead of the PR wing of Diageo and Heineken",
    "id" : 775943833397846017,
    "created_at" : "2016-09-14 06:26:40 +0000",
    "user" : {
      "name" : "Dam\u00EFe\u00F1 M\u00FAlley \u00B8",
      "screen_name" : "damienmulley",
      "protected" : false,
      "id_str" : "193753",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004142705511944193\/y_8rA5rW_normal.jpg",
      "id" : 193753,
      "verified" : false
    }
  },
  "id" : 775948899643195392,
  "created_at" : "2016-09-14 06:46:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "indices" : [ 3, 14 ],
      "id_str" : "166540104",
      "id" : 166540104
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "soccer",
      "indices" : [ 70, 77 ]
    }, {
      "text" : "rstats",
      "indices" : [ 78, 85 ]
    }, {
      "text" : "networkgraph",
      "indices" : [ 86, 99 ]
    }, {
      "text" : "visNetwork",
      "indices" : [ 100, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/cNwDFbdfaD",
      "expanded_url" : "https:\/\/longhowlam.wordpress.com\/2016\/09\/12\/some-insights-in-soccer-transfers-using-market-basket-analysis\/",
      "display_url" : "longhowlam.wordpress.com\/2016\/09\/12\/som\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "775946388060286984",
  "text" : "RT @longhowlam: Some insights  soccer players transfers between clubs #soccer #rstats #networkgraph #visNetwork\n\nhttps:\/\/t.co\/cNwDFbdfaD",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "soccer",
        "indices" : [ 54, 61 ]
      }, {
        "text" : "rstats",
        "indices" : [ 62, 69 ]
      }, {
        "text" : "networkgraph",
        "indices" : [ 70, 83 ]
      }, {
        "text" : "visNetwork",
        "indices" : [ 84, 95 ]
      } ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/cNwDFbdfaD",
        "expanded_url" : "https:\/\/longhowlam.wordpress.com\/2016\/09\/12\/some-insights-in-soccer-transfers-using-market-basket-analysis\/",
        "display_url" : "longhowlam.wordpress.com\/2016\/09\/12\/som\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "775287459831742465",
    "text" : "Some insights  soccer players transfers between clubs #soccer #rstats #networkgraph #visNetwork\n\nhttps:\/\/t.co\/cNwDFbdfaD",
    "id" : 775287459831742465,
    "created_at" : "2016-09-12 10:58:29 +0000",
    "user" : {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "protected" : false,
      "id_str" : "166540104",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/677041208711970821\/BYuLlEyn_normal.jpg",
      "id" : 166540104,
      "verified" : false
    }
  },
  "id" : 775946388060286984,
  "created_at" : "2016-09-14 06:36:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Koenfucius",
      "screen_name" : "koenfucius",
      "indices" : [ 3, 14 ],
      "id_str" : "22464371",
      "id" : 22464371
    }, {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 98, 107 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/p3syI7HtDg",
      "expanded_url" : "http:\/\/bit.ly\/2cFg0r0",
      "display_url" : "bit.ly\/2cFg0r0"
    } ]
  },
  "geo" : { },
  "id_str" : "775942001736855552",
  "text" : "RT @koenfucius: Four lessons on how to use data properly from a mathematical genius\u2014nice piece by @rshotton https:\/\/t.co\/p3syI7HtDg https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "richard shotton",
        "screen_name" : "rshotton",
        "indices" : [ 82, 91 ],
        "id_str" : "18188324",
        "id" : 18188324
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/koenfucius\/status\/775934859172728832\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/48U0PaSOt4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsSspB0UIAEpzAV.jpg",
        "id_str" : "775934856677040129",
        "id" : 775934856677040129,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsSspB0UIAEpzAV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 435,
          "resize" : "fit",
          "w" : 650
        }, {
          "h" : 435,
          "resize" : "fit",
          "w" : 650
        }, {
          "h" : 435,
          "resize" : "fit",
          "w" : 650
        }, {
          "h" : 435,
          "resize" : "fit",
          "w" : 650
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/48U0PaSOt4"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/p3syI7HtDg",
        "expanded_url" : "http:\/\/bit.ly\/2cFg0r0",
        "display_url" : "bit.ly\/2cFg0r0"
      } ]
    },
    "geo" : { },
    "id_str" : "775934859172728832",
    "text" : "Four lessons on how to use data properly from a mathematical genius\u2014nice piece by @rshotton https:\/\/t.co\/p3syI7HtDg https:\/\/t.co\/48U0PaSOt4",
    "id" : 775934859172728832,
    "created_at" : "2016-09-14 05:51:01 +0000",
    "user" : {
      "name" : "Koenfucius",
      "screen_name" : "koenfucius",
      "protected" : false,
      "id_str" : "22464371",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/553494434315522048\/z0a9Z6Xx_normal.jpeg",
      "id" : 22464371,
      "verified" : false
    }
  },
  "id" : 775942001736855552,
  "created_at" : "2016-09-14 06:19:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDD4A\uFE0FFREEDOM",
      "screen_name" : "FreedomOlly",
      "indices" : [ 3, 15 ],
      "id_str" : "965371324959924225",
      "id" : 965371324959924225
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775814832251633664",
  "text" : "RT @FreedomOlly: Don't worry too much about GBBO moving to C4. Nothing's going to happen until Mary Berry invokes Arctic Roll 50, and that\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775588488704036864",
    "text" : "Don't worry too much about GBBO moving to C4. Nothing's going to happen until Mary Berry invokes Arctic Roll 50, and that could take years.",
    "id" : 775588488704036864,
    "created_at" : "2016-09-13 06:54:40 +0000",
    "user" : {
      "name" : "Olly Clarke \uD83D\uDD3A",
      "screen_name" : "VariousOlly",
      "protected" : false,
      "id_str" : "390287562",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/884703749465399296\/OhE0HEq__normal.jpg",
      "id" : 390287562,
      "verified" : false
    }
  },
  "id" : 775814832251633664,
  "created_at" : "2016-09-13 21:54:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/775814297150693376\/photo\/1",
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/2z4j5uT9eo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsQ-98XXgAAZeDV.jpg",
      "id_str" : "775814269711646720",
      "id" : 775814269711646720,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsQ-98XXgAAZeDV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2z4j5uT9eo"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775814297150693376",
  "text" : "Exhausted...good night https:\/\/t.co\/2z4j5uT9eo",
  "id" : 775814297150693376,
  "created_at" : "2016-09-13 21:51:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emma McArdle",
      "screen_name" : "McArdlePhoto",
      "indices" : [ 3, 16 ],
      "id_str" : "4354408581",
      "id" : 4354408581
    }, {
      "name" : "Picture Ireland",
      "screen_name" : "PictureIreland",
      "indices" : [ 69, 84 ],
      "id_str" : "2190623134",
      "id" : 2190623134
    }, {
      "name" : "Photos of Dublin",
      "screen_name" : "PhotosOfDublin",
      "indices" : [ 85, 100 ],
      "id_str" : "1943866482",
      "id" : 1943866482
    }, {
      "name" : "Old DublinTown. com",
      "screen_name" : "OldDublinTown",
      "indices" : [ 101, 115 ],
      "id_str" : "473457952",
      "id" : 473457952
    }, {
      "name" : "Old Ireland",
      "screen_name" : "OldeEire",
      "indices" : [ 116, 125 ],
      "id_str" : "2493112992",
      "id" : 2493112992
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Howth",
      "indices" : [ 36, 42 ]
    }, {
      "text" : "Dublin",
      "indices" : [ 52, 59 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 60, 68 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775812881245933569",
  "text" : "RT @McArdlePhoto: Amazing sunset at #Howth Harbour! #Dublin #Ireland @PictureIreland @PhotosOfDublin @OldDublinTown @OldeEire https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Picture Ireland",
        "screen_name" : "PictureIreland",
        "indices" : [ 51, 66 ],
        "id_str" : "2190623134",
        "id" : 2190623134
      }, {
        "name" : "Photos of Dublin",
        "screen_name" : "PhotosOfDublin",
        "indices" : [ 67, 82 ],
        "id_str" : "1943866482",
        "id" : 1943866482
      }, {
        "name" : "Old DublinTown. com",
        "screen_name" : "OldDublinTown",
        "indices" : [ 83, 97 ],
        "id_str" : "473457952",
        "id" : 473457952
      }, {
        "name" : "Old Ireland",
        "screen_name" : "OldeEire",
        "indices" : [ 98, 107 ],
        "id_str" : "2493112992",
        "id" : 2493112992
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/McArdlePhoto\/status\/775769955409231872\/photo\/1",
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/l13HMJMzsl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsQWJxJW8AABgxE.jpg",
        "id_str" : "775769392881790976",
        "id" : 775769392881790976,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsQWJxJW8AABgxE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/l13HMJMzsl"
      } ],
      "hashtags" : [ {
        "text" : "Howth",
        "indices" : [ 18, 24 ]
      }, {
        "text" : "Dublin",
        "indices" : [ 34, 41 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 42, 50 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775769955409231872",
    "text" : "Amazing sunset at #Howth Harbour! #Dublin #Ireland @PictureIreland @PhotosOfDublin @OldDublinTown @OldeEire https:\/\/t.co\/l13HMJMzsl",
    "id" : 775769955409231872,
    "created_at" : "2016-09-13 18:55:45 +0000",
    "user" : {
      "name" : "Emma McArdle",
      "screen_name" : "McArdlePhoto",
      "protected" : false,
      "id_str" : "4354408581",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038477296137633796\/TJchp_P-_normal.jpg",
      "id" : 4354408581,
      "verified" : false
    }
  },
  "id" : 775812881245933569,
  "created_at" : "2016-09-13 21:46:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "indices" : [ 3, 19 ],
      "id_str" : "2735246778",
      "id" : 2735246778
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/705825638851149824\/photo\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/cnObD8GYGZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CcuYraMXIAE5Yn6.jpg",
      "id_str" : "705825638146580481",
      "id" : 705825638146580481,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcuYraMXIAE5Yn6.jpg",
      "sizes" : [ {
        "h" : 1323,
        "resize" : "fit",
        "w" : 1008
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1323,
        "resize" : "fit",
        "w" : 1008
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 914
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 518
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cnObD8GYGZ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775693819190865920",
  "text" : "RT @ThePracticalDev: The last programming book you'll ever need https:\/\/t.co\/cnObD8GYGZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/705825638851149824\/photo\/1",
        "indices" : [ 43, 66 ],
        "url" : "https:\/\/t.co\/cnObD8GYGZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CcuYraMXIAE5Yn6.jpg",
        "id_str" : "705825638146580481",
        "id" : 705825638146580481,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CcuYraMXIAE5Yn6.jpg",
        "sizes" : [ {
          "h" : 1323,
          "resize" : "fit",
          "w" : 1008
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1323,
          "resize" : "fit",
          "w" : 1008
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 914
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 518
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cnObD8GYGZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "705825638851149824",
    "text" : "The last programming book you'll ever need https:\/\/t.co\/cnObD8GYGZ",
    "id" : 705825638851149824,
    "created_at" : "2016-03-04 18:42:00 +0000",
    "user" : {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "protected" : false,
      "id_str" : "2735246778",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002604104194056192\/IEoNsLNM_normal.jpg",
      "id" : 2735246778,
      "verified" : false
    }
  },
  "id" : 775693819190865920,
  "created_at" : "2016-09-13 13:53:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hoyt Emerson",
      "screen_name" : "hoytemerson",
      "indices" : [ 3, 15 ],
      "id_str" : "94222298",
      "id" : 94222298
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775686302050254848",
  "text" : "RT @hoytemerson: Now all I see is less Data Scientist articles and more \"Marketers need to master machine learning\". Good luck with that.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775345871089115136",
    "text" : "Now all I see is less Data Scientist articles and more \"Marketers need to master machine learning\". Good luck with that.",
    "id" : 775345871089115136,
    "created_at" : "2016-09-12 14:50:35 +0000",
    "user" : {
      "name" : "Hoyt Emerson",
      "screen_name" : "hoytemerson",
      "protected" : false,
      "id_str" : "94222298",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/857421067446394881\/4zaXvAci_normal.jpg",
      "id" : 94222298,
      "verified" : false
    }
  },
  "id" : 775686302050254848,
  "created_at" : "2016-09-13 13:23:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "EARL Conference",
      "screen_name" : "earlconf",
      "indices" : [ 3, 12 ],
      "id_str" : "2330710330",
      "id" : 2330710330
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/earlconf\/status\/775336672028299265\/photo\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/1XBc3zxdsT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsKMl7QXYAEOIBP.jpg",
      "id_str" : "775336669050527745",
      "id" : 775336669050527745,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsKMl7QXYAEOIBP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 300,
        "resize" : "fit",
        "w" : 400
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1XBc3zxdsT"
    } ],
    "hashtags" : [ {
      "text" : "EARL2016",
      "indices" : [ 33, 42 ]
    }, {
      "text" : "rstats",
      "indices" : [ 100, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775685972386340868",
  "text" : "RT @earlconf: Follow our Hashtag #EARL2016 for the latest news about London's leading R Conference. #rstats https:\/\/t.co\/1XBc3zxdsT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/earlconf\/status\/775336672028299265\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/1XBc3zxdsT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsKMl7QXYAEOIBP.jpg",
        "id_str" : "775336669050527745",
        "id" : 775336669050527745,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsKMl7QXYAEOIBP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 400
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1XBc3zxdsT"
      } ],
      "hashtags" : [ {
        "text" : "EARL2016",
        "indices" : [ 19, 28 ]
      }, {
        "text" : "rstats",
        "indices" : [ 86, 93 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775336672028299265",
    "text" : "Follow our Hashtag #EARL2016 for the latest news about London's leading R Conference. #rstats https:\/\/t.co\/1XBc3zxdsT",
    "id" : 775336672028299265,
    "created_at" : "2016-09-12 14:14:02 +0000",
    "user" : {
      "name" : "EARL Conference",
      "screen_name" : "earlconf",
      "protected" : false,
      "id_str" : "2330710330",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/942787874424000512\/HFiIi4EM_normal.jpg",
      "id" : 2330710330,
      "verified" : false
    }
  },
  "id" : 775685972386340868,
  "created_at" : "2016-09-13 13:22:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ChinaAviationDaily",
      "screen_name" : "CNAviationDaily",
      "indices" : [ 3, 19 ],
      "id_str" : "260140968",
      "id" : 260140968
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/4cxwmI90Ri",
      "expanded_url" : "http:\/\/bit.ly\/2cESW8P",
      "display_url" : "bit.ly\/2cESW8P"
    } ]
  },
  "geo" : { },
  "id_str" : "775685409099620352",
  "text" : "RT @CNAviationDaily: Man Takes 7 Flights to Get from London to Aberdeen - Just for the Air Miles\nhttps:\/\/t.co\/4cxwmI90Ri https:\/\/t.co\/YbwLX\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CNAviationDaily\/status\/775540747785281537\/photo\/1",
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/YbwLXIZQ6Y",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsNF0x3UEAA2Kcw.jpg",
        "id_str" : "775540333878710272",
        "id" : 775540333878710272,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsNF0x3UEAA2Kcw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YbwLXIZQ6Y"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/CNAviationDaily\/status\/775540747785281537\/photo\/1",
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/YbwLXIZQ6Y",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsNF4FgUEAAxTn1.jpg",
        "id_str" : "775540390690557952",
        "id" : 775540390690557952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsNF4FgUEAAxTn1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YbwLXIZQ6Y"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/4cxwmI90Ri",
        "expanded_url" : "http:\/\/bit.ly\/2cESW8P",
        "display_url" : "bit.ly\/2cESW8P"
      } ]
    },
    "geo" : { },
    "id_str" : "775540747785281537",
    "text" : "Man Takes 7 Flights to Get from London to Aberdeen - Just for the Air Miles\nhttps:\/\/t.co\/4cxwmI90Ri https:\/\/t.co\/YbwLXIZQ6Y",
    "id" : 775540747785281537,
    "created_at" : "2016-09-13 03:44:57 +0000",
    "user" : {
      "name" : "ChinaAviationDaily",
      "screen_name" : "CNAviationDaily",
      "protected" : false,
      "id_str" : "260140968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/794438084708315136\/O9xrOV8j_normal.jpg",
      "id" : 260140968,
      "verified" : false
    }
  },
  "id" : 775685409099620352,
  "created_at" : "2016-09-13 13:19:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nico Miceli",
      "screen_name" : "NicoMiceli",
      "indices" : [ 3, 14 ],
      "id_str" : "280295417",
      "id" : 280295417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775683406529265665",
  "text" : "RT @NicoMiceli: LOL \"Statistics are like a bikini. What they reveal is suggestive, but what they conceal is vital.\" \u2013Aaron Levenstein #quot\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "quote",
        "indices" : [ 118, 124 ]
      }, {
        "text" : "stats",
        "indices" : [ 125, 131 ]
      }, {
        "text" : "measure",
        "indices" : [ 132, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "732281308047626240",
    "text" : "LOL \"Statistics are like a bikini. What they reveal is suggestive, but what they conceal is vital.\" \u2013Aaron Levenstein #quote #stats #measure",
    "id" : 732281308047626240,
    "created_at" : "2016-05-16 18:47:23 +0000",
    "user" : {
      "name" : "Nico Miceli",
      "screen_name" : "NicoMiceli",
      "protected" : false,
      "id_str" : "280295417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/799341307785256961\/fQB6BDAC_normal.jpg",
      "id" : 280295417,
      "verified" : false
    }
  },
  "id" : 775683406529265665,
  "created_at" : "2016-09-13 13:11:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "iworkwithdata",
      "indices" : [ 115, 129 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/h5bhsJEIe2",
      "expanded_url" : "https:\/\/github.com\/mryap\/rtb",
      "display_url" : "github.com\/mryap\/rtb"
    } ]
  },
  "geo" : { },
  "id_str" : "775645544106627072",
  "text" : "Do not think hiring manager got time to read an IEEE format paper. Putting coursework at  https:\/\/t.co\/h5bhsJEIe2  #iworkwithdata",
  "id" : 775645544106627072,
  "created_at" : "2016-09-13 10:41:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "indices" : [ 3, 17 ],
      "id_str" : "150246405",
      "id" : 150246405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/6UEbEl8YoG",
      "expanded_url" : "http:\/\/bit.ly\/2cmzXQw",
      "display_url" : "bit.ly\/2cmzXQw"
    } ]
  },
  "geo" : { },
  "id_str" : "775607897476718592",
  "text" : "RT @thejournal_ie: Dublin Bus strikes set to disrupt Culture Night this Friday https:\/\/t.co\/6UEbEl8YoG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/6UEbEl8YoG",
        "expanded_url" : "http:\/\/bit.ly\/2cmzXQw",
        "display_url" : "bit.ly\/2cmzXQw"
      } ]
    },
    "geo" : { },
    "id_str" : "775586310350184448",
    "text" : "Dublin Bus strikes set to disrupt Culture Night this Friday https:\/\/t.co\/6UEbEl8YoG",
    "id" : 775586310350184448,
    "created_at" : "2016-09-13 06:46:00 +0000",
    "user" : {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "protected" : false,
      "id_str" : "150246405",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948950612800139264\/bebzFmrQ_normal.jpg",
      "id" : 150246405,
      "verified" : true
    }
  },
  "id" : 775607897476718592,
  "created_at" : "2016-09-13 08:11:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melissa Fleming",
      "screen_name" : "melissarfleming",
      "indices" : [ 3, 19 ],
      "id_str" : "16079505",
      "id" : 16079505
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/0XoHjKvc05",
      "expanded_url" : "http:\/\/www.nytimes.com\/2016\/09\/07\/us\/syrian-refugees-christian-conservatives.html?smprod=nytcore-ipad&smid=nytcore-ipad-share",
      "display_url" : "nytimes.com\/2016\/09\/07\/us\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "775587531014496256",
  "text" : "RT @melissarfleming: \u201CI have been here for four months,\u201D Anwar said, \u201Cand I have seen nothing except goodness.\u201D https:\/\/t.co\/0XoHjKvc05",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/0XoHjKvc05",
        "expanded_url" : "http:\/\/www.nytimes.com\/2016\/09\/07\/us\/syrian-refugees-christian-conservatives.html?smprod=nytcore-ipad&smid=nytcore-ipad-share",
        "display_url" : "nytimes.com\/2016\/09\/07\/us\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773646591932657664",
    "text" : "\u201CI have been here for four months,\u201D Anwar said, \u201Cand I have seen nothing except goodness.\u201D https:\/\/t.co\/0XoHjKvc05",
    "id" : 773646591932657664,
    "created_at" : "2016-09-07 22:18:15 +0000",
    "user" : {
      "name" : "Melissa Fleming",
      "screen_name" : "melissarfleming",
      "protected" : false,
      "id_str" : "16079505",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/490463804568764416\/luJrpIbA_normal.jpeg",
      "id" : 16079505,
      "verified" : true
    }
  },
  "id" : 775587531014496256,
  "created_at" : "2016-09-13 06:50:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/cwVtSjKSYX",
      "expanded_url" : "http:\/\/econ.st\/2bE6O52",
      "display_url" : "econ.st\/2bE6O52"
    } ]
  },
  "geo" : { },
  "id_str" : "775314841779204096",
  "text" : "The Economist explains economics: What is the Nash equilibrium and why does it matter? https:\/\/t.co\/cwVtSjKSYX",
  "id" : 775314841779204096,
  "created_at" : "2016-09-12 12:47:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SiliconRepublic",
      "screen_name" : "siliconrepublic",
      "indices" : [ 93, 109 ],
      "id_str" : "14385329",
      "id" : 14385329
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/UOU06rMYfB",
      "expanded_url" : "https:\/\/www.siliconrepublic.com\/people\/bank-of-america-merrill-lynch-dublin-singapore-agile-software-developer",
      "display_url" : "siliconrepublic.com\/people\/bank-of\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "775301249868718080",
  "text" : "Friendliness and work-life balance welcome Singaporean to Dublin https:\/\/t.co\/UOU06rMYfB via @siliconrepublic",
  "id" : 775301249868718080,
  "created_at" : "2016-09-12 11:53:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775297379889283072",
  "text" : "IS there anyway to pull content from a sql file on MySQL database and convert to markdown and push it to GitHub (in one stroke)?",
  "id" : 775297379889283072,
  "created_at" : "2016-09-12 11:37:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Cliffe",
      "screen_name" : "JeremyCliffe",
      "indices" : [ 3, 16 ],
      "id_str" : "465965880",
      "id" : 465965880
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775296358366449664",
  "text" : "RT @JeremyCliffe: \"Hillary hid pneumonia for 2 days\" - America hyperventilates\n\n\"Mitterrand hid cancer, secret second family for over a dec\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "775226331579678720",
    "text" : "\"Hillary hid pneumonia for 2 days\" - America hyperventilates\n\n\"Mitterrand hid cancer, secret second family for over a decade\" - France yawns",
    "id" : 775226331579678720,
    "created_at" : "2016-09-12 06:55:35 +0000",
    "user" : {
      "name" : "Jeremy Cliffe",
      "screen_name" : "JeremyCliffe",
      "protected" : false,
      "id_str" : "465965880",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032651934677782530\/f6L5fvfu_normal.jpg",
      "id" : 465965880,
      "verified" : true
    }
  },
  "id" : 775296358366449664,
  "created_at" : "2016-09-12 11:33:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Guido Deckstein",
      "screen_name" : "guidodeckstein",
      "indices" : [ 3, 18 ],
      "id_str" : "1010810106",
      "id" : 1010810106
    }, {
      "name" : "TNW",
      "screen_name" : "TheNextWeb",
      "indices" : [ 112, 123 ],
      "id_str" : "10876852",
      "id" : 10876852
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/KxSPgZzmNL",
      "expanded_url" : "http:\/\/tnw.to\/2bCkwUh",
      "display_url" : "tnw.to\/2bCkwUh"
    } ]
  },
  "geo" : { },
  "id_str" : "775268779689439233",
  "text" : "RT @guidodeckstein: We looked at 137,052 tweets and learned hashtags were worthless https:\/\/t.co\/KxSPgZzmNL via @thenextweb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TNW",
        "screen_name" : "TheNextWeb",
        "indices" : [ 92, 103 ],
        "id_str" : "10876852",
        "id" : 10876852
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/KxSPgZzmNL",
        "expanded_url" : "http:\/\/tnw.to\/2bCkwUh",
        "display_url" : "tnw.to\/2bCkwUh"
      } ]
    },
    "geo" : { },
    "id_str" : "775249530732306432",
    "text" : "We looked at 137,052 tweets and learned hashtags were worthless https:\/\/t.co\/KxSPgZzmNL via @thenextweb",
    "id" : 775249530732306432,
    "created_at" : "2016-09-12 08:27:46 +0000",
    "user" : {
      "name" : "Guido Deckstein",
      "screen_name" : "guidodeckstein",
      "protected" : false,
      "id_str" : "1010810106",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/990674367221784576\/HpL_TR7__normal.jpg",
      "id" : 1010810106,
      "verified" : false
    }
  },
  "id" : 775268779689439233,
  "created_at" : "2016-09-12 09:44:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Guido Deckstein",
      "screen_name" : "guidodeckstein",
      "indices" : [ 3, 18 ],
      "id_str" : "1010810106",
      "id" : 1010810106
    }, {
      "name" : "TNW",
      "screen_name" : "TheNextWeb",
      "indices" : [ 112, 123 ],
      "id_str" : "10876852",
      "id" : 10876852
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/KxSPgZzmNL",
      "expanded_url" : "http:\/\/tnw.to\/2bCkwUh",
      "display_url" : "tnw.to\/2bCkwUh"
    } ]
  },
  "geo" : { },
  "id_str" : "775268780259717120",
  "text" : "RT @guidodeckstein: We looked at 137,052 tweets and learned hashtags were worthless https:\/\/t.co\/KxSPgZzmNL via @thenextweb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TNW",
        "screen_name" : "TheNextWeb",
        "indices" : [ 92, 103 ],
        "id_str" : "10876852",
        "id" : 10876852
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/KxSPgZzmNL",
        "expanded_url" : "http:\/\/tnw.to\/2bCkwUh",
        "display_url" : "tnw.to\/2bCkwUh"
      } ]
    },
    "geo" : { },
    "id_str" : "775249530732306432",
    "text" : "We looked at 137,052 tweets and learned hashtags were worthless https:\/\/t.co\/KxSPgZzmNL via @thenextweb",
    "id" : 775249530732306432,
    "created_at" : "2016-09-12 08:27:46 +0000",
    "user" : {
      "name" : "Guido Deckstein",
      "screen_name" : "guidodeckstein",
      "protected" : false,
      "id_str" : "1010810106",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/990674367221784576\/HpL_TR7__normal.jpg",
      "id" : 1010810106,
      "verified" : false
    }
  },
  "id" : 775268780259717120,
  "created_at" : "2016-09-12 09:44:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Three Ireland Care",
      "screen_name" : "ThreeCare",
      "indices" : [ 0, 10 ],
      "id_str" : "132937537",
      "id" : 132937537
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/775262789317320704\/photo\/1",
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/E6TdSFPHnj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CsJJY4eWcAExXmf.jpg",
      "id_str" : "775262777686519809",
      "id" : 775262777686519809,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsJJY4eWcAExXmf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 392
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 392
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 392
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 392
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/E6TdSFPHnj"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "775258156347822080",
  "geo" : { },
  "id_str" : "775262789317320704",
  "in_reply_to_user_id" : 132937537,
  "text" : "@ThreeCare It being sorted. Thank You. https:\/\/t.co\/E6TdSFPHnj",
  "id" : 775262789317320704,
  "in_reply_to_status_id" : 775258156347822080,
  "created_at" : "2016-09-12 09:20:27 +0000",
  "in_reply_to_screen_name" : "ThreeCare",
  "in_reply_to_user_id_str" : "132937537",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Three Ireland Care",
      "screen_name" : "ThreeCare",
      "indices" : [ 20, 30 ],
      "id_str" : "132937537",
      "id" : 132937537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "775255832237858816",
  "text" : "Trying to give $ to @ThreeCare but their \"Top up now\" button does not work for some reason.",
  "id" : 775255832237858816,
  "created_at" : "2016-09-12 08:52:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774970091825946628",
  "text" : "Should I deploy my wordpress site to the cloud? Will that save me USD 119 on hosting per year?",
  "id" : 774970091825946628,
  "created_at" : "2016-09-11 13:57:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/DT32dztzrz",
      "expanded_url" : "https:\/\/twitter.com\/LividEye\/status\/774942765796892672",
      "display_url" : "twitter.com\/LividEye\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774945792348024832",
  "text" : "Fascinating. https:\/\/t.co\/DT32dztzrz",
  "id" : 774945792348024832,
  "created_at" : "2016-09-11 12:20:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 29 ],
      "url" : "https:\/\/t.co\/Kdt9y7mQqH",
      "expanded_url" : "https:\/\/twitter.com\/MothershipSG\/status\/774861937096089600",
      "display_url" : "twitter.com\/MothershipSG\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774945316651040768",
  "text" : "None. https:\/\/t.co\/Kdt9y7mQqH",
  "id" : 774945316651040768,
  "created_at" : "2016-09-11 12:18:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LesNews",
      "screen_name" : "LesNews",
      "indices" : [ 3, 11 ],
      "id_str" : "57732899",
      "id" : 57732899
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/BQ9Xv7QAFj",
      "expanded_url" : "https:\/\/www.facebook.com\/itsmaeril\/photos\/a.203397339784701.1073741826.203395096451592\/431426983648401\/?type=3&theater",
      "display_url" : "facebook.com\/itsmaeril\/phot\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774874200100237312",
  "text" : "RT @LesNews: Une artiste fran\u00E7aise publie une bande dessin\u00E9e pour soutenir les victimes d'islamophobie https:\/\/t.co\/BQ9Xv7QAFj https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LesNews\/status\/774762213315842048\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/GVqHyqT3lW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsCCG4DW8AEYlB-.jpg",
        "id_str" : "774762190545022977",
        "id" : 774762190545022977,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsCCG4DW8AEYlB-.jpg",
        "sizes" : [ {
          "h" : 1270,
          "resize" : "fit",
          "w" : 877
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 470
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 829
        }, {
          "h" : 1270,
          "resize" : "fit",
          "w" : 877
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GVqHyqT3lW"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/LesNews\/status\/774762213315842048\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/GVqHyqT3lW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CsCCG4GXgAA7i9M.jpg",
        "id_str" : "774762190557642752",
        "id" : 774762190557642752,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CsCCG4GXgAA7i9M.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 871,
          "resize" : "fit",
          "w" : 871
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 871,
          "resize" : "fit",
          "w" : 871
        }, {
          "h" : 871,
          "resize" : "fit",
          "w" : 871
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GVqHyqT3lW"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/BQ9Xv7QAFj",
        "expanded_url" : "https:\/\/www.facebook.com\/itsmaeril\/photos\/a.203397339784701.1073741826.203395096451592\/431426983648401\/?type=3&theater",
        "display_url" : "facebook.com\/itsmaeril\/phot\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "774762213315842048",
    "text" : "Une artiste fran\u00E7aise publie une bande dessin\u00E9e pour soutenir les victimes d'islamophobie https:\/\/t.co\/BQ9Xv7QAFj https:\/\/t.co\/GVqHyqT3lW",
    "id" : 774762213315842048,
    "created_at" : "2016-09-11 00:11:20 +0000",
    "user" : {
      "name" : "LesNews",
      "screen_name" : "LesNews",
      "protected" : false,
      "id_str" : "57732899",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/757388832601939968\/c35A8HWZ_normal.jpg",
      "id" : 57732899,
      "verified" : false
    }
  },
  "id" : 774874200100237312,
  "created_at" : "2016-09-11 07:36:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/snmPqynslD",
      "expanded_url" : "https:\/\/twitter.com\/GavinDuffy\/status\/774747937209548800",
      "display_url" : "twitter.com\/GavinDuffy\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774873934542102528",
  "text" : "There is always an Irish connection. https:\/\/t.co\/snmPqynslD",
  "id" : 774873934542102528,
  "created_at" : "2016-09-11 07:35:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "indices" : [ 0, 8 ],
      "id_str" : "21071383",
      "id" : 21071383
    }, {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 9, 23 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "774815523309621248",
  "geo" : { },
  "id_str" : "774873013451972612",
  "in_reply_to_user_id" : 21071383,
  "text" : "@joleeqh @ofmeowandbake \u6740\u4EBA\u653E\uD83D\uDD25\uFF1F",
  "id" : 774873013451972612,
  "in_reply_to_status_id" : 774815523309621248,
  "created_at" : "2016-09-11 07:31:37 +0000",
  "in_reply_to_screen_name" : "joleeqh",
  "in_reply_to_user_id_str" : "21071383",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "indices" : [ 3, 9 ],
      "id_str" : "1344951",
      "id" : 1344951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774871179316396032",
  "text" : "RT @WIRED: The Senate just failed to advance the $1.1 billion Zika bill, blocking critical funding to combat the virus. https:\/\/t.co\/YRqH2c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/YRqH2caHn2",
        "expanded_url" : "http:\/\/bit.ly\/2csLBvY",
        "display_url" : "bit.ly\/2csLBvY"
      } ]
    },
    "geo" : { },
    "id_str" : "774845653814022145",
    "text" : "The Senate just failed to advance the $1.1 billion Zika bill, blocking critical funding to combat the virus. https:\/\/t.co\/YRqH2caHn2",
    "id" : 774845653814022145,
    "created_at" : "2016-09-11 05:42:54 +0000",
    "user" : {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "protected" : false,
      "id_str" : "1344951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/615598832726970372\/jsK-gBSt_normal.png",
      "id" : 1344951,
      "verified" : true
    }
  },
  "id" : 774871179316396032,
  "created_at" : "2016-09-11 07:24:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steve Leonard",
      "screen_name" : "steveleonardSG",
      "indices" : [ 3, 18 ],
      "id_str" : "2574271958",
      "id" : 2574271958
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "respect",
      "indices" : [ 109, 117 ]
    }, {
      "text" : "startups",
      "indices" : [ 118, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774870700918177792",
  "text" : "RT @steveleonardSG: Best part is founders chose to build a company \u2013 despite scholarships from top US univ's #respect #startups @iipl https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "respect",
        "indices" : [ 89, 97 ]
      }, {
        "text" : "startups",
        "indices" : [ 98, 107 ]
      } ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/8tz3IGKibM",
        "expanded_url" : "https:\/\/www.techinasia.com\/college-dropouts-raise-2-million-youth-job-site-glints?platform=hootsuite",
        "display_url" : "techinasia.com\/college-dropou\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "774798157846712320",
    "text" : "Best part is founders chose to build a company \u2013 despite scholarships from top US univ's #respect #startups @iipl https:\/\/t.co\/8tz3IGKibM",
    "id" : 774798157846712320,
    "created_at" : "2016-09-11 02:34:10 +0000",
    "user" : {
      "name" : "Steve Leonard",
      "screen_name" : "steveleonardSG",
      "protected" : false,
      "id_str" : "2574271958",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/483116363196542976\/3DLw1LIK_normal.jpeg",
      "id" : 2574271958,
      "verified" : false
    }
  },
  "id" : 774870700918177792,
  "created_at" : "2016-09-11 07:22:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SuzanneHarrington",
      "screen_name" : "soozysuze",
      "indices" : [ 3, 13 ],
      "id_str" : "211460146",
      "id" : 211460146
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WorldSuicidePreventionDay",
      "indices" : [ 15, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774714854603100162",
  "text" : "RT @soozysuze: #WorldSuicidePreventionDay -10 years this week since suicide of my ex husband.  He left 2 kids age 3 and 5.  If only he'd as\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WorldSuicidePreventionDay",
        "indices" : [ 0, 26 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "774627621745360896",
    "text" : "#WorldSuicidePreventionDay -10 years this week since suicide of my ex husband.  He left 2 kids age 3 and 5.  If only he'd asked for help",
    "id" : 774627621745360896,
    "created_at" : "2016-09-10 15:16:31 +0000",
    "user" : {
      "name" : "SuzanneHarrington",
      "screen_name" : "soozysuze",
      "protected" : false,
      "id_str" : "211460146",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1783665177\/me-march2010_020_normal.jpg",
      "id" : 211460146,
      "verified" : false
    }
  },
  "id" : 774714854603100162,
  "created_at" : "2016-09-10 21:03:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/AUtg7bdToB",
      "expanded_url" : "https:\/\/twitter.com\/dalejbarr\/status\/774654470512926720",
      "display_url" : "twitter.com\/dalejbarr\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774703321814802432",
  "text" : "Using R helps to facilitate reproducible research. Not possible with point and click. https:\/\/t.co\/AUtg7bdToB",
  "id" : 774703321814802432,
  "created_at" : "2016-09-10 20:17:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 27 ],
      "url" : "https:\/\/t.co\/MsGaMvdwyJ",
      "expanded_url" : "https:\/\/twitter.com\/MothershipSG\/status\/774562891752103936",
      "display_url" : "twitter.com\/MothershipSG\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774634072169742336",
  "text" : "LoL https:\/\/t.co\/MsGaMvdwyJ",
  "id" : 774634072169742336,
  "created_at" : "2016-09-10 15:42:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Hsien Loong",
      "screen_name" : "leehsienloong",
      "indices" : [ 3, 17 ],
      "id_str" : "34568673",
      "id" : 34568673
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Rio2016",
      "indices" : [ 110, 118 ]
    }, {
      "text" : "Paralympics",
      "indices" : [ 119, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774633827964686336",
  "text" : "RT @leehsienloong: Congrats to Yip Pin Xiu for gold medal &amp; world record win in the 100m backstroke S2 at #Rio2016 #Paralympics! \u2013 LHL http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Rio2016",
        "indices" : [ 91, 99 ]
      }, {
        "text" : "Paralympics",
        "indices" : [ 100, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/Y81z2ZBPJy",
        "expanded_url" : "http:\/\/str.sg\/4par",
        "display_url" : "str.sg\/4par"
      } ]
    },
    "geo" : { },
    "id_str" : "774449391855730688",
    "text" : "Congrats to Yip Pin Xiu for gold medal &amp; world record win in the 100m backstroke S2 at #Rio2016 #Paralympics! \u2013 LHL https:\/\/t.co\/Y81z2ZBPJy",
    "id" : 774449391855730688,
    "created_at" : "2016-09-10 03:28:18 +0000",
    "user" : {
      "name" : "Lee Hsien Loong",
      "screen_name" : "leehsienloong",
      "protected" : false,
      "id_str" : "34568673",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040783094607863808\/j9Pm25dJ_normal.jpg",
      "id" : 34568673,
      "verified" : true
    }
  },
  "id" : 774633827964686336,
  "created_at" : "2016-09-10 15:41:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Kuan Yew School",
      "screen_name" : "LKYSch",
      "indices" : [ 3, 10 ],
      "id_str" : "832683854",
      "id" : 832683854
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/swfPBzhC2N",
      "expanded_url" : "http:\/\/lkyspp.sg\/2cecyyC",
      "display_url" : "lkyspp.sg\/2cecyyC"
    } ]
  },
  "geo" : { },
  "id_str" : "774538132926652417",
  "text" : "RT @LKYSch: What are some lessons that Hong Kong can learn from Singapore on ethnic integration? https:\/\/t.co\/swfPBzhC2N",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/swfPBzhC2N",
        "expanded_url" : "http:\/\/lkyspp.sg\/2cecyyC",
        "display_url" : "lkyspp.sg\/2cecyyC"
      } ]
    },
    "geo" : { },
    "id_str" : "774535645876383745",
    "text" : "What are some lessons that Hong Kong can learn from Singapore on ethnic integration? https:\/\/t.co\/swfPBzhC2N",
    "id" : 774535645876383745,
    "created_at" : "2016-09-10 09:11:02 +0000",
    "user" : {
      "name" : "Lee Kuan Yew School",
      "screen_name" : "LKYSch",
      "protected" : false,
      "id_str" : "832683854",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/770553022900350977\/MYqyR2VV_normal.jpg",
      "id" : 832683854,
      "verified" : false
    }
  },
  "id" : 774538132926652417,
  "created_at" : "2016-09-10 09:20:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/YbQ53dGOST",
      "expanded_url" : "https:\/\/github.com\/mryap\/Programming-for-Big-Data-Assignment",
      "display_url" : "github.com\/mryap\/Programm\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774537805397651457",
  "text" : "I felt the endorphins being released when I moved files and codes around on my GitHub page. I am a geek! https:\/\/t.co\/YbQ53dGOST",
  "id" : 774537805397651457,
  "created_at" : "2016-09-10 09:19:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Creed TD",
      "screen_name" : "creedcnw",
      "indices" : [ 3, 12 ],
      "id_str" : "386584224",
      "id" : 386584224
    }, {
      "name" : "Irish Food Board",
      "screen_name" : "Bordbia",
      "indices" : [ 34, 42 ],
      "id_str" : "48673814",
      "id" : 48673814
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TradeAsia16",
      "indices" : [ 108, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774525449485582336",
  "text" : "RT @creedcnw: Proud to launch the @Bordbia Singapore office spreading our Origin Green story across SE Asia #TradeAsia16 https:\/\/t.co\/XB01L\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Food Board",
        "screen_name" : "Bordbia",
        "indices" : [ 20, 28 ],
        "id_str" : "48673814",
        "id" : 48673814
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/creedcnw\/status\/774508315040739328\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/XB01L2toJY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cr-bNBFXEAAbla3.jpg",
        "id_str" : "774508308862537728",
        "id" : 774508308862537728,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cr-bNBFXEAAbla3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1153
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2672,
          "resize" : "fit",
          "w" : 1504
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XB01L2toJY"
      } ],
      "hashtags" : [ {
        "text" : "TradeAsia16",
        "indices" : [ 94, 106 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "774508315040739328",
    "text" : "Proud to launch the @Bordbia Singapore office spreading our Origin Green story across SE Asia #TradeAsia16 https:\/\/t.co\/XB01L2toJY",
    "id" : 774508315040739328,
    "created_at" : "2016-09-10 07:22:26 +0000",
    "user" : {
      "name" : "Michael Creed TD",
      "screen_name" : "creedcnw",
      "protected" : false,
      "id_str" : "386584224",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/862294331675348992\/hmLcvTnA_normal.jpg",
      "id" : 386584224,
      "verified" : true
    }
  },
  "id" : 774525449485582336,
  "created_at" : "2016-09-10 08:30:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 9, 22 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774517827625713664",
  "text" : "Congrats @DigiWomenIRL on election to National Women's Council Of Ireland.",
  "id" : 774517827625713664,
  "created_at" : "2016-09-10 08:00:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 31 ],
      "url" : "https:\/\/t.co\/46sOVQw63Q",
      "expanded_url" : "https:\/\/twitter.com\/dublinbusnews\/status\/774401779941408769",
      "display_url" : "twitter.com\/dublinbusnews\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774503556707680256",
  "text" : "Yeah! \uD83D\uDC4F https:\/\/t.co\/46sOVQw63Q",
  "id" : 774503556707680256,
  "created_at" : "2016-09-10 07:03:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 3, 12 ],
      "id_str" : "45756727",
      "id" : 45756727
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Paralympics",
      "indices" : [ 77, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/rKzmv8PCQE",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/774417081659826176",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774502987293159424",
  "text" : "RT @LividEye: She deserved the same amount of honour and glory as Schooling. #Paralympics https:\/\/t.co\/rKzmv8PCQE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Paralympics",
        "indices" : [ 63, 75 ]
      } ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/rKzmv8PCQE",
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/774417081659826176",
        "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "774431605196632064",
    "text" : "She deserved the same amount of honour and glory as Schooling. #Paralympics https:\/\/t.co\/rKzmv8PCQE",
    "id" : 774431605196632064,
    "created_at" : "2016-09-10 02:17:37 +0000",
    "user" : {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "protected" : false,
      "id_str" : "45756727",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973927451775545344\/8ZHNGFY8_normal.jpg",
      "id" : 45756727,
      "verified" : false
    }
  },
  "id" : 774502987293159424,
  "created_at" : "2016-09-10 07:01:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    }, {
      "name" : "WITS Ireland",
      "screen_name" : "WITSIreland",
      "indices" : [ 21, 33 ],
      "id_str" : "92056570",
      "id" : 92056570
    }, {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 53, 66 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NWCIAGM",
      "indices" : [ 35, 43 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774502841469767680",
  "text" : "RT @DigiWomenIRL: RT @WITSIreland: #NWCIAGM Congrats @DigiWomenIRL on your election to NWCI board. Best wishes for your two years.&lt; Thank y\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "WITS Ireland",
        "screen_name" : "WITSIreland",
        "indices" : [ 3, 15 ],
        "id_str" : "92056570",
        "id" : 92056570
      }, {
        "name" : "DigiWomen",
        "screen_name" : "DigiWomenIRL",
        "indices" : [ 35, 48 ],
        "id_str" : "1221157964",
        "id" : 1221157964
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "NWCIAGM",
        "indices" : [ 17, 25 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "774499413658103808",
    "text" : "RT @WITSIreland: #NWCIAGM Congrats @DigiWomenIRL on your election to NWCI board. Best wishes for your two years.&lt; Thank you",
    "id" : 774499413658103808,
    "created_at" : "2016-09-10 06:47:04 +0000",
    "user" : {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "protected" : false,
      "id_str" : "1221157964",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527003323623145473\/C2yyI2ob_normal.png",
      "id" : 1221157964,
      "verified" : false
    }
  },
  "id" : 774502841469767680,
  "created_at" : "2016-09-10 07:00:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ciara",
      "screen_name" : "CiaraMPSI",
      "indices" : [ 3, 13 ],
      "id_str" : "832318844064690184",
      "id" : 832318844064690184
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/CiaraMPSI\/status\/773851813988671488\/photo\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/2mT7OtJ689",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cr1GGK-XEAA-pe-.jpg",
      "id_str" : "773851782816665600",
      "id" : 773851782816665600,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cr1GGK-XEAA-pe-.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 689
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 390
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1176
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1176
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2mT7OtJ689"
    } ],
    "hashtags" : [ {
      "text" : "repealthe8th",
      "indices" : [ 50, 63 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774287515306037248",
  "text" : "RT @CiaraMPSI: The 8th has an impact on everyone. #repealthe8th https:\/\/t.co\/2mT7OtJ689",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CiaraMPSI\/status\/773851813988671488\/photo\/1",
        "indices" : [ 49, 72 ],
        "url" : "https:\/\/t.co\/2mT7OtJ689",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cr1GGK-XEAA-pe-.jpg",
        "id_str" : "773851782816665600",
        "id" : 773851782816665600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cr1GGK-XEAA-pe-.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 689
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 390
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1176
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1176
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2mT7OtJ689"
      } ],
      "hashtags" : [ {
        "text" : "repealthe8th",
        "indices" : [ 35, 48 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773851813988671488",
    "text" : "The 8th has an impact on everyone. #repealthe8th https:\/\/t.co\/2mT7OtJ689",
    "id" : 773851813988671488,
    "created_at" : "2016-09-08 11:53:44 +0000",
    "user" : {
      "name" : "Ciara\u00EDoch \uD83C\uDFA8",
      "screen_name" : "Ciaraioch",
      "protected" : false,
      "id_str" : "4872636172",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001366011986235393\/BO-_RsKO_normal.jpg",
      "id" : 4872636172,
      "verified" : false
    }
  },
  "id" : 774287515306037248,
  "created_at" : "2016-09-09 16:45:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774278411225337856",
  "text" : "RT @topgold: Whoa! $10bn wiped off the value of Samsung after FAA cites Note 7 as fire hazard.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "774273411178696708",
    "text" : "Whoa! $10bn wiped off the value of Samsung after FAA cites Note 7 as fire hazard.",
    "id" : 774273411178696708,
    "created_at" : "2016-09-09 15:49:01 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 774278411225337856,
  "created_at" : "2016-09-09 16:08:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774237673598885888",
  "text" : "Where should I park my curated material?",
  "id" : 774237673598885888,
  "created_at" : "2016-09-09 13:27:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/qutEJBd6ZV",
      "expanded_url" : "http:\/\/mothership.sg\/2016\/09\/fairprice-xtras-put-tank-in-a-mall-makes-mainstream-sense-now-following-obama-slur\/",
      "display_url" : "mothership.sg\/2016\/09\/fairpr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774231975762821121",
  "text" : "FairPrice Xtra\u2019s \u2018Put tank in a mall\u2019 makes mainstream sense now following Obama slur https:\/\/t.co\/qutEJBd6ZV",
  "id" : 774231975762821121,
  "created_at" : "2016-09-09 13:04:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774229335104425985",
  "text" : "Curated all data science material to Github. Good bye Evernote. Don't put all the eggs in one basket.",
  "id" : 774229335104425985,
  "created_at" : "2016-09-09 12:53:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 35 ],
      "url" : "https:\/\/t.co\/fVNK20yqYK",
      "expanded_url" : "https:\/\/twitter.com\/hellofrmSG\/status\/774179641800036353",
      "display_url" : "twitter.com\/hellofrmSG\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774186030647902208",
  "text" : "I agreeded. https:\/\/t.co\/fVNK20yqYK",
  "id" : 774186030647902208,
  "created_at" : "2016-09-09 10:01:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/WOrAehKw7F",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BKISlO2DqxT\/",
      "display_url" : "instagram.com\/p\/BKISlO2DqxT\/"
    } ]
  },
  "geo" : { },
  "id_str" : "774172115603894272",
  "text" : "The sisters are all behind you so keep pushing! https:\/\/t.co\/WOrAehKw7F",
  "id" : 774172115603894272,
  "created_at" : "2016-09-09 09:06:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "indices" : [ 3, 11 ],
      "id_str" : "21071383",
      "id" : 21071383
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/6oEbGno4Ah",
      "expanded_url" : "https:\/\/twitter.com\/kibblesmith\/status\/773986537553981441",
      "display_url" : "twitter.com\/kibblesmith\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774170721148768256",
  "text" : "RT @joleeqh: LOL.  https:\/\/t.co\/6oEbGno4Ah",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 6, 29 ],
        "url" : "https:\/\/t.co\/6oEbGno4Ah",
        "expanded_url" : "https:\/\/twitter.com\/kibblesmith\/status\/773986537553981441",
        "display_url" : "twitter.com\/kibblesmith\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "774082412171014144",
    "text" : "LOL.  https:\/\/t.co\/6oEbGno4Ah",
    "id" : 774082412171014144,
    "created_at" : "2016-09-09 03:10:03 +0000",
    "user" : {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "protected" : false,
      "id_str" : "21071383",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/435265895854641152\/QCz6O1FM_normal.png",
      "id" : 21071383,
      "verified" : false
    }
  },
  "id" : 774170721148768256,
  "created_at" : "2016-09-09 09:00:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Hannan",
      "screen_name" : "DanielJHannan",
      "indices" : [ 3, 17 ],
      "id_str" : "85794542",
      "id" : 85794542
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774169241629003776",
  "text" : "RT @DanielJHannan: We don't want a binary school system. We want pluralism: grammar schools, technical schools, academies and specialist sc\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "774135814506713088",
    "text" : "We don't want a binary school system. We want pluralism: grammar schools, technical schools, academies and specialist schools of all kinds.",
    "id" : 774135814506713088,
    "created_at" : "2016-09-09 06:42:15 +0000",
    "user" : {
      "name" : "Daniel Hannan",
      "screen_name" : "DanielJHannan",
      "protected" : false,
      "id_str" : "85794542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/929023116755451905\/DoiHXe1M_normal.jpg",
      "id" : 85794542,
      "verified" : true
    }
  },
  "id" : 774169241629003776,
  "created_at" : "2016-09-09 08:55:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774018397973524480",
  "text" : "Switching Blog from Wordpress to GitHub signify the switch of my career.",
  "id" : 774018397973524480,
  "created_at" : "2016-09-08 22:55:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justin Lahart",
      "screen_name" : "jdlahart",
      "indices" : [ 3, 12 ],
      "id_str" : "22810429",
      "id" : 22810429
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774013670678532097",
  "text" : "RT @jdlahart: The hipsters of 2046 will be really into headphone jacks",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773538542366629888",
    "text" : "The hipsters of 2046 will be really into headphone jacks",
    "id" : 773538542366629888,
    "created_at" : "2016-09-07 15:08:54 +0000",
    "user" : {
      "name" : "Justin Lahart",
      "screen_name" : "jdlahart",
      "protected" : false,
      "id_str" : "22810429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1337028141\/DSC_0162_normal.jpg",
      "id" : 22810429,
      "verified" : true
    }
  },
  "id" : 774013670678532097,
  "created_at" : "2016-09-08 22:36:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Parag K. Mital",
      "screen_name" : "pkmital",
      "indices" : [ 3, 11 ],
      "id_str" : "44698222",
      "id" : 44698222
    }, {
      "name" : "Microsoft Azure",
      "screen_name" : "Azure",
      "indices" : [ 23, 29 ],
      "id_str" : "17000457",
      "id" : 17000457
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pkmital\/status\/761744889499361280\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/GD7knVSo7F",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CpJCciXUkAA71gA.jpg",
      "id_str" : "761744345007427584",
      "id" : 761744345007427584,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CpJCciXUkAA71gA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1130,
        "resize" : "fit",
        "w" : 1940
      }, {
        "h" : 1130,
        "resize" : "fit",
        "w" : 1940
      }, {
        "h" : 699,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GD7knVSo7F"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "774011043077058560",
  "text" : "RT @pkmital: Microsoft @azure's GPU offerings are insane.  Exciting times https:\/\/t.co\/GD7knVSo7F",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Microsoft Azure",
        "screen_name" : "Azure",
        "indices" : [ 10, 16 ],
        "id_str" : "17000457",
        "id" : 17000457
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pkmital\/status\/761744889499361280\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/GD7knVSo7F",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CpJCciXUkAA71gA.jpg",
        "id_str" : "761744345007427584",
        "id" : 761744345007427584,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CpJCciXUkAA71gA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1130,
          "resize" : "fit",
          "w" : 1940
        }, {
          "h" : 1130,
          "resize" : "fit",
          "w" : 1940
        }, {
          "h" : 699,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GD7knVSo7F"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "761744889499361280",
    "text" : "Microsoft @azure's GPU offerings are insane.  Exciting times https:\/\/t.co\/GD7knVSo7F",
    "id" : 761744889499361280,
    "created_at" : "2016-08-06 02:05:08 +0000",
    "user" : {
      "name" : "Parag K. Mital",
      "screen_name" : "pkmital",
      "protected" : false,
      "id_str" : "44698222",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/636221351003422720\/klDHTq8I_normal.png",
      "id" : 44698222,
      "verified" : false
    }
  },
  "id" : 774011043077058560,
  "created_at" : "2016-09-08 22:26:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/XBZhaQ5YS0",
      "expanded_url" : "http:\/\/www.inis.gov.ie\/en\/INIS\/Pages\/new-immigration-appointment-system",
      "display_url" : "inis.gov.ie\/en\/INIS\/Pages\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "774009200078909440",
  "text" : "This could not comes soon enough. Nearly a decade for them to implement this https:\/\/t.co\/XBZhaQ5YS0",
  "id" : 774009200078909440,
  "created_at" : "2016-09-08 22:19:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/9NzPD9htiW",
      "expanded_url" : "http:\/\/futurism.com\/tesla-model-s-driver-dies-after-crashing-into-a-tree-in-the-netherlands\/",
      "display_url" : "futurism.com\/tesla-model-s-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773998794195214336",
  "text" : "Tesla Model S Driver Dies After Crashing Into a Tree in the Netherlands https:\/\/t.co\/9NzPD9htiW",
  "id" : 773998794195214336,
  "created_at" : "2016-09-08 21:37:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773996734141849600",
  "text" : "@bharatidalela like to hear more about it from you.",
  "id" : 773996734141849600,
  "created_at" : "2016-09-08 21:29:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773996217063895041",
  "text" : "Using GitHub with R and RStudio works for me very well.  Now pushing all assignment and project to GitHub.",
  "id" : 773996217063895041,
  "created_at" : "2016-09-08 21:27:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TJ_FactCheck",
      "screen_name" : "TJ_FactCheck",
      "indices" : [ 3, 16 ],
      "id_str" : "745633307447136258",
      "id" : 745633307447136258
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773990092876681217",
  "text" : "RT @TJ_FactCheck: Imelda Munster says 79% bus service revenue in Lyon is from subsidy, 27% in Dublin. May still be true of Lyon, but data i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rtept",
        "indices" : [ 134, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773989131705786370",
    "text" : "Imelda Munster says 79% bus service revenue in Lyon is from subsidy, 27% in Dublin. May still be true of Lyon, but data is from 2007. #rtept",
    "id" : 773989131705786370,
    "created_at" : "2016-09-08 20:59:23 +0000",
    "user" : {
      "name" : "TJ_FactCheck",
      "screen_name" : "TJ_FactCheck",
      "protected" : false,
      "id_str" : "745633307447136258",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/755391342554017799\/mHr2gZKK_normal.jpg",
      "id" : 745633307447136258,
      "verified" : true
    }
  },
  "id" : 773990092876681217,
  "created_at" : "2016-09-08 21:03:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shower Thoughts",
      "screen_name" : "TheWeirdWorld",
      "indices" : [ 3, 17 ],
      "id_str" : "487736815",
      "id" : 487736815
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheWeirdWorld\/status\/773949469679357953\/photo\/1",
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/LZBc83ff9c",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cr2e7vRW8AAekkf.jpg",
      "id_str" : "773949460116336640",
      "id" : 773949460116336640,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cr2e7vRW8AAekkf.jpg",
      "sizes" : [ {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LZBc83ff9c"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773985371004764160",
  "text" : "RT @TheWeirdWorld: https:\/\/t.co\/LZBc83ff9c",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheWeirdWorld\/status\/773949469679357953\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/LZBc83ff9c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cr2e7vRW8AAekkf.jpg",
        "id_str" : "773949460116336640",
        "id" : 773949460116336640,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cr2e7vRW8AAekkf.jpg",
        "sizes" : [ {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LZBc83ff9c"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773949469679357953",
    "text" : "https:\/\/t.co\/LZBc83ff9c",
    "id" : 773949469679357953,
    "created_at" : "2016-09-08 18:21:47 +0000",
    "user" : {
      "name" : "Shower Thoughts",
      "screen_name" : "TheWeirdWorld",
      "protected" : false,
      "id_str" : "487736815",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/755109975790350336\/8hHO0Yr4_normal.jpg",
      "id" : 487736815,
      "verified" : false
    }
  },
  "id" : 773985371004764160,
  "created_at" : "2016-09-08 20:44:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/S8qpon3TUv",
      "expanded_url" : "http:\/\/cnn.it\/2c9ALpI",
      "display_url" : "cnn.it\/2c9ALpI"
    } ]
  },
  "geo" : { },
  "id_str" : "773984976652144640",
  "text" : "'What is Aleppo?' https:\/\/t.co\/S8qpon3TUv",
  "id" : 773984976652144640,
  "created_at" : "2016-09-08 20:42:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Debbie Hutchinson",
      "screen_name" : "GuevarasGirl",
      "indices" : [ 3, 16 ],
      "id_str" : "28445181",
      "id" : 28445181
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ChildSummit",
      "indices" : [ 18, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773926113882103808",
  "text" : "RT @GuevarasGirl: #ChildSummit most shocking stat is Irl has highest rate of suicide in EU for teenage girls, second highest for teen boys!!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ChildSummit",
        "indices" : [ 0, 12 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773875751632441344",
    "text" : "#ChildSummit most shocking stat is Irl has highest rate of suicide in EU for teenage girls, second highest for teen boys!!",
    "id" : 773875751632441344,
    "created_at" : "2016-09-08 13:28:51 +0000",
    "user" : {
      "name" : "Debbie Hutchinson",
      "screen_name" : "GuevarasGirl",
      "protected" : false,
      "id_str" : "28445181",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001166705345925121\/wu7_NtvT_normal.jpg",
      "id" : 28445181,
      "verified" : false
    }
  },
  "id" : 773926113882103808,
  "created_at" : "2016-09-08 16:48:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/dYAUTFMurM",
      "expanded_url" : "https:\/\/www.microsoftazurepass.com\/azureu",
      "display_url" : "microsoftazurepass.com\/azureu"
    } ]
  },
  "geo" : { },
  "id_str" : "773875780476628993",
  "text" : "Microsoft Azure Pass program is now oversubscribed, MS will no longer be issuing free Azure passcodes. https:\/\/t.co\/dYAUTFMurM",
  "id" : 773875780476628993,
  "created_at" : "2016-09-08 13:28:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/WzfXoGIYu2",
      "expanded_url" : "http:\/\/www.thejournal.ie\/manchester\/news\/",
      "display_url" : "thejournal.ie\/manchester\/new\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773838589092298752",
  "text" : "Now I have second thoughts of going over to Manchester. https:\/\/t.co\/WzfXoGIYu2",
  "id" : 773838589092298752,
  "created_at" : "2016-09-08 11:01:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 3, 12 ],
      "id_str" : "136690988",
      "id" : 136690988
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773835189147541504",
  "text" : "RT @ailieirv: \"The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself. There\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.pagemodo.com\" rel=\"nofollow\"\u003EPagemodo.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773830758616236033",
    "text" : "\"The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself. Therefore, all pr...",
    "id" : 773830758616236033,
    "created_at" : "2016-09-08 10:30:04 +0000",
    "user" : {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "protected" : false,
      "id_str" : "136690988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528642221956796416\/WDp_wLcX_normal.jpeg",
      "id" : 136690988,
      "verified" : false
    }
  },
  "id" : 773835189147541504,
  "created_at" : "2016-09-08 10:47:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lee Kuan Yew School",
      "screen_name" : "LKYSch",
      "indices" : [ 3, 10 ],
      "id_str" : "832683854",
      "id" : 832683854
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/yle2gav5m6",
      "expanded_url" : "http:\/\/lkyspp.sg\/2cnDFJf",
      "display_url" : "lkyspp.sg\/2cnDFJf"
    } ]
  },
  "geo" : { },
  "id_str" : "773826024811401216",
  "text" : "RT @LKYSch: Singapore suffers from a huge knowledge-skills gap despite high standards of education. https:\/\/t.co\/yle2gav5m6 https:\/\/t.co\/ut\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LKYSch\/status\/773810878927994880\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/utexEQvJXo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cr0g5HDWYAEJ9XL.jpg",
        "id_str" : "773810876495323137",
        "id" : 773810876495323137,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cr0g5HDWYAEJ9XL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/utexEQvJXo"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/yle2gav5m6",
        "expanded_url" : "http:\/\/lkyspp.sg\/2cnDFJf",
        "display_url" : "lkyspp.sg\/2cnDFJf"
      } ]
    },
    "geo" : { },
    "id_str" : "773810878927994880",
    "text" : "Singapore suffers from a huge knowledge-skills gap despite high standards of education. https:\/\/t.co\/yle2gav5m6 https:\/\/t.co\/utexEQvJXo",
    "id" : 773810878927994880,
    "created_at" : "2016-09-08 09:11:04 +0000",
    "user" : {
      "name" : "Lee Kuan Yew School",
      "screen_name" : "LKYSch",
      "protected" : false,
      "id_str" : "832683854",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/770553022900350977\/MYqyR2VV_normal.jpg",
      "id" : 832683854,
      "verified" : false
    }
  },
  "id" : 773826024811401216,
  "created_at" : "2016-09-08 10:11:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/Cw8ilTK755",
      "expanded_url" : "http:\/\/arxiv.org\/pdf\/1606.05611.pdf",
      "display_url" : "arxiv.org\/pdf\/1606.05611\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773825590835175425",
  "text" : "Using NLP &amp; ML to help HR improve the quality and speed of the whole recruiting process.https:\/\/t.co\/Cw8ilTK755",
  "id" : 773825590835175425,
  "created_at" : "2016-09-08 10:09:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Fire Brigade",
      "screen_name" : "DubFireBrigade",
      "indices" : [ 3, 18 ],
      "id_str" : "2798058029",
      "id" : 2798058029
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DublinBusStrike",
      "indices" : [ 68, 84 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773797382333009920",
  "text" : "RT @DubFireBrigade: Potentially more cyclists in Dublin with todays #DublinBusStrike, try and leave 1.5m as you pass- stay alive at 1.5 htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DubFireBrigade\/status\/773793088867225600\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/rTbwGSgXC3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CryJYmwXYAAmvBf.jpg",
        "id_str" : "773644291814154240",
        "id" : 773644291814154240,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CryJYmwXYAAmvBf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rTbwGSgXC3"
      } ],
      "hashtags" : [ {
        "text" : "DublinBusStrike",
        "indices" : [ 48, 64 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773793088867225600",
    "text" : "Potentially more cyclists in Dublin with todays #DublinBusStrike, try and leave 1.5m as you pass- stay alive at 1.5 https:\/\/t.co\/rTbwGSgXC3",
    "id" : 773793088867225600,
    "created_at" : "2016-09-08 08:00:23 +0000",
    "user" : {
      "name" : "Dublin Fire Brigade",
      "screen_name" : "DubFireBrigade",
      "protected" : false,
      "id_str" : "2798058029",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013704894518349824\/a65k7PEh_normal.jpg",
      "id" : 2798058029,
      "verified" : true
    }
  },
  "id" : 773797382333009920,
  "created_at" : "2016-09-08 08:17:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WeAreXpats - Tanya \u82D4\u5A05",
      "screen_name" : "WeAreXpats",
      "indices" : [ 3, 14 ],
      "id_str" : "4327719623",
      "id" : 4327719623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773786760505679872",
  "text" : "RT @WeAreXpats: Plenty of excitement in Japan after Kei Nishikori beat Andy Murray in the US open.  Follows on from a good year for Japan i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "773682909710422016",
    "text" : "Plenty of excitement in Japan after Kei Nishikori beat Andy Murray in the US open.  Follows on from a good year for Japan in the Olympics.",
    "id" : 773682909710422016,
    "created_at" : "2016-09-08 00:42:34 +0000",
    "user" : {
      "name" : "WeAreXpats - Tanya \u82D4\u5A05",
      "screen_name" : "WeAreXpats",
      "protected" : false,
      "id_str" : "4327719623",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039137255330406406\/QvCf4-2e_normal.jpg",
      "id" : 4327719623,
      "verified" : false
    }
  },
  "id" : 773786760505679872,
  "created_at" : "2016-09-08 07:35:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/zAHBKhaCEc",
      "expanded_url" : "https:\/\/twitter.com\/StatsInTheWild\/status\/773641804403052544",
      "display_url" : "twitter.com\/StatsInTheWild\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773644830530502658",
  "text" : "winning with data https:\/\/t.co\/zAHBKhaCEc",
  "id" : 773644830530502658,
  "created_at" : "2016-09-07 22:11:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773642533935120384",
  "text" : "Interesting. Add a github profile to my CV and see my salary prediction increase by $1k.",
  "id" : 773642533935120384,
  "created_at" : "2016-09-07 22:02:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 0, 14 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773626247771459584",
  "text" : "#irishbizparty From social media data to CRM to Google Analytics, I help business better inform with data",
  "id" : 773626247771459584,
  "created_at" : "2016-09-07 20:57:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/s08zhQq5eT",
      "expanded_url" : "https:\/\/www.ft.com\/content\/2e4c61f2-4ec8-11e6-8172-e39ecd3b86fc",
      "display_url" : "ft.com\/content\/2e4c61\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773624660843982848",
  "text" : "\"Mathematics in Singapore is not about knowing everything. It\u2019s about thinking like a mathematician\" https:\/\/t.co\/s08zhQq5eT",
  "id" : 773624660843982848,
  "created_at" : "2016-09-07 20:51:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/xRpXkHOC3H",
      "expanded_url" : "http:\/\/www.standard.co.uk\/Front\/airasia-flight-lands-in-melbourne-rather-than-malaysia-after-accidental-6000km-detour-a3338631.html",
      "display_url" : "standard.co.uk\/Front\/airasia-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773624064279769088",
  "text" : "Plane lands in Melbourne instead of Malaysia after pilot error https:\/\/t.co\/xRpXkHOC3H",
  "id" : 773624064279769088,
  "created_at" : "2016-09-07 20:48:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 3, 17 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773622297932525568",
  "text" : "hi #irishbizparty folks, long time no see!",
  "id" : 773622297932525568,
  "created_at" : "2016-09-07 20:41:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Reproducibility",
      "indices" : [ 85, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773620344905138176",
  "text" : "That epic feeling when you manage to push a project into github. I forgot the steps! #Reproducibility&amp;Replicability",
  "id" : 773620344905138176,
  "created_at" : "2016-09-07 20:33:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pedro Domingos",
      "screen_name" : "pmddomingos",
      "indices" : [ 3, 15 ],
      "id_str" : "3270678680",
      "id" : 3270678680
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/tUcz3ujrkT",
      "expanded_url" : "https:\/\/twitter.com\/cesifoti\/status\/773493878326427649",
      "display_url" : "twitter.com\/cesifoti\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773618735475228672",
  "text" : "RT @pmddomingos: Every decision-maker should read this. https:\/\/t.co\/tUcz3ujrkT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/tUcz3ujrkT",
        "expanded_url" : "https:\/\/twitter.com\/cesifoti\/status\/773493878326427649",
        "display_url" : "twitter.com\/cesifoti\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773582400056008704",
    "text" : "Every decision-maker should read this. https:\/\/t.co\/tUcz3ujrkT",
    "id" : 773582400056008704,
    "created_at" : "2016-09-07 18:03:11 +0000",
    "user" : {
      "name" : "Pedro Domingos",
      "screen_name" : "pmddomingos",
      "protected" : false,
      "id_str" : "3270678680",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/618287071187177473\/T_zdLZ7I_normal.jpg",
      "id" : 3270678680,
      "verified" : false
    }
  },
  "id" : 773618735475228672,
  "created_at" : "2016-09-07 20:27:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TJ_FactCheck",
      "screen_name" : "TJ_FactCheck",
      "indices" : [ 3, 16 ],
      "id_str" : "745633307447136258",
      "id" : 745633307447136258
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/wruOhjG7Y4",
      "expanded_url" : "http:\/\/jrnl.ie\/2967501f",
      "display_url" : "jrnl.ie\/2967501f"
    } ]
  },
  "geo" : { },
  "id_str" : "773618526594695168",
  "text" : "RT @TJ_FactCheck: We got out the red pen and went through this Dublin Bus drivers' leaflet on the strikes. https:\/\/t.co\/wruOhjG7Y4 https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TJ_FactCheck\/status\/773617992722677765\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/gRuY7Y1vTF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrxxZiGWgAAH0gX.jpg",
        "id_str" : "773617919464996864",
        "id" : 773617919464996864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrxxZiGWgAAH0gX.jpg",
        "sizes" : [ {
          "h" : 924,
          "resize" : "fit",
          "w" : 1304
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 850,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 482,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 924,
          "resize" : "fit",
          "w" : 1304
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gRuY7Y1vTF"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/wruOhjG7Y4",
        "expanded_url" : "http:\/\/jrnl.ie\/2967501f",
        "display_url" : "jrnl.ie\/2967501f"
      } ]
    },
    "geo" : { },
    "id_str" : "773617992722677765",
    "text" : "We got out the red pen and went through this Dublin Bus drivers' leaflet on the strikes. https:\/\/t.co\/wruOhjG7Y4 https:\/\/t.co\/gRuY7Y1vTF",
    "id" : 773617992722677765,
    "created_at" : "2016-09-07 20:24:37 +0000",
    "user" : {
      "name" : "TJ_FactCheck",
      "screen_name" : "TJ_FactCheck",
      "protected" : false,
      "id_str" : "745633307447136258",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/755391342554017799\/mHr2gZKK_normal.jpg",
      "id" : 745633307447136258,
      "verified" : true
    }
  },
  "id" : 773618526594695168,
  "created_at" : "2016-09-07 20:26:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/2TMkyZOygH",
      "expanded_url" : "https:\/\/twitter.com\/eviltofu\/status\/773594941977092096",
      "display_url" : "twitter.com\/eviltofu\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773613382998093828",
  "text" : "Long overdue. The last one is 2012? https:\/\/t.co\/2TMkyZOygH",
  "id" : 773613382998093828,
  "created_at" : "2016-09-07 20:06:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Washington Post",
      "screen_name" : "washingtonpost",
      "indices" : [ 3, 18 ],
      "id_str" : "2467791",
      "id" : 2467791
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/Y1HIAN22dU",
      "expanded_url" : "http:\/\/wapo.st\/2ct7hUk",
      "display_url" : "wapo.st\/2ct7hUk"
    } ]
  },
  "geo" : { },
  "id_str" : "773587718706237440",
  "text" : "RT @washingtonpost: The Tasmanian tiger went extinct 80 years ago today. But that took decades to figure out. https:\/\/t.co\/Y1HIAN22dU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/Y1HIAN22dU",
        "expanded_url" : "http:\/\/wapo.st\/2ct7hUk",
        "display_url" : "wapo.st\/2ct7hUk"
      } ]
    },
    "geo" : { },
    "id_str" : "773563252559183872",
    "text" : "The Tasmanian tiger went extinct 80 years ago today. But that took decades to figure out. https:\/\/t.co\/Y1HIAN22dU",
    "id" : 773563252559183872,
    "created_at" : "2016-09-07 16:47:06 +0000",
    "user" : {
      "name" : "Washington Post",
      "screen_name" : "washingtonpost",
      "protected" : false,
      "id_str" : "2467791",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875440182501277696\/n-Ic9nBO_normal.jpg",
      "id" : 2467791,
      "verified" : true
    }
  },
  "id" : 773587718706237440,
  "created_at" : "2016-09-07 18:24:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DeepLearning",
      "indices" : [ 64, 77 ]
    }, {
      "text" : "MachineLearning",
      "indices" : [ 114, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773587588502458368",
  "text" : "RT @randal_olson: Wow! $93,562,000 awarded by Canadian Gov. for #DeepLearning research at University of Montreal. #MachineLearning\n\nhttps:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DeepLearning",
        "indices" : [ 46, 59 ]
      }, {
        "text" : "MachineLearning",
        "indices" : [ 96, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/8Vr00rRqqf",
        "expanded_url" : "http:\/\/www.cfref-apogee.gc.ca\/results-resultats\/index-eng.aspx#a6",
        "display_url" : "cfref-apogee.gc.ca\/results-result\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773570335023169536",
    "text" : "Wow! $93,562,000 awarded by Canadian Gov. for #DeepLearning research at University of Montreal. #MachineLearning\n\nhttps:\/\/t.co\/8Vr00rRqqf",
    "id" : 773570335023169536,
    "created_at" : "2016-09-07 17:15:14 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 773587588502458368,
  "created_at" : "2016-09-07 18:23:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/VRFHa5pSDf",
      "expanded_url" : "https:\/\/www.theguardian.com\/world\/2016\/sep\/06\/uk-immigration-minister-confirms-work-will-begin-on-big-new-wall-in-calais?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/world\/2016\/sep\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773585610477010945",
  "text" : "RT @lisaocarroll: Trump will love this :UK immigration minister confirms work to start on \u00A31.9m Calais wall https:\/\/t.co\/VRFHa5pSDf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/VRFHa5pSDf",
        "expanded_url" : "https:\/\/www.theguardian.com\/world\/2016\/sep\/06\/uk-immigration-minister-confirms-work-will-begin-on-big-new-wall-in-calais?CMP=share_btn_tw",
        "display_url" : "theguardian.com\/world\/2016\/sep\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773584149437374464",
    "text" : "Trump will love this :UK immigration minister confirms work to start on \u00A31.9m Calais wall https:\/\/t.co\/VRFHa5pSDf",
    "id" : 773584149437374464,
    "created_at" : "2016-09-07 18:10:08 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 773585610477010945,
  "created_at" : "2016-09-07 18:15:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/cHhtKvPfzc",
      "expanded_url" : "http:\/\/andrewchen.co\/the-next-feature-fallacy-the-fallacy-that-the-next-new-feature-will-suddenly-make-people-use-your-product\/",
      "display_url" : "andrewchen.co\/the-next-featu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773530983647875072",
  "text" : "The fallacy that the next new feature (unless it solve a problem) will suddenly make people use your product https:\/\/t.co\/cHhtKvPfzc",
  "id" : 773530983647875072,
  "created_at" : "2016-09-07 14:38:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/CU4zf8noH9",
      "expanded_url" : "https:\/\/twitter.com\/ChuiSquared\/status\/773524478097821696",
      "display_url" : "twitter.com\/ChuiSquared\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773526985649905664",
  "text" : "I once came across a TV celebrity chef stick the chopsticks upright....oh dear https:\/\/t.co\/CU4zf8noH9",
  "id" : 773526985649905664,
  "created_at" : "2016-09-07 14:22:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "indices" : [ 3, 16 ],
      "id_str" : "205451951",
      "id" : 205451951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773514461613322240",
  "text" : "RT @TheChrisBray: Thanks to the following article I have now gained back the 2hours I could have lost later. Awesome and thank you! \uD83D\uDE02 https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/E5diAmM47b",
        "expanded_url" : "https:\/\/twitter.com\/shortlist\/status\/773449522726965248",
        "display_url" : "twitter.com\/shortlist\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773457144041574400",
    "text" : "Thanks to the following article I have now gained back the 2hours I could have lost later. Awesome and thank you! \uD83D\uDE02 https:\/\/t.co\/E5diAmM47b",
    "id" : 773457144041574400,
    "created_at" : "2016-09-07 09:45:27 +0000",
    "user" : {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "protected" : false,
      "id_str" : "205451951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/910574324540674048\/PkMoaUAB_normal.jpg",
      "id" : 205451951,
      "verified" : false
    }
  },
  "id" : 773514461613322240,
  "created_at" : "2016-09-07 13:33:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eason",
      "screen_name" : "easons",
      "indices" : [ 0, 7 ],
      "id_str" : "62758586",
      "id" : 62758586
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773512263697334273",
  "in_reply_to_user_id" : 62758586,
  "text" : "@easons Do you carry Learning Outcome and Keyword Journal (ISBN 978-1-909-376830) at your store?",
  "id" : 773512263697334273,
  "created_at" : "2016-09-07 13:24:29 +0000",
  "in_reply_to_screen_name" : "easons",
  "in_reply_to_user_id_str" : "62758586",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "indices" : [ 3, 16 ],
      "id_str" : "14826420",
      "id" : 14826420
    }, {
      "name" : "Inc.",
      "screen_name" : "Inc",
      "indices" : [ 107, 111 ],
      "id_str" : "16896485",
      "id" : 16896485
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "blondes",
      "indices" : [ 79, 87 ]
    }, {
      "text" : "leadership",
      "indices" : [ 88, 99 ]
    }, {
      "text" : "women",
      "indices" : [ 100, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/CjiqhGhrQ8",
      "expanded_url" : "http:\/\/www.inc.com\/minda-zetlin\/why-women-who-want-to-be-leaders-should-dye-their-hair-blonde-according-to-scien.html",
      "display_url" : "inc.com\/minda-zetlin\/w\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773482236196843521",
  "text" : "RT @iamkelvinlee: I wish this was a joke but it isn't. https:\/\/t.co\/CjiqhGhrQ8 #blondes #leadership #women @Inc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Inc.",
        "screen_name" : "Inc",
        "indices" : [ 89, 93 ],
        "id_str" : "16896485",
        "id" : 16896485
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "blondes",
        "indices" : [ 61, 69 ]
      }, {
        "text" : "leadership",
        "indices" : [ 70, 81 ]
      }, {
        "text" : "women",
        "indices" : [ 82, 88 ]
      } ],
      "urls" : [ {
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/CjiqhGhrQ8",
        "expanded_url" : "http:\/\/www.inc.com\/minda-zetlin\/why-women-who-want-to-be-leaders-should-dye-their-hair-blonde-according-to-scien.html",
        "display_url" : "inc.com\/minda-zetlin\/w\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773462443586646016",
    "text" : "I wish this was a joke but it isn't. https:\/\/t.co\/CjiqhGhrQ8 #blondes #leadership #women @Inc",
    "id" : 773462443586646016,
    "created_at" : "2016-09-07 10:06:31 +0000",
    "user" : {
      "name" : "Kelvin Lee",
      "screen_name" : "iamkelvinlee",
      "protected" : false,
      "id_str" : "14826420",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996378142682923010\/A8QOlqWt_normal.jpg",
      "id" : 14826420,
      "verified" : false
    }
  },
  "id" : 773482236196843521,
  "created_at" : "2016-09-07 11:25:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/FRC6mStPv9",
      "expanded_url" : "https:\/\/twitter.com\/lgreally\/status\/773472554291826689",
      "display_url" : "twitter.com\/lgreally\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773481596242493440",
  "text" : "\"they have algorithms we don\u2019t understand, which are a filter between what we do and how people receive it.\" https:\/\/t.co\/FRC6mStPv9",
  "id" : 773481596242493440,
  "created_at" : "2016-09-07 11:22:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 3, 17 ],
      "id_str" : "80549724",
      "id" : 80549724
    }, {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 19, 25 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773479829031251968",
  "text" : "RT @dublinbusnews: @mryap Hi, the last Airlink from the airport will be at 21:00 tonight.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "hello!",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "773167592857042944",
    "geo" : { },
    "id_str" : "773449130240860161",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap Hi, the last Airlink from the airport will be at 21:00 tonight.",
    "id" : 773449130240860161,
    "in_reply_to_status_id" : 773167592857042944,
    "created_at" : "2016-09-07 09:13:37 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "protected" : false,
      "id_str" : "80549724",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037301110988566529\/8dar7Mpu_normal.jpg",
      "id" : 80549724,
      "verified" : true
    }
  },
  "id" : 773479829031251968,
  "created_at" : "2016-09-07 11:15:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gerry Simpson",
      "screen_name" : "GerrySimpsonHRW",
      "indices" : [ 3, 19 ],
      "id_str" : "2365455722",
      "id" : 2365455722
    }, {
      "name" : "Human Rights Watch",
      "screen_name" : "hrw",
      "indices" : [ 107, 111 ],
      "id_str" : "14700316",
      "id" : 14700316
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/1JqvTRxjrf",
      "expanded_url" : "http:\/\/bit.ly\/2bS7jnk",
      "display_url" : "bit.ly\/2bS7jnk"
    } ]
  },
  "geo" : { },
  "id_str" : "773447895781634048",
  "text" : "RT @GerrySimpsonHRW: What forcing 70,000+ Syrian refugees to queue for water in desert looks like from sky @HRW https:\/\/t.co\/1JqvTRxjrf htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Human Rights Watch",
        "screen_name" : "hrw",
        "indices" : [ 86, 90 ],
        "id_str" : "14700316",
        "id" : 14700316
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GerrySimpsonHRW\/status\/773408669128126464\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/q4ZuVoyGcW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CruzD9TWgAAjh3i.jpg",
        "id_str" : "773408641600880640",
        "id" : 773408641600880640,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CruzD9TWgAAjh3i.jpg",
        "sizes" : [ {
          "h" : 710,
          "resize" : "fit",
          "w" : 946
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 710,
          "resize" : "fit",
          "w" : 946
        }, {
          "h" : 710,
          "resize" : "fit",
          "w" : 946
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/q4ZuVoyGcW"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/1JqvTRxjrf",
        "expanded_url" : "http:\/\/bit.ly\/2bS7jnk",
        "display_url" : "bit.ly\/2bS7jnk"
      } ]
    },
    "geo" : { },
    "id_str" : "773408669128126464",
    "text" : "What forcing 70,000+ Syrian refugees to queue for water in desert looks like from sky @HRW https:\/\/t.co\/1JqvTRxjrf https:\/\/t.co\/q4ZuVoyGcW",
    "id" : 773408669128126464,
    "created_at" : "2016-09-07 06:32:50 +0000",
    "user" : {
      "name" : "Gerry Simpson",
      "screen_name" : "GerrySimpsonHRW",
      "protected" : false,
      "id_str" : "2365455722",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/581069358404759552\/ReGNDtaG_normal.jpg",
      "id" : 2365455722,
      "verified" : false
    }
  },
  "id" : 773447895781634048,
  "created_at" : "2016-09-07 09:08:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IDA Ireland",
      "screen_name" : "IDAIRELAND",
      "indices" : [ 3, 14 ],
      "id_str" : "44889752",
      "id" : 44889752
    }, {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 92, 106 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/PjFF2cr0fg",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/technology\/intel-acquires-dublin-based-chipmaker-movidius-1.2781200#.V86M3sq00ZA.twitter",
      "display_url" : "irishtimes.com\/business\/techn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773445722087849988",
  "text" : "RT @IDAIRELAND: Intel acquires Dublin-based  chipmaker Movidius https:\/\/t.co\/PjFF2cr0fg via @IrishTimesBiz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Times Business",
        "screen_name" : "IrishTimesBiz",
        "indices" : [ 76, 90 ],
        "id_str" : "16737418",
        "id" : 16737418
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/PjFF2cr0fg",
        "expanded_url" : "http:\/\/www.irishtimes.com\/business\/technology\/intel-acquires-dublin-based-chipmaker-movidius-1.2781200#.V86M3sq00ZA.twitter",
        "display_url" : "irishtimes.com\/business\/techn\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773091172499779584",
    "text" : "Intel acquires Dublin-based  chipmaker Movidius https:\/\/t.co\/PjFF2cr0fg via @IrishTimesBiz",
    "id" : 773091172499779584,
    "created_at" : "2016-09-06 09:31:13 +0000",
    "user" : {
      "name" : "IDA Ireland",
      "screen_name" : "IDAIRELAND",
      "protected" : false,
      "id_str" : "44889752",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/902189616031305728\/-1KTUyC__normal.jpg",
      "id" : 44889752,
      "verified" : true
    }
  },
  "id" : 773445722087849988,
  "created_at" : "2016-09-07 09:00:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Juan Manuel Flores",
      "screen_name" : "jmapurple",
      "indices" : [ 0, 10 ],
      "id_str" : "1552175298",
      "id" : 1552175298
    }, {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 37, 51 ],
      "id_str" : "80549724",
      "id" : 80549724
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "773420374629515270",
  "geo" : { },
  "id_str" : "773445187641237504",
  "in_reply_to_user_id" : 1552175298,
  "text" : "@jmapurple I think so. Do Check with @dublinbusnews",
  "id" : 773445187641237504,
  "in_reply_to_status_id" : 773420374629515270,
  "created_at" : "2016-09-07 08:57:57 +0000",
  "in_reply_to_screen_name" : "jmapurple",
  "in_reply_to_user_id_str" : "1552175298",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 0, 12 ],
      "id_str" : "168148402",
      "id" : 168148402
    }, {
      "name" : "Haramkan Nikah Bawah Umur | End Child Marriage",
      "screen_name" : "iceflower23",
      "indices" : [ 13, 25 ],
      "id_str" : "194151594",
      "id" : 194151594
    }, {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 26, 37 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "773396804406738944",
  "geo" : { },
  "id_str" : "773415542820593664",
  "in_reply_to_user_id" : 168148402,
  "text" : "@m4riannelee @iceflower23 @hellofrmSG Priority usually given to EU citizen as NON-EU people need to get work permit.",
  "id" : 773415542820593664,
  "in_reply_to_status_id" : 773396804406738944,
  "created_at" : "2016-09-07 07:00:09 +0000",
  "in_reply_to_screen_name" : "m4riannelee",
  "in_reply_to_user_id_str" : "168148402",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paddy Cosgrave",
      "screen_name" : "paddycosgrave",
      "indices" : [ 3, 17 ],
      "id_str" : "8531792",
      "id" : 8531792
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773275898690867201",
  "text" : "RT @paddycosgrave: Irish SMEs are the beating heart of our economy. They deserve at least a level playing field! Thoughts anyone? https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/paddycosgrave\/status\/772747103101657088\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/VY1mHfQ458",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrlZYLnW8AQBy2Q.jpg",
        "id_str" : "772747083040354308",
        "id" : 772747083040354308,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrlZYLnW8AQBy2Q.jpg",
        "sizes" : [ {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 421,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VY1mHfQ458"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772747103101657088",
    "text" : "Irish SMEs are the beating heart of our economy. They deserve at least a level playing field! Thoughts anyone? https:\/\/t.co\/VY1mHfQ458",
    "id" : 772747103101657088,
    "created_at" : "2016-09-05 10:44:00 +0000",
    "user" : {
      "name" : "Paddy Cosgrave",
      "screen_name" : "paddycosgrave",
      "protected" : false,
      "id_str" : "8531792",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/708780056064167936\/APRRJ-ss_normal.jpg",
      "id" : 8531792,
      "verified" : false
    }
  },
  "id" : 773275898690867201,
  "created_at" : "2016-09-06 21:45:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC Two",
      "screen_name" : "BBCTwo",
      "indices" : [ 3, 10 ],
      "id_str" : "1586183960",
      "id" : 1586183960
    }, {
      "name" : "anita rani",
      "screen_name" : "itsanitarani",
      "indices" : [ 82, 95 ],
      "id_str" : "21138978",
      "id" : 21138978
    }, {
      "name" : "Ade Adepitan",
      "screen_name" : "AdeAdepitan",
      "indices" : [ 96, 108 ],
      "id_str" : "146772416",
      "id" : 146772416
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/Hjj6wz8lSI",
      "expanded_url" : "http:\/\/snpy.tv\/2cjFvLl",
      "display_url" : "snpy.tv\/2cjFvLl"
    } ]
  },
  "geo" : { },
  "id_str" : "773250101787684865",
  "text" : "RT @BBCTwo: New York: America's Busiest City continues tonight at 9pm on BBC Two. @itsanitarani @AdeAdepitan  https:\/\/t.co\/Hjj6wz8lSI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/snappytv.com\" rel=\"nofollow\"\u003ESnappyTV.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "anita rani",
        "screen_name" : "itsanitarani",
        "indices" : [ 70, 83 ],
        "id_str" : "21138978",
        "id" : 21138978
      }, {
        "name" : "Ade Adepitan",
        "screen_name" : "AdeAdepitan",
        "indices" : [ 84, 96 ],
        "id_str" : "146772416",
        "id" : 146772416
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/Hjj6wz8lSI",
        "expanded_url" : "http:\/\/snpy.tv\/2cjFvLl",
        "display_url" : "snpy.tv\/2cjFvLl"
      } ]
    },
    "geo" : { },
    "id_str" : "773077491573415936",
    "text" : "New York: America's Busiest City continues tonight at 9pm on BBC Two. @itsanitarani @AdeAdepitan  https:\/\/t.co\/Hjj6wz8lSI",
    "id" : 773077491573415936,
    "created_at" : "2016-09-06 08:36:51 +0000",
    "user" : {
      "name" : "BBC Two",
      "screen_name" : "BBCTwo",
      "protected" : false,
      "id_str" : "1586183960",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998184813185056768\/rwlaj3Jx_normal.jpg",
      "id" : 1586183960,
      "verified" : true
    }
  },
  "id" : 773250101787684865,
  "created_at" : "2016-09-06 20:02:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/4ZOHqe0nXT",
      "expanded_url" : "https:\/\/twitter.com\/koenfucius\/status\/773197330594095104",
      "display_url" : "twitter.com\/koenfucius\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773200293299363840",
  "text" : "The Singapore model - govt owns the land, 85% housing provide by govt &amp; state own enterprise generate 22% of GDP https:\/\/t.co\/4ZOHqe0nXT",
  "id" : 773200293299363840,
  "created_at" : "2016-09-06 16:44:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/hQFOcF4rDJ",
      "expanded_url" : "http:\/\/www.partner.getoptimise.com\/goodbye-wordpress-and-hello-postleaf",
      "display_url" : "partner.getoptimise.com\/goodbye-wordpr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773197883126456320",
  "text" : "Kicking the tire. Goodbye WordPress &amp; Hello PostLeaf https:\/\/t.co\/hQFOcF4rDJ",
  "id" : 773197883126456320,
  "created_at" : "2016-09-06 16:35:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Rady",
      "screen_name" : "benrady",
      "indices" : [ 3, 11 ],
      "id_str" : "24235240",
      "id" : 24235240
    }, {
      "name" : "h",
      "screen_name" : "hisnice",
      "indices" : [ 22, 30 ],
      "id_str" : "16907024",
      "id" : 16907024
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/benrady\/status\/769633917871718400\/photo\/1",
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/nM45WXQP6c",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cq5J8xkXgAEjSEf.jpg",
      "id_str" : "769633894773784577",
      "id" : 769633894773784577,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cq5J8xkXgAEjSEf.jpg",
      "sizes" : [ {
        "h" : 413,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 293,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nM45WXQP6c"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773196938086850560",
  "text" : "RT @benrady: [Hat tip @hisnice] https:\/\/t.co\/nM45WXQP6c",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "h",
        "screen_name" : "hisnice",
        "indices" : [ 9, 17 ],
        "id_str" : "16907024",
        "id" : 16907024
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/benrady\/status\/769633917871718400\/photo\/1",
        "indices" : [ 19, 42 ],
        "url" : "https:\/\/t.co\/nM45WXQP6c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cq5J8xkXgAEjSEf.jpg",
        "id_str" : "769633894773784577",
        "id" : 769633894773784577,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cq5J8xkXgAEjSEf.jpg",
        "sizes" : [ {
          "h" : 413,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 293,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nM45WXQP6c"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "769633917871718400",
    "text" : "[Hat tip @hisnice] https:\/\/t.co\/nM45WXQP6c",
    "id" : 769633917871718400,
    "created_at" : "2016-08-27 20:33:19 +0000",
    "user" : {
      "name" : "Ben Rady",
      "screen_name" : "benrady",
      "protected" : false,
      "id_str" : "24235240",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/97207603\/benrady-avatar_normal.jpg",
      "id" : 24235240,
      "verified" : false
    }
  },
  "id" : 773196938086850560,
  "created_at" : "2016-09-06 16:31:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dublin",
      "indices" : [ 17, 24 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "773167592857042944",
  "text" : "If you coming to #Dublin, the Airlink services (Dublin Bus Airport Services) won\u2019t run on this Thursday or Friday.",
  "id" : 773167592857042944,
  "created_at" : "2016-09-06 14:34:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/8AM5GMYlPH",
      "expanded_url" : "http:\/\/www.bandofcats.com\/21-cat-behaviors-32-cat-sounds-and-their-secret-meaning\/",
      "display_url" : "bandofcats.com\/21-cat-behavio\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773162711156985857",
  "text" : "\"Arched back with raised fur...With a kitten it usually means let\u2019s play!\" &lt; I thought the cat wants to attack me!  https:\/\/t.co\/8AM5GMYlPH",
  "id" : 773162711156985857,
  "created_at" : "2016-09-06 14:15:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/pkNQcM1WbM",
      "expanded_url" : "http:\/\/arxiv.org\/abs\/1411.2878",
      "display_url" : "arxiv.org\/abs\/1411.2878"
    } ]
  },
  "geo" : { },
  "id_str" : "773114216010158080",
  "text" : "A good rule-of-thumb for inactivity threshold should set at about 60 minutes. https:\/\/t.co\/pkNQcM1WbM",
  "id" : 773114216010158080,
  "created_at" : "2016-09-06 11:02:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "indices" : [ 3, 16 ],
      "id_str" : "205451951",
      "id" : 205451951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/7pr4LLmTcy",
      "expanded_url" : "https:\/\/twitter.com\/skynewsbreak\/status\/773044006116818944",
      "display_url" : "twitter.com\/skynewsbreak\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "773048362299977729",
  "text" : "RT @TheChrisBray: Excellent news! https:\/\/t.co\/7pr4LLmTcy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 39 ],
        "url" : "https:\/\/t.co\/7pr4LLmTcy",
        "expanded_url" : "https:\/\/twitter.com\/skynewsbreak\/status\/773044006116818944",
        "display_url" : "twitter.com\/skynewsbreak\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "773046815620988928",
    "text" : "Excellent news! https:\/\/t.co\/7pr4LLmTcy",
    "id" : 773046815620988928,
    "created_at" : "2016-09-06 06:34:58 +0000",
    "user" : {
      "name" : "Chris Bray",
      "screen_name" : "TheChrisBray",
      "protected" : false,
      "id_str" : "205451951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/910574324540674048\/PkMoaUAB_normal.jpg",
      "id" : 205451951,
      "verified" : false
    }
  },
  "id" : 773048362299977729,
  "created_at" : "2016-09-06 06:41:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "772882175981215744",
  "geo" : { },
  "id_str" : "772892534997680129",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold there always the option of dispatching down south?\uD83D\uDE00",
  "id" : 772892534997680129,
  "in_reply_to_status_id" : 772882175981215744,
  "created_at" : "2016-09-05 20:21:54 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "indices" : [ 3, 16 ],
      "id_str" : "854642300",
      "id" : 854642300
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/3thoughtbbls\/status\/772849719001382912\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/TPsX5tCpBK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Crm2in_WAAAOFIo.jpg",
      "id_str" : "772849517037223936",
      "id" : 772849517037223936,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Crm2in_WAAAOFIo.jpg",
      "sizes" : [ {
        "h" : 684,
        "resize" : "fit",
        "w" : 570
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 684,
        "resize" : "fit",
        "w" : 570
      }, {
        "h" : 684,
        "resize" : "fit",
        "w" : 570
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 567
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TPsX5tCpBK"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772868512800989185",
  "text" : "RT @3thoughtbbls: Does anyone know of an Irish printing company that designs these Instagram prop frames? https:\/\/t.co\/TPsX5tCpBK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/3thoughtbbls\/status\/772849719001382912\/photo\/1",
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/TPsX5tCpBK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Crm2in_WAAAOFIo.jpg",
        "id_str" : "772849517037223936",
        "id" : 772849517037223936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Crm2in_WAAAOFIo.jpg",
        "sizes" : [ {
          "h" : 684,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 684,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 684,
          "resize" : "fit",
          "w" : 570
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 567
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/TPsX5tCpBK"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772849719001382912",
    "text" : "Does anyone know of an Irish printing company that designs these Instagram prop frames? https:\/\/t.co\/TPsX5tCpBK",
    "id" : 772849719001382912,
    "created_at" : "2016-09-05 17:31:46 +0000",
    "user" : {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "protected" : false,
      "id_str" : "854642300",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459287079227101184\/Yb2pWHi8_normal.jpeg",
      "id" : 854642300,
      "verified" : false
    }
  },
  "id" : 772868512800989185,
  "created_at" : "2016-09-05 18:46:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772783664774148097",
  "text" : "I cringed whenever someone stuffed all keywords into a Linkedin profile.",
  "id" : 772783664774148097,
  "created_at" : "2016-09-05 13:09:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/sjckju8Sh4",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/aarontilley\/2016\/09\/01\/chinese-search-giant-baidu-open-sources-its-deep-learning-software\/#79fd87ee677f",
      "display_url" : "forbes.com\/sites\/aarontil\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772495169967054848",
  "text" : "Chinese Search Giant Baidu Open Sources Its Deep Learning Software https:\/\/t.co\/sjckju8Sh4",
  "id" : 772495169967054848,
  "created_at" : "2016-09-04 18:02:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Beschloss",
      "screen_name" : "BeschlossDC",
      "indices" : [ 3, 15 ],
      "id_str" : "874916178",
      "id" : 874916178
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Counts",
      "indices" : [ 126, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772464376079089664",
  "text" : "RT @BeschlossDC: Little Rock High School--9 African American students barred from entering by Arkansas Natl Guard today 1957: #Counts https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BeschlossDC\/status\/772432851564244992\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/oBL1UeNL42",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Crg7lGjUsAAY-Nv.jpg",
        "id_str" : "772432844694007808",
        "id" : 772432844694007808,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Crg7lGjUsAAY-Nv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 498
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 498
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 498
        }, {
          "h" : 326,
          "resize" : "fit",
          "w" : 498
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oBL1UeNL42"
      } ],
      "hashtags" : [ {
        "text" : "Counts",
        "indices" : [ 109, 116 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772432851564244992",
    "text" : "Little Rock High School--9 African American students barred from entering by Arkansas Natl Guard today 1957: #Counts https:\/\/t.co\/oBL1UeNL42",
    "id" : 772432851564244992,
    "created_at" : "2016-09-04 13:55:17 +0000",
    "user" : {
      "name" : "Michael Beschloss",
      "screen_name" : "BeschlossDC",
      "protected" : false,
      "id_str" : "874916178",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017422524898934784\/dGwBCGXO_normal.jpg",
      "id" : 874916178,
      "verified" : true
    }
  },
  "id" : 772464376079089664,
  "created_at" : "2016-09-04 16:00:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 3, 17 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772459478994345985",
  "text" : "RT @ladystormhold: Regret bringing so many things when I first moved here. If you ever move countries, just forget about mementos cos you w\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772437633817579520",
    "text" : "Regret bringing so many things when I first moved here. If you ever move countries, just forget about mementos cos you won't look at them.",
    "id" : 772437633817579520,
    "created_at" : "2016-09-04 14:14:17 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 772459478994345985,
  "created_at" : "2016-09-04 15:41:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marshall Kirkpatrick",
      "screen_name" : "marshallk",
      "indices" : [ 3, 13 ],
      "id_str" : "818340",
      "id" : 818340
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/marshallk\/status\/748604032772542465\/photo\/1",
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/BiYNLqCziu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CmOTYLYVYAAOAtv.jpg",
      "id_str" : "748604006654631936",
      "id" : 748604006654631936,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CmOTYLYVYAAOAtv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BiYNLqCziu"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772427552409477120",
  "text" : "RT @marshallk: Ouch. https:\/\/t.co\/BiYNLqCziu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/marshallk\/status\/748604032772542465\/photo\/1",
        "indices" : [ 6, 29 ],
        "url" : "https:\/\/t.co\/BiYNLqCziu",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CmOTYLYVYAAOAtv.jpg",
        "id_str" : "748604006654631936",
        "id" : 748604006654631936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CmOTYLYVYAAOAtv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BiYNLqCziu"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "748604032772542465",
    "text" : "Ouch. https:\/\/t.co\/BiYNLqCziu",
    "id" : 748604032772542465,
    "created_at" : "2016-06-30 19:48:04 +0000",
    "user" : {
      "name" : "Marshall Kirkpatrick",
      "screen_name" : "marshallk",
      "protected" : false,
      "id_str" : "818340",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/907721110170378240\/fLJctv47_normal.jpg",
      "id" : 818340,
      "verified" : true
    }
  },
  "id" : 772427552409477120,
  "created_at" : "2016-09-04 13:34:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/hb1eHI0L3s",
      "expanded_url" : "http:\/\/brohrer.github.io\/stop_chasing_unicorns.html",
      "display_url" : "brohrer.github.io\/stop_chasing_u\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772401274839007232",
  "text" : "Rem I had my 1st data analyst interview when I state I want to do A to Z....well you got to focus https:\/\/t.co\/hb1eHI0L3s",
  "id" : 772401274839007232,
  "created_at" : "2016-09-04 11:49:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/772384161948573696\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/ImxNP9f7ia",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrgPRcJVUAEBV-Q.jpg",
      "id_str" : "772384128381571073",
      "id" : 772384128381571073,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrgPRcJVUAEBV-Q.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 432,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 432,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 432,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ImxNP9f7ia"
    } ],
    "hashtags" : [ {
      "text" : "Zika",
      "indices" : [ 46, 51 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772399159445979136",
  "text" : "RT @ChannelNewsAsia: JUST IN: 27 new cases of #Zika in Singapore; total number now 242 https:\/\/t.co\/ImxNP9f7ia",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/772384161948573696\/photo\/1",
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/ImxNP9f7ia",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrgPRcJVUAEBV-Q.jpg",
        "id_str" : "772384128381571073",
        "id" : 772384128381571073,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrgPRcJVUAEBV-Q.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ImxNP9f7ia"
      } ],
      "hashtags" : [ {
        "text" : "Zika",
        "indices" : [ 25, 30 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772384161948573696",
    "text" : "JUST IN: 27 new cases of #Zika in Singapore; total number now 242 https:\/\/t.co\/ImxNP9f7ia",
    "id" : 772384161948573696,
    "created_at" : "2016-09-04 10:41:49 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 772399159445979136,
  "created_at" : "2016-09-04 11:41:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/BSW5C180m2",
      "expanded_url" : "http:\/\/dlvr.it\/MBFbfy",
      "display_url" : "dlvr.it\/MBFbfy"
    } ]
  },
  "geo" : { },
  "id_str" : "772399025106612224",
  "text" : "RT @ChannelNewsAsia: Hong Kong maids march against window-cleaning after deaths https:\/\/t.co\/BSW5C180m2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/BSW5C180m2",
        "expanded_url" : "http:\/\/dlvr.it\/MBFbfy",
        "display_url" : "dlvr.it\/MBFbfy"
      } ]
    },
    "geo" : { },
    "id_str" : "772381598138245120",
    "text" : "Hong Kong maids march against window-cleaning after deaths https:\/\/t.co\/BSW5C180m2",
    "id" : 772381598138245120,
    "created_at" : "2016-09-04 10:31:37 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 772399025106612224,
  "created_at" : "2016-09-04 11:40:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/UhytDToOXd",
      "expanded_url" : "https:\/\/twitter.com\/eviltofu\/status\/772376806867427328",
      "display_url" : "twitter.com\/eviltofu\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772378726256508928",
  "text" : "I missed this... https:\/\/t.co\/UhytDToOXd",
  "id" : 772378726256508928,
  "created_at" : "2016-09-04 10:20:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ST Sports Desk",
      "screen_name" : "STsportsdesk",
      "indices" : [ 3, 16 ],
      "id_str" : "308968000",
      "id" : 308968000
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OneTeamSG",
      "indices" : [ 22, 32 ]
    }, {
      "text" : "NetballNationsCup",
      "indices" : [ 101, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772359174684549120",
  "text" : "RT @STsportsdesk: The #OneTeamSG and Ireland netball teams are warming up for the first match of the #NetballNationsCup! https:\/\/t.co\/I05Ww\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STsportsdesk\/status\/772323924612816896\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/I05WwpLcvT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrfYd3dUIAATF-3.jpg",
        "id_str" : "772323868732039168",
        "id" : 772323868732039168,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrfYd3dUIAATF-3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/I05WwpLcvT"
      } ],
      "hashtags" : [ {
        "text" : "OneTeamSG",
        "indices" : [ 4, 14 ]
      }, {
        "text" : "NetballNationsCup",
        "indices" : [ 83, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772323924612816896",
    "text" : "The #OneTeamSG and Ireland netball teams are warming up for the first match of the #NetballNationsCup! https:\/\/t.co\/I05WwpLcvT",
    "id" : 772323924612816896,
    "created_at" : "2016-09-04 06:42:27 +0000",
    "user" : {
      "name" : "ST Sports Desk",
      "screen_name" : "STsportsdesk",
      "protected" : false,
      "id_str" : "308968000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/891352876332720128\/jRjrmFjC_normal.jpg",
      "id" : 308968000,
      "verified" : false
    }
  },
  "id" : 772359174684549120,
  "created_at" : "2016-09-04 09:02:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/MYz9uj60zu",
      "expanded_url" : "https:\/\/twitter.com\/evanpdunn\/status\/772197756316348416",
      "display_url" : "twitter.com\/evanpdunn\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772198188992368640",
  "text" : "Very True indeed when I began my learning journey on data analysis https:\/\/t.co\/MYz9uj60zu",
  "id" : 772198188992368640,
  "created_at" : "2016-09-03 22:22:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Kemp",
      "screen_name" : "eskimon",
      "indices" : [ 3, 11 ],
      "id_str" : "15177871",
      "id" : 15177871
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772191206675800064",
  "text" : "RT @eskimon: I am so tired of expats that believe they're culturally superior. Embrace the culture you're joining, or go back to your own.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771688858886877184",
    "text" : "I am so tired of expats that believe they're culturally superior. Embrace the culture you're joining, or go back to your own.",
    "id" : 771688858886877184,
    "created_at" : "2016-09-02 12:38:55 +0000",
    "user" : {
      "name" : "Simon Kemp",
      "screen_name" : "eskimon",
      "protected" : false,
      "id_str" : "15177871",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/673806239634169856\/ui-tRkJy_normal.jpg",
      "id" : 15177871,
      "verified" : true
    }
  },
  "id" : 772191206675800064,
  "created_at" : "2016-09-03 21:55:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brett Crosby",
      "screen_name" : "brettc",
      "indices" : [ 3, 10 ],
      "id_str" : "7718812",
      "id" : 7718812
    }, {
      "name" : "Google Analytics",
      "screen_name" : "googleanalytics",
      "indices" : [ 28, 44 ],
      "id_str" : "51263711",
      "id" : 51263711
    }, {
      "name" : "scott crosby",
      "screen_name" : "impunity",
      "indices" : [ 114, 123 ],
      "id_str" : "7719062",
      "id" : 7719062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772190992439078912",
  "text" : "RT @brettc: Ever wonder how @googleanalytics got started? Read the secret tale of Urchin Software Corp written by @impunity https:\/\/t.co\/ZV\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google Analytics",
        "screen_name" : "googleanalytics",
        "indices" : [ 16, 32 ],
        "id_str" : "51263711",
        "id" : 51263711
      }, {
        "name" : "scott crosby",
        "screen_name" : "impunity",
        "indices" : [ 102, 111 ],
        "id_str" : "7719062",
        "id" : 7719062
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/ZVMY68iRAs",
        "expanded_url" : "https:\/\/medium.com\/@impunity\/urchin-software-corp-89a1f5292999",
        "display_url" : "medium.com\/@impunity\/urch\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "771842442916143104",
    "text" : "Ever wonder how @googleanalytics got started? Read the secret tale of Urchin Software Corp written by @impunity https:\/\/t.co\/ZVMY68iRAs",
    "id" : 771842442916143104,
    "created_at" : "2016-09-02 22:49:13 +0000",
    "user" : {
      "name" : "Brett Crosby",
      "screen_name" : "brettc",
      "protected" : false,
      "id_str" : "7718812",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/483691567287267328\/jnHfZW7t_normal.png",
      "id" : 7718812,
      "verified" : true
    }
  },
  "id" : 772190992439078912,
  "created_at" : "2016-09-03 21:54:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Weckler",
      "screen_name" : "adrianweckler",
      "indices" : [ 3, 17 ],
      "id_str" : "18080002",
      "id" : 18080002
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772187244908150785",
  "text" : "RT @adrianweckler: Apple CEO Tim Cook: Brussels \"ignored\" Apple tax data leading to \"intentionally misleading\" ruling https:\/\/t.co\/PyPnv8Wb\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/adrianweckler\/status\/772187065555492866\/photo\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/PyPnv8WbT0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrdcBYAXEAUw0fi.jpg",
        "id_str" : "772187039810916357",
        "id" : 772187039810916357,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrdcBYAXEAUw0fi.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 778
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1327
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 441
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1327
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PyPnv8WbT0"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772187065555492866",
    "text" : "Apple CEO Tim Cook: Brussels \"ignored\" Apple tax data leading to \"intentionally misleading\" ruling https:\/\/t.co\/PyPnv8WbT0",
    "id" : 772187065555492866,
    "created_at" : "2016-09-03 21:38:37 +0000",
    "user" : {
      "name" : "Adrian Weckler",
      "screen_name" : "adrianweckler",
      "protected" : false,
      "id_str" : "18080002",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040108417703014400\/NY5mHJ0B_normal.jpg",
      "id" : 18080002,
      "verified" : true
    }
  },
  "id" : 772187244908150785,
  "created_at" : "2016-09-03 21:39:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/STForeignDesk\/status\/771697239638970370\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/PBP1SajqgN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrWebj8UkAAo7vf.jpg",
      "id_str" : "771697107505811456",
      "id" : 771697107505811456,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrWebj8UkAAo7vf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1365,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 2574,
        "resize" : "fit",
        "w" : 3861
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PBP1SajqgN"
    } ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 11, 21 ]
    }, {
      "text" : "Zika",
      "indices" : [ 61, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/29s14SSDPx",
      "expanded_url" : "http:\/\/str.sg\/4pZh",
      "display_url" : "str.sg\/4pZh"
    } ]
  },
  "geo" : { },
  "id_str" : "772184267753422849",
  "text" : "RT @STcom: #Singapore represents 'role model' in handling of #Zika cases: WHO https:\/\/t.co\/29s14SSDPx https:\/\/t.co\/PBP1SajqgN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STForeignDesk\/status\/771697239638970370\/photo\/1",
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/PBP1SajqgN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrWebj8UkAAo7vf.jpg",
        "id_str" : "771697107505811456",
        "id" : 771697107505811456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrWebj8UkAAo7vf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2574,
          "resize" : "fit",
          "w" : 3861
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PBP1SajqgN"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 0, 10 ]
      }, {
        "text" : "Zika",
        "indices" : [ 50, 55 ]
      } ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/29s14SSDPx",
        "expanded_url" : "http:\/\/str.sg\/4pZh",
        "display_url" : "str.sg\/4pZh"
      } ]
    },
    "geo" : { },
    "id_str" : "771702438713622528",
    "text" : "#Singapore represents 'role model' in handling of #Zika cases: WHO https:\/\/t.co\/29s14SSDPx https:\/\/t.co\/PBP1SajqgN",
    "id" : 771702438713622528,
    "created_at" : "2016-09-02 13:32:53 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 772184267753422849,
  "created_at" : "2016-09-03 21:27:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Wong",
      "screen_name" : "Botanygeek",
      "indices" : [ 3, 14 ],
      "id_str" : "189658688",
      "id" : 189658688
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772183843633856512",
  "text" : "RT @Botanygeek: Ladies &amp; gents, this is a multi-storey carpark in Singapore. I wouldn't mind that view. Imagine if we did the same. https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Botanygeek\/status\/772002446193397760\/photo\/1",
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/B0BN5Obfi5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cra0IHgXEAAE8PT.jpg",
        "id_str" : "772002437687349248",
        "id" : 772002437687349248,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cra0IHgXEAAE8PT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 564
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 564
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 564
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 564
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/B0BN5Obfi5"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772002446193397760",
    "text" : "Ladies &amp; gents, this is a multi-storey carpark in Singapore. I wouldn't mind that view. Imagine if we did the same. https:\/\/t.co\/B0BN5Obfi5",
    "id" : 772002446193397760,
    "created_at" : "2016-09-03 09:25:00 +0000",
    "user" : {
      "name" : "James Wong",
      "screen_name" : "Botanygeek",
      "protected" : false,
      "id_str" : "189658688",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/564461451025207296\/eBEO6IoN_normal.jpeg",
      "id" : 189658688,
      "verified" : true
    }
  },
  "id" : 772183843633856512,
  "created_at" : "2016-09-03 21:25:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/771885089261166592\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/L3bZUP6Fgd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrZJWuXUkAAnHoX.jpg",
      "id_str" : "771885040892416000",
      "id" : 771885040892416000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrZJWuXUkAAnHoX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 4032,
        "resize" : "fit",
        "w" : 3024
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/L3bZUP6Fgd"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772183334642388992",
  "text" : "RT @mrbrown: They should have this poster in Singapore trains too. https:\/\/t.co\/L3bZUP6Fgd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/771885089261166592\/photo\/1",
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/L3bZUP6Fgd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrZJWuXUkAAnHoX.jpg",
        "id_str" : "771885040892416000",
        "id" : 771885040892416000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrZJWuXUkAAnHoX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 4032,
          "resize" : "fit",
          "w" : 3024
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/L3bZUP6Fgd"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771885089261166592",
    "text" : "They should have this poster in Singapore trains too. https:\/\/t.co\/L3bZUP6Fgd",
    "id" : 771885089261166592,
    "created_at" : "2016-09-03 01:38:40 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 772183334642388992,
  "created_at" : "2016-09-03 21:23:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Apple",
      "screen_name" : "Apple",
      "indices" : [ 3, 9 ],
      "id_str" : "380749300",
      "id" : 380749300
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AppleEvent",
      "indices" : [ 114, 125 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/yLa2e4Xr2R",
      "expanded_url" : "http:\/\/apple.co\/live",
      "display_url" : "apple.co\/live"
    } ]
  },
  "geo" : { },
  "id_str" : "772150840576442368",
  "text" : "RT @Apple: September 7 at 10 a.m. PT\nWatch the event on https:\/\/t.co\/yLa2e4Xr2R.\nRT for a reminder on event day. \n#AppleEvent https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Apple\/status\/771787818095874048\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/e9MHNkLeoi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrXw7luVIAAzxAs.jpg",
        "id_str" : "771787817693290496",
        "id" : 771787817693290496,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrXw7luVIAAzxAs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/e9MHNkLeoi"
      } ],
      "hashtags" : [ {
        "text" : "AppleEvent",
        "indices" : [ 103, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 45, 68 ],
        "url" : "https:\/\/t.co\/yLa2e4Xr2R",
        "expanded_url" : "http:\/\/apple.co\/live",
        "display_url" : "apple.co\/live"
      } ]
    },
    "geo" : { },
    "id_str" : "771787818095874048",
    "text" : "September 7 at 10 a.m. PT\nWatch the event on https:\/\/t.co\/yLa2e4Xr2R.\nRT for a reminder on event day. \n#AppleEvent https:\/\/t.co\/e9MHNkLeoi",
    "id" : 771787818095874048,
    "created_at" : "2016-09-02 19:12:09 +0000",
    "user" : {
      "name" : "Apple",
      "screen_name" : "Apple",
      "protected" : false,
      "id_str" : "380749300",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/856508346190381057\/DaVvCgBo_normal.jpg",
      "id" : 380749300,
      "verified" : true
    }
  },
  "id" : 772150840576442368,
  "created_at" : "2016-09-03 19:14:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Mark Rowe",
      "screen_name" : "DrMarkRowe",
      "indices" : [ 3, 14 ],
      "id_str" : "843443562",
      "id" : 843443562
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "designedtomove",
      "indices" : [ 85, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/TyqjrgFA7S",
      "expanded_url" : "http:\/\/medicalxpress.com\/news\/2016-07-maximum-heart.html",
      "display_url" : "medicalxpress.com\/news\/2016-07-m\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772150631691845636",
  "text" : "RT @DrMarkRowe: Research shows the damaging effects of sitting so Move more sit less #designedtomove https:\/\/t.co\/TyqjrgFA7S",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "designedtomove",
        "indices" : [ 69, 84 ]
      } ],
      "urls" : [ {
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/TyqjrgFA7S",
        "expanded_url" : "http:\/\/medicalxpress.com\/news\/2016-07-maximum-heart.html",
        "display_url" : "medicalxpress.com\/news\/2016-07-m\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "772120726237220866",
    "text" : "Research shows the damaging effects of sitting so Move more sit less #designedtomove https:\/\/t.co\/TyqjrgFA7S",
    "id" : 772120726237220866,
    "created_at" : "2016-09-03 17:15:01 +0000",
    "user" : {
      "name" : "Dr. Mark Rowe",
      "screen_name" : "DrMarkRowe",
      "protected" : false,
      "id_str" : "843443562",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/971095391314771968\/7ZRxd87F_normal.jpg",
      "id" : 843443562,
      "verified" : false
    }
  },
  "id" : 772150631691845636,
  "created_at" : "2016-09-03 19:13:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772136976552255488",
  "text" : "RT @rshotton: Even though we're globalised Netflix data shows locally set films are still v popular\r\rFrom Algorithms to live by https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/772132912686886912\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/ca0a90xWPr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Crcqyj5WcAAZbUa.jpg",
        "id_str" : "772132909234941952",
        "id" : 772132909234941952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Crcqyj5WcAAZbUa.jpg",
        "sizes" : [ {
          "h" : 522,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 522,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 347,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 522,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ca0a90xWPr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772132912686886912",
    "text" : "Even though we're globalised Netflix data shows locally set films are still v popular\r\rFrom Algorithms to live by https:\/\/t.co\/ca0a90xWPr",
    "id" : 772132912686886912,
    "created_at" : "2016-09-03 18:03:26 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 772136976552255488,
  "created_at" : "2016-09-03 18:19:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ManageWP",
      "screen_name" : "managewp",
      "indices" : [ 3, 12 ],
      "id_str" : "272853910",
      "id" : 272853910
    }, {
      "name" : "WPLift",
      "screen_name" : "wplift",
      "indices" : [ 102, 109 ],
      "id_str" : "186398120",
      "id" : 186398120
    }, {
      "name" : "Donna Cavalier",
      "screen_name" : "DonnaCavalier",
      "indices" : [ 114, 128 ],
      "id_str" : "6322732",
      "id" : 6322732
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/wpoyoxAEQR",
      "expanded_url" : "https:\/\/managewp.org\/articles\/13296\/how-to-set-up-cloudflare-for-wordpress-the-complete-guide",
      "display_url" : "managewp.org\/articles\/13296\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772136575413215232",
  "text" : "RT @managewp: How to Set Up CloudFlare for WordPress \u2013 The Complete Guide https:\/\/t.co\/wpoyoxAEQR via @wplift and @DonnaCavalier https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/managewp.org\/\" rel=\"nofollow\"\u003EManageWP.org\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "WPLift",
        "screen_name" : "wplift",
        "indices" : [ 88, 95 ],
        "id_str" : "186398120",
        "id" : 186398120
      }, {
        "name" : "Donna Cavalier",
        "screen_name" : "DonnaCavalier",
        "indices" : [ 100, 114 ],
        "id_str" : "6322732",
        "id" : 6322732
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/managewp\/status\/772086796033417216\/photo\/1",
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/vWkF4drZgr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrcA2RiXYAESYax.jpg",
        "id_str" : "772086793537806337",
        "id" : 772086793537806337,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrcA2RiXYAESYax.jpg",
        "sizes" : [ {
          "h" : 397,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 397,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 397,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vWkF4drZgr"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/wpoyoxAEQR",
        "expanded_url" : "https:\/\/managewp.org\/articles\/13296\/how-to-set-up-cloudflare-for-wordpress-the-complete-guide",
        "display_url" : "managewp.org\/articles\/13296\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "772086796033417216",
    "text" : "How to Set Up CloudFlare for WordPress \u2013 The Complete Guide https:\/\/t.co\/wpoyoxAEQR via @wplift and @DonnaCavalier https:\/\/t.co\/vWkF4drZgr",
    "id" : 772086796033417216,
    "created_at" : "2016-09-03 15:00:11 +0000",
    "user" : {
      "name" : "ManageWP",
      "screen_name" : "managewp",
      "protected" : false,
      "id_str" : "272853910",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/768088760387067904\/outbNsnZ_normal.jpg",
      "id" : 272853910,
      "verified" : false
    }
  },
  "id" : 772136575413215232,
  "created_at" : "2016-09-03 18:17:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sustainable Business",
      "screen_name" : "GuardianSustBiz",
      "indices" : [ 3, 19 ],
      "id_str" : "116469425",
      "id" : 116469425
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/eLcnNmEpil",
      "expanded_url" : "https:\/\/www.theguardian.com\/sustainable-business\/2016\/sep\/03\/elon-musk-solar-roofs-sustainable-homes-solarcity-panels?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/sustainable-bu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "772131814785908737",
  "text" : "RT @GuardianSustBiz: Elon Musk aims to refit 5m homes with solar roofs https:\/\/t.co\/eLcnNmEpil",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/eLcnNmEpil",
        "expanded_url" : "https:\/\/www.theguardian.com\/sustainable-business\/2016\/sep\/03\/elon-musk-solar-roofs-sustainable-homes-solarcity-panels?CMP=share_btn_tw",
        "display_url" : "theguardian.com\/sustainable-bu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "772001942830735360",
    "text" : "Elon Musk aims to refit 5m homes with solar roofs https:\/\/t.co\/eLcnNmEpil",
    "id" : 772001942830735360,
    "created_at" : "2016-09-03 09:23:00 +0000",
    "user" : {
      "name" : "Sustainable Business",
      "screen_name" : "GuardianSustBiz",
      "protected" : false,
      "id_str" : "116469425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/952860713256615936\/TNGAvZQt_normal.jpg",
      "id" : 116469425,
      "verified" : true
    }
  },
  "id" : 772131814785908737,
  "created_at" : "2016-09-03 17:59:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/NrLKLCeJFN",
      "expanded_url" : "http:\/\/bit.ly\/2bKoS8O",
      "display_url" : "bit.ly\/2bKoS8O"
    } ]
  },
  "geo" : { },
  "id_str" : "772131683248242688",
  "text" : "Vehicles must be sprayed with insect repellent before entering Malaysia: Health official https:\/\/t.co\/NrLKLCeJFN",
  "id" : 772131683248242688,
  "created_at" : "2016-09-03 17:58:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CCTV NEWS",
      "screen_name" : "cctvnews",
      "indices" : [ 3, 12 ],
      "id_str" : "810730525421867008",
      "id" : 810730525421867008
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cctvnews\/status\/771867947140722688\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/VpHrC8OImK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrY5x6xUsAA1qqO.jpg",
      "id_str" : "771867915893125120",
      "id" : 771867915893125120,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrY5x6xUsAA1qqO.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 581,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 494,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 581,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 581,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VpHrC8OImK"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/cctvnews\/status\/771867947140722688\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/VpHrC8OImK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrY5yL1VIAALPR6.jpg",
      "id_str" : "771867920473333760",
      "id" : 771867920473333760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrY5yL1VIAALPR6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 425,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VpHrC8OImK"
    } ],
    "hashtags" : [ {
      "text" : "XiJinping",
      "indices" : [ 24, 34 ]
    }, {
      "text" : "G20China",
      "indices" : [ 85, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772130237274619904",
  "text" : "RT @cctvnews: President #XiJinping met with Singaporean PM Lee Hsien Loong on Friday #G20China https:\/\/t.co\/VpHrC8OImK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cctvnews\/status\/771867947140722688\/photo\/1",
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/VpHrC8OImK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrY5x6xUsAA1qqO.jpg",
        "id_str" : "771867915893125120",
        "id" : 771867915893125120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrY5x6xUsAA1qqO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 581,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 494,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 581,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 581,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VpHrC8OImK"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/cctvnews\/status\/771867947140722688\/photo\/1",
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/VpHrC8OImK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrY5yL1VIAALPR6.jpg",
        "id_str" : "771867920473333760",
        "id" : 771867920473333760,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrY5yL1VIAALPR6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 425,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VpHrC8OImK"
      } ],
      "hashtags" : [ {
        "text" : "XiJinping",
        "indices" : [ 10, 20 ]
      }, {
        "text" : "G20China",
        "indices" : [ 71, 80 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771867947140722688",
    "text" : "President #XiJinping met with Singaporean PM Lee Hsien Loong on Friday #G20China https:\/\/t.co\/VpHrC8OImK",
    "id" : 771867947140722688,
    "created_at" : "2016-09-03 00:30:33 +0000",
    "user" : {
      "name" : "CGTN",
      "screen_name" : "CGTNOfficial",
      "protected" : false,
      "id_str" : "1115874631",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/969543729500467202\/y-kyowAV_normal.jpg",
      "id" : 1115874631,
      "verified" : true
    }
  },
  "id" : 772130237274619904,
  "created_at" : "2016-09-03 17:52:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hindustan Times",
      "screen_name" : "htTweets",
      "indices" : [ 3, 12 ],
      "id_str" : "36327407",
      "id" : 36327407
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/htTweets\/status\/772119284969533446\/photo\/1",
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/6nm2D9dI5m",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrceXUlWYAA7Fjy.jpg",
      "id_str" : "772119247128518656",
      "id" : 772119247128518656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrceXUlWYAA7Fjy.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 362,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 362,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 362,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 362,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6nm2D9dI5m"
    } ],
    "hashtags" : [ {
      "text" : "Zika",
      "indices" : [ 63, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/lmS0Ecq46P",
      "expanded_url" : "http:\/\/read.ht\/BKL1",
      "display_url" : "read.ht\/BKL1"
    } ]
  },
  "geo" : { },
  "id_str" : "772128740038115328",
  "text" : "RT @htTweets: Man dies in Malaysia\u2019s first locally transmitted #Zika case https:\/\/t.co\/lmS0Ecq46P https:\/\/t.co\/6nm2D9dI5m",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/htTweets\/status\/772119284969533446\/photo\/1",
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/6nm2D9dI5m",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrceXUlWYAA7Fjy.jpg",
        "id_str" : "772119247128518656",
        "id" : 772119247128518656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrceXUlWYAA7Fjy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6nm2D9dI5m"
      } ],
      "hashtags" : [ {
        "text" : "Zika",
        "indices" : [ 49, 54 ]
      } ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/lmS0Ecq46P",
        "expanded_url" : "http:\/\/read.ht\/BKL1",
        "display_url" : "read.ht\/BKL1"
      } ]
    },
    "geo" : { },
    "id_str" : "772119284969533446",
    "text" : "Man dies in Malaysia\u2019s first locally transmitted #Zika case https:\/\/t.co\/lmS0Ecq46P https:\/\/t.co\/6nm2D9dI5m",
    "id" : 772119284969533446,
    "created_at" : "2016-09-03 17:09:17 +0000",
    "user" : {
      "name" : "Hindustan Times",
      "screen_name" : "htTweets",
      "protected" : false,
      "id_str" : "36327407",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876785263011287040\/cgUWlhF3_normal.jpg",
      "id" : 36327407,
      "verified" : true
    }
  },
  "id" : 772128740038115328,
  "created_at" : "2016-09-03 17:46:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mashableasia",
      "screen_name" : "mashableasia",
      "indices" : [ 3, 16 ],
      "id_str" : "1036198656787996673",
      "id" : 1036198656787996673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/R6AVl92ENt",
      "expanded_url" : "http:\/\/on.mash.to\/2byGXeF",
      "display_url" : "on.mash.to\/2byGXeF"
    } ]
  },
  "geo" : { },
  "id_str" : "772125521144668160",
  "text" : "RT @mashableasia: Singapore posts perfect clapback to BuzzFeed's ice-cream sandwich snub https:\/\/t.co\/R6AVl92ENt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/R6AVl92ENt",
        "expanded_url" : "http:\/\/on.mash.to\/2byGXeF",
        "display_url" : "on.mash.to\/2byGXeF"
      } ]
    },
    "geo" : { },
    "id_str" : "770598803372142593",
    "text" : "Singapore posts perfect clapback to BuzzFeed's ice-cream sandwich snub https:\/\/t.co\/R6AVl92ENt",
    "id" : 770598803372142593,
    "created_at" : "2016-08-30 12:27:26 +0000",
    "user" : {
      "name" : "Mashable Southeast Asia",
      "screen_name" : "MashableSEA",
      "protected" : false,
      "id_str" : "3198041813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025385102321299457\/lp2cJZTV_normal.jpg",
      "id" : 3198041813,
      "verified" : false
    }
  },
  "id" : 772125521144668160,
  "created_at" : "2016-09-03 17:34:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Azeem Azhar",
      "screen_name" : "azeem",
      "indices" : [ 3, 9 ],
      "id_str" : "7640782",
      "id" : 7640782
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/azeem\/status\/771215826795978757\/photo\/1",
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/NVka67YVZM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrPotN5VYAIMqgI.jpg",
      "id_str" : "771215824732381186",
      "id" : 771215824732381186,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrPotN5VYAIMqgI.jpg",
      "sizes" : [ {
        "h" : 380,
        "resize" : "fit",
        "w" : 599
      }, {
        "h" : 380,
        "resize" : "fit",
        "w" : 599
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 380,
        "resize" : "fit",
        "w" : 599
      }, {
        "h" : 380,
        "resize" : "fit",
        "w" : 599
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NVka67YVZM"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772124993064951809",
  "text" : "RT @azeem: Google's data centre in 1998. Don't trip on the wires! Worked out in the end... https:\/\/t.co\/NVka67YVZM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/azeem\/status\/771215826795978757\/photo\/1",
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/NVka67YVZM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrPotN5VYAIMqgI.jpg",
        "id_str" : "771215824732381186",
        "id" : 771215824732381186,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrPotN5VYAIMqgI.jpg",
        "sizes" : [ {
          "h" : 380,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 599
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NVka67YVZM"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771215826795978757",
    "text" : "Google's data centre in 1998. Don't trip on the wires! Worked out in the end... https:\/\/t.co\/NVka67YVZM",
    "id" : 771215826795978757,
    "created_at" : "2016-09-01 05:19:16 +0000",
    "user" : {
      "name" : "Azeem Azhar",
      "screen_name" : "azeem",
      "protected" : false,
      "id_str" : "7640782",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/847767475169222656\/ZFJMki2F_normal.jpg",
      "id" : 7640782,
      "verified" : true
    }
  },
  "id" : 772124993064951809,
  "created_at" : "2016-09-03 17:31:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 0, 11 ],
      "id_str" : "607156756",
      "id" : 607156756
    }, {
      "name" : "Patrick Pang",
      "screen_name" : "patrickpang",
      "indices" : [ 12, 24 ],
      "id_str" : "25312815",
      "id" : 25312815
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "772021820602064896",
  "geo" : { },
  "id_str" : "772122717797703681",
  "in_reply_to_user_id" : 607156756,
  "text" : "@hellofrmSG @patrickpang Hello from Dublin, Ireland",
  "id" : 772122717797703681,
  "in_reply_to_status_id" : 772021820602064896,
  "created_at" : "2016-09-03 17:22:55 +0000",
  "in_reply_to_screen_name" : "hellofrmSG",
  "in_reply_to_user_id_str" : "607156756",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ian",
      "screen_name" : "iamKohChang",
      "indices" : [ 3, 15 ],
      "id_str" : "18635893",
      "id" : 18635893
    }, {
      "name" : "CNN",
      "screen_name" : "CNN",
      "indices" : [ 37, 41 ],
      "id_str" : "759251",
      "id" : 759251
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/iamKohChang\/status\/772051868495204352\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/oGzxpDqx6E",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrbhFF0UEAARgVx.jpg",
      "id_str" : "772051863717810176",
      "id" : 772051863717810176,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrbhFF0UEAARgVx.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 382
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 539
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 539
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 539
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oGzxpDqx6E"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "772120283226501120",
  "text" : "RT @iamKohChang: This should be fun. @CNN that doesn't look like the Thai King https:\/\/t.co\/oGzxpDqx6E",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "CNN",
        "screen_name" : "CNN",
        "indices" : [ 20, 24 ],
        "id_str" : "759251",
        "id" : 759251
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iamKohChang\/status\/772051868495204352\/photo\/1",
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/oGzxpDqx6E",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrbhFF0UEAARgVx.jpg",
        "id_str" : "772051863717810176",
        "id" : 772051863717810176,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrbhFF0UEAARgVx.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 382
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 539
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 539
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 539
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oGzxpDqx6E"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "772051868495204352",
    "text" : "This should be fun. @CNN that doesn't look like the Thai King https:\/\/t.co\/oGzxpDqx6E",
    "id" : 772051868495204352,
    "created_at" : "2016-09-03 12:41:24 +0000",
    "user" : {
      "name" : "Ian",
      "screen_name" : "iamKohChang",
      "protected" : false,
      "id_str" : "18635893",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/94472981\/kohchangphoto-196_normal.jpg",
      "id" : 18635893,
      "verified" : false
    }
  },
  "id" : 772120283226501120,
  "created_at" : "2016-09-03 17:13:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/1zJ2LSKEtH",
      "expanded_url" : "https:\/\/www.swarmapp.com\/c\/lhZZGEEmde6",
      "display_url" : "swarmapp.com\/c\/lhZZGEEmde6"
    } ]
  },
  "geo" : { },
  "id_str" : "771994781635477504",
  "text" : "There is no bin for my chewing gum in this college! (@ Kylemore College Ballyfermot) https:\/\/t.co\/1zJ2LSKEtH",
  "id" : 771994781635477504,
  "created_at" : "2016-09-03 08:54:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Rentoul",
      "screen_name" : "JohnRentoul",
      "indices" : [ 3, 15 ],
      "id_str" : "14085096",
      "id" : 14085096
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771969518038032385",
  "text" : "RT @JohnRentoul: A basic income of \u00A38,320 a year, excl housing &amp; council tax benefit, requires income tax rates of 48%, 68% &amp; 73% https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/t2mggMV5mC",
        "expanded_url" : "https:\/\/www.iser.essex.ac.uk\/research\/publications\/working-papers\/euromod\/em6-15.pdf",
        "display_url" : "iser.essex.ac.uk\/research\/publi\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "771637445897007104",
    "geo" : { },
    "id_str" : "771672432620867584",
    "in_reply_to_user_id" : 14085096,
    "text" : "A basic income of \u00A38,320 a year, excl housing &amp; council tax benefit, requires income tax rates of 48%, 68% &amp; 73% https:\/\/t.co\/t2mggMV5mC",
    "id" : 771672432620867584,
    "in_reply_to_status_id" : 771637445897007104,
    "created_at" : "2016-09-02 11:33:39 +0000",
    "in_reply_to_screen_name" : "JohnRentoul",
    "in_reply_to_user_id_str" : "14085096",
    "user" : {
      "name" : "John Rentoul",
      "screen_name" : "JohnRentoul",
      "protected" : false,
      "id_str" : "14085096",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686469551119269888\/q9ToXXT7_normal.jpg",
      "id" : 14085096,
      "verified" : true
    }
  },
  "id" : 771969518038032385,
  "created_at" : "2016-09-03 07:14:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/Ymh85Hnsqu",
      "expanded_url" : "https:\/\/twitter.com\/revodavid\/status\/771814475422916609",
      "display_url" : "twitter.com\/revodavid\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771828660999421952",
  "text" : "I read about this in one my reference book! https:\/\/t.co\/Ymh85Hnsqu",
  "id" : 771828660999421952,
  "created_at" : "2016-09-02 21:54:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771800318434746368",
  "text" : "It not a Gaelscoil yet the school website's Info for Parents section is in Gaelic.",
  "id" : 771800318434746368,
  "created_at" : "2016-09-02 20:01:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Askingforafriend",
      "indices" : [ 0, 17 ]
    } ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/Bp88gvMWGc",
      "expanded_url" : "https:\/\/twitter.com\/cookmanchester\/status\/677122891083874307",
      "display_url" : "twitter.com\/cookmanchester\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771797181640667136",
  "text" : "#Askingforafriend Any other alternatives in Manchester? https:\/\/t.co\/Bp88gvMWGc",
  "id" : 771797181640667136,
  "created_at" : "2016-09-02 19:49:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/YbQ53dGOST",
      "expanded_url" : "https:\/\/github.com\/mryap\/Programming-for-Big-Data-Assignment",
      "display_url" : "github.com\/mryap\/Programm\u2026"
    }, {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/Cx4AmWsCao",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/771315854441058304",
      "display_url" : "twitter.com\/mryap\/status\/7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771795404090445824",
  "text" : "I practice what I tweeted. Just made available my course work at https:\/\/t.co\/YbQ53dGOST https:\/\/t.co\/Cx4AmWsCao",
  "id" : 771795404090445824,
  "created_at" : "2016-09-02 19:42:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alice Zheng",
      "screen_name" : "RainyData",
      "indices" : [ 127, 137 ],
      "id_str" : "2987567569",
      "id" : 2987567569
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/4ckUdsQP21",
      "expanded_url" : "http:\/\/www.oreilly.com\/data\/free\/archive.html?imm_mid=0e7547&cmp=em-data-free-na-stny16_nem4_end_summer",
      "display_url" : "oreilly.com\/data\/free\/arch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771733746785607681",
  "text" : "A collection of 83 free ebooks on data-related topics. https:\/\/t.co\/4ckUdsQP21 My fav is Evaluating Machine Learning Models by @rainydata",
  "id" : 771733746785607681,
  "created_at" : "2016-09-02 15:37:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/1G1L7Rr0L0",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/The_Tale_of_the_Princess_Kaguya",
      "display_url" : "en.wikipedia.org\/wiki\/The_Tale_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771732215533305856",
  "text" : "Just watched this on Film 4 https:\/\/t.co\/1G1L7Rr0L0  The English dub version",
  "id" : 771732215533305856,
  "created_at" : "2016-09-02 15:31:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "indices" : [ 0, 14 ],
      "id_str" : "14157788",
      "id" : 14157788
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "771726411203608576",
  "geo" : { },
  "id_str" : "771731497170694144",
  "in_reply_to_user_id" : 14157788,
  "text" : "@iheartbeijing good to hear that",
  "id" : 771731497170694144,
  "in_reply_to_status_id" : 771726411203608576,
  "created_at" : "2016-09-02 15:28:21 +0000",
  "in_reply_to_screen_name" : "iheartbeijing",
  "in_reply_to_user_id_str" : "14157788",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "indices" : [ 3, 19 ],
      "id_str" : "2735246778",
      "id" : 2735246778
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/771384348121628676\/photo\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/lvubi9LiK3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CrSBlQPXEAAf7Lf.jpg",
      "id_str" : "771383913201733632",
      "id" : 771383913201733632,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrSBlQPXEAAf7Lf.jpg",
      "sizes" : [ {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 500
      } ],
      "media_alt" : "A romantic, candlelit dinner would be incomplete without: An email about a critical production bug",
      "display_url" : "pic.twitter.com\/lvubi9LiK3"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771700563528523776",
  "text" : "RT @ThePracticalDev: Cards Against Developers https:\/\/t.co\/lvubi9LiK3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/771384348121628676\/photo\/1",
        "indices" : [ 25, 48 ],
        "url" : "https:\/\/t.co\/lvubi9LiK3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrSBlQPXEAAf7Lf.jpg",
        "id_str" : "771383913201733632",
        "id" : 771383913201733632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrSBlQPXEAAf7Lf.jpg",
        "sizes" : [ {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/lvubi9LiK3"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771384348121628676",
    "text" : "Cards Against Developers https:\/\/t.co\/lvubi9LiK3",
    "id" : 771384348121628676,
    "created_at" : "2016-09-01 16:28:54 +0000",
    "user" : {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "protected" : false,
      "id_str" : "2735246778",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002604104194056192\/IEoNsLNM_normal.jpg",
      "id" : 2735246778,
      "verified" : false
    }
  },
  "id" : 771700563528523776,
  "created_at" : "2016-09-02 13:25:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Fintech",
      "indices" : [ 103, 111 ]
    }, {
      "text" : "Food",
      "indices" : [ 112, 117 ]
    }, {
      "text" : "whisky",
      "indices" : [ 118, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771645413690478594",
  "text" : "RT @KapilMespil: Why Hong Kong must be on your Radar? Great insight on land of opportunities for Irish #Fintech #Food #whisky sector! https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KapilMespil\/status\/771618219744333824\/photo\/1",
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/9oyQybyRgD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrVWqzPVIAAFhEm.jpg",
        "id_str" : "771618204472909824",
        "id" : 771618204472909824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrVWqzPVIAAFhEm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9oyQybyRgD"
      } ],
      "hashtags" : [ {
        "text" : "Fintech",
        "indices" : [ 86, 94 ]
      }, {
        "text" : "Food",
        "indices" : [ 95, 100 ]
      }, {
        "text" : "whisky",
        "indices" : [ 101, 108 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771618219744333824",
    "text" : "Why Hong Kong must be on your Radar? Great insight on land of opportunities for Irish #Fintech #Food #whisky sector! https:\/\/t.co\/9oyQybyRgD",
    "id" : 771618219744333824,
    "created_at" : "2016-09-02 07:58:14 +0000",
    "user" : {
      "name" : "Kapil Khanna",
      "screen_name" : "KapilKhannaIRL",
      "protected" : false,
      "id_str" : "64289556",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930879798351683584\/EyPm5WP-_normal.jpg",
      "id" : 64289556,
      "verified" : false
    }
  },
  "id" : 771645413690478594,
  "created_at" : "2016-09-02 09:46:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Tax Payer",
      "screen_name" : "IrishTaxPayer",
      "indices" : [ 3, 17 ],
      "id_str" : "539392460",
      "id" : 539392460
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vinb",
      "indices" : [ 19, 24 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771475994762944514",
  "text" : "RT @IrishTaxPayer: #vinb Govt needs to ensure fathers, rather than tax payers, financially support single mothers",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "vinb",
        "indices" : [ 0, 5 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "762781537385480192",
    "text" : "#vinb Govt needs to ensure fathers, rather than tax payers, financially support single mothers",
    "id" : 762781537385480192,
    "created_at" : "2016-08-08 22:44:24 +0000",
    "user" : {
      "name" : "Irish Tax Payer",
      "screen_name" : "IrishTaxPayer",
      "protected" : false,
      "id_str" : "539392460",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1980455139\/revenuerising_normal.jpg",
      "id" : 539392460,
      "verified" : false
    }
  },
  "id" : 771475994762944514,
  "created_at" : "2016-09-01 22:33:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "FORMarchitecture",
      "screen_name" : "form_architect",
      "indices" : [ 3, 18 ],
      "id_str" : "98117240",
      "id" : 98117240
    }, {
      "name" : "David McCullagh",
      "screen_name" : "mcculld",
      "indices" : [ 27, 35 ],
      "id_str" : "242705768",
      "id" : 242705768
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rtept",
      "indices" : [ 131, 137 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771461066836602880",
  "text" : "RT @form_architect: Go on, @mcculld , ask Ruth if  Apple should be nationalised, like she wanted Dell nationalised ? For the lols. #rtept",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "David McCullagh",
        "screen_name" : "mcculld",
        "indices" : [ 7, 15 ],
        "id_str" : "242705768",
        "id" : 242705768
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rtept",
        "indices" : [ 111, 117 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771451505937354752",
    "text" : "Go on, @mcculld , ask Ruth if  Apple should be nationalised, like she wanted Dell nationalised ? For the lols. #rtept",
    "id" : 771451505937354752,
    "created_at" : "2016-09-01 20:55:46 +0000",
    "user" : {
      "name" : "FORMarchitecture",
      "screen_name" : "form_architect",
      "protected" : false,
      "id_str" : "98117240",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1282640288\/1101_3-Default-000_normal.png",
      "id" : 98117240,
      "verified" : false
    }
  },
  "id" : 771461066836602880,
  "created_at" : "2016-09-01 21:33:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Tax Payer",
      "screen_name" : "IrishTaxPayer",
      "indices" : [ 3, 17 ],
      "id_str" : "539392460",
      "id" : 539392460
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rtept",
      "indices" : [ 19, 25 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771460239346569216",
  "text" : "RT @IrishTaxPayer: #rtept perhaps 30 year old son should consider getting a job to fund his own housing",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rtept",
        "indices" : [ 0, 6 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771458957672116225",
    "text" : "#rtept perhaps 30 year old son should consider getting a job to fund his own housing",
    "id" : 771458957672116225,
    "created_at" : "2016-09-01 21:25:23 +0000",
    "user" : {
      "name" : "Irish Tax Payer",
      "screen_name" : "IrishTaxPayer",
      "protected" : false,
      "id_str" : "539392460",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1980455139\/revenuerising_normal.jpg",
      "id" : 539392460,
      "verified" : false
    }
  },
  "id" : 771460239346569216,
  "created_at" : "2016-09-01 21:30:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771387042110509056",
  "text" : "RT @lisaocarroll: JeeZe. Listening to RTE radio about abortion like going back to 80s. Some angry man called Ronan ranting about women as i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "771361694727671808",
    "text" : "JeeZe. Listening to RTE radio about abortion like going back to 80s. Some angry man called Ronan ranting about women as if he ran us....",
    "id" : 771361694727671808,
    "created_at" : "2016-09-01 14:58:53 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 771387042110509056,
  "created_at" : "2016-09-01 16:39:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cronan McNamara",
      "screen_name" : "cronanmcnamara",
      "indices" : [ 3, 18 ],
      "id_str" : "14846672",
      "id" : 14846672
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MichelinStar",
      "indices" : [ 63, 76 ]
    }, {
      "text" : "food",
      "indices" : [ 77, 82 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/nZhfnCwNcD",
      "expanded_url" : "http:\/\/buff.ly\/2bGoEmn",
      "display_url" : "buff.ly\/2bGoEmn"
    } ]
  },
  "geo" : { },
  "id_str" : "771361663840911360",
  "text" : "RT @cronanmcnamara: If you are in Singapore, visit this place: #MichelinStar #food for price of a Big Mac https:\/\/t.co\/nZhfnCwNcD https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cronanmcnamara\/status\/771361501827502081\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/EKUy1u4GcK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrRtMmAXYAA7b0u.jpg",
        "id_str" : "771361499315134464",
        "id" : 771361499315134464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrRtMmAXYAA7b0u.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EKUy1u4GcK"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/cronanmcnamara\/status\/771361501827502081\/photo\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/EKUy1u4GcK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CrRtMbYW8AAf9k_.jpg",
        "id_str" : "771361496462979072",
        "id" : 771361496462979072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CrRtMbYW8AAf9k_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        }, {
          "h" : 180,
          "resize" : "fit",
          "w" : 300
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EKUy1u4GcK"
      } ],
      "hashtags" : [ {
        "text" : "MichelinStar",
        "indices" : [ 43, 56 ]
      }, {
        "text" : "food",
        "indices" : [ 57, 62 ]
      } ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/nZhfnCwNcD",
        "expanded_url" : "http:\/\/buff.ly\/2bGoEmn",
        "display_url" : "buff.ly\/2bGoEmn"
      } ]
    },
    "geo" : { },
    "id_str" : "771361501827502081",
    "text" : "If you are in Singapore, visit this place: #MichelinStar #food for price of a Big Mac https:\/\/t.co\/nZhfnCwNcD https:\/\/t.co\/EKUy1u4GcK",
    "id" : 771361501827502081,
    "created_at" : "2016-09-01 14:58:07 +0000",
    "user" : {
      "name" : "Cronan McNamara",
      "screen_name" : "cronanmcnamara",
      "protected" : false,
      "id_str" : "14846672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/764055303688183808\/ctl5KPg__normal.jpg",
      "id" : 14846672,
      "verified" : true
    }
  },
  "id" : 771361663840911360,
  "created_at" : "2016-09-01 14:58:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/JJZ5MRrGhw",
      "expanded_url" : "http:\/\/www.nbcnews.com\/storyline\/zika-virus-outbreak\/cdc-says-pregnant-women-should-avoid-zika-affected-singapore-n640681",
      "display_url" : "nbcnews.com\/storyline\/zika\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771337617346625537",
  "text" : "CDC added Singapore to its list of Zika-affected countries that pregnant women should avoid. https:\/\/t.co\/JJZ5MRrGhw",
  "id" : 771337617346625537,
  "created_at" : "2016-09-01 13:23:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771320656390676480",
  "text" : "I can't remember when the last time I ftp files",
  "id" : 771320656390676480,
  "created_at" : "2016-09-01 12:15:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PLOSCompBio",
      "indices" : [ 37, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/rj5MpE5nIX",
      "expanded_url" : "http:\/\/dx.doi.org\/10.1371\/journal.pcbi.1003542",
      "display_url" : "dx.doi.org\/10.1371\/journa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "771315854441058304",
  "text" : "Make Your Data Analysis Reproducible #PLOSCompBio: 10 Simple Rules for the Care and Feeding of Scientific Data https:\/\/t.co\/rj5MpE5nIX",
  "id" : 771315854441058304,
  "created_at" : "2016-09-01 11:56:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "771132335811137537",
  "text" : "So a food blogger &amp; social media consultant was recruited by HSE to address staff about his vision for healthcare in Ireland...",
  "id" : 771132335811137537,
  "created_at" : "2016-08-31 23:47:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]