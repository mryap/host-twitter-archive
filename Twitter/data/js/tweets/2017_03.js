Grailbird.data.tweets_2017_03 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847779787556966400",
  "text" : "2. Advertisers need to be assured your website deliver the right type of customer.",
  "id" : 847779787556966400,
  "created_at" : "2017-03-31 11:57:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847779405296537601",
  "text" : "1. If you are selling advertising space on your site, consider making available the website traffic data when requested. Don't hide.",
  "id" : 847779405296537601,
  "created_at" : "2017-03-31 11:55:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/847777493985742848\/photo\/1",
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/uZvSeqSjHU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C8PolI8WsAAyyIG.jpg",
      "id_str" : "847776879629217792",
      "id" : 847776879629217792,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8PolI8WsAAyyIG.jpg",
      "sizes" : [ {
        "h" : 179,
        "resize" : "fit",
        "w" : 729
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 179,
        "resize" : "fit",
        "w" : 729
      }, {
        "h" : 179,
        "resize" : "fit",
        "w" : 729
      }, {
        "h" : 167,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uZvSeqSjHU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847777493985742848",
  "text" : "Request website traffic data for scrutiny 11 days ago, so far no response.... https:\/\/t.co\/uZvSeqSjHU",
  "id" : 847777493985742848,
  "created_at" : "2017-03-31 11:48:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "pitch4progress",
      "indices" : [ 42, 57 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847773733276221440",
  "text" : "I'm Attending All-Ireland Business Summit #pitch4progress If you need help with measuring your online biz, let connect.",
  "id" : 847773733276221440,
  "created_at" : "2017-03-31 11:33:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/847749567051218945\/photo\/1",
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/wxdOii9HQi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C8PPgOSVYAIwVgX.jpg",
      "id_str" : "847749307373543426",
      "id" : 847749307373543426,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8PPgOSVYAIwVgX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 391,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 920
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 920
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 920
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wxdOii9HQi"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847749567051218945",
  "text" : "Top 5 for the past 30 days. https:\/\/t.co\/wxdOii9HQi",
  "id" : 847749567051218945,
  "created_at" : "2017-03-31 09:57:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/847746289081008128\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/0d0kcGf7Ia",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C8PMwJkUQAENtvL.jpg",
      "id_str" : "847746282449813505",
      "id" : 847746282449813505,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8PMwJkUQAENtvL.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 528,
        "resize" : "fit",
        "w" : 467
      }, {
        "h" : 528,
        "resize" : "fit",
        "w" : 467
      }, {
        "h" : 528,
        "resize" : "fit",
        "w" : 467
      }, {
        "h" : 528,
        "resize" : "fit",
        "w" : 467
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0d0kcGf7Ia"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/cwWQYYmoQC",
      "expanded_url" : "https:\/\/analytics.twitter.com\/user\/mryap\/tweets",
      "display_url" : "analytics.twitter.com\/user\/mryap\/twe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "847746289081008128",
  "text" : "Doing a Time-series Analysis on my account https:\/\/t.co\/cwWQYYmoQC from 31 Dec 2016 to 31 March 2017. https:\/\/t.co\/0d0kcGf7Ia",
  "id" : 847746289081008128,
  "created_at" : "2017-03-31 09:44:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847735755287912451",
  "text" : "Googling Secondary pickets now.",
  "id" : 847735755287912451,
  "created_at" : "2017-03-31 09:02:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joe Mc Callion",
      "screen_name" : "joeymccallion",
      "indices" : [ 3, 17 ],
      "id_str" : "150649929",
      "id" : 150649929
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DublinBus",
      "indices" : [ 113, 123 ]
    }, {
      "text" : "irishrail",
      "indices" : [ 124, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847734888526618625",
  "text" : "RT @joeymccallion: The irony of not being able to make your morning lecture on Karl Marx due to a workers strike #DublinBus #irishrail",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DublinBus",
        "indices" : [ 94, 104 ]
      }, {
        "text" : "irishrail",
        "indices" : [ 105, 115 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "847725446179442690",
    "text" : "The irony of not being able to make your morning lecture on Karl Marx due to a workers strike #DublinBus #irishrail",
    "id" : 847725446179442690,
    "created_at" : "2017-03-31 08:21:11 +0000",
    "user" : {
      "name" : "Joe Mc Callion",
      "screen_name" : "joeymccallion",
      "protected" : false,
      "id_str" : "150649929",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007632960688939009\/5qwtRnVp_normal.jpg",
      "id" : 150649929,
      "verified" : false
    }
  },
  "id" : 847734888526618625,
  "created_at" : "2017-03-31 08:58:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gerry from Mayo",
      "screen_name" : "unlaoised",
      "indices" : [ 3, 13 ],
      "id_str" : "19677936",
      "id" : 19677936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847732229937680384",
  "text" : "RT @unlaoised: Hospital appts people  have waited months, years even, will have to be rescheduled. But let\u2019s celebrate solidarity https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/PPSinFmUjs",
        "expanded_url" : "https:\/\/twitter.com\/cllrmobrien\/status\/847702250034089984",
        "display_url" : "twitter.com\/cllrmobrien\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "847716134929416192",
    "text" : "Hospital appts people  have waited months, years even, will have to be rescheduled. But let\u2019s celebrate solidarity https:\/\/t.co\/PPSinFmUjs",
    "id" : 847716134929416192,
    "created_at" : "2017-03-31 07:44:11 +0000",
    "user" : {
      "name" : "Gerry from Mayo",
      "screen_name" : "unlaoised",
      "protected" : false,
      "id_str" : "19677936",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035265455819251712\/vVM2wSsx_normal.jpg",
      "id" : 19677936,
      "verified" : false
    }
  },
  "id" : 847732229937680384,
  "created_at" : "2017-03-31 08:48:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melissa",
      "screen_name" : "melissa__juarez",
      "indices" : [ 3, 19 ],
      "id_str" : "156973052",
      "id" : 156973052
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847729777679007745",
  "text" : "RT @melissa__juarez: Remember in Minority Report where Tom walks into Gap and the hologram knows his name &amp; last purchase? Adobe just did t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "adobesummit",
        "indices" : [ 127, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844240500076957696",
    "text" : "Remember in Minority Report where Tom walks into Gap and the hologram knows his name &amp; last purchase? Adobe just did that. #adobesummit",
    "id" : 844240500076957696,
    "created_at" : "2017-03-21 17:33:15 +0000",
    "user" : {
      "name" : "Melissa",
      "screen_name" : "melissa__juarez",
      "protected" : false,
      "id_str" : "156973052",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/782969303884664833\/hkWSXqL3_normal.jpg",
      "id" : 156973052,
      "verified" : false
    }
  },
  "id" : 847729777679007745,
  "created_at" : "2017-03-31 08:38:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/A0e0kSBIiS",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/BBz5Gg3?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/BBz5G\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "847729434878558208",
  "text" : "Strike notice served at 4:05am this morning?! https:\/\/t.co\/A0e0kSBIiS",
  "id" : 847729434878558208,
  "created_at" : "2017-03-31 08:37:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847580597341962241",
  "text" : "If your webpage has high bounce rate, try remove the newsletter signup form and see whether it helps.",
  "id" : 847580597341962241,
  "created_at" : "2017-03-30 22:45:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847570780137426946",
  "text" : "Dear website owner, I was just about to start reading your content and your newsletter signup form just hit me. You are so desperate.",
  "id" : 847570780137426946,
  "created_at" : "2017-03-30 22:06:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 3, 12 ],
      "id_str" : "136690988",
      "id" : 136690988
    }, {
      "name" : "Alex Kistenev",
      "screen_name" : "alexkistenev",
      "indices" : [ 74, 87 ],
      "id_str" : "332367960",
      "id" : 332367960
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/JxswtBPf4f",
      "expanded_url" : "https:\/\/productcoalition.com\/100-slack-apps-and-integrations-for-a-product-manager-52f44ec2d8f4#.7nnnr2ejm",
      "display_url" : "productcoalition.com\/100-slack-apps\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "847559455030030338",
  "text" : "RT @ailieirv: \u201C100+ Slack Apps and Integrations for a Product Manager\u201D by @alexkistenev https:\/\/t.co\/JxswtBPf4f",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Alex Kistenev",
        "screen_name" : "alexkistenev",
        "indices" : [ 60, 73 ],
        "id_str" : "332367960",
        "id" : 332367960
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/JxswtBPf4f",
        "expanded_url" : "https:\/\/productcoalition.com\/100-slack-apps-and-integrations-for-a-product-manager-52f44ec2d8f4#.7nnnr2ejm",
        "display_url" : "productcoalition.com\/100-slack-apps\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "846793477769834498",
    "text" : "\u201C100+ Slack Apps and Integrations for a Product Manager\u201D by @alexkistenev https:\/\/t.co\/JxswtBPf4f",
    "id" : 846793477769834498,
    "created_at" : "2017-03-28 18:37:52 +0000",
    "user" : {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "protected" : false,
      "id_str" : "136690988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528642221956796416\/WDp_wLcX_normal.jpeg",
      "id" : 136690988,
      "verified" : false
    }
  },
  "id" : 847559455030030338,
  "created_at" : "2017-03-30 21:21:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fermat's Library",
      "screen_name" : "fermatslibrary",
      "indices" : [ 3, 18 ],
      "id_str" : "3511430425",
      "id" : 3511430425
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847557566871851009",
  "text" : "RT @fermatslibrary: NEW PAPER:\n\nThis week we annotated a presentation by Alan Turing in 1951 where he discusses ML and AI.\n\nRead here: http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/fermatslibrary\/status\/846832073209921537\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/2noH0Zwtzz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C8CNRq0VsAEIjrx.jpg",
        "id_str" : "846832064636760065",
        "id" : 846832064636760065,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8CNRq0VsAEIjrx.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 244,
          "resize" : "fit",
          "w" : 483
        }, {
          "h" : 244,
          "resize" : "fit",
          "w" : 483
        }, {
          "h" : 244,
          "resize" : "fit",
          "w" : 483
        }, {
          "h" : 244,
          "resize" : "fit",
          "w" : 483
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2noH0Zwtzz"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/TM42FKe8Xs",
        "expanded_url" : "http:\/\/fermatslibrary.com\/s\/intelligent-machinery-a-heretical-theory",
        "display_url" : "fermatslibrary.com\/s\/intelligent-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "846832073209921537",
    "text" : "NEW PAPER:\n\nThis week we annotated a presentation by Alan Turing in 1951 where he discusses ML and AI.\n\nRead here: https:\/\/t.co\/TM42FKe8Xs https:\/\/t.co\/2noH0Zwtzz",
    "id" : 846832073209921537,
    "created_at" : "2017-03-28 21:11:14 +0000",
    "user" : {
      "name" : "Fermat's Library",
      "screen_name" : "fermatslibrary",
      "protected" : false,
      "id_str" : "3511430425",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/641822497457860608\/zd6kUAag_normal.png",
      "id" : 3511430425,
      "verified" : false
    }
  },
  "id" : 847557566871851009,
  "created_at" : "2017-03-30 21:14:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 0, 10 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "847499953446412290",
  "geo" : { },
  "id_str" : "847522892199989250",
  "in_reply_to_user_id" : 18849187,
  "text" : "@barryhand Yes it a CMS on top. In your case, you might not need it.",
  "id" : 847522892199989250,
  "in_reply_to_status_id" : 847499953446412290,
  "created_at" : "2017-03-30 18:56:18 +0000",
  "in_reply_to_screen_name" : "barryhand",
  "in_reply_to_user_id_str" : "18849187",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 0, 10 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/73PxKen8J8",
      "expanded_url" : "https:\/\/www.siteleaf.com",
      "display_url" : "siteleaf.com"
    } ]
  },
  "in_reply_to_status_id_str" : "847476359777497088",
  "geo" : { },
  "id_str" : "847483358481432576",
  "in_reply_to_user_id" : 18849187,
  "text" : "@barryhand checkout https:\/\/t.co\/73PxKen8J8",
  "id" : 847483358481432576,
  "in_reply_to_status_id" : 847476359777497088,
  "created_at" : "2017-03-30 16:19:12 +0000",
  "in_reply_to_screen_name" : "barryhand",
  "in_reply_to_user_id_str" : "18849187",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mailman Group",
      "screen_name" : "MailmanGroup",
      "indices" : [ 3, 16 ],
      "id_str" : "461734125",
      "id" : 461734125
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847471063818579970",
  "text" : "RT @MailmanGroup: It's fair to say China's advertising departments are having a lot of fun with Apple's new Product Red iPhone ads https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/E4LlWPL9Pd",
        "expanded_url" : "http:\/\/buff.ly\/2o4Llrw",
        "display_url" : "buff.ly\/2o4Llrw"
      } ]
    },
    "geo" : { },
    "id_str" : "847125746103570432",
    "text" : "It's fair to say China's advertising departments are having a lot of fun with Apple's new Product Red iPhone ads https:\/\/t.co\/E4LlWPL9Pd",
    "id" : 847125746103570432,
    "created_at" : "2017-03-29 16:38:11 +0000",
    "user" : {
      "name" : "Mailman Group",
      "screen_name" : "MailmanGroup",
      "protected" : false,
      "id_str" : "461734125",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882817090020835328\/ymYLIAm8_normal.jpg",
      "id" : 461734125,
      "verified" : false
    }
  },
  "id" : 847471063818579970,
  "created_at" : "2017-03-30 15:30:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Grainne Kirwan",
      "screen_name" : "Kirwanpsych",
      "indices" : [ 3, 15 ],
      "id_str" : "841360588034961408",
      "id" : 841360588034961408
    }, {
      "name" : "DIT",
      "screen_name" : "ditofficial",
      "indices" : [ 68, 80 ],
      "id_str" : "95665759",
      "id" : 95665759
    }, {
      "name" : "All Aboard 2017",
      "screen_name" : "allaboard2017",
      "indices" : [ 85, 99 ],
      "id_str" : "822086282847387649",
      "id" : 822086282847387649
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847380109048782850",
  "text" : "RT @Kirwanpsych: I'm excited to be talking about online identity in @ditofficial for @allaboard2017 next Monday - places available at https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DIT",
        "screen_name" : "ditofficial",
        "indices" : [ 51, 63 ],
        "id_str" : "95665759",
        "id" : 95665759
      }, {
        "name" : "All Aboard 2017",
        "screen_name" : "allaboard2017",
        "indices" : [ 68, 82 ],
        "id_str" : "822086282847387649",
        "id" : 822086282847387649
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/8gSIKBvDI5",
        "expanded_url" : "https:\/\/www.eventbrite.ie\/e\/who-do-you-think-you-are-online-registration-33112837388",
        "display_url" : "eventbrite.ie\/e\/who-do-you-t\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "847378770097045504",
    "text" : "I'm excited to be talking about online identity in @ditofficial for @allaboard2017 next Monday - places available at https:\/\/t.co\/8gSIKBvDI5",
    "id" : 847378770097045504,
    "created_at" : "2017-03-30 09:23:36 +0000",
    "user" : {
      "name" : "Dr. Grainne Kirwan",
      "screen_name" : "Kirwanpsych",
      "protected" : false,
      "id_str" : "841360588034961408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/873702410652438528\/Es1_T2CC_normal.jpg",
      "id" : 841360588034961408,
      "verified" : false
    }
  },
  "id" : 847380109048782850,
  "created_at" : "2017-03-30 09:28:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/XsYdwoV05Y",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BSQMvAoAZKX\/",
      "display_url" : "instagram.com\/p\/BSQMvAoAZKX\/"
    } ]
  },
  "geo" : { },
  "id_str" : "847342750785613827",
  "text" : "Dublin Docklands https:\/\/t.co\/XsYdwoV05Y",
  "id" : 847342750785613827,
  "created_at" : "2017-03-30 07:00:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "datascience",
      "indices" : [ 63, 75 ]
    }, {
      "text" : "SmartNation",
      "indices" : [ 119, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847212869439643649",
  "text" : "RT @GovTechSG: 10,000 public servants will receive training in #datascience to tackle challenges in the public sector. #SmartNation https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/locowise.com\" rel=\"nofollow\"\u003ELocowise Analytics\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "datascience",
        "indices" : [ 48, 60 ]
      }, {
        "text" : "SmartNation",
        "indices" : [ 104, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/IGmvZhWPRs",
        "expanded_url" : "http:\/\/j.mp\/2nzfHRF",
        "display_url" : "j.mp\/2nzfHRF"
      } ]
    },
    "geo" : { },
    "id_str" : "847074619387346944",
    "text" : "10,000 public servants will receive training in #datascience to tackle challenges in the public sector. #SmartNation https:\/\/t.co\/IGmvZhWPRs",
    "id" : 847074619387346944,
    "created_at" : "2017-03-29 13:15:01 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 847212869439643649,
  "created_at" : "2017-03-29 22:24:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/847210052498968576\/photo\/1",
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/3FSQvsJ94u",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C8HlBXHXsAANAEs.jpg",
      "id_str" : "847210016469921792",
      "id" : 847210016469921792,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8HlBXHXsAANAEs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3FSQvsJ94u"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/847210052498968576\/photo\/1",
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/3FSQvsJ94u",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C8HlBXJXUAAPzfP.jpg",
      "id_str" : "847210016478285824",
      "id" : 847210016478285824,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8HlBXJXUAAPzfP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3FSQvsJ94u"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/847210052498968576\/photo\/1",
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/3FSQvsJ94u",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C8HlBXGW4AAAPSl.jpg",
      "id_str" : "847210016465674240",
      "id" : 847210016465674240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8HlBXGW4AAAPSl.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3FSQvsJ94u"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/sWMOWZg08p",
      "expanded_url" : "https:\/\/www.google.ie\/supplierdiversity\/",
      "display_url" : "google.ie\/supplierdivers\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "847210052498968576",
  "text" : "Ireland is the first country outside US to roll out Google Small Business Supplier Diversity Program https:\/\/t.co\/sWMOWZg08p https:\/\/t.co\/3FSQvsJ94u",
  "id" : 847210052498968576,
  "created_at" : "2017-03-29 22:13:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Troy Hunt",
      "screen_name" : "troyhunt",
      "indices" : [ 3, 12 ],
      "id_str" : "14414286",
      "id" : 14414286
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847168525642993665",
  "text" : "RT @troyhunt: As a conference speaker, about the most annoying thing you can ask me to do is to use your slide template...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "809561811854848001",
    "text" : "As a conference speaker, about the most annoying thing you can ask me to do is to use your slide template...",
    "id" : 809561811854848001,
    "created_at" : "2016-12-16 00:52:31 +0000",
    "user" : {
      "name" : "Troy Hunt",
      "screen_name" : "troyhunt",
      "protected" : false,
      "id_str" : "14414286",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1154092629\/Square__small__normal.jpg",
      "id" : 14414286,
      "verified" : true
    }
  },
  "id" : 847168525642993665,
  "created_at" : "2017-03-29 19:28:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/bcBnsCilOl",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BSOc9h0gcA2\/",
      "display_url" : "instagram.com\/p\/BSOc9h0gcA2\/"
    } ]
  },
  "geo" : { },
  "id_str" : "847097004463439872",
  "text" : "I am not sure what I can provide to Google. About to find out. @ Google European Headoffice -\u2026 https:\/\/t.co\/bcBnsCilOl",
  "id" : 847097004463439872,
  "created_at" : "2017-03-29 14:43:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/R8U2RSqBI8",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BSOIMOvgY7I\/",
      "display_url" : "instagram.com\/p\/BSOIMOvgY7I\/"
    } ]
  },
  "geo" : { },
  "id_str" : "847051285090238464",
  "text" : "Love the sunshine from the window rooftop that project to the wall. https:\/\/t.co\/R8U2RSqBI8",
  "id" : 847051285090238464,
  "created_at" : "2017-03-29 11:42:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/lHuxnr3cmV",
      "expanded_url" : "http:\/\/allaboard2017.ie\/",
      "display_url" : "allaboard2017.ie"
    } ]
  },
  "geo" : { },
  "id_str" : "847035007793319937",
  "text" : "Series of national &amp; regional events to build confidence &amp; competence in digital skills for learning. https:\/\/t.co\/lHuxnr3cmV",
  "id" : 847035007793319937,
  "created_at" : "2017-03-29 10:37:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "indices" : [ 3, 16 ],
      "id_str" : "854642300",
      "id" : 854642300
    }, {
      "name" : "Ben Klaasen",
      "screen_name" : "trampeur",
      "indices" : [ 125, 134 ],
      "id_str" : "826753948681924608",
      "id" : 826753948681924608
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847034088997470208",
  "text" : "RT @3thoughtbbls: My husband Ben is walking from Dublin to Istanbul - If you're interested you can follow his wild adventure @trampeur! #pa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ben Klaasen",
        "screen_name" : "trampeur",
        "indices" : [ 107, 116 ],
        "id_str" : "826753948681924608",
        "id" : 826753948681924608
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/3thoughtbbls\/status\/831496506586849281\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/jJKOK880yb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C4oRHD9WYAA-x5G.jpg",
        "id_str" : "831495894222659584",
        "id" : 831495894222659584,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C4oRHD9WYAA-x5G.jpg",
        "sizes" : [ {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1400,
          "resize" : "fit",
          "w" : 1867
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1400,
          "resize" : "fit",
          "w" : 1867
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jJKOK880yb"
      } ],
      "hashtags" : [ {
        "text" : "patrickleighfermor",
        "indices" : [ 118, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "831496506586849281",
    "text" : "My husband Ben is walking from Dublin to Istanbul - If you're interested you can follow his wild adventure @trampeur! #patrickleighfermor https:\/\/t.co\/jJKOK880yb",
    "id" : 831496506586849281,
    "created_at" : "2017-02-14 13:33:10 +0000",
    "user" : {
      "name" : "Aisling Nelson",
      "screen_name" : "3thoughtbbls",
      "protected" : false,
      "id_str" : "854642300",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459287079227101184\/Yb2pWHi8_normal.jpeg",
      "id" : 854642300,
      "verified" : false
    }
  },
  "id" : 847034088997470208,
  "created_at" : "2017-03-29 10:33:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/IwRHnYS7Bu",
      "expanded_url" : "https:\/\/twitter.com\/jonboyes\/status\/847003265351008257",
      "display_url" : "twitter.com\/jonboyes\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "847026076794736642",
  "text" : "with guns blazing. https:\/\/t.co\/IwRHnYS7Bu",
  "id" : 847026076794736642,
  "created_at" : "2017-03-29 10:02:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GDS Hive",
      "screen_name" : "GDSHive",
      "indices" : [ 3, 11 ],
      "id_str" : "3743066294",
      "id" : 3743066294
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "847025852152004608",
  "text" : "RT @GDSHive: Kopi Chat at @Blk71Singapore ft. Daniel Lim, Data Scientist of @GovTechSG, sharing w startup community abt application of data\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GovTech (Singapore)",
        "screen_name" : "GovTechSG",
        "indices" : [ 63, 73 ],
        "id_str" : "123167035",
        "id" : 123167035
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GDSHive\/status\/847011532495962112\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/wCfG2Ik8d2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C8EwdAHVsAA19CL.jpg",
        "id_str" : "847011479727484928",
        "id" : 847011479727484928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8EwdAHVsAA19CL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wCfG2Ik8d2"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/GDSHive\/status\/847011532495962112\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/wCfG2Ik8d2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C8EweoDVAAAc6Nk.jpg",
        "id_str" : "847011507627950080",
        "id" : 847011507627950080,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8EweoDVAAAc6Nk.jpg",
        "sizes" : [ {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wCfG2Ik8d2"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "847011532495962112",
    "text" : "Kopi Chat at @Blk71Singapore ft. Daniel Lim, Data Scientist of @GovTechSG, sharing w startup community abt application of data sci in Govt. https:\/\/t.co\/wCfG2Ik8d2",
    "id" : 847011532495962112,
    "created_at" : "2017-03-29 09:04:20 +0000",
    "user" : {
      "name" : "GDS Hive",
      "screen_name" : "GDSHive",
      "protected" : false,
      "id_str" : "3743066294",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/649411335051083776\/fP5iPRNM_normal.png",
      "id" : 3743066294,
      "verified" : false
    }
  },
  "id" : 847025852152004608,
  "created_at" : "2017-03-29 10:01:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Diego Ventura",
      "screen_name" : "colochef",
      "indices" : [ 3, 12 ],
      "id_str" : "20244711",
      "id" : 20244711
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/UFqmvMiZCP",
      "expanded_url" : "http:\/\/tcrn.ch\/2nIVrNX",
      "display_url" : "tcrn.ch\/2nIVrNX"
    } ]
  },
  "geo" : { },
  "id_str" : "847025655271305216",
  "text" : "RT @colochef: Chinese internet giant Tencent buys 5% of\u00A0Tesla https:\/\/t.co\/UFqmvMiZCP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/UFqmvMiZCP",
        "expanded_url" : "http:\/\/tcrn.ch\/2nIVrNX",
        "display_url" : "tcrn.ch\/2nIVrNX"
      } ]
    },
    "geo" : { },
    "id_str" : "847018011177484289",
    "text" : "Chinese internet giant Tencent buys 5% of\u00A0Tesla https:\/\/t.co\/UFqmvMiZCP",
    "id" : 847018011177484289,
    "created_at" : "2017-03-29 09:30:05 +0000",
    "user" : {
      "name" : "Diego Ventura",
      "screen_name" : "colochef",
      "protected" : false,
      "id_str" : "20244711",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/699229374357368832\/U7cgCisc_normal.jpg",
      "id" : 20244711,
      "verified" : false
    }
  },
  "id" : 847025655271305216,
  "created_at" : "2017-03-29 10:00:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/QF0q5DwNcr",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BSN7cdMgSk2\/",
      "display_url" : "instagram.com\/p\/BSN7cdMgSk2\/"
    } ]
  },
  "geo" : { },
  "id_str" : "847023255894577152",
  "text" : "Where is the diversity? Not even a Dad on this panel! https:\/\/t.co\/QF0q5DwNcr",
  "id" : 847023255894577152,
  "created_at" : "2017-03-29 09:50:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846985831776763904",
  "text" : "It's the action not the  analytics that make a difference. Value from analytics comes in the form of action taken.",
  "id" : 846985831776763904,
  "created_at" : "2017-03-29 07:22:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846871120301248516",
  "text" : "This is new to me. HR interview me first before the direct manager.",
  "id" : 846871120301248516,
  "created_at" : "2017-03-28 23:46:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "85053026",
      "id" : 85053026
    }, {
      "name" : "Land Rover",
      "screen_name" : "LandRover",
      "indices" : [ 18, 28 ],
      "id_str" : "212332850",
      "id" : 212332850
    }, {
      "name" : "IFRC",
      "screen_name" : "Federation",
      "indices" : [ 37, 48 ],
      "id_str" : "16025112",
      "id" : 16025112
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846861681179447297",
  "text" : "RT @gavindbrown: .@LandRover unveils @Federation support vehicle w\/ roof-mounted drone that can take off\/land while vehicle is moving https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Land Rover",
        "screen_name" : "LandRover",
        "indices" : [ 1, 11 ],
        "id_str" : "212332850",
        "id" : 212332850
      }, {
        "name" : "IFRC",
        "screen_name" : "Federation",
        "indices" : [ 20, 31 ],
        "id_str" : "16025112",
        "id" : 16025112
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gavindbrown\/status\/846691184122716160\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/tXJ03k8mns",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C8ALO5-XwAArrMS.jpg",
        "id_str" : "846689080654151680",
        "id" : 846689080654151680,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8ALO5-XwAArrMS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 794,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1355,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 2709,
          "resize" : "fit",
          "w" : 4096
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tXJ03k8mns"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/YhBhPyZ8sk",
        "expanded_url" : "http:\/\/emergencyservicestimes.com\/land-rover-unveils-drone-fitted-search-rescue-vehicle-support-red-cross-red-crescent-disaster-response\/",
        "display_url" : "emergencyservicestimes.com\/land-rover-unv\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "846691184122716160",
    "text" : ".@LandRover unveils @Federation support vehicle w\/ roof-mounted drone that can take off\/land while vehicle is moving https:\/\/t.co\/YhBhPyZ8sk https:\/\/t.co\/tXJ03k8mns",
    "id" : 846691184122716160,
    "created_at" : "2017-03-28 11:51:23 +0000",
    "user" : {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "protected" : false,
      "id_str" : "85053026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844192952914231296\/mLoaFZ86_normal.jpg",
      "id" : 85053026,
      "verified" : false
    }
  },
  "id" : 846861681179447297,
  "created_at" : "2017-03-28 23:08:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/Q725h3hqDu",
      "expanded_url" : "https:\/\/pandas-datareader.readthedocs.io\/en\/latest\/remote_data.html",
      "display_url" : "pandas-datareader.readthedocs.io\/en\/latest\/remo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "846859176642932736",
  "text" : "Using pandas-datareader to grab data from multiple remote datasources. https:\/\/t.co\/Q725h3hqDu",
  "id" : 846859176642932736,
  "created_at" : "2017-03-28 22:58:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andy Sheridan",
      "screen_name" : "jnrbaker",
      "indices" : [ 3, 12 ],
      "id_str" : "135657284",
      "id" : 135657284
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jnrbaker\/status\/846629390872186880\/photo\/1",
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/ipqPbLSQlF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7_U7_OWkAAlT8_.jpg",
      "id_str" : "846629382017945600",
      "id" : 846629382017945600,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7_U7_OWkAAlT8_.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 904
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 904
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 904
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ipqPbLSQlF"
    } ],
    "hashtags" : [ {
      "text" : "Dublin",
      "indices" : [ 38, 45 ]
    }, {
      "text" : "Ireland",
      "indices" : [ 46, 54 ]
    }, {
      "text" : "architecture",
      "indices" : [ 55, 68 ]
    }, {
      "text" : "longexposure",
      "indices" : [ 69, 82 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846854192891351040",
  "text" : "RT @jnrbaker: St. Patrick's Cathedral #Dublin #Ireland #architecture #longexposure https:\/\/t.co\/ipqPbLSQlF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jnrbaker\/status\/846629390872186880\/photo\/1",
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/ipqPbLSQlF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7_U7_OWkAAlT8_.jpg",
        "id_str" : "846629382017945600",
        "id" : 846629382017945600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7_U7_OWkAAlT8_.jpg",
        "sizes" : [ {
          "h" : 640,
          "resize" : "fit",
          "w" : 904
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 904
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 904
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ipqPbLSQlF"
      } ],
      "hashtags" : [ {
        "text" : "Dublin",
        "indices" : [ 24, 31 ]
      }, {
        "text" : "Ireland",
        "indices" : [ 32, 40 ]
      }, {
        "text" : "architecture",
        "indices" : [ 41, 54 ]
      }, {
        "text" : "longexposure",
        "indices" : [ 55, 68 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "846629390872186880",
    "text" : "St. Patrick's Cathedral #Dublin #Ireland #architecture #longexposure https:\/\/t.co\/ipqPbLSQlF",
    "id" : 846629390872186880,
    "created_at" : "2017-03-28 07:45:51 +0000",
    "user" : {
      "name" : "Andy Sheridan",
      "screen_name" : "jnrbaker",
      "protected" : false,
      "id_str" : "135657284",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/956170675101818880\/qDBRTkPT_normal.jpg",
      "id" : 135657284,
      "verified" : false
    }
  },
  "id" : 846854192891351040,
  "created_at" : "2017-03-28 22:39:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Conu",
      "screen_name" : "AlexConu",
      "indices" : [ 3, 12 ],
      "id_str" : "12787512",
      "id" : 12787512
    }, {
      "name" : "SAS - Scandinavian Airlines",
      "screen_name" : "SAS",
      "indices" : [ 38, 42 ],
      "id_str" : "1379801",
      "id" : 1379801
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "northernlights",
      "indices" : [ 122, 137 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846846269121269761",
  "text" : "RT @AlexConu: Last night, flying with @SAS from Bod\u00F8 to Oslo. Big thank you to the crew for turning off the cabin lights. #northernlights #\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SAS - Scandinavian Airlines",
        "screen_name" : "SAS",
        "indices" : [ 24, 28 ],
        "id_str" : "1379801",
        "id" : 1379801
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AlexConu\/status\/846751329691336704\/photo\/1",
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/G5Q16AN6wQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C8BDaKZXkAUqJxB.jpg",
        "id_str" : "846750846692069381",
        "id" : 846750846692069381,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C8BDaKZXkAUqJxB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/G5Q16AN6wQ"
      } ],
      "hashtags" : [ {
        "text" : "northernlights",
        "indices" : [ 108, 123 ]
      }, {
        "text" : "travel",
        "indices" : [ 124, 131 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "846751329691336704",
    "text" : "Last night, flying with @SAS from Bod\u00F8 to Oslo. Big thank you to the crew for turning off the cabin lights. #northernlights #travel https:\/\/t.co\/G5Q16AN6wQ",
    "id" : 846751329691336704,
    "created_at" : "2017-03-28 15:50:23 +0000",
    "user" : {
      "name" : "Alex Conu",
      "screen_name" : "AlexConu",
      "protected" : false,
      "id_str" : "12787512",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/801817862407069696\/99YxFHRP_normal.jpg",
      "id" : 12787512,
      "verified" : false
    }
  },
  "id" : 846846269121269761,
  "created_at" : "2017-03-28 22:07:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846658829408440320",
  "text" : "RT @jackiepenangkit: Watch this place. Coming soon in Manchester, UK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "846653660482469888",
    "text" : "Watch this place. Coming soon in Manchester, UK",
    "id" : 846653660482469888,
    "created_at" : "2017-03-28 09:22:17 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 846658829408440320,
  "created_at" : "2017-03-28 09:42:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/846452571162267648\/photo\/1",
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/QeBdGoKQXO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C78zvYeW0AAddb-.jpg",
      "id_str" : "846452144085585920",
      "id" : 846452144085585920,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C78zvYeW0AAddb-.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QeBdGoKQXO"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/846452571162267648\/photo\/1",
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/QeBdGoKQXO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C78zyrEXkAIqM8y.jpg",
      "id_str" : "846452200616464386",
      "id" : 846452200616464386,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C78zyrEXkAIqM8y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QeBdGoKQXO"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846452571162267648",
  "text" : "From Well Designed. If your customer take extra steps to open their wallet, you got a winner. https:\/\/t.co\/QeBdGoKQXO",
  "id" : 846452571162267648,
  "created_at" : "2017-03-27 20:03:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846443615870205952",
  "text" : "2.  So marketer can adjust the frequency emails sent or consider other communication channels such as social media.",
  "id" : 846443615870205952,
  "created_at" : "2017-03-27 19:27:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846443564754178048",
  "text" : "1. Predictive Analytics Use Case: When Do Email Recipients Unsubscribe?",
  "id" : 846443564754178048,
  "created_at" : "2017-03-27 19:27:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ire_Biz_Insurance",
      "screen_name" : "IreBizInsurance",
      "indices" : [ 82, 98 ],
      "id_str" : "24779254",
      "id" : 24779254
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/v7eoOMrtPC",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-39190135",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "846442297537548289",
  "text" : "Yellow taxis 'have lower accident rate than blue ones' https:\/\/t.co\/v7eoOMrtPC cc @IreBizInsurance",
  "id" : 846442297537548289,
  "created_at" : "2017-03-27 19:22:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "All-Ireland Summit",
      "screen_name" : "All_IreSummit",
      "indices" : [ 3, 17 ],
      "id_str" : "2910755951",
      "id" : 2910755951
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "win",
      "indices" : [ 113, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846407269239717893",
  "text" : "RT @All_IreSummit: Want to attend the All Ireland Business Summit? Retweet &amp; follow to enter for a chance to #win 1 ticket. Winner announce\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/All_IreSummit\/status\/846368811968290817\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/HUfWN07Zy9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C77n8q-VsAEVtKN.jpg",
        "id_str" : "846368809506156545",
        "id" : 846368809506156545,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C77n8q-VsAEVtKN.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 545,
          "resize" : "fit",
          "w" : 942
        }, {
          "h" : 545,
          "resize" : "fit",
          "w" : 942
        }, {
          "h" : 393,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 545,
          "resize" : "fit",
          "w" : 942
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HUfWN07Zy9"
      } ],
      "hashtags" : [ {
        "text" : "win",
        "indices" : [ 94, 98 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "846368811968290817",
    "text" : "Want to attend the All Ireland Business Summit? Retweet &amp; follow to enter for a chance to #win 1 ticket. Winner announced 3rd April https:\/\/t.co\/HUfWN07Zy9",
    "id" : 846368811968290817,
    "created_at" : "2017-03-27 14:30:24 +0000",
    "user" : {
      "name" : "All-Ireland Summit",
      "screen_name" : "All_IreSummit",
      "protected" : false,
      "id_str" : "2910755951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017753195093905409\/pB7lTeOs_normal.jpg",
      "id" : 2910755951,
      "verified" : false
    }
  },
  "id" : 846407269239717893,
  "created_at" : "2017-03-27 17:03:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846405217319096320",
  "text" : "\"helping companies re-imagine the intersection of culture, talent, and brand.\" Huh?",
  "id" : 846405217319096320,
  "created_at" : "2017-03-27 16:55:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/qVrwDiqtMY",
      "expanded_url" : "http:\/\/www.theage.com.au\/nsw\/dont-teach-your-kids-coding-teach-them-how-to-live-online-20170323-gv5e9r.html",
      "display_url" : "theage.com.au\/nsw\/dont-teach\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "846352953158045696",
  "text" : "Don't teach your kids coding, teach them how to live online https:\/\/t.co\/qVrwDiqtMY",
  "id" : 846352953158045696,
  "created_at" : "2017-03-27 13:27:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Quest",
      "screen_name" : "richardquest",
      "indices" : [ 3, 16 ],
      "id_str" : "16681542",
      "id" : 16681542
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846113496240672770",
  "text" : "RT @richardquest: Enjoying durian in KL. don't know what all the fuss is about. Fresh durian Smells lovely and fruity. Taste is rich and cr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/richardquest\/status\/845903595736223746\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/AK68OtHTuk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C71AzwiU8AAkC8-.jpg",
        "id_str" : "845903562961907712",
        "id" : 845903562961907712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C71AzwiU8AAkC8-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AK68OtHTuk"
      } ],
      "hashtags" : [ {
        "text" : "notthatbad",
        "indices" : [ 126, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "845903595736223746",
    "text" : "Enjoying durian in KL. don't know what all the fuss is about. Fresh durian Smells lovely and fruity. Taste is rich and creamy #notthatbad https:\/\/t.co\/AK68OtHTuk",
    "id" : 845903595736223746,
    "created_at" : "2017-03-26 07:41:48 +0000",
    "user" : {
      "name" : "Richard Quest",
      "screen_name" : "richardquest",
      "protected" : false,
      "id_str" : "16681542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000601015536\/4c299dd05d3c6fd7208e18dbd7d824c4_normal.jpeg",
      "id" : 16681542,
      "verified" : true
    }
  },
  "id" : 846113496240672770,
  "created_at" : "2017-03-26 21:35:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/YdQNrC5fKQ",
      "expanded_url" : "http:\/\/on.natgeo.com\/2nejNPS",
      "display_url" : "on.natgeo.com\/2nejNPS"
    } ]
  },
  "geo" : { },
  "id_str" : "846081941216641029",
  "text" : "The Underappreciated Man Behind the \u201CBest Graphic Ever Produced\u201D https:\/\/t.co\/YdQNrC5fKQ",
  "id" : 846081941216641029,
  "created_at" : "2017-03-26 19:30:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "15908631",
      "id" : 15908631
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/gavinsblog\/status\/845693767315324928\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/UI0xYEMZjY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7yB9NiW4AABXkp.jpg",
      "id_str" : "845693718644580352",
      "id" : 845693718644580352,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7yB9NiW4AABXkp.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UI0xYEMZjY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846067414446067712",
  "text" : "RT @gavinsblog: Ireland's first Tesla supercharger station finally under construction in Laois. $TSLA https:\/\/t.co\/UI0xYEMZjY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gavinsblog\/status\/845693767315324928\/photo\/1",
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/UI0xYEMZjY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7yB9NiW4AABXkp.jpg",
        "id_str" : "845693718644580352",
        "id" : 845693718644580352,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7yB9NiW4AABXkp.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UI0xYEMZjY"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "845693767315324928",
    "text" : "Ireland's first Tesla supercharger station finally under construction in Laois. $TSLA https:\/\/t.co\/UI0xYEMZjY",
    "id" : 845693767315324928,
    "created_at" : "2017-03-25 17:48:01 +0000",
    "user" : {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "protected" : false,
      "id_str" : "15908631",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894331872716754945\/0KfaOf4I_normal.jpg",
      "id" : 15908631,
      "verified" : true
    }
  },
  "id" : 846067414446067712,
  "created_at" : "2017-03-26 18:32:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "846064393771200512",
  "text" : "\"...is a high end analytics provider \" \uD83D\uDE32",
  "id" : 846064393771200512,
  "created_at" : "2017-03-26 18:20:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "indices" : [ 95, 109 ],
      "id_str" : "150246405",
      "id" : 150246405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/v44k1OXuYt",
      "expanded_url" : "http:\/\/www.thejournal.ie\/pictures-glendalough-3306478-Mar2017\/",
      "display_url" : "thejournal.ie\/pictures-glend\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "846016805739139073",
  "text" : "RT @GeoffreyIRL: One man has spent five years capturing breathtaking views of Glendalough (via @thejournal_ie) https:\/\/t.co\/v44k1OXuYt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TheJournal.ie",
        "screen_name" : "thejournal_ie",
        "indices" : [ 78, 92 ],
        "id_str" : "150246405",
        "id" : 150246405
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/v44k1OXuYt",
        "expanded_url" : "http:\/\/www.thejournal.ie\/pictures-glendalough-3306478-Mar2017\/",
        "display_url" : "thejournal.ie\/pictures-glend\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "846015952349151232",
    "text" : "One man has spent five years capturing breathtaking views of Glendalough (via @thejournal_ie) https:\/\/t.co\/v44k1OXuYt",
    "id" : 846015952349151232,
    "created_at" : "2017-03-26 15:08:15 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 846016805739139073,
  "created_at" : "2017-03-26 15:11:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Think with Google",
      "screen_name" : "ThinkwithGoogle",
      "indices" : [ 3, 19 ],
      "id_str" : "260741931",
      "id" : 260741931
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/jGR4ItQ6M1",
      "expanded_url" : "https:\/\/goo.gl\/EuyxVC",
      "display_url" : "goo.gl\/EuyxVC"
    } ]
  },
  "geo" : { },
  "id_str" : "845990065532813312",
  "text" : "RT @ThinkwithGoogle: Testing can yield valuable learnings. But to test, you have to be willing to fail. https:\/\/t.co\/jGR4ItQ6M1 https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ThinkwithGoogle\/status\/842356025751683072\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/p7flL8aO2H",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7CmVt3XAAAIxDb.png",
        "id_str" : "842356022337536000",
        "id" : 842356022337536000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7CmVt3XAAAIxDb.png",
        "sizes" : [ {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/p7flL8aO2H"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/jGR4ItQ6M1",
        "expanded_url" : "https:\/\/goo.gl\/EuyxVC",
        "display_url" : "goo.gl\/EuyxVC"
      } ]
    },
    "geo" : { },
    "id_str" : "842356025751683072",
    "text" : "Testing can yield valuable learnings. But to test, you have to be willing to fail. https:\/\/t.co\/jGR4ItQ6M1 https:\/\/t.co\/p7flL8aO2H",
    "id" : 842356025751683072,
    "created_at" : "2017-03-16 12:45:01 +0000",
    "user" : {
      "name" : "Think with Google",
      "screen_name" : "ThinkwithGoogle",
      "protected" : false,
      "id_str" : "260741931",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039214337544450048\/vLHTs7Iy_normal.jpg",
      "id" : 260741931,
      "verified" : true
    }
  },
  "id" : 845990065532813312,
  "created_at" : "2017-03-26 13:25:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845938488705007616",
  "text" : "3.It all depends under what circumstances the deceased pass away. If deceased live thru a ripe old age &amp; family expect death is imminent.",
  "id" : 845938488705007616,
  "created_at" : "2017-03-26 10:00:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845938006838120448",
  "text" : "2. The deceased's children wear blue, grandchildren wear pink, great-grandchildren wear red. Funeral attire are not darker colored it seem.",
  "id" : 845938006838120448,
  "created_at" : "2017-03-26 09:58:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845936797771288576",
  "text" : "1. Learn a thing or two about Chinese tradition when it comes to funeral.",
  "id" : 845936797771288576,
  "created_at" : "2017-03-26 09:53:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Doug Hall",
      "screen_name" : "fastbloke",
      "indices" : [ 3, 13 ],
      "id_str" : "15143882",
      "id" : 15143882
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "measure",
      "indices" : [ 111, 119 ]
    } ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/ZwmWXxuWTJ",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/uk-39351833",
      "display_url" : "bbc.co.uk\/news\/uk-393518\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "845781044997558273",
  "text" : "RT @fastbloke: Nice work - https:\/\/t.co\/ZwmWXxuWTJ\n\nDigging out outliers finds value in improved data quality\n\n#measure",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "measure",
        "indices" : [ 96, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 12, 35 ],
        "url" : "https:\/\/t.co\/ZwmWXxuWTJ",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/uk-39351833",
        "display_url" : "bbc.co.uk\/news\/uk-393518\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844669187809267713",
    "text" : "Nice work - https:\/\/t.co\/ZwmWXxuWTJ\n\nDigging out outliers finds value in improved data quality\n\n#measure",
    "id" : 844669187809267713,
    "created_at" : "2017-03-22 21:56:42 +0000",
    "user" : {
      "name" : "Doug Hall",
      "screen_name" : "fastbloke",
      "protected" : false,
      "id_str" : "15143882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/867294825531441152\/awJCU6hn_normal.jpg",
      "id" : 15143882,
      "verified" : false
    }
  },
  "id" : 845781044997558273,
  "created_at" : "2017-03-25 23:34:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darran Anderson",
      "screen_name" : "Oniropolis",
      "indices" : [ 3, 14 ],
      "id_str" : "2314464974",
      "id" : 2314464974
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/iQ4RfB1aUi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8DXGW4AADmgZ.jpg",
      "id_str" : "844490958569922560",
      "id" : 844490958569922560,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8DXGW4AADmgZ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/iQ4RfB1aUi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8EPbW0AAtPXg.jpg",
      "id_str" : "844490973690384384",
      "id" : 844490973690384384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8EPbW0AAtPXg.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/iQ4RfB1aUi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8ES6X0AAVYjZ.jpg",
      "id_str" : "844490974625779712",
      "id" : 844490974625779712,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8ES6X0AAVYjZ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/iQ4RfB1aUi",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8E2sW0AAmMRb.jpg",
      "id_str" : "844490984230670336",
      "id" : 844490984230670336,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8E2sW0AAmMRb.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/RPN1cXwD9d",
      "expanded_url" : "http:\/\/aboutkazakhstan.com\/blog\/photos\/the-ship-graveyard-of-the-aral-sea\/",
      "display_url" : "aboutkazakhstan.com\/blog\/photos\/th\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "845778421053640705",
  "text" : "RT @Oniropolis: The ship graveyard of the Aral Sea https:\/\/t.co\/RPN1cXwD9d https:\/\/t.co\/iQ4RfB1aUi",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/iQ4RfB1aUi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8DXGW4AADmgZ.jpg",
        "id_str" : "844490958569922560",
        "id" : 844490958569922560,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8DXGW4AADmgZ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/iQ4RfB1aUi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8EPbW0AAtPXg.jpg",
        "id_str" : "844490973690384384",
        "id" : 844490973690384384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8EPbW0AAtPXg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/iQ4RfB1aUi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8ES6X0AAVYjZ.jpg",
        "id_str" : "844490974625779712",
        "id" : 844490974625779712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8ES6X0AAVYjZ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Oniropolis\/status\/844491054363611137\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/iQ4RfB1aUi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g8E2sW0AAmMRb.jpg",
        "id_str" : "844490984230670336",
        "id" : 844490984230670336,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g8E2sW0AAmMRb.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iQ4RfB1aUi"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/RPN1cXwD9d",
        "expanded_url" : "http:\/\/aboutkazakhstan.com\/blog\/photos\/the-ship-graveyard-of-the-aral-sea\/",
        "display_url" : "aboutkazakhstan.com\/blog\/photos\/th\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844491054363611137",
    "text" : "The ship graveyard of the Aral Sea https:\/\/t.co\/RPN1cXwD9d https:\/\/t.co\/iQ4RfB1aUi",
    "id" : 844491054363611137,
    "created_at" : "2017-03-22 10:08:51 +0000",
    "user" : {
      "name" : "Darran Anderson",
      "screen_name" : "Oniropolis",
      "protected" : false,
      "id_str" : "2314464974",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/906896783879569409\/SQdJQq9d_normal.jpg",
      "id" : 2314464974,
      "verified" : true
    }
  },
  "id" : 845778421053640705,
  "created_at" : "2017-03-25 23:24:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845325196240322560",
  "text" : "The slow, grinding wheel of bureaucracy. 3 months to get approval. How is a firm going to start the project on 3rd April?",
  "id" : 845325196240322560,
  "created_at" : "2017-03-24 17:23:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "indices" : [ 3, 16 ],
      "id_str" : "20192882",
      "id" : 20192882
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845323451594346497",
  "text" : "RT @mikewhitmore: How many browser tabs do you have open right now?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844952082767499264",
    "text" : "How many browser tabs do you have open right now?",
    "id" : 844952082767499264,
    "created_at" : "2017-03-23 16:40:49 +0000",
    "user" : {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "protected" : false,
      "id_str" : "20192882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/814556566179352577\/J3-2QBfu_normal.jpg",
      "id" : 20192882,
      "verified" : false
    }
  },
  "id" : 845323451594346497,
  "created_at" : "2017-03-24 17:16:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "indices" : [ 3, 19 ],
      "id_str" : "2735246778",
      "id" : 2735246778
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/844602665971732481\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/bkzW8jcTZF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7ihgmDXUAA_4C_.jpg",
      "id_str" : "844602511474511872",
      "id" : 844602511474511872,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7ihgmDXUAA_4C_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 444,
        "resize" : "fit",
        "w" : 1213
      }, {
        "h" : 249,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 439,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 444,
        "resize" : "fit",
        "w" : 1213
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bkzW8jcTZF"
    } ],
    "hashtags" : [ {
      "text" : "stacksurvey17",
      "indices" : [ 88, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845277723174096896",
  "text" : "RT @ThePracticalDev: A majority of developers feel underpaid. Why do you think this is? #stacksurvey17 https:\/\/t.co\/bkzW8jcTZF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ThePracticalDev\/status\/844602665971732481\/photo\/1",
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/bkzW8jcTZF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7ihgmDXUAA_4C_.jpg",
        "id_str" : "844602511474511872",
        "id" : 844602511474511872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7ihgmDXUAA_4C_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 444,
          "resize" : "fit",
          "w" : 1213
        }, {
          "h" : 249,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 439,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 444,
          "resize" : "fit",
          "w" : 1213
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bkzW8jcTZF"
      } ],
      "hashtags" : [ {
        "text" : "stacksurvey17",
        "indices" : [ 67, 81 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844602665971732481",
    "text" : "A majority of developers feel underpaid. Why do you think this is? #stacksurvey17 https:\/\/t.co\/bkzW8jcTZF",
    "id" : 844602665971732481,
    "created_at" : "2017-03-22 17:32:22 +0000",
    "user" : {
      "name" : "DEV Community \uD83D\uDC69\u200D\uD83D\uDCBB\uD83D\uDC68\u200D\uD83D\uDCBB",
      "screen_name" : "ThePracticalDev",
      "protected" : false,
      "id_str" : "2735246778",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002604104194056192\/IEoNsLNM_normal.jpg",
      "id" : 2735246778,
      "verified" : false
    }
  },
  "id" : 845277723174096896,
  "created_at" : "2017-03-24 14:14:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/tiMa4pwM1u",
      "expanded_url" : "https:\/\/shar.es\/1Q1ew5",
      "display_url" : "shar.es\/1Q1ew5"
    } ]
  },
  "geo" : { },
  "id_str" : "845273745589702656",
  "text" : "RT @yapphenghui: Lee Kuan Yew happy as S\u2019poreans at work &amp; not paying tribute to him on March 23, 2017 https:\/\/t.co\/tiMa4pwM1u",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/tiMa4pwM1u",
        "expanded_url" : "https:\/\/shar.es\/1Q1ew5",
        "display_url" : "shar.es\/1Q1ew5"
      } ]
    },
    "geo" : { },
    "id_str" : "845117638942507008",
    "text" : "Lee Kuan Yew happy as S\u2019poreans at work &amp; not paying tribute to him on March 23, 2017 https:\/\/t.co\/tiMa4pwM1u",
    "id" : 845117638942507008,
    "created_at" : "2017-03-24 03:38:41 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 845273745589702656,
  "created_at" : "2017-03-24 13:59:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845270101725237248",
  "text" : "\"Digital Data Scientist\" You can't be using abacus",
  "id" : 845270101725237248,
  "created_at" : "2017-03-24 13:44:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "845192818922422272",
  "text" : "I am all for paid twitter.",
  "id" : 845192818922422272,
  "created_at" : "2017-03-24 08:37:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844964660323454976",
  "text" : "RT @lisaocarroll: Bill Clinton: \"After all the breath he expended cursing the British over the years, he worked with two PMs and shook hand\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/Jv5rJiYZT0",
        "expanded_url" : "https:\/\/twitter.com\/rtenews\/status\/844941670344867841",
        "display_url" : "twitter.com\/rtenews\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844963424618627072",
    "text" : "Bill Clinton: \"After all the breath he expended cursing the British over the years, he worked with two PMs and shook hands with the Queen\" https:\/\/t.co\/Jv5rJiYZT0",
    "id" : 844963424618627072,
    "created_at" : "2017-03-23 17:25:53 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 844964660323454976,
  "created_at" : "2017-03-23 17:30:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/o1osacjeuZ",
      "expanded_url" : "http:\/\/str.sg\/47fU",
      "display_url" : "str.sg\/47fU"
    } ]
  },
  "geo" : { },
  "id_str" : "844922212364750849",
  "text" : "Many in the growing pool of PMETs are taking longer to find permanent jobs https:\/\/t.co\/o1osacjeuZ",
  "id" : 844922212364750849,
  "created_at" : "2017-03-23 14:42:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shahrizan Syawal",
      "screen_name" : "ShahrizanSyawal",
      "indices" : [ 3, 19 ],
      "id_str" : "101072792",
      "id" : 101072792
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ShahrizanSyawal\/status\/844830717104144384\/photo\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/8Imykz6Jav",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7lxCh_U8AER_dJ.jpg",
      "id_str" : "844830693406339073",
      "id" : 844830693406339073,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7lxCh_U8AER_dJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8Imykz6Jav"
    } ],
    "hashtags" : [ {
      "text" : "GTF2017",
      "indices" : [ 66, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844849896855236608",
  "text" : "RT @ShahrizanSyawal: Jack Ma \"people who complain seldom succeed\" #GTF2017 https:\/\/t.co\/8Imykz6Jav",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ShahrizanSyawal\/status\/844830717104144384\/photo\/1",
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/8Imykz6Jav",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7lxCh_U8AER_dJ.jpg",
        "id_str" : "844830693406339073",
        "id" : 844830693406339073,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7lxCh_U8AER_dJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8Imykz6Jav"
      } ],
      "hashtags" : [ {
        "text" : "GTF2017",
        "indices" : [ 45, 53 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844830717104144384",
    "text" : "Jack Ma \"people who complain seldom succeed\" #GTF2017 https:\/\/t.co\/8Imykz6Jav",
    "id" : 844830717104144384,
    "created_at" : "2017-03-23 08:38:33 +0000",
    "user" : {
      "name" : "Shahrizan Syawal",
      "screen_name" : "ShahrizanSyawal",
      "protected" : false,
      "id_str" : "101072792",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/670035905298599936\/x-vvYS4Q_normal.jpg",
      "id" : 101072792,
      "verified" : false
    }
  },
  "id" : 844849896855236608,
  "created_at" : "2017-03-23 09:54:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Bowen",
      "screen_name" : "BowenBBC",
      "indices" : [ 3, 12 ],
      "id_str" : "42501345",
      "id" : 42501345
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844849779779649537",
  "text" : "RT @BowenBBC: We all need social media. Front line Iraqi troops charge phones on the unit's generator close to jihadist positions https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BowenBBC\/status\/844847042300076032\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/95YUqZgtjD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7l_6AqVMAAmCZr.jpg",
        "id_str" : "844847039695368192",
        "id" : 844847039695368192,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7l_6AqVMAAmCZr.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/95YUqZgtjD"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844847042300076032",
    "text" : "We all need social media. Front line Iraqi troops charge phones on the unit's generator close to jihadist positions https:\/\/t.co\/95YUqZgtjD",
    "id" : 844847042300076032,
    "created_at" : "2017-03-23 09:43:26 +0000",
    "user" : {
      "name" : "Jeremy Bowen",
      "screen_name" : "BowenBBC",
      "protected" : false,
      "id_str" : "42501345",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531084150824894464\/hhVck5XX_normal.jpeg",
      "id" : 42501345,
      "verified" : true
    }
  },
  "id" : 844849779779649537,
  "created_at" : "2017-03-23 09:54:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "indices" : [ 3, 16 ],
      "id_str" : "20192882",
      "id" : 20192882
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844685420441022464",
  "text" : "RT @mikewhitmore: \"Money frees you from doing things you dislike. Since I dislike doing nearly everything, money is handy.\"\n~ Groucho Marx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844598105932623872",
    "text" : "\"Money frees you from doing things you dislike. Since I dislike doing nearly everything, money is handy.\"\n~ Groucho Marx",
    "id" : 844598105932623872,
    "created_at" : "2017-03-22 17:14:14 +0000",
    "user" : {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "protected" : false,
      "id_str" : "20192882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/814556566179352577\/J3-2QBfu_normal.jpg",
      "id" : 20192882,
      "verified" : false
    }
  },
  "id" : 844685420441022464,
  "created_at" : "2017-03-22 23:01:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "City A.M.",
      "screen_name" : "CityAM",
      "indices" : [ 91, 98 ],
      "id_str" : "45840503",
      "id" : 45840503
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/b202vSkiMX",
      "expanded_url" : "http:\/\/www.cityam.com\/261200\/singapore-london-cities-best-infrastructure-world",
      "display_url" : "cityam.com\/261200\/singapo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844683252820574208",
  "text" : "These are the cities with the best infrastructure in the world https:\/\/t.co\/b202vSkiMX via @CityAM",
  "id" : 844683252820574208,
  "created_at" : "2017-03-22 22:52:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "waterfordhour",
      "indices" : [ 115, 129 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/3S3XKGCgBj",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/make-every-customer-interaction-more-profitable-shiao-shyan-yap-%E5%8F%B6%E6%99%93%E8%BB%92",
      "display_url" : "linkedin.com\/pulse\/make-eve\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844670813089533953",
  "text" : "From social media, CRM, web analytics and PPC ads, I help business better inform with data https:\/\/t.co\/3S3XKGCgBj #waterfordhour",
  "id" : 844670813089533953,
  "created_at" : "2017-03-22 22:03:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WaterfordHour",
      "indices" : [ 89, 103 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844666640583311360",
  "text" : "Good evening all. Anyone use Google Analytics to track and measure your online presence? #WaterfordHour",
  "id" : 844666640583311360,
  "created_at" : "2017-03-22 21:46:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/G3q4S8JpMb",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/uk-scotland-scotland-politics-39354555",
      "display_url" : "bbc.com\/news\/uk-scotla\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844596346019610624",
  "text" : "Holyrood referendum debate halted after Westminster shooting https:\/\/t.co\/G3q4S8JpMb",
  "id" : 844596346019610624,
  "created_at" : "2017-03-22 17:07:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jacquelyn Cheok",
      "screen_name" : "JacCheokBT",
      "indices" : [ 3, 14 ],
      "id_str" : "2436730231",
      "id" : 2436730231
    }, {
      "name" : "Janil Puthucheary",
      "screen_name" : "jputhucheary",
      "indices" : [ 109, 122 ],
      "id_str" : "2811697760",
      "id" : 2811697760
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844592030030401536",
  "text" : "RT @JacCheokBT: We've disrupted ourselves in govt; govt's role is 2 enable value creation in private sector: @jputhucheary, Min IC, GovTech\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Janil Puthucheary",
        "screen_name" : "jputhucheary",
        "indices" : [ 93, 106 ],
        "id_str" : "2811697760",
        "id" : 2811697760
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JacCheokBT\/status\/844364726708559872\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/pwVSsb0Lds",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7fJOavVYAEt4FL.jpg",
        "id_str" : "844364704688463873",
        "id" : 844364704688463873,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7fJOavVYAEt4FL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/pwVSsb0Lds"
      } ],
      "hashtags" : [ {
        "text" : "BTLeadersForum",
        "indices" : [ 124, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844364726708559872",
    "text" : "We've disrupted ourselves in govt; govt's role is 2 enable value creation in private sector: @jputhucheary, Min IC, GovTech #BTLeadersForum https:\/\/t.co\/pwVSsb0Lds",
    "id" : 844364726708559872,
    "created_at" : "2017-03-22 01:46:53 +0000",
    "user" : {
      "name" : "Jacquelyn Cheok",
      "screen_name" : "JacCheokBT",
      "protected" : false,
      "id_str" : "2436730231",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/456096218246959104\/H191T5gA_normal.jpeg",
      "id" : 2436730231,
      "verified" : false
    }
  },
  "id" : 844592030030401536,
  "created_at" : "2017-03-22 16:50:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TechChatSG",
      "indices" : [ 106, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844520184790568962",
  "text" : "RT @GovTechSG: Any questions about what we do at GovTech? Share with us and we\u2019ll look to address them at #TechChatSG or post-event. https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/locowise.com\" rel=\"nofollow\"\u003ELocowise Analytics\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GovTechSG\/status\/844511483446804480\/photo\/1",
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/NJJlIyR49g",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7hOt6MVQAEOUXV.jpg",
        "id_str" : "844511480754028545",
        "id" : 844511480754028545,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7hOt6MVQAEOUXV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 994
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 994
        }, {
          "h" : 226,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 331,
          "resize" : "fit",
          "w" : 994
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NJJlIyR49g"
      } ],
      "hashtags" : [ {
        "text" : "TechChatSG",
        "indices" : [ 91, 102 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844511483446804480",
    "text" : "Any questions about what we do at GovTech? Share with us and we\u2019ll look to address them at #TechChatSG or post-event. https:\/\/t.co\/NJJlIyR49g",
    "id" : 844511483446804480,
    "created_at" : "2017-03-22 11:30:02 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 844520184790568962,
  "created_at" : "2017-03-22 12:04:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/OuXEgHzLki",
      "expanded_url" : "https:\/\/twitter.com\/heliumlife\/status\/843113974472167424",
      "display_url" : "twitter.com\/heliumlife\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844518387443228672",
  "text" : "So this is where my clan are buried back home. I have no idea until now. https:\/\/t.co\/OuXEgHzLki",
  "id" : 844518387443228672,
  "created_at" : "2017-03-22 11:57:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/KtfvgBRCS3",
      "expanded_url" : "https:\/\/github.com\/Quantipy\/quantipy",
      "display_url" : "github.com\/Quantipy\/quant\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844517617197043712",
  "text" : "Automatic generation of PowerPoint decks from survey data with this Python package. https:\/\/t.co\/KtfvgBRCS3",
  "id" : 844517617197043712,
  "created_at" : "2017-03-22 11:54:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 3, 15 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844516388681252866",
  "text" : "RT @gianniponzi: Verified by Visa you absolutely piece of  time wasting, annoying, frustrating  s****",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "844511187970768897",
    "text" : "Verified by Visa you absolutely piece of  time wasting, annoying, frustrating  s****",
    "id" : 844511187970768897,
    "created_at" : "2017-03-22 11:28:52 +0000",
    "user" : {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "protected" : false,
      "id_str" : "19793936",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034160433949761536\/GdlrLPNt_normal.jpg",
      "id" : 19793936,
      "verified" : false
    }
  },
  "id" : 844516388681252866,
  "created_at" : "2017-03-22 11:49:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/JArilw4fXj",
      "expanded_url" : "http:\/\/www.computerhope.com\/issues\/ch001290.htm",
      "display_url" : "computerhope.com\/issues\/ch00129\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844506737302556672",
  "text" : "Change capital letters to lowercase in MS Word https:\/\/t.co\/JArilw4fXj",
  "id" : 844506737302556672,
  "created_at" : "2017-03-22 11:11:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 118, 130 ]
    } ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/N1qsb3lQlh",
      "expanded_url" : "http:\/\/bit.ly\/TrackGAChange",
      "display_url" : "bit.ly\/TrackGAChange"
    } ]
  },
  "geo" : { },
  "id_str" : "844506582587228160",
  "text" : "How do you keep track of implementation and configuration changes in your web analytics tool? https:\/\/t.co\/N1qsb3lQlh #getoptimise",
  "id" : 844506582587228160,
  "created_at" : "2017-03-22 11:10:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/844493878552072192\/photo\/1",
      "indices" : [ 124, 147 ],
      "url" : "https:\/\/t.co\/ffGtcZIUYU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7g-pTmWkAAj1r9.jpg",
      "id_str" : "844493809488662528",
      "id" : 844493809488662528,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7g-pTmWkAAj1r9.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 298,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 169,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1341
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1341
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ffGtcZIUYU"
    } ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 111, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844493878552072192",
  "text" : "Do you need to pull Organic search data to get around Google\u2019s current 90 day limit on historical data? DM me. #getoptimise https:\/\/t.co\/ffGtcZIUYU",
  "id" : 844493878552072192,
  "created_at" : "2017-03-22 10:20:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844489989052399617",
  "text" : "Without resort to googling, does the word \"machine learning\" make any sense to you? (Practitioners pls refrain from voting on this) Thanks",
  "id" : 844489989052399617,
  "created_at" : "2017-03-22 10:04:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844237270215462912",
  "text" : "Does the use of Data Lake eliminates the \ndata-preparation process of Extract\/Transform\/Load (ETL)?",
  "id" : 844237270215462912,
  "created_at" : "2017-03-21 17:20:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Collings",
      "screen_name" : "collingsdg",
      "indices" : [ 3, 14 ],
      "id_str" : "767241331",
      "id" : 767241331
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844216582020587520",
  "text" : "RT @collingsdg: Working at home. Reading academic paper with highlighter in hand. My three year old says \"come play you can finish your col\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842662127060111364",
    "text" : "Working at home. Reading academic paper with highlighter in hand. My three year old says \"come play you can finish your colouring later\" \uD83D\uDE02",
    "id" : 842662127060111364,
    "created_at" : "2017-03-17 09:01:21 +0000",
    "user" : {
      "name" : "David Collings",
      "screen_name" : "collingsdg",
      "protected" : false,
      "id_str" : "767241331",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/715426796095799296\/leAizAi5_normal.jpg",
      "id" : 767241331,
      "verified" : false
    }
  },
  "id" : 844216582020587520,
  "created_at" : "2017-03-21 15:58:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844207959706165249",
  "text" : "Unfollow and follow someone on Twitter is just like renewing your marriage vows...",
  "id" : 844207959706165249,
  "created_at" : "2017-03-21 15:23:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/diU13ZaI3V",
      "expanded_url" : "http:\/\/webapps.stackexchange.com\/questions\/90704\/how-can-i-unfollow-everyone-at-once-on-twitter",
      "display_url" : "webapps.stackexchange.com\/questions\/9070\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844207637382266880",
  "text" : "Just do a mass unfollow using this developer trick https:\/\/t.co\/diU13ZaI3V",
  "id" : 844207637382266880,
  "created_at" : "2017-03-21 15:22:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/eBjJ2aPOls",
      "expanded_url" : "http:\/\/distill.pub\/about\/",
      "display_url" : "distill.pub\/about\/"
    } ]
  },
  "geo" : { },
  "id_str" : "844186470877450240",
  "text" : "Dedicated to clear explanations of machine learning https:\/\/t.co\/eBjJ2aPOls",
  "id" : 844186470877450240,
  "created_at" : "2017-03-21 13:58:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul S-H",
      "screen_name" : "simbeckhampson",
      "indices" : [ 3, 18 ],
      "id_str" : "39944598",
      "id" : 39944598
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 105, 108 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844176723633692672",
  "text" : "RT @simbeckhampson: \u201CThe business plans of the next 10,000 startups are easy to forecast: Take X and add #AI.\u201D - Kevin Kelly, Wired Magazin\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/simbeckhampson\/status\/832886078247936000\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/eI6FaD0Sha",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C48BdTvWcAAHRf1.jpg",
        "id_str" : "832886059113541632",
        "id" : 832886059113541632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C48BdTvWcAAHRf1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 635,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 635,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 635,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eI6FaD0Sha"
      } ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 85, 88 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "832886078247936000",
    "text" : "\u201CThe business plans of the next 10,000 startups are easy to forecast: Take X and add #AI.\u201D - Kevin Kelly, Wired Magazine https:\/\/t.co\/eI6FaD0Sha",
    "id" : 832886078247936000,
    "created_at" : "2017-02-18 09:34:49 +0000",
    "user" : {
      "name" : "Paul S-H",
      "screen_name" : "simbeckhampson",
      "protected" : false,
      "id_str" : "39944598",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959725407619682304\/67lXRkbl_normal.jpg",
      "id" : 39944598,
      "verified" : false
    }
  },
  "id" : 844176723633692672,
  "created_at" : "2017-03-21 13:19:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    }, {
      "name" : "Buffer",
      "screen_name" : "buffer",
      "indices" : [ 77, 84 ],
      "id_str" : "197962366",
      "id" : 197962366
    }, {
      "name" : "IFTTT",
      "screen_name" : "IFTTT",
      "indices" : [ 89, 95 ],
      "id_str" : "75079616",
      "id" : 75079616
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/5owZsfzaWK",
      "expanded_url" : "http:\/\/getoptimise.com\/blog\/business\/resources-for-growing-company\/",
      "display_url" : "getoptimise.com\/blog\/business\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844171584268992512",
  "text" : "RT @Dotnetster: Nice favourites.io mention on this blog, beside the likes of @buffer and @IFTTT https:\/\/t.co\/5owZsfzaWK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Buffer",
        "screen_name" : "buffer",
        "indices" : [ 61, 68 ],
        "id_str" : "197962366",
        "id" : 197962366
      }, {
        "name" : "IFTTT",
        "screen_name" : "IFTTT",
        "indices" : [ 73, 79 ],
        "id_str" : "75079616",
        "id" : 75079616
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/5owZsfzaWK",
        "expanded_url" : "http:\/\/getoptimise.com\/blog\/business\/resources-for-growing-company\/",
        "display_url" : "getoptimise.com\/blog\/business\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844169660316696581",
    "text" : "Nice favourites.io mention on this blog, beside the likes of @buffer and @IFTTT https:\/\/t.co\/5owZsfzaWK",
    "id" : 844169660316696581,
    "created_at" : "2017-03-21 12:51:45 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 844171584268992512,
  "created_at" : "2017-03-21 12:59:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844165245560111104",
  "text" : "What the use case of an Open Data Portal for non-public sector?",
  "id" : 844165245560111104,
  "created_at" : "2017-03-21 12:34:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/gJksaStKGa",
      "expanded_url" : "http:\/\/str.sg\/478k",
      "display_url" : "str.sg\/478k"
    } ]
  },
  "geo" : { },
  "id_str" : "844164226772033536",
  "text" : "Singapore overtakes tech mecca Silicon Valley as No. 1 for global start-up talent https:\/\/t.co\/gJksaStKGa",
  "id" : 844164226772033536,
  "created_at" : "2017-03-21 12:30:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Project Jupyter",
      "screen_name" : "ProjectJupyter",
      "indices" : [ 91, 106 ],
      "id_str" : "2460368252",
      "id" : 2460368252
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Irleand",
      "indices" : [ 14, 22 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "844119624643960832",
  "text" : "Journalist in #Irleand, if you want to learn how to show your data sources and analysis as @ProjectJupyter notebooks, please contact me.",
  "id" : 844119624643960832,
  "created_at" : "2017-03-21 09:32:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    }, {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 76, 82 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 42, 56 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 60, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/kQVZ09EJT6",
      "expanded_url" : "http:\/\/www.straitstimes.com\/multimedia\/photos\/in-pictures-st-patricks-day-celebrations-in-singapore",
      "display_url" : "straitstimes.com\/multimedia\/pho\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844117753401397248",
  "text" : "RT @IrlEmbSingapore: Some great photos of #StPatricksDay in #Singapore from @STcom https:\/\/t.co\/kQVZ09EJT6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Straits Times",
        "screen_name" : "STcom",
        "indices" : [ 55, 61 ],
        "id_str" : "37874853",
        "id" : 37874853
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 21, 35 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 39, 49 ]
      } ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/kQVZ09EJT6",
        "expanded_url" : "http:\/\/www.straitstimes.com\/multimedia\/photos\/in-pictures-st-patricks-day-celebrations-in-singapore",
        "display_url" : "straitstimes.com\/multimedia\/pho\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844021562059710464",
    "text" : "Some great photos of #StPatricksDay in #Singapore from @STcom https:\/\/t.co\/kQVZ09EJT6",
    "id" : 844021562059710464,
    "created_at" : "2017-03-21 03:03:16 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 844117753401397248,
  "created_at" : "2017-03-21 09:25:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Caines",
      "screen_name" : "klaptrap",
      "indices" : [ 3, 12 ],
      "id_str" : "29835266",
      "id" : 29835266
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/lrMVVt83mN",
      "expanded_url" : "https:\/\/govinsider.asia\/digital-gov\/singapore-appoints-puthucheary-as-first-govtech-minister\/",
      "display_url" : "govinsider.asia\/digital-gov\/si\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844117541618405376",
  "text" : "RT @klaptrap: Singapore appoints Puthucheary as GovTech Minister, Agency moves to Prime Minister's Office https:\/\/t.co\/lrMVVt83mN via @GovI\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GovInsider",
        "screen_name" : "GovInsider",
        "indices" : [ 120, 131 ],
        "id_str" : "3220267980",
        "id" : 3220267980
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 115 ],
        "url" : "https:\/\/t.co\/lrMVVt83mN",
        "expanded_url" : "https:\/\/govinsider.asia\/digital-gov\/singapore-appoints-puthucheary-as-first-govtech-minister\/",
        "display_url" : "govinsider.asia\/digital-gov\/si\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844018943945715712",
    "text" : "Singapore appoints Puthucheary as GovTech Minister, Agency moves to Prime Minister's Office https:\/\/t.co\/lrMVVt83mN via @GovInsider",
    "id" : 844018943945715712,
    "created_at" : "2017-03-21 02:52:52 +0000",
    "user" : {
      "name" : "Jonathan Caines",
      "screen_name" : "klaptrap",
      "protected" : false,
      "id_str" : "29835266",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/695152104990609408\/SOgVM7Hj_normal.png",
      "id" : 29835266,
      "verified" : false
    }
  },
  "id" : 844117541618405376,
  "created_at" : "2017-03-21 09:24:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/TNwosNWRkr",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BR5Q-pKAzJ1\/",
      "display_url" : "instagram.com\/p\/BR5Q-pKAzJ1\/"
    } ]
  },
  "geo" : { },
  "id_str" : "844115123337940993",
  "text" : "Tricolour flag flying in half mast in Dublin at a local supermarket. @ Dublin, Ireland https:\/\/t.co\/TNwosNWRkr",
  "id" : 844115123337940993,
  "created_at" : "2017-03-21 09:15:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wilfred Hughes",
      "screen_name" : "_wilfredh",
      "indices" : [ 3, 13 ],
      "id_str" : "46255806",
      "id" : 46255806
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/v2XURONjB0",
      "expanded_url" : "https:\/\/github.com\/datadesk\/california-crop-production-wages-analysis\/blob\/master\/03-analysis.ipynb",
      "display_url" : "github.com\/datadesk\/calif\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844092334111383552",
  "text" : "RT @_wilfredh: I'm impressed to see journalists showing their datasources and analysis as ipython notebooks: https:\/\/t.co\/v2XURONjB0",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/v2XURONjB0",
        "expanded_url" : "https:\/\/github.com\/datadesk\/california-crop-production-wages-analysis\/blob\/master\/03-analysis.ipynb",
        "display_url" : "github.com\/datadesk\/calif\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "843090571392684036",
    "text" : "I'm impressed to see journalists showing their datasources and analysis as ipython notebooks: https:\/\/t.co\/v2XURONjB0",
    "id" : 843090571392684036,
    "created_at" : "2017-03-18 13:23:50 +0000",
    "user" : {
      "name" : "Wilfred Hughes",
      "screen_name" : "_wilfredh",
      "protected" : false,
      "id_str" : "46255806",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/893808061898522624\/ES53mNK4_normal.jpg",
      "id" : 46255806,
      "verified" : false
    }
  },
  "id" : 844092334111383552,
  "created_at" : "2017-03-21 07:44:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Twitter Singapore",
      "screen_name" : "TwitterSG",
      "indices" : [ 3, 13 ],
      "id_str" : "2244983430",
      "id" : 2244983430
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LoveTwitter",
      "indices" : [ 38, 50 ]
    } ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/39WWuQgP4C",
      "expanded_url" : "https:\/\/twitter.com\/kevin_zanglei\/status\/844048264454270976",
      "display_url" : "twitter.com\/kevin_zanglei\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "844085850459815936",
  "text" : "RT @TwitterSG: We've turned 11 today! #LoveTwitter https:\/\/t.co\/39WWuQgP4C",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LoveTwitter",
        "indices" : [ 23, 35 ]
      } ],
      "urls" : [ {
        "indices" : [ 36, 59 ],
        "url" : "https:\/\/t.co\/39WWuQgP4C",
        "expanded_url" : "https:\/\/twitter.com\/kevin_zanglei\/status\/844048264454270976",
        "display_url" : "twitter.com\/kevin_zanglei\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "844055143616147456",
    "text" : "We've turned 11 today! #LoveTwitter https:\/\/t.co\/39WWuQgP4C",
    "id" : 844055143616147456,
    "created_at" : "2017-03-21 05:16:42 +0000",
    "user" : {
      "name" : "Twitter Singapore",
      "screen_name" : "TwitterSG",
      "protected" : false,
      "id_str" : "2244983430",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875094332365090816\/tcm6HqXp_normal.jpg",
      "id" : 2244983430,
      "verified" : true
    }
  },
  "id" : 844085850459815936,
  "created_at" : "2017-03-21 07:18:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/ffUGLelv4y",
      "expanded_url" : "https:\/\/twitter.com\/AP\/status\/843913617393291264",
      "display_url" : "twitter.com\/AP\/status\/8439\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843919052418568192",
  "text" : "The other soldier didn't seem to notice as the event unfold! Fail! https:\/\/t.co\/ffUGLelv4y",
  "id" : 843919052418568192,
  "created_at" : "2017-03-20 20:15:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Kirby",
      "screen_name" : "paul1kirby",
      "indices" : [ 3, 14 ],
      "id_str" : "1861764991",
      "id" : 1861764991
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843862906143985665",
  "text" : "RT @paul1kirby: 'What happens when the unemployed are given a year's benefits up front as capital to start a business'. https:\/\/t.co\/MuEAQE\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/MuEAQEAxkZ",
        "expanded_url" : "http:\/\/theconversation.com\/how-entrepreneurship-can-bring-people-off-benefits-if-theyre-given-a-chance-74499?utm_campaign=Echobox&utm_medium=Social&utm_source=Twitter#link_time=1489822576",
        "display_url" : "theconversation.com\/how-entreprene\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "843724072165015552",
    "text" : "'What happens when the unemployed are given a year's benefits up front as capital to start a business'. https:\/\/t.co\/MuEAQEAxkZ",
    "id" : 843724072165015552,
    "created_at" : "2017-03-20 07:21:09 +0000",
    "user" : {
      "name" : "Paul Kirby",
      "screen_name" : "paul1kirby",
      "protected" : false,
      "id_str" : "1861764991",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/419516029203922944\/iq8z0O5N_normal.jpeg",
      "id" : 1861764991,
      "verified" : false
    }
  },
  "id" : 843862906143985665,
  "created_at" : "2017-03-20 16:32:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lucy Tobin",
      "screen_name" : "lucytobin",
      "indices" : [ 3, 13 ],
      "id_str" : "17866959",
      "id" : 17866959
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843832767704158209",
  "text" : "RT @lucytobin: Teen, trying to impress girl, mauled by croc; MP asked about prevention: \"We can't legislate to protect d***heads\". https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/E2wi8G8jZv",
        "expanded_url" : "http:\/\/news.sky.com\/story\/aussie-teen-mauled-by-crocodile-trying-to-impress-british-backpacker-10808495",
        "display_url" : "news.sky.com\/story\/aussie-t\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "843831629676269568",
    "text" : "Teen, trying to impress girl, mauled by croc; MP asked about prevention: \"We can't legislate to protect d***heads\". https:\/\/t.co\/E2wi8G8jZv",
    "id" : 843831629676269568,
    "created_at" : "2017-03-20 14:28:32 +0000",
    "user" : {
      "name" : "Lucy Tobin",
      "screen_name" : "lucytobin",
      "protected" : false,
      "id_str" : "17866959",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/674331177659072512\/IhInnmYk_normal.jpg",
      "id" : 17866959,
      "verified" : true
    }
  },
  "id" : 843832767704158209,
  "created_at" : "2017-03-20 14:33:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843829887823757313",
  "text" : "Should I go with \"Established data science method in web\/marketing analytics\" for my new, fledgling venture?",
  "id" : 843829887823757313,
  "created_at" : "2017-03-20 14:21:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843817813752139776",
  "text" : "Did you ever get any branded corporate gifts that were practical and useful?",
  "id" : 843817813752139776,
  "created_at" : "2017-03-20 13:33:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters China",
      "screen_name" : "ReutersChina",
      "indices" : [ 3, 16 ],
      "id_str" : "599065130",
      "id" : 599065130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/CCxMrOIc80",
      "expanded_url" : "http:\/\/reut.rs\/2n549Wi",
      "display_url" : "reut.rs\/2n549Wi"
    } ]
  },
  "geo" : { },
  "id_str" : "843816851767541762",
  "text" : "RT @ReutersChina: South Korea complains to WTO over China's response to anti-missile system: minister https:\/\/t.co\/CCxMrOIc80",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/CCxMrOIc80",
        "expanded_url" : "http:\/\/reut.rs\/2n549Wi",
        "display_url" : "reut.rs\/2n549Wi"
      } ]
    },
    "geo" : { },
    "id_str" : "843656117544599553",
    "text" : "South Korea complains to WTO over China's response to anti-missile system: minister https:\/\/t.co\/CCxMrOIc80",
    "id" : 843656117544599553,
    "created_at" : "2017-03-20 02:51:07 +0000",
    "user" : {
      "name" : "Reuters China",
      "screen_name" : "ReutersChina",
      "protected" : false,
      "id_str" : "599065130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879875481427771392\/HmpIbr4g_normal.jpg",
      "id" : 599065130,
      "verified" : true
    }
  },
  "id" : 843816851767541762,
  "created_at" : "2017-03-20 13:29:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/cFeisT68rg",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-39325916",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843809118171398144",
  "text" : "M&amp;S pulls online ads from Google over extremist content fears https:\/\/t.co\/cFeisT68rg",
  "id" : 843809118171398144,
  "created_at" : "2017-03-20 12:59:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Carol Stewart",
      "screen_name" : "AboundSolutions",
      "indices" : [ 119, 135 ],
      "id_str" : "268418948",
      "id" : 268418948
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/j3dWh9WyV7",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/honest-did-you-think-woman-professor-robert-kelly-carol",
      "display_url" : "linkedin.com\/pulse\/honest-d\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843801518130249729",
  "text" : "\"Be honest, did you think the woman in the Professor Robert Kelly interview was the nanny?\" https:\/\/t.co\/j3dWh9WyV7 by @AboundSolutions",
  "id" : 843801518130249729,
  "created_at" : "2017-03-20 12:28:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/bVF0XLPfuv",
      "expanded_url" : "https:\/\/www.facebook.com\/MothershipSG\/videos\/vb.592308557475467\/1441941962512118\/?type=3&theater",
      "display_url" : "facebook.com\/MothershipSG\/v\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843779312125952000",
  "text" : "A Singaporean order beer in Gaeilge https:\/\/t.co\/bVF0XLPfuv  \uD83D\uDE02",
  "id" : 843779312125952000,
  "created_at" : "2017-03-20 11:00:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 83, 97 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/u7Gn9J38gG",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/economy\/dublin-placed-19th-in-list-of-world-s-smart-cities-1.3016556#.WM-xhkqY5ag.twitter",
      "display_url" : "irishtimes.com\/business\/econo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843774168671961089",
  "text" : "Dublin placed 19th in list of  world\u2019s  \u2018smart cities\u2019 https:\/\/t.co\/u7Gn9J38gG via @IrishTimesBiz",
  "id" : 843774168671961089,
  "created_at" : "2017-03-20 10:40:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "indices" : [ 3, 18 ],
      "id_str" : "1626294277",
      "id" : 1626294277
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/843350003540094976\/photo\/1",
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/IFUpixBuVU",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/C7QuRHLU8AEMcMX.jpg",
      "id_str" : "843349901744336897",
      "id" : 843349901744336897,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/C7QuRHLU8AEMcMX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 210,
        "resize" : "fit",
        "w" : 450
      }, {
        "h" : 210,
        "resize" : "fit",
        "w" : 450
      }, {
        "h" : 210,
        "resize" : "fit",
        "w" : 450
      }, {
        "h" : 210,
        "resize" : "fit",
        "w" : 450
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IFUpixBuVU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843767933306331136",
  "text" : "RT @spectatorindex: MEDIA: Japan's Maglev train traveling at 500 km\/h https:\/\/t.co\/IFUpixBuVU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/843350003540094976\/photo\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/IFUpixBuVU",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/C7QuRHLU8AEMcMX.jpg",
        "id_str" : "843349901744336897",
        "id" : 843349901744336897,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/C7QuRHLU8AEMcMX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 450
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 450
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 450
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 450
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IFUpixBuVU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "843350003540094976",
    "text" : "MEDIA: Japan's Maglev train traveling at 500 km\/h https:\/\/t.co\/IFUpixBuVU",
    "id" : 843350003540094976,
    "created_at" : "2017-03-19 06:34:44 +0000",
    "user" : {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "protected" : false,
      "id_str" : "1626294277",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839067030904922112\/LH4xqz-d_normal.jpg",
      "id" : 1626294277,
      "verified" : false
    }
  },
  "id" : 843767933306331136,
  "created_at" : "2017-03-20 10:15:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "indices" : [ 3, 14 ],
      "id_str" : "231474739",
      "id" : 231474739
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SmartNation",
      "indices" : [ 75, 87 ]
    }, {
      "text" : "DigiGov",
      "indices" : [ 88, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/G2N9Oap4JC",
      "expanded_url" : "http:\/\/www.straitstimes.com\/tech\/government-reorganises-to-turbo-charge-smart-nation-projects?xtor=CS3-19",
      "display_url" : "straitstimes.com\/tech\/governmen\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843758477289885696",
  "text" : "RT @heliumlife: GovTech will now come under Prime Minister's Office (PMO). #SmartNation #DigiGov https:\/\/t.co\/G2N9Oap4JC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SmartNation",
        "indices" : [ 59, 71 ]
      }, {
        "text" : "DigiGov",
        "indices" : [ 72, 80 ]
      } ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/G2N9Oap4JC",
        "expanded_url" : "http:\/\/www.straitstimes.com\/tech\/government-reorganises-to-turbo-charge-smart-nation-projects?xtor=CS3-19",
        "display_url" : "straitstimes.com\/tech\/governmen\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "843723161694744576",
    "text" : "GovTech will now come under Prime Minister's Office (PMO). #SmartNation #DigiGov https:\/\/t.co\/G2N9Oap4JC",
    "id" : 843723161694744576,
    "created_at" : "2017-03-20 07:17:32 +0000",
    "user" : {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "protected" : false,
      "id_str" : "231474739",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039125716262998018\/7LUaZasZ_normal.jpg",
      "id" : 231474739,
      "verified" : false
    }
  },
  "id" : 843758477289885696,
  "created_at" : "2017-03-20 09:37:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Novak",
      "screen_name" : "paleofuture",
      "indices" : [ 3, 15 ],
      "id_str" : "16877374",
      "id" : 16877374
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/mCrD8dGbdo",
      "expanded_url" : "https:\/\/twitter.com\/cbssunday\/status\/843473435519057921",
      "display_url" : "twitter.com\/cbssunday\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843560405767217153",
  "text" : "RT @paleofuture: Precisely zero people in the world were predicting that it was the United States, guys https:\/\/t.co\/mCrD8dGbdo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 110 ],
        "url" : "https:\/\/t.co\/mCrD8dGbdo",
        "expanded_url" : "https:\/\/twitter.com\/cbssunday\/status\/843473435519057921",
        "display_url" : "twitter.com\/cbssunday\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "843528557825466368",
    "text" : "Precisely zero people in the world were predicting that it was the United States, guys https:\/\/t.co\/mCrD8dGbdo",
    "id" : 843528557825466368,
    "created_at" : "2017-03-19 18:24:14 +0000",
    "user" : {
      "name" : "Matt Novak",
      "screen_name" : "paleofuture",
      "protected" : false,
      "id_str" : "16877374",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/993928221115670528\/lb2GYI6O_normal.jpg",
      "id" : 16877374,
      "verified" : false
    }
  },
  "id" : 843560405767217153,
  "created_at" : "2017-03-19 20:30:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mikko Hypponen",
      "screen_name" : "mikko",
      "indices" : [ 3, 9 ],
      "id_str" : "23566038",
      "id" : 23566038
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843452253386215424",
  "text" : "RT @mikko: I would not have thought of that. Using Blockchain to prevent rolling back the odometer in used cars (ie. 'clocking' or 'busting\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mikko\/status\/843020197611081728\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/e0K0SGeL2S",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7MCZFRWwAETwsF.jpg",
        "id_str" : "843020185183371265",
        "id" : 843020185183371265,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7MCZFRWwAETwsF.jpg",
        "sizes" : [ {
          "h" : 960,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/e0K0SGeL2S"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "843020197611081728",
    "text" : "I would not have thought of that. Using Blockchain to prevent rolling back the odometer in used cars (ie. 'clocking' or 'busting miles'. https:\/\/t.co\/e0K0SGeL2S",
    "id" : 843020197611081728,
    "created_at" : "2017-03-18 08:44:12 +0000",
    "user" : {
      "name" : "Mikko Hypponen",
      "screen_name" : "mikko",
      "protected" : false,
      "id_str" : "23566038",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879614951446466560\/tIIIPp2E_normal.jpg",
      "id" : 23566038,
      "verified" : false
    }
  },
  "id" : 843452253386215424,
  "created_at" : "2017-03-19 13:21:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Dotnetster\/status\/843431948164980737\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/RSvAGbSmLZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7R42hPWwAAR_M8.jpg",
      "id_str" : "843431908256169984",
      "id" : 843431908256169984,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7R42hPWwAAR_M8.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RSvAGbSmLZ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843432094013554688",
  "text" : "RT @Dotnetster: Clearing out some books. DM me if you want any of these. https:\/\/t.co\/RSvAGbSmLZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Dotnetster\/status\/843431948164980737\/photo\/1",
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/RSvAGbSmLZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7R42hPWwAAR_M8.jpg",
        "id_str" : "843431908256169984",
        "id" : 843431908256169984,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7R42hPWwAAR_M8.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RSvAGbSmLZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "843431948164980737",
    "text" : "Clearing out some books. DM me if you want any of these. https:\/\/t.co\/RSvAGbSmLZ",
    "id" : 843431948164980737,
    "created_at" : "2017-03-19 12:00:21 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 843432094013554688,
  "created_at" : "2017-03-19 12:00:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843431313600319489",
  "text" : "Looking for a  bike camera recorder. Any recommendation? Thanks",
  "id" : 843431313600319489,
  "created_at" : "2017-03-19 11:57:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/843254570126065664\/photo\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/de7pPTpm2B",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7PXip8WkAA6zdr.jpg",
      "id_str" : "843254545622929408",
      "id" : 843254545622929408,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7PXip8WkAA6zdr.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 283,
        "resize" : "fit",
        "w" : 757
      }, {
        "h" : 254,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 283,
        "resize" : "fit",
        "w" : 757
      }, {
        "h" : 283,
        "resize" : "fit",
        "w" : 757
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/de7pPTpm2B"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843254570126065664",
  "text" : "On a Saturday late evening, messing with Google API https:\/\/t.co\/de7pPTpm2B",
  "id" : 843254570126065664,
  "created_at" : "2017-03-19 00:15:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Meta S. Brown",
      "screen_name" : "metabrown312",
      "indices" : [ 3, 16 ],
      "id_str" : "53281785",
      "id" : 53281785
    }, {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 112, 119 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843203369107931136",
  "text" : "RT @metabrown312: The difference between analytics failure and analytics success is planning and execution. via @forbes https:\/\/t.co\/yMqNuD\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Forbes",
        "screen_name" : "Forbes",
        "indices" : [ 94, 101 ],
        "id_str" : "91478624",
        "id" : 91478624
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/yMqNuDiZ4v",
        "expanded_url" : "http:\/\/www.forbes.com\/sites\/metabrown\/2017\/01\/31\/86-of-executives-cant-make-analytics-pay-heres-how-you-can\/#2cd8b5a74e80",
        "display_url" : "forbes.com\/sites\/metabrow\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "842744058854473728",
    "text" : "The difference between analytics failure and analytics success is planning and execution. via @forbes https:\/\/t.co\/yMqNuDiZ4v",
    "id" : 842744058854473728,
    "created_at" : "2017-03-17 14:26:55 +0000",
    "user" : {
      "name" : "Meta S. Brown",
      "screen_name" : "metabrown312",
      "protected" : false,
      "id_str" : "53281785",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1171118967\/Meta_Brown_headshot_2_normal.jpg",
      "id" : 53281785,
      "verified" : false
    }
  },
  "id" : 843203369107931136,
  "created_at" : "2017-03-18 20:52:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mark little",
      "screen_name" : "marklittlenews",
      "indices" : [ 3, 18 ],
      "id_str" : "59503113",
      "id" : 59503113
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843197948938870784",
  "text" : "RT @marklittlenews: \u201CThis electronic message was no different than a bomb sent in the mail or anthrax sent in an envelope,\" https:\/\/t.co\/9I\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/9IqfvXS01x",
        "expanded_url" : "https:\/\/twitter.com\/jeffjarvis\/status\/843044675611250690",
        "display_url" : "twitter.com\/jeffjarvis\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "843061556229410817",
    "text" : "\u201CThis electronic message was no different than a bomb sent in the mail or anthrax sent in an envelope,\" https:\/\/t.co\/9IqfvXS01x",
    "id" : 843061556229410817,
    "created_at" : "2017-03-18 11:28:32 +0000",
    "user" : {
      "name" : "mark little",
      "screen_name" : "marklittlenews",
      "protected" : false,
      "id_str" : "59503113",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/823588595877421056\/NmV_Dl-Q_normal.jpg",
      "id" : 59503113,
      "verified" : true
    }
  },
  "id" : 843197948938870784,
  "created_at" : "2017-03-18 20:30:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    }, {
      "name" : "MTI Singapore",
      "screen_name" : "MTI_Sg",
      "indices" : [ 89, 96 ],
      "id_str" : "601598333",
      "id" : 601598333
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 48, 62 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843180038304468992",
  "text" : "RT @IrlEmbSingapore: Very productive meeting on #StPatricksDay b\/t Minister Noonan &amp; @MTI_Sg Minister Lim. Strong links continue to grow. @\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "MTI Singapore",
        "screen_name" : "MTI_Sg",
        "indices" : [ 68, 75 ],
        "id_str" : "601598333",
        "id" : 601598333
      }, {
        "name" : "Department of Finance",
        "screen_name" : "IRLDeptFinance",
        "indices" : [ 121, 136 ],
        "id_str" : "1310401350",
        "id" : 1310401350
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/842640466004791297\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/QnCwvUTz46",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7GpBjGU4AA5AtC.jpg",
        "id_str" : "842640449361731584",
        "id" : 842640449361731584,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7GpBjGU4AA5AtC.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1784,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 592,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1784,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1045,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QnCwvUTz46"
      } ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 27, 41 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842640466004791297",
    "text" : "Very productive meeting on #StPatricksDay b\/t Minister Noonan &amp; @MTI_Sg Minister Lim. Strong links continue to grow. @IRLDeptFinance https:\/\/t.co\/QnCwvUTz46",
    "id" : 842640466004791297,
    "created_at" : "2017-03-17 07:35:17 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 843180038304468992,
  "created_at" : "2017-03-18 19:19:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Brexit",
      "indices" : [ 132, 139 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843179757617463300",
  "text" : "RT @IrlEmbSingapore: Minister Noonan addresses the Economic Society of Singapore on Ireland's economic recovery &amp; challenges of #Brexit. @I\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Department of Finance",
        "screen_name" : "IRLDeptFinance",
        "indices" : [ 120, 135 ],
        "id_str" : "1310401350",
        "id" : 1310401350
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/842672986129747969\/photo\/1",
        "indices" : [ 145, 168 ],
        "url" : "https:\/\/t.co\/RfcgCJfip8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7HGmbLU4AAkE-R.jpg",
        "id_str" : "842672968727584768",
        "id" : 842672968727584768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7HGmbLU4AAkE-R.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 696,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 394,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1188,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1188,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RfcgCJfip8"
      } ],
      "hashtags" : [ {
        "text" : "Brexit",
        "indices" : [ 111, 118 ]
      }, {
        "text" : "SPD2017",
        "indices" : [ 136, 144 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842672986129747969",
    "text" : "Minister Noonan addresses the Economic Society of Singapore on Ireland's economic recovery &amp; challenges of #Brexit. @IRLDeptFinance #SPD2017 https:\/\/t.co\/RfcgCJfip8",
    "id" : 842672986129747969,
    "created_at" : "2017-03-17 09:44:30 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 843179757617463300,
  "created_at" : "2017-03-18 19:18:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Damien Tiernan",
      "screen_name" : "damienrte",
      "indices" : [ 3, 13 ],
      "id_str" : "204033329",
      "id" : 204033329
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/damienrte\/status\/843083817682456576\/video\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/BjXb09FPX5",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/843083496235188224\/pu\/img\/bIqBQgkKsawoYCbG.jpg",
      "id_str" : "843083496235188224",
      "id" : 843083496235188224,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/843083496235188224\/pu\/img\/bIqBQgkKsawoYCbG.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BjXb09FPX5"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843178489385828352",
  "text" : "RT @damienrte: Coastguard helicopter does fly past at funeral of Dara Fitzpatrick https:\/\/t.co\/BjXb09FPX5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/damienrte\/status\/843083817682456576\/video\/1",
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/BjXb09FPX5",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/843083496235188224\/pu\/img\/bIqBQgkKsawoYCbG.jpg",
        "id_str" : "843083496235188224",
        "id" : 843083496235188224,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/843083496235188224\/pu\/img\/bIqBQgkKsawoYCbG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BjXb09FPX5"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "843083817682456576",
    "text" : "Coastguard helicopter does fly past at funeral of Dara Fitzpatrick https:\/\/t.co\/BjXb09FPX5",
    "id" : 843083817682456576,
    "created_at" : "2017-03-18 12:57:00 +0000",
    "user" : {
      "name" : "Damien Tiernan",
      "screen_name" : "damienrte",
      "protected" : false,
      "id_str" : "204033329",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001139468873732097\/VHbKs3sT_normal.jpg",
      "id" : 204033329,
      "verified" : false
    }
  },
  "id" : 843178489385828352,
  "created_at" : "2017-03-18 19:13:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 6, 29 ],
      "url" : "https:\/\/t.co\/NoIS6chpkM",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/843145236834476032",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "843150874218250242",
  "text" : "\u5BF9\uD83D\uDC2E\u8C08\u7434\uFF1F https:\/\/t.co\/NoIS6chpkM",
  "id" : 843150874218250242,
  "created_at" : "2017-03-18 17:23:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "indices" : [ 3, 11 ],
      "id_str" : "20697268",
      "id" : 20697268
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 19, 27 ]
    }, {
      "text" : "IREvENG",
      "indices" : [ 79, 87 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "843150465646968832",
  "text" : "RT @KJoanne: Right #Ireland, let's start what we have come into the room to do #IREvENG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Ireland",
        "indices" : [ 6, 14 ]
      }, {
        "text" : "IREvENG",
        "indices" : [ 66, 74 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "843146067596263427",
    "text" : "Right #Ireland, let's start what we have come into the room to do #IREvENG",
    "id" : 843146067596263427,
    "created_at" : "2017-03-18 17:04:22 +0000",
    "user" : {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "protected" : false,
      "id_str" : "20697268",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005549984522043392\/YDQyk6cL_normal.jpg",
      "id" : 20697268,
      "verified" : false
    }
  },
  "id" : 843150465646968832,
  "created_at" : "2017-03-18 17:21:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/ppRrj4jUB8",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/842962855397744640",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842996550527782912",
  "text" : "According to Wikipedia, SAF is the first customer. https:\/\/t.co\/ppRrj4jUB8",
  "id" : 842996550527782912,
  "created_at" : "2017-03-18 07:10:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842799971728605190",
  "text" : "What data is missing from your web analytics tools for your needs? What do you use to fill that gap? Thanks.",
  "id" : 842799971728605190,
  "created_at" : "2017-03-17 18:09:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/842756486598217728\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/n0C8TEJFEc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7ISXrUWwAEJtVd.jpg",
      "id_str" : "842756278246096897",
      "id" : 842756278246096897,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7ISXrUWwAEJtVd.jpg",
      "sizes" : [ {
        "h" : 645,
        "resize" : "fit",
        "w" : 1104
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 397,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 645,
        "resize" : "fit",
        "w" : 1104
      }, {
        "h" : 645,
        "resize" : "fit",
        "w" : 1104
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/n0C8TEJFEc"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842756486598217728",
  "text" : "Can anyone tell me what happening? Is it got to do with library(googleAuthR)? Thanks https:\/\/t.co\/n0C8TEJFEc",
  "id" : 842756486598217728,
  "created_at" : "2017-03-17 15:16:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HeatherLeson",
      "screen_name" : "HeatherLeson",
      "indices" : [ 3, 16 ],
      "id_str" : "10313032",
      "id" : 10313032
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/r76iWRcv8X",
      "expanded_url" : "https:\/\/www.theguardian.com\/global-development-professionals-network\/2017\/mar\/17\/access-to-drinking-water-world-six-infographics?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/global-develop\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842725587487789056",
  "text" : "RT @HeatherLeson: Access to drinking water around the world \u2013 in five infographics https:\/\/t.co\/r76iWRcv8X",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 65, 88 ],
        "url" : "https:\/\/t.co\/r76iWRcv8X",
        "expanded_url" : "https:\/\/www.theguardian.com\/global-development-professionals-network\/2017\/mar\/17\/access-to-drinking-water-world-six-infographics?CMP=share_btn_tw",
        "display_url" : "theguardian.com\/global-develop\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "842722021016555520",
    "text" : "Access to drinking water around the world \u2013 in five infographics https:\/\/t.co\/r76iWRcv8X",
    "id" : 842722021016555520,
    "created_at" : "2017-03-17 12:59:21 +0000",
    "user" : {
      "name" : "HeatherLeson",
      "screen_name" : "HeatherLeson",
      "protected" : false,
      "id_str" : "10313032",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/900304683075592192\/4YNChpey_normal.jpg",
      "id" : 10313032,
      "verified" : false
    }
  },
  "id" : 842725587487789056,
  "created_at" : "2017-03-17 13:13:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shane O Leary",
      "screen_name" : "shaneoleary1",
      "indices" : [ 3, 16 ],
      "id_str" : "21563020",
      "id" : 21563020
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 28, 42 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842711652042727424",
  "text" : "RT @shaneoleary1: The 'what #StPatricksDay can teach us about business' posts have started! \uD83D\uDE48\n\n1) Drive all the \uD83D\uDC0D out of your company.\n\nAbo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 10, 24 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842708972633309184",
    "text" : "The 'what #StPatricksDay can teach us about business' posts have started! \uD83D\uDE48\n\n1) Drive all the \uD83D\uDC0D out of your company.\n\nAbout it really!",
    "id" : 842708972633309184,
    "created_at" : "2017-03-17 12:07:30 +0000",
    "user" : {
      "name" : "Shane O Leary",
      "screen_name" : "shaneoleary1",
      "protected" : false,
      "id_str" : "21563020",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/997798492386062336\/pOqDh1tb_normal.jpg",
      "id" : 21563020,
      "verified" : false
    }
  },
  "id" : 842711652042727424,
  "created_at" : "2017-03-17 12:18:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/842701094912516099\/photo\/1",
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/ecUt1luZ6D",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7HfUGpXQAEEwYm.jpg",
      "id_str" : "842700141769474049",
      "id" : 842700141769474049,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7HfUGpXQAEEwYm.jpg",
      "sizes" : [ {
        "h" : 271,
        "resize" : "fit",
        "w" : 826
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 271,
        "resize" : "fit",
        "w" : 826
      }, {
        "h" : 223,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 271,
        "resize" : "fit",
        "w" : 826
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ecUt1luZ6D"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842701094912516099",
  "text" : "Deploying data analyses code into interactive web applications https:\/\/t.co\/ecUt1luZ6D",
  "id" : 842701094912516099,
  "created_at" : "2017-03-17 11:36:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Henderson",
      "screen_name" : "johnhenderson",
      "indices" : [ 3, 17 ],
      "id_str" : "472398242",
      "id" : 472398242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842685207878754304",
  "text" : "RT @johnhenderson: 1300 employees working on machine learning! \uD83D\uDE2E \"The Mobile Internet Is Over. Baidu Goes All In on AI\" https:\/\/t.co\/cX1zdx\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/cX1zdxf9FE",
        "expanded_url" : "https:\/\/www.bloomberg.com\/amp\/news\/articles\/2017-03-16\/the-mobile-internet-is-over-baidu-goes-all-in-on-ai",
        "display_url" : "bloomberg.com\/amp\/news\/artic\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "842543653671374850",
    "text" : "1300 employees working on machine learning! \uD83D\uDE2E \"The Mobile Internet Is Over. Baidu Goes All In on AI\" https:\/\/t.co\/cX1zdxf9FE",
    "id" : 842543653671374850,
    "created_at" : "2017-03-17 01:10:35 +0000",
    "user" : {
      "name" : "John Henderson",
      "screen_name" : "johnhenderson",
      "protected" : false,
      "id_str" : "472398242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/765360881140051968\/6bX9z-B5_normal.jpg",
      "id" : 472398242,
      "verified" : true
    }
  },
  "id" : 842685207878754304,
  "created_at" : "2017-03-17 10:33:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "indices" : [ 3, 11 ],
      "id_str" : "20697268",
      "id" : 20697268
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/NrQ8hzXriK",
      "expanded_url" : "https:\/\/twitter.com\/channel4news\/status\/842661349415157760",
      "display_url" : "twitter.com\/channel4news\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842671213625466881",
  "text" : "RT @KJoanne: He's redeemed himself. https:\/\/t.co\/NrQ8hzXriK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/NrQ8hzXriK",
        "expanded_url" : "https:\/\/twitter.com\/channel4news\/status\/842661349415157760",
        "display_url" : "twitter.com\/channel4news\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "842663077166436353",
    "text" : "He's redeemed himself. https:\/\/t.co\/NrQ8hzXriK",
    "id" : 842663077166436353,
    "created_at" : "2017-03-17 09:05:08 +0000",
    "user" : {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "protected" : false,
      "id_str" : "20697268",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005549984522043392\/YDQyk6cL_normal.jpg",
      "id" : 20697268,
      "verified" : false
    }
  },
  "id" : 842671213625466881,
  "created_at" : "2017-03-17 09:37:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kurt Vonnegut",
      "screen_name" : "Kurt_Vonnegut",
      "indices" : [ 3, 17 ],
      "id_str" : "37350044",
      "id" : 37350044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842669830146838529",
  "text" : "RT @Kurt_Vonnegut: True terror is to wake up one morning and discover that your high school class is running the country.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/vonneguttwitterbothomepage.com\" rel=\"nofollow\"\u003EVaultTweeter\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842647449470746625",
    "text" : "True terror is to wake up one morning and discover that your high school class is running the country.",
    "id" : 842647449470746625,
    "created_at" : "2017-03-17 08:03:02 +0000",
    "user" : {
      "name" : "Kurt Vonnegut",
      "screen_name" : "Kurt_Vonnegut",
      "protected" : false,
      "id_str" : "37350044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/193924187\/Vonnegut_normal.jpg",
      "id" : 37350044,
      "verified" : false
    }
  },
  "id" : 842669830146838529,
  "created_at" : "2017-03-17 09:31:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 0, 12 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "842434722609434624",
  "geo" : { },
  "id_str" : "842669745673519104",
  "in_reply_to_user_id" : 19793936,
  "text" : "@gianniponzi Realized today is St Patrick's Day not tomorrow. \uD83D\uDE00",
  "id" : 842669745673519104,
  "in_reply_to_status_id" : 842434722609434624,
  "created_at" : "2017-03-17 09:31:38 +0000",
  "in_reply_to_screen_name" : "gianniponzi",
  "in_reply_to_user_id_str" : "19793936",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dara \u00D3 Briain",
      "screen_name" : "daraobriain",
      "indices" : [ 3, 15 ],
      "id_str" : "44874400",
      "id" : 44874400
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842669285910765570",
  "text" : "RT @daraobriain: Happy St. Patrick's day everyone! Remember it's Paddy, not Patty; shamrock is 3 leaves and not 4; Guinness isn't compulsor\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842663443257835521",
    "text" : "Happy St. Patrick's day everyone! Remember it's Paddy, not Patty; shamrock is 3 leaves and not 4; Guinness isn't compulsory for either of us",
    "id" : 842663443257835521,
    "created_at" : "2017-03-17 09:06:35 +0000",
    "user" : {
      "name" : "Dara \u00D3 Briain",
      "screen_name" : "daraobriain",
      "protected" : false,
      "id_str" : "44874400",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/251011476\/dobrian08_normal.jpg",
      "id" : 44874400,
      "verified" : false
    }
  },
  "id" : 842669285910765570,
  "created_at" : "2017-03-17 09:29:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Aoibhinn N\u00ED Sh\u00FAilleabh\u00E1in",
      "screen_name" : "aoibhinn_ni_s",
      "indices" : [ 3, 17 ],
      "id_str" : "274636870",
      "id" : 274636870
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842669200074334208",
  "text" : "RT @aoibhinn_ni_s: Not sure how this graphic is supposed to be interpreted...are English wins more important than Welsh ones @IrishTimesSpo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Times Sport",
        "screen_name" : "IrishTimesSport",
        "indices" : [ 106, 122 ],
        "id_str" : "875998886",
        "id" : 875998886
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/aoibhinn_ni_s\/status\/842306363212603392\/photo\/1",
        "indices" : [ 134, 157 ],
        "url" : "https:\/\/t.co\/X7PgowKxFd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7B5FpyWcAA1WJN.jpg",
        "id_str" : "842306268341628928",
        "id" : 842306268341628928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7B5FpyWcAA1WJN.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/X7PgowKxFd"
      } ],
      "hashtags" : [ {
        "text" : "badstats",
        "indices" : [ 124, 133 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842306363212603392",
    "text" : "Not sure how this graphic is supposed to be interpreted...are English wins more important than Welsh ones @IrishTimesSport? #badstats https:\/\/t.co\/X7PgowKxFd",
    "id" : 842306363212603392,
    "created_at" : "2017-03-16 09:27:40 +0000",
    "user" : {
      "name" : "Dr Aoibhinn N\u00ED Sh\u00FAilleabh\u00E1in",
      "screen_name" : "aoibhinn_ni_s",
      "protected" : false,
      "id_str" : "274636870",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/983251931547119617\/R87DL_Up_normal.jpg",
      "id" : 274636870,
      "verified" : true
    }
  },
  "id" : 842669200074334208,
  "created_at" : "2017-03-17 09:29:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Collins",
      "screen_name" : "LJCollins_",
      "indices" : [ 3, 14 ],
      "id_str" : "861836838071226368",
      "id" : 861836838071226368
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/LJCollins_\/status\/842537875539283968\/photo\/1",
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/WFGi0CX6de",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7FLs4yVwAE4_Wq.jpg",
      "id_str" : "842537839824846849",
      "id" : 842537839824846849,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7FLs4yVwAE4_Wq.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1713,
        "resize" : "fit",
        "w" : 1266
      }, {
        "h" : 1713,
        "resize" : "fit",
        "w" : 1266
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 503
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 887
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WFGi0CX6de"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842668014818545664",
  "text" : "RT @LJCollins_: The evolution of confirm shaming https:\/\/t.co\/WFGi0CX6de",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LJCollins_\/status\/842537875539283968\/photo\/1",
        "indices" : [ 33, 56 ],
        "url" : "https:\/\/t.co\/WFGi0CX6de",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7FLs4yVwAE4_Wq.jpg",
        "id_str" : "842537839824846849",
        "id" : 842537839824846849,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7FLs4yVwAE4_Wq.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1713,
          "resize" : "fit",
          "w" : 1266
        }, {
          "h" : 1713,
          "resize" : "fit",
          "w" : 1266
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 503
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 887
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WFGi0CX6de"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842537875539283968",
    "text" : "The evolution of confirm shaming https:\/\/t.co\/WFGi0CX6de",
    "id" : 842537875539283968,
    "created_at" : "2017-03-17 00:47:37 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 842668014818545664,
  "created_at" : "2017-03-17 09:24:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "indices" : [ 0, 13 ],
      "id_str" : "2300614526",
      "id" : 2300614526
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "842549516083630080",
  "geo" : { },
  "id_str" : "842551583741300738",
  "in_reply_to_user_id" : 2300614526,
  "text" : "@pachamaltese Thks.",
  "id" : 842551583741300738,
  "in_reply_to_status_id" : 842549516083630080,
  "created_at" : "2017-03-17 01:42:06 +0000",
  "in_reply_to_screen_name" : "pachamaltese",
  "in_reply_to_user_id_str" : "2300614526",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842549167553691648",
  "text" : "Current challenge: How to turn a R Script that works in RStudio into something that works via a browser that people want to pay for?",
  "id" : 842549167553691648,
  "created_at" : "2017-03-17 01:32:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fora.ie",
      "screen_name" : "Fora_ie",
      "indices" : [ 3, 11 ],
      "id_str" : "705366913266929666",
      "id" : 705366913266929666
    }, {
      "name" : "Sigmar Recruitment",
      "screen_name" : "SigmarIrl",
      "indices" : [ 121, 131 ],
      "id_str" : "43312010",
      "id" : 43312010
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/4bIRWbRGky",
      "expanded_url" : "http:\/\/bit.ly\/2mSKAQY",
      "display_url" : "bit.ly\/2mSKAQY"
    } ]
  },
  "geo" : { },
  "id_str" : "842543948958744576",
  "text" : "RT @Fora_ie: 'Free bars are great - but meaningful work is the top motivator for job candidates' https:\/\/t.co\/4bIRWbRGky @SigmarIrl https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Sigmar Recruitment",
        "screen_name" : "SigmarIrl",
        "indices" : [ 108, 118 ],
        "id_str" : "43312010",
        "id" : 43312010
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Fora_ie\/status\/842393781345374208\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/sHhOvnWBov",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DIregU8AA-WTh.jpg",
        "id_str" : "842393779566866432",
        "id" : 842393779566866432,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DIregU8AA-WTh.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 511
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 511
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 511
        }, {
          "h" : 338,
          "resize" : "fit",
          "w" : 511
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sHhOvnWBov"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/4bIRWbRGky",
        "expanded_url" : "http:\/\/bit.ly\/2mSKAQY",
        "display_url" : "bit.ly\/2mSKAQY"
      } ]
    },
    "geo" : { },
    "id_str" : "842393781345374208",
    "text" : "'Free bars are great - but meaningful work is the top motivator for job candidates' https:\/\/t.co\/4bIRWbRGky @SigmarIrl https:\/\/t.co\/sHhOvnWBov",
    "id" : 842393781345374208,
    "created_at" : "2017-03-16 15:15:03 +0000",
    "user" : {
      "name" : "Fora.ie",
      "screen_name" : "Fora_ie",
      "protected" : false,
      "id_str" : "705366913266929666",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/718134307668291584\/QsC-w1I5_normal.jpg",
      "id" : 705366913266929666,
      "verified" : true
    }
  },
  "id" : 842543948958744576,
  "created_at" : "2017-03-17 01:11:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jennifer Baker",
      "screen_name" : "BrusselsGeek",
      "indices" : [ 3, 16 ],
      "id_str" : "29577436",
      "id" : 29577436
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "EUdataP",
      "indices" : [ 46, 54 ]
    }, {
      "text" : "startups",
      "indices" : [ 80, 89 ]
    }, {
      "text" : "innovation",
      "indices" : [ 95, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/9TMtR96zgI",
      "expanded_url" : "http:\/\/bit.ly\/2mZrglo",
      "display_url" : "bit.ly\/2mZrglo"
    } ]
  },
  "geo" : { },
  "id_str" : "842538304696389633",
  "text" : "RT @BrusselsGeek: Full version of yesterday's #EUdataP rant calling bullshit on #startups tech #innovation claims! https:\/\/t.co\/9TMtR96zgI\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BrusselsGeek\/status\/842003891789148162\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/5JIjSArb9c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C69lMkXW0AAnpNR.jpg",
        "id_str" : "842002921936048128",
        "id" : 842002921936048128,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C69lMkXW0AAnpNR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 624
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 624
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 624
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 624
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5JIjSArb9c"
      } ],
      "hashtags" : [ {
        "text" : "EUdataP",
        "indices" : [ 28, 36 ]
      }, {
        "text" : "startups",
        "indices" : [ 62, 71 ]
      }, {
        "text" : "innovation",
        "indices" : [ 77, 88 ]
      } ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/9TMtR96zgI",
        "expanded_url" : "http:\/\/bit.ly\/2mZrglo",
        "display_url" : "bit.ly\/2mZrglo"
      } ]
    },
    "geo" : { },
    "id_str" : "842003891789148162",
    "text" : "Full version of yesterday's #EUdataP rant calling bullshit on #startups tech #innovation claims! https:\/\/t.co\/9TMtR96zgI https:\/\/t.co\/5JIjSArb9c",
    "id" : 842003891789148162,
    "created_at" : "2017-03-15 13:25:46 +0000",
    "user" : {
      "name" : "Jennifer Baker",
      "screen_name" : "BrusselsGeek",
      "protected" : false,
      "id_str" : "29577436",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/878230512812347393\/PsTwuod__normal.jpg",
      "id" : 29577436,
      "verified" : true
    }
  },
  "id" : 842538304696389633,
  "created_at" : "2017-03-17 00:49:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842537665543258112",
  "text" : "Going by my twitter timeline,  co-working space spawning all over Dublin....",
  "id" : 842537665543258112,
  "created_at" : "2017-03-17 00:46:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    }, {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 115, 129 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/mZ9IbBtd2n",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/technology\/no-the-illegal-irish-in-us-should-not-be-a-special-case-1.3011573#.WMpp_fVJrfY.twitter",
      "display_url" : "irishtimes.com\/business\/techn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842533799053447169",
  "text" : "RT @klillington: My column: No, the illegal Irish in US should not be a \u2018special\u2019 case https:\/\/t.co\/mZ9IbBtd2n via @IrishTimesBiz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Times Business",
        "screen_name" : "IrishTimesBiz",
        "indices" : [ 98, 112 ],
        "id_str" : "16737418",
        "id" : 16737418
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/mZ9IbBtd2n",
        "expanded_url" : "http:\/\/www.irishtimes.com\/business\/technology\/no-the-illegal-irish-in-us-should-not-be-a-special-case-1.3011573#.WMpp_fVJrfY.twitter",
        "display_url" : "irishtimes.com\/business\/techn\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "842322976527794176",
    "text" : "My column: No, the illegal Irish in US should not be a \u2018special\u2019 case https:\/\/t.co\/mZ9IbBtd2n via @IrishTimesBiz",
    "id" : 842322976527794176,
    "created_at" : "2017-03-16 10:33:41 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 842533799053447169,
  "created_at" : "2017-03-17 00:31:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 0, 12 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "842432577763958784",
  "geo" : { },
  "id_str" : "842433249318195200",
  "in_reply_to_user_id" : 19793936,
  "text" : "@gianniponzi signing off so soon? It still Thursday. :)",
  "id" : 842433249318195200,
  "in_reply_to_status_id" : 842432577763958784,
  "created_at" : "2017-03-16 17:51:52 +0000",
  "in_reply_to_screen_name" : "gianniponzi",
  "in_reply_to_user_id_str" : "19793936",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paulette Aniskoff",
      "screen_name" : "PAniskoff",
      "indices" : [ 3, 13 ],
      "id_str" : "821172724701786112",
      "id" : 821172724701786112
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/7nacdgvvv7",
      "expanded_url" : "https:\/\/twitter.com\/juliacarriew\/status\/842177483004813313",
      "display_url" : "twitter.com\/juliacarriew\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842432101307805696",
  "text" : "RT @PAniskoff: America \u2764 https:\/\/t.co\/7nacdgvvv7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 10, 33 ],
        "url" : "https:\/\/t.co\/7nacdgvvv7",
        "expanded_url" : "https:\/\/twitter.com\/juliacarriew\/status\/842177483004813313",
        "display_url" : "twitter.com\/juliacarriew\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "842189983091986433",
    "text" : "America \u2764 https:\/\/t.co\/7nacdgvvv7",
    "id" : 842189983091986433,
    "created_at" : "2017-03-16 01:45:13 +0000",
    "user" : {
      "name" : "Paulette Aniskoff",
      "screen_name" : "PAniskoff",
      "protected" : false,
      "id_str" : "821172724701786112",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/821184826128605185\/k-6Z_stj_normal.jpg",
      "id" : 821172724701786112,
      "verified" : true
    }
  },
  "id" : 842432101307805696,
  "created_at" : "2017-03-16 17:47:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 62, 76 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 82, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842430408809680896",
  "text" : "RT @IrlEmbSingapore: Great to have Minister Michael Noonan at #StPatricksDay\u2618\uFE0F in #Singapore with Irish dance, food, and fun! @IRLDeptFinan\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Department of Finance",
        "screen_name" : "IRLDeptFinance",
        "indices" : [ 105, 120 ],
        "id_str" : "1310401350",
        "id" : 1310401350
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/842405819467538432\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/kUAx1WXYYK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DTnfKVwAEWxsp.jpg",
        "id_str" : "842405805651509249",
        "id" : 842405805651509249,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DTnfKVwAEWxsp.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kUAx1WXYYK"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/842405819467538432\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/kUAx1WXYYK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DTngSVsAIzzrw.jpg",
        "id_str" : "842405805953495042",
        "id" : 842405805953495042,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DTngSVsAIzzrw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kUAx1WXYYK"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/842405819467538432\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/kUAx1WXYYK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DTnfLVAAEfZ-3.jpg",
        "id_str" : "842405805655654401",
        "id" : 842405805655654401,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DTnfLVAAEfZ-3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kUAx1WXYYK"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/842405819467538432\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/kUAx1WXYYK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DTngRVwAEmQWq.jpg",
        "id_str" : "842405805949304833",
        "id" : 842405805949304833,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DTngRVwAEmQWq.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kUAx1WXYYK"
      } ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 41, 55 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 61, 71 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842405819467538432",
    "text" : "Great to have Minister Michael Noonan at #StPatricksDay\u2618\uFE0F in #Singapore with Irish dance, food, and fun! @IRLDeptFinance https:\/\/t.co\/kUAx1WXYYK",
    "id" : 842405819467538432,
    "created_at" : "2017-03-16 16:02:53 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 842430408809680896,
  "created_at" : "2017-03-16 17:40:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9 News",
      "screen_name" : "rtenews",
      "indices" : [ 3, 11 ],
      "id_str" : "8973062",
      "id" : 8973062
    }, {
      "name" : "RT\u00C9 news2day",
      "screen_name" : "news2dayRTE",
      "indices" : [ 14, 26 ],
      "id_str" : "268328620",
      "id" : 268328620
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842430144753065985",
  "text" : "RT @rtenews: .@news2dayRTE went to a Dublin school that is holding an international day to celebrate the 30 different nationalities in the\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RT\u00C9 news2day",
        "screen_name" : "news2dayRTE",
        "indices" : [ 1, 13 ],
        "id_str" : "268328620",
        "id" : 268328620
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rtenews\/status\/842424584561389568\/video\/1",
        "indices" : [ 133, 156 ],
        "url" : "https:\/\/t.co\/mlp2K7YWlQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DkbHhXAAAyWw4.jpg",
        "id_str" : "842422165198778368",
        "id" : 842422165198778368,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DkbHhXAAAyWw4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mlp2K7YWlQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842424584561389568",
    "text" : ".@news2dayRTE went to a Dublin school that is holding an international day to celebrate the 30 different nationalities in the school https:\/\/t.co\/mlp2K7YWlQ",
    "id" : 842424584561389568,
    "created_at" : "2017-03-16 17:17:27 +0000",
    "user" : {
      "name" : "RT\u00C9 News",
      "screen_name" : "rtenews",
      "protected" : false,
      "id_str" : "8973062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/535035010596630529\/3SmYLBeN_normal.png",
      "id" : 8973062,
      "verified" : true
    }
  },
  "id" : 842430144753065985,
  "created_at" : "2017-03-16 17:39:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victoria Macarthur\uD83C\uDF42",
      "screen_name" : "cufa",
      "indices" : [ 3, 8 ],
      "id_str" : "9522142",
      "id" : 9522142
    }, {
      "name" : "Style at Home",
      "screen_name" : "StyleAtHome",
      "indices" : [ 14, 26 ],
      "id_str" : "14789144",
      "id" : 14789144
    }, {
      "name" : "Brian Nisbet",
      "screen_name" : "natural20",
      "indices" : [ 29, 39 ],
      "id_str" : "20601237",
      "id" : 20601237
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cufa\/status\/842428881827184640\/photo\/1",
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/bl2xCQam62",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DolHwWkAALdIx.jpg",
      "id_str" : "842428854752940032",
      "id" : 842428854752940032,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DolHwWkAALdIx.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 810,
        "resize" : "fit",
        "w" : 568
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 477
      }, {
        "h" : 810,
        "resize" : "fit",
        "w" : 568
      }, {
        "h" : 810,
        "resize" : "fit",
        "w" : 568
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bl2xCQam62"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842429382467719171",
  "text" : "RT @cufa: WTF @StyleAtHome?! @natural20 spot on here. https:\/\/t.co\/bl2xCQam62",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Style at Home",
        "screen_name" : "StyleAtHome",
        "indices" : [ 4, 16 ],
        "id_str" : "14789144",
        "id" : 14789144
      }, {
        "name" : "Brian Nisbet",
        "screen_name" : "natural20",
        "indices" : [ 19, 29 ],
        "id_str" : "20601237",
        "id" : 20601237
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cufa\/status\/842428881827184640\/photo\/1",
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/bl2xCQam62",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C7DolHwWkAALdIx.jpg",
        "id_str" : "842428854752940032",
        "id" : 842428854752940032,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7DolHwWkAALdIx.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 810,
          "resize" : "fit",
          "w" : 568
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 477
        }, {
          "h" : 810,
          "resize" : "fit",
          "w" : 568
        }, {
          "h" : 810,
          "resize" : "fit",
          "w" : 568
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bl2xCQam62"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842428881827184640",
    "text" : "WTF @StyleAtHome?! @natural20 spot on here. https:\/\/t.co\/bl2xCQam62",
    "id" : 842428881827184640,
    "created_at" : "2017-03-16 17:34:31 +0000",
    "user" : {
      "name" : "Victoria Macarthur\uD83C\uDF42",
      "screen_name" : "cufa",
      "protected" : false,
      "id_str" : "9522142",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/801760278081900544\/v_oHHLtP_normal.jpg",
      "id" : 9522142,
      "verified" : false
    }
  },
  "id" : 842429382467719171,
  "created_at" : "2017-03-16 17:36:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 91, 101 ]
    } ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/tuaGsJJHy5",
      "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6246980902379982848\/",
      "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842391543893901312",
  "text" : "Seeking a Tech Lead (CTO) to build the Peoplewave suite of cloud-based HR tech software in #Singapore.  https:\/\/t.co\/tuaGsJJHy5",
  "id" : 842391543893901312,
  "created_at" : "2017-03-16 15:06:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/nLM8uPG3AS",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-europe-39292755",
      "display_url" : "bbc.com\/news\/world-eur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842352344104873984",
  "text" : "BBC News - French school shooting: 'Several hurt' in Grasse attack https:\/\/t.co\/nLM8uPG3AS",
  "id" : 842352344104873984,
  "created_at" : "2017-03-16 12:30:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/EKwEtOttGl",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/842334496217276421",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "842341634574557184",
  "text" : "A hero doing a slow walk from an explosion. That epic feeling... https:\/\/t.co\/EKwEtOttGl",
  "id" : 842341634574557184,
  "created_at" : "2017-03-16 11:47:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/842334496217276421\/photo\/1",
      "indices" : [ 135, 158 ],
      "url" : "https:\/\/t.co\/VrSjeYNg32",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C7CSka7WgAEymwf.jpg",
      "id_str" : "842334284719489025",
      "id" : 842334284719489025,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C7CSka7WgAEymwf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 298,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 169,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1341
      }, {
        "h" : 333,
        "resize" : "fit",
        "w" : 1341
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VrSjeYNg32"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842334496217276421",
  "text" : "Just help a client setup a daily automated pull of Organic search data to get around Google\u2019s current 90 day limit on historical data. https:\/\/t.co\/VrSjeYNg32",
  "id" : 842334496217276421,
  "created_at" : "2017-03-16 11:19:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Zille",
      "screen_name" : "helenzille",
      "indices" : [ 3, 14 ],
      "id_str" : "20521828",
      "id" : 20521828
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842331517548396544",
  "text" : "RT @helenzille: I think Singapore lessons are: 1) Meritocracy; 2) multiculturalism; 3) work ethic; 4) open to globalism; 4) English. 5) Fut\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842250631054258176",
    "text" : "I think Singapore lessons are: 1) Meritocracy; 2) multiculturalism; 3) work ethic; 4) open to globalism; 4) English. 5) Future orientation.",
    "id" : 842250631054258176,
    "created_at" : "2017-03-16 05:46:13 +0000",
    "user" : {
      "name" : "Helen Zille",
      "screen_name" : "helenzille",
      "protected" : false,
      "id_str" : "20521828",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946234192593027072\/BBax8T0A_normal.jpg",
      "id" : 20521828,
      "verified" : true
    }
  },
  "id" : 842331517548396544,
  "created_at" : "2017-03-16 11:07:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helen Zille",
      "screen_name" : "helenzille",
      "indices" : [ 3, 14 ],
      "id_str" : "20521828",
      "id" : 20521828
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842331488448335874",
  "text" : "RT @helenzille: Singapore had no natural resources and 50 years ago was poorer than most African countries. Now they soar. What are the les\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "842249938801197056",
    "text" : "Singapore had no natural resources and 50 years ago was poorer than most African countries. Now they soar. What are the lessons?",
    "id" : 842249938801197056,
    "created_at" : "2017-03-16 05:43:28 +0000",
    "user" : {
      "name" : "Helen Zille",
      "screen_name" : "helenzille",
      "protected" : false,
      "id_str" : "20521828",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946234192593027072\/BBax8T0A_normal.jpg",
      "id" : 20521828,
      "verified" : true
    }
  },
  "id" : 842331488448335874,
  "created_at" : "2017-03-16 11:07:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allie Quinn",
      "screen_name" : "AlliequinnRD",
      "indices" : [ 3, 16 ],
      "id_str" : "2436393437",
      "id" : 2436393437
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Spotlight",
      "indices" : [ 86, 96 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842138334885515264",
  "text" : "RT @AlliequinnRD: Excellent coverage on Northern Ireland's GP crisis on tonight's BBC #Spotlight",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Spotlight",
        "indices" : [ 68, 78 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841789656727453696",
    "text" : "Excellent coverage on Northern Ireland's GP crisis on tonight's BBC #Spotlight",
    "id" : 841789656727453696,
    "created_at" : "2017-03-14 23:14:28 +0000",
    "user" : {
      "name" : "Allie Quinn",
      "screen_name" : "AlliequinnRD",
      "protected" : false,
      "id_str" : "2436393437",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/793576591649566720\/Hb2HXgV7_normal.jpg",
      "id" : 2436393437,
      "verified" : false
    }
  },
  "id" : 842138334885515264,
  "created_at" : "2017-03-15 22:19:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Thomson Reuters",
      "screen_name" : "thomsonreuters",
      "indices" : [ 3, 18 ],
      "id_str" : "14412844",
      "id" : 14412844
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fintech",
      "indices" : [ 58, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/Ba5H8nm5RR",
      "expanded_url" : "http:\/\/tmsnrt.rs\/2mPTU7X",
      "display_url" : "tmsnrt.rs\/2mPTU7X"
    } ]
  },
  "geo" : { },
  "id_str" : "842137808747794433",
  "text" : "RT @thomsonreuters: Thomson Reuters Labs join Singapore's #fintech hub: https:\/\/t.co\/Ba5H8nm5RR",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/adobe.com\" rel=\"nofollow\"\u003EAdobe\u00AE Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "fintech",
        "indices" : [ 38, 46 ]
      } ],
      "urls" : [ {
        "indices" : [ 52, 75 ],
        "url" : "https:\/\/t.co\/Ba5H8nm5RR",
        "expanded_url" : "http:\/\/tmsnrt.rs\/2mPTU7X",
        "display_url" : "tmsnrt.rs\/2mPTU7X"
      } ]
    },
    "geo" : { },
    "id_str" : "842134321406255105",
    "text" : "Thomson Reuters Labs join Singapore's #fintech hub: https:\/\/t.co\/Ba5H8nm5RR",
    "id" : 842134321406255105,
    "created_at" : "2017-03-15 22:04:02 +0000",
    "user" : {
      "name" : "Thomson Reuters",
      "screen_name" : "thomsonreuters",
      "protected" : false,
      "id_str" : "14412844",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766626447880355840\/U60rKbrn_normal.jpg",
      "id" : 14412844,
      "verified" : true
    }
  },
  "id" : 842137808747794433,
  "created_at" : "2017-03-15 22:17:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842128557526548480",
  "text" : "Learned that \u201CJupyter\u201D notebook,a popular data science tools is actually short for \u201CJulia, Python and R\u201D",
  "id" : 842128557526548480,
  "created_at" : "2017-03-15 21:41:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "842055471838175233",
  "text" : "RT @ronanlyons: Every square metre of new office space (see those cranes!) needs about three square metres of residential space to accommod\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/AcuM5nBWLk",
        "expanded_url" : "https:\/\/twitter.com\/qz\/status\/841981270414483456",
        "display_url" : "twitter.com\/qz\/status\/8419\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841992609656647680",
    "text" : "Every square metre of new office space (see those cranes!) needs about three square metres of residential space to accommodate the workers. https:\/\/t.co\/AcuM5nBWLk",
    "id" : 841992609656647680,
    "created_at" : "2017-03-15 12:40:56 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 842055471838175233,
  "created_at" : "2017-03-15 16:50:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Bizexpo_ie",
      "indices" : [ 129, 140 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/z6SvniOHRs",
      "expanded_url" : "http:\/\/bit.ly\/2mznpJm",
      "display_url" : "bit.ly\/2mznpJm"
    } ]
  },
  "geo" : { },
  "id_str" : "841979795546247169",
  "text" : "A survey indicate over 90% Google Analytics set up are not fit for purpose. Sign up https:\/\/t.co\/z6SvniOHRs to get free checkup. #Bizexpo_ie",
  "id" : 841979795546247169,
  "created_at" : "2017-03-15 11:50:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft Business",
      "screen_name" : "MicrosoftIRLBiz",
      "indices" : [ 3, 19 ],
      "id_str" : "2205279110",
      "id" : 2205279110
    }, {
      "name" : "Samantha Kelly",
      "screen_name" : "Tweetinggoddess",
      "indices" : [ 46, 62 ],
      "id_str" : "326869253",
      "id" : 326869253
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SmallBiz",
      "indices" : [ 112, 121 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841963995494023168",
  "text" : "RT @MicrosoftIRLBiz: FREE webinar series with @Tweetinggoddess! Don't miss the chance to learn how to grow your #SmallBiz with Twitter: htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/prod2.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr Prod2\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Samantha Kelly",
        "screen_name" : "Tweetinggoddess",
        "indices" : [ 25, 41 ],
        "id_str" : "326869253",
        "id" : 326869253
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MicrosoftIRLBiz\/status\/841640186559762433\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/kJpIGU9U64",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C64bSckWYAEUfKd.jpg",
        "id_str" : "841640184085110785",
        "id" : 841640184085110785,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C64bSckWYAEUfKd.jpg",
        "sizes" : [ {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kJpIGU9U64"
      } ],
      "hashtags" : [ {
        "text" : "SmallBiz",
        "indices" : [ 91, 100 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/tXX6moM1Np",
        "expanded_url" : "http:\/\/msft.social\/aCkwx4",
        "display_url" : "msft.social\/aCkwx4"
      } ]
    },
    "geo" : { },
    "id_str" : "841640186559762433",
    "text" : "FREE webinar series with @Tweetinggoddess! Don't miss the chance to learn how to grow your #SmallBiz with Twitter: https:\/\/t.co\/tXX6moM1Np https:\/\/t.co\/kJpIGU9U64",
    "id" : 841640186559762433,
    "created_at" : "2017-03-14 13:20:32 +0000",
    "user" : {
      "name" : "Microsoft Business",
      "screen_name" : "MicrosoftIRLBiz",
      "protected" : false,
      "id_str" : "2205279110",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875745178308489216\/R_e3dTDb_normal.jpg",
      "id" : 2205279110,
      "verified" : true
    }
  },
  "id" : 841963995494023168,
  "created_at" : "2017-03-15 10:47:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/ckVlDn0xDU",
      "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/singapore\/singapore-films-like-ilo-ilo-and-mee-pok-man-available-for-the\/3591638.html#.WMkZz3fn2UA.twitter",
      "display_url" : "channelnewsasia.com\/news\/singapore\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841961992856428545",
  "text" : "Singapore films like Ilo Ilo and Mee Pok Man available for the first time on iTunes https:\/\/t.co\/ckVlDn0xDU",
  "id" : 841961992856428545,
  "created_at" : "2017-03-15 10:39:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/dBehJhTJvD",
      "expanded_url" : "https:\/\/coconuts.co\/singapore\/lifestyle\/new-co-working-space-takes-11-floors-cbd-building-alongside-pool-gym-sky-garden\/",
      "display_url" : "coconuts.co\/singapore\/life\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841961544044937216",
  "text" : "This new co-working space takes up 11 floors in a CBD building alongside a pool, gym and sky garden https:\/\/t.co\/dBehJhTJvD",
  "id" : 841961544044937216,
  "created_at" : "2017-03-15 10:37:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/JRUwaVgfi9",
      "expanded_url" : "http:\/\/www.dttas.ie\/sites\/default\/files\/publications\/maritime\/english\/irish-coast-guard-publications\/ircgchcsikorskys92-0.PDF",
      "display_url" : "dttas.ie\/sites\/default\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841910376501514241",
  "text" : "Facts and figures of Irish Coast Guard's Sikorsky S-92 helicopter https:\/\/t.co\/JRUwaVgfi9",
  "id" : 841910376501514241,
  "created_at" : "2017-03-15 07:14:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kyle Cranmer",
      "screen_name" : "KyleCranmer",
      "indices" : [ 3, 15 ],
      "id_str" : "1556664198",
      "id" : 1556664198
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841780053440663554",
  "text" : "RT @KyleCranmer: A little experiment with kids in honor of \u03C0 day\nSlope of line = circumference \/ diameter = \u03C0 \nDemonstrated with 3 kitchen\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KyleCranmer\/status\/841660038695534593\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/iXqnjlAKnM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C64tVweVwAAbEiE.jpg",
        "id_str" : "841660032177520640",
        "id" : 841660032177520640,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C64tVweVwAAbEiE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 852,
          "resize" : "fit",
          "w" : 1136
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 852,
          "resize" : "fit",
          "w" : 1136
        }, {
          "h" : 852,
          "resize" : "fit",
          "w" : 1136
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iXqnjlAKnM"
      } ],
      "hashtags" : [ {
        "text" : "PiDay",
        "indices" : [ 128, 134 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841660038695534593",
    "text" : "A little experiment with kids in honor of \u03C0 day\nSlope of line = circumference \/ diameter = \u03C0 \nDemonstrated with 3 kitchen items\n#PiDay https:\/\/t.co\/iXqnjlAKnM",
    "id" : 841660038695534593,
    "created_at" : "2017-03-14 14:39:25 +0000",
    "user" : {
      "name" : "Kyle Cranmer",
      "screen_name" : "KyleCranmer",
      "protected" : false,
      "id_str" : "1556664198",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000065241474\/e47527eed661a899d666329eb2774477_normal.jpeg",
      "id" : 1556664198,
      "verified" : false
    }
  },
  "id" : 841780053440663554,
  "created_at" : "2017-03-14 22:36:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/BMQGPp1MRB",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-39244325",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841749539145031681",
  "text" : "BBC News - Why did people assume an Asian woman in BBC viral video was the nanny? https:\/\/t.co\/BMQGPp1MRB",
  "id" : 841749539145031681,
  "created_at" : "2017-03-14 20:35:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Data Scientists IRL",
      "screen_name" : "DataSci_Ireland",
      "indices" : [ 3, 19 ],
      "id_str" : "2813354394",
      "id" : 2813354394
    }, {
      "name" : "SiliconRepublic",
      "screen_name" : "siliconrepublic",
      "indices" : [ 116, 132 ],
      "id_str" : "14385329",
      "id" : 14385329
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/CjbApYJpFc",
      "expanded_url" : "https:\/\/www.siliconrepublic.com\/enterprise\/amazon-data-centre-dublin",
      "display_url" : "siliconrepublic.com\/enterprise\/ama\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841729887580893184",
  "text" : "RT @DataSci_Ireland: City of the clouds: Amazon to build \u20AC1bn Dublin data centre campus https:\/\/t.co\/CjbApYJpFc via @siliconrepublic",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SiliconRepublic",
        "screen_name" : "siliconrepublic",
        "indices" : [ 95, 111 ],
        "id_str" : "14385329",
        "id" : 14385329
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/CjbApYJpFc",
        "expanded_url" : "https:\/\/www.siliconrepublic.com\/enterprise\/amazon-data-centre-dublin",
        "display_url" : "siliconrepublic.com\/enterprise\/ama\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841711655922876416",
    "text" : "City of the clouds: Amazon to build \u20AC1bn Dublin data centre campus https:\/\/t.co\/CjbApYJpFc via @siliconrepublic",
    "id" : 841711655922876416,
    "created_at" : "2017-03-14 18:04:31 +0000",
    "user" : {
      "name" : "Data Scientists IRL",
      "screen_name" : "DataSci_Ireland",
      "protected" : false,
      "id_str" : "2813354394",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/597764057836494849\/w2Hts-sb_normal.jpg",
      "id" : 2813354394,
      "verified" : false
    }
  },
  "id" : 841729887580893184,
  "created_at" : "2017-03-14 19:16:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/PfIxDVJt6Y",
      "expanded_url" : "http:\/\/publichouse.sg\/im-not-proud-of-this-project-philip-yeo-on-suzhou-industrial-park\/",
      "display_url" : "publichouse.sg\/im-not-proud-o\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841711566684839936",
  "text" : "RT @yapphenghui: \u201CI\u2019m not proud of this project\u201D: Philip Yeo on Suzhou Industrial Park https:\/\/t.co\/PfIxDVJt6Y",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/PfIxDVJt6Y",
        "expanded_url" : "http:\/\/publichouse.sg\/im-not-proud-of-this-project-philip-yeo-on-suzhou-industrial-park\/",
        "display_url" : "publichouse.sg\/im-not-proud-o\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841707590497124352",
    "text" : "\u201CI\u2019m not proud of this project\u201D: Philip Yeo on Suzhou Industrial Park https:\/\/t.co\/PfIxDVJt6Y",
    "id" : 841707590497124352,
    "created_at" : "2017-03-14 17:48:22 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 841711566684839936,
  "created_at" : "2017-03-14 18:04:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Xbox IE",
      "screen_name" : "XboxIE_Official",
      "indices" : [ 3, 19 ],
      "id_str" : "2805425886",
      "id" : 2805425886
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841700543600971776",
  "text" : "RT @XboxIE_Official: We're celebrating our birthday with a giveaway. RT for your chance to win a Red Xbox One Controller. (1\/2) #15YearsofX\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/XboxIE_Official\/status\/841695463787102212\/photo\/1",
        "indices" : [ 122, 145 ],
        "url" : "https:\/\/t.co\/YEO1y8841e",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C65Nj_rVwAA9Pb5.jpg",
        "id_str" : "841695461148835840",
        "id" : 841695461148835840,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C65Nj_rVwAA9Pb5.jpg",
        "sizes" : [ {
          "h" : 920,
          "resize" : "fit",
          "w" : 920
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 920,
          "resize" : "fit",
          "w" : 920
        }, {
          "h" : 920,
          "resize" : "fit",
          "w" : 920
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YEO1y8841e"
      } ],
      "hashtags" : [ {
        "text" : "15YearsofXbox",
        "indices" : [ 107, 121 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841695463787102212",
    "text" : "We're celebrating our birthday with a giveaway. RT for your chance to win a Red Xbox One Controller. (1\/2) #15YearsofXbox https:\/\/t.co\/YEO1y8841e",
    "id" : 841695463787102212,
    "created_at" : "2017-03-14 17:00:11 +0000",
    "user" : {
      "name" : "Xbox IE",
      "screen_name" : "XboxIE_Official",
      "protected" : false,
      "id_str" : "2805425886",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875709226894622722\/raDcMpws_normal.jpg",
      "id" : 2805425886,
      "verified" : true
    }
  },
  "id" : 841700543600971776,
  "created_at" : "2017-03-14 17:20:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Razan Ibraheem - \u0631\u0632\u0627\u0646",
      "screen_name" : "IbrahRazan",
      "indices" : [ 3, 14 ],
      "id_str" : "2880165111",
      "id" : 2880165111
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841688183364571137",
  "text" : "RT @IbrahRazan: Only in Saudi Arabia: the first meeting for Girls Council in Qassim region was held without Girls, without Women, without t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IbrahRazan\/status\/841677365843484673\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/Jh1nK7D1k5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C649FOAWsAAv9UE.jpg",
        "id_str" : "841677340233084928",
        "id" : 841677340233084928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C649FOAWsAAv9UE.jpg",
        "sizes" : [ {
          "h" : 389,
          "resize" : "fit",
          "w" : 579
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 579
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 579
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 579
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Jh1nK7D1k5"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IbrahRazan\/status\/841677365843484673\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/Jh1nK7D1k5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C649F1YWwAEs2Jh.jpg",
        "id_str" : "841677350802735105",
        "id" : 841677350802735105,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C649F1YWwAEs2Jh.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 398,
          "resize" : "fit",
          "w" : 656
        }, {
          "h" : 398,
          "resize" : "fit",
          "w" : 656
        }, {
          "h" : 398,
          "resize" : "fit",
          "w" : 656
        }, {
          "h" : 398,
          "resize" : "fit",
          "w" : 656
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Jh1nK7D1k5"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IbrahRazan\/status\/841677365843484673\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/Jh1nK7D1k5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C649GdeXAAAUMD-.jpg",
        "id_str" : "841677361565335552",
        "id" : 841677361565335552,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C649GdeXAAAUMD-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 324,
          "resize" : "fit",
          "w" : 862
        }, {
          "h" : 324,
          "resize" : "fit",
          "w" : 862
        }, {
          "h" : 256,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 324,
          "resize" : "fit",
          "w" : 862
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Jh1nK7D1k5"
      } ],
      "hashtags" : [ {
        "text" : "womensday",
        "indices" : [ 128, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841677365843484673",
    "text" : "Only in Saudi Arabia: the first meeting for Girls Council in Qassim region was held without Girls, without Women, without them. #womensday https:\/\/t.co\/Jh1nK7D1k5",
    "id" : 841677365843484673,
    "created_at" : "2017-03-14 15:48:16 +0000",
    "user" : {
      "name" : "Razan Ibraheem - \u0631\u0632\u0627\u0646",
      "screen_name" : "IbrahRazan",
      "protected" : false,
      "id_str" : "2880165111",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/915980607267770368\/vS_muK6A_normal.jpg",
      "id" : 2880165111,
      "verified" : true
    }
  },
  "id" : 841688183364571137,
  "created_at" : "2017-03-14 16:31:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Snap Ireland",
      "screen_name" : "SnapIreland",
      "indices" : [ 3, 15 ],
      "id_str" : "867740762",
      "id" : 867740762
    }, {
      "name" : "All-Ireland Summit",
      "screen_name" : "All_IreSummit",
      "indices" : [ 55, 69 ],
      "id_str" : "2910755951",
      "id" : 2910755951
    }, {
      "name" : "Croke Park",
      "screen_name" : "CrokePark",
      "indices" : [ 70, 80 ],
      "id_str" : "193303801",
      "id" : 193303801
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TalktoSnap",
      "indices" : [ 104, 115 ]
    }, {
      "text" : "Pitchforprogress",
      "indices" : [ 116, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841677592449163264",
  "text" : "RT @SnapIreland: Step right up!  Be in to win a ticket @All_IreSummit @CrokePark RT + FOLLOW by 24March #TalktoSnap #Pitchforprogress https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "All-Ireland Summit",
        "screen_name" : "All_IreSummit",
        "indices" : [ 38, 52 ],
        "id_str" : "2910755951",
        "id" : 2910755951
      }, {
        "name" : "Croke Park",
        "screen_name" : "CrokePark",
        "indices" : [ 53, 63 ],
        "id_str" : "193303801",
        "id" : 193303801
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SnapIreland\/status\/840181023749099520\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/mePBmyZidZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6jrTjaWcAESlnk.png",
        "id_str" : "840180051660402689",
        "id" : 840180051660402689,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6jrTjaWcAESlnk.png",
        "sizes" : [ {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mePBmyZidZ"
      } ],
      "hashtags" : [ {
        "text" : "TalktoSnap",
        "indices" : [ 87, 98 ]
      }, {
        "text" : "Pitchforprogress",
        "indices" : [ 99, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/WotuQ5SVyE",
        "expanded_url" : "http:\/\/bit.ly\/SNAPAllIreland",
        "display_url" : "bit.ly\/SNAPAllIreland"
      } ]
    },
    "geo" : { },
    "id_str" : "840181023749099520",
    "text" : "Step right up!  Be in to win a ticket @All_IreSummit @CrokePark RT + FOLLOW by 24March #TalktoSnap #Pitchforprogress https:\/\/t.co\/WotuQ5SVyE https:\/\/t.co\/mePBmyZidZ",
    "id" : 840181023749099520,
    "created_at" : "2017-03-10 12:42:20 +0000",
    "user" : {
      "name" : "Snap Ireland",
      "screen_name" : "SnapIreland",
      "protected" : false,
      "id_str" : "867740762",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/707894486861484032\/kmg4E76l_normal.jpg",
      "id" : 867740762,
      "verified" : false
    }
  },
  "id" : 841677592449163264,
  "created_at" : "2017-03-14 15:49:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/0X7HOajT3G",
      "expanded_url" : "https:\/\/shar.es\/1Uy4Hp",
      "display_url" : "shar.es\/1Uy4Hp"
    } ]
  },
  "geo" : { },
  "id_str" : "841674167225331713",
  "text" : "RT @edwinksl: Singapore not an ideal test-bed for global innovation https:\/\/t.co\/0X7HOajT3G",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/0X7HOajT3G",
        "expanded_url" : "https:\/\/shar.es\/1Uy4Hp",
        "display_url" : "shar.es\/1Uy4Hp"
      } ]
    },
    "geo" : { },
    "id_str" : "841673532585197570",
    "text" : "Singapore not an ideal test-bed for global innovation https:\/\/t.co\/0X7HOajT3G",
    "id" : 841673532585197570,
    "created_at" : "2017-03-14 15:33:02 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 841674167225331713,
  "created_at" : "2017-03-14 15:35:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "don lavery",
      "screen_name" : "donlav",
      "indices" : [ 3, 10 ],
      "id_str" : "139837687",
      "id" : 139837687
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841663104094212098",
  "text" : "RT @donlav: Ireland needs more Maritime Patrol Aircraft. Two is not enough for  top cover for Coastguard, patrolling, air ambulance, and tr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841634667895873536",
    "text" : "Ireland needs more Maritime Patrol Aircraft. Two is not enough for  top cover for Coastguard, patrolling, air ambulance, and transport.",
    "id" : 841634667895873536,
    "created_at" : "2017-03-14 12:58:36 +0000",
    "user" : {
      "name" : "don lavery",
      "screen_name" : "donlav",
      "protected" : false,
      "id_str" : "139837687",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1029069440179228672\/PLMGNnPk_normal.jpg",
      "id" : 139837687,
      "verified" : false
    }
  },
  "id" : 841663104094212098,
  "created_at" : "2017-03-14 14:51:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/841612598630055936\/photo\/1",
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/B1WBlVpCuW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C64CMTKWkAAX5EU.jpg",
      "id_str" : "841612590690242560",
      "id" : 841612590690242560,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C64CMTKWkAAX5EU.jpg",
      "sizes" : [ {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/B1WBlVpCuW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841612598630055936",
  "text" : "Employees who used Firefox or Chrome remained in their jobs 15% longer than those who used IE &amp; Safari. (Pg4) https:\/\/t.co\/B1WBlVpCuW",
  "id" : 841612598630055936,
  "created_at" : "2017-03-14 11:30:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/NkRmLNuy76",
      "expanded_url" : "https:\/\/twitter.com\/DanielJHannan\/status\/841294105116364800",
      "display_url" : "twitter.com\/DanielJHannan\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841582764013023232",
  "text" : "A dream of liberty from Great Britain. Some of these territories were lost during WW2. A wake up call. https:\/\/t.co\/NkRmLNuy76",
  "id" : 841582764013023232,
  "created_at" : "2017-03-14 09:32:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GDS Hive",
      "screen_name" : "GDSHive",
      "indices" : [ 3, 11 ],
      "id_str" : "3743066294",
      "id" : 3743066294
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GDSHive\/status\/841580306339569664\/photo\/1",
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/FMOmPjVdJF",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/C63k0dTVAAEL7_4.jpg",
      "id_str" : "841580295258177537",
      "id" : 841580295258177537,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/C63k0dTVAAEL7_4.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 412
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 412
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 412
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 412
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FMOmPjVdJF"
    } ],
    "hashtags" : [ {
      "text" : "PiDay",
      "indices" : [ 33, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841581582037180416",
  "text" : "RT @GDSHive: Happy \u03C0 day, folks! #PiDay https:\/\/t.co\/FMOmPjVdJF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GDSHive\/status\/841580306339569664\/photo\/1",
        "indices" : [ 27, 50 ],
        "url" : "https:\/\/t.co\/FMOmPjVdJF",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/C63k0dTVAAEL7_4.jpg",
        "id_str" : "841580295258177537",
        "id" : 841580295258177537,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/C63k0dTVAAEL7_4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 412
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 412
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 412
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 412
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FMOmPjVdJF"
      } ],
      "hashtags" : [ {
        "text" : "PiDay",
        "indices" : [ 20, 26 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841580306339569664",
    "text" : "Happy \u03C0 day, folks! #PiDay https:\/\/t.co\/FMOmPjVdJF",
    "id" : 841580306339569664,
    "created_at" : "2017-03-14 09:22:35 +0000",
    "user" : {
      "name" : "GDS Hive",
      "screen_name" : "GDSHive",
      "protected" : false,
      "id_str" : "3743066294",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/649411335051083776\/fP5iPRNM_normal.png",
      "id" : 3743066294,
      "verified" : false
    }
  },
  "id" : 841581582037180416,
  "created_at" : "2017-03-14 09:27:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u2022 just jeremy \u2022",
      "screen_name" : "jergarl",
      "indices" : [ 3, 11 ],
      "id_str" : "941704664",
      "id" : 941704664
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841561787636031488",
  "text" : "RT @jergarl: My wife says I can join your gang but I have to be home by 9.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "817529971640070144",
    "text" : "My wife says I can join your gang but I have to be home by 9.",
    "id" : 817529971640070144,
    "created_at" : "2017-01-07 00:35:08 +0000",
    "user" : {
      "name" : "\u2022 just jeremy \u2022",
      "screen_name" : "jergarl",
      "protected" : false,
      "id_str" : "941704664",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1003647963388755968\/Se860yIS_normal.jpg",
      "id" : 941704664,
      "verified" : false
    }
  },
  "id" : 841561787636031488,
  "created_at" : "2017-03-14 08:09:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AUDA Property Buyers",
      "screen_name" : "AUDAProperties",
      "indices" : [ 3, 18 ],
      "id_str" : "776654212339937280",
      "id" : 776654212339937280
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "survey",
      "indices" : [ 111, 118 ]
    }, {
      "text" : "investment",
      "indices" : [ 119, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841560753647820801",
  "text" : "RT @AUDAProperties: Take 2: When is the best time to teach children how to invest for their financial freedom?\n#survey #investment #financi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "survey",
        "indices" : [ 91, 98 ]
      }, {
        "text" : "investment",
        "indices" : [ 99, 110 ]
      }, {
        "text" : "financial",
        "indices" : [ 111, 121 ]
      }, {
        "text" : "education",
        "indices" : [ 123, 133 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841522513918140417",
    "text" : "Take 2: When is the best time to teach children how to invest for their financial freedom?\n#survey #investment #financial  #education",
    "id" : 841522513918140417,
    "created_at" : "2017-03-14 05:32:56 +0000",
    "user" : {
      "name" : "AUDA Property Buyers",
      "screen_name" : "AUDAProperties",
      "protected" : false,
      "id_str" : "776654212339937280",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/776656822488797185\/m70bRKz2_normal.jpg",
      "id" : 776654212339937280,
      "verified" : false
    }
  },
  "id" : 841560753647820801,
  "created_at" : "2017-03-14 08:04:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/841546890365792256\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/vmGoiykpML",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C63GbpUWgAAllcm.jpg",
      "id_str" : "841546883638132736",
      "id" : 841546883638132736,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C63GbpUWgAAllcm.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 382
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vmGoiykpML"
    } ],
    "hashtags" : [ {
      "text" : "rescue116",
      "indices" : [ 44, 54 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841560516204085248",
  "text" : "RT @EimearMcCormack: LeRoisin en route now. #rescue116 https:\/\/t.co\/vmGoiykpML",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/841546890365792256\/photo\/1",
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/vmGoiykpML",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C63GbpUWgAAllcm.jpg",
        "id_str" : "841546883638132736",
        "id" : 841546883638132736,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C63GbpUWgAAllcm.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 382
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vmGoiykpML"
      } ],
      "hashtags" : [ {
        "text" : "rescue116",
        "indices" : [ 23, 33 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841546890365792256",
    "text" : "LeRoisin en route now. #rescue116 https:\/\/t.co\/vmGoiykpML",
    "id" : 841546890365792256,
    "created_at" : "2017-03-14 07:09:48 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 841560516204085248,
  "created_at" : "2017-03-14 08:03:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/841554735807238144\/photo\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/yhNe2kCjvB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C63NkWWXAAExjcy.jpg",
      "id_str" : "841554729746497537",
      "id" : 841554729746497537,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C63NkWWXAAExjcy.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 382
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yhNe2kCjvB"
    } ],
    "hashtags" : [ {
      "text" : "Rescue116",
      "indices" : [ 70, 80 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841560346687090694",
  "text" : "RT @EimearMcCormack: 7 vessels now searching with Le Roisin en route. #Rescue116 https:\/\/t.co\/yhNe2kCjvB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EimearMcCormack\/status\/841554735807238144\/photo\/1",
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/yhNe2kCjvB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C63NkWWXAAExjcy.jpg",
        "id_str" : "841554729746497537",
        "id" : 841554729746497537,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C63NkWWXAAExjcy.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 382
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yhNe2kCjvB"
      } ],
      "hashtags" : [ {
        "text" : "Rescue116",
        "indices" : [ 49, 59 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841554735807238144",
    "text" : "7 vessels now searching with Le Roisin en route. #Rescue116 https:\/\/t.co\/yhNe2kCjvB",
    "id" : 841554735807238144,
    "created_at" : "2017-03-14 07:40:59 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 841560346687090694,
  "created_at" : "2017-03-14 08:03:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sundar Pichai",
      "screen_name" : "sundarpichai",
      "indices" : [ 3, 16 ],
      "id_str" : "14130366",
      "id" : 14130366
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Holi",
      "indices" : [ 48, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/37IuUAstU1",
      "expanded_url" : "https:\/\/twitter.com\/Google\/status\/841326990636568576",
      "display_url" : "twitter.com\/Google\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841560022471573506",
  "text" : "RT @sundarpichai: Wishing everyone a very happy #Holi! https:\/\/t.co\/37IuUAstU1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Holi",
        "indices" : [ 30, 35 ]
      } ],
      "urls" : [ {
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/37IuUAstU1",
        "expanded_url" : "https:\/\/twitter.com\/Google\/status\/841326990636568576",
        "display_url" : "twitter.com\/Google\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841434921113546753",
    "text" : "Wishing everyone a very happy #Holi! https:\/\/t.co\/37IuUAstU1",
    "id" : 841434921113546753,
    "created_at" : "2017-03-13 23:44:52 +0000",
    "user" : {
      "name" : "Sundar Pichai",
      "screen_name" : "sundarpichai",
      "protected" : false,
      "id_str" : "14130366",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/864282616597405701\/M-FEJMZ0_normal.jpg",
      "id" : 14130366,
      "verified" : true
    }
  },
  "id" : 841560022471573506,
  "created_at" : "2017-03-14 08:01:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SARFamily",
      "indices" : [ 105, 115 ]
    }, {
      "text" : "Rescue116",
      "indices" : [ 116, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841557673011269632",
  "text" : "RT @ardrossan02: Terrible news coming from across the water. Thoughts and prayers with everyone involved #SARFamily #Rescue116 \n\nhttps:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SARFamily",
        "indices" : [ 88, 98 ]
      }, {
        "text" : "Rescue116",
        "indices" : [ 99, 109 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/2bWp8k848e",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/uk-northern-ireland-39264233",
        "display_url" : "bbc.co.uk\/news\/uk-northe\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841557018246803456",
    "text" : "Terrible news coming from across the water. Thoughts and prayers with everyone involved #SARFamily #Rescue116 \n\nhttps:\/\/t.co\/2bWp8k848e",
    "id" : 841557018246803456,
    "created_at" : "2017-03-14 07:50:03 +0000",
    "user" : {
      "name" : "Kevin Paterson \uD83D\uDE94\uD83D\uDE81",
      "screen_name" : "CoastguardKP",
      "protected" : false,
      "id_str" : "2210430895",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/996138750425083905\/E9Pk1KV2_normal.jpg",
      "id" : 2210430895,
      "verified" : false
    }
  },
  "id" : 841557673011269632,
  "created_at" : "2017-03-14 07:52:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Trinity College Dublin Global",
      "screen_name" : "tcdglobal",
      "indices" : [ 3, 13 ],
      "id_str" : "633723520",
      "id" : 633723520
    }, {
      "name" : "Trinity College Dublin",
      "screen_name" : "tcddublin",
      "indices" : [ 111, 121 ],
      "id_str" : "31537951",
      "id" : 31537951
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Holi2017",
      "indices" : [ 59, 68 ]
    }, {
      "text" : "Holi",
      "indices" : [ 82, 87 ]
    }, {
      "text" : "HoliFestivalOfColours",
      "indices" : [ 88, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841422126821302272",
  "text" : "RT @tcdglobal: We took this beautiful picture today of our #Holi2017 celebrations #Holi #HoliFestivalOfColours @tcddublin https:\/\/t.co\/cTJQ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Trinity College Dublin",
        "screen_name" : "tcddublin",
        "indices" : [ 96, 106 ],
        "id_str" : "31537951",
        "id" : 31537951
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tcdglobal\/status\/841374263638654981\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/cTJQafaMhU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C60pab1XUAAGLVy.jpg",
        "id_str" : "841374239513071616",
        "id" : 841374239513071616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C60pab1XUAAGLVy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cTJQafaMhU"
      } ],
      "hashtags" : [ {
        "text" : "Holi2017",
        "indices" : [ 44, 53 ]
      }, {
        "text" : "Holi",
        "indices" : [ 67, 72 ]
      }, {
        "text" : "HoliFestivalOfColours",
        "indices" : [ 73, 95 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841374263638654981",
    "text" : "We took this beautiful picture today of our #Holi2017 celebrations #Holi #HoliFestivalOfColours @tcddublin https:\/\/t.co\/cTJQafaMhU",
    "id" : 841374263638654981,
    "created_at" : "2017-03-13 19:43:51 +0000",
    "user" : {
      "name" : "Trinity College Dublin Global",
      "screen_name" : "tcdglobal",
      "protected" : false,
      "id_str" : "633723520",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/907977677075881984\/x9Num5ac_normal.jpg",
      "id" : 633723520,
      "verified" : false
    }
  },
  "id" : 841422126821302272,
  "created_at" : "2017-03-13 22:54:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CitySwifter",
      "screen_name" : "CitySwifter",
      "indices" : [ 3, 15 ],
      "id_str" : "4010872769",
      "id" : 4010872769
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/PopMhKmuTQ",
      "expanded_url" : "http:\/\/www.leitrimobserver.ie\/news\/local-news\/239763\/new-bus-service-for-north-leitrim.html",
      "display_url" : "leitrimobserver.ie\/news\/local-new\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841362196965789697",
  "text" : "RT @CitySwifter: Rural Ireland needs more of this: \"A demand-led rural service\" https:\/\/t.co\/PopMhKmuTQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/PopMhKmuTQ",
        "expanded_url" : "http:\/\/www.leitrimobserver.ie\/news\/local-news\/239763\/new-bus-service-for-north-leitrim.html",
        "display_url" : "leitrimobserver.ie\/news\/local-new\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841361771373965312",
    "text" : "Rural Ireland needs more of this: \"A demand-led rural service\" https:\/\/t.co\/PopMhKmuTQ",
    "id" : 841361771373965312,
    "created_at" : "2017-03-13 18:54:12 +0000",
    "user" : {
      "name" : "CitySwifter",
      "screen_name" : "CitySwifter",
      "protected" : false,
      "id_str" : "4010872769",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/772485585428746240\/BmcFz2Rc_normal.jpg",
      "id" : 4010872769,
      "verified" : false
    }
  },
  "id" : 841362196965789697,
  "created_at" : "2017-03-13 18:55:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/aYmagYlIzy",
      "expanded_url" : "https:\/\/www.coursera.org\/learn\/python-data-analysis\/lecture\/08sf6\/merging-dataframes",
      "display_url" : "coursera.org\/learn\/python-d\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841355791688966152",
  "text" : "Essential work process for data analyst. Merging Dataframes with Python. https:\/\/t.co\/aYmagYlIzy",
  "id" : 841355791688966152,
  "created_at" : "2017-03-13 18:30:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "airbnb",
      "indices" : [ 29, 36 ]
    }, {
      "text" : "nightat",
      "indices" : [ 37, 45 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/z8A0IVGCGe",
      "expanded_url" : "https:\/\/www.airbnb.ie\/night-at\/guinnessstorehouse?bev_ref=1489428400_godwheYFWwNpsMwZ&s=4",
      "display_url" : "airbnb.ie\/night-at\/guinn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841350906688000003",
  "text" : "Amazing! Guinness Storehouse #airbnb #nightat https:\/\/t.co\/z8A0IVGCGe",
  "id" : 841350906688000003,
  "created_at" : "2017-03-13 18:11:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/IDf7ONhffA",
      "expanded_url" : "http:\/\/www.zaobao.com.sg\/znews\/singapore\/story20170313-735148",
      "display_url" : "zaobao.com.sg\/znews\/singapor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841347421921804288",
  "text" : "\u62B9\u5730\u4E4B\u6C34\u5929\u4E0A\u6765 \u7EC4\u5C4B\u7701\u6C34\u8FBE\u4EBA\u4F38\u201C\u7BA1\u201D\u96C6\u96E8\u6C34 https:\/\/t.co\/IDf7ONhffA | She used rain water resulting a water bill of SGD 10 (6.63 Euro)",
  "id" : 841347421921804288,
  "created_at" : "2017-03-13 17:57:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "science",
      "indices" : [ 95, 103 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/56nQJ3MUJ2",
      "expanded_url" : "https:\/\/www.scientificamerican.com\/article\/can-facebooks-machine-learning-algorithms-accurately-predict-suicide\/?wt.mc=SA_Twitter-Share",
      "display_url" : "scientificamerican.com\/article\/can-fa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841328075593912321",
  "text" : "Can Facebook's Machine-Learning Algorithms Accurately Predict Suicide? https:\/\/t.co\/56nQJ3MUJ2 #science",
  "id" : 841328075593912321,
  "created_at" : "2017-03-13 16:40:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jill O'Herlihy",
      "screen_name" : "jilloherlihy",
      "indices" : [ 3, 16 ],
      "id_str" : "19965748",
      "id" : 19965748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841304967688400897",
  "text" : "RT @jilloherlihy: Anyone know anyone who can donate\/ sponsor 100 sandwiches\/wraps for a student mental health event in Dublin on 29 March?\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jilloherlihy\/status\/841302111073796096\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/hhqIpOHhYB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6znowmWwAISIgO.jpg",
        "id_str" : "841301917838000130",
        "id" : 841301917838000130,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6znowmWwAISIgO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 194,
          "resize" : "fit",
          "w" : 259
        }, {
          "h" : 194,
          "resize" : "fit",
          "w" : 259
        }, {
          "h" : 194,
          "resize" : "fit",
          "w" : 259
        }, {
          "h" : 194,
          "resize" : "fit",
          "w" : 259
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hhqIpOHhYB"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "841302111073796096",
    "text" : "Anyone know anyone who can donate\/ sponsor 100 sandwiches\/wraps for a student mental health event in Dublin on 29 March? https:\/\/t.co\/hhqIpOHhYB",
    "id" : 841302111073796096,
    "created_at" : "2017-03-13 14:57:08 +0000",
    "user" : {
      "name" : "Jill O'Herlihy",
      "screen_name" : "jilloherlihy",
      "protected" : false,
      "id_str" : "19965748",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1020632352106647552\/vLWpEBE9_normal.jpg",
      "id" : 19965748,
      "verified" : false
    }
  },
  "id" : 841304967688400897,
  "created_at" : "2017-03-13 15:08:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/egWY9ifyDz",
      "expanded_url" : "http:\/\/getoptimise.com\/blog\/business\/resources-for-growing-company\/",
      "display_url" : "getoptimise.com\/blog\/business\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841284165848903680",
  "text" : "RT @getoptimise: Updated \"Resources for Growing Business\" page with a new section \"Giveaway\" https:\/\/t.co\/egWY9ifyDz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/egWY9ifyDz",
        "expanded_url" : "http:\/\/getoptimise.com\/blog\/business\/resources-for-growing-company\/",
        "display_url" : "getoptimise.com\/blog\/business\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "841269937872211968",
    "text" : "Updated \"Resources for Growing Business\" page with a new section \"Giveaway\" https:\/\/t.co\/egWY9ifyDz",
    "id" : 841269937872211968,
    "created_at" : "2017-03-13 12:49:17 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 841284165848903680,
  "created_at" : "2017-03-13 13:45:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicolette Moore",
      "screen_name" : "MLM_Success4U",
      "indices" : [ 0, 14 ],
      "id_str" : "790354430293966848",
      "id" : 790354430293966848
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/bCbI8BI2uQ",
      "expanded_url" : "https:\/\/www.amazon.com\/gp\/help\/customer\/display.html?nodeId=201723200",
      "display_url" : "amazon.com\/gp\/help\/custom\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "839428120474234881",
  "geo" : { },
  "id_str" : "841263357709672448",
  "in_reply_to_user_id" : 790354430293966848,
  "text" : "@MLM_Success4U Amazon allows you to host a giveaway https:\/\/t.co\/bCbI8BI2uQ",
  "id" : 841263357709672448,
  "in_reply_to_status_id" : 839428120474234881,
  "created_at" : "2017-03-13 12:23:09 +0000",
  "in_reply_to_screen_name" : "MLM_Success4U",
  "in_reply_to_user_id_str" : "790354430293966848",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/yZihZnHWPE",
      "expanded_url" : "http:\/\/www.slate.com\/articles\/news_and_politics\/the_enlightened_manager\/2013\/12\/the_taxi_meter_effect_why_do_consumers_hate_paying_by_the_mile_or_the_minute.html",
      "display_url" : "slate.com\/articles\/news_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841232252835123200",
  "text" : "Businesses that charges per usage might risks producing a taxi-meter effect.https:\/\/t.co\/yZihZnHWPE",
  "id" : 841232252835123200,
  "created_at" : "2017-03-13 10:19:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "beloved comedy institution \u201Cthe pixelated boat\u201D",
      "screen_name" : "pixelatedboat",
      "indices" : [ 3, 17 ],
      "id_str" : "2999158160",
      "id" : 2999158160
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pixelatedboat\/status\/840786233328459777\/photo\/1",
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/b2eDcd2SkQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6sSmwdVAAADh0l.jpg",
      "id_str" : "840786212486971392",
      "id" : 840786212486971392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6sSmwdVAAADh0l.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 638,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 578,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 638,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 638,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/b2eDcd2SkQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841196054565535744",
  "text" : "RT @pixelatedboat: Libertarians are literally the dumbest people in the world https:\/\/t.co\/b2eDcd2SkQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pixelatedboat\/status\/840786233328459777\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/b2eDcd2SkQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6sSmwdVAAADh0l.jpg",
        "id_str" : "840786212486971392",
        "id" : 840786212486971392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6sSmwdVAAADh0l.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 578,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/b2eDcd2SkQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840786233328459777",
    "text" : "Libertarians are literally the dumbest people in the world https:\/\/t.co\/b2eDcd2SkQ",
    "id" : 840786233328459777,
    "created_at" : "2017-03-12 04:47:13 +0000",
    "user" : {
      "name" : "beloved comedy institution \u201Cthe pixelated boat\u201D",
      "screen_name" : "pixelatedboat",
      "protected" : false,
      "id_str" : "2999158160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875489341291675649\/hc8K1aT0_normal.jpg",
      "id" : 2999158160,
      "verified" : false
    }
  },
  "id" : 841196054565535744,
  "created_at" : "2017-03-13 07:55:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/5BWeGcMYMY",
      "expanded_url" : "http:\/\/www.todayonline.com\/singapore\/three-door-bus-hits-roads-six-month-trial",
      "display_url" : "todayonline.com\/singapore\/thre\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841195609491177472",
  "text" : "Three-door bus hits the roads in six-month trial https:\/\/t.co\/5BWeGcMYMY",
  "id" : 841195609491177472,
  "created_at" : "2017-03-13 07:53:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "indices" : [ 3, 16 ],
      "id_str" : "20192882",
      "id" : 20192882
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/zUyWYHqneB",
      "expanded_url" : "http:\/\/bit.ly\/2mxVR94",
      "display_url" : "bit.ly\/2mxVR94"
    } ]
  },
  "geo" : { },
  "id_str" : "841100605791842304",
  "text" : "RT @mikewhitmore: Finding your peer group: The single biggest boost your career can experience https:\/\/t.co\/zUyWYHqneB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/zUyWYHqneB",
        "expanded_url" : "http:\/\/bit.ly\/2mxVR94",
        "display_url" : "bit.ly\/2mxVR94"
      } ]
    },
    "geo" : { },
    "id_str" : "841078604847022080",
    "text" : "Finding your peer group: The single biggest boost your career can experience https:\/\/t.co\/zUyWYHqneB",
    "id" : 841078604847022080,
    "created_at" : "2017-03-13 00:09:00 +0000",
    "user" : {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "protected" : false,
      "id_str" : "20192882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/814556566179352577\/J3-2QBfu_normal.jpg",
      "id" : 20192882,
      "verified" : false
    }
  },
  "id" : 841100605791842304,
  "created_at" : "2017-03-13 01:36:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sam Ventura",
      "screen_name" : "stat_sam",
      "indices" : [ 3, 12 ],
      "id_str" : "113153829",
      "id" : 113153829
    }, {
      "name" : "DOCTOR Greg",
      "screen_name" : "StatsInTheWild",
      "indices" : [ 108, 123 ],
      "id_str" : "24096463",
      "id" : 24096463
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "841057098779766785",
  "text" : "RT @stat_sam: \"Simple models built on good input data will outperform complex models built on bad data.\" -- @StatsInTheWild to my statistic\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DOCTOR Greg",
        "screen_name" : "StatsInTheWild",
        "indices" : [ 94, 109 ],
        "id_str" : "24096463",
        "id" : 24096463
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839888832371769344",
    "text" : "\"Simple models built on good input data will outperform complex models built on bad data.\" -- @StatsInTheWild to my statistics seminar class",
    "id" : 839888832371769344,
    "created_at" : "2017-03-09 17:21:16 +0000",
    "user" : {
      "name" : "Sam Ventura",
      "screen_name" : "stat_sam",
      "protected" : false,
      "id_str" : "113153829",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1221203036\/myname_normal.jpg",
      "id" : 113153829,
      "verified" : false
    }
  },
  "id" : 841057098779766785,
  "created_at" : "2017-03-12 22:43:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/xYLc0FXVlh",
      "expanded_url" : "https:\/\/www.pri.org\/stories\/2014-04-16\/what-harvard-learned-studying-indias-lunchbox-delivery-system",
      "display_url" : "pri.org\/stories\/2014-0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841054797633638401",
  "text" : "Now on BBC4 The Lunch Box https:\/\/t.co\/xYLc0FXVlh",
  "id" : 841054797633638401,
  "created_at" : "2017-03-12 22:34:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/C28lL6rSdQ",
      "expanded_url" : "https:\/\/www.cloudera.com\/content\/dam\/www\/static\/documents\/analyst-reports\/idc-futurescape.pdf",
      "display_url" : "cloudera.com\/content\/dam\/ww\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "841022963335999488",
  "text" : "Enterprises moving their analytics workloads to the cloud https:\/\/t.co\/C28lL6rSdQ",
  "id" : 841022963335999488,
  "created_at" : "2017-03-12 20:27:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eleanor McKenna",
      "screen_name" : "onepixelout",
      "indices" : [ 0, 12 ],
      "id_str" : "40858592",
      "id" : 40858592
    }, {
      "name" : "Brian Gough",
      "screen_name" : "brian_gough",
      "indices" : [ 13, 25 ],
      "id_str" : "28535021",
      "id" : 28535021
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "840636400466833409",
  "geo" : { },
  "id_str" : "840965349650628608",
  "in_reply_to_user_id" : 40858592,
  "text" : "@onepixelout @brian_gough going by the ad I think the organisation is @Connectors_",
  "id" : 840965349650628608,
  "in_reply_to_status_id" : 840636400466833409,
  "created_at" : "2017-03-12 16:38:58 +0000",
  "in_reply_to_screen_name" : "onepixelout",
  "in_reply_to_user_id_str" : "40858592",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/4iHUZwjUjF",
      "expanded_url" : "https:\/\/twitter.com\/intercom\/status\/839592071937814539",
      "display_url" : "twitter.com\/intercom\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840963532325834753",
  "text" : "Honouring them by giving them an off day on IWD. https:\/\/t.co\/4iHUZwjUjF",
  "id" : 840963532325834753,
  "created_at" : "2017-03-12 16:31:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Pierotti",
      "screen_name" : "paul73p",
      "indices" : [ 3, 11 ],
      "id_str" : "298424747",
      "id" : 298424747
    }, {
      "name" : "The Boston Globe",
      "screen_name" : "BostonGlobe",
      "indices" : [ 125, 137 ],
      "id_str" : "95431448",
      "id" : 95431448
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/zTl6XDwpBr",
      "expanded_url" : "http:\/\/www.bostonglobe.com\/magazine\/2017\/03\/09\/the-biggest-threat-facing-middle-age-men-isn-smoking-obesity-loneliness\/k6saC9FnnHQCUbf5mJ8okL\/story.html?event=event25",
      "display_url" : "bostonglobe.com\/magazine\/2017\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840887889064005633",
  "text" : "RT @paul73p: The biggest threat facing middle-age men isn\u2019t smoking or obesity. It\u2019s loneliness\n\nhttps:\/\/t.co\/zTl6XDwpBr via @BostonGlobe",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Boston Globe",
        "screen_name" : "BostonGlobe",
        "indices" : [ 112, 124 ],
        "id_str" : "95431448",
        "id" : 95431448
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/zTl6XDwpBr",
        "expanded_url" : "http:\/\/www.bostonglobe.com\/magazine\/2017\/03\/09\/the-biggest-threat-facing-middle-age-men-isn-smoking-obesity-loneliness\/k6saC9FnnHQCUbf5mJ8okL\/story.html?event=event25",
        "display_url" : "bostonglobe.com\/magazine\/2017\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "840887288930394112",
    "text" : "The biggest threat facing middle-age men isn\u2019t smoking or obesity. It\u2019s loneliness\n\nhttps:\/\/t.co\/zTl6XDwpBr via @BostonGlobe",
    "id" : 840887288930394112,
    "created_at" : "2017-03-12 11:28:47 +0000",
    "user" : {
      "name" : "Paul Pierotti",
      "screen_name" : "paul73p",
      "protected" : false,
      "id_str" : "298424747",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/462206785634304000\/gOFQRSAc_normal.jpeg",
      "id" : 298424747,
      "verified" : false
    }
  },
  "id" : 840887889064005633,
  "created_at" : "2017-03-12 11:31:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ian Birrell",
      "screen_name" : "ianbirrell",
      "indices" : [ 3, 14 ],
      "id_str" : "142230413",
      "id" : 142230413
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840881362504699906",
  "text" : "RT @ianbirrell: Stat of the day: 160 container ships create same pollution as all the cars in world. There are 6000 such ships at sea https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ianbirrell\/status\/789035433141428224\/photo\/1",
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/ke2KD1h3UR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CvM3i1kWcAEoV9P.jpg",
        "id_str" : "789035431354593281",
        "id" : 789035431354593281,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CvM3i1kWcAEoV9P.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ke2KD1h3UR"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "789035433141428224",
    "text" : "Stat of the day: 160 container ships create same pollution as all the cars in world. There are 6000 such ships at sea https:\/\/t.co\/ke2KD1h3UR",
    "id" : 789035433141428224,
    "created_at" : "2016-10-20 09:28:01 +0000",
    "user" : {
      "name" : "Ian Birrell",
      "screen_name" : "ianbirrell",
      "protected" : false,
      "id_str" : "142230413",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/509659002699923456\/6ybSVCW__normal.png",
      "id" : 142230413,
      "verified" : false
    }
  },
  "id" : 840881362504699906,
  "created_at" : "2017-03-12 11:05:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/F7GzyUU5LV",
      "expanded_url" : "http:\/\/getoptimise.com\/blog\/uncategorized\/my-implementation-of-the-10-rules-for-reproducible-computational-research\/",
      "display_url" : "getoptimise.com\/blog\/uncategor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840880252788322304",
  "text" : "My Implementation for making Data Analysis Reproducible https:\/\/t.co\/F7GzyUU5LV",
  "id" : 840880252788322304,
  "created_at" : "2017-03-12 11:00:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "indices" : [ 3, 12 ],
      "id_str" : "130745589",
      "id" : 130745589
    }, {
      "name" : "Google Cloud",
      "screen_name" : "googlecloud",
      "indices" : [ 71, 83 ],
      "id_str" : "19367815",
      "id" : 19367815
    }, {
      "name" : "Kaggle",
      "screen_name" : "kaggle",
      "indices" : [ 85, 92 ],
      "id_str" : "80422885",
      "id" : 80422885
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840877083920388097",
  "text" : "RT @drfeifei: One of our first small steps towards democratizing AI :) @googlecloud  @kaggle to host $100k video recog. challenge https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google Cloud",
        "screen_name" : "googlecloud",
        "indices" : [ 57, 69 ],
        "id_str" : "19367815",
        "id" : 19367815
      }, {
        "name" : "Kaggle",
        "screen_name" : "kaggle",
        "indices" : [ 71, 78 ],
        "id_str" : "80422885",
        "id" : 80422885
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/NMcfaSgMOj",
        "expanded_url" : "http:\/\/tcrn.ch\/2lQKlDf",
        "display_url" : "tcrn.ch\/2lQKlDf"
      } ]
    },
    "geo" : { },
    "id_str" : "832497314816159745",
    "text" : "One of our first small steps towards democratizing AI :) @googlecloud  @kaggle to host $100k video recog. challenge https:\/\/t.co\/NMcfaSgMOj",
    "id" : 832497314816159745,
    "created_at" : "2017-02-17 07:50:01 +0000",
    "user" : {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "protected" : false,
      "id_str" : "130745589",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841385099799085056\/R1iX4QGX_normal.jpg",
      "id" : 130745589,
      "verified" : false
    }
  },
  "id" : 840877083920388097,
  "created_at" : "2017-03-12 10:48:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shirley Almosni Chiche",
      "screen_name" : "shirleyalmosni",
      "indices" : [ 3, 18 ],
      "id_str" : "146010723",
      "id" : 146010723
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/shirleyalmosni\/status\/840179669488005121\/photo\/1",
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/VxgH1iCvVy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6jq8qrWkAAZk1t.jpg",
      "id_str" : "840179658473771008",
      "id" : 840179658473771008,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6jq8qrWkAAZk1t.jpg",
      "sizes" : [ {
        "h" : 736,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 736,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 736,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 667,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VxgH1iCvVy"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840877002647314436",
  "text" : "RT @shirleyalmosni: \uD83D\uDC4D https:\/\/t.co\/VxgH1iCvVy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/shirleyalmosni\/status\/840179669488005121\/photo\/1",
        "indices" : [ 2, 25 ],
        "url" : "https:\/\/t.co\/VxgH1iCvVy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6jq8qrWkAAZk1t.jpg",
        "id_str" : "840179658473771008",
        "id" : 840179658473771008,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6jq8qrWkAAZk1t.jpg",
        "sizes" : [ {
          "h" : 736,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 736,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 736,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 667,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VxgH1iCvVy"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840179669488005121",
    "text" : "\uD83D\uDC4D https:\/\/t.co\/VxgH1iCvVy",
    "id" : 840179669488005121,
    "created_at" : "2017-03-10 12:36:57 +0000",
    "user" : {
      "name" : "Shirley Almosni Chiche",
      "screen_name" : "shirleyalmosni",
      "protected" : false,
      "id_str" : "146010723",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/805113530433339393\/PCJQ-_sP_normal.jpg",
      "id" : 146010723,
      "verified" : false
    }
  },
  "id" : 840877002647314436,
  "created_at" : "2017-03-12 10:47:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/dTdXINMHVq",
      "expanded_url" : "https:\/\/twitter.com\/ShellyAsquith\/status\/833717971109474305",
      "display_url" : "twitter.com\/ShellyAsquith\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840875294793822208",
  "text" : "International students pay 2 or 3 times more. https:\/\/t.co\/dTdXINMHVq",
  "id" : 840875294793822208,
  "created_at" : "2017-03-12 10:41:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shelly Asquith",
      "screen_name" : "ShellyAsquith",
      "indices" : [ 3, 17 ],
      "id_str" : "19452549",
      "id" : 19452549
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "1DayWithoutUs",
      "indices" : [ 113, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840874911186968578",
  "text" : "RT @ShellyAsquith: This photo shows a university department with - and without - migrant students and academics. #1DayWithoutUs https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ShellyAsquith\/status\/833696883881172992\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/vTjIY0OqpR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C5Hi3gTXUAAn4k5.jpg",
        "id_str" : "833696849232089088",
        "id" : 833696849232089088,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C5Hi3gTXUAAn4k5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1767
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 587
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1767
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1035
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vTjIY0OqpR"
      } ],
      "hashtags" : [ {
        "text" : "1DayWithoutUs",
        "indices" : [ 94, 108 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "833696883881172992",
    "text" : "This photo shows a university department with - and without - migrant students and academics. #1DayWithoutUs https:\/\/t.co\/vTjIY0OqpR",
    "id" : 833696883881172992,
    "created_at" : "2017-02-20 15:16:41 +0000",
    "user" : {
      "name" : "Shelly Asquith",
      "screen_name" : "ShellyAsquith",
      "protected" : false,
      "id_str" : "19452549",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998120824371494913\/CRBREcJM_normal.jpg",
      "id" : 19452549,
      "verified" : false
    }
  },
  "id" : 840874911186968578,
  "created_at" : "2017-03-12 10:39:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Danny Sullivan",
      "screen_name" : "dannysullivan",
      "indices" : [ 3, 17 ],
      "id_str" : "858051",
      "id" : 858051
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840847514936999941",
  "text" : "RT @dannysullivan: I always say \"please\" and \"thank you\" to Alexa. Because someday when she destroys humanity, I want her to remember I was\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "832480603542999041",
    "text" : "I always say \"please\" and \"thank you\" to Alexa. Because someday when she destroys humanity, I want her to remember I was the polite one.",
    "id" : 832480603542999041,
    "created_at" : "2017-02-17 06:43:37 +0000",
    "user" : {
      "name" : "Danny Sullivan",
      "screen_name" : "dannysullivan",
      "protected" : false,
      "id_str" : "858051",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973688643268222976\/-5K6hDKX_normal.jpg",
      "id" : 858051,
      "verified" : true
    }
  },
  "id" : 840847514936999941,
  "created_at" : "2017-03-12 08:50:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/XlRskBpLWt",
      "expanded_url" : "http:\/\/scienceofmarketing.org\/",
      "display_url" : "scienceofmarketing.org"
    } ]
  },
  "geo" : { },
  "id_str" : "840826659490152448",
  "text" : "A tool for estimating market size for B2B startups using the US Business Census. https:\/\/t.co\/XlRskBpLWt",
  "id" : 840826659490152448,
  "created_at" : "2017-03-12 07:27:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mindaugas",
      "screen_name" : "MindaugasLT",
      "indices" : [ 3, 15 ],
      "id_str" : "2263572329",
      "id" : 2263572329
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/1oyfvQ2Bz3",
      "expanded_url" : "https:\/\/twitter.com\/brian_gough\/status\/840613331320877058",
      "display_url" : "twitter.com\/brian_gough\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840697392387235842",
  "text" : "RT @MindaugasLT: \uD83D\uDE1E https:\/\/t.co\/1oyfvQ2Bz3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 2, 25 ],
        "url" : "https:\/\/t.co\/1oyfvQ2Bz3",
        "expanded_url" : "https:\/\/twitter.com\/brian_gough\/status\/840613331320877058",
        "display_url" : "twitter.com\/brian_gough\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "840630242037374976",
    "text" : "\uD83D\uDE1E https:\/\/t.co\/1oyfvQ2Bz3",
    "id" : 840630242037374976,
    "created_at" : "2017-03-11 18:27:22 +0000",
    "user" : {
      "name" : "Mindaugas",
      "screen_name" : "MindaugasLT",
      "protected" : false,
      "id_str" : "2263572329",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031844054168993792\/wosMSSZ1_normal.jpg",
      "id" : 2263572329,
      "verified" : false
    }
  },
  "id" : 840697392387235842,
  "created_at" : "2017-03-11 22:54:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "indices" : [ 3, 18 ],
      "id_str" : "1626294277",
      "id" : 1626294277
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/840180057360363520\/video\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/k4xuxfOsQr",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/840179911981645824\/pu\/img\/D_nuodnXNJwy4zXg.jpg",
      "id_str" : "840179911981645824",
      "id" : 840179911981645824,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/840179911981645824\/pu\/img\/D_nuodnXNJwy4zXg.jpg",
      "sizes" : [ {
        "h" : 404,
        "resize" : "fit",
        "w" : 718
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 718
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 718
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/k4xuxfOsQr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840645090712440832",
  "text" : "RT @spectatorindex: MEDIA: US F\/A-18 fighter pilot lands on aircraft carrier in conditions of no visibility https:\/\/t.co\/k4xuxfOsQr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/840180057360363520\/video\/1",
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/k4xuxfOsQr",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/840179911981645824\/pu\/img\/D_nuodnXNJwy4zXg.jpg",
        "id_str" : "840179911981645824",
        "id" : 840179911981645824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/840179911981645824\/pu\/img\/D_nuodnXNJwy4zXg.jpg",
        "sizes" : [ {
          "h" : 404,
          "resize" : "fit",
          "w" : 718
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 718
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 718
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/k4xuxfOsQr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840180057360363520",
    "text" : "MEDIA: US F\/A-18 fighter pilot lands on aircraft carrier in conditions of no visibility https:\/\/t.co\/k4xuxfOsQr",
    "id" : 840180057360363520,
    "created_at" : "2017-03-10 12:38:30 +0000",
    "user" : {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "protected" : false,
      "id_str" : "1626294277",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839067030904922112\/LH4xqz-d_normal.jpg",
      "id" : 1626294277,
      "verified" : false
    }
  },
  "id" : 840645090712440832,
  "created_at" : "2017-03-11 19:26:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karrie Kehoe",
      "screen_name" : "KarrieKehoe",
      "indices" : [ 3, 15 ],
      "id_str" : "20435144",
      "id" : 20435144
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/JlZAjHa3xw",
      "expanded_url" : "https:\/\/twitter.com\/datajconf\/status\/838857602880655362",
      "display_url" : "twitter.com\/datajconf\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840631181934747650",
  "text" : "RT @KarrieKehoe: Amazing. A data journalism conf in Dublin! I couldn't be happier or more excited! \uD83D\uDE01 https:\/\/t.co\/JlZAjHa3xw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/JlZAjHa3xw",
        "expanded_url" : "https:\/\/twitter.com\/datajconf\/status\/838857602880655362",
        "display_url" : "twitter.com\/datajconf\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "838860744884105217",
    "text" : "Amazing. A data journalism conf in Dublin! I couldn't be happier or more excited! \uD83D\uDE01 https:\/\/t.co\/JlZAjHa3xw",
    "id" : 838860744884105217,
    "created_at" : "2017-03-06 21:16:01 +0000",
    "user" : {
      "name" : "Karrie Kehoe",
      "screen_name" : "KarrieKehoe",
      "protected" : false,
      "id_str" : "20435144",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037026798620606465\/sJh7-BFu_normal.jpg",
      "id" : 20435144,
      "verified" : false
    }
  },
  "id" : 840631181934747650,
  "created_at" : "2017-03-11 18:31:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Atwood",
      "screen_name" : "codinghorror",
      "indices" : [ 3, 16 ],
      "id_str" : "5637652",
      "id" : 5637652
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840628418408566784",
  "text" : "RT @codinghorror: If we don't solve the password problem for users in my lifetime I am gonna haunt you from beyond the grave as a ghost htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/codinghorror\/status\/631238409269309440\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/Tf9EnwgoZv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CMKb_QvUAAAZ2vH.png",
        "id_str" : "631238408912764928",
        "id" : 631238408912764928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CMKb_QvUAAAZ2vH.png",
        "sizes" : [ {
          "h" : 342,
          "resize" : "fit",
          "w" : 410
        }, {
          "h" : 342,
          "resize" : "fit",
          "w" : 410
        }, {
          "h" : 342,
          "resize" : "fit",
          "w" : 410
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 342,
          "resize" : "fit",
          "w" : 410
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Tf9EnwgoZv"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "631238409269309440",
    "text" : "If we don't solve the password problem for users in my lifetime I am gonna haunt you from beyond the grave as a ghost http:\/\/t.co\/Tf9EnwgoZv",
    "id" : 631238409269309440,
    "created_at" : "2015-08-11 22:58:59 +0000",
    "user" : {
      "name" : "Jeff Atwood",
      "screen_name" : "codinghorror",
      "protected" : false,
      "id_str" : "5637652",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/632821853627678720\/zPKK7jql_normal.png",
      "id" : 5637652,
      "verified" : true
    }
  },
  "id" : 840628418408566784,
  "created_at" : "2017-03-11 18:20:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/y01D3BrB4o",
      "expanded_url" : "https:\/\/twitter.com\/seanjtaylor\/status\/840357353631309824",
      "display_url" : "twitter.com\/seanjtaylor\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840628128452079616",
  "text" : "This is so wrong (the pie chart) https:\/\/t.co\/y01D3BrB4o",
  "id" : 840628128452079616,
  "created_at" : "2017-03-11 18:18:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "840579936247246848",
  "geo" : { },
  "id_str" : "840625929198141441",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent I left after the IoT panel discussion. Hope to catch up with u some other time.",
  "id" : 840625929198141441,
  "in_reply_to_status_id" : 840579936247246848,
  "created_at" : "2017-03-11 18:10:14 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Telegraph",
      "screen_name" : "Telegraph",
      "indices" : [ 3, 13 ],
      "id_str" : "16343974",
      "id" : 16343974
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Telegraph\/status\/840608233660243968\/photo\/1",
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/e9iBsMNWDP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6pwsvoXAAAGfrJ.jpg",
      "id_str" : "840608194460319744",
      "id" : 840608194460319744,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6pwsvoXAAAGfrJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 850,
        "resize" : "fit",
        "w" : 1293
      }, {
        "h" : 850,
        "resize" : "fit",
        "w" : 1293
      }, {
        "h" : 447,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 789,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/e9iBsMNWDP"
    } ],
    "hashtags" : [ {
      "text" : "BOBcartoons",
      "indices" : [ 36, 48 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840622928786014211",
  "text" : "RT @Telegraph: Who let the kids in? #BOBcartoons https:\/\/t.co\/e9iBsMNWDP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Telegraph\/status\/840608233660243968\/photo\/1",
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/e9iBsMNWDP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6pwsvoXAAAGfrJ.jpg",
        "id_str" : "840608194460319744",
        "id" : 840608194460319744,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6pwsvoXAAAGfrJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 850,
          "resize" : "fit",
          "w" : 1293
        }, {
          "h" : 850,
          "resize" : "fit",
          "w" : 1293
        }, {
          "h" : 447,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 789,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/e9iBsMNWDP"
      } ],
      "hashtags" : [ {
        "text" : "BOBcartoons",
        "indices" : [ 21, 33 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840608233660243968",
    "text" : "Who let the kids in? #BOBcartoons https:\/\/t.co\/e9iBsMNWDP",
    "id" : 840608233660243968,
    "created_at" : "2017-03-11 16:59:55 +0000",
    "user" : {
      "name" : "The Telegraph",
      "screen_name" : "Telegraph",
      "protected" : false,
      "id_str" : "16343974",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943090005723041792\/2cjxINlJ_normal.jpg",
      "id" : 16343974,
      "verified" : true
    }
  },
  "id" : 840622928786014211,
  "created_at" : "2017-03-11 17:58:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kaggle",
      "screen_name" : "kaggle",
      "indices" : [ 3, 10 ],
      "id_str" : "80422885",
      "id" : 80422885
    }, {
      "name" : "Google Cloud",
      "screen_name" : "googlecloud",
      "indices" : [ 54, 66 ],
      "id_str" : "19367815",
      "id" : 19367815
    }, {
      "name" : "Anthony Goldbloom",
      "screen_name" : "antgoldbloom",
      "indices" : [ 96, 109 ],
      "id_str" : "167960058",
      "id" : 167960058
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/QOcxrK3DFs",
      "expanded_url" : "http:\/\/blog.kaggle.com\/2017\/03\/08\/kaggle-joins-google-cloud\/",
      "display_url" : "blog.kaggle.com\/2017\/03\/08\/kag\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840583088811958272",
  "text" : "RT @kaggle: We're proud and excited to be joining the @googlecloud team! Read more from our CEO @antgoldbloom here https:\/\/t.co\/QOcxrK3DFs\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google Cloud",
        "screen_name" : "googlecloud",
        "indices" : [ 42, 54 ],
        "id_str" : "19367815",
        "id" : 19367815
      }, {
        "name" : "Anthony Goldbloom",
        "screen_name" : "antgoldbloom",
        "indices" : [ 84, 97 ],
        "id_str" : "167960058",
        "id" : 167960058
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/kaggle\/status\/839552751248019456\/photo\/1",
        "indices" : [ 127, 150 ],
        "url" : "https:\/\/t.co\/e2OZRNf35t",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6awxwSXEAE2WZR.jpg",
        "id_str" : "839552749373165569",
        "id" : 839552749373165569,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6awxwSXEAE2WZR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 444
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 444
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 444
        }, {
          "h" : 273,
          "resize" : "fit",
          "w" : 444
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/e2OZRNf35t"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/QOcxrK3DFs",
        "expanded_url" : "http:\/\/blog.kaggle.com\/2017\/03\/08\/kaggle-joins-google-cloud\/",
        "display_url" : "blog.kaggle.com\/2017\/03\/08\/kag\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839552751248019456",
    "text" : "We're proud and excited to be joining the @googlecloud team! Read more from our CEO @antgoldbloom here https:\/\/t.co\/QOcxrK3DFs https:\/\/t.co\/e2OZRNf35t",
    "id" : 839552751248019456,
    "created_at" : "2017-03-08 19:05:48 +0000",
    "user" : {
      "name" : "Kaggle",
      "screen_name" : "kaggle",
      "protected" : false,
      "id_str" : "80422885",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1146317507\/twitter_normal.png",
      "id" : 80422885,
      "verified" : true
    }
  },
  "id" : 840583088811958272,
  "created_at" : "2017-03-11 15:20:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CareerZoo",
      "indices" : [ 105, 115 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840580726525480960",
  "text" : "Interesting to hear about Fleetmatics (an Irish Firm) offering - Commercial vehicle operation using IoT. #CareerZoo",
  "id" : 840580726525480960,
  "created_at" : "2017-03-11 15:10:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/840579281675792388\/photo\/1",
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/lfQ1c90eD6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6pWYMSWcAAe7TM.jpg",
      "id_str" : "840579254073061376",
      "id" : 840579254073061376,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6pWYMSWcAAe7TM.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lfQ1c90eD6"
    } ],
    "hashtags" : [ {
      "text" : "CareerZoo",
      "indices" : [ 65, 75 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840579281675792388",
  "text" : "The ability to act on the data is critical. IoT panel discussion #CareerZoo https:\/\/t.co\/lfQ1c90eD6",
  "id" : 840579281675792388,
  "created_at" : "2017-03-11 15:04:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/840576890071060481\/photo\/1",
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/wZDl5KgKlH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6pUD16W0AAx-d2.jpg",
      "id_str" : "840576705446203392",
      "id" : 840576705446203392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6pUD16W0AAx-d2.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wZDl5KgKlH"
    } ],
    "hashtags" : [ {
      "text" : "CareerZoo",
      "indices" : [ 55, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840576890071060481",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent giving a workshop on Imposter Syndrome #CareerZoo https:\/\/t.co\/wZDl5KgKlH",
  "id" : 840576890071060481,
  "created_at" : "2017-03-11 14:55:22 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NCIRL",
      "screen_name" : "NCIRL",
      "indices" : [ 43, 49 ],
      "id_str" : "12319392",
      "id" : 12319392
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/840573590907047936\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/G7zeQmGXNy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6pRKP-WsAEalGj.jpg",
      "id_str" : "840573516986626049",
      "id" : 840573516986626049,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6pRKP-WsAEalGj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/G7zeQmGXNy"
    } ],
    "hashtags" : [ {
      "text" : "CareerZoo",
      "indices" : [ 0, 10 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840573590907047936",
  "text" : "#CareerZoo Free data analytics course from @NCIRL https:\/\/t.co\/G7zeQmGXNy",
  "id" : 840573590907047936,
  "created_at" : "2017-03-11 14:42:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/840566165990735872\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/XggquAdQIs",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6pKdrPWwAAPzd3.jpg",
      "id_str" : "840566154141810688",
      "id" : 840566154141810688,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6pKdrPWwAAPzd3.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XggquAdQIs"
    } ],
    "hashtags" : [ {
      "text" : "CareerZoo",
      "indices" : [ 68, 78 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840566165990735872",
  "text" : "Consistency is key, Stop Playing Safe, Focus on the value you bring #CareerZoo https:\/\/t.co\/XggquAdQIs",
  "id" : 840566165990735872,
  "created_at" : "2017-03-11 14:12:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian McGreevy",
      "screen_name" : "AmphibiaSport",
      "indices" : [ 3, 17 ],
      "id_str" : "149186257",
      "id" : 149186257
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840533508699369472",
  "text" : "RT @AmphibiaSport: We have 1 of our new backpacks to give away.  It includes a illuminous rain cover with reflective logo for visibility. T\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AmphibiaSport\/status\/840270077714939904\/photo\/1",
        "indices" : [ 142, 165 ],
        "url" : "https:\/\/t.co\/Tn6VlhU01O",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6k9HNgWgAAK6zM.jpg",
        "id_str" : "840269999575040000",
        "id" : 840269999575040000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6k9HNgWgAAK6zM.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1181,
          "resize" : "fit",
          "w" : 1772
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1181,
          "resize" : "fit",
          "w" : 1772
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Tn6VlhU01O"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AmphibiaSport\/status\/840270077714939904\/photo\/1",
        "indices" : [ 142, 165 ],
        "url" : "https:\/\/t.co\/Tn6VlhU01O",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6k9HJrWcAATQSz.jpg",
        "id_str" : "840269998547431424",
        "id" : 840269998547431424,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6k9HJrWcAATQSz.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1181,
          "resize" : "fit",
          "w" : 1772
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1181,
          "resize" : "fit",
          "w" : 1772
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Tn6VlhU01O"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AmphibiaSport\/status\/840270077714939904\/photo\/1",
        "indices" : [ 142, 165 ],
        "url" : "https:\/\/t.co\/Tn6VlhU01O",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6k9HNaW0AUigQ-.jpg",
        "id_str" : "840269999549894661",
        "id" : 840269999549894661,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6k9HNaW0AUigQ-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1181,
          "resize" : "fit",
          "w" : 1772
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1181,
          "resize" : "fit",
          "w" : 1772
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Tn6VlhU01O"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AmphibiaSport\/status\/840270077714939904\/photo\/1",
        "indices" : [ 142, 165 ],
        "url" : "https:\/\/t.co\/Tn6VlhU01O",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6k9Kd3WYAUwiko.jpg",
        "id_str" : "840270055506075653",
        "id" : 840270055506075653,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6k9Kd3WYAUwiko.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 842,
          "resize" : "fit",
          "w" : 1271
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 795,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 842,
          "resize" : "fit",
          "w" : 1271
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Tn6VlhU01O"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840270077714939904",
    "text" : "We have 1 of our new backpacks to give away.  It includes a illuminous rain cover with reflective logo for visibility. To enter just RT&amp;F https:\/\/t.co\/Tn6VlhU01O",
    "id" : 840270077714939904,
    "created_at" : "2017-03-10 18:36:12 +0000",
    "user" : {
      "name" : "Adrian McGreevy",
      "screen_name" : "AmphibiaSport",
      "protected" : false,
      "id_str" : "149186257",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1124232543\/logo_low_res_normal.jpg",
      "id" : 149186257,
      "verified" : false
    }
  },
  "id" : 840533508699369472,
  "created_at" : "2017-03-11 12:02:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "@IrishRecruiter",
      "screen_name" : "IrishRecruiter",
      "indices" : [ 3, 18 ],
      "id_str" : "14699921",
      "id" : 14699921
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IrishRecruiter\/status\/840512382640230400\/photo\/1",
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/RmEBC48E2w",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6oZf16WgAAVeCS.jpg",
      "id_str" : "840512315296481280",
      "id" : 840512315296481280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6oZf16WgAAVeCS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RmEBC48E2w"
    } ],
    "hashtags" : [ {
      "text" : "diversity",
      "indices" : [ 77, 87 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840519593902891008",
  "text" : "RT @IrishRecruiter: What's the point of having a single sex panel discussing #diversity? https:\/\/t.co\/RmEBC48E2w",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishRecruiter\/status\/840512382640230400\/photo\/1",
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/RmEBC48E2w",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6oZf16WgAAVeCS.jpg",
        "id_str" : "840512315296481280",
        "id" : 840512315296481280,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6oZf16WgAAVeCS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RmEBC48E2w"
      } ],
      "hashtags" : [ {
        "text" : "diversity",
        "indices" : [ 57, 67 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840512382640230400",
    "text" : "What's the point of having a single sex panel discussing #diversity? https:\/\/t.co\/RmEBC48E2w",
    "id" : 840512382640230400,
    "created_at" : "2017-03-11 10:39:02 +0000",
    "user" : {
      "name" : "@IrishRecruiter",
      "screen_name" : "IrishRecruiter",
      "protected" : false,
      "id_str" : "14699921",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/592381802813227009\/HwjEDkH__normal.jpg",
      "id" : 14699921,
      "verified" : false
    }
  },
  "id" : 840519593902891008,
  "created_at" : "2017-03-11 11:07:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BizExpo_ie",
      "screen_name" : "BizExpo_ie",
      "indices" : [ 0, 11 ],
      "id_str" : "826056017586831361",
      "id" : 826056017586831361
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "840180488002248704",
  "geo" : { },
  "id_str" : "840516621282205696",
  "in_reply_to_user_id" : 826056017586831361,
  "text" : "@BizExpo_ie I can offer a Web\/Data Analytics Clinic for the attendees. Who can I talk to? Thanks",
  "id" : 840516621282205696,
  "in_reply_to_status_id" : 840180488002248704,
  "created_at" : "2017-03-11 10:55:53 +0000",
  "in_reply_to_screen_name" : "BizExpo_ie",
  "in_reply_to_user_id_str" : "826056017586831361",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840474885306646528",
  "text" : "RT @lisaocarroll: Brit Pret a Manger worker:I make 250 coffees an hour. With 6 people at tills, it's stressful. Most Brits don't last  http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/ykUihhvLdZ",
        "expanded_url" : "https:\/\/www.theguardian.com\/business\/2017\/mar\/11\/eu-hospitality-workers-brexit-pret-a-manger-hotels-restaurants?CMP=Share_iOSApp_Other",
        "display_url" : "theguardian.com\/business\/2017\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "840474247768219648",
    "text" : "Brit Pret a Manger worker:I make 250 coffees an hour. With 6 people at tills, it's stressful. Most Brits don't last  https:\/\/t.co\/ykUihhvLdZ",
    "id" : 840474247768219648,
    "created_at" : "2017-03-11 08:07:30 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 840474885306646528,
  "created_at" : "2017-03-11 08:10:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 77, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/6VHcXtUmNY",
      "expanded_url" : "http:\/\/support.chartbeat.com\/docs\/methodology.html",
      "display_url" : "support.chartbeat.com\/docs\/methodolo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840468634250149889",
  "text" : "Engagement is something we can, and should, measure. https:\/\/t.co\/6VHcXtUmNY #getoptimise",
  "id" : 840468634250149889,
  "created_at" : "2017-03-11 07:45:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Weckler",
      "screen_name" : "adrianweckler",
      "indices" : [ 3, 17 ],
      "id_str" : "18080002",
      "id" : 18080002
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/adrianweckler\/status\/840162266402168832\/photo\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/EDk9RSCKC6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6jbIMFXUAAE3O3.jpg",
      "id_str" : "840162264233758720",
      "id" : 840162264233758720,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6jbIMFXUAAE3O3.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EDk9RSCKC6"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840467767312711680",
  "text" : "RT @adrianweckler: The Irish army's slightly terrifying looking drone. Has 12 of them. Made by an Israeli company. https:\/\/t.co\/EDk9RSCKC6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/adrianweckler\/status\/840162266402168832\/photo\/1",
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/EDk9RSCKC6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6jbIMFXUAAE3O3.jpg",
        "id_str" : "840162264233758720",
        "id" : 840162264233758720,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6jbIMFXUAAE3O3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EDk9RSCKC6"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840162266402168832",
    "text" : "The Irish army's slightly terrifying looking drone. Has 12 of them. Made by an Israeli company. https:\/\/t.co\/EDk9RSCKC6",
    "id" : 840162266402168832,
    "created_at" : "2017-03-10 11:27:48 +0000",
    "user" : {
      "name" : "Adrian Weckler",
      "screen_name" : "adrianweckler",
      "protected" : false,
      "id_str" : "18080002",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040108417703014400\/NY5mHJ0B_normal.jpg",
      "id" : 18080002,
      "verified" : true
    }
  },
  "id" : 840467767312711680,
  "created_at" : "2017-03-11 07:41:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CareerZoo",
      "indices" : [ 23, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840273610950508548",
  "text" : "I available for hire.  #CareerZoo I use analytics to solve biz problems by leveraging machine learning, data mining &amp; data science.",
  "id" : 840273610950508548,
  "created_at" : "2017-03-10 18:50:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/NYkTvPCBGf",
      "expanded_url" : "http:\/\/tcrn.ch\/2m4SBhS",
      "display_url" : "tcrn.ch\/2m4SBhS"
    } ]
  },
  "geo" : { },
  "id_str" : "840264119387316224",
  "text" : "Google\u2019s Cloud Platform improves its free tier and adds always-free compute and storage https:\/\/t.co\/NYkTvPCBGf",
  "id" : 840264119387316224,
  "created_at" : "2017-03-10 18:12:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "cesar quintero",
      "screen_name" : "CesarQuintero",
      "indices" : [ 3, 17 ],
      "id_str" : "18491595",
      "id" : 18491595
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/CesarQuintero\/status\/840004945466146816\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/qgmTbtkcqG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6hMCBvVAAAzxn7.jpg",
      "id_str" : "840004928214990848",
      "id" : 840004928214990848,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6hMCBvVAAAzxn7.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qgmTbtkcqG"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840182069871423489",
  "text" : "RT @CesarQuintero: Just a guy walking his robot. Another day in San Francisco \uD83E\uDD16 https:\/\/t.co\/qgmTbtkcqG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CesarQuintero\/status\/840004945466146816\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/qgmTbtkcqG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6hMCBvVAAAzxn7.jpg",
        "id_str" : "840004928214990848",
        "id" : 840004928214990848,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6hMCBvVAAAzxn7.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qgmTbtkcqG"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840004945466146816",
    "text" : "Just a guy walking his robot. Another day in San Francisco \uD83E\uDD16 https:\/\/t.co\/qgmTbtkcqG",
    "id" : 840004945466146816,
    "created_at" : "2017-03-10 01:02:40 +0000",
    "user" : {
      "name" : "cesar quintero",
      "screen_name" : "CesarQuintero",
      "protected" : false,
      "id_str" : "18491595",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/880520639878017024\/iYkCqFuz_normal.jpg",
      "id" : 18491595,
      "verified" : false
    }
  },
  "id" : 840182069871423489,
  "created_at" : "2017-03-10 12:46:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 0, 9 ],
      "id_str" : "136690988",
      "id" : 136690988
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/mmFp8QSlcc",
      "expanded_url" : "https:\/\/twitter.com\/BICDublin\/status\/840174571881934848",
      "display_url" : "twitter.com\/BICDublin\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840181863721402370",
  "in_reply_to_user_id" : 136690988,
  "text" : "@ailieirv https:\/\/t.co\/mmFp8QSlcc",
  "id" : 840181863721402370,
  "created_at" : "2017-03-10 12:45:40 +0000",
  "in_reply_to_screen_name" : "ailieirv",
  "in_reply_to_user_id_str" : "136690988",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/98RqrVQBQy",
      "expanded_url" : "https:\/\/twitter.com\/historyinmoment\/status\/840122595441688576",
      "display_url" : "twitter.com\/historyinmomen\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840168287711449088",
  "text" : "RT @yapphenghui: succinct https:\/\/t.co\/98RqrVQBQy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 9, 32 ],
        "url" : "https:\/\/t.co\/98RqrVQBQy",
        "expanded_url" : "https:\/\/twitter.com\/historyinmoment\/status\/840122595441688576",
        "display_url" : "twitter.com\/historyinmomen\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "840161520084385797",
    "text" : "succinct https:\/\/t.co\/98RqrVQBQy",
    "id" : 840161520084385797,
    "created_at" : "2017-03-10 11:24:50 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 840168287711449088,
  "created_at" : "2017-03-10 11:51:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "The Verge",
      "screen_name" : "verge",
      "indices" : [ 103, 109 ],
      "id_str" : "275686563",
      "id" : 275686563
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/miWBKqKfs9",
      "expanded_url" : "http:\/\/www.theverge.com\/2017\/3\/9\/14864552\/google-hangouts-chat-update-announced-slack-group-messaging-video?utm_campaign=theverge&utm_content=entry&utm_medium=social&utm_source=twitter",
      "display_url" : "theverge.com\/2017\/3\/9\/14864\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840168175836766208",
  "text" : "RT @edwinksl: Google Hangouts is getting a major overhaul to take on Slack https:\/\/t.co\/miWBKqKfs9 via @Verge",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Verge",
        "screen_name" : "verge",
        "indices" : [ 89, 95 ],
        "id_str" : "275686563",
        "id" : 275686563
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/miWBKqKfs9",
        "expanded_url" : "http:\/\/www.theverge.com\/2017\/3\/9\/14864552\/google-hangouts-chat-update-announced-slack-group-messaging-video?utm_campaign=theverge&utm_content=entry&utm_medium=social&utm_source=twitter",
        "display_url" : "theverge.com\/2017\/3\/9\/14864\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "840165192986198016",
    "text" : "Google Hangouts is getting a major overhaul to take on Slack https:\/\/t.co\/miWBKqKfs9 via @Verge",
    "id" : 840165192986198016,
    "created_at" : "2017-03-10 11:39:26 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 840168175836766208,
  "created_at" : "2017-03-10 11:51:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mental Health IRL",
      "screen_name" : "MentalHealthIrl",
      "indices" : [ 3, 19 ],
      "id_str" : "1655473056",
      "id" : 1655473056
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MentalHealthIrl\/status\/840117537668976640\/photo\/1",
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/nHB99COHQZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6iyb7VV0AAFt90.jpg",
      "id_str" : "840117523358011392",
      "id" : 840117523358011392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6iyb7VV0AAFt90.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nHB99COHQZ"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/MentalHealthIrl\/status\/840117537668976640\/photo\/1",
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/nHB99COHQZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6iyb7NVwAItZ-X.jpg",
      "id_str" : "840117523324452866",
      "id" : 840117523324452866,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6iyb7NVwAItZ-X.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nHB99COHQZ"
    } ],
    "hashtags" : [ {
      "text" : "BeActive",
      "indices" : [ 62, 71 ]
    }, {
      "text" : "mindfulwalking",
      "indices" : [ 72, 87 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840155595164467200",
  "text" : "RT @MentalHealthIrl: A lovely pre-work walk to start the day. #BeActive #mindfulwalking https:\/\/t.co\/nHB99COHQZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MentalHealthIrl\/status\/840117537668976640\/photo\/1",
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/nHB99COHQZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6iyb7VV0AAFt90.jpg",
        "id_str" : "840117523358011392",
        "id" : 840117523358011392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6iyb7VV0AAFt90.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nHB99COHQZ"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MentalHealthIrl\/status\/840117537668976640\/photo\/1",
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/nHB99COHQZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6iyb7NVwAItZ-X.jpg",
        "id_str" : "840117523324452866",
        "id" : 840117523324452866,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6iyb7NVwAItZ-X.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nHB99COHQZ"
      } ],
      "hashtags" : [ {
        "text" : "BeActive",
        "indices" : [ 41, 50 ]
      }, {
        "text" : "mindfulwalking",
        "indices" : [ 51, 66 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "840117537668976640",
    "text" : "A lovely pre-work walk to start the day. #BeActive #mindfulwalking https:\/\/t.co\/nHB99COHQZ",
    "id" : 840117537668976640,
    "created_at" : "2017-03-10 08:30:04 +0000",
    "user" : {
      "name" : "Mental Health IRL",
      "screen_name" : "MentalHealthIrl",
      "protected" : false,
      "id_str" : "1655473056",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024262104080166912\/K8HmsB-7_normal.jpg",
      "id" : 1655473056,
      "verified" : false
    }
  },
  "id" : 840155595164467200,
  "created_at" : "2017-03-10 11:01:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840153118939668481",
  "text" : "Which car insurance company in Ireland would you recommend? Thanks",
  "id" : 840153118939668481,
  "created_at" : "2017-03-10 10:51:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/XsOD2VGpkt",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/839410335568248833",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840151040108376064",
  "text" : "I am still seeking input. Anyone? Leads don't just drop on your lap? https:\/\/t.co\/XsOD2VGpkt",
  "id" : 840151040108376064,
  "created_at" : "2017-03-10 10:43:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/gpsCAIcUL7",
      "expanded_url" : "https:\/\/twitter.com\/NGIreland\/status\/839905569704198144",
      "display_url" : "twitter.com\/NGIreland\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "840131728630980608",
  "text" : "Looking forward to this in 2017 https:\/\/t.co\/gpsCAIcUL7",
  "id" : 840131728630980608,
  "created_at" : "2017-03-10 09:26:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "840100540289712128",
  "text" : "RT @mrbrown: In Singapore, 18-year-old can train with rifle and kill your enemy. But cannot watch R21 movies and now cannot smoke until you\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839990680709603329",
    "text" : "In Singapore, 18-year-old can train with rifle and kill your enemy. But cannot watch R21 movies and now cannot smoke until you're 21.",
    "id" : 839990680709603329,
    "created_at" : "2017-03-10 00:05:59 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 840100540289712128,
  "created_at" : "2017-03-10 07:22:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/wdZVeAvZ7Z",
      "expanded_url" : "https:\/\/petapixel.com\/2017\/03\/07\/popular-photography-dead-80-years-top-photo-magazine\/",
      "display_url" : "petapixel.com\/2017\/03\/07\/pop\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839945085324312576",
  "text" : "Popular Photography is Dead After 80 Years as a Top Photo Magazine https:\/\/t.co\/wdZVeAvZ7Z",
  "id" : 839945085324312576,
  "created_at" : "2017-03-09 21:04:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/8dsTjiZy6u",
      "expanded_url" : "https:\/\/www.iedr.ie\/blog\/64-irish-consumers-little-no-trust-businesses-use-free-email-addresses-like-gmail-eircom\/",
      "display_url" : "iedr.ie\/blog\/64-irish-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839936319237328896",
  "text" : "64% of Irish consumers have little or no trust in businesses that use free email addresses like Gmail and Eircom  https:\/\/t.co\/8dsTjiZy6u",
  "id" : 839936319237328896,
  "created_at" : "2017-03-09 20:29:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/JdrpFwaO61",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-39142030",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839872460535193600",
  "text" : "Why high-flying Singapore wants more than grades https:\/\/t.co\/JdrpFwaO61",
  "id" : 839872460535193600,
  "created_at" : "2017-03-09 16:16:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dj patil",
      "screen_name" : "dpatil",
      "indices" : [ 3, 10 ],
      "id_str" : "14839109",
      "id" : 14839109
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839867201423290368",
  "text" : "RT @dpatil: I've got a Ph.D. in math\nHealth care policy is harder",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "836335436763058176",
    "text" : "I've got a Ph.D. in math\nHealth care policy is harder",
    "id" : 836335436763058176,
    "created_at" : "2017-02-27 22:01:21 +0000",
    "user" : {
      "name" : "dj patil",
      "screen_name" : "dpatil",
      "protected" : false,
      "id_str" : "14839109",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841324436787675137\/TarQkEM__normal.jpg",
      "id" : 14839109,
      "verified" : true
    }
  },
  "id" : 839867201423290368,
  "created_at" : "2017-03-09 15:55:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/d3pOMZ2876",
      "expanded_url" : "http:\/\/willgrant.org\/post\/five-criteria-startup-success.html",
      "display_url" : "willgrant.org\/post\/five-crit\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839866483995983872",
  "text" : "Five success criteria for startups https:\/\/t.co\/d3pOMZ2876",
  "id" : 839866483995983872,
  "created_at" : "2017-03-09 15:52:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/Wx2RV8haWm",
      "expanded_url" : "https:\/\/twitter.com\/oiwoods\/status\/839838955411406853",
      "display_url" : "twitter.com\/oiwoods\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839865261587038208",
  "text" : "My grandmother used to bring her home brewed soup to the restaurant. https:\/\/t.co\/Wx2RV8haWm",
  "id" : 839865261587038208,
  "created_at" : "2017-03-09 15:47:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    }, {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 19, 32 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    }, {
      "name" : "Career Zoo",
      "screen_name" : "CareerZootweets",
      "indices" : [ 61, 77 ],
      "id_str" : "340394880",
      "id" : 340394880
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/3Ls6q5jWTG",
      "expanded_url" : "http:\/\/www.careerzoo.ie\/allies-workshops.html",
      "display_url" : "careerzoo.ie\/allies-worksho\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839837143195344896",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent of @DigiWomenIRL on Imposter Syndrome at the @CareerZootweets workshop this Sat 11th Mar https:\/\/t.co\/3Ls6q5jWTG",
  "id" : 839837143195344896,
  "created_at" : "2017-03-09 13:55:53 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Pulch",
      "screen_name" : "MichaelPulchEU",
      "indices" : [ 3, 18 ],
      "id_str" : "2852724951",
      "id" : 2852724951
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 39, 49 ]
    }, {
      "text" : "EU",
      "indices" : [ 102, 105 ]
    }, {
      "text" : "ASEAN",
      "indices" : [ 111, 117 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839833319068090368",
  "text" : "RT @MichaelPulchEU: FTA signature with #Singapore will underpin the dynamic trade relationship of the #EU with #ASEAN. https:\/\/t.co\/YvWMCh3\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 19, 29 ]
      }, {
        "text" : "EU",
        "indices" : [ 82, 85 ]
      }, {
        "text" : "ASEAN",
        "indices" : [ 91, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/YvWMCh3W1e",
        "expanded_url" : "https:\/\/twitter.com\/malmstromeu\/status\/839437222382350336",
        "display_url" : "twitter.com\/malmstromeu\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839731414937419777",
    "text" : "FTA signature with #Singapore will underpin the dynamic trade relationship of the #EU with #ASEAN. https:\/\/t.co\/YvWMCh3W1e",
    "id" : 839731414937419777,
    "created_at" : "2017-03-09 06:55:45 +0000",
    "user" : {
      "name" : "Michael Pulch",
      "screen_name" : "MichaelPulchEU",
      "protected" : false,
      "id_str" : "2852724951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966510151942389761\/OeWG1oUG_normal.jpg",
      "id" : 2852724951,
      "verified" : true
    }
  },
  "id" : 839833319068090368,
  "created_at" : "2017-03-09 13:40:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/hej5fPaEwE",
      "expanded_url" : "https:\/\/www.thesun.co.uk\/motors\/3032149\/small-electrics-firm-set-to-produce-worlds-first-fully-electric-hypercar\/",
      "display_url" : "thesun.co.uk\/motors\/3032149\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839832242092457984",
  "text" : "Small electrics firm set to produce world's first fully-electric hypercar https:\/\/t.co\/hej5fPaEwE",
  "id" : 839832242092457984,
  "created_at" : "2017-03-09 13:36:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Healy",
      "screen_name" : "jonathanhealy",
      "indices" : [ 3, 17 ],
      "id_str" : "21304205",
      "id" : 21304205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/Zq8FJIkWej",
      "expanded_url" : "https:\/\/twitter.com\/redfmnews\/status\/839794686701207552",
      "display_url" : "twitter.com\/redfmnews\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839796508849471490",
  "text" : "RT @jonathanhealy: \uD83D\uDE44 https:\/\/t.co\/Zq8FJIkWej",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 2, 25 ],
        "url" : "https:\/\/t.co\/Zq8FJIkWej",
        "expanded_url" : "https:\/\/twitter.com\/redfmnews\/status\/839794686701207552",
        "display_url" : "twitter.com\/redfmnews\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839794839768215552",
    "text" : "\uD83D\uDE44 https:\/\/t.co\/Zq8FJIkWej",
    "id" : 839794839768215552,
    "created_at" : "2017-03-09 11:07:47 +0000",
    "user" : {
      "name" : "Jonathan Healy",
      "screen_name" : "jonathanhealy",
      "protected" : false,
      "id_str" : "21304205",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/979798752323100673\/MyUJMmJA_normal.jpg",
      "id" : 21304205,
      "verified" : true
    }
  },
  "id" : 839796508849471490,
  "created_at" : "2017-03-09 11:14:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 0, 11 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "839770789192814593",
  "geo" : { },
  "id_str" : "839796414133710852",
  "in_reply_to_user_id" : 26903787,
  "text" : "@Dotnetster I use R to do it and let compare notes? You can DM me \uD83D\uDE01",
  "id" : 839796414133710852,
  "in_reply_to_status_id" : 839770789192814593,
  "created_at" : "2017-03-09 11:14:02 +0000",
  "in_reply_to_screen_name" : "Dotnetster",
  "in_reply_to_user_id_str" : "26903787",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Modern Farmer",
      "screen_name" : "ModFarm",
      "indices" : [ 54, 62 ],
      "id_str" : "944467345",
      "id" : 944467345
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/0IcqxEkjvt",
      "expanded_url" : "http:\/\/modernfarmer.com\/2017\/03\/laundry-garden-irrigate-graywater\/",
      "display_url" : "modernfarmer.com\/2017\/03\/laundr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839795904689352705",
  "text" : "Laundry-to-Garden: How to Irrigate with Graywater via @modfarm https:\/\/t.co\/0IcqxEkjvt",
  "id" : 839795904689352705,
  "created_at" : "2017-03-09 11:12:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839664840402681856",
  "text" : "Once you used R and Python for Data Analysis, you do not want to go back to Excel...",
  "id" : 839664840402681856,
  "created_at" : "2017-03-09 02:31:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/os4uWbRjzL",
      "expanded_url" : "https:\/\/twitter.com\/shaneoleary1\/status\/839533957922373638",
      "display_url" : "twitter.com\/shaneoleary1\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839658373595414530",
  "text" : "Slide 25 is very true. I am the only one in the extended family to use twitter extensively. https:\/\/t.co\/os4uWbRjzL",
  "id" : 839658373595414530,
  "created_at" : "2017-03-09 02:05:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/bgYLFIPfma",
      "expanded_url" : "http:\/\/adage.com\/article\/digital\/p-g-decided-facebook-ad-targeting-worth-money\/305390\/",
      "display_url" : "adage.com\/article\/digita\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839657160426553344",
  "text" : "Why P&amp;G Decided Facebook Ad Targeting Wasn't Worth the Money https:\/\/t.co\/bgYLFIPfma",
  "id" : 839657160426553344,
  "created_at" : "2017-03-09 02:00:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Angela Bassa",
      "screen_name" : "AngeBassa",
      "indices" : [ 3, 13 ],
      "id_str" : "937467860",
      "id" : 937467860
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839648730353631233",
  "text" : "RT @AngeBassa: All data is manipulated data. It doesn\u2019t spontaneously appear inside a data frame.\n\nHumans define what data is, collect it,\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839450260208566273",
    "text" : "All data is manipulated data. It doesn\u2019t spontaneously appear inside a data frame.\n\nHumans define what data is, collect it, interpret it\u2026",
    "id" : 839450260208566273,
    "created_at" : "2017-03-08 12:18:32 +0000",
    "user" : {
      "name" : "Angela Bassa",
      "screen_name" : "AngeBassa",
      "protected" : false,
      "id_str" : "937467860",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/925692072060956673\/-ZGUs27H_normal.jpg",
      "id" : 937467860,
      "verified" : false
    }
  },
  "id" : 839648730353631233,
  "created_at" : "2017-03-09 01:27:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/ygEcYOULYo",
      "expanded_url" : "https:\/\/angel.co\/profile_answers\/yap-shiao-shyan\/1",
      "display_url" : "angel.co\/profile_answer\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839541433099042819",
  "text" : "My answer to \"What's the most useful business-related book you've ever read?\" on AngelList https:\/\/t.co\/ygEcYOULYo",
  "id" : 839541433099042819,
  "created_at" : "2017-03-08 18:20:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IWD2017",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ {
      "indices" : [ 9, 32 ],
      "url" : "https:\/\/t.co\/bcMCWSoSF5",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/839453321173794818",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839532094468603905",
  "text" : "#IWD2017 https:\/\/t.co\/bcMCWSoSF5",
  "id" : 839532094468603905,
  "created_at" : "2017-03-08 17:43:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 3, 17 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/YEvhrnSm1e",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/financial-services\/aig-chooses-luxembourg-over-dublin-as-european-insurance-base-post-brexit-1.3002464?utm_source=dlvr.it&utm_medium=twitter",
      "display_url" : "irishtimes.com\/business\/finan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839524262914506754",
  "text" : "RT @IrishTimesBiz: AIG chooses Luxembourg over Dublin as European insurance base post Brexit https:\/\/t.co\/YEvhrnSm1e",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/YEvhrnSm1e",
        "expanded_url" : "http:\/\/www.irishtimes.com\/business\/financial-services\/aig-chooses-luxembourg-over-dublin-as-european-insurance-base-post-brexit-1.3002464?utm_source=dlvr.it&utm_medium=twitter",
        "display_url" : "irishtimes.com\/business\/finan\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839485457477976064",
    "text" : "AIG chooses Luxembourg over Dublin as European insurance base post Brexit https:\/\/t.co\/YEvhrnSm1e",
    "id" : 839485457477976064,
    "created_at" : "2017-03-08 14:38:24 +0000",
    "user" : {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "protected" : false,
      "id_str" : "16737418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/955744226062544898\/l9mPtwix_normal.jpg",
      "id" : 16737418,
      "verified" : true
    }
  },
  "id" : 839524262914506754,
  "created_at" : "2017-03-08 17:12:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/IDOnwZ1Llz",
      "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/839503936340045824",
      "display_url" : "twitter.com\/lisaocarroll\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839522724217307136",
  "text" : "Immigration should be controlled. https:\/\/t.co\/IDOnwZ1Llz",
  "id" : 839522724217307136,
  "created_at" : "2017-03-08 17:06:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/xwgvv1M99o",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BRYjPYagevs\/",
      "display_url" : "instagram.com\/p\/BRYjPYagevs\/"
    } ]
  },
  "geo" : { },
  "id_str" : "839510944086700032",
  "text" : "Beautiful day in Dublin @ Rathmines Omniplex https:\/\/t.co\/xwgvv1M99o",
  "id" : 839510944086700032,
  "created_at" : "2017-03-08 16:19:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/cDAxuAp4Rs",
      "expanded_url" : "http:\/\/shr.gs\/cmsVuTZ",
      "display_url" : "shr.gs\/cmsVuTZ"
    } ]
  },
  "geo" : { },
  "id_str" : "839473228993163265",
  "text" : "Dublin 'far from ready to face challenges of Brexit' https:\/\/t.co\/cDAxuAp4Rs",
  "id" : 839473228993163265,
  "created_at" : "2017-03-08 13:49:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Trinity College Dublin Global",
      "screen_name" : "tcdglobal",
      "indices" : [ 3, 13 ],
      "id_str" : "633723520",
      "id" : 633723520
    }, {
      "name" : "Trinity College Dublin",
      "screen_name" : "tcddublin",
      "indices" : [ 25, 35 ],
      "id_str" : "31537951",
      "id" : 31537951
    }, {
      "name" : "Nanyang Polytechnic",
      "screen_name" : "nyptweets",
      "indices" : [ 76, 86 ],
      "id_str" : "66295552",
      "id" : 66295552
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ThinkTrinity",
      "indices" : [ 87, 100 ]
    }, {
      "text" : "singapore",
      "indices" : [ 101, 111 ]
    }, {
      "text" : "trinitycollegedublin",
      "indices" : [ 112, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839465197714690049",
  "text" : "RT @tcdglobal: Colin the @tcddublin bear wishes he could join in the fun at @nyptweets #ThinkTrinity #singapore #trinitycollegedublin https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Trinity College Dublin",
        "screen_name" : "tcddublin",
        "indices" : [ 10, 20 ],
        "id_str" : "31537951",
        "id" : 31537951
      }, {
        "name" : "Nanyang Polytechnic",
        "screen_name" : "nyptweets",
        "indices" : [ 61, 71 ],
        "id_str" : "66295552",
        "id" : 66295552
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tcdglobal\/status\/839461612624961536\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/CLoZWh7N8d",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6Zc_wpXQAEf0Px.jpg",
        "id_str" : "839460631011082241",
        "id" : 839460631011082241,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6Zc_wpXQAEf0Px.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CLoZWh7N8d"
      } ],
      "hashtags" : [ {
        "text" : "ThinkTrinity",
        "indices" : [ 72, 85 ]
      }, {
        "text" : "singapore",
        "indices" : [ 86, 96 ]
      }, {
        "text" : "trinitycollegedublin",
        "indices" : [ 97, 118 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839461612624961536",
    "text" : "Colin the @tcddublin bear wishes he could join in the fun at @nyptweets #ThinkTrinity #singapore #trinitycollegedublin https:\/\/t.co\/CLoZWh7N8d",
    "id" : 839461612624961536,
    "created_at" : "2017-03-08 13:03:39 +0000",
    "user" : {
      "name" : "Trinity College Dublin Global",
      "screen_name" : "tcdglobal",
      "protected" : false,
      "id_str" : "633723520",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/907977677075881984\/x9Num5ac_normal.jpg",
      "id" : 633723520,
      "verified" : false
    }
  },
  "id" : 839465197714690049,
  "created_at" : "2017-03-08 13:17:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Life",
      "screen_name" : "IrishTimesLife",
      "indices" : [ 67, 82 ],
      "id_str" : "1114933915",
      "id" : 1114933915
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/PMwTzk4uzC",
      "expanded_url" : "http:\/\/www.irishtimes.com\/life-and-style\/people\/mums-get-a-lot-of-support-in-ireland-1.2992133#.WMAAM0rOsQE.twitter",
      "display_url" : "irishtimes.com\/life-and-style\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839460640330821632",
  "text" : "\u2018Mums get a lot of support in Ireland\u2019 https:\/\/t.co\/PMwTzk4uzC via @IrishTimesLife",
  "id" : 839460640330821632,
  "created_at" : "2017-03-08 12:59:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839453321173794818",
  "text" : "Honouring International Women's Day - this should be a gazetted public holiday today.",
  "id" : 839453321173794818,
  "created_at" : "2017-03-08 12:30:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/839426739440914432\/photo\/1",
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/Pok6OWywcV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6Y-Ju2WMAABKpL.jpg",
      "id_str" : "839426717466898432",
      "id" : 839426717466898432,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6Y-Ju2WMAABKpL.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Pok6OWywcV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839426739440914432",
  "text" : "Algorithms need managers, too. Harvard Business Review Jan Feb 2016 https:\/\/t.co\/Pok6OWywcV",
  "id" : 839426739440914432,
  "created_at" : "2017-03-08 10:45:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dmxdublin",
      "indices" : [ 22, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839412282979385344",
  "text" : "Wonder any pundits at #dmxdublin will utter \"TV is dead\" (again)?",
  "id" : 839412282979385344,
  "created_at" : "2017-03-08 09:47:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Krithika ram",
      "screen_name" : "krithikaram",
      "indices" : [ 3, 15 ],
      "id_str" : "287717601",
      "id" : 287717601
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/krithikaram\/status\/839402864170700800\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/EbsU7Y78Zc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6YoYeaWQAQI5kQ.jpg",
      "id_str" : "839402781496721412",
      "id" : 839402781496721412,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6YoYeaWQAQI5kQ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 3024,
        "resize" : "fit",
        "w" : 5376
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EbsU7Y78Zc"
    } ],
    "hashtags" : [ {
      "text" : "iwdccd",
      "indices" : [ 49, 56 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839410971550478336",
  "text" : "RT @krithikaram: Think Critically, Act Ethically #iwdccd https:\/\/t.co\/EbsU7Y78Zc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/krithikaram\/status\/839402864170700800\/photo\/1",
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/EbsU7Y78Zc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6YoYeaWQAQI5kQ.jpg",
        "id_str" : "839402781496721412",
        "id" : 839402781496721412,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6YoYeaWQAQI5kQ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 3024,
          "resize" : "fit",
          "w" : 5376
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EbsU7Y78Zc"
      } ],
      "hashtags" : [ {
        "text" : "iwdccd",
        "indices" : [ 32, 39 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839402864170700800",
    "text" : "Think Critically, Act Ethically #iwdccd https:\/\/t.co\/EbsU7Y78Zc",
    "id" : 839402864170700800,
    "created_at" : "2017-03-08 09:10:12 +0000",
    "user" : {
      "name" : "Krithika ram",
      "screen_name" : "krithikaram",
      "protected" : true,
      "id_str" : "287717601",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974239814605209600\/Y3qpUMrV_normal.jpg",
      "id" : 287717601,
      "verified" : false
    }
  },
  "id" : 839410971550478336,
  "created_at" : "2017-03-08 09:42:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839410335568248833",
  "text" : "Anyone giving away books related to your industry domain for lead generation? I want to try it. Any pitfall I should know?",
  "id" : 839410335568248833,
  "created_at" : "2017-03-08 09:39:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elusive Moose",
      "screen_name" : "elusive_moose",
      "indices" : [ 3, 17 ],
      "id_str" : "94125752",
      "id" : 94125752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839409932894097408",
  "text" : "RT @elusive_moose: Holmenkollen turns 125 years this year! The ski jump has been rebuilt many times since 1892, most recently in 2010 @Visi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "VisitOSLO",
        "screen_name" : "VisitOSLO",
        "indices" : [ 115, 125 ],
        "id_str" : "22660254",
        "id" : 22660254
      }, {
        "name" : "Holmenkollen",
        "screen_name" : "_Holmenkollen",
        "indices" : [ 126, 140 ],
        "id_str" : "335819930",
        "id" : 335819930
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/elusive_moose\/status\/839111427206045700\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/mAi687o3fc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6UfKeuXQAAj3ES.jpg",
        "id_str" : "839111170481143808",
        "id" : 839111170481143808,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6UfKeuXQAAj3ES.jpg",
        "sizes" : [ {
          "h" : 1050,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1050,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 788,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 446,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mAi687o3fc"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839111427206045700",
    "text" : "Holmenkollen turns 125 years this year! The ski jump has been rebuilt many times since 1892, most recently in 2010 @VisitOSLO @_Holmenkollen https:\/\/t.co\/mAi687o3fc",
    "id" : 839111427206045700,
    "created_at" : "2017-03-07 13:52:08 +0000",
    "user" : {
      "name" : "Elusive Moose",
      "screen_name" : "elusive_moose",
      "protected" : false,
      "id_str" : "94125752",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2740494266\/150b8ecda0a4a5f9b2b4583ef228b0cd_normal.png",
      "id" : 94125752,
      "verified" : false
    }
  },
  "id" : 839409932894097408,
  "created_at" : "2017-03-08 09:38:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DubEntWeek",
      "indices" : [ 9, 20 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839382884729384960",
  "text" : "Any free #DubEntWeek walk-in event this morning?",
  "id" : 839382884729384960,
  "created_at" : "2017-03-08 07:50:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/wwYsqU1uoC",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/bali-wont-cover-statues-saudi-king-065812256.html?soc_src=social-sh&soc_trk=tw",
      "display_url" : "sg.news.yahoo.com\/bali-wont-cove\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839382510777823232",
  "text" : "Bali won't cover up statues for this king https:\/\/t.co\/wwYsqU1uoC",
  "id" : 839382510777823232,
  "created_at" : "2017-03-08 07:49:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft Cloud Show",
      "screen_name" : "mscloudshow",
      "indices" : [ 3, 15 ],
      "id_str" : "1583094072",
      "id" : 1583094072
    }, {
      "name" : "Google",
      "screen_name" : "Google",
      "indices" : [ 44, 51 ],
      "id_str" : "20536157",
      "id" : 20536157
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mscloudshow\/status\/839377693456084992\/photo\/1",
      "indices" : [ 120, 143 ],
      "url" : "https:\/\/t.co\/NPbIf7AStJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6YRj6CWQAAB_rS.png",
      "id_str" : "839377689123373056",
      "id" : 839377689123373056,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6YRj6CWQAAB_rS.png",
      "sizes" : [ {
        "h" : 1600,
        "resize" : "fit",
        "w" : 1594
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1196
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1600,
        "resize" : "fit",
        "w" : 1594
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 677
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NPbIf7AStJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/BD6DxKTGkA",
      "expanded_url" : "http:\/\/buff.ly\/2ljGbpb",
      "display_url" : "buff.ly\/2ljGbpb"
    } ]
  },
  "geo" : { },
  "id_str" : "839379577331929088",
  "text" : "RT @mscloudshow: GPUs are now available for @Google Compute Engine &amp; Cloud Machine Learning https:\/\/t.co\/BD6DxKTGkA https:\/\/t.co\/NPbIf7AStJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 27, 34 ],
        "id_str" : "20536157",
        "id" : 20536157
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mscloudshow\/status\/839377693456084992\/photo\/1",
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/NPbIf7AStJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6YRj6CWQAAB_rS.png",
        "id_str" : "839377689123373056",
        "id" : 839377689123373056,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6YRj6CWQAAB_rS.png",
        "sizes" : [ {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1594
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1196
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1594
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 677
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NPbIf7AStJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/BD6DxKTGkA",
        "expanded_url" : "http:\/\/buff.ly\/2ljGbpb",
        "display_url" : "buff.ly\/2ljGbpb"
      } ]
    },
    "geo" : { },
    "id_str" : "839377693456084992",
    "text" : "GPUs are now available for @Google Compute Engine &amp; Cloud Machine Learning https:\/\/t.co\/BD6DxKTGkA https:\/\/t.co\/NPbIf7AStJ",
    "id" : 839377693456084992,
    "created_at" : "2017-03-08 07:30:11 +0000",
    "user" : {
      "name" : "Microsoft Cloud Show",
      "screen_name" : "mscloudshow",
      "protected" : false,
      "id_str" : "1583094072",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876426079648591873\/CdPtXCRI_normal.jpg",
      "id" : 1583094072,
      "verified" : false
    }
  },
  "id" : 839379577331929088,
  "created_at" : "2017-03-08 07:37:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DMXDublin",
      "indices" : [ 24, 34 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839378835393167360",
  "text" : "Anyone can't make it to #DMXDublin last minute?",
  "id" : 839378835393167360,
  "created_at" : "2017-03-08 07:34:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Existential Comics",
      "screen_name" : "existentialcoms",
      "indices" : [ 3, 19 ],
      "id_str" : "2163374389",
      "id" : 2163374389
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839280405878554624",
  "text" : "RT @existentialcoms: Socialists online: we want to build an all inclusive movement.\n\nExcept anyone who has even a slightly different opinio\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839225458965164032",
    "text" : "Socialists online: we want to build an all inclusive movement.\n\nExcept anyone who has even a slightly different opinion than us. Fuck them.",
    "id" : 839225458965164032,
    "created_at" : "2017-03-07 21:25:16 +0000",
    "user" : {
      "name" : "Existential Comics",
      "screen_name" : "existentialcoms",
      "protected" : false,
      "id_str" : "2163374389",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/792916557403795456\/d-iEnfPD_normal.jpg",
      "id" : 2163374389,
      "verified" : false
    }
  },
  "id" : 839280405878554624,
  "created_at" : "2017-03-08 01:03:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rita Panahi",
      "screen_name" : "RitaPanahi",
      "indices" : [ 3, 14 ],
      "id_str" : "25235151",
      "id" : 25235151
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/QoktsuA68f",
      "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/2017\/03\/07\/heinous-poachers-slay-vince-rhino-horn-inside-french-zoo-first\/",
      "display_url" : "telegraph.co.uk\/news\/2017\/03\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839233743118286849",
  "text" : "RT @RitaPanahi: Despicable. Not even safe in a zoo in Paris. \nhttps:\/\/t.co\/QoktsuA68f",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/QoktsuA68f",
        "expanded_url" : "http:\/\/www.telegraph.co.uk\/news\/2017\/03\/07\/heinous-poachers-slay-vince-rhino-horn-inside-french-zoo-first\/",
        "display_url" : "telegraph.co.uk\/news\/2017\/03\/0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839217109347422208",
    "text" : "Despicable. Not even safe in a zoo in Paris. \nhttps:\/\/t.co\/QoktsuA68f",
    "id" : 839217109347422208,
    "created_at" : "2017-03-07 20:52:05 +0000",
    "user" : {
      "name" : "Rita Panahi",
      "screen_name" : "RitaPanahi",
      "protected" : false,
      "id_str" : "25235151",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/901301264406896640\/Eis46gPG_normal.jpg",
      "id" : 25235151,
      "verified" : true
    }
  },
  "id" : 839233743118286849,
  "created_at" : "2017-03-07 21:58:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Georgina Cosma",
      "screen_name" : "gcosma1",
      "indices" : [ 3, 11 ],
      "id_str" : "224161598",
      "id" : 224161598
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839225079586246656",
  "text" : "RT @gcosma1: Am looking for someone with experience in Data Mining and creating Ontologies to start as soon as possible.\nhttps:\/\/t.co\/fCbfS\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/fCbfSi8CFe",
        "expanded_url" : "https:\/\/vacancies.ntu.ac.uk\/displayjob.aspx?jobid=3869",
        "display_url" : "vacancies.ntu.ac.uk\/displayjob.asp\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839221868464852992",
    "text" : "Am looking for someone with experience in Data Mining and creating Ontologies to start as soon as possible.\nhttps:\/\/t.co\/fCbfSi8CFe",
    "id" : 839221868464852992,
    "created_at" : "2017-03-07 21:11:00 +0000",
    "user" : {
      "name" : "Georgina Cosma",
      "screen_name" : "gcosma1",
      "protected" : false,
      "id_str" : "224161598",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1021696679643435009\/n0mhaVnq_normal.jpg",
      "id" : 224161598,
      "verified" : false
    }
  },
  "id" : 839225079586246656,
  "created_at" : "2017-03-07 21:23:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Rent Controller",
      "screen_name" : "RentController",
      "indices" : [ 3, 18 ],
      "id_str" : "4100470341",
      "id" : 4100470341
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/u6oBZcRIXB",
      "expanded_url" : "https:\/\/twitter.com\/ByRosenberg\/status\/839223116605739008",
      "display_url" : "twitter.com\/ByRosenberg\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839224771187453954",
  "text" : "RT @RentController: This is how you fix a housing crisis. BUILD MORE HOMES EVERYWHERE. https:\/\/t.co\/u6oBZcRIXB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/u6oBZcRIXB",
        "expanded_url" : "https:\/\/twitter.com\/ByRosenberg\/status\/839223116605739008",
        "display_url" : "twitter.com\/ByRosenberg\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839224375870164993",
    "text" : "This is how you fix a housing crisis. BUILD MORE HOMES EVERYWHERE. https:\/\/t.co\/u6oBZcRIXB",
    "id" : 839224375870164993,
    "created_at" : "2017-03-07 21:20:57 +0000",
    "user" : {
      "name" : "The Rent Controller",
      "screen_name" : "RentController",
      "protected" : false,
      "id_str" : "4100470341",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/663481026094698496\/OvypFW9I_normal.jpg",
      "id" : 4100470341,
      "verified" : false
    }
  },
  "id" : 839224771187453954,
  "created_at" : "2017-03-07 21:22:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "thebrandbuilder",
      "screen_name" : "JobsEnterInnov",
      "indices" : [ 3, 18 ],
      "id_str" : "878152342125686784",
      "id" : 878152342125686784
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839193779231928320",
  "text" : "RT @JobsEnterInnov: This portal helps with regulations affecting your business &amp; gives links to relevant agencies,tools &amp; contact points ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JobsEnterInnov\/status\/836155881662341120\/photo\/1",
        "indices" : [ 149, 172 ],
        "url" : "https:\/\/t.co\/gYYFA9lAWO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C5qeJ-0WUAA-lBz.jpg",
        "id_str" : "836154575149223936",
        "id" : 836154575149223936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C5qeJ-0WUAA-lBz.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 188,
          "resize" : "fit",
          "w" : 618
        }, {
          "h" : 188,
          "resize" : "fit",
          "w" : 618
        }, {
          "h" : 188,
          "resize" : "fit",
          "w" : 618
        }, {
          "h" : 188,
          "resize" : "fit",
          "w" : 618
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gYYFA9lAWO"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/rePBpkOshs",
        "expanded_url" : "http:\/\/www.businessregulation.ie",
        "display_url" : "businessregulation.ie"
      } ]
    },
    "geo" : { },
    "id_str" : "836155881662341120",
    "text" : "This portal helps with regulations affecting your business &amp; gives links to relevant agencies,tools &amp; contact points https:\/\/t.co\/rePBpkOshs https:\/\/t.co\/gYYFA9lAWO",
    "id" : 836155881662341120,
    "created_at" : "2017-02-27 10:07:51 +0000",
    "user" : {
      "name" : "Department of Business, Enterprise and Innovation",
      "screen_name" : "EnterInnov",
      "protected" : false,
      "id_str" : "144888395",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031485178575810560\/buY93aiW_normal.jpg",
      "id" : 144888395,
      "verified" : true
    }
  },
  "id" : 839193779231928320,
  "created_at" : "2017-03-07 19:19:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Plato Dublin",
      "screen_name" : "PlatoDublin",
      "indices" : [ 3, 15 ],
      "id_str" : "248590483",
      "id" : 248590483
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839188188279238657",
  "text" : "RT @PlatoDublin: Coming up: PLATO Data Protection \u2013What Your Business Needs to Know! 8am to 10am Thurs Mar 9,IBM Damastown. For info https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PlatoDublin\/status\/839086828992987138\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/Dq4hwAC2Fj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6UI2UcWMAEYig8.jpg",
        "id_str" : "839086634868027393",
        "id" : 839086634868027393,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6UI2UcWMAEYig8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 848,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 904,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 904,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Dq4hwAC2Fj"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/iTYT71HMxk",
        "expanded_url" : "https:\/\/goo.gl\/IZS2F0",
        "display_url" : "goo.gl\/IZS2F0"
      } ]
    },
    "geo" : { },
    "id_str" : "839086828992987138",
    "text" : "Coming up: PLATO Data Protection \u2013What Your Business Needs to Know! 8am to 10am Thurs Mar 9,IBM Damastown. For info https:\/\/t.co\/iTYT71HMxk https:\/\/t.co\/Dq4hwAC2Fj",
    "id" : 839086828992987138,
    "created_at" : "2017-03-07 12:14:24 +0000",
    "user" : {
      "name" : "Plato Dublin",
      "screen_name" : "PlatoDublin",
      "protected" : false,
      "id_str" : "248590483",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/702449712125886465\/9BvjMT9v_normal.jpg",
      "id" : 248590483,
      "verified" : false
    }
  },
  "id" : 839188188279238657,
  "created_at" : "2017-03-07 18:57:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BonVoyageurs",
      "screen_name" : "BonVoyageurs",
      "indices" : [ 3, 16 ],
      "id_str" : "49081015",
      "id" : 49081015
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "luxury",
      "indices" : [ 98, 105 ]
    }, {
      "text" : "travel",
      "indices" : [ 106, 113 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 114, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839178801988915201",
  "text" : "RT @BonVoyageurs: By law new buildings in Singapore must be green Rooftop gardens green facades ! #luxury #travel #Singapore https:\/\/t.co\/v\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/meetedgar.com\" rel=\"nofollow\"\u003EMeet Edgar\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BonVoyageurs\/status\/739950252141203456\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/v6b2nnMlxq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CkTU1Q2WkAIckYa.jpg",
        "id_str" : "739950250316697602",
        "id" : 739950250316697602,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CkTU1Q2WkAIckYa.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 599,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 509,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 599,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 599,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/v6b2nnMlxq"
      } ],
      "hashtags" : [ {
        "text" : "luxury",
        "indices" : [ 80, 87 ]
      }, {
        "text" : "travel",
        "indices" : [ 88, 95 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 96, 106 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "739950252141203456",
    "text" : "By law new buildings in Singapore must be green Rooftop gardens green facades ! #luxury #travel #Singapore https:\/\/t.co\/v6b2nnMlxq",
    "id" : 739950252141203456,
    "created_at" : "2016-06-06 22:41:02 +0000",
    "user" : {
      "name" : "BonVoyageurs",
      "screen_name" : "BonVoyageurs",
      "protected" : false,
      "id_str" : "49081015",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797169883385630720\/pxXcuvkO_normal.jpg",
      "id" : 49081015,
      "verified" : false
    }
  },
  "id" : 839178801988915201,
  "created_at" : "2017-03-07 18:19:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 0, 11 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "783765722455478276",
  "geo" : { },
  "id_str" : "839177641076928512",
  "in_reply_to_user_id" : 26903787,
  "text" : "@Dotnetster You can also use R too.",
  "id" : 839177641076928512,
  "in_reply_to_status_id" : 783765722455478276,
  "created_at" : "2017-03-07 18:15:15 +0000",
  "in_reply_to_screen_name" : "Dotnetster",
  "in_reply_to_user_id_str" : "26903787",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/HWuoPJD47q",
      "expanded_url" : "http:\/\/favourites.io\/home",
      "display_url" : "favourites.io\/home"
    } ]
  },
  "geo" : { },
  "id_str" : "839174207481200641",
  "text" : "Searching, organising &amp; sharing your twitter favourites is now easy and fun with favourites.io https:\/\/t.co\/HWuoPJD47q",
  "id" : 839174207481200641,
  "created_at" : "2017-03-07 18:01:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Google Webmasters",
      "screen_name" : "googlewmc",
      "indices" : [ 3, 13 ],
      "id_str" : "22046611",
      "id" : 22046611
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839167563242274817",
  "text" : "RT @googlewmc: Do you use the site: search operator? We\u2019d love to know how, why, and what info you'd like to see \u2014 tell us more: https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/googlewmc\/status\/838802701714620421\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/Xdr3mv9ijx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6QEmYNWAAAsJ5s.jpg",
        "id_str" : "838800487977713664",
        "id" : 838800487977713664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6QEmYNWAAAsJ5s.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 597
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 597
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 597
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 597
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Xdr3mv9ijx"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/7XptrrLQGU",
        "expanded_url" : "https:\/\/goo.gl\/wIBCml",
        "display_url" : "goo.gl\/wIBCml"
      } ]
    },
    "geo" : { },
    "id_str" : "838802701714620421",
    "text" : "Do you use the site: search operator? We\u2019d love to know how, why, and what info you'd like to see \u2014 tell us more: https:\/\/t.co\/7XptrrLQGU https:\/\/t.co\/Xdr3mv9ijx",
    "id" : 838802701714620421,
    "created_at" : "2017-03-06 17:25:22 +0000",
    "user" : {
      "name" : "Google Webmasters",
      "screen_name" : "googlewmc",
      "protected" : false,
      "id_str" : "22046611",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/433487860453212160\/9zYKnHeo_normal.png",
      "id" : 22046611,
      "verified" : true
    }
  },
  "id" : 839167563242274817,
  "created_at" : "2017-03-07 17:35:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 28 ],
      "url" : "https:\/\/t.co\/etkgH7ken0",
      "expanded_url" : "https:\/\/twitter.com\/googleanalytics\/status\/839159968129495041",
      "display_url" : "twitter.com\/googleanalytic\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839166356616581120",
  "text" : "Yes! https:\/\/t.co\/etkgH7ken0",
  "id" : 839166356616581120,
  "created_at" : "2017-03-07 17:30:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/v7eoOMrtPC",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-39190135",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839166155654852608",
  "text" : "BBC News - Yellow taxis 'have lower accident rate than blue ones' https:\/\/t.co\/v7eoOMrtPC",
  "id" : 839166155654852608,
  "created_at" : "2017-03-07 17:29:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sassygem",
      "screen_name" : "sassygem2",
      "indices" : [ 3, 13 ],
      "id_str" : "3251284802",
      "id" : 3251284802
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839133894972694528",
  "text" : "RT @sassygem2: You're all going to be someone's rare, medium or well done steak.\n\n-me as I passed by a cattle ranch.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "837124510327320576",
    "text" : "You're all going to be someone's rare, medium or well done steak.\n\n-me as I passed by a cattle ranch.",
    "id" : 837124510327320576,
    "created_at" : "2017-03-02 02:16:50 +0000",
    "user" : {
      "name" : "Sassygem",
      "screen_name" : "sassygem2",
      "protected" : false,
      "id_str" : "3251284802",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966128751443521536\/W1jCTNgt_normal.jpg",
      "id" : 3251284802,
      "verified" : false
    }
  },
  "id" : 839133894972694528,
  "created_at" : "2017-03-07 15:21:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839130718760546304",
  "text" : "Dear marketers, which do you think is important?",
  "id" : 839130718760546304,
  "created_at" : "2017-03-07 15:08:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/bFHrx9qGVc",
      "expanded_url" : "https:\/\/twitter.com\/Ina\/status\/829606626357346304",
      "display_url" : "twitter.com\/Ina\/status\/829\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839129062580838408",
  "text" : "and a politician wants to bring manufacturing back from China using labour to the rust belt .... https:\/\/t.co\/bFHrx9qGVc",
  "id" : 839129062580838408,
  "created_at" : "2017-03-07 15:02:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/zTl9LzEGW1",
      "expanded_url" : "https:\/\/twitter.com\/SuzieCoog\/status\/831666296379019270",
      "display_url" : "twitter.com\/SuzieCoog\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839128077426884608",
  "text" : "Slowly rise my hand up. https:\/\/t.co\/zTl9LzEGW1",
  "id" : 839128077426884608,
  "created_at" : "2017-03-07 14:58:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Strategyzer",
      "screen_name" : "strategyzer",
      "indices" : [ 3, 15 ],
      "id_str" : "1413587545",
      "id" : 1413587545
    }, {
      "name" : "Rita Gunther McGrath",
      "screen_name" : "rgmcgrath",
      "indices" : [ 95, 105 ],
      "id_str" : "25002538",
      "id" : 25002538
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/strategyzer\/status\/831844282524893185\/photo\/1",
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/hX6Mr8WlPF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C4tN9stUkAAoGSo.jpg",
      "id_str" : "831844278548598784",
      "id" : 831844278548598784,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C4tN9stUkAAoGSo.jpg",
      "sizes" : [ {
        "h" : 1800,
        "resize" : "fit",
        "w" : 2700
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1365,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hX6Mr8WlPF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839127698584797188",
  "text" : "RT @strategyzer: \"Don't put innovation units under powerful leaders of the existing business.\" @rgmcgrath https:\/\/t.co\/hX6Mr8WlPF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rita Gunther McGrath",
        "screen_name" : "rgmcgrath",
        "indices" : [ 78, 88 ],
        "id_str" : "25002538",
        "id" : 25002538
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/strategyzer\/status\/831844282524893185\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/hX6Mr8WlPF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C4tN9stUkAAoGSo.jpg",
        "id_str" : "831844278548598784",
        "id" : 831844278548598784,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C4tN9stUkAAoGSo.jpg",
        "sizes" : [ {
          "h" : 1800,
          "resize" : "fit",
          "w" : 2700
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hX6Mr8WlPF"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "831844282524893185",
    "text" : "\"Don't put innovation units under powerful leaders of the existing business.\" @rgmcgrath https:\/\/t.co\/hX6Mr8WlPF",
    "id" : 831844282524893185,
    "created_at" : "2017-02-15 12:35:06 +0000",
    "user" : {
      "name" : "Strategyzer",
      "screen_name" : "strategyzer",
      "protected" : false,
      "id_str" : "1413587545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/532206585246806016\/7CjqCfU__normal.png",
      "id" : 1413587545,
      "verified" : false
    }
  },
  "id" : 839127698584797188,
  "created_at" : "2017-03-07 14:56:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ciar\u00E1n McCann",
      "screen_name" : "C_McCann",
      "indices" : [ 3, 12 ],
      "id_str" : "162736486",
      "id" : 162736486
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/UPkrJYD36o",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/environment\/more-than-12-000-cyclists-a-day-commute-into-dublin-city-1.2982547#.WKxMxBcWAbM.twitter",
      "display_url" : "irishtimes.com\/news\/environme\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839126774848040960",
  "text" : "RT @C_McCann: More than 12,000 cyclists a day commute into Dublin city https:\/\/t.co\/UPkrJYD36o",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/UPkrJYD36o",
        "expanded_url" : "http:\/\/www.irishtimes.com\/news\/environment\/more-than-12-000-cyclists-a-day-commute-into-dublin-city-1.2982547#.WKxMxBcWAbM.twitter",
        "display_url" : "irishtimes.com\/news\/environme\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "834045264633720835",
    "text" : "More than 12,000 cyclists a day commute into Dublin city https:\/\/t.co\/UPkrJYD36o",
    "id" : 834045264633720835,
    "created_at" : "2017-02-21 14:21:01 +0000",
    "user" : {
      "name" : "Ciar\u00E1n McCann",
      "screen_name" : "C_McCann",
      "protected" : false,
      "id_str" : "162736486",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/529309198870261760\/n_jVqcde_normal.jpeg",
      "id" : 162736486,
      "verified" : false
    }
  },
  "id" : 839126774848040960,
  "created_at" : "2017-03-07 14:53:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/839125612673499136\/photo\/1",
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/X6BQduj5mY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6UsFFVXQAAnmeS.jpg",
      "id_str" : "839125371417214976",
      "id" : 839125371417214976,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6UsFFVXQAAnmeS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 268,
        "resize" : "fit",
        "w" : 596
      }, {
        "h" : 268,
        "resize" : "fit",
        "w" : 596
      }, {
        "h" : 268,
        "resize" : "fit",
        "w" : 596
      }, {
        "h" : 268,
        "resize" : "fit",
        "w" : 596
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/X6BQduj5mY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839125612673499136",
  "text" : "Pet peeve: share a link on Twitter with third party tool that you have to authorize to view it. What the point? https:\/\/t.co\/X6BQduj5mY",
  "id" : 839125612673499136,
  "created_at" : "2017-03-07 14:48:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ADWEAK",
      "screen_name" : "adweak",
      "indices" : [ 3, 10 ],
      "id_str" : "37009612",
      "id" : 37009612
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839121649341460480",
  "text" : "RT @adweak: BREAKING: Brands Need To Shift More Dollars To Digital, Social According To Digital, Social Agencies",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "836744176364482560",
    "text" : "BREAKING: Brands Need To Shift More Dollars To Digital, Social According To Digital, Social Agencies",
    "id" : 836744176364482560,
    "created_at" : "2017-03-01 01:05:32 +0000",
    "user" : {
      "name" : "ADWEAK",
      "screen_name" : "adweak",
      "protected" : false,
      "id_str" : "37009612",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030164750326411265\/Zx6ar39Y_normal.jpg",
      "id" : 37009612,
      "verified" : false
    }
  },
  "id" : 839121649341460480,
  "created_at" : "2017-03-07 14:32:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vishnu N",
      "screen_name" : "drvnanduri",
      "indices" : [ 3, 14 ],
      "id_str" : "202738150",
      "id" : 202738150
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Blockchain",
      "indices" : [ 46, 57 ]
    } ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/nFn4UgFkMd",
      "expanded_url" : "https:\/\/twitter.com\/spirosmargaris\/status\/836813868471107584",
      "display_url" : "twitter.com\/spirosmargaris\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839121532089745408",
  "text" : "RT @drvnanduri: Quite a simple explanation of #Blockchain https:\/\/t.co\/nFn4UgFkMd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Blockchain",
        "indices" : [ 30, 41 ]
      } ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/nFn4UgFkMd",
        "expanded_url" : "https:\/\/twitter.com\/spirosmargaris\/status\/836813868471107584",
        "display_url" : "twitter.com\/spirosmargaris\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "836814846951829506",
    "text" : "Quite a simple explanation of #Blockchain https:\/\/t.co\/nFn4UgFkMd",
    "id" : 836814846951829506,
    "created_at" : "2017-03-01 05:46:21 +0000",
    "user" : {
      "name" : "Vishnu N",
      "screen_name" : "drvnanduri",
      "protected" : false,
      "id_str" : "202738150",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/682627066928365568\/3S5RzvNl_normal.jpg",
      "id" : 202738150,
      "verified" : false
    }
  },
  "id" : 839121532089745408,
  "created_at" : "2017-03-07 14:32:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The CQI",
      "screen_name" : "Qualityworld",
      "indices" : [ 3, 16 ],
      "id_str" : "41105954",
      "id" : 41105954
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/FixKoZNVDA",
      "expanded_url" : "https:\/\/twitter.com\/mitsmr\/status\/838510704172941317",
      "display_url" : "twitter.com\/mitsmr\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839120687604314112",
  "text" : "RT @Qualityworld: Which model reflects your career? https:\/\/t.co\/FixKoZNVDA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/FixKoZNVDA",
        "expanded_url" : "https:\/\/twitter.com\/mitsmr\/status\/838510704172941317",
        "display_url" : "twitter.com\/mitsmr\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "838807316086546434",
    "text" : "Which model reflects your career? https:\/\/t.co\/FixKoZNVDA",
    "id" : 838807316086546434,
    "created_at" : "2017-03-06 17:43:43 +0000",
    "user" : {
      "name" : "The CQI",
      "screen_name" : "Qualityworld",
      "protected" : false,
      "id_str" : "41105954",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/704229668807905280\/LYfdD6sA_normal.jpg",
      "id" : 41105954,
      "verified" : false
    }
  },
  "id" : 839120687604314112,
  "created_at" : "2017-03-07 14:28:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 49, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/3S3XKGCgBj",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/make-every-customer-interaction-more-profitable-shiao-shyan-yap-%E5%8F%B6%E6%99%93%E8%BB%92",
      "display_url" : "linkedin.com\/pulse\/make-eve\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839112334236876801",
  "text" : "Make every customer interaction more profitable. #getoptimise https:\/\/t.co\/3S3XKGCgBj",
  "id" : 839112334236876801,
  "created_at" : "2017-03-07 13:55:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Connolly",
      "screen_name" : "medavidconnolly",
      "indices" : [ 3, 19 ],
      "id_str" : "532578930",
      "id" : 532578930
    }, {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "indices" : [ 113, 121 ],
      "id_str" : "137327916",
      "id" : 137327916
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DubEntWeek",
      "indices" : [ 91, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839101680956366849",
  "text" : "RT @medavidconnolly: Last remaining spots for Start Your Own Business Bootcamp Day run for #DubEntWeek on Sat in @GECinD8 check out: https:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The GEC",
        "screen_name" : "GECinD8",
        "indices" : [ 92, 100 ],
        "id_str" : "137327916",
        "id" : 137327916
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/medavidconnolly\/status\/839100045173587968\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/qFDNtwMQzm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6US1hHXEAMOVQL.jpg",
        "id_str" : "839097616206073859",
        "id" : 839097616206073859,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6US1hHXEAMOVQL.jpg",
        "sizes" : [ {
          "h" : 260,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 486,
          "resize" : "fit",
          "w" : 1271
        }, {
          "h" : 486,
          "resize" : "fit",
          "w" : 1271
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qFDNtwMQzm"
      } ],
      "hashtags" : [ {
        "text" : "DubEntWeek",
        "indices" : [ 70, 81 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/BpIVWJ8hmN",
        "expanded_url" : "http:\/\/bit.ly\/2m2Q6kt",
        "display_url" : "bit.ly\/2m2Q6kt"
      } ]
    },
    "geo" : { },
    "id_str" : "839100045173587968",
    "text" : "Last remaining spots for Start Your Own Business Bootcamp Day run for #DubEntWeek on Sat in @GECinD8 check out: https:\/\/t.co\/BpIVWJ8hmN https:\/\/t.co\/qFDNtwMQzm",
    "id" : 839100045173587968,
    "created_at" : "2017-03-07 13:06:55 +0000",
    "user" : {
      "name" : "David Connolly",
      "screen_name" : "medavidconnolly",
      "protected" : false,
      "id_str" : "532578930",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/817306164966916096\/QxInRHjR_normal.jpg",
      "id" : 532578930,
      "verified" : false
    }
  },
  "id" : 839101680956366849,
  "created_at" : "2017-03-07 13:13:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Data Scientists IRL",
      "screen_name" : "DataSci_Ireland",
      "indices" : [ 3, 19 ],
      "id_str" : "2813354394",
      "id" : 2813354394
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Statistics",
      "indices" : [ 77, 88 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839077038233370624",
  "text" : "RT @DataSci_Ireland: Save the dates - CASI 2017 - 37th Conference on Applied #Statistics in Ireland\nMullingar, Westmeath 15-17 May, 2017 ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twuffer.com\" rel=\"nofollow\"\u003ETwuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Statistics",
        "indices" : [ 56, 67 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/fe4ye0m2yg",
        "expanded_url" : "http:\/\/www.casi.ie\/CASI_2017\/index.html",
        "display_url" : "casi.ie\/CASI_2017\/inde\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "839070648379060224",
    "text" : "Save the dates - CASI 2017 - 37th Conference on Applied #Statistics in Ireland\nMullingar, Westmeath 15-17 May, 2017 https:\/\/t.co\/fe4ye0m2yg",
    "id" : 839070648379060224,
    "created_at" : "2017-03-07 11:10:06 +0000",
    "user" : {
      "name" : "Data Scientists IRL",
      "screen_name" : "DataSci_Ireland",
      "protected" : false,
      "id_str" : "2813354394",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/597764057836494849\/w2Hts-sb_normal.jpg",
      "id" : 2813354394,
      "verified" : false
    }
  },
  "id" : 839077038233370624,
  "created_at" : "2017-03-07 11:35:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "irishbizparty",
      "indices" : [ 54, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/YGT0NvGdur",
      "expanded_url" : "https:\/\/twitter.com\/Tweetinggoddess\/status\/838768316537843713",
      "display_url" : "twitter.com\/Tweetinggoddes\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "839072719010414593",
  "text" : "All the best Samantha! You have come a long way since #irishbizparty on Twitter https:\/\/t.co\/YGT0NvGdur",
  "id" : 839072719010414593,
  "created_at" : "2017-03-07 11:18:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "EVENTSinFingal",
      "screen_name" : "EVENTSinFingal",
      "indices" : [ 3, 18 ],
      "id_str" : "372724703",
      "id" : 372724703
    }, {
      "name" : "Fingal Emergency Management",
      "screen_name" : "FingalEMU",
      "indices" : [ 24, 34 ],
      "id_str" : "4822108360",
      "id" : 4822108360
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839072038774009856",
  "text" : "RT @EVENTSinFingal: Our @FingalEMU Emergency Management Unit provides essential supports at our events and are ready 24\/7 to respond #Resil\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Fingal Emergency Management",
        "screen_name" : "FingalEMU",
        "indices" : [ 4, 14 ],
        "id_str" : "4822108360",
        "id" : 4822108360
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EVENTSinFingal\/status\/839069132247138304\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/hbYEvdYsA0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6T4yk2WUAAI9VQ.jpg",
        "id_str" : "839068978366533632",
        "id" : 839068978366533632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6T4yk2WUAAI9VQ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 356,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 658,
          "resize" : "fit",
          "w" : 1256
        }, {
          "h" : 629,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 658,
          "resize" : "fit",
          "w" : 1256
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hbYEvdYsA0"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/EVENTSinFingal\/status\/839069132247138304\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/hbYEvdYsA0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6T40xEXEAAvVro.jpg",
        "id_str" : "839069016006266880",
        "id" : 839069016006266880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6T40xEXEAAvVro.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hbYEvdYsA0"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/EVENTSinFingal\/status\/839069132247138304\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/hbYEvdYsA0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6T43-PXMAE8ZCk.jpg",
        "id_str" : "839069071081680897",
        "id" : 839069071081680897,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6T43-PXMAE8ZCk.jpg",
        "sizes" : [ {
          "h" : 252,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 851
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 851
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 851
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hbYEvdYsA0"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/EVENTSinFingal\/status\/839069132247138304\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/hbYEvdYsA0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6T45WWWUAAo4_h.jpg",
        "id_str" : "839069094733303808",
        "id" : 839069094733303808,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6T45WWWUAAo4_h.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 689
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 390
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1176
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1176
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hbYEvdYsA0"
      } ],
      "hashtags" : [ {
        "text" : "Resilience",
        "indices" : [ 113, 124 ]
      }, {
        "text" : "OurCouncilDay",
        "indices" : [ 125, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "839069132247138304",
    "text" : "Our @FingalEMU Emergency Management Unit provides essential supports at our events and are ready 24\/7 to respond #Resilience #OurCouncilDay https:\/\/t.co\/hbYEvdYsA0",
    "id" : 839069132247138304,
    "created_at" : "2017-03-07 11:04:04 +0000",
    "user" : {
      "name" : "EVENTSinFingal",
      "screen_name" : "EVENTSinFingal",
      "protected" : false,
      "id_str" : "372724703",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030063623861952512\/PFfKq9Js_normal.jpg",
      "id" : 372724703,
      "verified" : false
    }
  },
  "id" : 839072038774009856,
  "created_at" : "2017-03-07 11:15:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Will Goodbody",
      "screen_name" : "willgoodbody",
      "indices" : [ 3, 16 ],
      "id_str" : "107100041",
      "id" : 107100041
    }, {
      "name" : "Ulster Bank",
      "screen_name" : "UlsterBank",
      "indices" : [ 95, 106 ],
      "id_str" : "2473800468",
      "id" : 2473800468
    }, {
      "name" : "KBC Customer Service",
      "screen_name" : "AskKBCIreland",
      "indices" : [ 113, 127 ],
      "id_str" : "1702103214",
      "id" : 1702103214
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839054706118955008",
  "text" : "RT @willgoodbody: BREAKING: Apple's announced Apple Pay is available in Ireland from today for @ulsterbank &amp; @AskKBCIreland customers. Here\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ulster Bank",
        "screen_name" : "UlsterBank",
        "indices" : [ 77, 88 ],
        "id_str" : "2473800468",
        "id" : 2473800468
      }, {
        "name" : "KBC Customer Service",
        "screen_name" : "AskKBCIreland",
        "indices" : [ 95, 109 ],
        "id_str" : "1702103214",
        "id" : 1702103214
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/willgoodbody\/status\/838992632852303872\/video\/1",
        "indices" : [ 142, 165 ],
        "url" : "https:\/\/t.co\/ckC84mzH6X",
        "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/838777661493297152\/img\/bVnCrAO5VMjHSOid.jpg",
        "id_str" : "838777661493297152",
        "id" : 838777661493297152,
        "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/838777661493297152\/img\/bVnCrAO5VMjHSOid.jpg",
        "sizes" : [ {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ckC84mzH6X"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "838992632852303872",
    "text" : "BREAKING: Apple's announced Apple Pay is available in Ireland from today for @ulsterbank &amp; @AskKBCIreland customers. Here's how it works: https:\/\/t.co\/ckC84mzH6X",
    "id" : 838992632852303872,
    "created_at" : "2017-03-07 06:00:06 +0000",
    "user" : {
      "name" : "Will Goodbody",
      "screen_name" : "willgoodbody",
      "protected" : false,
      "id_str" : "107100041",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1036158717912403968\/cBpjloId_normal.jpg",
      "id" : 107100041,
      "verified" : true
    }
  },
  "id" : 839054706118955008,
  "created_at" : "2017-03-07 10:06:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/QeMTgFxaxi",
      "expanded_url" : "http:\/\/bit.ly\/2mR7GIS",
      "display_url" : "bit.ly\/2mR7GIS"
    } ]
  },
  "geo" : { },
  "id_str" : "839052397293027328",
  "text" : "https:\/\/t.co\/QeMTgFxaxi &lt;- why isn't there vetting for foreign national coming to Singapore?",
  "id" : 839052397293027328,
  "created_at" : "2017-03-07 09:57:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/38jPLQq4Zr",
      "expanded_url" : "http:\/\/bit.ly\/2n0js0e",
      "display_url" : "bit.ly\/2n0js0e"
    } ]
  },
  "geo" : { },
  "id_str" : "839051751814750208",
  "text" : "Brilliant! Four new libraries to open in Singapore this year https:\/\/t.co\/38jPLQq4Zr",
  "id" : 839051751814750208,
  "created_at" : "2017-03-07 09:55:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839048362112462849",
  "text" : "Maybe the church can consider giving up on school, hospital and ovaries for lent?",
  "id" : 839048362112462849,
  "created_at" : "2017-03-07 09:41:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FirstTweet",
      "indices" : [ 11, 22 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/w9I5ELPHR9",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/statuses\/338355162",
      "display_url" : "twitter.com\/mryap\/statuses\u2026"
    }, {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/U5PZs8xHSI",
      "expanded_url" : "http:\/\/first-tweets.com",
      "display_url" : "first-tweets.com"
    } ]
  },
  "geo" : { },
  "id_str" : "839013884069572609",
  "text" : "I found my #FirstTweet: https:\/\/t.co\/w9I5ELPHR9. What was yours? https:\/\/t.co\/U5PZs8xHSI",
  "id" : 839013884069572609,
  "created_at" : "2017-03-07 07:24:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "El_Poderoso",
      "screen_name" : "ronanodowd",
      "indices" : [ 3, 14 ],
      "id_str" : "21014709",
      "id" : 21014709
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CBLive",
      "indices" : [ 130, 137 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "839008882689261568",
  "text" : "RT @ronanodowd: Time to get the Catholic Church out of our schools, our hospitals, our bodies. Complete separation Church\/State. \n#CBLive",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CBLive",
        "indices" : [ 114, 121 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "838887352697241600",
    "text" : "Time to get the Catholic Church out of our schools, our hospitals, our bodies. Complete separation Church\/State. \n#CBLive",
    "id" : 838887352697241600,
    "created_at" : "2017-03-06 23:01:45 +0000",
    "user" : {
      "name" : "El_Poderoso",
      "screen_name" : "ronanodowd",
      "protected" : false,
      "id_str" : "21014709",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002520363375394816\/PNFga0Xx_normal.jpg",
      "id" : 21014709,
      "verified" : false
    }
  },
  "id" : 839008882689261568,
  "created_at" : "2017-03-07 07:04:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MITSloan Mgmt Review",
      "screen_name" : "mitsmr",
      "indices" : [ 83, 90 ],
      "id_str" : "15692087",
      "id" : 15692087
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/Eer6ayviHv",
      "expanded_url" : "http:\/\/mitsmr.com\/2coIYb4",
      "display_url" : "mitsmr.com\/2coIYb4"
    } ]
  },
  "geo" : { },
  "id_str" : "838833638758313985",
  "text" : "Designing and Developing Analytics-Based Data Products https:\/\/t.co\/Eer6ayviHv via @mitsmr",
  "id" : 838833638758313985,
  "created_at" : "2017-03-06 19:28:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/hIIUVM8D5X",
      "expanded_url" : "http:\/\/po.st\/HGe8Bn",
      "display_url" : "po.st\/HGe8Bn"
    } ]
  },
  "geo" : { },
  "id_str" : "838826500707209217",
  "text" : "500 years ago, China destroyed its world-dominating navy because its political elite was afraid of free tr... https:\/\/t.co\/hIIUVM8D5X",
  "id" : 838826500707209217,
  "created_at" : "2017-03-06 18:59:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/1iuVMvE4YU",
      "expanded_url" : "https:\/\/shar.es\/1UvDvf",
      "display_url" : "shar.es\/1UvDvf"
    } ]
  },
  "geo" : { },
  "id_str" : "838766625373450240",
  "text" : "Local Enterprise Week - Start Your Own Business Bootcamp - Local Enterprise Office - DublinCity https:\/\/t.co\/1iuVMvE4YU",
  "id" : 838766625373450240,
  "created_at" : "2017-03-06 15:02:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Tech Summit",
      "screen_name" : "DubTechSummit",
      "indices" : [ 0, 14 ],
      "id_str" : "3832562849",
      "id" : 3832562849
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/NRZyTVwtYP",
      "expanded_url" : "https:\/\/www.linkedin.com\/groups\/8592900",
      "display_url" : "linkedin.com\/groups\/8592900"
    } ]
  },
  "in_reply_to_status_id_str" : "838335786302894081",
  "geo" : { },
  "id_str" : "838752487448408069",
  "in_reply_to_user_id" : 3832562849,
  "text" : "@DubTechSummit we are connecting on LinkedIn here. https:\/\/t.co\/NRZyTVwtYP",
  "id" : 838752487448408069,
  "in_reply_to_status_id" : 838335786302894081,
  "created_at" : "2017-03-06 14:05:50 +0000",
  "in_reply_to_screen_name" : "DubTechSummit",
  "in_reply_to_user_id_str" : "3832562849",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/EiM4IdyLc1",
      "expanded_url" : "https:\/\/twitter.com\/RichieMcCormack\/status\/837956547527467009",
      "display_url" : "twitter.com\/RichieMcCormac\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838732621014269952",
  "text" : "\uD83D\uDE02 https:\/\/t.co\/EiM4IdyLc1",
  "id" : 838732621014269952,
  "created_at" : "2017-03-06 12:46:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/838731931546173441\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/MZMycHVwNr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6PF75LWYAEnRPC.jpg",
      "id_str" : "838731588372422657",
      "id" : 838731588372422657,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6PF75LWYAEnRPC.jpg",
      "sizes" : [ {
        "h" : 544,
        "resize" : "fit",
        "w" : 681
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 544,
        "resize" : "fit",
        "w" : 681
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 544,
        "resize" : "fit",
        "w" : 681
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MZMycHVwNr"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/838731931546173441\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/MZMycHVwNr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6PF9TWWAAARVS_.jpg",
      "id_str" : "838731612577726464",
      "id" : 838731612577726464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6PF9TWWAAARVS_.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 432,
        "resize" : "fit",
        "w" : 764
      }, {
        "h" : 432,
        "resize" : "fit",
        "w" : 764
      }, {
        "h" : 385,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 432,
        "resize" : "fit",
        "w" : 764
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MZMycHVwNr"
    } ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 72, 84 ]
    } ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/8NtLOXd7bJ",
      "expanded_url" : "http:\/\/getoptimise.com\/blog\/data-science\/unsupervised-machine-learning-101\/",
      "display_url" : "getoptimise.com\/blog\/data-scie\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838731931546173441",
  "text" : "My blog post: Unsupervised Machine Learning 101 https:\/\/t.co\/8NtLOXd7bJ #getoptimise https:\/\/t.co\/MZMycHVwNr",
  "id" : 838731931546173441,
  "created_at" : "2017-03-06 12:44:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/838693507032285184\/photo\/1",
      "indices" : [ 138, 161 ],
      "url" : "https:\/\/t.co\/XpzQDJHPWm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6OjAbmWMAAuNMJ.jpg",
      "id_str" : "838693183424966656",
      "id" : 838693183424966656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6OjAbmWMAAuNMJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 365,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 591,
        "resize" : "fit",
        "w" : 1102
      }, {
        "h" : 591,
        "resize" : "fit",
        "w" : 1102
      }, {
        "h" : 591,
        "resize" : "fit",
        "w" : 1102
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XpzQDJHPWm"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/qDekvrtwLq",
      "expanded_url" : "https:\/\/t.co",
      "display_url" : "t.co"
    } ]
  },
  "geo" : { },
  "id_str" : "838693507032285184",
  "text" : "Issue of using https:\/\/t.co\/qDekvrtwLq - Twitter's Shortened Link shown as referral source in Google Analytics without tagging them first https:\/\/t.co\/XpzQDJHPWm",
  "id" : 838693507032285184,
  "created_at" : "2017-03-06 10:11:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "indices" : [ 3, 15 ],
      "id_str" : "21564577",
      "id" : 21564577
    }, {
      "name" : "Arralis",
      "screen_name" : "arralis",
      "indices" : [ 46, 54 ],
      "id_str" : "211097333",
      "id" : 211097333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/wNPLOzqiJr",
      "expanded_url" : "http:\/\/www.thetimes.co.uk\/article\/aea0478c-00df-11e7-8489-aa00e6d4223d",
      "display_url" : "thetimes.co.uk\/article\/aea047\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838683808249561088",
  "text" : "RT @chris_byrne: Limerick Satellite tech firm @arralis in 40m China deal https:\/\/t.co\/wNPLOzqiJr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Arralis",
        "screen_name" : "arralis",
        "indices" : [ 29, 37 ],
        "id_str" : "211097333",
        "id" : 211097333
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/wNPLOzqiJr",
        "expanded_url" : "http:\/\/www.thetimes.co.uk\/article\/aea0478c-00df-11e7-8489-aa00e6d4223d",
        "display_url" : "thetimes.co.uk\/article\/aea047\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "838396209429688321",
    "text" : "Limerick Satellite tech firm @arralis in 40m China deal https:\/\/t.co\/wNPLOzqiJr",
    "id" : 838396209429688321,
    "created_at" : "2017-03-05 14:30:07 +0000",
    "user" : {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "protected" : false,
      "id_str" : "21564577",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028212548859043840\/9i6LBuYR_normal.jpg",
      "id" : 21564577,
      "verified" : false
    }
  },
  "id" : 838683808249561088,
  "created_at" : "2017-03-06 09:32:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Enterprise Nation",
      "screen_name" : "e_nation",
      "indices" : [ 3, 12 ],
      "id_str" : "38157321",
      "id" : 38157321
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ecommerce",
      "indices" : [ 97, 107 ]
    }, {
      "text" : "AmazonAcademy",
      "indices" : [ 114, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/2fouDADkOB",
      "expanded_url" : "http:\/\/buff.ly\/2lJqZio",
      "display_url" : "buff.ly\/2lJqZio"
    } ]
  },
  "geo" : { },
  "id_str" : "838551890505908228",
  "text" : "RT @e_nation: Amazon Academy expands to Manchester https:\/\/t.co\/2fouDADkOB Register for the free #ecommerce event #AmazonAcademy #Mancheste\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/e_nation\/status\/838430476385857537\/photo\/1",
        "indices" : [ 127, 150 ],
        "url" : "https:\/\/t.co\/Z9FzPy47Ze",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6K0EuwU0AM_gFo.jpg",
        "id_str" : "838430474007531523",
        "id" : 838430474007531523,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6K0EuwU0AM_gFo.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 962
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 962
        }, {
          "h" : 376,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 962
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Z9FzPy47Ze"
      } ],
      "hashtags" : [ {
        "text" : "ecommerce",
        "indices" : [ 83, 93 ]
      }, {
        "text" : "AmazonAcademy",
        "indices" : [ 100, 114 ]
      }, {
        "text" : "Manchester",
        "indices" : [ 115, 126 ]
      } ],
      "urls" : [ {
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/2fouDADkOB",
        "expanded_url" : "http:\/\/buff.ly\/2lJqZio",
        "display_url" : "buff.ly\/2lJqZio"
      } ]
    },
    "geo" : { },
    "id_str" : "838430476385857537",
    "text" : "Amazon Academy expands to Manchester https:\/\/t.co\/2fouDADkOB Register for the free #ecommerce event #AmazonAcademy #Manchester https:\/\/t.co\/Z9FzPy47Ze",
    "id" : 838430476385857537,
    "created_at" : "2017-03-05 16:46:17 +0000",
    "user" : {
      "name" : "Enterprise Nation",
      "screen_name" : "e_nation",
      "protected" : false,
      "id_str" : "38157321",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/598128530577293312\/pJbY2i5r_normal.jpg",
      "id" : 38157321,
      "verified" : false
    }
  },
  "id" : 838551890505908228,
  "created_at" : "2017-03-06 00:48:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AdrienneJ",
      "screen_name" : "AdrienneJoCo",
      "indices" : [ 3, 16 ],
      "id_str" : "55014247",
      "id" : 55014247
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "838542645840465920",
  "text" : "RT @AdrienneJoCo: Among the hardest parts of Mam's work: the thinly veiled or silent local hostility in Tuam to this situation being uncove\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tuambabies",
        "indices" : [ 125, 136 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "837948981737062401",
    "geo" : { },
    "id_str" : "837950001162645504",
    "in_reply_to_user_id" : 55014247,
    "text" : "Among the hardest parts of Mam's work: the thinly veiled or silent local hostility in Tuam to this situation being uncovered #tuambabies",
    "id" : 837950001162645504,
    "in_reply_to_status_id" : 837948981737062401,
    "created_at" : "2017-03-04 08:57:03 +0000",
    "in_reply_to_screen_name" : "AdrienneJoCo",
    "in_reply_to_user_id_str" : "55014247",
    "user" : {
      "name" : "AdrienneJ",
      "screen_name" : "AdrienneJoCo",
      "protected" : false,
      "id_str" : "55014247",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991613401985003521\/yuO-poN3_normal.jpg",
      "id" : 55014247,
      "verified" : false
    }
  },
  "id" : 838542645840465920,
  "created_at" : "2017-03-06 00:12:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/RfsGIN2ZJQ",
      "expanded_url" : "http:\/\/www.scmp.com\/week-asia\/geopolitics\/article\/2073483\/brexit-britain-missing-asia-policy",
      "display_url" : "scmp.com\/week-asia\/geop\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838476028523708416",
  "text" : "Brexit Britain is missing an Asia policy https:\/\/t.co\/RfsGIN2ZJQ",
  "id" : 838476028523708416,
  "created_at" : "2017-03-05 19:47:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LEO DLR",
      "screen_name" : "leo_dlr",
      "indices" : [ 39, 47 ],
      "id_str" : "38725814",
      "id" : 38725814
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "entweekdlr",
      "indices" : [ 82, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/ewNjmPqLXD",
      "expanded_url" : "http:\/\/www.dunlaoghaire.ie\/event\/dlr-enterprise-week-2017\/2017-12-03\/",
      "display_url" : "dunlaoghaire.ie\/event\/dlr-ente\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838472067712434177",
  "text" : "RT @DigiD_L: The Local Enterprise week @leo_dlr will take place from Tomorrow on! #entweekdlr https:\/\/t.co\/ewNjmPqLXD https:\/\/t.co\/8rJryqxn\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "LEO DLR",
        "screen_name" : "leo_dlr",
        "indices" : [ 26, 34 ],
        "id_str" : "38725814",
        "id" : 38725814
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DigiD_L\/status\/838449035799330817\/photo\/1",
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/8rJryqxnxe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C5__ZlDWUAE_but.jpg",
        "id_str" : "837668870622171137",
        "id" : 837668870622171137,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C5__ZlDWUAE_but.jpg",
        "sizes" : [ {
          "h" : 264,
          "resize" : "fit",
          "w" : 692
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 264,
          "resize" : "fit",
          "w" : 692
        }, {
          "h" : 259,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 264,
          "resize" : "fit",
          "w" : 692
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8rJryqxnxe"
      } ],
      "hashtags" : [ {
        "text" : "entweekdlr",
        "indices" : [ 69, 80 ]
      } ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/ewNjmPqLXD",
        "expanded_url" : "http:\/\/www.dunlaoghaire.ie\/event\/dlr-enterprise-week-2017\/2017-12-03\/",
        "display_url" : "dunlaoghaire.ie\/event\/dlr-ente\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "838449035799330817",
    "text" : "The Local Enterprise week @leo_dlr will take place from Tomorrow on! #entweekdlr https:\/\/t.co\/ewNjmPqLXD https:\/\/t.co\/8rJryqxnxe",
    "id" : 838449035799330817,
    "created_at" : "2017-03-05 18:00:02 +0000",
    "user" : {
      "name" : "Digital Dun Laoghaire Town",
      "screen_name" : "DigitalDLT",
      "protected" : false,
      "id_str" : "779071402397802496",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948842534712893443\/LxKiuoNe_normal.jpg",
      "id" : 779071402397802496,
      "verified" : false
    }
  },
  "id" : 838472067712434177,
  "created_at" : "2017-03-05 19:31:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/cMreqimmme",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BRQuY8HgwI2\/",
      "display_url" : "instagram.com\/p\/BRQuY8HgwI2\/"
    } ]
  },
  "geo" : { },
  "id_str" : "838409563145658369",
  "text" : "Does this bear resemble to the Firefox    (browser) logo? https:\/\/t.co\/cMreqimmme",
  "id" : 838409563145658369,
  "created_at" : "2017-03-05 15:23:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/xG0ez6FZJN",
      "expanded_url" : "http:\/\/stats.grok.se\/",
      "display_url" : "stats.grok.se"
    } ]
  },
  "geo" : { },
  "id_str" : "838400653328285697",
  "text" : "To browse Wikipedia page view statistics, visit https:\/\/t.co\/xG0ez6FZJN and have a look around.",
  "id" : 838400653328285697,
  "created_at" : "2017-03-05 14:47:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Kuestenmacher",
      "screen_name" : "simongerman600",
      "indices" : [ 3, 18 ],
      "id_str" : "359188534",
      "id" : 359188534
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/simongerman600\/status\/838079165135978496\/photo\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/WmGg4xm8HR",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6F0jtxU8AEu-_L.jpg",
      "id_str" : "838079162598354945",
      "id" : 838079162598354945,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6F0jtxU8AEu-_L.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 451,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 637,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 637,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 637,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WmGg4xm8HR"
    } ],
    "hashtags" : [ {
      "text" : "Map",
      "indices" : [ 20, 24 ]
    }, {
      "text" : "EU",
      "indices" : [ 86, 89 ]
    } ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/T2Se9mkVEE",
      "expanded_url" : "http:\/\/buff.ly\/2lvJffA",
      "display_url" : "buff.ly\/2lvJffA"
    } ]
  },
  "geo" : { },
  "id_str" : "838384981802115073",
  "text" : "RT @simongerman600: #Map shows how much each country contributes to and receives from #EU. https:\/\/t.co\/T2Se9mkVEE https:\/\/t.co\/WmGg4xm8HR",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/simongerman600\/status\/838079165135978496\/photo\/1",
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/WmGg4xm8HR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6F0jtxU8AEu-_L.jpg",
        "id_str" : "838079162598354945",
        "id" : 838079162598354945,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6F0jtxU8AEu-_L.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 637,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 637,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 637,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WmGg4xm8HR"
      } ],
      "hashtags" : [ {
        "text" : "Map",
        "indices" : [ 0, 4 ]
      }, {
        "text" : "EU",
        "indices" : [ 66, 69 ]
      } ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/T2Se9mkVEE",
        "expanded_url" : "http:\/\/buff.ly\/2lvJffA",
        "display_url" : "buff.ly\/2lvJffA"
      } ]
    },
    "geo" : { },
    "id_str" : "838079165135978496",
    "text" : "#Map shows how much each country contributes to and receives from #EU. https:\/\/t.co\/T2Se9mkVEE https:\/\/t.co\/WmGg4xm8HR",
    "id" : 838079165135978496,
    "created_at" : "2017-03-04 17:30:18 +0000",
    "user" : {
      "name" : "Simon Kuestenmacher",
      "screen_name" : "simongerman600",
      "protected" : false,
      "id_str" : "359188534",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/916213395422035968\/__3Ac2dP_normal.jpg",
      "id" : 359188534,
      "verified" : false
    }
  },
  "id" : 838384981802115073,
  "created_at" : "2017-03-05 13:45:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/wJ49lYBVhe",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/magazine-39003902",
      "display_url" : "bbc.com\/news\/magazine-\u2026"
    }, {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/DqxCs2mKFz",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/magazine-38846022",
      "display_url" : "bbc.com\/news\/magazine-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838219520707805185",
  "text" : "Two stories that can turn into a movie https:\/\/t.co\/wJ49lYBVhe | https:\/\/t.co\/DqxCs2mKFz",
  "id" : 838219520707805185,
  "created_at" : "2017-03-05 02:48:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kevin O'Brien",
      "screen_name" : "dragonflystats",
      "indices" : [ 3, 18 ],
      "id_str" : "1291259790",
      "id" : 1291259790
    }, {
      "name" : "Harvard University",
      "screen_name" : "Harvard",
      "indices" : [ 75, 83 ],
      "id_str" : "39585367",
      "id" : 39585367
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/eMc0SHD1ya",
      "expanded_url" : "http:\/\/news.harvard.edu\/gazette\/story\/2017\/03\/making-math-more-lego-like\/?utm_source=facebook&utm_medium=social&utm_campaign=hu-facebook-general",
      "display_url" : "news.harvard.edu\/gazette\/story\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "838140467304882176",
  "text" : "RT @dragonflystats: Making math more Lego-like https:\/\/t.co\/eMc0SHD1ya via @harvard",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Harvard University",
        "screen_name" : "Harvard",
        "indices" : [ 55, 63 ],
        "id_str" : "39585367",
        "id" : 39585367
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 27, 50 ],
        "url" : "https:\/\/t.co\/eMc0SHD1ya",
        "expanded_url" : "http:\/\/news.harvard.edu\/gazette\/story\/2017\/03\/making-math-more-lego-like\/?utm_source=facebook&utm_medium=social&utm_campaign=hu-facebook-general",
        "display_url" : "news.harvard.edu\/gazette\/story\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "838133139969093633",
    "text" : "Making math more Lego-like https:\/\/t.co\/eMc0SHD1ya via @harvard",
    "id" : 838133139969093633,
    "created_at" : "2017-03-04 21:04:46 +0000",
    "user" : {
      "name" : "Kevin O'Brien",
      "screen_name" : "dragonflystats",
      "protected" : false,
      "id_str" : "1291259790",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3427713318\/840e4aa0a6dca9263a0507fbfe4c4074_normal.jpeg",
      "id" : 1291259790,
      "verified" : false
    }
  },
  "id" : 838140467304882176,
  "created_at" : "2017-03-04 21:33:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RTE One",
      "screen_name" : "RTEOne",
      "indices" : [ 3, 10 ],
      "id_str" : "406900550",
      "id" : 406900550
    }, {
      "name" : "Dr Sabina Brennan",
      "screen_name" : "Sabina_Brennan",
      "indices" : [ 117, 132 ],
      "id_str" : "19810263",
      "id" : 19810263
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837939883859660801",
  "text" : "RT @RTEOne: Ageism is the most commonly experienced prejudice and the most tolerated compared to racism and sexism - @Sabina_Brennan #latel\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/snappytv.com\" rel=\"nofollow\"\u003ESnappyTV.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dr Sabina Brennan",
        "screen_name" : "Sabina_Brennan",
        "indices" : [ 105, 120 ],
        "id_str" : "19810263",
        "id" : 19810263
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RTEOne\/status\/837805720141627392\/video\/1",
        "indices" : [ 131, 154 ],
        "url" : "https:\/\/t.co\/CU8hwOU2Ew",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6B723HUoAAtJdt.jpg",
        "id_str" : "837805567426994176",
        "id" : 837805567426994176,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6B723HUoAAtJdt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CU8hwOU2Ew"
      } ],
      "hashtags" : [ {
        "text" : "latelate",
        "indices" : [ 121, 130 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "837805720141627392",
    "text" : "Ageism is the most commonly experienced prejudice and the most tolerated compared to racism and sexism - @Sabina_Brennan #latelate https:\/\/t.co\/CU8hwOU2Ew",
    "id" : 837805720141627392,
    "created_at" : "2017-03-03 23:23:44 +0000",
    "user" : {
      "name" : "RTE One",
      "screen_name" : "RTEOne",
      "protected" : false,
      "id_str" : "406900550",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/837407755761758208\/D5SCH0t2_normal.jpg",
      "id" : 406900550,
      "verified" : true
    }
  },
  "id" : 837939883859660801,
  "created_at" : "2017-03-04 08:16:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GCPBigData\/status\/837681422877929472\/photo\/1",
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/rwHJ1PLVCM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C6AKsYSU4AAvYES.png",
      "id_str" : "837681288240750592",
      "id" : 837681288240750592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6AKsYSU4AAvYES.png",
      "sizes" : [ {
        "h" : 459,
        "resize" : "fit",
        "w" : 350
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 350
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 350
      }, {
        "h" : 459,
        "resize" : "fit",
        "w" : 350
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rwHJ1PLVCM"
    } ],
    "hashtags" : [ {
      "text" : "GoogleNext17",
      "indices" : [ 98, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/SI2eHqrKNN",
      "expanded_url" : "https:\/\/cloud.google.com\/blog\/big-data\/2017\/03\/data-science-on-the-google-cloud-platform-the-first-book",
      "display_url" : "cloud.google.com\/blog\/big-data\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837926995099009024",
  "text" : "RT @GCPBigData: Data Science on the Google Cloud Platform: the first book https:\/\/t.co\/SI2eHqrKNN #GoogleNext17 https:\/\/t.co\/rwHJ1PLVCM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GCPBigData\/status\/837681422877929472\/photo\/1",
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/rwHJ1PLVCM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C6AKsYSU4AAvYES.png",
        "id_str" : "837681288240750592",
        "id" : 837681288240750592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C6AKsYSU4AAvYES.png",
        "sizes" : [ {
          "h" : 459,
          "resize" : "fit",
          "w" : 350
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 350
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 350
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 350
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rwHJ1PLVCM"
      } ],
      "hashtags" : [ {
        "text" : "GoogleNext17",
        "indices" : [ 82, 95 ]
      } ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/SI2eHqrKNN",
        "expanded_url" : "https:\/\/cloud.google.com\/blog\/big-data\/2017\/03\/data-science-on-the-google-cloud-platform-the-first-book",
        "display_url" : "cloud.google.com\/blog\/big-data\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "837681422877929472",
    "text" : "Data Science on the Google Cloud Platform: the first book https:\/\/t.co\/SI2eHqrKNN #GoogleNext17 https:\/\/t.co\/rwHJ1PLVCM",
    "id" : 837681422877929472,
    "created_at" : "2017-03-03 15:09:49 +0000",
    "user" : {
      "name" : "GCP Data & ML",
      "screen_name" : "GCPDataML",
      "protected" : false,
      "id_str" : "4725778206",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/691546141964414976\/J4q1rBCQ_normal.jpg",
      "id" : 4725778206,
      "verified" : false
    }
  },
  "id" : 837926995099009024,
  "created_at" : "2017-03-04 07:25:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/8uJXEd0Ky0",
      "expanded_url" : "http:\/\/selnd.com\/2lFJyDX",
      "display_url" : "selnd.com\/2lFJyDX"
    } ]
  },
  "geo" : { },
  "id_str" : "837840292715823104",
  "text" : "Has machine learning created a new model for SEO ranking? https:\/\/t.co\/8uJXEd0Ky0",
  "id" : 837840292715823104,
  "created_at" : "2017-03-04 01:41:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "don lavery",
      "screen_name" : "donlav",
      "indices" : [ 0, 7 ],
      "id_str" : "139837687",
      "id" : 139837687
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/kKnFoPXNdN",
      "expanded_url" : "http:\/\/www.independent.ie\/irish-news\/us-marines-will-put-irish-troop-carriers-to-the-test-28957636.html",
      "display_url" : "independent.ie\/irish-news\/us-\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "836204999499636738",
  "geo" : { },
  "id_str" : "837828635923001344",
  "in_reply_to_user_id" : 139837687,
  "text" : "@donlav Singapore's Terrex used Timoney system. I google and leads to your article! https:\/\/t.co\/kKnFoPXNdN",
  "id" : 837828635923001344,
  "in_reply_to_status_id" : 836204999499636738,
  "created_at" : "2017-03-04 00:54:47 +0000",
  "in_reply_to_screen_name" : "donlav",
  "in_reply_to_user_id_str" : "139837687",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 106, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/VeLvyN5BsS",
      "expanded_url" : "https:\/\/whatdoesmysitecost.com\/test\/170228_RJ_VVQ",
      "display_url" : "whatdoesmysitecost.com\/test\/170228_RJ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837814753749831680",
  "text" : "How much it costs for someone to use my site on mobile networks around the world? https:\/\/t.co\/VeLvyN5BsS #getoptimise",
  "id" : 837814753749831680,
  "created_at" : "2017-03-03 23:59:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "1221157964",
      "id" : 1221157964
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/9pHGhPkHEX",
      "expanded_url" : "https:\/\/twitter.com\/RyanAcademy\/status\/837701660344451073",
      "display_url" : "twitter.com\/RyanAcademy\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837726620991516672",
  "text" : "RT @DigiWomenIRL: Programmes like this can really help grow and develop your business. https:\/\/t.co\/9pHGhPkHEX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/9pHGhPkHEX",
        "expanded_url" : "https:\/\/twitter.com\/RyanAcademy\/status\/837701660344451073",
        "display_url" : "twitter.com\/RyanAcademy\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "837726265427836933",
    "text" : "Programmes like this can really help grow and develop your business. https:\/\/t.co\/9pHGhPkHEX",
    "id" : 837726265427836933,
    "created_at" : "2017-03-03 18:08:00 +0000",
    "user" : {
      "name" : "DigiWomen",
      "screen_name" : "DigiWomenIRL",
      "protected" : false,
      "id_str" : "1221157964",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/527003323623145473\/C2yyI2ob_normal.png",
      "id" : 1221157964,
      "verified" : false
    }
  },
  "id" : 837726620991516672,
  "created_at" : "2017-03-03 18:09:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/h6bgZhhqSs",
      "expanded_url" : "http:\/\/str.sg\/4nVj",
      "display_url" : "str.sg\/4nVj"
    } ]
  },
  "geo" : { },
  "id_str" : "837713952800927744",
  "text" : "Weapon of choice: keyboard. Battlefield : Virtual  https:\/\/t.co\/h6bgZhhqSs",
  "id" : 837713952800927744,
  "created_at" : "2017-03-03 17:19:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dermot Casey",
      "screen_name" : "dermotcasey",
      "indices" : [ 3, 15 ],
      "id_str" : "6297382",
      "id" : 6297382
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/0nHGeAEYg1",
      "expanded_url" : "https:\/\/twitter.com\/ChristineBohan\/status\/837634122453499905",
      "display_url" : "twitter.com\/ChristineBohan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837679973154508801",
  "text" : "RT @dermotcasey: Beyond parody.. beyond irony... https:\/\/t.co\/0nHGeAEYg1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 55 ],
        "url" : "https:\/\/t.co\/0nHGeAEYg1",
        "expanded_url" : "https:\/\/twitter.com\/ChristineBohan\/status\/837634122453499905",
        "display_url" : "twitter.com\/ChristineBohan\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "837677025309622272",
    "text" : "Beyond parody.. beyond irony... https:\/\/t.co\/0nHGeAEYg1",
    "id" : 837677025309622272,
    "created_at" : "2017-03-03 14:52:20 +0000",
    "user" : {
      "name" : "Dermot Casey",
      "screen_name" : "dermotcasey",
      "protected" : false,
      "id_str" : "6297382",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1004359497559740416\/CCWk8-wq_normal.jpg",
      "id" : 6297382,
      "verified" : false
    }
  },
  "id" : 837679973154508801,
  "created_at" : "2017-03-03 15:04:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/VZLl2Q1tmb",
      "expanded_url" : "https:\/\/youtu.be\/Kra1eWAiKvE",
      "display_url" : "youtu.be\/Kra1eWAiKvE"
    } ]
  },
  "geo" : { },
  "id_str" : "837654104923074561",
  "text" : "As someone who drink coffee, this TV ads reasoante with me  https:\/\/t.co\/VZLl2Q1tmb",
  "id" : 837654104923074561,
  "created_at" : "2017-03-03 13:21:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 7, 30 ],
      "url" : "https:\/\/t.co\/L3pQsMKTtu",
      "expanded_url" : "https:\/\/gitter.im\/home",
      "display_url" : "gitter.im\/home"
    } ]
  },
  "geo" : { },
  "id_str" : "837648844179582982",
  "text" : "Gitter https:\/\/t.co\/L3pQsMKTtu is the Slack for the developer communities",
  "id" : 837648844179582982,
  "created_at" : "2017-03-03 13:00:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/PgKQxlOlvP",
      "expanded_url" : "http:\/\/m.todayonline.com\/singapore\/public-service-has-lost-its-heart-mps-say",
      "display_url" : "m.todayonline.com\/singapore\/publ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837635821041762304",
  "text" : "RT @yapphenghui: KPI not measured by \u2665 \nPublic service has lost its heart, MPs say | TODAYonline https:\/\/t.co\/PgKQxlOlvP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/PgKQxlOlvP",
        "expanded_url" : "http:\/\/m.todayonline.com\/singapore\/public-service-has-lost-its-heart-mps-say",
        "display_url" : "m.todayonline.com\/singapore\/publ\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "837103395882192896",
    "text" : "KPI not measured by \u2665 \nPublic service has lost its heart, MPs say | TODAYonline https:\/\/t.co\/PgKQxlOlvP",
    "id" : 837103395882192896,
    "created_at" : "2017-03-02 00:52:56 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 837635821041762304,
  "created_at" : "2017-03-03 12:08:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/A0kx2UZ5T0",
      "expanded_url" : "http:\/\/getoptimise.com\/contact\/",
      "display_url" : "getoptimise.com\/contact\/"
    } ]
  },
  "geo" : { },
  "id_str" : "837627262316998656",
  "text" : "Industry survey indicates 95% Google Analytics set up are not fit for purpose. Sign up at https:\/\/t.co\/A0kx2UZ5T0 to get free checkup",
  "id" : 837627262316998656,
  "created_at" : "2017-03-03 11:34:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/DzhNxjlHCM",
      "expanded_url" : "https:\/\/twitter.com\/iprospectirl\/status\/837578740918018048",
      "display_url" : "twitter.com\/iprospectirl\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837603305161392129",
  "text" : "Good to see marketers applying statistical  \n approach to measurement  https:\/\/t.co\/DzhNxjlHCM",
  "id" : 837603305161392129,
  "created_at" : "2017-03-03 09:59:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LinkedIn",
      "screen_name" : "LinkedIn",
      "indices" : [ 104, 113 ],
      "id_str" : "13058772",
      "id" : 13058772
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/8SMunTsloj",
      "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/best-answer-ive-ever-heard-sell-me-pen-dailius-r-wilson-",
      "display_url" : "linkedin.com\/pulse\/best-ans\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837421051533492224",
  "text" : "I came across this \"Sell me this pen\" on a Hong Kong TV series decdades ago  https:\/\/t.co\/8SMunTsloj on @LinkedIn",
  "id" : 837421051533492224,
  "created_at" : "2017-03-02 21:55:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837418799787827201",
  "text" : "Any one on Twitter heading for tomorrow Google Breakfast Briefing?",
  "id" : 837418799787827201,
  "created_at" : "2017-03-02 21:46:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Robinson",
      "screen_name" : "drob",
      "indices" : [ 3, 8 ],
      "id_str" : "46245868",
      "id" : 46245868
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/drob\/status\/836671612615172097\/photo\/1",
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/xi1zX61wK7",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/C5xz7XrWQAQ3siw.jpg",
      "id_str" : "836671094589374468",
      "id" : 836671094589374468,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/C5xz7XrWQAQ3siw.jpg",
      "sizes" : [ {
        "h" : 274,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 400
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 400
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xi1zX61wK7"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837392320337149953",
  "text" : "RT @drob: You can't have Stack Overflow run on AWS\n\nIf AWS went down, they'd never be able to fix it https:\/\/t.co\/xi1zX61wK7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/drob\/status\/836671612615172097\/photo\/1",
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/xi1zX61wK7",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/C5xz7XrWQAQ3siw.jpg",
        "id_str" : "836671094589374468",
        "id" : 836671094589374468,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/C5xz7XrWQAQ3siw.jpg",
        "sizes" : [ {
          "h" : 274,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 400
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xi1zX61wK7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "836668748450811905",
    "geo" : { },
    "id_str" : "836671612615172097",
    "in_reply_to_user_id" : 46245868,
    "text" : "You can't have Stack Overflow run on AWS\n\nIf AWS went down, they'd never be able to fix it https:\/\/t.co\/xi1zX61wK7",
    "id" : 836671612615172097,
    "in_reply_to_status_id" : 836668748450811905,
    "created_at" : "2017-02-28 20:17:11 +0000",
    "in_reply_to_screen_name" : "drob",
    "in_reply_to_user_id_str" : "46245868",
    "user" : {
      "name" : "David Robinson",
      "screen_name" : "drob",
      "protected" : false,
      "id_str" : "46245868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876529727284039680\/dfvG_Dy4_normal.jpg",
      "id" : 46245868,
      "verified" : false
    }
  },
  "id" : 837392320337149953,
  "created_at" : "2017-03-02 20:01:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/cWbMjKq9jA",
      "expanded_url" : "https:\/\/sylvaindeville.net\/2015\/07\/17\/writing-academic-papers-in-plain-text-with-markdown-and-jupyter-notebook\/",
      "display_url" : "sylvaindeville.net\/2015\/07\/17\/wri\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "837097185896431618",
  "text" : "https:\/\/t.co\/cWbMjKq9jA",
  "id" : 837097185896431618,
  "created_at" : "2017-03-02 00:28:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837049469757702145",
  "text" : "&lt; An Idiot Aboard....Told someone that there is black mark on his fore head &amp; was informed today is Ash Wednesday (the first day of Lent)",
  "id" : 837049469757702145,
  "created_at" : "2017-03-01 21:18:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julia Galef",
      "screen_name" : "juliagalef",
      "indices" : [ 3, 14 ],
      "id_str" : "18691746",
      "id" : 18691746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837015711369265152",
  "text" : "RT @juliagalef: This news site requires you to pass a quiz before commenting (to prove you actually read the article):\nhttps:\/\/t.co\/yetG0yC\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/yetG0yCmRw",
        "expanded_url" : "http:\/\/www.niemanlab.org\/2017\/03\/this-site-is-taking-the-edge-off-rant-mode-by-making-readers-pass-a-quiz-before-commenting\/",
        "display_url" : "niemanlab.org\/2017\/03\/this-s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "837014458257956864",
    "text" : "This news site requires you to pass a quiz before commenting (to prove you actually read the article):\nhttps:\/\/t.co\/yetG0yCmRw",
    "id" : 837014458257956864,
    "created_at" : "2017-03-01 18:59:32 +0000",
    "user" : {
      "name" : "Julia Galef",
      "screen_name" : "juliagalef",
      "protected" : false,
      "id_str" : "18691746",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506698231003086848\/0oylp8R3_normal.jpeg",
      "id" : 18691746,
      "verified" : true
    }
  },
  "id" : 837015711369265152,
  "created_at" : "2017-03-01 19:04:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/837011607700049921\/photo\/1",
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/wKmXGY2BQa",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C52pjxBWUAAayPv.jpg",
      "id_str" : "837011537680289792",
      "id" : 837011537680289792,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C52pjxBWUAAayPv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 390,
        "resize" : "fit",
        "w" : 1096
      }, {
        "h" : 242,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 390,
        "resize" : "fit",
        "w" : 1096
      }, {
        "h" : 390,
        "resize" : "fit",
        "w" : 1096
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wKmXGY2BQa"
    } ],
    "hashtags" : [ {
      "text" : "DataScience",
      "indices" : [ 91, 103 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837011607700049921",
  "text" : "Trying to install a R package. How come the same code works in RStudio but not in Juypter? #DataScience Thanks https:\/\/t.co\/wKmXGY2BQa",
  "id" : 837011607700049921,
  "created_at" : "2017-03-01 18:48:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LaBright",
      "screen_name" : "LaBright_MEX",
      "indices" : [ 3, 16 ],
      "id_str" : "3033875847",
      "id" : 3033875847
    }, {
      "name" : "RayPoynter",
      "screen_name" : "RayPoynter",
      "indices" : [ 49, 60 ],
      "id_str" : "11541372",
      "id" : 11541372
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/LaBright_MEX\/status\/837008494737965057\/photo\/1",
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/DgoCVcsgl0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C52mxgpUoAAP38V.jpg",
      "id_str" : "837008475267833856",
      "id" : 837008475267833856,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C52mxgpUoAAP38V.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DgoCVcsgl0"
    } ],
    "hashtags" : [ {
      "text" : "NewMR",
      "indices" : [ 61, 67 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837009249746169861",
  "text" : "RT @LaBright_MEX: Straight to my \u2764\uFE0F, thx Tyrone, @RayPoynter #NewMR https:\/\/t.co\/DgoCVcsgl0",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RayPoynter",
        "screen_name" : "RayPoynter",
        "indices" : [ 31, 42 ],
        "id_str" : "11541372",
        "id" : 11541372
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LaBright_MEX\/status\/837008494737965057\/photo\/1",
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/DgoCVcsgl0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C52mxgpUoAAP38V.jpg",
        "id_str" : "837008475267833856",
        "id" : 837008475267833856,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C52mxgpUoAAP38V.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DgoCVcsgl0"
      } ],
      "hashtags" : [ {
        "text" : "NewMR",
        "indices" : [ 43, 49 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "837008494737965057",
    "text" : "Straight to my \u2764\uFE0F, thx Tyrone, @RayPoynter #NewMR https:\/\/t.co\/DgoCVcsgl0",
    "id" : 837008494737965057,
    "created_at" : "2017-03-01 18:35:50 +0000",
    "user" : {
      "name" : "LaBright",
      "screen_name" : "LaBright_MEX",
      "protected" : false,
      "id_str" : "3033875847",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/836265024167477249\/2euswH_j_normal.jpg",
      "id" : 3033875847,
      "verified" : false
    }
  },
  "id" : 837009249746169861,
  "created_at" : "2017-03-01 18:38:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andy Cotgreave",
      "screen_name" : "acotgreave",
      "indices" : [ 3, 14 ],
      "id_str" : "119704541",
      "id" : 119704541
    }, {
      "name" : "Jewel Loree",
      "screen_name" : "jeweloree",
      "indices" : [ 117, 127 ],
      "id_str" : "16121674",
      "id" : 16121674
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837009125796114434",
  "text" : "RT @acotgreave: 'What's the point of seeing and understanding your data if you can't share your understanding?' says @jeweloree #tapestryco\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jewel Loree",
        "screen_name" : "jeweloree",
        "indices" : [ 101, 111 ],
        "id_str" : "16121674",
        "id" : 16121674
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tapestryconf",
        "indices" : [ 112, 125 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "837008613692567554",
    "text" : "'What's the point of seeing and understanding your data if you can't share your understanding?' says @jeweloree #tapestryconf",
    "id" : 837008613692567554,
    "created_at" : "2017-03-01 18:36:19 +0000",
    "user" : {
      "name" : "Andy Cotgreave",
      "screen_name" : "acotgreave",
      "protected" : false,
      "id_str" : "119704541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930182378030600194\/1gmP-Qj-_normal.jpg",
      "id" : 119704541,
      "verified" : false
    }
  },
  "id" : 837009125796114434,
  "created_at" : "2017-03-01 18:38:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/837009019181150210\/photo\/1",
      "indices" : [ 131, 154 ],
      "url" : "https:\/\/t.co\/QZzClSZKiO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C52nPfgXQAEcFUt.jpg",
      "id_str" : "837008990357897217",
      "id" : 837008990357897217,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C52nPfgXQAEcFUt.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 170,
        "resize" : "fit",
        "w" : 696
      }, {
        "h" : 170,
        "resize" : "fit",
        "w" : 696
      }, {
        "h" : 170,
        "resize" : "fit",
        "w" : 696
      }, {
        "h" : 166,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QZzClSZKiO"
    } ],
    "hashtags" : [ {
      "text" : "DataScience",
      "indices" : [ 111, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "837009019181150210",
  "text" : "How comes I can run R script in Rstudio without any issue but unable to do so with the same script on Juypter? #DataScience Thanks https:\/\/t.co\/QZzClSZKiO",
  "id" : 837009019181150210,
  "created_at" : "2017-03-01 18:37:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/HV5Um3BaR2",
      "expanded_url" : "https:\/\/makingscience.withgoogle.com\/science-journal?lang=en",
      "display_url" : "makingscience.withgoogle.com\/science-journa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "836914441803399169",
  "text" : "Using the Science Journal app, turn your Android phone into a pocket science laboratory https:\/\/t.co\/HV5Um3BaR2",
  "id" : 836914441803399169,
  "created_at" : "2017-03-01 12:22:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "singapore",
      "indices" : [ 97, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/PylDjXHvJa",
      "expanded_url" : "http:\/\/thisissgchineseprivilege.tumblr.com\/",
      "display_url" : "thisissgchineseprivilege.tumblr.com"
    } ]
  },
  "geo" : { },
  "id_str" : "836883978170793985",
  "text" : "Probably treason\/blasphemy to share this, but this should not be ignore? https:\/\/t.co\/PylDjXHvJa #singapore",
  "id" : 836883978170793985,
  "created_at" : "2017-03-01 10:21:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/uIhPfQ79AI",
      "expanded_url" : "http:\/\/www.irishtimes.com\/life-and-style\/people\/new-to-the-parish-i-like-ireland-because-it-is-safe-1.2982556#.WLaa-GlGA-Q.twitter",
      "display_url" : "irishtimes.com\/life-and-style\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "836878009894305792",
  "text" : "New to the Parish: \u2018I like Ireland because it is safe\u2019 https:\/\/t.co\/uIhPfQ79AI",
  "id" : 836878009894305792,
  "created_at" : "2017-03-01 09:57:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Graphic Mint",
      "screen_name" : "Graphic_Mint",
      "indices" : [ 3, 16 ],
      "id_str" : "18087978",
      "id" : 18087978
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "836877714753798144",
  "text" : "RT @Graphic_Mint: We are looking for YOU to test out digital products. Be rewarded for your time. Anyone can sign up!  https:\/\/t.co\/D57EPVX\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/D57EPVXlPa",
        "expanded_url" : "http:\/\/ow.ly\/EQtw309qM1c",
        "display_url" : "ow.ly\/EQtw309qM1c"
      } ]
    },
    "geo" : { },
    "id_str" : "836856117078671362",
    "text" : "We are looking for YOU to test out digital products. Be rewarded for your time. Anyone can sign up!  https:\/\/t.co\/D57EPVXlPa",
    "id" : 836856117078671362,
    "created_at" : "2017-03-01 08:30:21 +0000",
    "user" : {
      "name" : "Graphic Mint",
      "screen_name" : "Graphic_Mint",
      "protected" : false,
      "id_str" : "18087978",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/685406377766981632\/uVJ7wMrY_normal.jpg",
      "id" : 18087978,
      "verified" : false
    }
  },
  "id" : 836877714753798144,
  "created_at" : "2017-03-01 09:56:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 121, 144 ],
      "url" : "https:\/\/t.co\/oIgIi26Yb7",
      "expanded_url" : "https:\/\/pages.gitlab.io",
      "display_url" : "pages.gitlab.io"
    } ]
  },
  "geo" : { },
  "id_str" : "836875223370776580",
  "text" : "According to a vendor, Static websites are fast, powerful &amp; seen as the next big thing in publishing to the internet.https:\/\/t.co\/oIgIi26Yb7",
  "id" : 836875223370776580,
  "created_at" : "2017-03-01 09:46:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5B8C\u5168\u611F\u899ADreamer",
      "screen_name" : "chezevo",
      "indices" : [ 3, 11 ],
      "id_str" : "194632356",
      "id" : 194632356
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/fyAjQxM9FD",
      "expanded_url" : "https:\/\/mainichi.jp\/english\/articles\/20170223\/p2a\/00m\/0na\/006000c",
      "display_url" : "mainichi.jp\/english\/articl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "836847154857717763",
  "text" : "RT @chezevo: Cherry blossoms forecasted to flower slightly later than usual - The Mainichi https:\/\/t.co\/fyAjQxM9FD",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/fyAjQxM9FD",
        "expanded_url" : "https:\/\/mainichi.jp\/english\/articles\/20170223\/p2a\/00m\/0na\/006000c",
        "display_url" : "mainichi.jp\/english\/articl\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "836777995452870656",
    "text" : "Cherry blossoms forecasted to flower slightly later than usual - The Mainichi https:\/\/t.co\/fyAjQxM9FD",
    "id" : 836777995452870656,
    "created_at" : "2017-03-01 03:19:55 +0000",
    "user" : {
      "name" : "\u5B8C\u5168\u611F\u899ADreamer",
      "screen_name" : "chezevo",
      "protected" : false,
      "id_str" : "194632356",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026457918864932864\/8n-shK7e_normal.jpg",
      "id" : 194632356,
      "verified" : false
    }
  },
  "id" : 836847154857717763,
  "created_at" : "2017-03-01 07:54:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]