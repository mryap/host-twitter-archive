Grailbird.data.tweets_2015_09 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dan mentos",
      "screen_name" : "DanMentos",
      "indices" : [ 3, 13 ],
      "id_str" : "1621424731",
      "id" : 1621424731
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649321458116771840",
  "text" : "RT @DanMentos: LIFE HACK: Answer your phone \"Hello you're on the air\" and 99% of the time people will just hang up",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "533053464968822784",
    "text" : "LIFE HACK: Answer your phone \"Hello you're on the air\" and 99% of the time people will just hang up",
    "id" : 533053464968822784,
    "created_at" : "2014-11-14 00:27:04 +0000",
    "user" : {
      "name" : "dan mentos",
      "screen_name" : "DanMentos",
      "protected" : false,
      "id_str" : "1621424731",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/548378022768701441\/2zz0Vk61_normal.png",
      "id" : 1621424731,
      "verified" : false
    }
  },
  "id" : 649321458116771840,
  "created_at" : "2015-09-30 20:34:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Farr Institute",
      "screen_name" : "FarrInstitute",
      "indices" : [ 3, 17 ],
      "id_str" : "2815695695",
      "id" : 2815695695
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "genomics",
      "indices" : [ 86, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649319780311277569",
  "text" : "RT @FarrInstitute: \"Scientists estimate that, in the next 10 years, the data needs of #genomics will outpace those of astronomy\"\nhttp:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "genomics",
        "indices" : [ 67, 76 ]
      } ],
      "urls" : [ {
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/pEsWfje1h0",
        "expanded_url" : "http:\/\/mobile.the-scientist.com\/article\/43483\/big-data-problem#.VZ698YQI2ls.twitter",
        "display_url" : "mobile.the-scientist.com\/article\/43483\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "619212874389585920",
    "text" : "\"Scientists estimate that, in the next 10 years, the data needs of #genomics will outpace those of astronomy\"\nhttp:\/\/t.co\/pEsWfje1h0",
    "id" : 619212874389585920,
    "created_at" : "2015-07-09 18:33:48 +0000",
    "user" : {
      "name" : "The Farr Institute",
      "screen_name" : "FarrInstitute",
      "protected" : false,
      "id_str" : "2815695695",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/522393721539547137\/izAlbZFF_normal.jpeg",
      "id" : 2815695695,
      "verified" : false
    }
  },
  "id" : 649319780311277569,
  "created_at" : "2015-09-30 20:27:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Flora Shedden",
      "screen_name" : "florashedden",
      "indices" : [ 11, 24 ],
      "id_str" : "845338280",
      "id" : 845338280
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649312978471387136",
  "text" : "Sad to see @florashedden go. No!!!!!",
  "id" : 649312978471387136,
  "created_at" : "2015-09-30 20:00:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/649292129823518720\/photo\/1",
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/HNN7XHR7Tk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CQK_wHrWwAAL2n4.jpg",
      "id_str" : "649292129710292992",
      "id" : 649292129710292992,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CQK_wHrWwAAL2n4.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HNN7XHR7Tk"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 77 ],
      "url" : "http:\/\/t.co\/HDhZL6m5M4",
      "expanded_url" : "http:\/\/ift.tt\/1PNXiGh",
      "display_url" : "ift.tt\/1PNXiGh"
    } ]
  },
  "geo" : { },
  "id_str" : "649292129823518720",
  "text" : "Tomorrow meal plan - pasta with leek, cherry tomatoes. http:\/\/t.co\/HDhZL6m5M4 http:\/\/t.co\/HNN7XHR7Tk",
  "id" : 649292129823518720,
  "created_at" : "2015-09-30 18:38:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/A0juK0PH6K",
      "expanded_url" : "https:\/\/twitter.com\/sales_shout\/status\/647523508638121984",
      "display_url" : "twitter.com\/sales_shout\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "649280201579458560",
  "text" : "Non-Irish Nationals need to renew their residence visa spend 1 whole working day. https:\/\/t.co\/A0juK0PH6K",
  "id" : 649280201579458560,
  "created_at" : "2015-09-30 17:50:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Haruki Murakami",
      "screen_name" : "harukimurakami_",
      "indices" : [ 3, 19 ],
      "id_str" : "73553243",
      "id" : 73553243
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649224705396862976",
  "text" : "RT @harukimurakami_: If you only read the books that everyone else is reading, you can only think what everyone else is thinking.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter for  iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "319968161669730305",
    "text" : "If you only read the books that everyone else is reading, you can only think what everyone else is thinking.",
    "id" : 319968161669730305,
    "created_at" : "2013-04-05 00:22:09 +0000",
    "user" : {
      "name" : "Haruki Murakami",
      "screen_name" : "harukimurakami_",
      "protected" : false,
      "id_str" : "73553243",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/410598690\/E69D91E4B88AE698A5E6A8B94-3f928_normal.jpg",
      "id" : 73553243,
      "verified" : false
    }
  },
  "id" : 649224705396862976,
  "created_at" : "2015-09-30 14:10:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 3, 12 ],
      "id_str" : "136690988",
      "id" : 136690988
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649222670362521600",
  "text" : "RT @ailieirv: I'm writing a blog post promoting workshop spaces that can be rented by freelancers in Ireland. Get in touch if you want to b\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "649221754855014400",
    "text" : "I'm writing a blog post promoting workshop spaces that can be rented by freelancers in Ireland. Get in touch if you want to be on it.",
    "id" : 649221754855014400,
    "created_at" : "2015-09-30 13:58:22 +0000",
    "user" : {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "protected" : false,
      "id_str" : "136690988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/528642221956796416\/WDp_wLcX_normal.jpeg",
      "id" : 136690988,
      "verified" : false
    }
  },
  "id" : 649222670362521600,
  "created_at" : "2015-09-30 14:02:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "indices" : [ 3, 12 ],
      "id_str" : "11503282",
      "id" : 11503282
    }, {
      "name" : "Financial Times",
      "screen_name" : "FT",
      "indices" : [ 98, 101 ],
      "id_str" : "18949452",
      "id" : 18949452
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 126 ],
      "url" : "http:\/\/t.co\/PnINFLT5Y8",
      "expanded_url" : "http:\/\/bit.ly\/1FHJEEc",
      "display_url" : "bit.ly\/1FHJEEc"
    } ]
  },
  "geo" : { },
  "id_str" : "649184969332535296",
  "text" : "RT @ladyxtel: FT\u00B2 - a new analytics-driven, robust content marketing offering from the good folks @FT.  http:\/\/t.co\/PnINFLT5Y8 http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Financial Times",
        "screen_name" : "FT",
        "indices" : [ 84, 87 ],
        "id_str" : "18949452",
        "id" : 18949452
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ladyxtel\/status\/649180716786450432\/photo\/1",
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/drIjydqWU9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CQJabBmW8AAQ1vA.jpg",
        "id_str" : "649180716627062784",
        "id" : 649180716627062784,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CQJabBmW8AAQ1vA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 670
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 670
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 670
        }, {
          "h" : 460,
          "resize" : "fit",
          "w" : 670
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/drIjydqWU9"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 112 ],
        "url" : "http:\/\/t.co\/PnINFLT5Y8",
        "expanded_url" : "http:\/\/bit.ly\/1FHJEEc",
        "display_url" : "bit.ly\/1FHJEEc"
      } ]
    },
    "geo" : { },
    "id_str" : "649180716786450432",
    "text" : "FT\u00B2 - a new analytics-driven, robust content marketing offering from the good folks @FT.  http:\/\/t.co\/PnINFLT5Y8 http:\/\/t.co\/drIjydqWU9",
    "id" : 649180716786450432,
    "created_at" : "2015-09-30 11:15:18 +0000",
    "user" : {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "protected" : false,
      "id_str" : "11503282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849872363671830528\/5iExzLy9_normal.jpg",
      "id" : 11503282,
      "verified" : true
    }
  },
  "id" : 649184969332535296,
  "created_at" : "2015-09-30 11:32:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Loh",
      "screen_name" : "Loh",
      "indices" : [ 3, 7 ],
      "id_str" : "69123976",
      "id" : 69123976
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649184676410732544",
  "text" : "RT @Loh: I feel like a good litmus test for STEM awareness in policy-makers would be to ask them, \"What, actually, is a litmus test?\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "649183864150081536",
    "text" : "I feel like a good litmus test for STEM awareness in policy-makers would be to ask them, \"What, actually, is a litmus test?\"",
    "id" : 649183864150081536,
    "created_at" : "2015-09-30 11:27:49 +0000",
    "user" : {
      "name" : "Loh",
      "screen_name" : "Loh",
      "protected" : false,
      "id_str" : "69123976",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1020512469826056194\/OT2mtbQ5_normal.jpg",
      "id" : 69123976,
      "verified" : true
    }
  },
  "id" : 649184676410732544,
  "created_at" : "2015-09-30 11:31:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/M0UQTRTMAC",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/guan-eng-tells-minister-quit-over-malaysians-prefer-065900691.html",
      "display_url" : "sg.news.yahoo.com\/guan-eng-tells\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "649184518935588864",
  "text" : "Things that politicians commented about Technology. https:\/\/t.co\/M0UQTRTMAC",
  "id" : 649184518935588864,
  "created_at" : "2015-09-30 11:30:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan McIntosh",
      "screen_name" : "radicalbytes",
      "indices" : [ 3, 16 ],
      "id_str" : "16823185",
      "id" : 16823185
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649162736841162752",
  "text" : "RT @radicalbytes: Everyday some internet tough guy calls me a \u201Cwoman\" for suggesting users shouldn't have to put up with online abuse. http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/radicalbytes\/status\/649039175400951808\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/XcsicrtWNe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CQHZO-yUAAAvi1f.png",
        "id_str" : "649038672713482240",
        "id" : 649038672713482240,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CQHZO-yUAAAvi1f.png",
        "sizes" : [ {
          "h" : 380,
          "resize" : "fit",
          "w" : 657
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 657
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 657
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 657
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XcsicrtWNe"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "649039175400951808",
    "text" : "Everyday some internet tough guy calls me a \u201Cwoman\" for suggesting users shouldn't have to put up with online abuse. http:\/\/t.co\/XcsicrtWNe",
    "id" : 649039175400951808,
    "created_at" : "2015-09-30 01:52:52 +0000",
    "user" : {
      "name" : "Jonathan McIntosh",
      "screen_name" : "radicalbytes",
      "protected" : false,
      "id_str" : "16823185",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/636279253772840960\/llaz-W8-_normal.jpg",
      "id" : 16823185,
      "verified" : false
    }
  },
  "id" : 649162736841162752,
  "created_at" : "2015-09-30 10:03:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Felicia Sonmez",
      "screen_name" : "feliciasonmez",
      "indices" : [ 3, 17 ],
      "id_str" : "137466464",
      "id" : 137466464
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 135 ],
      "url" : "http:\/\/t.co\/H6OwVla1bz",
      "expanded_url" : "http:\/\/on.wsj.com\/1OFNq3N",
      "display_url" : "on.wsj.com\/1OFNq3N"
    } ]
  },
  "geo" : { },
  "id_str" : "649162404539072512",
  "text" : "RT @feliciasonmez: Apple Music made its China debut today, and fans of public dancing have reason to be excited: http:\/\/t.co\/H6OwVla1bz htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChinaRealTime\/status\/649161964950843392\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/R8KLvbctaI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CQJJXfwUEAANEAg.jpg",
        "id_str" : "649161964304732160",
        "id" : 649161964304732160,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CQJJXfwUEAANEAg.jpg",
        "sizes" : [ {
          "h" : 369,
          "resize" : "fit",
          "w" : 553
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 553
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 553
        }, {
          "h" : 369,
          "resize" : "fit",
          "w" : 553
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/R8KLvbctaI"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 116 ],
        "url" : "http:\/\/t.co\/H6OwVla1bz",
        "expanded_url" : "http:\/\/on.wsj.com\/1OFNq3N",
        "display_url" : "on.wsj.com\/1OFNq3N"
      } ]
    },
    "geo" : { },
    "id_str" : "649162274163281920",
    "text" : "Apple Music made its China debut today, and fans of public dancing have reason to be excited: http:\/\/t.co\/H6OwVla1bz http:\/\/t.co\/R8KLvbctaI",
    "id" : 649162274163281920,
    "created_at" : "2015-09-30 10:02:01 +0000",
    "user" : {
      "name" : "Felicia Sonmez",
      "screen_name" : "feliciasonmez",
      "protected" : false,
      "id_str" : "137466464",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014338227866439686\/uGzjPIR5_normal.jpg",
      "id" : 137466464,
      "verified" : true
    }
  },
  "id" : 649162404539072512,
  "created_at" : "2015-09-30 10:02:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "649007764027670528",
  "text" : "RT @SimonPRepublic: Great little video by the Scarlet Alliance on how sex workers help disabled men have intimacy &amp; sex with women https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/FNJ3jnW8lT",
        "expanded_url" : "https:\/\/vimeo.com\/23523628",
        "display_url" : "vimeo.com\/23523628"
      } ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 53.3183533681498, -6.23897297287496 ]
    },
    "id_str" : "647185860589449220",
    "text" : "Great little video by the Scarlet Alliance on how sex workers help disabled men have intimacy &amp; sex with women https:\/\/t.co\/FNJ3jnW8lT",
    "id" : 647185860589449220,
    "created_at" : "2015-09-24 23:08:27 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 649007764027670528,
  "created_at" : "2015-09-29 23:48:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WWF-Singapore",
      "screen_name" : "wwfsg",
      "indices" : [ 3, 9 ],
      "id_str" : "76209337",
      "id" : 76209337
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sghaze",
      "indices" : [ 11, 18 ]
    }, {
      "text" : "XtheHaze",
      "indices" : [ 113, 122 ]
    } ],
    "urls" : [ {
      "indices" : [ 60, 82 ],
      "url" : "http:\/\/t.co\/XbnqmPaNyB",
      "expanded_url" : "http:\/\/bit.ly\/1K9ljCL",
      "display_url" : "bit.ly\/1K9ljCL"
    } ]
  },
  "geo" : { },
  "id_str" : "649003568549244928",
  "text" : "RT @wwfsg: #sghaze Power up the pledges to stop the haze at http:\/\/t.co\/XbnqmPaNyB. Demand haze-free products to #XtheHaze \uD83D\uDE37 http:\/\/t.co\/O2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wwfsg\/status\/641869583624859649\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/O2zMLMcTuB",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/COhg_DkWEAAlRZm.png",
        "id_str" : "641869583306067968",
        "id" : 641869583306067968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/COhg_DkWEAAlRZm.png",
        "sizes" : [ {
          "h" : 380,
          "resize" : "fit",
          "w" : 380
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 380
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 380
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 380,
          "resize" : "fit",
          "w" : 380
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/O2zMLMcTuB"
      } ],
      "hashtags" : [ {
        "text" : "sghaze",
        "indices" : [ 0, 7 ]
      }, {
        "text" : "XtheHaze",
        "indices" : [ 102, 111 ]
      } ],
      "urls" : [ {
        "indices" : [ 49, 71 ],
        "url" : "http:\/\/t.co\/XbnqmPaNyB",
        "expanded_url" : "http:\/\/bit.ly\/1K9ljCL",
        "display_url" : "bit.ly\/1K9ljCL"
      } ]
    },
    "geo" : { },
    "id_str" : "641869583624859649",
    "text" : "#sghaze Power up the pledges to stop the haze at http:\/\/t.co\/XbnqmPaNyB. Demand haze-free products to #XtheHaze \uD83D\uDE37 http:\/\/t.co\/O2zMLMcTuB",
    "id" : 641869583624859649,
    "created_at" : "2015-09-10 07:03:28 +0000",
    "user" : {
      "name" : "WWF-Singapore",
      "screen_name" : "wwfsg",
      "protected" : false,
      "id_str" : "76209337",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/886823009599725568\/KXtokfyQ_normal.jpg",
      "id" : 76209337,
      "verified" : true
    }
  },
  "id" : 649003568549244928,
  "created_at" : "2015-09-29 23:31:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Murray Newlands",
      "screen_name" : "MurrayNewlands",
      "indices" : [ 3, 18 ],
      "id_str" : "21360280",
      "id" : 21360280
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 118 ],
      "url" : "http:\/\/t.co\/8sQdBaWXPH",
      "expanded_url" : "http:\/\/bit.ly\/1KrLx6S",
      "display_url" : "bit.ly\/1KrLx6S"
    } ]
  },
  "geo" : { },
  "id_str" : "648976311273263104",
  "text" : "RT @MurrayNewlands: Singapore\u2019s National Research Foundation to invest another $28M in startups http:\/\/t.co\/8sQdBaWXPH http:\/\/t.co\/NMjpcgsV\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/erased5282099.com\" rel=\"nofollow\"\u003Eerased5282099\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MurrayNewlands\/status\/648976140149784576\/photo\/1",
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/NMjpcgsVi1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CQGgXCdUcAAdCO7.jpg",
        "id_str" : "648976138975342592",
        "id" : 648976138975342592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CQGgXCdUcAAdCO7.jpg",
        "sizes" : [ {
          "h" : 399,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 399,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 339,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 399,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NMjpcgsVi1"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 98 ],
        "url" : "http:\/\/t.co\/8sQdBaWXPH",
        "expanded_url" : "http:\/\/bit.ly\/1KrLx6S",
        "display_url" : "bit.ly\/1KrLx6S"
      } ]
    },
    "geo" : { },
    "id_str" : "648976140149784576",
    "text" : "Singapore\u2019s National Research Foundation to invest another $28M in startups http:\/\/t.co\/8sQdBaWXPH http:\/\/t.co\/NMjpcgsVi1",
    "id" : 648976140149784576,
    "created_at" : "2015-09-29 21:42:23 +0000",
    "user" : {
      "name" : "Murray Newlands",
      "screen_name" : "MurrayNewlands",
      "protected" : false,
      "id_str" : "21360280",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/701441124758466564\/dQIjxJO3_normal.jpg",
      "id" : 21360280,
      "verified" : true
    }
  },
  "id" : 648976311273263104,
  "created_at" : "2015-09-29 21:43:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/648900651464957952\/photo\/1",
      "indices" : [ 41, 63 ],
      "url" : "http:\/\/t.co\/4MSZSREDNk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CQFbtE9WgAIub1y.jpg",
      "id_str" : "648900651301371906",
      "id" : 648900651301371906,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CQFbtE9WgAIub1y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4MSZSREDNk"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 40 ],
      "url" : "http:\/\/t.co\/Az0WFcDtTv",
      "expanded_url" : "http:\/\/ift.tt\/1GeW9lw",
      "display_url" : "ift.tt\/1GeW9lw"
    } ]
  },
  "geo" : { },
  "id_str" : "648900651464957952",
  "text" : "O'Connells Bridge http:\/\/t.co\/Az0WFcDtTv http:\/\/t.co\/4MSZSREDNk",
  "id" : 648900651464957952,
  "created_at" : "2015-09-29 16:42:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/rV7xditMdR",
      "expanded_url" : "https:\/\/www.siliconrepublic.com\/start-ups\/2015\/09\/28\/dublin-needs-to-double-down-on-its-vision-to-become-europes-tech-capital",
      "display_url" : "siliconrepublic.com\/start-ups\/2015\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648791139701669888",
  "text" : "\"Think Big\" &lt; not just for Dublin but whole of ireland. https:\/\/t.co\/rV7xditMdR",
  "id" : 648791139701669888,
  "created_at" : "2015-09-29 09:27:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u00C1ine Dempsey",
      "screen_name" : "AineDempsey",
      "indices" : [ 3, 15 ],
      "id_str" : "64555862",
      "id" : 64555862
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648779082872811520",
  "text" : "RT @AineDempsey: \"We gave a slice of pizza to a homeless guy\" ... Yeah, that's called being decent, you don't need to try become internet f\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "648544578744786944",
    "text" : "\"We gave a slice of pizza to a homeless guy\" ... Yeah, that's called being decent, you don't need to try become internet famous",
    "id" : 648544578744786944,
    "created_at" : "2015-09-28 17:07:31 +0000",
    "user" : {
      "name" : "\u00C1ine Dempsey",
      "screen_name" : "AineDempsey",
      "protected" : false,
      "id_str" : "64555862",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/983312157898149888\/FFI9WHlB_normal.jpg",
      "id" : 64555862,
      "verified" : false
    }
  },
  "id" : 648779082872811520,
  "created_at" : "2015-09-29 08:39:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "indices" : [ 3, 15 ],
      "id_str" : "33462545",
      "id" : 33462545
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/8O7lXRa6WB",
      "expanded_url" : "http:\/\/flip.it\/d8ldZ",
      "display_url" : "flip.it\/d8ldZ"
    } ]
  },
  "geo" : { },
  "id_str" : "648777760723312641",
  "text" : "RT @yapphenghui: Newswire: Ridley Scott knew there was water on Mars, didn\u2019t have time to change The Martian\n\nhttp:\/\/t.co\/8O7lXRa6WB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/8O7lXRa6WB",
        "expanded_url" : "http:\/\/flip.it\/d8ldZ",
        "display_url" : "flip.it\/d8ldZ"
      } ]
    },
    "geo" : { },
    "id_str" : "648775140470185984",
    "text" : "Newswire: Ridley Scott knew there was water on Mars, didn\u2019t have time to change The Martian\n\nhttp:\/\/t.co\/8O7lXRa6WB",
    "id" : 648775140470185984,
    "created_at" : "2015-09-29 08:23:41 +0000",
    "user" : {
      "name" : "Yap Pheng Hui \u8449\u9D6C\u98DB",
      "screen_name" : "yapphenghui",
      "protected" : false,
      "id_str" : "33462545",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2206236724\/_____2012APR-1_tweeter_normal.jpg",
      "id" : 33462545,
      "verified" : false
    }
  },
  "id" : 648777760723312641,
  "created_at" : "2015-09-29 08:34:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Bus",
      "screen_name" : "dublinbusnews",
      "indices" : [ 8, 22 ],
      "id_str" : "80549724",
      "id" : 80549724
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648774018145865728",
  "text" : "Nice of @dublinbusnews No. 123 driver to stop at Bus Stop No. 2185 to inform a blind passenger the bus number in case she miss it.",
  "id" : 648774018145865728,
  "created_at" : "2015-09-29 08:19:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fb",
      "indices" : [ 83, 86 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648761452497661952",
  "text" : "RT @SimonPRepublic: Can anyone recommend a dog-friendly hotel in Northern Ireland? #fb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows Phone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "fb",
        "indices" : [ 63, 66 ]
      } ],
      "urls" : [ ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 53.3175726996422, -6.23769262164575 ]
    },
    "id_str" : "648755671484264448",
    "text" : "Can anyone recommend a dog-friendly hotel in Northern Ireland? #fb",
    "id" : 648755671484264448,
    "created_at" : "2015-09-29 07:06:20 +0000",
    "user" : {
      "name" : "Simon Palmer \uD83C\uDFA7\uD83D\uDCFB\uD83C\uDDEC\uD83C\uDDE7\uD83C\uDDEE\uD83C\uDDEA",
      "screen_name" : "SimonJohnPalmer",
      "protected" : false,
      "id_str" : "20548160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766050295835230209\/Vx9bcd6l_normal.jpg",
      "id" : 20548160,
      "verified" : true
    }
  },
  "id" : 648761452497661952,
  "created_at" : "2015-09-29 07:29:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Water",
      "screen_name" : "IrishWater",
      "indices" : [ 3, 14 ],
      "id_str" : "1147795944",
      "id" : 1147795944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648580722018656257",
  "text" : "RT @IrishWater: Did you know the water you used in today's shower will take 2 days to return safely to the environment? http:\/\/t.co\/nvhgVkI\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishWater\/status\/645870112973590528\/photo\/1",
        "indices" : [ 104, 126 ],
        "url" : "http:\/\/t.co\/nvhgVkIggX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CM2KfY0VEAA7Bz_.jpg",
        "id_str" : "634315394371620864",
        "id" : 634315394371620864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CM2KfY0VEAA7Bz_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nvhgVkIggX"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "645870112973590528",
    "text" : "Did you know the water you used in today's shower will take 2 days to return safely to the environment? http:\/\/t.co\/nvhgVkIggX",
    "id" : 645870112973590528,
    "created_at" : "2015-09-21 08:00:09 +0000",
    "user" : {
      "name" : "Irish Water",
      "screen_name" : "IrishWater",
      "protected" : false,
      "id_str" : "1147795944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877444273825886208\/J15goFjO_normal.jpg",
      "id" : 1147795944,
      "verified" : true
    }
  },
  "id" : 648580722018656257,
  "created_at" : "2015-09-28 19:31:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BI Tech",
      "screen_name" : "SAI",
      "indices" : [ 3, 7 ],
      "id_str" : "8841372",
      "id" : 8841372
    }, {
      "name" : "BI UK Tech",
      "screen_name" : "BIUK_Tech",
      "indices" : [ 99, 109 ],
      "id_str" : "3034814919",
      "id" : 3034814919
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 94 ],
      "url" : "http:\/\/t.co\/ocUs0qkajq",
      "expanded_url" : "http:\/\/read.bi\/1R28oIr",
      "display_url" : "read.bi\/1R28oIr"
    } ]
  },
  "geo" : { },
  "id_str" : "648579755940413441",
  "text" : "RT @SAI: Chinese smartphone giant Xiaomi is muscling in on Apple's turf http:\/\/t.co\/ocUs0qkajq via @BIUK_Tech",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BI UK Tech",
        "screen_name" : "BIUK_Tech",
        "indices" : [ 90, 100 ],
        "id_str" : "3034814919",
        "id" : 3034814919
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 63, 85 ],
        "url" : "http:\/\/t.co\/ocUs0qkajq",
        "expanded_url" : "http:\/\/read.bi\/1R28oIr",
        "display_url" : "read.bi\/1R28oIr"
      } ]
    },
    "geo" : { },
    "id_str" : "647000173021954048",
    "text" : "Chinese smartphone giant Xiaomi is muscling in on Apple's turf http:\/\/t.co\/ocUs0qkajq via @BIUK_Tech",
    "id" : 647000173021954048,
    "created_at" : "2015-09-24 10:50:36 +0000",
    "user" : {
      "name" : "BI Tech",
      "screen_name" : "SAI",
      "protected" : false,
      "id_str" : "8841372",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/887665936735637509\/n0YaP0YQ_normal.jpg",
      "id" : 8841372,
      "verified" : true
    }
  },
  "id" : 648579755940413441,
  "created_at" : "2015-09-28 19:27:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amanda",
      "screen_name" : "Hamperlady",
      "indices" : [ 15, 26 ],
      "id_str" : "19398524",
      "id" : 19398524
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648579487668506624",
  "text" : "@bharatidalela @Hamperlady 6 weeks processing?",
  "id" : 648579487668506624,
  "created_at" : "2015-09-28 19:26:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/RhSZroYHym",
      "expanded_url" : "https:\/\/twitter.com\/Hamperlady\/status\/648553562067353600",
      "display_url" : "twitter.com\/Hamperlady\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648560650004922368",
  "text" : "To borrow the word of an Irish famous film star :\"shake them down for a few quid.\"  https:\/\/t.co\/RhSZroYHym",
  "id" : 648560650004922368,
  "created_at" : "2015-09-28 18:11:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Syed Balkhi",
      "screen_name" : "syedbalkhi",
      "indices" : [ 3, 14 ],
      "id_str" : "16281313",
      "id" : 16281313
    }, {
      "name" : "Alex King",
      "screen_name" : "alexkingorg",
      "indices" : [ 20, 32 ],
      "id_str" : "101143",
      "id" : 101143
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WordPress",
      "indices" : [ 94, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648558704921575424",
  "text" : "RT @syedbalkhi: RIP @alexkingorg - Will miss all the good times we had and thank you for your #WordPress contributions.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Alex King",
        "screen_name" : "alexkingorg",
        "indices" : [ 4, 16 ],
        "id_str" : "101143",
        "id" : 101143
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WordPress",
        "indices" : [ 78, 88 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "648556453733777408",
    "text" : "RIP @alexkingorg - Will miss all the good times we had and thank you for your #WordPress contributions.",
    "id" : 648556453733777408,
    "created_at" : "2015-09-28 17:54:42 +0000",
    "user" : {
      "name" : "Syed Balkhi",
      "screen_name" : "syedbalkhi",
      "protected" : false,
      "id_str" : "16281313",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/517443601110802432\/niI3ANd0_normal.jpeg",
      "id" : 16281313,
      "verified" : true
    }
  },
  "id" : 648558704921575424,
  "created_at" : "2015-09-28 18:03:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/RhSZroYHym",
      "expanded_url" : "https:\/\/twitter.com\/Hamperlady\/status\/648553562067353600",
      "display_url" : "twitter.com\/Hamperlady\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648558383549816832",
  "text" : "Not to mention, you can't leave Ireland for 1 year during your application.  https:\/\/t.co\/RhSZroYHym",
  "id" : 648558383549816832,
  "created_at" : "2015-09-28 18:02:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 92 ],
      "url" : "http:\/\/t.co\/0YtniyEyiw",
      "expanded_url" : "http:\/\/gu.com\/p\/465x6\/stw",
      "display_url" : "gu.com\/p\/465x6\/stw"
    } ]
  },
  "geo" : { },
  "id_str" : "648476916350107648",
  "text" : "Insurance giant's cycling 'study' shows the perils of poor statistics http:\/\/t.co\/0YtniyEyiw",
  "id" : 648476916350107648,
  "created_at" : "2015-09-28 12:38:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http:\/\/t.co\/068kFtfYrp",
      "expanded_url" : "http:\/\/adexchanger.com\/publishers\/the-secret-way-publishers-are-going-viral-on-facebook\/",
      "display_url" : "adexchanger.com\/publishers\/the\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648434555368701952",
  "text" : "A powerful way Publisher use to reach their audience measure by cost per visit http:\/\/t.co\/068kFtfYrp &lt; this run afoul of FB rule",
  "id" : 648434555368701952,
  "created_at" : "2015-09-28 09:50:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/dwzEaRw7fr",
      "expanded_url" : "https:\/\/twitter.com\/YouGov\/status\/648424827427115009",
      "display_url" : "twitter.com\/YouGov\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648427272605667328",
  "text" : "Ireland not in the pics... https:\/\/t.co\/dwzEaRw7fr",
  "id" : 648427272605667328,
  "created_at" : "2015-09-28 09:21:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 28 ],
      "url" : "https:\/\/t.co\/YuvQR8FHZG",
      "expanded_url" : "https:\/\/twitter.com\/NewNationsg\/status\/648425646687817728",
      "display_url" : "twitter.com\/NewNationsg\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648426918891663360",
  "text" : "LOL  https:\/\/t.co\/YuvQR8FHZG",
  "id" : 648426918891663360,
  "created_at" : "2015-09-28 09:19:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/83z9eVqphH",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Ilo_Ilo",
      "display_url" : "en.wikipedia.org\/wiki\/Ilo_Ilo"
    } ]
  },
  "geo" : { },
  "id_str" : "648251434748805120",
  "text" : "On BBC 4 now, showing Singapore Film Ilo Ilo https:\/\/t.co\/83z9eVqphH",
  "id" : 648251434748805120,
  "created_at" : "2015-09-27 21:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/Xyf7hHutny",
      "expanded_url" : "https:\/\/newsroom.fb.com\/news\/2015\/09\/updates-for-facebook-notes\/",
      "display_url" : "newsroom.fb.com\/news\/2015\/09\/u\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648234298915573760",
  "text" : "The new Facebook Notes https:\/\/t.co\/Xyf7hHutny",
  "id" : 648234298915573760,
  "created_at" : "2015-09-27 20:34:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steve Leonard",
      "screen_name" : "steveleonardSG",
      "indices" : [ 3, 18 ],
      "id_str" : "2574271958",
      "id" : 2574271958
    }, {
      "name" : "Google",
      "screen_name" : "Google",
      "indices" : [ 30, 37 ],
      "id_str" : "20536157",
      "id" : 20536157
    }, {
      "name" : "Microsoft",
      "screen_name" : "Microsoft",
      "indices" : [ 38, 48 ],
      "id_str" : "74286565",
      "id" : 74286565
    }, {
      "name" : "Adobe",
      "screen_name" : "Adobe",
      "indices" : [ 49, 55 ],
      "id_str" : "63786611",
      "id" : 63786611
    }, {
      "name" : "Narendra Modi",
      "screen_name" : "narendramodi",
      "indices" : [ 121, 134 ],
      "id_str" : "18839785",
      "id" : 18839785
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648229049823797248",
  "text" : "RT @steveleonardSG: Amazing \u2014 @google @Microsoft @Adobe and 20% of tech start-ups in Valley run by Indian-origin leaders @narendramodi  htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 10, 17 ],
        "id_str" : "20536157",
        "id" : 20536157
      }, {
        "name" : "Microsoft",
        "screen_name" : "Microsoft",
        "indices" : [ 18, 28 ],
        "id_str" : "74286565",
        "id" : 74286565
      }, {
        "name" : "Adobe",
        "screen_name" : "Adobe",
        "indices" : [ 29, 35 ],
        "id_str" : "63786611",
        "id" : 63786611
      }, {
        "name" : "Narendra Modi",
        "screen_name" : "narendramodi",
        "indices" : [ 101, 114 ],
        "id_str" : "18839785",
        "id" : 18839785
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/pnLzKdGcDc",
        "expanded_url" : "https:\/\/twitter.com\/techmeme\/status\/647985846654644224",
        "display_url" : "twitter.com\/techmeme\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "648046818949894144",
    "text" : "Amazing \u2014 @google @Microsoft @Adobe and 20% of tech start-ups in Valley run by Indian-origin leaders @narendramodi  https:\/\/t.co\/pnLzKdGcDc",
    "id" : 648046818949894144,
    "created_at" : "2015-09-27 08:09:36 +0000",
    "user" : {
      "name" : "Steve Leonard",
      "screen_name" : "steveleonardSG",
      "protected" : false,
      "id_str" : "2574271958",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/483116363196542976\/3DLw1LIK_normal.jpeg",
      "id" : 2574271958,
      "verified" : false
    }
  },
  "id" : 648229049823797248,
  "created_at" : "2015-09-27 20:13:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mustafa khalili",
      "screen_name" : "muskhalili",
      "indices" : [ 3, 14 ],
      "id_str" : "181523152",
      "id" : 181523152
    }, {
      "name" : "Hamish Gill | 35mmc",
      "screen_name" : "HamishGill",
      "indices" : [ 72, 83 ],
      "id_str" : "1447275072",
      "id" : 1447275072
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "feelingsmutual",
      "indices" : [ 84, 99 ]
    } ],
    "urls" : [ {
      "indices" : [ 29, 51 ],
      "url" : "http:\/\/t.co\/00grSJrWlM",
      "expanded_url" : "http:\/\/www.35mmc.com\/13\/04\/2014\/leica-m7\/",
      "display_url" : "35mmc.com\/13\/04\/2014\/lei\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648223544590094337",
  "text" : "RT @muskhalili: The Leica M7 http:\/\/t.co\/00grSJrWlM excellent review by @HamishGill #feelingsmutual",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hamish Gill | 35mmc",
        "screen_name" : "HamishGill",
        "indices" : [ 56, 67 ],
        "id_str" : "1447275072",
        "id" : 1447275072
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "feelingsmutual",
        "indices" : [ 68, 83 ]
      } ],
      "urls" : [ {
        "indices" : [ 13, 35 ],
        "url" : "http:\/\/t.co\/00grSJrWlM",
        "expanded_url" : "http:\/\/www.35mmc.com\/13\/04\/2014\/leica-m7\/",
        "display_url" : "35mmc.com\/13\/04\/2014\/lei\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "648203831176757253",
    "text" : "The Leica M7 http:\/\/t.co\/00grSJrWlM excellent review by @HamishGill #feelingsmutual",
    "id" : 648203831176757253,
    "created_at" : "2015-09-27 18:33:31 +0000",
    "user" : {
      "name" : "mustafa khalili",
      "screen_name" : "muskhalili",
      "protected" : false,
      "id_str" : "181523152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/584380877100187649\/1iI3VBJL_normal.jpg",
      "id" : 181523152,
      "verified" : true
    }
  },
  "id" : 648223544590094337,
  "created_at" : "2015-09-27 19:51:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lorenzo Pasqualis",
      "screen_name" : "lpasqualis",
      "indices" : [ 3, 14 ],
      "id_str" : "19576991",
      "id" : 19576991
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648221983394693120",
  "text" : "RT @lpasqualis: The European web-cookie law is a joke that will slow down the EU in a race toward innovation.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "648209405452791809",
    "text" : "The European web-cookie law is a joke that will slow down the EU in a race toward innovation.",
    "id" : 648209405452791809,
    "created_at" : "2015-09-27 18:55:40 +0000",
    "user" : {
      "name" : "Lorenzo Pasqualis",
      "screen_name" : "lpasqualis",
      "protected" : false,
      "id_str" : "19576991",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034201018685255680\/Cx4d4OIH_normal.jpg",
      "id" : 19576991,
      "verified" : false
    }
  },
  "id" : 648221983394693120,
  "created_at" : "2015-09-27 19:45:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Joe Haslam \u2618",
      "screen_name" : "joehas",
      "indices" : [ 3, 10 ],
      "id_str" : "10836092",
      "id" : 10836092
    }, {
      "name" : "Narendra Modi",
      "screen_name" : "narendramodi",
      "indices" : [ 117, 130 ],
      "id_str" : "18839785",
      "id" : 18839785
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648185507923501056",
  "text" : "RT @joehas: As the CEOs of Google, Microsoft &amp; Adobe are Indian born, a star studded gala to meet Prime Minister @narendramodi http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Narendra Modi",
        "screen_name" : "narendramodi",
        "indices" : [ 105, 118 ],
        "id_str" : "18839785",
        "id" : 18839785
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 141 ],
        "url" : "http:\/\/t.co\/HejEAuLi0G",
        "expanded_url" : "http:\/\/indianexpress.com\/article\/india\/india-news-india\/in-silicon-valley-pm-modi-meets-satya-nadella-sundar-pichai-at-digital-india-dinner\/",
        "display_url" : "indianexpress.com\/article\/india\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "648177136919027712",
    "text" : "As the CEOs of Google, Microsoft &amp; Adobe are Indian born, a star studded gala to meet Prime Minister @narendramodi http:\/\/t.co\/HejEAuLi0G",
    "id" : 648177136919027712,
    "created_at" : "2015-09-27 16:47:26 +0000",
    "user" : {
      "name" : "Joe Haslam \u2618",
      "screen_name" : "joehas",
      "protected" : false,
      "id_str" : "10836092",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978292949862764544\/yCfpWt-E_normal.jpg",
      "id" : 10836092,
      "verified" : false
    }
  },
  "id" : 648185507923501056,
  "created_at" : "2015-09-27 17:20:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http:\/\/t.co\/BRa46XRj14",
      "expanded_url" : "http:\/\/beta.data.gov.sg\/",
      "display_url" : "beta.data.gov.sg"
    } ]
  },
  "geo" : { },
  "id_str" : "648084048842244096",
  "text" : "Welcome to Singapore new open data portal http:\/\/t.co\/BRa46XRj14",
  "id" : 648084048842244096,
  "created_at" : "2015-09-27 10:37:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline McFerran",
      "screen_name" : "TheNotoriousOAP",
      "indices" : [ 3, 19 ],
      "id_str" : "1162475924",
      "id" : 1162475924
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IREvROM",
      "indices" : [ 43, 51 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648068390972694528",
  "text" : "RT @TheNotoriousOAP: So I now have a spare #IREvROM ticket for today! Category D. If anyone wants it? Sadly you'll be sitting next to me. #\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IREvROM",
        "indices" : [ 22, 30 ]
      }, {
        "text" : "rugby",
        "indices" : [ 117, 123 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "648065448983064576",
    "text" : "So I now have a spare #IREvROM ticket for today! Category D. If anyone wants it? Sadly you'll be sitting next to me. #rugby",
    "id" : 648065448983064576,
    "created_at" : "2015-09-27 09:23:38 +0000",
    "user" : {
      "name" : "Pauline McFerran",
      "screen_name" : "TheNotoriousOAP",
      "protected" : false,
      "id_str" : "1162475924",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/982803401695289344\/09vMKXHH_normal.jpg",
      "id" : 1162475924,
      "verified" : false
    }
  },
  "id" : 648068390972694528,
  "created_at" : "2015-09-27 09:35:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648066347843342336",
  "text" : "Hashtag IBYE looks like Ireland (Good) Bye. It could be the hashtag when a tech conference moved to Lisbon.",
  "id" : 648066347843342336,
  "created_at" : "2015-09-27 09:27:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Roe",
      "screen_name" : "jasonroe",
      "indices" : [ 3, 12 ],
      "id_str" : "2643021",
      "id" : 2643021
    }, {
      "name" : "Web Summit Has Moved",
      "screen_name" : "websummithq",
      "indices" : [ 118, 130 ],
      "id_str" : "776335315183304708",
      "id" : 776335315183304708
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "648065539827466240",
  "text" : "RT @jasonroe: Irish Government admits we aren't at the races for top tech investment.. this article says it all Sigh! @WebSummitHQ  http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Web Summit Has Moved",
        "screen_name" : "websummithq",
        "indices" : [ 104, 116 ],
        "id_str" : "776335315183304708",
        "id" : 776335315183304708
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/ZOVQD7yG4S",
        "expanded_url" : "http:\/\/m.independent.ie\/business\/technology\/dublin-runs-with-the-alsorans-as-government-admits-we-arent-at-the-races-for-top-tech-investments-31560068.html?utm_source=Newsweaver&utm_medium=email&utm_term=Adrian+Weckler%3A+Dublin+runs+with+the+also-rans+as+Government+admits+we+arent+at+the+races+for+top+tech+investments&utm_campaign=%27I+recognise+that+I+used+an+inappropriate+analogy%27+-+Johnny+Ronan+apologises",
        "display_url" : "m.independent.ie\/business\/techn\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "648059421520711680",
    "text" : "Irish Government admits we aren't at the races for top tech investment.. this article says it all Sigh! @WebSummitHQ  http:\/\/t.co\/ZOVQD7yG4S",
    "id" : 648059421520711680,
    "created_at" : "2015-09-27 08:59:41 +0000",
    "user" : {
      "name" : "Jason Roe",
      "screen_name" : "jasonroe",
      "protected" : false,
      "id_str" : "2643021",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034463683659878400\/mVWdG73g_normal.jpg",
      "id" : 2643021,
      "verified" : false
    }
  },
  "id" : 648065539827466240,
  "created_at" : "2015-09-27 09:23:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/jkqpV97FzP",
      "expanded_url" : "https:\/\/twitter.com\/ailieirv\/status\/647432889068597249",
      "display_url" : "twitter.com\/ailieirv\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "648051475554263040",
  "text" : "Thank You Ailish.  https:\/\/t.co\/jkqpV97FzP",
  "id" : 648051475554263040,
  "created_at" : "2015-09-27 08:28:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/z5ySd2clHW",
      "expanded_url" : "https:\/\/twitter.com\/hdipsda",
      "display_url" : "twitter.com\/hdipsda"
    } ]
  },
  "geo" : { },
  "id_str" : "647913498694025216",
  "text" : "Currently studying Higher Diploma in Science in Data Analytics at the National College of Ireland. Tweeting at http:\/\/t.co\/z5ySd2clHW",
  "id" : 647913498694025216,
  "created_at" : "2015-09-26 23:19:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Drogheda",
      "indices" : [ 41, 50 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 112, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647758340777504768",
  "text" : "RT @IrlEmbSingapore: George Coleman from #Drogheda is buried in Fort Canning Park. Coleman designed many famous #Singapore buildings http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/647693661132599296\/photo\/1",
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/QgxhvYYqBg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CP0R8_oUkAAmovI.jpg",
        "id_str" : "647693660981596160",
        "id" : 647693660981596160,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CP0R8_oUkAAmovI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 660,
          "resize" : "fit",
          "w" : 880
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 660,
          "resize" : "fit",
          "w" : 880
        }, {
          "h" : 660,
          "resize" : "fit",
          "w" : 880
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QgxhvYYqBg"
      } ],
      "hashtags" : [ {
        "text" : "Drogheda",
        "indices" : [ 20, 29 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 91, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647693661132599296",
    "text" : "George Coleman from #Drogheda is buried in Fort Canning Park. Coleman designed many famous #Singapore buildings http:\/\/t.co\/QgxhvYYqBg",
    "id" : 647693661132599296,
    "created_at" : "2015-09-26 08:46:17 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 647758340777504768,
  "created_at" : "2015-09-26 13:03:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David",
      "screen_name" : "davegantly",
      "indices" : [ 3, 14 ],
      "id_str" : "16738907",
      "id" : 16738907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/YokDEnN00d",
      "expanded_url" : "https:\/\/twitter.com\/LanaReddy\/status\/647757037720477701",
      "display_url" : "twitter.com\/LanaReddy\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647758155473137664",
  "text" : "RT @davegantly: Only 90ish sleeps!! So excited!!  https:\/\/t.co\/YokDEnN00d",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/YokDEnN00d",
        "expanded_url" : "https:\/\/twitter.com\/LanaReddy\/status\/647757037720477701",
        "display_url" : "twitter.com\/LanaReddy\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "647757374825099264",
    "text" : "Only 90ish sleeps!! So excited!!  https:\/\/t.co\/YokDEnN00d",
    "id" : 647757374825099264,
    "created_at" : "2015-09-26 12:59:27 +0000",
    "user" : {
      "name" : "David",
      "screen_name" : "davegantly",
      "protected" : false,
      "id_str" : "16738907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001158218519863296\/rY-yu2r7_normal.jpg",
      "id" : 16738907,
      "verified" : false
    }
  },
  "id" : 647758155473137664,
  "created_at" : "2015-09-26 13:02:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u0130yad el-Baghdadi | \u0625\u064A\u0627\u062F \u0627\u0644\u0628\u063A\u062F\u0627\u062F\u064A",
      "screen_name" : "iyad_elbaghdadi",
      "indices" : [ 3, 19 ],
      "id_str" : "18725815",
      "id" : 18725815
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647756884322177025",
  "text" : "RT @iyad_elbaghdadi: \"But these refugees look well dressed &amp; fed\". For the 1000th time, they're NOT fleeing poverty, they're fleeing war. h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iyad_elbaghdadi\/status\/647320836156878848\/photo\/1",
        "indices" : [ 121, 143 ],
        "url" : "http:\/\/t.co\/u7D2XesZwj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPu-nhLWIAQ0R6x.jpg",
        "id_str" : "647320557587931140",
        "id" : 647320557587931140,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPu-nhLWIAQ0R6x.jpg",
        "sizes" : [ {
          "h" : 314,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 314,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 314,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 314,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/u7D2XesZwj"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647320836156878848",
    "text" : "\"But these refugees look well dressed &amp; fed\". For the 1000th time, they're NOT fleeing poverty, they're fleeing war. http:\/\/t.co\/u7D2XesZwj",
    "id" : 647320836156878848,
    "created_at" : "2015-09-25 08:04:48 +0000",
    "user" : {
      "name" : "\u0130yad el-Baghdadi | \u0625\u064A\u0627\u062F \u0627\u0644\u0628\u063A\u062F\u0627\u062F\u064A",
      "screen_name" : "iyad_elbaghdadi",
      "protected" : false,
      "id_str" : "18725815",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/952890636880969728\/E3d7MFHV_normal.jpg",
      "id" : 18725815,
      "verified" : true
    }
  },
  "id" : 647756884322177025,
  "created_at" : "2015-09-26 12:57:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "danhannanMEP",
      "screen_name" : "DanHannanMEP",
      "indices" : [ 3, 16 ],
      "id_str" : "747141373376204800",
      "id" : 747141373376204800
    }, {
      "name" : "Wayne Dahlberg",
      "screen_name" : "waynedahlberg",
      "indices" : [ 98, 112 ],
      "id_str" : "8749312",
      "id" : 8749312
    }, {
      "name" : "Will Heaven",
      "screen_name" : "WillHeaven",
      "indices" : [ 113, 124 ],
      "id_str" : "18421050",
      "id" : 18421050
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647754971597918208",
  "text" : "RT @DanHannanMEP: A truly wonderful photograph. We have forgotten how to live in the present. Via @waynedahlberg @WillHeaven. http:\/\/t.co\/O\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Wayne Dahlberg",
        "screen_name" : "waynedahlberg",
        "indices" : [ 80, 94 ],
        "id_str" : "8749312",
        "id" : 8749312
      }, {
        "name" : "Will Heaven",
        "screen_name" : "WillHeaven",
        "indices" : [ 95, 106 ],
        "id_str" : "18421050",
        "id" : 18421050
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DanHannanMEP\/status\/647745678962245632\/photo\/1",
        "indices" : [ 108, 130 ],
        "url" : "http:\/\/t.co\/Oe1zMNAITX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CP1BQqmWcAA_KAm.png",
        "id_str" : "647745675980075008",
        "id" : 647745675980075008,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CP1BQqmWcAA_KAm.png",
        "sizes" : [ {
          "h" : 344,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 344,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 344,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 344,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Oe1zMNAITX"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647745678962245632",
    "text" : "A truly wonderful photograph. We have forgotten how to live in the present. Via @waynedahlberg @WillHeaven. http:\/\/t.co\/Oe1zMNAITX",
    "id" : 647745678962245632,
    "created_at" : "2015-09-26 12:12:59 +0000",
    "user" : {
      "name" : "Daniel Hannan",
      "screen_name" : "DanielJHannan",
      "protected" : false,
      "id_str" : "85794542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/929023116755451905\/DoiHXe1M_normal.jpg",
      "id" : 85794542,
      "verified" : true
    }
  },
  "id" : 647754971597918208,
  "created_at" : "2015-09-26 12:49:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/SQ9ocsiH1Y",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/647753174795747328",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647754880111747072",
  "text" : "The country phonetic sound like \"smoke you till you dead\" in Mandarin. https:\/\/t.co\/SQ9ocsiH1Y",
  "id" : 647754880111747072,
  "created_at" : "2015-09-26 12:49:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Felicia Sonmez",
      "screen_name" : "feliciasonmez",
      "indices" : [ 3, 17 ],
      "id_str" : "137466464",
      "id" : 137466464
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "beijing",
      "indices" : [ 59, 67 ]
    }, {
      "text" : "\u4E2D\u79CB\u8282\u5FEB\u4E50",
      "indices" : [ 68, 74 ]
    } ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/RUVBtS51cf",
      "expanded_url" : "https:\/\/instagram.com\/p\/8FY2NSyzbB\/",
      "display_url" : "instagram.com\/p\/8FY2NSyzbB\/"
    } ]
  },
  "geo" : { },
  "id_str" : "647683145182117888",
  "text" : "RT @feliciasonmez: giant mooncakes for Mid-Autumn Festival #beijing #\u4E2D\u79CB\u8282\u5FEB\u4E50 https:\/\/t.co\/RUVBtS51cf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "beijing",
        "indices" : [ 40, 48 ]
      }, {
        "text" : "\u4E2D\u79CB\u8282\u5FEB\u4E50",
        "indices" : [ 49, 55 ]
      } ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/RUVBtS51cf",
        "expanded_url" : "https:\/\/instagram.com\/p\/8FY2NSyzbB\/",
        "display_url" : "instagram.com\/p\/8FY2NSyzbB\/"
      } ]
    },
    "geo" : { },
    "id_str" : "647662947553148928",
    "text" : "giant mooncakes for Mid-Autumn Festival #beijing #\u4E2D\u79CB\u8282\u5FEB\u4E50 https:\/\/t.co\/RUVBtS51cf",
    "id" : 647662947553148928,
    "created_at" : "2015-09-26 06:44:14 +0000",
    "user" : {
      "name" : "Felicia Sonmez",
      "screen_name" : "feliciasonmez",
      "protected" : false,
      "id_str" : "137466464",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014338227866439686\/uGzjPIR5_normal.jpg",
      "id" : 137466464,
      "verified" : true
    }
  },
  "id" : 647683145182117888,
  "created_at" : "2015-09-26 08:04:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "indices" : [ 3, 11 ],
      "id_str" : "15019743",
      "id" : 15019743
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647682242806980610",
  "text" : "RT @2bscene: Yes, this Irish politician was actually voted into government. This is just one example of their idiotic strategies..\nhttp:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/ISx0wswXnD",
        "expanded_url" : "http:\/\/m.independent.ie\/irish-news\/digital-obsession\/every-child-over-five-should-have-an-ipad-says-noonan-31559634.html",
        "display_url" : "m.independent.ie\/irish-news\/dig\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "647666191062933504",
    "text" : "Yes, this Irish politician was actually voted into government. This is just one example of their idiotic strategies..\nhttp:\/\/t.co\/ISx0wswXnD",
    "id" : 647666191062933504,
    "created_at" : "2015-09-26 06:57:07 +0000",
    "user" : {
      "name" : "Tom Doyle",
      "screen_name" : "2bscene",
      "protected" : false,
      "id_str" : "15019743",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/779435193312608256\/NJW57jaX_normal.jpg",
      "id" : 15019743,
      "verified" : false
    }
  },
  "id" : 647682242806980610,
  "created_at" : "2015-09-26 08:00:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vintage",
      "screen_name" : "MsveraChou",
      "indices" : [ 3, 14 ],
      "id_str" : "573757199",
      "id" : 573757199
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647681256591892480",
  "text" : "RT @MsveraChou: \u771F\u7684\u5F88\u53D7\u4E0D\u4E86\u90A3\u4E9B\u4F86\u6FB3\u9580\u65C5\u904A\u7684\u4E2D\u570B\u5973\u4EBA\uFF0C\u5728\u9910\u5EF3\u88E1\u626F\u8457\u55D3\u5B50\u8DDF\u9694\u58C1\u684C\u8B1B\u8A71\uFF0C\u978B\u5B50\u812B\u6389\u628A\u817F\u6A6B\u5728\u65C1\u908A\u7684\u6905\u5B50\u4E0A\uFF0C\u770B\u4F86\u4E5F\u624D\u4E09\u5341\u6B72\u5DE6\u53F3\uFF0C\u8B1B\u500B\u96FB\u8A71\u62FF\u8457iPhone\u7528\u558A\u7684\uFF0C\u5728\u4E2D\u570B\u90FD\u6C92\u770B\u904E\u9019\u9EBC\u919C\u964B\u7684\u3002",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647672657832349696",
    "text" : "\u771F\u7684\u5F88\u53D7\u4E0D\u4E86\u90A3\u4E9B\u4F86\u6FB3\u9580\u65C5\u904A\u7684\u4E2D\u570B\u5973\u4EBA\uFF0C\u5728\u9910\u5EF3\u88E1\u626F\u8457\u55D3\u5B50\u8DDF\u9694\u58C1\u684C\u8B1B\u8A71\uFF0C\u978B\u5B50\u812B\u6389\u628A\u817F\u6A6B\u5728\u65C1\u908A\u7684\u6905\u5B50\u4E0A\uFF0C\u770B\u4F86\u4E5F\u624D\u4E09\u5341\u6B72\u5DE6\u53F3\uFF0C\u8B1B\u500B\u96FB\u8A71\u62FF\u8457iPhone\u7528\u558A\u7684\uFF0C\u5728\u4E2D\u570B\u90FD\u6C92\u770B\u904E\u9019\u9EBC\u919C\u964B\u7684\u3002",
    "id" : 647672657832349696,
    "created_at" : "2015-09-26 07:22:49 +0000",
    "user" : {
      "name" : "Vintage",
      "screen_name" : "MsveraChou",
      "protected" : false,
      "id_str" : "573757199",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011484054007382016\/CDIi4vqr_normal.jpg",
      "id" : 573757199,
      "verified" : false
    }
  },
  "id" : 647681256591892480,
  "created_at" : "2015-09-26 07:56:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "Lee Kuan Yew School",
      "screen_name" : "LKYSch",
      "indices" : [ 28, 35 ],
      "id_str" : "832683854",
      "id" : 832683854
    }, {
      "name" : "Taavi Kotka",
      "screen_name" : "taavikotka",
      "indices" : [ 52, 63 ],
      "id_str" : "333443465",
      "id" : 333443465
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Estonia",
      "indices" : [ 83, 91 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647556020206718976",
  "text" : "RT @GeoffreyIRL: Great talk @LKYSch this evening by @taavikotka on e-residency. Go #Estonia, the largest country in the world! http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Lee Kuan Yew School",
        "screen_name" : "LKYSch",
        "indices" : [ 11, 18 ],
        "id_str" : "832683854",
        "id" : 832683854
      }, {
        "name" : "Taavi Kotka",
        "screen_name" : "taavikotka",
        "indices" : [ 35, 46 ],
        "id_str" : "333443465",
        "id" : 333443465
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/647427182411673600\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/MbwhjdEJki",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPwflBmUcAEiJDB.jpg",
        "id_str" : "647427167379288065",
        "id" : 647427167379288065,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPwflBmUcAEiJDB.jpg",
        "sizes" : [ {
          "h" : 529,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 796,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 796,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 796,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MbwhjdEJki"
      } ],
      "hashtags" : [ {
        "text" : "Estonia",
        "indices" : [ 66, 74 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647427182411673600",
    "text" : "Great talk @LKYSch this evening by @taavikotka on e-residency. Go #Estonia, the largest country in the world! http:\/\/t.co\/MbwhjdEJki",
    "id" : 647427182411673600,
    "created_at" : "2015-09-25 15:07:23 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 647556020206718976,
  "created_at" : "2015-09-25 23:39:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647546774136193025",
  "text" : "What if the centenarian kick the bucket on national TV?\nJust Asking",
  "id" : 647546774136193025,
  "created_at" : "2015-09-25 23:02:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/lh5Jn2AV4I",
      "expanded_url" : "https:\/\/processing.org\/",
      "display_url" : "processing.org"
    } ]
  },
  "geo" : { },
  "id_str" : "647380720399314944",
  "text" : "Learn about this https:\/\/t.co\/lh5Jn2AV4I from a secondary school student at a recent Open Evening.",
  "id" : 647380720399314944,
  "created_at" : "2015-09-25 12:02:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 73 ],
      "url" : "http:\/\/t.co\/Bmd0868rjE",
      "expanded_url" : "http:\/\/www.harvarddesignmagazine.org\/issues\/39\/built-on-sand-singapore-and-the-new-state-of-risk",
      "display_url" : "harvarddesignmagazine.org\/issues\/39\/buil\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647379236030955520",
  "text" : "Built on Sand: Singapore and the New State of Risk http:\/\/t.co\/Bmd0868rjE",
  "id" : 647379236030955520,
  "created_at" : "2015-09-25 11:56:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Skilling",
      "screen_name" : "dskilling",
      "indices" : [ 3, 13 ],
      "id_str" : "238014380",
      "id" : 238014380
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647291660808929280",
  "text" : "RT @dskilling: 'Singapore manufacturing shrinks 7% in August as output falls for 7th straight month': weak economic signs in S'pore http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/nnVM0qKaI8",
        "expanded_url" : "http:\/\/str.sg\/Z24A",
        "display_url" : "str.sg\/Z24A"
      } ]
    },
    "geo" : { },
    "id_str" : "647289300011237377",
    "text" : "'Singapore manufacturing shrinks 7% in August as output falls for 7th straight month': weak economic signs in S'pore http:\/\/t.co\/nnVM0qKaI8",
    "id" : 647289300011237377,
    "created_at" : "2015-09-25 05:59:29 +0000",
    "user" : {
      "name" : "David Skilling",
      "screen_name" : "dskilling",
      "protected" : false,
      "id_str" : "238014380",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/626307724070903809\/lpleNABF_normal.jpg",
      "id" : 238014380,
      "verified" : false
    }
  },
  "id" : 647291660808929280,
  "created_at" : "2015-09-25 06:08:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 3, 12 ],
      "id_str" : "45756727",
      "id" : 45756727
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/LividEye\/status\/647206942209740800\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/dcmWRUKyhv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPtXRumVAAAJiOM.jpg",
      "id_str" : "647206933535981568",
      "id" : 647206933535981568,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPtXRumVAAAJiOM.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dcmWRUKyhv"
    } ],
    "hashtags" : [ {
      "text" : "sghaze",
      "indices" : [ 75, 82 ]
    }, {
      "text" : "youaredoingitwrong",
      "indices" : [ 88, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647291297728987136",
  "text" : "RT @LividEye: Did someone not read the manual on how to wear your mask for #sghaze?! :\/ #youaredoingitwrong http:\/\/t.co\/dcmWRUKyhv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LividEye\/status\/647206942209740800\/photo\/1",
        "indices" : [ 94, 116 ],
        "url" : "http:\/\/t.co\/dcmWRUKyhv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPtXRumVAAAJiOM.jpg",
        "id_str" : "647206933535981568",
        "id" : 647206933535981568,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPtXRumVAAAJiOM.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dcmWRUKyhv"
      } ],
      "hashtags" : [ {
        "text" : "sghaze",
        "indices" : [ 61, 68 ]
      }, {
        "text" : "youaredoingitwrong",
        "indices" : [ 74, 93 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647206942209740800",
    "text" : "Did someone not read the manual on how to wear your mask for #sghaze?! :\/ #youaredoingitwrong http:\/\/t.co\/dcmWRUKyhv",
    "id" : 647206942209740800,
    "created_at" : "2015-09-25 00:32:14 +0000",
    "user" : {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "protected" : false,
      "id_str" : "45756727",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973927451775545344\/8ZHNGFY8_normal.jpg",
      "id" : 45756727,
      "verified" : false
    }
  },
  "id" : 647291297728987136,
  "created_at" : "2015-09-25 06:07:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/647160679267438593\/photo\/1",
      "indices" : [ 62, 84 ],
      "url" : "http:\/\/t.co\/pz4xgD8hpq",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPstNXqWwAAl3a9.jpg",
      "id_str" : "647160679171014656",
      "id" : 647160679171014656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPstNXqWwAAl3a9.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/pz4xgD8hpq"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 61 ],
      "url" : "http:\/\/t.co\/4xc1WPEwBp",
      "expanded_url" : "http:\/\/ift.tt\/1MsTpFX",
      "display_url" : "ift.tt\/1MsTpFX"
    } ]
  },
  "geo" : { },
  "id_str" : "647160679267438593",
  "text" : "I picking up the word Ass in this ads. http:\/\/t.co\/4xc1WPEwBp http:\/\/t.co\/pz4xgD8hpq",
  "id" : 647160679267438593,
  "created_at" : "2015-09-24 21:28:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/4rhPr1AyOf",
      "expanded_url" : "https:\/\/twitter.com\/GrangeWeb\/status\/647130590500777984",
      "display_url" : "twitter.com\/GrangeWeb\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647152310683086848",
  "text" : "Or a sex worker. https:\/\/t.co\/4rhPr1AyOf",
  "id" : 647152310683086848,
  "created_at" : "2015-09-24 20:55:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/647150757813350400\/photo\/1",
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/zjrpydpxxF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPskL3AWwAEooeZ.jpg",
      "id_str" : "647150757620400129",
      "id" : 647150757620400129,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPskL3AWwAEooeZ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/zjrpydpxxF"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 44 ],
      "url" : "http:\/\/t.co\/nEU3gdQNZL",
      "expanded_url" : "http:\/\/ift.tt\/1NX6muI",
      "display_url" : "ift.tt\/1NX6muI"
    } ]
  },
  "geo" : { },
  "id_str" : "647150757813350400",
  "text" : "Leg among the bushes. http:\/\/t.co\/nEU3gdQNZL http:\/\/t.co\/zjrpydpxxF",
  "id" : 647150757813350400,
  "created_at" : "2015-09-24 20:48:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/647081976286085121\/photo\/1",
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/nIBQgZUxTI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPrloP5WcAAZr5G.jpg",
      "id_str" : "647081976105693184",
      "id" : 647081976105693184,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPrloP5WcAAZr5G.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nIBQgZUxTI"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 60 ],
      "url" : "http:\/\/t.co\/71nh52PCKf",
      "expanded_url" : "http:\/\/ift.tt\/1iOfdSQ",
      "display_url" : "ift.tt\/1iOfdSQ"
    } ]
  },
  "geo" : { },
  "id_str" : "647081976286085121",
  "text" : "All you need is a pen and creativity. http:\/\/t.co\/71nh52PCKf http:\/\/t.co\/nIBQgZUxTI",
  "id" : 647081976286085121,
  "created_at" : "2015-09-24 16:15:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kirsten Han \u97E9\u4FD0\u9896",
      "screen_name" : "kixes",
      "indices" : [ 3, 9 ],
      "id_str" : "27182288",
      "id" : 27182288
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647061875209977856",
  "text" : "RT @kixes: Please keep an eye out for migrant workers who may not be aware of what protections they are entitled to from their employers. #\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "sghaze",
        "indices" : [ 127, 134 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "647016059321487360",
    "geo" : { },
    "id_str" : "647016204792532992",
    "in_reply_to_user_id" : 27182288,
    "text" : "Please keep an eye out for migrant workers who may not be aware of what protections they are entitled to from their employers. #sghaze",
    "id" : 647016204792532992,
    "in_reply_to_status_id" : 647016059321487360,
    "created_at" : "2015-09-24 11:54:18 +0000",
    "in_reply_to_screen_name" : "kixes",
    "in_reply_to_user_id_str" : "27182288",
    "user" : {
      "name" : "Kirsten Han \u97E9\u4FD0\u9896",
      "screen_name" : "kixes",
      "protected" : false,
      "id_str" : "27182288",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013433484604391424\/jv74veqG_normal.jpg",
      "id" : 27182288,
      "verified" : true
    }
  },
  "id" : 647061875209977856,
  "created_at" : "2015-09-24 14:55:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647055869998432256",
  "text" : "Trying to catch a glimpse of @bharatidalela on Channel 4 game show while having lunch.",
  "id" : 647055869998432256,
  "created_at" : "2015-09-24 14:31:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lydia Gall",
      "screen_name" : "LydsG",
      "indices" : [ 3, 9 ],
      "id_str" : "26786894",
      "id" : 26786894
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Syria",
      "indices" : [ 19, 25 ]
    }, {
      "text" : "Sweden",
      "indices" : [ 66, 73 ]
    }, {
      "text" : "refugees",
      "indices" : [ 107, 116 ]
    }, {
      "text" : "Croatia",
      "indices" : [ 117, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647055581312892928",
  "text" : "RT @LydsG: 80 year #Syria lady who could barely walk picked up by #Sweden volunteer. UNHCR rep did nothing #refugees #Croatia http:\/\/t.co\/e\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LydsG\/status\/647034145105817604\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/eAWWc1voQW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPq5_XLVEAAGktB.jpg",
        "id_str" : "647033994685517824",
        "id" : 647033994685517824,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPq5_XLVEAAGktB.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eAWWc1voQW"
      } ],
      "hashtags" : [ {
        "text" : "Syria",
        "indices" : [ 8, 14 ]
      }, {
        "text" : "Sweden",
        "indices" : [ 55, 62 ]
      }, {
        "text" : "refugees",
        "indices" : [ 96, 105 ]
      }, {
        "text" : "Croatia",
        "indices" : [ 106, 114 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "647034145105817604",
    "text" : "80 year #Syria lady who could barely walk picked up by #Sweden volunteer. UNHCR rep did nothing #refugees #Croatia http:\/\/t.co\/eAWWc1voQW",
    "id" : 647034145105817604,
    "created_at" : "2015-09-24 13:05:36 +0000",
    "user" : {
      "name" : "Lydia Gall",
      "screen_name" : "LydsG",
      "protected" : false,
      "id_str" : "26786894",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/461001996544118784\/dvCyQV8l_normal.jpeg",
      "id" : 26786894,
      "verified" : true
    }
  },
  "id" : 647055581312892928,
  "created_at" : "2015-09-24 14:30:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/fFppmpkTi0",
      "expanded_url" : "https:\/\/www.facebook.com\/photo.php?fbid=1032133023473411&set=a.237253116294743.60557.100000301687726&type=3",
      "display_url" : "facebook.com\/photo.php?fbid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647042897745940480",
  "text" : "Internet addiction is just like opium addiction 100 years ago and 100 years after https:\/\/t.co\/fFppmpkTi0",
  "id" : 647042897745940480,
  "created_at" : "2015-09-24 13:40:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Miller",
      "screen_name" : "thatdavidmiller",
      "indices" : [ 3, 19 ],
      "id_str" : "14376262",
      "id" : 14376262
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647041987342942208",
  "text" : "RT @thatdavidmiller: \"Hi, I'd like to add you to my professional network on linkedin\"\n\nA Caption That Works for Every New Yorker Cartoon\n\nh\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/3057qvKprc",
        "expanded_url" : "http:\/\/www.theatlantic.com\/notes\/2015\/09\/a-new-universal-new-yorker-cartoon-caption-id-like-to-add-you-to-my-professional-network-linkedin\/406783\/",
        "display_url" : "theatlantic.com\/notes\/2015\/09\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "646961997007953921",
    "text" : "\"Hi, I'd like to add you to my professional network on linkedin\"\n\nA Caption That Works for Every New Yorker Cartoon\n\nhttp:\/\/t.co\/3057qvKprc",
    "id" : 646961997007953921,
    "created_at" : "2015-09-24 08:18:54 +0000",
    "user" : {
      "name" : "David Miller",
      "screen_name" : "thatdavidmiller",
      "protected" : false,
      "id_str" : "14376262",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/461533981805002752\/r73owPuS_normal.jpeg",
      "id" : 14376262,
      "verified" : false
    }
  },
  "id" : 647041987342942208,
  "created_at" : "2015-09-24 13:36:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9",
      "screen_name" : "rte",
      "indices" : [ 3, 7 ],
      "id_str" : "1245699895",
      "id" : 1245699895
    }, {
      "name" : "RTE One",
      "screen_name" : "RTEOne",
      "indices" : [ 125, 132 ],
      "id_str" : "406900550",
      "id" : 406900550
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Crumlin",
      "indices" : [ 16, 24 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647041441789804544",
  "text" : "RT @rte: Watch: #Crumlin continues tonight focusing on the Centre of Haematology and Oncology at Crumlin Children's Hospital @RTEOne | 10.1\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "RTE One",
        "screen_name" : "RTEOne",
        "indices" : [ 116, 123 ],
        "id_str" : "406900550",
        "id" : 406900550
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Crumlin",
        "indices" : [ 7, 15 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644609600197632001",
    "text" : "Watch: #Crumlin continues tonight focusing on the Centre of Haematology and Oncology at Crumlin Children's Hospital @RTEOne | 10.15pm",
    "id" : 644609600197632001,
    "created_at" : "2015-09-17 20:31:19 +0000",
    "user" : {
      "name" : "RT\u00C9",
      "screen_name" : "rte",
      "protected" : false,
      "id_str" : "1245699895",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1022054151755390976\/uwPyNGwq_normal.jpg",
      "id" : 1245699895,
      "verified" : true
    }
  },
  "id" : 647041441789804544,
  "created_at" : "2015-09-24 13:34:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Congregation",
      "screen_name" : "congregation13",
      "indices" : [ 3, 18 ],
      "id_str" : "1969116109",
      "id" : 1969116109
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cong15",
      "indices" : [ 68, 75 ]
    } ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/AGktKmhZWC",
      "expanded_url" : "http:\/\/www.congregation.ie\/2015-attendees\/index.html",
      "display_url" : "congregation.ie\/2015-attendees\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647039964543328257",
  "text" : "RT @congregation13: Check out the 60 speakers and topics so far for #cong15.  Submissions accepted till Sept. http:\/\/t.co\/AGktKmhZWC http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/congregation13\/status\/636152637734391808\/photo\/1",
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/kVFA3zh8wR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CNQRc8hWEAI1G0e.jpg",
        "id_str" : "636152636346077186",
        "id" : 636152636346077186,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CNQRc8hWEAI1G0e.jpg",
        "sizes" : [ {
          "h" : 1133,
          "resize" : "fit",
          "w" : 938
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 563
        }, {
          "h" : 1133,
          "resize" : "fit",
          "w" : 938
        }, {
          "h" : 1133,
          "resize" : "fit",
          "w" : 938
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kVFA3zh8wR"
      } ],
      "hashtags" : [ {
        "text" : "cong15",
        "indices" : [ 48, 55 ]
      } ],
      "urls" : [ {
        "indices" : [ 90, 112 ],
        "url" : "http:\/\/t.co\/AGktKmhZWC",
        "expanded_url" : "http:\/\/www.congregation.ie\/2015-attendees\/index.html",
        "display_url" : "congregation.ie\/2015-attendees\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "636152637734391808",
    "text" : "Check out the 60 speakers and topics so far for #cong15.  Submissions accepted till Sept. http:\/\/t.co\/AGktKmhZWC http:\/\/t.co\/kVFA3zh8wR",
    "id" : 636152637734391808,
    "created_at" : "2015-08-25 12:26:22 +0000",
    "user" : {
      "name" : "Congregation",
      "screen_name" : "congregation13",
      "protected" : false,
      "id_str" : "1969116109",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1015221210865651715\/-Z2MmN-n_normal.jpg",
      "id" : 1969116109,
      "verified" : false
    }
  },
  "id" : 647039964543328257,
  "created_at" : "2015-09-24 13:28:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "647039528990076928",
  "text" : "Lazy to cook today.",
  "id" : 647039528990076928,
  "created_at" : "2015-09-24 13:26:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/NzOE6E1wkk",
      "expanded_url" : "http:\/\/clearpreso.com\/2014\/09\/22\/im-going-to-wasup-kerry-and-you-should-join-me\/",
      "display_url" : "clearpreso.com\/2014\/09\/22\/im-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647039432755965952",
  "text" : "RT @Clearpreso: Pro-tip just spend your Websummit tix \u20AC going to loads of events like this one last year. \u20AC20 http:\/\/t.co\/NzOE6E1wkk http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Clearpreso\/status\/647037788907536384\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/FUaNSj5BMq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPq9cJBWEAAsi_j.png",
        "id_str" : "647037787636633600",
        "id" : 647037787636633600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPq9cJBWEAAsi_j.png",
        "sizes" : [ {
          "h" : 289,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 686,
          "resize" : "fit",
          "w" : 1616
        }, {
          "h" : 509,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 686,
          "resize" : "fit",
          "w" : 1616
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FUaNSj5BMq"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 116 ],
        "url" : "http:\/\/t.co\/NzOE6E1wkk",
        "expanded_url" : "http:\/\/clearpreso.com\/2014\/09\/22\/im-going-to-wasup-kerry-and-you-should-join-me\/",
        "display_url" : "clearpreso.com\/2014\/09\/22\/im-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "647037788907536384",
    "text" : "Pro-tip just spend your Websummit tix \u20AC going to loads of events like this one last year. \u20AC20 http:\/\/t.co\/NzOE6E1wkk http:\/\/t.co\/FUaNSj5BMq",
    "id" : 647037788907536384,
    "created_at" : "2015-09-24 13:20:04 +0000",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 647039432755965952,
  "created_at" : "2015-09-24 13:26:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "indices" : [ 3, 16 ],
      "id_str" : "768968",
      "id" : 768968
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 123 ],
      "url" : "http:\/\/t.co\/jFmcQ5gxP9",
      "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/singapore\/schools-to-close-on\/2147982.html#.VgP3njmgRjA.twitter",
      "display_url" : "channelnewsasia.com\/news\/singapore\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "647037512926543873",
  "text" : "RT @coolinsights: Schools to close on Friday due to worsening haze situation: MOE - Channel NewsAsia http:\/\/t.co\/jFmcQ5gxP9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/jFmcQ5gxP9",
        "expanded_url" : "http:\/\/www.channelnewsasia.com\/news\/singapore\/schools-to-close-on\/2147982.html#.VgP3njmgRjA.twitter",
        "display_url" : "channelnewsasia.com\/news\/singapore\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "647036846011092992",
    "text" : "Schools to close on Friday due to worsening haze situation: MOE - Channel NewsAsia http:\/\/t.co\/jFmcQ5gxP9",
    "id" : 647036846011092992,
    "created_at" : "2015-09-24 13:16:20 +0000",
    "user" : {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "protected" : false,
      "id_str" : "768968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665181228841046016\/dvZ4PiEU_normal.jpg",
      "id" : 768968,
      "verified" : false
    }
  },
  "id" : 647037512926543873,
  "created_at" : "2015-09-24 13:18:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/646958821093605376\/photo\/1",
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/yRmfkExdAy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPp1nrLWgAAhnYa.jpg",
      "id_str" : "646958820946771968",
      "id" : 646958820946771968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPp1nrLWgAAhnYa.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yRmfkExdAy"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/cnZSNIrBjm",
      "expanded_url" : "http:\/\/ift.tt\/1KD67Pt",
      "display_url" : "ift.tt\/1KD67Pt"
    } ]
  },
  "geo" : { },
  "id_str" : "646958821093605376",
  "text" : "Secondary school Open evening. So far I am impressed by Palmerstown C C open evening. It l\u2026 http:\/\/t.co\/cnZSNIrBjm http:\/\/t.co\/yRmfkExdAy",
  "id" : 646958821093605376,
  "created_at" : "2015-09-24 08:06:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "646800146735435776",
  "text" : "@bharatidalela @Azhreicb it is now on TV?",
  "id" : 646800146735435776,
  "created_at" : "2015-09-23 21:35:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 28 ],
      "url" : "https:\/\/t.co\/mfOHNqcOV0",
      "expanded_url" : "https:\/\/twitter.com\/orangemanta\/status\/646684309278081024",
      "display_url" : "twitter.com\/orangemanta\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "646721916787687425",
  "text" : "Yes! https:\/\/t.co\/mfOHNqcOV0",
  "id" : 646721916787687425,
  "created_at" : "2015-09-23 16:24:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MerrionStreet.ie",
      "screen_name" : "merrionstreet",
      "indices" : [ 3, 17 ],
      "id_str" : "148000327",
      "id" : 148000327
    }, {
      "name" : "Enda Kenny",
      "screen_name" : "EndaKennyTD",
      "indices" : [ 29, 41 ],
      "id_str" : "135514272",
      "id" : 135514272
    }, {
      "name" : "Narendra Modi",
      "screen_name" : "narendramodi",
      "indices" : [ 61, 74 ],
      "id_str" : "18839785",
      "id" : 18839785
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ModiInIreland",
      "indices" : [ 116, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "646721915730722817",
  "text" : "RT @merrionstreet: Taoiseach @EndaKennyTD presents Indian PM @narendramodi with a personalised Irish cricket jersey #ModiInIreland http:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Enda Kenny",
        "screen_name" : "EndaKennyTD",
        "indices" : [ 10, 22 ],
        "id_str" : "135514272",
        "id" : 135514272
      }, {
        "name" : "Narendra Modi",
        "screen_name" : "narendramodi",
        "indices" : [ 42, 55 ],
        "id_str" : "18839785",
        "id" : 18839785
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/merrionstreet\/status\/646660749725724672\/photo\/1",
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/Ism87D8r3c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPlmfntUkAABpQE.jpg",
        "id_str" : "646660714925428736",
        "id" : 646660714925428736,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPlmfntUkAABpQE.jpg",
        "sizes" : [ {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ism87D8r3c"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/merrionstreet\/status\/646660749725724672\/photo\/1",
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/Ism87D8r3c",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPlmfoYUcAAN9EZ.jpg",
        "id_str" : "646660715105775616",
        "id" : 646660715105775616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPlmfoYUcAAN9EZ.jpg",
        "sizes" : [ {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ism87D8r3c"
      } ],
      "hashtags" : [ {
        "text" : "ModiInIreland",
        "indices" : [ 97, 111 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "646660749725724672",
    "text" : "Taoiseach @EndaKennyTD presents Indian PM @narendramodi with a personalised Irish cricket jersey #ModiInIreland http:\/\/t.co\/Ism87D8r3c",
    "id" : 646660749725724672,
    "created_at" : "2015-09-23 12:21:51 +0000",
    "user" : {
      "name" : "MerrionStreet.ie",
      "screen_name" : "merrionstreet",
      "protected" : false,
      "id_str" : "148000327",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964227595314266112\/jhnJklBd_normal.jpg",
      "id" : 148000327,
      "verified" : true
    }
  },
  "id" : 646721915730722817,
  "created_at" : "2015-09-23 16:24:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/XuHn6Gw5Le",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=pWQUTKprf7s",
      "display_url" : "youtube.com\/watch?v=pWQUTK\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "646707759556218881",
  "text" : "As an overseas Singaporean, I can identify some part of it https:\/\/t.co\/XuHn6Gw5Le LOL",
  "id" : 646707759556218881,
  "created_at" : "2015-09-23 15:28:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/mJn3UczEf1",
      "expanded_url" : "https:\/\/www.swarmapp.com\/c\/6ppjYPgYcCX",
      "display_url" : "swarmapp.com\/c\/6ppjYPgYcCX"
    } ]
  },
  "geo" : { },
  "id_str" : "646683924702363648",
  "text" : "Saw a new copy of GoT DVD on the librarian desk. (@ Walkinstown Library in Dublin) https:\/\/t.co\/mJn3UczEf1",
  "id" : 646683924702363648,
  "created_at" : "2015-09-23 13:53:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 39 ],
      "url" : "https:\/\/t.co\/Retw03Z8vZ",
      "expanded_url" : "https:\/\/twitter.com\/paddycosgrave\/status\/643414670309748736",
      "display_url" : "twitter.com\/paddycosgrave\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "646452072267689984",
  "text" : "Dublin please.  https:\/\/t.co\/Retw03Z8vZ",
  "id" : 646452072267689984,
  "created_at" : "2015-09-22 22:32:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/f0nmp1r5Yt",
      "expanded_url" : "https:\/\/twitter.com\/business\/status\/646365927022415873",
      "display_url" : "twitter.com\/business\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "646400113917739008",
  "text" : "RT @klillington: There\u2019s an iPhone gender pay equality index? Who knew https:\/\/t.co\/f0nmp1r5Yt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/f0nmp1r5Yt",
        "expanded_url" : "https:\/\/twitter.com\/business\/status\/646365927022415873",
        "display_url" : "twitter.com\/business\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "646374895480348672",
    "text" : "There\u2019s an iPhone gender pay equality index? Who knew https:\/\/t.co\/f0nmp1r5Yt",
    "id" : 646374895480348672,
    "created_at" : "2015-09-22 17:25:58 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 646400113917739008,
  "created_at" : "2015-09-22 19:06:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mary McKenna",
      "screen_name" : "MMaryMcKenna",
      "indices" : [ 3, 16 ],
      "id_str" : "20276317",
      "id" : 20276317
    }, {
      "name" : "jon bradford",
      "screen_name" : "jd",
      "indices" : [ 81, 84 ],
      "id_str" : "781240",
      "id" : 781240
    }, {
      "name" : "IIBN",
      "screen_name" : "IIBN",
      "indices" : [ 118, 123 ],
      "id_str" : "260708683",
      "id" : 260708683
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "entrepreneurs",
      "indices" : [ 59, 73 ]
    }, {
      "text" : "scaleup",
      "indices" : [ 92, 100 ]
    }, {
      "text" : "iibn",
      "indices" : [ 124, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "646399541391028225",
  "text" : "RT @MMaryMcKenna: Bad idea to try &amp; turn everyone into #entrepreneurs - says @jd on our #scaleup panel tonight at @iibn #iibn http:\/\/t.co\/Z\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "jon bradford",
        "screen_name" : "jd",
        "indices" : [ 63, 66 ],
        "id_str" : "781240",
        "id" : 781240
      }, {
        "name" : "IIBN",
        "screen_name" : "IIBN",
        "indices" : [ 100, 105 ],
        "id_str" : "260708683",
        "id" : 260708683
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MMaryMcKenna\/status\/646391332307603456\/photo\/1",
        "indices" : [ 112, 134 ],
        "url" : "http:\/\/t.co\/ZeH8bH4EEi",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPhxfWwW8AE-Z8F.jpg",
        "id_str" : "646391330025959425",
        "id" : 646391330025959425,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPhxfWwW8AE-Z8F.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ZeH8bH4EEi"
      } ],
      "hashtags" : [ {
        "text" : "entrepreneurs",
        "indices" : [ 41, 55 ]
      }, {
        "text" : "scaleup",
        "indices" : [ 74, 82 ]
      }, {
        "text" : "iibn",
        "indices" : [ 106, 111 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "646391332307603456",
    "text" : "Bad idea to try &amp; turn everyone into #entrepreneurs - says @jd on our #scaleup panel tonight at @iibn #iibn http:\/\/t.co\/ZeH8bH4EEi",
    "id" : 646391332307603456,
    "created_at" : "2015-09-22 18:31:17 +0000",
    "user" : {
      "name" : "Mary McKenna",
      "screen_name" : "MMaryMcKenna",
      "protected" : false,
      "id_str" : "20276317",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040600018800001027\/tVSqaMTv_normal.jpg",
      "id" : 20276317,
      "verified" : false
    }
  },
  "id" : 646399541391028225,
  "created_at" : "2015-09-22 19:03:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TIME",
      "screen_name" : "TIME",
      "indices" : [ 3, 8 ],
      "id_str" : "14293310",
      "id" : 14293310
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 85 ],
      "url" : "http:\/\/t.co\/v1ECsMti0U",
      "expanded_url" : "http:\/\/ti.me\/1PoNCBL",
      "display_url" : "ti.me\/1PoNCBL"
    } ]
  },
  "geo" : { },
  "id_str" : "646392993885327360",
  "text" : "RT @TIME: Monkey should get rights to famous selfie, PETA says http:\/\/t.co\/v1ECsMti0U",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 75 ],
        "url" : "http:\/\/t.co\/v1ECsMti0U",
        "expanded_url" : "http:\/\/ti.me\/1PoNCBL",
        "display_url" : "ti.me\/1PoNCBL"
      } ]
    },
    "geo" : { },
    "id_str" : "646375928046723073",
    "text" : "Monkey should get rights to famous selfie, PETA says http:\/\/t.co\/v1ECsMti0U",
    "id" : 646375928046723073,
    "created_at" : "2015-09-22 17:30:04 +0000",
    "user" : {
      "name" : "TIME",
      "screen_name" : "TIME",
      "protected" : false,
      "id_str" : "14293310",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1700796190\/Picture_24_normal.png",
      "id" : 14293310,
      "verified" : true
    }
  },
  "id" : 646392993885327360,
  "created_at" : "2015-09-22 18:37:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HDIPSDA",
      "indices" : [ 99, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "646390201196093440",
  "text" : "From Linkedin Search: 631 data analyst jobs opening in UK versus 69 in Ireland and 61 in Singapore #HDIPSDA",
  "id" : 646390201196093440,
  "created_at" : "2015-09-22 18:26:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "834055856",
      "id" : 834055856
    }, {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 108, 118 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "expat",
      "indices" : [ 119, 125 ]
    } ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/whKfpMXogd",
      "expanded_url" : "http:\/\/tinyurl.com\/nlr6q96",
      "display_url" : "tinyurl.com\/nlr6q96"
    } ]
  },
  "geo" : { },
  "id_str" : "646273447979560960",
  "text" : "RT @expatsblog: Singaporean Expat Living in Northern Ireland - Interview with Joanne http:\/\/t.co\/whKfpMXogd @starduest #expat #NorthernIrel\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.expatsblog.com\" rel=\"nofollow\"\u003EExpatsBlog.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "check bio",
        "screen_name" : "starduest",
        "indices" : [ 92, 102 ],
        "id_str" : "824059172731830272",
        "id" : 824059172731830272
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "expat",
        "indices" : [ 103, 109 ]
      }, {
        "text" : "NorthernIreland",
        "indices" : [ 110, 126 ]
      }, {
        "text" : "archive",
        "indices" : [ 127, 135 ]
      } ],
      "urls" : [ {
        "indices" : [ 69, 91 ],
        "url" : "http:\/\/t.co\/whKfpMXogd",
        "expanded_url" : "http:\/\/tinyurl.com\/nlr6q96",
        "display_url" : "tinyurl.com\/nlr6q96"
      } ]
    },
    "geo" : { },
    "id_str" : "646260704622870528",
    "text" : "Singaporean Expat Living in Northern Ireland - Interview with Joanne http:\/\/t.co\/whKfpMXogd @starduest #expat #NorthernIreland #archive",
    "id" : 646260704622870528,
    "created_at" : "2015-09-22 09:52:13 +0000",
    "user" : {
      "name" : "Expats Blog",
      "screen_name" : "expatsblog",
      "protected" : false,
      "id_str" : "834055856",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2627803659\/logo_normal.png",
      "id" : 834055856,
      "verified" : false
    }
  },
  "id" : 646273447979560960,
  "created_at" : "2015-09-22 10:42:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 3, 12 ],
      "id_str" : "45756727",
      "id" : 45756727
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/BMSbQ8shD5",
      "expanded_url" : "http:\/\/bloom.bg\/1WflxRu",
      "display_url" : "bloom.bg\/1WflxRu"
    } ]
  },
  "geo" : { },
  "id_str" : "646272798709686272",
  "text" : "RT @LividEye: The kids are suing the government?! Lee Kuan Yew\u2019s Two Younger Children File Case on Interview Deal http:\/\/t.co\/BMSbQ8shD5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 100, 122 ],
        "url" : "http:\/\/t.co\/BMSbQ8shD5",
        "expanded_url" : "http:\/\/bloom.bg\/1WflxRu",
        "display_url" : "bloom.bg\/1WflxRu"
      } ]
    },
    "geo" : { },
    "id_str" : "646264478401589248",
    "text" : "The kids are suing the government?! Lee Kuan Yew\u2019s Two Younger Children File Case on Interview Deal http:\/\/t.co\/BMSbQ8shD5",
    "id" : 646264478401589248,
    "created_at" : "2015-09-22 10:07:13 +0000",
    "user" : {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "protected" : false,
      "id_str" : "45756727",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973927451775545344\/8ZHNGFY8_normal.jpg",
      "id" : 45756727,
      "verified" : false
    }
  },
  "id" : 646272798709686272,
  "created_at" : "2015-09-22 10:40:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/Oq2RB5hiF2",
      "expanded_url" : "https:\/\/twitter.com\/blackadlerqueen\/status\/646266498370015232",
      "display_url" : "twitter.com\/blackadlerquee\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "646272426515562496",
  "text" : "Life random moment. https:\/\/t.co\/Oq2RB5hiF2",
  "id" : 646272426515562496,
  "created_at" : "2015-09-22 10:38:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645983030054678528",
  "text" : "To all secondary school, it helps if your website puts down the Dublin Bus Number to reach your campus.",
  "id" : 645983030054678528,
  "created_at" : "2015-09-21 15:28:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vegetarian Butcher",
      "screen_name" : "Vegebutcher",
      "indices" : [ 3, 15 ],
      "id_str" : "204736152",
      "id" : 204736152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/5gsL6WhWiW",
      "expanded_url" : "http:\/\/news.therawfoodworld.com\/vegetarian-schools\/",
      "display_url" : "news.therawfoodworld.com\/vegetarian-sch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "645979245479600128",
  "text" : "RT @Vegebutcher: Interesting: French lawmaker is proposing all schools go vegetarian!\n\nhttp:\/\/t.co\/5gsL6WhWiW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 92 ],
        "url" : "http:\/\/t.co\/5gsL6WhWiW",
        "expanded_url" : "http:\/\/news.therawfoodworld.com\/vegetarian-schools\/",
        "display_url" : "news.therawfoodworld.com\/vegetarian-sch\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "645961634356293632",
    "text" : "Interesting: French lawmaker is proposing all schools go vegetarian!\n\nhttp:\/\/t.co\/5gsL6WhWiW",
    "id" : 645961634356293632,
    "created_at" : "2015-09-21 14:03:49 +0000",
    "user" : {
      "name" : "Vegetarian Butcher",
      "screen_name" : "Vegebutcher",
      "protected" : false,
      "id_str" : "204736152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815862410603008000\/7IA4eNla_normal.jpg",
      "id" : 204736152,
      "verified" : false
    }
  },
  "id" : 645979245479600128,
  "created_at" : "2015-09-21 15:13:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645886642201477121",
  "text" : "Seriously, all of you never have some outrageous time in your youth?",
  "id" : 645886642201477121,
  "created_at" : "2015-09-21 09:05:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645885637405618176",
  "text" : "Note to son : Don't do anything outrageous during your uni time you never know when you will become a public figure.  \uD83D\uDC16",
  "id" : 645885637405618176,
  "created_at" : "2015-09-21 09:01:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alser",
      "screen_name" : "Alser82",
      "indices" : [ 3, 11 ],
      "id_str" : "39577613",
      "id" : 39577613
    }, {
      "name" : "Met \u00C9ireann",
      "screen_name" : "MetEireann",
      "indices" : [ 117, 128 ],
      "id_str" : "74394857",
      "id" : 74394857
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RTE",
      "indices" : [ 82, 86 ]
    }, {
      "text" : "weather",
      "indices" : [ 87, 95 ]
    }, {
      "text" : "neverknewitwasthere",
      "indices" : [ 96, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645694816878116864",
  "text" : "RT @Alser82: RTE pointing out Ireland just in case we didn't know where it was... #RTE #weather #neverknewitwasthere @MetEireann http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Met \u00C9ireann",
        "screen_name" : "MetEireann",
        "indices" : [ 104, 115 ],
        "id_str" : "74394857",
        "id" : 74394857
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Alser82\/status\/631520664533512192\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/YUoH9tVig8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CMOcsQgWEAAk2Hk.jpg",
        "id_str" : "631520656920809472",
        "id" : 631520656920809472,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CMOcsQgWEAAk2Hk.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YUoH9tVig8"
      } ],
      "hashtags" : [ {
        "text" : "RTE",
        "indices" : [ 69, 73 ]
      }, {
        "text" : "weather",
        "indices" : [ 74, 82 ]
      }, {
        "text" : "neverknewitwasthere",
        "indices" : [ 83, 103 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "631520664533512192",
    "text" : "RTE pointing out Ireland just in case we didn't know where it was... #RTE #weather #neverknewitwasthere @MetEireann http:\/\/t.co\/YUoH9tVig8",
    "id" : 631520664533512192,
    "created_at" : "2015-08-12 17:40:34 +0000",
    "user" : {
      "name" : "Alser",
      "screen_name" : "Alser82",
      "protected" : false,
      "id_str" : "39577613",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032736913243942912\/XM-po2DC_normal.jpg",
      "id" : 39577613,
      "verified" : false
    }
  },
  "id" : 645694816878116864,
  "created_at" : "2015-09-20 20:23:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shintaro Kano",
      "screen_name" : "shintarokano",
      "indices" : [ 3, 16 ],
      "id_str" : "456002600",
      "id" : 456002600
    }, {
      "name" : "Rugby World Cup",
      "screen_name" : "rugbyworldcup",
      "indices" : [ 100, 114 ],
      "id_str" : "55459700",
      "id" : 55459700
    }, {
      "name" : "\u30E9\u30B0\u30D3\u30FC\u30EF\u30FC\u30EB\u30C9\u30AB\u30C3\u30D7",
      "screen_name" : "rugbyworldcupjp",
      "indices" : [ 115, 131 ],
      "id_str" : "2507597982",
      "id" : 2507597982
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "JPN",
      "indices" : [ 49, 53 ]
    }, {
      "text" : "RWC2015",
      "indices" : [ 81, 89 ]
    }, {
      "text" : "JapanWay",
      "indices" : [ 90, 99 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645650057597984769",
  "text" : "RT @shintarokano: Slightly bigger media crowd at #JPN training today. Wonder why #RWC2015 #JapanWay @rugbyworldcup @rugbyworldcupjp http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rugby World Cup",
        "screen_name" : "rugbyworldcup",
        "indices" : [ 82, 96 ],
        "id_str" : "55459700",
        "id" : 55459700
      }, {
        "name" : "\u30E9\u30B0\u30D3\u30FC\u30EF\u30FC\u30EB\u30C9\u30AB\u30C3\u30D7",
        "screen_name" : "rugbyworldcupjp",
        "indices" : [ 97, 113 ],
        "id_str" : "2507597982",
        "id" : 2507597982
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/shintarokano\/status\/645629046160760832\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/ESA1NQlM1t",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPW8LUSWEAAlQ_B.jpg",
        "id_str" : "645629024207769600",
        "id" : 645629024207769600,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPW8LUSWEAAlQ_B.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ESA1NQlM1t"
      } ],
      "hashtags" : [ {
        "text" : "JPN",
        "indices" : [ 31, 35 ]
      }, {
        "text" : "RWC2015",
        "indices" : [ 63, 71 ]
      }, {
        "text" : "JapanWay",
        "indices" : [ 72, 81 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "645629046160760832",
    "text" : "Slightly bigger media crowd at #JPN training today. Wonder why #RWC2015 #JapanWay @rugbyworldcup @rugbyworldcupjp http:\/\/t.co\/ESA1NQlM1t",
    "id" : 645629046160760832,
    "created_at" : "2015-09-20 16:02:14 +0000",
    "user" : {
      "name" : "Shintaro Kano",
      "screen_name" : "shintarokano",
      "protected" : false,
      "id_str" : "456002600",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964924261507846144\/8w86JXjT_normal.jpg",
      "id" : 456002600,
      "verified" : false
    }
  },
  "id" : 645650057597984769,
  "created_at" : "2015-09-20 17:25:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "qwerty",
      "screen_name" : "sensegraph",
      "indices" : [ 3, 14 ],
      "id_str" : "3165876629",
      "id" : 3165876629
    }, {
      "name" : "Product Hunt",
      "screen_name" : "ProductHunt",
      "indices" : [ 68, 80 ],
      "id_str" : "2208027565",
      "id" : 2208027565
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645648887634612224",
  "text" : "RT @sensegraph: Do you know, that there are 51 verified users among @ProductHunt upvoters?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Product Hunt",
        "screen_name" : "ProductHunt",
        "indices" : [ 52, 64 ],
        "id_str" : "2208027565",
        "id" : 2208027565
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643095552981344260",
    "text" : "Do you know, that there are 51 verified users among @ProductHunt upvoters?",
    "id" : 643095552981344260,
    "created_at" : "2015-09-13 16:15:02 +0000",
    "user" : {
      "name" : "qwerty",
      "screen_name" : "sensegraph",
      "protected" : false,
      "id_str" : "3165876629",
      "profile_image_url_https" : "https:\/\/abs.twimg.com\/sticky\/default_profile_images\/default_profile_normal.png",
      "id" : 3165876629,
      "verified" : false
    }
  },
  "id" : 645648887634612224,
  "created_at" : "2015-09-20 17:21:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645620648950284288",
  "text" : "For some, statistics can be sadistic LOL But you can't do data without an understanding of statistics.",
  "id" : 645620648950284288,
  "created_at" : "2015-09-20 15:28:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http:\/\/t.co\/aZDCYYXwN9",
      "expanded_url" : "http:\/\/dit.ie\/news\/archive2015\/latest\/title,115488,en.html",
      "display_url" : "dit.ie\/news\/archive20\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "645535666152042496",
  "text" : "The DIT graduate that make Apple keynote presentation http:\/\/t.co\/aZDCYYXwN9",
  "id" : 645535666152042496,
  "created_at" : "2015-09-20 09:51:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel CF Ng",
      "screen_name" : "danielcfng",
      "indices" : [ 3, 14 ],
      "id_str" : "20998868",
      "id" : 20998868
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ASEAN",
      "indices" : [ 92, 98 ]
    } ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/sX0dTcHeKl",
      "expanded_url" : "https:\/\/lnkd.in\/bqFnBVy",
      "display_url" : "lnkd.in\/bqFnBVy"
    } ]
  },
  "geo" : { },
  "id_str" : "645534167380439040",
  "text" : "RT @danielcfng: Governments in Southeast Asia are upping their push to Smart Nation status  #ASEAN\n\nhttps:\/\/t.co\/sX0dTcHeKl",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ASEAN",
        "indices" : [ 76, 82 ]
      } ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/sX0dTcHeKl",
        "expanded_url" : "https:\/\/lnkd.in\/bqFnBVy",
        "display_url" : "lnkd.in\/bqFnBVy"
      } ]
    },
    "geo" : { },
    "id_str" : "645468368758181892",
    "text" : "Governments in Southeast Asia are upping their push to Smart Nation status  #ASEAN\n\nhttps:\/\/t.co\/sX0dTcHeKl",
    "id" : 645468368758181892,
    "created_at" : "2015-09-20 05:23:45 +0000",
    "user" : {
      "name" : "Daniel CF Ng",
      "screen_name" : "danielcfng",
      "protected" : false,
      "id_str" : "20998868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/711482920209227777\/4aXGoT3u_normal.jpg",
      "id" : 20998868,
      "verified" : false
    }
  },
  "id" : 645534167380439040,
  "created_at" : "2015-09-20 09:45:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jesse van Doren",
      "screen_name" : "jessevandoren",
      "indices" : [ 3, 17 ],
      "id_str" : "359426539",
      "id" : 359426539
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jessevandoren\/status\/639688418650423296\/photo\/1",
      "indices" : [ 102, 124 ],
      "url" : "http:\/\/t.co\/SyMe6TSv0B",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COChN59UkAAKoyQ.jpg",
      "id_str" : "639688407355002880",
      "id" : 639688407355002880,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COChN59UkAAKoyQ.jpg",
      "sizes" : [ {
        "h" : 608,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 608,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 608,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 608,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/SyMe6TSv0B"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645531437819645952",
  "text" : "RT @jessevandoren: Thinking different. At Dutch rail stations: \"was your train delayed?\" A) Yes B) No http:\/\/t.co\/SyMe6TSv0B",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jessevandoren\/status\/639688418650423296\/photo\/1",
        "indices" : [ 83, 105 ],
        "url" : "http:\/\/t.co\/SyMe6TSv0B",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COChN59UkAAKoyQ.jpg",
        "id_str" : "639688407355002880",
        "id" : 639688407355002880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COChN59UkAAKoyQ.jpg",
        "sizes" : [ {
          "h" : 608,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/SyMe6TSv0B"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639688418650423296",
    "text" : "Thinking different. At Dutch rail stations: \"was your train delayed?\" A) Yes B) No http:\/\/t.co\/SyMe6TSv0B",
    "id" : 639688418650423296,
    "created_at" : "2015-09-04 06:36:18 +0000",
    "user" : {
      "name" : "Jesse van Doren",
      "screen_name" : "jessevandoren",
      "protected" : false,
      "id_str" : "359426539",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/602072459949379585\/oukzOibL_normal.jpg",
      "id" : 359426539,
      "verified" : true
    }
  },
  "id" : 645531437819645952,
  "created_at" : "2015-09-20 09:34:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645525025592840196",
  "text" : "The national chain pharmacy is so new to the neighbourhood that I can't find Sunday store opening hours on their website.",
  "id" : 645525025592840196,
  "created_at" : "2015-09-20 09:08:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/645505453468819457\/photo\/1",
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/tvaroEHnrI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPVLybMWIAADXy8.jpg",
      "id_str" : "645505451262615552",
      "id" : 645505451262615552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPVLybMWIAADXy8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tvaroEHnrI"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645505453468819457",
  "text" : "Knew nothing about rugby. Realise the significant of Japan win. It like Japan defeat German or Brazil in football. http:\/\/t.co\/tvaroEHnrI",
  "id" : 645505453468819457,
  "created_at" : "2015-09-20 07:51:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Open Data Manchester",
      "screen_name" : "opendatamcr",
      "indices" : [ 3, 15 ],
      "id_str" : "149120416",
      "id" : 149120416
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "open",
      "indices" : [ 133, 138 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/PSZH3jaa7f",
      "expanded_url" : "https:\/\/www.eventbrite.co.uk\/e\/data-visualisation-series-getting-to-grips-with-data-tickets-18520999809",
      "display_url" : "eventbrite.co.uk\/e\/data-visuali\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "645360279027642368",
  "text" : "RT @opendatamcr: Want to make data useful? Open Data Manchester data visualisation workshop next Tues. https:\/\/t.co\/PSZH3jaa7f It is #open \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "open",
        "indices" : [ 116, 121 ]
      }, {
        "text" : "free",
        "indices" : [ 128, 133 ]
      }, {
        "text" : "opendata",
        "indices" : [ 134, 143 ]
      } ],
      "urls" : [ {
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/PSZH3jaa7f",
        "expanded_url" : "https:\/\/www.eventbrite.co.uk\/e\/data-visualisation-series-getting-to-grips-with-data-tickets-18520999809",
        "display_url" : "eventbrite.co.uk\/e\/data-visuali\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "644416809857380352",
    "text" : "Want to make data useful? Open Data Manchester data visualisation workshop next Tues. https:\/\/t.co\/PSZH3jaa7f It is #open &amp; #free #opendata",
    "id" : 644416809857380352,
    "created_at" : "2015-09-17 07:45:14 +0000",
    "user" : {
      "name" : "Open Data Manchester",
      "screen_name" : "opendatamcr",
      "protected" : false,
      "id_str" : "149120416",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034345641906982912\/Dnp7Lzr1_normal.jpg",
      "id" : 149120416,
      "verified" : false
    }
  },
  "id" : 645360279027642368,
  "created_at" : "2015-09-19 22:14:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "15908631",
      "id" : 15908631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/BZmHs3nn6j",
      "expanded_url" : "https:\/\/www.facebook.com\/thisisiradio\/videos\/vb.179573255437092\/975494939178249\/?type=2&theater",
      "display_url" : "facebook.com\/thisisiradio\/v\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "645323764239101953",
  "text" : "RT @gavinsblog: then watch how Irish fans (!) reacted to Japan winning https:\/\/t.co\/BZmHs3nn6j",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/BZmHs3nn6j",
        "expanded_url" : "https:\/\/www.facebook.com\/thisisiradio\/videos\/vb.179573255437092\/975494939178249\/?type=2&theater",
        "display_url" : "facebook.com\/thisisiradio\/v\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "645320930034589697",
    "geo" : { },
    "id_str" : "645321849933570048",
    "in_reply_to_user_id" : 15908631,
    "text" : "then watch how Irish fans (!) reacted to Japan winning https:\/\/t.co\/BZmHs3nn6j",
    "id" : 645321849933570048,
    "in_reply_to_status_id" : 645320930034589697,
    "created_at" : "2015-09-19 19:41:33 +0000",
    "in_reply_to_screen_name" : "gavinsblog",
    "in_reply_to_user_id_str" : "15908631",
    "user" : {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "protected" : false,
      "id_str" : "15908631",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894331872716754945\/0KfaOf4I_normal.jpg",
      "id" : 15908631,
      "verified" : true
    }
  },
  "id" : 645323764239101953,
  "created_at" : "2015-09-19 19:49:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Olly Barratt",
      "screen_name" : "ollybarratt",
      "indices" : [ 3, 15 ],
      "id_str" : "29451711",
      "id" : 29451711
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645323186331062272",
  "text" : "RT @ollybarratt: Train pulls into Brighton, SA fans insist Japan supporters get off first, give them a guard of honour and cheer them off. \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "645318805900644355",
    "text" : "Train pulls into Brighton, SA fans insist Japan supporters get off first, give them a guard of honour and cheer them off. Amazing day.",
    "id" : 645318805900644355,
    "created_at" : "2015-09-19 19:29:27 +0000",
    "user" : {
      "name" : "Olly Barratt",
      "screen_name" : "ollybarratt",
      "protected" : false,
      "id_str" : "29451711",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002470637062033409\/qHSWLtfb_normal.jpg",
      "id" : 29451711,
      "verified" : true
    }
  },
  "id" : 645323186331062272,
  "created_at" : "2015-09-19 19:46:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Boyd's Backyard\u2122",
      "screen_name" : "TheBoydP",
      "indices" : [ 3, 12 ],
      "id_str" : "738576612",
      "id" : 738576612
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645308525112074240",
  "text" : "RT @TheBoydP: Introverts have fun too, we just don't care if you know...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "520727032570060801",
    "text" : "Introverts have fun too, we just don't care if you know...",
    "id" : 520727032570060801,
    "created_at" : "2014-10-11 00:06:13 +0000",
    "user" : {
      "name" : "Boyd's Backyard\u2122",
      "screen_name" : "TheBoydP",
      "protected" : false,
      "id_str" : "738576612",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/810677303156080641\/Y-bEHHOm_normal.jpg",
      "id" : 738576612,
      "verified" : false
    }
  },
  "id" : 645308525112074240,
  "created_at" : "2015-09-19 18:48:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin Tan",
      "screen_name" : "ultra_slacker",
      "indices" : [ 3, 17 ],
      "id_str" : "23318166",
      "id" : 23318166
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Japan",
      "indices" : [ 30, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645299700531339264",
  "text" : "RT @ultra_slacker: Take a bow #Japan on prob the most impressive sporting performance this decade. Feel privileged to hv watched this histo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Japan",
        "indices" : [ 11, 17 ]
      }, {
        "text" : "rwc2015",
        "indices" : [ 132, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "645295067637268480",
    "text" : "Take a bow #Japan on prob the most impressive sporting performance this decade. Feel privileged to hv watched this historic moment. #rwc2015",
    "id" : 645295067637268480,
    "created_at" : "2015-09-19 17:55:07 +0000",
    "user" : {
      "name" : "Gavin Tan",
      "screen_name" : "ultra_slacker",
      "protected" : false,
      "id_str" : "23318166",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/458793066476761088\/6qPn3lol_normal.jpeg",
      "id" : 23318166,
      "verified" : false
    }
  },
  "id" : 645299700531339264,
  "created_at" : "2015-09-19 18:13:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/645298343002902528\/photo\/1",
      "indices" : [ 65, 87 ],
      "url" : "http:\/\/t.co\/RDHhVYiLcW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPSPbIdWEAA_yS8.jpg",
      "id_str" : "645298342910627840",
      "id" : 645298342910627840,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPSPbIdWEAA_yS8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RDHhVYiLcW"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http:\/\/t.co\/XydW042Gr9",
      "expanded_url" : "http:\/\/ift.tt\/1NDEzhn",
      "display_url" : "ift.tt\/1NDEzhn"
    } ]
  },
  "geo" : { },
  "id_str" : "645298343002902528",
  "text" : "Double yolks lotus seed paste moon cakes. http:\/\/t.co\/XydW042Gr9 http:\/\/t.co\/RDHhVYiLcW",
  "id" : 645298343002902528,
  "created_at" : "2015-09-19 18:08:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/QnZA4LBdEA",
      "expanded_url" : "https:\/\/twitter.com\/iheartbeijing\/status\/645261618222202880",
      "display_url" : "twitter.com\/iheartbeijing\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "645287444041105409",
  "text" : "Would love to hear the joke. https:\/\/t.co\/QnZA4LBdEA",
  "id" : 645287444041105409,
  "created_at" : "2015-09-19 17:24:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deborah Malone",
      "screen_name" : "DeborahCMalone",
      "indices" : [ 3, 18 ],
      "id_str" : "15083027",
      "id" : 15083027
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645286986006294528",
  "text" : "RT @DeborahCMalone: That feeling when you make epic food but you're so full and tired you don't want to wash the dishes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "645263829920817152",
    "text" : "That feeling when you make epic food but you're so full and tired you don't want to wash the dishes",
    "id" : 645263829920817152,
    "created_at" : "2015-09-19 15:51:00 +0000",
    "user" : {
      "name" : "Deborah Malone",
      "screen_name" : "DeborahCMalone",
      "protected" : false,
      "id_str" : "15083027",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978024771886731266\/purcRX01_normal.jpg",
      "id" : 15083027,
      "verified" : false
    }
  },
  "id" : 645286986006294528,
  "created_at" : "2015-09-19 17:23:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Agagooga",
      "screen_name" : "gssq",
      "indices" : [ 3, 8 ],
      "id_str" : "674573",
      "id" : 674573
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/gssq\/status\/645120300460429312\/photo\/1",
      "indices" : [ 55, 77 ],
      "url" : "http:\/\/t.co\/abg2TNPvhB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPPtfrEWwAAvatC.jpg",
      "id_str" : "645120300036833280",
      "id" : 645120300036833280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPPtfrEWwAAvatC.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 844,
        "resize" : "fit",
        "w" : 1500
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 844,
        "resize" : "fit",
        "w" : 1500
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/abg2TNPvhB"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "645180928348827648",
  "text" : "RT @gssq: Being the cheapest in Singapore isn't enough http:\/\/t.co\/abg2TNPvhB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gssq\/status\/645120300460429312\/photo\/1",
        "indices" : [ 45, 67 ],
        "url" : "http:\/\/t.co\/abg2TNPvhB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPPtfrEWwAAvatC.jpg",
        "id_str" : "645120300036833280",
        "id" : 645120300036833280,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPPtfrEWwAAvatC.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 844,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 844,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/abg2TNPvhB"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "645120300460429312",
    "text" : "Being the cheapest in Singapore isn't enough http:\/\/t.co\/abg2TNPvhB",
    "id" : 645120300460429312,
    "created_at" : "2015-09-19 06:20:40 +0000",
    "user" : {
      "name" : "Agagooga",
      "screen_name" : "gssq",
      "protected" : false,
      "id_str" : "674573",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011469424740597760\/oiUmuNh6_normal.jpg",
      "id" : 674573,
      "verified" : false
    }
  },
  "id" : 645180928348827648,
  "created_at" : "2015-09-19 10:21:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kevin sexton",
      "screen_name" : "sextonireland",
      "indices" : [ 3, 17 ],
      "id_str" : "319590522",
      "id" : 319590522
    }, {
      "name" : "Startup Ireland",
      "screen_name" : "StartupIreland",
      "indices" : [ 65, 80 ],
      "id_str" : "1565535649",
      "id" : 1565535649
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startupIRL",
      "indices" : [ 53, 64 ]
    } ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/pKPYkB5Moz",
      "expanded_url" : "https:\/\/twitter.com\/IDAIRELAND\/status\/645149665206345728",
      "display_url" : "twitter.com\/IDAIRELAND\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "645179316448423936",
  "text" : "RT @sextonireland: Now let's edge it up a notch with #startupIRL @startupireland  https:\/\/t.co\/pKPYkB5Moz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Startup Ireland",
        "screen_name" : "StartupIreland",
        "indices" : [ 46, 61 ],
        "id_str" : "1565535649",
        "id" : 1565535649
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "startupIRL",
        "indices" : [ 34, 45 ]
      } ],
      "urls" : [ {
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/pKPYkB5Moz",
        "expanded_url" : "https:\/\/twitter.com\/IDAIRELAND\/status\/645149665206345728",
        "display_url" : "twitter.com\/IDAIRELAND\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "645159941670629376",
    "text" : "Now let's edge it up a notch with #startupIRL @startupireland  https:\/\/t.co\/pKPYkB5Moz",
    "id" : 645159941670629376,
    "created_at" : "2015-09-19 08:58:11 +0000",
    "user" : {
      "name" : "kevin sexton",
      "screen_name" : "sextonireland",
      "protected" : false,
      "id_str" : "319590522",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1415963349\/Soldierspic_ps02.1_normal.jpg",
      "id" : 319590522,
      "verified" : false
    }
  },
  "id" : 645179316448423936,
  "created_at" : "2015-09-19 10:15:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dublinese \u6708\u5149",
      "screen_name" : "rachel_violin",
      "indices" : [ 0, 14 ],
      "id_str" : "116755889",
      "id" : 116755889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "644999692997279744",
  "geo" : { },
  "id_str" : "645000713446289408",
  "in_reply_to_user_id" : 116755889,
  "text" : "@rachel_violin good to hear that. :)",
  "id" : 645000713446289408,
  "in_reply_to_status_id" : 644999692997279744,
  "created_at" : "2015-09-18 22:25:28 +0000",
  "in_reply_to_screen_name" : "rachel_violin",
  "in_reply_to_user_id_str" : "116755889",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "indices" : [ 3, 11 ],
      "id_str" : "20697268",
      "id" : 20697268
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TRTPT",
      "indices" : [ 13, 19 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644913228246568961",
  "text" : "RT @KJoanne: #TRTPT, some of Ireland's Navy boats serve in the Med, rescuing refugees. The L\u00C9 Niamh is currently serving there https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TRTPT",
        "indices" : [ 0, 6 ]
      } ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/BaTvbhaRcn",
        "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/L%C3%89_Niamh_%28P52%29",
        "display_url" : "en.wikipedia.org\/wiki\/L%C3%89_N\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "644907600178495488",
    "text" : "#TRTPT, some of Ireland's Navy boats serve in the Med, rescuing refugees. The L\u00C9 Niamh is currently serving there https:\/\/t.co\/BaTvbhaRcn",
    "id" : 644907600178495488,
    "created_at" : "2015-09-18 16:15:28 +0000",
    "user" : {
      "name" : "JK",
      "screen_name" : "KJoanne",
      "protected" : false,
      "id_str" : "20697268",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005549984522043392\/YDQyk6cL_normal.jpg",
      "id" : 20697268,
      "verified" : false
    }
  },
  "id" : 644913228246568961,
  "created_at" : "2015-09-18 16:37:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vala Afshar",
      "screen_name" : "ValaAfshar",
      "indices" : [ 3, 14 ],
      "id_str" : "259725229",
      "id" : 259725229
    }, {
      "name" : "Facebook",
      "screen_name" : "facebook",
      "indices" : [ 43, 52 ],
      "id_str" : "2425151",
      "id" : 2425151
    }, {
      "name" : "WhatsApp Inc.",
      "screen_name" : "WhatsApp",
      "indices" : [ 81, 90 ],
      "id_str" : "40148479",
      "id" : 40148479
    }, {
      "name" : "LinkedIn",
      "screen_name" : "LinkedIn",
      "indices" : [ 93, 102 ],
      "id_str" : "13058772",
      "id" : 13058772
    }, {
      "name" : "Instagram",
      "screen_name" : "instagram",
      "indices" : [ 121, 131 ],
      "id_str" : "180505807",
      "id" : 180505807
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DF15",
      "indices" : [ 34, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644896089271857152",
  "text" : "RT @ValaAfshar: Global Population #DF15\n\n1 @facebook\n2 China\n3 Tencent\n4 India\n5 @WhatsApp\n6 @LinkedIn\n7 United States\n8 @instagram\n9 @twit\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Facebook",
        "screen_name" : "facebook",
        "indices" : [ 27, 36 ],
        "id_str" : "2425151",
        "id" : 2425151
      }, {
        "name" : "WhatsApp Inc.",
        "screen_name" : "WhatsApp",
        "indices" : [ 65, 74 ],
        "id_str" : "40148479",
        "id" : 40148479
      }, {
        "name" : "LinkedIn",
        "screen_name" : "LinkedIn",
        "indices" : [ 77, 86 ],
        "id_str" : "13058772",
        "id" : 13058772
      }, {
        "name" : "Instagram",
        "screen_name" : "instagram",
        "indices" : [ 105, 115 ],
        "id_str" : "180505807",
        "id" : 180505807
      }, {
        "name" : "Twitter",
        "screen_name" : "Twitter",
        "indices" : [ 118, 126 ],
        "id_str" : "783214",
        "id" : 783214
      }, {
        "name" : "Snapchat",
        "screen_name" : "Snapchat",
        "indices" : [ 131, 140 ],
        "id_str" : "376502929",
        "id" : 376502929
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DF15",
        "indices" : [ 18, 23 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644648241733111809",
    "text" : "Global Population #DF15\n\n1 @facebook\n2 China\n3 Tencent\n4 India\n5 @WhatsApp\n6 @LinkedIn\n7 United States\n8 @instagram\n9 @twitter \n10 @Snapchat",
    "id" : 644648241733111809,
    "created_at" : "2015-09-17 23:04:52 +0000",
    "user" : {
      "name" : "Vala Afshar",
      "screen_name" : "ValaAfshar",
      "protected" : false,
      "id_str" : "259725229",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1259558245\/vala_300dpi_normal.jpg",
      "id" : 259725229,
      "verified" : true
    }
  },
  "id" : 644896089271857152,
  "created_at" : "2015-09-18 15:29:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/RMTOIldfRG",
      "expanded_url" : "https:\/\/sg.sports.yahoo.com\/blogs\/futbolita\/van-gaal-enjoys-night-out-060855550.html",
      "display_url" : "sg.sports.yahoo.com\/blogs\/futbolit\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644895522680111105",
  "text" : "Went to Manchester a few times but never hear of this restaurant. :) https:\/\/t.co\/RMTOIldfRG",
  "id" : 644895522680111105,
  "created_at" : "2015-09-18 15:27:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stephen Kearon",
      "screen_name" : "skearon",
      "indices" : [ 3, 11 ],
      "id_str" : "6287902",
      "id" : 6287902
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "vinb",
      "indices" : [ 13, 18 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644644396437213184",
  "text" : "RT @skearon: #vinb social welfare is supposed to be a temporary safety net, but far too many view it as a way of life",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.tweetcaster.com\" rel=\"nofollow\"\u003ETweetCaster for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "vinb",
        "indices" : [ 0, 5 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644636370674282496",
    "text" : "#vinb social welfare is supposed to be a temporary safety net, but far too many view it as a way of life",
    "id" : 644636370674282496,
    "created_at" : "2015-09-17 22:17:42 +0000",
    "user" : {
      "name" : "Stephen Kearon",
      "screen_name" : "skearon",
      "protected" : false,
      "id_str" : "6287902",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/951582416807882752\/JEYghLQj_normal.jpg",
      "id" : 6287902,
      "verified" : false
    }
  },
  "id" : 644644396437213184,
  "created_at" : "2015-09-17 22:49:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anna Rodgers",
      "screen_name" : "AnnaRodgersDocs",
      "indices" : [ 3, 19 ],
      "id_str" : "140451282",
      "id" : 140451282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644643470108413952",
  "text" : "RT @AnnaRodgersDocs: Absolutely loved working with the nurses and consultants in ICU - such a wonderful group of people who work so hard. T\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Crumlin",
        "indices" : [ 134, 142 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644558191481749504",
    "text" : "Absolutely loved working with the nurses and consultants in ICU - such a wonderful group of people who work so hard. Tears &amp; joy. #Crumlin",
    "id" : 644558191481749504,
    "created_at" : "2015-09-17 17:07:02 +0000",
    "user" : {
      "name" : "Anna Rodgers",
      "screen_name" : "AnnaRodgersDocs",
      "protected" : false,
      "id_str" : "140451282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1525135103\/Anna_MG_6565_normal.jpg",
      "id" : 140451282,
      "verified" : false
    }
  },
  "id" : 644643470108413952,
  "created_at" : "2015-09-17 22:45:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Crumlin",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644641930425581568",
  "text" : "#Crumlin Factual documentary on RTE One. Respect what Mrs and her colleagues do day-in day-out for the children.",
  "id" : 644641930425581568,
  "created_at" : "2015-09-17 22:39:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/644601408055066625\/photo\/1",
      "indices" : [ 56, 78 ],
      "url" : "http:\/\/t.co\/Gaqa5NVGmp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPIVkLSWsAAqNct.jpg",
      "id_str" : "644601407916650496",
      "id" : 644601407916650496,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPIVkLSWsAAqNct.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Gaqa5NVGmp"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 55 ],
      "url" : "http:\/\/t.co\/NNEQIbFVat",
      "expanded_url" : "http:\/\/ift.tt\/1F54v4g",
      "display_url" : "ift.tt\/1F54v4g"
    } ]
  },
  "geo" : { },
  "id_str" : "644601408055066625",
  "text" : "Grand Canal Dublin (Green Route) http:\/\/t.co\/NNEQIbFVat http:\/\/t.co\/Gaqa5NVGmp",
  "id" : 644601408055066625,
  "created_at" : "2015-09-17 19:58:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 38 ],
      "url" : "http:\/\/t.co\/bNF42eq7nm",
      "expanded_url" : "http:\/\/www.rte.ie\/news\/2015\/0917\/728543-horse-luas\/",
      "display_url" : "rte.ie\/news\/2015\/0917\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644594116463038464",
  "text" : "Only in Ireland http:\/\/t.co\/bNF42eq7nm",
  "id" : 644594116463038464,
  "created_at" : "2015-09-17 19:29:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644557161734012928",
  "text" : "Jialat, everything I got to follow up. Follow up inquiry than thing starts to move.",
  "id" : 644557161734012928,
  "created_at" : "2015-09-17 17:02:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/ifNh2piTuv",
      "expanded_url" : "https:\/\/flic.kr\/s\/aHskjWfTSW",
      "display_url" : "flic.kr\/s\/aHskjWfTSW"
    } ]
  },
  "geo" : { },
  "id_str" : "644538063679553536",
  "text" : "Exploring Grand Canal Dublin by bike https:\/\/t.co\/ifNh2piTuv",
  "id" : 644538063679553536,
  "created_at" : "2015-09-17 15:47:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Chris Hadfield",
      "screen_name" : "Cmdr_Hadfield",
      "indices" : [ 3, 17 ],
      "id_str" : "186154646",
      "id" : 186154646
    }, {
      "name" : "Ahmed Mohamed",
      "screen_name" : "IStandWithAhmed",
      "indices" : [ 22, 38 ],
      "id_str" : "3583264572",
      "id" : 3583264572
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644537202765111297",
  "text" : "RT @Cmdr_Hadfield: Hi @IStandWithAhmed ! I'd love you to join us for our science show Generator in Toronto on 28 Oct. There's a ticket wait\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ahmed Mohamed",
        "screen_name" : "IStandWithAhmed",
        "indices" : [ 3, 19 ],
        "id_str" : "3583264572",
        "id" : 3583264572
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644177398553030656",
    "text" : "Hi @IStandWithAhmed ! I'd love you to join us for our science show Generator in Toronto on 28 Oct. There's a ticket waiting for you.",
    "id" : 644177398553030656,
    "created_at" : "2015-09-16 15:53:54 +0000",
    "user" : {
      "name" : "Chris Hadfield",
      "screen_name" : "Cmdr_Hadfield",
      "protected" : false,
      "id_str" : "186154646",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/620788674414952456\/y_ozO3uO_normal.png",
      "id" : 186154646,
      "verified" : true
    }
  },
  "id" : 644537202765111297,
  "created_at" : "2015-09-17 15:43:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NewsTalk",
      "screen_name" : "NewsTalk",
      "indices" : [ 0, 9 ],
      "id_str" : "15767551",
      "id" : 15767551
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/gWsvttlOgO",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/644528460434677760",
      "display_url" : "twitter.com\/mryap\/status\/6\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644528937234792448",
  "in_reply_to_user_id" : 15767551,
  "text" : "@newstalk Your Twitter card pic is wrong. That a U2 spy plane https:\/\/t.co\/gWsvttlOgO",
  "id" : 644528937234792448,
  "created_at" : "2015-09-17 15:10:48 +0000",
  "in_reply_to_screen_name" : "NewsTalk",
  "in_reply_to_user_id_str" : "15767551",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Newstalk",
      "screen_name" : "NewstalkFM",
      "indices" : [ 81, 92 ],
      "id_str" : "22646514",
      "id" : 22646514
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 76 ],
      "url" : "http:\/\/t.co\/CBrbimCvnp",
      "expanded_url" : "http:\/\/www.newstalk.com\/reader\/47.301\/55415\/0\/#.VfrXNKg4vT0.twitter",
      "display_url" : "newstalk.com\/reader\/47.301\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644528460434677760",
  "text" : "Why did a US spy plane spend five hours over Ireland? http:\/\/t.co\/CBrbimCvnp via @NewstalkFM",
  "id" : 644528460434677760,
  "created_at" : "2015-09-17 15:08:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644522537611829248",
  "text" : "@bharatidalela @Azhreicb You get paid to do this for your day job? :)",
  "id" : 644522537611829248,
  "created_at" : "2015-09-17 14:45:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 27 ],
      "url" : "https:\/\/t.co\/m2E1rHxNpB",
      "expanded_url" : "https:\/\/twitter.com\/CiamhieMc\/status\/644484973655928832",
      "display_url" : "twitter.com\/CiamhieMc\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644485524963622912",
  "text" : "LOL https:\/\/t.co\/m2E1rHxNpB",
  "id" : 644485524963622912,
  "created_at" : "2015-09-17 12:18:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Isobel Butler",
      "screen_name" : "butlerisobel",
      "indices" : [ 3, 16 ],
      "id_str" : "108239343",
      "id" : 108239343
    }, {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "indices" : [ 117, 131 ],
      "id_str" : "150246405",
      "id" : 150246405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644448145338929153",
  "text" : "RT @butlerisobel: I've studied for years, so why does JobBridge advertise for the positions I should be filling?(via @thejournal_ie) http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TheJournal.ie",
        "screen_name" : "thejournal_ie",
        "indices" : [ 99, 113 ],
        "id_str" : "150246405",
        "id" : 150246405
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/DaYdlSGXQR",
        "expanded_url" : "http:\/\/jrnl.ie\/2302648",
        "display_url" : "jrnl.ie\/2302648"
      } ]
    },
    "geo" : { },
    "id_str" : "643765272088854528",
    "text" : "I've studied for years, so why does JobBridge advertise for the positions I should be filling?(via @thejournal_ie) http:\/\/t.co\/DaYdlSGXQR",
    "id" : 643765272088854528,
    "created_at" : "2015-09-15 12:36:16 +0000",
    "user" : {
      "name" : "Isobel Butler",
      "screen_name" : "butlerisobel",
      "protected" : false,
      "id_str" : "108239343",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/984151318234251264\/prYiWeKr_normal.jpg",
      "id" : 108239343,
      "verified" : false
    }
  },
  "id" : 644448145338929153,
  "created_at" : "2015-09-17 09:49:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/kol0NdJQWT",
      "expanded_url" : "https:\/\/mammothhq.com\/mryap",
      "display_url" : "mammothhq.com\/mryap"
    }, {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/YMWwnFAcF7",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=N8UmUxcepQM",
      "display_url" : "youtube.com\/watch?v=N8UmUx\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644447273347362816",
  "text" : "Ditching evernote for https:\/\/t.co\/kol0NdJQWT \nhttps:\/\/t.co\/YMWwnFAcF7",
  "id" : 644447273347362816,
  "created_at" : "2015-09-17 09:46:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644439609364086784",
  "text" : "RT @klillington: Big ICANN public meeting comes to Dublin next month &amp; YOU can attend\/contribute. Learn more w\/me &amp; panel tonight 5:30pm @d\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644439205733646340",
    "text" : "Big ICANN public meeting comes to Dublin next month &amp; YOU can attend\/contribute. Learn more w\/me &amp; panel tonight 5:30pm @deanhoteldublin",
    "id" : 644439205733646340,
    "created_at" : "2015-09-17 09:14:14 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 644439609364086784,
  "created_at" : "2015-09-17 09:15:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steve Herman",
      "screen_name" : "W7VOA",
      "indices" : [ 3, 9 ],
      "id_str" : "17919393",
      "id" : 17919393
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Tsunami",
      "indices" : [ 11, 19 ]
    }, {
      "text" : "Alaska",
      "indices" : [ 46, 53 ]
    }, {
      "text" : "California",
      "indices" : [ 55, 66 ]
    }, {
      "text" : "Canada",
      "indices" : [ 68, 75 ]
    }, {
      "text" : "Chile",
      "indices" : [ 77, 83 ]
    }, {
      "text" : "ChileQuake",
      "indices" : [ 107, 118 ]
    }, {
      "text" : "VOAalert",
      "indices" : [ 119, 128 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http:\/\/t.co\/EdAuPmcHXn",
      "expanded_url" : "http:\/\/ntwc.arh.noaa.gov\/php\/obs1.php",
      "display_url" : "ntwc.arh.noaa.gov\/php\/obs1.php"
    } ]
  },
  "geo" : { },
  "id_str" : "644429240591646720",
  "text" : "RT @W7VOA: #Tsunami observation\/forecasts for #Alaska, #California, #Canada, #Chile http:\/\/t.co\/EdAuPmcHXn #ChileQuake #VOAalert http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/W7VOA\/status\/644377856827486210\/photo\/1",
        "indices" : [ 118, 140 ],
        "url" : "http:\/\/t.co\/wtZNJTBuOp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPFKGwZUwAAYUq-.jpg",
        "id_str" : "644377701621481472",
        "id" : 644377701621481472,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPFKGwZUwAAYUq-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 649
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wtZNJTBuOp"
      } ],
      "hashtags" : [ {
        "text" : "Tsunami",
        "indices" : [ 0, 8 ]
      }, {
        "text" : "Alaska",
        "indices" : [ 35, 42 ]
      }, {
        "text" : "California",
        "indices" : [ 44, 55 ]
      }, {
        "text" : "Canada",
        "indices" : [ 57, 64 ]
      }, {
        "text" : "Chile",
        "indices" : [ 66, 72 ]
      }, {
        "text" : "ChileQuake",
        "indices" : [ 96, 107 ]
      }, {
        "text" : "VOAalert",
        "indices" : [ 108, 117 ]
      } ],
      "urls" : [ {
        "indices" : [ 73, 95 ],
        "url" : "http:\/\/t.co\/EdAuPmcHXn",
        "expanded_url" : "http:\/\/ntwc.arh.noaa.gov\/php\/obs1.php",
        "display_url" : "ntwc.arh.noaa.gov\/php\/obs1.php"
      } ]
    },
    "geo" : { },
    "id_str" : "644377856827486210",
    "text" : "#Tsunami observation\/forecasts for #Alaska, #California, #Canada, #Chile http:\/\/t.co\/EdAuPmcHXn #ChileQuake #VOAalert http:\/\/t.co\/wtZNJTBuOp",
    "id" : 644377856827486210,
    "created_at" : "2015-09-17 05:10:27 +0000",
    "user" : {
      "name" : "Steve Herman",
      "screen_name" : "W7VOA",
      "protected" : false,
      "id_str" : "17919393",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014548080669192192\/Lo5vS5FC_normal.jpg",
      "id" : 17919393,
      "verified" : true
    }
  },
  "id" : 644429240591646720,
  "created_at" : "2015-09-17 08:34:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5B64\u519B\u672A\u6B7B",
      "screen_name" : "Scswga",
      "indices" : [ 3, 10 ],
      "id_str" : "257844864",
      "id" : 257844864
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644405918545911808",
  "text" : "RT @Scswga: \u7ED9\u4E00\u5973\u540C\u4E8B\u4FEE\u7535\u8111\uFF0C\u6CA1\u4E8B\u95F2\u804A\u3002\n\n\u3000\u6211\uFF1A\u201C\u4F60\u7684\u80CC\u5305\u771F\u4E0D\u9519\uFF0C\u5728\u54EA\u4E70\u7684\uFF1F\u201D\n\n\u3000\u5973\u540C\u4E8B\uFF1A\u201C\u4F60\u8981\u662F\u559C\u6B22\u7684\u8BDD\uFF0C\u5C31\u9001\u7ED9\u4F60\u597D\u4E86\u3002\u201D\n\n\u3000\u6211\uFF1A\u201C\u4F60\u4E5F\u597D\u6F02\u4EAE\u3002\u201D\n\n\u3000\u5973\u540C\u4E8B\u2026\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644379350456909824",
    "text" : "\u7ED9\u4E00\u5973\u540C\u4E8B\u4FEE\u7535\u8111\uFF0C\u6CA1\u4E8B\u95F2\u804A\u3002\n\n\u3000\u6211\uFF1A\u201C\u4F60\u7684\u80CC\u5305\u771F\u4E0D\u9519\uFF0C\u5728\u54EA\u4E70\u7684\uFF1F\u201D\n\n\u3000\u5973\u540C\u4E8B\uFF1A\u201C\u4F60\u8981\u662F\u559C\u6B22\u7684\u8BDD\uFF0C\u5C31\u9001\u7ED9\u4F60\u597D\u4E86\u3002\u201D\n\n\u3000\u6211\uFF1A\u201C\u4F60\u4E5F\u597D\u6F02\u4EAE\u3002\u201D\n\n\u3000\u5973\u540C\u4E8B\u2026\u2026",
    "id" : 644379350456909824,
    "created_at" : "2015-09-17 05:16:23 +0000",
    "user" : {
      "name" : "\u5B64\u519B\u672A\u6B7B",
      "screen_name" : "Scswga",
      "protected" : false,
      "id_str" : "257844864",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1703168532\/Scswga_normal.jpg",
      "id" : 257844864,
      "verified" : false
    }
  },
  "id" : 644405918545911808,
  "created_at" : "2015-09-17 07:01:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MHC Tech Law",
      "screen_name" : "MHCTechLaw",
      "indices" : [ 3, 14 ],
      "id_str" : "2775735621",
      "id" : 2775735621
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "techlaw",
      "indices" : [ 100, 108 ]
    }, {
      "text" : "blog",
      "indices" : [ 109, 114 ]
    } ],
    "urls" : [ {
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/NhYsllp453",
      "expanded_url" : "http:\/\/buff.ly\/1ibuCvG",
      "display_url" : "buff.ly\/1ibuCvG"
    } ]
  },
  "geo" : { },
  "id_str" : "644264958079856640",
  "text" : "RT @MHCTechLaw: Awards of Damages for Data Protection Breaches \u2013 UK and Irish Approaches Contrasted #techlaw #blog http:\/\/t.co\/NhYsllp453",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "techlaw",
        "indices" : [ 84, 92 ]
      }, {
        "text" : "blog",
        "indices" : [ 93, 98 ]
      } ],
      "urls" : [ {
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/NhYsllp453",
        "expanded_url" : "http:\/\/buff.ly\/1ibuCvG",
        "display_url" : "buff.ly\/1ibuCvG"
      } ]
    },
    "geo" : { },
    "id_str" : "643880282995601408",
    "text" : "Awards of Damages for Data Protection Breaches \u2013 UK and Irish Approaches Contrasted #techlaw #blog http:\/\/t.co\/NhYsllp453",
    "id" : 643880282995601408,
    "created_at" : "2015-09-15 20:13:16 +0000",
    "user" : {
      "name" : "MHC Tech Law",
      "screen_name" : "MHCTechLaw",
      "protected" : false,
      "id_str" : "2775735621",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/512954665562947584\/yv6TdR3R_normal.jpeg",
      "id" : 2775735621,
      "verified" : false
    }
  },
  "id" : 644264958079856640,
  "created_at" : "2015-09-16 21:41:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "British Museum",
      "screen_name" : "britishmuseum",
      "indices" : [ 3, 17 ],
      "id_str" : "19066345",
      "id" : 19066345
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644257610728734720",
  "text" : "RT @britishmuseum: Celtic art has changed over time but it tends to be ambiguous, abstract and swirling, like on this shield and cross http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/britishmuseum\/status\/644111921055170560\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/RrLXA8zygE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPBYYO9W8AAGObN.jpg",
        "id_str" : "644111920069537792",
        "id" : 644111920069537792,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPBYYO9W8AAGObN.jpg",
        "sizes" : [ {
          "h" : 2074,
          "resize" : "fit",
          "w" : 1300
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 752
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 426
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1284
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RrLXA8zygE"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/britishmuseum\/status\/644111921055170560\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/RrLXA8zygE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPBYYNxWgAAKQTu.jpg",
        "id_str" : "644111919750742016",
        "id" : 644111919750742016,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPBYYNxWgAAKQTu.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1062
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 602
        }, {
          "h" : 1469,
          "resize" : "fit",
          "w" : 1300
        }, {
          "h" : 1469,
          "resize" : "fit",
          "w" : 1300
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RrLXA8zygE"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644111921055170560",
    "text" : "Celtic art has changed over time but it tends to be ambiguous, abstract and swirling, like on this shield and cross http:\/\/t.co\/RrLXA8zygE",
    "id" : 644111921055170560,
    "created_at" : "2015-09-16 11:33:43 +0000",
    "user" : {
      "name" : "British Museum",
      "screen_name" : "britishmuseum",
      "protected" : false,
      "id_str" : "19066345",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/552394937585500160\/BN7fW_Et_normal.jpeg",
      "id" : 19066345,
      "verified" : true
    }
  },
  "id" : 644257610728734720,
  "created_at" : "2015-09-16 21:12:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644246502479687680",
  "text" : "How many of you really see a bomb before? Not those on tv, video games, cartoon and movies.",
  "id" : 644246502479687680,
  "created_at" : "2015-09-16 20:28:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samantha Thebridge",
      "screen_name" : "samthebridge",
      "indices" : [ 3, 16 ],
      "id_str" : "106940242",
      "id" : 106940242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644245562791956480",
  "text" : "RT @samthebridge: Dear anti-vaxxers, I\u2019ve got frigging whooping cough because some dick decided their kid didn\u2019t need the pertussis vax.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644243148592447488",
    "text" : "Dear anti-vaxxers, I\u2019ve got frigging whooping cough because some dick decided their kid didn\u2019t need the pertussis vax.",
    "id" : 644243148592447488,
    "created_at" : "2015-09-16 20:15:10 +0000",
    "user" : {
      "name" : "Samantha Thebridge",
      "screen_name" : "samthebridge",
      "protected" : false,
      "id_str" : "106940242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/949501124897251329\/7aaB5olr_normal.jpg",
      "id" : 106940242,
      "verified" : false
    }
  },
  "id" : 644245562791956480,
  "created_at" : "2015-09-16 20:24:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "horzionvideogame",
      "indices" : [ 0, 17 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644238815083786240",
  "text" : "#horzionvideogame  playing video game can improve your cognitive ability.",
  "id" : 644238815083786240,
  "created_at" : "2015-09-16 19:57:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 11, 34 ],
      "url" : "https:\/\/t.co\/fxrc1emeqr",
      "expanded_url" : "https:\/\/data.gov.ie\/data",
      "display_url" : "data.gov.ie\/data"
    } ]
  },
  "geo" : { },
  "id_str" : "644186694317666304",
  "text" : "Bookmarked https:\/\/t.co\/fxrc1emeqr",
  "id" : 644186694317666304,
  "created_at" : "2015-09-16 16:30:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9 News",
      "screen_name" : "rtenews",
      "indices" : [ 3, 11 ],
      "id_str" : "8973062",
      "id" : 8973062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 79, 101 ],
      "url" : "http:\/\/t.co\/yQ5EQDzHs1",
      "expanded_url" : "http:\/\/bit.ly\/1F2CnyA",
      "display_url" : "bit.ly\/1F2CnyA"
    } ]
  },
  "geo" : { },
  "id_str" : "644184510771372033",
  "text" : "RT @rtenews: Syrian refugee tripped by camerawoman offered job as soccer coach http:\/\/t.co\/yQ5EQDzHs1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 88 ],
        "url" : "http:\/\/t.co\/yQ5EQDzHs1",
        "expanded_url" : "http:\/\/bit.ly\/1F2CnyA",
        "display_url" : "bit.ly\/1F2CnyA"
      } ]
    },
    "geo" : { },
    "id_str" : "644180001982689280",
    "text" : "Syrian refugee tripped by camerawoman offered job as soccer coach http:\/\/t.co\/yQ5EQDzHs1",
    "id" : 644180001982689280,
    "created_at" : "2015-09-16 16:04:15 +0000",
    "user" : {
      "name" : "RT\u00C9 News",
      "screen_name" : "rtenews",
      "protected" : false,
      "id_str" : "8973062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/535035010596630529\/3SmYLBeN_normal.png",
      "id" : 8973062,
      "verified" : true
    }
  },
  "id" : 644184510771372033,
  "created_at" : "2015-09-16 16:22:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ahmed Mohamed",
      "screen_name" : "IStandWithAhmed",
      "indices" : [ 3, 19 ],
      "id_str" : "3583264572",
      "id" : 3583264572
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IStandWithAhmed\/status\/644179809170509824\/photo\/1",
      "indices" : [ 46, 68 ],
      "url" : "http:\/\/t.co\/YCxOOeOz3Z",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPCWH3CUcAAHkz5.jpg",
      "id_str" : "644179808490909696",
      "id" : 644179808490909696,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPCWH3CUcAAHkz5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YCxOOeOz3Z"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644181930783678464",
  "text" : "RT @IStandWithAhmed: Going to meet my lawyer. http:\/\/t.co\/YCxOOeOz3Z",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IStandWithAhmed\/status\/644179809170509824\/photo\/1",
        "indices" : [ 25, 47 ],
        "url" : "http:\/\/t.co\/YCxOOeOz3Z",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPCWH3CUcAAHkz5.jpg",
        "id_str" : "644179808490909696",
        "id" : 644179808490909696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPCWH3CUcAAHkz5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YCxOOeOz3Z"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644179809170509824",
    "text" : "Going to meet my lawyer. http:\/\/t.co\/YCxOOeOz3Z",
    "id" : 644179809170509824,
    "created_at" : "2015-09-16 16:03:29 +0000",
    "user" : {
      "name" : "Ahmed Mohamed",
      "screen_name" : "IStandWithAhmed",
      "protected" : false,
      "id_str" : "3583264572",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1008494019532460032\/vfWrcl7U_normal.jpg",
      "id" : 3583264572,
      "verified" : true
    }
  },
  "id" : 644181930783678464,
  "created_at" : "2015-09-16 16:11:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Team AIB Racing",
      "screen_name" : "TeamAIBRacing",
      "indices" : [ 3, 17 ],
      "id_str" : "2805579478",
      "id" : 2805579478
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TeamAIBRacing\/status\/644155645923475456\/photo\/1",
      "indices" : [ 34, 56 ],
      "url" : "http:\/\/t.co\/BhuSSI6lhF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPCAI0yUwAE6S3y.jpg",
      "id_str" : "644155635811008513",
      "id" : 644155635811008513,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPCAI0yUwAE6S3y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BhuSSI6lhF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644163112153821184",
  "text" : "RT @TeamAIBRacing: The champions. http:\/\/t.co\/BhuSSI6lhF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TeamAIBRacing\/status\/644155645923475456\/photo\/1",
        "indices" : [ 15, 37 ],
        "url" : "http:\/\/t.co\/BhuSSI6lhF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CPCAI0yUwAE6S3y.jpg",
        "id_str" : "644155635811008513",
        "id" : 644155635811008513,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPCAI0yUwAE6S3y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BhuSSI6lhF"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644155645923475456",
    "text" : "The champions. http:\/\/t.co\/BhuSSI6lhF",
    "id" : 644155645923475456,
    "created_at" : "2015-09-16 14:27:28 +0000",
    "user" : {
      "name" : "Team AIB Racing",
      "screen_name" : "TeamAIBRacing",
      "protected" : false,
      "id_str" : "2805579478",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/631819755624734720\/iQnBjtRB_normal.jpg",
      "id" : 2805579478,
      "verified" : false
    }
  },
  "id" : 644163112153821184,
  "created_at" : "2015-09-16 14:57:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SarcasticRover",
      "screen_name" : "SarcasticRover",
      "indices" : [ 3, 18 ],
      "id_str" : "740109097",
      "id" : 740109097
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644162849569415168",
  "text" : "RT @SarcasticRover: If Ahmed made a poem, it would be subversive. If he made art, it would be inciteful. He made science and it was a threa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644156209562411008",
    "text" : "If Ahmed made a poem, it would be subversive. If he made art, it would be inciteful. He made science and it was a threat.\n\nThat's racism.",
    "id" : 644156209562411008,
    "created_at" : "2015-09-16 14:29:42 +0000",
    "user" : {
      "name" : "SarcasticRover",
      "screen_name" : "SarcasticRover",
      "protected" : false,
      "id_str" : "740109097",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839728901165203456\/mFZ-sZXQ_normal.jpg",
      "id" : 740109097,
      "verified" : false
    }
  },
  "id" : 644162849569415168,
  "created_at" : "2015-09-16 14:56:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Dooley",
      "screen_name" : "mattldooley",
      "indices" : [ 3, 15 ],
      "id_str" : "29413575",
      "id" : 29413575
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/5fBdt6z6TT",
      "expanded_url" : "https:\/\/instagram.com\/p\/7sImQUpIOa\/",
      "display_url" : "instagram.com\/p\/7sImQUpIOa\/"
    } ]
  },
  "geo" : { },
  "id_str" : "644158369180348416",
  "text" : "RT @mattldooley: Bankers, disrupters, politicians, to regulators. 41 out of top 100 fintech companies are in HK https:\/\/t.co\/5fBdt6z6TT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/5fBdt6z6TT",
        "expanded_url" : "https:\/\/instagram.com\/p\/7sImQUpIOa\/",
        "display_url" : "instagram.com\/p\/7sImQUpIOa\/"
      } ]
    },
    "geo" : { },
    "id_str" : "644108718318198784",
    "text" : "Bankers, disrupters, politicians, to regulators. 41 out of top 100 fintech companies are in HK https:\/\/t.co\/5fBdt6z6TT",
    "id" : 644108718318198784,
    "created_at" : "2015-09-16 11:21:00 +0000",
    "user" : {
      "name" : "Matt Dooley",
      "screen_name" : "mattldooley",
      "protected" : false,
      "id_str" : "29413575",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/737813232606281729\/LCt3csug_normal.jpg",
      "id" : 29413575,
      "verified" : false
    }
  },
  "id" : 644158369180348416,
  "created_at" : "2015-09-16 14:38:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "indices" : [ 3, 12 ],
      "id_str" : "36823",
      "id" : 36823
    }, {
      "name" : "Ahmed Mohamed",
      "screen_name" : "IStandWithAhmed",
      "indices" : [ 72, 88 ],
      "id_str" : "3583264572",
      "id" : 3583264572
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644154191485751296",
  "text" : "RT @anildash: Ahmed and family have set up an official Twitter account: @IStandWithAhmed Please follow it for their own updates.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ahmed Mohamed",
        "screen_name" : "IStandWithAhmed",
        "indices" : [ 58, 74 ],
        "id_str" : "3583264572",
        "id" : 3583264572
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "644139245809045504",
    "geo" : { },
    "id_str" : "644152449616506880",
    "in_reply_to_user_id" : 36823,
    "text" : "Ahmed and family have set up an official Twitter account: @IStandWithAhmed Please follow it for their own updates.",
    "id" : 644152449616506880,
    "in_reply_to_status_id" : 644139245809045504,
    "created_at" : "2015-09-16 14:14:46 +0000",
    "in_reply_to_screen_name" : "anildash",
    "in_reply_to_user_id_str" : "36823",
    "user" : {
      "name" : "Anil Dash",
      "screen_name" : "anildash",
      "protected" : false,
      "id_str" : "36823",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040592282032984071\/RC8We7BG_normal.jpg",
      "id" : 36823,
      "verified" : true
    }
  },
  "id" : 644154191485751296,
  "created_at" : "2015-09-16 14:21:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/iOdlBi7lFd",
      "expanded_url" : "https:\/\/twitter.com\/gilleechi\/status\/644152123685474304",
      "display_url" : "twitter.com\/gilleechi\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644153585291390976",
  "text" : "Which part of the day are mobile traffic? https:\/\/t.co\/iOdlBi7lFd",
  "id" : 644153585291390976,
  "created_at" : "2015-09-16 14:19:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CeADAR Ireland",
      "screen_name" : "CeADARIreland",
      "indices" : [ 117, 131 ],
      "id_str" : "1153461168",
      "id" : 1153461168
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644145742085554176",
  "text" : "RT @Mark_cpp: \"A boeing 737 generates 240 terabytes of data from its engines during a single flight across the US\" - @CeADARIreland at #Pre\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "CeADAR Ireland",
        "screen_name" : "CeADARIreland",
        "indices" : [ 103, 117 ],
        "id_str" : "1153461168",
        "id" : 1153461168
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "PredictConf",
        "indices" : [ 121, 133 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644110156163383297",
    "text" : "\"A boeing 737 generates 240 terabytes of data from its engines during a single flight across the US\" - @CeADARIreland at #PredictConf",
    "id" : 644110156163383297,
    "created_at" : "2015-09-16 11:26:42 +0000",
    "user" : {
      "name" : "Mark Lambe \uD83D\uDC68\uD83C\uDFFB\u200D\uD83D\uDCBB",
      "screen_name" : "MarkLambe_",
      "protected" : false,
      "id_str" : "357063176",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030885705260720129\/8EXPuc6h_normal.jpg",
      "id" : 357063176,
      "verified" : false
    }
  },
  "id" : 644145742085554176,
  "created_at" : "2015-09-16 13:48:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/s8KJMtco3J",
      "expanded_url" : "https:\/\/twitter.com\/colettebrowne\/status\/643773259713003521",
      "display_url" : "twitter.com\/colettebrowne\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644130234443431937",
  "text" : "\uD83D\uDE06 https:\/\/t.co\/s8KJMtco3J",
  "id" : 644130234443431937,
  "created_at" : "2015-09-16 12:46:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/644118557085839361\/photo\/1",
      "indices" : [ 71, 93 ],
      "url" : "http:\/\/t.co\/XuZGTJ5DeE",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPBeaepWwAQc6Kw.png",
      "id_str" : "644118555710111748",
      "id" : 644118555710111748,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPBeaepWwAQc6Kw.png",
      "sizes" : [ {
        "h" : 155,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 180,
        "resize" : "fit",
        "w" : 790
      }, {
        "h" : 180,
        "resize" : "fit",
        "w" : 790
      }, {
        "h" : 180,
        "resize" : "fit",
        "w" : 790
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XuZGTJ5DeE"
    } ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 58, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 7, 29 ],
      "url" : "http:\/\/t.co\/r3BqnbgGr2",
      "expanded_url" : "http:\/\/www.iubenda.com\/en",
      "display_url" : "iubenda.com\/en"
    } ]
  },
  "geo" : { },
  "id_str" : "644118557085839361",
  "text" : "I used http:\/\/t.co\/r3BqnbgGr2 to generate privacy policy. #getoptimise http:\/\/t.co\/XuZGTJ5DeE",
  "id" : 644118557085839361,
  "created_at" : "2015-09-16 12:00:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/1EnmbWu7gw",
      "expanded_url" : "https:\/\/www.techinasia.com\/estonia-e-residency-singapore\/",
      "display_url" : "techinasia.com\/estonia-e-resi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644114219756072960",
  "text" : "Estonia opens its e-residency program to Singapore https:\/\/t.co\/1EnmbWu7gw",
  "id" : 644114219756072960,
  "created_at" : "2015-09-16 11:42:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tech in Asia",
      "screen_name" : "techinasia",
      "indices" : [ 116, 127 ],
      "id_str" : "44078873",
      "id" : 44078873
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/VHAHFYb4X4",
      "expanded_url" : "https:\/\/e-estonia.com\/e-residents\/about\/",
      "display_url" : "e-estonia.com\/e-residents\/ab\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644102347258470400",
  "text" : "Now you can be an e-residents of Estonia. Opening up to Singaporeans in early 2016 https:\/\/t.co\/VHAHFYb4X4 Link via @techinasia",
  "id" : 644102347258470400,
  "created_at" : "2015-09-16 10:55:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samantha Yuen",
      "screen_name" : "duzkiez",
      "indices" : [ 3, 11 ],
      "id_str" : "15798192",
      "id" : 15798192
    }, {
      "name" : "Aleksandra Melnikova",
      "screen_name" : "alex_andr_a",
      "indices" : [ 95, 107 ],
      "id_str" : "118891632",
      "id" : 118891632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http:\/\/t.co\/Op10Z9zGAL",
      "expanded_url" : "http:\/\/everythingwithatwist.com\/2015\/09\/15\/local-food-represented-as-social-ecomomic-data-by-susanne-jaschko-and-moritz-stefaner\/",
      "display_url" : "everythingwithatwist.com\/2015\/09\/15\/loc\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644096246186123264",
  "text" : "RT @duzkiez: Social economic data represented beautifully with food http:\/\/t.co\/Op10Z9zGAL via @alex_andr_a",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Aleksandra Melnikova",
        "screen_name" : "alex_andr_a",
        "indices" : [ 82, 94 ],
        "id_str" : "118891632",
        "id" : 118891632
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 77 ],
        "url" : "http:\/\/t.co\/Op10Z9zGAL",
        "expanded_url" : "http:\/\/everythingwithatwist.com\/2015\/09\/15\/local-food-represented-as-social-ecomomic-data-by-susanne-jaschko-and-moritz-stefaner\/",
        "display_url" : "everythingwithatwist.com\/2015\/09\/15\/loc\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643959223777234944",
    "text" : "Social economic data represented beautifully with food http:\/\/t.co\/Op10Z9zGAL via @alex_andr_a",
    "id" : 643959223777234944,
    "created_at" : "2015-09-16 01:26:57 +0000",
    "user" : {
      "name" : "Samantha Yuen",
      "screen_name" : "duzkiez",
      "protected" : false,
      "id_str" : "15798192",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/561441538069118976\/dTtvRTFO_normal.jpeg",
      "id" : 15798192,
      "verified" : false
    }
  },
  "id" : 644096246186123264,
  "created_at" : "2015-09-16 10:31:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/644094979925770240\/photo\/1",
      "indices" : [ 79, 101 ],
      "url" : "http:\/\/t.co\/eSOhHNa3QW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CPBI-I5WsAAiwKo.jpg",
      "id_str" : "644094979091116032",
      "id" : 644094979091116032,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CPBI-I5WsAAiwKo.jpg",
      "sizes" : [ {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eSOhHNa3QW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644094979925770240",
  "text" : "Mr Lee Kuan Yew would have been 92 today. In memory of him, I post this quote. http:\/\/t.co\/eSOhHNa3QW",
  "id" : 644094979925770240,
  "created_at" : "2015-09-16 10:26:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644092172338352129",
  "text" : "Acceptance deadline is 22\/9 but the college website states the course commerce on 21\/9",
  "id" : 644092172338352129,
  "created_at" : "2015-09-16 10:15:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Heath W. Black",
      "screen_name" : "heathwblack",
      "indices" : [ 3, 15 ],
      "id_str" : "14092439",
      "id" : 14092439
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HelpAhmedMake",
      "indices" : [ 23, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/7JZlHHdY06",
      "expanded_url" : "https:\/\/docs.google.com\/forms\/d\/1ws7e8WyQvrsLfhSFvdGot3n9NWKfY3XLPBgbHaZDy3k\/viewform?c=0&w=1",
      "display_url" : "docs.google.com\/forms\/d\/1ws7e8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644091776580591616",
  "text" : "RT @heathwblack: Let\u2019s #HelpAhmedMake a world of exploration &amp; creativity, not Islamophobic hatred. \n\nhttps:\/\/t.co\/7JZlHHdY06 https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "HelpAhmedMake",
        "indices" : [ 6, 20 ]
      } ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/7JZlHHdY06",
        "expanded_url" : "https:\/\/docs.google.com\/forms\/d\/1ws7e8WyQvrsLfhSFvdGot3n9NWKfY3XLPBgbHaZDy3k\/viewform?c=0&w=1",
        "display_url" : "docs.google.com\/forms\/d\/1ws7e8\u2026"
      }, {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/CDYvZzmsDw",
        "expanded_url" : "https:\/\/twitter.com\/anildash\/status\/644020453724585984",
        "display_url" : "twitter.com\/anildash\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "644023561552683008",
    "text" : "Let\u2019s #HelpAhmedMake a world of exploration &amp; creativity, not Islamophobic hatred. \n\nhttps:\/\/t.co\/7JZlHHdY06 https:\/\/t.co\/CDYvZzmsDw",
    "id" : 644023561552683008,
    "created_at" : "2015-09-16 05:42:37 +0000",
    "user" : {
      "name" : "Heath W. Black",
      "screen_name" : "heathwblack",
      "protected" : false,
      "id_str" : "14092439",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/965465559105126401\/QNhTFpyv_normal.jpg",
      "id" : 14092439,
      "verified" : false
    }
  },
  "id" : 644091776580591616,
  "created_at" : "2015-09-16 10:13:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "indices" : [ 3, 15 ],
      "id_str" : "14107081",
      "id" : 14107081
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644091601753665536",
  "text" : "RT @geekonomics: SIA A-380 bound for Los Angeles returns to Tokyo due to engine trouble. Yikes I have taken this route many times\nhttp:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 135 ],
        "url" : "http:\/\/t.co\/ZVxyjxVFZI",
        "expanded_url" : "http:\/\/www.straitstimes.com\/singapore\/sia-a-380-bound-for-los-angeles-returns-to-tokyo-because-of-engine-trouble?xtor=CS3-18",
        "display_url" : "straitstimes.com\/singapore\/sia-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "644090697578971136",
    "text" : "SIA A-380 bound for Los Angeles returns to Tokyo due to engine trouble. Yikes I have taken this route many times\nhttp:\/\/t.co\/ZVxyjxVFZI",
    "id" : 644090697578971136,
    "created_at" : "2015-09-16 10:09:23 +0000",
    "user" : {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "protected" : false,
      "id_str" : "14107081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815459302941093888\/HFWgUA6V_normal.jpg",
      "id" : 14107081,
      "verified" : false
    }
  },
  "id" : 644091601753665536,
  "created_at" : "2015-09-16 10:12:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/9WB4jj3RuC",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/thousands-ethnic-malays-rally-support-malaysian-leader-063950820.html",
      "display_url" : "sg.news.yahoo.com\/thousands-ethn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644089822001369088",
  "text" : "..push their way through barricades into Kuala Lumpur's Chinatown, shouting \"This is Malay land.\"  https:\/\/t.co\/9WB4jj3RuC",
  "id" : 644089822001369088,
  "created_at" : "2015-09-16 10:05:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "dublinese \u6708\u5149",
      "screen_name" : "rachel_violin",
      "indices" : [ 0, 14 ],
      "id_str" : "116755889",
      "id" : 116755889
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/7VI1XfPI16",
      "expanded_url" : "https:\/\/twitter.com\/Indo_Travel_\/status\/644050690600497152",
      "display_url" : "twitter.com\/Indo_Travel_\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "644053979236814848",
  "in_reply_to_user_id" : 116755889,
  "text" : "@rachel_violin  Have you read this? https:\/\/t.co\/7VI1XfPI16",
  "id" : 644053979236814848,
  "created_at" : "2015-09-16 07:43:29 +0000",
  "in_reply_to_screen_name" : "rachel_violin",
  "in_reply_to_user_id_str" : "116755889",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644048179202256897",
  "text" : "Here a pic of me holding a cup of coffee that on Instagram and auto post to Twitter via IFTTT",
  "id" : 644048179202256897,
  "created_at" : "2015-09-16 07:20:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644046437370753025",
  "text" : "Data-Driven marketing is just like saying Data Driven Weather forecasting.",
  "id" : 644046437370753025,
  "created_at" : "2015-09-16 07:13:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ghazala Irshad",
      "screen_name" : "ghazalairshad",
      "indices" : [ 3, 17 ],
      "id_str" : "25529018",
      "id" : 25529018
    }, {
      "name" : "Hend Amry",
      "screen_name" : "LibyaLiberty",
      "indices" : [ 20, 33 ],
      "id_str" : "254873869",
      "id" : 254873869
    }, {
      "name" : "Clive Thompson",
      "screen_name" : "pomeranian99",
      "indices" : [ 34, 47 ],
      "id_str" : "661403",
      "id" : 661403
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644036583490981888",
  "text" : "RT @ghazalairshad: .@LibyaLiberty @pomeranian99 Ahmed's classmates &amp; other students are tweeting that they're bringing clocks to school in \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hend Amry",
        "screen_name" : "LibyaLiberty",
        "indices" : [ 1, 14 ],
        "id_str" : "254873869",
        "id" : 254873869
      }, {
        "name" : "Clive Thompson",
        "screen_name" : "pomeranian99",
        "indices" : [ 15, 28 ],
        "id_str" : "661403",
        "id" : 661403
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "644004512441610240",
    "geo" : { },
    "id_str" : "644005276568408064",
    "in_reply_to_user_id" : 254873869,
    "text" : ".@LibyaLiberty @pomeranian99 Ahmed's classmates &amp; other students are tweeting that they're bringing clocks to school in solidarity tomorrow.",
    "id" : 644005276568408064,
    "in_reply_to_status_id" : 644004512441610240,
    "created_at" : "2015-09-16 04:29:57 +0000",
    "in_reply_to_screen_name" : "LibyaLiberty",
    "in_reply_to_user_id_str" : "254873869",
    "user" : {
      "name" : "Ghazala Irshad",
      "screen_name" : "ghazalairshad",
      "protected" : false,
      "id_str" : "25529018",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964462061530034177\/V7fWKlTh_normal.jpg",
      "id" : 25529018,
      "verified" : true
    }
  },
  "id" : 644036583490981888,
  "created_at" : "2015-09-16 06:34:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dave Weinstein",
      "screen_name" : "dhw",
      "indices" : [ 3, 7 ],
      "id_str" : "16203557",
      "id" : 16203557
    }, {
      "name" : "_defcon_",
      "screen_name" : "_defcon_",
      "indices" : [ 107, 116 ],
      "id_str" : "4891250338",
      "id" : 4891250338
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644036139700109312",
  "text" : "RT @dhw: Has anyone set up a fund to bring Ahmed Mohamed (and a parent or parents, since he is a minor) to @_defcon_ ? Seriously, I'll kick\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "_defcon_",
        "screen_name" : "_defcon_",
        "indices" : [ 98, 107 ],
        "id_str" : "4891250338",
        "id" : 4891250338
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "644023101672427520",
    "text" : "Has anyone set up a fund to bring Ahmed Mohamed (and a parent or parents, since he is a minor) to @_defcon_ ? Seriously, I'll kick in...",
    "id" : 644023101672427520,
    "created_at" : "2015-09-16 05:40:47 +0000",
    "user" : {
      "name" : "Dave Weinstein",
      "screen_name" : "dhw",
      "protected" : false,
      "id_str" : "16203557",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1091521143\/Dave_normal.jpg",
      "id" : 16203557,
      "verified" : false
    }
  },
  "id" : 644036139700109312,
  "created_at" : "2015-09-16 06:32:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Indian Stats",
      "screen_name" : "Indian_stats",
      "indices" : [ 3, 16 ],
      "id_str" : "1490927714",
      "id" : 1490927714
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "644034443225792512",
  "text" : "RT @Indian_stats: me: papa I want the iPhone 6S\n\npapa: and I want a child who is a doctor and studies hard in school but we all don't get w\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641694963533774848",
    "text" : "me: papa I want the iPhone 6S\n\npapa: and I want a child who is a doctor and studies hard in school but we all don't get what we want",
    "id" : 641694963533774848,
    "created_at" : "2015-09-09 19:29:36 +0000",
    "user" : {
      "name" : "Indian Stats",
      "screen_name" : "Indian_stats",
      "protected" : false,
      "id_str" : "1490927714",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/748309916721381376\/mDouWOSx_normal.jpg",
      "id" : 1490927714,
      "verified" : false
    }
  },
  "id" : 644034443225792512,
  "created_at" : "2015-09-16 06:25:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Colin McGovern",
      "screen_name" : "cmcgovern",
      "indices" : [ 3, 13 ],
      "id_str" : "20393461",
      "id" : 20393461
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643907930341933056",
  "text" : "RT @cmcgovern: Seriously, what the hell is a rock star intern and why aren\u2019t you just paying them if they\u2019re so amazing? https:\/\/t.co\/lc2NZ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/lc2NZdBr1d",
        "expanded_url" : "https:\/\/twitter.com\/SAI\/status\/643892342861221888",
        "display_url" : "twitter.com\/SAI\/status\/643\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643893092295254016",
    "text" : "Seriously, what the hell is a rock star intern and why aren\u2019t you just paying them if they\u2019re so amazing? https:\/\/t.co\/lc2NZdBr1d",
    "id" : 643893092295254016,
    "created_at" : "2015-09-15 21:04:10 +0000",
    "user" : {
      "name" : "Colin McGovern",
      "screen_name" : "cmcgovern",
      "protected" : false,
      "id_str" : "20393461",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/602555759449264129\/Dk3bXn6F_normal.jpg",
      "id" : 20393461,
      "verified" : false
    }
  },
  "id" : 643907930341933056,
  "created_at" : "2015-09-15 22:03:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643875711841923072",
  "text" : "On Linkedin, someone job title \"provider relations\" What is that?",
  "id" : 643875711841923072,
  "created_at" : "2015-09-15 19:55:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/woHQnD0uFH",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=4z-5wyVDAmo&feature=youtu.be",
      "display_url" : "youtube.com\/watch?v=4z-5wy\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643862837811671040",
  "text" : "Using Excel to generate SQL Insert Statements  https:\/\/t.co\/woHQnD0uFH",
  "id" : 643862837811671040,
  "created_at" : "2015-09-15 19:03:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643832882625159168",
  "text" : "Without data you\u2019re just another person with an opinion. - W. Edwards Deming (1900-1993)",
  "id" : 643832882625159168,
  "created_at" : "2015-09-15 17:04:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Air Corps",
      "screen_name" : "IrishAirCorps",
      "indices" : [ 3, 17 ],
      "id_str" : "171238684",
      "id" : 171238684
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643806270869381120",
  "text" : "RT @IrishAirCorps: 101 Squadron's vigilance paid off yesterday capturing this pod off the West Coast. Snapped by Cpl Pat O'Meara http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishAirCorps\/status\/643130381928132608\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/ZhVCyZT6fd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COzbrFgWIAEhfBc.jpg",
        "id_str" : "643130380065841153",
        "id" : 643130380065841153,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COzbrFgWIAEhfBc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 857,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 486,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1500,
          "resize" : "fit",
          "w" : 2100
        }, {
          "h" : 1463,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ZhVCyZT6fd"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IrishAirCorps\/status\/643130381928132608\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/ZhVCyZT6fd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COzbrDJWgAEnndi.jpg",
        "id_str" : "643130379432525825",
        "id" : 643130379432525825,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COzbrDJWgAEnndi.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 857,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 486,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1500,
          "resize" : "fit",
          "w" : 2100
        }, {
          "h" : 1463,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ZhVCyZT6fd"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643130381928132608",
    "text" : "101 Squadron's vigilance paid off yesterday capturing this pod off the West Coast. Snapped by Cpl Pat O'Meara http:\/\/t.co\/ZhVCyZT6fd",
    "id" : 643130381928132608,
    "created_at" : "2015-09-13 18:33:26 +0000",
    "user" : {
      "name" : "Irish Air Corps",
      "screen_name" : "IrishAirCorps",
      "protected" : false,
      "id_str" : "171238684",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/684346277530865664\/06CJB6yt_normal.png",
      "id" : 171238684,
      "verified" : true
    }
  },
  "id" : 643806270869381120,
  "created_at" : "2015-09-15 15:19:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CO Data Science",
      "screen_name" : "CODataScience",
      "indices" : [ 3, 17 ],
      "id_str" : "3535210840",
      "id" : 3535210840
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Manchester",
      "indices" : [ 67, 78 ]
    }, {
      "text" : "CivilServiceLive",
      "indices" : [ 79, 96 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643805611382173696",
  "text" : "RT @CODataScience: Busy morning in the Data and Innovation zone at #Manchester #CivilServiceLive talking data science in government. http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CODataScience\/status\/643393665608630274\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/GP3wGVITQB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3LIOwWoAA_Nio.jpg",
        "id_str" : "643393664044146688",
        "id" : 643393664044146688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3LIOwWoAA_Nio.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GP3wGVITQB"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/CODataScience\/status\/643393665608630274\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/GP3wGVITQB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3LIOQWIAAaxA2.jpg",
        "id_str" : "643393663909896192",
        "id" : 643393663909896192,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3LIOQWIAAaxA2.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GP3wGVITQB"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/CODataScience\/status\/643393665608630274\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/GP3wGVITQB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3LIQ1WcAAwTm0.jpg",
        "id_str" : "643393664601976832",
        "id" : 643393664601976832,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3LIQ1WcAAwTm0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GP3wGVITQB"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/CODataScience\/status\/643393665608630274\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/GP3wGVITQB",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3LISBXAAA7rg0.jpg",
        "id_str" : "643393664920780800",
        "id" : 643393664920780800,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3LISBXAAA7rg0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GP3wGVITQB"
      } ],
      "hashtags" : [ {
        "text" : "Manchester",
        "indices" : [ 48, 59 ]
      }, {
        "text" : "CivilServiceLive",
        "indices" : [ 60, 77 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643393665608630274",
    "text" : "Busy morning in the Data and Innovation zone at #Manchester #CivilServiceLive talking data science in government. http:\/\/t.co\/GP3wGVITQB",
    "id" : 643393665608630274,
    "created_at" : "2015-09-14 11:59:38 +0000",
    "user" : {
      "name" : "CO Data Science",
      "screen_name" : "CODataScience",
      "protected" : false,
      "id_str" : "3535210840",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/639748457876557824\/cfMqa_ip_normal.png",
      "id" : 3535210840,
      "verified" : false
    }
  },
  "id" : 643805611382173696,
  "created_at" : "2015-09-15 15:16:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643796273229864960",
  "text" : "How is organising a conference on a new sector going to put a country on a global map?",
  "id" : 643796273229864960,
  "created_at" : "2015-09-15 14:39:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Wall Street Journal",
      "screen_name" : "WSJ",
      "indices" : [ 3, 7 ],
      "id_str" : "3108351",
      "id" : 3108351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 122 ],
      "url" : "http:\/\/t.co\/MVFMM12zxA",
      "expanded_url" : "http:\/\/on.wsj.com\/1EZHyPW",
      "display_url" : "on.wsj.com\/1EZHyPW"
    } ]
  },
  "geo" : { },
  "id_str" : "643790046844350464",
  "text" : "RT @WSJ: What country puts 2% of its trash in landfills, burns 38% for power and recycles the rest?\nhttp:\/\/t.co\/MVFMM12zxA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/MVFMM12zxA",
        "expanded_url" : "http:\/\/on.wsj.com\/1EZHyPW",
        "display_url" : "on.wsj.com\/1EZHyPW"
      } ]
    },
    "geo" : { },
    "id_str" : "643763714295005184",
    "text" : "What country puts 2% of its trash in landfills, burns 38% for power and recycles the rest?\nhttp:\/\/t.co\/MVFMM12zxA",
    "id" : 643763714295005184,
    "created_at" : "2015-09-15 12:30:04 +0000",
    "user" : {
      "name" : "The Wall Street Journal",
      "screen_name" : "WSJ",
      "protected" : false,
      "id_str" : "3108351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/971415515754266624\/zCX0q9d5_normal.jpg",
      "id" : 3108351,
      "verified" : true
    }
  },
  "id" : 643790046844350464,
  "created_at" : "2015-09-15 14:14:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643789531611824128",
  "text" : "Anyone enrolling their kids to secondary school next year in Dublin? Can I talk to you?",
  "id" : 643789531611824128,
  "created_at" : "2015-09-15 14:12:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/UqrUiEmXme",
      "expanded_url" : "http:\/\/www.tomshardware.co.uk\/forum\/245098-28-keyboard-keys-function-wrong-shift",
      "display_url" : "tomshardware.co.uk\/forum\/245098-2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643768703427985408",
  "text" : "One of the problem when you done with clean install is you unable to type @ Here the solution. http:\/\/t.co\/UqrUiEmXme",
  "id" : 643768703427985408,
  "created_at" : "2015-09-15 12:49:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/FUr7tD3rL8",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/fight-ban-urged-australian-boxer-dies-062236287--spt.html",
      "display_url" : "sg.news.yahoo.com\/fight-ban-urge\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643753566369390592",
  "text" : "Just signed a consent form for my kid to learn boxing in school...https:\/\/t.co\/FUr7tD3rL8",
  "id" : 643753566369390592,
  "created_at" : "2015-09-15 11:49:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Galway 2020",
      "screen_name" : "galway2020",
      "indices" : [ 3, 14 ],
      "id_str" : "2353144260",
      "id" : 2353144260
    }, {
      "name" : "PictureDiaryGalway",
      "screen_name" : "PicDiaryGalway",
      "indices" : [ 117, 132 ],
      "id_str" : "3377245197",
      "id" : 3377245197
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643747636588244992",
  "text" : "RT @galway2020: \"All tribes are Welcome Here\" Galway declares open doors &amp; hearts for refugees yesterday.\nPhoto: @PicDiaryGalway http:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "PictureDiaryGalway",
        "screen_name" : "PicDiaryGalway",
        "indices" : [ 101, 116 ],
        "id_str" : "3377245197",
        "id" : 3377245197
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/galway2020\/status\/643068741274705920\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/DZ26FLG2HI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COyjnJdWUAEPRqw.png",
        "id_str" : "643068739756380161",
        "id" : 643068739756380161,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COyjnJdWUAEPRqw.png",
        "sizes" : [ {
          "h" : 685,
          "resize" : "fit",
          "w" : 515
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 511
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 515
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 685,
          "resize" : "fit",
          "w" : 515
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DZ26FLG2HI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643068741274705920",
    "text" : "\"All tribes are Welcome Here\" Galway declares open doors &amp; hearts for refugees yesterday.\nPhoto: @PicDiaryGalway http:\/\/t.co\/DZ26FLG2HI",
    "id" : 643068741274705920,
    "created_at" : "2015-09-13 14:28:30 +0000",
    "user" : {
      "name" : "Galway 2020",
      "screen_name" : "galway2020",
      "protected" : false,
      "id_str" : "2353144260",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/928944358153510913\/_8YU2g0B_normal.jpg",
      "id" : 2353144260,
      "verified" : false
    }
  },
  "id" : 643747636588244992,
  "created_at" : "2015-09-15 11:26:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "yesireland",
      "indices" : [ 116, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643747207846436864",
  "text" : "Impressed by an organisation response. Email about enrollment form yesterday pm and got it via postal mail this am! #yesireland !",
  "id" : 643747207846436864,
  "created_at" : "2015-09-15 11:24:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Omar Waraich",
      "screen_name" : "OmarWaraich",
      "indices" : [ 3, 15 ],
      "id_str" : "20771519",
      "id" : 20771519
    }, {
      "name" : "Jamie McGeever",
      "screen_name" : "ReutersJamie",
      "indices" : [ 17, 30 ],
      "id_str" : "288980311",
      "id" : 288980311
    }, {
      "name" : "The Times of London",
      "screen_name" : "thetimes",
      "indices" : [ 31, 40 ],
      "id_str" : "6107422",
      "id" : 6107422
    }, {
      "name" : "Dawn Foster",
      "screen_name" : "DawnHFoster",
      "indices" : [ 41, 53 ],
      "id_str" : "15344145",
      "id" : 15344145
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/oWl5iQhp8Q",
      "expanded_url" : "https:\/\/twitter.com\/OmarWaraich\/status\/643651121114914816",
      "display_url" : "twitter.com\/OmarWaraich\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643745939962904577",
  "text" : "RT @OmarWaraich: @ReutersJamie @thetimes @DawnHFoster https:\/\/t.co\/oWl5iQhp8Q",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jamie McGeever",
        "screen_name" : "ReutersJamie",
        "indices" : [ 0, 13 ],
        "id_str" : "288980311",
        "id" : 288980311
      }, {
        "name" : "The Times of London",
        "screen_name" : "thetimes",
        "indices" : [ 14, 23 ],
        "id_str" : "6107422",
        "id" : 6107422
      }, {
        "name" : "Dawn Foster",
        "screen_name" : "DawnHFoster",
        "indices" : [ 24, 36 ],
        "id_str" : "15344145",
        "id" : 15344145
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/oWl5iQhp8Q",
        "expanded_url" : "https:\/\/twitter.com\/OmarWaraich\/status\/643651121114914816",
        "display_url" : "twitter.com\/OmarWaraich\/st\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "643528765138731008",
    "geo" : { },
    "id_str" : "643677345304473600",
    "in_reply_to_user_id" : 288980311,
    "text" : "@ReutersJamie @thetimes @DawnHFoster https:\/\/t.co\/oWl5iQhp8Q",
    "id" : 643677345304473600,
    "in_reply_to_status_id" : 643528765138731008,
    "created_at" : "2015-09-15 06:46:52 +0000",
    "in_reply_to_screen_name" : "ReutersJamie",
    "in_reply_to_user_id_str" : "288980311",
    "user" : {
      "name" : "Omar Waraich",
      "screen_name" : "OmarWaraich",
      "protected" : false,
      "id_str" : "20771519",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885716761697452033\/F6x5mXn8_normal.jpg",
      "id" : 20771519,
      "verified" : true
    }
  },
  "id" : 643745939962904577,
  "created_at" : "2015-09-15 11:19:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 93 ],
      "url" : "http:\/\/t.co\/J7bGmCJEri",
      "expanded_url" : "http:\/\/www.reuters.com\/article\/2015\/09\/14\/us-singapore-election-succession-idUSKCN0RE2AK20150914",
      "display_url" : "reuters.com\/article\/2015\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643555996783243264",
  "text" : "The idea is not outlandish to me that Mr Tharman as Singapore next PM. http:\/\/t.co\/J7bGmCJEri",
  "id" : 643555996783243264,
  "created_at" : "2015-09-14 22:44:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laura Stewart",
      "screen_name" : "ledavies",
      "indices" : [ 3, 12 ],
      "id_str" : "22996085",
      "id" : 22996085
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643522798258913281",
  "text" : "RT @ledavies: I don't like the premise of Girls Can Code. Taking women with no interest in the industry &amp; using them to prove a whole gende\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643515687655370752",
    "text" : "I don't like the premise of Girls Can Code. Taking women with no interest in the industry &amp; using them to prove a whole gender is of value?",
    "id" : 643515687655370752,
    "created_at" : "2015-09-14 20:04:30 +0000",
    "user" : {
      "name" : "Laura Stewart",
      "screen_name" : "ledavies",
      "protected" : false,
      "id_str" : "22996085",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/753540471285243904\/IkYdy7XP_normal.jpg",
      "id" : 22996085,
      "verified" : false
    }
  },
  "id" : 643522798258913281,
  "created_at" : "2015-09-14 20:32:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eileen Burbidge",
      "screen_name" : "eileentso",
      "indices" : [ 3, 13 ],
      "id_str" : "22301442",
      "id" : 22301442
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643522271227813889",
  "text" : "RT @eileentso: trailer for this eve's Girls Can Code (BBC3 at 9PM), and no comment as to who made Ellie cry (I felt abs awful!) https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/TFZBQvNrX5",
        "expanded_url" : "https:\/\/www.youtube.com\/watch?v=F0fzGBclsKs",
        "display_url" : "youtube.com\/watch?v=F0fzGB\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643466593016631296",
    "text" : "trailer for this eve's Girls Can Code (BBC3 at 9PM), and no comment as to who made Ellie cry (I felt abs awful!) https:\/\/t.co\/TFZBQvNrX5",
    "id" : 643466593016631296,
    "created_at" : "2015-09-14 16:49:25 +0000",
    "user" : {
      "name" : "Eileen Burbidge",
      "screen_name" : "eileentso",
      "protected" : false,
      "id_str" : "22301442",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/854459135429156864\/Amr8qM7b_normal.jpg",
      "id" : 22301442,
      "verified" : false
    }
  },
  "id" : 643522271227813889,
  "created_at" : "2015-09-14 20:30:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RAF Careers Lincoln",
      "screen_name" : "AfcoLincoln",
      "indices" : [ 3, 15 ],
      "id_str" : "2815025099",
      "id" : 2815025099
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643521565288714240",
  "text" : "RT @AfcoLincoln: BBC3 Girls Can Code-damn right they can!One of AFCO girls worked on the AWACS software team coding for 5 years #thisgirlca\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "GIRL GEEKS \u00AE",
        "screen_name" : "girl_geeks",
        "indices" : [ 124, 135 ],
        "id_str" : "40544415",
        "id" : 40544415
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "thisgirlcan",
        "indices" : [ 111, 123 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643515811735465984",
    "text" : "BBC3 Girls Can Code-damn right they can!One of AFCO girls worked on the AWACS software team coding for 5 years #thisgirlcan @girl_geeks",
    "id" : 643515811735465984,
    "created_at" : "2015-09-14 20:05:00 +0000",
    "user" : {
      "name" : "RAF Careers Lincoln",
      "screen_name" : "AfcoLincoln",
      "protected" : false,
      "id_str" : "2815025099",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973162114759184384\/CQBe7nSF_normal.jpg",
      "id" : 2815025099,
      "verified" : false
    }
  },
  "id" : 643521565288714240,
  "created_at" : "2015-09-14 20:27:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643483161897340928",
  "text" : "When an oriental name is mentioned, all the kids in the library look towards our direction...",
  "id" : 643483161897340928,
  "created_at" : "2015-09-14 17:55:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/643482647164002304\/photo\/1",
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/QVbf3jmEBe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CO4cDt3WEAAuuTM.jpg",
      "id_str" : "643482646937473024",
      "id" : 643482646937473024,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO4cDt3WEAAuuTM.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QVbf3jmEBe"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 63 ],
      "url" : "http:\/\/t.co\/UrBqi353YL",
      "expanded_url" : "http:\/\/ift.tt\/1M7cSf0",
      "display_url" : "ift.tt\/1M7cSf0"
    } ]
  },
  "geo" : { },
  "id_str" : "643482647164002304",
  "text" : "At a local library for an award ceremony http:\/\/t.co\/UrBqi353YL http:\/\/t.co\/QVbf3jmEBe",
  "id" : 643482647164002304,
  "created_at" : "2015-09-14 17:53:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/j49q8jq1wR",
      "expanded_url" : "https:\/\/instagram.com\/p\/7nr4OuBZPz\/",
      "display_url" : "instagram.com\/p\/7nr4OuBZPz\/"
    } ]
  },
  "geo" : { },
  "id_str" : "643482614402281473",
  "text" : "At a local library for an award ceremony https:\/\/t.co\/j49q8jq1wR",
  "id" : 643482614402281473,
  "created_at" : "2015-09-14 17:53:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fiona Shields",
      "screen_name" : "fshields",
      "indices" : [ 3, 12 ],
      "id_str" : "41328650",
      "id" : 41328650
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/9XpCQzrAv8",
      "expanded_url" : "http:\/\/www.theguardian.com\/environment\/gallery\/2015\/sep\/14\/the-british-wildlife-photography-awards-2015-winners-in-pictures",
      "display_url" : "theguardian.com\/environment\/ga\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643479899915821056",
  "text" : "RT @fshields: Winners of the British Wildlife Photography Awards, so gorgeous I've tweeted twice! http:\/\/t.co\/9XpCQzrAv8 http:\/\/t.co\/1aF7CZ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/fshields\/status\/643476794046324736\/photo\/1",
        "indices" : [ 107, 129 ],
        "url" : "http:\/\/t.co\/1aF7CZyByb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO4WuskWgAAjFJv.jpg",
        "id_str" : "643476788253982720",
        "id" : 643476788253982720,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO4WuskWgAAjFJv.jpg",
        "sizes" : [ {
          "h" : 467,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 467,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 467,
          "resize" : "fit",
          "w" : 700
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1aF7CZyByb"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 106 ],
        "url" : "http:\/\/t.co\/9XpCQzrAv8",
        "expanded_url" : "http:\/\/www.theguardian.com\/environment\/gallery\/2015\/sep\/14\/the-british-wildlife-photography-awards-2015-winners-in-pictures",
        "display_url" : "theguardian.com\/environment\/ga\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643476794046324736",
    "text" : "Winners of the British Wildlife Photography Awards, so gorgeous I've tweeted twice! http:\/\/t.co\/9XpCQzrAv8 http:\/\/t.co\/1aF7CZyByb",
    "id" : 643476794046324736,
    "created_at" : "2015-09-14 17:29:57 +0000",
    "user" : {
      "name" : "Fiona Shields",
      "screen_name" : "fshields",
      "protected" : false,
      "id_str" : "41328650",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/949380291994554368\/1TpCfvBW_normal.jpg",
      "id" : 41328650,
      "verified" : true
    }
  },
  "id" : 643479899915821056,
  "created_at" : "2015-09-14 17:42:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643458598731956224",
  "text" : "Fifth PM in eight years. In other country, it consider political unstable....",
  "id" : 643458598731956224,
  "created_at" : "2015-09-14 16:17:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643454286404390912",
  "text" : "3 down and 1 more to go. Finger cross on the outcome by this week.",
  "id" : 643454286404390912,
  "created_at" : "2015-09-14 16:00:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LinkedInPowerProfiles",
      "indices" : [ 17, 39 ]
    } ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/IxuMpNpPQn",
      "expanded_url" : "https:\/\/lists.linkedin.com\/power-profiles\/sg\/industry\/finance",
      "display_url" : "lists.linkedin.com\/power-profiles\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643453943566217216",
  "text" : "See who made the #LinkedInPowerProfiles Finance list for 2015 in Singapore. https:\/\/t.co\/IxuMpNpPQn",
  "id" : 643453943566217216,
  "created_at" : "2015-09-14 15:59:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "indices" : [ 3, 11 ],
      "id_str" : "26425084",
      "id" : 26425084
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643453631765839873",
  "text" : "RT @oiwoods: Took me a while to learn this too: programmatic is often significantly more expensive than an ordinary ad buy https:\/\/t.co\/Vil\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/Vil5qW4kFF",
        "expanded_url" : "https:\/\/twitter.com\/chiefmartec\/status\/643453126951989248",
        "display_url" : "twitter.com\/chiefmartec\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643453383647453184",
    "text" : "Took me a while to learn this too: programmatic is often significantly more expensive than an ordinary ad buy https:\/\/t.co\/Vil5qW4kFF",
    "id" : 643453383647453184,
    "created_at" : "2015-09-14 15:56:56 +0000",
    "user" : {
      "name" : "Oli",
      "screen_name" : "oiwoods",
      "protected" : false,
      "id_str" : "26425084",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/914900128707764225\/Vmx_JkpE_normal.jpg",
      "id" : 26425084,
      "verified" : false
    }
  },
  "id" : 643453631765839873,
  "created_at" : "2015-09-14 15:57:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sumisha Naidu",
      "screen_name" : "sumishanaidu",
      "indices" : [ 3, 16 ],
      "id_str" : "709414788405665793",
      "id" : 709414788405665793
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "haze",
      "indices" : [ 115, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643453385782505473",
  "text" : "RT @sumishanaidu: Schools in Selangor, Wilayah Persekutuan, Putrajaya, Negeri Sembilan, Melaka to be closed due to #haze  https:\/\/t.co\/5PGN\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "haze",
        "indices" : [ 97, 102 ]
      } ],
      "urls" : [ {
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/5PGNdMlkHE",
        "expanded_url" : "https:\/\/twitter.com\/pkamalanathan\/status\/643447723274825728",
        "display_url" : "twitter.com\/pkamalanathan\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643452790501605376",
    "text" : "Schools in Selangor, Wilayah Persekutuan, Putrajaya, Negeri Sembilan, Melaka to be closed due to #haze  https:\/\/t.co\/5PGNdMlkHE",
    "id" : 643452790501605376,
    "created_at" : "2015-09-14 15:54:34 +0000",
    "user" : {
      "name" : "Sumisha Naidu",
      "screen_name" : "SumishaCNA",
      "protected" : false,
      "id_str" : "20583139",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966947225510207488\/nqJPLLmP_normal.jpg",
      "id" : 20583139,
      "verified" : true
    }
  },
  "id" : 643453385782505473,
  "created_at" : "2015-09-14 15:56:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yue Jie",
      "screen_name" : "yuejie1994",
      "indices" : [ 3, 14 ],
      "id_str" : "20223368",
      "id" : 20223368
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sghaze",
      "indices" : [ 118, 125 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643438624789393408",
  "text" : "RT @yuejie1994: So I bought this 1 - 2 years back when it was also hazy af. Think I should wear it again. Yay or nay? #sghaze http:\/\/t.co\/m\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/yuejie1994\/status\/643427586186440704\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/mWGAGYHRwc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3p7XUUAAAZiTR.jpg",
        "id_str" : "643427527864614912",
        "id" : 643427527864614912,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3p7XUUAAAZiTR.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mWGAGYHRwc"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/yuejie1994\/status\/643427586186440704\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/mWGAGYHRwc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3p8b2UsAAXjjs.jpg",
        "id_str" : "643427546260877312",
        "id" : 643427546260877312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3p8b2UsAAXjjs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mWGAGYHRwc"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/yuejie1994\/status\/643427586186440704\/photo\/1",
        "indices" : [ 110, 132 ],
        "url" : "http:\/\/t.co\/mWGAGYHRwc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3p903UsAASwHe.jpg",
        "id_str" : "643427570155827200",
        "id" : 643427570155827200,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3p903UsAASwHe.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mWGAGYHRwc"
      } ],
      "hashtags" : [ {
        "text" : "sghaze",
        "indices" : [ 102, 109 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643427586186440704",
    "text" : "So I bought this 1 - 2 years back when it was also hazy af. Think I should wear it again. Yay or nay? #sghaze http:\/\/t.co\/mWGAGYHRwc",
    "id" : 643427586186440704,
    "created_at" : "2015-09-14 14:14:25 +0000",
    "user" : {
      "name" : "Yue Jie",
      "screen_name" : "yuejie1994",
      "protected" : false,
      "id_str" : "20223368",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1441997730\/DSC_0110_normal.jpg",
      "id" : 20223368,
      "verified" : false
    }
  },
  "id" : 643438624789393408,
  "created_at" : "2015-09-14 14:58:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643437043570339844",
  "text" : "Some fee-paying secondary school in Ireland is processing application for 2026. Wow seems when parents here so \"kiasu\"...",
  "id" : 643437043570339844,
  "created_at" : "2015-09-14 14:52:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ant O'Fearghail",
      "screen_name" : "aofarre",
      "indices" : [ 3, 11 ],
      "id_str" : "139702052",
      "id" : 139702052
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643426946664275968",
  "text" : "RT @aofarre: Americans, when I ask you what's to do in your city, you pointing me to the Irish pub is like me pointing you to McDonalds in \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643426150333685760",
    "text" : "Americans, when I ask you what's to do in your city, you pointing me to the Irish pub is like me pointing you to McDonalds in Dublin.",
    "id" : 643426150333685760,
    "created_at" : "2015-09-14 14:08:43 +0000",
    "user" : {
      "name" : "Ant O'Fearghail",
      "screen_name" : "aofarre",
      "protected" : false,
      "id_str" : "139702052",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025583227443007488\/OYjDXGuU_normal.jpg",
      "id" : 139702052,
      "verified" : false
    }
  },
  "id" : 643426946664275968,
  "created_at" : "2015-09-14 14:11:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HSE Ireland",
      "screen_name" : "HSELive",
      "indices" : [ 0, 8 ],
      "id_str" : "69298376",
      "id" : 69298376
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/HXbiq9m7DO",
      "expanded_url" : "https:\/\/twitter.com\/nytimes\/status\/643325557992726528",
      "display_url" : "twitter.com\/nytimes\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643426539913236480",
  "in_reply_to_user_id" : 69298376,
  "text" : "@HSElive should make available these figures.  https:\/\/t.co\/HXbiq9m7DO",
  "id" : 643426539913236480,
  "created_at" : "2015-09-14 14:10:16 +0000",
  "in_reply_to_screen_name" : "HSELive",
  "in_reply_to_user_id_str" : "69298376",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643424861696749568",
  "text" : "Hearsay: Some family friend got bullied in secondary school and no action taken. Me:  Anti-Bully policy paper is good for the bird cage.",
  "id" : 643424861696749568,
  "created_at" : "2015-09-14 14:03:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sghaze",
      "indices" : [ 47, 54 ]
    } ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/pcn6UtIe0B",
      "expanded_url" : "https:\/\/webreathewhatwebuy.com\/index.php\/haze\/",
      "display_url" : "webreathewhatwebuy.com\/index.php\/haze\/"
    } ]
  },
  "geo" : { },
  "id_str" : "643415739857285120",
  "text" : "We breath what we buy. https:\/\/t.co\/pcn6UtIe0B #sghaze",
  "id" : 643415739857285120,
  "created_at" : "2015-09-14 13:27:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/643414213650743296\/photo\/1",
      "indices" : [ 66, 88 ],
      "url" : "http:\/\/t.co\/lDCeVBC8Zg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CO3d0XqWIAA-vix.jpg",
      "id_str" : "643414213558411264",
      "id" : 643414213558411264,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO3d0XqWIAA-vix.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lDCeVBC8Zg"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http:\/\/t.co\/Cagos8k8da",
      "expanded_url" : "http:\/\/ift.tt\/1LtzuUq",
      "display_url" : "ift.tt\/1LtzuUq"
    } ]
  },
  "geo" : { },
  "id_str" : "643414213650743296",
  "text" : "Mushroom Farfalle (\"butterfly \") for lunch http:\/\/t.co\/Cagos8k8da http:\/\/t.co\/lDCeVBC8Zg",
  "id" : 643414213650743296,
  "created_at" : "2015-09-14 13:21:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/PRTnr1EzEa",
      "expanded_url" : "https:\/\/instagram.com\/p\/7nMw8JBZEc\/",
      "display_url" : "instagram.com\/p\/7nMw8JBZEc\/"
    } ]
  },
  "geo" : { },
  "id_str" : "643414194172391424",
  "text" : "Mushroom Farfalle (\"butterfly \") for lunch https:\/\/t.co\/PRTnr1EzEa",
  "id" : 643414194172391424,
  "created_at" : "2015-09-14 13:21:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/HIQjOMA2qf",
      "expanded_url" : "http:\/\/ow.ly\/SblvQ",
      "display_url" : "ow.ly\/SblvQ"
    } ]
  },
  "geo" : { },
  "id_str" : "643408312466305024",
  "text" : "RT @orangemanta: Irish Social Entrepreneurs! Here is your chance to speak at the Web Summit http:\/\/t.co\/HIQjOMA2qf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 97 ],
        "url" : "http:\/\/t.co\/HIQjOMA2qf",
        "expanded_url" : "http:\/\/ow.ly\/SblvQ",
        "display_url" : "ow.ly\/SblvQ"
      } ]
    },
    "geo" : { },
    "id_str" : "643407210937192449",
    "text" : "Irish Social Entrepreneurs! Here is your chance to speak at the Web Summit http:\/\/t.co\/HIQjOMA2qf",
    "id" : 643407210937192449,
    "created_at" : "2015-09-14 12:53:27 +0000",
    "user" : {
      "name" : "Ron Immink",
      "screen_name" : "ronimmink",
      "protected" : false,
      "id_str" : "14627081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/926388478627254272\/nei8n_Rr_normal.jpg",
      "id" : 14627081,
      "verified" : false
    }
  },
  "id" : 643408312466305024,
  "created_at" : "2015-09-14 12:57:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Forget boys, get PAID!",
      "screen_name" : "blackadlerqueen",
      "indices" : [ 3, 19 ],
      "id_str" : "15689909",
      "id" : 15689909
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/blackadlerqueen\/status\/643372891333984256\/photo\/1",
      "indices" : [ 72, 94 ],
      "url" : "http:\/\/t.co\/nlwKrDFc27",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CO24O0AU8AAUHJb.jpg",
      "id_str" : "643372886401544192",
      "id" : 643372886401544192,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO24O0AU8AAUHJb.jpg",
      "sizes" : [ {
        "h" : 3264,
        "resize" : "fit",
        "w" : 1836
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nlwKrDFc27"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643373023152721920",
  "text" : "RT @blackadlerqueen: This, E.L James, is the true Fifty Shades of Grey. http:\/\/t.co\/nlwKrDFc27",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for BlackBerry\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/blackadlerqueen\/status\/643372891333984256\/photo\/1",
        "indices" : [ 51, 73 ],
        "url" : "http:\/\/t.co\/nlwKrDFc27",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO24O0AU8AAUHJb.jpg",
        "id_str" : "643372886401544192",
        "id" : 643372886401544192,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO24O0AU8AAUHJb.jpg",
        "sizes" : [ {
          "h" : 3264,
          "resize" : "fit",
          "w" : 1836
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nlwKrDFc27"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643372891333984256",
    "text" : "This, E.L James, is the true Fifty Shades of Grey. http:\/\/t.co\/nlwKrDFc27",
    "id" : 643372891333984256,
    "created_at" : "2015-09-14 10:37:05 +0000",
    "user" : {
      "name" : "Forget boys, get PAID!",
      "screen_name" : "blackadlerqueen",
      "protected" : false,
      "id_str" : "15689909",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038620037068648448\/TPug7Sl2_normal.jpg",
      "id" : 15689909,
      "verified" : false
    }
  },
  "id" : 643373023152721920,
  "created_at" : "2015-09-14 10:37:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "indices" : [ 3, 15 ],
      "id_str" : "15191926",
      "id" : 15191926
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 131 ],
      "url" : "http:\/\/t.co\/3jYpq7dIty",
      "expanded_url" : "http:\/\/ift.tt\/1EXtE0B",
      "display_url" : "ift.tt\/1EXtE0B"
    } ]
  },
  "geo" : { },
  "id_str" : "643372390790090752",
  "text" : "RT @clementchio: Gotta admit, it's quite a beautiful sight looking at this -m\u0336i\u0336s\u0336t\u0336 \u0336 haze from the air-co\u2026 http:\/\/t.co\/3jYpq7dIty http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/clementchio\/status\/643371438460772352\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/PvK7wUfs15",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CO226hxWcAA0Php.jpg",
        "id_str" : "643371438397878272",
        "id" : 643371438397878272,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CO226hxWcAA0Php.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 819
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 819
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 544
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 819
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PvK7wUfs15"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/3jYpq7dIty",
        "expanded_url" : "http:\/\/ift.tt\/1EXtE0B",
        "display_url" : "ift.tt\/1EXtE0B"
      } ]
    },
    "geo" : { },
    "id_str" : "643371438460772352",
    "text" : "Gotta admit, it's quite a beautiful sight looking at this -m\u0336i\u0336s\u0336t\u0336 \u0336 haze from the air-co\u2026 http:\/\/t.co\/3jYpq7dIty http:\/\/t.co\/PvK7wUfs15",
    "id" : 643371438460772352,
    "created_at" : "2015-09-14 10:31:18 +0000",
    "user" : {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "protected" : false,
      "id_str" : "15191926",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/582481791899971584\/UZf4Qfb2_normal.jpg",
      "id" : 15191926,
      "verified" : false
    }
  },
  "id" : 643372390790090752,
  "created_at" : "2015-09-14 10:35:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643144474684735492",
  "text" : "Learnt that a 2 bedroom in a gated-apartment rent out at EUR1600 in D12!",
  "id" : 643144474684735492,
  "created_at" : "2015-09-13 19:29:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ade Walsh",
      "screen_name" : "AdeWalsh",
      "indices" : [ 0, 9 ],
      "id_str" : "78301422",
      "id" : 78301422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "643089132730847232",
  "geo" : { },
  "id_str" : "643114173958893569",
  "in_reply_to_user_id" : 78301422,
  "text" : "@AdeWalsh That the type of price I think. :)",
  "id" : 643114173958893569,
  "in_reply_to_status_id" : 643089132730847232,
  "created_at" : "2015-09-13 17:29:02 +0000",
  "in_reply_to_screen_name" : "AdeWalsh",
  "in_reply_to_user_id_str" : "78301422",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fintech Singapore",
      "screen_name" : "FintechSIN",
      "indices" : [ 3, 14 ],
      "id_str" : "3141199916",
      "id" : 3141199916
    }, {
      "name" : "fastacash",
      "screen_name" : "fastacash",
      "indices" : [ 56, 66 ],
      "id_str" : "546688076",
      "id" : 546688076
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643078486765268993",
  "text" : "RT @FintechSIN: Singapores Social Media Payment Service @fastacash wants to attack Europe! they are hiring a VP Sales Europe https:\/\/t.co\/m\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "fastacash",
        "screen_name" : "fastacash",
        "indices" : [ 40, 50 ],
        "id_str" : "546688076",
        "id" : 546688076
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/mI4H7RBwRg",
        "expanded_url" : "https:\/\/www.linkedin.com\/jobs2\/view\/84823231?trk=vsrp_jobs_res_name&trkInfo=VSRPsearchId:112032011442109065919,VSRPtargetId:84823231,VSRPcmpt:primary",
        "display_url" : "linkedin.com\/jobs2\/view\/848\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643057342712410112",
    "text" : "Singapores Social Media Payment Service @fastacash wants to attack Europe! they are hiring a VP Sales Europe https:\/\/t.co\/mI4H7RBwRg\u201D",
    "id" : 643057342712410112,
    "created_at" : "2015-09-13 13:43:12 +0000",
    "user" : {
      "name" : "Fintech Singapore",
      "screen_name" : "FintechSIN",
      "protected" : false,
      "id_str" : "3141199916",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024155504770043905\/e8hnyyfn_normal.jpg",
      "id" : 3141199916,
      "verified" : false
    }
  },
  "id" : 643078486765268993,
  "created_at" : "2015-09-13 15:07:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dara \u00D3 Briain",
      "screen_name" : "daraobriain",
      "indices" : [ 3, 15 ],
      "id_str" : "44874400",
      "id" : 44874400
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643042766600183808",
  "text" : "RT @daraobriain: Had a lovely weekend in Dublin, but have urgently returned to London as I'm told my family are in danger. https:\/\/t.co\/HAI\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/HAIpCoHycH",
        "expanded_url" : "https:\/\/twitter.com\/david_cameron\/status\/642984909980725248",
        "display_url" : "twitter.com\/david_cameron\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "643026233761091584",
    "text" : "Had a lovely weekend in Dublin, but have urgently returned to London as I'm told my family are in danger. https:\/\/t.co\/HAIpCoHycH",
    "id" : 643026233761091584,
    "created_at" : "2015-09-13 11:39:35 +0000",
    "user" : {
      "name" : "Dara \u00D3 Briain",
      "screen_name" : "daraobriain",
      "protected" : false,
      "id_str" : "44874400",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/251011476\/dobrian08_normal.jpg",
      "id" : 44874400,
      "verified" : false
    }
  },
  "id" : 643042766600183808,
  "created_at" : "2015-09-13 12:45:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Muhammad Lila",
      "screen_name" : "MuhammadLila",
      "indices" : [ 3, 16 ],
      "id_str" : "26059158",
      "id" : 26059158
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643042189686272000",
  "text" : "RT @MuhammadLila: Remember the refugee father\/son who were kicked, tripped by Hungarian camerawoman? They've made it safely to Germany http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MuhammadLila\/status\/643022419431657472\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/sBxV6lotsm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COx5dz8UYAA3AjG.jpg",
        "id_str" : "643022399873507328",
        "id" : 643022399873507328,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COx5dz8UYAA3AjG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sBxV6lotsm"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MuhammadLila\/status\/643022419431657472\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/sBxV6lotsm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COx5d0rUsAAKTkE.jpg",
        "id_str" : "643022400070660096",
        "id" : 643022400070660096,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COx5d0rUsAAKTkE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 571,
          "resize" : "fit",
          "w" : 762
        }, {
          "h" : 571,
          "resize" : "fit",
          "w" : 762
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 571,
          "resize" : "fit",
          "w" : 762
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sBxV6lotsm"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MuhammadLila\/status\/643022419431657472\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/sBxV6lotsm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COx5d2CVAAAv5-V.jpg",
        "id_str" : "643022400435585024",
        "id" : 643022400435585024,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COx5d2CVAAAv5-V.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 599
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 599
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sBxV6lotsm"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643022419431657472",
    "text" : "Remember the refugee father\/son who were kicked, tripped by Hungarian camerawoman? They've made it safely to Germany http:\/\/t.co\/sBxV6lotsm",
    "id" : 643022419431657472,
    "created_at" : "2015-09-13 11:24:26 +0000",
    "user" : {
      "name" : "Muhammad Lila",
      "screen_name" : "MuhammadLila",
      "protected" : false,
      "id_str" : "26059158",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947225943424229376\/1ZvI4k4h_normal.jpg",
      "id" : 26059158,
      "verified" : true
    }
  },
  "id" : 643042189686272000,
  "created_at" : "2015-09-13 12:42:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ade Walsh",
      "screen_name" : "AdeWalsh",
      "indices" : [ 0, 9 ],
      "id_str" : "78301422",
      "id" : 78301422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "643035560131624961",
  "geo" : { },
  "id_str" : "643040264458760192",
  "in_reply_to_user_id" : 78301422,
  "text" : "@AdeWalsh sadap? How much it cost in Ireland?",
  "id" : 643040264458760192,
  "in_reply_to_status_id" : 643035560131624961,
  "created_at" : "2015-09-13 12:35:20 +0000",
  "in_reply_to_screen_name" : "AdeWalsh",
  "in_reply_to_user_id_str" : "78301422",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/h7yZadIbcA",
      "expanded_url" : "https:\/\/twitter.com\/thejournal_ie\/status\/643016283605172224",
      "display_url" : "twitter.com\/thejournal_ie\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "643025227853758464",
  "text" : "Waiting for the right price to sell it to fund social community project is one thing I can think of.  https:\/\/t.co\/h7yZadIbcA",
  "id" : 643025227853758464,
  "created_at" : "2015-09-13 11:35:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason",
      "screen_name" : "NickMotown",
      "indices" : [ 3, 14 ],
      "id_str" : "101012795",
      "id" : 101012795
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643022955295961088",
  "text" : "RT @NickMotown: I have a friend who says her calves look really sexy in high heels.\n\nI have given the RSPCA directions to her farm.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "643020656964206593",
    "text" : "I have a friend who says her calves look really sexy in high heels.\n\nI have given the RSPCA directions to her farm.",
    "id" : 643020656964206593,
    "created_at" : "2015-09-13 11:17:26 +0000",
    "user" : {
      "name" : "Jason",
      "screen_name" : "NickMotown",
      "protected" : false,
      "id_str" : "101012795",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1036217068050362369\/OnFmT4WF_normal.jpg",
      "id" : 101012795,
      "verified" : false
    }
  },
  "id" : 643022955295961088,
  "created_at" : "2015-09-13 11:26:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/L02xNEpiRB",
      "expanded_url" : "https:\/\/instagram.com\/p\/7j0UoPnReD\/",
      "display_url" : "instagram.com\/p\/7j0UoPnReD\/"
    } ]
  },
  "geo" : { },
  "id_str" : "643022728337997824",
  "text" : "The type of breakfast I miss from home on a weekend. https:\/\/t.co\/L02xNEpiRB",
  "id" : 643022728337997824,
  "created_at" : "2015-09-13 11:25:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "50 Nerds of Grey",
      "screen_name" : "50NerdsofGrey",
      "indices" : [ 3, 17 ],
      "id_str" : "600029236",
      "id" : 600029236
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "643019727321829376",
  "text" : "RT @50NerdsofGrey: 'How do you feel about wearing black leather?' he asked.\n'Ok,' she said.\n'Good,' he smiled, 'Then you can be Darth Vader\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640285048579735552",
    "text" : "'How do you feel about wearing black leather?' he asked.\n'Ok,' she said.\n'Good,' he smiled, 'Then you can be Darth Vader and I'll be R2-D2.'",
    "id" : 640285048579735552,
    "created_at" : "2015-09-05 22:07:06 +0000",
    "user" : {
      "name" : "50 Nerds of Grey",
      "screen_name" : "50NerdsofGrey",
      "protected" : false,
      "id_str" : "600029236",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/743751026784632832\/iudv-RAE_normal.jpg",
      "id" : 600029236,
      "verified" : false
    }
  },
  "id" : 643019727321829376,
  "created_at" : "2015-09-13 11:13:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 98 ],
      "url" : "http:\/\/t.co\/A4eNDBLKnR",
      "expanded_url" : "http:\/\/ti.me\/1KKlWJD",
      "display_url" : "ti.me\/1KKlWJD"
    } ]
  },
  "geo" : { },
  "id_str" : "643014028311285760",
  "text" : "Artist creates 'Syrian Super Mario' to show the hardship of being a refugee http:\/\/t.co\/A4eNDBLKnR",
  "id" : 643014028311285760,
  "created_at" : "2015-09-13 10:51:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TED Talks",
      "screen_name" : "TEDTalks",
      "indices" : [ 3, 12 ],
      "id_str" : "15492359",
      "id" : 15492359
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TEDTalks\/status\/642786683738554368\/photo\/1",
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/j38sHLeAp5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COujFSfUAAAd9G7.png",
      "id_str" : "642786683088273408",
      "id" : 642786683088273408,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COujFSfUAAAd9G7.png",
      "sizes" : [ {
        "h" : 413,
        "resize" : "fit",
        "w" : 707
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 707
      }, {
        "h" : 397,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 413,
        "resize" : "fit",
        "w" : 707
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/j38sHLeAp5"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 110 ],
      "url" : "http:\/\/t.co\/MXsjofmwb9",
      "expanded_url" : "http:\/\/t.ted.com\/pQFJ0r8",
      "display_url" : "t.ted.com\/pQFJ0r8"
    } ]
  },
  "geo" : { },
  "id_str" : "642844259754844160",
  "text" : "RT @TEDTalks: Have six minutes to spare? Plenty of time to learn 40 Chinese characters: http:\/\/t.co\/MXsjofmwb9 http:\/\/t.co\/j38sHLeAp5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TEDTalks\/status\/642786683738554368\/photo\/1",
        "indices" : [ 97, 119 ],
        "url" : "http:\/\/t.co\/j38sHLeAp5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COujFSfUAAAd9G7.png",
        "id_str" : "642786683088273408",
        "id" : 642786683088273408,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COujFSfUAAAd9G7.png",
        "sizes" : [ {
          "h" : 413,
          "resize" : "fit",
          "w" : 707
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 707
        }, {
          "h" : 397,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 413,
          "resize" : "fit",
          "w" : 707
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/j38sHLeAp5"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 96 ],
        "url" : "http:\/\/t.co\/MXsjofmwb9",
        "expanded_url" : "http:\/\/t.ted.com\/pQFJ0r8",
        "display_url" : "t.ted.com\/pQFJ0r8"
      } ]
    },
    "geo" : { },
    "id_str" : "642786683738554368",
    "text" : "Have six minutes to spare? Plenty of time to learn 40 Chinese characters: http:\/\/t.co\/MXsjofmwb9 http:\/\/t.co\/j38sHLeAp5",
    "id" : 642786683738554368,
    "created_at" : "2015-09-12 19:47:42 +0000",
    "user" : {
      "name" : "TED Talks",
      "screen_name" : "TEDTalks",
      "protected" : false,
      "id_str" : "15492359",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877631054525472768\/Xp5FAPD5_normal.jpg",
      "id" : 15492359,
      "verified" : true
    }
  },
  "id" : 642844259754844160,
  "created_at" : "2015-09-12 23:36:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "Rachel Allen",
      "screen_name" : "rachelallen1",
      "indices" : [ 32, 45 ],
      "id_str" : "134608396",
      "id" : 134608396
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/642615308625166337\/photo\/1",
      "indices" : [ 98, 120 ],
      "url" : "http:\/\/t.co\/dJUbZrKzFP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COsHM72VEAADN_a.jpg",
      "id_str" : "642615290635816960",
      "id" : 642615290635816960,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COsHM72VEAADN_a.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dJUbZrKzFP"
    } ],
    "hashtags" : [ {
      "text" : "AllThingsSweet",
      "indices" : [ 67, 82 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642830452936548352",
  "text" : "RT @GeoffreyIRL: Lovely to meet @rachelallen1 in Singapore signing #AllThingsSweet. Safe travels! http:\/\/t.co\/dJUbZrKzFP",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rachel Allen",
        "screen_name" : "rachelallen1",
        "indices" : [ 15, 28 ],
        "id_str" : "134608396",
        "id" : 134608396
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/642615308625166337\/photo\/1",
        "indices" : [ 81, 103 ],
        "url" : "http:\/\/t.co\/dJUbZrKzFP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COsHM72VEAADN_a.jpg",
        "id_str" : "642615290635816960",
        "id" : 642615290635816960,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COsHM72VEAADN_a.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dJUbZrKzFP"
      } ],
      "hashtags" : [ {
        "text" : "AllThingsSweet",
        "indices" : [ 50, 65 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642615308625166337",
    "text" : "Lovely to meet @rachelallen1 in Singapore signing #AllThingsSweet. Safe travels! http:\/\/t.co\/dJUbZrKzFP",
    "id" : 642615308625166337,
    "created_at" : "2015-09-12 08:26:43 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 642830452936548352,
  "created_at" : "2015-09-12 22:41:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Arthur Charpentier \uD83D\uDD87",
      "screen_name" : "freakonometrics",
      "indices" : [ 3, 19 ],
      "id_str" : "105530526",
      "id" : 105530526
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 71 ],
      "url" : "http:\/\/t.co\/tqLymdc9gM",
      "expanded_url" : "http:\/\/www.business2community.com\/business-intelligence\/the-problems-with-analytics-01319746?utm_content=bufferce69c&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer",
      "display_url" : "business2community.com\/business-intel\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642830165727387648",
  "text" : "RT @freakonometrics: Problem with data analytics http:\/\/t.co\/tqLymdc9gM\n1. The data is wrong\n2. You\u2019re using the wrong metrics\n3. You\u2019re mi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 28, 50 ],
        "url" : "http:\/\/t.co\/tqLymdc9gM",
        "expanded_url" : "http:\/\/www.business2community.com\/business-intelligence\/the-problems-with-analytics-01319746?utm_content=bufferce69c&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer",
        "display_url" : "business2community.com\/business-intel\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "642828530112462848",
    "text" : "Problem with data analytics http:\/\/t.co\/tqLymdc9gM\n1. The data is wrong\n2. You\u2019re using the wrong metrics\n3. You\u2019re missing the big picture",
    "id" : 642828530112462848,
    "created_at" : "2015-09-12 22:33:59 +0000",
    "user" : {
      "name" : "Arthur Charpentier \uD83D\uDD87",
      "screen_name" : "freakonometrics",
      "protected" : false,
      "id_str" : "105530526",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947792239501160448\/HUFZgVrV_normal.jpg",
      "id" : 105530526,
      "verified" : true
    }
  },
  "id" : 642830165727387648,
  "created_at" : "2015-09-12 22:40:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ari Kopoulos",
      "screen_name" : "aricochet",
      "indices" : [ 3, 13 ],
      "id_str" : "21166399",
      "id" : 21166399
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642811939412930560",
  "text" : "RT @aricochet: A 21st-Century Migrant\u2019s Essentials: Food, Shelter, Smart phone...or how tech is disrupting the traffickers! http:\/\/t.co\/yCz\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 109, 131 ],
        "url" : "http:\/\/t.co\/yCzTpe4wYn",
        "expanded_url" : "http:\/\/nyti.ms\/1NzvSpz",
        "display_url" : "nyti.ms\/1NzvSpz"
      } ]
    },
    "geo" : { },
    "id_str" : "642810758712049665",
    "text" : "A 21st-Century Migrant\u2019s Essentials: Food, Shelter, Smart phone...or how tech is disrupting the traffickers! http:\/\/t.co\/yCzTpe4wYn",
    "id" : 642810758712049665,
    "created_at" : "2015-09-12 21:23:22 +0000",
    "user" : {
      "name" : "Ari Kopoulos",
      "screen_name" : "aricochet",
      "protected" : false,
      "id_str" : "21166399",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/690048969997697025\/-DH3tJvK_normal.jpg",
      "id" : 21166399,
      "verified" : false
    }
  },
  "id" : 642811939412930560,
  "created_at" : "2015-09-12 21:28:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642811068297949184",
  "text" : "Music education is compulsory in Brazil - Marin Alsop",
  "id" : 642811068297949184,
  "created_at" : "2015-09-12 21:24:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Miriam Lee",
      "screen_name" : "MiriamLee92",
      "indices" : [ 3, 15 ],
      "id_str" : "39268295",
      "id" : 39268295
    }, {
      "name" : "Riverdance",
      "screen_name" : "Riverdance",
      "indices" : [ 69, 80 ],
      "id_str" : "46386100",
      "id" : 46386100
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MiriamLee92\/status\/642804130407387136\/photo\/1",
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/2LcLN4DNPn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COuy72WWoAATGn0.jpg",
      "id_str" : "642804113101725696",
      "id" : 642804113101725696,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COuy72WWoAATGn0.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2LcLN4DNPn"
    } ],
    "hashtags" : [ {
      "text" : "bbcproms2015",
      "indices" : [ 81, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642810940552048641",
  "text" : "RT @MiriamLee92: The ladies all ready for the BBC Proms in the Park! @Riverdance #bbcproms2015 http:\/\/t.co\/2LcLN4DNPn",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Riverdance",
        "screen_name" : "Riverdance",
        "indices" : [ 52, 63 ],
        "id_str" : "46386100",
        "id" : 46386100
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MiriamLee92\/status\/642804130407387136\/photo\/1",
        "indices" : [ 78, 100 ],
        "url" : "http:\/\/t.co\/2LcLN4DNPn",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COuy72WWoAATGn0.jpg",
        "id_str" : "642804113101725696",
        "id" : 642804113101725696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COuy72WWoAATGn0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2LcLN4DNPn"
      } ],
      "hashtags" : [ {
        "text" : "bbcproms2015",
        "indices" : [ 64, 77 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642804130407387136",
    "text" : "The ladies all ready for the BBC Proms in the Park! @Riverdance #bbcproms2015 http:\/\/t.co\/2LcLN4DNPn",
    "id" : 642804130407387136,
    "created_at" : "2015-09-12 20:57:02 +0000",
    "user" : {
      "name" : "Miriam Lee",
      "screen_name" : "MiriamLee92",
      "protected" : false,
      "id_str" : "39268295",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/855471579685367809\/IfKs5L6B_normal.jpg",
      "id" : 39268295,
      "verified" : false
    }
  },
  "id" : 642810940552048641,
  "created_at" : "2015-09-12 21:24:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Verus Racing",
      "screen_name" : "verusracing",
      "indices" : [ 3, 15 ],
      "id_str" : "1923174793",
      "id" : 1923174793
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "F1inSchoolsWF",
      "indices" : [ 119, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642782927089176577",
  "text" : "RT @verusracing: Our car has been cleared for racing by the Engineering judges! We're loving Singapore at the moment!  #F1inSchoolsWF http:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/verusracing\/status\/642691368712671236\/photo\/1",
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/wJOuGGPQmS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COtMYbCVEAASpYb.jpg",
        "id_str" : "642691354288525312",
        "id" : 642691354288525312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COtMYbCVEAASpYb.jpg",
        "sizes" : [ {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wJOuGGPQmS"
      } ],
      "hashtags" : [ {
        "text" : "F1inSchoolsWF",
        "indices" : [ 102, 116 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642691368712671236",
    "text" : "Our car has been cleared for racing by the Engineering judges! We're loving Singapore at the moment!  #F1inSchoolsWF http:\/\/t.co\/wJOuGGPQmS",
    "id" : 642691368712671236,
    "created_at" : "2015-09-12 13:28:57 +0000",
    "user" : {
      "name" : "Verus Racing",
      "screen_name" : "verusracing",
      "protected" : false,
      "id_str" : "1923174793",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/545690045147934721\/g4WR9DqA_normal.png",
      "id" : 1923174793,
      "verified" : false
    }
  },
  "id" : 642782927089176577,
  "created_at" : "2015-09-12 19:32:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/elF5T5kLTT",
      "expanded_url" : "https:\/\/twitter.com\/starduest\/status\/641279697414955008",
      "display_url" : "twitter.com\/starduest\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642773060169388032",
  "text" : "What can Singapore learn from Ireland? https:\/\/t.co\/elF5T5kLTT",
  "id" : 642773060169388032,
  "created_at" : "2015-09-12 18:53:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JL Pagano",
      "screen_name" : "JL_Pagano",
      "indices" : [ 3, 13 ],
      "id_str" : "14613602",
      "id" : 14613602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642758750265344001",
  "text" : "RT @JL_Pagano: 50 cent charge in newsagent to top up a Leap card that will save me less than 50 cent on the bus I'm about to get. Couldn't \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642713936815616000",
    "text" : "50 cent charge in newsagent to top up a Leap card that will save me less than 50 cent on the bus I'm about to get. Couldn't make it up!",
    "id" : 642713936815616000,
    "created_at" : "2015-09-12 14:58:38 +0000",
    "user" : {
      "name" : "JL Pagano",
      "screen_name" : "JL_Pagano",
      "protected" : false,
      "id_str" : "14613602",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1016609086144688129\/rCyaGjgp_normal.jpg",
      "id" : 14613602,
      "verified" : false
    }
  },
  "id" : 642758750265344001,
  "created_at" : "2015-09-12 17:56:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642755417521692672",
  "text" : "Don't understand why people are putting their date of birth on LinkedIn profile.",
  "id" : 642755417521692672,
  "created_at" : "2015-09-12 17:43:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Annie Hoey \uD83E\uDD84",
      "screen_name" : "hoeyannie",
      "indices" : [ 3, 13 ],
      "id_str" : "243362664",
      "id" : 243362664
    }, {
      "name" : "Atheist Ireland",
      "screen_name" : "atheistie",
      "indices" : [ 119, 129 ],
      "id_str" : "54630705",
      "id" : 54630705
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RepealThe8th",
      "indices" : [ 105, 118 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642752727894872064",
  "text" : "RT @hoeyannie: \"Religious people don't have a right to impose their religious beliefs on pregnant women\" #RepealThe8th @atheistie",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Atheist Ireland",
        "screen_name" : "atheistie",
        "indices" : [ 104, 114 ],
        "id_str" : "54630705",
        "id" : 54630705
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RepealThe8th",
        "indices" : [ 90, 103 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642715870695981056",
    "text" : "\"Religious people don't have a right to impose their religious beliefs on pregnant women\" #RepealThe8th @atheistie",
    "id" : 642715870695981056,
    "created_at" : "2015-09-12 15:06:19 +0000",
    "user" : {
      "name" : "Annie Hoey \uD83E\uDD84",
      "screen_name" : "hoeyannie",
      "protected" : false,
      "id_str" : "243362664",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/934309640564887552\/bIX4wLm4_normal.jpg",
      "id" : 243362664,
      "verified" : false
    }
  },
  "id" : 642752727894872064,
  "created_at" : "2015-09-12 17:32:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642750936499286017",
  "text" : "@bharatidalela @Azhreicb \uD83D\uDE04",
  "id" : 642750936499286017,
  "created_at" : "2015-09-12 17:25:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642750873664401408",
  "text" : "\uD83D\uDE04testing",
  "id" : 642750873664401408,
  "created_at" : "2015-09-12 17:25:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642727197778776064",
  "text" : "I no longer use Apple iTunes.",
  "id" : 642727197778776064,
  "created_at" : "2015-09-12 15:51:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ade Walsh",
      "screen_name" : "AdeWalsh",
      "indices" : [ 0, 9 ],
      "id_str" : "78301422",
      "id" : 78301422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "642712646324776960",
  "geo" : { },
  "id_str" : "642720587920576512",
  "in_reply_to_user_id" : 78301422,
  "text" : "@AdeWalsh From your own garden?",
  "id" : 642720587920576512,
  "in_reply_to_status_id" : 642712646324776960,
  "created_at" : "2015-09-12 15:25:03 +0000",
  "in_reply_to_screen_name" : "AdeWalsh",
  "in_reply_to_user_id_str" : "78301422",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/642654750643736576\/photo\/1",
      "indices" : [ 17, 39 ],
      "url" : "http:\/\/t.co\/ZqgqgdWJsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COsqu3lWcAAV_pK.jpg",
      "id_str" : "642654356513386496",
      "id" : 642654356513386496,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COsqu3lWcAAV_pK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZqgqgdWJsH"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/642654750643736576\/photo\/1",
      "indices" : [ 17, 39 ],
      "url" : "http:\/\/t.co\/ZqgqgdWJsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COsqz3GWoAAxAtw.jpg",
      "id_str" : "642654442282721280",
      "id" : 642654442282721280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COsqz3GWoAAxAtw.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZqgqgdWJsH"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/642654750643736576\/photo\/1",
      "indices" : [ 17, 39 ],
      "url" : "http:\/\/t.co\/ZqgqgdWJsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COsq8r3WoAAR7uj.jpg",
      "id_str" : "642654593885839360",
      "id" : 642654593885839360,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COsq8r3WoAAR7uj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZqgqgdWJsH"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/642654750643736576\/photo\/1",
      "indices" : [ 17, 39 ],
      "url" : "http:\/\/t.co\/ZqgqgdWJsH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COsq9H7XAAA81Oo.jpg",
      "id_str" : "642654601418833920",
      "id" : 642654601418833920,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COsq9H7XAAA81Oo.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZqgqgdWJsH"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642654750643736576",
  "text" : "Weekend reading. http:\/\/t.co\/ZqgqgdWJsH",
  "id" : 642654750643736576,
  "created_at" : "2015-09-12 11:03:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 0, 8 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/NO4sgzX8z6",
      "expanded_url" : "https:\/\/twitter.com\/xinguozhi\/status\/642607236066377728",
      "display_url" : "twitter.com\/xinguozhi\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642650760635940865",
  "in_reply_to_user_id" : 574253,
  "text" : "@mrbrown you are being mentioned in this article. https:\/\/t.co\/NO4sgzX8z6",
  "id" : 642650760635940865,
  "created_at" : "2015-09-12 10:47:35 +0000",
  "in_reply_to_screen_name" : "mrbrown",
  "in_reply_to_user_id_str" : "574253",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642467562303946753",
  "text" : "From a FB group, I learn that to advertise a position at irishjobs.ie and jobs.ie it cost (EUR 995+vat) and (EUR 600+vat) respectively!",
  "id" : 642467562303946753,
  "created_at" : "2015-09-11 22:39:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Loreto Bello Gude",
      "screen_name" : "loretobgude",
      "indices" : [ 3, 15 ],
      "id_str" : "77040973",
      "id" : 77040973
    }, {
      "name" : "TravelnKids",
      "screen_name" : "TravelnKids",
      "indices" : [ 84, 96 ],
      "id_str" : "113619900",
      "id" : 113619900
    }, {
      "name" : "Sebastian St. George",
      "screen_name" : "YogaArmy",
      "indices" : [ 97, 106 ],
      "id_str" : "24134009",
      "id" : 24134009
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http:\/\/t.co\/VKpAUlE0HB",
      "expanded_url" : "https:\/\/twitter.com\/archpics\/status\/548218925767737344\/photo\/1",
      "display_url" : "pic.twitter.com\/VKpAUlE0HB"
    } ]
  },
  "geo" : { },
  "id_str" : "642455640925995009",
  "text" : "RT @loretobgude: Entrance to the Saint Petersburg Mosque http:\/\/t.co\/VKpAUlE0HB RT  @TravelnKids @YogaArmy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TravelnKids",
        "screen_name" : "TravelnKids",
        "indices" : [ 67, 79 ],
        "id_str" : "113619900",
        "id" : 113619900
      }, {
        "name" : "Sebastian St. George",
        "screen_name" : "YogaArmy",
        "indices" : [ 80, 89 ],
        "id_str" : "24134009",
        "id" : 24134009
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 40, 62 ],
        "url" : "http:\/\/t.co\/VKpAUlE0HB",
        "expanded_url" : "https:\/\/twitter.com\/archpics\/status\/548218925767737344\/photo\/1",
        "display_url" : "pic.twitter.com\/VKpAUlE0HB"
      } ]
    },
    "geo" : { },
    "id_str" : "642440938149838848",
    "text" : "Entrance to the Saint Petersburg Mosque http:\/\/t.co\/VKpAUlE0HB RT  @TravelnKids @YogaArmy",
    "id" : 642440938149838848,
    "created_at" : "2015-09-11 20:53:50 +0000",
    "user" : {
      "name" : "Loreto Bello Gude",
      "screen_name" : "loretobgude",
      "protected" : false,
      "id_str" : "77040973",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000330286181\/1ede53f9e760259a4a66734219397e9a_normal.jpeg",
      "id" : 77040973,
      "verified" : false
    }
  },
  "id" : 642455640925995009,
  "created_at" : "2015-09-11 21:52:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ivan Mazour",
      "screen_name" : "IvanMazour",
      "indices" : [ 3, 14 ],
      "id_str" : "115016715",
      "id" : 115016715
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "respect",
      "indices" : [ 84, 92 ]
    }, {
      "text" : "hmm",
      "indices" : [ 125, 129 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642454960626647040",
  "text" : "RT @IvanMazour: Oh wow. Girl who plays Amy Farrah Fowler on Big Bang has a real PhD #respect. Although she doesn't like tech #hmm - http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "respect",
        "indices" : [ 68, 76 ]
      }, {
        "text" : "hmm",
        "indices" : [ 109, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/JmNqWcR9P6",
        "expanded_url" : "http:\/\/www.theguardian.com\/tv-and-radio\/2015\/sep\/11\/mayim-bialik-star-big-bang-theory",
        "display_url" : "theguardian.com\/tv-and-radio\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "642438513074552834",
    "text" : "Oh wow. Girl who plays Amy Farrah Fowler on Big Bang has a real PhD #respect. Although she doesn't like tech #hmm - http:\/\/t.co\/JmNqWcR9P6",
    "id" : 642438513074552834,
    "created_at" : "2015-09-11 20:44:12 +0000",
    "user" : {
      "name" : "Ivan Mazour",
      "screen_name" : "IvanMazour",
      "protected" : false,
      "id_str" : "115016715",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000751753157\/608a03e46f1ba6c4a53fc59a2fd8a397_normal.jpeg",
      "id" : 115016715,
      "verified" : false
    }
  },
  "id" : 642454960626647040,
  "created_at" : "2015-09-11 21:49:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "mirgantwelcome",
      "indices" : [ 51, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 67, 89 ],
      "url" : "http:\/\/t.co\/C43kS9eouZ",
      "expanded_url" : "http:\/\/www.straitstimes.com\/singapore\/goal-15000-25000-new-citizens-a-year",
      "display_url" : "straitstimes.com\/singapore\/goal\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642420330808647680",
  "text" : "With this new mandate, 15-25K new citizens a year. #mirgantwelcome http:\/\/t.co\/C43kS9eouZ",
  "id" : 642420330808647680,
  "created_at" : "2015-09-11 19:31:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642399055566475264",
  "text" : "@bharatidalela @Azhreicb good to hear that. All the best. Pics will be good. ;)",
  "id" : 642399055566475264,
  "created_at" : "2015-09-11 18:07:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642367501712625664",
  "text" : "I think \"average Singaporeans\" know what sample count is if not then there is something wrong with our much touted edu system?",
  "id" : 642367501712625664,
  "created_at" : "2015-09-11 16:02:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 93 ],
      "url" : "http:\/\/t.co\/teyTIe25xT",
      "expanded_url" : "http:\/\/www.rachelallen.com\/post\/cake",
      "display_url" : "rachelallen.com\/post\/cake"
    }, {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/ouO2iU9GvJ",
      "expanded_url" : "https:\/\/twitter.com\/BBCKnow_Asia\/status\/642323569867927556",
      "display_url" : "twitter.com\/BBCKnow_Asia\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642357301043687424",
  "text" : "She actually in Singapore! I always use her lemon drizzle cake recipe. http:\/\/t.co\/teyTIe25xT    https:\/\/t.co\/ouO2iU9GvJ",
  "id" : 642357301043687424,
  "created_at" : "2015-09-11 15:21:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/1T4KdTnvPS",
      "expanded_url" : "https:\/\/twitter.com\/markstanley\/status\/642348689009283072",
      "display_url" : "twitter.com\/markstanley\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642353835437092864",
  "text" : "Govt should set aside the \u20AC1.4bn tax revenue for raining day instead of pandering to those interest group. https:\/\/t.co\/1T4KdTnvPS",
  "id" : 642353835437092864,
  "created_at" : "2015-09-11 15:07:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MOE Singapore",
      "screen_name" : "fakeMOE",
      "indices" : [ 3, 11 ],
      "id_str" : "220817090",
      "id" : 220817090
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642352926917267456",
  "text" : "RT @fakeMOE: This year before your PSLE and N, O, A level results release, we will now issue sample results with a +-4% margin of error. \uD83D\uDE0C \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "majulahMOEsg",
        "indices" : [ 126, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642349848243531776",
    "text" : "This year before your PSLE and N, O, A level results release, we will now issue sample results with a +-4% margin of error. \uD83D\uDE0C #majulahMOEsg",
    "id" : 642349848243531776,
    "created_at" : "2015-09-11 14:51:52 +0000",
    "user" : {
      "name" : "MOE Singapore",
      "screen_name" : "fakeMOE",
      "protected" : false,
      "id_str" : "220817090",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1679576694\/MOEparody_normal.png",
      "id" : 220817090,
      "verified" : false
    }
  },
  "id" : 642352926917267456,
  "created_at" : "2015-09-11 15:04:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kirsten Han \u97E9\u4FD0\u9896",
      "screen_name" : "kixes",
      "indices" : [ 3, 9 ],
      "id_str" : "27182288",
      "id" : 27182288
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642348120454643712",
  "text" : "RT @kixes: Sample count meant to reduce speculation. But everyone is speculating about how truly scientific and accurate the sample count i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GE2015",
        "indices" : [ 131, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642339592314601473",
    "text" : "Sample count meant to reduce speculation. But everyone is speculating about how truly scientific and accurate the sample count is. #GE2015",
    "id" : 642339592314601473,
    "created_at" : "2015-09-11 14:11:07 +0000",
    "user" : {
      "name" : "Kirsten Han \u97E9\u4FD0\u9896",
      "screen_name" : "kixes",
      "protected" : false,
      "id_str" : "27182288",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013433484604391424\/jv74veqG_normal.jpg",
      "id" : 27182288,
      "verified" : true
    }
  },
  "id" : 642348120454643712,
  "created_at" : "2015-09-11 14:45:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Niamh O'Reilly",
      "screen_name" : "nurserydublin",
      "indices" : [ 3, 17 ],
      "id_str" : "249793310",
      "id" : 249793310
    }, {
      "name" : "IrishParentingBlogs",
      "screen_name" : "ParentBlogsIE",
      "indices" : [ 24, 38 ],
      "id_str" : "872002578",
      "id" : 872002578
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "readfeelact",
      "indices" : [ 68, 80 ]
    }, {
      "text" : "cuppaforcalais",
      "indices" : [ 81, 96 ]
    } ],
    "urls" : [ {
      "indices" : [ 110, 132 ],
      "url" : "http:\/\/t.co\/FJCz20dnYi",
      "expanded_url" : "http:\/\/www.gofundme.com\/9zwfscys",
      "display_url" : "gofundme.com\/9zwfscys"
    } ]
  },
  "geo" : { },
  "id_str" : "642331144973848580",
  "text" : "RT @nurserydublin: Join @ParentBlogsIE virtual coffee morning today #readfeelact #cuppaforcalais. Donate here http:\/\/t.co\/FJCz20dnYi http:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "IrishParentingBlogs",
        "screen_name" : "ParentBlogsIE",
        "indices" : [ 5, 19 ],
        "id_str" : "872002578",
        "id" : 872002578
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nurserydublin\/status\/642320012733206528\/photo\/1",
        "indices" : [ 114, 136 ],
        "url" : "http:\/\/t.co\/iBCxsKjjJ9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COn6o-dXAAEJXZJ.jpg",
        "id_str" : "642320003744858113",
        "id" : 642320003744858113,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COn6o-dXAAEJXZJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/iBCxsKjjJ9"
      } ],
      "hashtags" : [ {
        "text" : "readfeelact",
        "indices" : [ 49, 61 ]
      }, {
        "text" : "cuppaforcalais",
        "indices" : [ 62, 77 ]
      } ],
      "urls" : [ {
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/FJCz20dnYi",
        "expanded_url" : "http:\/\/www.gofundme.com\/9zwfscys",
        "display_url" : "gofundme.com\/9zwfscys"
      } ]
    },
    "geo" : { },
    "id_str" : "642320012733206528",
    "text" : "Join @ParentBlogsIE virtual coffee morning today #readfeelact #cuppaforcalais. Donate here http:\/\/t.co\/FJCz20dnYi http:\/\/t.co\/iBCxsKjjJ9",
    "id" : 642320012733206528,
    "created_at" : "2015-09-11 12:53:19 +0000",
    "user" : {
      "name" : "Niamh O'Reilly",
      "screen_name" : "nurserydublin",
      "protected" : false,
      "id_str" : "249793310",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/720958878318333957\/p_jPb6tF_normal.jpg",
      "id" : 249793310,
      "verified" : false
    }
  },
  "id" : 642331144973848580,
  "created_at" : "2015-09-11 13:37:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kwan Jin Yao",
      "screen_name" : "guanyinmiao",
      "indices" : [ 3, 15 ],
      "id_str" : "37145570",
      "id" : 37145570
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GE2015",
      "indices" : [ 116, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642329055849771009",
  "text" : "RT @guanyinmiao: Because those are median wages. Have to account for rate of increase for low-income households too #GE2015  https:\/\/t.co\/J\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GE2015",
        "indices" : [ 99, 106 ]
      } ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/Jrfc2nQTAI",
        "expanded_url" : "https:\/\/twitter.com\/CampusEyeNUS\/status\/642328206305980417",
        "display_url" : "twitter.com\/CampusEyeNUS\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "642328360723419136",
    "text" : "Because those are median wages. Have to account for rate of increase for low-income households too #GE2015  https:\/\/t.co\/Jrfc2nQTAI",
    "id" : 642328360723419136,
    "created_at" : "2015-09-11 13:26:29 +0000",
    "user" : {
      "name" : "Kwan Jin Yao",
      "screen_name" : "guanyinmiao",
      "protected" : false,
      "id_str" : "37145570",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/518027838100746241\/hqleyzau_normal.jpeg",
      "id" : 37145570,
      "verified" : false
    }
  },
  "id" : 642329055849771009,
  "created_at" : "2015-09-11 13:29:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "indices" : [ 3, 11 ],
      "id_str" : "115624161",
      "id" : 115624161
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/YahooSG\/status\/642308148607320064\/photo\/1",
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/Q6hxr3uIv9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COnv2x6UsAA7c06.jpg",
      "id_str" : "642308146266943488",
      "id" : 642308146266943488,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COnv2x6UsAA7c06.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Q6hxr3uIv9"
    } ],
    "hashtags" : [ {
      "text" : "ge2015",
      "indices" : [ 56, 63 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642328129046990848",
  "text" : "RT @YahooSG: Hougang Ave 5: The WP flags are coming out #ge2015 http:\/\/t.co\/Q6hxr3uIv9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.echofon.com\/\" rel=\"nofollow\"\u003EEchofon\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/YahooSG\/status\/642308148607320064\/photo\/1",
        "indices" : [ 51, 73 ],
        "url" : "http:\/\/t.co\/Q6hxr3uIv9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COnv2x6UsAA7c06.jpg",
        "id_str" : "642308146266943488",
        "id" : 642308146266943488,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COnv2x6UsAA7c06.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Q6hxr3uIv9"
      } ],
      "hashtags" : [ {
        "text" : "ge2015",
        "indices" : [ 43, 50 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642308148607320064",
    "text" : "Hougang Ave 5: The WP flags are coming out #ge2015 http:\/\/t.co\/Q6hxr3uIv9",
    "id" : 642308148607320064,
    "created_at" : "2015-09-11 12:06:10 +0000",
    "user" : {
      "name" : "Yahoo Singapore",
      "screen_name" : "YahooSG",
      "protected" : false,
      "id_str" : "115624161",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630948009312784384\/3fz0flC2_normal.jpg",
      "id" : 115624161,
      "verified" : false
    }
  },
  "id" : 642328129046990848,
  "created_at" : "2015-09-11 13:25:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "avgeek",
      "indices" : [ 68, 75 ]
    } ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/eXgz2BANb2",
      "expanded_url" : "https:\/\/instagram.com\/p\/7eKSjYNQap\/",
      "display_url" : "instagram.com\/p\/7eKSjYNQap\/"
    } ]
  },
  "geo" : { },
  "id_str" : "642304114483949568",
  "text" : "The 3 work house of the Singapore Air Force https:\/\/t.co\/eXgz2BANb2 #avgeek",
  "id" : 642304114483949568,
  "created_at" : "2015-09-11 11:50:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 27 ],
      "url" : "https:\/\/t.co\/iclkvNTlzZ",
      "expanded_url" : "https:\/\/twitter.com\/Scswga\/status\/642302493087498241",
      "display_url" : "twitter.com\/Scswga\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642302820218183680",
  "text" : "LoL https:\/\/t.co\/iclkvNTlzZ",
  "id" : 642302820218183680,
  "created_at" : "2015-09-11 11:45:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/xRazIREwcc",
      "expanded_url" : "https:\/\/twitter.com\/jasonroe\/status\/642299497503150080",
      "display_url" : "twitter.com\/jasonroe\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642301751043977216",
  "text" : "Looking forward to predictconf https:\/\/t.co\/xRazIREwcc",
  "id" : 642301751043977216,
  "created_at" : "2015-09-11 11:40:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/642300239198732288\/photo\/1",
      "indices" : [ 53, 75 ],
      "url" : "http:\/\/t.co\/6Hpk3Dlw4b",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COnoqC6WUAAyPfs.jpg",
      "id_str" : "642300230910758912",
      "id" : 642300230910758912,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COnoqC6WUAAyPfs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6Hpk3Dlw4b"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642300239198732288",
  "text" : "Finding out how other professional using statistics. http:\/\/t.co\/6Hpk3Dlw4b",
  "id" : 642300239198732288,
  "created_at" : "2015-09-11 11:34:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Data4Good",
      "indices" : [ 51, 61 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 84 ],
      "url" : "http:\/\/t.co\/SnbpNQtaYD",
      "expanded_url" : "http:\/\/magazine.good.is\/articles\/suicide-prevention-week-data-driven-efforts",
      "display_url" : "magazine.good.is\/articles\/suici\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642108429801259013",
  "text" : "US Army data-heavy approach to suicide prevention. #Data4Good http:\/\/t.co\/SnbpNQtaYD",
  "id" : 642108429801259013,
  "created_at" : "2015-09-10 22:52:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anil Batra",
      "screen_name" : "AnilBatra",
      "indices" : [ 3, 13 ],
      "id_str" : "13882422",
      "id" : 13882422
    }, {
      "name" : "Elon Musk",
      "screen_name" : "elonmusk",
      "indices" : [ 52, 61 ],
      "id_str" : "44196397",
      "id" : 44196397
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "642057978808561664",
  "text" : "RT @AnilBatra: Are we running out of land on earth? @elonmusk why do we need to make Mars livable? Make earth livable first.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Elon Musk",
        "screen_name" : "elonmusk",
        "indices" : [ 37, 46 ],
        "id_str" : "44196397",
        "id" : 44196397
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "642051817451319296",
    "text" : "Are we running out of land on earth? @elonmusk why do we need to make Mars livable? Make earth livable first.",
    "id" : 642051817451319296,
    "created_at" : "2015-09-10 19:07:36 +0000",
    "user" : {
      "name" : "Anil Batra",
      "screen_name" : "AnilBatra",
      "protected" : false,
      "id_str" : "13882422",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/851565351070347264\/Nf6x_pY9_normal.jpg",
      "id" : 13882422,
      "verified" : false
    }
  },
  "id" : 642057978808561664,
  "created_at" : "2015-09-10 19:32:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "paul hayes",
      "screen_name" : "paulhayesman",
      "indices" : [ 0, 13 ],
      "id_str" : "76963073",
      "id" : 76963073
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "642052104648044544",
  "geo" : { },
  "id_str" : "642057978821128192",
  "in_reply_to_user_id" : 76963073,
  "text" : "@paulhayesman it is a sauna?",
  "id" : 642057978821128192,
  "in_reply_to_status_id" : 642052104648044544,
  "created_at" : "2015-09-10 19:32:05 +0000",
  "in_reply_to_screen_name" : "paulhayesman",
  "in_reply_to_user_id_str" : "76963073",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/2ZY2GBJqDZ",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-34205952",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "642018783561940992",
  "text" : "South East Asia's haze - social media reacts http:\/\/t.co\/2ZY2GBJqDZ",
  "id" : 642018783561940992,
  "created_at" : "2015-09-10 16:56:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shahrul",
      "screen_name" : "walashah",
      "indices" : [ 3, 12 ],
      "id_str" : "544894484",
      "id" : 544894484
    }, {
      "name" : "FourFourTwo \u26BD\uFE0F",
      "screen_name" : "FourFourTwo",
      "indices" : [ 117, 129 ],
      "id_str" : "34891363",
      "id" : 34891363
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 112 ],
      "url" : "http:\/\/t.co\/MLHMbwM8lQ",
      "expanded_url" : "http:\/\/po.st\/UxCUfg",
      "display_url" : "po.st\/UxCUfg"
    } ]
  },
  "geo" : { },
  "id_str" : "641964852773318657",
  "text" : "RT @walashah: Iceland and Syria make a mockery of Singapore's tired excuses | FourFourTwo http:\/\/t.co\/MLHMbwM8lQ via @FourFourTwo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "FourFourTwo \u26BD\uFE0F",
        "screen_name" : "FourFourTwo",
        "indices" : [ 103, 115 ],
        "id_str" : "34891363",
        "id" : 34891363
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 98 ],
        "url" : "http:\/\/t.co\/MLHMbwM8lQ",
        "expanded_url" : "http:\/\/po.st\/UxCUfg",
        "display_url" : "po.st\/UxCUfg"
      } ]
    },
    "geo" : { },
    "id_str" : "641810327370362882",
    "text" : "Iceland and Syria make a mockery of Singapore's tired excuses | FourFourTwo http:\/\/t.co\/MLHMbwM8lQ via @FourFourTwo",
    "id" : 641810327370362882,
    "created_at" : "2015-09-10 03:08:00 +0000",
    "user" : {
      "name" : "Shahrul",
      "screen_name" : "walashah",
      "protected" : false,
      "id_str" : "544894484",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647253626927235072\/oNLn8-OK_normal.jpg",
      "id" : 544894484,
      "verified" : false
    }
  },
  "id" : 641964852773318657,
  "created_at" : "2015-09-10 13:22:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Byers",
      "screen_name" : "davidbyers26",
      "indices" : [ 3, 16 ],
      "id_str" : "27841030",
      "id" : 27841030
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641960252041220096",
  "text" : "RT @davidbyers26: \"A compliment isn\u2019t misogynistic. Why can\u2019t feminists understand this?\" Riproaring column by Brendan O'Neill http:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 109, 131 ],
        "url" : "http:\/\/t.co\/eythg1XOS2",
        "expanded_url" : "http:\/\/specc.ie\/1LlNwXT",
        "display_url" : "specc.ie\/1LlNwXT"
      } ]
    },
    "geo" : { },
    "id_str" : "641936555020550144",
    "text" : "\"A compliment isn\u2019t misogynistic. Why can\u2019t feminists understand this?\" Riproaring column by Brendan O'Neill http:\/\/t.co\/eythg1XOS2",
    "id" : 641936555020550144,
    "created_at" : "2015-09-10 11:29:35 +0000",
    "user" : {
      "name" : "David Byers",
      "screen_name" : "davidbyers26",
      "protected" : false,
      "id_str" : "27841030",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039501932128292865\/lrqNO3fD_normal.jpg",
      "id" : 27841030,
      "verified" : true
    }
  },
  "id" : 641960252041220096,
  "created_at" : "2015-09-10 13:03:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/641954356842078208\/photo\/1",
      "indices" : [ 35, 57 ],
      "url" : "http:\/\/t.co\/QqmDajuIs1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COiuFhYWEAAaVwQ.jpg",
      "id_str" : "641954356783353856",
      "id" : 641954356783353856,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COiuFhYWEAAaVwQ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/QqmDajuIs1"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 34 ],
      "url" : "http:\/\/t.co\/JxlQscq51a",
      "expanded_url" : "http:\/\/ift.tt\/1JWVBCh",
      "display_url" : "ift.tt\/1JWVBCh"
    } ]
  },
  "geo" : { },
  "id_str" : "641954356842078208",
  "text" : "Lunch today http:\/\/t.co\/JxlQscq51a http:\/\/t.co\/QqmDajuIs1",
  "id" : 641954356842078208,
  "created_at" : "2015-09-10 12:40:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "indices" : [ 3, 16 ],
      "id_str" : "768968",
      "id" : 768968
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/coolinsights\/status\/641927520313446404\/photo\/1",
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/RifOMvrGcJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COiVrbcUYAMc0Em.jpg",
      "id_str" : "641927520233742339",
      "id" : 641927520233742339,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COiVrbcUYAMc0Em.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 565,
        "resize" : "fit",
        "w" : 986
      }, {
        "h" : 565,
        "resize" : "fit",
        "w" : 986
      }, {
        "h" : 565,
        "resize" : "fit",
        "w" : 986
      }, {
        "h" : 390,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RifOMvrGcJ"
    } ],
    "hashtags" : [ {
      "text" : "SGhaze",
      "indices" : [ 87, 94 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641928914529599488",
  "text" : "RT @coolinsights: The Air Quality Index has hit unhealthy levels. Please stay indoors. #SGhaze http:\/\/t.co\/RifOMvrGcJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitshot.com\" rel=\"nofollow\"\u003ETwitshot.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/coolinsights\/status\/641927520313446404\/photo\/1",
        "indices" : [ 77, 99 ],
        "url" : "http:\/\/t.co\/RifOMvrGcJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COiVrbcUYAMc0Em.jpg",
        "id_str" : "641927520233742339",
        "id" : 641927520233742339,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COiVrbcUYAMc0Em.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 565,
          "resize" : "fit",
          "w" : 986
        }, {
          "h" : 565,
          "resize" : "fit",
          "w" : 986
        }, {
          "h" : 565,
          "resize" : "fit",
          "w" : 986
        }, {
          "h" : 390,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RifOMvrGcJ"
      } ],
      "hashtags" : [ {
        "text" : "SGhaze",
        "indices" : [ 69, 76 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641927520313446404",
    "text" : "The Air Quality Index has hit unhealthy levels. Please stay indoors. #SGhaze http:\/\/t.co\/RifOMvrGcJ",
    "id" : 641927520313446404,
    "created_at" : "2015-09-10 10:53:41 +0000",
    "user" : {
      "name" : "Walter Lim",
      "screen_name" : "coolinsights",
      "protected" : false,
      "id_str" : "768968",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/665181228841046016\/dvZ4PiEU_normal.jpg",
      "id" : 768968,
      "verified" : false
    }
  },
  "id" : 641928914529599488,
  "created_at" : "2015-09-10 10:59:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Springboard",
      "screen_name" : "SpringboardHEA",
      "indices" : [ 0, 15 ],
      "id_str" : "507249109",
      "id" : 507249109
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641910201252212736",
  "geo" : { },
  "id_str" : "641911105766486016",
  "in_reply_to_user_id" : 507249109,
  "text" : "@SpringboardHEA for those applied, when can they learn about the outcome of their application? Thanks.",
  "id" : 641911105766486016,
  "in_reply_to_status_id" : 641910201252212736,
  "created_at" : "2015-09-10 09:48:28 +0000",
  "in_reply_to_screen_name" : "SpringboardHEA",
  "in_reply_to_user_id_str" : "507249109",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 0, 12 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641901981192355840",
  "geo" : { },
  "id_str" : "641910106297339904",
  "in_reply_to_user_id" : 18721044,
  "text" : "@klillington also peak industry award.",
  "id" : 641910106297339904,
  "in_reply_to_status_id" : 641901981192355840,
  "created_at" : "2015-09-10 09:44:30 +0000",
  "in_reply_to_screen_name" : "klillington",
  "in_reply_to_user_id_str" : "18721044",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "indices" : [ 3, 15 ],
      "id_str" : "14107081",
      "id" : 14107081
    }, {
      "name" : "Jacqueline Poh",
      "screen_name" : "PohJacqueline",
      "indices" : [ 75, 89 ],
      "id_str" : "2526271542",
      "id" : 2526271542
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "YoungTechFest",
      "indices" : [ 93, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641908778376540160",
  "text" : "RT @geekonomics: Future jobs will either be high tech or high touch argues @PohJacqueline at #YoungTechFest how about your job today?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jacqueline Poh",
        "screen_name" : "PohJacqueline",
        "indices" : [ 58, 72 ],
        "id_str" : "2526271542",
        "id" : 2526271542
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "YoungTechFest",
        "indices" : [ 76, 90 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641908452709634048",
    "text" : "Future jobs will either be high tech or high touch argues @PohJacqueline at #YoungTechFest how about your job today?",
    "id" : 641908452709634048,
    "created_at" : "2015-09-10 09:37:55 +0000",
    "user" : {
      "name" : "Nicholas Aaron Khoo",
      "screen_name" : "geekonomics",
      "protected" : false,
      "id_str" : "14107081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/815459302941093888\/HFWgUA6V_normal.jpg",
      "id" : 14107081,
      "verified" : false
    }
  },
  "id" : 641908778376540160,
  "created_at" : "2015-09-10 09:39:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Department of Justice & Equality",
      "screen_name" : "DeptJusticeIRL",
      "indices" : [ 3, 18 ],
      "id_str" : "1960909274",
      "id" : 1960909274
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MigrationEU",
      "indices" : [ 122, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641905354197082113",
  "text" : "RT @DeptJusticeIRL: Government approves \u2018Irish Refugee Protection Programme\u2019 Ireland to accept up to 4,000 in response to #MigrationEU cris\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Frances Fitzgerald",
        "screen_name" : "FitzgeraldFrncs",
        "indices" : [ 122, 138 ],
        "id_str" : "111338251",
        "id" : 111338251
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "MigrationEU",
        "indices" : [ 102, 114 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641885254991630336",
    "text" : "Government approves \u2018Irish Refugee Protection Programme\u2019 Ireland to accept up to 4,000 in response to #MigrationEU crisis @FitzgeraldFrncs",
    "id" : 641885254991630336,
    "created_at" : "2015-09-10 08:05:45 +0000",
    "user" : {
      "name" : "Department of Justice & Equality",
      "screen_name" : "DeptJusticeIRL",
      "protected" : false,
      "id_str" : "1960909274",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012737499003793408\/9m5L7FEw_normal.jpg",
      "id" : 1960909274,
      "verified" : true
    }
  },
  "id" : 641905354197082113,
  "created_at" : "2015-09-10 09:25:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenData",
      "indices" : [ 102, 111 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641901816951779328",
  "text" : "\u201CIt\u2019s easy to lie with statistics. It\u2019s hard to tell the truth without statistics.\u201D \u2013 Andrejs Dunkels #OpenData",
  "id" : 641901816951779328,
  "created_at" : "2015-09-10 09:11:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Jakarta Post",
      "screen_name" : "jakpost",
      "indices" : [ 0, 8 ],
      "id_str" : "68577507",
      "id" : 68577507
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641552949005565952",
  "geo" : { },
  "id_str" : "641901104574406656",
  "in_reply_to_user_id" : 68577507,
  "text" : "@jakpost Please tell your minister to take charge of Indonesia airspace that polluting Singapore and Malaysia.",
  "id" : 641901104574406656,
  "in_reply_to_status_id" : 641552949005565952,
  "created_at" : "2015-09-10 09:08:43 +0000",
  "in_reply_to_screen_name" : "jakpost",
  "in_reply_to_user_id_str" : "68577507",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641722918901297152",
  "text" : "Never knew girl bone was crushed for foot binding when they are 5. Jaysus.",
  "id" : 641722918901297152,
  "created_at" : "2015-09-09 21:20:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amanda Foreman",
      "screen_name" : "DrAmandaForeman",
      "indices" : [ 13, 29 ],
      "id_str" : "322029112",
      "id" : 322029112
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 72 ],
      "url" : "http:\/\/t.co\/4wwP1DzFMi",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b06b8g1d",
      "display_url" : "bbc.co.uk\/programmes\/b06\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641722637589331969",
  "text" : "Just watched @DrAmandaForeman The Ascent of Women http:\/\/t.co\/4wwP1DzFMi Reminds me of my grandma foot binding.",
  "id" : 641722637589331969,
  "created_at" : "2015-09-09 21:19:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Theo Lynn",
      "screen_name" : "theolynn",
      "indices" : [ 0, 9 ],
      "id_str" : "22193590",
      "id" : 22193590
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MSDM",
      "indices" : [ 19, 24 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641709319721984000",
  "geo" : { },
  "id_str" : "641719632123043840",
  "in_reply_to_user_id" : 22193590,
  "text" : "@theolynn Just the #MSDM Thanks",
  "id" : 641719632123043840,
  "in_reply_to_status_id" : 641709319721984000,
  "created_at" : "2015-09-09 21:07:37 +0000",
  "in_reply_to_screen_name" : "theolynn",
  "in_reply_to_user_id_str" : "22193590",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Weckler",
      "screen_name" : "adrianweckler",
      "indices" : [ 3, 17 ],
      "id_str" : "18080002",
      "id" : 18080002
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/adrianweckler\/status\/641668402705141760\/photo\/1",
      "indices" : [ 69, 91 ],
      "url" : "http:\/\/t.co\/8dYIhviuc9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COeqAu2UYAEfc-X.jpg",
      "id_str" : "641668401476165633",
      "id" : 641668401476165633,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COeqAu2UYAEfc-X.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8dYIhviuc9"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641671713483325444",
  "text" : "RT @adrianweckler: Irene Walsh of Dublin firm 3D Medical on stage... http:\/\/t.co\/8dYIhviuc9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/adrianweckler\/status\/641668402705141760\/photo\/1",
        "indices" : [ 50, 72 ],
        "url" : "http:\/\/t.co\/8dYIhviuc9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COeqAu2UYAEfc-X.jpg",
        "id_str" : "641668401476165633",
        "id" : 641668401476165633,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COeqAu2UYAEfc-X.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8dYIhviuc9"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641668402705141760",
    "text" : "Irene Walsh of Dublin firm 3D Medical on stage... http:\/\/t.co\/8dYIhviuc9",
    "id" : 641668402705141760,
    "created_at" : "2015-09-09 17:44:03 +0000",
    "user" : {
      "name" : "Adrian Weckler",
      "screen_name" : "adrianweckler",
      "protected" : false,
      "id_str" : "18080002",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040108417703014400\/NY5mHJ0B_normal.jpg",
      "id" : 18080002,
      "verified" : true
    }
  },
  "id" : 641671713483325444,
  "created_at" : "2015-09-09 17:57:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641667829998166018",
  "text" : "Attempt to use Windows version of Safari to view the event does not work too.",
  "id" : 641667829998166018,
  "created_at" : "2015-09-09 17:41:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641656635748823040",
  "geo" : { },
  "id_str" : "641665033412476928",
  "in_reply_to_user_id" : 815066809,
  "text" : "@meowinginbkk ok now it make sense to me. Need to bring another people.",
  "id" : 641665033412476928,
  "in_reply_to_status_id" : 641656635748823040,
  "created_at" : "2015-09-09 17:30:40 +0000",
  "in_reply_to_screen_name" : "ofmeowandbake",
  "in_reply_to_user_id_str" : "815066809",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AppleEvent",
      "indices" : [ 59, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 36, 58 ],
      "url" : "http:\/\/t.co\/7gLYnPm3CS",
      "expanded_url" : "http:\/\/download.cnet.com\/Apple-Safari\/3000-2356_4-10697481.html",
      "display_url" : "download.cnet.com\/Apple-Safari\/3\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641664895046520833",
  "text" : "The 2nd most popular website now is http:\/\/t.co\/7gLYnPm3CS #AppleEvent",
  "id" : 641664895046520833,
  "created_at" : "2015-09-09 17:30:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/641658539212177408\/photo\/1",
      "indices" : [ 32, 54 ],
      "url" : "http:\/\/t.co\/efDOCyoK47",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COeg_jIWoAA1ghK.jpg",
      "id_str" : "641658485546065920",
      "id" : 641658485546065920,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COeg_jIWoAA1ghK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/efDOCyoK47"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641658539212177408",
  "text" : "Does not work on my TV browser. http:\/\/t.co\/efDOCyoK47",
  "id" : 641658539212177408,
  "created_at" : "2015-09-09 17:04:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/4mQYUW6MQ6",
      "expanded_url" : "http:\/\/pocket.co\/sokLy7",
      "display_url" : "pocket.co\/sokLy7"
    } ]
  },
  "geo" : { },
  "id_str" : "641651949071376384",
  "text" : "Ditching cable TV in Ireland is not remotely difficult and the savings make for choice viewing http:\/\/t.co\/4mQYUW6MQ6",
  "id" : 641651949071376384,
  "created_at" : "2015-09-09 16:38:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641640009422999552",
  "text" : "What do event organiser mean when they mention : \"You are welcome as a +1\"",
  "id" : 641640009422999552,
  "created_at" : "2015-09-09 15:51:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "indices" : [ 3, 15 ],
      "id_str" : "14479046",
      "id" : 14479046
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/daniel_kane\/status\/641560275250991104\/photo\/1",
      "indices" : [ 87, 109 ],
      "url" : "http:\/\/t.co\/uEZGAjsvSF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COdHp-qUkAAInTW.jpg",
      "id_str" : "641560258444300288",
      "id" : 641560258444300288,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COdHp-qUkAAInTW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 345,
        "resize" : "fit",
        "w" : 614
      }, {
        "h" : 345,
        "resize" : "fit",
        "w" : 614
      }, {
        "h" : 345,
        "resize" : "fit",
        "w" : 614
      }, {
        "h" : 345,
        "resize" : "fit",
        "w" : 614
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uEZGAjsvSF"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 86 ],
      "url" : "http:\/\/t.co\/R5apbrbHrw",
      "expanded_url" : "http:\/\/www.rte.ie\/news\/2015\/0909\/726606-migrants\/",
      "display_url" : "rte.ie\/news\/2015\/0909\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641560640461643776",
  "text" : "RT @daniel_kane: Juncker urges humanity and dignity on refugees http:\/\/t.co\/R5apbrbHrw http:\/\/t.co\/uEZGAjsvSF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/daniel_kane\/status\/641560275250991104\/photo\/1",
        "indices" : [ 70, 92 ],
        "url" : "http:\/\/t.co\/uEZGAjsvSF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COdHp-qUkAAInTW.jpg",
        "id_str" : "641560258444300288",
        "id" : 641560258444300288,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COdHp-qUkAAInTW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 345,
          "resize" : "fit",
          "w" : 614
        }, {
          "h" : 345,
          "resize" : "fit",
          "w" : 614
        }, {
          "h" : 345,
          "resize" : "fit",
          "w" : 614
        }, {
          "h" : 345,
          "resize" : "fit",
          "w" : 614
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uEZGAjsvSF"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 69 ],
        "url" : "http:\/\/t.co\/R5apbrbHrw",
        "expanded_url" : "http:\/\/www.rte.ie\/news\/2015\/0909\/726606-migrants\/",
        "display_url" : "rte.ie\/news\/2015\/0909\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "641560275250991104",
    "text" : "Juncker urges humanity and dignity on refugees http:\/\/t.co\/R5apbrbHrw http:\/\/t.co\/uEZGAjsvSF",
    "id" : 641560275250991104,
    "created_at" : "2015-09-09 10:34:23 +0000",
    "user" : {
      "name" : "Daniel Kane",
      "screen_name" : "daniel_kane",
      "protected" : false,
      "id_str" : "14479046",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959158926339137536\/dxkfCxt4_normal.jpg",
      "id" : 14479046,
      "verified" : false
    }
  },
  "id" : 641560640461643776,
  "created_at" : "2015-09-09 10:35:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "indices" : [ 3, 11 ],
      "id_str" : "21071383",
      "id" : 21071383
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Pickering",
      "indices" : [ 18, 28 ]
    } ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/0oDWo3AeQW",
      "expanded_url" : "https:\/\/twitter.com\/hellofrmSG\/status\/641529251347480576",
      "display_url" : "twitter.com\/hellofrmSG\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641532968801071104",
  "text" : "RT @joleeqh: Wow, #Pickering so kiang! https:\/\/t.co\/0oDWo3AeQW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Pickering",
        "indices" : [ 5, 15 ]
      } ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/0oDWo3AeQW",
        "expanded_url" : "https:\/\/twitter.com\/hellofrmSG\/status\/641529251347480576",
        "display_url" : "twitter.com\/hellofrmSG\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "641529788885106689",
    "text" : "Wow, #Pickering so kiang! https:\/\/t.co\/0oDWo3AeQW",
    "id" : 641529788885106689,
    "created_at" : "2015-09-09 08:33:15 +0000",
    "user" : {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "protected" : false,
      "id_str" : "21071383",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/435265895854641152\/QCz6O1FM_normal.png",
      "id" : 21071383,
      "verified" : false
    }
  },
  "id" : 641532968801071104,
  "created_at" : "2015-09-09 08:45:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641532196277350400",
  "text" : "A text alert sound on my cellphone thinking somebody might text me for a coffee meetup turns out a reminder to pay bill. LOL",
  "id" : 641532196277350400,
  "created_at" : "2015-09-09 08:42:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Theo Lynn",
      "screen_name" : "theolynn",
      "indices" : [ 0, 9 ],
      "id_str" : "22193590",
      "id" : 22193590
    }, {
      "name" : "DCUBS",
      "screen_name" : "DCUBS",
      "indices" : [ 10, 16 ],
      "id_str" : "957926752885538817",
      "id" : 957926752885538817
    }, {
      "name" : "DCU",
      "screen_name" : "DublinCityUni",
      "indices" : [ 17, 31 ],
      "id_str" : "70713759",
      "id" : 70713759
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641512616322572288",
  "geo" : { },
  "id_str" : "641525411382493184",
  "in_reply_to_user_id" : 22193590,
  "text" : "@theolynn @DCUBS @DublinCityUni How many students do you have in each new batch?",
  "id" : 641525411382493184,
  "in_reply_to_status_id" : 641512616322572288,
  "created_at" : "2015-09-09 08:15:51 +0000",
  "in_reply_to_screen_name" : "theolynn",
  "in_reply_to_user_id_str" : "22193590",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lena Goh",
      "screen_name" : "lenagoh",
      "indices" : [ 3, 11 ],
      "id_str" : "24663949",
      "id" : 24663949
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dldtelaviv2015",
      "indices" : [ 13, 28 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641510609314541568",
  "text" : "RT @lenagoh: #dldtelaviv2015 3 out of 7 Israel startups are founded by women!super cool\uD83D\uDE00",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dldtelaviv2015",
        "indices" : [ 0, 15 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641229582029340672",
    "text" : "#dldtelaviv2015 3 out of 7 Israel startups are founded by women!super cool\uD83D\uDE00",
    "id" : 641229582029340672,
    "created_at" : "2015-09-08 12:40:20 +0000",
    "user" : {
      "name" : "Lena Goh",
      "screen_name" : "lenagoh",
      "protected" : false,
      "id_str" : "24663949",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/745172557993648128\/YlAyzaWL_normal.jpg",
      "id" : 24663949,
      "verified" : false
    }
  },
  "id" : 641510609314541568,
  "created_at" : "2015-09-09 07:17:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jacqueline Poh",
      "screen_name" : "PohJacqueline",
      "indices" : [ 3, 17 ],
      "id_str" : "2526271542",
      "id" : 2526271542
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "internet",
      "indices" : [ 33, 42 ]
    }, {
      "text" : "ge2015",
      "indices" : [ 48, 55 ]
    }, {
      "text" : "singapore",
      "indices" : [ 102, 112 ]
    }, {
      "text" : "telecom",
      "indices" : [ 113, 121 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641510194988625920",
  "text" : "RT @PohJacqueline: How to access #internet at a #ge2015 rally? Cellular-on-Wheels boost connectivity. #singapore #telecom http:\/\/t.co\/Mbj3d\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PohJacqueline\/status\/641458185266552834\/photo\/1",
        "indices" : [ 103, 125 ],
        "url" : "http:\/\/t.co\/Mbj3dV6XyP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CObqzCjVAAAjjx_.jpg",
        "id_str" : "641458159526150144",
        "id" : 641458159526150144,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CObqzCjVAAAjjx_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Mbj3dV6XyP"
      } ],
      "hashtags" : [ {
        "text" : "internet",
        "indices" : [ 14, 23 ]
      }, {
        "text" : "ge2015",
        "indices" : [ 29, 36 ]
      }, {
        "text" : "singapore",
        "indices" : [ 83, 93 ]
      }, {
        "text" : "telecom",
        "indices" : [ 94, 102 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641458185266552834",
    "text" : "How to access #internet at a #ge2015 rally? Cellular-on-Wheels boost connectivity. #singapore #telecom http:\/\/t.co\/Mbj3dV6XyP",
    "id" : 641458185266552834,
    "created_at" : "2015-09-09 03:48:43 +0000",
    "user" : {
      "name" : "Jacqueline Poh",
      "screen_name" : "PohJacqueline",
      "protected" : false,
      "id_str" : "2526271542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930688328986394624\/uMp6iUjq_normal.jpg",
      "id" : 2526271542,
      "verified" : true
    }
  },
  "id" : 641510194988625920,
  "created_at" : "2015-09-09 07:15:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Kneen",
      "screen_name" : "jasonkneen",
      "indices" : [ 3, 14 ],
      "id_str" : "6392872",
      "id" : 6392872
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BA2276",
      "indices" : [ 112, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641510055070801920",
  "text" : "RT @jasonkneen: Pleased to see that so many laptops, duty free and other replaceable items made it off safely.  #BA2276",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BA2276",
        "indices" : [ 96, 103 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641506446866313216",
    "text" : "Pleased to see that so many laptops, duty free and other replaceable items made it off safely.  #BA2276",
    "id" : 641506446866313216,
    "created_at" : "2015-09-09 07:00:30 +0000",
    "user" : {
      "name" : "Jason Kneen",
      "screen_name" : "jasonkneen",
      "protected" : false,
      "id_str" : "6392872",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017164222655008768\/kcyGSFNP_normal.jpg",
      "id" : 6392872,
      "verified" : true
    }
  },
  "id" : 641510055070801920,
  "created_at" : "2015-09-09 07:14:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641418929903128576",
  "text" : "Done doing a clean install of Window 8.1!",
  "id" : 641418929903128576,
  "created_at" : "2015-09-09 01:12:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641411235481829376",
  "text" : "Want to sleep and I can't waiting for the PC to be getting ready after doing a clean install....",
  "id" : 641411235481829376,
  "created_at" : "2015-09-09 00:42:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDC10 Jase \uD83E\uDD84",
      "screen_name" : "BKKJase",
      "indices" : [ 3, 11 ],
      "id_str" : "51528497",
      "id" : 51528497
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641407824669618176",
  "text" : "RT @BKKJase: Hey Apple. Just so you know. iTunes now sucks dick on all platforms. Fire your UI designers. You made it confusing and hard to\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641404555515719680",
    "text" : "Hey Apple. Just so you know. iTunes now sucks dick on all platforms. Fire your UI designers. You made it confusing and hard to use \uD83D\uDC4E",
    "id" : 641404555515719680,
    "created_at" : "2015-09-09 00:15:37 +0000",
    "user" : {
      "name" : "\uD83D\uDC10 Jase \uD83E\uDD84",
      "screen_name" : "BKKJase",
      "protected" : false,
      "id_str" : "51528497",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012166437778046976\/nxQVRxmr_normal.jpg",
      "id" : 51528497,
      "verified" : false
    }
  },
  "id" : 641407824669618176,
  "created_at" : "2015-09-09 00:28:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amanda",
      "screen_name" : "Hamperlady",
      "indices" : [ 0, 11 ],
      "id_str" : "19398524",
      "id" : 19398524
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641406933744881664",
  "geo" : { },
  "id_str" : "641407685045395456",
  "in_reply_to_user_id" : 19398524,
  "text" : "@Hamperlady where are u?",
  "id" : 641407685045395456,
  "in_reply_to_status_id" : 641406933744881664,
  "created_at" : "2015-09-09 00:28:03 +0000",
  "in_reply_to_screen_name" : "Hamperlady",
  "in_reply_to_user_id_str" : "19398524",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alan K'necht",
      "screen_name" : "aknecht",
      "indices" : [ 3, 11 ],
      "id_str" : "17269569",
      "id" : 17269569
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 126 ],
      "url" : "http:\/\/t.co\/XVerr2Vzva",
      "expanded_url" : "http:\/\/klou.tt\/wtmvvle1gl45",
      "display_url" : "klou.tt\/wtmvvle1gl45"
    } ]
  },
  "geo" : { },
  "id_str" : "641379142026534912",
  "text" : "RT @aknecht: Is data analytics about causes ... or correlations? No debate, it's how you use the data - http:\/\/t.co\/XVerr2Vzva",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/klout.com\" rel=\"nofollow\"\u003EPost with Klout\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 113 ],
        "url" : "http:\/\/t.co\/XVerr2Vzva",
        "expanded_url" : "http:\/\/klou.tt\/wtmvvle1gl45",
        "display_url" : "klou.tt\/wtmvvle1gl45"
      } ]
    },
    "geo" : { },
    "id_str" : "641306274777292800",
    "text" : "Is data analytics about causes ... or correlations? No debate, it's how you use the data - http:\/\/t.co\/XVerr2Vzva",
    "id" : 641306274777292800,
    "created_at" : "2015-09-08 17:45:05 +0000",
    "user" : {
      "name" : "Alan K'necht",
      "screen_name" : "aknecht",
      "protected" : false,
      "id_str" : "17269569",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966870794428133377\/44g1HhFR_normal.jpg",
      "id" : 17269569,
      "verified" : true
    }
  },
  "id" : 641379142026534912,
  "created_at" : "2015-09-08 22:34:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/641375530453979136\/photo\/1",
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/LZcn5XwVvn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COafpY8WIAABbff.jpg",
      "id_str" : "641375530365886464",
      "id" : 641375530365886464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COafpY8WIAABbff.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LZcn5XwVvn"
    } ],
    "hashtags" : [ {
      "text" : "ireland",
      "indices" : [ 33, 41 ]
    }, {
      "text" : "huaweisnapys",
      "indices" : [ 43, 56 ]
    } ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http:\/\/t.co\/zK5P75fhkY",
      "expanded_url" : "http:\/\/ift.tt\/1ESFiJU",
      "display_url" : "ift.tt\/1ESFiJU"
    } ]
  },
  "geo" : { },
  "id_str" : "641375530453979136",
  "text" : "A different kind of racing club. #ireland  #huaweisnapys http:\/\/t.co\/zK5P75fhkY http:\/\/t.co\/LZcn5XwVvn",
  "id" : 641375530453979136,
  "created_at" : "2015-09-08 22:20:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/gPFzufe4NH",
      "expanded_url" : "https:\/\/twitter.com\/IrishTimesLife\/status\/640832898870808576",
      "display_url" : "twitter.com\/IrishTimesLife\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641374301699092481",
  "text" : "Who is lonely and want to meet up for coffee? Please DM me.  https:\/\/t.co\/gPFzufe4NH",
  "id" : 641374301699092481,
  "created_at" : "2015-09-08 22:15:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641351861958635521",
  "text" : "The BBC3 presenter amazed that Chinese give half of their income to the parents. Volley of laughter. Sunshine, it no brainer in Asia.",
  "id" : 641351861958635521,
  "created_at" : "2015-09-08 20:46:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 130, 142 ]
    } ],
    "urls" : [ {
      "indices" : [ 107, 129 ],
      "url" : "http:\/\/t.co\/UQZ5iyC1wC",
      "expanded_url" : "http:\/\/www.marketingweek.com\/2015\/05\/06\/six-brands-that-are-using-data-to-drive-business-success\/",
      "display_url" : "marketingweek.com\/2015\/05\/06\/six\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641264824626905088",
  "text" : "You capture, measure\/analyse traffic &amp; see how visitors behave on your site, but what\u2019s the next step? http:\/\/t.co\/UQZ5iyC1wC #getoptimise",
  "id" : 641264824626905088,
  "created_at" : "2015-09-08 15:00:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jonathan Sullivan",
      "screen_name" : "jonlsullivan",
      "indices" : [ 3, 16 ],
      "id_str" : "111019435",
      "id" : 111019435
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641238862774333440",
  "text" : "RT @jonlsullivan: Reading how FOCAC prompted formation of Korea\/Brazil\/India\/Singapore-Africa Fora, Turkey-Africa partnership, Taiwan-Afric\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.twitter.com\" rel=\"nofollow\"\u003ETwitter for Windows\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641238636030246912",
    "text" : "Reading how FOCAC prompted formation of Korea\/Brazil\/India\/Singapore-Africa Fora, Turkey-Africa partnership, Taiwan-Africa summit etc",
    "id" : 641238636030246912,
    "created_at" : "2015-09-08 13:16:19 +0000",
    "user" : {
      "name" : "Jonathan Sullivan",
      "screen_name" : "jonlsullivan",
      "protected" : false,
      "id_str" : "111019435",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/995767072260001792\/Jm9mgNBI_normal.jpg",
      "id" : 111019435,
      "verified" : false
    }
  },
  "id" : 641238862774333440,
  "created_at" : "2015-09-08 13:17:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641238655441485824",
  "text" : "On an Operating System : \"This might take over an hour\" Usually is half a day!",
  "id" : 641238655441485824,
  "created_at" : "2015-09-08 13:16:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 3, 14 ],
      "id_str" : "816653",
      "id" : 816653
    }, {
      "name" : "Sheji Ho",
      "screen_name" : "sheji_acommerce",
      "indices" : [ 123, 139 ],
      "id_str" : "1703411918",
      "id" : 1703411918
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 119 ],
      "url" : "http:\/\/t.co\/61PJzPzW49",
      "expanded_url" : "http:\/\/tcrn.ch\/1UDYSvp",
      "display_url" : "tcrn.ch\/1UDYSvp"
    } ]
  },
  "geo" : { },
  "id_str" : "641221130116812800",
  "text" : "RT @TechCrunch: Why Southeast Asia Is Leading The World\u2019s Most Disruptive Mobile Business Models http:\/\/t.co\/61PJzPzW49 by @sheji_acommerce",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/10up.com\" rel=\"nofollow\"\u003E10up Publish Tweet\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Sheji Ho",
        "screen_name" : "sheji_acommerce",
        "indices" : [ 107, 123 ],
        "id_str" : "1703411918",
        "id" : 1703411918
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 103 ],
        "url" : "http:\/\/t.co\/61PJzPzW49",
        "expanded_url" : "http:\/\/tcrn.ch\/1UDYSvp",
        "display_url" : "tcrn.ch\/1UDYSvp"
      } ]
    },
    "geo" : { },
    "id_str" : "641219550508220418",
    "text" : "Why Southeast Asia Is Leading The World\u2019s Most Disruptive Mobile Business Models http:\/\/t.co\/61PJzPzW49 by @sheji_acommerce",
    "id" : 641219550508220418,
    "created_at" : "2015-09-08 12:00:28 +0000",
    "user" : {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "protected" : false,
      "id_str" : "816653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/969240943671955456\/mGuud28F_normal.jpg",
      "id" : 816653,
      "verified" : true
    }
  },
  "id" : 641221130116812800,
  "created_at" : "2015-09-08 12:06:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/641216390234513408\/photo\/1",
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/jlkVcYVqIN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COYO6NdWsAAcxUQ.jpg",
      "id_str" : "641216390154858496",
      "id" : 641216390154858496,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COYO6NdWsAAcxUQ.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jlkVcYVqIN"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 79 ],
      "url" : "http:\/\/t.co\/DQN82MmL8z",
      "expanded_url" : "http:\/\/ift.tt\/1K45Pju",
      "display_url" : "ift.tt\/1K45Pju"
    } ]
  },
  "geo" : { },
  "id_str" : "641216390234513408",
  "text" : "Cycle from home to here yesterday. A distance of 11.2 KM http:\/\/t.co\/DQN82MmL8z http:\/\/t.co\/jlkVcYVqIN",
  "id" : 641216390234513408,
  "created_at" : "2015-09-08 11:47:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Orlaith Carmody",
      "screen_name" : "OrlaithCarmody",
      "indices" : [ 3, 18 ],
      "id_str" : "117470356",
      "id" : 117470356
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/i2f5g9hYB1",
      "expanded_url" : "https:\/\/twitter.com\/colettebrowne\/status\/640992317185355776",
      "display_url" : "twitter.com\/colettebrowne\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641210471144665088",
  "text" : "RT @OrlaithCarmody: Nice one Kevin Humphries! Don't do as I say, do as I do https:\/\/t.co\/i2f5g9hYB1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/i2f5g9hYB1",
        "expanded_url" : "https:\/\/twitter.com\/colettebrowne\/status\/640992317185355776",
        "display_url" : "twitter.com\/colettebrowne\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "640992989335154688",
    "text" : "Nice one Kevin Humphries! Don't do as I say, do as I do https:\/\/t.co\/i2f5g9hYB1",
    "id" : 640992989335154688,
    "created_at" : "2015-09-07 21:00:12 +0000",
    "user" : {
      "name" : "Orlaith Carmody",
      "screen_name" : "OrlaithCarmody",
      "protected" : false,
      "id_str" : "117470356",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006498877376270336\/h2pjNSwh_normal.jpg",
      "id" : 117470356,
      "verified" : false
    }
  },
  "id" : 641210471144665088,
  "created_at" : "2015-09-08 11:24:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/EAsBg66ILB",
      "expanded_url" : "https:\/\/www.youtube.com\/watch?v=akY3mzIfU2M",
      "display_url" : "youtube.com\/watch?v=akY3mz\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641207141441929217",
  "text" : "At a recent talk, this was held as a good example of using story to communicate a company strategy. https:\/\/t.co\/EAsBg66ILB",
  "id" : 641207141441929217,
  "created_at" : "2015-09-08 11:11:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Star Wars UK",
      "screen_name" : "StarWarsUK",
      "indices" : [ 3, 14 ],
      "id_str" : "2250737179",
      "id" : 2250737179
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641203510395150336",
  "text" : "RT @StarWarsUK: The Force Awakens will be released in the UK on December 17th. The hundred day countdown starts today! http:\/\/t.co\/GRlWtZfW\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/StarWarsUK\/status\/641023410093494272\/photo\/1",
        "indices" : [ 103, 125 ],
        "url" : "http:\/\/t.co\/GRlWtZfWMl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COVQM-fWcAEatI6.jpg",
        "id_str" : "641006705833308161",
        "id" : 641006705833308161,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COVQM-fWcAEatI6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GRlWtZfWMl"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "641023410093494272",
    "text" : "The Force Awakens will be released in the UK on December 17th. The hundred day countdown starts today! http:\/\/t.co\/GRlWtZfWMl",
    "id" : 641023410093494272,
    "created_at" : "2015-09-07 23:01:05 +0000",
    "user" : {
      "name" : "Star Wars UK",
      "screen_name" : "StarWarsUK",
      "protected" : false,
      "id_str" : "2250737179",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877111101216915456\/Ej0kX9J1_normal.jpg",
      "id" : 2250737179,
      "verified" : true
    }
  },
  "id" : 641203510395150336,
  "created_at" : "2015-09-08 10:56:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 0, 9 ],
      "id_str" : "45756727",
      "id" : 45756727
    }, {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 10, 20 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641200986598457346",
  "geo" : { },
  "id_str" : "641203054994423808",
  "in_reply_to_user_id" : 45756727,
  "text" : "@LividEye @starduest What is TLT?",
  "id" : 641203054994423808,
  "in_reply_to_status_id" : 641200986598457346,
  "created_at" : "2015-09-08 10:54:55 +0000",
  "in_reply_to_screen_name" : "LividEye",
  "in_reply_to_user_id_str" : "45756727",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    }, {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 11, 20 ],
      "id_str" : "45756727",
      "id" : 45756727
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641199579757682688",
  "geo" : { },
  "id_str" : "641202953437773824",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest @LividEye Same over here.",
  "id" : 641202953437773824,
  "in_reply_to_status_id" : 641199579757682688,
  "created_at" : "2015-09-08 10:54:31 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/nLVa0iRwXU",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/philippine-cooks-over-worlds-kitchens-075358123.html",
      "display_url" : "sg.news.yahoo.com\/philippine-coo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641197833656340480",
  "text" : "Philippine cooks take over the world's kitchens https:\/\/t.co\/nLVa0iRwXU",
  "id" : 641197833656340480,
  "created_at" : "2015-09-08 10:34:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ge2015",
      "indices" : [ 101, 108 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641196583057530880",
  "text" : "Anyone campaigning on a platform of free open space (i.e. stop building shopping mall in every town) #ge2015",
  "id" : 641196583057530880,
  "created_at" : "2015-09-08 10:29:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641194646367019008",
  "geo" : { },
  "id_str" : "641195540529737732",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest Thinking of coming back...",
  "id" : 641195540529737732,
  "in_reply_to_status_id" : 641194646367019008,
  "created_at" : "2015-09-08 10:25:04 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 14, 24 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641195014169710593",
  "geo" : { },
  "id_str" : "641195397449445376",
  "in_reply_to_user_id" : 815066809,
  "text" : "@meowinginbkk @starduest Not going. I missed the period I suppose to register to vote.",
  "id" : 641195397449445376,
  "in_reply_to_status_id" : 641195014169710593,
  "created_at" : "2015-09-08 10:24:30 +0000",
  "in_reply_to_screen_name" : "ofmeowandbake",
  "in_reply_to_user_id_str" : "815066809",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "641191968454877184",
  "geo" : { },
  "id_str" : "641194208133517312",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest Same experience, 8 years in Ireland also changed the lens I judged Singapore, my home.",
  "id" : 641194208133517312,
  "in_reply_to_status_id" : 641191968454877184,
  "created_at" : "2015-09-08 10:19:46 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/IhjMKGdLtt",
      "expanded_url" : "https:\/\/twitter.com\/starduest\/status\/641154212487954432",
      "display_url" : "twitter.com\/starduest\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641193218105823232",
  "text" : "I thought Window Vista is the one. https:\/\/t.co\/IhjMKGdLtt",
  "id" : 641193218105823232,
  "created_at" : "2015-09-08 10:15:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Felicia Sonmez",
      "screen_name" : "feliciasonmez",
      "indices" : [ 3, 17 ],
      "id_str" : "137466464",
      "id" : 137466464
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChinaRealTime\/status\/641186851160723456\/photo\/1",
      "indices" : [ 97, 119 ],
      "url" : "http:\/\/t.co\/HPyWhOKWei",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COX0CwDVEAATNzX.png",
      "id_str" : "641186850065944576",
      "id" : 641186850065944576,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COX0CwDVEAATNzX.png",
      "sizes" : [ {
        "h" : 848,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 481,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 848,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 848,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HPyWhOKWei"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 96 ],
      "url" : "http:\/\/t.co\/vFem9RwpSm",
      "expanded_url" : "http:\/\/on.wsj.com\/1Nn5IXd",
      "display_url" : "on.wsj.com\/1Nn5IXd"
    } ]
  },
  "geo" : { },
  "id_str" : "641189042294509568",
  "text" : "RT @feliciasonmez: Has Bon Jovi's China tour been shot through the heart? http:\/\/t.co\/vFem9RwpSm http:\/\/t.co\/HPyWhOKWei By me for @ChinaRea\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "WSJ China Real Time",
        "screen_name" : "ChinaRealTime",
        "indices" : [ 111, 125 ],
        "id_str" : "16994529",
        "id" : 16994529
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChinaRealTime\/status\/641186851160723456\/photo\/1",
        "indices" : [ 78, 100 ],
        "url" : "http:\/\/t.co\/HPyWhOKWei",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COX0CwDVEAATNzX.png",
        "id_str" : "641186850065944576",
        "id" : 641186850065944576,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COX0CwDVEAATNzX.png",
        "sizes" : [ {
          "h" : 848,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 848,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 848,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HPyWhOKWei"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 77 ],
        "url" : "http:\/\/t.co\/vFem9RwpSm",
        "expanded_url" : "http:\/\/on.wsj.com\/1Nn5IXd",
        "display_url" : "on.wsj.com\/1Nn5IXd"
      } ]
    },
    "geo" : { },
    "id_str" : "641187241465765890",
    "text" : "Has Bon Jovi's China tour been shot through the heart? http:\/\/t.co\/vFem9RwpSm http:\/\/t.co\/HPyWhOKWei By me for @ChinaRealTime",
    "id" : 641187241465765890,
    "created_at" : "2015-09-08 09:52:05 +0000",
    "user" : {
      "name" : "Felicia Sonmez",
      "screen_name" : "feliciasonmez",
      "protected" : false,
      "id_str" : "137466464",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014338227866439686\/uGzjPIR5_normal.jpg",
      "id" : 137466464,
      "verified" : true
    }
  },
  "id" : 641189042294509568,
  "created_at" : "2015-09-08 09:59:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/FaL0QnmAKJ",
      "expanded_url" : "https:\/\/twitter.com\/lgreally\/status\/641187540364464128",
      "display_url" : "twitter.com\/lgreally\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "641188914070441984",
  "text" : "I aware that non-Irish national doctors and nurses leaving Ireland for the last couple of years to Australia.  https:\/\/t.co\/FaL0QnmAKJ",
  "id" : 641188914070441984,
  "created_at" : "2015-09-08 09:58:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "an xiao mina",
      "screen_name" : "anxiaostudio",
      "indices" : [ 3, 16 ],
      "id_str" : "17721892",
      "id" : 17721892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641140397473931264",
  "text" : "RT @anxiaostudio: The most problematic expression of this is often around technology -- rich world has phones and laptops, poor world does \u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "640928076780572673",
    "geo" : { },
    "id_str" : "640928644479610880",
    "in_reply_to_user_id" : 17721892,
    "text" : "The most problematic expression of this is often around technology -- rich world has phones and laptops, poor world does not and cannot.",
    "id" : 640928644479610880,
    "in_reply_to_status_id" : 640928076780572673,
    "created_at" : "2015-09-07 16:44:31 +0000",
    "in_reply_to_screen_name" : "anxiaostudio",
    "in_reply_to_user_id_str" : "17721892",
    "user" : {
      "name" : "an xiao mina",
      "screen_name" : "anxiaostudio",
      "protected" : false,
      "id_str" : "17721892",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/543432219109244928\/nuFAV2Ey_normal.jpeg",
      "id" : 17721892,
      "verified" : false
    }
  },
  "id" : 641140397473931264,
  "created_at" : "2015-09-08 06:45:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maureen Kelly",
      "screen_name" : "KRON4MKelly",
      "indices" : [ 3, 15 ],
      "id_str" : "90730707",
      "id" : 90730707
    }, {
      "name" : "KRON4 News",
      "screen_name" : "kron4news",
      "indices" : [ 81, 91 ],
      "id_str" : "19031057",
      "id" : 19031057
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/KRON4MKelly\/status\/640957647491960832\/photo\/1",
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/VBXPU4b24X",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COUjlZdVEAEregI.jpg",
      "id_str" : "640957647366197249",
      "id" : 640957647366197249,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COUjlZdVEAEregI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 389,
        "resize" : "fit",
        "w" : 519
      }, {
        "h" : 389,
        "resize" : "fit",
        "w" : 519
      }, {
        "h" : 389,
        "resize" : "fit",
        "w" : 519
      }, {
        "h" : 389,
        "resize" : "fit",
        "w" : 519
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VBXPU4b24X"
    } ],
    "hashtags" : [ {
      "text" : "SF",
      "indices" : [ 57, 60 ]
    }, {
      "text" : "turnthehatearound",
      "indices" : [ 62, 80 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641005111582584832",
  "text" : "RT @KRON4MKelly: Someone alters anti Chinese graffiti in #SF. #turnthehatearound @kron4news http:\/\/t.co\/VBXPU4b24X",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "KRON4 News",
        "screen_name" : "kron4news",
        "indices" : [ 64, 74 ],
        "id_str" : "19031057",
        "id" : 19031057
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KRON4MKelly\/status\/640957647491960832\/photo\/1",
        "indices" : [ 75, 97 ],
        "url" : "http:\/\/t.co\/VBXPU4b24X",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COUjlZdVEAEregI.jpg",
        "id_str" : "640957647366197249",
        "id" : 640957647366197249,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COUjlZdVEAEregI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 519
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 519
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 519
        }, {
          "h" : 389,
          "resize" : "fit",
          "w" : 519
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VBXPU4b24X"
      } ],
      "hashtags" : [ {
        "text" : "SF",
        "indices" : [ 40, 43 ]
      }, {
        "text" : "turnthehatearound",
        "indices" : [ 45, 63 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640957647491960832",
    "text" : "Someone alters anti Chinese graffiti in #SF. #turnthehatearound @kron4news http:\/\/t.co\/VBXPU4b24X",
    "id" : 640957647491960832,
    "created_at" : "2015-09-07 18:39:46 +0000",
    "user" : {
      "name" : "Maureen Kelly",
      "screen_name" : "KRON4MKelly",
      "protected" : false,
      "id_str" : "90730707",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000563473802\/8c720f4dd050aff29ee9cd4d31729a7c_normal.jpeg",
      "id" : 90730707,
      "verified" : true
    }
  },
  "id" : 641005111582584832,
  "created_at" : "2015-09-07 21:48:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641003527192621056",
  "text" : "Come to think about it news is a collection of \"What is wrong with our world?!\"",
  "id" : 641003527192621056,
  "created_at" : "2015-09-07 21:42:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9 ClaireByrneLive",
      "screen_name" : "ClaireByrneLive",
      "indices" : [ 3, 19 ],
      "id_str" : "2900855145",
      "id" : 2900855145
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cblive",
      "indices" : [ 113, 120 ]
    }, {
      "text" : "migrantcrisis",
      "indices" : [ 121, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "641000434342199296",
  "text" : "RT @ClaireByrneLive: Ian O\u2019Doherty: \u2018Where are the Government going to put refugees if not in direct provision?\u2019 #cblive #migrantcrisis",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "cblive",
        "indices" : [ 92, 99 ]
      }, {
        "text" : "migrantcrisis",
        "indices" : [ 100, 114 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640999446902407168",
    "text" : "Ian O\u2019Doherty: \u2018Where are the Government going to put refugees if not in direct provision?\u2019 #cblive #migrantcrisis",
    "id" : 640999446902407168,
    "created_at" : "2015-09-07 21:25:51 +0000",
    "user" : {
      "name" : "RT\u00C9 ClaireByrneLive",
      "screen_name" : "ClaireByrneLive",
      "protected" : false,
      "id_str" : "2900855145",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/557191239280254977\/6_y5Yert_normal.jpeg",
      "id" : 2900855145,
      "verified" : true
    }
  },
  "id" : 641000434342199296,
  "created_at" : "2015-09-07 21:29:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    }, {
      "name" : "Ireland \/ Bridget",
      "screen_name" : "ireland",
      "indices" : [ 31, 39 ],
      "id_str" : "1143551",
      "id" : 1143551
    }, {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "indices" : [ 125, 139 ],
      "id_str" : "52052800",
      "id" : 52052800
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640999566922391553",
  "text" : "RT @topgold: When we first got @Ireland tweeting 7 years ago, I never thought we would enjoy its proven diversity. Welcoming @DrUmarAlQadri\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ireland \/ Bridget",
        "screen_name" : "ireland",
        "indices" : [ 18, 26 ],
        "id_str" : "1143551",
        "id" : 1143551
      }, {
        "name" : "Shaykh Dr Umar Al-Qadri",
        "screen_name" : "DrUmarAlQadri",
        "indices" : [ 112, 126 ],
        "id_str" : "52052800",
        "id" : 52052800
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "640118033315405824",
    "geo" : { },
    "id_str" : "640613000924237824",
    "in_reply_to_user_id" : 52052800,
    "text" : "When we first got @Ireland tweeting 7 years ago, I never thought we would enjoy its proven diversity. Welcoming @DrUmarAlQadri next week.",
    "id" : 640613000924237824,
    "in_reply_to_status_id" : 640118033315405824,
    "created_at" : "2015-09-06 19:50:16 +0000",
    "in_reply_to_screen_name" : "DrUmarAlQadri",
    "in_reply_to_user_id_str" : "52052800",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 640999566922391553,
  "created_at" : "2015-09-07 21:26:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "indices" : [ 3, 17 ],
      "id_str" : "52052800",
      "id" : 52052800
    }, {
      "name" : "Ireland \/ Bridget",
      "screen_name" : "ireland",
      "indices" : [ 61, 69 ],
      "id_str" : "1143551",
      "id" : 1143551
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640999394108645377",
  "text" : "RT @DrUmarAlQadri: Excited to be the first Muslim curator of @ireland for one week from tomorrow morning. Hope it will increase understandi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ireland \/ Bridget",
        "screen_name" : "ireland",
        "indices" : [ 42, 50 ],
        "id_str" : "1143551",
        "id" : 1143551
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640621205901414400",
    "text" : "Excited to be the first Muslim curator of @ireland for one week from tomorrow morning. Hope it will increase understanding and acceptance.",
    "id" : 640621205901414400,
    "created_at" : "2015-09-06 20:22:52 +0000",
    "user" : {
      "name" : "Shaykh Dr Umar Al-Qadri",
      "screen_name" : "DrUmarAlQadri",
      "protected" : false,
      "id_str" : "52052800",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026918392907919360\/mMi9i844_normal.jpg",
      "id" : 52052800,
      "verified" : false
    }
  },
  "id" : 640999394108645377,
  "created_at" : "2015-09-07 21:25:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GE2015",
      "indices" : [ 45, 52 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640908350578946048",
  "text" : "Did any candidate champion for minimum wage? #GE2015",
  "id" : 640908350578946048,
  "created_at" : "2015-09-07 15:23:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/640903451061395456\/photo\/1",
      "indices" : [ 36, 58 ],
      "url" : "http:\/\/t.co\/0k0jlbJCE4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COTySwNWcAAcShN.jpg",
      "id_str" : "640903450985918464",
      "id" : 640903450985918464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COTySwNWcAAcShN.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0k0jlbJCE4"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 35 ],
      "url" : "http:\/\/t.co\/7u73no23Cv",
      "expanded_url" : "http:\/\/ift.tt\/1ENMcQM",
      "display_url" : "ift.tt\/1ENMcQM"
    } ]
  },
  "geo" : { },
  "id_str" : "640903451061395456",
  "text" : "\"Street Art\" http:\/\/t.co\/7u73no23Cv http:\/\/t.co\/0k0jlbJCE4",
  "id" : 640903451061395456,
  "created_at" : "2015-09-07 15:04:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 90, 98 ],
      "id_str" : "1652541",
      "id" : 1652541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 85 ],
      "url" : "http:\/\/t.co\/rt85SOstki",
      "expanded_url" : "http:\/\/reut.rs\/1ipQ2Wd",
      "display_url" : "reut.rs\/1ipQ2Wd"
    } ]
  },
  "geo" : { },
  "id_str" : "640896240029409281",
  "text" : "In rich Gulf Arab states, some feel shamed by refugee response http:\/\/t.co\/rt85SOstki via @Reuters",
  "id" : 640896240029409281,
  "created_at" : "2015-09-07 14:35:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/MbR44yhUxl",
      "expanded_url" : "https:\/\/twitter.com\/klillington\/status\/640872987676311552",
      "display_url" : "twitter.com\/klillington\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640875080545869824",
  "text" : "Ditto that. Maybe \"wantrepreneur\" might be more apt.  https:\/\/t.co\/MbR44yhUxl",
  "id" : 640875080545869824,
  "created_at" : "2015-09-07 13:11:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/w1sXX2GzNa",
      "expanded_url" : "https:\/\/twitter.com\/klillington\/status\/640869555179495425",
      "display_url" : "twitter.com\/klillington\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640874579989299200",
  "text" : "Another is mumtrepreneur (eye roll) https:\/\/t.co\/w1sXX2GzNa",
  "id" : 640874579989299200,
  "created_at" : "2015-09-07 13:09:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "housingcrisis",
      "indices" : [ 65, 79 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 103 ],
      "url" : "http:\/\/t.co\/x75FIaFNPq",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/we-ve-got-problems-in-ireland-but-this-is-different-the-migrant-crisis-hits-home-1.2340720#.Veqd8VRZutM.twitter",
      "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640869587261739008",
  "text" : "Why can't they extend the love to those homelessness in Ireland? #housingcrisis \nhttp:\/\/t.co\/x75FIaFNPq",
  "id" : 640869587261739008,
  "created_at" : "2015-09-07 12:49:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yahoo Mail",
      "screen_name" : "yahoomail",
      "indices" : [ 25, 35 ],
      "id_str" : "14870662",
      "id" : 14870662
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/V0HTeEo0VY",
      "expanded_url" : "https:\/\/twitter.com\/search?q=yahoo%20mail&src=typd",
      "display_url" : "twitter.com\/search?q=yahoo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640866274147475457",
  "text" : "I also have problem with @yahoomail  https:\/\/t.co\/V0HTeEo0VY",
  "id" : 640866274147475457,
  "created_at" : "2015-09-07 12:36:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 3, 13 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 136 ],
      "url" : "http:\/\/t.co\/og2ZNCW6wg",
      "expanded_url" : "http:\/\/thediplomat.com\/2015\/09\/singapores-impressive-food-security\/",
      "display_url" : "thediplomat.com\/2015\/09\/singap\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640864885098872832",
  "text" : "RT @starduest: We are first in food affordability apparently\n\nSingapore\u2019s Impressive Food Security | The Diplomat http:\/\/t.co\/og2ZNCW6wg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 121 ],
        "url" : "http:\/\/t.co\/og2ZNCW6wg",
        "expanded_url" : "http:\/\/thediplomat.com\/2015\/09\/singapores-impressive-food-security\/",
        "display_url" : "thediplomat.com\/2015\/09\/singap\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "640864425461854208",
    "text" : "We are first in food affordability apparently\n\nSingapore\u2019s Impressive Food Security | The Diplomat http:\/\/t.co\/og2ZNCW6wg",
    "id" : 640864425461854208,
    "created_at" : "2015-09-07 12:29:20 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 640864885098872832,
  "created_at" : "2015-09-07 12:31:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sassy Little Hobbit",
      "screen_name" : "LI_politico",
      "indices" : [ 3, 15 ],
      "id_str" : "166794157",
      "id" : 166794157
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/LI_politico\/status\/640706097590874112\/photo\/1",
      "indices" : [ 66, 88 ],
      "url" : "http:\/\/t.co\/p8trb3fatc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COQ-zAMUEAAc-yX.jpg",
      "id_str" : "640706092939350016",
      "id" : 640706092939350016,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COQ-zAMUEAAc-yX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 576
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/p8trb3fatc"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640796126988357632",
  "text" : "RT @LI_politico: There's so many confusing things happening here. http:\/\/t.co\/p8trb3fatc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LI_politico\/status\/640706097590874112\/photo\/1",
        "indices" : [ 49, 71 ],
        "url" : "http:\/\/t.co\/p8trb3fatc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COQ-zAMUEAAc-yX.jpg",
        "id_str" : "640706092939350016",
        "id" : 640706092939350016,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COQ-zAMUEAAc-yX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 576
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/p8trb3fatc"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640706097590874112",
    "text" : "There's so many confusing things happening here. http:\/\/t.co\/p8trb3fatc",
    "id" : 640706097590874112,
    "created_at" : "2015-09-07 02:00:12 +0000",
    "user" : {
      "name" : "Sassy Little Hobbit",
      "screen_name" : "LI_politico",
      "protected" : false,
      "id_str" : "166794157",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/889926571288662017\/cS9iGovo_normal.jpg",
      "id" : 166794157,
      "verified" : false
    }
  },
  "id" : 640796126988357632,
  "created_at" : "2015-09-07 07:57:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Prof Kalwant Bhopal",
      "screen_name" : "KalwantBhopal",
      "indices" : [ 3, 17 ],
      "id_str" : "2407630674",
      "id" : 2407630674
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/9kO8fsgH72",
      "expanded_url" : "https:\/\/www.timeshighereducation.co.uk\/news\/academics-minority-more-two-thirds-uk-universities",
      "display_url" : "timeshighereducation.co.uk\/news\/academics\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640793068988440577",
  "text" : "RT @KalwantBhopal: Academics in the minority at more than two-thirds of UK universities. This is astounding! https:\/\/t.co\/9kO8fsgH72 via @t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003EMobile Web\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TimesHigherEducation",
        "screen_name" : "timeshighered",
        "indices" : [ 118, 132 ],
        "id_str" : "23602600",
        "id" : 23602600
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/9kO8fsgH72",
        "expanded_url" : "https:\/\/www.timeshighereducation.co.uk\/news\/academics-minority-more-two-thirds-uk-universities",
        "display_url" : "timeshighereducation.co.uk\/news\/academics\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "640559564912480256",
    "text" : "Academics in the minority at more than two-thirds of UK universities. This is astounding! https:\/\/t.co\/9kO8fsgH72 via @timeshighered",
    "id" : 640559564912480256,
    "created_at" : "2015-09-06 16:17:55 +0000",
    "user" : {
      "name" : "Prof Kalwant Bhopal",
      "screen_name" : "KalwantBhopal",
      "protected" : false,
      "id_str" : "2407630674",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/583669283298877440\/mCay36pZ_normal.jpg",
      "id" : 2407630674,
      "verified" : false
    }
  },
  "id" : 640793068988440577,
  "created_at" : "2015-09-07 07:45:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Elliott",
      "screen_name" : "zenpeacekeeper",
      "indices" : [ 3, 18 ],
      "id_str" : "35667496",
      "id" : 35667496
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640792788083318784",
  "text" : "RT @zenpeacekeeper: I don't understand how possessing a cellphone makes someone a less convincing refugee. They're fleeing war, not visitin\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640696750701809664",
    "text" : "I don't understand how possessing a cellphone makes someone a less convincing refugee. They're fleeing war, not visiting from 18th century.",
    "id" : 640696750701809664,
    "created_at" : "2015-09-07 01:23:03 +0000",
    "user" : {
      "name" : "Marianne Elliott",
      "screen_name" : "zenpeacekeeper",
      "protected" : false,
      "id_str" : "35667496",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023654479516774400\/8QCyjosK_normal.jpg",
      "id" : 35667496,
      "verified" : true
    }
  },
  "id" : 640792788083318784,
  "created_at" : "2015-09-07 07:44:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Open Minds",
      "screen_name" : "MindsOpened",
      "indices" : [ 3, 15 ],
      "id_str" : "1870056282",
      "id" : 1870056282
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640792679857655808",
  "text" : "RT @MindsOpened: Be kind to everything that lives.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.kucukbeyin.com\/\" rel=\"nofollow\"\u003Efahdjfasdfajsdlkfjalsj\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640791367963308032",
    "text" : "Be kind to everything that lives.",
    "id" : 640791367963308032,
    "created_at" : "2015-09-07 07:39:02 +0000",
    "user" : {
      "name" : "Open Minds",
      "screen_name" : "MindsOpened",
      "protected" : false,
      "id_str" : "1870056282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/473116416615280641\/_NLFmh-w_normal.jpeg",
      "id" : 1870056282,
      "verified" : false
    }
  },
  "id" : 640792679857655808,
  "created_at" : "2015-09-07 07:44:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rebecca Lynn",
      "screen_name" : "VCRebecca",
      "indices" : [ 3, 13 ],
      "id_str" : "144400453",
      "id" : 144400453
    }, {
      "name" : "Yelp",
      "screen_name" : "Yelp",
      "indices" : [ 62, 67 ],
      "id_str" : "17215802",
      "id" : 17215802
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/VCRebecca\/status\/639570005848231937\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/hKtYxLdULb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COA1iAJVAAAqLyB.png",
      "id_str" : "639570005357559808",
      "id" : 639570005357559808,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COA1iAJVAAAqLyB.png",
      "sizes" : [ {
        "h" : 813,
        "resize" : "fit",
        "w" : 1469
      }, {
        "h" : 376,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 664,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 813,
        "resize" : "fit",
        "w" : 1469
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hKtYxLdULb"
    } ],
    "hashtags" : [ {
      "text" : "machinelearning",
      "indices" : [ 91, 107 ]
    } ],
    "urls" : [ {
      "indices" : [ 68, 90 ],
      "url" : "http:\/\/t.co\/oPTHKS0rlD",
      "expanded_url" : "http:\/\/bit.ly\/1JR0HU2",
      "display_url" : "bit.ly\/1JR0HU2"
    } ]
  },
  "geo" : { },
  "id_str" : "640655254296637440",
  "text" : "RT @VCRebecca: Automatically Categorizing Yelp Businesses via @Yelp http:\/\/t.co\/oPTHKS0rlD #machinelearning http:\/\/t.co\/hKtYxLdULb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/6builder.com\" rel=\"nofollow\"\u003E6Builder\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Yelp",
        "screen_name" : "Yelp",
        "indices" : [ 47, 52 ],
        "id_str" : "17215802",
        "id" : 17215802
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/VCRebecca\/status\/639570005848231937\/photo\/1",
        "indices" : [ 93, 115 ],
        "url" : "http:\/\/t.co\/hKtYxLdULb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COA1iAJVAAAqLyB.png",
        "id_str" : "639570005357559808",
        "id" : 639570005357559808,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COA1iAJVAAAqLyB.png",
        "sizes" : [ {
          "h" : 813,
          "resize" : "fit",
          "w" : 1469
        }, {
          "h" : 376,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 664,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 813,
          "resize" : "fit",
          "w" : 1469
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hKtYxLdULb"
      } ],
      "hashtags" : [ {
        "text" : "machinelearning",
        "indices" : [ 76, 92 ]
      } ],
      "urls" : [ {
        "indices" : [ 53, 75 ],
        "url" : "http:\/\/t.co\/oPTHKS0rlD",
        "expanded_url" : "http:\/\/bit.ly\/1JR0HU2",
        "display_url" : "bit.ly\/1JR0HU2"
      } ]
    },
    "geo" : { },
    "id_str" : "639570005848231937",
    "text" : "Automatically Categorizing Yelp Businesses via @Yelp http:\/\/t.co\/oPTHKS0rlD #machinelearning http:\/\/t.co\/hKtYxLdULb",
    "id" : 639570005848231937,
    "created_at" : "2015-09-03 22:45:46 +0000",
    "user" : {
      "name" : "Rebecca Lynn",
      "screen_name" : "VCRebecca",
      "protected" : false,
      "id_str" : "144400453",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/581686231110590465\/bPrcWPHT_normal.jpg",
      "id" : 144400453,
      "verified" : false
    }
  },
  "id" : 640655254296637440,
  "created_at" : "2015-09-06 22:38:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/pTK6YrMPx1",
      "expanded_url" : "https:\/\/twitter.com\/starduest\/status\/640564536760770560",
      "display_url" : "twitter.com\/starduest\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640575963567386624",
  "text" : "A little bird inform me that Junior Doctor on weekend shift got reprimand for calling those experience Consultant.  https:\/\/t.co\/pTK6YrMPx1",
  "id" : 640575963567386624,
  "created_at" : "2015-09-06 17:23:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IrelandBeforeYouDie",
      "screen_name" : "IrelandB4UDie",
      "indices" : [ 3, 17 ],
      "id_str" : "2415991542",
      "id" : 2415991542
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IrelandB4UDie\/status\/640566429981495297\/photo\/1",
      "indices" : [ 116, 138 ],
      "url" : "http:\/\/t.co\/qOShbcj83B",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COO_xibW8AAlKni.jpg",
      "id_str" : "640566429792792576",
      "id" : 640566429792792576,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COO_xibW8AAlKni.jpg",
      "sizes" : [ {
        "h" : 400,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qOShbcj83B"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 115 ],
      "url" : "http:\/\/t.co\/YtDcmCr5sH",
      "expanded_url" : "http:\/\/goo.gl\/wBRvAR",
      "display_url" : "goo.gl\/wBRvAR"
    } ]
  },
  "geo" : { },
  "id_str" : "640574469841518592",
  "text" : "RT @IrelandB4UDie: The Leinster Bucket List: 28 AMAZING places to experience before you die. http:\/\/t.co\/YtDcmCr5sH http:\/\/t.co\/qOShbcj83B",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/postcron.com\" rel=\"nofollow\"\u003EPostcron App\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrelandB4UDie\/status\/640566429981495297\/photo\/1",
        "indices" : [ 97, 119 ],
        "url" : "http:\/\/t.co\/qOShbcj83B",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COO_xibW8AAlKni.jpg",
        "id_str" : "640566429792792576",
        "id" : 640566429792792576,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COO_xibW8AAlKni.jpg",
        "sizes" : [ {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qOShbcj83B"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 96 ],
        "url" : "http:\/\/t.co\/YtDcmCr5sH",
        "expanded_url" : "http:\/\/goo.gl\/wBRvAR",
        "display_url" : "goo.gl\/wBRvAR"
      } ]
    },
    "geo" : { },
    "id_str" : "640566429981495297",
    "text" : "The Leinster Bucket List: 28 AMAZING places to experience before you die. http:\/\/t.co\/YtDcmCr5sH http:\/\/t.co\/qOShbcj83B",
    "id" : 640566429981495297,
    "created_at" : "2015-09-06 16:45:12 +0000",
    "user" : {
      "name" : "IrelandBeforeYouDie",
      "screen_name" : "IrelandB4UDie",
      "protected" : false,
      "id_str" : "2415991542",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/740795959903457280\/L7yWCg0K_normal.jpg",
      "id" : 2415991542,
      "verified" : false
    }
  },
  "id" : 640574469841518592,
  "created_at" : "2015-09-06 17:17:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft Singapore",
      "screen_name" : "Microsoft_SG",
      "indices" : [ 0, 13 ],
      "id_str" : "229311774",
      "id" : 229311774
    }, {
      "name" : "SonySingapore",
      "screen_name" : "SonySingapore",
      "indices" : [ 15, 29 ],
      "id_str" : "87147796",
      "id" : 87147796
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640573446683013120",
  "in_reply_to_user_id" : 229311774,
  "text" : "@Microsoft_SG  @sonysingapore How can I find Win 8.1 product key? I can't reboot from my vaio laptop.  Need to perform a clean installation",
  "id" : 640573446683013120,
  "created_at" : "2015-09-06 17:13:05 +0000",
  "in_reply_to_screen_name" : "Microsoft_SG",
  "in_reply_to_user_id_str" : "229311774",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 69 ],
      "url" : "http:\/\/t.co\/zvGyyEbQVe",
      "expanded_url" : "http:\/\/www.makeuseof.com\/tag\/four-places-find-windows-8-product-key\/",
      "display_url" : "makeuseof.com\/tag\/four-place\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640572841956638720",
  "text" : "Four Places To Find Your Windows 8 Product Key http:\/\/t.co\/zvGyyEbQVe",
  "id" : 640572841956638720,
  "created_at" : "2015-09-06 17:10:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 102 ],
      "url" : "http:\/\/t.co\/i3iogHwPjU",
      "expanded_url" : "http:\/\/johnislam.blogspot.ie\/2014\/02\/lam-on-whether-our-military-spending-is.html",
      "display_url" : "johnislam.blogspot.ie\/2014\/02\/lam-on\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640507158048673793",
  "text" : "Can Singapore cut military spending by 44%? A blogger's Monte Carlo simulations.http:\/\/t.co\/i3iogHwPjU Data Analysis FTW",
  "id" : 640507158048673793,
  "created_at" : "2015-09-06 12:49:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/K8Bnmyl8GZ",
      "expanded_url" : "https:\/\/www.facebook.com\/photo.php?fbid=10153591600018545&set=a.10150177977848545.310648.500628544&type=1&fref=nf",
      "display_url" : "facebook.com\/photo.php?fbid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640503829616828417",
  "text" : "https:\/\/t.co\/K8Bnmyl8GZ\n\"It's 1984 and my mother arrives in the UK with 89 other Vietnamese refugees known as the boat people\"",
  "id" : 640503829616828417,
  "created_at" : "2015-09-06 12:36:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "indices" : [ 3, 15 ],
      "id_str" : "15191926",
      "id" : 15191926
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/clementchio\/status\/640498385091461120\/photo\/1",
      "indices" : [ 69, 91 ],
      "url" : "http:\/\/t.co\/EI6UGrKzpS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COOB4z0W8AAtN7z.jpg",
      "id_str" : "640498384999215104",
      "id" : 640498384999215104,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COOB4z0W8AAtN7z.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EI6UGrKzpS"
    } ],
    "hashtags" : [ {
      "text" : "WPrally",
      "indices" : [ 17, 25 ]
    }, {
      "text" : "GE2015",
      "indices" : [ 38, 45 ]
    } ],
    "urls" : [ {
      "indices" : [ 46, 68 ],
      "url" : "http:\/\/t.co\/qYesemvmWa",
      "expanded_url" : "http:\/\/ift.tt\/1LcRzG3",
      "display_url" : "ift.tt\/1LcRzG3"
    } ]
  },
  "geo" : { },
  "id_str" : "640500473703858176",
  "text" : "RT @clementchio: #WPrally Box Seats.\n\n#GE2015 http:\/\/t.co\/qYesemvmWa http:\/\/t.co\/EI6UGrKzpS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/clementchio\/status\/640498385091461120\/photo\/1",
        "indices" : [ 52, 74 ],
        "url" : "http:\/\/t.co\/EI6UGrKzpS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COOB4z0W8AAtN7z.jpg",
        "id_str" : "640498384999215104",
        "id" : 640498384999215104,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COOB4z0W8AAtN7z.jpg",
        "sizes" : [ {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EI6UGrKzpS"
      } ],
      "hashtags" : [ {
        "text" : "WPrally",
        "indices" : [ 0, 8 ]
      }, {
        "text" : "GE2015",
        "indices" : [ 21, 28 ]
      } ],
      "urls" : [ {
        "indices" : [ 29, 51 ],
        "url" : "http:\/\/t.co\/qYesemvmWa",
        "expanded_url" : "http:\/\/ift.tt\/1LcRzG3",
        "display_url" : "ift.tt\/1LcRzG3"
      } ]
    },
    "geo" : { },
    "id_str" : "640498385091461120",
    "text" : "#WPrally Box Seats.\n\n#GE2015 http:\/\/t.co\/qYesemvmWa http:\/\/t.co\/EI6UGrKzpS",
    "id" : 640498385091461120,
    "created_at" : "2015-09-06 12:14:49 +0000",
    "user" : {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "protected" : false,
      "id_str" : "15191926",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/582481791899971584\/UZf4Qfb2_normal.jpg",
      "id" : 15191926,
      "verified" : false
    }
  },
  "id" : 640500473703858176,
  "created_at" : "2015-09-06 12:23:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/AtsrTLs7bj",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/640497720721977344",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640500378195369984",
  "text" : "\"You cannot do it by just taxing the top 1%. ...because the top 1% know how to move their $ around\"  https:\/\/t.co\/AtsrTLs7bj",
  "id" : 640500378195369984,
  "created_at" : "2015-09-06 12:22:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clement Chio",
      "screen_name" : "clementchio",
      "indices" : [ 0, 12 ],
      "id_str" : "15191926",
      "id" : 15191926
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "640486501797654532",
  "geo" : { },
  "id_str" : "640498516633210880",
  "in_reply_to_user_id" : 15191926,
  "text" : "@clementchio For a moment I thought you referring that Gurmit Singh.",
  "id" : 640498516633210880,
  "in_reply_to_status_id" : 640486501797654532,
  "created_at" : "2015-09-06 12:15:20 +0000",
  "in_reply_to_screen_name" : "clementchio",
  "in_reply_to_user_id_str" : "15191926",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cherian George",
      "screen_name" : "cheriangeorge",
      "indices" : [ 0, 14 ],
      "id_str" : "22563086",
      "id" : 22563086
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "640186219066880000",
  "geo" : { },
  "id_str" : "640218080212328448",
  "in_reply_to_user_id" : 22563086,
  "text" : "@cheriangeorge @meowinginbkk My 1st time come across a diamond shape graph and I can't make sense out of it.",
  "id" : 640218080212328448,
  "in_reply_to_status_id" : 640186219066880000,
  "created_at" : "2015-09-05 17:40:59 +0000",
  "in_reply_to_screen_name" : "cheriangeorge",
  "in_reply_to_user_id_str" : "22563086",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Valerio De Cesaris",
      "screen_name" : "ValerioDeC",
      "indices" : [ 3, 14 ],
      "id_str" : "388118396",
      "id" : 388118396
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640217444536205312",
  "text" : "RT @ValerioDeC: When in 1956 120,000 Hungarian refugees were resettled in Europe, no one in Budapest suggested anti-immigrant walls http:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ValerioDeC\/status\/626371307253407746\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/e3mMuaNICI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CLFRWC6WUAAuhhX.jpg",
        "id_str" : "626371262361784320",
        "id" : 626371262361784320,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CLFRWC6WUAAuhhX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 328,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 328,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 328,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 328,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/e3mMuaNICI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "626371307253407746",
    "text" : "When in 1956 120,000 Hungarian refugees were resettled in Europe, no one in Budapest suggested anti-immigrant walls http:\/\/t.co\/e3mMuaNICI",
    "id" : 626371307253407746,
    "created_at" : "2015-07-29 12:38:51 +0000",
    "user" : {
      "name" : "Valerio De Cesaris",
      "screen_name" : "ValerioDeC",
      "protected" : false,
      "id_str" : "388118396",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/558203678528524288\/lCcdUH4f_normal.jpeg",
      "id" : 388118396,
      "verified" : false
    }
  },
  "id" : 640217444536205312,
  "created_at" : "2015-09-05 17:38:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 105 ],
      "url" : "http:\/\/t.co\/I2uXru99Xe",
      "expanded_url" : "http:\/\/mothership.sg\/2015\/09\/this-photo-of-foreign-workers-toiling-at-singfirsts-ge2015-rally-is-kind-of-ironic\/",
      "display_url" : "mothership.sg\/2015\/09\/this-p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640148376688939008",
  "text" : "Oh the irony! Photo of foreign workers toiling at Singaporean First\u2019s GE2015 rally http:\/\/t.co\/I2uXru99Xe",
  "id" : 640148376688939008,
  "created_at" : "2015-09-05 13:04:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "640140746796863488",
  "geo" : { },
  "id_str" : "640146852986388481",
  "in_reply_to_user_id" : 19288130,
  "text" : "@damacfad I think the unemployment rates include both citizens and non-citizens",
  "id" : 640146852986388481,
  "in_reply_to_status_id" : 640140746796863488,
  "created_at" : "2015-09-05 12:57:57 +0000",
  "in_reply_to_screen_name" : "declanmacfadden",
  "in_reply_to_user_id_str" : "19288130",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/LYVU2ubc3E",
      "expanded_url" : "https:\/\/twitter.com\/MuireannNiR\/status\/640142581939195904",
      "display_url" : "twitter.com\/MuireannNiR\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640146228211224576",
  "text" : "Ireland you are brilliant! https:\/\/t.co\/LYVU2ubc3E",
  "id" : 640146228211224576,
  "created_at" : "2015-09-05 12:55:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 3, 12 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640136692528230400",
  "text" : "RT @maryrose: standing at famine memorial to show politicians that Irish people care about what happens to fellow humans, and not to be afr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640132995417022464",
    "text" : "standing at famine memorial to show politicians that Irish people care about what happens to fellow humans, and not to be afraid to be bold",
    "id" : 640132995417022464,
    "created_at" : "2015-09-05 12:02:53 +0000",
    "user" : {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "protected" : false,
      "id_str" : "1291131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034068926370594816\/eS9-sUNe_normal.jpg",
      "id" : 1291131,
      "verified" : false
    }
  },
  "id" : 640136692528230400,
  "created_at" : "2015-09-05 12:17:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ENGvIRE",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640133321842913280",
  "text" : "#ENGvIRE starting soon in an hour time. Time to leave Twitter for a while.",
  "id" : 640133321842913280,
  "created_at" : "2015-09-05 12:04:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hengah",
      "indices" : [ 28, 35 ]
    }, {
      "text" : "sgelections",
      "indices" : [ 36, 48 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640132594080841728",
  "text" : "lim swee say not in my name #hengah #sgelections",
  "id" : 640132594080841728,
  "created_at" : "2015-09-05 12:01:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Data",
      "screen_name" : "IrishTimesData",
      "indices" : [ 3, 18 ],
      "id_str" : "2991914909",
      "id" : 2991914909
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 134 ],
      "url" : "http:\/\/t.co\/nEZU4ioaeU",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/world\/syrian-refugees-international-numbers-in-an-irish-context-1.2340748",
      "display_url" : "irishtimes.com\/news\/world\/syr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "640132197685575680",
  "text" : "RT @IrishTimesData: No. of registered Syrian refugees reached 4.1m last month: the numbers in an Irish context.\nhttp:\/\/t.co\/nEZU4ioaeU http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrishTimesData\/status\/640124242802020352\/photo\/1",
        "indices" : [ 115, 137 ],
        "url" : "http:\/\/t.co\/REvibVattS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COGI8M5WIAA2hpj.png",
        "id_str" : "639943189898600448",
        "id" : 639943189898600448,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COGI8M5WIAA2hpj.png",
        "sizes" : [ {
          "h" : 394,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 394,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 394,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 394,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/REvibVattS"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 92, 114 ],
        "url" : "http:\/\/t.co\/nEZU4ioaeU",
        "expanded_url" : "http:\/\/www.irishtimes.com\/news\/world\/syrian-refugees-international-numbers-in-an-irish-context-1.2340748",
        "display_url" : "irishtimes.com\/news\/world\/syr\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "640124242802020352",
    "text" : "No. of registered Syrian refugees reached 4.1m last month: the numbers in an Irish context.\nhttp:\/\/t.co\/nEZU4ioaeU http:\/\/t.co\/REvibVattS",
    "id" : 640124242802020352,
    "created_at" : "2015-09-05 11:28:07 +0000",
    "user" : {
      "name" : "Irish Times Data",
      "screen_name" : "IrishTimesData",
      "protected" : false,
      "id_str" : "2991914909",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/562631235785076737\/uIvlV_KD_normal.png",
      "id" : 2991914909,
      "verified" : false
    }
  },
  "id" : 640132197685575680,
  "created_at" : "2015-09-05 11:59:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "hengah",
      "indices" : [ 26, 33 ]
    }, {
      "text" : "sgelections",
      "indices" : [ 35, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640131299156578304",
  "text" : "There should be a hashtag #hengah. #sgelections",
  "id" : 640131299156578304,
  "created_at" : "2015-09-05 11:56:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Mates",
      "screen_name" : "jamesmatesitv",
      "indices" : [ 3, 17 ],
      "id_str" : "85590185",
      "id" : 85590185
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640131170580172800",
  "text" : "RT @jamesmatesitv: Finnish PM winning much praise for opening his home to Syrian refugees. Turns out it's his spare home. Still good, I gue\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "640102461856989184",
    "text" : "Finnish PM winning much praise for opening his home to Syrian refugees. Turns out it's his spare home. Still good, I guess.",
    "id" : 640102461856989184,
    "created_at" : "2015-09-05 10:01:34 +0000",
    "user" : {
      "name" : "James Mates",
      "screen_name" : "jamesmatesitv",
      "protected" : false,
      "id_str" : "85590185",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/776726732799868928\/XGjiXmw0_normal.jpg",
      "id" : 85590185,
      "verified" : true
    }
  },
  "id" : 640131170580172800,
  "created_at" : "2015-09-05 11:55:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "normalcy enjoyer",
      "screen_name" : "AbrasiveGhost",
      "indices" : [ 3, 17 ],
      "id_str" : "2901991664",
      "id" : 2901991664
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640103364756381697",
  "text" : "RT @AbrasiveGhost: SON: Dad whats Twitter\n\nME: u know when one dog barks &amp; every single one in the area starts howling?\n\nSON: ya\n\nME: its l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639601942973562880",
    "text" : "SON: Dad whats Twitter\n\nME: u know when one dog barks &amp; every single one in the area starts howling?\n\nSON: ya\n\nME: its like that but all day",
    "id" : 639601942973562880,
    "created_at" : "2015-09-04 00:52:41 +0000",
    "user" : {
      "name" : "normalcy enjoyer",
      "screen_name" : "AbrasiveGhost",
      "protected" : false,
      "id_str" : "2901991664",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/981700008830521347\/lkuXa6MD_normal.jpg",
      "id" : 2901991664,
      "verified" : false
    }
  },
  "id" : 640103364756381697,
  "created_at" : "2015-09-05 10:05:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elena Schulte",
      "screen_name" : "SchulteElena",
      "indices" : [ 3, 16 ],
      "id_str" : "2785501093",
      "id" : 2785501093
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ux",
      "indices" : [ 100, 103 ]
    }, {
      "text" : "research",
      "indices" : [ 104, 113 ]
    }, {
      "text" : "usability",
      "indices" : [ 114, 124 ]
    } ],
    "urls" : [ {
      "indices" : [ 77, 99 ],
      "url" : "http:\/\/t.co\/rrhi4hmv4N",
      "expanded_url" : "http:\/\/ow.ly\/RAZzt",
      "display_url" : "ow.ly\/RAZzt"
    } ]
  },
  "geo" : { },
  "id_str" : "640099355911962628",
  "text" : "RT @SchulteElena: \u201C10 Things I Learned From Taking Over 100 Usability Tests\u201D http:\/\/t.co\/rrhi4hmv4N #ux #research #usability",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ux",
        "indices" : [ 82, 85 ]
      }, {
        "text" : "research",
        "indices" : [ 86, 95 ]
      }, {
        "text" : "usability",
        "indices" : [ 96, 106 ]
      } ],
      "urls" : [ {
        "indices" : [ 59, 81 ],
        "url" : "http:\/\/t.co\/rrhi4hmv4N",
        "expanded_url" : "http:\/\/ow.ly\/RAZzt",
        "display_url" : "ow.ly\/RAZzt"
      } ]
    },
    "geo" : { },
    "id_str" : "640031811150708736",
    "text" : "\u201C10 Things I Learned From Taking Over 100 Usability Tests\u201D http:\/\/t.co\/rrhi4hmv4N #ux #research #usability",
    "id" : 640031811150708736,
    "created_at" : "2015-09-05 05:20:49 +0000",
    "user" : {
      "name" : "Elena Schulte",
      "screen_name" : "SchulteElena",
      "protected" : false,
      "id_str" : "2785501093",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/641267457433452544\/ergNAsYC_normal.jpg",
      "id" : 2785501093,
      "verified" : false
    }
  },
  "id" : 640099355911962628,
  "created_at" : "2015-09-05 09:49:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640098299182194688",
  "text" : "Learn coding to solve world problem. Use data to solve world problem. There seems to be no shortage of these kinds of thing.",
  "id" : 640098299182194688,
  "created_at" : "2015-09-05 09:45:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Conor McGregor",
      "screen_name" : "TheNotoriousMMA",
      "indices" : [ 3, 19 ],
      "id_str" : "369583954",
      "id" : 369583954
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "640096374206394368",
  "text" : "RT @TheNotoriousMMA: This life isn't for everyone. It's for the insanely driven individual.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "636468462164967424",
    "text" : "This life isn't for everyone. It's for the insanely driven individual.",
    "id" : 636468462164967424,
    "created_at" : "2015-08-26 09:21:21 +0000",
    "user" : {
      "name" : "Conor McGregor",
      "screen_name" : "TheNotoriousMMA",
      "protected" : false,
      "id_str" : "369583954",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/798351849984294912\/okhePpJW_normal.jpg",
      "id" : 369583954,
      "verified" : true
    }
  },
  "id" : 640096374206394368,
  "created_at" : "2015-09-05 09:37:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lawrence Jones MBE",
      "screen_name" : "Lawrence_Jones",
      "indices" : [ 3, 18 ],
      "id_str" : "36912401",
      "id" : 36912401
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "perk",
      "indices" : [ 95, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/Rf9TXIImHn",
      "expanded_url" : "https:\/\/twitter.com\/ukfast\/status\/639715477732859905",
      "display_url" : "twitter.com\/ukfast\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639929202054164480",
  "text" : "RT @Lawrence_Jones: Boardroom breakfast for the account managers and sales team every Friday. \n#perk  https:\/\/t.co\/Rf9TXIImHn",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "perk",
        "indices" : [ 75, 80 ]
      } ],
      "urls" : [ {
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/Rf9TXIImHn",
        "expanded_url" : "https:\/\/twitter.com\/ukfast\/status\/639715477732859905",
        "display_url" : "twitter.com\/ukfast\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "639914731676024833",
    "text" : "Boardroom breakfast for the account managers and sales team every Friday. \n#perk  https:\/\/t.co\/Rf9TXIImHn",
    "id" : 639914731676024833,
    "created_at" : "2015-09-04 21:35:35 +0000",
    "user" : {
      "name" : "Lawrence Jones MBE",
      "screen_name" : "Lawrence_Jones",
      "protected" : false,
      "id_str" : "36912401",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/700910743814688768\/qSPs3ngr_normal.jpg",
      "id" : 36912401,
      "verified" : true
    }
  },
  "id" : 639929202054164480,
  "created_at" : "2015-09-04 22:33:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mustafa al-Najafi",
      "screen_name" : "MustafaNajafi",
      "indices" : [ 3, 17 ],
      "id_str" : "213625224",
      "id" : 213625224
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639914127092264960",
  "text" : "RT @MustafaNajafi: Iraqis carry posters of Angela Merkel in protests praising her &amp; saying she's done more for them than Iraqi officials ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MustafaNajafi\/status\/639907362418069504\/photo\/1",
        "indices" : [ 122, 144 ],
        "url" : "http:\/\/t.co\/3AYPnD9QWv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COFoWWzW8AAWnew.jpg",
        "id_str" : "639907355350724608",
        "id" : 639907355350724608,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COFoWWzW8AAWnew.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 436
        }, {
          "h" : 718,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 718,
          "resize" : "fit",
          "w" : 460
        }, {
          "h" : 718,
          "resize" : "fit",
          "w" : 460
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3AYPnD9QWv"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MustafaNajafi\/status\/639907362418069504\/photo\/1",
        "indices" : [ 122, 144 ],
        "url" : "http:\/\/t.co\/3AYPnD9QWv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COFoWW5WoAAuTYy.jpg",
        "id_str" : "639907355375869952",
        "id" : 639907355375869952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COFoWW5WoAAuTYy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 540,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3AYPnD9QWv"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MustafaNajafi\/status\/639907362418069504\/photo\/1",
        "indices" : [ 122, 144 ],
        "url" : "http:\/\/t.co\/3AYPnD9QWv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COFoWXIWgAQX4QE.jpg",
        "id_str" : "639907355438776324",
        "id" : 639907355438776324,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COFoWXIWgAQX4QE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3AYPnD9QWv"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639907362418069504",
    "text" : "Iraqis carry posters of Angela Merkel in protests praising her &amp; saying she's done more for them than Iraqi officials http:\/\/t.co\/3AYPnD9QWv",
    "id" : 639907362418069504,
    "created_at" : "2015-09-04 21:06:18 +0000",
    "user" : {
      "name" : "Mustafa al-Najafi",
      "screen_name" : "MustafaNajafi",
      "protected" : false,
      "id_str" : "213625224",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040210479392673793\/cT2hYbDJ_normal.jpg",
      "id" : 213625224,
      "verified" : false
    }
  },
  "id" : 639914127092264960,
  "created_at" : "2015-09-04 21:33:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Her.ie",
      "screen_name" : "Herdotie",
      "indices" : [ 3, 12 ],
      "id_str" : "580089228",
      "id" : 580089228
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Herdotie\/status\/639909598971891712\/photo\/1",
      "indices" : [ 118, 140 ],
      "url" : "http:\/\/t.co\/kvrxUPWqGV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COFqY8kVAAAN8Gj.jpg",
      "id_str" : "639909598871224320",
      "id" : 639909598871224320,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COFqY8kVAAAN8Gj.jpg",
      "sizes" : [ {
        "h" : 340,
        "resize" : "fit",
        "w" : 647
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 647
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 647
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 647
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/kvrxUPWqGV"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/oq2TErJGHD",
      "expanded_url" : "http:\/\/bit.ly\/1VCjfvp",
      "display_url" : "bit.ly\/1VCjfvp"
    } ]
  },
  "geo" : { },
  "id_str" : "639911046946365441",
  "text" : "RT @Herdotie: PICS: Female Graduates Reveal The Worst Things They Were Asked At Job Interviews http:\/\/t.co\/oq2TErJGHD http:\/\/t.co\/kvrxUPWqGV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Herdotie\/status\/639909598971891712\/photo\/1",
        "indices" : [ 104, 126 ],
        "url" : "http:\/\/t.co\/kvrxUPWqGV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COFqY8kVAAAN8Gj.jpg",
        "id_str" : "639909598871224320",
        "id" : 639909598871224320,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COFqY8kVAAAN8Gj.jpg",
        "sizes" : [ {
          "h" : 340,
          "resize" : "fit",
          "w" : 647
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 647
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 647
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 647
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kvrxUPWqGV"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 103 ],
        "url" : "http:\/\/t.co\/oq2TErJGHD",
        "expanded_url" : "http:\/\/bit.ly\/1VCjfvp",
        "display_url" : "bit.ly\/1VCjfvp"
      } ]
    },
    "geo" : { },
    "id_str" : "639909598971891712",
    "text" : "PICS: Female Graduates Reveal The Worst Things They Were Asked At Job Interviews http:\/\/t.co\/oq2TErJGHD http:\/\/t.co\/kvrxUPWqGV",
    "id" : 639909598971891712,
    "created_at" : "2015-09-04 21:15:11 +0000",
    "user" : {
      "name" : "Her.ie",
      "screen_name" : "Herdotie",
      "protected" : false,
      "id_str" : "580089228",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/760779763380264960\/jMFfyqtG_normal.jpg",
      "id" : 580089228,
      "verified" : true
    }
  },
  "id" : 639911046946365441,
  "created_at" : "2015-09-04 21:20:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639848190615928832",
  "text" : "Ikea is actually a Swedish word for argument.",
  "id" : 639848190615928832,
  "created_at" : "2015-09-04 17:11:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 67 ],
      "url" : "http:\/\/t.co\/XiVyypkpXm",
      "expanded_url" : "http:\/\/blogs.sas.com\/content\/customeranalytics\/2014\/11\/17\/marketing-analytics-for-attribution-modeling\/",
      "display_url" : "blogs.sas.com\/content\/custom\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639794431818903555",
  "text" : "Marketing analytics for attribution modeling http:\/\/t.co\/XiVyypkpXm",
  "id" : 639794431818903555,
  "created_at" : "2015-09-04 13:37:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/5LX4zhuSGW",
      "expanded_url" : "https:\/\/twitter.com\/meowinginbkk\/status\/639788019638112256",
      "display_url" : "twitter.com\/meowinginbkk\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639789609044545536",
  "text" : "Imagine this is the type of party that going to take Singapore to the next 50 years?! https:\/\/t.co\/5LX4zhuSGW",
  "id" : 639789609044545536,
  "created_at" : "2015-09-04 13:18:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Horizon 2020\uD83C\uDDEA\uD83C\uDDFA",
      "screen_name" : "EU_H2020",
      "indices" : [ 3, 12 ],
      "id_str" : "1379662802",
      "id" : 1379662802
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/EU_H2020\/status\/639698187977003008\/photo\/1",
      "indices" : [ 111, 133 ],
      "url" : "http:\/\/t.co\/apNZENmgq5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN-KMYyWoAArxTL.jpg",
      "id_str" : "639381617526611968",
      "id" : 639381617526611968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN-KMYyWoAArxTL.jpg",
      "sizes" : [ {
        "h" : 999,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 523
      }, {
        "h" : 999,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 999,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/apNZENmgq5"
    } ],
    "hashtags" : [ {
      "text" : "iCapital",
      "indices" : [ 101, 110 ]
    } ],
    "urls" : [ {
      "indices" : [ 78, 100 ],
      "url" : "http:\/\/t.co\/GiVRdN7HDg",
      "expanded_url" : "http:\/\/europa.eu\/!UP69UK",
      "display_url" : "europa.eu\/!UP69UK"
    } ]
  },
  "geo" : { },
  "id_str" : "639789112556437505",
  "text" : "RT @EU_H2020: Could your city become the next European Capital of Innovation? http:\/\/t.co\/GiVRdN7HDg #iCapital http:\/\/t.co\/apNZENmgq5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EU_H2020\/status\/639698187977003008\/photo\/1",
        "indices" : [ 97, 119 ],
        "url" : "http:\/\/t.co\/apNZENmgq5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN-KMYyWoAArxTL.jpg",
        "id_str" : "639381617526611968",
        "id" : 639381617526611968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN-KMYyWoAArxTL.jpg",
        "sizes" : [ {
          "h" : 999,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 523
        }, {
          "h" : 999,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 999,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/apNZENmgq5"
      } ],
      "hashtags" : [ {
        "text" : "iCapital",
        "indices" : [ 87, 96 ]
      } ],
      "urls" : [ {
        "indices" : [ 64, 86 ],
        "url" : "http:\/\/t.co\/GiVRdN7HDg",
        "expanded_url" : "http:\/\/europa.eu\/!UP69UK",
        "display_url" : "europa.eu\/!UP69UK"
      } ]
    },
    "geo" : { },
    "id_str" : "639698187977003008",
    "text" : "Could your city become the next European Capital of Innovation? http:\/\/t.co\/GiVRdN7HDg #iCapital http:\/\/t.co\/apNZENmgq5",
    "id" : 639698187977003008,
    "created_at" : "2015-09-04 07:15:07 +0000",
    "user" : {
      "name" : "Horizon 2020\uD83C\uDDEA\uD83C\uDDFA",
      "screen_name" : "EU_H2020",
      "protected" : false,
      "id_str" : "1379662802",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/918843312794091520\/rZnafSo8_normal.jpg",
      "id" : 1379662802,
      "verified" : true
    }
  },
  "id" : 639789112556437505,
  "created_at" : "2015-09-04 13:16:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639758352642347009",
  "text" : "Can't wait for Monday cos waiting for outcome of an application. Ireland you need to move faster.",
  "id" : 639758352642347009,
  "created_at" : "2015-09-04 11:14:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victor LaValle",
      "screen_name" : "victorlavalle",
      "indices" : [ 3, 17 ],
      "id_str" : "50659173",
      "id" : 50659173
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639757444982382592",
  "text" : "RT @victorlavalle: Don't think of it as \"diversity.\"\nThink of it as \"reality.\"",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639138646457778176",
    "text" : "Don't think of it as \"diversity.\"\nThink of it as \"reality.\"",
    "id" : 639138646457778176,
    "created_at" : "2015-09-02 18:11:42 +0000",
    "user" : {
      "name" : "Victor LaValle",
      "screen_name" : "victorlavalle",
      "protected" : false,
      "id_str" : "50659173",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/908775368580059136\/L8zd2r8s_normal.jpg",
      "id" : 50659173,
      "verified" : true
    }
  },
  "id" : 639757444982382592,
  "created_at" : "2015-09-04 11:10:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Helena Liu",
      "screen_name" : "helenaliu",
      "indices" : [ 3, 13 ],
      "id_str" : "15283747",
      "id" : 15283747
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 108 ],
      "url" : "http:\/\/t.co\/apK7OrSouS",
      "expanded_url" : "http:\/\/gu.com\/p\/4bdy5\/stw",
      "display_url" : "gu.com\/p\/4bdy5\/stw"
    } ]
  },
  "geo" : { },
  "id_str" : "639757157613895680",
  "text" : "RT @helenaliu: The 200-year-old painting that puts Europe's fear of migrants to shame http:\/\/t.co\/apK7OrSouS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 93 ],
        "url" : "http:\/\/t.co\/apK7OrSouS",
        "expanded_url" : "http:\/\/gu.com\/p\/4bdy5\/stw",
        "display_url" : "gu.com\/p\/4bdy5\/stw"
      } ]
    },
    "geo" : { },
    "id_str" : "635413396448350208",
    "text" : "The 200-year-old painting that puts Europe's fear of migrants to shame http:\/\/t.co\/apK7OrSouS",
    "id" : 635413396448350208,
    "created_at" : "2015-08-23 11:28:53 +0000",
    "user" : {
      "name" : "Helena Liu",
      "screen_name" : "helenaliu",
      "protected" : false,
      "id_str" : "15283747",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/918381579654676480\/e3uEGcGd_normal.jpg",
      "id" : 15283747,
      "verified" : false
    }
  },
  "id" : 639757157613895680,
  "created_at" : "2015-09-04 11:09:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639755566835990529",
  "text" : "I have not change career direction. I still helping organisation make sense of their website data.",
  "id" : 639755566835990529,
  "created_at" : "2015-09-04 11:03:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639754846980206593",
  "text" : "Laptop unable to boot up but you need to retrieve those precious files now? I can help.",
  "id" : 639754846980206593,
  "created_at" : "2015-09-04 11:00:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 66 ],
      "url" : "http:\/\/t.co\/Uh77gooRlW",
      "expanded_url" : "http:\/\/www.rte.ie\/news\/2015\/0904\/725569-ireland-refugees\/",
      "display_url" : "rte.ie\/news\/2015\/0904\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639749936054202368",
  "text" : "Ireland may accept more than 1,800 refugees http:\/\/t.co\/Uh77gooRlW",
  "id" : 639749936054202368,
  "created_at" : "2015-09-04 10:40:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639749507652128768",
  "text" : "RT @klillington: Yes, &amp; constant Irish emigration ever since, too; often illegal. No nation has a weaker anti-refugee stance https:\/\/t.co\/h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/hGlbLVYU5V",
        "expanded_url" : "https:\/\/twitter.com\/falcarraghcsc\/status\/639709190747910144",
        "display_url" : "twitter.com\/falcarraghcsc\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "639748783841144832",
    "text" : "Yes, &amp; constant Irish emigration ever since, too; often illegal. No nation has a weaker anti-refugee stance https:\/\/t.co\/hGlbLVYU5V",
    "id" : 639748783841144832,
    "created_at" : "2015-09-04 10:36:10 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 639749507652128768,
  "created_at" : "2015-09-04 10:39:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639749379033858048",
  "text" : "Northern Ireland 'agree to accommodate 2,000 migrants'",
  "id" : 639749379033858048,
  "created_at" : "2015-09-04 10:38:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Cameron",
      "screen_name" : "David_Cameron",
      "indices" : [ 3, 17 ],
      "id_str" : "103065157",
      "id" : 103065157
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639747449398149120",
  "text" : "RT @David_Cameron: Taking refugees direct from camps allows a safe route to the UK, rather than the hazardous journey that's cost so many l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639745943034839040",
    "text" : "Taking refugees direct from camps allows a safe route to the UK, rather than the hazardous journey that's cost so many lives (2\/2).",
    "id" : 639745943034839040,
    "created_at" : "2015-09-04 10:24:53 +0000",
    "user" : {
      "name" : "David Cameron",
      "screen_name" : "David_Cameron",
      "protected" : false,
      "id_str" : "103065157",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/664868323377717248\/E7dALt_-_normal.jpg",
      "id" : 103065157,
      "verified" : true
    }
  },
  "id" : 639747449398149120,
  "created_at" : "2015-09-04 10:30:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/MvKEr2YMu8",
      "expanded_url" : "https:\/\/twitter.com\/LiamPomfret\/status\/639588261787111424",
      "display_url" : "twitter.com\/LiamPomfret\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639720666955169792",
  "text" : "Reputation Economy in action.  https:\/\/t.co\/MvKEr2YMu8",
  "id" : 639720666955169792,
  "created_at" : "2015-09-04 08:44:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Elsom",
      "screen_name" : "JasonElsom",
      "indices" : [ 3, 14 ],
      "id_str" : "33552794",
      "id" : 33552794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 128 ],
      "url" : "http:\/\/t.co\/VPDWoi70U7",
      "expanded_url" : "http:\/\/www.ecoti.ms\/5y6t0b",
      "display_url" : "ecoti.ms\/5y6t0b"
    } ]
  },
  "geo" : { },
  "id_str" : "639720257964404736",
  "text" : "RT @JasonElsom: Indian PM bows to teachers - Teachers play big role in students' life: PM Narendra Modi - http:\/\/t.co\/VPDWoi70U7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 112 ],
        "url" : "http:\/\/t.co\/VPDWoi70U7",
        "expanded_url" : "http:\/\/www.ecoti.ms\/5y6t0b",
        "display_url" : "ecoti.ms\/5y6t0b"
      } ]
    },
    "geo" : { },
    "id_str" : "639719233635618817",
    "text" : "Indian PM bows to teachers - Teachers play big role in students' life: PM Narendra Modi - http:\/\/t.co\/VPDWoi70U7",
    "id" : 639719233635618817,
    "created_at" : "2015-09-04 08:38:45 +0000",
    "user" : {
      "name" : "Jason Elsom",
      "screen_name" : "JasonElsom",
      "protected" : false,
      "id_str" : "33552794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930570946284605441\/pUTEgAby_normal.jpg",
      "id" : 33552794,
      "verified" : false
    }
  },
  "id" : 639720257964404736,
  "created_at" : "2015-09-04 08:42:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639718844962074624",
  "text" : "The best growth hacker cannot grow a broken product - Aaron Ginn",
  "id" : 639718844962074624,
  "created_at" : "2015-09-04 08:37:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639693902459633665",
  "text" : "@avalanchelynn trying to make room for the last drop of petrol. I only see type if behavior in S'pore.",
  "id" : 639693902459633665,
  "created_at" : "2015-09-04 06:58:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Catherine Drea",
      "screen_name" : "foxglovelane",
      "indices" : [ 3, 16 ],
      "id_str" : "38410778",
      "id" : 38410778
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/foxglovelane\/status\/639690878509039617\/photo\/1",
      "indices" : [ 76, 98 ],
      "url" : "http:\/\/t.co\/Tw4OniHb1K",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COCjdvUWoAAJyus.jpg",
      "id_str" : "639690878399979520",
      "id" : 639690878399979520,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COCjdvUWoAAJyus.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Tw4OniHb1K"
    } ],
    "hashtags" : [ {
      "text" : "Ireland",
      "indices" : [ 44, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 75 ],
      "url" : "http:\/\/t.co\/pgDj7Md78l",
      "expanded_url" : "http:\/\/ift.tt\/1NQwNSF",
      "display_url" : "ift.tt\/1NQwNSF"
    } ]
  },
  "geo" : { },
  "id_str" : "639693260127125504",
  "text" : "RT @foxglovelane: The greenest green valley #Ireland http:\/\/t.co\/pgDj7Md78l http:\/\/t.co\/Tw4OniHb1K",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/foxglovelane\/status\/639690878509039617\/photo\/1",
        "indices" : [ 58, 80 ],
        "url" : "http:\/\/t.co\/Tw4OniHb1K",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/COCjdvUWoAAJyus.jpg",
        "id_str" : "639690878399979520",
        "id" : 639690878399979520,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COCjdvUWoAAJyus.jpg",
        "sizes" : [ {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Tw4OniHb1K"
      } ],
      "hashtags" : [ {
        "text" : "Ireland",
        "indices" : [ 26, 34 ]
      } ],
      "urls" : [ {
        "indices" : [ 35, 57 ],
        "url" : "http:\/\/t.co\/pgDj7Md78l",
        "expanded_url" : "http:\/\/ift.tt\/1NQwNSF",
        "display_url" : "ift.tt\/1NQwNSF"
      } ]
    },
    "geo" : { },
    "id_str" : "639690878509039617",
    "text" : "The greenest green valley #Ireland http:\/\/t.co\/pgDj7Md78l http:\/\/t.co\/Tw4OniHb1K",
    "id" : 639690878509039617,
    "created_at" : "2015-09-04 06:46:04 +0000",
    "user" : {
      "name" : "Catherine Drea",
      "screen_name" : "foxglovelane",
      "protected" : false,
      "id_str" : "38410778",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/612211626595495936\/0qfYlVLN_normal.jpg",
      "id" : 38410778,
      "verified" : false
    }
  },
  "id" : 639693260127125504,
  "created_at" : "2015-09-04 06:55:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 117 ],
      "url" : "http:\/\/t.co\/o8T4ciPe45",
      "expanded_url" : "http:\/\/ow.ly\/RCDQ8",
      "display_url" : "ow.ly\/RCDQ8"
    } ]
  },
  "geo" : { },
  "id_str" : "639682090078994432",
  "text" : "RT @orangemanta: Denmark Just Produced 140% of its Electricity Needs with Renewable Wind Power http:\/\/t.co\/o8T4ciPe45",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 100 ],
        "url" : "http:\/\/t.co\/o8T4ciPe45",
        "expanded_url" : "http:\/\/ow.ly\/RCDQ8",
        "display_url" : "ow.ly\/RCDQ8"
      } ]
    },
    "geo" : { },
    "id_str" : "639682015202295808",
    "text" : "Denmark Just Produced 140% of its Electricity Needs with Renewable Wind Power http:\/\/t.co\/o8T4ciPe45",
    "id" : 639682015202295808,
    "created_at" : "2015-09-04 06:10:51 +0000",
    "user" : {
      "name" : "Ron Immink",
      "screen_name" : "ronimmink",
      "protected" : false,
      "id_str" : "14627081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/926388478627254272\/nei8n_Rr_normal.jpg",
      "id" : 14627081,
      "verified" : false
    }
  },
  "id" : 639682090078994432,
  "created_at" : "2015-09-04 06:11:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Thrillist",
      "screen_name" : "Thrillist",
      "indices" : [ 52, 62 ],
      "id_str" : "16402507",
      "id" : 16402507
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 85 ],
      "url" : "http:\/\/t.co\/ZihE5IIWLD",
      "expanded_url" : "http:\/\/bit.ly\/1JTRBGF",
      "display_url" : "bit.ly\/1JTRBGF"
    } ]
  },
  "geo" : { },
  "id_str" : "639584664974426112",
  "text" : "The 16 Best Street-Food Cities in the World, Ranked,@Thrillist http:\/\/t.co\/ZihE5IIWLD",
  "id" : 639584664974426112,
  "created_at" : "2015-09-03 23:44:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639569237498036225",
  "text" : "2. He failed to see his country (and UK) meddling are part of the problem too.",
  "id" : 639569237498036225,
  "created_at" : "2015-09-03 22:42:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639568860597886976",
  "text" : "1. On BBC World News, a former US official claimed the refugee crisis is because of Russian\/Iranian involvement in Syria.",
  "id" : 639568860597886976,
  "created_at" : "2015-09-03 22:41:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639546152497586176",
  "text" : "Shouldn't Russian open her door to Syrian refugees?",
  "id" : 639546152497586176,
  "created_at" : "2015-09-03 21:10:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SugarRush",
      "indices" : [ 0, 10 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639538722552324096",
  "text" : "#SugarRush  You can't tax away the problem. Awareness is the key.",
  "id" : 639538722552324096,
  "created_at" : "2015-09-03 20:41:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/639533400664182784\/photo\/1",
      "indices" : [ 65, 87 ],
      "url" : "http:\/\/t.co\/zPeVsdShmo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/COAUPUhWsAE_LLQ.jpg",
      "id_str" : "639533400525811713",
      "id" : 639533400525811713,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/COAUPUhWsAE_LLQ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/zPeVsdShmo"
    } ],
    "hashtags" : [ {
      "text" : "food",
      "indices" : [ 36, 41 ]
    } ],
    "urls" : [ {
      "indices" : [ 42, 64 ],
      "url" : "http:\/\/t.co\/4DWSUGIXfh",
      "expanded_url" : "http:\/\/ift.tt\/1hYoOpU",
      "display_url" : "ift.tt\/1hYoOpU"
    } ]
  },
  "geo" : { },
  "id_str" : "639533400664182784",
  "text" : "South East Asia style fired noodle. #food http:\/\/t.co\/4DWSUGIXfh http:\/\/t.co\/zPeVsdShmo",
  "id" : 639533400664182784,
  "created_at" : "2015-09-03 20:20:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639500990086017024",
  "text" : "Going by my Twitter list, Social Media and digital marketing professional are dime a dozen.",
  "id" : 639500990086017024,
  "created_at" : "2015-09-03 18:11:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/4Q09V671nv",
      "expanded_url" : "https:\/\/twitter.com\/socialmedia2day\/status\/639035290615717889",
      "display_url" : "twitter.com\/socialmedia2da\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639498744094965761",
  "text" : "Can this turn the tide against Slack? https:\/\/t.co\/4Q09V671nv",
  "id" : 639498744094965761,
  "created_at" : "2015-09-03 18:02:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639487239983075328",
  "text" : "Ireland, let call for a referendum whether to take more refugee.",
  "id" : 639487239983075328,
  "created_at" : "2015-09-03 17:16:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Callan Tham",
      "screen_name" : "ctham",
      "indices" : [ 3, 9 ],
      "id_str" : "23011555",
      "id" : 23011555
    }, {
      "name" : "\u2115\uD835\uDD60\uD835\uDD65\uD835\uDD52\uD835\uDD53\uD835\uDD5A\uD835\uDD5D\uD835\uDD5A\uD835\uDD52",
      "screen_name" : "notabilia",
      "indices" : [ 14, 24 ],
      "id_str" : "824431685076815872",
      "id" : 824431685076815872
    }, {
      "name" : "WSJ Asia",
      "screen_name" : "WSJAsia",
      "indices" : [ 28, 36 ],
      "id_str" : "28137012",
      "id" : 28137012
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/WSJAsia\/status\/639346947350994944\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/kUcK7CRNyN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN9qqSkWgAAhotF.png",
      "id_str" : "639346946881257472",
      "id" : 639346946881257472,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN9qqSkWgAAhotF.png",
      "sizes" : [ {
        "h" : 340,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/kUcK7CRNyN"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 107 ],
      "url" : "http:\/\/t.co\/MMPwxJIRJF",
      "expanded_url" : "http:\/\/on.wsj.com\/1NZBA3p",
      "display_url" : "on.wsj.com\/1NZBA3p"
    } ]
  },
  "geo" : { },
  "id_str" : "639478761663164417",
  "text" : "RT @ctham: RT @notabilia RT @WSJAsia Singapore election to test shift on immigration\nhttp:\/\/t.co\/MMPwxJIRJF http:\/\/t.co\/kUcK7CRNyN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.myplume.com\/\" rel=\"nofollow\"\u003EPlume\u00A0for\u00A0Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\u2115\uD835\uDD60\uD835\uDD65\uD835\uDD52\uD835\uDD53\uD835\uDD5A\uD835\uDD5D\uD835\uDD5A\uD835\uDD52",
        "screen_name" : "notabilia",
        "indices" : [ 3, 13 ],
        "id_str" : "824431685076815872",
        "id" : 824431685076815872
      }, {
        "name" : "WSJ Asia",
        "screen_name" : "WSJAsia",
        "indices" : [ 17, 25 ],
        "id_str" : "28137012",
        "id" : 28137012
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WSJAsia\/status\/639346947350994944\/photo\/1",
        "indices" : [ 97, 119 ],
        "url" : "http:\/\/t.co\/kUcK7CRNyN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN9qqSkWgAAhotF.png",
        "id_str" : "639346946881257472",
        "id" : 639346946881257472,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN9qqSkWgAAhotF.png",
        "sizes" : [ {
          "h" : 340,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kUcK7CRNyN"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 96 ],
        "url" : "http:\/\/t.co\/MMPwxJIRJF",
        "expanded_url" : "http:\/\/on.wsj.com\/1NZBA3p",
        "display_url" : "on.wsj.com\/1NZBA3p"
      } ]
    },
    "in_reply_to_status_id_str" : "639346947350994944",
    "geo" : { },
    "id_str" : "639353919747944451",
    "in_reply_to_user_id" : 28137012,
    "text" : "RT @notabilia RT @WSJAsia Singapore election to test shift on immigration\nhttp:\/\/t.co\/MMPwxJIRJF http:\/\/t.co\/kUcK7CRNyN",
    "id" : 639353919747944451,
    "in_reply_to_status_id" : 639346947350994944,
    "created_at" : "2015-09-03 08:27:07 +0000",
    "in_reply_to_screen_name" : "WSJAsia",
    "in_reply_to_user_id_str" : "28137012",
    "user" : {
      "name" : "Callan Tham",
      "screen_name" : "ctham",
      "protected" : false,
      "id_str" : "23011555",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2612846065\/zxfg4dpk8klhp6jgkpir_normal.jpeg",
      "id" : 23011555,
      "verified" : false
    }
  },
  "id" : 639478761663164417,
  "created_at" : "2015-09-03 16:43:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639477483771924480",
  "text" : "Need to access those precious files from a laptop that unable to boot up? I can help. (Involve prying the hard drive from the laptop)",
  "id" : 639477483771924480,
  "created_at" : "2015-09-03 16:38:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Springboardplus",
      "indices" : [ 55, 71 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639470379518529536",
  "text" : "Asking for a friend. Has anyone got a reply from those #Springboardplus course they apply\uFF1F",
  "id" : 639470379518529536,
  "created_at" : "2015-09-03 16:09:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639468483617628160",
  "text" : "2.  U need birth cert to paper qualification to be certified by officials &amp; the public service is not functioning in a war torn country.",
  "id" : 639468483617628160,
  "created_at" : "2015-09-03 16:02:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639467946432196608",
  "text" : "1. Some expect refugees in a war-torn country to go thru a propose Australia style point system to gain entry.",
  "id" : 639467946432196608,
  "created_at" : "2015-09-03 16:00:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ailish Irvine",
      "screen_name" : "ailieirv",
      "indices" : [ 0, 9 ],
      "id_str" : "136690988",
      "id" : 136690988
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "639464931566448640",
  "geo" : { },
  "id_str" : "639467356868079616",
  "in_reply_to_user_id" : 136690988,
  "text" : "@ailieirv Just hazard a guess here. Could it be IoT?",
  "id" : 639467356868079616,
  "in_reply_to_status_id" : 639464931566448640,
  "created_at" : "2015-09-03 15:57:53 +0000",
  "in_reply_to_screen_name" : "ailieirv",
  "in_reply_to_user_id_str" : "136690988",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Mates",
      "screen_name" : "jamesmatesitv",
      "indices" : [ 3, 17 ],
      "id_str" : "85590185",
      "id" : 85590185
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639423696172675072",
  "text" : "RT @jamesmatesitv: International media being shepherded away from the train. Whatever happens next, Hungarians don't want world to see http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jamesmatesitv\/status\/639419308993609728\/photo\/1",
        "indices" : [ 116, 138 ],
        "url" : "http:\/\/t.co\/YTF6fhS8nL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN-sbwlWsAE5ZIS.jpg",
        "id_str" : "639419265007923201",
        "id" : 639419265007923201,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN-sbwlWsAE5ZIS.jpg",
        "sizes" : [ {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YTF6fhS8nL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639419308993609728",
    "text" : "International media being shepherded away from the train. Whatever happens next, Hungarians don't want world to see http:\/\/t.co\/YTF6fhS8nL",
    "id" : 639419308993609728,
    "created_at" : "2015-09-03 12:46:57 +0000",
    "user" : {
      "name" : "James Mates",
      "screen_name" : "jamesmatesitv",
      "protected" : false,
      "id_str" : "85590185",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/776726732799868928\/XGjiXmw0_normal.jpg",
      "id" : 85590185,
      "verified" : true
    }
  },
  "id" : 639423696172675072,
  "created_at" : "2015-09-03 13:04:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/jm0n2A4yah",
      "expanded_url" : "https:\/\/twitter.com\/yapphenghui\/status\/639363397604126720",
      "display_url" : "twitter.com\/yapphenghui\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639365597961289728",
  "text" : "\"It is rare indeed that any PAP MP would so doggedly champion the poor.\" https:\/\/t.co\/jm0n2A4yah",
  "id" : 639365597961289728,
  "created_at" : "2015-09-03 09:13:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 3, 12 ],
      "id_str" : "45756727",
      "id" : 45756727
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639363879022252032",
  "text" : "RT @LividEye: Today is a day where all my teacher friends will be shamelessly collecting presents, pretending to be shy but still accept it\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639363050319904768",
    "text" : "Today is a day where all my teacher friends will be shamelessly collecting presents, pretending to be shy but still accept it ANYWAYS. LOL:P",
    "id" : 639363050319904768,
    "created_at" : "2015-09-03 09:03:24 +0000",
    "user" : {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "protected" : false,
      "id_str" : "45756727",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973927451775545344\/8ZHNGFY8_normal.jpg",
      "id" : 45756727,
      "verified" : false
    }
  },
  "id" : 639363879022252032,
  "created_at" : "2015-09-03 09:06:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mob barley",
      "screen_name" : "contrabandkarma",
      "indices" : [ 3, 19 ],
      "id_str" : "36384189",
      "id" : 36384189
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/contrabandkarma\/status\/639355966744428544\/photo\/1",
      "indices" : [ 58, 80 ],
      "url" : "http:\/\/t.co\/jMjL8BWp4b",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN9y25LVAAAeT03.jpg",
      "id_str" : "639355959496736768",
      "id" : 639355959496736768,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN9y25LVAAAeT03.jpg",
      "sizes" : [ {
        "h" : 606,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 606,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 515,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 606,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jMjL8BWp4b"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639361382362783744",
  "text" : "RT @contrabandkarma: A most unfortunate photo for the PM. http:\/\/t.co\/jMjL8BWp4b",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/contrabandkarma\/status\/639355966744428544\/photo\/1",
        "indices" : [ 37, 59 ],
        "url" : "http:\/\/t.co\/jMjL8BWp4b",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN9y25LVAAAeT03.jpg",
        "id_str" : "639355959496736768",
        "id" : 639355959496736768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN9y25LVAAAeT03.jpg",
        "sizes" : [ {
          "h" : 606,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 606,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 515,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 606,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jMjL8BWp4b"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639355966744428544",
    "text" : "A most unfortunate photo for the PM. http:\/\/t.co\/jMjL8BWp4b",
    "id" : 639355966744428544,
    "created_at" : "2015-09-03 08:35:15 +0000",
    "user" : {
      "name" : "mob barley",
      "screen_name" : "contrabandkarma",
      "protected" : false,
      "id_str" : "36384189",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011613271026102272\/lRmKYZfU_normal.jpg",
      "id" : 36384189,
      "verified" : false
    }
  },
  "id" : 639361382362783744,
  "created_at" : "2015-09-03 08:56:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/D2t1ReRlyR",
      "expanded_url" : "http:\/\/www.unhcr.org\/pages\/49e48e0fa7f.html",
      "display_url" : "unhcr.org\/pages\/49e48e0f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639356479074648064",
  "text" : "Since the Syrian crisis began in 2011, Turkey - estimated to host over one million Syrians. http:\/\/t.co\/D2t1ReRlyR",
  "id" : 639356479074648064,
  "created_at" : "2015-09-03 08:37:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ScottBourne",
      "screen_name" : "scottbourne",
      "indices" : [ 3, 15 ],
      "id_str" : "8637672",
      "id" : 8637672
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Fujifilm",
      "indices" : [ 31, 40 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639352403364593664",
  "text" : "RT @scottbourne: Interested in #Fujifilm cameras\/lenses? Check out @WeShootFuji - http:\/\/t.co\/kQ6M6jqllF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Fujifilm",
        "indices" : [ 14, 23 ]
      } ],
      "urls" : [ {
        "indices" : [ 65, 87 ],
        "url" : "http:\/\/t.co\/kQ6M6jqllF",
        "expanded_url" : "http:\/\/www.weshootfuji.com",
        "display_url" : "weshootfuji.com"
      } ]
    },
    "geo" : { },
    "id_str" : "639294374845648896",
    "text" : "Interested in #Fujifilm cameras\/lenses? Check out @WeShootFuji - http:\/\/t.co\/kQ6M6jqllF",
    "id" : 639294374845648896,
    "created_at" : "2015-09-03 04:30:31 +0000",
    "user" : {
      "name" : "ScottBourne",
      "screen_name" : "scottbourne",
      "protected" : false,
      "id_str" : "8637672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/971930102958182401\/XP8aqZtL_normal.jpg",
      "id" : 8637672,
      "verified" : true
    }
  },
  "id" : 639352403364593664,
  "created_at" : "2015-09-03 08:21:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 76, 88 ],
      "id_str" : "979910827",
      "id" : 979910827
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/639140548926906370\/photo\/1",
      "indices" : [ 89, 111 ],
      "url" : "http:\/\/t.co\/AHumz5cbEH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN6u8SoWoAA_5VW.png",
      "id_str" : "639140547949666304",
      "id" : 639140547949666304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN6u8SoWoAA_5VW.png",
      "sizes" : [ {
        "h" : 191,
        "resize" : "fit",
        "w" : 687
      }, {
        "h" : 191,
        "resize" : "fit",
        "w" : 687
      }, {
        "h" : 189,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 191,
        "resize" : "fit",
        "w" : 687
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/AHumz5cbEH"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http:\/\/t.co\/WsPhePdd1k",
      "expanded_url" : "http:\/\/www.asiasentinel.com\/econ-business\/ireland-opens-hong-kong-consulate\/",
      "display_url" : "asiasentinel.com\/econ-business\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639140548926906370",
  "text" : "Irish connection to China's brief Republic http:\/\/t.co\/WsPhePdd1k  Link via @GeoffreyIRL http:\/\/t.co\/AHumz5cbEH",
  "id" : 639140548926906370,
  "created_at" : "2015-09-02 18:19:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/639137536141619200\/photo\/1",
      "indices" : [ 115, 137 ],
      "url" : "http:\/\/t.co\/N4muN7XlkJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN6sM-LWEAAm6Bb.jpg",
      "id_str" : "639137535982178304",
      "id" : 639137535982178304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN6sM-LWEAAm6Bb.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/N4muN7XlkJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/9papjHhTNZ",
      "expanded_url" : "http:\/\/ift.tt\/1NaNb04",
      "display_url" : "ift.tt\/1NaNb04"
    } ]
  },
  "geo" : { },
  "id_str" : "639137536141619200",
  "text" : "This little connector is just you need to move files from your laptop hard drive (removed)\u2026 http:\/\/t.co\/9papjHhTNZ http:\/\/t.co\/N4muN7XlkJ",
  "id" : 639137536141619200,
  "created_at" : "2015-09-02 18:07:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jinhua Kuek \u90ED\u8FDB\u534E",
      "screen_name" : "kuekj",
      "indices" : [ 3, 9 ],
      "id_str" : "288041858",
      "id" : 288041858
    }, {
      "name" : "Channel 8 News 8\u9891\u9053\u65B0\u95FB",
      "screen_name" : "ch8newsSG",
      "indices" : [ 18, 28 ],
      "id_str" : "365886581",
      "id" : 365886581
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sgelections",
      "indices" : [ 100, 112 ]
    }, {
      "text" : "ge2015",
      "indices" : [ 113, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639114638479728640",
  "text" : "RT @kuekj: Thanks @ch8newssg for featuring this tweet and the map of rally sites! Much appreciated. #sgelections #ge2015  https:\/\/t.co\/Mbpy\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Channel 8 News 8\u9891\u9053\u65B0\u95FB",
        "screen_name" : "ch8newsSG",
        "indices" : [ 7, 17 ],
        "id_str" : "365886581",
        "id" : 365886581
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "sgelections",
        "indices" : [ 89, 101 ]
      }, {
        "text" : "ge2015",
        "indices" : [ 102, 109 ]
      } ],
      "urls" : [ {
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/Mbpyv7rX1x",
        "expanded_url" : "https:\/\/twitter.com\/kuekj\/status\/638927456414527488",
        "display_url" : "twitter.com\/kuekj\/status\/6\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "639094122951901184",
    "text" : "Thanks @ch8newssg for featuring this tweet and the map of rally sites! Much appreciated. #sgelections #ge2015  https:\/\/t.co\/Mbpyv7rX1x",
    "id" : 639094122951901184,
    "created_at" : "2015-09-02 15:14:47 +0000",
    "user" : {
      "name" : "Jinhua Kuek \u90ED\u8FDB\u534E",
      "screen_name" : "kuekj",
      "protected" : false,
      "id_str" : "288041858",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005496172897636353\/1F5AoGBi_normal.jpg",
      "id" : 288041858,
      "verified" : false
    }
  },
  "id" : 639114638479728640,
  "created_at" : "2015-09-02 16:36:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639070277373161472",
  "text" : "What the definition of Emergency meeting? 12 days later...",
  "id" : 639070277373161472,
  "created_at" : "2015-09-02 13:40:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bill Frelick",
      "screen_name" : "BillFrelick",
      "indices" : [ 3, 15 ],
      "id_str" : "516570220",
      "id" : 516570220
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639053779074183168",
  "text" : "RT @BillFrelick: Not a number, not an illegal migrant, but a child who died trying to reach safety in Europe. Open safe &amp; legal paths. http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BillFrelick\/status\/639052313890529280\/photo\/1",
        "indices" : [ 122, 144 ],
        "url" : "http:\/\/t.co\/WXR7eY3KDz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN5esCOUwAAsZoW.jpg",
        "id_str" : "639052307737395200",
        "id" : 639052307737395200,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN5esCOUwAAsZoW.jpg",
        "sizes" : [ {
          "h" : 396,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WXR7eY3KDz"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "639052313890529280",
    "text" : "Not a number, not an illegal migrant, but a child who died trying to reach safety in Europe. Open safe &amp; legal paths. http:\/\/t.co\/WXR7eY3KDz",
    "id" : 639052313890529280,
    "created_at" : "2015-09-02 12:28:39 +0000",
    "user" : {
      "name" : "Bill Frelick",
      "screen_name" : "BillFrelick",
      "protected" : false,
      "id_str" : "516570220",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/923761882556706816\/k1SXmajd_normal.jpg",
      "id" : 516570220,
      "verified" : true
    }
  },
  "id" : 639053779074183168,
  "created_at" : "2015-09-02 12:34:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pamela Pavliscak",
      "screen_name" : "paminthelab",
      "indices" : [ 119, 131 ],
      "id_str" : "1093221138",
      "id" : 1093221138
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 114 ],
      "url" : "http:\/\/t.co\/N2lUQJ87Ne",
      "expanded_url" : "http:\/\/www.uxmatters.com\/mt\/archives\/2015\/08\/the-data-informed-customer-journey.php#sthash.JtwHnEeT.dpuf",
      "display_url" : "uxmatters.com\/mt\/archives\/20\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639038488206307328",
  "text" : "\u201CIt\u2019s likely that another Web site or app is framing the experience of your site or app...\" http:\/\/t.co\/N2lUQJ87Ne via @paminthelab",
  "id" : 639038488206307328,
  "created_at" : "2015-09-02 11:33:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/M3R6bRWOPD",
      "expanded_url" : "https:\/\/twitter.com\/atheistie\/status\/638608710856798208",
      "display_url" : "twitter.com\/atheistie\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639037951058538497",
  "text" : "I have no problem opting out on our own. School is very accommodating. https:\/\/t.co\/M3R6bRWOPD",
  "id" : 639037951058538497,
  "created_at" : "2015-09-02 11:31:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/d1K4lfazTR",
      "expanded_url" : "https:\/\/twitter.com\/yuhui\/status\/639024571903946752",
      "display_url" : "twitter.com\/yuhui\/status\/6\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "639036086619086848",
  "text" : "Replaced cows with income level and still make sense.  https:\/\/t.co\/d1K4lfazTR",
  "id" : 639036086619086848,
  "created_at" : "2015-09-02 11:24:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ann O'Dea",
      "screen_name" : "AnnODeaSR",
      "indices" : [ 3, 13 ],
      "id_str" : "402099174",
      "id" : 402099174
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 119 ],
      "url" : "http:\/\/t.co\/LDuHl7zoJT",
      "expanded_url" : "http:\/\/ti.me\/1LQ2ZoX",
      "display_url" : "ti.me\/1LQ2ZoX"
    } ]
  },
  "geo" : { },
  "id_str" : "639034549851324416",
  "text" : "RT @AnnODeaSR: Thousands of Icelanders have offered to take Syrian refugees into their own homes http:\/\/t.co\/LDuHl7zoJT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 82, 104 ],
        "url" : "http:\/\/t.co\/LDuHl7zoJT",
        "expanded_url" : "http:\/\/ti.me\/1LQ2ZoX",
        "display_url" : "ti.me\/1LQ2ZoX"
      } ]
    },
    "geo" : { },
    "id_str" : "638820672714600448",
    "text" : "Thousands of Icelanders have offered to take Syrian refugees into their own homes http:\/\/t.co\/LDuHl7zoJT",
    "id" : 638820672714600448,
    "created_at" : "2015-09-01 21:08:11 +0000",
    "user" : {
      "name" : "Ann O'Dea",
      "screen_name" : "AnnODeaSR",
      "protected" : false,
      "id_str" : "402099174",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011983281586409472\/_30Y0y7i_normal.jpg",
      "id" : 402099174,
      "verified" : true
    }
  },
  "id" : 639034549851324416,
  "created_at" : "2015-09-02 11:18:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emirates Airline",
      "screen_name" : "emirates",
      "indices" : [ 3, 12 ],
      "id_str" : "821045162",
      "id" : 821045162
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "emiratespilots",
      "indices" : [ 107, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639019148304101376",
  "text" : "RT @emirates: We have over 270 wide-bodied jets on order from Boeing and Airbus. And big opportunities for #emiratespilots too. https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ads.twitter.com\" rel=\"nofollow\"\u003ETwitter Ads\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "emiratespilots",
        "indices" : [ 93, 108 ]
      } ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/thUA2890bT",
        "expanded_url" : "https:\/\/cards.twitter.com\/cards\/18ce53wo2un\/ul26",
        "display_url" : "cards.twitter.com\/cards\/18ce53wo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "634675317659992064",
    "text" : "We have over 270 wide-bodied jets on order from Boeing and Airbus. And big opportunities for #emiratespilots too. https:\/\/t.co\/thUA2890bT",
    "id" : 634675317659992064,
    "created_at" : "2015-08-21 10:36:02 +0000",
    "user" : {
      "name" : "Emirates Airline",
      "screen_name" : "emirates",
      "protected" : false,
      "id_str" : "821045162",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876359897356656641\/Wet6RQxI_normal.jpg",
      "id" : 821045162,
      "verified" : true
    }
  },
  "id" : 639019148304101376,
  "created_at" : "2015-09-02 10:16:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/639017557731737600\/photo\/1",
      "indices" : [ 14, 36 ],
      "url" : "http:\/\/t.co\/D6LbuGrnfD",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN4_FAQWgAAY6Yp.jpg",
      "id_str" : "639017552333668352",
      "id" : 639017552333668352,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN4_FAQWgAAY6Yp.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 768
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/D6LbuGrnfD"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "639017557731737600",
  "text" : "Good morning. http:\/\/t.co\/D6LbuGrnfD",
  "id" : 639017557731737600,
  "created_at" : "2015-09-02 10:10:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/638860687436066816\/photo\/1",
      "indices" : [ 53, 75 ],
      "url" : "http:\/\/t.co\/V74y0HL0bq",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN2waNuXAAAgcPZ.png",
      "id_str" : "638860686563672064",
      "id" : 638860686563672064,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN2waNuXAAAgcPZ.png",
      "sizes" : [ {
        "h" : 493,
        "resize" : "fit",
        "w" : 513
      }, {
        "h" : 493,
        "resize" : "fit",
        "w" : 513
      }, {
        "h" : 493,
        "resize" : "fit",
        "w" : 513
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 493,
        "resize" : "fit",
        "w" : 513
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/V74y0HL0bq"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "638860687436066816",
  "text" : "On my FB news feed. This is interesting development. http:\/\/t.co\/V74y0HL0bq",
  "id" : 638860687436066816,
  "created_at" : "2015-09-01 23:47:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 3, 13 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 116 ],
      "url" : "http:\/\/t.co\/7zTMKWOCmf",
      "expanded_url" : "http:\/\/wapo.st\/1JGydIq",
      "display_url" : "wapo.st\/1JGydIq"
    } ]
  },
  "geo" : { },
  "id_str" : "638845415010500608",
  "text" : "RT @starduest: How it feels when white people shame your culture\u2019s food \u2014 then make it trendy http:\/\/t.co\/7zTMKWOCmf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 79, 101 ],
        "url" : "http:\/\/t.co\/7zTMKWOCmf",
        "expanded_url" : "http:\/\/wapo.st\/1JGydIq",
        "display_url" : "wapo.st\/1JGydIq"
      } ]
    },
    "geo" : { },
    "id_str" : "638638167499894784",
    "text" : "How it feels when white people shame your culture\u2019s food \u2014 then make it trendy http:\/\/t.co\/7zTMKWOCmf",
    "id" : 638638167499894784,
    "created_at" : "2015-09-01 09:02:59 +0000",
    "user" : {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "protected" : false,
      "id_str" : "275589813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922138705145380864\/k8Pzy5Q4_normal.jpg",
      "id" : 275589813,
      "verified" : false
    }
  },
  "id" : 638845415010500608,
  "created_at" : "2015-09-01 22:46:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Keith Chen",
      "screen_name" : "MKeithChen",
      "indices" : [ 3, 14 ],
      "id_str" : "621213380",
      "id" : 621213380
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 108 ],
      "url" : "http:\/\/t.co\/bBmy20j2A2",
      "expanded_url" : "http:\/\/wpo.st\/i_pT0",
      "display_url" : "wpo.st\/i_pT0"
    } ]
  },
  "geo" : { },
  "id_str" : "638840639413747712",
  "text" : "RT @MKeithChen: How statistics guided me through life, death and \u2018The Price Is Right\u2019 http:\/\/t.co\/bBmy20j2A2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 92 ],
        "url" : "http:\/\/t.co\/bBmy20j2A2",
        "expanded_url" : "http:\/\/wpo.st\/i_pT0",
        "display_url" : "wpo.st\/i_pT0"
      } ]
    },
    "geo" : { },
    "id_str" : "629681080077979648",
    "text" : "How statistics guided me through life, death and \u2018The Price Is Right\u2019 http:\/\/t.co\/bBmy20j2A2",
    "id" : 629681080077979648,
    "created_at" : "2015-08-07 15:50:43 +0000",
    "user" : {
      "name" : "Keith Chen",
      "screen_name" : "MKeithChen",
      "protected" : false,
      "id_str" : "621213380",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3402234392\/21fc1732b712c9aa25fa522bd4b9015d_normal.jpeg",
      "id" : 621213380,
      "verified" : false
    }
  },
  "id" : 638840639413747712,
  "created_at" : "2015-09-01 22:27:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brian O'Connell",
      "screen_name" : "oconnellbrian",
      "indices" : [ 0, 14 ],
      "id_str" : "108597502",
      "id" : 108597502
    }, {
      "name" : "Today Sean O'Rourke",
      "screen_name" : "TodaySOR",
      "indices" : [ 15, 24 ],
      "id_str" : "242702809",
      "id" : 242702809
    }, {
      "name" : "Migrant Rights Centre Ireland",
      "screen_name" : "MigrantRightsIr",
      "indices" : [ 119, 135 ],
      "id_str" : "239405285",
      "id" : 239405285
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "638812454357745665",
  "geo" : { },
  "id_str" : "638820209936076800",
  "in_reply_to_user_id" : 108597502,
  "text" : "@oconnellbrian @TodaySOR IS there anything all of us can do to help the family and senting those abusers a message? cc @MigrantRightsIR",
  "id" : 638820209936076800,
  "in_reply_to_status_id" : 638812454357745665,
  "created_at" : "2015-09-01 21:06:21 +0000",
  "in_reply_to_screen_name" : "oconnellbrian",
  "in_reply_to_user_id_str" : "108597502",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "638816726109458433",
  "geo" : { },
  "id_str" : "638817163109789700",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest Still backing up before trying to save the HD. Most probably to re-install the OS",
  "id" : 638817163109789700,
  "in_reply_to_status_id" : 638816726109458433,
  "created_at" : "2015-09-01 20:54:14 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "indices" : [ 3, 14 ],
      "id_str" : "15908631",
      "id" : 15908631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/madPXWEAQY",
      "expanded_url" : "https:\/\/twitter.com\/oconnellbrian\/status\/638812454357745665",
      "display_url" : "twitter.com\/oconnellbrian\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "638816745529020417",
  "text" : "RT @gavinsblog: Land of 1,000 welcomes.  https:\/\/t.co\/madPXWEAQY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 25, 48 ],
        "url" : "https:\/\/t.co\/madPXWEAQY",
        "expanded_url" : "https:\/\/twitter.com\/oconnellbrian\/status\/638812454357745665",
        "display_url" : "twitter.com\/oconnellbrian\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "638813375787593728",
    "text" : "Land of 1,000 welcomes.  https:\/\/t.co\/madPXWEAQY",
    "id" : 638813375787593728,
    "created_at" : "2015-09-01 20:39:12 +0000",
    "user" : {
      "name" : "Gavin Sheridan",
      "screen_name" : "gavinsblog",
      "protected" : false,
      "id_str" : "15908631",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/894331872716754945\/0KfaOf4I_normal.jpg",
      "id" : 15908631,
      "verified" : true
    }
  },
  "id" : 638816745529020417,
  "created_at" : "2015-09-01 20:52:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 87 ],
      "url" : "http:\/\/t.co\/JPAfLUi0LZ",
      "expanded_url" : "http:\/\/lumixlx100.tumblr.com\/",
      "display_url" : "lumixlx100.tumblr.com"
    } ]
  },
  "geo" : { },
  "id_str" : "638814133358579712",
  "text" : "Panasonic are offering a massive \u00A3200 UK cash back on the LX100. http:\/\/t.co\/JPAfLUi0LZ",
  "id" : 638814133358579712,
  "created_at" : "2015-09-01 20:42:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "check bio",
      "screen_name" : "starduest",
      "indices" : [ 0, 10 ],
      "id_str" : "824059172731830272",
      "id" : 824059172731830272
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/638810727948656640\/photo\/1",
      "indices" : [ 103, 125 ],
      "url" : "http:\/\/t.co\/6a7xo66EhS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN2C-LXWwAQ2Zoe.jpg",
      "id_str" : "638810726870728708",
      "id" : 638810726870728708,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN2C-LXWwAQ2Zoe.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6a7xo66EhS"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "636174947098955777",
  "geo" : { },
  "id_str" : "638810727948656640",
  "in_reply_to_user_id" : 275589813,
  "text" : "@starduest I using this external SATA HD enclosure. Got it from Maplin. The cheapest option available. http:\/\/t.co\/6a7xo66EhS",
  "id" : 638810727948656640,
  "in_reply_to_status_id" : 636174947098955777,
  "created_at" : "2015-09-01 20:28:40 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/638787000296964096\/photo\/1",
      "indices" : [ 39, 61 ],
      "url" : "http:\/\/t.co\/JDwl6zJZyu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN1tZGtWgAA2HX0.jpg",
      "id_str" : "638787000221466624",
      "id" : 638787000221466624,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN1tZGtWgAA2HX0.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/JDwl6zJZyu"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 16, 38 ],
      "url" : "http:\/\/t.co\/bXgeIW8Ijm",
      "expanded_url" : "http:\/\/ift.tt\/1EwfMdi",
      "display_url" : "ift.tt\/1EwfMdi"
    } ]
  },
  "geo" : { },
  "id_str" : "638787000296964096",
  "text" : "Home cook lunch http:\/\/t.co\/bXgeIW8Ijm http:\/\/t.co\/JDwl6zJZyu",
  "id" : 638787000296964096,
  "created_at" : "2015-09-01 18:54:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/638779339241332736\/photo\/1",
      "indices" : [ 49, 71 ],
      "url" : "http:\/\/t.co\/FAsufPnXCT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN1mbKMWwAEqTf-.jpg",
      "id_str" : "638779338935156737",
      "id" : 638779338935156737,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN1mbKMWwAEqTf-.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FAsufPnXCT"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 48 ],
      "url" : "http:\/\/t.co\/wUD4fAHS9Z",
      "expanded_url" : "http:\/\/ift.tt\/1LINSdO",
      "display_url" : "ift.tt\/1LINSdO"
    } ]
  },
  "geo" : { },
  "id_str" : "638779339241332736",
  "text" : "Friendly neighborhood cat http:\/\/t.co\/wUD4fAHS9Z http:\/\/t.co\/FAsufPnXCT",
  "id" : 638779339241332736,
  "created_at" : "2015-09-01 18:23:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "638770129644429313",
  "text" : "\"Cowards die many times before their deaths\"from William Shakespeare works Julius Caesar",
  "id" : 638770129644429313,
  "created_at" : "2015-09-01 17:47:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 3, 19 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "durham",
      "indices" : [ 129, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "638738653741715456",
  "text" : "RT @interactivemark: Which restaurants near you have been featured on a cooking show? This site\u2019ll tell you (here\u2019s the list for #durham) h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "durham",
        "indices" : [ 108, 115 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 139 ],
        "url" : "http:\/\/t.co\/8ivu8TjCZJ",
        "expanded_url" : "http:\/\/bit.ly\/1JzTJS7",
        "display_url" : "bit.ly\/1JzTJS7"
      } ]
    },
    "geo" : { },
    "id_str" : "638723698690801664",
    "text" : "Which restaurants near you have been featured on a cooking show? This site\u2019ll tell you (here\u2019s the list for #durham) http:\/\/t.co\/8ivu8TjCZJ",
    "id" : 638723698690801664,
    "created_at" : "2015-09-01 14:42:51 +0000",
    "user" : {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "protected" : false,
      "id_str" : "14340736",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974702103984791552\/eHtIZ2ef_normal.jpg",
      "id" : 14340736,
      "verified" : false
    }
  },
  "id" : 638738653741715456,
  "created_at" : "2015-09-01 15:42:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/9gbeWSESO7",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/pkr-wants-putrajaya-come-clean-planned-rm3b-warship-071300850.html",
      "display_url" : "sg.news.yahoo.com\/pkr-wants-putr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "638715203828498432",
  "text" : "Malaysia govt eyeing the French-made Mistral class vessel initially built for the Russian government. https:\/\/t.co\/9gbeWSESO7",
  "id" : 638715203828498432,
  "created_at" : "2015-09-01 14:09:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Airbus",
      "screen_name" : "Airbus",
      "indices" : [ 3, 10 ],
      "id_str" : "15425377",
      "id" : 15425377
    }, {
      "name" : "Emirates Airline",
      "screen_name" : "emirates",
      "indices" : [ 52, 61 ],
      "id_str" : "821045162",
      "id" : 821045162
    }, {
      "name" : "Manchester Airport",
      "screen_name" : "manairport",
      "indices" : [ 88, 99 ],
      "id_str" : "19913832",
      "id" : 19913832
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Airbus\/status\/638712409868365824\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/t8tIB1rYS1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN0pjXMXAAEl1MH.jpg",
      "id_str" : "638712409654493185",
      "id" : 638712409654493185,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN0pjXMXAAEl1MH.jpg",
      "sizes" : [ {
        "h" : 674,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 674,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 674,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 448,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/t8tIB1rYS1"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/Airbus\/status\/638712409868365824\/photo\/1",
      "indices" : [ 108, 130 ],
      "url" : "http:\/\/t.co\/t8tIB1rYS1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/CN0pjVUWsAAucwF.jpg",
      "id_str" : "638712409151156224",
      "id" : 638712409151156224,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN0pjVUWsAAucwF.jpg",
      "sizes" : [ {
        "h" : 480,
        "resize" : "fit",
        "w" : 144
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 144
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 144
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 144
      }, {
        "h" : 144,
        "resize" : "crop",
        "w" : 144
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/t8tIB1rYS1"
    } ],
    "hashtags" : [ {
      "text" : "A380",
      "indices" : [ 77, 82 ]
    }, {
      "text" : "avgeek",
      "indices" : [ 100, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "638714245803327492",
  "text" : "RT @Airbus: Big city, big plane. It\u2019s 5 years since @emirates started flying #A380 from @manairport #avgeek http:\/\/t.co\/t8tIB1rYS1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Emirates Airline",
        "screen_name" : "emirates",
        "indices" : [ 40, 49 ],
        "id_str" : "821045162",
        "id" : 821045162
      }, {
        "name" : "Manchester Airport",
        "screen_name" : "manairport",
        "indices" : [ 76, 87 ],
        "id_str" : "19913832",
        "id" : 19913832
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Airbus\/status\/638712409868365824\/photo\/1",
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/t8tIB1rYS1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN0pjXMXAAEl1MH.jpg",
        "id_str" : "638712409654493185",
        "id" : 638712409654493185,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN0pjXMXAAEl1MH.jpg",
        "sizes" : [ {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 674,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 448,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/t8tIB1rYS1"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Airbus\/status\/638712409868365824\/photo\/1",
        "indices" : [ 96, 118 ],
        "url" : "http:\/\/t.co\/t8tIB1rYS1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/CN0pjVUWsAAucwF.jpg",
        "id_str" : "638712409151156224",
        "id" : 638712409151156224,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/CN0pjVUWsAAucwF.jpg",
        "sizes" : [ {
          "h" : 480,
          "resize" : "fit",
          "w" : 144
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 144
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 144
        }, {
          "h" : 480,
          "resize" : "fit",
          "w" : 144
        }, {
          "h" : 144,
          "resize" : "crop",
          "w" : 144
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/t8tIB1rYS1"
      } ],
      "hashtags" : [ {
        "text" : "A380",
        "indices" : [ 65, 70 ]
      }, {
        "text" : "avgeek",
        "indices" : [ 88, 95 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "638712409868365824",
    "text" : "Big city, big plane. It\u2019s 5 years since @emirates started flying #A380 from @manairport #avgeek http:\/\/t.co\/t8tIB1rYS1",
    "id" : 638712409868365824,
    "created_at" : "2015-09-01 13:57:59 +0000",
    "user" : {
      "name" : "Airbus",
      "screen_name" : "Airbus",
      "protected" : false,
      "id_str" : "15425377",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007497242264076293\/XLNMqJRx_normal.jpg",
      "id" : 15425377,
      "verified" : true
    }
  },
  "id" : 638714245803327492,
  "created_at" : "2015-09-01 14:05:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 139 ],
      "url" : "http:\/\/t.co\/tsjIwyuXqW",
      "expanded_url" : "http:\/\/www.vanityfair.com\/news\/2015\/08\/is-silicon-valley-in-another-bubble",
      "display_url" : "vanityfair.com\/news\/2015\/08\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "638713959449788416",
  "text" : "GIC a Singapore's sovereign wealth fund led a $150 million round of funding for Square, the mobile-payments company. http:\/\/t.co\/tsjIwyuXqW",
  "id" : 638713959449788416,
  "created_at" : "2015-09-01 14:04:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/am8MvvdluH",
      "expanded_url" : "https:\/\/en.wikipedia.org\/wiki\/Unicorn_(finance)",
      "display_url" : "en.wikipedia.org\/wiki\/Unicorn_(\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "638713259735056385",
  "text" : "Singapore PM using the word \"Unicorn\" to describe Singapore...a buzz word widely use in the startup community. https:\/\/t.co\/am8MvvdluH",
  "id" : 638713259735056385,
  "created_at" : "2015-09-01 14:01:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/EH8WSMeWFg",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/638708796257255424",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "638711347568582656",
  "text" : "\u7B11\u91CC\u85CF\u5200\uFF0C\u5148\u793C\u540E\u5175 https:\/\/t.co\/EH8WSMeWFg",
  "id" : 638711347568582656,
  "created_at" : "2015-09-01 13:53:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "638666758589652992",
  "text" : "\"For the first time since the country's independence, all seats will be contested.\" I suspect more candidates going to lost their deposit.",
  "id" : 638666758589652992,
  "created_at" : "2015-09-01 10:56:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "indices" : [ 3, 11 ],
      "id_str" : "21071383",
      "id" : 21071383
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/EczyuGZyI3",
      "expanded_url" : "https:\/\/twitter.com\/LEGO_Singapore\/status\/638592180701917185",
      "display_url" : "twitter.com\/LEGO_Singapore\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "638613907226107904",
  "text" : "RT @joleeqh: Gotta spend $ again!! https:\/\/t.co\/EczyuGZyI3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 22, 45 ],
        "url" : "https:\/\/t.co\/EczyuGZyI3",
        "expanded_url" : "https:\/\/twitter.com\/LEGO_Singapore\/status\/638592180701917185",
        "display_url" : "twitter.com\/LEGO_Singapore\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "638613354295136256",
    "text" : "Gotta spend $ again!! https:\/\/t.co\/EczyuGZyI3",
    "id" : 638613354295136256,
    "created_at" : "2015-09-01 07:24:23 +0000",
    "user" : {
      "name" : "Jo Lee",
      "screen_name" : "joleeqh",
      "protected" : false,
      "id_str" : "21071383",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/435265895854641152\/QCz6O1FM_normal.png",
      "id" : 21071383,
      "verified" : false
    }
  },
  "id" : 638613907226107904,
  "created_at" : "2015-09-01 07:26:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]