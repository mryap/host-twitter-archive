Grailbird.data.tweets_2018_02 = 
 [ {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Bergen",
      "screen_name" : "mhbergen",
      "indices" : [ 3, 12 ],
      "id_str" : "17261311",
      "id" : 17261311
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mhbergen\/status\/966876932313763841\/photo\/1",
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/ewigWOw2LG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWsJcMcVAAAfhlR.jpg",
      "id_str" : "966876924982132736",
      "id" : 966876924982132736,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWsJcMcVAAAfhlR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ewigWOw2LG"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968957502783148032",
  "text" : "RT @mhbergen: Here is my report from a day in the south bay. https:\/\/t.co\/ewigWOw2LG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mhbergen\/status\/966876932313763841\/photo\/1",
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/ewigWOw2LG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWsJcMcVAAAfhlR.jpg",
        "id_str" : "966876924982132736",
        "id" : 966876924982132736,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWsJcMcVAAAfhlR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 500,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewigWOw2LG"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966876932313763841",
    "text" : "Here is my report from a day in the south bay. https:\/\/t.co\/ewigWOw2LG",
    "id" : 966876932313763841,
    "created_at" : "2018-02-23 03:26:39 +0000",
    "user" : {
      "name" : "Mark Bergen",
      "screen_name" : "mhbergen",
      "protected" : false,
      "id_str" : "17261311",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/767435662400233472\/OGxFtuub_normal.jpg",
      "id" : 17261311,
      "verified" : true
    }
  },
  "id" : 968957502783148032,
  "created_at" : "2018-02-28 21:14:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Gothelf",
      "screen_name" : "jboogie",
      "indices" : [ 3, 11 ],
      "id_str" : "9384812",
      "id" : 9384812
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "agile",
      "indices" : [ 101, 107 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968883539394613250",
  "text" : "RT @jboogie: Join me in Dublin on Apr 25 for this 1-day, fun, hands-on workshop designed to get your #agile process moving forward more eff\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jboogie\/status\/968756420224192513\/photo\/1",
        "indices" : [ 259, 282 ],
        "url" : "https:\/\/t.co\/M62gmRiDgh",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXG21GgX0AAU7J4.jpg",
        "id_str" : "968756418257080320",
        "id" : 968756418257080320,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXG21GgX0AAU7J4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 525,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 525,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 525,
          "resize" : "fit",
          "w" : 700
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/M62gmRiDgh"
      } ],
      "hashtags" : [ {
        "text" : "agile",
        "indices" : [ 88, 94 ]
      } ],
      "urls" : [ {
        "indices" : [ 235, 258 ],
        "url" : "https:\/\/t.co\/Hgdfb9X47t",
        "expanded_url" : "https:\/\/buff.ly\/2GLyAaR",
        "display_url" : "buff.ly\/2GLyAaR"
      } ]
    },
    "geo" : { },
    "id_str" : "968756420224192513",
    "text" : "Join me in Dublin on Apr 25 for this 1-day, fun, hands-on workshop designed to get your #agile process moving forward more effectively:\n\n\u201CSense &amp; Respond: How To Build &amp; Lead Successful Lean Practices In High Growth Companies\u201D https:\/\/t.co\/Hgdfb9X47t https:\/\/t.co\/M62gmRiDgh",
    "id" : 968756420224192513,
    "created_at" : "2018-02-28 07:55:03 +0000",
    "user" : {
      "name" : "Jeff Gothelf",
      "screen_name" : "jboogie",
      "protected" : false,
      "id_str" : "9384812",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038705935471063040\/723yLaBX_normal.jpg",
      "id" : 9384812,
      "verified" : true
    }
  },
  "id" : 968883539394613250,
  "created_at" : "2018-02-28 16:20:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DrTempest van Schaik",
      "screen_name" : "Dr_Tempest",
      "indices" : [ 3, 14 ],
      "id_str" : "2370233868",
      "id" : 2370233868
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI",
      "indices" : [ 28, 31 ]
    }, {
      "text" : "healthcare",
      "indices" : [ 50, 61 ]
    }, {
      "text" : "genomics",
      "indices" : [ 81, 90 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968883179884105728",
  "text" : "RT @Dr_Tempest: Microsoft's #AI is expanding into #healthcare in a big way! From #genomics to physician note-taking to cardiology to medica\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 12, 15 ]
      }, {
        "text" : "healthcare",
        "indices" : [ 34, 45 ]
      }, {
        "text" : "genomics",
        "indices" : [ 65, 74 ]
      }, {
        "text" : "biomedicalEngineers",
        "indices" : [ 156, 176 ]
      } ],
      "urls" : [ {
        "indices" : [ 178, 201 ],
        "url" : "https:\/\/t.co\/kvA8DDoV0H",
        "expanded_url" : "https:\/\/blogs.microsoft.com\/blog\/2018\/02\/28\/microsofts-focus-transforming-healthcare-intelligent-health-ai-cloud\/",
        "display_url" : "blogs.microsoft.com\/blog\/2018\/02\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "968877654840631296",
    "text" : "Microsoft's #AI is expanding into #healthcare in a big way! From #genomics to physician note-taking to cardiology to medical imaging. Exciting times for us #biomedicalEngineers! https:\/\/t.co\/kvA8DDoV0H",
    "id" : 968877654840631296,
    "created_at" : "2018-02-28 15:56:48 +0000",
    "user" : {
      "name" : "DrTempest van Schaik",
      "screen_name" : "Dr_Tempest",
      "protected" : false,
      "id_str" : "2370233868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037376811141922816\/vC3N7BcB_normal.jpg",
      "id" : 2370233868,
      "verified" : false
    }
  },
  "id" : 968883179884105728,
  "created_at" : "2018-02-28 16:18:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/968866249676591110\/video\/1",
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/eFc6vbtScI",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/968866128863879168\/pu\/img\/M6C8RW_Dpxms_-a5.jpg",
      "id_str" : "968866128863879168",
      "id" : 968866128863879168,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/968866128863879168\/pu\/img\/M6C8RW_Dpxms_-a5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 480
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eFc6vbtScI"
    } ],
    "hashtags" : [ {
      "text" : "Snowmageddon",
      "indices" : [ 13, 26 ]
    }, {
      "text" : "snowday2018",
      "indices" : [ 27, 39 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968866249676591110",
  "text" : "Bad Morning? #Snowmageddon #snowday2018 https:\/\/t.co\/eFc6vbtScI",
  "id" : 968866249676591110,
  "created_at" : "2018-02-28 15:11:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Malboury Jones",
      "screen_name" : "Malboury",
      "indices" : [ 3, 12 ],
      "id_str" : "91761120",
      "id" : 91761120
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968764243679043584",
  "text" : "RT @Malboury: We moved into new offices, but this wall has been left open for a few weeks now. I knew what I had to do. https:\/\/t.co\/kRQcqM\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Malboury\/status\/968163458679263238\/photo\/1",
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/kRQcqM4UDe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DW-bgn0WkAER2b3.jpg",
        "id_str" : "968163429654630401",
        "id" : 968163429654630401,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW-bgn0WkAER2b3.jpg",
        "sizes" : [ {
          "h" : 1170,
          "resize" : "fit",
          "w" : 2080
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kRQcqM4UDe"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Malboury\/status\/968163458679263238\/photo\/1",
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/kRQcqM4UDe",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DW-bhl0XcAEVp4L.jpg",
        "id_str" : "968163446297686017",
        "id" : 968163446297686017,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW-bhl0XcAEVp4L.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1170,
          "resize" : "fit",
          "w" : 2080
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kRQcqM4UDe"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968163458679263238",
    "text" : "We moved into new offices, but this wall has been left open for a few weeks now. I knew what I had to do. https:\/\/t.co\/kRQcqM4UDe",
    "id" : 968163458679263238,
    "created_at" : "2018-02-26 16:38:50 +0000",
    "user" : {
      "name" : "Malboury Jones",
      "screen_name" : "Malboury",
      "protected" : false,
      "id_str" : "91761120",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/748211980813615104\/rn0tDaLU_normal.jpg",
      "id" : 91761120,
      "verified" : false
    }
  },
  "id" : 968764243679043584,
  "created_at" : "2018-02-28 08:26:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elea McDonnell Feit",
      "screen_name" : "eleafeit",
      "indices" : [ 3, 12 ],
      "id_str" : "107600670",
      "id" : 107600670
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968760238332678145",
  "text" : "RT @eleafeit: My thoughts on A\/B testing: \nWe are not looking to find truth.  We are looking to make good decisions.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968635207132831744",
    "text" : "My thoughts on A\/B testing: \nWe are not looking to find truth.  We are looking to make good decisions.",
    "id" : 968635207132831744,
    "created_at" : "2018-02-27 23:53:24 +0000",
    "user" : {
      "name" : "Elea McDonnell Feit",
      "screen_name" : "eleafeit",
      "protected" : false,
      "id_str" : "107600670",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1022142398086963200\/M1AUZ37a_normal.jpg",
      "id" : 107600670,
      "verified" : false
    }
  },
  "id" : 968760238332678145,
  "created_at" : "2018-02-28 08:10:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/968617412269572096\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/4V5oT1lj8F",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXE4ZyIXcAI6F_Q.jpg",
      "id_str" : "968617410466115586",
      "id" : 968617410466115586,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXE4ZyIXcAI6F_Q.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/4V5oT1lj8F"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/9LIKWifPzD",
      "expanded_url" : "http:\/\/ift.tt\/2BUeNa4",
      "display_url" : "ift.tt\/2BUeNa4"
    } ]
  },
  "geo" : { },
  "id_str" : "968617412269572096",
  "text" : "Last 2 CHinese New Year goodies. https:\/\/t.co\/9LIKWifPzD https:\/\/t.co\/4V5oT1lj8F",
  "id" : 968617412269572096,
  "created_at" : "2018-02-27 22:42:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rtept",
      "indices" : [ 92, 98 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968608685986639872",
  "text" : "DID the Minister mention New York and Germany is copying Ireland rural broadband expertise? #rtept",
  "id" : 968608685986639872,
  "created_at" : "2018-02-27 22:08:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Legg",
      "screen_name" : "memenow",
      "indices" : [ 3, 11 ],
      "id_str" : "15902234",
      "id" : 15902234
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/memenow\/status\/968599350497685504\/video\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/mhzzlqOZkV",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/968599153247956993\/pu\/img\/l9WYy3Sz84k445FI.jpg",
      "id_str" : "968599153247956993",
      "id" : 968599153247956993,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/968599153247956993\/pu\/img\/l9WYy3Sz84k445FI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mhzzlqOZkV"
    } ],
    "hashtags" : [ {
      "text" : "Sneachta",
      "indices" : [ 13, 22 ]
    }, {
      "text" : "beastfromtheast",
      "indices" : [ 28, 44 ]
    }, {
      "text" : "WestCork",
      "indices" : [ 45, 54 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968604508426301443",
  "text" : "RT @memenow: #Sneachta Fox \n#beastfromtheast\n#WestCork https:\/\/t.co\/mhzzlqOZkV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/memenow\/status\/968599350497685504\/video\/1",
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/mhzzlqOZkV",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/968599153247956993\/pu\/img\/l9WYy3Sz84k445FI.jpg",
        "id_str" : "968599153247956993",
        "id" : 968599153247956993,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/968599153247956993\/pu\/img\/l9WYy3Sz84k445FI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mhzzlqOZkV"
      } ],
      "hashtags" : [ {
        "text" : "Sneachta",
        "indices" : [ 0, 9 ]
      }, {
        "text" : "beastfromtheast",
        "indices" : [ 15, 31 ]
      }, {
        "text" : "WestCork",
        "indices" : [ 32, 41 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968599350497685504",
    "text" : "#Sneachta Fox \n#beastfromtheast\n#WestCork https:\/\/t.co\/mhzzlqOZkV",
    "id" : 968599350497685504,
    "created_at" : "2018-02-27 21:30:55 +0000",
    "user" : {
      "name" : "Adrian Legg",
      "screen_name" : "memenow",
      "protected" : false,
      "id_str" : "15902234",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009655475489656832\/PI6rYrEf_normal.jpg",
      "id" : 15902234,
      "verified" : false
    }
  },
  "id" : 968604508426301443,
  "created_at" : "2018-02-27 21:51:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "indices" : [ 3, 14 ],
      "id_str" : "166540104",
      "id" : 166540104
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 79, 86 ]
    } ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/KlwC9N0mCh",
      "expanded_url" : "http:\/\/datastorm-open.github.io\/visNetwork\/tree.html",
      "display_url" : "datastorm-open.github.io\/visNetwork\/tre\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "968604098634362880",
  "text" : "RT @longhowlam: There is a nice trick to create an interactive decison tree in #rstats with visNetwork.\nhttps:\/\/t.co\/KlwC9N0mCh https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/longhowlam\/status\/968597824806977541\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/VVvjlGvuUR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXEmP4mXcAAoQUn.jpg",
        "id_str" : "968597449194565632",
        "id" : 968597449194565632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXEmP4mXcAAoQUn.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 435
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 435
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 435
        }, {
          "h" : 315,
          "resize" : "fit",
          "w" : 435
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VVvjlGvuUR"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/longhowlam\/status\/968597824806977541\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/VVvjlGvuUR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXEmSbxW0AA-gp9.jpg",
        "id_str" : "968597492995641344",
        "id" : 968597492995641344,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXEmSbxW0AA-gp9.jpg",
        "sizes" : [ {
          "h" : 876,
          "resize" : "fit",
          "w" : 1208
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 876,
          "resize" : "fit",
          "w" : 1208
        }, {
          "h" : 493,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 870,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VVvjlGvuUR"
      } ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 63, 70 ]
      } ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/KlwC9N0mCh",
        "expanded_url" : "http:\/\/datastorm-open.github.io\/visNetwork\/tree.html",
        "display_url" : "datastorm-open.github.io\/visNetwork\/tre\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "968597824806977541",
    "text" : "There is a nice trick to create an interactive decison tree in #rstats with visNetwork.\nhttps:\/\/t.co\/KlwC9N0mCh https:\/\/t.co\/VVvjlGvuUR",
    "id" : 968597824806977541,
    "created_at" : "2018-02-27 21:24:51 +0000",
    "user" : {
      "name" : "Longhow Lam \u6797 \u9686 \u8C6A",
      "screen_name" : "longhowlam",
      "protected" : false,
      "id_str" : "166540104",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/677041208711970821\/BYuLlEyn_normal.jpg",
      "id" : 166540104,
      "verified" : false
    }
  },
  "id" : 968604098634362880,
  "created_at" : "2018-02-27 21:49:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Points Guy",
      "screen_name" : "thepointsguy",
      "indices" : [ 3, 16 ],
      "id_str" : "34176543",
      "id" : 34176543
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968589723500535812",
  "text" : "RT @thepointsguy: It's safe to say this is the most over-the-top, extravagant and luxurious first class cabin in the sky. Seriously.\n\nFor m\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/prod1.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr Prod1\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/thepointsguy\/status\/968275378866475008\/video\/1",
        "indices" : [ 180, 203 ],
        "url" : "https:\/\/t.co\/isoCEuhsRu",
        "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/968274942222532609\/img\/eC08yXL_vP4Krg5e.jpg",
        "id_str" : "968274942222532609",
        "id" : 968274942222532609,
        "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/968274942222532609\/img\/eC08yXL_vP4Krg5e.jpg",
        "sizes" : [ {
          "h" : 421,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1162
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1162
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1162
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/isoCEuhsRu"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 156, 179 ],
        "url" : "https:\/\/t.co\/iow1DSw00v",
        "expanded_url" : "http:\/\/ms.spr.ly\/6012r0PGj",
        "display_url" : "ms.spr.ly\/6012r0PGj"
      } ]
    },
    "geo" : { },
    "id_str" : "968275378866475008",
    "text" : "It's safe to say this is the most over-the-top, extravagant and luxurious first class cabin in the sky. Seriously.\n\nFor more on what it was like to fly it: https:\/\/t.co\/iow1DSw00v https:\/\/t.co\/isoCEuhsRu",
    "id" : 968275378866475008,
    "created_at" : "2018-02-27 00:03:34 +0000",
    "user" : {
      "name" : "The Points Guy",
      "screen_name" : "thepointsguy",
      "protected" : false,
      "id_str" : "34176543",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1019589513906196480\/OH_NBsft_normal.jpg",
      "id" : 34176543,
      "verified" : true
    }
  },
  "id" : 968589723500535812,
  "created_at" : "2018-02-27 20:52:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ReadinessIsYourOnlyProtection",
      "indices" : [ 58, 88 ]
    } ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/cNNXEt8vti",
      "expanded_url" : "https:\/\/twitter.com\/Colmogorman\/status\/968256104286351362",
      "display_url" : "twitter.com\/Colmogorman\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "968587919882686464",
  "text" : "Good to see people are taking it seriously and preparing. #ReadinessIsYourOnlyProtection https:\/\/t.co\/cNNXEt8vti",
  "id" : 968587919882686464,
  "created_at" : "2018-02-27 20:45:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Financial Times",
      "screen_name" : "FinancialTimes",
      "indices" : [ 104, 119 ],
      "id_str" : "4898091",
      "id" : 4898091
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/KknZmVTwrj",
      "expanded_url" : "https:\/\/www.ft.com\/content\/b84a6ff8-17e3-11e8-9376-4a6390addb44",
      "display_url" : "ft.com\/content\/b84a6f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "968536190948343808",
  "text" : "Anatomy of Failure: Why America loses every war it starts, by Harlan Ullman https:\/\/t.co\/KknZmVTwrj via @financialtimes",
  "id" : 968536190948343808,
  "created_at" : "2018-02-27 17:19:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 158, 181 ],
      "url" : "https:\/\/t.co\/R9EBWkpGWn",
      "expanded_url" : "http:\/\/firstround.com\/review\/im-sorry-but-those-are-vanity-metrics",
      "display_url" : "firstround.com\/review\/im-sorr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "968533235843190790",
  "text" : "\"daily active users to revenue growth often slant toward what investors want to measure, showing if a company is valuable, not how it can create more value.\" https:\/\/t.co\/R9EBWkpGWn",
  "id" : 968533235843190790,
  "created_at" : "2018-02-27 17:08:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968434432158978050",
  "text" : "I am grounded not because of weather but the annual safety inspection is due today anytime from 9 to 5.",
  "id" : 968434432158978050,
  "created_at" : "2018-02-27 10:35:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968432214001242114",
  "text" : "For a nation that takes pride in their independence (rightly so) there is no singing of national anthem in school before class starts.",
  "id" : 968432214001242114,
  "created_at" : "2018-02-27 10:26:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968426298564599808",
  "text" : "I always make it a point to revisit those filled position (thanks to those who update their Linkedin profile) why I have not being selected for the role.",
  "id" : 968426298564599808,
  "created_at" : "2018-02-27 10:03:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/FNFevuHJfj",
      "expanded_url" : "http:\/\/str.sg\/o5Dt",
      "display_url" : "str.sg\/o5Dt"
    } ]
  },
  "geo" : { },
  "id_str" : "968391714271649793",
  "text" : "Last time I checked in Dublin is about EUR 6.95  https:\/\/t.co\/FNFevuHJfj",
  "id" : 968391714271649793,
  "created_at" : "2018-02-27 07:45:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ann Donnelly",
      "screen_name" : "ann_donnelly",
      "indices" : [ 3, 16 ],
      "id_str" : "17484033",
      "id" : 17484033
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968387661013635077",
  "text" : "RT @ann_donnelly: Is there a reason why James Joyce is not in the Irish secondary school syllabus?!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968264659710201856",
    "text" : "Is there a reason why James Joyce is not in the Irish secondary school syllabus?!",
    "id" : 968264659710201856,
    "created_at" : "2018-02-26 23:20:59 +0000",
    "user" : {
      "name" : "Ann Donnelly",
      "screen_name" : "ann_donnelly",
      "protected" : false,
      "id_str" : "17484033",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1033138108928348160\/u35SDSYz_normal.jpg",
      "id" : 17484033,
      "verified" : false
    }
  },
  "id" : 968387661013635077,
  "created_at" : "2018-02-27 07:29:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PJ Gallagher",
      "screen_name" : "pjgallagher",
      "indices" : [ 3, 15 ],
      "id_str" : "22624932",
      "id" : 22624932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968262277290954754",
  "text" : "RT @pjgallagher: There is no bread or butter left in Dun Laoghaire, it seems Ireland prepares for snow by making shit loads of toast and sa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968183381061259264",
    "text" : "There is no bread or butter left in Dun Laoghaire, it seems Ireland prepares for snow by making shit loads of toast and sandwiches.",
    "id" : 968183381061259264,
    "created_at" : "2018-02-26 17:58:00 +0000",
    "user" : {
      "name" : "PJ Gallagher",
      "screen_name" : "pjgallagher",
      "protected" : false,
      "id_str" : "22624932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028975732582752257\/yluiCAPB_normal.jpg",
      "id" : 22624932,
      "verified" : true
    }
  },
  "id" : 968262277290954754,
  "created_at" : "2018-02-26 23:11:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 91, 107 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/LtDndNcjwz",
      "expanded_url" : "http:\/\/po.st\/Jfui5R",
      "display_url" : "po.st\/Jfui5R"
    } ]
  },
  "geo" : { },
  "id_str" : "968253800044793862",
  "text" : "'Wake up' and stop Rohingya abuses: Nobel laureates to Suu Kyi https:\/\/t.co\/LtDndNcjwz via @ChannelNewsAsia",
  "id" : 968253800044793862,
  "created_at" : "2018-02-26 22:37:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/968178209555697664\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/gFBI39dBWJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DW-oq5CWkAE6Qd3.jpg",
      "id_str" : "968177899726606337",
      "id" : 968177899726606337,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW-oq5CWkAE6Qd3.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 1362
      }, {
        "h" : 696,
        "resize" : "fit",
        "w" : 1362
      }, {
        "h" : 613,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 347,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gFBI39dBWJ"
    } ],
    "hashtags" : [ {
      "text" : "Azure",
      "indices" : [ 91, 97 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968178209555697664",
  "text" : "I try deploying to Azure with SQL Server setup but where do I access my login credentials? #Azure Thanks https:\/\/t.co\/gFBI39dBWJ",
  "id" : 968178209555697664,
  "created_at" : "2018-02-26 17:37:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/edVlHJleDQ",
      "expanded_url" : "http:\/\/shr.gs\/c6QQNeU",
      "display_url" : "shr.gs\/c6QQNeU"
    } ]
  },
  "geo" : { },
  "id_str" : "968142241989578754",
  "text" : "Singapore may renegotiate EU trade deal after Brexit removes British markets https:\/\/t.co\/edVlHJleDQ",
  "id" : 968142241989578754,
  "created_at" : "2018-02-26 15:14:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    }, {
      "name" : "Bruce Bixler",
      "screen_name" : "BruceBixler49",
      "indices" : [ 10, 24 ],
      "id_str" : "84331472",
      "id" : 84331472
    }, {
      "name" : "Social-Hire.com",
      "screen_name" : "Social_Hire",
      "indices" : [ 25, 37 ],
      "id_str" : "390191371",
      "id" : 390191371
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/968137572936544257\/photo\/1",
      "indices" : [ 149, 172 ],
      "url" : "https:\/\/t.co\/yxo6a4MGWV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DW-DijvW4AEdWHC.jpg",
      "id_str" : "968137074640609281",
      "id" : 968137074640609281,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW-DijvW4AEdWHC.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 692,
        "resize" : "fit",
        "w" : 1270
      }, {
        "h" : 371,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 654,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 692,
        "resize" : "fit",
        "w" : 1270
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yxo6a4MGWV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "968131178543898624",
  "geo" : { },
  "id_str" : "968137572936544257",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose @BruceBixler49 @Social_Hire In your example, \u201CDirector of Franchise Development\u201D, there is \u201CFranchise development\u201D in the skills sections. https:\/\/t.co\/yxo6a4MGWV",
  "id" : 968137572936544257,
  "in_reply_to_status_id" : 968131178543898624,
  "created_at" : "2018-02-26 14:55:59 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 3, 12 ],
      "id_str" : "1291131",
      "id" : 1291131
    }, {
      "name" : "Bruce Bixler",
      "screen_name" : "BruceBixler49",
      "indices" : [ 67, 81 ],
      "id_str" : "84331472",
      "id" : 84331472
    }, {
      "name" : "Social-Hire.com",
      "screen_name" : "Social_Hire",
      "indices" : [ 123, 135 ],
      "id_str" : "390191371",
      "id" : 390191371
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LinkedIn",
      "indices" : [ 53, 62 ]
    }, {
      "text" : "SocialMedia",
      "indices" : [ 106, 118 ]
    } ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/9izjshFWdb",
      "expanded_url" : "https:\/\/buff.ly\/2sKPDrm",
      "display_url" : "buff.ly\/2sKPDrm"
    } ]
  },
  "geo" : { },
  "id_str" : "968135250206437376",
  "text" : "RT @maryrose: Don\u2019t Waste Your Time with KEYWORDS on #LinkedIn! by @BruceBixler49 https:\/\/t.co\/9izjshFWdb #SocialMedia via @Social_Hire htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bruce Bixler",
        "screen_name" : "BruceBixler49",
        "indices" : [ 53, 67 ],
        "id_str" : "84331472",
        "id" : 84331472
      }, {
        "name" : "Social-Hire.com",
        "screen_name" : "Social_Hire",
        "indices" : [ 109, 121 ],
        "id_str" : "390191371",
        "id" : 390191371
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/maryrose\/status\/968131178543898624\/photo\/1",
        "indices" : [ 122, 145 ],
        "url" : "https:\/\/t.co\/6r52CPhGc5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DW9-LMlUQAAdx4F.jpg",
        "id_str" : "968131175729348608",
        "id" : 968131175729348608,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW9-LMlUQAAdx4F.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 648,
          "resize" : "fit",
          "w" : 1025
        }, {
          "h" : 648,
          "resize" : "fit",
          "w" : 1025
        }, {
          "h" : 430,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 648,
          "resize" : "fit",
          "w" : 1025
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6r52CPhGc5"
      } ],
      "hashtags" : [ {
        "text" : "LinkedIn",
        "indices" : [ 39, 48 ]
      }, {
        "text" : "SocialMedia",
        "indices" : [ 92, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 68, 91 ],
        "url" : "https:\/\/t.co\/9izjshFWdb",
        "expanded_url" : "https:\/\/buff.ly\/2sKPDrm",
        "display_url" : "buff.ly\/2sKPDrm"
      } ]
    },
    "geo" : { },
    "id_str" : "968131178543898624",
    "text" : "Don\u2019t Waste Your Time with KEYWORDS on #LinkedIn! by @BruceBixler49 https:\/\/t.co\/9izjshFWdb #SocialMedia via @Social_Hire https:\/\/t.co\/6r52CPhGc5",
    "id" : 968131178543898624,
    "created_at" : "2018-02-26 14:30:34 +0000",
    "user" : {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "protected" : false,
      "id_str" : "1291131",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034068926370594816\/eS9-sUNe_normal.jpg",
      "id" : 1291131,
      "verified" : false
    }
  },
  "id" : 968135250206437376,
  "created_at" : "2018-02-26 14:46:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jennie mcginn",
      "screen_name" : "JennieMcGinn",
      "indices" : [ 0, 13 ],
      "id_str" : "149082308",
      "id" : 149082308
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "968079606203977728",
  "geo" : { },
  "id_str" : "968081160042696704",
  "in_reply_to_user_id" : 149082308,
  "text" : "@JennieMcGinn Do you accept the invitation?",
  "id" : 968081160042696704,
  "in_reply_to_status_id" : 968079606203977728,
  "created_at" : "2018-02-26 11:11:49 +0000",
  "in_reply_to_screen_name" : "JennieMcGinn",
  "in_reply_to_user_id_str" : "149082308",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/5gL00OqHz6",
      "expanded_url" : "https:\/\/www.reddit.com\/r\/funny\/comments\/7zm0un\/jeff_goldblum_request_escalates_quickly\/",
      "display_url" : "reddit.com\/r\/funny\/commen\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "968071691091021826",
  "text" : "Hotel gets thumbs up for filling room with Jeff Goldblum pictures https:\/\/t.co\/5gL00OqHz6",
  "id" : 968071691091021826,
  "created_at" : "2018-02-26 10:34:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Temasek",
      "screen_name" : "Temasek",
      "indices" : [ 3, 11 ],
      "id_str" : "1549668462",
      "id" : 1549668462
    }, {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 125, 137 ],
      "id_str" : "41085467",
      "id" : 41085467
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 27, 37 ]
    }, {
      "text" : "bus",
      "indices" : [ 87, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/Q457ohrUGa",
      "expanded_url" : "http:\/\/tmsk.sg\/gF",
      "display_url" : "tmsk.sg\/gF"
    } ]
  },
  "geo" : { },
  "id_str" : "968069079700557825",
  "text" : "RT @Temasek: Get on board: #Singapore is one step closer to launching on-demand public #bus services https:\/\/t.co\/Q457ohrUGa @TODAYonline",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.falcon.io\" rel=\"nofollow\"\u003EFalcon Social Media Management \u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TODAY",
        "screen_name" : "TODAYonline",
        "indices" : [ 112, 124 ],
        "id_str" : "41085467",
        "id" : 41085467
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 14, 24 ]
      }, {
        "text" : "bus",
        "indices" : [ 74, 78 ]
      } ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/Q457ohrUGa",
        "expanded_url" : "http:\/\/tmsk.sg\/gF",
        "display_url" : "tmsk.sg\/gF"
      } ]
    },
    "geo" : { },
    "id_str" : "966243603076009984",
    "text" : "Get on board: #Singapore is one step closer to launching on-demand public #bus services https:\/\/t.co\/Q457ohrUGa @TODAYonline",
    "id" : 966243603076009984,
    "created_at" : "2018-02-21 09:30:01 +0000",
    "user" : {
      "name" : "Temasek",
      "screen_name" : "Temasek",
      "protected" : false,
      "id_str" : "1549668462",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617977683780112384\/JAICZY_m_normal.jpg",
      "id" : 1549668462,
      "verified" : true
    }
  },
  "id" : 968069079700557825,
  "created_at" : "2018-02-26 10:23:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Acas",
      "screen_name" : "acasorguk",
      "indices" : [ 3, 13 ],
      "id_str" : "36380531",
      "id" : 36380531
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "internships",
      "indices" : [ 51, 63 ]
    }, {
      "text" : "workexperience",
      "indices" : [ 65, 80 ]
    }, {
      "text" : "volunteers",
      "indices" : [ 93, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968068063513010176",
  "text" : "RT @acasorguk: Do you know the differences between #internships, #workexperience placements, #volunteers &amp; what employment rights they have\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Student Volunteering Week",
        "screen_name" : "StudentVolWeek",
        "indices" : [ 163, 178 ],
        "id_str" : "1083818154",
        "id" : 1083818154
      }, {
        "name" : "NUS UK",
        "screen_name" : "nusuk",
        "indices" : [ 179, 185 ],
        "id_str" : "19904718",
        "id" : 19904718
      }, {
        "name" : "Student Hubs",
        "screen_name" : "studenthubs",
        "indices" : [ 186, 198 ],
        "id_str" : "64715663",
        "id" : 64715663
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/acasorguk\/status\/965881259988848640\/photo\/1",
        "indices" : [ 199, 222 ],
        "url" : "https:\/\/t.co\/EhoyV00Hk7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWd_4mgUMAAGnwq.jpg",
        "id_str" : "965881255479750656",
        "id" : 965881255479750656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWd_4mgUMAAGnwq.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1367,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 4016,
          "resize" : "fit",
          "w" : 6016
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EhoyV00Hk7"
      } ],
      "hashtags" : [ {
        "text" : "internships",
        "indices" : [ 36, 48 ]
      }, {
        "text" : "workexperience",
        "indices" : [ 50, 65 ]
      }, {
        "text" : "volunteers",
        "indices" : [ 78, 89 ]
      }, {
        "text" : "SVW2018",
        "indices" : [ 154, 162 ]
      } ],
      "urls" : [ {
        "indices" : [ 130, 153 ],
        "url" : "https:\/\/t.co\/f8sIGELZ9s",
        "expanded_url" : "http:\/\/bit.ly\/1EniZFd",
        "display_url" : "bit.ly\/1EniZFd"
      } ]
    },
    "geo" : { },
    "id_str" : "965881259988848640",
    "text" : "Do you know the differences between #internships, #workexperience placements, #volunteers &amp; what employment rights they have? https:\/\/t.co\/f8sIGELZ9s #SVW2018 @StudentVolWeek @NUSUK @studenthubs https:\/\/t.co\/EhoyV00Hk7",
    "id" : 965881259988848640,
    "created_at" : "2018-02-20 09:30:12 +0000",
    "user" : {
      "name" : "Acas",
      "screen_name" : "acasorguk",
      "protected" : false,
      "id_str" : "36380531",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/935074944114978816\/YIr9l2y-_normal.jpg",
      "id" : 36380531,
      "verified" : true
    }
  },
  "id" : 968068063513010176,
  "created_at" : "2018-02-26 10:19:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968067635823947779",
  "text" : "Interesting to note that you can insert an emoji on Linkedin post.",
  "id" : 968067635823947779,
  "created_at" : "2018-02-26 10:18:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GDPR",
      "indices" : [ 17, 22 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968055559244206080",
  "text" : "When it Comes to #GDPR, it's hard to Separate Myth From Fact.",
  "id" : 968055559244206080,
  "created_at" : "2018-02-26 09:30:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GrantNav",
      "indices" : [ 8, 17 ]
    }, {
      "text" : "Manchester",
      "indices" : [ 73, 84 ]
    }, {
      "text" : "opendata",
      "indices" : [ 85, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/e19DeLKHPS",
      "expanded_url" : "http:\/\/bit.ly\/mcrgrant",
      "display_url" : "bit.ly\/mcrgrant"
    } ]
  },
  "geo" : { },
  "id_str" : "968051137722179584",
  "text" : "Explore #GrantNav to find out who is funding what, where and how much in #Manchester #opendata  https:\/\/t.co\/e19DeLKHPS",
  "id" : 968051137722179584,
  "created_at" : "2018-02-26 09:12:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 3, 13 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968050422601736192",
  "text" : "RT @barryhand: When did LinkedIn turn into Aesop's Fables? Every second post some smug made up story.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968050123283619840",
    "text" : "When did LinkedIn turn into Aesop's Fables? Every second post some smug made up story.",
    "id" : 968050123283619840,
    "created_at" : "2018-02-26 09:08:29 +0000",
    "user" : {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "protected" : false,
      "id_str" : "18849187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614532770300329984\/xJWWAtSu_normal.jpg",
      "id" : 18849187,
      "verified" : false
    }
  },
  "id" : 968050422601736192,
  "created_at" : "2018-02-26 09:09:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "UN Climate Change",
      "screen_name" : "UNFCCC",
      "indices" : [ 3, 10 ],
      "id_str" : "17463923",
      "id" : 17463923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "968021392758378496",
  "text" : "RT @UNFCCC: Whilst it was cold in Europe today, a reminder that the  temperature in the Arctic was more than 20\u00B0C warmer than normal for th\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Zack Labe",
        "screen_name" : "ZLabe",
        "indices" : [ 148, 154 ],
        "id_str" : "344395626",
        "id" : 344395626
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/UNFCCC\/status\/967901517641670656\/photo\/1",
        "indices" : [ 262, 285 ],
        "url" : "https:\/\/t.co\/dZdXF3x2RS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DW6tSsPXcAAPhYJ.jpg",
        "id_str" : "967901506556096512",
        "id" : 967901506556096512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW6tSsPXcAAPhYJ.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/dZdXF3x2RS"
      } ],
      "hashtags" : [ {
        "text" : "ClimateChange",
        "indices" : [ 247, 261 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967901517641670656",
    "text" : "Whilst it was cold in Europe today, a reminder that the  temperature in the Arctic was more than 20\u00B0C warmer than normal for this date. Dataviz vis @ZLabe. See how the red line for 2018 compares with the white line that shows average temperatures #ClimateChange https:\/\/t.co\/dZdXF3x2RS",
    "id" : 967901517641670656,
    "created_at" : "2018-02-25 23:17:59 +0000",
    "user" : {
      "name" : "UN Climate Change",
      "screen_name" : "UNFCCC",
      "protected" : false,
      "id_str" : "17463923",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/558658454030540802\/CGw9qhaD_normal.jpeg",
      "id" : 17463923,
      "verified" : true
    }
  },
  "id" : 968021392758378496,
  "created_at" : "2018-02-26 07:14:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kyle Shevlin",
      "screen_name" : "kyleshevlin",
      "indices" : [ 3, 15 ],
      "id_str" : "85008547",
      "id" : 85008547
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967889955455471616",
  "text" : "RT @kyleshevlin: Friends, if someone you admire and interact with on Twitter is visiting your town, take a shot on meeting up with them. Yo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967161510878314497",
    "text" : "Friends, if someone you admire and interact with on Twitter is visiting your town, take a shot on meeting up with them. You\u2019ll be surprised how often it works out.",
    "id" : 967161510878314497,
    "created_at" : "2018-02-23 22:17:27 +0000",
    "user" : {
      "name" : "Kyle Shevlin",
      "screen_name" : "kyleshevlin",
      "protected" : false,
      "id_str" : "85008547",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/786039150667411456\/t_0mWTZk_normal.jpg",
      "id" : 85008547,
      "verified" : false
    }
  },
  "id" : 967889955455471616,
  "created_at" : "2018-02-25 22:32:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "From the Flight Deck",
      "screen_name" : "Golfcharlie232",
      "indices" : [ 3, 18 ],
      "id_str" : "930755719",
      "id" : 930755719
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "avgeek",
      "indices" : [ 87, 94 ]
    }, {
      "text" : "pilotsview",
      "indices" : [ 95, 106 ]
    }, {
      "text" : "pilotlife",
      "indices" : [ 107, 117 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/3rnQiug6SW",
      "expanded_url" : "https:\/\/theaviationist.com\/2018\/02\/24\/check-out-these-amazing-photos-taken-by-a-u-2-pilot-at-the-edge-of-space\/",
      "display_url" : "theaviationist.com\/2018\/02\/24\/che\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967864439738335234",
  "text" : "RT @Golfcharlie232: Photos taken by a U-2 pilot at 70,000 feet https:\/\/t.co\/3rnQiug6SW #avgeek #pilotsview #pilotlife",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "avgeek",
        "indices" : [ 67, 74 ]
      }, {
        "text" : "pilotsview",
        "indices" : [ 75, 86 ]
      }, {
        "text" : "pilotlife",
        "indices" : [ 87, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 43, 66 ],
        "url" : "https:\/\/t.co\/3rnQiug6SW",
        "expanded_url" : "https:\/\/theaviationist.com\/2018\/02\/24\/check-out-these-amazing-photos-taken-by-a-u-2-pilot-at-the-edge-of-space\/",
        "display_url" : "theaviationist.com\/2018\/02\/24\/che\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "967731147865280512",
    "text" : "Photos taken by a U-2 pilot at 70,000 feet https:\/\/t.co\/3rnQiug6SW #avgeek #pilotsview #pilotlife",
    "id" : 967731147865280512,
    "created_at" : "2018-02-25 12:00:59 +0000",
    "user" : {
      "name" : "From the Flight Deck",
      "screen_name" : "Golfcharlie232",
      "protected" : false,
      "id_str" : "930755719",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/675437992551559173\/Fdru0LpD_normal.jpg",
      "id" : 930755719,
      "verified" : false
    }
  },
  "id" : 967864439738335234,
  "created_at" : "2018-02-25 20:50:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "marketing",
      "indices" : [ 30, 40 ]
    }, {
      "text" : "Manchester",
      "indices" : [ 79, 90 ]
    }, {
      "text" : "PNLive",
      "indices" : [ 126, 133 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967863849880899585",
  "text" : "RT @ProlificLive: The largest #marketing expo outside of London is arriving in #Manchester next week.\n\nRegister for your free #PNLive ticke\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "marketing",
        "indices" : [ 12, 22 ]
      }, {
        "text" : "Manchester",
        "indices" : [ 61, 72 ]
      }, {
        "text" : "PNLive",
        "indices" : [ 108, 115 ]
      } ],
      "urls" : [ {
        "indices" : [ 134, 157 ],
        "url" : "https:\/\/t.co\/KTUssvWCnm",
        "expanded_url" : "http:\/\/bit.ly\/2DWsDY5",
        "display_url" : "bit.ly\/2DWsDY5"
      } ]
    },
    "geo" : { },
    "id_str" : "967860754148835329",
    "text" : "The largest #marketing expo outside of London is arriving in #Manchester next week.\n\nRegister for your free #PNLive ticket here -&gt; https:\/\/t.co\/KTUssvWCnm",
    "id" : 967860754148835329,
    "created_at" : "2018-02-25 20:36:00 +0000",
    "user" : {
      "name" : "Marketing Show North",
      "screen_name" : "MktgShowNorth",
      "protected" : false,
      "id_str" : "3004063229",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989514715968823302\/t9SCLJNL_normal.jpg",
      "id" : 3004063229,
      "verified" : false
    }
  },
  "id" : 967863849880899585,
  "created_at" : "2018-02-25 20:48:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 3, 15 ],
      "id_str" : "168148402",
      "id" : 168148402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/wq43oQawwY",
      "expanded_url" : "https:\/\/youtu.be\/8GdA4z9G4Qg",
      "display_url" : "youtu.be\/8GdA4z9G4Qg"
    } ]
  },
  "geo" : { },
  "id_str" : "967834268486504449",
  "text" : "RT @m4riannelee: Samsung Galaxy Unpacked 2018 live stream https:\/\/t.co\/wq43oQawwY \n\nLoving it already!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 41, 64 ],
        "url" : "https:\/\/t.co\/wq43oQawwY",
        "expanded_url" : "https:\/\/youtu.be\/8GdA4z9G4Qg",
        "display_url" : "youtu.be\/8GdA4z9G4Qg"
      } ]
    },
    "geo" : { },
    "id_str" : "967825001243140097",
    "text" : "Samsung Galaxy Unpacked 2018 live stream https:\/\/t.co\/wq43oQawwY \n\nLoving it already!",
    "id" : 967825001243140097,
    "created_at" : "2018-02-25 18:13:56 +0000",
    "user" : {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "protected" : false,
      "id_str" : "168148402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797414171038060544\/7AkMKjj5_normal.jpg",
      "id" : 168148402,
      "verified" : false
    }
  },
  "id" : 967834268486504449,
  "created_at" : "2018-02-25 18:50:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967815235238354946",
  "text" : "Why people are dying around the age of 50 nowadays?",
  "id" : 967815235238354946,
  "created_at" : "2018-02-25 17:35:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Philip Larkin",
      "screen_name" : "philiplarkin",
      "indices" : [ 3, 16 ],
      "id_str" : "18997900",
      "id" : 18997900
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967796687573078016",
  "text" : "RT @philiplarkin: Just spotted a cat on someone's porch, miaowing to be let in. Without thinking, I walked up to the door, rang the bell, n\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967477083625619457",
    "text" : "Just spotted a cat on someone's porch, miaowing to be let in. Without thinking, I walked up to the door, rang the bell, nodded to the cat and left. It was only I rounded the corner I realised what I'd done as I heard the owner shouting FUCK ME SARAH THE CAT JUST RANG THE DOORBELL",
    "id" : 967477083625619457,
    "created_at" : "2018-02-24 19:11:26 +0000",
    "user" : {
      "name" : "Philip Larkin",
      "screen_name" : "philiplarkin",
      "protected" : false,
      "id_str" : "18997900",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032743101700423685\/RuQo8mpG_normal.jpg",
      "id" : 18997900,
      "verified" : false
    }
  },
  "id" : 967796687573078016,
  "created_at" : "2018-02-25 16:21:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/oLuKFWeec4",
      "expanded_url" : "https:\/\/twitter.com\/OverheardDublin\/status\/967776638183313408",
      "display_url" : "twitter.com\/OverheardDubli\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967796373272907777",
  "text" : "\uD83D\uDE02 https:\/\/t.co\/oLuKFWeec4",
  "id" : 967796373272907777,
  "created_at" : "2018-02-25 16:20:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/967772566789197825\/photo\/1",
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/cHjdQq0OAV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DW44A76XkAARM2r.jpg",
      "id_str" : "967772558664896512",
      "id" : 967772558664896512,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW44A76XkAARM2r.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cHjdQq0OAV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967772566789197825",
  "text" : "Don't think they sell in the supermarket... https:\/\/t.co\/cHjdQq0OAV",
  "id" : 967772566789197825,
  "created_at" : "2018-02-25 14:45:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eric Holscher",
      "screen_name" : "ericholscher",
      "indices" : [ 3, 16 ],
      "id_str" : "11385212",
      "id" : 11385212
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967707954077200385",
  "text" : "RT @ericholscher: We should start a movement called \"1% for OSS\", a program where VC's and startup companies agree to invest 1% of their fu\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "sustain",
        "indices" : [ 187, 195 ]
      }, {
        "text" : "sustainoss",
        "indices" : [ 196, 207 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966845161194979328",
    "text" : "We should start a movement called \"1% for OSS\", a program where VC's and startup companies agree to invest 1% of their funding round into the infrastructure that their companies rely on. #sustain #sustainoss",
    "id" : 966845161194979328,
    "created_at" : "2018-02-23 01:20:24 +0000",
    "user" : {
      "name" : "Eric Holscher",
      "screen_name" : "ericholscher",
      "protected" : false,
      "id_str" : "11385212",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686618084279336960\/9UXiuf6J_normal.jpg",
      "id" : 11385212,
      "verified" : true
    }
  },
  "id" : 967707954077200385,
  "created_at" : "2018-02-25 10:28:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/967698027061358593\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/7e6kBAj4mG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DW30OhlW4AAssoo.jpg",
      "id_str" : "967698025324929024",
      "id" : 967698025324929024,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DW30OhlW4AAssoo.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/7e6kBAj4mG"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/rP9QmDi7Hq",
      "expanded_url" : "http:\/\/ift.tt\/2ETzKkj",
      "display_url" : "ift.tt\/2ETzKkj"
    } ]
  },
  "geo" : { },
  "id_str" : "967698027061358593",
  "text" : "Good Morning https:\/\/t.co\/rP9QmDi7Hq https:\/\/t.co\/7e6kBAj4mG",
  "id" : 967698027061358593,
  "created_at" : "2018-02-25 09:49:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "cgosimon",
      "screen_name" : "cgosimon",
      "indices" : [ 3, 12 ],
      "id_str" : "14831040",
      "id" : 14831040
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cgosimon\/status\/952441693667672066\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/64fmriUOQZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DTfAq9IVoAECHQ7.jpg",
      "id_str" : "952441690408722433",
      "id" : 952441690408722433,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DTfAq9IVoAECHQ7.jpg",
      "sizes" : [ {
        "h" : 282,
        "resize" : "fit",
        "w" : 598
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 598
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 598
      }, {
        "h" : 282,
        "resize" : "fit",
        "w" : 598
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/64fmriUOQZ"
    } ],
    "hashtags" : [ {
      "text" : "storymapping_2",
      "indices" : [ 49, 64 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967689181165182976",
  "text" : "RT @cgosimon: What if we just kept on iterating? #storymapping_2.0 https:\/\/t.co\/64fmriUOQZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/twitterrific.com\/ios\" rel=\"nofollow\"\u003ETwitterrific for iOS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cgosimon\/status\/952441693667672066\/photo\/1",
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/64fmriUOQZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DTfAq9IVoAECHQ7.jpg",
        "id_str" : "952441690408722433",
        "id" : 952441690408722433,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DTfAq9IVoAECHQ7.jpg",
        "sizes" : [ {
          "h" : 282,
          "resize" : "fit",
          "w" : 598
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 598
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 598
        }, {
          "h" : 282,
          "resize" : "fit",
          "w" : 598
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/64fmriUOQZ"
      } ],
      "hashtags" : [ {
        "text" : "storymapping_2",
        "indices" : [ 35, 50 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "952441693667672066",
    "text" : "What if we just kept on iterating? #storymapping_2.0 https:\/\/t.co\/64fmriUOQZ",
    "id" : 952441693667672066,
    "created_at" : "2018-01-14 07:26:09 +0000",
    "user" : {
      "name" : "cgosimon",
      "screen_name" : "cgosimon",
      "protected" : false,
      "id_str" : "14831040",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/63241525\/Mugshot_normal.jpg",
      "id" : 14831040,
      "verified" : false
    }
  },
  "id" : 967689181165182976,
  "created_at" : "2018-02-25 09:14:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darragh Doyle",
      "screen_name" : "darraghdoyle",
      "indices" : [ 3, 16 ],
      "id_str" : "2200721",
      "id" : 2200721
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/QhjZFgIl7v",
      "expanded_url" : "https:\/\/twitter.com\/francisxyzk\/status\/967446990094290944",
      "display_url" : "twitter.com\/francisxyzk\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967687749485637632",
  "text" : "RT @darraghdoyle: Isn't this beautiful? \u2665\uFE0F https:\/\/t.co\/QhjZFgIl7v",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 25, 48 ],
        "url" : "https:\/\/t.co\/QhjZFgIl7v",
        "expanded_url" : "https:\/\/twitter.com\/francisxyzk\/status\/967446990094290944",
        "display_url" : "twitter.com\/francisxyzk\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "967549557717512192",
    "text" : "Isn't this beautiful? \u2665\uFE0F https:\/\/t.co\/QhjZFgIl7v",
    "id" : 967549557717512192,
    "created_at" : "2018-02-24 23:59:25 +0000",
    "user" : {
      "name" : "Darragh Doyle",
      "screen_name" : "darraghdoyle",
      "protected" : false,
      "id_str" : "2200721",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/649950567951900672\/9oElFhuS_normal.jpg",
      "id" : 2200721,
      "verified" : false
    }
  },
  "id" : 967687749485637632,
  "created_at" : "2018-02-25 09:08:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 264, 287 ],
      "url" : "https:\/\/t.co\/3XxeXjaOTz",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/950385068043841538",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967498859046952961",
  "text" : "Currently working with an independent food stall trader who has to hire a helper on living wAges. Not such nonsense as \u201CThis is an unpaid internship but we can guarantee you will learn heaps about business \u2013 ask our other interns, we\u2019re pretty sure they all love\" https:\/\/t.co\/3XxeXjaOTz",
  "id" : 967498859046952961,
  "created_at" : "2018-02-24 20:37:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gus Kenworthy",
      "screen_name" : "guskenworthy",
      "indices" : [ 3, 16 ],
      "id_str" : "75892358",
      "id" : 75892358
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/guskenworthy\/status\/966701117479571457\/photo\/1",
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/Hw9DGicjN0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWppg5sVAAAB33j.jpg",
      "id_str" : "966701083987869696",
      "id" : 966701083987869696,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWppg5sVAAAB33j.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Hw9DGicjN0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967488223726489611",
  "text" : "RT @guskenworthy: same https:\/\/t.co\/Hw9DGicjN0",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/guskenworthy\/status\/966701117479571457\/photo\/1",
        "indices" : [ 5, 28 ],
        "url" : "https:\/\/t.co\/Hw9DGicjN0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWppg5sVAAAB33j.jpg",
        "id_str" : "966701083987869696",
        "id" : 966701083987869696,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWppg5sVAAAB33j.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Hw9DGicjN0"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966701117479571457",
    "text" : "same https:\/\/t.co\/Hw9DGicjN0",
    "id" : 966701117479571457,
    "created_at" : "2018-02-22 15:48:01 +0000",
    "user" : {
      "name" : "Gus Kenworthy",
      "screen_name" : "guskenworthy",
      "protected" : false,
      "id_str" : "75892358",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/763800818441003008\/o4IDxOqw_normal.jpg",
      "id" : 75892358,
      "verified" : true
    }
  },
  "id" : 967488223726489611,
  "created_at" : "2018-02-24 19:55:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Cullen",
      "screen_name" : "jamescullen123",
      "indices" : [ 3, 18 ],
      "id_str" : "51524591",
      "id" : 51524591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967488080797229057",
  "text" : "RT @jamescullen123: For those that are interested. These are all the fonts used on the classic Huy Fong Sriracha Hot Sauce bottle. https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jamescullen123\/status\/966672438816858113\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/eYWyNbDXJE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWpPcuHXkAA9aSF.jpg",
        "id_str" : "966672424858259456",
        "id" : 966672424858259456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWpPcuHXkAA9aSF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1967
        }, {
          "h" : 2074,
          "resize" : "fit",
          "w" : 1992
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 653
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1153
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eYWyNbDXJE"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966672438816858113",
    "text" : "For those that are interested. These are all the fonts used on the classic Huy Fong Sriracha Hot Sauce bottle. https:\/\/t.co\/eYWyNbDXJE",
    "id" : 966672438816858113,
    "created_at" : "2018-02-22 13:54:03 +0000",
    "user" : {
      "name" : "James Cullen",
      "screen_name" : "jamescullen123",
      "protected" : false,
      "id_str" : "51524591",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1778478399\/Twitter_normal.png",
      "id" : 51524591,
      "verified" : false
    }
  },
  "id" : 967488080797229057,
  "created_at" : "2018-02-24 19:55:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "cgosimon",
      "screen_name" : "cgosimon",
      "indices" : [ 3, 12 ],
      "id_str" : "14831040",
      "id" : 14831040
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967487740236521472",
  "text" : "RT @cgosimon: I have a feeling that \u201Cin the Real World\u201D is just an aggressive way of saying \u201Cin my experience\u201D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966911437615505410",
    "text" : "I have a feeling that \u201Cin the Real World\u201D is just an aggressive way of saying \u201Cin my experience\u201D",
    "id" : 966911437615505410,
    "created_at" : "2018-02-23 05:43:45 +0000",
    "user" : {
      "name" : "cgosimon",
      "screen_name" : "cgosimon",
      "protected" : false,
      "id_str" : "14831040",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/63241525\/Mugshot_normal.jpg",
      "id" : 14831040,
      "verified" : false
    }
  },
  "id" : 967487740236521472,
  "created_at" : "2018-02-24 19:53:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/ZYZovLUWID",
      "expanded_url" : "https:\/\/twitter.com\/sakirkhader\/status\/966644734738432000",
      "display_url" : "twitter.com\/sakirkhader\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967460659784011777",
  "text" : "RT @lisaocarroll: Tells a thousand words https:\/\/t.co\/ZYZovLUWID",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/ZYZovLUWID",
        "expanded_url" : "https:\/\/twitter.com\/sakirkhader\/status\/966644734738432000",
        "display_url" : "twitter.com\/sakirkhader\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966806426743078912",
    "text" : "Tells a thousand words https:\/\/t.co\/ZYZovLUWID",
    "id" : 966806426743078912,
    "created_at" : "2018-02-22 22:46:29 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 967460659784011777,
  "created_at" : "2018-02-24 18:06:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967459719626547205",
  "text" : "RT @getoptimise: This story will help you think more deeply about the experience of getting value out of data science\/ machine learning\/ bi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 151, 174 ],
        "url" : "https:\/\/t.co\/Q05a5Trgrl",
        "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/modelling-mortgage-retention-harry-powell\/",
        "display_url" : "linkedin.com\/pulse\/modellin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "967319898258071552",
    "text" : "This story will help you think more deeply about the experience of getting value out of data science\/ machine learning\/ big data in your organisation. https:\/\/t.co\/Q05a5Trgrl",
    "id" : 967319898258071552,
    "created_at" : "2018-02-24 08:46:50 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 967459719626547205,
  "created_at" : "2018-02-24 18:02:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967459692158095361",
  "text" : "RT @getoptimise: If you're looking to become a \"full service\" marketing agency, my white label analytics services can help you get there. B\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 235, 258 ],
        "url" : "https:\/\/t.co\/oA3OGdRr7l",
        "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/966014936827691008",
        "display_url" : "twitter.com\/getoptimise\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "967453487507402753",
    "text" : "If you're looking to become a \"full service\" marketing agency, my white label analytics services can help you get there. Bring in clients with your core competency (SEO, PPC, Design, etc.) and fill in the spaces by partnering with me. https:\/\/t.co\/oA3OGdRr7l",
    "id" : 967453487507402753,
    "created_at" : "2018-02-24 17:37:40 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 967459692158095361,
  "created_at" : "2018-02-24 18:02:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hertz",
      "screen_name" : "Hertz",
      "indices" : [ 3, 9 ],
      "id_str" : "18001417",
      "id" : 18001417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967439514582093832",
  "text" : "RT @Hertz: We have notified the NRA that we are ending the NRA\u2019s rental car discount program with Hertz.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/groundsparkapp.com\" rel=\"nofollow\"\u003EGroundSpark App\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967132184439066626",
    "text" : "We have notified the NRA that we are ending the NRA\u2019s rental car discount program with Hertz.",
    "id" : 967132184439066626,
    "created_at" : "2018-02-23 20:20:55 +0000",
    "user" : {
      "name" : "Hertz",
      "screen_name" : "Hertz",
      "protected" : false,
      "id_str" : "18001417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/954041705753276421\/XxgGo14K_normal.jpg",
      "id" : 18001417,
      "verified" : true
    }
  },
  "id" : 967439514582093832,
  "created_at" : "2018-02-24 16:42:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Delta",
      "screen_name" : "Delta",
      "indices" : [ 3, 9 ],
      "id_str" : "5920532",
      "id" : 5920532
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967439319454625793",
  "text" : "RT @Delta: Delta is reaching out to the NRA to let them know we will be ending their contract for discounted rates through our group travel\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/prod2.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr Prod2\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967391778897891328",
    "text" : "Delta is reaching out to the NRA to let them know we will be ending their contract for discounted rates through our group travel program. We will be requesting that the NRA remove our information from their website.",
    "id" : 967391778897891328,
    "created_at" : "2018-02-24 13:32:28 +0000",
    "user" : {
      "name" : "Delta",
      "screen_name" : "Delta",
      "protected" : false,
      "id_str" : "5920532",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/781541813760954368\/hTiVxR-B_normal.jpg",
      "id" : 5920532,
      "verified" : true
    }
  },
  "id" : 967439319454625793,
  "created_at" : "2018-02-24 16:41:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Symantec",
      "screen_name" : "symantec",
      "indices" : [ 3, 12 ],
      "id_str" : "17476533",
      "id" : 17476533
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967439305328250881",
  "text" : "RT @symantec: Symantec has stopped its discount program with the National Rifle Association.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.salesforce.com\" rel=\"nofollow\"\u003ESalesforce - Social Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967072793484517376",
    "text" : "Symantec has stopped its discount program with the National Rifle Association.",
    "id" : 967072793484517376,
    "created_at" : "2018-02-23 16:24:55 +0000",
    "user" : {
      "name" : "Symantec",
      "screen_name" : "symantec",
      "protected" : false,
      "id_str" : "17476533",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/859537872461975553\/xvwOCFoG_normal.jpg",
      "id" : 17476533,
      "verified" : true
    }
  },
  "id" : 967439305328250881,
  "created_at" : "2018-02-24 16:41:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/967374908908101633\/photo\/1",
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/GPCHTcZGh9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWzOWkbWsAAYiyU.jpg",
      "id_str" : "967374907108667392",
      "id" : 967374907108667392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWzOWkbWsAAYiyU.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GPCHTcZGh9"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/hzIUJtjA3X",
      "expanded_url" : "http:\/\/ift.tt\/2EOeccT",
      "display_url" : "ift.tt\/2EOeccT"
    } ]
  },
  "geo" : { },
  "id_str" : "967374908908101633",
  "text" : "The colour matches. https:\/\/t.co\/hzIUJtjA3X https:\/\/t.co\/GPCHTcZGh9",
  "id" : 967374908908101633,
  "created_at" : "2018-02-24 12:25:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/QFToyMRSP3",
      "expanded_url" : "http:\/\/www.thedailybrick.co.uk\/lego-parts\/technic\/liftarm\/lego-half-beam-with-3-rotor-blades-44374.html",
      "display_url" : "thedailybrick.co.uk\/lego-parts\/tec\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967345439325466624",
  "text" : "Anyone got these to lend my 14 year old for a week?https:\/\/t.co\/QFToyMRSP3 IT for his science projects. Thanks",
  "id" : 967345439325466624,
  "created_at" : "2018-02-24 10:28:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 5, 28 ],
      "url" : "https:\/\/t.co\/FwihAeSgzG",
      "expanded_url" : "https:\/\/twitter.com\/kelseyhightower\/status\/967216884478291968",
      "display_url" : "twitter.com\/kelseyhightowe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967314575556268033",
  "text" : "\u540E\u751F\u53EF\u5A01 https:\/\/t.co\/FwihAeSgzG",
  "id" : 967314575556268033,
  "created_at" : "2018-02-24 08:25:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/9GfE5Uy043",
      "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/966636440695328768",
      "display_url" : "twitter.com\/getoptimise\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967310958187241472",
  "text" : "Dear marketing folks, the term is actually machine learning not AI. https:\/\/t.co\/9GfE5Uy043",
  "id" : 967310958187241472,
  "created_at" : "2018-02-24 08:11:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/8l4z8XswLN",
      "expanded_url" : "https:\/\/mopinion.com\/mobile-app-feedback-surveys-sdk\/",
      "display_url" : "mopinion.com\/mobile-app-fee\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967309791516413953",
  "text" : "Looking for ways to collect feedback in your native app(s)? SDKs might be the answer https:\/\/t.co\/8l4z8XswLN",
  "id" : 967309791516413953,
  "created_at" : "2018-02-24 08:06:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "indices" : [ 3, 16 ],
      "id_str" : "19698834",
      "id" : 19698834
    }, {
      "name" : "SingaporePoliceForce",
      "screen_name" : "SingaporePolice",
      "indices" : [ 20, 36 ],
      "id_str" : "38140836",
      "id" : 38140836
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "social",
      "indices" : [ 44, 51 ]
    }, {
      "text" : "media",
      "indices" : [ 52, 58 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/HxU16vP8Wn",
      "expanded_url" : "http:\/\/ow.ly\/GDBi30iACDh",
      "display_url" : "ow.ly\/GDBi30iACDh"
    } ]
  },
  "geo" : { },
  "id_str" : "967304962324746241",
  "text" : "RT @MarketingEds: . @SingaporePolice steals #social #media hearts with punny posts  https:\/\/t.co\/HxU16vP8Wn",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SingaporePoliceForce",
        "screen_name" : "SingaporePolice",
        "indices" : [ 2, 18 ],
        "id_str" : "38140836",
        "id" : 38140836
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "social",
        "indices" : [ 26, 33 ]
      }, {
        "text" : "media",
        "indices" : [ 34, 40 ]
      } ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/HxU16vP8Wn",
        "expanded_url" : "http:\/\/ow.ly\/GDBi30iACDh",
        "display_url" : "ow.ly\/GDBi30iACDh"
      } ]
    },
    "geo" : { },
    "id_str" : "967262878117003264",
    "text" : ". @SingaporePolice steals #social #media hearts with punny posts  https:\/\/t.co\/HxU16vP8Wn",
    "id" : 967262878117003264,
    "created_at" : "2018-02-24 05:00:15 +0000",
    "user" : {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "protected" : false,
      "id_str" : "19698834",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948003366164033536\/_ojmapns_normal.jpg",
      "id" : 19698834,
      "verified" : false
    }
  },
  "id" : 967304962324746241,
  "created_at" : "2018-02-24 07:47:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "indices" : [ 3, 13 ],
      "id_str" : "123167035",
      "id" : 123167035
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967298742545190913",
  "text" : "RT @GovTechSG: Do you, or your company, have a knack for solving problems? We are looking for companies to co-create innovative digital sol\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialbakers.com\/\" rel=\"nofollow\"\u003ESocialbakers\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GovTechSG\/status\/966919306511507456\/photo\/1",
        "indices" : [ 283, 306 ],
        "url" : "https:\/\/t.co\/qTI4f3MhSE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWsv-_HVAAIhO7F.jpg",
        "id_str" : "966919304141668354",
        "id" : 966919304141668354,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWsv-_HVAAIhO7F.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 409,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 409,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 409,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 409,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qTI4f3MhSE"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 259, 282 ],
        "url" : "https:\/\/t.co\/Nu0P0YKtJp",
        "expanded_url" : "http:\/\/bit.ly\/2EJ0qIm",
        "display_url" : "bit.ly\/2EJ0qIm"
      } ]
    },
    "geo" : { },
    "id_str" : "966919306511507456",
    "text" : "Do you, or your company, have a knack for solving problems? We are looking for companies to co-create innovative digital solutions to address the needs of public sector &amp; help transform public service delivery. Details of these problem statements are at: https:\/\/t.co\/Nu0P0YKtJp https:\/\/t.co\/qTI4f3MhSE",
    "id" : 966919306511507456,
    "created_at" : "2018-02-23 06:15:01 +0000",
    "user" : {
      "name" : "GovTech (Singapore)",
      "screen_name" : "GovTechSG",
      "protected" : false,
      "id_str" : "123167035",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/784240735277060098\/lhMmgjx6_normal.jpg",
      "id" : 123167035,
      "verified" : true
    }
  },
  "id" : 967298742545190913,
  "created_at" : "2018-02-24 07:22:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BusinessDaily Africa",
      "screen_name" : "BD_Africa",
      "indices" : [ 62, 72 ],
      "id_str" : "288311590",
      "id" : 288311590
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/X8PNqUeddL",
      "expanded_url" : "https:\/\/www.businessdailyafrica.com\/corporate\/tech\/Google-enables-M-Pesa-payments-on-its-app-store\/4258474-4315886-4s85ya\/index.html",
      "display_url" : "businessdailyafrica.com\/corporate\/tech\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967111568356933637",
  "text" : "M-Pesa now enabled on Google Play https:\/\/t.co\/X8PNqUeddL via @BD_Africa",
  "id" : 967111568356933637,
  "created_at" : "2018-02-23 18:59:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/967037534923640833\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/My9jFt1V88",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWubgurXkAAJ2QJ.jpg",
      "id_str" : "967037531589152768",
      "id" : 967037531589152768,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWubgurXkAAJ2QJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 564,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 564,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 564,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 564,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/My9jFt1V88"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/vxHcuWjS2t",
      "expanded_url" : "http:\/\/econ.st\/2HFtYEm",
      "display_url" : "econ.st\/2HFtYEm"
    } ]
  },
  "geo" : { },
  "id_str" : "967110807040471040",
  "text" : "RT @TheEconomist: How does your country stack up? https:\/\/t.co\/vxHcuWjS2t https:\/\/t.co\/My9jFt1V88",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheEconomist\/status\/967037534923640833\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/My9jFt1V88",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWubgurXkAAJ2QJ.jpg",
        "id_str" : "967037531589152768",
        "id" : 967037531589152768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWubgurXkAAJ2QJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 564,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/My9jFt1V88"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 32, 55 ],
        "url" : "https:\/\/t.co\/vxHcuWjS2t",
        "expanded_url" : "http:\/\/econ.st\/2HFtYEm",
        "display_url" : "econ.st\/2HFtYEm"
      } ]
    },
    "geo" : { },
    "id_str" : "967037534923640833",
    "text" : "How does your country stack up? https:\/\/t.co\/vxHcuWjS2t https:\/\/t.co\/My9jFt1V88",
    "id" : 967037534923640833,
    "created_at" : "2018-02-23 14:04:49 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 967110807040471040,
  "created_at" : "2018-02-23 18:55:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft",
      "screen_name" : "Microsoft",
      "indices" : [ 3, 13 ],
      "id_str" : "74286565",
      "id" : 74286565
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967068291607953409",
  "text" : "RT @Microsoft: Microsoft for Startups is a new program that connects startups with resources to propel their growth. Learn more: https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/prod2.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr Prod2\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/KIrovNw78J",
        "expanded_url" : "http:\/\/msft.social\/zuGNa3",
        "display_url" : "msft.social\/zuGNa3"
      }, {
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/4Fy3xjqgYJ",
        "expanded_url" : "https:\/\/cards.twitter.com\/cards\/18ce54m087w\/5cb1v",
        "display_url" : "cards.twitter.com\/cards\/18ce54m0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "963806037018103809",
    "text" : "Microsoft for Startups is a new program that connects startups with resources to propel their growth. Learn more: https:\/\/t.co\/KIrovNw78J https:\/\/t.co\/4Fy3xjqgYJ",
    "id" : 963806037018103809,
    "created_at" : "2018-02-14 16:04:00 +0000",
    "user" : {
      "name" : "Microsoft",
      "screen_name" : "Microsoft",
      "protected" : false,
      "id_str" : "74286565",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875416480547917824\/R6wl9gWl_normal.jpg",
      "id" : 74286565,
      "verified" : true
    }
  },
  "id" : 967068291607953409,
  "created_at" : "2018-02-23 16:07:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 257, 280 ],
      "url" : "https:\/\/t.co\/zEkZHW8MhZ",
      "expanded_url" : "https:\/\/ssir.org\/articles\/entry\/using_data_for_action_and_for_impact",
      "display_url" : "ssir.org\/articles\/entry\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967062477463805952",
  "text" : "There is a growing urgency in the social sector to make better use of data to inform decision-making and evaluate performance, but many organizations struggle to do this. This article provides a framework to help nonprofits and social businesses do better. https:\/\/t.co\/zEkZHW8MhZ",
  "id" : 967062477463805952,
  "created_at" : "2018-02-23 15:43:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/967054923602055168\/photo\/1",
      "indices" : [ 126, 149 ],
      "url" : "https:\/\/t.co\/DzNjkAsFCp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWurUjCX4AAaZnV.jpg",
      "id_str" : "967054914492030976",
      "id" : 967054914492030976,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWurUjCX4AAaZnV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DzNjkAsFCp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "967054923602055168",
  "text" : "Working with Shell Terminal has a grip over me. I about to type \"exit\" in WhatApps on Windows PC to exit the applications. \uD83D\uDE02\uD83D\uDE02 https:\/\/t.co\/DzNjkAsFCp",
  "id" : 967054923602055168,
  "created_at" : "2018-02-23 15:13:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "socialentrepreneursireland",
      "indices" : [ 45, 72 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/YAzvV1pjSW",
      "expanded_url" : "https:\/\/twitter.com\/SEIreland\/status\/963729408984080384",
      "display_url" : "twitter.com\/SEIreland\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "967053940218060800",
  "text" : "IF anyone is seeking a partner, let connect. #socialentrepreneursireland https:\/\/t.co\/YAzvV1pjSW",
  "id" : 967053940218060800,
  "created_at" : "2018-02-23 15:10:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966996506262810624\/photo\/1",
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/xKMChl9bPZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWt2KgfW4AEcU5P.jpg",
      "id_str" : "966996467893329921",
      "id" : 966996467893329921,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWt2KgfW4AEcU5P.jpg",
      "sizes" : [ {
        "h" : 509,
        "resize" : "fit",
        "w" : 511
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 509,
        "resize" : "fit",
        "w" : 511
      }, {
        "h" : 509,
        "resize" : "fit",
        "w" : 511
      }, {
        "h" : 509,
        "resize" : "fit",
        "w" : 511
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xKMChl9bPZ"
    } ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 101, 113 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966996506262810624",
  "text" : "I have a \u20AC120 ad credit to give away. Claim it today to promote your website. First to DM me. Thanks #getoptimise https:\/\/t.co\/xKMChl9bPZ",
  "id" : 966996506262810624,
  "created_at" : "2018-02-23 11:21:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christopher Titus",
      "screen_name" : "TitusNation",
      "indices" : [ 3, 15 ],
      "id_str" : "40996128",
      "id" : 40996128
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966989044533153792",
  "text" : "RT @TitusNation: Peter Wang, the JROTC member who was shot multiple times while holding the door so others could escape, was awarded the JR\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TitusNation\/status\/966026954255183872\/photo\/1",
        "indices" : [ 281, 304 ],
        "url" : "https:\/\/t.co\/Ay6tf5DwU7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWgEY-lVAAAioAR.jpg",
        "id_str" : "966026947233906688",
        "id" : 966026947233906688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWgEY-lVAAAioAR.jpg",
        "sizes" : [ {
          "h" : 919,
          "resize" : "fit",
          "w" : 634
        }, {
          "h" : 919,
          "resize" : "fit",
          "w" : 634
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 919,
          "resize" : "fit",
          "w" : 634
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 469
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Ay6tf5DwU7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966026954255183872",
    "text" : "Peter Wang, the JROTC member who was shot multiple times while holding the door so others could escape, was awarded the JROTC Heroism medal at his funeral today. He was also posthumously admitted to West Point.  Which was his dream. He was 15. This is Peter and his Mom. \nNo words https:\/\/t.co\/Ay6tf5DwU7",
    "id" : 966026954255183872,
    "created_at" : "2018-02-20 19:09:08 +0000",
    "user" : {
      "name" : "Christopher Titus",
      "screen_name" : "TitusNation",
      "protected" : false,
      "id_str" : "40996128",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/965002284626100224\/vjdCBc9e_normal.jpg",
      "id" : 40996128,
      "verified" : true
    }
  },
  "id" : 966989044533153792,
  "created_at" : "2018-02-23 10:52:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 128, 151 ],
      "url" : "https:\/\/t.co\/X4lSJPGGwM",
      "expanded_url" : "https:\/\/www.facebook.com\/photo.php?fbid=10155830268025266&set=a.156383930265.151459.501550265&type=3&theater",
      "display_url" : "facebook.com\/photo.php?fbid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "966974486443642880",
  "text" : "These 2 met under such unlikely circumstances and would later become two of the world's most successful internet entrepreneurs. https:\/\/t.co\/X4lSJPGGwM",
  "id" : 966974486443642880,
  "created_at" : "2018-02-23 09:54:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 0, 11 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "966971507564994560",
  "geo" : { },
  "id_str" : "966972123985084418",
  "in_reply_to_user_id" : 4747144932,
  "text" : "@CaroBowler Same over here. Appeared on my timeline. I got to google it. There always an addreviation for every minority group...",
  "id" : 966972123985084418,
  "in_reply_to_status_id" : 966971507564994560,
  "created_at" : "2018-02-23 09:44:54 +0000",
  "in_reply_to_screen_name" : "CaroBowler",
  "in_reply_to_user_id_str" : "4747144932",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966970760769277952\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/3t06Yu3AXe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWtexcUWsAAjsat.jpg",
      "id_str" : "966970748509270016",
      "id" : 966970748509270016,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWtexcUWsAAjsat.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3t06Yu3AXe"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966970760769277952",
  "text" : "I just learn about this abbreviation https:\/\/t.co\/3t06Yu3AXe",
  "id" : 966970760769277952,
  "created_at" : "2018-02-23 09:39:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dean Vipond",
      "screen_name" : "DeanVipond",
      "indices" : [ 3, 14 ],
      "id_str" : "25058207",
      "id" : 25058207
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966968993461481472",
  "text" : "RT @DeanVipond: OK, so if you\u2019re a student from any group under-represented in tech (Women, BAME, LGBTQ), and would like to go to the ace @\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Up Front Conf",
        "screen_name" : "upfrontconf",
        "indices" : [ 122, 134 ],
        "id_str" : "2786234275",
        "id" : 2786234275
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966958105807507456",
    "text" : "OK, so if you\u2019re a student from any group under-represented in tech (Women, BAME, LGBTQ), and would like to go to the ace @upfrontconf in Manc, I can buy tix for the first 2 folk to reply to me.",
    "id" : 966958105807507456,
    "created_at" : "2018-02-23 08:49:12 +0000",
    "user" : {
      "name" : "Dean Vipond",
      "screen_name" : "DeanVipond",
      "protected" : false,
      "id_str" : "25058207",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/104018764\/dvbid_twitpic_normal.gif",
      "id" : 25058207,
      "verified" : false
    }
  },
  "id" : 966968993461481472,
  "created_at" : "2018-02-23 09:32:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 3, 18 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Drimnagh",
      "indices" : [ 57, 66 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/13AswtdNRX",
      "expanded_url" : "https:\/\/twitter.com\/acloudonastring\/status\/966437664579837952",
      "display_url" : "twitter.com\/acloudonastrin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "966968871193399296",
  "text" : "RT @paulinesargent: Gorgous pics of our lovely castle in #Drimnagh :-) https:\/\/t.co\/13AswtdNRX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Drimnagh",
        "indices" : [ 37, 46 ]
      } ],
      "urls" : [ {
        "indices" : [ 51, 74 ],
        "url" : "https:\/\/t.co\/13AswtdNRX",
        "expanded_url" : "https:\/\/twitter.com\/acloudonastring\/status\/966437664579837952",
        "display_url" : "twitter.com\/acloudonastrin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966968718222819328",
    "text" : "Gorgous pics of our lovely castle in #Drimnagh :-) https:\/\/t.co\/13AswtdNRX",
    "id" : 966968718222819328,
    "created_at" : "2018-02-23 09:31:22 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "protected" : false,
      "id_str" : "22997944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002264625130409984\/wlpdomJK_normal.jpg",
      "id" : 22997944,
      "verified" : false
    }
  },
  "id" : 966968871193399296,
  "created_at" : "2018-02-23 09:31:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966968731653099520\/photo\/1",
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/VUEM570xij",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWtc5HkW4AAvRsd.jpg",
      "id_str" : "966968681354944512",
      "id" : 966968681354944512,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWtc5HkW4AAvRsd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VUEM570xij"
    } ],
    "hashtags" : [ {
      "text" : "BetterDublin",
      "indices" : [ 82, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966968731653099520",
  "text" : "Good to see there a plan for direct rail service from city centre to the airport. #BetterDublin https:\/\/t.co\/VUEM570xij",
  "id" : 966968731653099520,
  "created_at" : "2018-02-23 09:31:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "indices" : [ 3, 9 ],
      "id_str" : "1344951",
      "id" : 1344951
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966836195002077184",
  "text" : "RT @WIRED: A photojournalist named Park Jongwoo was granted rare access to the two-mile-wide swath of land between North and South Korea\u2014an\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WIRED\/status\/966826487499378688\/photo\/1",
        "indices" : [ 244, 267 ],
        "url" : "https:\/\/t.co\/HCWxowLZQQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWrbkMOVAAEH8Es.jpg",
        "id_str" : "966826484827488257",
        "id" : 966826484827488257,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWrbkMOVAAEH8Es.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1133,
          "resize" : "fit",
          "w" : 1700
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1133,
          "resize" : "fit",
          "w" : 1700
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HCWxowLZQQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 220, 243 ],
        "url" : "https:\/\/t.co\/uF0bklfbhS",
        "expanded_url" : "http:\/\/bit.ly\/2EO9W9e",
        "display_url" : "bit.ly\/2EO9W9e"
      } ]
    },
    "geo" : { },
    "id_str" : "966826487499378688",
    "text" : "A photojournalist named Park Jongwoo was granted rare access to the two-mile-wide swath of land between North and South Korea\u2014an area called the Korean Demilitarized Zone. See more stunning shots of the Korean DMZ here: https:\/\/t.co\/uF0bklfbhS https:\/\/t.co\/HCWxowLZQQ",
    "id" : 966826487499378688,
    "created_at" : "2018-02-23 00:06:12 +0000",
    "user" : {
      "name" : "WIRED",
      "screen_name" : "WIRED",
      "protected" : false,
      "id_str" : "1344951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/615598832726970372\/jsK-gBSt_normal.png",
      "id" : 1344951,
      "verified" : true
    }
  },
  "id" : 966836195002077184,
  "created_at" : "2018-02-23 00:44:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966825829710909440\/photo\/1",
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/Tj2KAceG9k",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWra99VX4AAiBNp.jpg",
      "id_str" : "966825827995475968",
      "id" : 966825827995475968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWra99VX4AAiBNp.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Tj2KAceG9k"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/YXwKkz8XJe",
      "expanded_url" : "http:\/\/ift.tt\/2EKYivB",
      "display_url" : "ift.tt\/2EKYivB"
    } ]
  },
  "geo" : { },
  "id_str" : "966825829710909440",
  "text" : "At the Manchester Art Gallery few days ago https:\/\/t.co\/YXwKkz8XJe https:\/\/t.co\/Tj2KAceG9k",
  "id" : 966825829710909440,
  "created_at" : "2018-02-23 00:03:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "indices" : [ 3, 11 ],
      "id_str" : "13311832",
      "id" : 13311832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/33uXwhg30M",
      "expanded_url" : "http:\/\/www.chicagotribune.com\/news\/chicagoinc\/ct-met-rauner-chocolate-milk-0223-chicago-inc-20180222-story.html",
      "display_url" : "chicagotribune.com\/news\/chicagoin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "966768929870962690",
  "text" : "RT @dr_pete: Meanwhile, here in Illinois... https:\/\/t.co\/33uXwhg30M",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 31, 54 ],
        "url" : "https:\/\/t.co\/33uXwhg30M",
        "expanded_url" : "http:\/\/www.chicagotribune.com\/news\/chicagoinc\/ct-met-rauner-chocolate-milk-0223-chicago-inc-20180222-story.html",
        "display_url" : "chicagotribune.com\/news\/chicagoin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966767315634319362",
    "text" : "Meanwhile, here in Illinois... https:\/\/t.co\/33uXwhg30M",
    "id" : 966767315634319362,
    "created_at" : "2018-02-22 20:11:04 +0000",
    "user" : {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "protected" : false,
      "id_str" : "13311832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/542152616487038976\/-UUMowce_normal.jpeg",
      "id" : 13311832,
      "verified" : false
    }
  },
  "id" : 966768929870962690,
  "created_at" : "2018-02-22 20:17:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC Future",
      "screen_name" : "BBC_Future",
      "indices" : [ 80, 91 ],
      "id_str" : "410605574",
      "id" : 410605574
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/7JcPe9rJOS",
      "expanded_url" : "http:\/\/www.bbc.com\/future\/story\/20180220-the-nation-that-thrived-by-nudging-its-population?ocid=ww.social.link.twitter",
      "display_url" : "bbc.com\/future\/story\/2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "966760578239868934",
  "text" : "The nation that thrived by \u2018nudging\u2019 its population https:\/\/t.co\/7JcPe9rJOS via @BBC_Future",
  "id" : 966760578239868934,
  "created_at" : "2018-02-22 19:44:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DubEntWeek",
      "indices" : [ 76, 87 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966616485690073089",
  "text" : "In your field of work, what is the one job you try desperately avoid doing? #DubEntWeek",
  "id" : 966616485690073089,
  "created_at" : "2018-02-22 10:11:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966615855214809088",
  "text" : "RT @getoptimise: Product Hunt are always on the lookout for amazing new product to feature. Not all products are eligible. Check if your pr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 165, 188 ],
        "url" : "https:\/\/t.co\/SjfTuhxn4i",
        "expanded_url" : "https:\/\/help.producthunt.com\/#what-kind-of-products-do-you-allow-on-product-hunt",
        "display_url" : "help.producthunt.com\/#what-kind-of-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "958652102208819200",
    "text" : "Product Hunt are always on the lookout for amazing new product to feature. Not all products are eligible. Check if your product is eligible on Product Hunt\u2019s FAQs.  https:\/\/t.co\/SjfTuhxn4i",
    "id" : 958652102208819200,
    "created_at" : "2018-01-31 10:44:06 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 966615855214809088,
  "created_at" : "2018-02-22 10:09:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966615546329489408",
  "text" : "RT @getoptimise: Google Analytics Spreadsheet add-on makes it easier for Google Analytics users to access, visualize, share, and manipulate\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 157, 180 ],
        "url" : "https:\/\/t.co\/hxkH9DqqkS",
        "expanded_url" : "https:\/\/chrome.google.com\/webstore\/detail\/google-analytics\/fefimfimnhjjkomigakinmjileehfopp",
        "display_url" : "chrome.google.com\/webstore\/detai\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "962389532862709760",
    "text" : "Google Analytics Spreadsheet add-on makes it easier for Google Analytics users to access, visualize, share, and manipulate their data in Google Spreadsheets https:\/\/t.co\/hxkH9DqqkS",
    "id" : 962389532862709760,
    "created_at" : "2018-02-10 18:15:19 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 966615546329489408,
  "created_at" : "2018-02-22 10:07:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/KLcjejxKBh",
      "expanded_url" : "https:\/\/asciinema.org\/",
      "display_url" : "asciinema.org"
    } ]
  },
  "geo" : { },
  "id_str" : "966608551446810624",
  "text" : "Record, replay and share your terminal sessions https:\/\/t.co\/KLcjejxKBh",
  "id" : 966608551446810624,
  "created_at" : "2018-02-22 09:40:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "A Data Guy",
      "screen_name" : "PanelDataGuru",
      "indices" : [ 3, 17 ],
      "id_str" : "803663808153743360",
      "id" : 803663808153743360
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966597826842120192",
  "text" : "RT @PanelDataGuru: Marketing analytics is receiving great attention because of radical technology changes in the marketing environment. Thi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "bigdata",
        "indices" : [ 211, 219 ]
      }, {
        "text" : "ML",
        "indices" : [ 242, 245 ]
      } ],
      "urls" : [ {
        "indices" : [ 248, 271 ],
        "url" : "https:\/\/t.co\/joUJ8Po2VR",
        "expanded_url" : "http:\/\/www.tandfonline.com\/doi\/abs\/10.1080\/10528008.2017.1421049",
        "display_url" : "tandfonline.com\/doi\/abs\/10.108\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966538641379078146",
    "text" : "Marketing analytics is receiving great attention because of radical technology changes in the marketing environment. This  study aims to assist the design and implementation of a marketing analytics course with #bigdata and machine learning (#ML): https:\/\/t.co\/joUJ8Po2VR",
    "id" : 966538641379078146,
    "created_at" : "2018-02-22 05:02:24 +0000",
    "user" : {
      "name" : "A Data Guy",
      "screen_name" : "PanelDataGuru",
      "protected" : false,
      "id_str" : "803663808153743360",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030203409431842816\/QT5UoF8w_normal.jpg",
      "id" : 803663808153743360,
      "verified" : false
    }
  },
  "id" : 966597826842120192,
  "created_at" : "2018-02-22 08:57:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GDPR",
      "indices" : [ 48, 53 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966596932247474176",
  "text" : "Like to hear from SME how they prepare for this #GDPR",
  "id" : 966596932247474176,
  "created_at" : "2018-02-22 08:54:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966423508766085120\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/EwZn70hqJ8",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWltDycXcAA_SN4.jpg",
      "id_str" : "966423506895466496",
      "id" : 966423506895466496,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWltDycXcAA_SN4.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EwZn70hqJ8"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/HMGrwQvGZf",
      "expanded_url" : "http:\/\/ift.tt\/2EHW4Bb",
      "display_url" : "ift.tt\/2EHW4Bb"
    } ]
  },
  "geo" : { },
  "id_str" : "966423508766085120",
  "text" : "Chinese New Year fingers food. https:\/\/t.co\/HMGrwQvGZf https:\/\/t.co\/EwZn70hqJ8",
  "id" : 966423508766085120,
  "created_at" : "2018-02-21 21:24:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/YcoyLucZxy",
      "expanded_url" : "https:\/\/twitter.com\/levymarket\/status\/964107035481640961",
      "display_url" : "twitter.com\/levymarket\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "966418339244331008",
  "text" : "RT @jackiepenangkit: Look forward to the April event for my first opportunity.  https:\/\/t.co\/YcoyLucZxy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/YcoyLucZxy",
        "expanded_url" : "https:\/\/twitter.com\/levymarket\/status\/964107035481640961",
        "display_url" : "twitter.com\/levymarket\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966417850838630400",
    "text" : "Look forward to the April event for my first opportunity.  https:\/\/t.co\/YcoyLucZxy",
    "id" : 966417850838630400,
    "created_at" : "2018-02-21 21:02:25 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 966418339244331008,
  "created_at" : "2018-02-21 21:04:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Shaw",
      "screen_name" : "tomwillfixit",
      "indices" : [ 3, 16 ],
      "id_str" : "514735531",
      "id" : 514735531
    }, {
      "name" : "Docker Dublin",
      "screen_name" : "DockerDublin",
      "indices" : [ 80, 93 ],
      "id_str" : "2378777234",
      "id" : 2378777234
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/tomwillfixit\/status\/966406742274985985\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/q3zyYyeSEQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWldoh4XUAEval5.jpg",
      "id_str" : "966406545918611457",
      "id" : 966406545918611457,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWldoh4XUAEval5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/q3zyYyeSEQ"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/tomwillfixit\/status\/966406742274985985\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/q3zyYyeSEQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWldsm4W0AAWaRK.jpg",
      "id_str" : "966406615980232704",
      "id" : 966406615980232704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWldsm4W0AAWaRK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/q3zyYyeSEQ"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/tomwillfixit\/status\/966406742274985985\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/q3zyYyeSEQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWldw0nWkAIL64i.jpg",
      "id_str" : "966406688386486274",
      "id" : 966406688386486274,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWldw0nWkAIL64i.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/q3zyYyeSEQ"
    } ],
    "hashtags" : [ {
      "text" : "Docker",
      "indices" : [ 94, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966412301262839809",
  "text" : "RT @tomwillfixit: \"Docker gives Data scientists fast reproducible environments\" @DockerDublin #Docker https:\/\/t.co\/q3zyYyeSEQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Docker Dublin",
        "screen_name" : "DockerDublin",
        "indices" : [ 62, 75 ],
        "id_str" : "2378777234",
        "id" : 2378777234
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tomwillfixit\/status\/966406742274985985\/photo\/1",
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/q3zyYyeSEQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWldoh4XUAEval5.jpg",
        "id_str" : "966406545918611457",
        "id" : 966406545918611457,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWldoh4XUAEval5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/q3zyYyeSEQ"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/tomwillfixit\/status\/966406742274985985\/photo\/1",
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/q3zyYyeSEQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWldsm4W0AAWaRK.jpg",
        "id_str" : "966406615980232704",
        "id" : 966406615980232704,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWldsm4W0AAWaRK.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/q3zyYyeSEQ"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/tomwillfixit\/status\/966406742274985985\/photo\/1",
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/q3zyYyeSEQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWldw0nWkAIL64i.jpg",
        "id_str" : "966406688386486274",
        "id" : 966406688386486274,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWldw0nWkAIL64i.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/q3zyYyeSEQ"
      } ],
      "hashtags" : [ {
        "text" : "Docker",
        "indices" : [ 76, 83 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966406742274985985",
    "text" : "\"Docker gives Data scientists fast reproducible environments\" @DockerDublin #Docker https:\/\/t.co\/q3zyYyeSEQ",
    "id" : 966406742274985985,
    "created_at" : "2018-02-21 20:18:16 +0000",
    "user" : {
      "name" : "Tom Shaw",
      "screen_name" : "tomwillfixit",
      "protected" : false,
      "id_str" : "514735531",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/902439603013079040\/NG_OdOwh_normal.jpg",
      "id" : 514735531,
      "verified" : false
    }
  },
  "id" : 966412301262839809,
  "created_at" : "2018-02-21 20:40:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "966403725332971520",
  "geo" : { },
  "id_str" : "966406233212235776",
  "in_reply_to_user_id" : 9465632,
  "text" : "Working with someone not in the Tech industry so tool like Slack is out of the question but project stakeholders has WhatApps on their phone.",
  "id" : 966406233212235776,
  "in_reply_to_status_id" : 966403725332971520,
  "created_at" : "2018-02-21 20:16:15 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966403725332971520",
  "text" : "I like WhatApps integration with Opera and this allows me to work on a laptop.",
  "id" : 966403725332971520,
  "created_at" : "2018-02-21 20:06:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966362693555847168\/photo\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/jLnaPkg2eK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWk1v4GX4AA7Ffa.jpg",
      "id_str" : "966362691676921856",
      "id" : 966362691676921856,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWk1v4GX4AA7Ffa.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/jLnaPkg2eK"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/ZgO7tHYXbY",
      "expanded_url" : "http:\/\/ift.tt\/2BGSIf2",
      "display_url" : "ift.tt\/2BGSIf2"
    } ]
  },
  "geo" : { },
  "id_str" : "966362693555847168",
  "text" : "Lovely weather today. https:\/\/t.co\/ZgO7tHYXbY https:\/\/t.co\/jLnaPkg2eK",
  "id" : 966362693555847168,
  "created_at" : "2018-02-21 17:23:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966349391052320773\/photo\/1",
      "indices" : [ 17, 40 ],
      "url" : "https:\/\/t.co\/bfteJCvlpa",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWkpfgBWAAUhM5F.jpg",
      "id_str" : "966349216195936261",
      "id" : 966349216195936261,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWkpfgBWAAUhM5F.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bfteJCvlpa"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966349391052320773",
  "text" : "In other news... https:\/\/t.co\/bfteJCvlpa",
  "id" : 966349391052320773,
  "created_at" : "2018-02-21 16:30:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RaspberryPi",
      "indices" : [ 49, 61 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966345554501537793",
  "text" : "RT @randal_olson: When the person who stocks the #RaspberryPi magazines at the local supermarket doesn't know what a Raspberry Pi is. #prog\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/966341810279731200\/photo\/1",
        "indices" : [ 154, 177 ],
        "url" : "https:\/\/t.co\/z3OT6olEFG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWkisCLVoAILLHi.jpg",
        "id_str" : "966341734941696002",
        "id" : 966341734941696002,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWkisCLVoAILLHi.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 4032,
          "resize" : "fit",
          "w" : 3024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/z3OT6olEFG"
      } ],
      "hashtags" : [ {
        "text" : "RaspberryPi",
        "indices" : [ 31, 43 ]
      }, {
        "text" : "programming",
        "indices" : [ 116, 128 ]
      } ],
      "urls" : [ {
        "indices" : [ 130, 153 ],
        "url" : "https:\/\/t.co\/JFNSs2006s",
        "expanded_url" : "https:\/\/www.reddit.com\/r\/ProgrammerHumor\/comments\/7z2715\/my_local_supermarket_stocks_the_raspberry_pi\/",
        "display_url" : "reddit.com\/r\/ProgrammerHu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966341810279731200",
    "text" : "When the person who stocks the #RaspberryPi magazines at the local supermarket doesn't know what a Raspberry Pi is. #programming\n\nhttps:\/\/t.co\/JFNSs2006s https:\/\/t.co\/z3OT6olEFG",
    "id" : 966341810279731200,
    "created_at" : "2018-02-21 16:00:15 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 966345554501537793,
  "created_at" : "2018-02-21 16:15:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "World Index",
      "screen_name" : "theworldindex",
      "indices" : [ 3, 17 ],
      "id_str" : "938419883745869824",
      "id" : 938419883745869824
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966326114762272768",
  "text" : "RT @theworldindex: World's most expensive cities to live\n\n1.\uD83C\uDDEC\uD83C\uDDE7 London\n2.\uD83C\uDDEB\uD83C\uDDF7 Paris\n3.\uD83C\uDDE6\uD83C\uDDEA Dubai\n4.\uD83C\uDDEE\uD83C\uDDEA Dublin\n5.\uD83C\uDDED\uD83C\uDDF0 Hong Kong\n6.\uD83C\uDDEE\uD83C\uDDF9 Milan\n7.\uD83C\uDDE6\uD83C\uDDFA Melb\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/theworldindex\/status\/964761183722057728\/photo\/1",
        "indices" : [ 249, 272 ],
        "url" : "https:\/\/t.co\/En1PAwSuaO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWOE2ziWsAAkwwP.jpg",
        "id_str" : "964760822269521920",
        "id" : 964760822269521920,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWOE2ziWsAAkwwP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 536,
          "resize" : "fit",
          "w" : 858
        }, {
          "h" : 536,
          "resize" : "fit",
          "w" : 858
        }, {
          "h" : 536,
          "resize" : "fit",
          "w" : 858
        }, {
          "h" : 425,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/En1PAwSuaO"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964761183722057728",
    "text" : "World's most expensive cities to live\n\n1.\uD83C\uDDEC\uD83C\uDDE7 London\n2.\uD83C\uDDEB\uD83C\uDDF7 Paris\n3.\uD83C\uDDE6\uD83C\uDDEA Dubai\n4.\uD83C\uDDEE\uD83C\uDDEA Dublin\n5.\uD83C\uDDED\uD83C\uDDF0 Hong Kong\n6.\uD83C\uDDEE\uD83C\uDDF9 Milan\n7.\uD83C\uDDE6\uD83C\uDDFA Melbourne\n8.\uD83C\uDDE6\uD83C\uDDFA Sydney\n9.\uD83C\uDDEA\uD83C\uDDF8 Barcelona\n10.\uD83C\uDDFA\uD83C\uDDF8 NY City\n11.\uD83C\uDDEA\uD83C\uDDF8 Madrid\n12.\uD83C\uDDE6\uD83C\uDDEA Abu Dhabi\n13.\uD83C\uDDEE\uD83C\uDDF9 Rome\n14.\uD83C\uDDF8\uD83C\uDDEC Singapore\n15.\uD83C\uDDF7\uD83C\uDDFA Moscow\n\n(Lovemoney) https:\/\/t.co\/En1PAwSuaO",
    "id" : 964761183722057728,
    "created_at" : "2018-02-17 07:19:25 +0000",
    "user" : {
      "name" : "World Index",
      "screen_name" : "theworldindex",
      "protected" : false,
      "id_str" : "938419883745869824",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966631777220603904\/bSPrJFRy_normal.jpg",
      "id" : 938419883745869824,
      "verified" : false
    }
  },
  "id" : 966326114762272768,
  "created_at" : "2018-02-21 14:57:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966325809920249857\/photo\/1",
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/NrsFVrq6B0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWkUJneX4AAEE7o.jpg",
      "id_str" : "966325750495436800",
      "id" : 966325750495436800,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWkUJneX4AAEE7o.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NrsFVrq6B0"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966325809920249857\/photo\/1",
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/NrsFVrq6B0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWkUJniWkAAlg6u.jpg",
      "id_str" : "966325750512128000",
      "id" : 966325750512128000,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWkUJniWkAAlg6u.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NrsFVrq6B0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966325809920249857",
  "text" : "Road sealed off. Traffic diverted. I am speculating but keeping it to myself. https:\/\/t.co\/NrsFVrq6B0",
  "id" : 966325809920249857,
  "created_at" : "2018-02-21 14:56:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex  Papageorgiou",
      "screen_name" : "alpapag",
      "indices" : [ 3, 11 ],
      "id_str" : "23337615",
      "id" : 23337615
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "googleanalyticsR",
      "indices" : [ 79, 96 ]
    }, {
      "text" : "google2pandas",
      "indices" : [ 97, 111 ]
    }, {
      "text" : "rstats",
      "indices" : [ 112, 119 ]
    }, {
      "text" : "GoogleAnalytics",
      "indices" : [ 120, 136 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966284901149560832",
  "text" : "RT @alpapag: New post: 5 Steps to Get Google Analytics Ready for Data Science \n#googleanalyticsR #google2pandas #rstats #GoogleAnalytics ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "googleanalyticsR",
        "indices" : [ 66, 83 ]
      }, {
        "text" : "google2pandas",
        "indices" : [ 84, 98 ]
      }, {
        "text" : "rstats",
        "indices" : [ 99, 106 ]
      }, {
        "text" : "GoogleAnalytics",
        "indices" : [ 107, 123 ]
      } ],
      "urls" : [ {
        "indices" : [ 124, 147 ],
        "url" : "https:\/\/t.co\/P37Eqq0l1G",
        "expanded_url" : "http:\/\/www.linkedin.com\/pulse\/5-steps-get-google-analytics-ready-data-science-papageorgiou\/",
        "display_url" : "linkedin.com\/pulse\/5-steps-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "966015157678891008",
    "text" : "New post: 5 Steps to Get Google Analytics Ready for Data Science \n#googleanalyticsR #google2pandas #rstats #GoogleAnalytics https:\/\/t.co\/P37Eqq0l1G",
    "id" : 966015157678891008,
    "created_at" : "2018-02-20 18:22:15 +0000",
    "user" : {
      "name" : "Alex  Papageorgiou",
      "screen_name" : "alpapag",
      "protected" : false,
      "id_str" : "23337615",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/961669503921868807\/xUZ8FP3k_normal.jpg",
      "id" : 23337615,
      "verified" : false
    }
  },
  "id" : 966284901149560832,
  "created_at" : "2018-02-21 12:14:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/966257509160902656\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/isBR94ZgiJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWjWEt3X0AAwuMq.jpg",
      "id_str" : "966257496590635008",
      "id" : 966257496590635008,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWjWEt3X0AAwuMq.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/isBR94ZgiJ"
    } ],
    "hashtags" : [ {
      "text" : "PyDataDublin",
      "indices" : [ 18, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966257509160902656",
  "text" : "I have Docker and #PyDataDublin on \nmeetup the same day and same time... https:\/\/t.co\/isBR94ZgiJ",
  "id" : 966257509160902656,
  "created_at" : "2018-02-21 10:25:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 3, 14 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/Vkc47s5UT9",
      "expanded_url" : "http:\/\/favourites.io",
      "display_url" : "favourites.io"
    } ]
  },
  "geo" : { },
  "id_str" : "966236658566942721",
  "text" : "RT @Dotnetster: So am thinking about adding additional social networks to https:\/\/t.co\/Vkc47s5UT9. I want it to be a store for all your lik\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/Vkc47s5UT9",
        "expanded_url" : "http:\/\/favourites.io",
        "display_url" : "favourites.io"
      } ]
    },
    "geo" : { },
    "id_str" : "966224428278468609",
    "text" : "So am thinking about adding additional social networks to https:\/\/t.co\/Vkc47s5UT9. I want it to be a store for all your likes. What other social networks do you bookmark stuff with the intention of reading it later?",
    "id" : 966224428278468609,
    "created_at" : "2018-02-21 08:13:49 +0000",
    "user" : {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "protected" : false,
      "id_str" : "26903787",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459298001505099777\/lQd1OjeL_normal.jpeg",
      "id" : 26903787,
      "verified" : false
    }
  },
  "id" : 966236658566942721,
  "created_at" : "2018-02-21 09:02:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/TLyanlaY0e",
      "expanded_url" : "https:\/\/twitter.com\/summerbrennan\/status\/965501332202209280",
      "display_url" : "twitter.com\/summerbrennan\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "966236477502971904",
  "text" : "All animals live matter. https:\/\/t.co\/TLyanlaY0e",
  "id" : 966236477502971904,
  "created_at" : "2018-02-21 09:01:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter\uD83C\uDF0BBrannen",
      "screen_name" : "PeterBrannen1",
      "indices" : [ 3, 17 ],
      "id_str" : "1372200414",
      "id" : 1372200414
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966090572690808832",
  "text" : "RT @PeterBrannen1: The only way to stop a bad guy with asbestos is a good guy with asbestos\n\nIf you ban asbestos, building contractors woul\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "965655220373065729",
    "text" : "The only way to stop a bad guy with asbestos is a good guy with asbestos\n\nIf you ban asbestos, building contractors would just find another way to kill occupants with carcinogenic insulation\n\nThe problem isn't asbestos, it's mental health\n\nWe need to put asbestos in every school",
    "id" : 965655220373065729,
    "created_at" : "2018-02-19 18:32:00 +0000",
    "user" : {
      "name" : "Peter\uD83C\uDF0BBrannen",
      "screen_name" : "PeterBrannen1",
      "protected" : false,
      "id_str" : "1372200414",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/975430888099074049\/gLrKU5CK_normal.jpg",
      "id" : 1372200414,
      "verified" : true
    }
  },
  "id" : 966090572690808832,
  "created_at" : "2018-02-20 23:21:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Noddlepod",
      "screen_name" : "Noddlepod",
      "indices" : [ 3, 13 ],
      "id_str" : "284703421",
      "id" : 284703421
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966033426708090881",
  "text" : "RT @Noddlepod: We cannot always predict who and when would benefit from what we know and the resources we come across. Build a culture wher\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/meetedgar.com\" rel=\"nofollow\"\u003EMeet Edgar\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Noddlepod\/status\/965890063719682048\/photo\/1",
        "indices" : [ 276, 299 ],
        "url" : "https:\/\/t.co\/ATlIXky9Ov",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWeH4qLVoAAzc_f.jpg",
        "id_str" : "965890052558528512",
        "id" : 965890052558528512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWeH4qLVoAAzc_f.jpg",
        "sizes" : [ {
          "h" : 700,
          "resize" : "fit",
          "w" : 1050
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 1050
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 1050
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ATlIXky9Ov"
      } ],
      "hashtags" : [ {
        "text" : "faciltiationtip",
        "indices" : [ 241, 257 ]
      }, {
        "text" : "knowledgesharing",
        "indices" : [ 258, 275 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "965890063719682048",
    "text" : "We cannot always predict who and when would benefit from what we know and the resources we come across. Build a culture where \"making a note of something\" is a collective activity and \"knowledge shared\" dont disappear in an activity stream. #faciltiationtip #knowledgesharing https:\/\/t.co\/ATlIXky9Ov",
    "id" : 965890063719682048,
    "created_at" : "2018-02-20 10:05:11 +0000",
    "user" : {
      "name" : "Noddlepod",
      "screen_name" : "Noddlepod",
      "protected" : false,
      "id_str" : "284703421",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/789080948541390848\/ZWbteXC1_normal.jpg",
      "id" : 284703421,
      "verified" : false
    }
  },
  "id" : 966033426708090881,
  "created_at" : "2018-02-20 19:34:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "966020826058412032",
  "text" : "What company\/product were you most sad\/surprised when it shut down? For me is Everpix",
  "id" : 966020826058412032,
  "created_at" : "2018-02-20 18:44:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "indices" : [ 3, 16 ],
      "id_str" : "19698834",
      "id" : 19698834
    }, {
      "name" : "Luis Suarez",
      "screen_name" : "LuisSuarez9",
      "indices" : [ 65, 77 ],
      "id_str" : "213745334",
      "id" : 213745334
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CNY",
      "indices" : [ 18, 22 ]
    }, {
      "text" : "TourismMalaysia",
      "indices" : [ 37, 53 ]
    } ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/kP8TdCLtT2",
      "expanded_url" : "http:\/\/ow.ly\/ry9u30iv2Dd",
      "display_url" : "ow.ly\/ry9u30iv2Dd"
    } ]
  },
  "geo" : { },
  "id_str" : "966018672497250305",
  "text" : "RT @MarketingEds: #CNY greeting from #TourismMalaysia ambassador @LuisSuarez9  gets red card https:\/\/t.co\/kP8TdCLtT2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Luis Suarez",
        "screen_name" : "LuisSuarez9",
        "indices" : [ 47, 59 ],
        "id_str" : "213745334",
        "id" : 213745334
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CNY",
        "indices" : [ 0, 4 ]
      }, {
        "text" : "TourismMalaysia",
        "indices" : [ 19, 35 ]
      } ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/kP8TdCLtT2",
        "expanded_url" : "http:\/\/ow.ly\/ry9u30iv2Dd",
        "display_url" : "ow.ly\/ry9u30iv2Dd"
      } ]
    },
    "geo" : { },
    "id_str" : "965911495161929728",
    "text" : "#CNY greeting from #TourismMalaysia ambassador @LuisSuarez9  gets red card https:\/\/t.co\/kP8TdCLtT2",
    "id" : 965911495161929728,
    "created_at" : "2018-02-20 11:30:20 +0000",
    "user" : {
      "name" : "MARKETING MAGAZINE",
      "screen_name" : "MarketingEds",
      "protected" : false,
      "id_str" : "19698834",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948003366164033536\/_ojmapns_normal.jpg",
      "id" : 19698834,
      "verified" : false
    }
  },
  "id" : 966018672497250305,
  "created_at" : "2018-02-20 18:36:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/TxaPhshSft",
      "expanded_url" : "http:\/\/technode.com\/2018\/02\/18\/wechat-hongbao-2018\/",
      "display_url" : "technode.com\/2018\/02\/18\/wec\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965985718857355266",
  "text" : "688 million people used WeChat hongbao (red envelope) on Chinese New Year Eve - https:\/\/t.co\/TxaPhshSft",
  "id" : 965985718857355266,
  "created_at" : "2018-02-20 16:25:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Manchester",
      "indices" : [ 18, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965957227566780421",
  "text" : "A pop-up stall in #Manchester Oxford road are not very forthcoming to our enquiries about the rental rates. Granted they already have some FnB tenant.",
  "id" : 965957227566780421,
  "created_at" : "2018-02-20 14:32:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965953274573619206\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/6Kxt9W2PPP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWfBVqBW0AEl49Y.jpg",
      "id_str" : "965953222895652865",
      "id" : 965953222895652865,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWfBVqBW0AEl49Y.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6Kxt9W2PPP"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965953274573619206",
  "text" : "The importance of reframing (pg 48, Desiging for Growth) https:\/\/t.co\/6Kxt9W2PPP",
  "id" : 965953274573619206,
  "created_at" : "2018-02-20 14:16:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965949167620247553\/photo\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/IrZYdorzvv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWe9m8UWsAABDta.jpg",
      "id_str" : "965949121818439680",
      "id" : 965949121818439680,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWe9m8UWsAABDta.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IrZYdorzvv"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965949167620247553\/photo\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/IrZYdorzvv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWe9m8UW4AAWhdb.jpg",
      "id_str" : "965949121818451968",
      "id" : 965949121818451968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWe9m8UW4AAWhdb.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IrZYdorzvv"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965949167620247553",
  "text" : "Has always being baffled by the term \"design thinking\". Now I am not confuse anymore. https:\/\/t.co\/IrZYdorzvv",
  "id" : 965949167620247553,
  "created_at" : "2018-02-20 14:00:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deborah Kay",
      "screen_name" : "debbiediscovers",
      "indices" : [ 3, 19 ],
      "id_str" : "3060836348",
      "id" : 3060836348
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bots",
      "indices" : [ 101, 106 ]
    }, {
      "text" : "AI",
      "indices" : [ 107, 110 ]
    }, {
      "text" : "chatbots",
      "indices" : [ 111, 120 ]
    } ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/LvJb1f5zre",
      "expanded_url" : "https:\/\/buff.ly\/2Az0V4J",
      "display_url" : "buff.ly\/2Az0V4J"
    } ]
  },
  "geo" : { },
  "id_str" : "965917366843568129",
  "text" : "RT @debbiediscovers: The 'robot lawyer' giving free legal advice to refugees https:\/\/t.co\/LvJb1f5zre #bots #AI #chatbots",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "bots",
        "indices" : [ 80, 85 ]
      }, {
        "text" : "AI",
        "indices" : [ 86, 89 ]
      }, {
        "text" : "chatbots",
        "indices" : [ 90, 99 ]
      } ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/LvJb1f5zre",
        "expanded_url" : "https:\/\/buff.ly\/2Az0V4J",
        "display_url" : "buff.ly\/2Az0V4J"
      } ]
    },
    "geo" : { },
    "id_str" : "965617259644321793",
    "text" : "The 'robot lawyer' giving free legal advice to refugees https:\/\/t.co\/LvJb1f5zre #bots #AI #chatbots",
    "id" : 965617259644321793,
    "created_at" : "2018-02-19 16:01:09 +0000",
    "user" : {
      "name" : "Deborah Kay",
      "screen_name" : "debbiediscovers",
      "protected" : false,
      "id_str" : "3060836348",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1027068089618198536\/wIl1K2Fm_normal.jpg",
      "id" : 3060836348,
      "verified" : false
    }
  },
  "id" : 965917366843568129,
  "created_at" : "2018-02-20 11:53:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MSP",
      "screen_name" : "TweetsbyMSP",
      "indices" : [ 3, 15 ],
      "id_str" : "2610093733",
      "id" : 2610093733
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965903509907533824",
  "text" : "RT @TweetsbyMSP: The search is now on for UK\u2019s most innovative tech start-ups! We're very excited to announce our free desk competition wit\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TweetsbyMSP\/status\/965611735544664064\/photo\/1",
        "indices" : [ 276, 299 ],
        "url" : "https:\/\/t.co\/bZilbxitNj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWaKwU0WkAIYv9_.jpg",
        "id_str" : "965611732944195586",
        "id" : 965611732944195586,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWaKwU0WkAIYv9_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bZilbxitNj"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/TweetsbyMSP\/status\/965611735544664064\/photo\/1",
        "indices" : [ 276, 299 ],
        "url" : "https:\/\/t.co\/bZilbxitNj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWaKwRnXkAAO8SG.jpg",
        "id_str" : "965611732084428800",
        "id" : 965611732084428800,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWaKwRnXkAAO8SG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bZilbxitNj"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 252, 275 ],
        "url" : "https:\/\/t.co\/8SwWqA7fbq",
        "expanded_url" : "https:\/\/mspl.co.uk\/news\/search-is-on-for-uk-s-most-innovative-tech-start-ups\/",
        "display_url" : "mspl.co.uk\/news\/search-is\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "965611735544664064",
    "text" : "The search is now on for UK\u2019s most innovative tech start-ups! We're very excited to announce our free desk competition with access to a specialist business growth programme for digital innovators at our Tech Incubator, opening this May! Read more here https:\/\/t.co\/8SwWqA7fbq https:\/\/t.co\/bZilbxitNj",
    "id" : 965611735544664064,
    "created_at" : "2018-02-19 15:39:12 +0000",
    "user" : {
      "name" : "MSP",
      "screen_name" : "TweetsbyMSP",
      "protected" : false,
      "id_str" : "2610093733",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/888024781181812737\/HAeRJVzR_normal.jpg",
      "id" : 2610093733,
      "verified" : false
    }
  },
  "id" : 965903509907533824,
  "created_at" : "2018-02-20 10:58:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 122, 145 ],
      "url" : "https:\/\/t.co\/Q39J2Ym2EU",
      "expanded_url" : "http:\/\/bit.ly\/2hlOBx6",
      "display_url" : "bit.ly\/2hlOBx6"
    } ]
  },
  "geo" : { },
  "id_str" : "965878229423869952",
  "text" : "A pop-up shopping and dining complex constructed of re-purposed shipping containers. Hatch - what is it and where is it?  https:\/\/t.co\/Q39J2Ym2EU",
  "id" : 965878229423869952,
  "created_at" : "2018-02-20 09:18:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/HOlKvh0kND",
      "expanded_url" : "https:\/\/blog.ycombinator.com\/why-toys\/",
      "display_url" : "blog.ycombinator.com\/why-toys\/"
    } ]
  },
  "geo" : { },
  "id_str" : "965873734233083904",
  "text" : "Making fun toys vs. making serious tools https:\/\/t.co\/HOlKvh0kND",
  "id" : 965873734233083904,
  "created_at" : "2018-02-20 09:00:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 8, 23 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965703376884391936",
  "text" : "Is that @paulinesargent on How's Your Driving on RTE?",
  "id" : 965703376884391936,
  "created_at" : "2018-02-19 21:43:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/OXqzaJchvu",
      "expanded_url" : "https:\/\/twitter.com\/mehdirhasan\/status\/964924382094209024",
      "display_url" : "twitter.com\/mehdirhasan\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965701153370624002",
  "text" : "This is brilliant. https:\/\/t.co\/OXqzaJchvu",
  "id" : 965701153370624002,
  "created_at" : "2018-02-19 21:34:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sophie Freiermuth",
      "screen_name" : "wickedgeekie",
      "indices" : [ 3, 16 ],
      "id_str" : "21292066",
      "id" : 21292066
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UX",
      "indices" : [ 99, 102 ]
    }, {
      "text" : "toosenior",
      "indices" : [ 122, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965699348246999040",
  "text" : "RT @wickedgeekie: Meanwhile on LinkedIn, I inadvertently triggered a lively conversation on senior #UX designers that are #toosenior to hir\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "UX",
        "indices" : [ 81, 84 ]
      }, {
        "text" : "toosenior",
        "indices" : [ 104, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 167, 190 ],
        "url" : "https:\/\/t.co\/QcZKmFeHNY",
        "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6370221055927214080",
        "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "965691699640127488",
    "text" : "Meanwhile on LinkedIn, I inadvertently triggered a lively conversation on senior #UX designers that are #toosenior to hire. \nThis way please, and remember to be kind. https:\/\/t.co\/QcZKmFeHNY",
    "id" : 965691699640127488,
    "created_at" : "2018-02-19 20:56:57 +0000",
    "user" : {
      "name" : "Sophie Freiermuth",
      "screen_name" : "wickedgeekie",
      "protected" : false,
      "id_str" : "21292066",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/874067305487114240\/4xRANO1X_normal.png",
      "id" : 21292066,
      "verified" : false
    }
  },
  "id" : 965699348246999040,
  "created_at" : "2018-02-19 21:27:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965684672389877761\/photo\/1",
      "indices" : [ 180, 203 ],
      "url" : "https:\/\/t.co\/cd0ZFYgA9a",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWbLxEAW4AUU-pf.jpg",
      "id_str" : "965683213866819589",
      "id" : 965683213866819589,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWbLxEAW4AUU-pf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1364
      }, {
        "h" : 348,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 614,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 698,
        "resize" : "fit",
        "w" : 1364
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cd0ZFYgA9a"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965684672389877761\/photo\/1",
      "indices" : [ 180, 203 ],
      "url" : "https:\/\/t.co\/cd0ZFYgA9a",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWbLyTZW0AEIQMH.jpg",
      "id_str" : "965683235178074113",
      "id" : 965683235178074113,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWbLyTZW0AEIQMH.jpg",
      "sizes" : [ {
        "h" : 738,
        "resize" : "fit",
        "w" : 1338
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 375,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 738,
        "resize" : "fit",
        "w" : 1338
      }, {
        "h" : 662,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cd0ZFYgA9a"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965684672389877761",
  "text" : "Both offers speed test on how fast your website loads. One ask for your detail before you are shown the test results.  Another give you the results first before asking your email. https:\/\/t.co\/cd0ZFYgA9a",
  "id" : 965684672389877761,
  "created_at" : "2018-02-19 20:29:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/BRVPJJz7yC",
      "expanded_url" : "https:\/\/mothership.sg\/2018\/02\/mosque-distribute-oranges-drinks-cny\/",
      "display_url" : "mothership.sg\/2018\/02\/mosque\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965658135695364096",
  "text" : "S\u2019pore mosque distributes oranges  https:\/\/t.co\/BRVPJJz7yC",
  "id" : 965658135695364096,
  "created_at" : "2018-02-19 18:43:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 119, 142 ],
      "url" : "https:\/\/t.co\/s0559qyrwB",
      "expanded_url" : "http:\/\/faculty.chicagobooth.edu\/christopher.hsee\/vita\/Papers\/DistinctionBias.pdf",
      "display_url" : "faculty.chicagobooth.edu\/christopher.hs\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965645085533704192",
  "text" : "Distinction bias\u200A\u2014\u200Aa tendency to over-value the effect of small quantitative differences when comparing options. [PDF] https:\/\/t.co\/s0559qyrwB",
  "id" : 965645085533704192,
  "created_at" : "2018-02-19 17:51:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/965629781596401664\/photo\/1",
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/490WM53Nkp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWabKx1XkAAsjUO.jpg",
      "id_str" : "965629779595726848",
      "id" : 965629779595726848,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWabKx1XkAAsjUO.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/490WM53Nkp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965630181913382913",
  "text" : "RT @GeoffreyIRL: Now there\u2019s a thought... https:\/\/t.co\/490WM53Nkp",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GeoffreyIRL\/status\/965629781596401664\/photo\/1",
        "indices" : [ 25, 48 ],
        "url" : "https:\/\/t.co\/490WM53Nkp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWabKx1XkAAsjUO.jpg",
        "id_str" : "965629779595726848",
        "id" : 965629779595726848,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWabKx1XkAAsjUO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/490WM53Nkp"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "965629781596401664",
    "text" : "Now there\u2019s a thought... https:\/\/t.co\/490WM53Nkp",
    "id" : 965629781596401664,
    "created_at" : "2018-02-19 16:50:55 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 965630181913382913,
  "created_at" : "2018-02-19 16:52:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "965622494450323461",
  "geo" : { },
  "id_str" : "965629955278286851",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent A convenient \"health and safety\" I would say.  School can always bring the student to the library unless there is no one to supervise.",
  "id" : 965629955278286851,
  "in_reply_to_status_id" : 965622494450323461,
  "created_at" : "2018-02-19 16:51:36 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "G M Police",
      "screen_name" : "gmpolice",
      "indices" : [ 3, 12 ],
      "id_str" : "21857067",
      "id" : 21857067
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965565903999062016",
  "text" : "RT @gmpolice: Sunday 18 February 2018 saw huge crowds line the streets of Manchester to celebrate Chinese New Year.\nView more images from t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/crowdcontrolhq.com\/\" rel=\"nofollow\"\u003ECrowdControlHQ\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/r9wnyxrIDX",
        "expanded_url" : "https:\/\/goo.gl\/rcBCfQ",
        "display_url" : "goo.gl\/rcBCfQ"
      } ]
    },
    "geo" : { },
    "id_str" : "965560963478024192",
    "text" : "Sunday 18 February 2018 saw huge crowds line the streets of Manchester to celebrate Chinese New Year.\nView more images from the event here: https:\/\/t.co\/r9wnyxrIDX",
    "id" : 965560963478024192,
    "created_at" : "2018-02-19 12:17:27 +0000",
    "user" : {
      "name" : "G M Police",
      "screen_name" : "gmpolice",
      "protected" : false,
      "id_str" : "21857067",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034358229545955328\/U1DbvR3W_normal.jpg",
      "id" : 21857067,
      "verified" : true
    }
  },
  "id" : 965565903999062016,
  "created_at" : "2018-02-19 12:37:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/V2NvWz2UjP",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/965517797609521152",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965533897776881664",
  "text" : "\uD83E\uDD14 https:\/\/t.co\/V2NvWz2UjP",
  "id" : 965533897776881664,
  "created_at" : "2018-02-19 10:29:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 30, 46 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Manchester",
      "indices" : [ 78, 89 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965533510839799808",
  "text" : "Working behind the scene with @jackiepenangkit to bring Penang Street Food to #Manchester",
  "id" : 965533510839799808,
  "created_at" : "2018-02-19 10:28:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965533161076740096",
  "text" : "RT @ChannelNewsAsia: NEW: GST to apply to imported services, such as apps, movies and music downloaded from overseas from Jan 2020; e-comme\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/965517030798409728\/photo\/1",
        "indices" : [ 184, 207 ],
        "url" : "https:\/\/t.co\/J5ZZDIVe66",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWYzpa-VoAAq49S.jpg",
        "id_str" : "965515956825989120",
        "id" : 965515956825989120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWYzpa-VoAAq49S.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J5ZZDIVe66"
      } ],
      "hashtags" : [ {
        "text" : "SGBudget2018",
        "indices" : [ 145, 158 ]
      } ],
      "urls" : [ {
        "indices" : [ 160, 183 ],
        "url" : "https:\/\/t.co\/GmwWlQZ6Oh",
        "expanded_url" : "http:\/\/cna.asia\/sgbudget2018live",
        "display_url" : "cna.asia\/sgbudget2018li\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "965515584749281281",
    "geo" : { },
    "id_str" : "965517030798409728",
    "in_reply_to_user_id" : 38400130,
    "text" : "NEW: GST to apply to imported services, such as apps, movies and music downloaded from overseas from Jan 2020; e-commerce for goods not affected #SGBudget2018\n\nhttps:\/\/t.co\/GmwWlQZ6Oh https:\/\/t.co\/J5ZZDIVe66",
    "id" : 965517030798409728,
    "in_reply_to_status_id" : 965515584749281281,
    "created_at" : "2018-02-19 09:22:53 +0000",
    "in_reply_to_screen_name" : "ChannelNewsAsia",
    "in_reply_to_user_id_str" : "38400130",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 965533161076740096,
  "created_at" : "2018-02-19 10:26:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jackiepenangkit\/status\/965525047195656194\/photo\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/aqL0d7ttGf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWY7mCAX0AAk_xv.jpg",
      "id_str" : "965524694677049344",
      "id" : 965524694677049344,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWY7mCAX0AAk_xv.jpg",
      "sizes" : [ {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/aqL0d7ttGf"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965532658250940416",
  "text" : "RT @jackiepenangkit: Checking out the next location https:\/\/t.co\/aqL0d7ttGf",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jackiepenangkit\/status\/965525047195656194\/photo\/1",
        "indices" : [ 31, 54 ],
        "url" : "https:\/\/t.co\/aqL0d7ttGf",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWY7mCAX0AAk_xv.jpg",
        "id_str" : "965524694677049344",
        "id" : 965524694677049344,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWY7mCAX0AAk_xv.jpg",
        "sizes" : [ {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 2976,
          "resize" : "fit",
          "w" : 3968
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aqL0d7ttGf"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "965525047195656194",
    "text" : "Checking out the next location https:\/\/t.co\/aqL0d7ttGf",
    "id" : 965525047195656194,
    "created_at" : "2018-02-19 09:54:44 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 965532658250940416,
  "created_at" : "2018-02-19 10:24:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965499113184874496",
  "text" : "RT @ChannelNewsAsia: Singapore Govt to prepare for 3 major shifts in coming decade: \n\n- Shift in global economic weight towards Asia\n- Emer\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/965489393195601920\/photo\/1",
        "indices" : [ 193, 216 ],
        "url" : "https:\/\/t.co\/e396hufKfZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWYaTGgVAAEkeJH.jpg",
        "id_str" : "965488085583593473",
        "id" : 965488085583593473,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWYaTGgVAAEkeJH.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1187,
          "resize" : "fit",
          "w" : 1591
        }, {
          "h" : 895,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 507,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1187,
          "resize" : "fit",
          "w" : 1591
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/e396hufKfZ"
      } ],
      "hashtags" : [ {
        "text" : "SGBudget2018",
        "indices" : [ 179, 192 ]
      } ],
      "urls" : [ {
        "indices" : [ 154, 177 ],
        "url" : "https:\/\/t.co\/GmwWlQZ6Oh",
        "expanded_url" : "http:\/\/cna.asia\/sgbudget2018live",
        "display_url" : "cna.asia\/sgbudget2018li\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "965489393195601920",
    "text" : "Singapore Govt to prepare for 3 major shifts in coming decade: \n\n- Shift in global economic weight towards Asia\n- Emergence of new tech\n- Ageing society\n\nhttps:\/\/t.co\/GmwWlQZ6Oh  #SGBudget2018 https:\/\/t.co\/e396hufKfZ",
    "id" : 965489393195601920,
    "created_at" : "2018-02-19 07:33:03 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 965499113184874496,
  "created_at" : "2018-02-19 08:11:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sean J. Taylor",
      "screen_name" : "seanjtaylor",
      "indices" : [ 3, 15 ],
      "id_str" : "20963651",
      "id" : 20963651
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965385341946552320",
  "text" : "RT @seanjtaylor: Pessimist: fits a churn prediction model\nOptimist: fits a retention prediction model\nRealist: runs an A\/B test to improve\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964623975174189056",
    "text" : "Pessimist: fits a churn prediction model\nOptimist: fits a retention prediction model\nRealist: runs an A\/B test to improve the product",
    "id" : 964623975174189056,
    "created_at" : "2018-02-16 22:14:12 +0000",
    "user" : {
      "name" : "Sean J. Taylor",
      "screen_name" : "seanjtaylor",
      "protected" : false,
      "id_str" : "20963651",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/592774240845340673\/15noASOk_normal.jpg",
      "id" : 20963651,
      "verified" : false
    }
  },
  "id" : 965385341946552320,
  "created_at" : "2018-02-19 00:39:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Josh Dawsey",
      "screen_name" : "jdawsey1",
      "indices" : [ 3, 12 ],
      "id_str" : "38936142",
      "id" : 38936142
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jdawsey1\/status\/964888877176680450\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/kA4vkAvXJO",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWP5Uc-WkAAE1pL.jpg",
      "id_str" : "964888874957836288",
      "id" : 964888874957836288,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWP5Uc-WkAAE1pL.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/kA4vkAvXJO"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965380475345924096",
  "text" : "RT @jdawsey1: WSJ devotes a full page to the names and victims of the last 28 years of school shootings. https:\/\/t.co\/kA4vkAvXJO",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jdawsey1\/status\/964888877176680450\/photo\/1",
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/kA4vkAvXJO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWP5Uc-WkAAE1pL.jpg",
        "id_str" : "964888874957836288",
        "id" : 964888874957836288,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWP5Uc-WkAAE1pL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kA4vkAvXJO"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964888877176680450",
    "text" : "WSJ devotes a full page to the names and victims of the last 28 years of school shootings. https:\/\/t.co\/kA4vkAvXJO",
    "id" : 964888877176680450,
    "created_at" : "2018-02-17 15:46:49 +0000",
    "user" : {
      "name" : "Josh Dawsey",
      "screen_name" : "jdawsey1",
      "protected" : false,
      "id_str" : "38936142",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037084919300845568\/yAkIuETf_normal.jpg",
      "id" : 38936142,
      "verified" : true
    }
  },
  "id" : 965380475345924096,
  "created_at" : "2018-02-19 00:20:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Samuel Scott",
      "screen_name" : "samueljscott",
      "indices" : [ 3, 16 ],
      "id_str" : "99086752",
      "id" : 99086752
    }, {
      "name" : "adcontrarian",
      "screen_name" : "AdContrarian",
      "indices" : [ 106, 119 ],
      "id_str" : "39105959",
      "id" : 39105959
    }, {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "indices" : [ 124, 134 ],
      "id_str" : "911413417",
      "id" : 911413417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "965337456315895809",
  "text" : "RT @samueljscott: In an online world that focuses on boring \"content,\" good copywriting such as that from @AdContrarian and @Dr_Draper is a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "adcontrarian",
        "screen_name" : "AdContrarian",
        "indices" : [ 88, 101 ],
        "id_str" : "39105959",
        "id" : 39105959
      }, {
        "name" : "Ryan Wallman",
        "screen_name" : "Dr_Draper",
        "indices" : [ 106, 116 ],
        "id_str" : "911413417",
        "id" : 911413417
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 246, 269 ],
        "url" : "https:\/\/t.co\/2en7XEaq8K",
        "expanded_url" : "http:\/\/typeagroup.cmail19.com\/t\/ViewEmail\/d\/7A650B45BD71BCBA2540EF23F30FEDED\/0A51BFBF69597730B4B1B1F623478121",
        "display_url" : "typeagroup.cmail19.com\/t\/ViewEmail\/d\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "965282283757408264",
    "text" : "In an online world that focuses on boring \"content,\" good copywriting such as that from @AdContrarian and @Dr_Draper is a lost art. Take how the former refers to Google Chrome's ad blocking as \"Dracula guarding the blood bank.\" Bloody brilliant. https:\/\/t.co\/2en7XEaq8K",
    "id" : 965282283757408264,
    "created_at" : "2018-02-18 17:50:05 +0000",
    "user" : {
      "name" : "Samuel Scott",
      "screen_name" : "samueljscott",
      "protected" : false,
      "id_str" : "99086752",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/901871029458530304\/-II4J2Tu_normal.jpg",
      "id" : 99086752,
      "verified" : true
    }
  },
  "id" : 965337456315895809,
  "created_at" : "2018-02-18 21:29:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "indices" : [ 3, 19 ],
      "id_str" : "14340736",
      "id" : 14340736
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/6OWnAOrbHG",
      "expanded_url" : "https:\/\/twitter.com\/FoxNews\/status\/965266900417699844",
      "display_url" : "twitter.com\/FoxNews\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965332133857366017",
  "text" : "RT @interactivemark: I could never become a full-blooded Republican because the required lobotomy isn't my jam. https:\/\/t.co\/6OWnAOrbHG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/6OWnAOrbHG",
        "expanded_url" : "https:\/\/twitter.com\/FoxNews\/status\/965266900417699844",
        "display_url" : "twitter.com\/FoxNews\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "965286206903914496",
    "text" : "I could never become a full-blooded Republican because the required lobotomy isn't my jam. https:\/\/t.co\/6OWnAOrbHG",
    "id" : 965286206903914496,
    "created_at" : "2018-02-18 18:05:40 +0000",
    "user" : {
      "name" : "Mark",
      "screen_name" : "interactivemark",
      "protected" : false,
      "id_str" : "14340736",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/974702103984791552\/eHtIZ2ef_normal.jpg",
      "id" : 14340736,
      "verified" : false
    }
  },
  "id" : 965332133857366017,
  "created_at" : "2018-02-18 21:08:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965248939644669952\/photo\/1",
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/7IdGSFcgpp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWVAy4BX0AEU1iT.jpg",
      "id_str" : "965248937916616705",
      "id" : 965248937916616705,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWVAy4BX0AEU1iT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/7IdGSFcgpp"
    } ],
    "hashtags" : [ {
      "text" : "readmcr",
      "indices" : [ 0, 8 ]
    }, {
      "text" : "legocharlie",
      "indices" : [ 9, 21 ]
    } ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/rM2KnA2JVO",
      "expanded_url" : "http:\/\/ift.tt\/2Ctv4zt",
      "display_url" : "ift.tt\/2Ctv4zt"
    } ]
  },
  "geo" : { },
  "id_str" : "965248939644669952",
  "text" : "#readmcr #legocharlie https:\/\/t.co\/rM2KnA2JVO https:\/\/t.co\/7IdGSFcgpp",
  "id" : 965248939644669952,
  "created_at" : "2018-02-18 15:37:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/965248235546824704\/photo\/1",
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/ekWWpEFZNJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWVAJzxW0AE5SMe.jpg",
      "id_str" : "965248232401063937",
      "id" : 965248232401063937,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWVAJzxW0AE5SMe.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ekWWpEFZNJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/jOHjvIqPly",
      "expanded_url" : "http:\/\/ift.tt\/2ExZGpg",
      "display_url" : "ift.tt\/2ExZGpg"
    } ]
  },
  "geo" : { },
  "id_str" : "965248235546824704",
  "text" : "https:\/\/t.co\/jOHjvIqPly https:\/\/t.co\/ekWWpEFZNJ",
  "id" : 965248235546824704,
  "created_at" : "2018-02-18 15:34:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/HH02IGT5eY",
      "expanded_url" : "https:\/\/twitter.com\/Lee_Saunders30\/status\/959806307845931008",
      "display_url" : "twitter.com\/Lee_Saunders30\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965170915003772929",
  "text" : "I agreed. https:\/\/t.co\/HH02IGT5eY",
  "id" : 965170915003772929,
  "created_at" : "2018-02-18 10:27:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/YwObeotote",
      "expanded_url" : "http:\/\/osxdaily.com\/2017\/11\/20\/stop-autoplay-video-audio-chrome\/",
      "display_url" : "osxdaily.com\/2017\/11\/20\/sto\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "965124151924183040",
  "text" : "How to Stop Autoplay Video in Chrome -  https:\/\/t.co\/YwObeotote",
  "id" : 965124151924183040,
  "created_at" : "2018-02-18 07:21:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964988990293204992\/photo\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/MwiCHlshaz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWRUXT8XUAE_8Ga.jpg",
      "id_str" : "964988979631312897",
      "id" : 964988979631312897,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWRUXT8XUAE_8Ga.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 798,
        "resize" : "fit",
        "w" : 532
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 453
      }, {
        "h" : 798,
        "resize" : "fit",
        "w" : 532
      }, {
        "h" : 798,
        "resize" : "fit",
        "w" : 532
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/MwiCHlshaz"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964988990293204992",
  "text" : "Someone in the family mentioned we should checkout this \"Jackie Chan IRA\" film... https:\/\/t.co\/MwiCHlshaz",
  "id" : 964988990293204992,
  "created_at" : "2018-02-17 22:24:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964987023030411264\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/cPtp1R0Ncj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWRSlT_XUAE8SdX.jpg",
      "id_str" : "964987021138808833",
      "id" : 964987021138808833,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWRSlT_XUAE8SdX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cPtp1R0Ncj"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/UCbPW3U1ag",
      "expanded_url" : "http:\/\/ift.tt\/2GlRgxW",
      "display_url" : "ift.tt\/2GlRgxW"
    } ]
  },
  "geo" : { },
  "id_str" : "964987023030411264",
  "text" : "Reunion dinner on the eve of CNY https:\/\/t.co\/UCbPW3U1ag https:\/\/t.co\/cPtp1R0Ncj",
  "id" : 964987023030411264,
  "created_at" : "2018-02-17 22:16:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Juan Ar\u00E9valo",
      "screen_name" : "juarelerrr",
      "indices" : [ 3, 14 ],
      "id_str" : "782265789634252800",
      "id" : 782265789634252800
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964979149457842178",
  "text" : "RT @juarelerrr: This is a major stopper for the development of AI. Please, share your code, benchmark your algorithms, use public dataset.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 167, 190 ],
        "url" : "https:\/\/t.co\/sMllFDrqfb",
        "expanded_url" : "https:\/\/twitter.com\/Reza_Zadeh\/status\/964615000022163456",
        "display_url" : "twitter.com\/Reza_Zadeh\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "964783123216158720",
    "text" : "This is a major stopper for the development of AI. Please, share your code, benchmark your algorithms, use public dataset. In other words, let science be scientific!! https:\/\/t.co\/sMllFDrqfb",
    "id" : 964783123216158720,
    "created_at" : "2018-02-17 08:46:36 +0000",
    "user" : {
      "name" : "Juan Ar\u00E9valo",
      "screen_name" : "juarelerrr",
      "protected" : false,
      "id_str" : "782265789634252800",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/909864609951174656\/_l6QtLnW_normal.jpg",
      "id" : 782265789634252800,
      "verified" : false
    }
  },
  "id" : 964979149457842178,
  "created_at" : "2018-02-17 21:45:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen Holub",
      "screen_name" : "allenholub",
      "indices" : [ 3, 14 ],
      "id_str" : "21375846",
      "id" : 21375846
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964933196348616706",
  "text" : "RT @allenholub: Agile environments are learning environments. Everything you do, from process changes to implementing a story, is an experi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "959840863676321792",
    "text" : "Agile environments are learning environments. Everything you do, from process changes to implementing a story, is an experiment. If that experiment doesn\u2019t work out the way you expect, it\u2019s not \u201Ca failure,\u201D it\u2019s data. Use that data to formulate the next experiment.",
    "id" : 959840863676321792,
    "created_at" : "2018-02-03 17:27:49 +0000",
    "user" : {
      "name" : "Allen Holub",
      "screen_name" : "allenholub",
      "protected" : false,
      "id_str" : "21375846",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/551535448\/DSC_0085.close.cropped_normal.jpg",
      "id" : 21375846,
      "verified" : false
    }
  },
  "id" : 964933196348616706,
  "created_at" : "2018-02-17 18:42:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Siobh\u00E1n Grayson",
      "screen_name" : "siobhan_grayson",
      "indices" : [ 0, 16 ],
      "id_str" : "4120580026",
      "id" : 4120580026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "964821031008587776",
  "geo" : { },
  "id_str" : "964822313937010688",
  "in_reply_to_user_id" : 4120580026,
  "text" : "@siobhan_grayson I will when the same messAge pop up next time. \uD83D\uDC4C\uD83D\uDE01",
  "id" : 964822313937010688,
  "in_reply_to_status_id" : 964821031008587776,
  "created_at" : "2018-02-17 11:22:19 +0000",
  "in_reply_to_screen_name" : "siobhan_grayson",
  "in_reply_to_user_id_str" : "4120580026",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964819722654056448\/photo\/1",
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/yy3MLk2IIQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWO6Y3bXUAAlcHk.jpg",
      "id_str" : "964819681545768960",
      "id" : 964819681545768960,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWO6Y3bXUAAlcHk.jpg",
      "sizes" : [ {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yy3MLk2IIQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964819722654056448",
  "text" : "Since when Google? https:\/\/t.co\/yy3MLk2IIQ",
  "id" : 964819722654056448,
  "created_at" : "2018-02-17 11:12:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "President of Ireland",
      "screen_name" : "PresidentIRL",
      "indices" : [ 3, 16 ],
      "id_str" : "569892832",
      "id" : 569892832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964818291385659392",
  "text" : "RT @PresidentIRL: \"I am delighted to send my best wishes to all members of the Chinese community in Ireland as you celebrate the commenceme\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ChineseNewYear",
        "indices" : [ 150, 165 ]
      }, {
        "text" : "XinNianKuaiLe",
        "indices" : [ 166, 180 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964400725210972161",
    "text" : "\"I am delighted to send my best wishes to all members of the Chinese community in Ireland as you celebrate the commencement of the Year of the Dog.\"\n\n#ChineseNewYear #XinNianKuaiLe 2018.",
    "id" : 964400725210972161,
    "created_at" : "2018-02-16 07:27:05 +0000",
    "user" : {
      "name" : "President of Ireland",
      "screen_name" : "PresidentIRL",
      "protected" : false,
      "id_str" : "569892832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/768749418187292672\/TSQViQLu_normal.jpg",
      "id" : 569892832,
      "verified" : true
    }
  },
  "id" : 964818291385659392,
  "created_at" : "2018-02-17 11:06:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 3, 9 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964783423373094912",
  "text" : "RT @mryap: What do you use to keep track of implementation and configuration changes in your web analytics tool? I create this \nhttps:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/AP6gE6g6uN",
        "expanded_url" : "http:\/\/bit.ly\/GANotes",
        "display_url" : "bit.ly\/GANotes"
      } ]
    },
    "geo" : { },
    "id_str" : "962046480658780161",
    "text" : "What do you use to keep track of implementation and configuration changes in your web analytics tool? I create this \nhttps:\/\/t.co\/AP6gE6g6uN",
    "id" : 962046480658780161,
    "created_at" : "2018-02-09 19:32:09 +0000",
    "user" : {
      "name" : "hello!",
      "screen_name" : "mryap",
      "protected" : false,
      "id_str" : "9465632",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
      "id" : 9465632,
      "verified" : false
    }
  },
  "id" : 964783423373094912,
  "created_at" : "2018-02-17 08:47:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Boylan",
      "screen_name" : "tom_nalyob",
      "indices" : [ 3, 14 ],
      "id_str" : "27388210",
      "id" : 27388210
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964782458624503808",
  "text" : "RT @tom_nalyob: In America, they say the only way to stop a bad guy with a gun is a good guy with a gun. \n\nBut that just sounds like someon\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964174179250720770",
    "text" : "In America, they say the only way to stop a bad guy with a gun is a good guy with a gun. \n\nBut that just sounds like someone trying to sell two guns.",
    "id" : 964174179250720770,
    "created_at" : "2018-02-15 16:26:52 +0000",
    "user" : {
      "name" : "Tom Boylan",
      "screen_name" : "tom_nalyob",
      "protected" : false,
      "id_str" : "27388210",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/964521090524557314\/FSx0uawF_normal.jpg",
      "id" : 27388210,
      "verified" : false
    }
  },
  "id" : 964782458624503808,
  "created_at" : "2018-02-17 08:43:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 101, 117 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/O6kd8nhlmQ",
      "expanded_url" : "http:\/\/po.st\/cZo441",
      "display_url" : "po.st\/cZo441"
    } ]
  },
  "geo" : { },
  "id_str" : "964756715215474688",
  "text" : "RT @edwinksl: Uber plans to sell Southeast Asia business to Grab: Report https:\/\/t.co\/O6kd8nhlmQ via @ChannelNewsAsia",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Channel NewsAsia",
        "screen_name" : "ChannelNewsAsia",
        "indices" : [ 87, 103 ],
        "id_str" : "38400130",
        "id" : 38400130
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/O6kd8nhlmQ",
        "expanded_url" : "http:\/\/po.st\/cZo441",
        "display_url" : "po.st\/cZo441"
      } ]
    },
    "geo" : { },
    "id_str" : "964694154184929280",
    "text" : "Uber plans to sell Southeast Asia business to Grab: Report https:\/\/t.co\/O6kd8nhlmQ via @ChannelNewsAsia",
    "id" : 964694154184929280,
    "created_at" : "2018-02-17 02:53:04 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 964756715215474688,
  "created_at" : "2018-02-17 07:01:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964631989935661058",
  "text" : "RT @enormous: \u201CReverse sit spin. Not everyone could do that.\u201D says the commentator on the short program. Stating the bleedin\u2019 obvious, I sa\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "WinterOlympics2018",
        "indices" : [ 128, 147 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964624106158215169",
    "text" : "\u201CReverse sit spin. Not everyone could do that.\u201D says the commentator on the short program. Stating the bleedin\u2019 obvious, I say. #WinterOlympics2018",
    "id" : 964624106158215169,
    "created_at" : "2018-02-16 22:14:43 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 964631989935661058,
  "created_at" : "2018-02-16 22:46:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Adereth",
      "screen_name" : "adereth",
      "indices" : [ 3, 11 ],
      "id_str" : "19135093",
      "id" : 19135093
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964628862012403713",
  "text" : "RT @adereth: Radia Perlman told the perfect story about how engineers come up with solutions before understanding the problem:\n\n\"When my so\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964315654969991168",
    "text" : "Radia Perlman told the perfect story about how engineers come up with solutions before understanding the problem:\n\n\"When my son was 3, he came to me crying 'my hand! my hand!'\n\nI started kissing it. *mwah* *mwah* *mwah* 'Tell me what happened...'\n\n'I got pee on it.'\"",
    "id" : 964315654969991168,
    "created_at" : "2018-02-16 01:49:02 +0000",
    "user" : {
      "name" : "Matt Adereth",
      "screen_name" : "adereth",
      "protected" : false,
      "id_str" : "19135093",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/878978642029666305\/er6nNCF0_normal.jpg",
      "id" : 19135093,
      "verified" : false
    }
  },
  "id" : 964628862012403713,
  "created_at" : "2018-02-16 22:33:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maria McHale",
      "screen_name" : "mchale_maria",
      "indices" : [ 3, 16 ],
      "id_str" : "3176753950",
      "id" : 3176753950
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/rdFk9Y2xQF",
      "expanded_url" : "https:\/\/twitter.com\/OrrCollins\/status\/964415859610365953",
      "display_url" : "twitter.com\/OrrCollins\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "964604004855623680",
  "text" : "RT @mchale_maria: This looks amazing \uD83D\uDE0D https:\/\/t.co\/rdFk9Y2xQF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 21, 44 ],
        "url" : "https:\/\/t.co\/rdFk9Y2xQF",
        "expanded_url" : "https:\/\/twitter.com\/OrrCollins\/status\/964415859610365953",
        "display_url" : "twitter.com\/OrrCollins\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "964597945772462081",
    "text" : "This looks amazing \uD83D\uDE0D https:\/\/t.co\/rdFk9Y2xQF",
    "id" : 964597945772462081,
    "created_at" : "2018-02-16 20:30:46 +0000",
    "user" : {
      "name" : "Maria McHale",
      "screen_name" : "mchale_maria",
      "protected" : false,
      "id_str" : "3176753950",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839173953843245056\/GnsOQaEh_normal.jpg",
      "id" : 3176753950,
      "verified" : false
    }
  },
  "id" : 964604004855623680,
  "created_at" : "2018-02-16 20:54:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PEANUTS",
      "screen_name" : "Snoopy",
      "indices" : [ 3, 10 ],
      "id_str" : "245548093",
      "id" : 245548093
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Snoopy\/status\/964509686996328448\/photo\/1",
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/v6nT5BAmHW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWKgcq4VoAECapS.jpg",
      "id_str" : "964509684618076161",
      "id" : 964509684618076161,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWKgcq4VoAECapS.jpg",
      "sizes" : [ {
        "h" : 612,
        "resize" : "fit",
        "w" : 612
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 612,
        "resize" : "fit",
        "w" : 612
      }, {
        "h" : 612,
        "resize" : "fit",
        "w" : 612
      }, {
        "h" : 612,
        "resize" : "fit",
        "w" : 612
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/v6nT5BAmHW"
    } ],
    "hashtags" : [ {
      "text" : "LunarNewYear",
      "indices" : [ 42, 55 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964539200023523328",
  "text" : "RT @Snoopy: Welcoming the Year of the Dog #LunarNewYear https:\/\/t.co\/v6nT5BAmHW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Snoopy\/status\/964509686996328448\/photo\/1",
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/v6nT5BAmHW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWKgcq4VoAECapS.jpg",
        "id_str" : "964509684618076161",
        "id" : 964509684618076161,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWKgcq4VoAECapS.jpg",
        "sizes" : [ {
          "h" : 612,
          "resize" : "fit",
          "w" : 612
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 612
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 612
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 612
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/v6nT5BAmHW"
      } ],
      "hashtags" : [ {
        "text" : "LunarNewYear",
        "indices" : [ 30, 43 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964509686996328448",
    "text" : "Welcoming the Year of the Dog #LunarNewYear https:\/\/t.co\/v6nT5BAmHW",
    "id" : 964509686996328448,
    "created_at" : "2018-02-16 14:40:03 +0000",
    "user" : {
      "name" : "PEANUTS",
      "screen_name" : "Snoopy",
      "protected" : false,
      "id_str" : "245548093",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1009783805446537216\/ZrFhw7Vy_normal.jpg",
      "id" : 245548093,
      "verified" : true
    }
  },
  "id" : 964539200023523328,
  "created_at" : "2018-02-16 16:37:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 3, 13 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/KfO6bRXT2c",
      "expanded_url" : "https:\/\/www.kapwing.com\/blog\/why-i-dont-use-my-real-photo\/",
      "display_url" : "kapwing.com\/blog\/why-i-don\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "964498119609208832",
  "text" : "RT @barryhand: Why I don't use my real photo when messaging with customers on my website\u00A0-\u00A0Kapwing Blog https:\/\/t.co\/KfO6bRXT2c",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/KfO6bRXT2c",
        "expanded_url" : "https:\/\/www.kapwing.com\/blog\/why-i-dont-use-my-real-photo\/",
        "display_url" : "kapwing.com\/blog\/why-i-don\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "964430635778523136",
    "text" : "Why I don't use my real photo when messaging with customers on my website\u00A0-\u00A0Kapwing Blog https:\/\/t.co\/KfO6bRXT2c",
    "id" : 964430635778523136,
    "created_at" : "2018-02-16 09:25:56 +0000",
    "user" : {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "protected" : false,
      "id_str" : "18849187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614532770300329984\/xJWWAtSu_normal.jpg",
      "id" : 18849187,
      "verified" : false
    }
  },
  "id" : 964498119609208832,
  "created_at" : "2018-02-16 13:54:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MSN Singapore",
      "screen_name" : "MSN_Singapore",
      "indices" : [ 3, 17 ],
      "id_str" : "3196089602",
      "id" : 3196089602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964463833401217024",
  "text" : "RT @MSN_Singapore: Japanese anime blows minds with detailed depictions of landmarks in Singapore like The Merlion and Maxwell Food Centre h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/oxfordblue.azurewebsites.net\" rel=\"nofollow\"\u003EOxfordBlue-Twitter\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MSN_Singapore\/status\/961617398267756544\/photo\/1",
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/jBKpUqzAek",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVhZ7WRU8AAv3eJ.jpg",
        "id_str" : "961617396569010176",
        "id" : 961617396569010176,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVhZ7WRU8AAv3eJ.jpg",
        "sizes" : [ {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 300,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jBKpUqzAek"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/eyrzXeuU8p",
        "expanded_url" : "http:\/\/www.msn.com\/en-sg\/news\/singapore\/japanese-anime-blows-minds-with-detailed-depictions-of-landmarks-in-singapore-like-the-merlion-and-maxwell-food-centre\/ar-BBIQ8JY?ocid=ob-tw-ensg-489",
        "display_url" : "msn.com\/en-sg\/news\/sin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "961617398267756544",
    "text" : "Japanese anime blows minds with detailed depictions of landmarks in Singapore like The Merlion and Maxwell Food Centre https:\/\/t.co\/eyrzXeuU8p https:\/\/t.co\/jBKpUqzAek",
    "id" : 961617398267756544,
    "created_at" : "2018-02-08 15:07:08 +0000",
    "user" : {
      "name" : "MSN Singapore",
      "screen_name" : "MSN_Singapore",
      "protected" : false,
      "id_str" : "3196089602",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/602701463417032706\/jbu8jvht_normal.png",
      "id" : 3196089602,
      "verified" : false
    }
  },
  "id" : 964463833401217024,
  "created_at" : "2018-02-16 11:37:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 85, 101 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/kmWPRYyoQ1",
      "expanded_url" : "http:\/\/po.st\/y06FJH",
      "display_url" : "po.st\/y06FJH"
    } ]
  },
  "geo" : { },
  "id_str" : "964440864251559937",
  "text" : "Malaysia apologises for Chinese New Year ad with rooster https:\/\/t.co\/kmWPRYyoQ1 via @ChannelNewsAsia",
  "id" : 964440864251559937,
  "created_at" : "2018-02-16 10:06:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "wiccanleaks #FBPE",
      "screen_name" : "meophamister",
      "indices" : [ 3, 16 ],
      "id_str" : "757570101541478401",
      "id" : 757570101541478401
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/meophamister\/status\/964226226905866240\/photo\/1",
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/V2ZtjDeunT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWGeop6XUAEhn2U.jpg",
      "id_str" : "964226216516603905",
      "id" : 964226216516603905,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWGeop6XUAEhn2U.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 1136,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/V2ZtjDeunT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964430967459823617",
  "text" : "RT @meophamister: Retweeted without comment https:\/\/t.co\/V2ZtjDeunT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/meophamister\/status\/964226226905866240\/photo\/1",
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/V2ZtjDeunT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWGeop6XUAEhn2U.jpg",
        "id_str" : "964226216516603905",
        "id" : 964226216516603905,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWGeop6XUAEhn2U.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1136,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1136,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 1136,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/V2ZtjDeunT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964226226905866240",
    "text" : "Retweeted without comment https:\/\/t.co\/V2ZtjDeunT",
    "id" : 964226226905866240,
    "created_at" : "2018-02-15 19:53:41 +0000",
    "user" : {
      "name" : "wiccanleaks #FBPE",
      "screen_name" : "meophamister",
      "protected" : false,
      "id_str" : "757570101541478401",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/831530435704401920\/PNHV5Vjt_normal.jpg",
      "id" : 757570101541478401,
      "verified" : false
    }
  },
  "id" : 964430967459823617,
  "created_at" : "2018-02-16 09:27:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "don lavery",
      "screen_name" : "donlav",
      "indices" : [ 0, 7 ],
      "id_str" : "139837687",
      "id" : 139837687
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "964202583890931712",
  "geo" : { },
  "id_str" : "964268630883078145",
  "in_reply_to_user_id" : 139837687,
  "text" : "@donlav Yes.",
  "id" : 964268630883078145,
  "in_reply_to_status_id" : 964202583890931712,
  "created_at" : "2018-02-15 22:42:11 +0000",
  "in_reply_to_screen_name" : "donlav",
  "in_reply_to_user_id_str" : "139837687",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964267975980322816\/photo\/1",
      "indices" : [ 153, 176 ],
      "url" : "https:\/\/t.co\/TVz9WDLg87",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWHEnRYW4AEyyFm.jpg",
      "id_str" : "964267974193504257",
      "id" : 964267974193504257,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWHEnRYW4AEyyFm.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TVz9WDLg87"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 129, 152 ],
      "url" : "https:\/\/t.co\/yZ1CCrkFTJ",
      "expanded_url" : "http:\/\/ift.tt\/2C2BTfB",
      "display_url" : "ift.tt\/2C2BTfB"
    } ]
  },
  "geo" : { },
  "id_str" : "964267975980322816",
  "text" : "This temple is one of the few Buddhist monasteries in England. It going to be busy after 12am - first day of the Lunar New Year. https:\/\/t.co\/yZ1CCrkFTJ https:\/\/t.co\/TVz9WDLg87",
  "id" : 964267975980322816,
  "created_at" : "2018-02-15 22:39:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964263618584940544\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/Hy4CBwqtCB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWHApnBXcAEjl9N.jpg",
      "id_str" : "964263616315879425",
      "id" : 964263616315879425,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWHApnBXcAEjl9N.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Hy4CBwqtCB"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/erb1sETspr",
      "expanded_url" : "http:\/\/ift.tt\/2EyLKLC",
      "display_url" : "ift.tt\/2EyLKLC"
    } ]
  },
  "geo" : { },
  "id_str" : "964263618584940544",
  "text" : "Manchester Fo Guan Shan Temple https:\/\/t.co\/erb1sETspr https:\/\/t.co\/Hy4CBwqtCB",
  "id" : 964263618584940544,
  "created_at" : "2018-02-15 22:22:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 129, 152 ],
      "url" : "https:\/\/t.co\/RR3zVnkR0S",
      "expanded_url" : "https:\/\/twitter.com\/donlav\/status\/964129295185653760",
      "display_url" : "twitter.com\/donlav\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "964201966741139457",
  "text" : "As someone who handled the military version M-16 for national service, this is ture and we are not allow to switch to auto fire. https:\/\/t.co\/RR3zVnkR0S",
  "id" : 964201966741139457,
  "created_at" : "2018-02-15 18:17:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "indices" : [ 3, 7 ],
      "id_str" : "380648579",
      "id" : 380648579
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BREAKING",
      "indices" : [ 9, 18 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964196321862344705",
  "text" : "RT @AFP: #BREAKING EU agrees common defence is NATO 'mission alone': Mattis",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BREAKING",
        "indices" : [ 0, 9 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964096273342259200",
    "text" : "#BREAKING EU agrees common defence is NATO 'mission alone': Mattis",
    "id" : 964096273342259200,
    "created_at" : "2018-02-15 11:17:18 +0000",
    "user" : {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "protected" : false,
      "id_str" : "380648579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/697343883630481408\/c08JBfBB_normal.jpg",
      "id" : 380648579,
      "verified" : true
    }
  },
  "id" : 964196321862344705,
  "created_at" : "2018-02-15 17:54:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964126901316014081",
  "text" : "On this day, Singapore fall to the Japanese on Feb. 15, 1942 , the first day of Chinese New Year.",
  "id" : 964126901316014081,
  "created_at" : "2018-02-15 13:19:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mothership.sg",
      "screen_name" : "MothershipSG",
      "indices" : [ 3, 16 ],
      "id_str" : "1619325942",
      "id" : 1619325942
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/phucwCptjB",
      "expanded_url" : "http:\/\/bit.ly\/2HfTK1P",
      "display_url" : "bit.ly\/2HfTK1P"
    } ]
  },
  "geo" : { },
  "id_str" : "964119856223277057",
  "text" : "RT @MothershipSG: S\u2019pore lady decorates HDB corridor, creating awesome Chinese New Year atmosphere for neighbours: https:\/\/t.co\/phucwCptjB\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MothershipSG\/status\/963998232622739461\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/jGhf1QH3Zr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWDPNDfUMAAzAc4.jpg",
        "id_str" : "963998143439187968",
        "id" : 963998143439187968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWDPNDfUMAAzAc4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1366
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1366
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/jGhf1QH3Zr"
      } ],
      "hashtags" : [ {
        "text" : "CNY",
        "indices" : [ 121, 125 ]
      }, {
        "text" : "CNY2018",
        "indices" : [ 126, 134 ]
      } ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/phucwCptjB",
        "expanded_url" : "http:\/\/bit.ly\/2HfTK1P",
        "display_url" : "bit.ly\/2HfTK1P"
      } ]
    },
    "geo" : { },
    "id_str" : "963998232622739461",
    "text" : "S\u2019pore lady decorates HDB corridor, creating awesome Chinese New Year atmosphere for neighbours: https:\/\/t.co\/phucwCptjB #CNY #CNY2018 https:\/\/t.co\/jGhf1QH3Zr",
    "id" : 963998232622739461,
    "created_at" : "2018-02-15 04:47:43 +0000",
    "user" : {
      "name" : "Mothership.sg",
      "screen_name" : "MothershipSG",
      "protected" : false,
      "id_str" : "1619325942",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/880244811273388032\/mzvF03Lz_normal.jpg",
      "id" : 1619325942,
      "verified" : true
    }
  },
  "id" : 964119856223277057,
  "created_at" : "2018-02-15 12:51:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964118573110218752\/photo\/1",
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/qyU8OP6Cs5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWE8kdWXUAAnTbN.jpg",
      "id_str" : "964118392285384704",
      "id" : 964118392285384704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWE8kdWXUAAnTbN.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qyU8OP6Cs5"
    } ],
    "hashtags" : [ {
      "text" : "cnyeve",
      "indices" : [ 53, 60 ]
    }, {
      "text" : "CNY2018",
      "indices" : [ 61, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964118573110218752",
  "text" : "May the Year of the Dog brings health and happiness. #cnyeve #CNY2018 https:\/\/t.co\/qyU8OP6Cs5",
  "id" : 964118573110218752,
  "created_at" : "2018-02-15 12:45:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/DkOf1Klz8i",
      "expanded_url" : "https:\/\/m.facebook.com\/photo.php?fbid=10155365237187934&id=93889432933&set=a.106297597933.96699.93889432933&source=54",
      "display_url" : "m.facebook.com\/photo.php?fbid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "964109347331760128",
  "text" : "This one appears on my FB feed https:\/\/t.co\/DkOf1Klz8i",
  "id" : 964109347331760128,
  "created_at" : "2018-02-15 12:09:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "964107911810224129",
  "geo" : { },
  "id_str" : "964108007159320577",
  "in_reply_to_user_id" : 9465632,
  "text" : "Took them this long to realise?",
  "id" : 964108007159320577,
  "in_reply_to_status_id" : 964107911810224129,
  "created_at" : "2018-02-15 12:03:55 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 28, 40 ],
      "id_str" : "41085467",
      "id" : 41085467
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/6cBeIaMlrq",
      "expanded_url" : "http:\/\/www.todayonline.com\/singapore\/some-low-wage-workers-working-slaves-says-labour-mp#.WoV23NnomAc.twitter",
      "display_url" : "todayonline.com\/singapore\/some\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "964107911810224129",
  "text" : "https:\/\/t.co\/6cBeIaMlrq via @TODAYonline",
  "id" : 964107911810224129,
  "created_at" : "2018-02-15 12:03:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/964106050046750721\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/ZhoGVVw75U",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWExV1NWkAEW06-.jpg",
      "id_str" : "964106046364094465",
      "id" : 964106046364094465,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWExV1NWkAEW06-.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZhoGVVw75U"
    } ],
    "hashtags" : [ {
      "text" : "Manchester",
      "indices" : [ 38, 49 ]
    } ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/Wixpgbpm36",
      "expanded_url" : "http:\/\/ift.tt\/2Bv30yU",
      "display_url" : "ift.tt\/2Bv30yU"
    } ]
  },
  "geo" : { },
  "id_str" : "964106050046750721",
  "text" : "Man with the white shoes at the cafe. #Manchester https:\/\/t.co\/Wixpgbpm36 https:\/\/t.co\/ZhoGVVw75U",
  "id" : 964106050046750721,
  "created_at" : "2018-02-15 11:56:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ian Miell",
      "screen_name" : "ianmiell",
      "indices" : [ 3, 12 ],
      "id_str" : "58017706",
      "id" : 58017706
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964067813131456512",
  "text" : "RT @ianmiell: Grateful that I live in a country where there is a very low risk my children will be slaughtered at school.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "964050372712128512",
    "text" : "Grateful that I live in a country where there is a very low risk my children will be slaughtered at school.",
    "id" : 964050372712128512,
    "created_at" : "2018-02-15 08:14:54 +0000",
    "user" : {
      "name" : "Ian Miell",
      "screen_name" : "ianmiell",
      "protected" : false,
      "id_str" : "58017706",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/854765666267693062\/lweeA3oX_normal.jpg",
      "id" : 58017706,
      "verified" : false
    }
  },
  "id" : 964067813131456512,
  "created_at" : "2018-02-15 09:24:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "964066843613900801",
  "text" : "So where are the so call \"good guys with gun\"?",
  "id" : 964066843613900801,
  "created_at" : "2018-02-15 09:20:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "963877204613586946",
  "geo" : { },
  "id_str" : "963879004083277825",
  "in_reply_to_user_id" : 9465632,
  "text" : "The topic is about whether parent should have access to children mobile devices on demand.",
  "id" : 963879004083277825,
  "in_reply_to_status_id" : 963877204613586946,
  "created_at" : "2018-02-14 20:53:57 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963877204613586946\/photo\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/avy6ZWMu1N",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWBgq1JXkAIeRIn.jpg",
      "id_str" : "963876609194430466",
      "id" : 963876609194430466,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWBgq1JXkAIeRIn.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/avy6ZWMu1N"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963877204613586946\/photo\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/avy6ZWMu1N",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWBgq1NXcAAOyE7.jpg",
      "id_str" : "963876609211199488",
      "id" : 963876609211199488,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWBgq1NXcAAOyE7.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/avy6ZWMu1N"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963877204613586946",
  "text" : "Teenagers talk show in China with full product placement. No issues or concerns. https:\/\/t.co\/avy6ZWMu1N",
  "id" : 963877204613586946,
  "created_at" : "2018-02-14 20:46:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963810619844644864\/photo\/1",
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/hlJX1FUhVB",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DWAkppeW4AAfJvu.jpg",
      "id_str" : "963810618183704576",
      "id" : 963810618183704576,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWAkppeW4AAfJvu.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hlJX1FUhVB"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/qg0dck1RE6",
      "expanded_url" : "http:\/\/ift.tt\/2nZX0ql",
      "display_url" : "ift.tt\/2nZX0ql"
    } ]
  },
  "geo" : { },
  "id_str" : "963810619844644864",
  "text" : "Chinese News Year song https:\/\/t.co\/qg0dck1RE6 https:\/\/t.co\/hlJX1FUhVB",
  "id" : 963810619844644864,
  "created_at" : "2018-02-14 16:22:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963809802450325509",
  "text" : "You can call R from these Microsoft data products\u00A0\u00A0 -\nSQL Server (the database) \nPower BI (the reporting and visualization tool),\nVisual Studio (the integrated development environment)",
  "id" : 963809802450325509,
  "created_at" : "2018-02-14 16:18:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963799912390561792",
  "text" : "\u9C9C\u6C95\u597D\u559D\uFF0C\u4F60\u4EEC\u77E5\u9053\u4E3A\u4EC0\u4E48\u5417\uFF1F\u56E0\u4E3A\u6709\u9C7C\u53C8\u6709\u7F8A\u3002",
  "id" : 963799912390561792,
  "created_at" : "2018-02-14 15:39:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Dean",
      "screen_name" : "JeffDean",
      "indices" : [ 3, 12 ],
      "id_str" : "911297187664949248",
      "id" : 911297187664949248
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/jeiKBdh1P9",
      "expanded_url" : "http:\/\/distill.pub",
      "display_url" : "distill.pub"
    } ]
  },
  "geo" : { },
  "id_str" : "963793884034760705",
  "text" : "RT @JeffDean: https:\/\/t.co\/jeiKBdh1P9 is a new online journal dedicated to clear explanations of everything related to machine learning (if\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/jeiKBdh1P9",
        "expanded_url" : "http:\/\/distill.pub",
        "display_url" : "distill.pub"
      }, {
        "indices" : [ 281, 304 ],
        "url" : "https:\/\/t.co\/imxOswhsnt",
        "expanded_url" : "https:\/\/twitter.com\/distillpub\/status\/946110124669710337",
        "display_url" : "twitter.com\/distillpub\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "946125342615379969",
    "text" : "https:\/\/t.co\/jeiKBdh1P9 is a new online journal dedicated to clear explanations of everything related to machine learning (if you haven't checked out their articles, you should!). To incent people to produce this sort of content, they offer cash prizes for great work in this area https:\/\/t.co\/imxOswhsnt",
    "id" : 946125342615379969,
    "created_at" : "2017-12-27 21:07:14 +0000",
    "user" : {
      "name" : "Jeff Dean",
      "screen_name" : "JeffDean",
      "protected" : false,
      "id_str" : "911297187664949248",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/935325968280907776\/AcBo6zJc_normal.jpg",
      "id" : 911297187664949248,
      "verified" : true
    }
  },
  "id" : 963793884034760705,
  "created_at" : "2018-02-14 15:15:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/hdT5LlNqaz",
      "expanded_url" : "http:\/\/str.sg\/oiA7",
      "display_url" : "str.sg\/oiA7"
    } ]
  },
  "geo" : { },
  "id_str" : "963789390496624641",
  "text" : "RT @STcom: Woman refuses to leave handbag during security check, climbs into X-ray machine at China train station https:\/\/t.co\/hdT5LlNqaz h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/963591520417857541\/photo\/1",
        "indices" : [ 127, 150 ],
        "url" : "https:\/\/t.co\/M9prgLeXsp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV9dWz1VoAEGosH.jpg",
        "id_str" : "963591491733004289",
        "id" : 963591491733004289,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV9dWz1VoAEGosH.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 520,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 520,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 520,
          "resize" : "fit",
          "w" : 780
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/M9prgLeXsp"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/hdT5LlNqaz",
        "expanded_url" : "http:\/\/str.sg\/oiA7",
        "display_url" : "str.sg\/oiA7"
      } ]
    },
    "geo" : { },
    "id_str" : "963591520417857541",
    "text" : "Woman refuses to leave handbag during security check, climbs into X-ray machine at China train station https:\/\/t.co\/hdT5LlNqaz https:\/\/t.co\/M9prgLeXsp",
    "id" : 963591520417857541,
    "created_at" : "2018-02-14 01:51:35 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 963789390496624641,
  "created_at" : "2018-02-14 14:57:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DrTempest van Schaik",
      "screen_name" : "Dr_Tempest",
      "indices" : [ 3, 14 ],
      "id_str" : "2370233868",
      "id" : 2370233868
    }, {
      "name" : "Microsoft",
      "screen_name" : "Microsoft",
      "indices" : [ 34, 44 ],
      "id_str" : "74286565",
      "id" : 74286565
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SouthAfrica",
      "indices" : [ 71, 83 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963760689461547009",
  "text" : "RT @Dr_Tempest: Very pleased that @Microsoft will open data centres in #SouthAfrica later this year. Tech giants are moving in, start-up sc\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Microsoft",
        "screen_name" : "Microsoft",
        "indices" : [ 18, 28 ],
        "id_str" : "74286565",
        "id" : 74286565
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SouthAfrica",
        "indices" : [ 55, 67 ]
      }, {
        "text" : "proudlySouthAfrican",
        "indices" : [ 229, 249 ]
      } ],
      "urls" : [ {
        "indices" : [ 250, 273 ],
        "url" : "https:\/\/t.co\/GsF0z7CtTH",
        "expanded_url" : "http:\/\/fortune.com\/2017\/05\/18\/microsoft-africa-cloud-data-center\/",
        "display_url" : "fortune.com\/2017\/05\/18\/mic\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "963758691777503232",
    "text" : "Very pleased that @Microsoft will open data centres in #SouthAfrica later this year. Tech giants are moving in, start-up scene is growing, new jobs, skills &amp; home-grown solutions. The outlook is good for SA's digital economy #proudlySouthAfrican\nhttps:\/\/t.co\/GsF0z7CtTH",
    "id" : 963758691777503232,
    "created_at" : "2018-02-14 12:55:52 +0000",
    "user" : {
      "name" : "DrTempest van Schaik",
      "screen_name" : "Dr_Tempest",
      "protected" : false,
      "id_str" : "2370233868",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037376811141922816\/vC3N7BcB_normal.jpg",
      "id" : 2370233868,
      "verified" : false
    }
  },
  "id" : 963760689461547009,
  "created_at" : "2018-02-14 13:03:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "indices" : [ 3, 14 ],
      "id_str" : "2870653293",
      "id" : 2870653293
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963756366430130177",
  "text" : "RT @susie_dent: Firkytoodling: a nice slice of Victorian slang for kissing, cuddling and a little bit more besides.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963741632238583808",
    "text" : "Firkytoodling: a nice slice of Victorian slang for kissing, cuddling and a little bit more besides.",
    "id" : 963741632238583808,
    "created_at" : "2018-02-14 11:48:05 +0000",
    "user" : {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "protected" : false,
      "id_str" : "2870653293",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531825590878228480\/4s2F9F6W_normal.jpeg",
      "id" : 2870653293,
      "verified" : true
    }
  },
  "id" : 963756366430130177,
  "created_at" : "2018-02-14 12:46:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963711313456320512\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/iJlUAnqdez",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV_J7YHWsAASIr8.jpg",
      "id_str" : "963710867203338240",
      "id" : 963710867203338240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV_J7YHWsAASIr8.jpg",
      "sizes" : [ {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/iJlUAnqdez"
    } ],
    "hashtags" : [ {
      "text" : "Olympics",
      "indices" : [ 63, 72 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963711313456320512",
  "text" : "United Korea vs Japan. Political connotation at so many levels.#Olympics https:\/\/t.co\/iJlUAnqdez",
  "id" : 963711313456320512,
  "created_at" : "2018-02-14 09:47:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maria Falaschi",
      "screen_name" : "mfalaschi",
      "indices" : [ 3, 13 ],
      "id_str" : "24478021",
      "id" : 24478021
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/hjCvrJ9VwV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zVsDVQAE1Hoh.jpg",
      "id_str" : "963545292975980545",
      "id" : 963545292975980545,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zVsDVQAE1Hoh.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/hjCvrJ9VwV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zW3WUQAApoLk.jpg",
      "id_str" : "963545313188265984",
      "id" : 963545313188265984,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zW3WUQAApoLk.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/hjCvrJ9VwV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zs--UMAAa67J.jpg",
      "id_str" : "963545693192204288",
      "id" : 963545693192204288,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zs--UMAAa67J.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/hjCvrJ9VwV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zuC8VoAA5nM5.jpg",
      "id_str" : "963545711437520896",
      "id" : 963545711437520896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zuC8VoAA5nM5.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1152,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
    } ],
    "hashtags" : [ {
      "text" : "ua1175",
      "indices" : [ 42, 49 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963705919497437184",
  "text" : "RT @mfalaschi: Scariest flight of my life #ua1175 https:\/\/t.co\/hjCvrJ9VwV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/hjCvrJ9VwV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zVsDVQAE1Hoh.jpg",
        "id_str" : "963545292975980545",
        "id" : 963545292975980545,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zVsDVQAE1Hoh.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/hjCvrJ9VwV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zW3WUQAApoLk.jpg",
        "id_str" : "963545313188265984",
        "id" : 963545313188265984,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zW3WUQAApoLk.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/hjCvrJ9VwV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zs--UMAAa67J.jpg",
        "id_str" : "963545693192204288",
        "id" : 963545693192204288,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zs--UMAAa67J.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/mfalaschi\/status\/963545726654361600\/photo\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/hjCvrJ9VwV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8zuC8VoAA5nM5.jpg",
        "id_str" : "963545711437520896",
        "id" : 963545711437520896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8zuC8VoAA5nM5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1152,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/hjCvrJ9VwV"
      } ],
      "hashtags" : [ {
        "text" : "ua1175",
        "indices" : [ 27, 34 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963545726654361600",
    "text" : "Scariest flight of my life #ua1175 https:\/\/t.co\/hjCvrJ9VwV",
    "id" : 963545726654361600,
    "created_at" : "2018-02-13 22:49:37 +0000",
    "user" : {
      "name" : "Maria Falaschi",
      "screen_name" : "mfalaschi",
      "protected" : false,
      "id_str" : "24478021",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/524255852434190338\/CX8VMZ15_normal.jpeg",
      "id" : 24478021,
      "verified" : false
    }
  },
  "id" : 963705919497437184,
  "created_at" : "2018-02-14 09:26:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "German Embassy London",
      "screen_name" : "GermanEmbassy",
      "indices" : [ 3, 17 ],
      "id_str" : "134793154",
      "id" : 134793154
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963690383778762752",
  "text" : "RT @GermanEmbassy: Every year on this day, a wreath is laid anonymously outside the German Embassy in memory of the bombing of Dresden. \n\nW\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GermanEmbassy\/status\/963392355423354880\/photo\/1",
        "indices" : [ 182, 205 ],
        "url" : "https:\/\/t.co\/0j1OOIHlC8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV6oNv-XUAACDoX.jpg",
        "id_str" : "963392324473671680",
        "id" : 963392324473671680,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV6oNv-XUAACDoX.jpg",
        "sizes" : [ {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0j1OOIHlC8"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963392355423354880",
    "text" : "Every year on this day, a wreath is laid anonymously outside the German Embassy in memory of the bombing of Dresden. \n\nWe are deeply grateful for this kind gesture of reconciliation https:\/\/t.co\/0j1OOIHlC8",
    "id" : 963392355423354880,
    "created_at" : "2018-02-13 12:40:11 +0000",
    "user" : {
      "name" : "German Embassy London",
      "screen_name" : "GermanEmbassy",
      "protected" : false,
      "id_str" : "134793154",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/817000999604932608\/VxExtVEr_normal.jpg",
      "id" : 134793154,
      "verified" : true
    }
  },
  "id" : 963690383778762752,
  "created_at" : "2018-02-14 08:24:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "indices" : [ 3, 12 ],
      "id_str" : "130745589",
      "id" : 130745589
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963689985068105730",
  "text" : "RT @drfeifei: \u201CI often tell my students not to be misled by the name \u2018artificial  intelligence\u2019 \u2014 there is nothing artificial about it. A.I\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Cade Metz",
        "screen_name" : "CadeMetz",
        "indices" : [ 243, 252 ],
        "id_str" : "21066583",
        "id" : 21066583
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 253, 276 ],
        "url" : "https:\/\/t.co\/QbB8IGCPf5",
        "expanded_url" : "https:\/\/nyti.ms\/2BWhGZ1",
        "display_url" : "nyti.ms\/2BWhGZ1"
      } ]
    },
    "geo" : { },
    "id_str" : "963564896225918976",
    "text" : "\u201CI often tell my students not to be misled by the name \u2018artificial  intelligence\u2019 \u2014 there is nothing artificial about it. A.I. is made by humans, intended to behave by humans and, ultimately, to impact humans  lives and human society.\u201D Thanks @CadeMetz https:\/\/t.co\/QbB8IGCPf5",
    "id" : 963564896225918976,
    "created_at" : "2018-02-14 00:05:48 +0000",
    "user" : {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "protected" : false,
      "id_str" : "130745589",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841385099799085056\/R1iX4QGX_normal.jpg",
      "id" : 130745589,
      "verified" : false
    }
  },
  "id" : 963689985068105730,
  "created_at" : "2018-02-14 08:22:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "indices" : [ 3, 12 ],
      "id_str" : "11503282",
      "id" : 11503282
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "spacetech",
      "indices" : [ 76, 86 ]
    }, {
      "text" : "SpaceGeek",
      "indices" : [ 117, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963688799950786560",
  "text" : "RT @ladyxtel: Amazing. There\u2019s a space technology fund and a couple of cool #spacetech companies mentioned here too  #SpaceGeek. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "spacetech",
        "indices" : [ 62, 72 ]
      }, {
        "text" : "SpaceGeek",
        "indices" : [ 103, 113 ]
      } ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/fmrEajgp0M",
        "expanded_url" : "https:\/\/twitter.com\/michaeltegos\/status\/963230591574396929",
        "display_url" : "twitter.com\/michaeltegos\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "963618858144751616",
    "text" : "Amazing. There\u2019s a space technology fund and a couple of cool #spacetech companies mentioned here too  #SpaceGeek. https:\/\/t.co\/fmrEajgp0M",
    "id" : 963618858144751616,
    "created_at" : "2018-02-14 03:40:13 +0000",
    "user" : {
      "name" : "christel quek",
      "screen_name" : "ladyxtel",
      "protected" : false,
      "id_str" : "11503282",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849872363671830528\/5iExzLy9_normal.jpg",
      "id" : 11503282,
      "verified" : true
    }
  },
  "id" : 963688799950786560,
  "created_at" : "2018-02-14 08:18:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963507850717941760\/photo\/1",
      "indices" : [ 121, 144 ],
      "url" : "https:\/\/t.co\/mKM7WQIrhT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV8Q4j1W0AAdKrA.jpg",
      "id_str" : "963507409158328320",
      "id" : 963507409158328320,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV8Q4j1W0AAdKrA.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mKM7WQIrhT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963507850717941760",
  "text" : "Is it tech giants responsibility to do this? Identify target audience to serve them relevant ads is different ball game. https:\/\/t.co\/mKM7WQIrhT",
  "id" : 963507850717941760,
  "created_at" : "2018-02-13 20:19:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "179569408",
      "id" : 179569408
    }, {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 81, 95 ],
      "id_str" : "179569408",
      "id" : 179569408
    }, {
      "name" : "Cathay Pacific UK & IE",
      "screen_name" : "cathaypacificUK",
      "indices" : [ 115, 131 ],
      "id_str" : "37588890",
      "id" : 37588890
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963470604342124545",
  "text" : "RT @DublinAirport: To celebrate the new four times weekly service this June from @DublinAirport  to Hong Kong with @cathaypacificUK, we\u2019ve\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dublin Airport",
        "screen_name" : "DublinAirport",
        "indices" : [ 62, 76 ],
        "id_str" : "179569408",
        "id" : 179569408
      }, {
        "name" : "Cathay Pacific UK & IE",
        "screen_name" : "cathaypacificUK",
        "indices" : [ 96, 112 ],
        "id_str" : "37588890",
        "id" : 37588890
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DublinAirport\/status\/963395151577088000\/photo\/1",
        "indices" : [ 275, 298 ],
        "url" : "https:\/\/t.co\/sbTsRusOUl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV6qyGMVMAAn_Rh.jpg",
        "id_str" : "963395147936378880",
        "id" : 963395147936378880,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV6qyGMVMAAn_Rh.jpg",
        "sizes" : [ {
          "h" : 667,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 667,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 667,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sbTsRusOUl"
      } ],
      "hashtags" : [ {
        "text" : "DUBCathayPacific",
        "indices" : [ 257, 274 ]
      } ],
      "urls" : [ {
        "indices" : [ 233, 256 ],
        "url" : "https:\/\/t.co\/Pj9f8qxtH2",
        "expanded_url" : "http:\/\/ow.ly\/trTG30in5Xr",
        "display_url" : "ow.ly\/trTG30in5Xr"
      } ]
    },
    "geo" : { },
    "id_str" : "963395151577088000",
    "text" : "To celebrate the new four times weekly service this June from @DublinAirport  to Hong Kong with @cathaypacificUK, we\u2019ve got two return flights to give away. Just follow us &amp; retweet a competition tweet to enter. Find out more at https:\/\/t.co\/Pj9f8qxtH2 #DUBCathayPacific https:\/\/t.co\/sbTsRusOUl",
    "id" : 963395151577088000,
    "created_at" : "2018-02-13 12:51:17 +0000",
    "user" : {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "protected" : false,
      "id_str" : "179569408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013556776321650694\/bdnXxxoU_normal.jpg",
      "id" : 179569408,
      "verified" : true
    }
  },
  "id" : 963470604342124545,
  "created_at" : "2018-02-13 17:51:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963470059904659457\/photo\/1",
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/zEDkQBWsRc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV7u6YXWkAA9tSa.jpg",
      "id_str" : "963470057044086784",
      "id" : 963470057044086784,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV7u6YXWkAA9tSa.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/zEDkQBWsRc"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 31 ],
      "url" : "https:\/\/t.co\/v2klZyRKvZ",
      "expanded_url" : "http:\/\/ift.tt\/2F4jBcJ",
      "display_url" : "ift.tt\/2F4jBcJ"
    } ]
  },
  "geo" : { },
  "id_str" : "963470059904659457",
  "text" : "Arrived https:\/\/t.co\/v2klZyRKvZ https:\/\/t.co\/zEDkQBWsRc",
  "id" : 963470059904659457,
  "created_at" : "2018-02-13 17:48:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/963366162871603201\/photo\/1",
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/LjZ6WtqkGC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV6Qa14WsAAIYJ9.jpg",
      "id_str" : "963366161118310400",
      "id" : 963366161118310400,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV6Qa14WsAAIYJ9.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/LjZ6WtqkGC"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/oMa08PsjZM",
      "expanded_url" : "http:\/\/ift.tt\/2BqaOBW",
      "display_url" : "ift.tt\/2BqaOBW"
    } ]
  },
  "geo" : { },
  "id_str" : "963366162871603201",
  "text" : "Lunar New Year candies and cookie. https:\/\/t.co\/oMa08PsjZM https:\/\/t.co\/LjZ6WtqkGC",
  "id" : 963366162871603201,
  "created_at" : "2018-02-13 10:56:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Borrys Hasian",
      "screen_name" : "borryshasian",
      "indices" : [ 3, 16 ],
      "id_str" : "28290909",
      "id" : 28290909
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963315020426891264",
  "text" : "RT @borryshasian: Designer, or anyone, should stop putting these meaningless bars on your resume. What does almost full bar mean? And 1\/6 b\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/borryshasian\/status\/963017645304905728\/photo\/1",
        "indices" : [ 170, 193 ],
        "url" : "https:\/\/t.co\/pE6CqRTlWW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV1Ta-3VAAAEiSt.jpg",
        "id_str" : "963017618343919616",
        "id" : 963017618343919616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV1Ta-3VAAAEiSt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/pE6CqRTlWW"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963017645304905728",
    "text" : "Designer, or anyone, should stop putting these meaningless bars on your resume. What does almost full bar mean? And 1\/6 bar? Instead, show your skills through your work. https:\/\/t.co\/pE6CqRTlWW",
    "id" : 963017645304905728,
    "created_at" : "2018-02-12 11:51:13 +0000",
    "user" : {
      "name" : "Borrys Hasian",
      "screen_name" : "borryshasian",
      "protected" : false,
      "id_str" : "28290909",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013761573490343936\/kJMyE7Bn_normal.jpg",
      "id" : 28290909,
      "verified" : false
    }
  },
  "id" : 963315020426891264,
  "created_at" : "2018-02-13 07:32:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Peters",
      "screen_name" : "tom_peters",
      "indices" : [ 3, 14 ],
      "id_str" : "18028509",
      "id" : 18028509
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963314641588846592",
  "text" : "RT @tom_peters: \"Speed is everything in 2018\" total bullshit:\nRelationships take time. \nRecruiting allies to your cause takes time.\nReading\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963145102389534721",
    "text" : "\"Speed is everything in 2018\" total bullshit:\nRelationships take time. \nRecruiting allies to your cause takes time.\nReading\/studying take time.\nPractice &amp; prep take time.\nMBWA takes time.\nThoughtfulness\/small gestures take time. \n\"The last 10%\" takes time.\nEXCELLENCE takes time.",
    "id" : 963145102389534721,
    "created_at" : "2018-02-12 20:17:41 +0000",
    "user" : {
      "name" : "Tom Peters",
      "screen_name" : "tom_peters",
      "protected" : false,
      "id_str" : "18028509",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/509454796042928128\/Db2nYm-L_normal.jpeg",
      "id" : 18028509,
      "verified" : true
    }
  },
  "id" : 963314641588846592,
  "created_at" : "2018-02-13 07:31:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "indices" : [ 3, 19 ],
      "id_str" : "22700048",
      "id" : 22700048
    }, {
      "name" : "Morning Ireland",
      "screen_name" : "morningireland",
      "indices" : [ 66, 81 ],
      "id_str" : "22790104",
      "id" : 22790104
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963314289800024064",
  "text" : "RT @EimearMcCormack: What\u2019s the data based on? 1 bed, 2 bed ... ? @morningireland",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Morning Ireland",
        "screen_name" : "morningireland",
        "indices" : [ 45, 60 ],
        "id_str" : "22790104",
        "id" : 22790104
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963310888144031744",
    "text" : "What\u2019s the data based on? 1 bed, 2 bed ... ? @morningireland",
    "id" : 963310888144031744,
    "created_at" : "2018-02-13 07:16:27 +0000",
    "user" : {
      "name" : "Eimear McCormack \u26F5\uFE0F",
      "screen_name" : "EimearMcCormack",
      "protected" : false,
      "id_str" : "22700048",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007226705583501313\/CwZLRyZa_normal.jpg",
      "id" : 22700048,
      "verified" : false
    }
  },
  "id" : 963314289800024064,
  "created_at" : "2018-02-13 07:29:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/AmuMM6iZv0",
      "expanded_url" : "https:\/\/www.theguardian.com\/us-news\/2018\/feb\/12\/stephen-mader-west-virginia-police-officer-settles-lawsuit?CMP=share_btn_tw",
      "display_url" : "theguardian.com\/us-news\/2018\/f\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "963200696542814208",
  "text" : "Police officer fired for not shooting black suspect wins $175,000 https:\/\/t.co\/AmuMM6iZv0",
  "id" : 963200696542814208,
  "created_at" : "2018-02-12 23:58:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vala Afshar",
      "screen_name" : "ValaAfshar",
      "indices" : [ 3, 14 ],
      "id_str" : "259725229",
      "id" : 259725229
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/907602259915272192\/video\/1",
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/G0eGan83BJ",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/907593164562853888\/pu\/img\/AjgIDv22AM_YGwKA.jpg",
      "id_str" : "907593164562853888",
      "id" : 907593164562853888,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/907593164562853888\/pu\/img\/AjgIDv22AM_YGwKA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/G0eGan83BJ"
    } ],
    "hashtags" : [ {
      "text" : "WEF18",
      "indices" : [ 68, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963194078186950656",
  "text" : "RT @ValaAfshar: India is building roads from recycled plastic waste #WEF18 https:\/\/t.co\/G0eGan83BJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/907602259915272192\/video\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/G0eGan83BJ",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/907593164562853888\/pu\/img\/AjgIDv22AM_YGwKA.jpg",
        "id_str" : "907593164562853888",
        "id" : 907593164562853888,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/907593164562853888\/pu\/img\/AjgIDv22AM_YGwKA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/G0eGan83BJ"
      } ],
      "hashtags" : [ {
        "text" : "WEF18",
        "indices" : [ 52, 58 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "955487748424650753",
    "text" : "India is building roads from recycled plastic waste #WEF18 https:\/\/t.co\/G0eGan83BJ",
    "id" : 955487748424650753,
    "created_at" : "2018-01-22 17:10:05 +0000",
    "user" : {
      "name" : "Vala Afshar",
      "screen_name" : "ValaAfshar",
      "protected" : false,
      "id_str" : "259725229",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1259558245\/vala_300dpi_normal.jpg",
      "id" : 259725229,
      "verified" : true
    }
  },
  "id" : 963194078186950656,
  "created_at" : "2018-02-12 23:32:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "juan",
      "screen_name" : "juanbuis",
      "indices" : [ 3, 12 ],
      "id_str" : "36672594",
      "id" : 36672594
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/juanbuis\/status\/962733158042161152\/photo\/1",
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/2ofnSRZiMF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVxQs83XcAAONRd.jpg",
      "id_str" : "962733153533259776",
      "id" : 962733153533259776,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVxQs83XcAAONRd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 1566
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 1566
      }, {
        "h" : 345,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 608,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2ofnSRZiMF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "963106714391281666",
  "text" : "RT @juanbuis: well i'm sold https:\/\/t.co\/2ofnSRZiMF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/hy.perl.ink\" rel=\"nofollow\"\u003ETweet Tray\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/juanbuis\/status\/962733158042161152\/photo\/1",
        "indices" : [ 14, 37 ],
        "url" : "https:\/\/t.co\/2ofnSRZiMF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVxQs83XcAAONRd.jpg",
        "id_str" : "962733153533259776",
        "id" : 962733153533259776,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVxQs83XcAAONRd.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 794,
          "resize" : "fit",
          "w" : 1566
        }, {
          "h" : 794,
          "resize" : "fit",
          "w" : 1566
        }, {
          "h" : 345,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 608,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2ofnSRZiMF"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "962733158042161152",
    "text" : "well i'm sold https:\/\/t.co\/2ofnSRZiMF",
    "id" : 962733158042161152,
    "created_at" : "2018-02-11 17:00:46 +0000",
    "user" : {
      "name" : "juan",
      "screen_name" : "juanbuis",
      "protected" : false,
      "id_str" : "36672594",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/757924077629743105\/D-8v_kZh_normal.jpg",
      "id" : 36672594,
      "verified" : true
    }
  },
  "id" : 963106714391281666,
  "created_at" : "2018-02-12 17:45:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mcc",
      "screen_name" : "mcclure111",
      "indices" : [ 3, 14 ],
      "id_str" : "312426579",
      "id" : 312426579
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mcclure111\/status\/960888817044606977\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/lD1C221CaA",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVXDR-QU0AAL1sb.jpg",
      "id_str" : "960888809050132480",
      "id" : 960888809050132480,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVXDR-QU0AAL1sb.jpg",
      "sizes" : [ {
        "h" : 597,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 597,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 597,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 597,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/lD1C221CaA"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962759865914576896",
  "text" : "RT @mcclure111: SILICON VALLEY CONTINUES TO INNOVATE https:\/\/t.co\/lD1C221CaA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mcclure111\/status\/960888817044606977\/photo\/1",
        "indices" : [ 37, 60 ],
        "url" : "https:\/\/t.co\/lD1C221CaA",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVXDR-QU0AAL1sb.jpg",
        "id_str" : "960888809050132480",
        "id" : 960888809050132480,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVXDR-QU0AAL1sb.jpg",
        "sizes" : [ {
          "h" : 597,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/lD1C221CaA"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "960888817044606977",
    "text" : "SILICON VALLEY CONTINUES TO INNOVATE https:\/\/t.co\/lD1C221CaA",
    "id" : 960888817044606977,
    "created_at" : "2018-02-06 14:52:01 +0000",
    "user" : {
      "name" : "mcc",
      "screen_name" : "mcclure111",
      "protected" : false,
      "id_str" : "312426579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/852519442047225857\/juzc1JeR_normal.jpg",
      "id" : 312426579,
      "verified" : false
    }
  },
  "id" : 962759865914576896,
  "created_at" : "2018-02-11 18:46:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 3, 11 ],
      "id_str" : "1652541",
      "id" : 1652541
    }, {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 68, 76 ],
      "id_str" : "1652541",
      "id" : 1652541
    }, {
      "name" : "Reuters Investigates",
      "screen_name" : "specialreports",
      "indices" : [ 89, 104 ],
      "id_str" : "372862603",
      "id" : 372862603
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/yDSyvfD96X",
      "expanded_url" : "http:\/\/reut.rs\/2Ead8LO",
      "display_url" : "reut.rs\/2Ead8LO"
    } ]
  },
  "geo" : { },
  "id_str" : "962754817922949120",
  "text" : "RT @Reuters: America is the world\u2019s leading exporter of body parts, @Reuters found. Read @specialreports: \n https:\/\/t.co\/yDSyvfD96X https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Reuters Top News",
        "screen_name" : "Reuters",
        "indices" : [ 55, 63 ],
        "id_str" : "1652541",
        "id" : 1652541
      }, {
        "name" : "Reuters Investigates",
        "screen_name" : "specialreports",
        "indices" : [ 76, 91 ],
        "id_str" : "372862603",
        "id" : 372862603
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Reuters\/status\/962742333191188487\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/MxP1CSlwzK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVxZC_OWAAAi8Tk.jpg",
        "id_str" : "962742328216649728",
        "id" : 962742328216649728,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVxZC_OWAAAi8Tk.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 596,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1795,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1052,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2580,
          "resize" : "fit",
          "w" : 2944
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MxP1CSlwzK"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/yDSyvfD96X",
        "expanded_url" : "http:\/\/reut.rs\/2Ead8LO",
        "display_url" : "reut.rs\/2Ead8LO"
      } ]
    },
    "geo" : { },
    "id_str" : "962742333191188487",
    "text" : "America is the world\u2019s leading exporter of body parts, @Reuters found. Read @specialreports: \n https:\/\/t.co\/yDSyvfD96X https:\/\/t.co\/MxP1CSlwzK",
    "id" : 962742333191188487,
    "created_at" : "2018-02-11 17:37:13 +0000",
    "user" : {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "protected" : false,
      "id_str" : "1652541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877554927932891136\/ZBEs235N_normal.jpg",
      "id" : 1652541,
      "verified" : true
    }
  },
  "id" : 962754817922949120,
  "created_at" : "2018-02-11 18:26:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 14, 37 ],
      "url" : "https:\/\/t.co\/Dv3wPXN5aw",
      "expanded_url" : "https:\/\/drive.google.com\/file\/d\/1GY97vWBKpbQUu9T5NFmgMekdXYjmKQln\/view?usp=drivesdk",
      "display_url" : "drive.google.com\/file\/d\/1GY97vW\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "962747233086181377",
  "text" : "Listening to \nhttps:\/\/t.co\/Dv3wPXN5aw while getting ready for Lunar New Year",
  "id" : 962747233086181377,
  "created_at" : "2018-02-11 17:56:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "indices" : [ 3, 15 ],
      "id_str" : "3760488922",
      "id" : 3760488922
    }, {
      "name" : "Spot",
      "screen_name" : "talk2spot",
      "indices" : [ 17, 27 ],
      "id_str" : "959248957988782080",
      "id" : 959248957988782080
    }, {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 28, 35 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/drjuliashaw\/status\/962709753272160256\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/Vz26PtRqHI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVw7aeKWkAAGZcR.jpg",
      "id_str" : "962709746309566464",
      "id" : 962709746309566464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVw7aeKWkAAGZcR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 943,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 943,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 943,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 541
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Vz26PtRqHI"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962731635627188224",
  "text" : "RT @drjuliashaw: @talk2spot @Forbes  https:\/\/t.co\/Vz26PtRqHI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Spot",
        "screen_name" : "talk2spot",
        "indices" : [ 0, 10 ],
        "id_str" : "959248957988782080",
        "id" : 959248957988782080
      }, {
        "name" : "Forbes",
        "screen_name" : "Forbes",
        "indices" : [ 11, 18 ],
        "id_str" : "91478624",
        "id" : 91478624
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/drjuliashaw\/status\/962709753272160256\/photo\/1",
        "indices" : [ 20, 43 ],
        "url" : "https:\/\/t.co\/Vz26PtRqHI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVw7aeKWkAAGZcR.jpg",
        "id_str" : "962709746309566464",
        "id" : 962709746309566464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVw7aeKWkAAGZcR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 541
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Vz26PtRqHI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "962701532293730305",
    "geo" : { },
    "id_str" : "962709753272160256",
    "in_reply_to_user_id" : 3760488922,
    "text" : "@talk2spot @Forbes  https:\/\/t.co\/Vz26PtRqHI",
    "id" : 962709753272160256,
    "in_reply_to_status_id" : 962701532293730305,
    "created_at" : "2018-02-11 15:27:46 +0000",
    "in_reply_to_screen_name" : "drjuliashaw",
    "in_reply_to_user_id_str" : "3760488922",
    "user" : {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "protected" : false,
      "id_str" : "3760488922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989936592591650817\/TRkR3cmy_normal.jpg",
      "id" : 3760488922,
      "verified" : true
    }
  },
  "id" : 962731635627188224,
  "created_at" : "2018-02-11 16:54:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryanair",
      "screen_name" : "Ryanair",
      "indices" : [ 34, 42 ],
      "id_str" : "1542862735",
      "id" : 1542862735
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/962640654034718720\/photo\/1",
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/FIyORWI8eC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVv7kzOW0AE5DFF.jpg",
      "id_str" : "962639555018018817",
      "id" : 962639555018018817,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVv7kzOW0AE5DFF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FIyORWI8eC"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962640654034718720",
  "text" : "Family travel in a group of 3 and @ryanair split us up so they can up-sell the \"sit-together\" option. https:\/\/t.co\/FIyORWI8eC",
  "id" : 962640654034718720,
  "created_at" : "2018-02-11 10:53:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962633508249391104",
  "text" : "Got to Google for the airline FAQ section. Just can't find it on their own website.",
  "id" : 962633508249391104,
  "created_at" : "2018-02-11 10:24:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "I Am Devloper",
      "screen_name" : "iamdevloper",
      "indices" : [ 3, 15 ],
      "id_str" : "564919357",
      "id" : 564919357
    }, {
      "name" : "HERE Technologies",
      "screen_name" : "here",
      "indices" : [ 65, 70 ],
      "id_str" : "702586508",
      "id" : 702586508
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "general",
      "indices" : [ 78, 86 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962405846297858048",
  "text" : "RT @iamdevloper: People not answering your email in the office?\n\n@here in the #general channel in Slack and announce there's leftover dough\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "HERE Technologies",
        "screen_name" : "here",
        "indices" : [ 48, 53 ],
        "id_str" : "702586508",
        "id" : 702586508
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "general",
        "indices" : [ 61, 69 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "959076171056013312",
    "text" : "People not answering your email in the office?\n\n@here in the #general channel in Slack and announce there's leftover doughnuts by your desk.",
    "id" : 959076171056013312,
    "created_at" : "2018-02-01 14:49:12 +0000",
    "user" : {
      "name" : "I Am Devloper",
      "screen_name" : "iamdevloper",
      "protected" : false,
      "id_str" : "564919357",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/477397164453527552\/uh2w1u1o_normal.jpeg",
      "id" : 564919357,
      "verified" : false
    }
  },
  "id" : 962405846297858048,
  "created_at" : "2018-02-10 19:20:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962403695936798721",
  "text" : "RT @getoptimise: How Google and Apple solve one of photography\u2019s biggest problems? : what do I do with my massive photo archive other than,\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/JXOLX3URN4",
        "expanded_url" : "https:\/\/thenextweb.com\/google\/2018\/02\/09\/how-google-and-apple-solve-one-of-photographys-biggest-problems\/",
        "display_url" : "thenextweb.com\/google\/2018\/02\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "962402875329703937",
    "text" : "How Google and Apple solve one of photography\u2019s biggest problems? : what do I do with my massive photo archive other than, uh, archive it? https:\/\/t.co\/JXOLX3URN4",
    "id" : 962402875329703937,
    "created_at" : "2018-02-10 19:08:20 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 962403695936798721,
  "created_at" : "2018-02-10 19:11:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Faithkeeper \u63A8\u7279\u8C08\u5175",
      "screen_name" : "tpenghui",
      "indices" : [ 3, 12 ],
      "id_str" : "762668853834883073",
      "id" : 762668853834883073
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SingaporeAirshow2018",
      "indices" : [ 73, 94 ]
    }, {
      "text" : "SingaporeAirshow",
      "indices" : [ 95, 112 ]
    }, {
      "text" : "SGAirshow",
      "indices" : [ 113, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962382317586182150",
  "text" : "RT @tpenghui: Royal Malaysian Air Force SU-30MKM Fighter's \"Tailslide\" @ #SingaporeAirshow2018 #SingaporeAirshow #SGAirshow https:\/\/t.co\/S3\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tpenghui\/status\/961192766045745153\/video\/1",
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/S35NTwEDCI",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/961192468141154304\/pu\/img\/zlTt09RO5vdHz2nV.jpg",
        "id_str" : "961192468141154304",
        "id" : 961192468141154304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/961192468141154304\/pu\/img\/zlTt09RO5vdHz2nV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/S35NTwEDCI"
      } ],
      "hashtags" : [ {
        "text" : "SingaporeAirshow2018",
        "indices" : [ 59, 80 ]
      }, {
        "text" : "SingaporeAirshow",
        "indices" : [ 81, 98 ]
      }, {
        "text" : "SGAirshow",
        "indices" : [ 99, 109 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "961192766045745153",
    "text" : "Royal Malaysian Air Force SU-30MKM Fighter's \"Tailslide\" @ #SingaporeAirshow2018 #SingaporeAirshow #SGAirshow https:\/\/t.co\/S35NTwEDCI",
    "id" : 961192766045745153,
    "created_at" : "2018-02-07 10:59:48 +0000",
    "user" : {
      "name" : "Faithkeeper \u63A8\u7279\u8C08\u5175",
      "screen_name" : "tpenghui",
      "protected" : false,
      "id_str" : "762668853834883073",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/764317431837790208\/fydlf0p8_normal.jpg",
      "id" : 762668853834883073,
      "verified" : false
    }
  },
  "id" : 962382317586182150,
  "created_at" : "2018-02-10 17:46:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 122, 145 ],
      "url" : "https:\/\/t.co\/9d2QiaFSMG",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/962308292293939200",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "962380015240400896",
  "text" : "Need to prioritise. Only work on it if there is enough interest. If not is an indication that not a problem to be solved. https:\/\/t.co\/9d2QiaFSMG",
  "id" : 962380015240400896,
  "created_at" : "2018-02-10 17:37:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 130, 153 ],
      "url" : "https:\/\/t.co\/P7wwkYxZw4",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/962046480658780161",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "962308292293939200",
  "text" : "What do I need to pick up so I can turn the form into a chrome extension and work as overlay when user log into Google Analytics? https:\/\/t.co\/P7wwkYxZw4",
  "id" : 962308292293939200,
  "created_at" : "2018-02-10 12:52:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GDPR",
      "indices" : [ 36, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962307368922075136",
  "text" : "RT @EmeraldDeLeeuw: Self-proclaimed #GDPR experts \uD83E\uDD13should be banned from using the internet and should lose their right to free speech. #eu\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GDPR",
        "indices" : [ 16, 21 ]
      }, {
        "text" : "eudatap",
        "indices" : [ 116, 124 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "962303385201446912",
    "text" : "Self-proclaimed #GDPR experts \uD83E\uDD13should be banned from using the internet and should lose their right to free speech. #eudatap",
    "id" : 962303385201446912,
    "created_at" : "2018-02-10 12:33:00 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 962307368922075136,
  "created_at" : "2018-02-10 12:48:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 3, 18 ],
      "id_str" : "22997944",
      "id" : 22997944
    }, {
      "name" : "whykay \uD83D\uDC69\uD83C\uDFFB\u200D\uD83D\uDCBB\uD83D\uDC08\uD83C\uDF08",
      "screen_name" : "whykay",
      "indices" : [ 34, 41 ],
      "id_str" : "75483",
      "id" : 75483
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/ZMiSaWuGJv",
      "expanded_url" : "https:\/\/twitter.com\/techlimerick\/status\/962090583597764609",
      "display_url" : "twitter.com\/techlimerick\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "962299990285701121",
  "text" : "RT @paulinesargent: Great article @whykay :-) https:\/\/t.co\/ZMiSaWuGJv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "whykay \uD83D\uDC69\uD83C\uDFFB\u200D\uD83D\uDCBB\uD83D\uDC08\uD83C\uDF08",
        "screen_name" : "whykay",
        "indices" : [ 14, 21 ],
        "id_str" : "75483",
        "id" : 75483
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/ZMiSaWuGJv",
        "expanded_url" : "https:\/\/twitter.com\/techlimerick\/status\/962090583597764609",
        "display_url" : "twitter.com\/techlimerick\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "962293867168108545",
    "text" : "Great article @whykay :-) https:\/\/t.co\/ZMiSaWuGJv",
    "id" : 962293867168108545,
    "created_at" : "2018-02-10 11:55:11 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "protected" : false,
      "id_str" : "22997944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002264625130409984\/wlpdomJK_normal.jpg",
      "id" : 22997944,
      "verified" : false
    }
  },
  "id" : 962299990285701121,
  "created_at" : "2018-02-10 12:19:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/962295933022232576\/photo\/1",
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/u8iQ5DcQ1h",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVrDDP_XUAAFgVN.jpg",
      "id_str" : "962295930996346880",
      "id" : 962295930996346880,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVrDDP_XUAAFgVN.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/u8iQ5DcQ1h"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/mGnpxTONVn",
      "expanded_url" : "http:\/\/ift.tt\/2G3aEj2",
      "display_url" : "ift.tt\/2G3aEj2"
    } ]
  },
  "geo" : { },
  "id_str" : "962295933022232576",
  "text" : "Match Day https:\/\/t.co\/mGnpxTONVn https:\/\/t.co\/u8iQ5DcQ1h",
  "id" : 962295933022232576,
  "created_at" : "2018-02-10 12:03:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 3, 11 ],
      "id_str" : "1652541",
      "id" : 1652541
    }, {
      "name" : "Brenda Goh",
      "screen_name" : "brendagoh_",
      "indices" : [ 107, 118 ],
      "id_str" : "20517700",
      "id" : 20517700
    }, {
      "name" : "Gerry Doyle",
      "screen_name" : "mgerrydoyle",
      "indices" : [ 119, 131 ],
      "id_str" : "788697546",
      "id" : 788697546
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/MJhxfkaVrw",
      "expanded_url" : "http:\/\/reut.rs\/2C6D4pQ",
      "display_url" : "reut.rs\/2C6D4pQ"
    } ]
  },
  "geo" : { },
  "id_str" : "962273648152367104",
  "text" : "RT @Reuters: ICYMI | U.S., Israeli drone makers keep wary eye on rising Chinese https:\/\/t.co\/MJhxfkaVrw by @brendagoh_ @mgerrydoyle |  More\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Brenda Goh",
        "screen_name" : "brendagoh_",
        "indices" : [ 94, 105 ],
        "id_str" : "20517700",
        "id" : 20517700
      }, {
        "name" : "Gerry Doyle",
        "screen_name" : "mgerrydoyle",
        "indices" : [ 106, 118 ],
        "id_str" : "788697546",
        "id" : 788697546
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Reuters\/status\/962031127635558400\/photo\/1",
        "indices" : [ 196, 219 ],
        "url" : "https:\/\/t.co\/ewXs1YixSw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVnSNBOVAAA52Ek.jpg",
        "id_str" : "962031116528975872",
        "id" : 962031116528975872,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVnSNBOVAAA52Ek.jpg",
        "sizes" : [ {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewXs1YixSw"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Reuters\/status\/962031127635558400\/photo\/1",
        "indices" : [ 196, 219 ],
        "url" : "https:\/\/t.co\/ewXs1YixSw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVnSNUoV4AE5kMw.jpg",
        "id_str" : "962031121738358785",
        "id" : 962031121738358785,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVnSNUoV4AE5kMw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewXs1YixSw"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/Reuters\/status\/962031127635558400\/photo\/1",
        "indices" : [ 196, 219 ],
        "url" : "https:\/\/t.co\/ewXs1YixSw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVnSNiAUQAEDxDw.jpg",
        "id_str" : "962031125328576513",
        "id" : 962031125328576513,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVnSNiAUQAEDxDw.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewXs1YixSw"
      } ],
      "hashtags" : [ {
        "text" : "SingaporeAirShow2018",
        "indices" : [ 136, 157 ]
      }, {
        "text" : "SGAirshow",
        "indices" : [ 158, 168 ]
      } ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/MJhxfkaVrw",
        "expanded_url" : "http:\/\/reut.rs\/2C6D4pQ",
        "display_url" : "reut.rs\/2C6D4pQ"
      }, {
        "indices" : [ 172, 195 ],
        "url" : "https:\/\/t.co\/3miHmXiMmt",
        "expanded_url" : "http:\/\/reut.rs\/2Ec8FMJ",
        "display_url" : "reut.rs\/2Ec8FMJ"
      } ]
    },
    "geo" : { },
    "id_str" : "962031127635558400",
    "text" : "ICYMI | U.S., Israeli drone makers keep wary eye on rising Chinese https:\/\/t.co\/MJhxfkaVrw by @brendagoh_ @mgerrydoyle |  More from the #SingaporeAirShow2018 #SGAirshow at https:\/\/t.co\/3miHmXiMmt https:\/\/t.co\/ewXs1YixSw",
    "id" : 962031127635558400,
    "created_at" : "2018-02-09 18:31:09 +0000",
    "user" : {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "protected" : false,
      "id_str" : "1652541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877554927932891136\/ZBEs235N_normal.jpg",
      "id" : 1652541,
      "verified" : true
    }
  },
  "id" : 962273648152367104,
  "created_at" : "2018-02-10 10:34:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Faithkeeper \u63A8\u7279\u8C08\u5175",
      "screen_name" : "tpenghui",
      "indices" : [ 3, 12 ],
      "id_str" : "762668853834883073",
      "id" : 762668853834883073
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "F",
      "indices" : [ 23, 25 ]
    }, {
      "text" : "SingaporeAirshow2018",
      "indices" : [ 48, 69 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962272676604760064",
  "text" : "RT @tpenghui: The USMC #F-35Bs arriving for the #SingaporeAirshow2018 were fitted with Luneberg radar reflectors (rectangular blocks on fus\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "F",
        "indices" : [ 9, 11 ]
      }, {
        "text" : "SingaporeAirshow2018",
        "indices" : [ 34, 55 ]
      }, {
        "text" : "SGAirshow",
        "indices" : [ 212, 222 ]
      }, {
        "text" : "SingaporeAirshow",
        "indices" : [ 224, 241 ]
      }, {
        "text" : "RSAF50",
        "indices" : [ 242, 249 ]
      } ],
      "urls" : [ {
        "indices" : [ 250, 273 ],
        "url" : "https:\/\/t.co\/CzAr0pi0WS",
        "expanded_url" : "http:\/\/www.straitstimes.com\/videos\/two-f-35b-fighter-jets-in-singapore-for-airshow\/5725329689001",
        "display_url" : "straitstimes.com\/videos\/two-f-3\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "960162716819189760",
    "text" : "The USMC #F-35Bs arriving for the #SingaporeAirshow2018 were fitted with Luneberg radar reflectors (rectangular blocks on fuselage forward of tail fins) so that they can be detected by Changi air traffic control #SGAirshow  #SingaporeAirshow #RSAF50 https:\/\/t.co\/CzAr0pi0WS",
    "id" : 960162716819189760,
    "created_at" : "2018-02-04 14:46:45 +0000",
    "user" : {
      "name" : "Faithkeeper \u63A8\u7279\u8C08\u5175",
      "screen_name" : "tpenghui",
      "protected" : false,
      "id_str" : "762668853834883073",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/764317431837790208\/fydlf0p8_normal.jpg",
      "id" : 762668853834883073,
      "verified" : false
    }
  },
  "id" : 962272676604760064,
  "created_at" : "2018-02-10 10:30:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The RSAF",
      "screen_name" : "TheRSAF",
      "indices" : [ 3, 11 ],
      "id_str" : "723323122011635712",
      "id" : 723323122011635712
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TheRSAF\/status\/961495442310627328\/video\/1",
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/oydFwv8bDa",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/961494866420051968\/pu\/img\/x2oPIsmgMykoYJu-.jpg",
      "id_str" : "961494866420051968",
      "id" : 961494866420051968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/961494866420051968\/pu\/img\/x2oPIsmgMykoYJu-.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oydFwv8bDa"
    } ],
    "hashtags" : [ {
      "text" : "RSAF50",
      "indices" : [ 54, 61 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962271459262586880",
  "text" : "RT @TheRSAF: Wondering how our F-15SG got its special #RSAF50 paint scheme? Here's how its done in 100 seconds! https:\/\/t.co\/oydFwv8bDa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheRSAF\/status\/961495442310627328\/video\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/oydFwv8bDa",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/961494866420051968\/pu\/img\/x2oPIsmgMykoYJu-.jpg",
        "id_str" : "961494866420051968",
        "id" : 961494866420051968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/961494866420051968\/pu\/img\/x2oPIsmgMykoYJu-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oydFwv8bDa"
      } ],
      "hashtags" : [ {
        "text" : "RSAF50",
        "indices" : [ 41, 48 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "961495442310627328",
    "text" : "Wondering how our F-15SG got its special #RSAF50 paint scheme? Here's how its done in 100 seconds! https:\/\/t.co\/oydFwv8bDa",
    "id" : 961495442310627328,
    "created_at" : "2018-02-08 07:02:31 +0000",
    "user" : {
      "name" : "The RSAF",
      "screen_name" : "TheRSAF",
      "protected" : false,
      "id_str" : "723323122011635712",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/953835853360476161\/0lAaoQzd_normal.jpg",
      "id" : 723323122011635712,
      "verified" : true
    }
  },
  "id" : 962271459262586880,
  "created_at" : "2018-02-10 10:26:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Temasek",
      "screen_name" : "Temasek",
      "indices" : [ 3, 11 ],
      "id_str" : "1549668462",
      "id" : 1549668462
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TemasekInvests",
      "indices" : [ 91, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/i0jNJufUIT",
      "expanded_url" : "http:\/\/tmsk.sg\/lg",
      "display_url" : "tmsk.sg\/lg"
    } ]
  },
  "geo" : { },
  "id_str" : "962111180516061190",
  "text" : "RT @Temasek: Singapore company develops world\u2019s first Zika antibody in record nine months! #TemasekInvests https:\/\/t.co\/i0jNJufUIT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.falcon.io\" rel=\"nofollow\"\u003EFalcon Social Media Management \u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TemasekInvests",
        "indices" : [ 78, 93 ]
      } ],
      "urls" : [ {
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/i0jNJufUIT",
        "expanded_url" : "http:\/\/tmsk.sg\/lg",
        "display_url" : "tmsk.sg\/lg"
      } ]
    },
    "geo" : { },
    "id_str" : "961210593901506560",
    "text" : "Singapore company develops world\u2019s first Zika antibody in record nine months! #TemasekInvests https:\/\/t.co\/i0jNJufUIT",
    "id" : 961210593901506560,
    "created_at" : "2018-02-07 12:10:38 +0000",
    "user" : {
      "name" : "Temasek",
      "screen_name" : "Temasek",
      "protected" : false,
      "id_str" : "1549668462",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617977683780112384\/JAICZY_m_normal.jpg",
      "id" : 1549668462,
      "verified" : true
    }
  },
  "id" : 962111180516061190,
  "created_at" : "2018-02-09 23:49:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Ayling",
      "screen_name" : "AlexHunted2018",
      "indices" : [ 0, 15 ],
      "id_str" : "949000844418142208",
      "id" : 949000844418142208
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Hunted",
      "indices" : [ 34, 41 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "961729908379652097",
  "geo" : { },
  "id_str" : "962091954770202632",
  "in_reply_to_user_id" : 949000844418142208,
  "text" : "@AlexHunted2018 Watched the final #Hunted episode on catch up. You and your Dad do great! Happy for both of you.",
  "id" : 962091954770202632,
  "in_reply_to_status_id" : 961729908379652097,
  "created_at" : "2018-02-09 22:32:51 +0000",
  "in_reply_to_screen_name" : "AlexHunted2018",
  "in_reply_to_user_id_str" : "949000844418142208",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tourism Ireland",
      "screen_name" : "GoToIrelandCA",
      "indices" : [ 3, 17 ],
      "id_str" : "87453990",
      "id" : 87453990
    }, {
      "name" : "Bunratty Castle & Folk Park",
      "screen_name" : "BunrattyCastle",
      "indices" : [ 123, 138 ],
      "id_str" : "47437612",
      "id" : 47437612
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "962066992076214273",
  "text" : "RT @GoToIrelandCA: Known as Ireland\u2019s gentle giants, Irish wolfhounds are a symbol of Celtic culture and ancient heritage. @BunrattyCastle\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bunratty Castle & Folk Park",
        "screen_name" : "BunrattyCastle",
        "indices" : [ 104, 119 ],
        "id_str" : "47437612",
        "id" : 47437612
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GoToIrelandCA\/status\/962008346491416578\/photo\/1",
        "indices" : [ 249, 272 ],
        "url" : "https:\/\/t.co\/cY13UuaymS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVm9fYhVwAEMJkK.jpg",
        "id_str" : "962008342276194305",
        "id" : 962008342276194305,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVm9fYhVwAEMJkK.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 2432,
          "resize" : "fit",
          "w" : 3648
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cY13UuaymS"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 225, 248 ],
        "url" : "https:\/\/t.co\/kc602HXbaV",
        "expanded_url" : "http:\/\/bit.ly\/1kys7EQ",
        "display_url" : "bit.ly\/1kys7EQ"
      } ]
    },
    "geo" : { },
    "id_str" : "962008346491416578",
    "text" : "Known as Ireland\u2019s gentle giants, Irish wolfhounds are a symbol of Celtic culture and ancient heritage. @BunrattyCastle wants your help naming two of its new female wolfhounds, check out their Facebook page for more details! https:\/\/t.co\/kc602HXbaV https:\/\/t.co\/cY13UuaymS",
    "id" : 962008346491416578,
    "created_at" : "2018-02-09 17:00:37 +0000",
    "user" : {
      "name" : "Tourism Ireland",
      "screen_name" : "GoToIrelandCA",
      "protected" : false,
      "id_str" : "87453990",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/817399107555905536\/Sm1mSeM0_normal.jpg",
      "id" : 87453990,
      "verified" : true
    }
  },
  "id" : 962066992076214273,
  "created_at" : "2018-02-09 20:53:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/AP6gE6g6uN",
      "expanded_url" : "http:\/\/bit.ly\/GANotes",
      "display_url" : "bit.ly\/GANotes"
    } ]
  },
  "geo" : { },
  "id_str" : "962046480658780161",
  "text" : "What do you use to keep track of implementation and configuration changes in your web analytics tool? I create this \nhttps:\/\/t.co\/AP6gE6g6uN",
  "id" : 962046480658780161,
  "created_at" : "2018-02-09 19:32:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    }, {
      "name" : "Hasselblad",
      "screen_name" : "Hasselblad",
      "indices" : [ 59, 70 ],
      "id_str" : "791729785",
      "id" : 791729785
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/zTzmgQel4d",
      "expanded_url" : "https:\/\/www.theverge.com\/2018\/2\/6\/16977522\/hasselblad-camera-factory-tour",
      "display_url" : "theverge.com\/2018\/2\/6\/16977\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961969694201827334",
  "text" : "RT @topgold: Because I will never afford one, I read about @Hasselblad https:\/\/t.co\/zTzmgQel4d",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hasselblad",
        "screen_name" : "Hasselblad",
        "indices" : [ 46, 57 ],
        "id_str" : "791729785",
        "id" : 791729785
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/zTzmgQel4d",
        "expanded_url" : "https:\/\/www.theverge.com\/2018\/2\/6\/16977522\/hasselblad-camera-factory-tour",
        "display_url" : "theverge.com\/2018\/2\/6\/16977\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "961966876820729856",
    "text" : "Because I will never afford one, I read about @Hasselblad https:\/\/t.co\/zTzmgQel4d",
    "id" : 961966876820729856,
    "created_at" : "2018-02-09 14:15:50 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 961969694201827334,
  "created_at" : "2018-02-09 14:27:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 109, 125 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/3PbBvEiBJP",
      "expanded_url" : "http:\/\/po.st\/wLvmFW",
      "display_url" : "po.st\/wLvmFW"
    } ]
  },
  "geo" : { },
  "id_str" : "961947382660370433",
  "text" : "CNB to keep 'very close watch' on Escobar eatery named after Colombian drug lord https:\/\/t.co\/3PbBvEiBJP via @ChannelNewsAsia",
  "id" : 961947382660370433,
  "created_at" : "2018-02-09 12:58:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "735484288997556225",
  "geo" : { },
  "id_str" : "961939075530940416",
  "in_reply_to_user_id" : 27853769,
  "text" : "@webmawie I know it a bit late. Do you still need help? Thanks",
  "id" : 961939075530940416,
  "in_reply_to_status_id" : 735484288997556225,
  "created_at" : "2018-02-09 12:25:22 +0000",
  "in_reply_to_screen_name" : "MarieHLGER",
  "in_reply_to_user_id_str" : "27853769",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 128, 151 ],
      "url" : "https:\/\/t.co\/wXUIb8lfHv",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/948959120467611649",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961935621169274883",
  "text" : "In the long term, running the business alone can be emotionally taxing. Bringing on another person can be tremendously helpful. https:\/\/t.co\/wXUIb8lfHv",
  "id" : 961935621169274883,
  "created_at" : "2018-02-09 12:11:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tableau Software",
      "screen_name" : "tableau",
      "indices" : [ 3, 11 ],
      "id_str" : "14792516",
      "id" : 14792516
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/961929441977958400\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/Haaq93JllF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVl1hGXWsAEHgHT.jpg",
      "id_str" : "961929206925012993",
      "id" : 961929206925012993,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVl1hGXWsAEHgHT.jpg",
      "sizes" : [ {
        "h" : 365,
        "resize" : "fit",
        "w" : 399
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 365,
        "resize" : "fit",
        "w" : 399
      }, {
        "h" : 365,
        "resize" : "fit",
        "w" : 399
      }, {
        "h" : 365,
        "resize" : "fit",
        "w" : 399
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Haaq93JllF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961929441977958400",
  "text" : "hi @tableau there is a mistake here. https:\/\/t.co\/Haaq93JllF",
  "id" : 961929441977958400,
  "created_at" : "2018-02-09 11:47:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDCA1Rachael Evans",
      "screen_name" : "RachaelSEvans",
      "indices" : [ 0, 14 ],
      "id_str" : "388818157",
      "id" : 388818157
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "961544854919634944",
  "geo" : { },
  "id_str" : "961885594006835200",
  "in_reply_to_user_id" : 388818157,
  "text" : "@RachaelSEvans Friday is here now",
  "id" : 961885594006835200,
  "in_reply_to_status_id" : 961544854919634944,
  "created_at" : "2018-02-09 08:52:51 +0000",
  "in_reply_to_screen_name" : "RachaelSEvans",
  "in_reply_to_user_id_str" : "388818157",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Shann Biglione",
      "screen_name" : "LeShann",
      "indices" : [ 3, 11 ],
      "id_str" : "23428324",
      "id" : 23428324
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961880783664300034",
  "text" : "RT @LeShann: You know what campaigns like It\u2019s a Tide Ad and Crocodile Dundee Tourism Australia have in common? \n\nNot AI.\nNot VR.\nNot AR.\nN\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "961857164812861440",
    "text" : "You know what campaigns like It\u2019s a Tide Ad and Crocodile Dundee Tourism Australia have in common? \n\nNot AI.\nNot VR.\nNot AR.\nNot big data.\nNot Blockchain.\nNot social purpose.\nNot social war rooms.\n\nBut good old creativity, human empathy, mass reach, and marketers with a spine.",
    "id" : 961857164812861440,
    "created_at" : "2018-02-09 06:59:53 +0000",
    "user" : {
      "name" : "Shann Biglione",
      "screen_name" : "LeShann",
      "protected" : false,
      "id_str" : "23428324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/881822430896848896\/2aBySYZ9_normal.jpg",
      "id" : 23428324,
      "verified" : false
    }
  },
  "id" : 961880783664300034,
  "created_at" : "2018-02-09 08:33:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/961880526318657536\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/TPfHr5YB3Q",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVlI8JQVQAAc-JO.jpg",
      "id_str" : "961880193534083072",
      "id" : 961880193534083072,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVlI8JQVQAAc-JO.jpg",
      "sizes" : [ {
        "h" : 134,
        "resize" : "crop",
        "w" : 134
      }, {
        "h" : 134,
        "resize" : "fit",
        "w" : 683
      }, {
        "h" : 134,
        "resize" : "fit",
        "w" : 683
      }, {
        "h" : 133,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 134,
        "resize" : "fit",
        "w" : 683
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TPfHr5YB3Q"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961880526318657536",
  "text" : "The dom8in name I seeking is being taken. Javascript, API, Markdown + .io https:\/\/t.co\/TPfHr5YB3Q",
  "id" : 961880526318657536,
  "created_at" : "2018-02-09 08:32:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961871671761121281",
  "text" : "RT @getoptimise: You\u2019re stuck at a company which is slow to adopt new things. A sideline jobs allows you to work on things that make an imp\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "961871077235310599",
    "text" : "You\u2019re stuck at a company which is slow to adopt new things. A sideline jobs allows you to work on things that make an impact and fulfills your self-actualization needs. Contact @getoptimise now.",
    "id" : 961871077235310599,
    "created_at" : "2018-02-09 07:55:10 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 961871671761121281,
  "created_at" : "2018-02-09 07:57:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lucian Teo",
      "screen_name" : "lucian",
      "indices" : [ 65, 72 ],
      "id_str" : "860351",
      "id" : 860351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/HiotaX1hST",
      "expanded_url" : "https:\/\/info.moravia.com\/blog\/why-japanese-web-design-is-still-the-way-it-is",
      "display_url" : "info.moravia.com\/blog\/why-japan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961866216900935685",
  "text" : "Zen is for monks, not merchants https:\/\/t.co\/HiotaX1hST Link via @lucian",
  "id" : 961866216900935685,
  "created_at" : "2018-02-09 07:35:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Moravia",
      "screen_name" : "Moravia",
      "indices" : [ 3, 11 ],
      "id_str" : "47314263",
      "id" : 47314263
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/o87ab0fj5k",
      "expanded_url" : "https:\/\/hubs.ly\/H09XDqj0",
      "display_url" : "hubs.ly\/H09XDqj0"
    } ]
  },
  "geo" : { },
  "id_str" : "961866001175388160",
  "text" : "RT @Moravia: Unknown language discovered in Southeast Asia https:\/\/t.co\/o87ab0fj5k",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hubspot.com\/\" rel=\"nofollow\"\u003EHubSpot\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/o87ab0fj5k",
        "expanded_url" : "https:\/\/hubs.ly\/H09XDqj0",
        "display_url" : "hubs.ly\/H09XDqj0"
      } ]
    },
    "geo" : { },
    "id_str" : "961367948907745281",
    "text" : "Unknown language discovered in Southeast Asia https:\/\/t.co\/o87ab0fj5k",
    "id" : 961367948907745281,
    "created_at" : "2018-02-07 22:35:55 +0000",
    "user" : {
      "name" : "Moravia",
      "screen_name" : "Moravia",
      "protected" : false,
      "id_str" : "47314263",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839094285047844864\/w0FsWKtB_normal.jpg",
      "id" : 47314263,
      "verified" : false
    }
  },
  "id" : 961866001175388160,
  "created_at" : "2018-02-09 07:34:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lucian Teo",
      "screen_name" : "lucian",
      "indices" : [ 3, 10 ],
      "id_str" : "860351",
      "id" : 860351
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/lucian\/status\/956755906913382400\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/A7wj7IZT3m",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DUcUbRlU8AEBPLf.jpg",
      "id_str" : "956755904648507393",
      "id" : 956755904648507393,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DUcUbRlU8AEBPLf.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/A7wj7IZT3m"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 29, 52 ],
      "url" : "https:\/\/t.co\/MqnXKRoAKH",
      "expanded_url" : "http:\/\/ift.tt\/2BsltZ3",
      "display_url" : "ift.tt\/2BsltZ3"
    } ]
  },
  "geo" : { },
  "id_str" : "961865636153409536",
  "text" : "RT @lucian: Start of school. https:\/\/t.co\/MqnXKRoAKH https:\/\/t.co\/A7wj7IZT3m",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lucian\/status\/956755906913382400\/photo\/1",
        "indices" : [ 41, 64 ],
        "url" : "https:\/\/t.co\/A7wj7IZT3m",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DUcUbRlU8AEBPLf.jpg",
        "id_str" : "956755904648507393",
        "id" : 956755904648507393,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DUcUbRlU8AEBPLf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/A7wj7IZT3m"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 17, 40 ],
        "url" : "https:\/\/t.co\/MqnXKRoAKH",
        "expanded_url" : "http:\/\/ift.tt\/2BsltZ3",
        "display_url" : "ift.tt\/2BsltZ3"
      } ]
    },
    "geo" : { },
    "id_str" : "956755906913382400",
    "text" : "Start of school. https:\/\/t.co\/MqnXKRoAKH https:\/\/t.co\/A7wj7IZT3m",
    "id" : 956755906913382400,
    "created_at" : "2018-01-26 05:09:18 +0000",
    "user" : {
      "name" : "Lucian Teo",
      "screen_name" : "lucian",
      "protected" : false,
      "id_str" : "860351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/749394642895040514\/RqC_aepW_normal.jpg",
      "id" : 860351,
      "verified" : false
    }
  },
  "id" : 961865636153409536,
  "created_at" : "2018-02-09 07:33:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "indices" : [ 0, 12 ],
      "id_str" : "21564577",
      "id" : 21564577
    }, {
      "name" : "Morning Ireland",
      "screen_name" : "morningireland",
      "indices" : [ 13, 28 ],
      "id_str" : "22790104",
      "id" : 22790104
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "961858663710277632",
  "geo" : { },
  "id_str" : "961862968785776640",
  "in_reply_to_user_id" : 21564577,
  "text" : "@chris_byrne @morningireland Good Morning Chris!",
  "id" : 961862968785776640,
  "in_reply_to_status_id" : 961858663710277632,
  "created_at" : "2018-02-09 07:22:56 +0000",
  "in_reply_to_screen_name" : "chris_byrne",
  "in_reply_to_user_id_str" : "21564577",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rachel Thomas",
      "screen_name" : "math_rachel",
      "indices" : [ 3, 15 ],
      "id_str" : "1408142352",
      "id" : 1408142352
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961861404834721792",
  "text" : "RT @math_rachel: I\u2019m quoted in this. I think software used to make decisions such as hiring, firing, parole, prison sentences, etc. should\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 153, 176 ],
        "url" : "https:\/\/t.co\/LEWRXk9bAT",
        "expanded_url" : "https:\/\/www.politico.com\/agenda\/story\/2018\/02\/07\/algorithmic-bias-software-recommendations-000631",
        "display_url" : "politico.com\/agenda\/story\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "961761943265492992",
    "text" : "I\u2019m quoted in this. I think software used to make decisions such as hiring, firing, parole, prison sentences, etc. should be required to be open source. https:\/\/t.co\/LEWRXk9bAT",
    "id" : 961761943265492992,
    "created_at" : "2018-02-09 00:41:30 +0000",
    "user" : {
      "name" : "Rachel Thomas",
      "screen_name" : "math_rachel",
      "protected" : false,
      "id_str" : "1408142352",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/802658972784214016\/_MmQnI3K_normal.jpg",
      "id" : 1408142352,
      "verified" : false
    }
  },
  "id" : 961861404834721792,
  "created_at" : "2018-02-09 07:16:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rory Carrick",
      "screen_name" : "EatDrinkRunFun",
      "indices" : [ 3, 18 ],
      "id_str" : "135944259",
      "id" : 135944259
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961734108664729600",
  "text" : "RT @EatDrinkRunFun: HELP: College assignment &amp; looking a little help. I need to grab as many subscribers as possible for a temporary websit\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 243, 266 ],
        "url" : "https:\/\/t.co\/Se0dINjaSb",
        "expanded_url" : "https:\/\/trinityrunning.wordpress.com\/join-us\/",
        "display_url" : "trinityrunning.wordpress.com\/join-us\/"
      } ]
    },
    "geo" : { },
    "id_str" : "961631634578690048",
    "text" : "HELP: College assignment &amp; looking a little help. I need to grab as many subscribers as possible for a temporary website. if anyone could oblige, much appreciated. Emails wont be used for anything outside it &amp; deleted after .RT please https:\/\/t.co\/Se0dINjaSb",
    "id" : 961631634578690048,
    "created_at" : "2018-02-08 16:03:42 +0000",
    "user" : {
      "name" : "Rory Carrick",
      "screen_name" : "EatDrinkRunFun",
      "protected" : false,
      "id_str" : "135944259",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/981196852317048832\/vn9hX5dN_normal.jpg",
      "id" : 135944259,
      "verified" : false
    }
  },
  "id" : 961734108664729600,
  "created_at" : "2018-02-08 22:50:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Risa Mish",
      "screen_name" : "risamish",
      "indices" : [ 3, 12 ],
      "id_str" : "281747865",
      "id" : 281747865
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961685647416332290",
  "text" : "RT @risamish: \"How to Maintain Friendships\": positivity (laughter, affirmation, gratitude), consistency, &amp; vulnerability (revealing\/sharing\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/risamish\/status\/957644326582063110\/photo\/1",
        "indices" : [ 284, 307 ],
        "url" : "https:\/\/t.co\/CZh8X3vqat",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DUo7hkcWkAAmHIG.jpg",
        "id_str" : "957643318673969152",
        "id" : 957643318673969152,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DUo7hkcWkAAmHIG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 419,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 419,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 419,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 356,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CZh8X3vqat"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 260, 283 ],
        "url" : "https:\/\/t.co\/DZPQNmtCNc",
        "expanded_url" : "https:\/\/nyti.ms\/2Dj2m5K",
        "display_url" : "nyti.ms\/2Dj2m5K"
      } ]
    },
    "geo" : { },
    "id_str" : "957644326582063110",
    "text" : "\"How to Maintain Friendships\": positivity (laughter, affirmation, gratitude), consistency, &amp; vulnerability (revealing\/sharing). What not to do? Say, \"I'm too busy\", which leaves friends wondering if they aren't valued. Tell them when you can meet instead. https:\/\/t.co\/DZPQNmtCNc https:\/\/t.co\/CZh8X3vqat",
    "id" : 957644326582063110,
    "created_at" : "2018-01-28 15:59:34 +0000",
    "user" : {
      "name" : "Risa Mish",
      "screen_name" : "risamish",
      "protected" : false,
      "id_str" : "281747865",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3331301550\/45204b72233b3e2a3a98bd4ce0a83547_normal.jpeg",
      "id" : 281747865,
      "verified" : false
    }
  },
  "id" : 961685647416332290,
  "created_at" : "2018-02-08 19:38:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 0, 11 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "959730406944776192",
  "geo" : { },
  "id_str" : "961684191510450176",
  "in_reply_to_user_id" : 4747144932,
  "text" : "@CaroBowler Economic rice is a oxymoron.",
  "id" : 961684191510450176,
  "in_reply_to_status_id" : 959730406944776192,
  "created_at" : "2018-02-08 19:32:33 +0000",
  "in_reply_to_screen_name" : "CaroBowler",
  "in_reply_to_user_id_str" : "4747144932",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/8Xl8BxwgEE",
      "expanded_url" : "https:\/\/twitter.com\/Inc\/status\/960164665354522624",
      "display_url" : "twitter.com\/Inc\/status\/960\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961637107482718208",
  "text" : "RT @getoptimise: Google memo to online marketers. Wake up your idea and be prepared. https:\/\/t.co\/8Xl8BxwgEE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 91 ],
        "url" : "https:\/\/t.co\/8Xl8BxwgEE",
        "expanded_url" : "https:\/\/twitter.com\/Inc\/status\/960164665354522624",
        "display_url" : "twitter.com\/Inc\/status\/960\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "960662823943667713",
    "text" : "Google memo to online marketers. Wake up your idea and be prepared. https:\/\/t.co\/8Xl8BxwgEE",
    "id" : 960662823943667713,
    "created_at" : "2018-02-05 23:54:00 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 961637107482718208,
  "created_at" : "2018-02-08 16:25:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/IVHVqSjqhg",
      "expanded_url" : "http:\/\/www.citizensinformation.ie\/en\/justice\/civil_law\/commissioners_for_oaths.html",
      "display_url" : "citizensinformation.ie\/en\/justice\/civ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961614673681448960",
  "text" : "I am seeking \"Commissioner for Oaths\" in Dublin. Thanks https:\/\/t.co\/IVHVqSjqhg Any one?",
  "id" : 961614673681448960,
  "created_at" : "2018-02-08 14:56:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/961561085261185024\/photo\/1",
      "indices" : [ 153, 176 ],
      "url" : "https:\/\/t.co\/bPDBMX0nFN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVgmsvnW0AAk9iZ.jpg",
      "id_str" : "961561070581239808",
      "id" : 961561070581239808,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVgmsvnW0AAk9iZ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 582,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 330,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1444,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 994,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bPDBMX0nFN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961561085261185024",
  "text" : "Frame the problem in a way that captures the real essence of the problem and is understood by those seeking a solution. (Pg 10, The Smart Solution Book) https:\/\/t.co\/bPDBMX0nFN",
  "id" : 961561085261185024,
  "created_at" : "2018-02-08 11:23:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "nicole forsgren",
      "screen_name" : "nicolefv",
      "indices" : [ 3, 12 ],
      "id_str" : "50124536",
      "id" : 50124536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961540171027140610",
  "text" : "RT @nicolefv: If you only read ONE article on DevOps, THIS should be it: Containers Will Not Fix Your Broken Culture (and Other Hard Truths\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bridget Kromhout",
        "screen_name" : "bridgetkromhout",
        "indices" : [ 130, 146 ],
        "id_str" : "1465659204",
        "id" : 1465659204
      }, {
        "name" : "ACM Queue",
        "screen_name" : "ACMQueue",
        "indices" : [ 194, 203 ],
        "id_str" : "86967199",
        "id" : 86967199
      }, {
        "name" : "Jez Humble",
        "screen_name" : "jezhumble",
        "indices" : [ 222, 232 ],
        "id_str" : "15685575",
        "id" : 15685575
      }, {
        "name" : "Mik Kersten",
        "screen_name" : "mik_kersten",
        "indices" : [ 233, 245 ],
        "id_str" : "5902882",
        "id" : 5902882
      }, {
        "name" : "Quorum of One",
        "screen_name" : "postwait",
        "indices" : [ 246, 255 ],
        "id_str" : "9580822",
        "id" : 9580822
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "DevOps",
        "indices" : [ 175, 182 ]
      } ],
      "urls" : [ {
        "indices" : [ 264, 287 ],
        "url" : "https:\/\/t.co\/AcNq12R0Sk",
        "expanded_url" : "https:\/\/twitter.com\/ACMQueue\/status\/961314158779265026",
        "display_url" : "twitter.com\/ACMQueue\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "961338668551041024",
    "text" : "If you only read ONE article on DevOps, THIS should be it: Containers Will Not Fix Your Broken Culture (and Other Hard Truths) by @bridgetkromhout \n\nThe latest in the special #DevOps issue from @ACMQueue, with articles by @jezhumble @mik_kersten @postwait and me! https:\/\/t.co\/AcNq12R0Sk",
    "id" : 961338668551041024,
    "created_at" : "2018-02-07 20:39:34 +0000",
    "user" : {
      "name" : "nicole forsgren",
      "screen_name" : "nicolefv",
      "protected" : false,
      "id_str" : "50124536",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1018656587840307201\/TDSPHshk_normal.jpg",
      "id" : 50124536,
      "verified" : false
    }
  },
  "id" : 961540171027140610,
  "created_at" : "2018-02-08 10:00:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/961536556598202370\/photo\/1",
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/TGXo4vO9k6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVgQXGeXcAEp-0U.jpg",
      "id_str" : "961536509504614401",
      "id" : 961536509504614401,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVgQXGeXcAEp-0U.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/TGXo4vO9k6"
    } ],
    "hashtags" : [ {
      "text" : "DubEntWeek",
      "indices" : [ 19, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961536556598202370",
  "text" : "Looking forward to #DubEntWeek https:\/\/t.co\/TGXo4vO9k6",
  "id" : 961536556598202370,
  "created_at" : "2018-02-08 09:45:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/961536228830179328\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/xpWqeWgGrs",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVgQGq8X4AE1AMa.jpg",
      "id_str" : "961536227236372481",
      "id" : 961536227236372481,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVgQGq8X4AE1AMa.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xpWqeWgGrs"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/QDHkKi4Rg6",
      "expanded_url" : "http:\/\/ift.tt\/2BgFrtn",
      "display_url" : "ift.tt\/2BgFrtn"
    } ]
  },
  "geo" : { },
  "id_str" : "961536228830179328",
  "text" : "Good Morning https:\/\/t.co\/QDHkKi4Rg6 https:\/\/t.co\/xpWqeWgGrs",
  "id" : 961536228830179328,
  "created_at" : "2018-02-08 09:44:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Netlify",
      "screen_name" : "Netlify",
      "indices" : [ 95, 103 ],
      "id_str" : "2571501973",
      "id" : 2571501973
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961500350262972417",
  "text" : "Someone launch a rocket with car. I am trying to launch a Static Site with Git as back-end and @Netlify as front-end.",
  "id" : 961500350262972417,
  "created_at" : "2018-02-08 07:22:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Piers Scott",
      "screen_name" : "pdscott",
      "indices" : [ 0, 8 ],
      "id_str" : "19360077",
      "id" : 19360077
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "961319310387023872",
  "geo" : { },
  "id_str" : "961340229851533312",
  "in_reply_to_user_id" : 19360077,
  "text" : "@pdscott I was informed that he set up the first electric car dealership in space.",
  "id" : 961340229851533312,
  "in_reply_to_status_id" : 961319310387023872,
  "created_at" : "2018-02-07 20:45:46 +0000",
  "in_reply_to_screen_name" : "pdscott",
  "in_reply_to_user_id_str" : "19360077",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961323304245133312",
  "text" : "People are paying tributes to their love on....LinkedIn!",
  "id" : 961323304245133312,
  "created_at" : "2018-02-07 19:38:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/tI8hr4q6Gu",
      "expanded_url" : "https:\/\/twitter.com\/beeonaposy\/status\/960688632498737152",
      "display_url" : "twitter.com\/beeonaposy\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961298291924324357",
  "text" : "Promptly printed this out and stick to wall. https:\/\/t.co\/tI8hr4q6Gu",
  "id" : 961298291924324357,
  "created_at" : "2018-02-07 17:59:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Netlify CMS",
      "screen_name" : "NetlifyCMS",
      "indices" : [ 50, 61 ],
      "id_str" : "790596805943787520",
      "id" : 790596805943787520
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/961273082437623808\/photo\/1",
      "indices" : [ 156, 179 ],
      "url" : "https:\/\/t.co\/EocPdj9mVx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVcfxcRX0AAXZBz.jpg",
      "id_str" : "961271979729997824",
      "id" : 961271979729997824,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVcfxcRX0AAXZBz.jpg",
      "sizes" : [ {
        "h" : 742,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 742,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 652,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 369,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EocPdj9mVx"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961273082437623808",
  "text" : "This is handy. Editorial workflow that comes with @NetlifyCMS. Just edited the config.yml file to reflect GitHub owner and repo.  Infrastructure as code. \uD83D\uDE4C https:\/\/t.co\/EocPdj9mVx",
  "id" : 961273082437623808,
  "created_at" : "2018-02-07 16:18:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michele Bannister",
      "screen_name" : "astrokiwi",
      "indices" : [ 3, 13 ],
      "id_str" : "21820958",
      "id" : 21820958
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961248276455534592",
  "text" : "RT @astrokiwi: The heavy lift to orbit has been managed before. Electric cars - there's already three on the Moon. This, now: this is the s\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "FalconHeavy",
        "indices" : [ 176, 188 ]
      } ],
      "urls" : [ {
        "indices" : [ 189, 212 ],
        "url" : "https:\/\/t.co\/jwJJfAr4yx",
        "expanded_url" : "https:\/\/twitter.com\/lorengrush\/status\/960983661687529473",
        "display_url" : "twitter.com\/lorengrush\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "960983987643666434",
    "text" : "The heavy lift to orbit has been managed before. Electric cars - there's already three on the Moon. This, now: this is the stunning part. A perfect ballet of reusable rockets. #FalconHeavy https:\/\/t.co\/jwJJfAr4yx",
    "id" : 960983987643666434,
    "created_at" : "2018-02-06 21:10:11 +0000",
    "user" : {
      "name" : "Michele Bannister",
      "screen_name" : "astrokiwi",
      "protected" : false,
      "id_str" : "21820958",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/507318853810921472\/LzUc6F7D_normal.jpeg",
      "id" : 21820958,
      "verified" : false
    }
  },
  "id" : 961248276455534592,
  "created_at" : "2018-02-07 14:40:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Martin Bryant",
      "screen_name" : "MartinSFP",
      "indices" : [ 3, 13 ],
      "id_str" : "10064382",
      "id" : 10064382
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961227466244227072",
  "text" : "RT @MartinSFP: Oh god. Every terrible business presentation about ambition and scale is going to have this photo in it now, isn't it? https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MartinSFP\/status\/961038445463461888\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/IHBN300Z23",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVZLW5bXUAA7atJ.jpg",
        "id_str" : "961038427234979840",
        "id" : 961038427234979840,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVZLW5bXUAA7atJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1880
        }, {
          "h" : 362,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1880
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IHBN300Z23"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "961038445463461888",
    "text" : "Oh god. Every terrible business presentation about ambition and scale is going to have this photo in it now, isn't it? https:\/\/t.co\/IHBN300Z23",
    "id" : 961038445463461888,
    "created_at" : "2018-02-07 00:46:35 +0000",
    "user" : {
      "name" : "Martin Bryant",
      "screen_name" : "MartinSFP",
      "protected" : false,
      "id_str" : "10064382",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1016368429333401601\/VXVzgUOw_normal.jpg",
      "id" : 10064382,
      "verified" : true
    }
  },
  "id" : 961227466244227072,
  "created_at" : "2018-02-07 13:17:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/hXzzyNCAyX",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/BBIM7FK?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/BBIM7\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "961166861298749440",
  "text" : "The US Navy's new drone warship can drive itself as it hunts submarines https:\/\/t.co\/hXzzyNCAyX",
  "id" : 961166861298749440,
  "created_at" : "2018-02-07 09:16:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Julie Ann Mugabe",
      "screen_name" : "Julie_Johnsoned",
      "indices" : [ 3, 19 ],
      "id_str" : "3174826456",
      "id" : 3174826456
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Julie_Johnsoned\/status\/960660001982418949\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/qSkx3110tu",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVTzLOTU0AAqkxK.jpg",
      "id_str" : "960659994680020992",
      "id" : 960659994680020992,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVTzLOTU0AAqkxK.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 633
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 1241
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 1241
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1116
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qSkx3110tu"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "961146758888919040",
  "text" : "RT @Julie_Johnsoned: Lmao thinking about these types of ghouls having an absolute meltdown today https:\/\/t.co\/qSkx3110tu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Julie_Johnsoned\/status\/960660001982418949\/photo\/1",
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/qSkx3110tu",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVTzLOTU0AAqkxK.jpg",
        "id_str" : "960659994680020992",
        "id" : 960659994680020992,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVTzLOTU0AAqkxK.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 633
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 1241
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 1241
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1116
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qSkx3110tu"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "960660001982418949",
    "text" : "Lmao thinking about these types of ghouls having an absolute meltdown today https:\/\/t.co\/qSkx3110tu",
    "id" : 960660001982418949,
    "created_at" : "2018-02-05 23:42:47 +0000",
    "user" : {
      "name" : "Julie Ann Mugabe",
      "screen_name" : "Julie_Johnsoned",
      "protected" : false,
      "id_str" : "3174826456",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/835300351087620097\/Rvfs7B8l_normal.jpg",
      "id" : 3174826456,
      "verified" : false
    }
  },
  "id" : 961146758888919040,
  "created_at" : "2018-02-07 07:56:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RAIZA \uD83D\uDDA4",
      "screen_name" : "razzyfrazzy",
      "indices" : [ 0, 12 ],
      "id_str" : "14174374",
      "id" : 14174374
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "960578430143160320",
  "geo" : { },
  "id_str" : "960877311066345472",
  "in_reply_to_user_id" : 14174374,
  "text" : "@razzyfrazzy What motivates you to go back to this app?",
  "id" : 960877311066345472,
  "in_reply_to_status_id" : 960578430143160320,
  "created_at" : "2018-02-06 14:06:17 +0000",
  "in_reply_to_screen_name" : "razzyfrazzy",
  "in_reply_to_user_id_str" : "14174374",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "indices" : [ 3, 15 ],
      "id_str" : "21564577",
      "id" : 21564577
    }, {
      "name" : "Coderdojo Dingle",
      "screen_name" : "DingleDojo",
      "indices" : [ 50, 61 ],
      "id_str" : "704894450",
      "id" : 704894450
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SaferInternetDay",
      "indices" : [ 17, 34 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960832231173484544",
  "text" : "RT @chris_byrne: #SaferInternetDay We established @dingledojo in 2012 and have always advocated that Children under age 13 should not have\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Coderdojo Dingle",
        "screen_name" : "DingleDojo",
        "indices" : [ 33, 44 ],
        "id_str" : "704894450",
        "id" : 704894450
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/chris_byrne\/status\/960829200532942851\/photo\/1",
        "indices" : [ 247, 270 ],
        "url" : "https:\/\/t.co\/7szpWyvYCt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVWM2MQWAAAmAY3.jpg",
        "id_str" : "960828958144069632",
        "id" : 960828958144069632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVWM2MQWAAAmAY3.jpg",
        "sizes" : [ {
          "h" : 529,
          "resize" : "fit",
          "w" : 710
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 710
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 507,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 710
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/7szpWyvYCt"
      } ],
      "hashtags" : [ {
        "text" : "SaferInternetDay",
        "indices" : [ 0, 17 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "960829200532942851",
    "text" : "#SaferInternetDay We established @dingledojo in 2012 and have always advocated that Children under age 13 should not have smartphones or access to Social media. You cannot rely on Social media sites to vet their content or verify users correctly. https:\/\/t.co\/7szpWyvYCt",
    "id" : 960829200532942851,
    "created_at" : "2018-02-06 10:55:07 +0000",
    "user" : {
      "name" : "Hey Chris !",
      "screen_name" : "chris_byrne",
      "protected" : false,
      "id_str" : "21564577",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028212548859043840\/9i6LBuYR_normal.jpg",
      "id" : 21564577,
      "verified" : false
    }
  },
  "id" : 960832231173484544,
  "created_at" : "2018-02-06 11:07:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 151, 174 ],
      "url" : "https:\/\/t.co\/enCfLSmkD1",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/960671298216169476",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "960801233857662976",
  "text" : "You need to setup a YAML config to describe the content model of your site, and typically tweak the main layout of the CMS a bit to fit your own site. https:\/\/t.co\/enCfLSmkD1",
  "id" : 960801233857662976,
  "created_at" : "2018-02-06 09:03:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Kuestenmacher",
      "screen_name" : "simongerman600",
      "indices" : [ 3, 18 ],
      "id_str" : "359188534",
      "id" : 359188534
    }, {
      "name" : "Financial Times",
      "screen_name" : "FT",
      "indices" : [ 64, 67 ],
      "id_str" : "18949452",
      "id" : 18949452
    }, {
      "name" : "Martin Stabe",
      "screen_name" : "martinstabe",
      "indices" : [ 80, 92 ],
      "id_str" : "75503",
      "id" : 75503
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dataviz",
      "indices" : [ 25, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960784648396820480",
  "text" : "RT @simongerman600: This #dataviz overview table is used by the @FT team around @martinstabe to decide which visualization to use. It's a h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Financial Times",
        "screen_name" : "FT",
        "indices" : [ 44, 47 ],
        "id_str" : "18949452",
        "id" : 18949452
      }, {
        "name" : "Martin Stabe",
        "screen_name" : "martinstabe",
        "indices" : [ 60, 72 ],
        "id_str" : "75503",
        "id" : 75503
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/simongerman600\/status\/958814656738529281\/photo\/1",
        "indices" : [ 262, 285 ],
        "url" : "https:\/\/t.co\/l8lyrNFEIO",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DU5k2QIX4AIFOvt.jpg",
        "id_str" : "958814653882294274",
        "id" : 958814653882294274,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DU5k2QIX4AIFOvt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1449,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 481,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1449,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 849,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/l8lyrNFEIO"
      } ],
      "hashtags" : [ {
        "text" : "dataviz",
        "indices" : [ 5, 13 ]
      } ],
      "urls" : [ {
        "indices" : [ 238, 261 ],
        "url" : "https:\/\/t.co\/JioXOveFnO",
        "expanded_url" : "https:\/\/buff.ly\/2ElZOow",
        "display_url" : "buff.ly\/2ElZOow"
      } ]
    },
    "geo" : { },
    "id_str" : "958814656738529281",
    "text" : "This #dataviz overview table is used by the @FT team around @martinstabe to decide which visualization to use. It's a handy starting point. Highly recommend looking at this. High-res version (if you want to print this) can be found here: https:\/\/t.co\/JioXOveFnO https:\/\/t.co\/l8lyrNFEIO",
    "id" : 958814656738529281,
    "created_at" : "2018-01-31 21:30:02 +0000",
    "user" : {
      "name" : "Simon Kuestenmacher",
      "screen_name" : "simongerman600",
      "protected" : false,
      "id_str" : "359188534",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/916213395422035968\/__3Ac2dP_normal.jpg",
      "id" : 359188534,
      "verified" : false
    }
  },
  "id" : 960784648396820480,
  "created_at" : "2018-02-06 07:58:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960673051217100800",
  "text" : "If you keen to explore this, message me. I got a site for you to play with.",
  "id" : 960673051217100800,
  "created_at" : "2018-02-06 00:34:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "960671127084457986",
  "geo" : { },
  "id_str" : "960671298216169476",
  "in_reply_to_user_id" : 9465632,
  "text" : "Pair up with Netlify CMS to have the best of both world. Ease of updating for non-technical user and get the speed, security and scalability of Static Site.",
  "id" : 960671298216169476,
  "in_reply_to_status_id" : 960671127084457986,
  "created_at" : "2018-02-06 00:27:40 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 56, 68 ]
    } ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/P44SOyDSzO",
      "expanded_url" : "https:\/\/davidwalsh.name\/introduction-static-site-generators",
      "display_url" : "davidwalsh.name\/introduction-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "960671127084457986",
  "text" : "What is Static Site Generators? https:\/\/t.co\/P44SOyDSzO #getoptimise",
  "id" : 960671127084457986,
  "created_at" : "2018-02-06 00:26:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960660789970522112",
  "text" : "RT @getoptimise: \"Never let people determine your future. If there is no clear path, make one.\" and here what I being up to https:\/\/t.co\/tk\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/tkBGysjcLK",
        "expanded_url" : "https:\/\/www.linkedin.com\/company\/get-optimise\/",
        "display_url" : "linkedin.com\/company\/get-op\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "960658457664815104",
    "text" : "\"Never let people determine your future. If there is no clear path, make one.\" and here what I being up to https:\/\/t.co\/tkBGysjcLK",
    "id" : 960658457664815104,
    "created_at" : "2018-02-05 23:36:39 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 960660789970522112,
  "created_at" : "2018-02-05 23:45:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/jePL4HXrYL",
      "expanded_url" : "https:\/\/www.cindyalvarez.com\/you-need-to-make-wanting-no-longer-free\/",
      "display_url" : "cindyalvarez.com\/you-need-to-ma\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "960631897670942722",
  "text" : "\"Why You Shouldn't Ask Customers 'Would You Like ____?' Questions https:\/\/t.co\/jePL4HXrYL",
  "id" : 960631897670942722,
  "created_at" : "2018-02-05 21:51:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/vAg7RYYDII",
      "expanded_url" : "http:\/\/a.msn.com\/00\/en-ie\/BBII386?ocid=st",
      "display_url" : "a.msn.com\/00\/en-ie\/BBII3\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "960571436233523200",
  "text" : "DPD courier who was fined for day off to see doctor dies from diabetes https:\/\/t.co\/vAg7RYYDII",
  "id" : 960571436233523200,
  "created_at" : "2018-02-05 17:50:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/yNVMg2kBSu",
      "expanded_url" : "https:\/\/twitter.com\/SFA_Irl\/status\/960506136842657792",
      "display_url" : "twitter.com\/SFA_Irl\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "960568371459887105",
  "text" : "ANyone heading to this event? https:\/\/t.co\/yNVMg2kBSu",
  "id" : 960568371459887105,
  "created_at" : "2018-02-05 17:38:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RAIZA \uD83D\uDDA4",
      "screen_name" : "razzyfrazzy",
      "indices" : [ 0, 12 ],
      "id_str" : "14174374",
      "id" : 14174374
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "960542208062504961",
  "geo" : { },
  "id_str" : "960549979105517568",
  "in_reply_to_user_id" : 14174374,
  "text" : "@razzyfrazzy Memrise. What about you?",
  "id" : 960549979105517568,
  "in_reply_to_status_id" : 960542208062504961,
  "created_at" : "2018-02-05 16:25:35 +0000",
  "in_reply_to_screen_name" : "razzyfrazzy",
  "in_reply_to_user_id_str" : "14174374",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Thomas Sowell",
      "screen_name" : "ThomasSowell",
      "indices" : [ 3, 16 ],
      "id_str" : "57338175",
      "id" : 57338175
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960533977525309443",
  "text" : "RT @ThomasSowell: \"Too many journalists see their work as an opportunity to promote their own pet political notions, rather than a responsi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "960295707226845185",
    "text" : "\"Too many journalists see their work as an opportunity to promote their own pet political notions, rather than a responsibility to inform the public and let their readers and viewers decide for themselves.\"",
    "id" : 960295707226845185,
    "created_at" : "2018-02-04 23:35:12 +0000",
    "user" : {
      "name" : "Thomas Sowell",
      "screen_name" : "ThomasSowell",
      "protected" : false,
      "id_str" : "57338175",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/316502615\/sowell2_normal.bmp",
      "id" : 57338175,
      "verified" : false
    }
  },
  "id" : 960533977525309443,
  "created_at" : "2018-02-05 15:22:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Innovation Academy",
      "screen_name" : "UCD_Innovators",
      "indices" : [ 38, 53 ],
      "id_str" : "497301948",
      "id" : 497301948
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960436207191822336",
  "text" : "Like to talk to people who has enroll @UCD_Innovators Postgraduate Certificate in Innovation, Entrepreneurship and Enterprise. Thanks",
  "id" : 960436207191822336,
  "created_at" : "2018-02-05 08:53:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Milos Vojinovic",
      "screen_name" : "infinite_milos",
      "indices" : [ 3, 18 ],
      "id_str" : "2772149848",
      "id" : 2772149848
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/infinite_milos\/status\/960210613959102464\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/9HdDrnXILx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVNabkbXUAEg73a.jpg",
      "id_str" : "960210575241465857",
      "id" : 960210575241465857,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVNabkbXUAEg73a.jpg",
      "sizes" : [ {
        "h" : 1555,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 516,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 911,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1866,
        "resize" : "fit",
        "w" : 2457
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9HdDrnXILx"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960435582618013697",
  "text" : "RT @infinite_milos: Berlin wall stood for 10316 days, and tomorrow is 10316 days since it is gone. https:\/\/t.co\/9HdDrnXILx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/infinite_milos\/status\/960210613959102464\/photo\/1",
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/9HdDrnXILx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVNabkbXUAEg73a.jpg",
        "id_str" : "960210575241465857",
        "id" : 960210575241465857,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVNabkbXUAEg73a.jpg",
        "sizes" : [ {
          "h" : 1555,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 516,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 911,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1866,
          "resize" : "fit",
          "w" : 2457
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9HdDrnXILx"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "960210613959102464",
    "text" : "Berlin wall stood for 10316 days, and tomorrow is 10316 days since it is gone. https:\/\/t.co\/9HdDrnXILx",
    "id" : 960210613959102464,
    "created_at" : "2018-02-04 17:57:04 +0000",
    "user" : {
      "name" : "Milos Vojinovic",
      "screen_name" : "infinite_milos",
      "protected" : false,
      "id_str" : "2772149848",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/843024230107271168\/UoH2At-j_normal.jpg",
      "id" : 2772149848,
      "verified" : false
    }
  },
  "id" : 960435582618013697,
  "created_at" : "2018-02-05 08:51:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gerard Keown",
      "screen_name" : "gerkeown",
      "indices" : [ 3, 12 ],
      "id_str" : "323468919",
      "id" : 323468919
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "960410051772022784",
  "text" : "RT @gerkeown: 150 years ago today, Countess Constance Markievicz was born. First woman elected to the Irish and the UK parliaments; first w\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/gerkeown\/status\/960124676121997312\/photo\/1",
        "indices" : [ 206, 229 ],
        "url" : "https:\/\/t.co\/n5UJ4EjQFG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVMMS-XWkAAbUMt.jpg",
        "id_str" : "960124665678172160",
        "id" : 960124665678172160,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVMMS-XWkAAbUMt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 350,
          "resize" : "fit",
          "w" : 506
        }, {
          "h" : 350,
          "resize" : "fit",
          "w" : 506
        }, {
          "h" : 350,
          "resize" : "fit",
          "w" : 506
        }, {
          "h" : 350,
          "resize" : "fit",
          "w" : 506
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/n5UJ4EjQFG"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "960124676121997312",
    "text" : "150 years ago today, Countess Constance Markievicz was born. First woman elected to the Irish and the UK parliaments; first woman minister in Ireland, second in the world; symbol of Irish-Polish friendship https:\/\/t.co\/n5UJ4EjQFG",
    "id" : 960124676121997312,
    "created_at" : "2018-02-04 12:15:35 +0000",
    "user" : {
      "name" : "Gerard Keown",
      "screen_name" : "gerkeown",
      "protected" : false,
      "id_str" : "323468919",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/663825647811170304\/hivSfTvJ_normal.jpg",
      "id" : 323468919,
      "verified" : true
    }
  },
  "id" : 960410051772022784,
  "created_at" : "2018-02-05 07:09:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darragh McCashin",
      "screen_name" : "DarraghMcCashin",
      "indices" : [ 3, 19 ],
      "id_str" : "419652244",
      "id" : 419652244
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "mentalhealthhour",
      "indices" : [ 74, 91 ]
    } ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/mDfgnZqhQU",
      "expanded_url" : "https:\/\/twitter.com\/MentalHealth_Hr\/status\/960267382689329152",
      "display_url" : "twitter.com\/MentalHealth_H\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "960297588741337088",
  "text" : "RT @DarraghMcCashin: Prevention in action, IMO. Fair play to all involved #mentalhealthhour https:\/\/t.co\/mDfgnZqhQU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "mentalhealthhour",
        "indices" : [ 53, 70 ]
      } ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/mDfgnZqhQU",
        "expanded_url" : "https:\/\/twitter.com\/MentalHealth_Hr\/status\/960267382689329152",
        "display_url" : "twitter.com\/MentalHealth_H\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "960269692999995392",
    "text" : "Prevention in action, IMO. Fair play to all involved #mentalhealthhour https:\/\/t.co\/mDfgnZqhQU",
    "id" : 960269692999995392,
    "created_at" : "2018-02-04 21:51:50 +0000",
    "user" : {
      "name" : "Darragh McCashin",
      "screen_name" : "DarraghMcCashin",
      "protected" : false,
      "id_str" : "419652244",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/949743183969292289\/QOc75OLx_normal.jpg",
      "id" : 419652244,
      "verified" : false
    }
  },
  "id" : 960297588741337088,
  "created_at" : "2018-02-04 23:42:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 0, 12 ],
      "id_str" : "85053026",
      "id" : 85053026
    }, {
      "name" : "Lilah Raptopoulos",
      "screen_name" : "lilahrap",
      "indices" : [ 13, 22 ],
      "id_str" : "21505453",
      "id" : 21505453
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bulletjournal",
      "indices" : [ 88, 102 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "960184437660176388",
  "geo" : { },
  "id_str" : "960201237584064512",
  "in_reply_to_user_id" : 85053026,
  "text" : "@gavindbrown @lilahrap I bought the dotted notebook one a year ago after learning about #bulletjournal and can't bear to deface it with ink....\uD83D\uDE01",
  "id" : 960201237584064512,
  "in_reply_to_status_id" : 960184437660176388,
  "created_at" : "2018-02-04 17:19:49 +0000",
  "in_reply_to_screen_name" : "gavindbrown",
  "in_reply_to_user_id_str" : "85053026",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hannah Ritchie",
      "screen_name" : "HannahRitchie02",
      "indices" : [ 3, 19 ],
      "id_str" : "2666969059",
      "id" : 2666969059
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959887535081213952",
  "text" : "RT @HannahRitchie02: Persistent drought &amp; water extraction means Cape Town is approaching Day Zero: weeks away from running out of water.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Guardian Environment",
        "screen_name" : "guardianeco",
        "indices" : [ 260, 272 ],
        "id_str" : "20582958",
        "id" : 20582958
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/HannahRitchie02\/status\/959884426275680257\/photo\/1",
        "indices" : [ 273, 296 ],
        "url" : "https:\/\/t.co\/C05e5SiY49",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVIwt5AXkAABv3_.jpg",
        "id_str" : "959883235537948672",
        "id" : 959883235537948672,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVIwt5AXkAABv3_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 464,
          "resize" : "fit",
          "w" : 655
        }, {
          "h" : 464,
          "resize" : "fit",
          "w" : 655
        }, {
          "h" : 464,
          "resize" : "fit",
          "w" : 655
        }, {
          "h" : 464,
          "resize" : "fit",
          "w" : 655
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/C05e5SiY49"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "959884426275680257",
    "text" : "Persistent drought &amp; water extraction means Cape Town is approaching Day Zero: weeks away from running out of water.\n \nResidents asked to limit to 50L per day. Take a 5min shower &amp; flush the loo 5 times &amp; you're already 40% over the limit.\nSource: @guardianeco https:\/\/t.co\/C05e5SiY49",
    "id" : 959884426275680257,
    "created_at" : "2018-02-03 20:20:55 +0000",
    "user" : {
      "name" : "Hannah Ritchie",
      "screen_name" : "HannahRitchie02",
      "protected" : false,
      "id_str" : "2666969059",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846843881559937024\/yqcdQhHx_normal.jpg",
      "id" : 2666969059,
      "verified" : false
    }
  },
  "id" : 959887535081213952,
  "created_at" : "2018-02-03 20:33:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CapeTown",
      "indices" : [ 22, 31 ]
    }, {
      "text" : "water",
      "indices" : [ 50, 56 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959851496790773760",
  "text" : "RT @randal_olson: How #CapeTown is running out of #water. \n\nThe interactive at the end really drives home how bad the situation is. #Global\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/959844658334478336\/photo\/1",
        "indices" : [ 176, 199 ],
        "url" : "https:\/\/t.co\/PTnwTmeArv",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DVINS0dUMAI7eUj.jpg",
        "id_str" : "959844287553744898",
        "id" : 959844287553744898,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DVINS0dUMAI7eUj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 610,
          "resize" : "fit",
          "w" : 1270
        }, {
          "h" : 610,
          "resize" : "fit",
          "w" : 1270
        }, {
          "h" : 327,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PTnwTmeArv"
      } ],
      "hashtags" : [ {
        "text" : "CapeTown",
        "indices" : [ 4, 13 ]
      }, {
        "text" : "water",
        "indices" : [ 32, 38 ]
      }, {
        "text" : "GlobalWarming",
        "indices" : [ 114, 128 ]
      }, {
        "text" : "ClimateChange",
        "indices" : [ 129, 143 ]
      } ],
      "urls" : [ {
        "indices" : [ 152, 175 ],
        "url" : "https:\/\/t.co\/9lPdwGGSAE",
        "expanded_url" : "https:\/\/www.theguardian.com\/cities\/ng-interactive\/2018\/feb\/03\/day-zero-how-cape-town-running-out-water",
        "display_url" : "theguardian.com\/cities\/ng-inte\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "959844658334478336",
    "text" : "How #CapeTown is running out of #water. \n\nThe interactive at the end really drives home how bad the situation is. #GlobalWarming #ClimateChange\n\nStory: https:\/\/t.co\/9lPdwGGSAE https:\/\/t.co\/PTnwTmeArv",
    "id" : 959844658334478336,
    "created_at" : "2018-02-03 17:42:54 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 959851496790773760,
  "created_at" : "2018-02-03 18:10:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/959832713179037696\/photo\/1",
      "indices" : [ 152, 175 ],
      "url" : "https:\/\/t.co\/Et2Zrl5IV9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVICNSqXkAUBAac.jpg",
      "id_str" : "959832097954435077",
      "id" : 959832097954435077,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVICNSqXkAUBAac.jpg",
      "sizes" : [ {
        "h" : 350,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 618,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 702,
        "resize" : "fit",
        "w" : 1364
      }, {
        "h" : 702,
        "resize" : "fit",
        "w" : 1364
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Et2Zrl5IV9"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959832713179037696",
  "text" : "My Fav Trick: \"Ctrl-Shift-I\" bring out the panel on browser right hand side and edit the text. Is there a way to save it, share it and come back later? https:\/\/t.co\/Et2Zrl5IV9",
  "id" : 959832713179037696,
  "created_at" : "2018-02-03 16:55:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 0, 11 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "959656956066590721",
  "geo" : { },
  "id_str" : "959706307677868032",
  "in_reply_to_user_id" : 4747144932,
  "text" : "@CaroBowler \uD83D\uDE2C did u order from those air con food court?",
  "id" : 959706307677868032,
  "in_reply_to_status_id" : 959656956066590721,
  "created_at" : "2018-02-03 08:33:08 +0000",
  "in_reply_to_screen_name" : "CaroBowler",
  "in_reply_to_user_id_str" : "4747144932",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amazing Maps",
      "screen_name" : "Amazing_Maps",
      "indices" : [ 3, 16 ],
      "id_str" : "1571270053",
      "id" : 1571270053
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Amazing_Maps\/status\/938203890805329920\/photo\/1",
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/KjapWdROFm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DQUrdz8XkAAigso.jpg",
      "id_str" : "938203888536227840",
      "id" : 938203888536227840,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQUrdz8XkAAigso.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 464
      }, {
        "h" : 1486,
        "resize" : "fit",
        "w" : 1015
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 820
      }, {
        "h" : 1486,
        "resize" : "fit",
        "w" : 1015
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KjapWdROFm"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/xoh36eczRt",
      "expanded_url" : "https:\/\/buff.ly\/2iUScTO",
      "display_url" : "buff.ly\/2iUScTO"
    } ]
  },
  "geo" : { },
  "id_str" : "959577762708295681",
  "text" : "RT @Amazing_Maps: Doggerland - The Europe That Was\n\nSource: https:\/\/t.co\/xoh36eczRt https:\/\/t.co\/KjapWdROFm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Amazing_Maps\/status\/938203890805329920\/photo\/1",
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/KjapWdROFm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DQUrdz8XkAAigso.jpg",
        "id_str" : "938203888536227840",
        "id" : 938203888536227840,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DQUrdz8XkAAigso.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 464
        }, {
          "h" : 1486,
          "resize" : "fit",
          "w" : 1015
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 820
        }, {
          "h" : 1486,
          "resize" : "fit",
          "w" : 1015
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KjapWdROFm"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/xoh36eczRt",
        "expanded_url" : "https:\/\/buff.ly\/2iUScTO",
        "display_url" : "buff.ly\/2iUScTO"
      } ]
    },
    "geo" : { },
    "id_str" : "938203890805329920",
    "text" : "Doggerland - The Europe That Was\n\nSource: https:\/\/t.co\/xoh36eczRt https:\/\/t.co\/KjapWdROFm",
    "id" : 938203890805329920,
    "created_at" : "2017-12-06 00:30:13 +0000",
    "user" : {
      "name" : "Amazing Maps",
      "screen_name" : "Amazing_Maps",
      "protected" : false,
      "id_str" : "1571270053",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/670261890233311232\/AI4OcoZM_normal.jpg",
      "id" : 1571270053,
      "verified" : false
    }
  },
  "id" : 959577762708295681,
  "created_at" : "2018-02-03 00:02:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "indices" : [ 3, 16 ],
      "id_str" : "367008160",
      "id" : 367008160
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959577323522621441",
  "text" : "RT @managerspeak: But I don't want to be onboarded by a Happiness Engineer.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "959369953685143552",
    "text" : "But I don't want to be onboarded by a Happiness Engineer.",
    "id" : 959369953685143552,
    "created_at" : "2018-02-02 10:16:35 +0000",
    "user" : {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "protected" : false,
      "id_str" : "367008160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3689160554\/8dc837089c6fa2aa3348074a6b1e8ec1_normal.jpeg",
      "id" : 367008160,
      "verified" : false
    }
  },
  "id" : 959577323522621441,
  "created_at" : "2018-02-03 00:00:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maria Popova",
      "screen_name" : "brainpicker",
      "indices" : [ 3, 15 ],
      "id_str" : "9207632",
      "id" : 9207632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959557579432218624",
  "text" : "RT @brainpicker: \u201CDo not fear to be eccentric in opinion, for every opinion now accepted was once eccentric.\u201D\n\nBertrand Russell, one of the\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 246, 269 ],
        "url" : "https:\/\/t.co\/jEJYeRh6TZ",
        "expanded_url" : "https:\/\/www.brainpickings.org\/2012\/05\/02\/a-liberal-decalogue-bertrand-russell\/",
        "display_url" : "brainpickings.org\/2012\/05\/02\/a-l\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "959411508320133120",
    "text" : "\u201CDo not fear to be eccentric in opinion, for every opinion now accepted was once eccentric.\u201D\n\nBertrand Russell, one of the greatest intellects who ever lived, died on this day in 1970 and left us his 10 timeless commandments of critical thinking https:\/\/t.co\/jEJYeRh6TZ",
    "id" : 959411508320133120,
    "created_at" : "2018-02-02 13:01:43 +0000",
    "user" : {
      "name" : "Maria Popova",
      "screen_name" : "brainpicker",
      "protected" : false,
      "id_str" : "9207632",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/577255253852065794\/qGnSwsBR_normal.jpg",
      "id" : 9207632,
      "verified" : true
    }
  },
  "id" : 959557579432218624,
  "created_at" : "2018-02-02 22:42:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/959503177744814080\/photo\/1",
      "indices" : [ 127, 150 ],
      "url" : "https:\/\/t.co\/CIZXzJcINN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVDW1o7WAAY0loI.jpg",
      "id_str" : "959502937637650438",
      "id" : 959502937637650438,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVDW1o7WAAY0loI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 739,
        "resize" : "fit",
        "w" : 1360
      }, {
        "h" : 652,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 739,
        "resize" : "fit",
        "w" : 1360
      }, {
        "h" : 370,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/CIZXzJcINN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959503177744814080",
  "text" : "If I could get RStudio Cloud to work with Blogdown, it be great. I do not need an installed RStudio on a PC. Just a a browser. https:\/\/t.co\/CIZXzJcINN",
  "id" : 959503177744814080,
  "created_at" : "2018-02-02 19:05:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/qJoXphMx1M",
      "expanded_url" : "https:\/\/twitter.com\/m4riannelee\/status\/959442682744631296",
      "display_url" : "twitter.com\/m4riannelee\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "959469217748840448",
  "text" : "Tempted to saviour this \"2 in 1\" until I came across this. https:\/\/t.co\/qJoXphMx1M",
  "id" : 959469217748840448,
  "created_at" : "2018-02-02 16:51:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959371229286359043",
  "text" : "Google Chrome should have a Reader View built-in like Firefox",
  "id" : 959371229286359043,
  "created_at" : "2018-02-02 10:21:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/959369623887020032\/photo\/1",
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/Ib5hs7ViVC",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DVBdasyWkAEjMMu.jpg",
      "id_str" : "959369433910251521",
      "id" : 959369433910251521,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVBdasyWkAEjMMu.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Ib5hs7ViVC"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959369623887020032",
  "text" : "Irish Times Jan 25, 2018 https:\/\/t.co\/Ib5hs7ViVC",
  "id" : 959369623887020032,
  "created_at" : "2018-02-02 10:15:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 94, 110 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/Y1G4Vof6g8",
      "expanded_url" : "http:\/\/po.st\/jDM1oR",
      "display_url" : "po.st\/jDM1oR"
    } ]
  },
  "geo" : { },
  "id_str" : "959337018617720832",
  "text" : "Defence Ministry monitoring use of fitness trackers in army camps https:\/\/t.co\/Y1G4Vof6g8 via @ChannelNewsAsia",
  "id" : 959337018617720832,
  "created_at" : "2018-02-02 08:05:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GlendaChongCNA",
      "screen_name" : "GlendaChongCNA",
      "indices" : [ 3, 18 ],
      "id_str" : "4825898098",
      "id" : 4825898098
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959333894326833154",
  "text" : "RT @GlendaChongCNA: 18 people injured, 3 seriously, after car ploughs into pedestrians in Shanghai: Chinese state media https:\/\/t.co\/0UEMfz\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TODAY",
        "screen_name" : "TODAYonline",
        "indices" : [ 128, 140 ],
        "id_str" : "41085467",
        "id" : 41085467
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/0UEMfzFaTI",
        "expanded_url" : "https:\/\/www.todayonline.com\/world\/18-people-injured-3-seriously-after-car-ploughs-pedestrians-shanghai-chinese-state-media#.WnPfhW8uywQ.twitter",
        "display_url" : "todayonline.com\/world\/18-peopl\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "959272268827258880",
    "text" : "18 people injured, 3 seriously, after car ploughs into pedestrians in Shanghai: Chinese state media https:\/\/t.co\/0UEMfzFaTI via @TODAYonline",
    "id" : 959272268827258880,
    "created_at" : "2018-02-02 03:48:25 +0000",
    "user" : {
      "name" : "GlendaChongCNA",
      "screen_name" : "GlendaChongCNA",
      "protected" : false,
      "id_str" : "4825898098",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/892277271720894464\/MKIczj0o_normal.jpg",
      "id" : 4825898098,
      "verified" : false
    }
  },
  "id" : 959333894326833154,
  "created_at" : "2018-02-02 07:53:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959331384509779968",
  "text" : "RT @getoptimise: You have your CRM, web analytics, email marketing tool, payment processors, survey tools and so on - but they don't talk t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "958986382474702848",
    "text" : "You have your CRM, web analytics, email marketing tool, payment processors, survey tools and so on - but they don't talk to each other.",
    "id" : 958986382474702848,
    "created_at" : "2018-02-01 08:52:25 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 959331384509779968,
  "created_at" : "2018-02-02 07:43:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 87, 94 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/88pGSs2epR",
      "expanded_url" : "https:\/\/www.futurelearn.com\/courses\/open-social-science-research\/2",
      "display_url" : "futurelearn.com\/courses\/open-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "959330917402673157",
  "text" : "An online MOOC in Reproducible research for social scientists. https:\/\/t.co\/88pGSs2epR #rstats",
  "id" : 959330917402673157,
  "created_at" : "2018-02-02 07:41:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Women Techmakers",
      "screen_name" : "WomenTechmakers",
      "indices" : [ 3, 19 ],
      "id_str" : "2362618909",
      "id" : 2362618909
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959327132848566273",
  "text" : "RT @WomenTechmakers: Want to win a ticket to Google I\/O? We're hosting our 5th Code Jam to I\/O for Women on Feb 17th; a single round, onlin\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WomenTechmakers\/status\/959103773896593408\/photo\/1",
        "indices" : [ 280, 303 ],
        "url" : "https:\/\/t.co\/xm4fbJOEt5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DU9qtZEUQAIL3z1.jpg",
        "id_str" : "959102573708853250",
        "id" : 959102573708853250,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DU9qtZEUQAIL3z1.jpg",
        "sizes" : [ {
          "h" : 2730,
          "resize" : "fit",
          "w" : 4095
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xm4fbJOEt5"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 256, 279 ],
        "url" : "https:\/\/t.co\/cPWQw2bPXK",
        "expanded_url" : "http:\/\/goo.gl\/2ZYxVS",
        "display_url" : "goo.gl\/2ZYxVS"
      } ]
    },
    "geo" : { },
    "id_str" : "959103773896593408",
    "text" : "Want to win a ticket to Google I\/O? We're hosting our 5th Code Jam to I\/O for Women on Feb 17th; a single round, online algorithmic problem solving challenge where the top 150 participants receive a ticket to I\/O and a $500 USD travel stipend. Register at https:\/\/t.co\/cPWQw2bPXK https:\/\/t.co\/xm4fbJOEt5",
    "id" : 959103773896593408,
    "created_at" : "2018-02-01 16:38:53 +0000",
    "user" : {
      "name" : "Women Techmakers",
      "screen_name" : "WomenTechmakers",
      "protected" : false,
      "id_str" : "2362618909",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877282359229390848\/SJDbzttm_normal.jpg",
      "id" : 2362618909,
      "verified" : false
    }
  },
  "id" : 959327132848566273,
  "created_at" : "2018-02-02 07:26:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrs wee",
      "screen_name" : "mrs_wee",
      "indices" : [ 3, 11 ],
      "id_str" : "64187979",
      "id" : 64187979
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959326674289504257",
  "text" : "RT @mrs_wee: I love the choice of pic. Kudos to housewife who painstakingly kiapped all the socks from small to big. Effort sia https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/fb5cfiOmhk",
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/959041638533251072",
        "display_url" : "twitter.com\/STcom\/status\/9\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "959046093047660549",
    "text" : "I love the choice of pic. Kudos to housewife who painstakingly kiapped all the socks from small to big. Effort sia https:\/\/t.co\/fb5cfiOmhk",
    "id" : 959046093047660549,
    "created_at" : "2018-02-01 12:49:41 +0000",
    "user" : {
      "name" : "mrs wee",
      "screen_name" : "mrs_wee",
      "protected" : false,
      "id_str" : "64187979",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/790942979477295105\/ZbvZa8NU_normal.jpg",
      "id" : 64187979,
      "verified" : false
    }
  },
  "id" : 959326674289504257,
  "created_at" : "2018-02-02 07:24:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959190205289910274",
  "text" : "RT @getoptimise: Which business tools do you use to develop, validate and execute new ideas?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "959171490699673601",
    "text" : "Which business tools do you use to develop, validate and execute new ideas?",
    "id" : 959171490699673601,
    "created_at" : "2018-02-01 21:07:58 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 959190205289910274,
  "created_at" : "2018-02-01 22:22:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959059482461515778",
  "text" : "For half a day effort, I saved USD30\/year for hosting per site.",
  "id" : 959059482461515778,
  "created_at" : "2018-02-01 13:42:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yihui Xie",
      "screen_name" : "xieyihui",
      "indices" : [ 10, 19 ],
      "id_str" : "39010299",
      "id" : 39010299
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/sbke8ymG6K",
      "expanded_url" : "https:\/\/github.com\/yihui\/travis-exitwp",
      "display_url" : "github.com\/yihui\/travis-e\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "959058088300032000",
  "text" : "Thanks to @xieyihui for https:\/\/t.co\/sbke8ymG6K Successfully convert all WordPress post to Markdown. Content free from the cage of database.",
  "id" : 959058088300032000,
  "created_at" : "2018-02-01 13:37:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 135, 158 ],
      "url" : "https:\/\/t.co\/4cSjpp6Nlr",
      "expanded_url" : "https:\/\/getoptimise.netlify.com\/2018\/01\/28\/blogdown-site-workflow\/",
      "display_url" : "getoptimise.netlify.com\/2018\/01\/28\/blo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "959030579080679424",
  "text" : "Best part of this process is that your content is saved on a local folder and on GitHub. Not locked in a WordPress CMS (i.e. database) https:\/\/t.co\/4cSjpp6Nlr",
  "id" : 959030579080679424,
  "created_at" : "2018-02-01 11:48:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/959011011939504129\/photo\/1",
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/tW96Iv0xnW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DU8XZTpX0AUS0oX.jpg",
      "id_str" : "959010969191174149",
      "id" : 959010969191174149,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DU8XZTpX0AUS0oX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tW96Iv0xnW"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "959006601112969216",
  "geo" : { },
  "id_str" : "959011011939504129",
  "in_reply_to_user_id" : 9465632,
  "text" : "After that tweet, I got this... https:\/\/t.co\/tW96Iv0xnW",
  "id" : 959011011939504129,
  "in_reply_to_status_id" : 959006601112969216,
  "created_at" : "2018-02-01 10:30:17 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "959006601112969216",
  "text" : "\"...no other country in the world is trying to bring high-speed broadband to every single home and premises.\" An Ireland Minister claims. \uD83E\uDD14",
  "id" : 959006601112969216,
  "created_at" : "2018-02-01 10:12:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/MUVfbwKUtE",
      "expanded_url" : "https:\/\/twitter.com\/MaximMag\/status\/958758060880728064",
      "display_url" : "twitter.com\/MaximMag\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "959004147864473601",
  "text" : "RT @Clearpreso: I have a feeling that in 2018 literally everything will be deemed offensive by some people https:\/\/t.co\/MUVfbwKUtE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 91, 114 ],
        "url" : "https:\/\/t.co\/MUVfbwKUtE",
        "expanded_url" : "https:\/\/twitter.com\/MaximMag\/status\/958758060880728064",
        "display_url" : "twitter.com\/MaximMag\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "958996683148447744",
    "text" : "I have a feeling that in 2018 literally everything will be deemed offensive by some people https:\/\/t.co\/MUVfbwKUtE",
    "id" : 958996683148447744,
    "created_at" : "2018-02-01 09:33:21 +0000",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 959004147864473601,
  "created_at" : "2018-02-01 10:03:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Observable",
      "screen_name" : "observablehq",
      "indices" : [ 3, 16 ],
      "id_str" : "905255756789825536",
      "id" : 905255756789825536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/qfdZ1fyiR6",
      "expanded_url" : "https:\/\/beta.observablehq.com",
      "display_url" : "beta.observablehq.com"
    } ]
  },
  "geo" : { },
  "id_str" : "958984638017523712",
  "text" : "RT @observablehq: Hello, world! \uD83C\uDF89 Announcing Observable notebooks: a better way to code, discover, and communicate. https:\/\/t.co\/qfdZ1fyiR6",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/qfdZ1fyiR6",
        "expanded_url" : "https:\/\/beta.observablehq.com",
        "display_url" : "beta.observablehq.com"
      } ]
    },
    "geo" : { },
    "id_str" : "958726175123128321",
    "text" : "Hello, world! \uD83C\uDF89 Announcing Observable notebooks: a better way to code, discover, and communicate. https:\/\/t.co\/qfdZ1fyiR6",
    "id" : 958726175123128321,
    "created_at" : "2018-01-31 15:38:27 +0000",
    "user" : {
      "name" : "Observable",
      "screen_name" : "observablehq",
      "protected" : false,
      "id_str" : "905255756789825536",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/970805785503477760\/HfTZJiZo_normal.jpg",
      "id" : 905255756789825536,
      "verified" : false
    }
  },
  "id" : 958984638017523712,
  "created_at" : "2018-02-01 08:45:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 3, 14 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "958983687122640896",
  "text" : "RT @CaroBowler: Ten years in Singapore today. Craving Luigis chips, smothered in salt, vinegar with a pot of curry sauce as only someone fr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Longford",
        "indices" : [ 126, 135 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "958968671371608065",
    "text" : "Ten years in Singapore today. Craving Luigis chips, smothered in salt, vinegar with a pot of curry sauce as only someone from #Longford can. Majulah Singapura, thanks for having me!",
    "id" : 958968671371608065,
    "created_at" : "2018-02-01 07:42:02 +0000",
    "user" : {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "protected" : false,
      "id_str" : "4747144932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686824073884643329\/U-3rp4zx_normal.jpg",
      "id" : 4747144932,
      "verified" : false
    }
  },
  "id" : 958983687122640896,
  "created_at" : "2018-02-01 08:41:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]