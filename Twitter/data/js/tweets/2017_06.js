Grailbird.data.tweets_2017_06 = 
 [ {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/880809263828004865\/photo\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/GZfWCpfLHF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDlDW2TXsAA7yn8.jpg",
      "id_str" : "880809261940584448",
      "id" : 880809261940584448,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDlDW2TXsAA7yn8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GZfWCpfLHF"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/PGDd5eJenO",
      "expanded_url" : "http:\/\/ift.tt\/2ttIWJb",
      "display_url" : "ift.tt\/2ttIWJb"
    } ]
  },
  "geo" : { },
  "id_str" : "880809263828004865",
  "text" : "Road trip. Seeing niece off to her first year in college. https:\/\/t.co\/PGDd5eJenO https:\/\/t.co\/GZfWCpfLHF",
  "id" : 880809263828004865,
  "created_at" : "2017-06-30 15:24:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/aaZGale5KS",
      "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2017-06-29\/singapore-regulator-says-1mdb-related-penalties-heavy-enough",
      "display_url" : "bloomberg.com\/news\/articles\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880572201715458048",
  "text" : "https:\/\/t.co\/aaZGale5KS",
  "id" : 880572201715458048,
  "created_at" : "2017-06-29 23:42:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Natalie Harrower",
      "screen_name" : "natalieharrower",
      "indices" : [ 3, 19 ],
      "id_str" : "17073404",
      "id" : 17073404
    }, {
      "name" : "Richard Ovenden",
      "screen_name" : "richove",
      "indices" : [ 86, 94 ],
      "id_str" : "14923196",
      "id" : 14923196
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/natalieharrower\/status\/880408996376698881\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/A7FoQ7aj6M",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDfXTuoWAAE-kxJ.jpg",
      "id_str" : "880408986108952577",
      "id" : 880408986108952577,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDfXTuoWAAE-kxJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/A7FoQ7aj6M"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880410541503553536",
  "text" : "RT @natalieharrower: The web is not digital preservation! Some stats on link rot from @richove https:\/\/t.co\/A7FoQ7aj6M",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Richard Ovenden",
        "screen_name" : "richove",
        "indices" : [ 65, 73 ],
        "id_str" : "14923196",
        "id" : 14923196
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/natalieharrower\/status\/880408996376698881\/photo\/1",
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/A7FoQ7aj6M",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDfXTuoWAAE-kxJ.jpg",
        "id_str" : "880408986108952577",
        "id" : 880408986108952577,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDfXTuoWAAE-kxJ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/A7FoQ7aj6M"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "880408996376698881",
    "text" : "The web is not digital preservation! Some stats on link rot from @richove https:\/\/t.co\/A7FoQ7aj6M",
    "id" : 880408996376698881,
    "created_at" : "2017-06-29 12:53:56 +0000",
    "user" : {
      "name" : "Dr Natalie Harrower",
      "screen_name" : "natalieharrower",
      "protected" : false,
      "id_str" : "17073404",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992918977172000769\/pPUq69N4_normal.jpg",
      "id" : 17073404,
      "verified" : false
    }
  },
  "id" : 880410541503553536,
  "created_at" : "2017-06-29 13:00:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/uMScPTksLC",
      "expanded_url" : "https:\/\/twitter.com\/Jimalytics\/status\/880164806321373184",
      "display_url" : "twitter.com\/Jimalytics\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880395815998468096",
  "text" : "Hear Hear https:\/\/t.co\/uMScPTksLC",
  "id" : 880395815998468096,
  "created_at" : "2017-06-29 12:01:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/3HlsM4G652",
      "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/875797942531239936",
      "display_url" : "twitter.com\/getoptimise\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880340400443056128",
  "text" : "https:\/\/t.co\/3HlsM4G652",
  "id" : 880340400443056128,
  "created_at" : "2017-06-29 08:21:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/avjf4s8sUy",
      "expanded_url" : "https:\/\/twitter.com\/ofmeowandbake\/status\/880319058435948544",
      "display_url" : "twitter.com\/ofmeowandbake\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880338328972742660",
  "text" : "The long arm of the law catches up with overseas Singaporean. https:\/\/t.co\/avjf4s8sUy",
  "id" : 880338328972742660,
  "created_at" : "2017-06-29 08:13:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Crespo",
      "screen_name" : "inFocusDCPhoto",
      "indices" : [ 3, 18 ],
      "id_str" : "756894403",
      "id" : 756894403
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880337991658422273",
  "text" : "RT @inFocusDCPhoto: British series (Happy Valley, Sherlock...) are usually too short, while American series (Homeland, Suits) go on forever\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "880337828831518720",
    "text" : "British series (Happy Valley, Sherlock...) are usually too short, while American series (Homeland, Suits) go on forever. Balance?",
    "id" : 880337828831518720,
    "created_at" : "2017-06-29 08:11:08 +0000",
    "user" : {
      "name" : "David Crespo",
      "screen_name" : "inFocusDCPhoto",
      "protected" : false,
      "id_str" : "756894403",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/953726733383061511\/lRYwh7MC_normal.jpg",
      "id" : 756894403,
      "verified" : false
    }
  },
  "id" : 880337991658422273,
  "created_at" : "2017-06-29 08:11:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 51, 61 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880331998354485249",
  "text" : "Any short-term Mandarin class for a 13-year old in #Singapore? Thanks",
  "id" : 880331998354485249,
  "created_at" : "2017-06-29 07:47:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 0, 14 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "880319058435948544",
  "geo" : { },
  "id_str" : "880326382214979584",
  "in_reply_to_user_id" : 815066809,
  "text" : "@ofmeowandbake The authority should put this down in every passport issued. :)",
  "id" : 880326382214979584,
  "in_reply_to_status_id" : 880319058435948544,
  "created_at" : "2017-06-29 07:25:39 +0000",
  "in_reply_to_screen_name" : "ofmeowandbake",
  "in_reply_to_user_id_str" : "815066809",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/EVn2WUcrv2",
      "expanded_url" : "https:\/\/twitter.com\/foxnic\/status\/879317111377727489",
      "display_url" : "twitter.com\/foxnic\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880323800532791298",
  "text" : "Imagine in a society where majority are like this lady, billions could be saved on healthcare. https:\/\/t.co\/EVn2WUcrv2",
  "id" : 880323800532791298,
  "created_at" : "2017-06-29 07:15:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elise Tan",
      "screen_name" : "elisetanyl",
      "indices" : [ 3, 14 ],
      "id_str" : "1352088325",
      "id" : 1352088325
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "banks",
      "indices" : [ 60, 66 ]
    }, {
      "text" : "e",
      "indices" : [ 80, 82 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880323137589493761",
  "text" : "RT @elisetanyl: Time for a change: Singapore: MAS to permit #banks to invest in #e-commerce, other non-finance businesses: https:\/\/t.co\/eim\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "banks",
        "indices" : [ 44, 50 ]
      }, {
        "text" : "e",
        "indices" : [ 64, 66 ]
      } ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/eimVuV83zT",
        "expanded_url" : "https:\/\/www.dealstreetasia.com\/stories\/singapore-mas-banks-non-finance-business-76138\/",
        "display_url" : "dealstreetasia.com\/stories\/singap\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "880322830172377090",
    "text" : "Time for a change: Singapore: MAS to permit #banks to invest in #e-commerce, other non-finance businesses: https:\/\/t.co\/eimVuV83zT",
    "id" : 880322830172377090,
    "created_at" : "2017-06-29 07:11:32 +0000",
    "user" : {
      "name" : "Elise Tan",
      "screen_name" : "elisetanyl",
      "protected" : false,
      "id_str" : "1352088325",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/628578120342212608\/NvCYxf3R_normal.jpg",
      "id" : 1352088325,
      "verified" : false
    }
  },
  "id" : 880323137589493761,
  "created_at" : "2017-06-29 07:12:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/59FBRIbH7l",
      "expanded_url" : "https:\/\/twitter.com\/genemurphy\/status\/880121632152530945",
      "display_url" : "twitter.com\/genemurphy\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880270427301347328",
  "text" : "Does Stripe counts? Thanks https:\/\/t.co\/59FBRIbH7l",
  "id" : 880270427301347328,
  "created_at" : "2017-06-29 03:43:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernard Leong",
      "screen_name" : "bleongcw",
      "indices" : [ 3, 12 ],
      "id_str" : "49672291",
      "id" : 49672291
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/7BzSk971PN",
      "expanded_url" : "https:\/\/bloom.bg\/2tlT0Ef?t=1498684636",
      "display_url" : "bloom.bg\/2tlT0Ef?t=1498\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880269410467852288",
  "text" : "RT @bleongcw: Tencent Rules China. The Problem Is the Rest of the World https:\/\/t.co\/7BzSk971PN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/7BzSk971PN",
        "expanded_url" : "https:\/\/bloom.bg\/2tlT0Ef?t=1498684636",
        "display_url" : "bloom.bg\/2tlT0Ef?t=1498\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "880245709550768129",
    "text" : "Tencent Rules China. The Problem Is the Rest of the World https:\/\/t.co\/7BzSk971PN",
    "id" : 880245709550768129,
    "created_at" : "2017-06-29 02:05:05 +0000",
    "user" : {
      "name" : "Bernard Leong",
      "screen_name" : "bernardleong",
      "protected" : false,
      "id_str" : "2193071",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/750746934801944576\/wFn6x_6g_normal.jpg",
      "id" : 2193071,
      "verified" : true
    }
  },
  "id" : 880269410467852288,
  "created_at" : "2017-06-29 03:39:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/paKafiowEd",
      "expanded_url" : "https:\/\/twitter.com\/Angelheartnight\/status\/879987919355817984",
      "display_url" : "twitter.com\/Angelheartnigh\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880268948230488064",
  "text" : "\uD83D\uDE02 https:\/\/t.co\/paKafiowEd",
  "id" : 880268948230488064,
  "created_at" : "2017-06-29 03:37:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880246674169356288",
  "text" : "As of 28th June 2017, exchange rate to Malaysia Ringgit\n1 SGD = RM 3.06\n1 EUR = RM 4.8\n1 GBP = RM 5.43",
  "id" : 880246674169356288,
  "created_at" : "2017-06-29 02:08:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/880245191894028290\/photo\/1",
      "indices" : [ 118, 141 ],
      "url" : "https:\/\/t.co\/dKt6hjnz40",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDdCVieXkAAxgpd.jpg",
      "id_str" : "880245189973086208",
      "id" : 880245189973086208,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDdCVieXkAAxgpd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/dKt6hjnz40"
    } ],
    "hashtags" : [ {
      "text" : "Malaysia",
      "indices" : [ 84, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/8TGJIYYJQj",
      "expanded_url" : "http:\/\/ift.tt\/2tqT85R",
      "display_url" : "ift.tt\/2tqT85R"
    } ]
  },
  "geo" : { },
  "id_str" : "880245191894028290",
  "text" : "Data Protection policy announcement at an insurance agency customer service centre. #Malaysia https:\/\/t.co\/8TGJIYYJQj https:\/\/t.co\/dKt6hjnz40",
  "id" : 880245191894028290,
  "created_at" : "2017-06-29 02:03:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "P\u00E1draig Belton",
      "screen_name" : "PadraigBelton",
      "indices" : [ 3, 17 ],
      "id_str" : "14671775",
      "id" : 14671775
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880232919742136320",
  "text" : "RT @PadraigBelton: A tiny interview I did today on how the magic money tree might just save Northern Ireland from direct rule. https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.google.com\/\" rel=\"nofollow\"\u003EGoogle\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/Ql3KosNMWi",
        "expanded_url" : "http:\/\/youtu.be\/zZBhdQCROA0?a",
        "display_url" : "youtu.be\/zZBhdQCROA0?a"
      } ]
    },
    "geo" : { },
    "id_str" : "880231832180195328",
    "text" : "A tiny interview I did today on how the magic money tree might just save Northern Ireland from direct rule. https:\/\/t.co\/Ql3KosNMWi",
    "id" : 880231832180195328,
    "created_at" : "2017-06-29 01:09:57 +0000",
    "user" : {
      "name" : "P\u00E1draig Belton",
      "screen_name" : "PadraigBelton",
      "protected" : false,
      "id_str" : "14671775",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/53815024\/patrick-belton_normal.jpg",
      "id" : 14671775,
      "verified" : false
    }
  },
  "id" : 880232919742136320,
  "created_at" : "2017-06-29 01:14:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/CP4lDMTMv9",
      "expanded_url" : "https:\/\/twitter.com\/ManelWatchIre\/status\/784797499882147840",
      "display_url" : "twitter.com\/ManelWatchIre\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880077987671982082",
  "text" : "RT @topgold: Helpful resource for those truly concerned about diversity on stage. https:\/\/t.co\/CP4lDMTMv9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/CP4lDMTMv9",
        "expanded_url" : "https:\/\/twitter.com\/ManelWatchIre\/status\/784797499882147840",
        "display_url" : "twitter.com\/ManelWatchIre\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "880068412025843712",
    "text" : "Helpful resource for those truly concerned about diversity on stage. https:\/\/t.co\/CP4lDMTMv9",
    "id" : 880068412025843712,
    "created_at" : "2017-06-28 14:20:34 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 880077987671982082,
  "created_at" : "2017-06-28 14:58:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880050930908971008",
  "text" : "Is Lazada the Amazon of South East Asia?",
  "id" : 880050930908971008,
  "created_at" : "2017-06-28 13:11:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/880042271990845440\/photo\/1",
      "indices" : [ 140, 163 ],
      "url" : "https:\/\/t.co\/eSNDGGhbT9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDaJx_XXcAAR_cA.jpg",
      "id_str" : "880042269113544704",
      "id" : 880042269113544704,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDaJx_XXcAAR_cA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eSNDGGhbT9"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/9S4U4hTiak",
      "expanded_url" : "http:\/\/ift.tt\/2sh7cyI",
      "display_url" : "ift.tt\/2sh7cyI"
    } ]
  },
  "geo" : { },
  "id_str" : "880042271990845440",
  "text" : "Each counters operates by an independent biz selling same range of mobile phone under one retail space. This is od\u2026 https:\/\/t.co\/9S4U4hTiak https:\/\/t.co\/eSNDGGhbT9",
  "id" : 880042271990845440,
  "created_at" : "2017-06-28 12:36:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R. Taylor Raborn",
      "screen_name" : "rtraborn",
      "indices" : [ 3, 12 ],
      "id_str" : "37722615",
      "id" : 37722615
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 16, 23 ]
    } ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/ZDmQ7JraC7",
      "expanded_url" : "https:\/\/twitter.com\/jessenleon\/status\/879443899068145665",
      "display_url" : "twitter.com\/jessenleon\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880033341889589248",
  "text" : "RT @rtraborn: \uD83D\uDE0E #rstats https:\/\/t.co\/ZDmQ7JraC7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 2, 9 ]
      } ],
      "urls" : [ {
        "indices" : [ 10, 33 ],
        "url" : "https:\/\/t.co\/ZDmQ7JraC7",
        "expanded_url" : "https:\/\/twitter.com\/jessenleon\/status\/879443899068145665",
        "display_url" : "twitter.com\/jessenleon\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879737045119356929",
    "text" : "\uD83D\uDE0E #rstats https:\/\/t.co\/ZDmQ7JraC7",
    "id" : 879737045119356929,
    "created_at" : "2017-06-27 16:23:50 +0000",
    "user" : {
      "name" : "R. Taylor Raborn",
      "screen_name" : "rtraborn",
      "protected" : false,
      "id_str" : "37722615",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/438047335482802176\/1J1Y4RF0_normal.png",
      "id" : 37722615,
      "verified" : false
    }
  },
  "id" : 880033341889589248,
  "created_at" : "2017-06-28 12:01:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "indices" : [ 3, 15 ],
      "id_str" : "979910827",
      "id" : 979910827
    }, {
      "name" : "Eventbrite Ireland",
      "screen_name" : "EventbriteIE",
      "indices" : [ 125, 138 ],
      "id_str" : "2251793672",
      "id" : 2251793672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/ugsYWPXb3F",
      "expanded_url" : "https:\/\/www.eventbrite.ie\/e\/tiger-street-eats-tickets-35434977973?utm-medium=discovery&utm-campaign=social&utm-content=attendeeshare&aff=estw&utm-source=tw&utm-term=listing",
      "display_url" : "eventbrite.ie\/e\/tiger-street\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880032320123674624",
  "text" : "RT @GeoffreyIRL: Singaporean street food event sold out. Is there a gap in the market, I ask myself. https:\/\/t.co\/ugsYWPXb3F @EventbriteIE",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Eventbrite Ireland",
        "screen_name" : "EventbriteIE",
        "indices" : [ 108, 121 ],
        "id_str" : "2251793672",
        "id" : 2251793672
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/ugsYWPXb3F",
        "expanded_url" : "https:\/\/www.eventbrite.ie\/e\/tiger-street-eats-tickets-35434977973?utm-medium=discovery&utm-campaign=social&utm-content=attendeeshare&aff=estw&utm-source=tw&utm-term=listing",
        "display_url" : "eventbrite.ie\/e\/tiger-street\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879899293334867969",
    "text" : "Singaporean street food event sold out. Is there a gap in the market, I ask myself. https:\/\/t.co\/ugsYWPXb3F @EventbriteIE",
    "id" : 879899293334867969,
    "created_at" : "2017-06-28 03:08:33 +0000",
    "user" : {
      "name" : "Geoffrey Keating",
      "screen_name" : "GeoffreyIRL",
      "protected" : false,
      "id_str" : "979910827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844699521489801216\/XKPaTr9h_normal.jpg",
      "id" : 979910827,
      "verified" : true
    }
  },
  "id" : 880032320123674624,
  "created_at" : "2017-06-28 11:57:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 12, 35 ],
      "url" : "https:\/\/t.co\/7OErAus9LL",
      "expanded_url" : "https:\/\/twitter.com\/smrt_singapore\/status\/880026280665563138",
      "display_url" : "twitter.com\/smrt_singapore\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880031520236331008",
  "text" : "Not again?! https:\/\/t.co\/7OErAus9LL",
  "id" : 880031520236331008,
  "created_at" : "2017-06-28 11:53:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Russell",
      "screen_name" : "jonrussell",
      "indices" : [ 3, 14 ],
      "id_str" : "6132422",
      "id" : 6132422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "880024056736628736",
  "text" : "RT @jonrussell: Alibaba\u2019s stake in Lazada has gone from 51% to 83% at a now $3.15B valuation \u2014 just Temasek and Lazada management held thei\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "879990279217963008",
    "geo" : { },
    "id_str" : "879990624480501761",
    "in_reply_to_user_id" : 6132422,
    "text" : "Alibaba\u2019s stake in Lazada has gone from 51% to 83% at a now $3.15B valuation \u2014 just Temasek and Lazada management held their shares",
    "id" : 879990624480501761,
    "in_reply_to_status_id" : 879990279217963008,
    "created_at" : "2017-06-28 09:11:28 +0000",
    "in_reply_to_screen_name" : "jonrussell",
    "in_reply_to_user_id_str" : "6132422",
    "user" : {
      "name" : "Jon Russell",
      "screen_name" : "jonrussell",
      "protected" : false,
      "id_str" : "6132422",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/794545496576536576\/6b4UeCSt_normal.jpg",
      "id" : 6132422,
      "verified" : true
    }
  },
  "id" : 880024056736628736,
  "created_at" : "2017-06-28 11:24:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "indices" : [ 3, 17 ],
      "id_str" : "16737418",
      "id" : 16737418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/UbVJWjKIx3",
      "expanded_url" : "http:\/\/www.irishtimes.com\/business\/financial-services\/bank-of-china-opens-dublin-branch-as-trade-links-develop-1.3136458?utm_source=dlvr.it&utm_medium=twitter",
      "display_url" : "irishtimes.com\/business\/finan\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "880023994249789440",
  "text" : "RT @IrishTimesBiz: Bank of China opens Dublin branch as trade links develop https:\/\/t.co\/UbVJWjKIx3",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/UbVJWjKIx3",
        "expanded_url" : "http:\/\/www.irishtimes.com\/business\/financial-services\/bank-of-china-opens-dublin-branch-as-trade-links-develop-1.3136458?utm_source=dlvr.it&utm_medium=twitter",
        "display_url" : "irishtimes.com\/business\/finan\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879998608841388033",
    "text" : "Bank of China opens Dublin branch as trade links develop https:\/\/t.co\/UbVJWjKIx3",
    "id" : 879998608841388033,
    "created_at" : "2017-06-28 09:43:12 +0000",
    "user" : {
      "name" : "Irish Times Business",
      "screen_name" : "IrishTimesBiz",
      "protected" : false,
      "id_str" : "16737418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/955744226062544898\/l9mPtwix_normal.jpg",
      "id" : 16737418,
      "verified" : true
    }
  },
  "id" : 880023994249789440,
  "created_at" : "2017-06-28 11:24:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 3, 14 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 32, 42 ]
    }, {
      "text" : "payment",
      "indices" : [ 85, 93 ]
    }, {
      "text" : "PayNow",
      "indices" : [ 103, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879998094120583172",
  "text" : "RT @CaroBowler: Check. It. Out! #Singapore takes a further step to cashless with P2P #payment platform #PayNow launching July. Exciting #fi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 16, 26 ]
      }, {
        "text" : "payment",
        "indices" : [ 69, 77 ]
      }, {
        "text" : "PayNow",
        "indices" : [ 87, 94 ]
      }, {
        "text" : "fintech",
        "indices" : [ 120, 128 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "879679716533428224",
    "text" : "Check. It. Out! #Singapore takes a further step to cashless with P2P #payment platform #PayNow launching July. Exciting #fintech progress!",
    "id" : 879679716533428224,
    "created_at" : "2017-06-27 12:36:02 +0000",
    "user" : {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "protected" : false,
      "id_str" : "4747144932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686824073884643329\/U-3rp4zx_normal.jpg",
      "id" : 4747144932,
      "verified" : false
    }
  },
  "id" : 879998094120583172,
  "created_at" : "2017-06-28 09:41:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/H8lBl776Ci",
      "expanded_url" : "https:\/\/twitter.com\/sazmartin\/status\/879984795354320898",
      "display_url" : "twitter.com\/sazmartin\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879996353765457923",
  "text" : "Consider Singapore as your hub into other Asia countries. https:\/\/t.co\/H8lBl776Ci",
  "id" : 879996353765457923,
  "created_at" : "2017-06-28 09:34:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 22, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879867816538537985",
  "text" : "Available for work in #Singapore from 4th July to 4th August 2017",
  "id" : 879867816538537985,
  "created_at" : "2017-06-28 01:03:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 70, 80 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879687129688702976",
  "text" : "Learnt from news that 94% people in Thailand uses Line messaging app. #Singapore",
  "id" : 879687129688702976,
  "created_at" : "2017-06-27 13:05:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eisen",
      "screen_name" : "eisen",
      "indices" : [ 3, 9 ],
      "id_str" : "3137941",
      "id" : 3137941
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/879356248868573185\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/JENnyI3OGY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDQZyk9VoAE4DEY.jpg",
      "id_str" : "879356183949189121",
      "id" : 879356183949189121,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDQZyk9VoAE4DEY.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/JENnyI3OGY"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/879356248868573185\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/JENnyI3OGY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDQZzQJUMAAxrla.jpg",
      "id_str" : "879356195542151168",
      "id" : 879356195542151168,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDQZzQJUMAAxrla.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/JENnyI3OGY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879669855338635264",
  "text" : "RT @eisen: Two more Brutalist buildings in Singapore: \n\nJurong Town Hall, 1974; the State Courts, 1975. https:\/\/t.co\/JENnyI3OGY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/879356248868573185\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/JENnyI3OGY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDQZyk9VoAE4DEY.jpg",
        "id_str" : "879356183949189121",
        "id" : 879356183949189121,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDQZyk9VoAE4DEY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JENnyI3OGY"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/879356248868573185\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/JENnyI3OGY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDQZzQJUMAAxrla.jpg",
        "id_str" : "879356195542151168",
        "id" : 879356195542151168,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDQZzQJUMAAxrla.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JENnyI3OGY"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "878628576983097344",
    "geo" : { },
    "id_str" : "879356248868573185",
    "in_reply_to_user_id" : 3137941,
    "text" : "Two more Brutalist buildings in Singapore: \n\nJurong Town Hall, 1974; the State Courts, 1975. https:\/\/t.co\/JENnyI3OGY",
    "id" : 879356248868573185,
    "in_reply_to_status_id" : 878628576983097344,
    "created_at" : "2017-06-26 15:10:42 +0000",
    "in_reply_to_screen_name" : "eisen",
    "in_reply_to_user_id_str" : "3137941",
    "user" : {
      "name" : "Eisen",
      "screen_name" : "eisen",
      "protected" : false,
      "id_str" : "3137941",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992783130837708800\/sIDbrsWw_normal.jpg",
      "id" : 3137941,
      "verified" : false
    }
  },
  "id" : 879669855338635264,
  "created_at" : "2017-06-27 11:56:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eisen",
      "screen_name" : "eisen",
      "indices" : [ 3, 9 ],
      "id_str" : "3137941",
      "id" : 3137941
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879669840335650816",
  "text" : "RT @eisen: Four Brutalist buildings in Singapore:\n\nGolden Mile Complex, 1973; People's Park Complex, 1973; Shaw Tower, 1976; TripleOne Some\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/878628576983097344\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/o0DnYVQhAr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDGDxWbVYAAhbWb.jpg",
        "id_str" : "878628286171013120",
        "id" : 878628286171013120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDGDxWbVYAAhbWb.jpg",
        "sizes" : [ {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 682,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o0DnYVQhAr"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/878628576983097344\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/o0DnYVQhAr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDGDyHcVYAAKxpZ.jpg",
        "id_str" : "878628299328544768",
        "id" : 878628299328544768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDGDyHcVYAAKxpZ.jpg",
        "sizes" : [ {
          "h" : 643,
          "resize" : "fit",
          "w" : 987
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 443,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 643,
          "resize" : "fit",
          "w" : 987
        }, {
          "h" : 643,
          "resize" : "fit",
          "w" : 987
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o0DnYVQhAr"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/878628576983097344\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/o0DnYVQhAr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDGDyzBUIAAP9nz.jpg",
        "id_str" : "878628311026376704",
        "id" : 878628311026376704,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDGDyzBUIAAP9nz.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o0DnYVQhAr"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/eisen\/status\/878628576983097344\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/o0DnYVQhAr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDGDzm_U0AIdMDX.jpg",
        "id_str" : "878628324976676866",
        "id" : 878628324976676866,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDGDzm_U0AIdMDX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o0DnYVQhAr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "878628160065028096",
    "geo" : { },
    "id_str" : "878628576983097344",
    "in_reply_to_user_id" : 3137941,
    "text" : "Four Brutalist buildings in Singapore:\n\nGolden Mile Complex, 1973; People's Park Complex, 1973; Shaw Tower, 1976; TripleOne Somerset, 1977. https:\/\/t.co\/o0DnYVQhAr",
    "id" : 878628576983097344,
    "in_reply_to_status_id" : 878628160065028096,
    "created_at" : "2017-06-24 14:59:11 +0000",
    "in_reply_to_screen_name" : "eisen",
    "in_reply_to_user_id_str" : "3137941",
    "user" : {
      "name" : "Eisen",
      "screen_name" : "eisen",
      "protected" : false,
      "id_str" : "3137941",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/992783130837708800\/sIDbrsWw_normal.jpg",
      "id" : 3137941,
      "verified" : false
    }
  },
  "id" : 879669840335650816,
  "created_at" : "2017-06-27 11:56:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/879666509647368200\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/qMshLUPqoE",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDU0BxtXYAAV4z9.jpg",
      "id_str" : "879666507348926464",
      "id" : 879666507348926464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDU0BxtXYAAV4z9.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/qMshLUPqoE"
    } ],
    "hashtags" : [ {
      "text" : "singapore",
      "indices" : [ 60, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/yWNBy82Hpf",
      "expanded_url" : "http:\/\/ift.tt\/2tfB8Ly",
      "display_url" : "ift.tt\/2tfB8Ly"
    } ]
  },
  "geo" : { },
  "id_str" : "879666509647368200",
  "text" : "Tap out for more data for better public transport services. #singapore https:\/\/t.co\/yWNBy82Hpf https:\/\/t.co\/qMshLUPqoE",
  "id" : 879666509647368200,
  "created_at" : "2017-06-27 11:43:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879665353856401410",
  "text" : "Can't get my head around why a smartphone offer  2 WeChat app as a feature? One for lover and one for family it is?\uD83D\uDE0E",
  "id" : 879665353856401410,
  "created_at" : "2017-06-27 11:38:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879662011310686208",
  "text" : "Will u be surprised if I tell you that some dim sum items in Manchester taste better than those in Malaysia?",
  "id" : 879662011310686208,
  "created_at" : "2017-06-27 11:25:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "malcolm-aim\u00E9",
      "screen_name" : "malcolmaime",
      "indices" : [ 3, 15 ],
      "id_str" : "96427897",
      "id" : 96427897
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879369784948703232",
  "text" : "RT @malcolmaime: if buzzfeed has ever embedded one of ur tweets in an article u can put \"freelance writer for buzzfeed\" on ur resume",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878634836872790016",
    "text" : "if buzzfeed has ever embedded one of ur tweets in an article u can put \"freelance writer for buzzfeed\" on ur resume",
    "id" : 878634836872790016,
    "created_at" : "2017-06-24 15:24:04 +0000",
    "user" : {
      "name" : "malcolm-aim\u00E9",
      "screen_name" : "malcolmaime",
      "protected" : false,
      "id_str" : "96427897",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037486747117006852\/0hRU0RM1_normal.jpg",
      "id" : 96427897,
      "verified" : false
    }
  },
  "id" : 879369784948703232,
  "created_at" : "2017-06-26 16:04:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Writer Louis Leung",
      "screen_name" : "ricedaddy7",
      "indices" : [ 3, 14 ],
      "id_str" : "1842701690",
      "id" : 1842701690
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879368998390857734",
  "text" : "RT @ricedaddy7: I always hold the door open for ladies, but they never want to get in the van...",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "879325170854395904",
    "text" : "I always hold the door open for ladies, but they never want to get in the van...",
    "id" : 879325170854395904,
    "created_at" : "2017-06-26 13:07:12 +0000",
    "user" : {
      "name" : "Writer Louis Leung",
      "screen_name" : "ricedaddy7",
      "protected" : false,
      "id_str" : "1842701690",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1015932733082750978\/nOxCjxwQ_normal.jpg",
      "id" : 1842701690,
      "verified" : false
    }
  },
  "id" : 879368998390857734,
  "created_at" : "2017-06-26 16:01:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "indices" : [ 3, 11 ],
      "id_str" : "13311832",
      "id" : 13311832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/93wqdft4aW",
      "expanded_url" : "https:\/\/twitter.com\/ianmcclellland\/status\/879146365481254912",
      "display_url" : "twitter.com\/ianmcclellland\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879367242412048384",
  "text" : "RT @dr_pete: How can Justin Trudeau govern when strange women are just leaping on him? https:\/\/t.co\/93wqdft4aW",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/93wqdft4aW",
        "expanded_url" : "https:\/\/twitter.com\/ianmcclellland\/status\/879146365481254912",
        "display_url" : "twitter.com\/ianmcclellland\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879360662799044608",
    "text" : "How can Justin Trudeau govern when strange women are just leaping on him? https:\/\/t.co\/93wqdft4aW",
    "id" : 879360662799044608,
    "created_at" : "2017-06-26 15:28:14 +0000",
    "user" : {
      "name" : "Dr. Pete Meyers",
      "screen_name" : "dr_pete",
      "protected" : false,
      "id_str" : "13311832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/542152616487038976\/-UUMowce_normal.jpeg",
      "id" : 13311832,
      "verified" : false
    }
  },
  "id" : 879367242412048384,
  "created_at" : "2017-06-26 15:54:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ben Poole",
      "screen_name" : "poolio",
      "indices" : [ 3, 10 ],
      "id_str" : "14103014",
      "id" : 14103014
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879366896130314241",
  "text" : "RT @poolio: Free lunch theorem: for any idea, there exists a dataset where that idea performs well.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878060006968406016",
    "text" : "Free lunch theorem: for any idea, there exists a dataset where that idea performs well.",
    "id" : 878060006968406016,
    "created_at" : "2017-06-23 01:19:53 +0000",
    "user" : {
      "name" : "Ben Poole",
      "screen_name" : "poolio",
      "protected" : false,
      "id_str" : "14103014",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/577742437621235712\/GCgLg1rC_normal.png",
      "id" : 14103014,
      "verified" : false
    }
  },
  "id" : 879366896130314241,
  "created_at" : "2017-06-26 15:53:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 3, 14 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    }, {
      "name" : "KPMG",
      "screen_name" : "KPMG",
      "indices" : [ 30, 35 ],
      "id_str" : "267939430",
      "id" : 267939430
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ASEAN",
      "indices" : [ 50, 56 ]
    }, {
      "text" : "fintech",
      "indices" : [ 57, 65 ]
    }, {
      "text" : "M2020EU",
      "indices" : [ 127, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879338631332155393",
  "text" : "RT @CaroBowler: Great to hear @KPMG talking about #ASEAN #fintech. There is a lot more in Asia than just China, come find out! #M2020EU #M2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "KPMG",
        "screen_name" : "KPMG",
        "indices" : [ 14, 19 ],
        "id_str" : "267939430",
        "id" : 267939430
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ASEAN",
        "indices" : [ 34, 40 ]
      }, {
        "text" : "fintech",
        "indices" : [ 41, 49 ]
      }, {
        "text" : "M2020EU",
        "indices" : [ 111, 119 ]
      }, {
        "text" : "M2020Asia",
        "indices" : [ 120, 130 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "879338023594455040",
    "text" : "Great to hear @KPMG talking about #ASEAN #fintech. There is a lot more in Asia than just China, come find out! #M2020EU #M2020Asia",
    "id" : 879338023594455040,
    "created_at" : "2017-06-26 13:58:16 +0000",
    "user" : {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "protected" : false,
      "id_str" : "4747144932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686824073884643329\/U-3rp4zx_normal.jpg",
      "id" : 4747144932,
      "verified" : false
    }
  },
  "id" : 879338631332155393,
  "created_at" : "2017-06-26 14:00:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Terence Cosgrave",
      "screen_name" : "terencecosgrave",
      "indices" : [ 3, 19 ],
      "id_str" : "256030065",
      "id" : 256030065
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/toX9LLOKJa",
      "expanded_url" : "https:\/\/twitter.com\/irishexaminer\/status\/879317355322638336",
      "display_url" : "twitter.com\/irishexaminer\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879327708009832448",
  "text" : "RT @terencecosgrave: Washing the cars, washing the money :-) https:\/\/t.co\/toX9LLOKJa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/toX9LLOKJa",
        "expanded_url" : "https:\/\/twitter.com\/irishexaminer\/status\/879317355322638336",
        "display_url" : "twitter.com\/irishexaminer\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879317806432628738",
    "text" : "Washing the cars, washing the money :-) https:\/\/t.co\/toX9LLOKJa",
    "id" : 879317806432628738,
    "created_at" : "2017-06-26 12:37:56 +0000",
    "user" : {
      "name" : "Terence Cosgrave",
      "screen_name" : "terencecosgrave",
      "protected" : false,
      "id_str" : "256030065",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1007013784580476930\/bYYr8g1r_normal.jpg",
      "id" : 256030065,
      "verified" : false
    }
  },
  "id" : 879327708009832448,
  "created_at" : "2017-06-26 13:17:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Not TMConnects",
      "screen_name" : "TmConnects",
      "indices" : [ 0, 11 ],
      "id_str" : "955707784288387072",
      "id" : 955707784288387072
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879312139990712320",
  "in_reply_to_user_id" : 337579340,
  "text" : "@TMConnects understand there is limit to the volume of data users can download. What the limit for Streamyx 2mbps plan? Thank You.",
  "id" : 879312139990712320,
  "created_at" : "2017-06-26 12:15:25 +0000",
  "in_reply_to_screen_name" : "helpmeunifi",
  "in_reply_to_user_id_str" : "337579340",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CIO.com",
      "screen_name" : "CIOonline",
      "indices" : [ 71, 81 ],
      "id_str" : "22873424",
      "id" : 22873424
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/e0tU22QFOp",
      "expanded_url" : "http:\/\/www.cio.com\/article\/3198472\/careers-staffing\/the-hard-truths-of-navigating-ageism-in-it.html",
      "display_url" : "cio.com\/article\/319847\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879254318230118401",
  "text" : "The hard truths of navigating ageism in IT https:\/\/t.co\/e0tU22QFOp via @CIOonline",
  "id" : 879254318230118401,
  "created_at" : "2017-06-26 08:25:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "indices" : [ 3, 12 ],
      "id_str" : "45756727",
      "id" : 45756727
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/PQ6cMF2Amb",
      "expanded_url" : "http:\/\/sc.mp\/IXZ2nR",
      "display_url" : "sc.mp\/IXZ2nR"
    } ]
  },
  "geo" : { },
  "id_str" : "879237189522997248",
  "text" : "RT @LividEye: 'Hire as many women as possible' - Jack Ma's secret of Alibaba's success https:\/\/t.co\/PQ6cMF2Amb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/PQ6cMF2Amb",
        "expanded_url" : "http:\/\/sc.mp\/IXZ2nR",
        "display_url" : "sc.mp\/IXZ2nR"
      } ]
    },
    "geo" : { },
    "id_str" : "878100929572093954",
    "text" : "'Hire as many women as possible' - Jack Ma's secret of Alibaba's success https:\/\/t.co\/PQ6cMF2Amb",
    "id" : 878100929572093954,
    "created_at" : "2017-06-23 04:02:30 +0000",
    "user" : {
      "name" : "LividEye Yoshikawa",
      "screen_name" : "LividEye",
      "protected" : false,
      "id_str" : "45756727",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/973927451775545344\/8ZHNGFY8_normal.jpg",
      "id" : 45756727,
      "verified" : false
    }
  },
  "id" : 879237189522997248,
  "created_at" : "2017-06-26 07:17:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "LTA",
      "screen_name" : "LTAsg",
      "indices" : [ 3, 9 ],
      "id_str" : "68321763",
      "id" : 68321763
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "NUS",
      "indices" : [ 32, 36 ]
    }, {
      "text" : "Dover",
      "indices" : [ 55, 61 ]
    }, {
      "text" : "BuonaVista",
      "indices" : [ 68, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879233601245650944",
  "text" : "RT @LTAsg: A larger test bed in #NUS, SG Science Park, #Dover &amp; #BuonaVista provides more on-road scenarios for better tech development. #S\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LTAsg\/status\/878177587574390785\/photo\/1",
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/QjtaIwKxvu",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DC_pzk0VYAE9Syv.jpg",
        "id_str" : "878177524626317313",
        "id" : 878177524626317313,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC_pzk0VYAE9Syv.jpg",
        "sizes" : [ {
          "h" : 827,
          "resize" : "fit",
          "w" : 802
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 827,
          "resize" : "fit",
          "w" : 802
        }, {
          "h" : 827,
          "resize" : "fit",
          "w" : 802
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 659
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QjtaIwKxvu"
      } ],
      "hashtags" : [ {
        "text" : "NUS",
        "indices" : [ 21, 25 ]
      }, {
        "text" : "Dover",
        "indices" : [ 44, 50 ]
      }, {
        "text" : "BuonaVista",
        "indices" : [ 57, 68 ]
      }, {
        "text" : "SmartNation",
        "indices" : [ 130, 142 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878177587574390785",
    "text" : "A larger test bed in #NUS, SG Science Park, #Dover &amp; #BuonaVista provides more on-road scenarios for better tech development. #SmartNation https:\/\/t.co\/QjtaIwKxvu",
    "id" : 878177587574390785,
    "created_at" : "2017-06-23 09:07:07 +0000",
    "user" : {
      "name" : "LTA",
      "screen_name" : "LTAsg",
      "protected" : false,
      "id_str" : "68321763",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/582352199814008832\/GVZJ5FiC_normal.jpg",
      "id" : 68321763,
      "verified" : true
    }
  },
  "id" : 879233601245650944,
  "created_at" : "2017-06-26 07:03:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "indices" : [ 3, 13 ],
      "id_str" : "77901568",
      "id" : 77901568
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "edtech",
      "indices" : [ 38, 45 ]
    }, {
      "text" : "startupweekend",
      "indices" : [ 46, 61 ]
    }, {
      "text" : "Dublin",
      "indices" : [ 70, 77 ]
    } ],
    "urls" : [ {
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/2qQ4SIWKi4",
      "expanded_url" : "https:\/\/twitter.com\/RachelFay_\/status\/867683644558774273",
      "display_url" : "twitter.com\/RachelFay_\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879231680552226816",
  "text" : "RT @allentv4u: Keep an eye out for an #edtech #startupweekend soon in #Dublin! https:\/\/t.co\/2qQ4SIWKi4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "edtech",
        "indices" : [ 23, 30 ]
      }, {
        "text" : "startupweekend",
        "indices" : [ 31, 46 ]
      }, {
        "text" : "Dublin",
        "indices" : [ 55, 62 ]
      } ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/2qQ4SIWKi4",
        "expanded_url" : "https:\/\/twitter.com\/RachelFay_\/status\/867683644558774273",
        "display_url" : "twitter.com\/RachelFay_\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "867831037468004353",
    "text" : "Keep an eye out for an #edtech #startupweekend soon in #Dublin! https:\/\/t.co\/2qQ4SIWKi4",
    "id" : 867831037468004353,
    "created_at" : "2017-05-25 19:53:37 +0000",
    "user" : {
      "name" : "Allen ThomasVarghese",
      "screen_name" : "allentv4u",
      "protected" : false,
      "id_str" : "77901568",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/787980171202686976\/QbSu0IKB_normal.jpg",
      "id" : 77901568,
      "verified" : false
    }
  },
  "id" : 879231680552226816,
  "created_at" : "2017-06-26 06:55:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lovin Dublin",
      "screen_name" : "LovinDublin",
      "indices" : [ 111, 123 ],
      "id_str" : "18760904",
      "id" : 18760904
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/mkke3MgaNC",
      "expanded_url" : "https:\/\/lovindublin.com\/dublin\/this-new-street-food-experience-brings-something-entirely-new-to-dublin",
      "display_url" : "lovindublin.com\/dublin\/this-ne\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879231206423908352",
  "text" : "This New Singapore Street Food Experience Brings Something Entirely New To Dublin  https:\/\/t.co\/mkke3MgaNC via @lovindublin",
  "id" : 879231206423908352,
  "created_at" : "2017-06-26 06:53:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "indices" : [ 3, 16 ],
      "id_str" : "2300614526",
      "id" : 2300614526
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 71, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/XnAeFW5KmZ",
      "expanded_url" : "http:\/\/pacha.hk\/2017-06-24_working_with_spss_data_in_r.html",
      "display_url" : "pacha.hk\/2017-06-24_wor\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879216359120359428",
  "text" : "RT @pachamaltese: Working With SPSS\u00A9 Data in R https:\/\/t.co\/XnAeFW5KmZ #rstats",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 53, 60 ]
      } ],
      "urls" : [ {
        "indices" : [ 29, 52 ],
        "url" : "https:\/\/t.co\/XnAeFW5KmZ",
        "expanded_url" : "http:\/\/pacha.hk\/2017-06-24_working_with_spss_data_in_r.html",
        "display_url" : "pacha.hk\/2017-06-24_wor\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "878737674466721793",
    "text" : "Working With SPSS\u00A9 Data in R https:\/\/t.co\/XnAeFW5KmZ #rstats",
    "id" : 878737674466721793,
    "created_at" : "2017-06-24 22:12:42 +0000",
    "user" : {
      "name" : "Pach\u00E1 \u5E15\u590F",
      "screen_name" : "pachamaltese",
      "protected" : false,
      "id_str" : "2300614526",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/955616100645068800\/nexQrTFV_normal.jpg",
      "id" : 2300614526,
      "verified" : false
    }
  },
  "id" : 879216359120359428,
  "created_at" : "2017-06-26 05:54:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cam Carlyle",
      "screen_name" : "CarlyleCam",
      "indices" : [ 3, 14 ],
      "id_str" : "365545586",
      "id" : 365545586
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/CarlyleCam\/status\/879198084403802112\/photo\/1",
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/GZB8g8VJAr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDOJtKwWAAAFipP.jpg",
      "id_str" : "879197761341620224",
      "id" : 879197761341620224,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDOJtKwWAAAFipP.jpg",
      "sizes" : [ {
        "h" : 700,
        "resize" : "fit",
        "w" : 389
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 378
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 389
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 389
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GZB8g8VJAr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879206351104131074",
  "text" : "RT @CarlyleCam: Every coding tutorial ever written https:\/\/t.co\/GZB8g8VJAr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CarlyleCam\/status\/879198084403802112\/photo\/1",
        "indices" : [ 35, 58 ],
        "url" : "https:\/\/t.co\/GZB8g8VJAr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDOJtKwWAAAFipP.jpg",
        "id_str" : "879197761341620224",
        "id" : 879197761341620224,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDOJtKwWAAAFipP.jpg",
        "sizes" : [ {
          "h" : 700,
          "resize" : "fit",
          "w" : 389
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 378
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 389
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 389
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GZB8g8VJAr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "879198084403802112",
    "text" : "Every coding tutorial ever written https:\/\/t.co\/GZB8g8VJAr",
    "id" : 879198084403802112,
    "created_at" : "2017-06-26 04:42:12 +0000",
    "user" : {
      "name" : "Cam Carlyle",
      "screen_name" : "CarlyleCam",
      "protected" : false,
      "id_str" : "365545586",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/865680248926601216\/HssDzCOm_normal.jpg",
      "id" : 365545586,
      "verified" : false
    }
  },
  "id" : 879206351104131074,
  "created_at" : "2017-06-26 05:15:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/879189491453489152\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/oh9vXrDTUe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDOCLsBXcAAHb0J.jpg",
      "id_str" : "879189489574440960",
      "id" : 879189489574440960,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDOCLsBXcAAHb0J.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oh9vXrDTUe"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/3tA6Ib0LPm",
      "expanded_url" : "http:\/\/ift.tt\/2taRuEZ",
      "display_url" : "ift.tt\/2taRuEZ"
    } ]
  },
  "geo" : { },
  "id_str" : "879189491453489152",
  "text" : "Mandarin Chinese from AI to Jack Ma to Chinese e-commerce holiday Singles' Day. https:\/\/t.co\/3tA6Ib0LPm https:\/\/t.co\/oh9vXrDTUe",
  "id" : 879189491453489152,
  "created_at" : "2017-06-26 04:08:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Cleland",
      "screen_name" : "pulsedaniel",
      "indices" : [ 3, 15 ],
      "id_str" : "831593475829268481",
      "id" : 831593475829268481
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pulsedaniel\/status\/878281687632924672\/photo\/1",
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/3omDYKKQHU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBIicKXcAAqOtn.jpg",
      "id_str" : "878281683849670656",
      "id" : 878281683849670656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBIicKXcAAqOtn.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 767,
        "resize" : "fit",
        "w" : 614
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 544
      }, {
        "h" : 767,
        "resize" : "fit",
        "w" : 614
      }, {
        "h" : 767,
        "resize" : "fit",
        "w" : 614
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3omDYKKQHU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879153975106207744",
  "text" : "RT @pulsedaniel: Looking up in Hong Kong https:\/\/t.co\/3omDYKKQHU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pulsedaniel\/status\/878281687632924672\/photo\/1",
        "indices" : [ 24, 47 ],
        "url" : "https:\/\/t.co\/3omDYKKQHU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBIicKXcAAqOtn.jpg",
        "id_str" : "878281683849670656",
        "id" : 878281683849670656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBIicKXcAAqOtn.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 767,
          "resize" : "fit",
          "w" : 614
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 544
        }, {
          "h" : 767,
          "resize" : "fit",
          "w" : 614
        }, {
          "h" : 767,
          "resize" : "fit",
          "w" : 614
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3omDYKKQHU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878281687632924672",
    "text" : "Looking up in Hong Kong https:\/\/t.co\/3omDYKKQHU",
    "id" : 878281687632924672,
    "created_at" : "2017-06-23 16:00:46 +0000",
    "user" : {
      "name" : "Daniel Cleland",
      "screen_name" : "pulsedaniel",
      "protected" : false,
      "id_str" : "831593475829268481",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/940937064492789760\/JO0DuykR_normal.jpg",
      "id" : 831593475829268481,
      "verified" : false
    }
  },
  "id" : 879153975106207744,
  "created_at" : "2017-06-26 01:46:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bas Holtrop",
      "screen_name" : "sbholtrop",
      "indices" : [ 3, 13 ],
      "id_str" : "23763236",
      "id" : 23763236
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/sbholtrop\/status\/878611398527520768\/photo\/1",
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/cY5g6ZFIZt",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDF0ZvSXYAI3XGc.jpg",
      "id_str" : "878611387853004802",
      "id" : 878611387853004802,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDF0ZvSXYAI3XGc.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 397
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 560
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 560
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 560
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cY5g6ZFIZt"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879150976954802176",
  "text" : "RT @sbholtrop: The mobile web in 2017 https:\/\/t.co\/cY5g6ZFIZt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sbholtrop\/status\/878611398527520768\/photo\/1",
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/cY5g6ZFIZt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDF0ZvSXYAI3XGc.jpg",
        "id_str" : "878611387853004802",
        "id" : 878611387853004802,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDF0ZvSXYAI3XGc.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 397
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 560
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 560
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 560
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cY5g6ZFIZt"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878611398527520768",
    "text" : "The mobile web in 2017 https:\/\/t.co\/cY5g6ZFIZt",
    "id" : 878611398527520768,
    "created_at" : "2017-06-24 13:50:55 +0000",
    "user" : {
      "name" : "Bas Holtrop",
      "screen_name" : "sbholtrop",
      "protected" : false,
      "id_str" : "23763236",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/674966751063334913\/khZ77jRf_normal.jpg",
      "id" : 23763236,
      "verified" : false
    }
  },
  "id" : 879150976954802176,
  "created_at" : "2017-06-26 01:35:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LoveIsLove",
      "indices" : [ 64, 75 ]
    }, {
      "text" : "Pride2017",
      "indices" : [ 76, 86 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879149519912419328",
  "text" : "Hope the love is extend also to the minority in your community. #LoveIsLove #Pride2017",
  "id" : 879149519912419328,
  "created_at" : "2017-06-26 01:29:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/879148598033862658\/photo\/1",
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/K1S7kpiRLR",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDNc_VQWsAE7e1N.jpg",
      "id_str" : "879148595374632961",
      "id" : 879148595374632961,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDNc_VQWsAE7e1N.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/K1S7kpiRLR"
    } ],
    "hashtags" : [ {
      "text" : "singapore",
      "indices" : [ 42, 52 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/wbwZkOf6cz",
      "expanded_url" : "http:\/\/ift.tt\/2rRGlVQ",
      "display_url" : "ift.tt\/2rRGlVQ"
    } ]
  },
  "geo" : { },
  "id_str" : "879148598033862658",
  "text" : "Housing a nation in high rise apartments. #singapore https:\/\/t.co\/wbwZkOf6cz https:\/\/t.co\/K1S7kpiRLR",
  "id" : 879148598033862658,
  "created_at" : "2017-06-26 01:25:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RT\u00C9 ClaireByrneLive",
      "screen_name" : "ClaireByrneLive",
      "indices" : [ 3, 19 ],
      "id_str" : "2900855145",
      "id" : 2900855145
    }, {
      "name" : "Orla Guerin",
      "screen_name" : "OrlaGuerin",
      "indices" : [ 28, 39 ],
      "id_str" : "2435596224",
      "id" : 2435596224
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/5EnW5e5W5u",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/world-middle-east-40400543",
      "display_url" : "bbc.co.uk\/news\/world-mid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879074980385832960",
  "text" : "RT @ClaireByrneLive: WATCH: @OrlaGuerin dodging IS snipers on Mosul frontline via@BBCNews  https:\/\/t.co\/5EnW5e5W5u",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Orla Guerin",
        "screen_name" : "OrlaGuerin",
        "indices" : [ 7, 18 ],
        "id_str" : "2435596224",
        "id" : 2435596224
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/5EnW5e5W5u",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/world-middle-east-40400543",
        "display_url" : "bbc.co.uk\/news\/world-mid\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879069895580889088",
    "text" : "WATCH: @OrlaGuerin dodging IS snipers on Mosul frontline via@BBCNews  https:\/\/t.co\/5EnW5e5W5u",
    "id" : 879069895580889088,
    "created_at" : "2017-06-25 20:12:50 +0000",
    "user" : {
      "name" : "RT\u00C9 ClaireByrneLive",
      "screen_name" : "ClaireByrneLive",
      "protected" : false,
      "id_str" : "2900855145",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/557191239280254977\/6_y5Yert_normal.jpeg",
      "id" : 2900855145,
      "verified" : true
    }
  },
  "id" : 879074980385832960,
  "created_at" : "2017-06-25 20:33:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Daniel Lin",
      "screen_name" : "danwlin",
      "indices" : [ 3, 11 ],
      "id_str" : "144408677",
      "id" : 144408677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879070719576915968",
  "text" : "RT @danwlin: 2016: I can\u2019t wait for this year to end. Next year can\u2019t be worse.\n\n2017: Remember to call your senators and ask them not to k\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878668027201556481",
    "text" : "2016: I can\u2019t wait for this year to end. Next year can\u2019t be worse.\n\n2017: Remember to call your senators and ask them not to kill you",
    "id" : 878668027201556481,
    "created_at" : "2017-06-24 17:35:57 +0000",
    "user" : {
      "name" : "Daniel Lin",
      "screen_name" : "danwlin",
      "protected" : false,
      "id_str" : "144408677",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/795435685184409600\/-zrffrTa_normal.jpg",
      "id" : 144408677,
      "verified" : false
    }
  },
  "id" : 879070719576915968,
  "created_at" : "2017-06-25 20:16:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNNMoney",
      "screen_name" : "CNNMoney",
      "indices" : [ 67, 76 ],
      "id_str" : "16184358",
      "id" : 16184358
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/coVDKE2erg",
      "expanded_url" : "http:\/\/cnnmon.ie\/2qw6ABf",
      "display_url" : "cnnmon.ie\/2qw6ABf"
    } ]
  },
  "geo" : { },
  "id_str" : "879070319411027970",
  "text" : "The coal miner who became a data miner https:\/\/t.co\/coVDKE2erg via @CNNMoney",
  "id" : 879070319411027970,
  "created_at" : "2017-06-25 20:14:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "chicken onnastick",
      "screen_name" : "eSZactLee",
      "indices" : [ 3, 13 ],
      "id_str" : "15279755",
      "id" : 15279755
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/UDqkYsFVvl",
      "expanded_url" : "https:\/\/twitter.com\/thisisinsider\/status\/718078247024533508",
      "display_url" : "twitter.com\/thisisinsider\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879068124783312896",
  "text" : "RT @eSZactLee: Oh they just discovered mos burger' rice burger https:\/\/t.co\/UDqkYsFVvl",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 48, 71 ],
        "url" : "https:\/\/t.co\/UDqkYsFVvl",
        "expanded_url" : "https:\/\/twitter.com\/thisisinsider\/status\/718078247024533508",
        "display_url" : "twitter.com\/thisisinsider\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "878600888268447747",
    "text" : "Oh they just discovered mos burger' rice burger https:\/\/t.co\/UDqkYsFVvl",
    "id" : 878600888268447747,
    "created_at" : "2017-06-24 13:09:10 +0000",
    "user" : {
      "name" : "chicken onnastick",
      "screen_name" : "eSZactLee",
      "protected" : false,
      "id_str" : "15279755",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/978629475326820353\/Yu0BxyK__normal.jpg",
      "id" : 15279755,
      "verified" : false
    }
  },
  "id" : 879068124783312896,
  "created_at" : "2017-06-25 20:05:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/878820691314855936\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/yYl1krjVlJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDIywq1U0AAzxZq.jpg",
      "id_str" : "878820689003728896",
      "id" : 878820689003728896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDIywq1U0AAzxZq.jpg",
      "sizes" : [ {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 363,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yYl1krjVlJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/2rugvt6evz",
      "expanded_url" : "https:\/\/cna.asia\/2t8kGMN",
      "display_url" : "cna.asia\/2t8kGMN"
    } ]
  },
  "geo" : { },
  "id_str" : "879066918031769600",
  "text" : "RT @ChannelNewsAsia: Two decades after handover, scant love for China among Hong Kong youth https:\/\/t.co\/2rugvt6evz https:\/\/t.co\/yYl1krjVlJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/878820691314855936\/photo\/1",
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/yYl1krjVlJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDIywq1U0AAzxZq.jpg",
        "id_str" : "878820689003728896",
        "id" : 878820689003728896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDIywq1U0AAzxZq.jpg",
        "sizes" : [ {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yYl1krjVlJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/2rugvt6evz",
        "expanded_url" : "https:\/\/cna.asia\/2t8kGMN",
        "display_url" : "cna.asia\/2t8kGMN"
      } ]
    },
    "geo" : { },
    "id_str" : "878820691314855936",
    "text" : "Two decades after handover, scant love for China among Hong Kong youth https:\/\/t.co\/2rugvt6evz https:\/\/t.co\/yYl1krjVlJ",
    "id" : 878820691314855936,
    "created_at" : "2017-06-25 03:42:35 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 879066918031769600,
  "created_at" : "2017-06-25 20:01:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Albert Ruiz",
      "screen_name" : "databreak",
      "indices" : [ 3, 13 ],
      "id_str" : "2869056617",
      "id" : 2869056617
    }, {
      "name" : "Chromebook.Net",
      "screen_name" : "ChromebookNet",
      "indices" : [ 97, 111 ],
      "id_str" : "303688490",
      "id" : 303688490
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Chromebook",
      "indices" : [ 40, 51 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/P7nT6jrBjN",
      "expanded_url" : "http:\/\/buff.ly\/2t4SOsC",
      "display_url" : "buff.ly\/2t4SOsC"
    } ]
  },
  "geo" : { },
  "id_str" : "879065639876304896",
  "text" : "RT @databreak: Should you buy a 2GB RAM #Chromebook or do you really need 4GB? See this guide by @chromebooknet https:\/\/t.co\/P7nT6jrBjN htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Chromebook.Net",
        "screen_name" : "ChromebookNet",
        "indices" : [ 82, 96 ],
        "id_str" : "303688490",
        "id" : 303688490
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/databreak\/status\/878991692334084096\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/irxuB6QNrK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDLOSN0XcAAnp5y.jpg",
        "id_str" : "878991689632935936",
        "id" : 878991689632935936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDLOSN0XcAAnp5y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/irxuB6QNrK"
      } ],
      "hashtags" : [ {
        "text" : "Chromebook",
        "indices" : [ 25, 36 ]
      } ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/P7nT6jrBjN",
        "expanded_url" : "http:\/\/buff.ly\/2t4SOsC",
        "display_url" : "buff.ly\/2t4SOsC"
      } ]
    },
    "geo" : { },
    "id_str" : "878991692334084096",
    "text" : "Should you buy a 2GB RAM #Chromebook or do you really need 4GB? See this guide by @chromebooknet https:\/\/t.co\/P7nT6jrBjN https:\/\/t.co\/irxuB6QNrK",
    "id" : 878991692334084096,
    "created_at" : "2017-06-25 15:02:04 +0000",
    "user" : {
      "name" : "Albert Ruiz",
      "screen_name" : "databreak",
      "protected" : false,
      "id_str" : "2869056617",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/641466768356151296\/SMlZSpOA_normal.png",
      "id" : 2869056617,
      "verified" : false
    }
  },
  "id" : 879065639876304896,
  "created_at" : "2017-06-25 19:55:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Audi Khalid",
      "screen_name" : "audikhalidphoto",
      "indices" : [ 3, 19 ],
      "id_str" : "803539002515070977",
      "id" : 803539002515070977
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "879062975906070528",
  "text" : "RT @audikhalidphoto: Singapore landscape from One Raffles Place. Part of a series of high-resolution panoramas I'm producing now. https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/878966505555206144\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/ewp7zQCuIN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDK3W0gUQAAE9C_.jpg",
        "id_str" : "878966479969861632",
        "id" : 878966479969861632,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDK3W0gUQAAE9C_.jpg",
        "sizes" : [ {
          "h" : 1054,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1798,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 597,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1798,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewp7zQCuIN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/878966505555206144\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/ewp7zQCuIN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDK3W0fUAAAJxVM.jpg",
        "id_str" : "878966479965650944",
        "id" : 878966479965650944,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDK3W0fUAAAJxVM.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewp7zQCuIN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/878966505555206144\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/ewp7zQCuIN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDK3W1iVwAEzWET.jpg",
        "id_str" : "878966480246784001",
        "id" : 878966480246784001,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDK3W1iVwAEzWET.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1274,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 423,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 746,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1274,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewp7zQCuIN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/audikhalidphoto\/status\/878966505555206144\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/ewp7zQCuIN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDK3W1rUAAQCIT3.jpg",
        "id_str" : "878966480284418052",
        "id" : 878966480284418052,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDK3W1rUAAQCIT3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ewp7zQCuIN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "878941816686428160",
    "geo" : { },
    "id_str" : "878966505555206144",
    "in_reply_to_user_id" : 803539002515070977,
    "text" : "Singapore landscape from One Raffles Place. Part of a series of high-resolution panoramas I'm producing now. https:\/\/t.co\/ewp7zQCuIN",
    "id" : 878966505555206144,
    "in_reply_to_status_id" : 878941816686428160,
    "created_at" : "2017-06-25 13:21:59 +0000",
    "in_reply_to_screen_name" : "audikhalidphoto",
    "in_reply_to_user_id_str" : "803539002515070977",
    "user" : {
      "name" : "Audi Khalid",
      "screen_name" : "audikhalidphoto",
      "protected" : false,
      "id_str" : "803539002515070977",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/803540398815645696\/eRcv9E1D_normal.jpg",
      "id" : 803539002515070977,
      "verified" : false
    }
  },
  "id" : 879062975906070528,
  "created_at" : "2017-06-25 19:45:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "indices" : [ 3, 14 ],
      "id_str" : "231474739",
      "id" : 231474739
    }, {
      "name" : "datagovsg",
      "screen_name" : "datagovsg",
      "indices" : [ 21, 31 ],
      "id_str" : "3739197619",
      "id" : 3739197619
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/RZgt7y1IjH",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/878963521811226624",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "879062625060929536",
  "text" : "RT @heliumlife: Yay! @datagovsg https:\/\/t.co\/RZgt7y1IjH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "datagovsg",
        "screen_name" : "datagovsg",
        "indices" : [ 5, 15 ],
        "id_str" : "3739197619",
        "id" : 3739197619
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 39 ],
        "url" : "https:\/\/t.co\/RZgt7y1IjH",
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/878963521811226624",
        "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "879018377523548160",
    "text" : "Yay! @datagovsg https:\/\/t.co\/RZgt7y1IjH",
    "id" : 879018377523548160,
    "created_at" : "2017-06-25 16:48:07 +0000",
    "user" : {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "protected" : false,
      "id_str" : "231474739",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039125716262998018\/7LUaZasZ_normal.jpg",
      "id" : 231474739,
      "verified" : false
    }
  },
  "id" : 879062625060929536,
  "created_at" : "2017-06-25 19:43:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Docker",
      "screen_name" : "Docker",
      "indices" : [ 3, 10 ],
      "id_str" : "1138959692",
      "id" : 1138959692
    }, {
      "name" : "honestbee",
      "screen_name" : "honestbeesg",
      "indices" : [ 46, 58 ],
      "id_str" : "2896172509",
      "id" : 2896172509
    }, {
      "name" : "Vincent De Smet",
      "screen_name" : "vincentdesmet",
      "indices" : [ 75, 89 ],
      "id_str" : "57076661",
      "id" : 57076661
    }, {
      "name" : "Portainer.io",
      "screen_name" : "portainerio",
      "indices" : [ 115, 127 ],
      "id_str" : "779234043669774336",
      "id" : 779234043669774336
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Docker",
      "indices" : [ 12, 19 ]
    }, {
      "text" : "meetup",
      "indices" : [ 30, 37 ]
    }, {
      "text" : "DockerCaptain",
      "indices" : [ 60, 74 ]
    }, {
      "text" : "LinuxKit",
      "indices" : [ 93, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878808196877402112",
  "text" : "RT @Docker: #Docker Singapore #meetup 2morrow @honestbeesg: #DockerCaptain @vincentdesmet on #LinuxKit + a talk on @portainerio. https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.salesforce.com\" rel=\"nofollow\"\u003ESalesforce - Social Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "honestbee",
        "screen_name" : "honestbeesg",
        "indices" : [ 34, 46 ],
        "id_str" : "2896172509",
        "id" : 2896172509
      }, {
        "name" : "Vincent De Smet",
        "screen_name" : "vincentdesmet",
        "indices" : [ 63, 77 ],
        "id_str" : "57076661",
        "id" : 57076661
      }, {
        "name" : "Portainer.io",
        "screen_name" : "portainerio",
        "indices" : [ 103, 115 ],
        "id_str" : "779234043669774336",
        "id" : 779234043669774336
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Docker",
        "indices" : [ 0, 7 ]
      }, {
        "text" : "meetup",
        "indices" : [ 18, 25 ]
      }, {
        "text" : "DockerCaptain",
        "indices" : [ 48, 62 ]
      }, {
        "text" : "LinuxKit",
        "indices" : [ 81, 90 ]
      } ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/zI3zF2gdol",
        "expanded_url" : "https:\/\/www.meetup.com\/Docker-Singapore\/events\/240964120\/",
        "display_url" : "meetup.com\/Docker-Singapo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "878787338935181312",
    "text" : "#Docker Singapore #meetup 2morrow @honestbeesg: #DockerCaptain @vincentdesmet on #LinuxKit + a talk on @portainerio. https:\/\/t.co\/zI3zF2gdol",
    "id" : 878787338935181312,
    "created_at" : "2017-06-25 01:30:03 +0000",
    "user" : {
      "name" : "Docker",
      "screen_name" : "Docker",
      "protected" : false,
      "id_str" : "1138959692",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012762345238454272\/Q9jiI1pL_normal.jpg",
      "id" : 1138959692,
      "verified" : true
    }
  },
  "id" : 878808196877402112,
  "created_at" : "2017-06-25 02:52:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878784831584219138",
  "text" : "Wishing all my Twitter connection Selamat Hari Raya!",
  "id" : 878784831584219138,
  "created_at" : "2017-06-25 01:20:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/878783077161545728\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/vJr3fxB054",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDIQjQdXgAEf9xF.jpg",
      "id_str" : "878783075190276097",
      "id" : 878783075190276097,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDIQjQdXgAEf9xF.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vJr3fxB054"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/CCeQadwJeT",
      "expanded_url" : "http:\/\/ift.tt\/2sOrzQU",
      "display_url" : "ift.tt\/2sOrzQU"
    } ]
  },
  "geo" : { },
  "id_str" : "878783077161545728",
  "text" : "Yam Cake a local delight. https:\/\/t.co\/CCeQadwJeT https:\/\/t.co\/vJr3fxB054",
  "id" : 878783077161545728,
  "created_at" : "2017-06-25 01:13:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "indices" : [ 3, 13 ],
      "id_str" : "18849187",
      "id" : 18849187
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878780141823905793",
  "text" : "RT @barryhand: You can add Pride to the marketers calendar of campaign bandwagonry.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878694049825390592",
    "text" : "You can add Pride to the marketers calendar of campaign bandwagonry.",
    "id" : 878694049825390592,
    "created_at" : "2017-06-24 19:19:21 +0000",
    "user" : {
      "name" : "Baz Hand",
      "screen_name" : "barryhand",
      "protected" : false,
      "id_str" : "18849187",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/614532770300329984\/xJWWAtSu_normal.jpg",
      "id" : 18849187,
      "verified" : false
    }
  },
  "id" : 878780141823905793,
  "created_at" : "2017-06-25 01:01:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Vincent Graff",
      "screen_name" : "vincegraff",
      "indices" : [ 3, 14 ],
      "id_str" : "17966138",
      "id" : 17966138
    }, {
      "name" : "Hugo Rifkind",
      "screen_name" : "hugorifkind",
      "indices" : [ 16, 28 ],
      "id_str" : "20526449",
      "id" : 20526449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878773709246308352",
  "text" : "RT @vincegraff: @hugorifkind I'd much rather spend the weekend in the dentist's chair than in a tent at Glastonbury",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hugo Rifkind",
        "screen_name" : "hugorifkind",
        "indices" : [ 0, 12 ],
        "id_str" : "20526449",
        "id" : 20526449
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "878161616533045248",
    "geo" : { },
    "id_str" : "878162292566704128",
    "in_reply_to_user_id" : 20526449,
    "text" : "@hugorifkind I'd much rather spend the weekend in the dentist's chair than in a tent at Glastonbury",
    "id" : 878162292566704128,
    "in_reply_to_status_id" : 878161616533045248,
    "created_at" : "2017-06-23 08:06:20 +0000",
    "in_reply_to_screen_name" : "hugorifkind",
    "in_reply_to_user_id_str" : "20526449",
    "user" : {
      "name" : "Vincent Graff",
      "screen_name" : "vincegraff",
      "protected" : false,
      "id_str" : "17966138",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/581050580744957953\/R_wJ1m0h_normal.jpg",
      "id" : 17966138,
      "verified" : false
    }
  },
  "id" : 878773709246308352,
  "created_at" : "2017-06-25 00:35:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patrick Mooney",
      "screen_name" : "PatrickMooney",
      "indices" : [ 3, 17 ],
      "id_str" : "19165946",
      "id" : 19165946
    }, {
      "name" : "Google",
      "screen_name" : "Google",
      "indices" : [ 95, 102 ],
      "id_str" : "20536157",
      "id" : 20536157
    }, {
      "name" : "Dublin LGBTQ Pride",
      "screen_name" : "DublinPride",
      "indices" : [ 103, 115 ],
      "id_str" : "14529546",
      "id" : 14529546
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/PatrickMooney\/status\/878578296451149824\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/Vae8098ZtT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDFWTBWXoAEp8dK.jpg",
      "id_str" : "878578287093719041",
      "id" : 878578287093719041,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDFWTBWXoAEp8dK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Vae8098ZtT"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/PatrickMooney\/status\/878578296451149824\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/Vae8098ZtT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDFWTB9XUAAlKvY.jpg",
      "id_str" : "878578287257276416",
      "id" : 878578287257276416,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDFWTB9XUAAlKvY.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Vae8098ZtT"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/PatrickMooney\/status\/878578296451149824\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/Vae8098ZtT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDFWTBWXYAAiAAa.jpg",
      "id_str" : "878578287093702656",
      "id" : 878578287093702656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDFWTBWXYAAiAAa.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1152
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Vae8098ZtT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878582251004411904",
  "text" : "RT @PatrickMooney: Well done Google. Open Google Maps and head to Dublin and you get this!!!!! @Google @DublinPride https:\/\/t.co\/Vae8098ZtT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 76, 83 ],
        "id_str" : "20536157",
        "id" : 20536157
      }, {
        "name" : "Dublin LGBTQ Pride",
        "screen_name" : "DublinPride",
        "indices" : [ 84, 96 ],
        "id_str" : "14529546",
        "id" : 14529546
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PatrickMooney\/status\/878578296451149824\/photo\/1",
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/Vae8098ZtT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDFWTBWXoAEp8dK.jpg",
        "id_str" : "878578287093719041",
        "id" : 878578287093719041,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDFWTBWXoAEp8dK.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Vae8098ZtT"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/PatrickMooney\/status\/878578296451149824\/photo\/1",
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/Vae8098ZtT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDFWTB9XUAAlKvY.jpg",
        "id_str" : "878578287257276416",
        "id" : 878578287257276416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDFWTB9XUAAlKvY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Vae8098ZtT"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/PatrickMooney\/status\/878578296451149824\/photo\/1",
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/Vae8098ZtT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDFWTBWXYAAiAAa.jpg",
        "id_str" : "878578287093702656",
        "id" : 878578287093702656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDFWTBWXYAAiAAa.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1152
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Vae8098ZtT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878578296451149824",
    "text" : "Well done Google. Open Google Maps and head to Dublin and you get this!!!!! @Google @DublinPride https:\/\/t.co\/Vae8098ZtT",
    "id" : 878578296451149824,
    "created_at" : "2017-06-24 11:39:23 +0000",
    "user" : {
      "name" : "Patrick Mooney",
      "screen_name" : "PatrickMooney",
      "protected" : false,
      "id_str" : "19165946",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/957714070098137088\/pRG4flXD_normal.jpg",
      "id" : 19165946,
      "verified" : false
    }
  },
  "id" : 878582251004411904,
  "created_at" : "2017-06-24 11:55:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/4QiqU2kzYv",
      "expanded_url" : "http:\/\/str.sg\/4EhH",
      "display_url" : "str.sg\/4EhH"
    } ]
  },
  "geo" : { },
  "id_str" : "878550506628497408",
  "text" : "Malaysia's anti-terror chief targeted https:\/\/t.co\/4QiqU2kzYv",
  "id" : 878550506628497408,
  "created_at" : "2017-06-24 09:48:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SPOTLIGHT Movie",
      "screen_name" : "SpotlightMovie",
      "indices" : [ 8, 23 ],
      "id_str" : "2884545496",
      "id" : 2884545496
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878532773220306944",
  "text" : "Watched @SpotlightMovie on a flight back home.  I highly recommend it",
  "id" : 878532773220306944,
  "created_at" : "2017-06-24 08:38:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Waze Ireland",
      "screen_name" : "WazeIreland",
      "indices" : [ 3, 15 ],
      "id_str" : "1066071678",
      "id" : 1066071678
    }, {
      "name" : "waze",
      "screen_name" : "waze",
      "indices" : [ 68, 73 ],
      "id_str" : "31171669",
      "id" : 31171669
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878526905992888320",
  "text" : "RT @WazeIreland: Did you know you can use an Eircode to navigate in @waze? Just enter the code and we'll take you straight there https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "waze",
        "screen_name" : "waze",
        "indices" : [ 51, 56 ],
        "id_str" : "31171669",
        "id" : 31171669
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WazeIreland\/status\/878308462614069248\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/AhqrxnLPoD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBg4m7XUAA5tsL.jpg",
        "id_str" : "878308452975726592",
        "id" : 878308452975726592,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBg4m7XUAA5tsL.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 382
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 674
        }, {
          "h" : 1333,
          "resize" : "fit",
          "w" : 749
        }, {
          "h" : 1333,
          "resize" : "fit",
          "w" : 749
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AhqrxnLPoD"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/WazeIreland\/status\/878308462614069248\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/AhqrxnLPoD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBg4m4XsAAUDUL.jpg",
        "id_str" : "878308452963168256",
        "id" : 878308452963168256,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBg4m4XsAAUDUL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1311,
          "resize" : "fit",
          "w" : 749
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 388
        }, {
          "h" : 1311,
          "resize" : "fit",
          "w" : 749
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 686
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AhqrxnLPoD"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/WazeIreland\/status\/878308462614069248\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/AhqrxnLPoD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBg4oEXYAIgi71.jpg",
        "id_str" : "878308453281914882",
        "id" : 878308453281914882,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBg4oEXYAIgi71.jpg",
        "sizes" : [ {
          "h" : 1219,
          "resize" : "fit",
          "w" : 749
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 418
        }, {
          "h" : 1219,
          "resize" : "fit",
          "w" : 749
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 737
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/AhqrxnLPoD"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878308462614069248",
    "text" : "Did you know you can use an Eircode to navigate in @waze? Just enter the code and we'll take you straight there https:\/\/t.co\/AhqrxnLPoD",
    "id" : 878308462614069248,
    "created_at" : "2017-06-23 17:47:10 +0000",
    "user" : {
      "name" : "Waze Ireland",
      "screen_name" : "WazeIreland",
      "protected" : false,
      "id_str" : "1066071678",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3149040080\/d6cc519fcf44ea032a06cea380b52cbb_normal.jpeg",
      "id" : 1066071678,
      "verified" : false
    }
  },
  "id" : 878526905992888320,
  "created_at" : "2017-06-24 08:15:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Lian",
      "screen_name" : "davidlian",
      "indices" : [ 3, 13 ],
      "id_str" : "2000251",
      "id" : 2000251
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878525598196314112",
  "text" : "RT @davidlian: Not all stories are about achieving your dream. Some are about surviving disappointment. - Ron Howard.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878203107813019648",
    "text" : "Not all stories are about achieving your dream. Some are about surviving disappointment. - Ron Howard.",
    "id" : 878203107813019648,
    "created_at" : "2017-06-23 10:48:31 +0000",
    "user" : {
      "name" : "David Lian",
      "screen_name" : "davidlian",
      "protected" : true,
      "id_str" : "2000251",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/991462850383118336\/1vdB6gT3_normal.jpg",
      "id" : 2000251,
      "verified" : false
    }
  },
  "id" : 878525598196314112,
  "created_at" : "2017-06-24 08:09:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/mijijAOTnA",
      "expanded_url" : "https:\/\/support.google.com\/chrome\/answer\/2765944?hl=en",
      "display_url" : "support.google.com\/chrome\/answer\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "878520298617815040",
  "text" : "Home coming see me fixing family PC problem such as this. https:\/\/t.co\/mijijAOTnA",
  "id" : 878520298617815040,
  "created_at" : "2017-06-24 07:48:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "179569408",
      "id" : 179569408
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Travel",
      "indices" : [ 119, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878518667398356992",
  "text" : "RT @DublinAirport: Pop \u20AC1 into any of the honesty water stations in the terminal and grab a bottle of water on the go. #Travel https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DublinAirport\/status\/878305452945281025\/photo\/1",
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/H0S2F67ANR",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBeJ1SXcAM2znB.jpg",
        "id_str" : "878305450353192963",
        "id" : 878305450353192963,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBeJ1SXcAM2znB.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 607
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 607
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 607
        }, {
          "h" : 513,
          "resize" : "fit",
          "w" : 607
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/H0S2F67ANR"
      } ],
      "hashtags" : [ {
        "text" : "Travel",
        "indices" : [ 100, 107 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878305452945281025",
    "text" : "Pop \u20AC1 into any of the honesty water stations in the terminal and grab a bottle of water on the go. #Travel https:\/\/t.co\/H0S2F67ANR",
    "id" : 878305452945281025,
    "created_at" : "2017-06-23 17:35:12 +0000",
    "user" : {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "protected" : false,
      "id_str" : "179569408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013556776321650694\/bdnXxxoU_normal.jpg",
      "id" : 179569408,
      "verified" : true
    }
  },
  "id" : 878518667398356992,
  "created_at" : "2017-06-24 07:42:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Liz Ryan",
      "screen_name" : "humanworkplace",
      "indices" : [ 3, 18 ],
      "id_str" : "8374662",
      "id" : 8374662
    }, {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 89, 96 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "humanworkplace",
      "indices" : [ 121, 136 ]
    } ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/CAZri1lk83",
      "expanded_url" : "http:\/\/www.forbes.com\/sites\/lizryan\/2017\/06\/23\/its-hard-to-find-qualified-applicants-another-business-lie\/#1cc4494200a7",
      "display_url" : "forbes.com\/sites\/lizryan\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "878461476612186113",
  "text" : "RT @humanworkplace: 'It's Hard To Find Qualified Applicants' -- Another Business Lie via @forbes https:\/\/t.co\/CAZri1lk83 #humanworkplace",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Forbes",
        "screen_name" : "Forbes",
        "indices" : [ 69, 76 ],
        "id_str" : "91478624",
        "id" : 91478624
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "humanworkplace",
        "indices" : [ 101, 116 ]
      } ],
      "urls" : [ {
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/CAZri1lk83",
        "expanded_url" : "http:\/\/www.forbes.com\/sites\/lizryan\/2017\/06\/23\/its-hard-to-find-qualified-applicants-another-business-lie\/#1cc4494200a7",
        "display_url" : "forbes.com\/sites\/lizryan\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "878452634180476928",
    "text" : "'It's Hard To Find Qualified Applicants' -- Another Business Lie via @forbes https:\/\/t.co\/CAZri1lk83 #humanworkplace",
    "id" : 878452634180476928,
    "created_at" : "2017-06-24 03:20:03 +0000",
    "user" : {
      "name" : "Liz Ryan",
      "screen_name" : "humanworkplace",
      "protected" : false,
      "id_str" : "8374662",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1570051568\/side_purple_normal.jpg",
      "id" : 8374662,
      "verified" : false
    }
  },
  "id" : 878461476612186113,
  "created_at" : "2017-06-24 03:55:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 0, 14 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "878443966194044928",
  "geo" : { },
  "id_str" : "878444933127872512",
  "in_reply_to_user_id" : 815066809,
  "text" : "@ofmeowandbake Just a short trip. \uD83D\uDE00",
  "id" : 878444933127872512,
  "in_reply_to_status_id" : 878443966194044928,
  "created_at" : "2017-06-24 02:49:27 +0000",
  "in_reply_to_screen_name" : "ofmeowandbake",
  "in_reply_to_user_id_str" : "815066809",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IDA Ireland",
      "screen_name" : "IDAIRELAND",
      "indices" : [ 3, 14 ],
      "id_str" : "44889752",
      "id" : 44889752
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/IDAIRELAND\/status\/878236113915092992\/photo\/1",
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/tAZpdgiUmT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDAduJbXsAAW49z.jpg",
      "id_str" : "878234605979152384",
      "id" : 878234605979152384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDAduJbXsAAW49z.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 975,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1188,
        "resize" : "fit",
        "w" : 2496
      }, {
        "h" : 571,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tAZpdgiUmT"
    } ],
    "hashtags" : [ {
      "text" : "MedTechWeek",
      "indices" : [ 92, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878432929763938304",
  "text" : "RT @IDAIRELAND: A third of the world's supply of contact lenses are manufactured in Ireland #MedTechWeek https:\/\/t.co\/tAZpdgiUmT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IDAIRELAND\/status\/878236113915092992\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/tAZpdgiUmT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDAduJbXsAAW49z.jpg",
        "id_str" : "878234605979152384",
        "id" : 878234605979152384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDAduJbXsAAW49z.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 324,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 975,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1188,
          "resize" : "fit",
          "w" : 2496
        }, {
          "h" : 571,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tAZpdgiUmT"
      } ],
      "hashtags" : [ {
        "text" : "MedTechWeek",
        "indices" : [ 76, 88 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878236113915092992",
    "text" : "A third of the world's supply of contact lenses are manufactured in Ireland #MedTechWeek https:\/\/t.co\/tAZpdgiUmT",
    "id" : 878236113915092992,
    "created_at" : "2017-06-23 12:59:41 +0000",
    "user" : {
      "name" : "IDA Ireland",
      "screen_name" : "IDAIRELAND",
      "protected" : false,
      "id_str" : "44889752",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/902189616031305728\/-1KTUyC__normal.jpg",
      "id" : 44889752,
      "verified" : true
    }
  },
  "id" : 878432929763938304,
  "created_at" : "2017-06-24 02:01:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/878431646428647424\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/msq94jbJHZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDDQ7UTXoAESKUA.jpg",
      "id_str" : "878431644818055169",
      "id" : 878431644818055169,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDDQ7UTXoAESKUA.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/msq94jbJHZ"
    } ],
    "hashtags" : [ {
      "text" : "singapore",
      "indices" : [ 45, 55 ]
    }, {
      "text" : "yaphomecoming",
      "indices" : [ 56, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/etzNmln5iK",
      "expanded_url" : "http:\/\/ift.tt\/2t1qgQE",
      "display_url" : "ift.tt\/2t1qgQE"
    } ]
  },
  "geo" : { },
  "id_str" : "878431646428647424",
  "text" : "Playground on top of a multi storey carpark. #singapore #yaphomecoming https:\/\/t.co\/etzNmln5iK https:\/\/t.co\/msq94jbJHZ",
  "id" : 878431646428647424,
  "created_at" : "2017-06-24 01:56:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emily Feng",
      "screen_name" : "EmilyZFeng",
      "indices" : [ 3, 14 ],
      "id_str" : "1878264721",
      "id" : 1878264721
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/Fw0pIoCf5B",
      "expanded_url" : "https:\/\/www.ft.com\/content\/8a06dd5e-5752-11e7-9fed-c19e2700005f",
      "display_url" : "ft.com\/content\/8a06dd\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "878430451710414848",
  "text" : "RT @EmilyZFeng: This is why ppl want to leave - gov can take aware your core business in an instant\nhttps:\/\/t.co\/Fw0pIoCf5B",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/Fw0pIoCf5B",
        "expanded_url" : "https:\/\/www.ft.com\/content\/8a06dd5e-5752-11e7-9fed-c19e2700005f",
        "display_url" : "ft.com\/content\/8a06dd\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "878060793446584321",
    "text" : "This is why ppl want to leave - gov can take aware your core business in an instant\nhttps:\/\/t.co\/Fw0pIoCf5B",
    "id" : 878060793446584321,
    "created_at" : "2017-06-23 01:23:01 +0000",
    "user" : {
      "name" : "Emily Feng",
      "screen_name" : "EmilyZFeng",
      "protected" : false,
      "id_str" : "1878264721",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/883000719803957252\/NQIx7gk3_normal.jpg",
      "id" : 1878264721,
      "verified" : true
    }
  },
  "id" : 878430451710414848,
  "created_at" : "2017-06-24 01:51:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Spinks",
      "screen_name" : "DavidSpinks",
      "indices" : [ 3, 15 ],
      "id_str" : "15427422",
      "id" : 15427422
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878429670718316544",
  "text" : "RT @DavidSpinks: What Facebook considers \"meaningful communities\" \n\n-Identity\n-Fulfills need \n-Voice matters \n-Distinct culture \n-Sense of\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DavidSpinks\/status\/878319197108838400\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/cRjbb5y1lK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DDBqp2mUMAE79pj.jpg",
        "id_str" : "878319194600648705",
        "id" : 878319194600648705,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDBqp2mUMAE79pj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cRjbb5y1lK"
      } ],
      "hashtags" : [ {
        "text" : "fcs2017",
        "indices" : [ 131, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878319197108838400",
    "text" : "What Facebook considers \"meaningful communities\" \n\n-Identity\n-Fulfills need \n-Voice matters \n-Distinct culture \n-Sense of safety \n\n#fcs2017 https:\/\/t.co\/cRjbb5y1lK",
    "id" : 878319197108838400,
    "created_at" : "2017-06-23 18:29:49 +0000",
    "user" : {
      "name" : "David Spinks",
      "screen_name" : "DavidSpinks",
      "protected" : false,
      "id_str" : "15427422",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032014245326516224\/ItRs1bhg_normal.jpg",
      "id" : 15427422,
      "verified" : false
    }
  },
  "id" : 878429670718316544,
  "created_at" : "2017-06-24 01:48:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/878429440719691776\/photo\/1",
      "indices" : [ 137, 160 ],
      "url" : "https:\/\/t.co\/hQJZ0uOVM6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDDO66XXYAAzB4D.jpg",
      "id_str" : "878429438832238592",
      "id" : 878429438832238592,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDDO66XXYAAzB4D.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hQJZ0uOVM6"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/u9YZ9KnokJ",
      "expanded_url" : "http:\/\/ift.tt\/2t4WMCx",
      "display_url" : "ift.tt\/2t4WMCx"
    } ]
  },
  "geo" : { },
  "id_str" : "878429440719691776",
  "text" : "Red Packet (with cash enclosed) used in Asia given out on aspacious occasion from marriage to first day of work. https:\/\/t.co\/u9YZ9KnokJ https:\/\/t.co\/hQJZ0uOVM6",
  "id" : 878429440719691776,
  "created_at" : "2017-06-24 01:47:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SuperheroNetwork",
      "screen_name" : "SuperherosArt",
      "indices" : [ 3, 17 ],
      "id_str" : "4767986565",
      "id" : 4767986565
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/SuperherosArt\/status\/878029970206932992\/photo\/1",
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/SZSW85YWZ2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DC9jmoBUMAAHOF3.jpg",
      "id_str" : "878029967589650432",
      "id" : 878029967589650432,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC9jmoBUMAAHOF3.jpg",
      "sizes" : [ {
        "h" : 553,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 598,
        "resize" : "fit",
        "w" : 736
      }, {
        "h" : 598,
        "resize" : "fit",
        "w" : 736
      }, {
        "h" : 598,
        "resize" : "fit",
        "w" : 736
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/SZSW85YWZ2"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878426060320235520",
  "text" : "RT @SuperherosArt: https:\/\/t.co\/SZSW85YWZ2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SuperherosArt\/status\/878029970206932992\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/SZSW85YWZ2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DC9jmoBUMAAHOF3.jpg",
        "id_str" : "878029967589650432",
        "id" : 878029967589650432,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC9jmoBUMAAHOF3.jpg",
        "sizes" : [ {
          "h" : 553,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 736
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/SZSW85YWZ2"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878029970206932992",
    "text" : "https:\/\/t.co\/SZSW85YWZ2",
    "id" : 878029970206932992,
    "created_at" : "2017-06-22 23:20:32 +0000",
    "user" : {
      "name" : "SuperheroNetwork",
      "screen_name" : "SuperherosArt",
      "protected" : false,
      "id_str" : "4767986565",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/775239888518144000\/u8J2XZsP_normal.jpg",
      "id" : 4767986565,
      "verified" : false
    }
  },
  "id" : 878426060320235520,
  "created_at" : "2017-06-24 01:34:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/878263763723354113\/photo\/1",
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/2I4V1WJnrQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DDA4PQmV0AA4Vh7.jpg",
      "id_str" : "878263762142220288",
      "id" : 878263762142220288,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DDA4PQmV0AA4Vh7.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/2I4V1WJnrQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/wi5qqoPdIY",
      "expanded_url" : "http:\/\/ift.tt\/2sYSsUs",
      "display_url" : "ift.tt\/2sYSsUs"
    } ]
  },
  "geo" : { },
  "id_str" : "878263763723354113",
  "text" : "Using messaging app to report a fault. https:\/\/t.co\/wi5qqoPdIY https:\/\/t.co\/2I4V1WJnrQ",
  "id" : 878263763723354113,
  "created_at" : "2017-06-23 14:49:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edward Tufte",
      "screen_name" : "EdwardTufte",
      "indices" : [ 3, 15 ],
      "id_str" : "152862026",
      "id" : 152862026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878236404379131904",
  "text" : "RT @EdwardTufte: Glyphs in general have this problem,\ngoes back to Bell Labs R\/S graphics 30 years ago\nGlyph codes also one-off, a drag  fo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 133, 140 ]
      } ],
      "urls" : [ {
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/iYx16lhlYP",
        "expanded_url" : "https:\/\/twitter.com\/LukeBornn\/status\/864856335191388162",
        "display_url" : "twitter.com\/LukeBornn\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "876606767198855169",
    "text" : "Glyphs in general have this problem,\ngoes back to Bell Labs R\/S graphics 30 years ago\nGlyph codes also one-off, a drag  for viewers.\n#rstats https:\/\/t.co\/iYx16lhlYP",
    "id" : 876606767198855169,
    "created_at" : "2017-06-19 01:05:14 +0000",
    "user" : {
      "name" : "Edward Tufte",
      "screen_name" : "EdwardTufte",
      "protected" : false,
      "id_str" : "152862026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/458847900957556736\/uArnnoBy_normal.png",
      "id" : 152862026,
      "verified" : false
    }
  },
  "id" : 878236404379131904,
  "created_at" : "2017-06-23 13:00:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "85053026",
      "id" : 85053026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/H24T3JX38G",
      "expanded_url" : "https:\/\/twitter.com\/ABC\/status\/878052418071736320",
      "display_url" : "twitter.com\/ABC\/status\/878\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "878235942816854018",
  "text" : "RT @gavindbrown: Quick acting police officer in China risks own life to save toddler on busy road. See video. https:\/\/t.co\/H24T3JX38G",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/H24T3JX38G",
        "expanded_url" : "https:\/\/twitter.com\/ABC\/status\/878052418071736320",
        "display_url" : "twitter.com\/ABC\/status\/878\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "878227296922021888",
    "text" : "Quick acting police officer in China risks own life to save toddler on busy road. See video. https:\/\/t.co\/H24T3JX38G",
    "id" : 878227296922021888,
    "created_at" : "2017-06-23 12:24:38 +0000",
    "user" : {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "protected" : false,
      "id_str" : "85053026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844192952914231296\/mLoaFZ86_normal.jpg",
      "id" : 85053026,
      "verified" : false
    }
  },
  "id" : 878235942816854018,
  "created_at" : "2017-06-23 12:59:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/UntP2hZLSN",
      "expanded_url" : "https:\/\/uk.news.yahoo.com\/australia-send-spy-planes-help-philippines-fight-militants-095427396.html",
      "display_url" : "uk.news.yahoo.com\/australia-send\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "878198057921257473",
  "text" : "https:\/\/t.co\/UntP2hZLSN",
  "id" : 878198057921257473,
  "created_at" : "2017-06-23 10:28:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878196904231804928",
  "text" : "In the northern part of Malaysia in a town where lady are expect not to bare the shoulder and neck...",
  "id" : 878196904231804928,
  "created_at" : "2017-06-23 10:23:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Hall",
      "screen_name" : "SimonHallNews",
      "indices" : [ 3, 17 ],
      "id_str" : "260655588",
      "id" : 260655588
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878192915624755201",
  "text" : "RT @SimonHallNews: Boys at Isca Academy in Exeter wear skirts to school in protest at not being allowed to wear shorts in hot weather. http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SimonHallNews\/status\/877794305766772736\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/XHrffnSQEN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DC6NQFJXcAAbN6A.jpg",
        "id_str" : "877794284782710784",
        "id" : 877794284782710784,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC6NQFJXcAAbN6A.jpg",
        "sizes" : [ {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XHrffnSQEN"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/SimonHallNews\/status\/877794305766772736\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/XHrffnSQEN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DC6NQFLXcAALT5K.jpg",
        "id_str" : "877794284791099392",
        "id" : 877794284791099392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC6NQFLXcAALT5K.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XHrffnSQEN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "877794305766772736",
    "text" : "Boys at Isca Academy in Exeter wear skirts to school in protest at not being allowed to wear shorts in hot weather. https:\/\/t.co\/XHrffnSQEN",
    "id" : 877794305766772736,
    "created_at" : "2017-06-22 07:44:05 +0000",
    "user" : {
      "name" : "Simon Hall",
      "screen_name" : "SimonHallNews",
      "protected" : false,
      "id_str" : "260655588",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/852539640972275712\/_c4lAqTJ_normal.jpg",
      "id" : 260655588,
      "verified" : true
    }
  },
  "id" : 878192915624755201,
  "created_at" : "2017-06-23 10:08:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HuiChi Man",
      "screen_name" : "manhuichi",
      "indices" : [ 3, 13 ],
      "id_str" : "4914275915",
      "id" : 4914275915
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878192833299009536",
  "text" : "RT @manhuichi: Looking for an internship this summer?\nIT Administrator. Paid internship \u20AC11k Based in South Dublin and is full time 9-5 #in\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "intern",
        "indices" : [ 121, 128 ]
      }, {
        "text" : "jobfairy",
        "indices" : [ 129, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "878191978185228288",
    "text" : "Looking for an internship this summer?\nIT Administrator. Paid internship \u20AC11k Based in South Dublin and is full time 9-5 #intern #jobfairy",
    "id" : 878191978185228288,
    "created_at" : "2017-06-23 10:04:18 +0000",
    "user" : {
      "name" : "HuiChi Man",
      "screen_name" : "manhuichi",
      "protected" : false,
      "id_str" : "4914275915",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/903241241286074368\/6orwToiv_normal.jpg",
      "id" : 4914275915,
      "verified" : false
    }
  },
  "id" : 878192833299009536,
  "created_at" : "2017-06-23 10:07:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878186531764150273",
  "text" : "It always the case when you are travelling and do not have internet access and you got to response to important email.",
  "id" : 878186531764150273,
  "created_at" : "2017-06-23 09:42:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scott Herman",
      "screen_name" : "scott_h_herman",
      "indices" : [ 3, 18 ],
      "id_str" : "273852943",
      "id" : 273852943
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/scott_h_herman\/status\/878042642885431296\/photo\/1",
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/vFmKqse8lQ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DC9vDrOUIAIOLHy.jpg",
      "id_str" : "878042561293590530",
      "id" : 878042561293590530,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC9vDrOUIAIOLHy.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 910,
        "resize" : "fit",
        "w" : 1694
      }, {
        "h" : 645,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 365,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 910,
        "resize" : "fit",
        "w" : 1694
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/vFmKqse8lQ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "878153282107039746",
  "text" : "RT @scott_h_herman: Trigger colors. Back by popular demand! https:\/\/t.co\/vFmKqse8lQ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/scott_h_herman\/status\/878042642885431296\/photo\/1",
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/vFmKqse8lQ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DC9vDrOUIAIOLHy.jpg",
        "id_str" : "878042561293590530",
        "id" : 878042561293590530,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC9vDrOUIAIOLHy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 910,
          "resize" : "fit",
          "w" : 1694
        }, {
          "h" : 645,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 365,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 910,
          "resize" : "fit",
          "w" : 1694
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/vFmKqse8lQ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "877332650867933184",
    "geo" : { },
    "id_str" : "878042642885431296",
    "in_reply_to_user_id" : 273852943,
    "text" : "Trigger colors. Back by popular demand! https:\/\/t.co\/vFmKqse8lQ",
    "id" : 878042642885431296,
    "in_reply_to_status_id" : 877332650867933184,
    "created_at" : "2017-06-23 00:10:53 +0000",
    "in_reply_to_screen_name" : "scott_h_herman",
    "in_reply_to_user_id_str" : "273852943",
    "user" : {
      "name" : "Scott Herman",
      "screen_name" : "scott_h_herman",
      "protected" : false,
      "id_str" : "273852943",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000040963466\/a8443815e8a4b241addfd4dc9b86d1bb_normal.jpeg",
      "id" : 273852943,
      "verified" : false
    }
  },
  "id" : 878153282107039746,
  "created_at" : "2017-06-23 07:30:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Internet of Shit",
      "screen_name" : "internetofshit",
      "indices" : [ 3, 18 ],
      "id_str" : "3356531254",
      "id" : 3356531254
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/8tIkVjjXju",
      "expanded_url" : "https:\/\/twitter.com\/isotopp\/status\/877444175708475393",
      "display_url" : "twitter.com\/isotopp\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "878148684537450497",
  "text" : "RT @internetofshit: a whole new spin on \"how many engineers does it take to screw in a lightbulb\" https:\/\/t.co\/8tIkVjjXju",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/8tIkVjjXju",
        "expanded_url" : "https:\/\/twitter.com\/isotopp\/status\/877444175708475393",
        "display_url" : "twitter.com\/isotopp\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "877913655110774784",
    "text" : "a whole new spin on \"how many engineers does it take to screw in a lightbulb\" https:\/\/t.co\/8tIkVjjXju",
    "id" : 877913655110774784,
    "created_at" : "2017-06-22 15:38:20 +0000",
    "user" : {
      "name" : "Internet of Shit",
      "screen_name" : "internetofshit",
      "protected" : false,
      "id_str" : "3356531254",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/616895706150797312\/ol4PeiHz_normal.png",
      "id" : 3356531254,
      "verified" : false
    }
  },
  "id" : 878148684537450497,
  "created_at" : "2017-06-23 07:12:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/878084359961620480\/photo\/1",
      "indices" : [ 78, 101 ],
      "url" : "https:\/\/t.co\/J29067Ckh1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DC-VEh-XoAAaCkX.jpg",
      "id_str" : "878084357432516608",
      "id" : 878084357432516608,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC-VEh-XoAAaCkX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/J29067Ckh1"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/YdANSdRQCr",
      "expanded_url" : "http:\/\/ift.tt\/2sHoGkU",
      "display_url" : "ift.tt\/2sHoGkU"
    } ]
  },
  "geo" : { },
  "id_str" : "878084359961620480",
  "text" : "You can use messaging apps to purchase coach tickets. https:\/\/t.co\/YdANSdRQCr https:\/\/t.co\/J29067Ckh1",
  "id" : 878084359961620480,
  "created_at" : "2017-06-23 02:56:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marina Nicholas",
      "screen_name" : "_marinanicholas",
      "indices" : [ 3, 19 ],
      "id_str" : "783311779795505152",
      "id" : 783311779795505152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/l9RIXzLkK5",
      "expanded_url" : "https:\/\/techcrunch.com\/2017\/06\/20\/google-launches-its-ai-powered-jobs-search-engine\/?ncid=rss",
      "display_url" : "techcrunch.com\/2017\/06\/20\/goo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "877608456135376897",
  "text" : "RT @_marinanicholas: Google launches its AI-powered jobs search engine\nhttps:\/\/t.co\/l9RIXzLkK5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/l9RIXzLkK5",
        "expanded_url" : "https:\/\/techcrunch.com\/2017\/06\/20\/google-launches-its-ai-powered-jobs-search-engine\/?ncid=rss",
        "display_url" : "techcrunch.com\/2017\/06\/20\/goo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "877604947755286528",
    "text" : "Google launches its AI-powered jobs search engine\nhttps:\/\/t.co\/l9RIXzLkK5",
    "id" : 877604947755286528,
    "created_at" : "2017-06-21 19:11:39 +0000",
    "user" : {
      "name" : "Marina Nicholas",
      "screen_name" : "_marinanicholas",
      "protected" : false,
      "id_str" : "783311779795505152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/783313433307602944\/4bhC1dmr_normal.jpg",
      "id" : 783311779795505152,
      "verified" : false
    }
  },
  "id" : 877608456135376897,
  "created_at" : "2017-06-21 19:25:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "GIANTWeb",
      "screen_name" : "giantapp",
      "indices" : [ 3, 12 ],
      "id_str" : "393421408",
      "id" : 393421408
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "quote",
      "indices" : [ 89, 95 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "877608398614708225",
  "text" : "RT @giantapp: A wealth of information creates a poverty of attention. - Herbert A. Simon #quote",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.tweetjukebox.com\" rel=\"nofollow\"\u003ETweet Jukebox\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "quote",
        "indices" : [ 75, 81 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "877605847697735680",
    "text" : "A wealth of information creates a poverty of attention. - Herbert A. Simon #quote",
    "id" : 877605847697735680,
    "created_at" : "2017-06-21 19:15:13 +0000",
    "user" : {
      "name" : "GIANTWeb",
      "screen_name" : "giantapp",
      "protected" : false,
      "id_str" : "393421408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2979227051\/3fa2bca927d7f3bf38d564c641818679_normal.jpeg",
      "id" : 393421408,
      "verified" : false
    }
  },
  "id" : 877608398614708225,
  "created_at" : "2017-06-21 19:25:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/877563811116593152\/photo\/1",
      "indices" : [ 135, 158 ],
      "url" : "https:\/\/t.co\/g9kahqEh79",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DC27oomXYAUqikW.jpg",
      "id_str" : "877563809174609925",
      "id" : 877563809174609925,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DC27oomXYAUqikW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/g9kahqEh79"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/X5LLJgNBXC",
      "expanded_url" : "http:\/\/ift.tt\/2sQLv7D",
      "display_url" : "ift.tt\/2sQLv7D"
    } ]
  },
  "geo" : { },
  "id_str" : "877563811116593152",
  "text" : "On TV, a talk show from China where participants of various nationalites interact in perfect Mandarin Chinese! https:\/\/t.co\/X5LLJgNBXC https:\/\/t.co\/g9kahqEh79",
  "id" : 877563811116593152,
  "created_at" : "2017-06-21 16:28:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/4ND0HtTzDL",
      "expanded_url" : "https:\/\/www.eremedia.com\/sourcecon\/its-happening-google-launches-its-jobs-search-engine\/",
      "display_url" : "eremedia.com\/sourcecon\/its-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "877520652206391296",
  "text" : "https:\/\/t.co\/4ND0HtTzDL",
  "id" : 877520652206391296,
  "created_at" : "2017-06-21 13:36:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "877518558749573120",
  "text" : "Have 2nd thought handing over IC to a 3rd party pop-up shop in Bugis while registering for a mobile no.",
  "id" : 877518558749573120,
  "created_at" : "2017-06-21 13:28:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steven",
      "screen_name" : "cloudrangerAWS",
      "indices" : [ 3, 18 ],
      "id_str" : "972129486287368193",
      "id" : 972129486287368193
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "cloud",
      "indices" : [ 84, 90 ]
    }, {
      "text" : "awscloud",
      "indices" : [ 91, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/YLWtHwkzl8",
      "expanded_url" : "http:\/\/buff.ly\/2rAL6TH",
      "display_url" : "buff.ly\/2rAL6TH"
    } ]
  },
  "geo" : { },
  "id_str" : "877515289167921152",
  "text" : "RT @cloudrangerAWS: In the Works \u2013 AWS Region in Hong Kong https:\/\/t.co\/YLWtHwkzl8  #cloud #awscloud",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "cloud",
        "indices" : [ 64, 70 ]
      }, {
        "text" : "awscloud",
        "indices" : [ 71, 80 ]
      } ],
      "urls" : [ {
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/YLWtHwkzl8",
        "expanded_url" : "http:\/\/buff.ly\/2rAL6TH",
        "display_url" : "buff.ly\/2rAL6TH"
      } ]
    },
    "geo" : { },
    "id_str" : "877514667614228480",
    "text" : "In the Works \u2013 AWS Region in Hong Kong https:\/\/t.co\/YLWtHwkzl8  #cloud #awscloud",
    "id" : 877514667614228480,
    "created_at" : "2017-06-21 13:12:54 +0000",
    "user" : {
      "name" : "CloudRanger",
      "screen_name" : "cloudrangerHQ",
      "protected" : false,
      "id_str" : "4023999209",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1003988258597023746\/ybG_smzD_normal.jpg",
      "id" : 4023999209,
      "verified" : false
    }
  },
  "id" : 877515289167921152,
  "created_at" : "2017-06-21 13:15:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/877337279164891136\/photo\/1",
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/Rl0tFh5183",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCztmsqVYAEr711.jpg",
      "id_str" : "877337276509937665",
      "id" : 877337276509937665,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCztmsqVYAEr711.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Rl0tFh5183"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/s6T7Oo0beR",
      "expanded_url" : "http:\/\/ift.tt\/2sOefOj",
      "display_url" : "ift.tt\/2sOefOj"
    } ]
  },
  "geo" : { },
  "id_str" : "877337279164891136",
  "text" : "The new Singapore coin appears to look like acracade tokens https:\/\/t.co\/s6T7Oo0beR https:\/\/t.co\/Rl0tFh5183",
  "id" : 877337279164891136,
  "created_at" : "2017-06-21 01:28:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/877038846969597952\/photo\/1",
      "indices" : [ 45, 68 ],
      "url" : "https:\/\/t.co\/uGVKPudAUA",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCveLrtXoAEMXod.jpg",
      "id_str" : "877038844746637313",
      "id" : 877038844746637313,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCveLrtXoAEMXod.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/uGVKPudAUA"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/TOktm49Kd6",
      "expanded_url" : "http:\/\/ift.tt\/2suxASQ",
      "display_url" : "ift.tt\/2suxASQ"
    } ]
  },
  "geo" : { },
  "id_str" : "877038846969597952",
  "text" : "Touch down at Changi https:\/\/t.co\/TOktm49Kd6 https:\/\/t.co\/uGVKPudAUA",
  "id" : 877038846969597952,
  "created_at" : "2017-06-20 05:42:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876902978015215616",
  "text" : "Flight delayed in transit at Abu Dhabi Airport. Not flying with that Airline again.",
  "id" : 876902978015215616,
  "created_at" : "2017-06-19 20:42:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "EU Council",
      "screen_name" : "EUCouncil",
      "indices" : [ 3, 13 ],
      "id_str" : "206717989",
      "id" : 206717989
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "roamlikeathome",
      "indices" : [ 70, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876888221426339840",
  "text" : "RT @EUCouncil: No more reasons to decline a call when abroad! Now you #roamlikeathome &amp; pay the same as back home when you call+surf on hol\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EUCouncil\/status\/875223852539756546\/video\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/tfBBI93hjN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCTJYXmXcAALQsR.jpg",
        "id_str" : "875043636144156672",
        "id" : 875043636144156672,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCTJYXmXcAALQsR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tfBBI93hjN"
      } ],
      "hashtags" : [ {
        "text" : "roamlikeathome",
        "indices" : [ 55, 70 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875223852539756546",
    "text" : "No more reasons to decline a call when abroad! Now you #roamlikeathome &amp; pay the same as back home when you call+surf on holiday in the EU. https:\/\/t.co\/tfBBI93hjN",
    "id" : 875223852539756546,
    "created_at" : "2017-06-15 05:30:01 +0000",
    "user" : {
      "name" : "EU Council",
      "screen_name" : "EUCouncil",
      "protected" : false,
      "id_str" : "206717989",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/997381475203977216\/sJGPZ8JT_normal.jpg",
      "id" : 206717989,
      "verified" : true
    }
  },
  "id" : 876888221426339840,
  "created_at" : "2017-06-19 19:43:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aleesha Tully",
      "screen_name" : "aleeshajulia",
      "indices" : [ 3, 16 ],
      "id_str" : "20982899",
      "id" : 20982899
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dublin",
      "indices" : [ 57, 64 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876705857706364928",
  "text" : "RT @aleeshajulia: As close to Kavanagh as you can get in #Dublin. \"Canal water, preferably, so stilly Greeny at the heart of summer.\" https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/aleeshajulia\/status\/876701880885358593\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/5NUnj6dJaw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCqrtDNXYAATquZ.jpg",
        "id_str" : "876701867920809984",
        "id" : 876701867920809984,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCqrtDNXYAATquZ.jpg",
        "sizes" : [ {
          "h" : 891,
          "resize" : "fit",
          "w" : 1334
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 891,
          "resize" : "fit",
          "w" : 1334
        }, {
          "h" : 801,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5NUnj6dJaw"
      } ],
      "hashtags" : [ {
        "text" : "Dublin",
        "indices" : [ 39, 46 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/mRtwrzEWH2",
        "expanded_url" : "https:\/\/instagram.com\/p\/BVgyxNmj7S7\/",
        "display_url" : "instagram.com\/p\/BVgyxNmj7S7\/"
      } ]
    },
    "geo" : { },
    "id_str" : "876701880885358593",
    "text" : "As close to Kavanagh as you can get in #Dublin. \"Canal water, preferably, so stilly Greeny at the heart of summer.\" https:\/\/t.co\/mRtwrzEWH2 https:\/\/t.co\/5NUnj6dJaw",
    "id" : 876701880885358593,
    "created_at" : "2017-06-19 07:23:11 +0000",
    "user" : {
      "name" : "Aleesha Tully",
      "screen_name" : "aleeshajulia",
      "protected" : false,
      "id_str" : "20982899",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1010492793658146818\/xj00_U4F_normal.jpg",
      "id" : 20982899,
      "verified" : false
    }
  },
  "id" : 876705857706364928,
  "created_at" : "2017-06-19 07:38:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "whykay \uD83D\uDC69\uD83C\uDFFB\u200D\uD83D\uDCBB\uD83D\uDC08\uD83C\uDF08",
      "screen_name" : "whykay",
      "indices" : [ 3, 10 ],
      "id_str" : "75483",
      "id" : 75483
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "FrTed",
      "indices" : [ 110, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876569223962800129",
  "text" : "RT @whykay: Well, that was an unexpected headline to see... a Rose from Hong Kong! \uD83D\uDE2E Still reminds me of that #FrTed's ep https:\/\/t.co\/6XRl\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "FrTed",
        "indices" : [ 98, 104 ]
      } ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/6XRlLdwtft",
        "expanded_url" : "https:\/\/twitter.com\/father_ted_\/status\/501444578764148737",
        "display_url" : "twitter.com\/father_ted_\/st\u2026"
      }, {
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/dFPbyV5CXq",
        "expanded_url" : "https:\/\/twitter.com\/irishchinese\/status\/875796266105655297",
        "display_url" : "twitter.com\/irishchinese\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875816027929665536",
    "text" : "Well, that was an unexpected headline to see... a Rose from Hong Kong! \uD83D\uDE2E Still reminds me of that #FrTed's ep https:\/\/t.co\/6XRlLdwtft \uD83E\uDD23 https:\/\/t.co\/dFPbyV5CXq",
    "id" : 875816027929665536,
    "created_at" : "2017-06-16 20:43:07 +0000",
    "user" : {
      "name" : "whykay \uD83D\uDC69\uD83C\uDFFB\u200D\uD83D\uDCBB\uD83D\uDC08\uD83C\uDF08",
      "screen_name" : "whykay",
      "protected" : false,
      "id_str" : "75483",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/921662883452375041\/4pny2jQz_normal.jpg",
      "id" : 75483,
      "verified" : false
    }
  },
  "id" : 876569223962800129,
  "created_at" : "2017-06-18 22:36:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/876559935185842177\/photo\/1",
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/bI4wMYvOGk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCoqcmjW0AAWw2p.jpg",
      "id_str" : "876559748350529536",
      "id" : 876559748350529536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCoqcmjW0AAWw2p.jpg",
      "sizes" : [ {
        "h" : 88,
        "resize" : "fit",
        "w" : 322
      }, {
        "h" : 88,
        "resize" : "fit",
        "w" : 322
      }, {
        "h" : 88,
        "resize" : "fit",
        "w" : 322
      }, {
        "h" : 88,
        "resize" : "fit",
        "w" : 322
      }, {
        "h" : 88,
        "resize" : "crop",
        "w" : 88
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bI4wMYvOGk"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "876558552864874498",
  "geo" : { },
  "id_str" : "876559935185842177",
  "in_reply_to_user_id" : 9465632,
  "text" : "Good luck if you want to copy the detail direction to a hotel. https:\/\/t.co\/bI4wMYvOGk",
  "id" : 876559935185842177,
  "in_reply_to_status_id" : 876558552864874498,
  "created_at" : "2017-06-18 21:59:08 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876558552864874498",
  "text" : "Why do some website don't allow you to select text?",
  "id" : 876558552864874498,
  "created_at" : "2017-06-18 21:53:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/876367967801495553\/photo\/1",
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/r40hXaEKmL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCl8BaHXkAAV_U8.jpg",
      "id_str" : "876367966132211712",
      "id" : 876367966132211712,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCl8BaHXkAAV_U8.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/r40hXaEKmL"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/UnpK23sNkA",
      "expanded_url" : "http:\/\/ift.tt\/2sGDovm",
      "display_url" : "ift.tt\/2sGDovm"
    } ]
  },
  "geo" : { },
  "id_str" : "876367967801495553",
  "text" : "Demonstration for homeless in Dublin https:\/\/t.co\/UnpK23sNkA https:\/\/t.co\/r40hXaEKmL",
  "id" : 876367967801495553,
  "created_at" : "2017-06-18 09:16:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Neal Cross",
      "screen_name" : "Neal_X",
      "indices" : [ 3, 10 ],
      "id_str" : "349579272",
      "id" : 349579272
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876328677763665920",
  "text" : "RT @Neal_X: Astonishing story of an entrepreneur replacing opium poppies with mulberry in Afghanistan, hope I manage to achieve\u2026https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/2JqO1gkoSD",
        "expanded_url" : "https:\/\/lnkd.in\/ftsd4Fz",
        "display_url" : "lnkd.in\/ftsd4Fz"
      } ]
    },
    "geo" : { },
    "id_str" : "876254412011917312",
    "text" : "Astonishing story of an entrepreneur replacing opium poppies with mulberry in Afghanistan, hope I manage to achieve\u2026https:\/\/t.co\/2JqO1gkoSD",
    "id" : 876254412011917312,
    "created_at" : "2017-06-18 01:45:06 +0000",
    "user" : {
      "name" : "Neal Cross",
      "screen_name" : "Neal_X",
      "protected" : false,
      "id_str" : "349579272",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/640127153758081025\/QRygZHRk_normal.jpg",
      "id" : 349579272,
      "verified" : false
    }
  },
  "id" : 876328677763665920,
  "created_at" : "2017-06-18 06:40:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/jm11l09f88",
      "expanded_url" : "https:\/\/azure.microsoft.com\/en-us\/resources\/templates\/?v=17.23h",
      "display_url" : "azure.microsoft.com\/en-us\/resource\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "876165820291919872",
  "text" : "Infrastructure as code https:\/\/t.co\/jm11l09f88",
  "id" : 876165820291919872,
  "created_at" : "2017-06-17 19:53:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/nyNk2CpGCZ",
      "expanded_url" : "https:\/\/twitter.com\/databreak\/status\/876138463808114688",
      "display_url" : "twitter.com\/databreak\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "876143308124413953",
  "text" : "My $ is on the Shark https:\/\/t.co\/nyNk2CpGCZ",
  "id" : 876143308124413953,
  "created_at" : "2017-06-17 18:23:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Etihad Airways",
      "screen_name" : "EtihadAirways",
      "indices" : [ 0, 14 ],
      "id_str" : "45621423",
      "id" : 45621423
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "876130506928271360",
  "geo" : { },
  "id_str" : "876132029112385540",
  "in_reply_to_user_id" : 9465632,
  "text" : "@EtihadAirways I aware u changed the aircraft &amp; inform us in Jan but why change our seat arrangement?",
  "id" : 876132029112385540,
  "in_reply_to_status_id" : 876130506928271360,
  "created_at" : "2017-06-17 17:38:48 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rachel Thomas",
      "screen_name" : "math_rachel",
      "indices" : [ 3, 15 ],
      "id_str" : "1408142352",
      "id" : 1408142352
    }, {
      "name" : "bradford cross",
      "screen_name" : "bradfordcross",
      "indices" : [ 71, 85 ],
      "id_str" : "36153601",
      "id" : 36153601
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/math_rachel\/status\/875817282244493312\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/ZIbDazgCA2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCeG5ToV0AArCxE.jpg",
      "id_str" : "875816971626991616",
      "id" : 875816971626991616,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCeG5ToV0AArCxE.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 539,
        "resize" : "fit",
        "w" : 565
      }, {
        "h" : 539,
        "resize" : "fit",
        "w" : 565
      }, {
        "h" : 539,
        "resize" : "fit",
        "w" : 565
      }, {
        "h" : 539,
        "resize" : "fit",
        "w" : 565
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ZIbDazgCA2"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/p67Z8fbRkl",
      "expanded_url" : "http:\/\/www.bradfordcross.com\/blog\/2017\/6\/13\/vertical-ai-startups-solving-industry-specific-problems-by-combining-ai-and-subject-matter-expertise",
      "display_url" : "bradfordcross.com\/blog\/2017\/6\/13\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "876130935800041472",
  "text" : "RT @math_rachel: Today\u2019s neural net novelty is tomorrow\u2019s open source. @bradfordcross https:\/\/t.co\/p67Z8fbRkl https:\/\/t.co\/ZIbDazgCA2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "bradford cross",
        "screen_name" : "bradfordcross",
        "indices" : [ 54, 68 ],
        "id_str" : "36153601",
        "id" : 36153601
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/math_rachel\/status\/875817282244493312\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/ZIbDazgCA2",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCeG5ToV0AArCxE.jpg",
        "id_str" : "875816971626991616",
        "id" : 875816971626991616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCeG5ToV0AArCxE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 539,
          "resize" : "fit",
          "w" : 565
        }, {
          "h" : 539,
          "resize" : "fit",
          "w" : 565
        }, {
          "h" : 539,
          "resize" : "fit",
          "w" : 565
        }, {
          "h" : 539,
          "resize" : "fit",
          "w" : 565
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ZIbDazgCA2"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/p67Z8fbRkl",
        "expanded_url" : "http:\/\/www.bradfordcross.com\/blog\/2017\/6\/13\/vertical-ai-startups-solving-industry-specific-problems-by-combining-ai-and-subject-matter-expertise",
        "display_url" : "bradfordcross.com\/blog\/2017\/6\/13\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875817282244493312",
    "text" : "Today\u2019s neural net novelty is tomorrow\u2019s open source. @bradfordcross https:\/\/t.co\/p67Z8fbRkl https:\/\/t.co\/ZIbDazgCA2",
    "id" : 875817282244493312,
    "created_at" : "2017-06-16 20:48:06 +0000",
    "user" : {
      "name" : "Rachel Thomas",
      "screen_name" : "math_rachel",
      "protected" : false,
      "id_str" : "1408142352",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/802658972784214016\/_MmQnI3K_normal.jpg",
      "id" : 1408142352,
      "verified" : false
    }
  },
  "id" : 876130935800041472,
  "created_at" : "2017-06-17 17:34:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Etihad Airways",
      "screen_name" : "EtihadAirways",
      "indices" : [ 0, 14 ],
      "id_str" : "45621423",
      "id" : 45621423
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "876130506928271360",
  "in_reply_to_user_id" : 45621423,
  "text" : "@EtihadAirways We booked the flight few months back.Check-in today &amp; found you have separate 3 of us. What going on? \uD83D\uDE2C",
  "id" : 876130506928271360,
  "created_at" : "2017-06-17 17:32:45 +0000",
  "in_reply_to_screen_name" : "EtihadAirways",
  "in_reply_to_user_id_str" : "45621423",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/876051359442886656\/photo\/1",
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/q4VwdcHVVZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DChcEVeXsAAi5rU.jpg",
      "id_str" : "876051357077319680",
      "id" : 876051357077319680,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DChcEVeXsAAi5rU.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/q4VwdcHVVZ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/0cpfA85cHb",
      "expanded_url" : "http:\/\/ift.tt\/2sc5oW3",
      "display_url" : "ift.tt\/2sc5oW3"
    } ]
  },
  "geo" : { },
  "id_str" : "876051359442886656",
  "text" : "Brilliant weather in Dublin,TCD https:\/\/t.co\/0cpfA85cHb https:\/\/t.co\/q4VwdcHVVZ",
  "id" : 876051359442886656,
  "created_at" : "2017-06-17 12:18:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SQLSatDublin",
      "indices" : [ 15, 28 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875973141243322370",
  "text" : "Heading to TCD #SQLSatDublin event",
  "id" : 875973141243322370,
  "created_at" : "2017-06-17 07:07:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/0zwn9SpOX4",
      "expanded_url" : "http:\/\/tabula.technology\/",
      "display_url" : "tabula.technology"
    } ]
  },
  "geo" : { },
  "id_str" : "875813289594429442",
  "text" : "Need to extract data tables from PDF? Checkout https:\/\/t.co\/0zwn9SpOX4",
  "id" : 875813289594429442,
  "created_at" : "2017-06-16 20:32:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/Lr6f6HTTcN",
      "expanded_url" : "http:\/\/brilliantnoise.com\/wp-content\/uploads\/NokiaSocial.pdf",
      "display_url" : "brilliantnoise.com\/wp-content\/upl\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875812904632758273",
  "text" : "Nokia\u2019s principles for social media marketing. https:\/\/t.co\/Lr6f6HTTcN",
  "id" : 875812904632758273,
  "created_at" : "2017-06-16 20:30:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875806914248101890",
  "text" : "What do you think of the new Twitter interface design?",
  "id" : 875806914248101890,
  "created_at" : "2017-06-16 20:06:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/v2ekgpXwqw",
      "expanded_url" : "http:\/\/www.cleverlayover.com\/blog\/index.php\/growing-a-marketplace-how-to-balance-sellers-and-buyers-using-customer-lifetime-value\/",
      "display_url" : "cleverlayover.com\/blog\/index.php\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875806628267913216",
  "text" : "Growing a marketplace? How to balance sellers and buyers using customer lifetime value https:\/\/t.co\/v2ekgpXwqw",
  "id" : 875806628267913216,
  "created_at" : "2017-06-16 20:05:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875781336337965056",
  "text" : "I wonder at times what friends, associate would say about me to the media when I died.\uD83D\uDE01",
  "id" : 875781336337965056,
  "created_at" : "2017-06-16 18:25:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zarino Zappia",
      "screen_name" : "zarino",
      "indices" : [ 3, 10 ],
      "id_str" : "3613361",
      "id" : 3613361
    }, {
      "name" : "Andy Cotgreave",
      "screen_name" : "acotgreave",
      "indices" : [ 12, 23 ],
      "id_str" : "119704541",
      "id" : 119704541
    }, {
      "name" : "Donald J. Trump",
      "screen_name" : "realDonaldTrump",
      "indices" : [ 24, 40 ],
      "id_str" : "25073877",
      "id" : 25073877
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875770994195091456",
  "text" : "RT @zarino: @acotgreave @realDonaldTrump Can't believe I'm justifying something that toad says, but, reach could easily top 100m once you f\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Andy Cotgreave",
        "screen_name" : "acotgreave",
        "indices" : [ 0, 11 ],
        "id_str" : "119704541",
        "id" : 119704541
      }, {
        "name" : "Donald J. Trump",
        "screen_name" : "realDonaldTrump",
        "indices" : [ 12, 28 ],
        "id_str" : "25073877",
        "id" : 25073877
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "875747082463834112",
    "geo" : { },
    "id_str" : "875756857771663360",
    "in_reply_to_user_id" : 119704541,
    "text" : "@acotgreave @realDonaldTrump Can't believe I'm justifying something that toad says, but, reach could easily top 100m once you factor in followers retweeting\/sharing.",
    "id" : 875756857771663360,
    "in_reply_to_status_id" : 875747082463834112,
    "created_at" : "2017-06-16 16:48:00 +0000",
    "in_reply_to_screen_name" : "acotgreave",
    "in_reply_to_user_id_str" : "119704541",
    "user" : {
      "name" : "Zarino Zappia",
      "screen_name" : "zarino",
      "protected" : false,
      "id_str" : "3613361",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1770997410\/IMG_3153_small_normal.jpg",
      "id" : 3613361,
      "verified" : false
    }
  },
  "id" : 875770994195091456,
  "created_at" : "2017-06-16 17:44:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    }, {
      "name" : "Gerald Ashley",
      "screen_name" : "Gerald_Ashley",
      "indices" : [ 67, 81 ],
      "id_str" : "223157835",
      "id" : 223157835
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/875753025880694785\/photo\/1",
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/XpG9cho1uJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCdKhbfXgAEAwI6.jpg",
      "id_str" : "875750590722310145",
      "id" : 875750590722310145,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCdKhbfXgAEAwI6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XpG9cho1uJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875755734281183232",
  "text" : "RT @rshotton: Not sure if the asterix was needed in this ad...\n\nHT @Gerald_Ashley https:\/\/t.co\/XpG9cho1uJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Gerald Ashley",
        "screen_name" : "Gerald_Ashley",
        "indices" : [ 53, 67 ],
        "id_str" : "223157835",
        "id" : 223157835
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/875753025880694785\/photo\/1",
        "indices" : [ 68, 91 ],
        "url" : "https:\/\/t.co\/XpG9cho1uJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCdKhbfXgAEAwI6.jpg",
        "id_str" : "875750590722310145",
        "id" : 875750590722310145,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCdKhbfXgAEAwI6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XpG9cho1uJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875753025880694785",
    "text" : "Not sure if the asterix was needed in this ad...\n\nHT @Gerald_Ashley https:\/\/t.co\/XpG9cho1uJ",
    "id" : 875753025880694785,
    "created_at" : "2017-06-16 16:32:46 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 875755734281183232,
  "created_at" : "2017-06-16 16:43:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yashar Ali \uD83D\uDC18",
      "screen_name" : "yashar",
      "indices" : [ 3, 10 ],
      "id_str" : "11744152",
      "id" : 11744152
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875744506083254272",
  "text" : "RT @yashar: Helmut Kohl, who served as Chancellor of Germany for 16 years and presided over re-unification, is dead at 87. https:\/\/t.co\/XEZ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/yashar\/status\/875740728235704322\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/XEZy00Nxsq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCdBfuCUMAc1BAs.jpg",
        "id_str" : "875740665736343559",
        "id" : 875740665736343559,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCdBfuCUMAc1BAs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 400
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 400
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XEZy00Nxsq"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875740728235704322",
    "text" : "Helmut Kohl, who served as Chancellor of Germany for 16 years and presided over re-unification, is dead at 87. https:\/\/t.co\/XEZy00Nxsq",
    "id" : 875740728235704322,
    "created_at" : "2017-06-16 15:43:54 +0000",
    "user" : {
      "name" : "Yashar Ali \uD83D\uDC18",
      "screen_name" : "yashar",
      "protected" : false,
      "id_str" : "11744152",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/654406754164346884\/mofw7nin_normal.jpg",
      "id" : 11744152,
      "verified" : true
    }
  },
  "id" : 875744506083254272,
  "created_at" : "2017-06-16 15:58:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875742137270358017",
  "text" : "Asking for a little friend. Can the palace temporary provide shelter to those resident?",
  "id" : 875742137270358017,
  "created_at" : "2017-06-16 15:49:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Weiner",
      "screen_name" : "jeffweiner",
      "indices" : [ 3, 14 ],
      "id_str" : "20348377",
      "id" : 20348377
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875739038996729857",
  "text" : "RT @jeffweiner: Only 1 company on earth can buy grocery chain, be rumored to buy enterprise software company &amp; in both cases be lauded for\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.linkedin.com\/\" rel=\"nofollow\"\u003ELinkedIn\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875729299109806080",
    "text" : "Only 1 company on earth can buy grocery chain, be rumored to buy enterprise software company &amp; in both cases be lauded for strategic vision",
    "id" : 875729299109806080,
    "created_at" : "2017-06-16 14:58:29 +0000",
    "user" : {
      "name" : "Jeff Weiner",
      "screen_name" : "jeffweiner",
      "protected" : false,
      "id_str" : "20348377",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/500480798886084611\/P5XUPlTJ_normal.jpeg",
      "id" : 20348377,
      "verified" : true
    }
  },
  "id" : 875739038996729857,
  "created_at" : "2017-06-16 15:37:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Victoria Macarthur\uD83C\uDF42",
      "screen_name" : "cufa",
      "indices" : [ 0, 5 ],
      "id_str" : "9522142",
      "id" : 9522142
    }, {
      "name" : "The GEC",
      "screen_name" : "GECinD8",
      "indices" : [ 6, 14 ],
      "id_str" : "137327916",
      "id" : 137327916
    }, {
      "name" : "Dublin BIC",
      "screen_name" : "BICDublin",
      "indices" : [ 15, 25 ],
      "id_str" : "567309612",
      "id" : 567309612
    }, {
      "name" : "Eamonn Sayers",
      "screen_name" : "EamonnWSayers",
      "indices" : [ 26, 40 ],
      "id_str" : "2153284135",
      "id" : 2153284135
    }, {
      "name" : "The Liberties Dublin",
      "screen_name" : "libertiesdublin",
      "indices" : [ 41, 57 ],
      "id_str" : "762230522",
      "id" : 762230522
    }, {
      "name" : "StartupDublin.com",
      "screen_name" : "TechinDub",
      "indices" : [ 58, 68 ],
      "id_str" : "4796026179",
      "id" : 4796026179
    }, {
      "name" : "Sean@Find A Venue",
      "screen_name" : "findavenue",
      "indices" : [ 69, 80 ],
      "id_str" : "285546571",
      "id" : 285546571
    }, {
      "name" : "huggity",
      "screen_name" : "huggity",
      "indices" : [ 81, 89 ],
      "id_str" : "319613578",
      "id" : 319613578
    }, {
      "name" : "Ticketsolve",
      "screen_name" : "ticketsolvers",
      "indices" : [ 90, 104 ],
      "id_str" : "125292341",
      "id" : 125292341
    }, {
      "name" : "TeelingWhiskey",
      "screen_name" : "TeelingWhiskey",
      "indices" : [ 105, 120 ],
      "id_str" : "608869901",
      "id" : 608869901
    }, {
      "name" : "Surplus Stock WW",
      "screen_name" : "SurplusStockWW",
      "indices" : [ 121, 136 ],
      "id_str" : "155313569",
      "id" : 155313569
    }, {
      "name" : "Michael Culligan",
      "screen_name" : "mculligan",
      "indices" : [ 137, 147 ],
      "id_str" : "211907015",
      "id" : 211907015
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "875687549968240640",
  "geo" : { },
  "id_str" : "875690449637449728",
  "in_reply_to_user_id" : 9522142,
  "text" : "@cufa @GECinD8 @BICDublin @EamonnWSayers @libertiesdublin @TechinDub @findavenue @huggity @ticketsolvers @TeelingWhiskey @SurplusStockWW @mculligan bouncing castle for GEC tenants? :)",
  "id" : 875690449637449728,
  "in_reply_to_status_id" : 875687549968240640,
  "created_at" : "2017-06-16 12:24:07 +0000",
  "in_reply_to_screen_name" : "cufa",
  "in_reply_to_user_id_str" : "9522142",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Manchester City Council",
      "screen_name" : "ManCityCouncil",
      "indices" : [ 3, 18 ],
      "id_str" : "26461381",
      "id" : 26461381
    }, {
      "name" : "Manchester Lit Fest",
      "screen_name" : "McrLitFest",
      "indices" : [ 106, 117 ],
      "id_str" : "63077885",
      "id" : 63077885
    }, {
      "name" : "Manchester Met Uni",
      "screen_name" : "ManMetUni",
      "indices" : [ 118, 128 ],
      "id_str" : "14743052",
      "id" : 14743052
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 82, 105 ],
      "url" : "https:\/\/t.co\/oGpswZcmA7",
      "expanded_url" : "http:\/\/socsi.in\/A48GR",
      "display_url" : "socsi.in\/A48GR"
    } ]
  },
  "geo" : { },
  "id_str" : "875685752188874752",
  "text" : "RT @ManCityCouncil: Manchester announces bid for UNESCO City of Literature status https:\/\/t.co\/oGpswZcmA7 @McrLitFest @ManMetUni  @Official\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialsignin.co.uk\" rel=\"nofollow\"\u003ESocialSignIn Application\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Manchester Lit Fest",
        "screen_name" : "McrLitFest",
        "indices" : [ 86, 97 ],
        "id_str" : "63077885",
        "id" : 63077885
      }, {
        "name" : "Manchester Met Uni",
        "screen_name" : "ManMetUni",
        "indices" : [ 98, 108 ],
        "id_str" : "14743052",
        "id" : 14743052
      }, {
        "name" : "The University of Manchester",
        "screen_name" : "OfficialUoM",
        "indices" : [ 110, 122 ],
        "id_str" : "2217874898",
        "id" : 2217874898
      }, {
        "name" : "Manchester Libraries",
        "screen_name" : "MancLibraries",
        "indices" : [ 123, 137 ],
        "id_str" : "7171922",
        "id" : 7171922
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ManCityCouncil\/status\/875655380247789568\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/i8V1dVZkOp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCbz7RZVYAADfX7.jpg",
        "id_str" : "875655377177632768",
        "id" : 875655377177632768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCbz7RZVYAADfX7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 677
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 677
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 677
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 677
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/i8V1dVZkOp"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/oGpswZcmA7",
        "expanded_url" : "http:\/\/socsi.in\/A48GR",
        "display_url" : "socsi.in\/A48GR"
      } ]
    },
    "geo" : { },
    "id_str" : "875655380247789568",
    "text" : "Manchester announces bid for UNESCO City of Literature status https:\/\/t.co\/oGpswZcmA7 @McrLitFest @ManMetUni  @OfficialUoM @MancLibraries https:\/\/t.co\/i8V1dVZkOp",
    "id" : 875655380247789568,
    "created_at" : "2017-06-16 10:04:46 +0000",
    "user" : {
      "name" : "Manchester City Council",
      "screen_name" : "ManCityCouncil",
      "protected" : false,
      "id_str" : "26461381",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001402764235329536\/QJtTAOkZ_normal.jpg",
      "id" : 26461381,
      "verified" : true
    }
  },
  "id" : 875685752188874752,
  "created_at" : "2017-06-16 12:05:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NYT Photo",
      "screen_name" : "nytimesphoto",
      "indices" : [ 3, 16 ],
      "id_str" : "22411875",
      "id" : 22411875
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875685644374347777",
  "text" : "RT @nytimesphoto: No, this is not an illustration from a book by Dr. Seuss. In Morocco, hungry goats graze in trees. https:\/\/t.co\/khs5Yfy4x\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/nytimesphoto\/status\/874318175323684865\/photo\/1",
        "indices" : [ 123, 146 ],
        "url" : "https:\/\/t.co\/kWLOd17Uu8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCIzvnBWAAYWm6D.jpg",
        "id_str" : "874318170684784646",
        "id" : 874318170684784646,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCIzvnBWAAYWm6D.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 560,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kWLOd17Uu8"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/khs5Yfy4xg",
        "expanded_url" : "http:\/\/nyti.ms\/2stJiQ4",
        "display_url" : "nyti.ms\/2stJiQ4"
      } ]
    },
    "geo" : { },
    "id_str" : "874318175323684865",
    "text" : "No, this is not an illustration from a book by Dr. Seuss. In Morocco, hungry goats graze in trees. https:\/\/t.co\/khs5Yfy4xg https:\/\/t.co\/kWLOd17Uu8",
    "id" : 874318175323684865,
    "created_at" : "2017-06-12 17:31:11 +0000",
    "user" : {
      "name" : "NYT Photo",
      "screen_name" : "nytimesphoto",
      "protected" : false,
      "id_str" : "22411875",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963809333552168960\/LwLR4AEJ_normal.jpg",
      "id" : 22411875,
      "verified" : true
    }
  },
  "id" : 875685644374347777,
  "created_at" : "2017-06-16 12:05:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/875684752300400640\/photo\/1",
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/FLk5ELG13F",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCcOo_FWAAEfX8i.jpg",
      "id_str" : "875684749838254081",
      "id" : 875684749838254081,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCcOo_FWAAEfX8i.jpg",
      "sizes" : [ {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/FLk5ELG13F"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/5hBOOm3LzV",
      "expanded_url" : "http:\/\/ift.tt\/2rDhkgg",
      "display_url" : "ift.tt\/2rDhkgg"
    } ]
  },
  "geo" : { },
  "id_str" : "875684752300400640",
  "text" : "Marlay Park,Dublin https:\/\/t.co\/5hBOOm3LzV https:\/\/t.co\/FLk5ELG13F",
  "id" : 875684752300400640,
  "created_at" : "2017-06-16 12:01:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/875642987283230721\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/tfsJpL2B6o",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCbooqfUwAIwOd-.jpg",
      "id_str" : "875642962868224002",
      "id" : 875642962868224002,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCbooqfUwAIwOd-.jpg",
      "sizes" : [ {
        "h" : 146,
        "resize" : "fit",
        "w" : 599
      }, {
        "h" : 146,
        "resize" : "crop",
        "w" : 146
      }, {
        "h" : 146,
        "resize" : "fit",
        "w" : 599
      }, {
        "h" : 146,
        "resize" : "fit",
        "w" : 599
      }, {
        "h" : 146,
        "resize" : "fit",
        "w" : 599
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tfsJpL2B6o"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875642987283230721",
  "text" : "Check for contrast between marketing experience and product experience. https:\/\/t.co\/tfsJpL2B6o",
  "id" : 875642987283230721,
  "created_at" : "2017-06-16 09:15:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Ramsden",
      "screen_name" : "danramsden",
      "indices" : [ 3, 14 ],
      "id_str" : "14077213",
      "id" : 14077213
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875639708415442944",
  "text" : "RT @danramsden: I\u2019m doing some research into Imposter syndrome and how creative people balance confidence and doubt. Please join in https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/lYVzcBVnlx",
        "expanded_url" : "https:\/\/goo.gl\/forms\/aJnWJEoxXVgkHjQ53",
        "display_url" : "goo.gl\/forms\/aJnWJEox\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "874919204708921346",
    "text" : "I\u2019m doing some research into Imposter syndrome and how creative people balance confidence and doubt. Please join in https:\/\/t.co\/lYVzcBVnlx",
    "id" : 874919204708921346,
    "created_at" : "2017-06-14 09:19:28 +0000",
    "user" : {
      "name" : "Dan Ramsden",
      "screen_name" : "danramsden",
      "protected" : false,
      "id_str" : "14077213",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/513009918874382336\/-gGLNWr-_normal.jpeg",
      "id" : 14077213,
      "verified" : false
    }
  },
  "id" : 875639708415442944,
  "created_at" : "2017-06-16 09:02:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/c05MSCESLH",
      "expanded_url" : "http:\/\/menwithpens.ca\/what-clients-need-to-hear\/",
      "display_url" : "menwithpens.ca\/what-clients-n\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875616467307249665",
  "text" : "5 Smart Sentences that Clients Need to Hear from You https:\/\/t.co\/c05MSCESLH",
  "id" : 875616467307249665,
  "created_at" : "2017-06-16 07:30:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Woo Huiren",
      "screen_name" : "woohuiren",
      "indices" : [ 3, 13 ],
      "id_str" : "1454430638",
      "id" : 1454430638
    }, {
      "name" : "Open Innovation Labs",
      "screen_name" : "RedHatLabs",
      "indices" : [ 73, 84 ],
      "id_str" : "784038868827922432",
      "id" : 784038868827922432
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/bpdlBai2ld",
      "expanded_url" : "http:\/\/red.ht\/2sbZ0N0",
      "display_url" : "red.ht\/2sbZ0N0"
    } ]
  },
  "geo" : { },
  "id_str" : "875609356221337600",
  "text" : "RT @woohuiren: Curious about impact mapping techniques? Justin Holmes of @RedHatLabs has a beginner's guide: https:\/\/t.co\/bpdlBai2ld #TheOp\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Open Innovation Labs",
        "screen_name" : "RedHatLabs",
        "indices" : [ 58, 69 ],
        "id_str" : "784038868827922432",
        "id" : 784038868827922432
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TheOpenOrg",
        "indices" : [ 118, 129 ]
      } ],
      "urls" : [ {
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/bpdlBai2ld",
        "expanded_url" : "http:\/\/red.ht\/2sbZ0N0",
        "display_url" : "red.ht\/2sbZ0N0"
      } ]
    },
    "geo" : { },
    "id_str" : "875594933167505409",
    "text" : "Curious about impact mapping techniques? Justin Holmes of @RedHatLabs has a beginner's guide: https:\/\/t.co\/bpdlBai2ld #TheOpenOrg",
    "id" : 875594933167505409,
    "created_at" : "2017-06-16 06:04:34 +0000",
    "user" : {
      "name" : "Woo Huiren",
      "screen_name" : "woohuiren",
      "protected" : false,
      "id_str" : "1454430638",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/663623265231892480\/dX2lyJHk_normal.jpg",
      "id" : 1454430638,
      "verified" : false
    }
  },
  "id" : 875609356221337600,
  "created_at" : "2017-06-16 07:01:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875609063714766848",
  "text" : "If your product product satisfy a particular market niche, you don't really have to work so hard on marketing?",
  "id" : 875609063714766848,
  "created_at" : "2017-06-16 07:00:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fiona Boyd",
      "screen_name" : "FionaK",
      "indices" : [ 3, 10 ],
      "id_str" : "10894022",
      "id" : 10894022
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/ElgnY6Rrfw",
      "expanded_url" : "http:\/\/www.thetechedvocate.org\/school-district-building-diy-broadband-network\/",
      "display_url" : "thetechedvocate.org\/school-distric\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875606916738588672",
  "text" : "RT @FionaK: A school district is building a DIY broadband network https:\/\/t.co\/ElgnY6Rrfw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/ElgnY6Rrfw",
        "expanded_url" : "http:\/\/www.thetechedvocate.org\/school-district-building-diy-broadband-network\/",
        "display_url" : "thetechedvocate.org\/school-distric\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875162595950436352",
    "text" : "A school district is building a DIY broadband network https:\/\/t.co\/ElgnY6Rrfw",
    "id" : 875162595950436352,
    "created_at" : "2017-06-15 01:26:37 +0000",
    "user" : {
      "name" : "Fiona Boyd",
      "screen_name" : "FionaK",
      "protected" : false,
      "id_str" : "10894022",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/789721873198092288\/MHtvgKm7_normal.jpg",
      "id" : 10894022,
      "verified" : false
    }
  },
  "id" : 875606916738588672,
  "created_at" : "2017-06-16 06:52:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/DN8ZN4C0OH",
      "expanded_url" : "http:\/\/bit.ly\/2s7OVlQ",
      "display_url" : "bit.ly\/2s7OVlQ"
    } ]
  },
  "geo" : { },
  "id_str" : "875606320254074880",
  "text" : "Just updated my Twitter profile https:\/\/t.co\/DN8ZN4C0OH Thanks for checking out.",
  "id" : 875606320254074880,
  "created_at" : "2017-06-16 06:49:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875396703821713408",
  "text" : "Lived in private apartment &amp; letter issued some months ago, it up to the resident to either stay put or run. We always chose to run.",
  "id" : 875396703821713408,
  "created_at" : "2017-06-15 16:56:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Holloway",
      "screen_name" : "dhollowayuk",
      "indices" : [ 3, 15 ],
      "id_str" : "20166648",
      "id" : 20166648
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/dhollowayuk\/status\/875354249101574144\/photo\/1",
      "indices" : [ 110, 133 ],
      "url" : "https:\/\/t.co\/sy3sgRdQhA",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCXiDF_VYAEQKme.jpg",
      "id_str" : "875354245368733697",
      "id" : 875354245368733697,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCXiDF_VYAEQKme.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 272,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 1000
      }, {
        "h" : 400,
        "resize" : "fit",
        "w" : 1000
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sy3sgRdQhA"
    } ],
    "hashtags" : [ {
      "text" : "Cloud",
      "indices" : [ 87, 93 ]
    }, {
      "text" : "Asia",
      "indices" : [ 94, 99 ]
    }, {
      "text" : "business",
      "indices" : [ 100, 109 ]
    } ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/cYXosccNTA",
      "expanded_url" : "http:\/\/bit.ly\/2sDf5xQ",
      "display_url" : "bit.ly\/2sDf5xQ"
    } ]
  },
  "geo" : { },
  "id_str" : "875355078516977664",
  "text" : "RT @dhollowayuk: Google launches new cloud region in Singapore https:\/\/t.co\/cYXosccNTA #Cloud #Asia #business https:\/\/t.co\/sy3sgRdQhA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dhollowayuk\/status\/875354249101574144\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/sy3sgRdQhA",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCXiDF_VYAEQKme.jpg",
        "id_str" : "875354245368733697",
        "id" : 875354245368733697,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCXiDF_VYAEQKme.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 272,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sy3sgRdQhA"
      } ],
      "hashtags" : [ {
        "text" : "Cloud",
        "indices" : [ 70, 76 ]
      }, {
        "text" : "Asia",
        "indices" : [ 77, 82 ]
      }, {
        "text" : "business",
        "indices" : [ 83, 92 ]
      } ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/cYXosccNTA",
        "expanded_url" : "http:\/\/bit.ly\/2sDf5xQ",
        "display_url" : "bit.ly\/2sDf5xQ"
      } ]
    },
    "geo" : { },
    "id_str" : "875354249101574144",
    "text" : "Google launches new cloud region in Singapore https:\/\/t.co\/cYXosccNTA #Cloud #Asia #business https:\/\/t.co\/sy3sgRdQhA",
    "id" : 875354249101574144,
    "created_at" : "2017-06-15 14:08:10 +0000",
    "user" : {
      "name" : "Dan Holloway",
      "screen_name" : "dhollowayuk",
      "protected" : false,
      "id_str" : "20166648",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/727433291867938816\/C4uohW_I_normal.jpg",
      "id" : 20166648,
      "verified" : false
    }
  },
  "id" : 875355078516977664,
  "created_at" : "2017-06-15 14:11:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft Ireland",
      "screen_name" : "Microsoftirl",
      "indices" : [ 3, 16 ],
      "id_str" : "2342240372",
      "id" : 2342240372
    }, {
      "name" : "John Frank",
      "screen_name" : "johnedwardfrank",
      "indices" : [ 93, 109 ],
      "id_str" : "24073354",
      "id" : 24073354
    }, {
      "name" : "Microsoft EU Policy",
      "screen_name" : "MicrosoftEU",
      "indices" : [ 113, 125 ],
      "id_str" : "2613385747",
      "id" : 2613385747
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875335384640757760",
  "text" : "RT @Microsoftirl: \"Like banks, we're now trusted with people's most important assets - data\" @johnedwardfrank of @microsoftEU #datasummitdu\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/prod2.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr Prod2\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "John Frank",
        "screen_name" : "johnedwardfrank",
        "indices" : [ 75, 91 ],
        "id_str" : "24073354",
        "id" : 24073354
      }, {
        "name" : "Microsoft EU Policy",
        "screen_name" : "MicrosoftEU",
        "indices" : [ 95, 107 ],
        "id_str" : "2613385747",
        "id" : 2613385747
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Microsoftirl\/status\/875282365194674176\/photo\/1",
        "indices" : [ 126, 149 ],
        "url" : "https:\/\/t.co\/VuY5t2D9MJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCWgq7yXYAA5_eb.jpg",
        "id_str" : "875282362057318400",
        "id" : 875282362057318400,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCWgq7yXYAA5_eb.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VuY5t2D9MJ"
      } ],
      "hashtags" : [ {
        "text" : "datasummitdublin",
        "indices" : [ 108, 125 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875282365194674176",
    "text" : "\"Like banks, we're now trusted with people's most important assets - data\" @johnedwardfrank of @microsoftEU #datasummitdublin https:\/\/t.co\/VuY5t2D9MJ",
    "id" : 875282365194674176,
    "created_at" : "2017-06-15 09:22:32 +0000",
    "user" : {
      "name" : "Microsoft Ireland",
      "screen_name" : "Microsoftirl",
      "protected" : false,
      "id_str" : "2342240372",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875717731080974336\/JiQ1F4GC_normal.jpg",
      "id" : 2342240372,
      "verified" : true
    }
  },
  "id" : 875335384640757760,
  "created_at" : "2017-06-15 12:53:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "P\u00E1draig Belton",
      "screen_name" : "PadraigBelton",
      "indices" : [ 3, 17 ],
      "id_str" : "14671775",
      "id" : 14671775
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/DztkYZwzF1",
      "expanded_url" : "https:\/\/twitter.com\/jolyonmaugham\/status\/875246963364696065",
      "display_url" : "twitter.com\/jolyonmaugham\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875332573563584512",
  "text" : "RT @PadraigBelton: Good man.  (And silk at Devereux Chambers.) https:\/\/t.co\/DztkYZwzF1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/DztkYZwzF1",
        "expanded_url" : "https:\/\/twitter.com\/jolyonmaugham\/status\/875246963364696065",
        "display_url" : "twitter.com\/jolyonmaugham\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875323743396143105",
    "text" : "Good man.  (And silk at Devereux Chambers.) https:\/\/t.co\/DztkYZwzF1",
    "id" : 875323743396143105,
    "created_at" : "2017-06-15 12:06:57 +0000",
    "user" : {
      "name" : "P\u00E1draig Belton",
      "screen_name" : "PadraigBelton",
      "protected" : false,
      "id_str" : "14671775",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/53815024\/patrick-belton_normal.jpg",
      "id" : 14671775,
      "verified" : false
    }
  },
  "id" : 875332573563584512,
  "created_at" : "2017-06-15 12:42:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross Tucker",
      "screen_name" : "Scienceofsport",
      "indices" : [ 3, 18 ],
      "id_str" : "19850521",
      "id" : 19850521
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875332327525822465",
  "text" : "RT @Scienceofsport: I'm giving two free public talks at UCD in Dublin on June 29th. One is on my research into head injuries in rugby... ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.facebook.com\/twitter\" rel=\"nofollow\"\u003EFacebook\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 117, 140 ],
        "url" : "https:\/\/t.co\/O9Ryf2aKvi",
        "expanded_url" : "http:\/\/fb.me\/1mJuJvyvr",
        "display_url" : "fb.me\/1mJuJvyvr"
      } ]
    },
    "geo" : { },
    "id_str" : "875316938943197185",
    "text" : "I'm giving two free public talks at UCD in Dublin on June 29th. One is on my research into head injuries in rugby... https:\/\/t.co\/O9Ryf2aKvi",
    "id" : 875316938943197185,
    "created_at" : "2017-06-15 11:39:55 +0000",
    "user" : {
      "name" : "Ross Tucker",
      "screen_name" : "Scienceofsport",
      "protected" : false,
      "id_str" : "19850521",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/824728074143100929\/HKSzNJhv_normal.jpg",
      "id" : 19850521,
      "verified" : true
    }
  },
  "id" : 875332327525822465,
  "created_at" : "2017-06-15 12:41:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/fXsvjwbknU",
      "expanded_url" : "http:\/\/blog.flickr.net\/en\/2017\/06\/06\/share-your-best-work-with-the-new-flickr-about-page\/",
      "display_url" : "blog.flickr.net\/en\/2017\/06\/06\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875299484812664833",
  "text" : "Share Your Best Work With The New Flickr About\u00A0Page https:\/\/t.co\/fXsvjwbknU",
  "id" : 875299484812664833,
  "created_at" : "2017-06-15 10:30:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Amber Baldet",
      "screen_name" : "AmberBaldet",
      "indices" : [ 3, 15 ],
      "id_str" : "14872837",
      "id" : 14872837
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875276788687044609",
  "text" : "RT @AmberBaldet: Now would probably be a good time to stop saying your company wants to be the Uber of anything.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "874947821698502656",
    "text" : "Now would probably be a good time to stop saying your company wants to be the Uber of anything.",
    "id" : 874947821698502656,
    "created_at" : "2017-06-14 11:13:11 +0000",
    "user" : {
      "name" : "Amber Baldet",
      "screen_name" : "AmberBaldet",
      "protected" : false,
      "id_str" : "14872837",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885671686732824576\/qVY8Zii8_normal.jpg",
      "id" : 14872837,
      "verified" : false
    }
  },
  "id" : 875276788687044609,
  "created_at" : "2017-06-15 09:00:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Satchin Panda",
      "screen_name" : "SatchinPanda",
      "indices" : [ 3, 16 ],
      "id_str" : "822894547",
      "id" : 822894547
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875253142975377408",
  "text" : "RT @SatchinPanda: One more reason to eat your broccoli. Sulforaphane improves glucose control in patients with type 2 diabetes https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/F4561ehznA",
        "expanded_url" : "http:\/\/stm.sciencemag.org\/content\/9\/394\/eaah4477",
        "display_url" : "stm.sciencemag.org\/content\/9\/394\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875177667397472257",
    "text" : "One more reason to eat your broccoli. Sulforaphane improves glucose control in patients with type 2 diabetes https:\/\/t.co\/F4561ehznA",
    "id" : 875177667397472257,
    "created_at" : "2017-06-15 02:26:30 +0000",
    "user" : {
      "name" : "Satchin Panda",
      "screen_name" : "SatchinPanda",
      "protected" : false,
      "id_str" : "822894547",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/994815475686273024\/IJZMXjG__normal.jpg",
      "id" : 822894547,
      "verified" : false
    }
  },
  "id" : 875253142975377408,
  "created_at" : "2017-06-15 07:26:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Antony Peyton",
      "screen_name" : "TonyBankingTech",
      "indices" : [ 3, 19 ],
      "id_str" : "3401396853",
      "id" : 3401396853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875252858735779840",
  "text" : "RT @TonyBankingTech: Indian migrants sent $62.7 billion in FY 2016-17, which is \u201Cgreater than foreign direct investment (FDI) inflows to In\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "payments",
        "indices" : [ 124, 133 ]
      } ],
      "urls" : [ {
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/kJm3PHJJzL",
        "expanded_url" : "https:\/\/twitter.com\/BankingTechno\/status\/875016828694781954",
        "display_url" : "twitter.com\/BankingTechno\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875017039651512320",
    "text" : "Indian migrants sent $62.7 billion in FY 2016-17, which is \u201Cgreater than foreign direct investment (FDI) inflows to India\u201D. #payments \uD83C\uDDEE\uD83C\uDDF3 https:\/\/t.co\/kJm3PHJJzL",
    "id" : 875017039651512320,
    "created_at" : "2017-06-14 15:48:13 +0000",
    "user" : {
      "name" : "Antony Peyton",
      "screen_name" : "TonyBankingTech",
      "protected" : false,
      "id_str" : "3401396853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/975414682197651458\/n00Tlkwp_normal.jpg",
      "id" : 3401396853,
      "verified" : false
    }
  },
  "id" : 875252858735779840,
  "created_at" : "2017-06-15 07:25:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/875242542354366465\/photo\/1",
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/7Pha0UZDcD",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCV8dB8XcAQxM9Q.jpg",
      "id_str" : "875242540773109764",
      "id" : 875242540773109764,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCV8dB8XcAQxM9Q.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/7Pha0UZDcD"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/zuOINGwl2Z",
      "expanded_url" : "http:\/\/ift.tt\/2rjxiN5",
      "display_url" : "ift.tt\/2rjxiN5"
    } ]
  },
  "geo" : { },
  "id_str" : "875242542354366465",
  "text" : "Morning at the bus stop https:\/\/t.co\/zuOINGwl2Z https:\/\/t.co\/7Pha0UZDcD",
  "id" : 875242542354366465,
  "created_at" : "2017-06-15 06:44:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/leemkuilschu\/status\/875001455815659522\/photo\/1",
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/koRSlUHRLU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCShLHlVoAILQc1.jpg",
      "id_str" : "875001440003006466",
      "id" : 875001440003006466,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCShLHlVoAILQc1.jpg",
      "sizes" : [ {
        "h" : 474,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 430,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 474,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 474,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/koRSlUHRLU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875220373385617413",
  "text" : "RT @leemkuilschu: Google begins beta testing \"feudalism\" https:\/\/t.co\/koRSlUHRLU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/leemkuilschu\/status\/875001455815659522\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/koRSlUHRLU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCShLHlVoAILQc1.jpg",
        "id_str" : "875001440003006466",
        "id" : 875001440003006466,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCShLHlVoAILQc1.jpg",
        "sizes" : [ {
          "h" : 474,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 430,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 474,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 474,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/koRSlUHRLU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875001455815659522",
    "text" : "Google begins beta testing \"feudalism\" https:\/\/t.co\/koRSlUHRLU",
    "id" : 875001455815659522,
    "created_at" : "2017-06-14 14:46:18 +0000",
    "user" : {
      "name" : "Cube Bitch",
      "screen_name" : "lamekuil",
      "protected" : false,
      "id_str" : "14823490",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1015728567831973889\/V8TgGmbx_normal.jpg",
      "id" : 14823490,
      "verified" : false
    }
  },
  "id" : 875220373385617413,
  "created_at" : "2017-06-15 05:16:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex MacCaw",
      "screen_name" : "maccaw",
      "indices" : [ 3, 10 ],
      "id_str" : "2006261",
      "id" : 2006261
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/1dUmQUZaQw",
      "expanded_url" : "https:\/\/twitter.com\/BIUK\/status\/875015029032202240",
      "display_url" : "twitter.com\/BIUK\/status\/87\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875217246351740928",
  "text" : "RT @maccaw: \uD83D\uDE02\n\nhttps:\/\/t.co\/1dUmQUZaQw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 3, 26 ],
        "url" : "https:\/\/t.co\/1dUmQUZaQw",
        "expanded_url" : "https:\/\/twitter.com\/BIUK\/status\/875015029032202240",
        "display_url" : "twitter.com\/BIUK\/status\/87\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "875038920584515584",
    "text" : "\uD83D\uDE02\n\nhttps:\/\/t.co\/1dUmQUZaQw",
    "id" : 875038920584515584,
    "created_at" : "2017-06-14 17:15:10 +0000",
    "user" : {
      "name" : "Alex MacCaw",
      "screen_name" : "maccaw",
      "protected" : false,
      "id_str" : "2006261",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/918603761861328896\/MSXrAULz_normal.jpg",
      "id" : 2006261,
      "verified" : true
    }
  },
  "id" : 875217246351740928,
  "created_at" : "2017-06-15 05:03:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/CbQoRjmhAH",
      "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/875030122192973824",
      "display_url" : "twitter.com\/TODAYonline\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875214834513567749",
  "text" : "From the local media https:\/\/t.co\/CbQoRjmhAH",
  "id" : 875214834513567749,
  "created_at" : "2017-06-15 04:54:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/Me0FXHnOyl",
      "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6280977972023357440",
      "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875213382411894784",
  "text" : "I think there is a serious misrepresentation on this brochure. Checkout my comments. https:\/\/t.co\/Me0FXHnOyl",
  "id" : 875213382411894784,
  "created_at" : "2017-06-15 04:48:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bindas Ladki",
      "screen_name" : "bindasladki",
      "indices" : [ 3, 15 ],
      "id_str" : "766834792394399744",
      "id" : 766834792394399744
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "875106812844007424",
  "text" : "RT @bindasladki: To the man who caught the baby, who the mother on the 10th floor was forced to throw from the building, you are a HERO. \uD83D\uDE22\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GrenfellTower",
        "indices" : [ 124, 138 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "875021518811545600",
    "text" : "To the man who caught the baby, who the mother on the 10th floor was forced to throw from the building, you are a HERO. \uD83D\uDE22\u2764\uFE0F #GrenfellTower",
    "id" : 875021518811545600,
    "created_at" : "2017-06-14 16:06:01 +0000",
    "user" : {
      "name" : "Bindas Ladki",
      "screen_name" : "bindasladki",
      "protected" : false,
      "id_str" : "766834792394399744",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/766902512813309952\/Th4tKxqS_normal.jpg",
      "id" : 766834792394399744,
      "verified" : false
    }
  },
  "id" : 875106812844007424,
  "created_at" : "2017-06-14 21:44:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/ZV0eyf5bHk",
      "expanded_url" : "https:\/\/www.quora.com\/Why-isnt-there-a-fire-alarm-at-every-HDB-block-in-Singapore",
      "display_url" : "quora.com\/Why-isnt-there\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "875036237089955841",
  "text" : "https:\/\/t.co\/ZV0eyf5bHk",
  "id" : 875036237089955841,
  "created_at" : "2017-06-14 17:04:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 0, 13 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "875018686377209856",
  "geo" : { },
  "id_str" : "875034166928302083",
  "in_reply_to_user_id" : 49413866,
  "text" : "@randal_olson Yeah I think that is the one.",
  "id" : 875034166928302083,
  "in_reply_to_status_id" : 875018686377209856,
  "created_at" : "2017-06-14 16:56:17 +0000",
  "in_reply_to_screen_name" : "randal_olson",
  "in_reply_to_user_id_str" : "49413866",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/875018733080768514\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/SfUTZjRKih",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCSw5nBWsAAHnRp.jpg",
      "id_str" : "875018731390414848",
      "id" : 875018731390414848,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCSw5nBWsAAHnRp.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/SfUTZjRKih"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/fdQdmM0wfY",
      "expanded_url" : "http:\/\/ift.tt\/2rrKO5i",
      "display_url" : "ift.tt\/2rrKO5i"
    } ]
  },
  "geo" : { },
  "id_str" : "875018733080768514",
  "text" : "Still works after all this year. Discovered in the store room. https:\/\/t.co\/fdQdmM0wfY https:\/\/t.co\/SfUTZjRKih",
  "id" : 875018733080768514,
  "created_at" : "2017-06-14 15:54:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874989304719843329",
  "text" : "RT @randal_olson: I'm thinking maybe the Titanic dataset because it has many desirable properties for an example dataset, but maybe it's al\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "874985825821478912",
    "geo" : { },
    "id_str" : "874986012405096449",
    "in_reply_to_user_id" : 49413866,
    "text" : "I'm thinking maybe the Titanic dataset because it has many desirable properties for an example dataset, but maybe it's also overdone.",
    "id" : 874986012405096449,
    "in_reply_to_status_id" : 874985825821478912,
    "created_at" : "2017-06-14 13:44:56 +0000",
    "in_reply_to_screen_name" : "randal_olson",
    "in_reply_to_user_id_str" : "49413866",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 874989304719843329,
  "created_at" : "2017-06-14 13:58:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MachineLearning",
      "indices" : [ 55, 71 ]
    }, {
      "text" : "DataScience",
      "indices" : [ 76, 88 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874989287653158913",
  "text" : "RT @randal_olson: What dataset should replace iris for #MachineLearning and #DataScience examples?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "MachineLearning",
        "indices" : [ 37, 53 ]
      }, {
        "text" : "DataScience",
        "indices" : [ 58, 70 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "874985825821478912",
    "text" : "What dataset should replace iris for #MachineLearning and #DataScience examples?",
    "id" : 874985825821478912,
    "created_at" : "2017-06-14 13:44:11 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 874989287653158913,
  "created_at" : "2017-06-14 13:57:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 0, 13 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "874985825821478912",
  "geo" : { },
  "id_str" : "874989218912718848",
  "in_reply_to_user_id" : 49413866,
  "text" : "@randal_olson I remember someone in US was proposing some housing data. Need to check my bookmark.",
  "id" : 874989218912718848,
  "in_reply_to_status_id" : 874985825821478912,
  "created_at" : "2017-06-14 13:57:40 +0000",
  "in_reply_to_screen_name" : "randal_olson",
  "in_reply_to_user_id_str" : "49413866",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "indices" : [ 3, 12 ],
      "id_str" : "18188324",
      "id" : 18188324
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874988882613370880",
  "text" : "RT @rshotton: Zhang experiment suggests ads should be cautious about crowbarring too many messages in\n\nUnseen Mind by @OgilvyChange https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rshotton\/status\/874988439162298369\/photo\/1",
        "indices" : [ 118, 141 ],
        "url" : "https:\/\/t.co\/3y3R2W7zrZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCSVWMhXgAA0AZe.jpg",
        "id_str" : "874988436167557120",
        "id" : 874988436167557120,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCSVWMhXgAA0AZe.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 651,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 651,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 432,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 651,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3y3R2W7zrZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "874988439162298369",
    "text" : "Zhang experiment suggests ads should be cautious about crowbarring too many messages in\n\nUnseen Mind by @OgilvyChange https:\/\/t.co\/3y3R2W7zrZ",
    "id" : 874988439162298369,
    "created_at" : "2017-06-14 13:54:35 +0000",
    "user" : {
      "name" : "richard shotton",
      "screen_name" : "rshotton",
      "protected" : false,
      "id_str" : "18188324",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943964321587105792\/anNpixt2_normal.jpg",
      "id" : 18188324,
      "verified" : false
    }
  },
  "id" : 874988882613370880,
  "created_at" : "2017-06-14 13:56:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HDB",
      "screen_name" : "Singapore_HDB",
      "indices" : [ 0, 14 ],
      "id_str" : "87096507",
      "id" : 87096507
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874888276213194754",
  "in_reply_to_user_id" : 87096507,
  "text" : "@Singapore_HDB It about time to install fire alarm in every household unit given what happening in London?",
  "id" : 874888276213194754,
  "created_at" : "2017-06-14 07:16:34 +0000",
  "in_reply_to_screen_name" : "Singapore_HDB",
  "in_reply_to_user_id_str" : "87096507",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mothership.sg",
      "screen_name" : "MothershipSG",
      "indices" : [ 3, 16 ],
      "id_str" : "1619325942",
      "id" : 1619325942
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/zvgLQXUX8T",
      "expanded_url" : "http:\/\/bit.ly\/2s8uxiU",
      "display_url" : "bit.ly\/2s8uxiU"
    } ]
  },
  "geo" : { },
  "id_str" : "874884023524478978",
  "text" : "RT @MothershipSG: Lee Hsien Yang wants to leave Singapore, publishes open letter with sister to criticise PM Lee https:\/\/t.co\/zvgLQXUX8T ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MothershipSG\/status\/874805415036428288\/photo\/1",
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/yYbBN0BYpX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCPu2ziUIAAW1VF.jpg",
        "id_str" : "874805377954488320",
        "id" : 874805377954488320,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCPu2ziUIAAW1VF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1363,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 799,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2439,
          "resize" : "fit",
          "w" : 3664
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yYbBN0BYpX"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/zvgLQXUX8T",
        "expanded_url" : "http:\/\/bit.ly\/2s8uxiU",
        "display_url" : "bit.ly\/2s8uxiU"
      } ]
    },
    "geo" : { },
    "id_str" : "874805415036428288",
    "text" : "Lee Hsien Yang wants to leave Singapore, publishes open letter with sister to criticise PM Lee https:\/\/t.co\/zvgLQXUX8T https:\/\/t.co\/yYbBN0BYpX",
    "id" : 874805415036428288,
    "created_at" : "2017-06-14 01:47:18 +0000",
    "user" : {
      "name" : "Mothership.sg",
      "screen_name" : "MothershipSG",
      "protected" : false,
      "id_str" : "1619325942",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/880244811273388032\/mzvF03Lz_normal.jpg",
      "id" : 1619325942,
      "verified" : true
    }
  },
  "id" : 874884023524478978,
  "created_at" : "2017-06-14 06:59:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mashableasia",
      "screen_name" : "mashableasia",
      "indices" : [ 3, 16 ],
      "id_str" : "1036198656787996673",
      "id" : 1036198656787996673
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/eRuy4PO3zg",
      "expanded_url" : "http:\/\/on.mash.to\/2skb6X4",
      "display_url" : "on.mash.to\/2skb6X4"
    } ]
  },
  "geo" : { },
  "id_str" : "874524156540354560",
  "text" : "RT @mashableasia: China busts underground ring that recycled urine and blood bags from hospitals for the plastic https:\/\/t.co\/eRuy4PO3zg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/eRuy4PO3zg",
        "expanded_url" : "http:\/\/on.mash.to\/2skb6X4",
        "display_url" : "on.mash.to\/2skb6X4"
      } ]
    },
    "geo" : { },
    "id_str" : "874235804771549185",
    "text" : "China busts underground ring that recycled urine and blood bags from hospitals for the plastic https:\/\/t.co\/eRuy4PO3zg",
    "id" : 874235804771549185,
    "created_at" : "2017-06-12 12:03:53 +0000",
    "user" : {
      "name" : "Mashable Southeast Asia",
      "screen_name" : "MashableSEA",
      "protected" : false,
      "id_str" : "3198041813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025385102321299457\/lp2cJZTV_normal.jpg",
      "id" : 3198041813,
      "verified" : false
    }
  },
  "id" : 874524156540354560,
  "created_at" : "2017-06-13 07:09:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/874403833442643968\/photo\/1",
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/o0SCM06QT6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCKBo0JXgAAA7U7.jpg",
      "id_str" : "874403815855980544",
      "id" : 874403815855980544,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCKBo0JXgAAA7U7.jpg",
      "sizes" : [ {
        "h" : 412,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 412,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 412,
        "resize" : "fit",
        "w" : 636
      }, {
        "h" : 412,
        "resize" : "fit",
        "w" : 636
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/o0SCM06QT6"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874403833442643968",
  "text" : "How do I start Portainer container with Docker flag -v \/var\/run\/docker.sock:\/var\/run\/docker.sock? https:\/\/t.co\/o0SCM06QT6",
  "id" : 874403833442643968,
  "created_at" : "2017-06-12 23:11:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Docker",
      "screen_name" : "Docker",
      "indices" : [ 19, 26 ],
      "id_str" : "1138959692",
      "id" : 1138959692
    }, {
      "name" : "Danielle Portainer",
      "screen_name" : "portainer",
      "indices" : [ 31, 41 ],
      "id_str" : "853190821",
      "id" : 853190821
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/874393156183261184\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/Kxhn3Aav0s",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCJ36VYW0AAgdcH.jpg",
      "id_str" : "874393121718718464",
      "id" : 874393121718718464,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCJ36VYW0AAgdcH.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 324,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 572,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 651,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 651,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Kxhn3Aav0s"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874393156183261184",
  "text" : "Successfully setup @Docker and @Portainer - the feeling is like lifting up the Sam Maguire Cup https:\/\/t.co\/Kxhn3Aav0s",
  "id" : 874393156183261184,
  "created_at" : "2017-06-12 22:29:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Patty Jenkins",
      "screen_name" : "PattyJenks",
      "indices" : [ 3, 14 ],
      "id_str" : "35990109",
      "id" : 35990109
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874380070617481217",
  "text" : "RT @PattyJenks: My producer just sent me this... ABSOLUTELY INCREDIBLE! This makes every hard day worth it. Thank you to whomever wrote it!\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/PattyJenks\/status\/874034832430424065\/photo\/1",
        "indices" : [ 137, 160 ],
        "url" : "https:\/\/t.co\/3DzIaMueIh",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCEx38QU0AAqLwA.jpg",
        "id_str" : "874034639823753216",
        "id" : 874034639823753216,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCEx38QU0AAqLwA.jpg",
        "sizes" : [ {
          "h" : 1282,
          "resize" : "fit",
          "w" : 962
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1282,
          "resize" : "fit",
          "w" : 962
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3DzIaMueIh"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "874034832430424065",
    "text" : "My producer just sent me this... ABSOLUTELY INCREDIBLE! This makes every hard day worth it. Thank you to whomever wrote it!!\u2764\uFE0F\u2764\uFE0F\u2764\uFE0F\u2764\uFE0F\u2764\uFE0F\u2764\uFE0F https:\/\/t.co\/3DzIaMueIh",
    "id" : 874034832430424065,
    "created_at" : "2017-06-11 22:45:17 +0000",
    "user" : {
      "name" : "Patty Jenkins",
      "screen_name" : "PattyJenks",
      "protected" : false,
      "id_str" : "35990109",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/979072448233689088\/In5TsPkk_normal.jpg",
      "id" : 35990109,
      "verified" : true
    }
  },
  "id" : 874380070617481217,
  "created_at" : "2017-06-12 21:37:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/gpnELUSsF4",
      "expanded_url" : "http:\/\/99percentinvisible.org\/episode\/ten-thousand-years\/",
      "display_url" : "99percentinvisible.org\/episode\/ten-th\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874360526981259268",
  "text" : "Things like this fascinates me. https:\/\/t.co\/gpnELUSsF4 Neither the content or comments refer to the use of Egyptian hieroglyphic",
  "id" : 874360526981259268,
  "created_at" : "2017-06-12 20:19:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kwan Jin Yao",
      "screen_name" : "guanyinmiao",
      "indices" : [ 3, 15 ],
      "id_str" : "37145570",
      "id" : 37145570
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Qatar",
      "indices" : [ 17, 23 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874277651489402883",
  "text" : "RT @guanyinmiao: #Qatar \"provides the answer to Singaporeans who have asked why our tiny city-state cannot rely on the \"world's policemen\"\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Qatar",
        "indices" : [ 0, 6 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "873571446265569280",
    "geo" : { },
    "id_str" : "873571558236626945",
    "in_reply_to_user_id" : 37145570,
    "text" : "#Qatar \"provides the answer to Singaporeans who have asked why our tiny city-state cannot rely on the \"world's policemen\" for its security.\"",
    "id" : 873571558236626945,
    "in_reply_to_status_id" : 873571446265569280,
    "created_at" : "2017-06-10 16:04:24 +0000",
    "in_reply_to_screen_name" : "guanyinmiao",
    "in_reply_to_user_id_str" : "37145570",
    "user" : {
      "name" : "Kwan Jin Yao",
      "screen_name" : "guanyinmiao",
      "protected" : false,
      "id_str" : "37145570",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/518027838100746241\/hqleyzau_normal.jpeg",
      "id" : 37145570,
      "verified" : false
    }
  },
  "id" : 874277651489402883,
  "created_at" : "2017-06-12 14:50:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/tP80SzrMio",
      "expanded_url" : "https:\/\/sg.news.yahoo.com\/first-woman-detained-isa-singapore-radicalism-mha-082630538.html",
      "display_url" : "sg.news.yahoo.com\/first-woman-de\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874275289970376713",
  "text" : "First woman detained under ISA in Singapore for radicalism https:\/\/t.co\/tP80SzrMio",
  "id" : 874275289970376713,
  "created_at" : "2017-06-12 14:40:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874259512034811905",
  "text" : "Queen's Speech is set to be delayed. Maybe due to that NI party's Never Work on Sunday policy?",
  "id" : 874259512034811905,
  "created_at" : "2017-06-12 13:38:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/lso0WcDgJa",
      "expanded_url" : "http:\/\/adage.com\/article\/agency-news\/agency-bangs-creative-process-weeks\/309301\/",
      "display_url" : "adage.com\/article\/agency\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874246269375479809",
  "text" : "Industry sources back home indicates that this is a typical turnaround time in Singapore agency. https:\/\/t.co\/lso0WcDgJa",
  "id" : 874246269375479809,
  "created_at" : "2017-06-12 12:45:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 20, 43 ],
      "url" : "https:\/\/t.co\/Gid4umKuYz",
      "expanded_url" : "https:\/\/www.forbes.com\/sites\/tedrall\/2015\/10\/30\/how-to-avoid-getting-locked-out-of-your-email-when-you-travel\/#2df1cf9c71d7",
      "display_url" : "forbes.com\/sites\/tedrall\/\u2026"
    }, {
      "indices" : [ 44, 67 ],
      "url" : "https:\/\/t.co\/rW0aiDgCdZ",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/874172100432592896",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874236478494302210",
  "text" : "I find the solution https:\/\/t.co\/Gid4umKuYz https:\/\/t.co\/rW0aiDgCdZ",
  "id" : 874236478494302210,
  "created_at" : "2017-06-12 12:06:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Ram",
      "screen_name" : "iamdanram",
      "indices" : [ 3, 13 ],
      "id_str" : "348416067",
      "id" : 348416067
    }, {
      "name" : "thebrandbuilder",
      "screen_name" : "JobsEnterInnov",
      "indices" : [ 56, 71 ],
      "id_str" : "878152342125686784",
      "id" : 878152342125686784
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Entrepreneur",
      "indices" : [ 101, 114 ]
    }, {
      "text" : "startup",
      "indices" : [ 130, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874176164528566272",
  "text" : "RT @iamdanram: Excited to participate in this morning's @JobsEnterInnov mid term review and share an #Entrepreneur perspective. \n\n#startup\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "thebrandbuilder",
        "screen_name" : "JobsEnterInnov",
        "indices" : [ 41, 56 ],
        "id_str" : "878152342125686784",
        "id" : 878152342125686784
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/iamdanram\/status\/874175538021224448\/video\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/HOQxXY6HS5",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/874175456060334080\/pu\/img\/Hl8xpYPumAP1pKfR.jpg",
        "id_str" : "874175456060334080",
        "id" : 874175456060334080,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/874175456060334080\/pu\/img\/Hl8xpYPumAP1pKfR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HOQxXY6HS5"
      } ],
      "hashtags" : [ {
        "text" : "Entrepreneur",
        "indices" : [ 86, 99 ]
      }, {
        "text" : "startup",
        "indices" : [ 115, 123 ]
      }, {
        "text" : "Government",
        "indices" : [ 124, 135 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "874175538021224448",
    "text" : "Excited to participate in this morning's @JobsEnterInnov mid term review and share an #Entrepreneur perspective. \n\n#startup #Government https:\/\/t.co\/HOQxXY6HS5",
    "id" : 874175538021224448,
    "created_at" : "2017-06-12 08:04:24 +0000",
    "user" : {
      "name" : "Dan Ram",
      "screen_name" : "iamdanram",
      "protected" : false,
      "id_str" : "348416067",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1006132501646073857\/XyEK3xFx_normal.jpg",
      "id" : 348416067,
      "verified" : false
    }
  },
  "id" : 874176164528566272,
  "created_at" : "2017-06-12 08:06:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WorldMeatFreeDay",
      "indices" : [ 27, 44 ]
    }, {
      "text" : "vegetarianpride",
      "indices" : [ 45, 61 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874175078451339265",
  "text" : "It just another day for me #WorldMeatFreeDay #vegetarianpride",
  "id" : 874175078451339265,
  "created_at" : "2017-06-12 08:02:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "WorldMeatFreeDay",
      "indices" : [ 99, 116 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874174835483607040",
  "text" : "Take the pledge and become meat free for one day on 12th June 2017. It's time to make a difference #WorldMeatFreeDay",
  "id" : 874174835483607040,
  "created_at" : "2017-06-12 08:01:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874172100432592896",
  "text" : "Unable to login to Yahoo Mail\/Gmail\/Outlook when abroad. How do you find a way around?",
  "id" : 874172100432592896,
  "created_at" : "2017-06-12 07:50:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "London & Partners",
      "screen_name" : "L_Pbusiness",
      "indices" : [ 3, 15 ],
      "id_str" : "23307018",
      "id" : 23307018
    }, {
      "name" : "London Tech Week",
      "screen_name" : "LDNTechWeek",
      "indices" : [ 69, 81 ],
      "id_str" : "1528708502",
      "id" : 1528708502
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LondonIsOpen",
      "indices" : [ 17, 30 ]
    }, {
      "text" : "tech",
      "indices" : [ 63, 68 ]
    }, {
      "text" : "LTW",
      "indices" : [ 114, 118 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874169638543872000",
  "text" : "RT @L_Pbusiness: #LondonIsOpen as Europe's largest festival of #tech @LDNTechWeek comes to the capital from today #LTW https:\/\/t.co\/ISEIjKM\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "London Tech Week",
        "screen_name" : "LDNTechWeek",
        "indices" : [ 52, 64 ],
        "id_str" : "1528708502",
        "id" : 1528708502
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/L_Pbusiness\/status\/874137314024443904\/video\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/ISEIjKMsE8",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/874130639112597504\/pu\/img\/Twyrpi0js_JZ0GKy.jpg",
        "id_str" : "874130639112597504",
        "id" : 874130639112597504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/874130639112597504\/pu\/img\/Twyrpi0js_JZ0GKy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ISEIjKMsE8"
      } ],
      "hashtags" : [ {
        "text" : "LondonIsOpen",
        "indices" : [ 0, 13 ]
      }, {
        "text" : "tech",
        "indices" : [ 46, 51 ]
      }, {
        "text" : "LTW",
        "indices" : [ 97, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "874137314024443904",
    "text" : "#LondonIsOpen as Europe's largest festival of #tech @LDNTechWeek comes to the capital from today #LTW https:\/\/t.co\/ISEIjKMsE8",
    "id" : 874137314024443904,
    "created_at" : "2017-06-12 05:32:31 +0000",
    "user" : {
      "name" : "London & Partners",
      "screen_name" : "L_Pbusiness",
      "protected" : false,
      "id_str" : "23307018",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/937680259683639301\/ZhGSLcP3_normal.jpg",
      "id" : 23307018,
      "verified" : false
    }
  },
  "id" : 874169638543872000,
  "created_at" : "2017-06-12 07:40:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tom Fishburne",
      "screen_name" : "tomfishburne",
      "indices" : [ 3, 16 ],
      "id_str" : "43407384",
      "id" : 43407384
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/mXdSVJ820K",
      "expanded_url" : "https:\/\/marketoonist.com\/2017\/06\/annoying-ads.html",
      "display_url" : "marketoonist.com\/2017\/06\/annoyi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874158255043465216",
  "text" : "RT @tomfishburne: \u201C8 Types of Annoying Ads\u201D - new cartoon and post on the rise of Ad Blockers https:\/\/t.co\/mXdSVJ820K https:\/\/t.co\/UlAKF29f\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tomfishburne\/status\/873927284599554052\/photo\/1",
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/UlAKF29fYV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCDQK40VYAExIe6.jpg",
        "id_str" : "873927213179035649",
        "id" : 873927213179035649,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCDQK40VYAExIe6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 347,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 612,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UlAKF29fYV"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/mXdSVJ820K",
        "expanded_url" : "https:\/\/marketoonist.com\/2017\/06\/annoying-ads.html",
        "display_url" : "marketoonist.com\/2017\/06\/annoyi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873927284599554052",
    "text" : "\u201C8 Types of Annoying Ads\u201D - new cartoon and post on the rise of Ad Blockers https:\/\/t.co\/mXdSVJ820K https:\/\/t.co\/UlAKF29fYV",
    "id" : 873927284599554052,
    "created_at" : "2017-06-11 15:37:56 +0000",
    "user" : {
      "name" : "Tom Fishburne",
      "screen_name" : "tomfishburne",
      "protected" : false,
      "id_str" : "43407384",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1577292457\/twitter_headshot_normal.jpg",
      "id" : 43407384,
      "verified" : false
    }
  },
  "id" : 874158255043465216,
  "created_at" : "2017-06-12 06:55:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "874155011865022464",
  "text" : "Post-Election everyone is analyzing together with Captain Hindsight",
  "id" : 874155011865022464,
  "created_at" : "2017-06-12 06:42:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/874154719953997825\/photo\/1",
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/I4kiHd7v1y",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCGfFZ9XkAAy_yI.jpg",
      "id_str" : "874154717903032320",
      "id" : 874154717903032320,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCGfFZ9XkAAy_yI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/I4kiHd7v1y"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 19, 42 ],
      "url" : "https:\/\/t.co\/sV9LBG0e6Q",
      "expanded_url" : "http:\/\/ift.tt\/2tba8sH",
      "display_url" : "ift.tt\/2tba8sH"
    } ]
  },
  "geo" : { },
  "id_str" : "874154719953997825",
  "text" : "7 more days to go. https:\/\/t.co\/sV9LBG0e6Q https:\/\/t.co\/I4kiHd7v1y",
  "id" : 874154719953997825,
  "created_at" : "2017-06-12 06:41:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 105, 128 ],
      "url" : "https:\/\/t.co\/aYPq6ANHYZ",
      "expanded_url" : "https:\/\/shar.es\/1RSvsF",
      "display_url" : "shar.es\/1RSvsF"
    } ]
  },
  "geo" : { },
  "id_str" : "874153807806169088",
  "text" : "In the end, you\u2019re going to compete with the big guys. If you\u2019re unlucky, it will be big tech companies  https:\/\/t.co\/aYPq6ANHYZ",
  "id" : 874153807806169088,
  "created_at" : "2017-06-12 06:38:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Springwise Intelligence",
      "screen_name" : "springwise",
      "indices" : [ 3, 14 ],
      "id_str" : "14863500",
      "id" : 14863500
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/springwise\/status\/874137455930339328\/photo\/1",
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/1TuXORSCwH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DCGPYdCVoAA357X.jpg",
      "id_str" : "874137452960653312",
      "id" : 874137452960653312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCGPYdCVoAA357X.jpg",
      "sizes" : [ {
        "h" : 703,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 398,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1TuXORSCwH"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/x12G9ul3qS",
      "expanded_url" : "https:\/\/www.springwise.com\/cycling-bell-beams-inside-cars\/",
      "display_url" : "springwise.com\/cycling-bell-b\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874151353974747136",
  "text" : "RT @springwise: Cycling bell beams inside cars\nhttps:\/\/t.co\/x12G9ul3qS https:\/\/t.co\/1TuXORSCwH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/springwise\/status\/874137455930339328\/photo\/1",
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/1TuXORSCwH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCGPYdCVoAA357X.jpg",
        "id_str" : "874137452960653312",
        "id" : 874137452960653312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCGPYdCVoAA357X.jpg",
        "sizes" : [ {
          "h" : 703,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 398,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/1TuXORSCwH"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 31, 54 ],
        "url" : "https:\/\/t.co\/x12G9ul3qS",
        "expanded_url" : "https:\/\/www.springwise.com\/cycling-bell-beams-inside-cars\/",
        "display_url" : "springwise.com\/cycling-bell-b\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "874137455930339328",
    "text" : "Cycling bell beams inside cars\nhttps:\/\/t.co\/x12G9ul3qS https:\/\/t.co\/1TuXORSCwH",
    "id" : 874137455930339328,
    "created_at" : "2017-06-12 05:33:04 +0000",
    "user" : {
      "name" : "Springwise Intelligence",
      "screen_name" : "springwise",
      "protected" : false,
      "id_str" : "14863500",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/798820732222382080\/ngBH8DW0_normal.jpg",
      "id" : 14863500,
      "verified" : false
    }
  },
  "id" : 874151353974747136,
  "created_at" : "2017-06-12 06:28:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hon Cheng",
      "screen_name" : "honcheng",
      "indices" : [ 3, 12 ],
      "id_str" : "15179859",
      "id" : 15179859
    }, {
      "name" : "Junjie",
      "screen_name" : "jjlin",
      "indices" : [ 86, 92 ],
      "id_str" : "15264384",
      "id" : 15264384
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "wwdc17",
      "indices" : [ 47, 54 ]
    } ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/zocUN8WqUC",
      "expanded_url" : "https:\/\/twitter.com\/elkcurrencyapp\/status\/872518158631960583",
      "display_url" : "twitter.com\/elkcurrencyapp\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874147639805325312",
  "text" : "RT @honcheng: An Apple Design Award for Elk at #wwdc17! A dream comes true for me and @jjlin https:\/\/t.co\/zocUN8WqUC",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Junjie",
        "screen_name" : "jjlin",
        "indices" : [ 72, 78 ],
        "id_str" : "15264384",
        "id" : 15264384
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "wwdc17",
        "indices" : [ 33, 40 ]
      } ],
      "urls" : [ {
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/zocUN8WqUC",
        "expanded_url" : "https:\/\/twitter.com\/elkcurrencyapp\/status\/872518158631960583",
        "display_url" : "twitter.com\/elkcurrencyapp\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872519936551157761",
    "text" : "An Apple Design Award for Elk at #wwdc17! A dream comes true for me and @jjlin https:\/\/t.co\/zocUN8WqUC",
    "id" : 872519936551157761,
    "created_at" : "2017-06-07 18:25:38 +0000",
    "user" : {
      "name" : "Hon Cheng",
      "screen_name" : "honcheng",
      "protected" : false,
      "id_str" : "15179859",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1441682628\/photo-1_normal.JPG",
      "id" : 15179859,
      "verified" : false
    }
  },
  "id" : 874147639805325312,
  "created_at" : "2017-06-12 06:13:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Barry Rogers",
      "screen_name" : "TheBarryRogers",
      "indices" : [ 3, 18 ],
      "id_str" : "2168576827",
      "id" : 2168576827
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/nQunS7g8us",
      "expanded_url" : "http:\/\/99percentinvisible.org\/episode\/ten-thousand-years\/",
      "display_url" : "99percentinvisible.org\/episode\/ten-th\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "874021644754448384",
  "text" : "RT @TheBarryRogers: How do we warn people 10,000 years from now about buried nuclear waste?  https:\/\/t.co\/nQunS7g8us",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 73, 96 ],
        "url" : "https:\/\/t.co\/nQunS7g8us",
        "expanded_url" : "http:\/\/99percentinvisible.org\/episode\/ten-thousand-years\/",
        "display_url" : "99percentinvisible.org\/episode\/ten-th\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "874005484893863936",
    "text" : "How do we warn people 10,000 years from now about buried nuclear waste?  https:\/\/t.co\/nQunS7g8us",
    "id" : 874005484893863936,
    "created_at" : "2017-06-11 20:48:40 +0000",
    "user" : {
      "name" : "Barry Rogers",
      "screen_name" : "TheBarryRogers",
      "protected" : false,
      "id_str" : "2168576827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/951186092572794880\/ovpRttTT_normal.jpg",
      "id" : 2168576827,
      "verified" : false
    }
  },
  "id" : 874021644754448384,
  "created_at" : "2017-06-11 21:52:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 36, 59 ],
      "url" : "https:\/\/t.co\/puMnkxVS2b",
      "expanded_url" : "https:\/\/www.yubico.com\/products\/yubikey-hardware\/",
      "display_url" : "yubico.com\/products\/yubik\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873971218487050241",
  "text" : "Everyone of you please get this now https:\/\/t.co\/puMnkxVS2b",
  "id" : 873971218487050241,
  "created_at" : "2017-06-11 18:32:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/ttOdUPsag4",
      "expanded_url" : "http:\/\/algorithms-tour.stitchfix.com\/",
      "display_url" : "algorithms-tour.stitchfix.com"
    } ]
  },
  "geo" : { },
  "id_str" : "873953088113192962",
  "text" : "A great example of simplifying the detail of data science into a format where the impacts are immediately apparent. https:\/\/t.co\/ttOdUPsag4",
  "id" : 873953088113192962,
  "created_at" : "2017-06-11 17:20:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AIRLIVE",
      "screen_name" : "airlivenet",
      "indices" : [ 3, 14 ],
      "id_str" : "2280470022",
      "id" : 2280470022
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MU736",
      "indices" : [ 39, 45 ]
    } ],
    "urls" : [ {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/vhC1QAodwE",
      "expanded_url" : "http:\/\/ift.tt\/2rjIWXe",
      "display_url" : "ift.tt\/2rjIWXe"
    } ]
  },
  "geo" : { },
  "id_str" : "873926704900833280",
  "text" : "RT @airlivenet: BREAKING China Eastern #MU736 suffered serious engine failure after takeoff from Sydney https:\/\/t.co\/vhC1QAodwE https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/airlivenet\/status\/873907543109709824\/photo\/1",
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/LKTuj4VHau",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DCC-RF3XgAAbB8N.jpg",
        "id_str" : "873907528551333888",
        "id" : 873907528551333888,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DCC-RF3XgAAbB8N.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 596
        }, {
          "h" : 697,
          "resize" : "fit",
          "w" : 611
        }, {
          "h" : 697,
          "resize" : "fit",
          "w" : 611
        }, {
          "h" : 697,
          "resize" : "fit",
          "w" : 611
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LKTuj4VHau"
      } ],
      "hashtags" : [ {
        "text" : "MU736",
        "indices" : [ 23, 29 ]
      } ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/vhC1QAodwE",
        "expanded_url" : "http:\/\/ift.tt\/2rjIWXe",
        "display_url" : "ift.tt\/2rjIWXe"
      } ]
    },
    "geo" : { },
    "id_str" : "873907543109709824",
    "text" : "BREAKING China Eastern #MU736 suffered serious engine failure after takeoff from Sydney https:\/\/t.co\/vhC1QAodwE https:\/\/t.co\/LKTuj4VHau",
    "id" : 873907543109709824,
    "created_at" : "2017-06-11 14:19:29 +0000",
    "user" : {
      "name" : "AIRLIVE",
      "screen_name" : "airlivenet",
      "protected" : false,
      "id_str" : "2280470022",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/828597831829168128\/DfHMnzf1_normal.jpg",
      "id" : 2280470022,
      "verified" : true
    }
  },
  "id" : 873926704900833280,
  "created_at" : "2017-06-11 15:35:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E50",
      "screen_name" : "shimmertje8",
      "indices" : [ 0, 12 ],
      "id_str" : "237988530",
      "id" : 237988530
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "873851356838207488",
  "geo" : { },
  "id_str" : "873926016535842816",
  "in_reply_to_user_id" : 237988530,
  "text" : "@shimmertje8 Thank you",
  "id" : 873926016535842816,
  "in_reply_to_status_id" : 873851356838207488,
  "created_at" : "2017-06-11 15:32:53 +0000",
  "in_reply_to_screen_name" : "shimmertje8",
  "in_reply_to_user_id_str" : "237988530",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ketan Joshi",
      "screen_name" : "KetanJ0",
      "indices" : [ 3, 11 ],
      "id_str" : "420100653",
      "id" : 420100653
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/KetanJ0\/status\/873450943832084480\/photo\/1",
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/DOqqNkTtdH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB8fANvVwAAmsox.jpg",
      "id_str" : "873450941281845248",
      "id" : 873450941281845248,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB8fANvVwAAmsox.jpg",
      "sizes" : [ {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DOqqNkTtdH"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873920218350968832",
  "text" : "RT @KetanJ0: A simple, 105 year old explanation of climate change https:\/\/t.co\/DOqqNkTtdH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/KetanJ0\/status\/873450943832084480\/photo\/1",
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/DOqqNkTtdH",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB8fANvVwAAmsox.jpg",
        "id_str" : "873450941281845248",
        "id" : 873450941281845248,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB8fANvVwAAmsox.jpg",
        "sizes" : [ {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DOqqNkTtdH"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873450943832084480",
    "text" : "A simple, 105 year old explanation of climate change https:\/\/t.co\/DOqqNkTtdH",
    "id" : 873450943832084480,
    "created_at" : "2017-06-10 08:05:07 +0000",
    "user" : {
      "name" : "Ketan Joshi",
      "screen_name" : "KetanJ0",
      "protected" : false,
      "id_str" : "420100653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1027724920136953856\/zE1ngs_1_normal.jpg",
      "id" : 420100653,
      "verified" : true
    }
  },
  "id" : 873920218350968832,
  "created_at" : "2017-06-11 15:09:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 0, 14 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "873830601953955845",
  "geo" : { },
  "id_str" : "873831190045839363",
  "in_reply_to_user_id" : 815066809,
  "text" : "@ofmeowandbake Yes. I applied directly one month b4 this agency contacted and interviewed me on the same position.",
  "id" : 873831190045839363,
  "in_reply_to_status_id" : 873830601953955845,
  "created_at" : "2017-06-11 09:16:05 +0000",
  "in_reply_to_screen_name" : "ofmeowandbake",
  "in_reply_to_user_id_str" : "815066809",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ian Fenn",
      "screen_name" : "ifenn",
      "indices" : [ 0, 6 ],
      "id_str" : "60163",
      "id" : 60163
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "873825755700101120",
  "geo" : { },
  "id_str" : "873826595689168898",
  "in_reply_to_user_id" : 60163,
  "text" : "@ifenn Thanks Ian. The company finally contacted me (right after this agency contacted &amp; interviewed me)",
  "id" : 873826595689168898,
  "in_reply_to_status_id" : 873825755700101120,
  "created_at" : "2017-06-11 08:57:49 +0000",
  "in_reply_to_screen_name" : "ifenn",
  "in_reply_to_user_id_str" : "60163",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/9ZphEkSYew",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/873824334271401984",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873824601800933376",
  "text" : "Should I inform the recruitment agency? What the protocol? https:\/\/t.co\/9ZphEkSYew",
  "id" : 873824601800933376,
  "created_at" : "2017-06-11 08:49:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 141, 164 ],
      "url" : "https:\/\/t.co\/chvpQ9vKMp",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/873823106430902274",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873824334271401984",
  "text" : "A recruitment agency represent the company reach out to me in June &amp; aware of my application. An interview was conducted by this agency. https:\/\/t.co\/chvpQ9vKMp",
  "id" : 873824334271401984,
  "created_at" : "2017-06-11 08:48:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873823106430902274",
  "text" : "A company recruiter get back to me on an application I applied in May.",
  "id" : 873823106430902274,
  "created_at" : "2017-06-11 08:43:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873816229047394304",
  "text" : "Does pasta bow tie with pesto goes well with red pepper? Thanks",
  "id" : 873816229047394304,
  "created_at" : "2017-06-11 08:16:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RepealEight",
      "screen_name" : "repealeight",
      "indices" : [ 3, 15 ],
      "id_str" : "2729381970",
      "id" : 2729381970
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873668056966471680",
  "text" : "RT @repealeight: Yet again women's lives, health &amp; choices are disregarded &amp; treated as nothing more than a political bargaining chip #repe\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "repealthe8th",
        "indices" : [ 125, 138 ]
      }, {
        "text" : "extend67",
        "indices" : [ 139, 148 ]
      } ],
      "urls" : [ {
        "indices" : [ 149, 172 ],
        "url" : "https:\/\/t.co\/OMZXo87FzI",
        "expanded_url" : "https:\/\/twitter.com\/carolinejmolloy\/status\/873438866111332352",
        "display_url" : "twitter.com\/carolinejmollo\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873658193850884096",
    "text" : "Yet again women's lives, health &amp; choices are disregarded &amp; treated as nothing more than a political bargaining chip #repealthe8th #extend67 https:\/\/t.co\/OMZXo87FzI",
    "id" : 873658193850884096,
    "created_at" : "2017-06-10 21:48:39 +0000",
    "user" : {
      "name" : "RepealEight",
      "screen_name" : "repealeight",
      "protected" : false,
      "id_str" : "2729381970",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/923552015770365952\/26HQSKiC_normal.jpg",
      "id" : 2729381970,
      "verified" : false
    }
  },
  "id" : 873668056966471680,
  "created_at" : "2017-06-10 22:27:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Se\u00E1n Kelly MEP",
      "screen_name" : "SeanKellyMEP",
      "indices" : [ 3, 16 ],
      "id_str" : "21440665",
      "id" : 21440665
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "brexit",
      "indices" : [ 38, 45 ]
    }, {
      "text" : "GE2017",
      "indices" : [ 78, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873650769072926721",
  "text" : "RT @SeanKellyMEP: Brits are gas.After #brexit they tweeted 'What's EU'? After #GE2017 they tweeted 'What's DUP'? What's next?After breakfas\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "brexit",
        "indices" : [ 20, 27 ]
      }, {
        "text" : "GE2017",
        "indices" : [ 60, 67 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873609975666532353",
    "text" : "Brits are gas.After #brexit they tweeted 'What's EU'? After #GE2017 they tweeted 'What's DUP'? What's next?After breakfast? What's weetabix?",
    "id" : 873609975666532353,
    "created_at" : "2017-06-10 18:37:03 +0000",
    "user" : {
      "name" : "Se\u00E1n Kelly MEP",
      "screen_name" : "SeanKellyMEP",
      "protected" : false,
      "id_str" : "21440665",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/586095857549869056\/IqbmJYoK_normal.png",
      "id" : 21440665,
      "verified" : true
    }
  },
  "id" : 873650769072926721,
  "created_at" : "2017-06-10 21:19:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 54, 77 ],
      "url" : "https:\/\/t.co\/mmrX9Gc60K",
      "expanded_url" : "https:\/\/stackoverflow.com\/a\/11103217\/6087933",
      "display_url" : "stackoverflow.com\/a\/11103217\/608\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873586148932812801",
  "text" : "How to find out which package version is loaded in R?\nhttps:\/\/t.co\/mmrX9Gc60K",
  "id" : 873586148932812801,
  "created_at" : "2017-06-10 17:02:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/Vl0qFjVJX2",
      "expanded_url" : "https:\/\/twitter.com\/xavierlur\/status\/873540985258622976",
      "display_url" : "twitter.com\/xavierlur\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873579581122510849",
  "text" : "All 4 are official languages in Singapore. https:\/\/t.co\/Vl0qFjVJX2",
  "id" : 873579581122510849,
  "created_at" : "2017-06-10 16:36:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873577580930228225",
  "text" : "I have no idea who Adam West was until his appearance in Big Bang Theory.",
  "id" : 873577580930228225,
  "created_at" : "2017-06-10 16:28:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "indices" : [ 3, 17 ],
      "id_str" : "14157788",
      "id" : 14157788
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/gK45hC9pHb",
      "expanded_url" : "https:\/\/twitter.com\/newscientist\/status\/873558401896386560",
      "display_url" : "twitter.com\/newscientist\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873574225562546176",
  "text" : "RT @iheartbeijing: I'm sensing a distopian future young adult novel plot in this news... https:\/\/t.co\/gK45hC9pHb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/gK45hC9pHb",
        "expanded_url" : "https:\/\/twitter.com\/newscientist\/status\/873558401896386560",
        "display_url" : "twitter.com\/newscientist\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873560098957918213",
    "text" : "I'm sensing a distopian future young adult novel plot in this news... https:\/\/t.co\/gK45hC9pHb",
    "id" : 873560098957918213,
    "created_at" : "2017-06-10 15:18:52 +0000",
    "user" : {
      "name" : "Elyse Ribbons \u67F3\u7D20\u82F1",
      "screen_name" : "iheartbeijing",
      "protected" : false,
      "id_str" : "14157788",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/469468312292712448\/kKTKW0ON_normal.jpeg",
      "id" : 14157788,
      "verified" : false
    }
  },
  "id" : 873574225562546176,
  "created_at" : "2017-06-10 16:15:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873563178231369730",
  "text" : "It always some \"obscure\" R package you need that not preloaded.",
  "id" : 873563178231369730,
  "created_at" : "2017-06-10 15:31:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/SjSXHUjmx0",
      "expanded_url" : "https:\/\/rnotebook.io\/",
      "display_url" : "rnotebook.io"
    } ]
  },
  "geo" : { },
  "id_str" : "873559607368667140",
  "text" : "Run your Jupyter R notebooks in the cloud https:\/\/t.co\/SjSXHUjmx0",
  "id" : 873559607368667140,
  "created_at" : "2017-06-10 15:16:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Malcolm Auld",
      "screen_name" : "MalcolmAuld",
      "indices" : [ 3, 15 ],
      "id_str" : "21810498",
      "id" : 21810498
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MalcolmAuld\/status\/872641424721838081\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/HfwZ6F8m3a",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBw-v9hVwAEYfDv.jpg",
      "id_str" : "872641421492207617",
      "id" : 872641421492207617,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBw-v9hVwAEYfDv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 408,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 408,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 408,
        "resize" : "fit",
        "w" : 550
      }, {
        "h" : 408,
        "resize" : "fit",
        "w" : 550
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HfwZ6F8m3a"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/GbBRu9WuIl",
      "expanded_url" : "http:\/\/themalcolmauldblog.com\/2017\/06\/08\/what-you-sell-and-what-people-buy-are-often-two-different-things\/",
      "display_url" : "themalcolmauldblog.com\/2017\/06\/08\/wha\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873542029246156800",
  "text" : "RT @MalcolmAuld: What you sell and what people buy are often two different\u00A0things\u2026 https:\/\/t.co\/GbBRu9WuIl https:\/\/t.co\/HfwZ6F8m3a",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MalcolmAuld\/status\/872641424721838081\/photo\/1",
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/HfwZ6F8m3a",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBw-v9hVwAEYfDv.jpg",
        "id_str" : "872641421492207617",
        "id" : 872641421492207617,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBw-v9hVwAEYfDv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 408,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 408,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 408,
          "resize" : "fit",
          "w" : 550
        }, {
          "h" : 408,
          "resize" : "fit",
          "w" : 550
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/HfwZ6F8m3a"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/GbBRu9WuIl",
        "expanded_url" : "http:\/\/themalcolmauldblog.com\/2017\/06\/08\/what-you-sell-and-what-people-buy-are-often-two-different-things\/",
        "display_url" : "themalcolmauldblog.com\/2017\/06\/08\/wha\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872641424721838081",
    "text" : "What you sell and what people buy are often two different\u00A0things\u2026 https:\/\/t.co\/GbBRu9WuIl https:\/\/t.co\/HfwZ6F8m3a",
    "id" : 872641424721838081,
    "created_at" : "2017-06-08 02:28:23 +0000",
    "user" : {
      "name" : "Malcolm Auld",
      "screen_name" : "MalcolmAuld",
      "protected" : false,
      "id_str" : "21810498",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/749789443641315328\/yTRUaGsc_normal.jpg",
      "id" : 21810498,
      "verified" : false
    }
  },
  "id" : 873542029246156800,
  "created_at" : "2017-06-10 14:07:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tim Harford",
      "screen_name" : "TimHarford",
      "indices" : [ 3, 14 ],
      "id_str" : "32493647",
      "id" : 32493647
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/9OyUFv4Y83",
      "expanded_url" : "http:\/\/timharford.com\/2017\/06\/where-the-truth-really-lies-with-statistics\/",
      "display_url" : "timharford.com\/2017\/06\/where-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873526311033208832",
  "text" : "RT @TimHarford: Where the truth really lies with statistics: https:\/\/t.co\/9OyUFv4Y83",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 45, 68 ],
        "url" : "https:\/\/t.co\/9OyUFv4Y83",
        "expanded_url" : "http:\/\/timharford.com\/2017\/06\/where-the-truth-really-lies-with-statistics\/",
        "display_url" : "timharford.com\/2017\/06\/where-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873518863211560960",
    "text" : "Where the truth really lies with statistics: https:\/\/t.co\/9OyUFv4Y83",
    "id" : 873518863211560960,
    "created_at" : "2017-06-10 12:35:00 +0000",
    "user" : {
      "name" : "Tim Harford",
      "screen_name" : "TimHarford",
      "protected" : false,
      "id_str" : "32493647",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1338890256\/Harford_Cropped_normal.JPG",
      "id" : 32493647,
      "verified" : true
    }
  },
  "id" : 873526311033208832,
  "created_at" : "2017-06-10 13:04:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/uACVQwmoUC",
      "expanded_url" : "https:\/\/blogs.technet.microsoft.com\/machinelearning\/2016\/05\/31\/rfm-a-simple-and-powerful-approach-to-event-modeling\/",
      "display_url" : "blogs.technet.microsoft.com\/machinelearnin\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873525343713468416",
  "text" : "Application of RFM used in direct marketing in churn prediction problems. https:\/\/t.co\/uACVQwmoUC",
  "id" : 873525343713468416,
  "created_at" : "2017-06-10 13:00:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/873514552775901184\/photo\/1",
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/PEjqQoxTCp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB9Y2wOXcAAIPGY.jpg",
      "id_str" : "873514550414503936",
      "id" : 873514550414503936,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB9Y2wOXcAAIPGY.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PEjqQoxTCp"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 60, 83 ],
      "url" : "https:\/\/t.co\/qk7WXRJOyX",
      "expanded_url" : "http:\/\/ift.tt\/2rWT8Ht",
      "display_url" : "ift.tt\/2rWT8Ht"
    } ]
  },
  "geo" : { },
  "id_str" : "873514552775901184",
  "text" : "Everything from the Left is always revolt, fist in the air. https:\/\/t.co\/qk7WXRJOyX https:\/\/t.co\/PEjqQoxTCp",
  "id" : 873514552775901184,
  "created_at" : "2017-06-10 12:17:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "indices" : [ 3, 7 ],
      "id_str" : "380648579",
      "id" : 380648579
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AFP\/status\/873209335207215104\/video\/1",
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/eheuqkmDPg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB4_4AjXsAASBiP.jpg",
      "id_str" : "873204972783112194",
      "id" : 873204972783112194,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB4_4AjXsAASBiP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 360,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eheuqkmDPg"
    } ],
    "hashtags" : [ {
      "text" : "TehranAttacks",
      "indices" : [ 71, 85 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873468782341226496",
  "text" : "RT @AFP: Iran: tens of thousands attend funerals of IS attacks victims #TehranAttacks https:\/\/t.co\/eheuqkmDPg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AFP\/status\/873209335207215104\/video\/1",
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/eheuqkmDPg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB4_4AjXsAASBiP.jpg",
        "id_str" : "873204972783112194",
        "id" : 873204972783112194,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB4_4AjXsAASBiP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 360,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eheuqkmDPg"
      } ],
      "hashtags" : [ {
        "text" : "TehranAttacks",
        "indices" : [ 62, 76 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873209335207215104",
    "text" : "Iran: tens of thousands attend funerals of IS attacks victims #TehranAttacks https:\/\/t.co\/eheuqkmDPg",
    "id" : 873209335207215104,
    "created_at" : "2017-06-09 16:05:03 +0000",
    "user" : {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "protected" : false,
      "id_str" : "380648579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/697343883630481408\/c08JBfBB_normal.jpg",
      "id" : 380648579,
      "verified" : true
    }
  },
  "id" : 873468782341226496,
  "created_at" : "2017-06-10 09:16:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/873456799424425984\/photo\/1",
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/nb6WFn8goJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB8kVHcWsAAYYZe.jpg",
      "id_str" : "873456797926993920",
      "id" : 873456797926993920,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB8kVHcWsAAYYZe.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nb6WFn8goJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/6xOxP6DxDO",
      "expanded_url" : "http:\/\/ift.tt\/2rbTywb",
      "display_url" : "ift.tt\/2rbTywb"
    } ]
  },
  "geo" : { },
  "id_str" : "873456799424425984",
  "text" : "Books on my shelf https:\/\/t.co\/6xOxP6DxDO https:\/\/t.co\/nb6WFn8goJ",
  "id" : 873456799424425984,
  "created_at" : "2017-06-10 08:28:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hong Kong Stream",
      "screen_name" : "hkstream",
      "indices" : [ 3, 12 ],
      "id_str" : "46861341",
      "id" : 46861341
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GUA",
      "indices" : [ 14, 18 ]
    } ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/gisEAdtHyD",
      "expanded_url" : "http:\/\/ift.tt\/2s2SbQA",
      "display_url" : "ift.tt\/2s2SbQA"
    } ]
  },
  "geo" : { },
  "id_str" : "873438245601906688",
  "text" : "RT @hkstream: #GUA Boxed in: life inside the 'coffin cubicles' of Hong Kong \u2013 in pictures https:\/\/t.co\/gisEAdtHyD",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GUA",
        "indices" : [ 0, 4 ]
      } ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/gisEAdtHyD",
        "expanded_url" : "http:\/\/ift.tt\/2s2SbQA",
        "display_url" : "ift.tt\/2s2SbQA"
      } ]
    },
    "geo" : { },
    "id_str" : "872358299567616000",
    "text" : "#GUA Boxed in: life inside the 'coffin cubicles' of Hong Kong \u2013 in pictures https:\/\/t.co\/gisEAdtHyD",
    "id" : 872358299567616000,
    "created_at" : "2017-06-07 07:43:20 +0000",
    "user" : {
      "name" : "Hong Kong Stream",
      "screen_name" : "hkstream",
      "protected" : false,
      "id_str" : "46861341",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/526260706690088960\/1ji6OSUj_normal.png",
      "id" : 46861341,
      "verified" : false
    }
  },
  "id" : 873438245601906688,
  "created_at" : "2017-06-10 07:14:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "J.K. Rowling",
      "screen_name" : "jk_rowling",
      "indices" : [ 3, 14 ],
      "id_str" : "62513246",
      "id" : 62513246
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873431922734755840",
  "text" : "RT @jk_rowling: If you can\u2019t disagree with a woman without reaching for all those filthy old insults, screw you and your politics. 2\/14",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "873207949564497920",
    "geo" : { },
    "id_str" : "873208043382743040",
    "in_reply_to_user_id" : 62513246,
    "text" : "If you can\u2019t disagree with a woman without reaching for all those filthy old insults, screw you and your politics. 2\/14",
    "id" : 873208043382743040,
    "in_reply_to_status_id" : 873207949564497920,
    "created_at" : "2017-06-09 15:59:55 +0000",
    "in_reply_to_screen_name" : "jk_rowling",
    "in_reply_to_user_id_str" : "62513246",
    "user" : {
      "name" : "J.K. Rowling",
      "screen_name" : "jk_rowling",
      "protected" : false,
      "id_str" : "62513246",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031233484419948544\/eUt5_nXy_normal.jpg",
      "id" : 62513246,
      "verified" : true
    }
  },
  "id" : 873431922734755840,
  "created_at" : "2017-06-10 06:49:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "History Lovers Club",
      "screen_name" : "historylvrsclub",
      "indices" : [ 3, 19 ],
      "id_str" : "3863605355",
      "id" : 3863605355
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/historylvrsclub\/status\/873198258734215173\/photo\/1",
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/ucKRqirVCz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB45MAYW0AA3xd6.jpg",
      "id_str" : "873198256179826688",
      "id" : 873198256179826688,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB45MAYW0AA3xd6.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 406
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 406
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 406
      }, {
        "h" : 600,
        "resize" : "fit",
        "w" : 406
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/ucKRqirVCz"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873429645584105472",
  "text" : "RT @historylvrsclub: Fidel Castro at the Lincoln Memorial. https:\/\/t.co\/ucKRqirVCz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/historylvrsclub\/status\/873198258734215173\/photo\/1",
        "indices" : [ 38, 61 ],
        "url" : "https:\/\/t.co\/ucKRqirVCz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB45MAYW0AA3xd6.jpg",
        "id_str" : "873198256179826688",
        "id" : 873198256179826688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB45MAYW0AA3xd6.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 406
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 406
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 406
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 406
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ucKRqirVCz"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873198258734215173",
    "text" : "Fidel Castro at the Lincoln Memorial. https:\/\/t.co\/ucKRqirVCz",
    "id" : 873198258734215173,
    "created_at" : "2017-06-09 15:21:02 +0000",
    "user" : {
      "name" : "History Lovers Club",
      "screen_name" : "historylvrsclub",
      "protected" : false,
      "id_str" : "3863605355",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/650726769058807808\/vWX4lILy_normal.jpg",
      "id" : 3863605355,
      "verified" : false
    }
  },
  "id" : 873429645584105472,
  "created_at" : "2017-06-10 06:40:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Analytics Cloud",
      "screen_name" : "AnalyticsCloud",
      "indices" : [ 3, 18 ],
      "id_str" : "1737613664",
      "id" : 1737613664
    }, {
      "name" : "Econsultancy",
      "screen_name" : "Econsultancy",
      "indices" : [ 32, 45 ],
      "id_str" : "11833202",
      "id" : 11833202
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873420127886290944",
  "text" : "RT @AnalyticsCloud: Adobe &amp; @Econsultancy teamed up to understand the state of digital marketing in South Africa. Download the report: http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/adobe.com\" rel=\"nofollow\"\u003EAdobe\u00AE Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Econsultancy",
        "screen_name" : "Econsultancy",
        "indices" : [ 12, 25 ],
        "id_str" : "11833202",
        "id" : 11833202
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AnalyticsCloud\/status\/872789068190318592\/photo\/1",
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/6DD3LHcVeZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBzFB5sUwAARkVV.jpg",
        "id_str" : "872789064260304896",
        "id" : 872789064260304896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBzFB5sUwAARkVV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6DD3LHcVeZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/7ATYFSrFUc",
        "expanded_url" : "https:\/\/adobe.ly\/2sGs133",
        "display_url" : "adobe.ly\/2sGs133"
      } ]
    },
    "geo" : { },
    "id_str" : "872789068190318592",
    "text" : "Adobe &amp; @Econsultancy teamed up to understand the state of digital marketing in South Africa. Download the report: https:\/\/t.co\/7ATYFSrFUc https:\/\/t.co\/6DD3LHcVeZ",
    "id" : 872789068190318592,
    "created_at" : "2017-06-08 12:15:04 +0000",
    "user" : {
      "name" : "Analytics Cloud",
      "screen_name" : "AnalyticsCloud",
      "protected" : false,
      "id_str" : "1737613664",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035220383337598976\/A_FXtjQy_normal.jpg",
      "id" : 1737613664,
      "verified" : true
    }
  },
  "id" : 873420127886290944,
  "created_at" : "2017-06-10 06:02:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Abdallah",
      "screen_name" : "sahouraxo",
      "indices" : [ 3, 13 ],
      "id_str" : "3293406121",
      "id" : 3293406121
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/sahouraxo\/status\/873258246651265024\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/u88m8xvy8H",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB5vvZCXUAANBBK.jpg",
      "id_str" : "873258237721792512",
      "id" : 873258237721792512,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB5vvZCXUAANBBK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 684,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 684,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 485,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 684,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/u88m8xvy8H"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873417738882076672",
  "text" : "RT @sahouraxo: Where are ISIS supporters tweeting from the most? Saudi Arabia! \n\nImagine my shock. https:\/\/t.co\/u88m8xvy8H",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sahouraxo\/status\/873258246651265024\/photo\/1",
        "indices" : [ 84, 107 ],
        "url" : "https:\/\/t.co\/u88m8xvy8H",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB5vvZCXUAANBBK.jpg",
        "id_str" : "873258237721792512",
        "id" : 873258237721792512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB5vvZCXUAANBBK.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 684,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 684,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 485,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 684,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/u88m8xvy8H"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873258246651265024",
    "text" : "Where are ISIS supporters tweeting from the most? Saudi Arabia! \n\nImagine my shock. https:\/\/t.co\/u88m8xvy8H",
    "id" : 873258246651265024,
    "created_at" : "2017-06-09 19:19:25 +0000",
    "user" : {
      "name" : "Sarah Abdallah",
      "screen_name" : "sahouraxo",
      "protected" : false,
      "id_str" : "3293406121",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1032372416390082567\/XF_2-zNO_normal.jpg",
      "id" : 3293406121,
      "verified" : false
    }
  },
  "id" : 873417738882076672,
  "created_at" : "2017-06-10 05:53:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maahil\uD83C\uDF3A\uD83C\uDF43",
      "screen_name" : "MaahilMohamed",
      "indices" : [ 3, 17 ],
      "id_str" : "585491922",
      "id" : 585491922
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/gM0NW2agoH",
      "expanded_url" : "https:\/\/twitter.com\/budumalli\/status\/873093338244587522",
      "display_url" : "twitter.com\/budumalli\/stat\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873414978384121858",
  "text" : "RT @MaahilMohamed: See this. Religiously tolerant Sri Lanka. \u2764\uFE0F https:\/\/t.co\/gM0NW2agoH",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 45, 68 ],
        "url" : "https:\/\/t.co\/gM0NW2agoH",
        "expanded_url" : "https:\/\/twitter.com\/budumalli\/status\/873093338244587522",
        "display_url" : "twitter.com\/budumalli\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873095854306902026",
    "text" : "See this. Religiously tolerant Sri Lanka. \u2764\uFE0F https:\/\/t.co\/gM0NW2agoH",
    "id" : 873095854306902026,
    "created_at" : "2017-06-09 08:34:07 +0000",
    "user" : {
      "name" : "Maahil\uD83C\uDF3A\uD83C\uDF43",
      "screen_name" : "MaahilMohamed",
      "protected" : false,
      "id_str" : "585491922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1003395700149764096\/RasmS7T-_normal.jpg",
      "id" : 585491922,
      "verified" : false
    }
  },
  "id" : 873414978384121858,
  "created_at" : "2017-06-10 05:42:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "indices" : [ 3, 14 ],
      "id_str" : "231474739",
      "id" : 231474739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873414807973658624",
  "text" : "RT @heliumlife: This Singaporean Duo Just Won The Apple Design Awards For Their Brilliant, Yet Simple Currency App \uD83C\uDDF8\uD83C\uDDEC \u2764\uFE0F\u00A0https:\/\/t.co\/UFMW8\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/UFMW8WmuPb",
        "expanded_url" : "https:\/\/vulcanpost.com\/612745\/elk-singapore-apple-design-awards\/",
        "display_url" : "vulcanpost.com\/612745\/elk-sin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873413788686467072",
    "text" : "This Singaporean Duo Just Won The Apple Design Awards For Their Brilliant, Yet Simple Currency App \uD83C\uDDF8\uD83C\uDDEC \u2764\uFE0F\u00A0https:\/\/t.co\/UFMW8WmuPb",
    "id" : 873413788686467072,
    "created_at" : "2017-06-10 05:37:29 +0000",
    "user" : {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "protected" : false,
      "id_str" : "231474739",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039125716262998018\/7LUaZasZ_normal.jpg",
      "id" : 231474739,
      "verified" : false
    }
  },
  "id" : 873414807973658624,
  "created_at" : "2017-06-10 05:41:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ina O' Murchu",
      "screen_name" : "Ina",
      "indices" : [ 3, 7 ],
      "id_str" : "617623",
      "id" : 617623
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/SAcBb9Ni2w",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/uk-northern-ireland-12179367",
      "display_url" : "bbc.com\/news\/uk-northe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873411427037585408",
  "text" : "RT @Ina: BBC News - Hacker translates DUP websites into Irish https:\/\/t.co\/SAcBb9Ni2w",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 53, 76 ],
        "url" : "https:\/\/t.co\/SAcBb9Ni2w",
        "expanded_url" : "http:\/\/www.bbc.com\/news\/uk-northern-ireland-12179367",
        "display_url" : "bbc.com\/news\/uk-northe\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873408633333305344",
    "text" : "BBC News - Hacker translates DUP websites into Irish https:\/\/t.co\/SAcBb9Ni2w",
    "id" : 873408633333305344,
    "created_at" : "2017-06-10 05:16:59 +0000",
    "user" : {
      "name" : "Ina O' Murchu",
      "screen_name" : "Ina",
      "protected" : false,
      "id_str" : "617623",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/968467794345029632\/r9gqerzj_normal.jpg",
      "id" : 617623,
      "verified" : false
    }
  },
  "id" : 873411427037585408,
  "created_at" : "2017-06-10 05:28:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Archaeology",
      "screen_name" : "irarchaeology",
      "indices" : [ 3, 17 ],
      "id_str" : "281480151",
      "id" : 281480151
    }, {
      "name" : "National Museum of Ireland",
      "screen_name" : "NMIreland",
      "indices" : [ 135, 145 ],
      "id_str" : "44133175",
      "id" : 44133175
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873297012313264129",
  "text" : "RT @irarchaeology: Basket made from rye straw &amp; briar bark from the Aran Islands. Dates from c 1900 &amp; is now on display at the @NMIreland C\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "National Museum of Ireland",
        "screen_name" : "NMIreland",
        "indices" : [ 116, 126 ],
        "id_str" : "44133175",
        "id" : 44133175
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/irarchaeology\/status\/873198888538308608\/photo\/1",
        "indices" : [ 147, 170 ],
        "url" : "https:\/\/t.co\/bkCvs1ENvl",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB45SyRXgAAEiuW.jpg",
        "id_str" : "873198372651499520",
        "id" : 873198372651499520,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB45SyRXgAAEiuW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 789
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 671
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 789
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 789
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bkCvs1ENvl"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873198888538308608",
    "text" : "Basket made from rye straw &amp; briar bark from the Aran Islands. Dates from c 1900 &amp; is now on display at the @NMIreland Country Life Museum https:\/\/t.co\/bkCvs1ENvl",
    "id" : 873198888538308608,
    "created_at" : "2017-06-09 15:23:32 +0000",
    "user" : {
      "name" : "Irish Archaeology",
      "screen_name" : "irarchaeology",
      "protected" : false,
      "id_str" : "281480151",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3481529470\/79aaf90b7fbc5eb2e8495348499136ce_normal.jpeg",
      "id" : 281480151,
      "verified" : false
    }
  },
  "id" : 873297012313264129,
  "created_at" : "2017-06-09 21:53:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Fried",
      "screen_name" : "jasonfried",
      "indices" : [ 3, 14 ],
      "id_str" : "14372143",
      "id" : 14372143
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873272939650707456",
  "text" : "RT @jasonfried: Pathetic\u2026 even Apple is promoting workaholism now. Check out this ad for their Planet Of The Apps show. https:\/\/t.co\/PKn30p\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jasonfried\/status\/873174201129205761\/photo\/1",
        "indices" : [ 104, 127 ],
        "url" : "https:\/\/t.co\/PKn30pWWgn",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB4jTiqWsAA14uZ.jpg",
        "id_str" : "873174196385394688",
        "id" : 873174196385394688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB4jTiqWsAA14uZ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 1000,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PKn30pWWgn"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873174201129205761",
    "text" : "Pathetic\u2026 even Apple is promoting workaholism now. Check out this ad for their Planet Of The Apps show. https:\/\/t.co\/PKn30pWWgn",
    "id" : 873174201129205761,
    "created_at" : "2017-06-09 13:45:26 +0000",
    "user" : {
      "name" : "Jason Fried",
      "screen_name" : "jasonfried",
      "protected" : false,
      "id_str" : "14372143",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3413742921\/0e9ef95e76c4a965b9b177fa2267d6c1_normal.png",
      "id" : 14372143,
      "verified" : true
    }
  },
  "id" : 873272939650707456,
  "created_at" : "2017-06-09 20:17:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ajay Nainani",
      "screen_name" : "ajayn23",
      "indices" : [ 3, 11 ],
      "id_str" : "412641812",
      "id" : 412641812
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/6GKgrIZBHZ",
      "expanded_url" : "https:\/\/www.wired.com\/2017\/06\/googles-ai-eye-doctor-gets-ready-go-work-india\/?mbid=social_twitter",
      "display_url" : "wired.com\/2017\/06\/google\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873256720499449856",
  "text" : "RT @ajayn23: It's wonderful to see AI improving people's lives - in this case, preventing blindness in diabetics. \n https:\/\/t.co\/6GKgrIZBHZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/6GKgrIZBHZ",
        "expanded_url" : "https:\/\/www.wired.com\/2017\/06\/googles-ai-eye-doctor-gets-ready-go-work-india\/?mbid=social_twitter",
        "display_url" : "wired.com\/2017\/06\/google\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872838634696658945",
    "text" : "It's wonderful to see AI improving people's lives - in this case, preventing blindness in diabetics. \n https:\/\/t.co\/6GKgrIZBHZ",
    "id" : 872838634696658945,
    "created_at" : "2017-06-08 15:32:01 +0000",
    "user" : {
      "name" : "Ajay Nainani",
      "screen_name" : "ajayn23",
      "protected" : false,
      "id_str" : "412641812",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875787927648743424\/MQjs2zD8_normal.jpg",
      "id" : 412641812,
      "verified" : false
    }
  },
  "id" : 873256720499449856,
  "created_at" : "2017-06-09 19:13:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "indices" : [ 3, 14 ],
      "id_str" : "607156756",
      "id" : 607156756
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873182888732372993",
  "text" : "RT @hellofrmSG: Books vs movie adaptions: which are good and which didn't make the cut?",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873076097146994688",
    "text" : "Books vs movie adaptions: which are good and which didn't make the cut?",
    "id" : 873076097146994688,
    "created_at" : "2017-06-09 07:15:37 +0000",
    "user" : {
      "name" : "hellofrmsg\/\/ \u201CSGverbatim\u201D",
      "screen_name" : "hellofrmSG",
      "protected" : false,
      "id_str" : "607156756",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040905442216407040\/xF1JwbXR_normal.jpg",
      "id" : 607156756,
      "verified" : false
    }
  },
  "id" : 873182888732372993,
  "created_at" : "2017-06-09 14:19:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "George Osborne",
      "screen_name" : "George_Osborne",
      "indices" : [ 3, 18 ],
      "id_str" : "1225696320",
      "id" : 1225696320
    }, {
      "name" : "Christian Adams",
      "screen_name" : "Adamstoon1",
      "indices" : [ 46, 57 ],
      "id_str" : "20530425",
      "id" : 20530425
    }, {
      "name" : "Evening Standard",
      "screen_name" : "EveningStandard",
      "indices" : [ 80, 96 ],
      "id_str" : "1168472395",
      "id" : 1168472395
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/George_Osborne\/status\/873145672672968704\/photo\/1",
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/u2VGmfxbIy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB4JW0fWsAAEyDU.jpg",
      "id_str" : "873145665408380928",
      "id" : 873145665408380928,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB4JW0fWsAAEyDU.jpg",
      "sizes" : [ {
        "h" : 1414,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 829,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 469,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1414,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/u2VGmfxbIy"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873182503657496576",
  "text" : "RT @George_Osborne: Here's a new cartoon from @Adamstoon1 for our third edition @EveningStandard - after Saatchis https:\/\/t.co\/u2VGmfxbIy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Christian Adams",
        "screen_name" : "Adamstoon1",
        "indices" : [ 26, 37 ],
        "id_str" : "20530425",
        "id" : 20530425
      }, {
        "name" : "Evening Standard",
        "screen_name" : "EveningStandard",
        "indices" : [ 60, 76 ],
        "id_str" : "1168472395",
        "id" : 1168472395
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/George_Osborne\/status\/873145672672968704\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/u2VGmfxbIy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB4JW0fWsAAEyDU.jpg",
        "id_str" : "873145665408380928",
        "id" : 873145665408380928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB4JW0fWsAAEyDU.jpg",
        "sizes" : [ {
          "h" : 1414,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 829,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 469,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1414,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/u2VGmfxbIy"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873145672672968704",
    "text" : "Here's a new cartoon from @Adamstoon1 for our third edition @EveningStandard - after Saatchis https:\/\/t.co\/u2VGmfxbIy",
    "id" : 873145672672968704,
    "created_at" : "2017-06-09 11:52:05 +0000",
    "user" : {
      "name" : "George Osborne",
      "screen_name" : "George_Osborne",
      "protected" : false,
      "id_str" : "1225696320",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/626832022061875200\/rAK0SMIW_normal.jpg",
      "id" : 1225696320,
      "verified" : true
    }
  },
  "id" : 873182503657496576,
  "created_at" : "2017-06-09 14:18:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nick de Semlyen",
      "screen_name" : "NickdeSemlyen",
      "indices" : [ 3, 17 ],
      "id_str" : "29697308",
      "id" : 29697308
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/NickdeSemlyen\/status\/873002021397442561\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/y2vKUoBZAo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB2GsuPXsAEvUST.jpg",
      "id_str" : "873002005664608257",
      "id" : 873002005664608257,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB2GsuPXsAEvUST.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 584,
        "resize" : "fit",
        "w" : 1264
      }, {
        "h" : 554,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 314,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 584,
        "resize" : "fit",
        "w" : 1264
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/y2vKUoBZAo"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873180418450886657",
  "text" : "RT @NickdeSemlyen: The Prime Minister, Lord Buckethead and Elmo, all in a line. God bless this country. https:\/\/t.co\/y2vKUoBZAo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/NickdeSemlyen\/status\/873002021397442561\/photo\/1",
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/y2vKUoBZAo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB2GsuPXsAEvUST.jpg",
        "id_str" : "873002005664608257",
        "id" : 873002005664608257,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB2GsuPXsAEvUST.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 584,
          "resize" : "fit",
          "w" : 1264
        }, {
          "h" : 554,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 314,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 584,
          "resize" : "fit",
          "w" : 1264
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y2vKUoBZAo"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873002021397442561",
    "text" : "The Prime Minister, Lord Buckethead and Elmo, all in a line. God bless this country. https:\/\/t.co\/y2vKUoBZAo",
    "id" : 873002021397442561,
    "created_at" : "2017-06-09 02:21:16 +0000",
    "user" : {
      "name" : "Nick de Semlyen",
      "screen_name" : "NickdeSemlyen",
      "protected" : false,
      "id_str" : "29697308",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/959152167541182464\/ctT3ebBW_normal.jpg",
      "id" : 29697308,
      "verified" : true
    }
  },
  "id" : 873180418450886657,
  "created_at" : "2017-06-09 14:10:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/YMKK23HsNz",
      "expanded_url" : "https:\/\/twitter.com\/marktigheST\/status\/873173669325000705",
      "display_url" : "twitter.com\/marktigheST\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873177584598691841",
  "text" : "RT @lisaocarroll: Meanwhile - v significant development here on Irish connection to London Bridge attacker https:\/\/t.co\/YMKK23HsNz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/YMKK23HsNz",
        "expanded_url" : "https:\/\/twitter.com\/marktigheST\/status\/873173669325000705",
        "display_url" : "twitter.com\/marktigheST\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873173983692279812",
    "text" : "Meanwhile - v significant development here on Irish connection to London Bridge attacker https:\/\/t.co\/YMKK23HsNz",
    "id" : 873173983692279812,
    "created_at" : "2017-06-09 13:44:35 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 873177584598691841,
  "created_at" : "2017-06-09 13:58:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873173946522370048",
  "text" : "A political party trying to secure a seat in Westminster and failed. Another got 7 seats but decide not to take up.",
  "id" : 873173946522370048,
  "created_at" : "2017-06-09 13:44:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Morpork Punt \uD83C\uDF40",
      "screen_name" : "ladystormhold",
      "indices" : [ 0, 14 ],
      "id_str" : "275589813",
      "id" : 275589813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "873141889242976258",
  "geo" : { },
  "id_str" : "873173394723917825",
  "in_reply_to_user_id" : 275589813,
  "text" : "@ladystormhold I think I know who you referring to.\uD83D\uDE09",
  "id" : 873173394723917825,
  "in_reply_to_status_id" : 873141889242976258,
  "created_at" : "2017-06-09 13:42:14 +0000",
  "in_reply_to_screen_name" : "ladystormhold",
  "in_reply_to_user_id_str" : "275589813",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 0, 12 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "873155093696319489",
  "geo" : { },
  "id_str" : "873173074828554240",
  "in_reply_to_user_id" : 19793936,
  "text" : "@gianniponzi It viable if you have your own transport. \uD83D\uDE00",
  "id" : 873173074828554240,
  "in_reply_to_status_id" : 873155093696319489,
  "created_at" : "2017-06-09 13:40:58 +0000",
  "in_reply_to_screen_name" : "gianniponzi",
  "in_reply_to_user_id_str" : "19793936",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873140806273695744",
  "text" : "\"Ireland is a small country so your commute time will be low.\"  I do not think u can live in Dublin &amp; commute to Wexford everyday.",
  "id" : 873140806273695744,
  "created_at" : "2017-06-09 11:32:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/873133261618388992\/photo\/1",
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/hVLR1vlK4t",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB399KBXkAADHFj.jpg",
      "id_str" : "873133129883684864",
      "id" : 873133129883684864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB399KBXkAADHFj.jpg",
      "sizes" : [ {
        "h" : 538,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 861,
        "resize" : "fit",
        "w" : 1920
      }, {
        "h" : 305,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 861,
        "resize" : "fit",
        "w" : 1920
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/hVLR1vlK4t"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873133261618388992",
  "text" : "I going to print this out when eating with extended family. \"Yap, you can take fish right?\" https:\/\/t.co\/hVLR1vlK4t",
  "id" : 873133261618388992,
  "created_at" : "2017-06-09 11:02:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/ifV0jLw2MV",
      "expanded_url" : "https:\/\/twitter.com\/pollytoynbee\/status\/873103711395954688",
      "display_url" : "twitter.com\/pollytoynbee\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "873130025473646593",
  "text" : "RT @ronanlyons: Apparently not a parody account. British journalists have a lot to learn about Northern Ireland. https:\/\/t.co\/ifV0jLw2MV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/ifV0jLw2MV",
        "expanded_url" : "https:\/\/twitter.com\/pollytoynbee\/status\/873103711395954688",
        "display_url" : "twitter.com\/pollytoynbee\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "873125043814912002",
    "text" : "Apparently not a parody account. British journalists have a lot to learn about Northern Ireland. https:\/\/t.co\/ifV0jLw2MV",
    "id" : 873125043814912002,
    "created_at" : "2017-06-09 10:30:06 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 873130025473646593,
  "created_at" : "2017-06-09 10:49:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/873068842028679168\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/0Ga3DWBnNs",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DB3DedyUIAAhCon.jpg",
      "id_str" : "873068830938898432",
      "id" : 873068830938898432,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB3DedyUIAAhCon.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 520,
        "resize" : "fit",
        "w" : 780
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 520,
        "resize" : "fit",
        "w" : 780
      }, {
        "h" : 520,
        "resize" : "fit",
        "w" : 780
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0Ga3DWBnNs"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/I4fm9LRswt",
      "expanded_url" : "http:\/\/str.sg\/4bUg",
      "display_url" : "str.sg\/4bUg"
    } ]
  },
  "geo" : { },
  "id_str" : "873126844727386112",
  "text" : "RT @STcom: Phuket beachgoers warned of 'Portuguese Men of War' https:\/\/t.co\/I4fm9LRswt https:\/\/t.co\/0Ga3DWBnNs",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/873068842028679168\/photo\/1",
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/0Ga3DWBnNs",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB3DedyUIAAhCon.jpg",
        "id_str" : "873068830938898432",
        "id" : 873068830938898432,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB3DedyUIAAhCon.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 520,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 520,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 520,
          "resize" : "fit",
          "w" : 780
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0Ga3DWBnNs"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 52, 75 ],
        "url" : "https:\/\/t.co\/I4fm9LRswt",
        "expanded_url" : "http:\/\/str.sg\/4bUg",
        "display_url" : "str.sg\/4bUg"
      } ]
    },
    "geo" : { },
    "id_str" : "873068842028679168",
    "text" : "Phuket beachgoers warned of 'Portuguese Men of War' https:\/\/t.co\/I4fm9LRswt https:\/\/t.co\/0Ga3DWBnNs",
    "id" : 873068842028679168,
    "created_at" : "2017-06-09 06:46:47 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 873126844727386112,
  "created_at" : "2017-06-09 10:37:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "startup",
      "indices" : [ 109, 117 ]
    }, {
      "text" : "entrepreneurship",
      "indices" : [ 118, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873091899132788738",
  "text" : "RT @Blk71Singapore: \"Remember, it is a small world. Today\u2019s fired employee could be your customer tomorrow.\" #startup #entrepreneurship #hi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Blk71Singapore\/status\/873082244805480448\/photo\/1",
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/MIbYWnpBU6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB3PrExVwAAZjdr.jpg",
        "id_str" : "873082241701756928",
        "id" : 873082241701756928,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB3PrExVwAAZjdr.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 417,
          "resize" : "fit",
          "w" : 744
        }, {
          "h" : 417,
          "resize" : "fit",
          "w" : 744
        }, {
          "h" : 417,
          "resize" : "fit",
          "w" : 744
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/MIbYWnpBU6"
      } ],
      "hashtags" : [ {
        "text" : "startup",
        "indices" : [ 89, 97 ]
      }, {
        "text" : "entrepreneurship",
        "indices" : [ 98, 115 ]
      }, {
        "text" : "hiring",
        "indices" : [ 116, 123 ]
      }, {
        "text" : "firing",
        "indices" : [ 124, 131 ]
      }, {
        "text" : "tips",
        "indices" : [ 132, 137 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873082244805480448",
    "text" : "\"Remember, it is a small world. Today\u2019s fired employee could be your customer tomorrow.\" #startup #entrepreneurship #hiring #firing #tips https:\/\/t.co\/MIbYWnpBU6",
    "id" : 873082244805480448,
    "created_at" : "2017-06-09 07:40:02 +0000",
    "user" : {
      "name" : "BLOCK71 Singapore",
      "screen_name" : "BLOCK71Sg",
      "protected" : false,
      "id_str" : "561914045",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882529473156882432\/_z_N24eP_normal.jpg",
      "id" : 561914045,
      "verified" : false
    }
  },
  "id" : 873091899132788738,
  "created_at" : "2017-06-09 08:18:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jackie Fox",
      "screen_name" : "jackiefox_",
      "indices" : [ 3, 14 ],
      "id_str" : "38239993",
      "id" : 38239993
    }, {
      "name" : "DUP",
      "screen_name" : "duponline",
      "indices" : [ 20, 30 ],
      "id_str" : "19977542",
      "id" : 19977542
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GE2017",
      "indices" : [ 117, 124 ]
    }, {
      "text" : "Election2017",
      "indices" : [ 125, 138 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873080516496375808",
  "text" : "RT @jackiefox_: DUP @duponline website has crashed following comments that they could have a role in hung parliament #GE2017 #Election2017\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "DUP",
        "screen_name" : "duponline",
        "indices" : [ 4, 14 ],
        "id_str" : "19977542",
        "id" : 19977542
      }, {
        "name" : "Morning Ireland",
        "screen_name" : "morningireland",
        "indices" : [ 123, 138 ],
        "id_str" : "22790104",
        "id" : 22790104
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jackiefox_\/status\/873065827158732801\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/tt7vRlg4Fo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DB3ApozV0AIqPIT.jpg",
        "id_str" : "873065724339671042",
        "id" : 873065724339671042,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DB3ApozV0AIqPIT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 666,
          "resize" : "fit",
          "w" : 880
        }, {
          "h" : 515,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 666,
          "resize" : "fit",
          "w" : 880
        }, {
          "h" : 666,
          "resize" : "fit",
          "w" : 880
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tt7vRlg4Fo"
      } ],
      "hashtags" : [ {
        "text" : "GE2017",
        "indices" : [ 101, 108 ]
      }, {
        "text" : "Election2017",
        "indices" : [ 109, 122 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "873065827158732801",
    "text" : "DUP @duponline website has crashed following comments that they could have a role in hung parliament #GE2017 #Election2017 @morningireland https:\/\/t.co\/tt7vRlg4Fo",
    "id" : 873065827158732801,
    "created_at" : "2017-06-09 06:34:48 +0000",
    "user" : {
      "name" : "Jackie Fox",
      "screen_name" : "jackiefox_",
      "protected" : false,
      "id_str" : "38239993",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/971485993286160390\/obSHpJ-I_normal.jpg",
      "id" : 38239993,
      "verified" : false
    }
  },
  "id" : 873080516496375808,
  "created_at" : "2017-06-09 07:33:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "873080097493794816",
  "text" : "I living in Ireland and google DUP now.",
  "id" : 873080097493794816,
  "created_at" : "2017-06-09 07:31:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "I am Germany \/\/ Sarah",
      "screen_name" : "I_amGermany",
      "indices" : [ 3, 15 ],
      "id_str" : "613201592",
      "id" : 613201592
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872929534739443714",
  "text" : "RT @I_amGermany: All eyes on the U.K., my friends. This is an extremely odd and eventful day for politics in the western hemisphere",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "872923753151434754",
    "text" : "All eyes on the U.K., my friends. This is an extremely odd and eventful day for politics in the western hemisphere",
    "id" : 872923753151434754,
    "created_at" : "2017-06-08 21:10:15 +0000",
    "user" : {
      "name" : "I am Germany \/\/ Sarah",
      "screen_name" : "I_amGermany",
      "protected" : false,
      "id_str" : "613201592",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1041382504882757633\/qFgEwA2m_normal.jpg",
      "id" : 613201592,
      "verified" : false
    }
  },
  "id" : 872929534739443714,
  "created_at" : "2017-06-08 21:33:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ExitPoll",
      "indices" : [ 106, 115 ]
    }, {
      "text" : "ElectionDay2017",
      "indices" : [ 116, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872929132069498880",
  "text" : "RT @DanMooreMUFC: Theresa May is currently praying that Diane Abbott counted the votes for the exit polls #ExitPoll #ElectionDay2017",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ExitPoll",
        "indices" : [ 88, 97 ]
      }, {
        "text" : "ElectionDay2017",
        "indices" : [ 98, 114 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "872925408039194628",
    "text" : "Theresa May is currently praying that Diane Abbott counted the votes for the exit polls #ExitPoll #ElectionDay2017",
    "id" : 872925408039194628,
    "created_at" : "2017-06-08 21:16:50 +0000",
    "user" : {
      "name" : "Dan Moore",
      "screen_name" : "DRM5515",
      "protected" : false,
      "id_str" : "96549765",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1027852317498982400\/w_Spk2p8_normal.jpg",
      "id" : 96549765,
      "verified" : false
    }
  },
  "id" : 872929132069498880,
  "created_at" : "2017-06-08 21:31:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cathy Heffernan",
      "screen_name" : "cathyheffernan",
      "indices" : [ 3, 18 ],
      "id_str" : "19658020",
      "id" : 19658020
    }, {
      "name" : "George Hook",
      "screen_name" : "ghook",
      "indices" : [ 47, 53 ],
      "id_str" : "21008866",
      "id" : 21008866
    }, {
      "name" : "Newstalk",
      "screen_name" : "NewstalkFM",
      "indices" : [ 76, 87 ],
      "id_str" : "22646514",
      "id" : 22646514
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cathyheffernan\/status\/872558890726174721\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/5FLIPW2zl1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBvzqsaXYAEBjLF.jpg",
      "id_str" : "872558867628122113",
      "id" : 872558867628122113,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBvzqsaXYAEBjLF.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 382
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 750
      }, {
        "h" : 1334,
        "resize" : "fit",
        "w" : 750
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5FLIPW2zl1"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/cathyheffernan\/status\/872558890726174721\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/5FLIPW2zl1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBvzqsYWsAAbPlB.jpg",
      "id_str" : "872558867619688448",
      "id" : 872558867619688448,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBvzqsYWsAAbPlB.jpg",
      "sizes" : [ {
        "h" : 752,
        "resize" : "fit",
        "w" : 745
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 752,
        "resize" : "fit",
        "w" : 745
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 674
      }, {
        "h" : 752,
        "resize" : "fit",
        "w" : 745
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5FLIPW2zl1"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872899789574242306",
  "text" : "RT @cathyheffernan: A letter from my father to @ghook after he announced on @NewstalkFM that he doesn't like Muslims https:\/\/t.co\/5FLIPW2zl1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "George Hook",
        "screen_name" : "ghook",
        "indices" : [ 27, 33 ],
        "id_str" : "21008866",
        "id" : 21008866
      }, {
        "name" : "Newstalk",
        "screen_name" : "NewstalkFM",
        "indices" : [ 56, 67 ],
        "id_str" : "22646514",
        "id" : 22646514
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cathyheffernan\/status\/872558890726174721\/photo\/1",
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/5FLIPW2zl1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBvzqsaXYAEBjLF.jpg",
        "id_str" : "872558867628122113",
        "id" : 872558867628122113,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBvzqsaXYAEBjLF.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 382
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        }, {
          "h" : 1334,
          "resize" : "fit",
          "w" : 750
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5FLIPW2zl1"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/cathyheffernan\/status\/872558890726174721\/photo\/1",
        "indices" : [ 97, 120 ],
        "url" : "https:\/\/t.co\/5FLIPW2zl1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBvzqsYWsAAbPlB.jpg",
        "id_str" : "872558867619688448",
        "id" : 872558867619688448,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBvzqsYWsAAbPlB.jpg",
        "sizes" : [ {
          "h" : 752,
          "resize" : "fit",
          "w" : 745
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 752,
          "resize" : "fit",
          "w" : 745
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 674
        }, {
          "h" : 752,
          "resize" : "fit",
          "w" : 745
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5FLIPW2zl1"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "872558890726174721",
    "text" : "A letter from my father to @ghook after he announced on @NewstalkFM that he doesn't like Muslims https:\/\/t.co\/5FLIPW2zl1",
    "id" : 872558890726174721,
    "created_at" : "2017-06-07 21:00:25 +0000",
    "user" : {
      "name" : "Cathy Heffernan",
      "screen_name" : "cathyheffernan",
      "protected" : false,
      "id_str" : "19658020",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/983653262791340038\/_9dM0AHR_normal.jpg",
      "id" : 19658020,
      "verified" : false
    }
  },
  "id" : 872899789574242306,
  "created_at" : "2017-06-08 19:35:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jason Johnson",
      "screen_name" : "DrJasonJohnson",
      "indices" : [ 3, 18 ],
      "id_str" : "50669520",
      "id" : 50669520
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872859631739166720",
  "text" : "RT @DrJasonJohnson: When your BOSS says \"I Hope\" you hear \"You better DO this\" that's pretty common understanding for any employee #ComeyTe\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ComeyTestimony",
        "indices" : [ 111, 126 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "872831297042907136",
    "text" : "When your BOSS says \"I Hope\" you hear \"You better DO this\" that's pretty common understanding for any employee #ComeyTestimony",
    "id" : 872831297042907136,
    "created_at" : "2017-06-08 15:02:52 +0000",
    "user" : {
      "name" : "Jason Johnson",
      "screen_name" : "DrJasonJohnson",
      "protected" : false,
      "id_str" : "50669520",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/931045513545297921\/wuX7b_Vy_normal.jpg",
      "id" : 50669520,
      "verified" : true
    }
  },
  "id" : 872859631739166720,
  "created_at" : "2017-06-08 16:55:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reza Zadeh",
      "screen_name" : "Reza_Zadeh",
      "indices" : [ 3, 14 ],
      "id_str" : "92839676",
      "id" : 92839676
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/PsgjO1Qdo0",
      "expanded_url" : "https:\/\/developer.apple.com\/documentation\/coreml\/converting_trained_models_to_core_ml",
      "display_url" : "developer.apple.com\/documentation\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872853384969277441",
  "text" : "RT @Reza_Zadeh: Apple CoreML allows automatic conversion of outside models into CoreML. https:\/\/t.co\/PsgjO1Qdo0 Supports Caffe, Keras, scik\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Reza_Zadeh\/status\/871902113345019905\/photo\/1",
        "indices" : [ 145, 168 ],
        "url" : "https:\/\/t.co\/SVC5jBPLPg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBmeMg_U0AAjh-3.jpg",
        "id_str" : "871901940724060160",
        "id" : 871901940724060160,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBmeMg_U0AAjh-3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 672,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 672,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 672,
          "resize" : "fit",
          "w" : 770
        }, {
          "h" : 593,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/SVC5jBPLPg"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 72, 95 ],
        "url" : "https:\/\/t.co\/PsgjO1Qdo0",
        "expanded_url" : "https:\/\/developer.apple.com\/documentation\/coreml\/converting_trained_models_to_core_ml",
        "display_url" : "developer.apple.com\/documentation\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "871902113345019905",
    "text" : "Apple CoreML allows automatic conversion of outside models into CoreML. https:\/\/t.co\/PsgjO1Qdo0 Supports Caffe, Keras, scikit-learn &amp; others https:\/\/t.co\/SVC5jBPLPg",
    "id" : 871902113345019905,
    "created_at" : "2017-06-06 01:30:37 +0000",
    "user" : {
      "name" : "Reza Zadeh",
      "screen_name" : "Reza_Zadeh",
      "protected" : false,
      "id_str" : "92839676",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/851697488817934336\/SoH_BlNa_normal.jpg",
      "id" : 92839676,
      "verified" : true
    }
  },
  "id" : 872853384969277441,
  "created_at" : "2017-06-08 16:30:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 24, 47 ],
      "url" : "https:\/\/t.co\/R9ianKUhTI",
      "expanded_url" : "https:\/\/www.quora.com\/",
      "display_url" : "quora.com"
    } ]
  },
  "geo" : { },
  "id_str" : "872804762839904256",
  "text" : "I spending more time on https:\/\/t.co\/R9ianKUhTI now",
  "id" : 872804762839904256,
  "created_at" : "2017-06-08 13:17:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brilliant Ads",
      "screen_name" : "Brilliant_Ads",
      "indices" : [ 3, 17 ],
      "id_str" : "564686965",
      "id" : 564686965
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Brilliant_Ads\/status\/870397805197754369\/photo\/1",
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/Vb4Z8AdjA7",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBRGMJ7UAAA7BDW.jpg",
      "id_str" : "870397802626416640",
      "id" : 870397802626416640,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBRGMJ7UAAA7BDW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Vb4Z8AdjA7"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872726370937446401",
  "text" : "RT @Brilliant_Ads: Total genius from Banksy: \u2018Modern Prison\u2019 https:\/\/t.co\/Vb4Z8AdjA7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Brilliant_Ads\/status\/870397805197754369\/photo\/1",
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/Vb4Z8AdjA7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBRGMJ7UAAA7BDW.jpg",
        "id_str" : "870397802626416640",
        "id" : 870397802626416640,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBRGMJ7UAAA7BDW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Vb4Z8AdjA7"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "870397805197754369",
    "text" : "Total genius from Banksy: \u2018Modern Prison\u2019 https:\/\/t.co\/Vb4Z8AdjA7",
    "id" : 870397805197754369,
    "created_at" : "2017-06-01 21:53:02 +0000",
    "user" : {
      "name" : "Brilliant Ads",
      "screen_name" : "Brilliant_Ads",
      "protected" : false,
      "id_str" : "564686965",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/568757608471736320\/HVxk-qDA_normal.jpeg",
      "id" : 564686965,
      "verified" : false
    }
  },
  "id" : 872726370937446401,
  "created_at" : "2017-06-08 08:05:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SarcasticRover",
      "screen_name" : "SarcasticRover",
      "indices" : [ 3, 18 ],
      "id_str" : "740109097",
      "id" : 740109097
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/hJGPqH1B2d",
      "expanded_url" : "https:\/\/twitter.com\/jeff_foust\/status\/872521464100028428",
      "display_url" : "twitter.com\/jeff_foust\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872695583697457152",
  "text" : "RT @SarcasticRover: WHY ARE THEY ALL HUMAN??? https:\/\/t.co\/hJGPqH1B2d",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/itunes.apple.com\/us\/app\/twitter\/id409789998?mt=12\" rel=\"nofollow\"\u003ETwitter for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/hJGPqH1B2d",
        "expanded_url" : "https:\/\/twitter.com\/jeff_foust\/status\/872521464100028428",
        "display_url" : "twitter.com\/jeff_foust\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872521944075194368",
    "text" : "WHY ARE THEY ALL HUMAN??? https:\/\/t.co\/hJGPqH1B2d",
    "id" : 872521944075194368,
    "created_at" : "2017-06-07 18:33:36 +0000",
    "user" : {
      "name" : "SarcasticRover",
      "screen_name" : "SarcasticRover",
      "protected" : false,
      "id_str" : "740109097",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839728901165203456\/mFZ-sZXQ_normal.jpg",
      "id" : 740109097,
      "verified" : false
    }
  },
  "id" : 872695583697457152,
  "created_at" : "2017-06-08 06:03:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "indices" : [ 3, 17 ],
      "id_str" : "15773853",
      "id" : 15773853
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "measure",
      "indices" : [ 105, 113 ]
    }, {
      "text" : "AI",
      "indices" : [ 114, 117 ]
    } ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/9s2vhJzxRh",
      "expanded_url" : "https:\/\/hbr.org\/2017\/06\/if-your-company-isnt-good-at-analytics-its-not-ready-for-ai",
      "display_url" : "hbr.org\/2017\/06\/if-you\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872695251177275394",
  "text" : "RT @LisaCollinsSG: If your company isn't good at analytics it's not ready for AI https:\/\/t.co\/9s2vhJzxRh #measure #AI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "measure",
        "indices" : [ 86, 94 ]
      }, {
        "text" : "AI",
        "indices" : [ 95, 98 ]
      } ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/9s2vhJzxRh",
        "expanded_url" : "https:\/\/hbr.org\/2017\/06\/if-your-company-isnt-good-at-analytics-its-not-ready-for-ai",
        "display_url" : "hbr.org\/2017\/06\/if-you\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872672745288482822",
    "text" : "If your company isn't good at analytics it's not ready for AI https:\/\/t.co\/9s2vhJzxRh #measure #AI",
    "id" : 872672745288482822,
    "created_at" : "2017-06-08 04:32:50 +0000",
    "user" : {
      "name" : "Lisa Collins",
      "screen_name" : "LisaCollinsSG",
      "protected" : false,
      "id_str" : "15773853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/422911882286993409\/2L1H6Nkh_normal.jpeg",
      "id" : 15773853,
      "verified" : false
    }
  },
  "id" : 872695251177275394,
  "created_at" : "2017-06-08 06:02:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "getoptimise",
      "indices" : [ 99, 111 ]
    } ],
    "urls" : [ {
      "indices" : [ 112, 135 ],
      "url" : "https:\/\/t.co\/2RjrXMjp1k",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/872526509575110656",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872529246094282753",
  "text" : "If you already being using GA and need access to those unsampled data, there is a way. Contact me. #getoptimise https:\/\/t.co\/2RjrXMjp1k",
  "id" : 872529246094282753,
  "created_at" : "2017-06-07 19:02:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872526509575110656",
  "text" : "If your website has millions of pageviews a month &amp; you need access to unsampled data but GA premium is too expensive $150k, consider Piwik",
  "id" : 872526509575110656,
  "created_at" : "2017-06-07 18:51:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David J. Maireles",
      "screen_name" : "davidjmaireles",
      "indices" : [ 3, 18 ],
      "id_str" : "1355369503",
      "id" : 1355369503
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Partnerships",
      "indices" : [ 20, 33 ]
    }, {
      "text" : "data",
      "indices" : [ 106, 111 ]
    }, {
      "text" : "creditcards",
      "indices" : [ 113, 125 ]
    }, {
      "text" : "banking",
      "indices" : [ 126, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872525214235717633",
  "text" : "RT @davidjmaireles: #Partnerships are key\nOverview of which stakeholders have access to which transaction #data \n#creditcards #banking\n\nhtt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/davidjmaireles\/status\/872326764411879426\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/61WQV02s4d",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBsgkTyXUAAykVL.jpg",
        "id_str" : "872326760985219072",
        "id" : 872326760985219072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBsgkTyXUAAykVL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 746,
          "resize" : "fit",
          "w" : 1237
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 746,
          "resize" : "fit",
          "w" : 1237
        }, {
          "h" : 724,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/61WQV02s4d"
      } ],
      "hashtags" : [ {
        "text" : "Partnerships",
        "indices" : [ 0, 13 ]
      }, {
        "text" : "data",
        "indices" : [ 86, 91 ]
      }, {
        "text" : "creditcards",
        "indices" : [ 93, 105 ]
      }, {
        "text" : "banking",
        "indices" : [ 106, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/dP6GcooxTl",
        "expanded_url" : "http:\/\/on.bcg.com\/2rRAPW0",
        "display_url" : "on.bcg.com\/2rRAPW0"
      } ]
    },
    "geo" : { },
    "id_str" : "872326764411879426",
    "text" : "#Partnerships are key\nOverview of which stakeholders have access to which transaction #data \n#creditcards #banking\n\nhttps:\/\/t.co\/dP6GcooxTl https:\/\/t.co\/61WQV02s4d",
    "id" : 872326764411879426,
    "created_at" : "2017-06-07 05:38:02 +0000",
    "user" : {
      "name" : "David J. Maireles",
      "screen_name" : "davidjmaireles",
      "protected" : false,
      "id_str" : "1355369503",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005205267372544000\/F8rsQYmu_normal.jpg",
      "id" : 1355369503,
      "verified" : false
    }
  },
  "id" : 872525214235717633,
  "created_at" : "2017-06-07 18:46:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DocHackenbush",
      "screen_name" : "DocHackenbush",
      "indices" : [ 3, 17 ],
      "id_str" : "145992071",
      "id" : 145992071
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/DocHackenbush\/status\/872036107587915776\/photo\/1",
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/0ztEyKMuXr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBoYNpPW0AIZSn8.jpg",
      "id_str" : "872036100537241602",
      "id" : 872036100537241602,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBoYNpPW0AIZSn8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0ztEyKMuXr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872521511038484482",
  "text" : "RT @DocHackenbush: finally a shop that caters to my needs https:\/\/t.co\/0ztEyKMuXr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DocHackenbush\/status\/872036107587915776\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/0ztEyKMuXr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBoYNpPW0AIZSn8.jpg",
        "id_str" : "872036100537241602",
        "id" : 872036100537241602,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBoYNpPW0AIZSn8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0ztEyKMuXr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "872036107587915776",
    "text" : "finally a shop that caters to my needs https:\/\/t.co\/0ztEyKMuXr",
    "id" : 872036107587915776,
    "created_at" : "2017-06-06 10:23:04 +0000",
    "user" : {
      "name" : "DocHackenbush",
      "screen_name" : "DocHackenbush",
      "protected" : false,
      "id_str" : "145992071",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1030057392929759233\/DcwUX2nu_normal.jpg",
      "id" : 145992071,
      "verified" : false
    }
  },
  "id" : 872521511038484482,
  "created_at" : "2017-06-07 18:31:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872468460315566080",
  "text" : "What are the things that are affordable in Europe but expensive in Singapore? (besides Vehicles)",
  "id" : 872468460315566080,
  "created_at" : "2017-06-07 15:01:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/CgeZCB579S",
      "expanded_url" : "http:\/\/for.tn\/2rsP1Ef",
      "display_url" : "for.tn\/2rsP1Ef"
    } ]
  },
  "geo" : { },
  "id_str" : "872448705038831616",
  "text" : "10 students lose Harvard acceptance over Facebook posts https:\/\/t.co\/CgeZCB579S",
  "id" : 872448705038831616,
  "created_at" : "2017-06-07 13:42:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 3, 17 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/hA85gGs6oZ",
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/872308722827227137",
      "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872440685919707136",
  "text" : "RT @ofmeowandbake: Can become more affordable for locals or not https:\/\/t.co\/hA85gGs6oZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 45, 68 ],
        "url" : "https:\/\/t.co\/hA85gGs6oZ",
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/872308722827227137",
        "display_url" : "twitter.com\/ChannelNewsAsi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872416034002657280",
    "text" : "Can become more affordable for locals or not https:\/\/t.co\/hA85gGs6oZ",
    "id" : 872416034002657280,
    "created_at" : "2017-06-07 11:32:45 +0000",
    "user" : {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "protected" : false,
      "id_str" : "815066809",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/988013070067810304\/AJgYqxuR_normal.jpg",
      "id" : 815066809,
      "verified" : false
    }
  },
  "id" : 872440685919707136,
  "created_at" : "2017-06-07 13:10:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Halal Navi",
      "screen_name" : "Halalnavi",
      "indices" : [ 3, 13 ],
      "id_str" : "2543249502",
      "id" : 2543249502
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Halalnavi\/status\/872411841527517185\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/7V0XG86hyo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBtt8h6XoAAqJMV.jpg",
      "id_str" : "872411839489089536",
      "id" : 872411839489089536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBtt8h6XoAAqJMV.jpg",
      "sizes" : [ {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 960,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/7V0XG86hyo"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/3C6IhrcqD7",
      "expanded_url" : "http:\/\/crwd.fr\/2rSQrse",
      "display_url" : "crwd.fr\/2rSQrse"
    } ]
  },
  "geo" : { },
  "id_str" : "872440447104409601",
  "text" : "RT @Halalnavi: I have a feeling you\u2019ll like this one \uD83D\uDE0D Top 10 Halal Friendly Ramen In Japan\n\nhttps:\/\/t.co\/3C6IhrcqD7 https:\/\/t.co\/7V0XG86hyo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.crowdfireapp.com\" rel=\"nofollow\"\u003ECrowdfire - Go Big\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Halalnavi\/status\/872411841527517185\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/7V0XG86hyo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBtt8h6XoAAqJMV.jpg",
        "id_str" : "872411839489089536",
        "id" : 872411839489089536,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBtt8h6XoAAqJMV.jpg",
        "sizes" : [ {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 960,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/7V0XG86hyo"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/3C6IhrcqD7",
        "expanded_url" : "http:\/\/crwd.fr\/2rSQrse",
        "display_url" : "crwd.fr\/2rSQrse"
      } ]
    },
    "geo" : { },
    "id_str" : "872411841527517185",
    "text" : "I have a feeling you\u2019ll like this one \uD83D\uDE0D Top 10 Halal Friendly Ramen In Japan\n\nhttps:\/\/t.co\/3C6IhrcqD7 https:\/\/t.co\/7V0XG86hyo",
    "id" : 872411841527517185,
    "created_at" : "2017-06-07 11:16:06 +0000",
    "user" : {
      "name" : "Halal Navi",
      "screen_name" : "Halalnavi",
      "protected" : false,
      "id_str" : "2543249502",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/785790367727628289\/Pa4o1qij_normal.jpg",
      "id" : 2543249502,
      "verified" : false
    }
  },
  "id" : 872440447104409601,
  "created_at" : "2017-06-07 13:09:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kotchka Babushka",
      "screen_name" : "ofmeowandbake",
      "indices" : [ 0, 14 ],
      "id_str" : "815066809",
      "id" : 815066809
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872411975325814788",
  "in_reply_to_user_id" : 815066809,
  "text" : "@ofmeowandbake Thanks for the retweet,",
  "id" : 872411975325814788,
  "created_at" : "2017-06-07 11:16:38 +0000",
  "in_reply_to_screen_name" : "ofmeowandbake",
  "in_reply_to_user_id_str" : "815066809",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 79, 89 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872409190391787520",
  "text" : "Which are the best value for money provider for internet access for a month in #Singapore? Thanks",
  "id" : 872409190391787520,
  "created_at" : "2017-06-07 11:05:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872408669719330816",
  "text" : "Someone is selling EUR 3.50 for a \uD83C\uDF69 in Dublin.",
  "id" : 872408669719330816,
  "created_at" : "2017-06-07 11:03:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maria McHale",
      "screen_name" : "mchale_maria",
      "indices" : [ 3, 16 ],
      "id_str" : "3176753950",
      "id" : 3176753950
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 28, 51 ],
      "url" : "https:\/\/t.co\/rvF2uu1ptu",
      "expanded_url" : "https:\/\/twitter.com\/vallmeister\/status\/872095749185646592",
      "display_url" : "twitter.com\/vallmeister\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872405275550134277",
  "text" : "RT @mchale_maria: This 100% https:\/\/t.co\/rvF2uu1ptu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 10, 33 ],
        "url" : "https:\/\/t.co\/rvF2uu1ptu",
        "expanded_url" : "https:\/\/twitter.com\/vallmeister\/status\/872095749185646592",
        "display_url" : "twitter.com\/vallmeister\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872397290010533888",
    "text" : "This 100% https:\/\/t.co\/rvF2uu1ptu",
    "id" : 872397290010533888,
    "created_at" : "2017-06-07 10:18:16 +0000",
    "user" : {
      "name" : "Maria McHale",
      "screen_name" : "mchale_maria",
      "protected" : false,
      "id_str" : "3176753950",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839173953843245056\/GnsOQaEh_normal.jpg",
      "id" : 3176753950,
      "verified" : false
    }
  },
  "id" : 872405275550134277,
  "created_at" : "2017-06-07 10:50:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mashableasia",
      "screen_name" : "mashableasia",
      "indices" : [ 3, 16 ],
      "id_str" : "1036198656787996673",
      "id" : 1036198656787996673
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mashableasia\/status\/872385357605736448\/photo\/1",
      "indices" : [ 117, 140 ],
      "url" : "https:\/\/t.co\/WeJdV3VVCS",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBtV22oXkAAy_7O.jpg",
      "id_str" : "872385353692450816",
      "id" : 872385353692450816,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBtV22oXkAAy_7O.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/WeJdV3VVCS"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/7B5vS158xd",
      "expanded_url" : "http:\/\/on.mash.to\/2sfe1jB",
      "display_url" : "on.mash.to\/2sfe1jB"
    } ]
  },
  "geo" : { },
  "id_str" : "872397567170158592",
  "text" : "RT @mashableasia: The world's best airport is about to become the only one with a hedge maze https:\/\/t.co\/7B5vS158xd https:\/\/t.co\/WeJdV3VVCS",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mashableasia\/status\/872385357605736448\/photo\/1",
        "indices" : [ 99, 122 ],
        "url" : "https:\/\/t.co\/WeJdV3VVCS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBtV22oXkAAy_7O.jpg",
        "id_str" : "872385353692450816",
        "id" : 872385353692450816,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBtV22oXkAAy_7O.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WeJdV3VVCS"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/7B5vS158xd",
        "expanded_url" : "http:\/\/on.mash.to\/2sfe1jB",
        "display_url" : "on.mash.to\/2sfe1jB"
      } ]
    },
    "geo" : { },
    "id_str" : "872385357605736448",
    "text" : "The world's best airport is about to become the only one with a hedge maze https:\/\/t.co\/7B5vS158xd https:\/\/t.co\/WeJdV3VVCS",
    "id" : 872385357605736448,
    "created_at" : "2017-06-07 09:30:52 +0000",
    "user" : {
      "name" : "Mashable Southeast Asia",
      "screen_name" : "MashableSEA",
      "protected" : false,
      "id_str" : "3198041813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025385102321299457\/lp2cJZTV_normal.jpg",
      "id" : 3198041813,
      "verified" : false
    }
  },
  "id" : 872397567170158592,
  "created_at" : "2017-06-07 10:19:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "indices" : [ 3, 14 ],
      "id_str" : "91166653",
      "id" : 91166653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872375516308934657",
  "text" : "RT @Clearpreso: Dublin needs faster commuter rail so people don't have to live in Dublin and don't have to pay crazy rent https:\/\/t.co\/udBA\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/udBAwsd2Tj",
        "expanded_url" : "https:\/\/twitter.com\/DubCham\/status\/872355949801070596",
        "display_url" : "twitter.com\/DubCham\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "872374948232343553",
    "text" : "Dublin needs faster commuter rail so people don't have to live in Dublin and don't have to pay crazy rent https:\/\/t.co\/udBAwsd2Tj",
    "id" : 872374948232343553,
    "created_at" : "2017-06-07 08:49:30 +0000",
    "user" : {
      "name" : "Ed Fidgeon-Kavanagh",
      "screen_name" : "Clearpreso",
      "protected" : false,
      "id_str" : "91166653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/922642272805629952\/bXhVKNFk_normal.jpg",
      "id" : 91166653,
      "verified" : false
    }
  },
  "id" : 872375516308934657,
  "created_at" : "2017-06-07 08:51:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Connie Chan",
      "screen_name" : "conniechan",
      "indices" : [ 3, 14 ],
      "id_str" : "8474512",
      "id" : 8474512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872372962833444864",
  "text" : "RT @conniechan: Apple is adding a QR reader in the native camera app for China. QR is such an efficient way of mobile payments https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/conniechan\/status\/871958284638576640\/photo\/1",
        "indices" : [ 135, 158 ],
        "url" : "https:\/\/t.co\/NFTi2T23Z3",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBnNh0VV0AA0lko.jpg",
        "id_str" : "871953983740432384",
        "id" : 871953983740432384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBnNh0VV0AA0lko.jpg",
        "sizes" : [ {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 400,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NFTi2T23Z3"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/FN1HbRPaH2",
        "expanded_url" : "http:\/\/a16z.com\/author\/connie-chan\/",
        "display_url" : "a16z.com\/author\/connie-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "871958284638576640",
    "text" : "Apple is adding a QR reader in the native camera app for China. QR is such an efficient way of mobile payments https:\/\/t.co\/FN1HbRPaH2 https:\/\/t.co\/NFTi2T23Z3",
    "id" : 871958284638576640,
    "created_at" : "2017-06-06 05:13:49 +0000",
    "user" : {
      "name" : "Connie Chan",
      "screen_name" : "conniechan",
      "protected" : false,
      "id_str" : "8474512",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1016709429855567872\/FJe4168O_normal.jpg",
      "id" : 8474512,
      "verified" : false
    }
  },
  "id" : 872372962833444864,
  "created_at" : "2017-06-07 08:41:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/872372351081615360\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/IOsBxMjt8y",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBtKBxZWsAIfwmN.jpg",
      "id_str" : "872372347126329346",
      "id" : 872372347126329346,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBtKBxZWsAIfwmN.jpg",
      "sizes" : [ {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 363,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IOsBxMjt8y"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 92, 115 ],
      "url" : "https:\/\/t.co\/A8GVGFMnxZ",
      "expanded_url" : "http:\/\/cna.asia\/2seDxWa",
      "display_url" : "cna.asia\/2seDxWa"
    } ]
  },
  "geo" : { },
  "id_str" : "872372858080702466",
  "text" : "RT @ChannelNewsAsia: Attackers raid Iran parliament and mausoleum, up to seven dead - media https:\/\/t.co\/A8GVGFMnxZ https:\/\/t.co\/IOsBxMjt8y",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/872372351081615360\/photo\/1",
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/IOsBxMjt8y",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBtKBxZWsAIfwmN.jpg",
        "id_str" : "872372347126329346",
        "id" : 872372347126329346,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBtKBxZWsAIfwmN.jpg",
        "sizes" : [ {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IOsBxMjt8y"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 71, 94 ],
        "url" : "https:\/\/t.co\/A8GVGFMnxZ",
        "expanded_url" : "http:\/\/cna.asia\/2seDxWa",
        "display_url" : "cna.asia\/2seDxWa"
      } ]
    },
    "geo" : { },
    "id_str" : "872372351081615360",
    "text" : "Attackers raid Iran parliament and mausoleum, up to seven dead - media https:\/\/t.co\/A8GVGFMnxZ https:\/\/t.co\/IOsBxMjt8y",
    "id" : 872372351081615360,
    "created_at" : "2017-06-07 08:39:11 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 872372858080702466,
  "created_at" : "2017-06-07 08:41:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Popular Science",
      "screen_name" : "PopSci",
      "indices" : [ 101, 108 ],
      "id_str" : "19722699",
      "id" : 19722699
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/tGxwa04uDF",
      "expanded_url" : "http:\/\/pops.ci\/V3kVxk",
      "display_url" : "pops.ci\/V3kVxk"
    } ]
  },
  "geo" : { },
  "id_str" : "872366295068618753",
  "text" : "Intel\u2019s new chip puts a teraflop in your desktop. Here's what that means https:\/\/t.co\/tGxwa04uDF via @PopSci",
  "id" : 872366295068618753,
  "created_at" : "2017-06-07 08:15:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/Q6IM0tozhd",
      "expanded_url" : "https:\/\/www.theguardian.com\/lifeandstyle\/2017\/jun\/04\/how-lego-clicked-the-super-brand-that-reinvented-itself",
      "display_url" : "theguardian.com\/lifeandstyle\/2\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872366141884256256",
  "text" : "How Lego clicked: the brand that reinvented itself https:\/\/t.co\/Q6IM0tozhd",
  "id" : 872366141884256256,
  "created_at" : "2017-06-07 08:14:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872342848267583490",
  "text" : "Want to update my email address for a mailing list. Can't proceed until you put down website url?! End up unsubscribing.",
  "id" : 872342848267583490,
  "created_at" : "2017-06-07 06:41:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "martinmcmahon",
      "screen_name" : "williamhboney1",
      "indices" : [ 3, 18 ],
      "id_str" : "506498124",
      "id" : 506498124
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RTEpt",
      "indices" : [ 133, 139 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872200700381593600",
  "text" : "RT @williamhboney1: It's too dangerous for terrorists in Dublin, they're likely to get shot in the crossfire of feuding drug lords \n\n#RTEpt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RTEpt",
        "indices" : [ 113, 119 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "872194778691194882",
    "text" : "It's too dangerous for terrorists in Dublin, they're likely to get shot in the crossfire of feuding drug lords \n\n#RTEpt",
    "id" : 872194778691194882,
    "created_at" : "2017-06-06 20:53:34 +0000",
    "user" : {
      "name" : "martinmcmahon",
      "screen_name" : "williamhboney1",
      "protected" : false,
      "id_str" : "506498124",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039929976722780160\/Wdy0EHip_normal.jpg",
      "id" : 506498124,
      "verified" : false
    }
  },
  "id" : 872200700381593600,
  "created_at" : "2017-06-06 21:17:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/6eFl7K91lN",
      "expanded_url" : "http:\/\/cnb.cx\/2rSuvv7",
      "display_url" : "cnb.cx\/2rSuvv7"
    } ]
  },
  "geo" : { },
  "id_str" : "872200369870450688",
  "text" : "Why Virtual Singapore isn\u2019t just another digital map https:\/\/t.co\/6eFl7K91lN",
  "id" : 872200369870450688,
  "created_at" : "2017-06-06 21:15:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872193316150018048",
  "text" : "If you use Google Analytics Remarketing features, starting May 15th 2017 Google requires you to clearly state this in your Privacy Policy.",
  "id" : 872193316150018048,
  "created_at" : "2017-06-06 20:47:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/kIjBCteLMw",
      "expanded_url" : "http:\/\/str.sg\/4Lzi",
      "display_url" : "str.sg\/4Lzi"
    } ]
  },
  "geo" : { },
  "id_str" : "872191793659613185",
  "text" : "The judge ruled in favour of the principal https:\/\/t.co\/kIjBCteLMw",
  "id" : 872191793659613185,
  "created_at" : "2017-06-06 20:41:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nicolas Glady",
      "screen_name" : "nicogla",
      "indices" : [ 3, 11 ],
      "id_str" : "246264429",
      "id" : 246264429
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "dataviz",
      "indices" : [ 96, 104 ]
    } ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/09BSCTTkmi",
      "expanded_url" : "https:\/\/goo.gl\/05EtSF",
      "display_url" : "goo.gl\/05EtSF"
    } ]
  },
  "geo" : { },
  "id_str" : "872187369776197633",
  "text" : "RT @nicogla: Five Data Visualization Mistakes that Make My Head Explode https:\/\/t.co\/09BSCTTkmi #dataviz",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "dataviz",
        "indices" : [ 83, 91 ]
      } ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/09BSCTTkmi",
        "expanded_url" : "https:\/\/goo.gl\/05EtSF",
        "display_url" : "goo.gl\/05EtSF"
      } ]
    },
    "geo" : { },
    "id_str" : "871725593708113920",
    "text" : "Five Data Visualization Mistakes that Make My Head Explode https:\/\/t.co\/09BSCTTkmi #dataviz",
    "id" : 871725593708113920,
    "created_at" : "2017-06-05 13:49:12 +0000",
    "user" : {
      "name" : "Nicolas Glady",
      "screen_name" : "nicogla",
      "protected" : false,
      "id_str" : "246264429",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/883070533318963200\/3hkHe4f4_normal.jpg",
      "id" : 246264429,
      "verified" : false
    }
  },
  "id" : 872187369776197633,
  "created_at" : "2017-06-06 20:24:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/872187006150946817\/photo\/1",
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/HJm0je6Guf",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBqhdPBXsAAF5F7.jpg",
      "id_str" : "872187001470169088",
      "id" : 872187001470169088,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBqhdPBXsAAF5F7.jpg",
      "sizes" : [ {
        "h" : 131,
        "resize" : "fit",
        "w" : 655
      }, {
        "h" : 131,
        "resize" : "fit",
        "w" : 655
      }, {
        "h" : 131,
        "resize" : "fit",
        "w" : 655
      }, {
        "h" : 131,
        "resize" : "crop",
        "w" : 131
      }, {
        "h" : 131,
        "resize" : "fit",
        "w" : 655
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HJm0je6Guf"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 8, 31 ],
      "url" : "https:\/\/t.co\/AybqmFfS9X",
      "expanded_url" : "http:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/london-attacks-british-police-name-two-of-three-assailants-1.3108230",
      "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "872187006150946817",
  "text" : "Source: https:\/\/t.co\/AybqmFfS9X https:\/\/t.co\/HJm0je6Guf",
  "id" : 872187006150946817,
  "created_at" : "2017-06-06 20:22:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alan Baxter",
      "screen_name" : "AlanBaxter",
      "indices" : [ 3, 14 ],
      "id_str" : "17670827",
      "id" : 17670827
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AlanBaxter\/status\/863259258443374593\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/RgneuZ4Bkt",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/C_rpsciUMAAgBHq.jpg",
      "id_str" : "863259228378509312",
      "id" : 863259228378509312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C_rpsciUMAAgBHq.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RgneuZ4Bkt"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872172415555694592",
  "text" : "RT @AlanBaxter: Neil Gaiman on imposter syndrome. https:\/\/t.co\/RgneuZ4Bkt",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AlanBaxter\/status\/863259258443374593\/photo\/1",
        "indices" : [ 34, 57 ],
        "url" : "https:\/\/t.co\/RgneuZ4Bkt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/C_rpsciUMAAgBHq.jpg",
        "id_str" : "863259228378509312",
        "id" : 863259228378509312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/C_rpsciUMAAgBHq.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RgneuZ4Bkt"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "863259258443374593",
    "text" : "Neil Gaiman on imposter syndrome. https:\/\/t.co\/RgneuZ4Bkt",
    "id" : 863259258443374593,
    "created_at" : "2017-05-13 05:07:00 +0000",
    "user" : {
      "name" : "Alan Baxter",
      "screen_name" : "AlanBaxter",
      "protected" : false,
      "id_str" : "17670827",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963385446263767040\/-PUaLBQA_normal.jpg",
      "id" : 17670827,
      "verified" : true
    }
  },
  "id" : 872172415555694592,
  "created_at" : "2017-06-06 19:24:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/871341049934991360\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/EdCchtCdcr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBegDfMWsAA_cCs.jpg",
      "id_str" : "871341034692849664",
      "id" : 871341034692849664,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBegDfMWsAA_cCs.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 233,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 458,
        "resize" : "fit",
        "w" : 1339
      }, {
        "h" : 458,
        "resize" : "fit",
        "w" : 1339
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EdCchtCdcr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872139938401325056",
  "text" : "RT @getoptimise: Help to verify client has properly set-up a User-ID implementation. https:\/\/t.co\/EdCchtCdcr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/871341049934991360\/photo\/1",
        "indices" : [ 68, 91 ],
        "url" : "https:\/\/t.co\/EdCchtCdcr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBegDfMWsAA_cCs.jpg",
        "id_str" : "871341034692849664",
        "id" : 871341034692849664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBegDfMWsAA_cCs.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 233,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 458,
          "resize" : "fit",
          "w" : 1339
        }, {
          "h" : 458,
          "resize" : "fit",
          "w" : 1339
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EdCchtCdcr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871341049934991360",
    "text" : "Help to verify client has properly set-up a User-ID implementation. https:\/\/t.co\/EdCchtCdcr",
    "id" : 871341049934991360,
    "created_at" : "2017-06-04 12:21:09 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 872139938401325056,
  "created_at" : "2017-06-06 17:15:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Flightradar24",
      "screen_name" : "flightradar24",
      "indices" : [ 3, 17 ],
      "id_str" : "134196350",
      "id" : 134196350
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "872128989908938755",
  "text" : "RT @flightradar24: How Qatar Airways flights are managing bans on flights through the airspace of Bahrain, Saudi Arabia, UAE, &amp; Egypt. http\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/flightradar24\/status\/871977931182157824\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/y8VOEzfUoC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBni6fSWsAEfq3Y.jpg",
        "id_str" : "871977497331675137",
        "id" : 871977497331675137,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBni6fSWsAEfq3Y.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y8VOEzfUoC"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/flightradar24\/status\/871977931182157824\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/y8VOEzfUoC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBni-G3XkAA1AmJ.jpg",
        "id_str" : "871977559495512064",
        "id" : 871977559495512064,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBni-G3XkAA1AmJ.jpg",
        "sizes" : [ {
          "h" : 1198,
          "resize" : "fit",
          "w" : 1531
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 939,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1198,
          "resize" : "fit",
          "w" : 1531
        }, {
          "h" : 532,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y8VOEzfUoC"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/flightradar24\/status\/871977931182157824\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/y8VOEzfUoC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBnjBxOXUAAGwbW.jpg",
        "id_str" : "871977622405861376",
        "id" : 871977622405861376,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBnjBxOXUAAGwbW.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 984
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 558
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y8VOEzfUoC"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/lXphkQfLja",
        "expanded_url" : "https:\/\/blog.flightradar24.com\/blog\/flight-ban-for-qatar-flights-in-uae-saudi-arabia-bahrain-and-egypt\/",
        "display_url" : "blog.flightradar24.com\/blog\/flight-ba\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "871977931182157824",
    "text" : "How Qatar Airways flights are managing bans on flights through the airspace of Bahrain, Saudi Arabia, UAE, &amp; Egypt. https:\/\/t.co\/lXphkQfLja https:\/\/t.co\/y8VOEzfUoC",
    "id" : 871977931182157824,
    "created_at" : "2017-06-06 06:31:54 +0000",
    "user" : {
      "name" : "Flightradar24",
      "screen_name" : "flightradar24",
      "protected" : false,
      "id_str" : "134196350",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/710502600593035265\/eU_SCiU9_normal.jpg",
      "id" : 134196350,
      "verified" : true
    }
  },
  "id" : 872128989908938755,
  "created_at" : "2017-06-06 16:32:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/ck9oBR9Vlb",
      "expanded_url" : "https:\/\/marketoonist.com\/2015\/02\/morewithless.html",
      "display_url" : "marketoonist.com\/2015\/02\/morewi\u2026"
    }, {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/9JYzuX4OY4",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/871716090547757056",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "871800925119275008",
  "text" : "\u201CAdvertising is a tax for unremarkable thinking\u201D - Founder of Geek Squad https:\/\/t.co\/ck9oBR9Vlb https:\/\/t.co\/9JYzuX4OY4",
  "id" : 871800925119275008,
  "created_at" : "2017-06-05 18:48:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "indices" : [ 3, 16 ],
      "id_str" : "20192882",
      "id" : 20192882
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mikewhitmore\/status\/871740652408721409\/photo\/1",
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/Sr6uDmIKcF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBkLgIdXUAELPtX.jpg",
      "id_str" : "871740649527267329",
      "id" : 871740649527267329,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBkLgIdXUAELPtX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 474,
        "resize" : "fit",
        "w" : 813
      }, {
        "h" : 396,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 474,
        "resize" : "fit",
        "w" : 813
      }, {
        "h" : 474,
        "resize" : "fit",
        "w" : 813
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Sr6uDmIKcF"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/xHeR3K95z3",
      "expanded_url" : "http:\/\/bit.ly\/2rlvLIC",
      "display_url" : "bit.ly\/2rlvLIC"
    } ]
  },
  "geo" : { },
  "id_str" : "871748936196919296",
  "text" : "RT @mikewhitmore: How Much Should I Charge? A 5-Step Formula for Designers https:\/\/t.co\/xHeR3K95z3 https:\/\/t.co\/Sr6uDmIKcF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mikewhitmore\/status\/871740652408721409\/photo\/1",
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/Sr6uDmIKcF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBkLgIdXUAELPtX.jpg",
        "id_str" : "871740649527267329",
        "id" : 871740649527267329,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBkLgIdXUAELPtX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 474,
          "resize" : "fit",
          "w" : 813
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 474,
          "resize" : "fit",
          "w" : 813
        }, {
          "h" : 474,
          "resize" : "fit",
          "w" : 813
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Sr6uDmIKcF"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 57, 80 ],
        "url" : "https:\/\/t.co\/xHeR3K95z3",
        "expanded_url" : "http:\/\/bit.ly\/2rlvLIC",
        "display_url" : "bit.ly\/2rlvLIC"
      } ]
    },
    "geo" : { },
    "id_str" : "871740652408721409",
    "text" : "How Much Should I Charge? A 5-Step Formula for Designers https:\/\/t.co\/xHeR3K95z3 https:\/\/t.co\/Sr6uDmIKcF",
    "id" : 871740652408721409,
    "created_at" : "2017-06-05 14:49:02 +0000",
    "user" : {
      "name" : "Mike Whitmore",
      "screen_name" : "mikewhitmore",
      "protected" : false,
      "id_str" : "20192882",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/814556566179352577\/J3-2QBfu_normal.jpg",
      "id" : 20192882,
      "verified" : false
    }
  },
  "id" : 871748936196919296,
  "created_at" : "2017-06-05 15:21:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/3CWRKc7bBh",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/871714097854861312",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "871716090547757056",
  "text" : "Diplomatically speaking: Have you exhaust all others viable options or are you barking at the wrong tree? https:\/\/t.co\/3CWRKc7bBh",
  "id" : 871716090547757056,
  "created_at" : "2017-06-05 13:11:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871714097854861312",
  "text" : "To the CEO who interviewed me pointing out I miss out PPC: because I don't your biz can afford it on those keywords u are targeting.",
  "id" : 871714097854861312,
  "created_at" : "2017-06-05 13:03:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Inman",
      "screen_name" : "Oatmeal",
      "indices" : [ 3, 11 ],
      "id_str" : "4519121",
      "id" : 4519121
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Oatmeal\/status\/810216943596793856\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/XKSzJnjCU5",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Cz53ke2UcAAjdj5.jpg",
      "id_str" : "810216451613159424",
      "id" : 810216451613159424,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cz53ke2UcAAjdj5.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 561
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1454,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 990
      }, {
        "h" : 1454,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XKSzJnjCU5"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871710974105059328",
  "text" : "RT @Oatmeal: What a mobile website is -supposed- to do https:\/\/t.co\/XKSzJnjCU5",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Oatmeal\/status\/810216943596793856\/photo\/1",
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/XKSzJnjCU5",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/Cz53ke2UcAAjdj5.jpg",
        "id_str" : "810216451613159424",
        "id" : 810216451613159424,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Cz53ke2UcAAjdj5.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 561
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1454,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 990
        }, {
          "h" : 1454,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XKSzJnjCU5"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "810216943596793856",
    "text" : "What a mobile website is -supposed- to do https:\/\/t.co\/XKSzJnjCU5",
    "id" : 810216943596793856,
    "created_at" : "2016-12-17 20:15:47 +0000",
    "user" : {
      "name" : "Matthew Inman",
      "screen_name" : "Oatmeal",
      "protected" : false,
      "id_str" : "4519121",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/793613884053532674\/teYItBjT_normal.jpg",
      "id" : 4519121,
      "verified" : true
    }
  },
  "id" : 871710974105059328,
  "created_at" : "2017-06-05 12:51:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matthew Inman",
      "screen_name" : "Oatmeal",
      "indices" : [ 16, 24 ],
      "id_str" : "4519121",
      "id" : 4519121
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871710942572228609",
  "text" : "RT @baldo_uc3m: @Oatmeal In EU, just add the cookie law disclaimer to all that bullshit :D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Matthew Inman",
        "screen_name" : "Oatmeal",
        "indices" : [ 0, 8 ],
        "id_str" : "4519121",
        "id" : 4519121
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "810216943596793856",
    "geo" : { },
    "id_str" : "813826160127864832",
    "in_reply_to_user_id" : 4519121,
    "text" : "@Oatmeal In EU, just add the cookie law disclaimer to all that bullshit :D",
    "id" : 813826160127864832,
    "in_reply_to_status_id" : 810216943596793856,
    "created_at" : "2016-12-27 19:17:31 +0000",
    "in_reply_to_screen_name" : "Oatmeal",
    "in_reply_to_user_id_str" : "4519121",
    "user" : {
      "name" : "Alejandro Baldominos",
      "screen_name" : "baldomin0s",
      "protected" : false,
      "id_str" : "308372471",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1375921587\/baldo_normal.png",
      "id" : 308372471,
      "verified" : false
    }
  },
  "id" : 871710942572228609,
  "created_at" : "2017-06-05 12:50:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "indices" : [ 3, 16 ],
      "id_str" : "5988062",
      "id" : 5988062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871628015083696128",
  "text" : "RT @TheEconomist: She sells street food. Her son studies architecture. \"We all want to have a professional in the family,\" she says https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/t1oqJGvXJq",
        "expanded_url" : "http:\/\/econ.st\/2rv519p",
        "display_url" : "econ.st\/2rv519p"
      } ]
    },
    "geo" : { },
    "id_str" : "871468514397257728",
    "text" : "She sells street food. Her son studies architecture. \"We all want to have a professional in the family,\" she says https:\/\/t.co\/t1oqJGvXJq",
    "id" : 871468514397257728,
    "created_at" : "2017-06-04 20:47:39 +0000",
    "user" : {
      "name" : "The Economist",
      "screen_name" : "TheEconomist",
      "protected" : false,
      "id_str" : "5988062",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/879361767914262528\/HdRauDM-_normal.jpg",
      "id" : 5988062,
      "verified" : true
    }
  },
  "id" : 871628015083696128,
  "created_at" : "2017-06-05 07:21:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pink Floyd - Steve\u00AE\u2122",
      "screen_name" : "steve_sps",
      "indices" : [ 3, 13 ],
      "id_str" : "338968305",
      "id" : 338968305
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ChrisMartin",
      "indices" : [ 37, 49 ]
    }, {
      "text" : "LiamGallagher",
      "indices" : [ 56, 70 ]
    }, {
      "text" : "WeStandTogether",
      "indices" : [ 114, 130 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871516982604177408",
  "text" : "RT @steve_sps: Never thought I'd see #ChrisMartin &amp; #LiamGallagher performing on the same stage..\n\nNice one...#WeStandTogether \uD83D\uDC4D\uD83C\uDFFC\uD83C\uDFB8\uD83D\uDE03\n\n#OneLo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/steve_sps\/status\/871516474342604801\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/bYR8XRyjns",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBg_moVXsAAs7X0.jpg",
        "id_str" : "871516460790820864",
        "id" : 871516460790820864,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBg_moVXsAAs7X0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bYR8XRyjns"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/steve_sps\/status\/871516474342604801\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/bYR8XRyjns",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBg_moeXUAATpNm.jpg",
        "id_str" : "871516460828545024",
        "id" : 871516460828545024,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBg_moeXUAATpNm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bYR8XRyjns"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/steve_sps\/status\/871516474342604801\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/bYR8XRyjns",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBg_mq6WsAESd1p.jpg",
        "id_str" : "871516461482815489",
        "id" : 871516461482815489,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBg_mq6WsAESd1p.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bYR8XRyjns"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/steve_sps\/status\/871516474342604801\/photo\/1",
        "indices" : [ 144, 167 ],
        "url" : "https:\/\/t.co\/bYR8XRyjns",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBg_muwXsAYFi4q.jpg",
        "id_str" : "871516462514679814",
        "id" : 871516462514679814,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBg_muwXsAYFi4q.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/bYR8XRyjns"
      } ],
      "hashtags" : [ {
        "text" : "ChrisMartin",
        "indices" : [ 22, 34 ]
      }, {
        "text" : "LiamGallagher",
        "indices" : [ 41, 55 ]
      }, {
        "text" : "WeStandTogether",
        "indices" : [ 99, 115 ]
      }, {
        "text" : "OneLoveManchester",
        "indices" : [ 122, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871516474342604801",
    "text" : "Never thought I'd see #ChrisMartin &amp; #LiamGallagher performing on the same stage..\n\nNice one...#WeStandTogether \uD83D\uDC4D\uD83C\uDFFC\uD83C\uDFB8\uD83D\uDE03\n\n#OneLoveManchester \u2764\uFE0F https:\/\/t.co\/bYR8XRyjns",
    "id" : 871516474342604801,
    "created_at" : "2017-06-04 23:58:14 +0000",
    "user" : {
      "name" : "Pink Floyd - Steve\u00AE\u2122",
      "screen_name" : "steve_sps",
      "protected" : false,
      "id_str" : "338968305",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017149424827543552\/HKxd4PpV_normal.jpg",
      "id" : 338968305,
      "verified" : false
    }
  },
  "id" : 871516982604177408,
  "created_at" : "2017-06-05 00:00:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darragh Doyle",
      "screen_name" : "darraghdoyle",
      "indices" : [ 3, 16 ],
      "id_str" : "2200721",
      "id" : 2200721
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871491529646493696",
  "text" : "RT @darraghdoyle: Talking to a London Paramedic:\n\nI always know the Irish when driving a ambulance in London. They're the ones who bless th\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/darraghdoyle\/status\/871470900507541504\/photo\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/8Cu0IFvH5j",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBgWJm_XsAErhup.jpg",
        "id_str" : "871470882237165569",
        "id" : 871470882237165569,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBgWJm_XsAErhup.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 395,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 876,
          "resize" : "fit",
          "w" : 1508
        }, {
          "h" : 876,
          "resize" : "fit",
          "w" : 1508
        }, {
          "h" : 697,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/8Cu0IFvH5j"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871470900507541504",
    "text" : "Talking to a London Paramedic:\n\nI always know the Irish when driving a ambulance in London. They're the ones who bless themselves as I pass. https:\/\/t.co\/8Cu0IFvH5j",
    "id" : 871470900507541504,
    "created_at" : "2017-06-04 20:57:08 +0000",
    "user" : {
      "name" : "Darragh Doyle",
      "screen_name" : "darraghdoyle",
      "protected" : false,
      "id_str" : "2200721",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/649950567951900672\/9oElFhuS_normal.jpg",
      "id" : 2200721,
      "verified" : false
    }
  },
  "id" : 871491529646493696,
  "created_at" : "2017-06-04 22:19:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rob Owers \uD83D\uDCE1\uD83D\uDC49\uD83D\uDCF1",
      "screen_name" : "robowers",
      "indices" : [ 3, 12 ],
      "id_str" : "17363638",
      "id" : 17363638
    }, {
      "name" : "Justin Bieber",
      "screen_name" : "justinbieber",
      "indices" : [ 67, 80 ],
      "id_str" : "27260086",
      "id" : 27260086
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/robowers\/status\/871460361773490176\/photo\/1",
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/YsoZWrYU3f",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBgMkw8XYAAQ0Rz.jpg",
      "id_str" : "871460353649106944",
      "id" : 871460353649106944,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBgMkw8XYAAQ0Rz.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 575,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 575,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 575,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YsoZWrYU3f"
    } ],
    "hashtags" : [ {
      "text" : "OneLoveManchester",
      "indices" : [ 96, 114 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871463498601160704",
  "text" : "RT @robowers: These kids and a policeman holding hands, dancing to @justinbieber at the back of #OneLoveManchester https:\/\/t.co\/YsoZWrYU3f",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Justin Bieber",
        "screen_name" : "justinbieber",
        "indices" : [ 53, 66 ],
        "id_str" : "27260086",
        "id" : 27260086
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/robowers\/status\/871460361773490176\/photo\/1",
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/YsoZWrYU3f",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBgMkw8XYAAQ0Rz.jpg",
        "id_str" : "871460353649106944",
        "id" : 871460353649106944,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBgMkw8XYAAQ0Rz.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 575,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 575,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 575,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YsoZWrYU3f"
      } ],
      "hashtags" : [ {
        "text" : "OneLoveManchester",
        "indices" : [ 82, 100 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871460361773490176",
    "text" : "These kids and a policeman holding hands, dancing to @justinbieber at the back of #OneLoveManchester https:\/\/t.co\/YsoZWrYU3f",
    "id" : 871460361773490176,
    "created_at" : "2017-06-04 20:15:15 +0000",
    "user" : {
      "name" : "Rob Owers \uD83D\uDCE1\uD83D\uDC49\uD83D\uDCF1",
      "screen_name" : "robowers",
      "protected" : false,
      "id_str" : "17363638",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/962397541466861576\/mJFHmGGh_normal.jpg",
      "id" : 17363638,
      "verified" : false
    }
  },
  "id" : 871463498601160704,
  "created_at" : "2017-06-04 20:27:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Python Ireland",
      "screen_name" : "PythonIreland",
      "indices" : [ 3, 17 ],
      "id_str" : "21097510",
      "id" : 21097510
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871396290646732800",
  "text" : "RT @PythonIreland: Save the Dates!!! PyCon Ireland will be taking place at the Radisson Blu Hotel (near Dublin Castle) Sat 21st \/ Sun 22nd\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "python",
        "indices" : [ 132, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "867699826477150208",
    "text" : "Save the Dates!!! PyCon Ireland will be taking place at the Radisson Blu Hotel (near Dublin Castle) Sat 21st \/ Sun 22nd of October. #python",
    "id" : 867699826477150208,
    "created_at" : "2017-05-25 11:12:14 +0000",
    "user" : {
      "name" : "Python Ireland",
      "screen_name" : "PythonIreland",
      "protected" : false,
      "id_str" : "21097510",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/907178042807775232\/z9oQEEMl_normal.jpg",
      "id" : 21097510,
      "verified" : false
    }
  },
  "id" : 871396290646732800,
  "created_at" : "2017-06-04 16:00:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    }, {
      "name" : "Alan Meaney",
      "screen_name" : "AlanMeaney",
      "indices" : [ 15, 26 ],
      "id_str" : "366688214",
      "id" : 366688214
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871391527989379072",
  "text" : "RT @patphelan: @AlanMeaney Would have gone global from day one, spent too long engaging irish agencies and thinking about UK being my bigge\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Alan Meaney",
        "screen_name" : "AlanMeaney",
        "indices" : [ 0, 11 ],
        "id_str" : "366688214",
        "id" : 366688214
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "871316382557843456",
    "geo" : { },
    "id_str" : "871317073812758528",
    "in_reply_to_user_id" : 366688214,
    "text" : "@AlanMeaney Would have gone global from day one, spent too long engaging irish agencies and thinking about UK being my biggest market",
    "id" : 871317073812758528,
    "in_reply_to_status_id" : 871316382557843456,
    "created_at" : "2017-06-04 10:45:53 +0000",
    "in_reply_to_screen_name" : "AlanMeaney",
    "in_reply_to_user_id_str" : "366688214",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 871391527989379072,
  "created_at" : "2017-06-04 15:41:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sean Mulvany",
      "screen_name" : "seanmulvany",
      "indices" : [ 3, 15 ],
      "id_str" : "146021653",
      "id" : 146021653
    }, {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 27, 37 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871374634826039296",
  "text" : "RT @seanmulvany: Worrying. @patphelan - \"I don't see scalable early stage start-ups coming out of Ireland\". Now shuttering a \u20AC25M fund. Lot\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Pat",
        "screen_name" : "patphelan",
        "indices" : [ 10, 20 ],
        "id_str" : "10737",
        "id" : 10737
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/seanmulvany\/status\/871343276611317760\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/6URNnv9Udt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBeiE_AXYAE-Vb7.jpg",
        "id_str" : "871343259435622401",
        "id" : 871343259435622401,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBeiE_AXYAE-Vb7.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 648
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1952
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1144
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1952
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6URNnv9Udt"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871343276611317760",
    "text" : "Worrying. @patphelan - \"I don't see scalable early stage start-ups coming out of Ireland\". Now shuttering a \u20AC25M fund. Lots to chew on here https:\/\/t.co\/6URNnv9Udt",
    "id" : 871343276611317760,
    "created_at" : "2017-06-04 12:30:00 +0000",
    "user" : {
      "name" : "Sean Mulvany",
      "screen_name" : "seanmulvany",
      "protected" : false,
      "id_str" : "146021653",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659045644947070976\/sELr1EuV_normal.jpg",
      "id" : 146021653,
      "verified" : false
    }
  },
  "id" : 871374634826039296,
  "created_at" : "2017-06-04 14:34:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Feminella \u23E3\u2708\uFE0F NYC",
      "screen_name" : "jxxf",
      "indices" : [ 3, 8 ],
      "id_str" : "19463693",
      "id" : 19463693
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871341472548827136",
  "text" : "RT @jxxf: If a junior developer accidentally destroys production on their first day, it's *your company's* fault, not theirs. https:\/\/t.co\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jxxf\/status\/871000661869154304\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/GCYWwBdRIy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBZqIZ6XUAAXkFg.jpg",
        "id_str" : "871000270569951232",
        "id" : 871000270569951232,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBZqIZ6XUAAXkFg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1896,
          "resize" : "fit",
          "w" : 1440
        }, {
          "h" : 1896,
          "resize" : "fit",
          "w" : 1440
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 516
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 911
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GCYWwBdRIy"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/jxxf\/status\/871000661869154304\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/GCYWwBdRIy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBZqSiDXcAAoUoM.jpg",
        "id_str" : "871000444553883648",
        "id" : 871000444553883648,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBZqSiDXcAAoUoM.jpg",
        "sizes" : [ {
          "h" : 534,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1131,
          "resize" : "fit",
          "w" : 1440
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1131,
          "resize" : "fit",
          "w" : 1440
        }, {
          "h" : 943,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GCYWwBdRIy"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/jxxf\/status\/871000661869154304\/photo\/1",
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/GCYWwBdRIy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBZqYoeXkAA1a5s.jpg",
        "id_str" : "871000549356965888",
        "id" : 871000549356965888,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBZqYoeXkAA1a5s.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 696,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 835,
          "resize" : "fit",
          "w" : 1440
        }, {
          "h" : 394,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 835,
          "resize" : "fit",
          "w" : 1440
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GCYWwBdRIy"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871000661869154304",
    "text" : "If a junior developer accidentally destroys production on their first day, it's *your company's* fault, not theirs. https:\/\/t.co\/GCYWwBdRIy",
    "id" : 871000661869154304,
    "created_at" : "2017-06-03 13:48:34 +0000",
    "user" : {
      "name" : "John Feminella \u23E3",
      "screen_name" : "jxxf",
      "protected" : false,
      "id_str" : "19463693",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3348427561\/9d7f08f1e103a16c8debd169301b9944_normal.jpeg",
      "id" : 19463693,
      "verified" : false
    }
  },
  "id" : 871341472548827136,
  "created_at" : "2017-06-04 12:22:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    }, {
      "name" : "noho vation",
      "screen_name" : "NohoVation",
      "indices" : [ 35, 46 ],
      "id_str" : "884034851476832256",
      "id" : 884034851476832256
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 109, 132 ],
      "url" : "https:\/\/t.co\/uKmiYYqU2j",
      "expanded_url" : "https:\/\/twitter.com\/tomlyonsbiz\/status\/871293452100804608",
      "display_url" : "twitter.com\/tomlyonsbiz\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "871308242349170688",
  "text" : "RT @patphelan: We decided to close @nohovation due to a lack of startups, will continue to invest personally https:\/\/t.co\/uKmiYYqU2j",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "noho vation",
        "screen_name" : "NohoVation",
        "indices" : [ 20, 31 ],
        "id_str" : "884034851476832256",
        "id" : 884034851476832256
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/uKmiYYqU2j",
        "expanded_url" : "https:\/\/twitter.com\/tomlyonsbiz\/status\/871293452100804608",
        "display_url" : "twitter.com\/tomlyonsbiz\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "871295713648549889",
    "text" : "We decided to close @nohovation due to a lack of startups, will continue to invest personally https:\/\/t.co\/uKmiYYqU2j",
    "id" : 871295713648549889,
    "created_at" : "2017-06-04 09:21:00 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 871308242349170688,
  "created_at" : "2017-06-04 10:10:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 3, 11 ],
      "id_str" : "1652541",
      "id" : 1652541
    }, {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "indices" : [ 43, 51 ],
      "id_str" : "1652541",
      "id" : 1652541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/9rdhxiI1eA",
      "expanded_url" : "http:\/\/reut.rs\/2rQyR9l",
      "display_url" : "reut.rs\/2rQyR9l"
    } ]
  },
  "geo" : { },
  "id_str" : "871306982271201280",
  "text" : "RT @Reuters: No story worth dying for: How @Reuters photographers reacted in Wednesday's Kabul attack. https:\/\/t.co\/9rdhxiI1eA https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Reuters Top News",
        "screen_name" : "Reuters",
        "indices" : [ 30, 38 ],
        "id_str" : "1652541",
        "id" : 1652541
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Reuters\/status\/871091395427258368\/photo\/1",
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/F7msUn5KvL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBa9AYPVoAA4zzH.jpg",
        "id_str" : "871091392147202048",
        "id" : 871091392147202048,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBa9AYPVoAA4zzH.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 459,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 780
        }, {
          "h" : 527,
          "resize" : "fit",
          "w" : 780
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/F7msUn5KvL"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 90, 113 ],
        "url" : "https:\/\/t.co\/9rdhxiI1eA",
        "expanded_url" : "http:\/\/reut.rs\/2rQyR9l",
        "display_url" : "reut.rs\/2rQyR9l"
      } ]
    },
    "geo" : { },
    "id_str" : "871091395427258368",
    "text" : "No story worth dying for: How @Reuters photographers reacted in Wednesday's Kabul attack. https:\/\/t.co\/9rdhxiI1eA https:\/\/t.co\/F7msUn5KvL",
    "id" : 871091395427258368,
    "created_at" : "2017-06-03 19:49:07 +0000",
    "user" : {
      "name" : "Reuters Top News",
      "screen_name" : "Reuters",
      "protected" : false,
      "id_str" : "1652541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/877554927932891136\/ZBEs235N_normal.jpg",
      "id" : 1652541,
      "verified" : true
    }
  },
  "id" : 871306982271201280,
  "created_at" : "2017-06-04 10:05:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TOLOnews",
      "screen_name" : "TOLOnews",
      "indices" : [ 3, 12 ],
      "id_str" : "168259012",
      "id" : 168259012
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Kabul",
      "indices" : [ 96, 102 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871306902646513664",
  "text" : "RT @TOLOnews: Footage shows the three back-to-back explosions at Saturday\u2019s cemetery bombing in #Kabul city, which killed at least 20 peopl\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TOLOnews\/status\/871274973742198785\/video\/1",
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/yf7npLtBOq",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/871274240082931712\/pu\/img\/--6p4xcbcJZR4jS4.jpg",
        "id_str" : "871274240082931712",
        "id" : 871274240082931712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/871274240082931712\/pu\/img\/--6p4xcbcJZR4jS4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/yf7npLtBOq"
      } ],
      "hashtags" : [ {
        "text" : "Kabul",
        "indices" : [ 82, 88 ]
      }, {
        "text" : "Afghanistan",
        "indices" : [ 128, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871274973742198785",
    "text" : "Footage shows the three back-to-back explosions at Saturday\u2019s cemetery bombing in #Kabul city, which killed at least 20 people. #Afghanistan https:\/\/t.co\/yf7npLtBOq",
    "id" : 871274973742198785,
    "created_at" : "2017-06-04 07:58:35 +0000",
    "user" : {
      "name" : "TOLOnews",
      "screen_name" : "TOLOnews",
      "protected" : false,
      "id_str" : "168259012",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/631385657022087169\/pUHtAZOh_normal.jpg",
      "id" : 168259012,
      "verified" : true
    }
  },
  "id" : 871306902646513664,
  "created_at" : "2017-06-04 10:05:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert Macfarlane",
      "screen_name" : "RobGMacfarlane",
      "indices" : [ 3, 18 ],
      "id_str" : "835252426265481216",
      "id" : 835252426265481216
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871285314958045185",
  "text" : "RT @RobGMacfarlane: Word of the day: \u201Cscribble-lark\u201D - folk-name for the yellowhammer, due to the inked-up, written-on appearance of its be\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RobGMacfarlane\/status\/871248933493911552\/photo\/1",
        "indices" : [ 133, 156 ],
        "url" : "https:\/\/t.co\/zHTupdTN3g",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBdMSBkWsAAuZBR.jpg",
        "id_str" : "871248925461753856",
        "id" : 871248925461753856,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBdMSBkWsAAuZBR.jpg",
        "sizes" : [ {
          "h" : 602,
          "resize" : "fit",
          "w" : 846
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 602,
          "resize" : "fit",
          "w" : 846
        }, {
          "h" : 602,
          "resize" : "fit",
          "w" : 846
        }, {
          "h" : 484,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zHTupdTN3g"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/RobGMacfarlane\/status\/871248933493911552\/photo\/1",
        "indices" : [ 133, 156 ],
        "url" : "https:\/\/t.co\/zHTupdTN3g",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBdMSMKWsAAh8df.jpg",
        "id_str" : "871248928305491968",
        "id" : 871248928305491968,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBdMSMKWsAAh8df.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2045,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 3367,
          "resize" : "fit",
          "w" : 3372
        }, {
          "h" : 679,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1198,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zHTupdTN3g"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871248933493911552",
    "text" : "Word of the day: \u201Cscribble-lark\u201D - folk-name for the yellowhammer, due to the inked-up, written-on appearance of its beautiful eggs. https:\/\/t.co\/zHTupdTN3g",
    "id" : 871248933493911552,
    "created_at" : "2017-06-04 06:15:07 +0000",
    "user" : {
      "name" : "Robert Macfarlane",
      "screen_name" : "RobGMacfarlane",
      "protected" : false,
      "id_str" : "835252426265481216",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/835849899006640129\/-qwlW-0a_normal.jpg",
      "id" : 835252426265481216,
      "verified" : true
    }
  },
  "id" : 871285314958045185,
  "created_at" : "2017-06-04 08:39:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871271153674973184",
  "text" : "I apologies if there is a high bounce rate as I accidentally click on the link to you site.",
  "id" : 871271153674973184,
  "created_at" : "2017-06-04 07:43:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ali A. Rizvi",
      "screen_name" : "aliamjadrizvi",
      "indices" : [ 3, 17 ],
      "id_str" : "20891396",
      "id" : 20891396
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871262682036436993",
  "text" : "RT @aliamjadrizvi: Virgin birth, angels, dead-messiah-resurrection: \"Respect my beliefs!\"\n\nClimate change: \"Where's the evidence?!\"\n\nFaith\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "870618239960797184",
    "text" : "Virgin birth, angels, dead-messiah-resurrection: \"Respect my beliefs!\"\n\nClimate change: \"Where's the evidence?!\"\n\nFaith rots the human mind.",
    "id" : 870618239960797184,
    "created_at" : "2017-06-02 12:28:58 +0000",
    "user" : {
      "name" : "Ali A. Rizvi",
      "screen_name" : "aliamjadrizvi",
      "protected" : false,
      "id_str" : "20891396",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/957331676723474432\/zW-9lQGz_normal.jpg",
      "id" : 20891396,
      "verified" : true
    }
  },
  "id" : 871262682036436993,
  "created_at" : "2017-06-04 07:09:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "85053026",
      "id" : 85053026
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871125432317489153",
  "text" : "RT @gavindbrown: All main news outlets reporting \"major incident\" on London Bridge - some reporting van hit people, others say knife attack\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 141, 164 ],
        "url" : "https:\/\/t.co\/vjLxwTA3hp",
        "expanded_url" : "https:\/\/twitter.com\/BBCBreaking\/status\/871117346169073664",
        "display_url" : "twitter.com\/BBCBreaking\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "871124747530125312",
    "text" : "All main news outlets reporting \"major incident\" on London Bridge - some reporting van hit people, others say knife attack, or shots fired!! https:\/\/t.co\/vjLxwTA3hp",
    "id" : 871124747530125312,
    "created_at" : "2017-06-03 22:01:39 +0000",
    "user" : {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "protected" : false,
      "id_str" : "85053026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844192952914231296\/mLoaFZ86_normal.jpg",
      "id" : 85053026,
      "verified" : false
    }
  },
  "id" : 871125432317489153,
  "created_at" : "2017-06-03 22:04:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AsapSCIENCE",
      "screen_name" : "AsapSCIENCE",
      "indices" : [ 3, 15 ],
      "id_str" : "592912724",
      "id" : 592912724
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AsapSCIENCE\/status\/871079045605330944\/photo\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/q5vRnorD6D",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DBU6UwsXYAEin6Y.jpg",
      "id_str" : "870666231308836865",
      "id" : 870666231308836865,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DBU6UwsXYAEin6Y.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 500
      }, {
        "h" : 274,
        "resize" : "fit",
        "w" : 500
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/q5vRnorD6D"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871113802305200132",
  "text" : "RT @AsapSCIENCE: The bio-luminescent defense of this fish is \uD83D\uDD25 [GIF via Reddit] https:\/\/t.co\/q5vRnorD6D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AsapSCIENCE\/status\/871079045605330944\/photo\/1",
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/q5vRnorD6D",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DBU6UwsXYAEin6Y.jpg",
        "id_str" : "870666231308836865",
        "id" : 870666231308836865,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DBU6UwsXYAEin6Y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 500
        }, {
          "h" : 274,
          "resize" : "fit",
          "w" : 500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/q5vRnorD6D"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "871079045605330944",
    "text" : "The bio-luminescent defense of this fish is \uD83D\uDD25 [GIF via Reddit] https:\/\/t.co\/q5vRnorD6D",
    "id" : 871079045605330944,
    "created_at" : "2017-06-03 19:00:02 +0000",
    "user" : {
      "name" : "AsapSCIENCE",
      "screen_name" : "AsapSCIENCE",
      "protected" : false,
      "id_str" : "592912724",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2596038915\/9e5p412uh6qwsuo8700e_normal.jpeg",
      "id" : 592912724,
      "verified" : true
    }
  },
  "id" : 871113802305200132,
  "created_at" : "2017-06-03 21:18:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "871044218160132097",
  "text" : "RT @randal_olson: Junior coder destroys company's production DB, gets fired, and threatened with legal action on first day of work.\n\nhttps:\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/871032452113989632\/photo\/1",
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/LCajQ8nVF0",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBaHNIbW0AEbzER.jpg",
        "id_str" : "871032237613043713",
        "id" : 871032237613043713,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBaHNIbW0AEbzER.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 452,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 798,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1114,
          "resize" : "fit",
          "w" : 1676
        }, {
          "h" : 1114,
          "resize" : "fit",
          "w" : 1676
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/LCajQ8nVF0"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/enhS2ZPik7",
        "expanded_url" : "https:\/\/np.reddit.com\/r\/cscareerquestions\/comments\/6ez8ag\/accidentally_destroyed_production_database_on\/",
        "display_url" : "np.reddit.com\/r\/cscareerques\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "871032452113989632",
    "text" : "Junior coder destroys company's production DB, gets fired, and threatened with legal action on first day of work.\n\nhttps:\/\/t.co\/enhS2ZPik7 https:\/\/t.co\/LCajQ8nVF0",
    "id" : 871032452113989632,
    "created_at" : "2017-06-03 15:54:54 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 871044218160132097,
  "created_at" : "2017-06-03 16:41:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/0AzwGgEIM9",
      "expanded_url" : "http:\/\/www.newyorker.com\/tech\/elements\/how-to-call-bullshit-on-big-data-a-practical-guide",
      "display_url" : "newyorker.com\/tech\/elements\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870955111656411136",
  "text" : "How to Call B.S. on Big Data: A Practical Guide https:\/\/t.co\/0AzwGgEIM9",
  "id" : 870955111656411136,
  "created_at" : "2017-06-03 10:47:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870953023626981376",
  "text" : "Dateline London where commentators and journalists who never get involved in politics talk about politics",
  "id" : 870953023626981376,
  "created_at" : "2017-06-03 10:39:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 3, 15 ],
      "id_str" : "41085467",
      "id" : 41085467
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 113, 136 ],
      "url" : "https:\/\/t.co\/xMFDSbTMiq",
      "expanded_url" : "http:\/\/tdy.sg\/2qRdgsf",
      "display_url" : "tdy.sg\/2qRdgsf"
    } ]
  },
  "geo" : { },
  "id_str" : "870952372935249920",
  "text" : "RT @TODAYonline: Singapore hawkers to feature in Anthony Bourdain\u2019s International Street Food Market in New York\nhttps:\/\/t.co\/xMFDSbTMiq ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/870943144078180352\/photo\/1",
        "indices" : [ 120, 143 ],
        "url" : "https:\/\/t.co\/3aFebzURRd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBYvUyiUAAEJl_k.jpg",
        "id_str" : "870935612152348673",
        "id" : 870935612152348673,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBYvUyiUAAEJl_k.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 683,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/3aFebzURRd"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 96, 119 ],
        "url" : "https:\/\/t.co\/xMFDSbTMiq",
        "expanded_url" : "http:\/\/tdy.sg\/2qRdgsf",
        "display_url" : "tdy.sg\/2qRdgsf"
      } ]
    },
    "geo" : { },
    "id_str" : "870943144078180352",
    "text" : "Singapore hawkers to feature in Anthony Bourdain\u2019s International Street Food Market in New York\nhttps:\/\/t.co\/xMFDSbTMiq https:\/\/t.co\/3aFebzURRd",
    "id" : 870943144078180352,
    "created_at" : "2017-06-03 10:00:01 +0000",
    "user" : {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "protected" : false,
      "id_str" : "41085467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/606854638801846272\/pi_RUbVK_normal.png",
      "id" : 41085467,
      "verified" : true
    }
  },
  "id" : 870952372935249920,
  "created_at" : "2017-06-03 10:36:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/vmNz9KU4lt",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-asia-39945650",
      "display_url" : "bbc.com\/news\/world-asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870888852881375232",
  "text" : "The man who built his plane using YouTube videos https:\/\/t.co\/vmNz9KU4lt",
  "id" : 870888852881375232,
  "created_at" : "2017-06-03 06:24:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/6vHLWVUOFY",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-us-canada-40141979",
      "display_url" : "bbc.com\/news\/world-us-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870888607552335872",
  "text" : "Surf wetsuit pioneer Jack O'Neill dies aged 94 https:\/\/t.co\/6vHLWVUOFY",
  "id" : 870888607552335872,
  "created_at" : "2017-06-03 06:23:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MFAsg",
      "screen_name" : "MFAsg",
      "indices" : [ 3, 9 ],
      "id_str" : "77165249",
      "id" : 77165249
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MFAsg\/status\/870524658184986625\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/Xv8woS1icK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBS5jmOVwAAaTSk.jpg",
      "id_str" : "870524649196601344",
      "id" : 870524649196601344,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBS5jmOVwAAaTSk.jpg",
      "sizes" : [ {
        "h" : 723,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 739,
        "resize" : "fit",
        "w" : 1227
      }, {
        "h" : 739,
        "resize" : "fit",
        "w" : 1227
      }, {
        "h" : 410,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Xv8woS1icK"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/MFAsg\/status\/870524658184986625\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/Xv8woS1icK",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBS5jmKVoAAlEJV.jpg",
      "id_str" : "870524649179815936",
      "id" : 870524649179815936,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBS5jmKVoAAlEJV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 494,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 902,
        "resize" : "fit",
        "w" : 1241
      }, {
        "h" : 902,
        "resize" : "fit",
        "w" : 1241
      }, {
        "h" : 872,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Xv8woS1icK"
    } ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 11, 21 ]
    }, {
      "text" : "Paris",
      "indices" : [ 54, 60 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870886711097688064",
  "text" : "RT @MFAsg: #Singapore reaffirms her commitment to the #Paris Agreement. https:\/\/t.co\/Xv8woS1icK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MFAsg\/status\/870524658184986625\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/Xv8woS1icK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBS5jmOVwAAaTSk.jpg",
        "id_str" : "870524649196601344",
        "id" : 870524649196601344,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBS5jmOVwAAaTSk.jpg",
        "sizes" : [ {
          "h" : 723,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 739,
          "resize" : "fit",
          "w" : 1227
        }, {
          "h" : 739,
          "resize" : "fit",
          "w" : 1227
        }, {
          "h" : 410,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Xv8woS1icK"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/MFAsg\/status\/870524658184986625\/photo\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/Xv8woS1icK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBS5jmKVoAAlEJV.jpg",
        "id_str" : "870524649179815936",
        "id" : 870524649179815936,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBS5jmKVoAAlEJV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 494,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 902,
          "resize" : "fit",
          "w" : 1241
        }, {
          "h" : 902,
          "resize" : "fit",
          "w" : 1241
        }, {
          "h" : 872,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Xv8woS1icK"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 0, 10 ]
      }, {
        "text" : "Paris",
        "indices" : [ 43, 49 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "870524658184986625",
    "text" : "#Singapore reaffirms her commitment to the #Paris Agreement. https:\/\/t.co\/Xv8woS1icK",
    "id" : 870524658184986625,
    "created_at" : "2017-06-02 06:17:06 +0000",
    "user" : {
      "name" : "MFAsg",
      "screen_name" : "MFAsg",
      "protected" : false,
      "id_str" : "77165249",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882800605684617217\/9ipor8FB_normal.jpg",
      "id" : 77165249,
      "verified" : true
    }
  },
  "id" : 870886711097688064,
  "created_at" : "2017-06-03 06:15:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "David Frum",
      "screen_name" : "davidfrum",
      "indices" : [ 3, 13 ],
      "id_str" : "18686907",
      "id" : 18686907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870728891387793408",
  "text" : "RT @davidfrum: But I prefer this one: There are more yoga instructors in the US than all coal employees, miners &amp; non-miners https:\/\/t.co\/B\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 114, 137 ],
        "url" : "https:\/\/t.co\/Bz4lMCXvOV",
        "expanded_url" : "http:\/\/www.prnewswire.com\/news-releases\/2016-yoga-in-america-study-conducted-by-yoga-journal-and-yoga-alliance-reveals-growth-and-benefits-of-the-practice-300203418.html",
        "display_url" : "prnewswire.com\/news-releases\/\u2026"
      }, {
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/vPUjZZlvC9",
        "expanded_url" : "https:\/\/twitter.com\/davidfrum\/status\/870648966995746816",
        "display_url" : "twitter.com\/davidfrum\/stat\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "870649230259519489",
    "text" : "But I prefer this one: There are more yoga instructors in the US than all coal employees, miners &amp; non-miners https:\/\/t.co\/Bz4lMCXvOV https:\/\/t.co\/vPUjZZlvC9",
    "id" : 870649230259519489,
    "created_at" : "2017-06-02 14:32:07 +0000",
    "user" : {
      "name" : "David Frum",
      "screen_name" : "davidfrum",
      "protected" : false,
      "id_str" : "18686907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930905542851252224\/dbfa8FlT_normal.jpg",
      "id" : 18686907,
      "verified" : true
    }
  },
  "id" : 870728891387793408,
  "created_at" : "2017-06-02 19:48:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "indices" : [ 3, 18 ],
      "id_str" : "1626294277",
      "id" : 1626294277
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/870226590025752577\/video\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/UCS5aHWDWL",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/870226242233974784\/pu\/img\/R2BaNj-mqTMMnwMm.jpg",
      "id_str" : "870226242233974784",
      "id" : 870226242233974784,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/870226242233974784\/pu\/img\/R2BaNj-mqTMMnwMm.jpg",
      "sizes" : [ {
        "h" : 404,
        "resize" : "fit",
        "w" : 718
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 718
      }, {
        "h" : 404,
        "resize" : "fit",
        "w" : 718
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/UCS5aHWDWL"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870683547790594050",
  "text" : "RT @spectatorindex: MEDIA: German soldier demonstrates stability of Leopard tank https:\/\/t.co\/UCS5aHWDWL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/870226590025752577\/video\/1",
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/UCS5aHWDWL",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/870226242233974784\/pu\/img\/R2BaNj-mqTMMnwMm.jpg",
        "id_str" : "870226242233974784",
        "id" : 870226242233974784,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/870226242233974784\/pu\/img\/R2BaNj-mqTMMnwMm.jpg",
        "sizes" : [ {
          "h" : 404,
          "resize" : "fit",
          "w" : 718
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 718
        }, {
          "h" : 404,
          "resize" : "fit",
          "w" : 718
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/UCS5aHWDWL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "870226590025752577",
    "text" : "MEDIA: German soldier demonstrates stability of Leopard tank https:\/\/t.co\/UCS5aHWDWL",
    "id" : 870226590025752577,
    "created_at" : "2017-06-01 10:32:41 +0000",
    "user" : {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "protected" : false,
      "id_str" : "1626294277",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839067030904922112\/LH4xqz-d_normal.jpg",
      "id" : 1626294277,
      "verified" : false
    }
  },
  "id" : 870683547790594050,
  "created_at" : "2017-06-02 16:48:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeffrey Snover",
      "screen_name" : "jsnover",
      "indices" : [ 3, 11 ],
      "id_str" : "18246129",
      "id" : 18246129
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870664394316943360",
  "text" : "RT @jsnover: PowerShell uses 98 well-defined verbs to strike a balance between easy to conceptualize and easy to remember https:\/\/t.co\/cFw0\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jsnover\/status\/870616882277003266\/photo\/1",
        "indices" : [ 109, 132 ],
        "url" : "https:\/\/t.co\/cFw07FYeYN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBUNbsaU0AA_YNp.jpg",
        "id_str" : "870616872395264000",
        "id" : 870616872395264000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBUNbsaU0AA_YNp.jpg",
        "sizes" : [ {
          "h" : 143,
          "resize" : "crop",
          "w" : 143
        }, {
          "h" : 143,
          "resize" : "fit",
          "w" : 214
        }, {
          "h" : 143,
          "resize" : "fit",
          "w" : 214
        }, {
          "h" : 143,
          "resize" : "fit",
          "w" : 214
        }, {
          "h" : 143,
          "resize" : "fit",
          "w" : 214
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/cFw07FYeYN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "870615999392866304",
    "geo" : { },
    "id_str" : "870616882277003266",
    "in_reply_to_user_id" : 18246129,
    "text" : "PowerShell uses 98 well-defined verbs to strike a balance between easy to conceptualize and easy to remember https:\/\/t.co\/cFw07FYeYN",
    "id" : 870616882277003266,
    "in_reply_to_status_id" : 870615999392866304,
    "created_at" : "2017-06-02 12:23:34 +0000",
    "in_reply_to_screen_name" : "jsnover",
    "in_reply_to_user_id_str" : "18246129",
    "user" : {
      "name" : "Jeffrey Snover",
      "screen_name" : "jsnover",
      "protected" : false,
      "id_str" : "18246129",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039650689620688896\/ZZgN5c9Y_normal.jpg",
      "id" : 18246129,
      "verified" : true
    }
  },
  "id" : 870664394316943360,
  "created_at" : "2017-06-02 15:32:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tore Groneng",
      "screen_name" : "ToreGroneng",
      "indices" : [ 3, 15 ],
      "id_str" : "384764261",
      "id" : 384764261
    }, {
      "name" : "Jeffrey Snover",
      "screen_name" : "jsnover",
      "indices" : [ 17, 25 ],
      "id_str" : "18246129",
      "id" : 18246129
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ToreGroneng\/status\/870630113393733635\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/mf5CxE59Gm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBUZdP0WsAAq0RT.jpg",
      "id_str" : "870630093219082240",
      "id" : 870630093219082240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBUZdP0WsAAq0RT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 657,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 711,
        "resize" : "fit",
        "w" : 736
      }, {
        "h" : 711,
        "resize" : "fit",
        "w" : 736
      }, {
        "h" : 711,
        "resize" : "fit",
        "w" : 736
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mf5CxE59Gm"
    } ],
    "hashtags" : [ {
      "text" : "PowerShell",
      "indices" : [ 85, 96 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870662909680128000",
  "text" : "RT @ToreGroneng: @jsnover Even before that, the Apollo program used verb-noun syntax #PowerShell https:\/\/t.co\/mf5CxE59Gm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jeffrey Snover",
        "screen_name" : "jsnover",
        "indices" : [ 0, 8 ],
        "id_str" : "18246129",
        "id" : 18246129
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ToreGroneng\/status\/870630113393733635\/photo\/1",
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/mf5CxE59Gm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBUZdP0WsAAq0RT.jpg",
        "id_str" : "870630093219082240",
        "id" : 870630093219082240,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBUZdP0WsAAq0RT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 657,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 711,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 711,
          "resize" : "fit",
          "w" : 736
        }, {
          "h" : 711,
          "resize" : "fit",
          "w" : 736
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mf5CxE59Gm"
      } ],
      "hashtags" : [ {
        "text" : "PowerShell",
        "indices" : [ 68, 79 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "870615999392866304",
    "geo" : { },
    "id_str" : "870630113393733635",
    "in_reply_to_user_id" : 18246129,
    "text" : "@jsnover Even before that, the Apollo program used verb-noun syntax #PowerShell https:\/\/t.co\/mf5CxE59Gm",
    "id" : 870630113393733635,
    "in_reply_to_status_id" : 870615999392866304,
    "created_at" : "2017-06-02 13:16:09 +0000",
    "in_reply_to_screen_name" : "jsnover",
    "in_reply_to_user_id_str" : "18246129",
    "user" : {
      "name" : "Tore Groneng",
      "screen_name" : "ToreGroneng",
      "protected" : false,
      "id_str" : "384764261",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2208965015\/tore_normal.jpg",
      "id" : 384764261,
      "verified" : false
    }
  },
  "id" : 870662909680128000,
  "created_at" : "2017-06-02 15:26:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "85053026",
      "id" : 85053026
    }, {
      "name" : "Iarnr\u00F3d \u00C9ireann",
      "screen_name" : "IrishRail",
      "indices" : [ 76, 86 ],
      "id_str" : "15115986",
      "id" : 15115986
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/yJoN44ifSc",
      "expanded_url" : "https:\/\/twitter.com\/rtenews\/status\/870593588983410688",
      "display_url" : "twitter.com\/rtenews\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870647088102690816",
  "text" : "RT @gavindbrown: The cognitive limitations of some drivers... see video via @IrishRail https:\/\/t.co\/yJoN44ifSc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Iarnr\u00F3d \u00C9ireann",
        "screen_name" : "IrishRail",
        "indices" : [ 59, 69 ],
        "id_str" : "15115986",
        "id" : 15115986
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/yJoN44ifSc",
        "expanded_url" : "https:\/\/twitter.com\/rtenews\/status\/870593588983410688",
        "display_url" : "twitter.com\/rtenews\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "870646744870268928",
    "text" : "The cognitive limitations of some drivers... see video via @IrishRail https:\/\/t.co\/yJoN44ifSc",
    "id" : 870646744870268928,
    "created_at" : "2017-06-02 14:22:14 +0000",
    "user" : {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "protected" : false,
      "id_str" : "85053026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844192952914231296\/mLoaFZ86_normal.jpg",
      "id" : 85053026,
      "verified" : false
    }
  },
  "id" : 870647088102690816,
  "created_at" : "2017-06-02 14:23:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870620908708384772",
  "text" : "Changed my password and did not realise Skype a\/c is linked to live.ie",
  "id" : 870620908708384772,
  "created_at" : "2017-06-02 12:39:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kacper staniul",
      "screen_name" : "kacperstaniul",
      "indices" : [ 3, 17 ],
      "id_str" : "3958250477",
      "id" : 3958250477
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/kacperstaniul\/status\/870602651859394562\/photo\/1",
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/azXmzo2gAx",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBUAfvjXYAEMjf7.jpg",
      "id_str" : "870602648306802689",
      "id" : 870602648306802689,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBUAfvjXYAEMjf7.jpg",
      "sizes" : [ {
        "h" : 361,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1596,
        "resize" : "fit",
        "w" : 3010
      }, {
        "h" : 636,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1086,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/azXmzo2gAx"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/Irga9O5Jpt",
      "expanded_url" : "http:\/\/bit.ly\/2qIfQpe",
      "display_url" : "bit.ly\/2qIfQpe"
    } ]
  },
  "geo" : { },
  "id_str" : "870619912091381760",
  "text" : "RT @kacperstaniul: Guesstimate the possible value of startup options https:\/\/t.co\/Irga9O5Jpt https:\/\/t.co\/azXmzo2gAx",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/kacperstaniul\/status\/870602651859394562\/photo\/1",
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/azXmzo2gAx",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBUAfvjXYAEMjf7.jpg",
        "id_str" : "870602648306802689",
        "id" : 870602648306802689,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBUAfvjXYAEMjf7.jpg",
        "sizes" : [ {
          "h" : 361,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1596,
          "resize" : "fit",
          "w" : 3010
        }, {
          "h" : 636,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1086,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/azXmzo2gAx"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/Irga9O5Jpt",
        "expanded_url" : "http:\/\/bit.ly\/2qIfQpe",
        "display_url" : "bit.ly\/2qIfQpe"
      } ]
    },
    "geo" : { },
    "id_str" : "870602651859394562",
    "text" : "Guesstimate the possible value of startup options https:\/\/t.co\/Irga9O5Jpt https:\/\/t.co\/azXmzo2gAx",
    "id" : 870602651859394562,
    "created_at" : "2017-06-02 11:27:01 +0000",
    "user" : {
      "name" : "kacper staniul",
      "screen_name" : "kacperstaniul",
      "protected" : false,
      "id_str" : "3958250477",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/818119209616109568\/zwkYOjBo_normal.jpg",
      "id" : 3958250477,
      "verified" : false
    }
  },
  "id" : 870619912091381760,
  "created_at" : "2017-06-02 12:35:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/870577854085464064\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/mmpTguODn2",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBTp7DkWAAAGDif.jpg",
      "id_str" : "870577828768645120",
      "id" : 870577828768645120,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBTp7DkWAAAGDif.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 540,
        "resize" : "fit",
        "w" : 769
      }, {
        "h" : 540,
        "resize" : "fit",
        "w" : 769
      }, {
        "h" : 478,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 540,
        "resize" : "fit",
        "w" : 769
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/mmpTguODn2"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870577854085464064",
  "text" : "Just realized the RStudio that I setup and run in the cloud 5 months ago is still up and running https:\/\/t.co\/mmpTguODn2",
  "id" : 870577854085464064,
  "created_at" : "2017-06-02 09:48:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emily Ross",
      "screen_name" : "emilyjaneross",
      "indices" : [ 3, 17 ],
      "id_str" : "18903353",
      "id" : 18903353
    }, {
      "name" : "SportsTech Ireland",
      "screen_name" : "SportsTechIRE",
      "indices" : [ 48, 62 ],
      "id_str" : "846493937934221312",
      "id" : 846493937934221312
    }, {
      "name" : "Martina Skelly",
      "screen_name" : "martina_skelly",
      "indices" : [ 75, 90 ],
      "id_str" : "19900274",
      "id" : 19900274
    }, {
      "name" : "Gr\u00E1inne Barry",
      "screen_name" : "grainnebbarry",
      "indices" : [ 95, 109 ],
      "id_str" : "226526723",
      "id" : 226526723
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870577020907991040",
  "text" : "RT @emilyjaneross: As one of the co-founders of @SportsTechIRE, (alongside @martina_skelly and @grainnebbarry) I'm SO EXCITED! Only two wee\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "SportsTech Ireland",
        "screen_name" : "SportsTechIRE",
        "indices" : [ 29, 43 ],
        "id_str" : "846493937934221312",
        "id" : 846493937934221312
      }, {
        "name" : "Martina Skelly",
        "screen_name" : "martina_skelly",
        "indices" : [ 56, 71 ],
        "id_str" : "19900274",
        "id" : 19900274
      }, {
        "name" : "Gr\u00E1inne Barry",
        "screen_name" : "grainnebbarry",
        "indices" : [ 76, 90 ],
        "id_str" : "226526723",
        "id" : 226526723
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 138, 161 ],
        "url" : "https:\/\/t.co\/SU4hDEN5z1",
        "expanded_url" : "https:\/\/twitter.com\/SportsTechIRE\/status\/870265209902034944",
        "display_url" : "twitter.com\/SportsTechIRE\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "870268381840146433",
    "text" : "As one of the co-founders of @SportsTechIRE, (alongside @martina_skelly and @grainnebbarry) I'm SO EXCITED! Only two weeks from launch :) https:\/\/t.co\/SU4hDEN5z1",
    "id" : 870268381840146433,
    "created_at" : "2017-06-01 13:18:45 +0000",
    "user" : {
      "name" : "Emily Ross",
      "screen_name" : "emilyjaneross",
      "protected" : false,
      "id_str" : "18903353",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/929856359687966720\/-8Vr-h7m_normal.jpg",
      "id" : 18903353,
      "verified" : false
    }
  },
  "id" : 870577020907991040,
  "created_at" : "2017-06-02 09:45:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/P03Xjx0sQS",
      "expanded_url" : "https:\/\/twitter.com\/sgsmesg\/status\/870576305707687937",
      "display_url" : "twitter.com\/sgsmesg\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870576640476295168",
  "text" : "\uD83D\uDE31 https:\/\/t.co\/P03Xjx0sQS",
  "id" : 870576640476295168,
  "created_at" : "2017-06-02 09:43:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870566495427887104",
  "text" : "Trying to get something done using college PC and network and my client website is ban.",
  "id" : 870566495427887104,
  "created_at" : "2017-06-02 09:03:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoogleBB",
      "indices" : [ 88, 97 ]
    } ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/ANlZ65GRtr",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/870284902171889666",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870535842942652416",
  "text" : "Do u put your localhost (development machine) traffic in the Referral Exclusions List ? #GoogleBB https:\/\/t.co\/ANlZ65GRtr",
  "id" : 870535842942652416,
  "created_at" : "2017-06-02 07:01:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoogleBB",
      "indices" : [ 0, 9 ]
    } ],
    "urls" : [ {
      "indices" : [ 10, 33 ],
      "url" : "https:\/\/t.co\/pr60rxMJgM",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BU0_YTiABff\/",
      "display_url" : "instagram.com\/p\/BU0_YTiABff\/"
    } ]
  },
  "geo" : { },
  "id_str" : "870535069219385344",
  "text" : "#GoogleBB https:\/\/t.co\/pr60rxMJgM",
  "id" : 870535069219385344,
  "created_at" : "2017-06-02 06:58:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/tO91e8zIS1",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BU0-EiqA1FR\/",
      "display_url" : "instagram.com\/p\/BU0-EiqA1FR\/"
    } ]
  },
  "geo" : { },
  "id_str" : "870532192375635969",
  "text" : "Good Morning @ Google European Headoffice - Dublin https:\/\/t.co\/tO91e8zIS1",
  "id" : 870532192375635969,
  "created_at" : "2017-06-02 06:47:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Willis Wee",
      "screen_name" : "williswee",
      "indices" : [ 3, 13 ],
      "id_str" : "22397966",
      "id" : 22397966
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/gqg0Iu7MXw",
      "expanded_url" : "http:\/\/ift.tt\/2qH5Po6",
      "display_url" : "ift.tt\/2qH5Po6"
    } ]
  },
  "geo" : { },
  "id_str" : "870523773677518848",
  "text" : "RT @williswee: A startup plots to trim the fat in the insurance industry https:\/\/t.co\/gqg0Iu7MXw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 58, 81 ],
        "url" : "https:\/\/t.co\/gqg0Iu7MXw",
        "expanded_url" : "http:\/\/ift.tt\/2qH5Po6",
        "display_url" : "ift.tt\/2qH5Po6"
      } ]
    },
    "geo" : { },
    "id_str" : "870496389087625217",
    "text" : "A startup plots to trim the fat in the insurance industry https:\/\/t.co\/gqg0Iu7MXw",
    "id" : 870496389087625217,
    "created_at" : "2017-06-02 04:24:46 +0000",
    "user" : {
      "name" : "Willis Wee",
      "screen_name" : "williswee",
      "protected" : false,
      "id_str" : "22397966",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/659171828\/ME_again2_normal.jpg",
      "id" : 22397966,
      "verified" : false
    }
  },
  "id" : 870523773677518848,
  "created_at" : "2017-06-02 06:13:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "futureofinnovation",
      "indices" : [ 87, 106 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870354087971311616",
  "text" : "RT @victoriabutt1: Some scientists would rather share their toothbrush than their data #futureofinnovation",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "futureofinnovation",
        "indices" : [ 68, 87 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "870349417114730496",
    "text" : "Some scientists would rather share their toothbrush than their data #futureofinnovation",
    "id" : 870349417114730496,
    "created_at" : "2017-06-01 18:40:46 +0000",
    "user" : {
      "name" : "Victoria Butt",
      "screen_name" : "victoriambutt",
      "protected" : false,
      "id_str" : "489422343",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972803230529998848\/DjePySYY_normal.jpg",
      "id" : 489422343,
      "verified" : false
    }
  },
  "id" : 870354087971311616,
  "created_at" : "2017-06-01 18:59:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/HAScjIOmPl",
      "expanded_url" : "https:\/\/support.google.com\/analytics\/answer\/2795830?hl=en-GB",
      "display_url" : "support.google.com\/analytics\/answ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870284902171889666",
  "text" : "Anyone knows how can I go about putting \"localhost:3000\" in the Referral Exclusions List? https:\/\/t.co\/HAScjIOmPl Thanks",
  "id" : 870284902171889666,
  "created_at" : "2017-06-01 14:24:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/JcExC9GAIF",
      "expanded_url" : "http:\/\/sc.mp\/CcPypi",
      "display_url" : "sc.mp\/CcPypi"
    } ]
  },
  "geo" : { },
  "id_str" : "870227714300665856",
  "text" : "Hong Kong tops Singapore as world\u2019s most competitive economy https:\/\/t.co\/JcExC9GAIF",
  "id" : 870227714300665856,
  "created_at" : "2017-06-01 10:37:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laura Crimmons",
      "screen_name" : "lauracrimmons",
      "indices" : [ 3, 17 ],
      "id_str" : "22129022",
      "id" : 22129022
    }, {
      "name" : "Liberal Democrats",
      "screen_name" : "LibDems",
      "indices" : [ 40, 48 ],
      "id_str" : "5680622",
      "id" : 5680622
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/lauracrimmons\/status\/870192081880403969\/photo\/1",
      "indices" : [ 49, 72 ],
      "url" : "https:\/\/t.co\/19rC2tWMbV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DBOK4h8W0AAn0eG.jpg",
      "id_str" : "870191856801468416",
      "id" : 870191856801468416,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBOK4h8W0AAn0eG.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1519,
        "resize" : "fit",
        "w" : 2423
      }, {
        "h" : 426,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 752,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1284,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/19rC2tWMbV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "870226600037687296",
  "text" : "RT @lauracrimmons: Well played 404 page @LibDems https:\/\/t.co\/19rC2tWMbV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Liberal Democrats",
        "screen_name" : "LibDems",
        "indices" : [ 21, 29 ],
        "id_str" : "5680622",
        "id" : 5680622
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lauracrimmons\/status\/870192081880403969\/photo\/1",
        "indices" : [ 30, 53 ],
        "url" : "https:\/\/t.co\/19rC2tWMbV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DBOK4h8W0AAn0eG.jpg",
        "id_str" : "870191856801468416",
        "id" : 870191856801468416,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DBOK4h8W0AAn0eG.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1519,
          "resize" : "fit",
          "w" : 2423
        }, {
          "h" : 426,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 752,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1284,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/19rC2tWMbV"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "870192081880403969",
    "text" : "Well played 404 page @LibDems https:\/\/t.co\/19rC2tWMbV",
    "id" : 870192081880403969,
    "created_at" : "2017-06-01 08:15:34 +0000",
    "user" : {
      "name" : "Laura Crimmons",
      "screen_name" : "lauracrimmons",
      "protected" : false,
      "id_str" : "22129022",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/811161767988658176\/BHYTiIXs_normal.jpg",
      "id" : 22129022,
      "verified" : false
    }
  },
  "id" : 870226600037687296,
  "created_at" : "2017-06-01 10:32:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "Bloomberg Technology",
      "screen_name" : "technology",
      "indices" : [ 105, 116 ],
      "id_str" : "21272440",
      "id" : 21272440
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/YfcTnTyyv9",
      "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2017-05-31\/china-s-road-to-world-tech-domination-begins-in-southeast-asia",
      "display_url" : "bloomberg.com\/news\/articles\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "870162609449971716",
  "text" : "RT @edwinksl: China's road to world tech domination begins in Southeast Asia https:\/\/t.co\/YfcTnTyyv9 via @technology",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bloomberg Technology",
        "screen_name" : "technology",
        "indices" : [ 91, 102 ],
        "id_str" : "21272440",
        "id" : 21272440
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 63, 86 ],
        "url" : "https:\/\/t.co\/YfcTnTyyv9",
        "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2017-05-31\/china-s-road-to-world-tech-domination-begins-in-southeast-asia",
        "display_url" : "bloomberg.com\/news\/articles\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "870099388227608577",
    "text" : "China's road to world tech domination begins in Southeast Asia https:\/\/t.co\/YfcTnTyyv9 via @technology",
    "id" : 870099388227608577,
    "created_at" : "2017-06-01 02:07:14 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 870162609449971716,
  "created_at" : "2017-06-01 06:18:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "indices" : [ 3, 14 ],
      "id_str" : "231474739",
      "id" : 231474739
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/xeBj19a6ZZ",
      "expanded_url" : "http:\/\/s.nikkei.com\/2rFvpOx",
      "display_url" : "s.nikkei.com\/2rFvpOx"
    } ]
  },
  "geo" : { },
  "id_str" : "870162219098656769",
  "text" : "RT @heliumlife: Japan begins licensing the sharing economy- Nikkei Asian Review https:\/\/t.co\/xeBj19a6ZZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 64, 87 ],
        "url" : "https:\/\/t.co\/xeBj19a6ZZ",
        "expanded_url" : "http:\/\/s.nikkei.com\/2rFvpOx",
        "display_url" : "s.nikkei.com\/2rFvpOx"
      } ]
    },
    "geo" : { },
    "id_str" : "870148816044826624",
    "text" : "Japan begins licensing the sharing economy- Nikkei Asian Review https:\/\/t.co\/xeBj19a6ZZ",
    "id" : 870148816044826624,
    "created_at" : "2017-06-01 05:23:38 +0000",
    "user" : {
      "name" : "\uD83D\uDE4B\uD83C\uDFFB Hui Yi Lee",
      "screen_name" : "heliumlife",
      "protected" : false,
      "id_str" : "231474739",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039125716262998018\/7LUaZasZ_normal.jpg",
      "id" : 231474739,
      "verified" : false
    }
  },
  "id" : 870162219098656769,
  "created_at" : "2017-06-01 06:16:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]