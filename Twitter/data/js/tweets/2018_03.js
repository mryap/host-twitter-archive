Grailbird.data.tweets_2018_03 = 
 [ {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Smith",
      "screen_name" : "DarkAndNerdy",
      "indices" : [ 3, 16 ],
      "id_str" : "5407522",
      "id" : 5407522
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "980176432134488065",
  "text" : "RT @DarkAndNerdy: Just saw a t-shirt. \u201CIf having a coffee in the morning doesn\u2019t wake you up, try deleting a table in a production database\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976986836525633536",
    "text" : "Just saw a t-shirt. \u201CIf having a coffee in the morning doesn\u2019t wake you up, try deleting a table in a production database instead\u201D",
    "id" : 976986836525633536,
    "created_at" : "2018-03-23 00:59:48 +0000",
    "user" : {
      "name" : "Jeff Smith",
      "screen_name" : "DarkAndNerdy",
      "protected" : false,
      "id_str" : "5407522",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1870330927\/421733_10150567223600658_706445657_9501844_409842126_n_normal.jpg",
      "id" : 5407522,
      "verified" : false
    }
  },
  "id" : 980176432134488065,
  "created_at" : "2018-03-31 20:14:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "von \"Von\" Traphaus",
      "screen_name" : "vonTraphaus",
      "indices" : [ 3, 15 ],
      "id_str" : "1316028764",
      "id" : 1316028764
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/vonTraphaus\/status\/980107196095594496\/photo\/1",
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/E6di9uZhrr",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZoKS9PU0AAV7v7.jpg",
      "id_str" : "980107189699268608",
      "id" : 980107189699268608,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZoKS9PU0AAV7v7.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 418,
        "resize" : "fit",
        "w" : 360
      }, {
        "h" : 418,
        "resize" : "fit",
        "w" : 360
      }, {
        "h" : 418,
        "resize" : "fit",
        "w" : 360
      }, {
        "h" : 418,
        "resize" : "fit",
        "w" : 360
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/E6di9uZhrr"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "980149587175493633",
  "text" : "RT @vonTraphaus: Happy Easter https:\/\/t.co\/E6di9uZhrr",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/vonTraphaus\/status\/980107196095594496\/photo\/1",
        "indices" : [ 13, 36 ],
        "url" : "https:\/\/t.co\/E6di9uZhrr",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZoKS9PU0AAV7v7.jpg",
        "id_str" : "980107189699268608",
        "id" : 980107189699268608,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZoKS9PU0AAV7v7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 360
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 360
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 360
        }, {
          "h" : 418,
          "resize" : "fit",
          "w" : 360
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/E6di9uZhrr"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "980107196095594496",
    "text" : "Happy Easter https:\/\/t.co\/E6di9uZhrr",
    "id" : 980107196095594496,
    "created_at" : "2018-03-31 15:38:59 +0000",
    "user" : {
      "name" : "von \"Von\" Traphaus",
      "screen_name" : "vonTraphaus",
      "protected" : false,
      "id_str" : "1316028764",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/984821942732509185\/Wsjo-ES7_normal.jpg",
      "id" : 1316028764,
      "verified" : false
    }
  },
  "id" : 980149587175493633,
  "created_at" : "2018-03-31 18:27:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/vTk06b5bGA",
      "expanded_url" : "http:\/\/str.sg\/oqjZ",
      "display_url" : "str.sg\/oqjZ"
    } ]
  },
  "geo" : { },
  "id_str" : "980140369080979456",
  "text" : "A Singaporean love story that broke racial and cultural barriers https:\/\/t.co\/vTk06b5bGA",
  "id" : 980140369080979456,
  "created_at" : "2018-03-31 17:50:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jameson Rohrer",
      "screen_name" : "Librarytech87",
      "indices" : [ 3, 17 ],
      "id_str" : "2683354057",
      "id" : 2683354057
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "980115601963343874",
  "text" : "RT @Librarytech87: Prison librarian here. I need your help in building up our collections. Comics, graphic novels, paperback fantasy, scien\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 255, 278 ],
        "url" : "https:\/\/t.co\/RKZX0EDaGF",
        "expanded_url" : "http:\/\/amzn.to\/2E2cCzz",
        "display_url" : "amzn.to\/2E2cCzz"
      } ]
    },
    "geo" : { },
    "id_str" : "980107725769080833",
    "text" : "Prison librarian here. I need your help in building up our collections. Comics, graphic novels, paperback fantasy, science fiction, historical fiction, suspense\/thriller\/mystery we can take! prison libraries need your help (Please share) amazon wish list https:\/\/t.co\/RKZX0EDaGF",
    "id" : 980107725769080833,
    "created_at" : "2018-03-31 15:41:06 +0000",
    "user" : {
      "name" : "Jameson Rohrer",
      "screen_name" : "Librarytech87",
      "protected" : false,
      "id_str" : "2683354057",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1010362983501815808\/o2KRT9C1_normal.jpg",
      "id" : 2683354057,
      "verified" : false
    }
  },
  "id" : 980115601963343874,
  "created_at" : "2018-03-31 16:12:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 238, 261 ],
      "url" : "https:\/\/t.co\/3xpx50lnL4",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/980082189781098497",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "980083303310155776",
  "text" : "You don't need to be in the tech space. Currently, partnering with an independent street food vendor working behind the scene from social media, going over regulatory requirements and setting up messaging tools to connect with customers. https:\/\/t.co\/3xpx50lnL4",
  "id" : 980083303310155776,
  "created_at" : "2018-03-31 14:04:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "980082189781098497",
  "text" : "Are you a solo founders, bootstrapped, set out to make money independently from customers not through an employer? Let collaborate and turn your bold idea into a successful product.",
  "id" : 980082189781098497,
  "created_at" : "2018-03-31 13:59:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/j0O5cEZ2D4",
      "expanded_url" : "https:\/\/www.indiehackers.com\/forum\/find-indie-hackers-in-your-area-343f07ab37",
      "display_url" : "indiehackers.com\/forum\/find-ind\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "980079657352679425",
  "text" : "I just replied to this thread and put Ireland down\nFind Indie Hackers in your area! https:\/\/t.co\/j0O5cEZ2D4",
  "id" : 980079657352679425,
  "created_at" : "2018-03-31 13:49:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ahmed Zahid",
      "screen_name" : "ahmedzahid",
      "indices" : [ 3, 14 ],
      "id_str" : "15330518",
      "id" : 15330518
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ahmedzahid\/status\/980067431287734272\/photo\/1",
      "indices" : [ 16, 39 ],
      "url" : "https:\/\/t.co\/H8BGxpLIlX",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZnl03-U8AAjTKV.jpg",
      "id_str" : "980067090471120896",
      "id" : 980067090471120896,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZnl03-U8AAjTKV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 700
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 700
      }, {
        "h" : 700,
        "resize" : "fit",
        "w" : 700
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/H8BGxpLIlX"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "980068965878829057",
  "text" : "RT @ahmedzahid: https:\/\/t.co\/H8BGxpLIlX",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ahmedzahid\/status\/980067431287734272\/photo\/1",
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/H8BGxpLIlX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZnl03-U8AAjTKV.jpg",
        "id_str" : "980067090471120896",
        "id" : 980067090471120896,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZnl03-U8AAjTKV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 700
        }, {
          "h" : 700,
          "resize" : "fit",
          "w" : 700
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/H8BGxpLIlX"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "980067431287734272",
    "text" : "https:\/\/t.co\/H8BGxpLIlX",
    "id" : 980067431287734272,
    "created_at" : "2018-03-31 13:00:59 +0000",
    "user" : {
      "name" : "Ahmed Zahid",
      "screen_name" : "ahmedzahid",
      "protected" : false,
      "id_str" : "15330518",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1876210557\/AhmedZahid_dp2_normal.jpg",
      "id" : 15330518,
      "verified" : false
    }
  },
  "id" : 980068965878829057,
  "created_at" : "2018-03-31 13:07:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "tonyofla",
      "screen_name" : "tonyofla",
      "indices" : [ 3, 12 ],
      "id_str" : "8252502",
      "id" : 8252502
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/fgQyS6CWS2",
      "expanded_url" : "https:\/\/www.independent.co.uk\/news\/world\/asia\/lost-camera-wash-up-taiwan-japan-park-lee-facebook-a8281721.html",
      "display_url" : "independent.co.uk\/news\/world\/asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "980065751188037632",
  "text" : "RT @tonyofla: Camera lost at sea for more than two years is found still working https:\/\/t.co\/fgQyS6CWS2",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/fgQyS6CWS2",
        "expanded_url" : "https:\/\/www.independent.co.uk\/news\/world\/asia\/lost-camera-wash-up-taiwan-japan-park-lee-facebook-a8281721.html",
        "display_url" : "independent.co.uk\/news\/world\/asi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "980064933286203392",
    "text" : "Camera lost at sea for more than two years is found still working https:\/\/t.co\/fgQyS6CWS2",
    "id" : 980064933286203392,
    "created_at" : "2018-03-31 12:51:03 +0000",
    "user" : {
      "name" : "tonyofla",
      "screen_name" : "tonyofla",
      "protected" : false,
      "id_str" : "8252502",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647693385881489409\/IODazz9V_normal.png",
      "id" : 8252502,
      "verified" : false
    }
  },
  "id" : 980065751188037632,
  "created_at" : "2018-03-31 12:54:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/aS34r58qpU",
      "expanded_url" : "https:\/\/twitter.com\/hireyap\/status\/980063266994032640",
      "display_url" : "twitter.com\/hireyap\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "980065126425473024",
  "text" : "If you can use Google Tag Manager, then you do not need to worry about gtag! https:\/\/t.co\/aS34r58qpU",
  "id" : 980065126425473024,
  "created_at" : "2018-03-31 12:51:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "c\u2113audia st\u0454\u2113\u2113\u2730r \u200F \u2702\uFE0F",
      "screen_name" : "ClaudiaStellar",
      "indices" : [ 3, 18 ],
      "id_str" : "357074010",
      "id" : 357074010
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MeToo",
      "indices" : [ 59, 65 ]
    }, {
      "text" : "RiceBunny",
      "indices" : [ 94, 104 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "980064635939368960",
  "text" : "RT @ClaudiaStellar: \"Chinese feminists found a way around [#MeToo censoring]\u2014they began using #RiceBunny in its place along with the rice b\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "MeToo",
        "indices" : [ 39, 45 ]
      }, {
        "text" : "RiceBunny",
        "indices" : [ 74, 84 ]
      } ],
      "urls" : [ {
        "indices" : [ 261, 284 ],
        "url" : "https:\/\/t.co\/cy9FTkHWzH",
        "expanded_url" : "https:\/\/www.wired.com\/story\/china-feminism-emoji-censorship\/",
        "display_url" : "wired.com\/story\/china-fe\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "979882045374640128",
    "text" : "\"Chinese feminists found a way around [#MeToo censoring]\u2014they began using #RiceBunny in its place along with the rice bowl &amp; bunny face emoji. When spoken aloud the words for 'rice bunny' are pronounced 'mi tu,' a homophone that cleverly evades detection.\"\nhttps:\/\/t.co\/cy9FTkHWzH",
    "id" : 979882045374640128,
    "created_at" : "2018-03-31 00:44:19 +0000",
    "user" : {
      "name" : "c\u2113audia st\u0454\u2113\u2113\u2730r \u200F \u2702\uFE0F",
      "screen_name" : "ClaudiaStellar",
      "protected" : false,
      "id_str" : "357074010",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039760159604658177\/_UxS7Ss4_normal.jpg",
      "id" : 357074010,
      "verified" : false
    }
  },
  "id" : 980064635939368960,
  "created_at" : "2018-03-31 12:49:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/979802674362306560\/photo\/1",
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/sSi71kA2z0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZj1THgW4AI-2eO.jpg",
      "id_str" : "979802627734167554",
      "id" : 979802627734167554,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZj1THgW4AI-2eO.jpg",
      "sizes" : [ {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sSi71kA2z0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979802674362306560",
  "text" : "Watching Riverdance on TG4 filled me with joy. https:\/\/t.co\/sSi71kA2z0",
  "id" : 979802674362306560,
  "created_at" : "2018-03-30 19:28:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dimitar Raykov \uD83E\uDD16",
      "screen_name" : "dreamture",
      "indices" : [ 3, 13 ],
      "id_str" : "176775729",
      "id" : 176775729
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/bSVFoGbZhR",
      "expanded_url" : "https:\/\/thenextweb.com\/science\/2018\/03\/27\/reseachers-develop-tiny-wearables-that-can-broadcast-information-about-what-you-imbibe\/",
      "display_url" : "thenextweb.com\/science\/2018\/0\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979736228085030914",
  "text" : "RT @dreamture: IoT = Internet of Teeth? \uD83D\uDE2Chttps:\/\/t.co\/bSVFoGbZhR",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/bSVFoGbZhR",
        "expanded_url" : "https:\/\/thenextweb.com\/science\/2018\/03\/27\/reseachers-develop-tiny-wearables-that-can-broadcast-information-about-what-you-imbibe\/",
        "display_url" : "thenextweb.com\/science\/2018\/0\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "979687568144044032",
    "text" : "IoT = Internet of Teeth? \uD83D\uDE2Chttps:\/\/t.co\/bSVFoGbZhR",
    "id" : 979687568144044032,
    "created_at" : "2018-03-30 11:51:32 +0000",
    "user" : {
      "name" : "Dimitar Raykov \uD83E\uDD16",
      "screen_name" : "dreamture",
      "protected" : false,
      "id_str" : "176775729",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/778845656953843712\/-s5O76EX_normal.jpg",
      "id" : 176775729,
      "verified" : false
    }
  },
  "id" : 979736228085030914,
  "created_at" : "2018-03-30 15:04:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 165, 188 ],
      "url" : "https:\/\/t.co\/MANnksIEbZ",
      "expanded_url" : "https:\/\/blog.docker.com\/2018\/03\/au-revoir\/",
      "display_url" : "blog.docker.com\/2018\/03\/au-rev\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979669163424780288",
  "text" : "\"When you create a company, your job is to make sure it can one day succeed without you. Then eventually that one day comes and the celebration can be bittersweet.\" https:\/\/t.co\/MANnksIEbZ",
  "id" : 979669163424780288,
  "created_at" : "2018-03-30 10:38:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Justinmind",
      "screen_name" : "just_in_mind",
      "indices" : [ 3, 16 ],
      "id_str" : "46068081",
      "id" : 46068081
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979666771320991744",
  "text" : "RT @just_in_mind: Want to be a UX winner? \n\nSimply follow us and retweet our tweet for a chance to win an annual Justinmind license. \n\nSo e\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/just_in_mind\/status\/978972474829283328\/photo\/1",
        "indices" : [ 270, 293 ],
        "url" : "https:\/\/t.co\/up5iVtOSfW",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZX90UuXcAUv5Og.jpg",
        "id_str" : "978967569381158917",
        "id" : 978967569381158917,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZX90UuXcAUv5Og.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 522
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 522
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 522
        }, {
          "h" : 352,
          "resize" : "fit",
          "w" : 522
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/up5iVtOSfW"
      } ],
      "hashtags" : [ {
        "text" : "justinmindmarchgiveaway",
        "indices" : [ 212, 236 ]
      }, {
        "text" : "UI",
        "indices" : [ 237, 240 ]
      }, {
        "text" : "UX",
        "indices" : [ 241, 244 ]
      }, {
        "text" : "competition",
        "indices" : [ 245, 257 ]
      }, {
        "text" : "freebie",
        "indices" : [ 258, 266 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978972474829283328",
    "text" : "Want to be a UX winner? \n\nSimply follow us and retweet our tweet for a chance to win an annual Justinmind license. \n\nSo easy you can do it with your eyes closed. \n\nWinner announced Monday April 2nd. Good luck! \n\n#justinmindmarchgiveaway #UI #UX #competition #freebie \uD83D\uDC23\uD83C\uDF81 https:\/\/t.co\/up5iVtOSfW",
    "id" : 978972474829283328,
    "created_at" : "2018-03-28 12:30:01 +0000",
    "user" : {
      "name" : "Justinmind",
      "screen_name" : "just_in_mind",
      "protected" : false,
      "id_str" : "46068081",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/900350843555336194\/P0C-an4u_normal.jpg",
      "id" : 46068081,
      "verified" : false
    }
  },
  "id" : 979666771320991744,
  "created_at" : "2018-03-30 10:28:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979663994276536322",
  "text" : "In other news, people are alarmed by 'preposterous' amount of data Google held on them.",
  "id" : 979663994276536322,
  "created_at" : "2018-03-30 10:17:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/979661025715871744\/photo\/1",
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/bJFm3mG5dd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZh0dPNW4AAb97h.jpg",
      "id_str" : "979660964600668160",
      "id" : 979660964600668160,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZh0dPNW4AAb97h.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/bJFm3mG5dd"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979661025715871744",
  "text" : "Social media can bring people together. Social Media allows small business to connect with their target audience. https:\/\/t.co\/bJFm3mG5dd",
  "id" : 979661025715871744,
  "created_at" : "2018-03-30 10:06:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andreeeeeeeeaaaaaa!",
      "screen_name" : "brandalisms",
      "indices" : [ 3, 15 ],
      "id_str" : "249318820",
      "id" : 249318820
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979658730299150336",
  "text" : "RT @brandalisms: Easter eggs my auntie has decorated. The process involves drawing the patterns on with wax (with a pinhead type tool that\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/brandalisms\/status\/979651465278115840\/photo\/1",
        "indices" : [ 275, 298 ],
        "url" : "https:\/\/t.co\/XyKjs5ea1i",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZhrzyCWsAABTIY.jpg",
        "id_str" : "979651456302231552",
        "id" : 979651456302231552,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZhrzyCWsAABTIY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XyKjs5ea1i"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/brandalisms\/status\/979651465278115840\/photo\/1",
        "indices" : [ 275, 298 ],
        "url" : "https:\/\/t.co\/XyKjs5ea1i",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZhrzyBX4AAysNX.jpg",
        "id_str" : "979651456298115072",
        "id" : 979651456298115072,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZhrzyBX4AAysNX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XyKjs5ea1i"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/brandalisms\/status\/979651465278115840\/photo\/1",
        "indices" : [ 275, 298 ],
        "url" : "https:\/\/t.co\/XyKjs5ea1i",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZhrzyFXcAEQVf7.jpg",
        "id_str" : "979651456314863617",
        "id" : 979651456314863617,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZhrzyFXcAEQVf7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XyKjs5ea1i"
      } ],
      "hashtags" : [ {
        "text" : "EasterWeekend2018",
        "indices" : [ 256, 274 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "979651465278115840",
    "text" : "Easter eggs my auntie has decorated. The process involves drawing the patterns on with wax (with a pinhead type tool that has a funnel that holds a tiny bit of wax), dipping the eggs into dye and then finally the wax is removed to reveal the design. Happy #EasterWeekend2018 https:\/\/t.co\/XyKjs5ea1i",
    "id" : 979651465278115840,
    "created_at" : "2018-03-30 09:28:05 +0000",
    "user" : {
      "name" : "Andreeeeeeeeaaaaaa!",
      "screen_name" : "brandalisms",
      "protected" : false,
      "id_str" : "249318820",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011698829190672385\/PynKXsQ__normal.jpg",
      "id" : 249318820,
      "verified" : false
    }
  },
  "id" : 979658730299150336,
  "created_at" : "2018-03-30 09:56:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "A Small Fiction",
      "screen_name" : "ASmallFiction",
      "indices" : [ 3, 17 ],
      "id_str" : "46769303",
      "id" : 46769303
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979645411907592193",
  "text" : "RT @ASmallFiction: They lived in castles in the sky.\n\n\"But we're not out of touch. We stay anchored,\" they said.\n\nAs their dropped anchors\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "979573165268484096",
    "text" : "They lived in castles in the sky.\n\n\"But we're not out of touch. We stay anchored,\" they said.\n\nAs their dropped anchors crushed those below.",
    "id" : 979573165268484096,
    "created_at" : "2018-03-30 04:16:56 +0000",
    "user" : {
      "name" : "A Small Fiction",
      "screen_name" : "ASmallFiction",
      "protected" : false,
      "id_str" : "46769303",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875394454449815552\/FAzOLgVh_normal.jpg",
      "id" : 46769303,
      "verified" : false
    }
  },
  "id" : 979645411907592193,
  "created_at" : "2018-03-30 09:04:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dina D. Pomeranz",
      "screen_name" : "DinaPomeranz",
      "indices" : [ 3, 16 ],
      "id_str" : "61016265",
      "id" : 61016265
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979636989111791621",
  "text" : "RT @DinaPomeranz: The richer a country, the fewer hours its inhabitants tend to work.\n\nHong Kong and Singapore are notable outliers.\n\nhttps\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DinaPomeranz\/status\/979388342033895425\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/fVhJ0y3DSS",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZd8f-vX4AAHTxI.jpg",
        "id_str" : "979388332835856384",
        "id" : 979388332835856384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZd8f-vX4AAHTxI.jpg",
        "sizes" : [ {
          "h" : 722,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 722,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 722,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/fVhJ0y3DSS"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 116, 139 ],
        "url" : "https:\/\/t.co\/uzWcd7QxM8",
        "expanded_url" : "https:\/\/ourworldindata.org\/working-hours",
        "display_url" : "ourworldindata.org\/working-hours"
      } ]
    },
    "geo" : { },
    "id_str" : "979388342033895425",
    "text" : "The richer a country, the fewer hours its inhabitants tend to work.\n\nHong Kong and Singapore are notable outliers.\n\nhttps:\/\/t.co\/uzWcd7QxM8 https:\/\/t.co\/fVhJ0y3DSS",
    "id" : 979388342033895425,
    "created_at" : "2018-03-29 16:02:31 +0000",
    "user" : {
      "name" : "Dina D. Pomeranz",
      "screen_name" : "DinaPomeranz",
      "protected" : false,
      "id_str" : "61016265",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/870528485827067904\/lbb8Za_Z_normal.jpg",
      "id" : 61016265,
      "verified" : false
    }
  },
  "id" : 979636989111791621,
  "created_at" : "2018-03-30 08:30:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979470917289037824",
  "text" : "Impressed by  some online biz directories value proposition. Email asking for  their monthly traffic on their website.  *crickets chirp*",
  "id" : 979470917289037824,
  "created_at" : "2018-03-29 21:30:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "jonny sun",
      "screen_name" : "jonnysun",
      "indices" : [ 3, 12 ],
      "id_str" : "26259576",
      "id" : 26259576
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979387055066304512",
  "text" : "RT @jonnysun: JOB INTERVIEWER: can you explain this gap in your resume\nME: yes its 7pts tall, separates two sections in a visually pleasing\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978715316367843328",
    "text" : "JOB INTERVIEWER: can you explain this gap in your resume\nME: yes its 7pts tall, separates two sections in a visually pleasing way, and aligns to a carefully proportioned grid\nINTERVIEWER: no, i mean here where it says you didn't work for two years\nME: i.. was designing my resume",
    "id" : 978715316367843328,
    "created_at" : "2018-03-27 19:28:09 +0000",
    "user" : {
      "name" : "jonny sun",
      "screen_name" : "jonnysun",
      "protected" : false,
      "id_str" : "26259576",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/932660941124243456\/cBQXIFUb_normal.jpg",
      "id" : 26259576,
      "verified" : true
    }
  },
  "id" : 979387055066304512,
  "created_at" : "2018-03-29 15:57:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "inhabitat",
      "screen_name" : "inhabitat",
      "indices" : [ 3, 13 ],
      "id_str" : "14150661",
      "id" : 14150661
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/inhabitat\/status\/979344076754509824\/photo\/1",
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/GXfriuxlwG",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZdUOwsVwAEmWsp.jpg",
      "id_str" : "979344056542150657",
      "id" : 979344056542150657,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZdUOwsVwAEmWsp.jpg",
      "sizes" : [ {
        "h" : 499,
        "resize" : "fit",
        "w" : 889
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 499,
        "resize" : "fit",
        "w" : 889
      }, {
        "h" : 499,
        "resize" : "fit",
        "w" : 889
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GXfriuxlwG"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/prhb4IeS5W",
      "expanded_url" : "https:\/\/inhabitat.com\/how-floating-solar-panels-are-helping-the-maldives-ditch-diesel-fuel\/",
      "display_url" : "inhabitat.com\/how-floating-s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979386441359818753",
  "text" : "RT @inhabitat: How floating solar panels are helping the Maldives ditch diesel fuel\nhttps:\/\/t.co\/prhb4IeS5W https:\/\/t.co\/GXfriuxlwG",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/inhabitat\/status\/979344076754509824\/photo\/1",
        "indices" : [ 93, 116 ],
        "url" : "https:\/\/t.co\/GXfriuxlwG",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZdUOwsVwAEmWsp.jpg",
        "id_str" : "979344056542150657",
        "id" : 979344056542150657,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZdUOwsVwAEmWsp.jpg",
        "sizes" : [ {
          "h" : 499,
          "resize" : "fit",
          "w" : 889
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 382,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 499,
          "resize" : "fit",
          "w" : 889
        }, {
          "h" : 499,
          "resize" : "fit",
          "w" : 889
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GXfriuxlwG"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 69, 92 ],
        "url" : "https:\/\/t.co\/prhb4IeS5W",
        "expanded_url" : "https:\/\/inhabitat.com\/how-floating-solar-panels-are-helping-the-maldives-ditch-diesel-fuel\/",
        "display_url" : "inhabitat.com\/how-floating-s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "979344076754509824",
    "text" : "How floating solar panels are helping the Maldives ditch diesel fuel\nhttps:\/\/t.co\/prhb4IeS5W https:\/\/t.co\/GXfriuxlwG",
    "id" : 979344076754509824,
    "created_at" : "2018-03-29 13:06:37 +0000",
    "user" : {
      "name" : "inhabitat",
      "screen_name" : "inhabitat",
      "protected" : false,
      "id_str" : "14150661",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/51865670\/OwliMcOwl_normal.jpg",
      "id" : 14150661,
      "verified" : false
    }
  },
  "id" : 979386441359818753,
  "created_at" : "2018-03-29 15:54:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Guardian",
      "screen_name" : "guardian",
      "indices" : [ 3, 12 ],
      "id_str" : "87818409",
      "id" : 87818409
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/xDEF58YYZd",
      "expanded_url" : "https:\/\/trib.al\/zCxOcnH",
      "display_url" : "trib.al\/zCxOcnH"
    } ]
  },
  "geo" : { },
  "id_str" : "979371117457616896",
  "text" : "RT @guardian: Amazon snaps up Bake Off sponsorship in biggest UK TV deal https:\/\/t.co\/xDEF58YYZd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/xDEF58YYZd",
        "expanded_url" : "https:\/\/trib.al\/zCxOcnH",
        "display_url" : "trib.al\/zCxOcnH"
      } ]
    },
    "geo" : { },
    "id_str" : "979242491424788480",
    "text" : "Amazon snaps up Bake Off sponsorship in biggest UK TV deal https:\/\/t.co\/xDEF58YYZd",
    "id" : 979242491424788480,
    "created_at" : "2018-03-29 06:22:58 +0000",
    "user" : {
      "name" : "The Guardian",
      "screen_name" : "guardian",
      "protected" : false,
      "id_str" : "87818409",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/952866338187423744\/0hj7a-EH_normal.jpg",
      "id" : 87818409,
      "verified" : true
    }
  },
  "id" : 979371117457616896,
  "created_at" : "2018-03-29 14:54:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    }, {
      "name" : "Levenshulme Market",
      "screen_name" : "levymarket",
      "indices" : [ 41, 52 ],
      "id_str" : "531410380",
      "id" : 531410380
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979363171965300736",
  "text" : "RT @jackiepenangkit: Dear folks, the UoM @levymarket event on Tues 10th April will now be held on Thurs 12th April, 11.30-2.30. Thank You.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Levenshulme Market",
        "screen_name" : "levymarket",
        "indices" : [ 20, 31 ],
        "id_str" : "531410380",
        "id" : 531410380
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "jackiepenangkitchen",
        "indices" : [ 118, 138 ]
      } ],
      "urls" : [ {
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/jwJHSsGJVI",
        "expanded_url" : "https:\/\/twitter.com\/jackiepenangkit\/status\/977932909532368901",
        "display_url" : "twitter.com\/jackiepenangki\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "979361709382144000",
    "text" : "Dear folks, the UoM @levymarket event on Tues 10th April will now be held on Thurs 12th April, 11.30-2.30. Thank You. #jackiepenangkitchen https:\/\/t.co\/jwJHSsGJVI",
    "id" : 979361709382144000,
    "created_at" : "2018-03-29 14:16:41 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 979363171965300736,
  "created_at" : "2018-03-29 14:22:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Gothelf",
      "screen_name" : "jboogie",
      "indices" : [ 3, 11 ],
      "id_str" : "9384812",
      "id" : 9384812
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979363089622749189",
  "text" : "RT @jboogie: As you sit down to write your measurable goals, okr's, kpi's, metrics etc, rememeber this: \n\n\"simple\" and \"easy\" are features\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "979324683945377792",
    "text" : "As you sit down to write your measurable goals, okr's, kpi's, metrics etc, rememeber this: \n\n\"simple\" and \"easy\" are features (output). They are not metrics.",
    "id" : 979324683945377792,
    "created_at" : "2018-03-29 11:49:34 +0000",
    "user" : {
      "name" : "Jeff Gothelf",
      "screen_name" : "jboogie",
      "protected" : false,
      "id_str" : "9384812",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038705935471063040\/723yLaBX_normal.jpg",
      "id" : 9384812,
      "verified" : true
    }
  },
  "id" : 979363089622749189,
  "created_at" : "2018-03-29 14:22:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 172, 195 ],
      "url" : "https:\/\/t.co\/VXPRbLFC2s",
      "expanded_url" : "https:\/\/zapier.com\/blog\/what-are-webhooks\/",
      "display_url" : "zapier.com\/blog\/what-are-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979351538861772800",
  "text" : "How PayPal tells your accounting app when your clients pay you, how Twilio routes phone calls to your number, and how WooCommerce can notify you about new orders in Slack. https:\/\/t.co\/VXPRbLFC2s",
  "id" : 979351538861772800,
  "created_at" : "2018-03-29 13:36:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/MKOIhQ3iqb",
      "expanded_url" : "https:\/\/cna.asia\/2E1kGjf",
      "display_url" : "cna.asia\/2E1kGjf"
    } ]
  },
  "geo" : { },
  "id_str" : "979334429393997824",
  "text" : "RT @ChannelNewsAsia: Rights lawyer Amal Clooney to represent Reuters reporters held in Myanmar https:\/\/t.co\/MKOIhQ3iqb https:\/\/t.co\/qmXaiZU\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/979332372221984769\/photo\/1",
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/qmXaiZUXjF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZdJmECVwAAtgw4.jpg",
        "id_str" : "979332362243784704",
        "id" : 979332362243784704,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZdJmECVwAAtgw4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/qmXaiZUXjF"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/MKOIhQ3iqb",
        "expanded_url" : "https:\/\/cna.asia\/2E1kGjf",
        "display_url" : "cna.asia\/2E1kGjf"
      } ]
    },
    "geo" : { },
    "id_str" : "979332372221984769",
    "text" : "Rights lawyer Amal Clooney to represent Reuters reporters held in Myanmar https:\/\/t.co\/MKOIhQ3iqb https:\/\/t.co\/qmXaiZUXjF",
    "id" : 979332372221984769,
    "created_at" : "2018-03-29 12:20:07 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 979334429393997824,
  "created_at" : "2018-03-29 12:28:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Olivia Solon",
      "screen_name" : "oliviasolon",
      "indices" : [ 3, 15 ],
      "id_str" : "137703483",
      "id" : 137703483
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/oliviasolon\/status\/978875614525575168\/photo\/1",
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/tTJwv9GpdU",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZWqK9yW0AELWzr.jpg",
      "id_str" : "978875599384137729",
      "id" : 978875599384137729,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZWqK9yW0AELWzr.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1926,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 1129,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 2502,
        "resize" : "fit",
        "w" : 2660
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/tTJwv9GpdU"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979333675279110144",
  "text" : "RT @oliviasolon: My husband\u2019s ingenious idea: a screensaver for cutting back on cellphone use https:\/\/t.co\/tTJwv9GpdU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/oliviasolon\/status\/978875614525575168\/photo\/1",
        "indices" : [ 77, 100 ],
        "url" : "https:\/\/t.co\/tTJwv9GpdU",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZWqK9yW0AELWzr.jpg",
        "id_str" : "978875599384137729",
        "id" : 978875599384137729,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZWqK9yW0AELWzr.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1926,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1129,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2502,
          "resize" : "fit",
          "w" : 2660
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tTJwv9GpdU"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978875614525575168",
    "text" : "My husband\u2019s ingenious idea: a screensaver for cutting back on cellphone use https:\/\/t.co\/tTJwv9GpdU",
    "id" : 978875614525575168,
    "created_at" : "2018-03-28 06:05:07 +0000",
    "user" : {
      "name" : "Olivia Solon",
      "screen_name" : "oliviasolon",
      "protected" : false,
      "id_str" : "137703483",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/707382834827120640\/R-Eb9YZB_normal.jpg",
      "id" : 137703483,
      "verified" : true
    }
  },
  "id" : 979333675279110144,
  "created_at" : "2018-03-29 12:25:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maria McHale",
      "screen_name" : "mchale_maria",
      "indices" : [ 0, 13 ],
      "id_str" : "3176753950",
      "id" : 3176753950
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "979328194112344065",
  "geo" : { },
  "id_str" : "979333214643916800",
  "in_reply_to_user_id" : 3176753950,
  "text" : "@mchale_maria Someone has to do it. :)",
  "id" : 979333214643916800,
  "in_reply_to_status_id" : 979328194112344065,
  "created_at" : "2018-03-29 12:23:28 +0000",
  "in_reply_to_screen_name" : "mchale_maria",
  "in_reply_to_user_id_str" : "3176753950",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979327123826626561",
  "text" : "RT @ronanlyons: A short thread about the prominent rape trial that concluded in Belfast yesterday. In particular, given some of the reactio\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IBelieveHer",
        "indices" : [ 263, 275 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "979306341004206081",
    "text" : "A short thread about the prominent rape trial that concluded in Belfast yesterday. In particular, given some of the reactions (not just those in support of the defendants but also by some leading media), I think it is important to understand what the jury found. #IBelieveHer",
    "id" : 979306341004206081,
    "created_at" : "2018-03-29 10:36:40 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 979327123826626561,
  "created_at" : "2018-03-29 11:59:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/7qTlyBnHzY",
      "expanded_url" : "https:\/\/twitter.com\/sineadredmond\/status\/978977925033857024",
      "display_url" : "twitter.com\/sineadredmond\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979322956462641152",
  "text" : "I find this obnoxious. They are cheering like this is some kind of sporting event. https:\/\/t.co\/7qTlyBnHzY",
  "id" : 979322956462641152,
  "created_at" : "2018-03-29 11:42:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "USA TODAY",
      "screen_name" : "USATODAY",
      "indices" : [ 93, 102 ],
      "id_str" : "15754281",
      "id" : 15754281
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/9zNcrPDw9w",
      "expanded_url" : "https:\/\/usat.ly\/2Gf2b0C",
      "display_url" : "usat.ly\/2Gf2b0C"
    } ]
  },
  "geo" : { },
  "id_str" : "979320201761230848",
  "text" : "'New airplane smell': Aboard a 24-hour Singapore delivery flight https:\/\/t.co\/9zNcrPDw9w via @usatoday",
  "id" : 979320201761230848,
  "created_at" : "2018-03-29 11:31:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Audrey Ellard Walsh",
      "screen_name" : "AudreyEWalsh",
      "indices" : [ 3, 16 ],
      "id_str" : "313256069",
      "id" : 313256069
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/AudreyEWalsh\/status\/979315713218433024\/photo\/1",
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/RGRsep17f1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZc6V3PVAAAhdSC.jpg",
      "id_str" : "979315591256276992",
      "id" : 979315591256276992,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZc6V3PVAAAhdSC.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 1024,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/RGRsep17f1"
    } ],
    "hashtags" : [ {
      "text" : "ibelieveher",
      "indices" : [ 34, 46 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979317404894466049",
  "text" : "RT @AudreyEWalsh: We believe her. #ibelieveher https:\/\/t.co\/RGRsep17f1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AudreyEWalsh\/status\/979315713218433024\/photo\/1",
        "indices" : [ 29, 52 ],
        "url" : "https:\/\/t.co\/RGRsep17f1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZc6V3PVAAAhdSC.jpg",
        "id_str" : "979315591256276992",
        "id" : 979315591256276992,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZc6V3PVAAAhdSC.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RGRsep17f1"
      } ],
      "hashtags" : [ {
        "text" : "ibelieveher",
        "indices" : [ 16, 28 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "979315713218433024",
    "text" : "We believe her. #ibelieveher https:\/\/t.co\/RGRsep17f1",
    "id" : 979315713218433024,
    "created_at" : "2018-03-29 11:13:55 +0000",
    "user" : {
      "name" : "Audrey Ellard Walsh",
      "screen_name" : "AudreyEWalsh",
      "protected" : false,
      "id_str" : "313256069",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/849529025634684929\/l_jfOCz5_normal.jpg",
      "id" : 313256069,
      "verified" : false
    }
  },
  "id" : 979317404894466049,
  "created_at" : "2018-03-29 11:20:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "indices" : [ 3, 14 ],
      "id_str" : "4747144932",
      "id" : 4747144932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/jmf0U6XQSY",
      "expanded_url" : "https:\/\/twitter.com\/jonrussell\/status\/979218309403963392",
      "display_url" : "twitter.com\/jonrussell\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979317275256938496",
  "text" : "RT @CaroBowler: Got to love the underdog. https:\/\/t.co\/jmf0U6XQSY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/jmf0U6XQSY",
        "expanded_url" : "https:\/\/twitter.com\/jonrussell\/status\/979218309403963392",
        "display_url" : "twitter.com\/jonrussell\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "979254082635415552",
    "text" : "Got to love the underdog. https:\/\/t.co\/jmf0U6XQSY",
    "id" : 979254082635415552,
    "created_at" : "2018-03-29 07:09:01 +0000",
    "user" : {
      "name" : "Caroline Bowler",
      "screen_name" : "CaroBowler",
      "protected" : false,
      "id_str" : "4747144932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686824073884643329\/U-3rp4zx_normal.jpg",
      "id" : 4747144932,
      "verified" : false
    }
  },
  "id" : 979317275256938496,
  "created_at" : "2018-03-29 11:20:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 0, 12 ],
      "id_str" : "168148402",
      "id" : 168148402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "979315509723217920",
  "geo" : { },
  "id_str" : "979316537990565890",
  "in_reply_to_user_id" : 168148402,
  "text" : "@m4riannelee Prefer to pay by cash. \uD83D\uDE03",
  "id" : 979316537990565890,
  "in_reply_to_status_id" : 979315509723217920,
  "created_at" : "2018-03-29 11:17:12 +0000",
  "in_reply_to_screen_name" : "m4riannelee",
  "in_reply_to_user_id_str" : "168148402",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "indices" : [ 3, 14 ],
      "id_str" : "15084853",
      "id" : 15084853
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/wlH8VPJ1lU",
      "expanded_url" : "https:\/\/www.irishtimes.com\/news\/crime-and-law\/courts\/district-court\/how-rape-trials-in-republic-differ-from-those-in-north-1.3443644?utm_source=dlvr.it&utm_medium=twitter",
      "display_url" : "irishtimes.com\/news\/crime-and\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979257513865633794",
  "text" : "RT @IrishTimes: How rape trials in Republic differ from those in North https:\/\/t.co\/wlH8VPJ1lU",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/wlH8VPJ1lU",
        "expanded_url" : "https:\/\/www.irishtimes.com\/news\/crime-and-law\/courts\/district-court\/how-rape-trials-in-republic-differ-from-those-in-north-1.3443644?utm_source=dlvr.it&utm_medium=twitter",
        "display_url" : "irishtimes.com\/news\/crime-and\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "979234943611027457",
    "text" : "How rape trials in Republic differ from those in North https:\/\/t.co\/wlH8VPJ1lU",
    "id" : 979234943611027457,
    "created_at" : "2018-03-29 05:52:58 +0000",
    "user" : {
      "name" : "The Irish Times",
      "screen_name" : "IrishTimes",
      "protected" : false,
      "id_str" : "15084853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2944517635\/5492cf569e6b0b1a9326a7d388009b60_normal.png",
      "id" : 15084853,
      "verified" : true
    }
  },
  "id" : 979257513865633794,
  "created_at" : "2018-03-29 07:22:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/GHlfwbToOh",
      "expanded_url" : "https:\/\/github.com\/docker\/hub-feedback\/issues\/1098#issuecomment-316309768",
      "display_url" : "github.com\/docker\/hub-fee\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "979022828203847681",
  "geo" : { },
  "id_str" : "979024058649063427",
  "in_reply_to_user_id" : 9465632,
  "text" : "So this $ docker logout solve the problem in my case https:\/\/t.co\/GHlfwbToOh",
  "id" : 979024058649063427,
  "in_reply_to_status_id" : 979022828203847681,
  "created_at" : "2018-03-28 15:54:59 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/979022828203847681\/photo\/1",
      "indices" : [ 64, 87 ],
      "url" : "https:\/\/t.co\/9sK942QO1o",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZYv20IWsAA-_y7.jpg",
      "id_str" : "979022587752787968",
      "id" : 979022587752787968,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZYv20IWsAA-_y7.jpg",
      "sizes" : [ {
        "h" : 206,
        "resize" : "fit",
        "w" : 1026
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 206,
        "resize" : "fit",
        "w" : 1026
      }, {
        "h" : 206,
        "resize" : "fit",
        "w" : 1026
      }, {
        "h" : 137,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9sK942QO1o"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "979022828203847681",
  "text" : "That \"Error unauthorized: incorrect username or password\" again https:\/\/t.co\/9sK942QO1o",
  "id" : 979022828203847681,
  "created_at" : "2018-03-28 15:50:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/iYVSN0Sqtb",
      "expanded_url" : "https:\/\/newsroom.fb.com\/news\/2017\/12\/introducing-messenger-kids-a-new-app-for-families-to-connect\/",
      "display_url" : "newsroom.fb.com\/news\/2017\/12\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "979021970154106882",
  "text" : "What do you all think of this? https:\/\/t.co\/iYVSN0Sqtb",
  "id" : 979021970154106882,
  "created_at" : "2018-03-28 15:46:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hi, Moms! With Ken",
      "screen_name" : "kengreller",
      "indices" : [ 3, 14 ],
      "id_str" : "21613813",
      "id" : 21613813
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978999561397129216",
  "text" : "RT @kengreller: Just saw Natalie Portman at a coffee shop and she has a cracked iPhone - feeling VERY grounded.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977975623170732032",
    "text" : "Just saw Natalie Portman at a coffee shop and she has a cracked iPhone - feeling VERY grounded.",
    "id" : 977975623170732032,
    "created_at" : "2018-03-25 18:28:53 +0000",
    "user" : {
      "name" : "Hi, Moms! With Ken",
      "screen_name" : "kengreller",
      "protected" : false,
      "id_str" : "21613813",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1008406006697783296\/YPOWSdO8_normal.jpg",
      "id" : 21613813,
      "verified" : false
    }
  },
  "id" : 978999561397129216,
  "created_at" : "2018-03-28 14:17:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah McInerney",
      "screen_name" : "SarahAMcInerney",
      "indices" : [ 3, 19 ],
      "id_str" : "45107252",
      "id" : 45107252
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IBelieveHer",
      "indices" : [ 25, 37 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978999247101202432",
  "text" : "RT @SarahAMcInerney: The #IBelieveHer hashtag gives an insight into how deeply upsetting this court case was for so many people. If nothing\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "IBelieveHer",
        "indices" : [ 4, 16 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978962174113714177",
    "text" : "The #IBelieveHer hashtag gives an insight into how deeply upsetting this court case was for so many people. If nothing else, it must surely lead to a change in how rape trials are conducted. Is there any other crime in which the alleged victim appears to be the one on trial?",
    "id" : 978962174113714177,
    "created_at" : "2018-03-28 11:49:05 +0000",
    "user" : {
      "name" : "Sarah McInerney",
      "screen_name" : "SarahAMcInerney",
      "protected" : false,
      "id_str" : "45107252",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/972397562996117504\/NGnvyrEP_normal.jpg",
      "id" : 45107252,
      "verified" : true
    }
  },
  "id" : 978999247101202432,
  "created_at" : "2018-03-28 14:16:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeremy Dwyer-Lindgren",
      "screen_name" : "photoJDL",
      "indices" : [ 3, 12 ],
      "id_str" : "1192182541",
      "id" : 1192182541
    }, {
      "name" : "Singapore Airlines",
      "screen_name" : "SingaporeAir",
      "indices" : [ 52, 65 ],
      "id_str" : "253340062",
      "id" : 253340062
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978976228790165505",
  "text" : "RT @photoJDL: Felicia, a cabin crew member on board @SingaporeAir #787 -10 delivery flight, takes a few minutes to watch as the northern li\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Singapore Airlines",
        "screen_name" : "SingaporeAir",
        "indices" : [ 38, 51 ],
        "id_str" : "253340062",
        "id" : 253340062
      }, {
        "name" : "Ben Mutzabaugh",
        "screen_name" : "TodayInTheSky",
        "indices" : [ 161, 175 ],
        "id_str" : "15811202",
        "id" : 15811202
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/photoJDL\/status\/978552081706450944\/photo\/1",
        "indices" : [ 176, 199 ],
        "url" : "https:\/\/t.co\/2TKJZzBKJA",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZSDT2yVwAEGiJA.jpg",
        "id_str" : "978551396193648641",
        "id" : 978551396193648641,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZSDT2yVwAEGiJA.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/2TKJZzBKJA"
      } ],
      "hashtags" : [ {
        "text" : "avgeek",
        "indices" : [ 153, 160 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978552081706450944",
    "text" : "Felicia, a cabin crew member on board @SingaporeAir #787 -10 delivery flight, takes a few minutes to watch as the northern lights dance through the sky. #avgeek @TodayInTheSky https:\/\/t.co\/2TKJZzBKJA",
    "id" : 978552081706450944,
    "created_at" : "2018-03-27 08:39:31 +0000",
    "user" : {
      "name" : "Jeremy Dwyer-Lindgren",
      "screen_name" : "photoJDL",
      "protected" : false,
      "id_str" : "1192182541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/501208276722135041\/KSR-5r9K_normal.jpeg",
      "id" : 1192182541,
      "verified" : false
    }
  },
  "id" : 978976228790165505,
  "created_at" : "2018-03-28 12:44:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Wenzhe Shi \uD83D\uDC15\uD83D\uDC08\uD83D\uDC0E",
      "screen_name" : "trustswz",
      "indices" : [ 3, 12 ],
      "id_str" : "211137580",
      "id" : 211137580
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978963273105264640",
  "text" : "RT @trustswz: We are hiring machine learning researcher in London. Join Twitter London\u2019s Machine Learning community (Including Magic Pony)\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/trustswz\/status\/978945063018553344\/photo\/1",
        "indices" : [ 213, 236 ],
        "url" : "https:\/\/t.co\/sqwYIx0aeo",
        "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DZXpUQNWkAA6Iq-.jpg",
        "id_str" : "978945028180578304",
        "id" : 978945028180578304,
        "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DZXpUQNWkAA6Iq-.jpg",
        "sizes" : [ {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 270,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sqwYIx0aeo"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 189, 212 ],
        "url" : "https:\/\/t.co\/2vPUnz6nsv",
        "expanded_url" : "https:\/\/careers.twitter.com\/en\/work-for-twitter\/201803\/machine-learning-researcher-applied-machine-learning-modeling.html",
        "display_url" : "careers.twitter.com\/en\/work-for-tw\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "978945063018553344",
    "text" : "We are hiring machine learning researcher in London. Join Twitter London\u2019s Machine Learning community (Including Magic Pony) and help to transform the way Twitter applies Machine Learning. https:\/\/t.co\/2vPUnz6nsv https:\/\/t.co\/sqwYIx0aeo",
    "id" : 978945063018553344,
    "created_at" : "2018-03-28 10:41:05 +0000",
    "user" : {
      "name" : "Wenzhe Shi \uD83D\uDC15\uD83D\uDC08\uD83D\uDC0E",
      "screen_name" : "trustswz",
      "protected" : false,
      "id_str" : "211137580",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/744242445114871809\/nhGfv3t__normal.jpg",
      "id" : 211137580,
      "verified" : false
    }
  },
  "id" : 978963273105264640,
  "created_at" : "2018-03-28 11:53:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/GDvZ2aWwve",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/av\/world-asia-china-43540148\/anthony-wong-hong-kong-actor-finds-brothers-after-bbc-report",
      "display_url" : "bbc.com\/news\/av\/world-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978781934884147200",
  "text" : "Anthony Wong \u9EC3\u79CB\u751F a well regarded Actor in Asia. Used to watch every of his movies back home. \nhttps:\/\/t.co\/GDvZ2aWwve",
  "id" : 978781934884147200,
  "created_at" : "2018-03-27 23:52:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bill Bishop",
      "screen_name" : "niubi",
      "indices" : [ 3, 9 ],
      "id_str" : "3512101",
      "id" : 3512101
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/niubi\/status\/978778708168400897\/photo\/1",
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/W0aaYbhXAZ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZVSC36WkAAGjxP.jpg",
      "id_str" : "978778703344865280",
      "id" : 978778703344865280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZVSC36WkAAGjxP.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 554
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 314
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 945
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 945
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/W0aaYbhXAZ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978779852106018816",
  "text" : "RT @niubi: It\u2019s official, Kim jung un went to beijing and met with Xi https:\/\/t.co\/W0aaYbhXAZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/niubi\/status\/978778708168400897\/photo\/1",
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/W0aaYbhXAZ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZVSC36WkAAGjxP.jpg",
        "id_str" : "978778703344865280",
        "id" : 978778703344865280,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZVSC36WkAAGjxP.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 554
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 314
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 945
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 945
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/W0aaYbhXAZ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978778708168400897",
    "text" : "It\u2019s official, Kim jung un went to beijing and met with Xi https:\/\/t.co\/W0aaYbhXAZ",
    "id" : 978778708168400897,
    "created_at" : "2018-03-27 23:40:03 +0000",
    "user" : {
      "name" : "Bill Bishop",
      "screen_name" : "niubi",
      "protected" : false,
      "id_str" : "3512101",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/910949265081536519\/YAQXtdLI_normal.jpg",
      "id" : 3512101,
      "verified" : true
    }
  },
  "id" : 978779852106018816,
  "created_at" : "2018-03-27 23:44:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/MWsxth6rF4",
      "expanded_url" : "http:\/\/po.st\/QCXtCl",
      "display_url" : "po.st\/QCXtCl"
    } ]
  },
  "geo" : { },
  "id_str" : "978775472019525633",
  "text" : "Teachers who drive can pay up to S$960 a year to park at their schools. https:\/\/t.co\/MWsxth6rF4",
  "id" : 978775472019525633,
  "created_at" : "2018-03-27 23:27:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/JsocBip61k",
      "expanded_url" : "http:\/\/str.sg\/oqQR",
      "display_url" : "str.sg\/oqQR"
    } ]
  },
  "geo" : { },
  "id_str" : "978775236907884544",
  "text" : "China Eastern plane discharges 30 tonnes of fuel for emergency landing to save passenger's life https:\/\/t.co\/JsocBip61k",
  "id" : 978775236907884544,
  "created_at" : "2018-03-27 23:26:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978760260532998144\/photo\/1",
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/eP1UcsdK7Z",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZVBMVLX0AEY5CA.jpg",
      "id_str" : "978760174122029057",
      "id" : 978760174122029057,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZVBMVLX0AEY5CA.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eP1UcsdK7Z"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978760260532998144\/photo\/1",
      "indices" : [ 91, 114 ],
      "url" : "https:\/\/t.co\/eP1UcsdK7Z",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZVBMVPXkAY49Sm.jpg",
      "id_str" : "978760174138789894",
      "id" : 978760174138789894,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZVBMVPXkAY49Sm.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eP1UcsdK7Z"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978760260532998144",
  "text" : "Back from a reception at a D2 hotel. The pastry is fine except the noodles and pak choy... https:\/\/t.co\/eP1UcsdK7Z",
  "id" : 978760260532998144,
  "created_at" : "2018-03-27 22:26:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GDPR",
      "indices" : [ 115, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978649708183851008",
  "text" : "RT @EmeraldDeLeeuw: Giving back to the Dublin Startup community. Book a session with me (Free!) to learn about how #GDPR impacts your #star\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GDPR",
        "indices" : [ 95, 100 ]
      }, {
        "text" : "startup",
        "indices" : [ 114, 122 ]
      } ],
      "urls" : [ {
        "indices" : [ 124, 147 ],
        "url" : "https:\/\/t.co\/rJsTEC7n9l",
        "expanded_url" : "https:\/\/twitter.com\/dogpatchlabs\/status\/978636832526266374",
        "display_url" : "twitter.com\/dogpatchlabs\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "978647212778512385",
    "text" : "Giving back to the Dublin Startup community. Book a session with me (Free!) to learn about how #GDPR impacts your #startup. https:\/\/t.co\/rJsTEC7n9l",
    "id" : 978647212778512385,
    "created_at" : "2018-03-27 14:57:32 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 978649708183851008,
  "created_at" : "2018-03-27 15:07:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Foreign Ministry",
      "screen_name" : "dfatirl",
      "indices" : [ 3, 11 ],
      "id_str" : "364198635",
      "id" : 364198635
    }, {
      "name" : "Simon Coveney",
      "screen_name" : "simoncoveney",
      "indices" : [ 24, 37 ],
      "id_str" : "118999126",
      "id" : 118999126
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978646795516604416",
  "text" : "RT @dfatirl: \uD83D\uDCF7\u00A0T\u00E1naiste @simoncoveney welcomes Deputy Prime Minister of Singapore, Mr. Teo Chee Hean \n\nA business lunch today celebrated th\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Simon Coveney",
        "screen_name" : "simoncoveney",
        "indices" : [ 11, 24 ],
        "id_str" : "118999126",
        "id" : 118999126
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dfatirl\/status\/978636823030390784\/photo\/1",
        "indices" : [ 223, 246 ],
        "url" : "https:\/\/t.co\/5UAwIMVwdg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZTIbk5XkAAYlX5.jpg",
        "id_str" : "978627395132559360",
        "id" : 978627395132559360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZTIbk5XkAAYlX5.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 372,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 656,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2239,
          "resize" : "fit",
          "w" : 4096
        }, {
          "h" : 1120,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5UAwIMVwdg"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/dfatirl\/status\/978636823030390784\/photo\/1",
        "indices" : [ 223, 246 ],
        "url" : "https:\/\/t.co\/5UAwIMVwdg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZTMPoNXkAAhQZX.jpg",
        "id_str" : "978631587909832704",
        "id" : 978631587909832704,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZTMPoNXkAAhQZX.jpg",
        "sizes" : [ {
          "h" : 2730,
          "resize" : "fit",
          "w" : 4096
        }, {
          "h" : 1365,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 800,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5UAwIMVwdg"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978636823030390784",
    "text" : "\uD83D\uDCF7\u00A0T\u00E1naiste @simoncoveney welcomes Deputy Prime Minister of Singapore, Mr. Teo Chee Hean \n\nA business lunch today celebrated the strength of our economic relationship, with Irish and Singaporean companies in attendance \uD83C\uDDEE\uD83C\uDDEA\uD83C\uDDF8\uD83C\uDDEC https:\/\/t.co\/5UAwIMVwdg",
    "id" : 978636823030390784,
    "created_at" : "2018-03-27 14:16:15 +0000",
    "user" : {
      "name" : "Irish Foreign Ministry",
      "screen_name" : "dfatirl",
      "protected" : false,
      "id_str" : "364198635",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013768347694006272\/HRSuQOaB_normal.jpg",
      "id" : 364198635,
      "verified" : true
    }
  },
  "id" : 978646795516604416,
  "created_at" : "2018-03-27 14:55:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/zYYXVcba3T",
      "expanded_url" : "https:\/\/www.facebook.com\/MrTeoCheeHean\/photos\/pcb.1552427451502092\/1552427281502109\/?type=3&theater",
      "display_url" : "facebook.com\/MrTeoCheeHean\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978575691372548096",
  "text" : "Someone is in town.  https:\/\/t.co\/zYYXVcba3T",
  "id" : 978575691372548096,
  "created_at" : "2018-03-27 10:13:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/g41nFUHbsl",
      "expanded_url" : "https:\/\/twitter.com\/lizzlaw_\/status\/978571101109239808",
      "display_url" : "twitter.com\/lizzlaw_\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978574153870643200",
  "text" : "Those were the days... https:\/\/t.co\/g41nFUHbsl",
  "id" : 978574153870643200,
  "created_at" : "2018-03-27 10:07:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Unpaid Britain",
      "screen_name" : "UnpaidBritain",
      "indices" : [ 3, 17 ],
      "id_str" : "4876021259",
      "id" : 4876021259
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978548855393353728",
  "text" : "RT @UnpaidBritain: Good for you. Employers have got away with selling the notion that \"experience\" is equivalent of wages. Payment by \"truc\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 204, 227 ],
        "url" : "https:\/\/t.co\/rbMmzNFYz2",
        "expanded_url" : "https:\/\/twitter.com\/IlyaGridneff\/status\/978530133857525761",
        "display_url" : "twitter.com\/IlyaGridneff\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "978547929651863552",
    "text" : "Good for you. Employers have got away with selling the notion that \"experience\" is equivalent of wages. Payment by \"truck\" was outlawed in 1831, so there's nothing new or modern (or acceptable) about it. https:\/\/t.co\/rbMmzNFYz2",
    "id" : 978547929651863552,
    "created_at" : "2018-03-27 08:23:01 +0000",
    "user" : {
      "name" : "Unpaid Britain",
      "screen_name" : "UnpaidBritain",
      "protected" : false,
      "id_str" : "4876021259",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/712968403271680000\/6wE_22O5_normal.jpg",
      "id" : 4876021259,
      "verified" : false
    }
  },
  "id" : 978548855393353728,
  "created_at" : "2018-03-27 08:26:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Home Office",
      "screen_name" : "ukhomeoffice",
      "indices" : [ 3, 16 ],
      "id_str" : "138037459",
      "id" : 138037459
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978548440140574720",
  "text" : "RT @ukhomeoffice: We\u2019re doubling the number of visas available for exceptionally talented people in technology, science, art and creative i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ukhomeoffice\/status\/930826727361863680\/video\/1",
        "indices" : [ 168, 191 ],
        "url" : "https:\/\/t.co\/843Tj5eZRl",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/930825337960910848\/pu\/img\/vfUdaEVoH-zF2-xR.jpg",
        "id_str" : "930825337960910848",
        "id" : 930825337960910848,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/930825337960910848\/pu\/img\/vfUdaEVoH-zF2-xR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/843Tj5eZRl"
      } ],
      "hashtags" : [ {
        "text" : "TechNation",
        "indices" : [ 156, 167 ]
      } ],
      "urls" : [ {
        "indices" : [ 132, 155 ],
        "url" : "https:\/\/t.co\/lTRzFqn7sg",
        "expanded_url" : "https:\/\/www.gov.uk\/government\/news\/government-doubles-exceptional-talent-visa-offer",
        "display_url" : "gov.uk\/government\/new\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "930826727361863680",
    "text" : "We\u2019re doubling the number of visas available for exceptionally talented people in technology, science, art and creative industries: https:\/\/t.co\/lTRzFqn7sg #TechNation https:\/\/t.co\/843Tj5eZRl",
    "id" : 930826727361863680,
    "created_at" : "2017-11-15 15:56:00 +0000",
    "user" : {
      "name" : "Home Office",
      "screen_name" : "ukhomeoffice",
      "protected" : false,
      "id_str" : "138037459",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/930022404407152640\/Rd9mnUue_normal.jpg",
      "id" : 138037459,
      "verified" : true
    }
  },
  "id" : 978548440140574720,
  "created_at" : "2018-03-27 08:25:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 3, 10 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Forbes\/status\/978393291837276160\/photo\/1",
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/VXrCZPy9qy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZPzg1yU0AA30fx.jpg",
      "id_str" : "978393289589051392",
      "id" : 978393289589051392,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZPzg1yU0AA30fx.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 577,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 577,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 577,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/VXrCZPy9qy"
    } ],
    "hashtags" : [ {
      "text" : "ForbesU30Asia",
      "indices" : [ 79, 93 ]
    } ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/f3OZ1OvTvy",
      "expanded_url" : "http:\/\/on.forbes.com\/6012DnlA2",
      "display_url" : "on.forbes.com\/6012DnlA2"
    } ]
  },
  "geo" : { },
  "id_str" : "978547632837742592",
  "text" : "RT @Forbes: Announcing the 2018 30 Under 30 Asia list: https:\/\/t.co\/f3OZ1OvTvy #ForbesU30Asia https:\/\/t.co\/VXrCZPy9qy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Forbes\/status\/978393291837276160\/photo\/1",
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/VXrCZPy9qy",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZPzg1yU0AA30fx.jpg",
        "id_str" : "978393289589051392",
        "id" : 978393289589051392,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZPzg1yU0AA30fx.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 577,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 577,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 577,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VXrCZPy9qy"
      } ],
      "hashtags" : [ {
        "text" : "ForbesU30Asia",
        "indices" : [ 67, 81 ]
      } ],
      "urls" : [ {
        "indices" : [ 43, 66 ],
        "url" : "https:\/\/t.co\/f3OZ1OvTvy",
        "expanded_url" : "http:\/\/on.forbes.com\/6012DnlA2",
        "display_url" : "on.forbes.com\/6012DnlA2"
      } ]
    },
    "geo" : { },
    "id_str" : "978393291837276160",
    "text" : "Announcing the 2018 30 Under 30 Asia list: https:\/\/t.co\/f3OZ1OvTvy #ForbesU30Asia https:\/\/t.co\/VXrCZPy9qy",
    "id" : 978393291837276160,
    "created_at" : "2018-03-26 22:08:33 +0000",
    "user" : {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "protected" : false,
      "id_str" : "91478624",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017039596083974149\/6AUhxLpr_normal.jpg",
      "id" : 91478624,
      "verified" : true
    }
  },
  "id" : 978547632837742592,
  "created_at" : "2018-03-27 08:21:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978523173397450753\/photo\/1",
      "indices" : [ 120, 143 ],
      "url" : "https:\/\/t.co\/7ZZxCbxVCP",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZRplPKX4AAKTlR.jpg",
      "id_str" : "978523107492356096",
      "id" : 978523107492356096,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZRplPKX4AAKTlR.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 230,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 233,
        "resize" : "fit",
        "w" : 689
      }, {
        "h" : 233,
        "resize" : "fit",
        "w" : 689
      }, {
        "h" : 233,
        "resize" : "fit",
        "w" : 689
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/7ZZxCbxVCP"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978523173397450753",
  "text" : "Data types are one of those things that you don\u2019t tend to care about until you get an error or some unexpected results. https:\/\/t.co\/7ZZxCbxVCP",
  "id" : 978523173397450753,
  "created_at" : "2018-03-27 06:44:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kaushik Basu",
      "screen_name" : "kaushikcbasu",
      "indices" : [ 3, 16 ],
      "id_str" : "17797666",
      "id" : 17797666
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978513385838006273",
  "text" : "RT @kaushikcbasu: This is symptomatic of what makes for exporting success.\nNo. of hours to clear govt border compliance for exports [World\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978459737225392128",
    "text" : "This is symptomatic of what makes for exporting success.\nNo. of hours to clear govt border compliance for exports [World Bank data]:\nSwitzerland 1\nUS 2\nSingapore 10\nS. Korea 13\nTaiwan 17\nChina 26\nSri Lanka 43\nMalaysia 45\nIndonesia 53\nVietnam 55\nB\u2019desh 100\nIndia 106\nZambia 120",
    "id" : 978459737225392128,
    "created_at" : "2018-03-27 02:32:34 +0000",
    "user" : {
      "name" : "Kaushik Basu",
      "screen_name" : "kaushikcbasu",
      "protected" : false,
      "id_str" : "17797666",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/961450229160693760\/-dargoyY_normal.jpg",
      "id" : 17797666,
      "verified" : true
    }
  },
  "id" : 978513385838006273,
  "created_at" : "2018-03-27 06:05:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978508676574171136",
  "text" : "Smashing Magazine ditch WordPress and move to JAMstack - stands for JavaScript, APIs, &amp; Markup; an approach to building websites that allows you to have all the web performance &amp; security benefits of a static website along with the dynamic capabilities of a database-driven CMS.",
  "id" : 978508676574171136,
  "created_at" : "2018-03-27 05:47:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 89, 100 ],
      "id_str" : "816653",
      "id" : 816653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/ymqOa7nrQy",
      "expanded_url" : "https:\/\/tcrn.ch\/2pHhjJY",
      "display_url" : "tcrn.ch\/2pHhjJY"
    } ]
  },
  "geo" : { },
  "id_str" : "978507220240433153",
  "text" : "RT @edwinksl: Foxconn buys peripheral maker Belkin for $866M https:\/\/t.co\/ymqOa7nrQy via @techcrunch",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TechCrunch",
        "screen_name" : "TechCrunch",
        "indices" : [ 75, 86 ],
        "id_str" : "816653",
        "id" : 816653
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/ymqOa7nrQy",
        "expanded_url" : "https:\/\/tcrn.ch\/2pHhjJY",
        "display_url" : "tcrn.ch\/2pHhjJY"
      } ]
    },
    "geo" : { },
    "id_str" : "978501389516263426",
    "text" : "Foxconn buys peripheral maker Belkin for $866M https:\/\/t.co\/ymqOa7nrQy via @techcrunch",
    "id" : 978501389516263426,
    "created_at" : "2018-03-27 05:18:05 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 978507220240433153,
  "created_at" : "2018-03-27 05:41:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/978505701981999104\/photo\/1",
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/xyWNhT5z8J",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZRZv52U8AAJ_Sj.jpg",
      "id_str" : "978505698563649536",
      "id" : 978505698563649536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZRZv52U8AAJ_Sj.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 363,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      }, {
        "h" : 529,
        "resize" : "fit",
        "w" : 991
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xyWNhT5z8J"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/Ohn7nlPL8K",
      "expanded_url" : "https:\/\/cna.asia\/2I8PW2h",
      "display_url" : "cna.asia\/2I8PW2h"
    } ]
  },
  "geo" : { },
  "id_str" : "978507140288647170",
  "text" : "RT @ChannelNewsAsia: Alibaba, Ford unveil car vending machine in China: Reports https:\/\/t.co\/Ohn7nlPL8K https:\/\/t.co\/xyWNhT5z8J",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/dlvrit.com\/\" rel=\"nofollow\"\u003Edlvr.it\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/978505701981999104\/photo\/1",
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/xyWNhT5z8J",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZRZv52U8AAJ_Sj.jpg",
        "id_str" : "978505698563649536",
        "id" : 978505698563649536,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZRZv52U8AAJ_Sj.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 363,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        }, {
          "h" : 529,
          "resize" : "fit",
          "w" : 991
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xyWNhT5z8J"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/Ohn7nlPL8K",
        "expanded_url" : "https:\/\/cna.asia\/2I8PW2h",
        "display_url" : "cna.asia\/2I8PW2h"
      } ]
    },
    "geo" : { },
    "id_str" : "978505701981999104",
    "text" : "Alibaba, Ford unveil car vending machine in China: Reports https:\/\/t.co\/Ohn7nlPL8K https:\/\/t.co\/xyWNhT5z8J",
    "id" : 978505701981999104,
    "created_at" : "2018-03-27 05:35:13 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 978507140288647170,
  "created_at" : "2018-03-27 05:40:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/saNI4PMRPP",
      "expanded_url" : "http:\/\/www.independent.co.uk\/news\/world\/asia\/taiwan-china-jets-air-force-drill-island-bashi-channel-a8274571.html",
      "display_url" : "independent.co.uk\/news\/world\/asi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978354432357347328",
  "text" : "China Air Force drill causes Taiwan to scramble jets https:\/\/t.co\/saNI4PMRPP",
  "id" : 978354432357347328,
  "created_at" : "2018-03-26 19:34:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Runa Sandvik",
      "screen_name" : "runasand",
      "indices" : [ 3, 12 ],
      "id_str" : "19959946",
      "id" : 19959946
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/runasand\/status\/978072135162826752\/photo\/1",
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/BJstjI5X7w",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZLPa7bWsAAkEJ9.jpg",
      "id_str" : "978072130628792320",
      "id" : 978072130628792320,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZLPa7bWsAAkEJ9.jpg",
      "sizes" : [ {
        "h" : 479,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 479,
        "resize" : "fit",
        "w" : 480
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/BJstjI5X7w"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978353951568547841",
  "text" : "RT @runasand: It me. https:\/\/t.co\/BJstjI5X7w",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/runasand\/status\/978072135162826752\/photo\/1",
        "indices" : [ 7, 30 ],
        "url" : "https:\/\/t.co\/BJstjI5X7w",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZLPa7bWsAAkEJ9.jpg",
        "id_str" : "978072130628792320",
        "id" : 978072130628792320,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZLPa7bWsAAkEJ9.jpg",
        "sizes" : [ {
          "h" : 479,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 479,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BJstjI5X7w"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978072135162826752",
    "text" : "It me. https:\/\/t.co\/BJstjI5X7w",
    "id" : 978072135162826752,
    "created_at" : "2018-03-26 00:52:23 +0000",
    "user" : {
      "name" : "Runa Sandvik",
      "screen_name" : "runasand",
      "protected" : false,
      "id_str" : "19959946",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2467580946\/as3tcci2j58vsr7bad0v_normal.jpeg",
      "id" : 19959946,
      "verified" : true
    }
  },
  "id" : 978353951568547841,
  "created_at" : "2018-03-26 19:32:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eugene Gu, MD",
      "screen_name" : "eugenegu",
      "indices" : [ 3, 12 ],
      "id_str" : "65497475",
      "id" : 65497475
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978325254102814721",
  "text" : "RT @eugenegu: As a surgeon, I\u2019ve operated on gunshot victims who\u2019ve had bullets tear through their intestines, cut through their spinal cor\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977954044562960385",
    "text" : "As a surgeon, I\u2019ve operated on gunshot victims who\u2019ve had bullets tear through their intestines, cut through their spinal cord, and pulverize their kidneys and liver. Rick Santorum telling kids to shut up and take CPR classes is simply unconscionable.",
    "id" : 977954044562960385,
    "created_at" : "2018-03-25 17:03:08 +0000",
    "user" : {
      "name" : "Eugene Gu, MD",
      "screen_name" : "eugenegu",
      "protected" : false,
      "id_str" : "65497475",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/827635388047044609\/dMD4D-0S_normal.jpg",
      "id" : 65497475,
      "verified" : true
    }
  },
  "id" : 978325254102814721,
  "created_at" : "2018-03-26 17:38:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Fire Brigade",
      "screen_name" : "DubFireBrigade",
      "indices" : [ 3, 18 ],
      "id_str" : "2798058029",
      "id" : 2798058029
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978245556626739201",
  "text" : "RT @DubFireBrigade: Amazon are carrying out a voluntary recall campaign for various models of its \u2018AmazonBasics\u2019 branded portable power pac\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "CCPC",
        "screen_name" : "CCPCIreland",
        "indices" : [ 237, 249 ],
        "id_str" : "41091295",
        "id" : 41091295
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DubFireBrigade\/status\/978232678767095808\/photo\/1",
        "indices" : [ 274, 297 ],
        "url" : "https:\/\/t.co\/YRjOzGQnCp",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZNhb8JU8AE-srk.png",
        "id_str" : "978232676699140097",
        "id" : 978232676699140097,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZNhb8JU8AE-srk.png",
        "sizes" : [ {
          "h" : 174,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 174,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 174,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 154,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YRjOzGQnCp"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 250, 273 ],
        "url" : "https:\/\/t.co\/gGPx7VaXPa",
        "expanded_url" : "https:\/\/buff.ly\/2usdPzZ",
        "display_url" : "buff.ly\/2usdPzZ"
      } ]
    },
    "geo" : { },
    "id_str" : "978232678767095808",
    "text" : "Amazon are carrying out a voluntary recall campaign for various models of its \u2018AmazonBasics\u2019 branded portable power packs. Safety issues have been reported including the appliance smoking, catching fire and\/or exploding. Further info on @CCPCIreland https:\/\/t.co\/gGPx7VaXPa https:\/\/t.co\/YRjOzGQnCp",
    "id" : 978232678767095808,
    "created_at" : "2018-03-26 11:30:19 +0000",
    "user" : {
      "name" : "Dublin Fire Brigade",
      "screen_name" : "DubFireBrigade",
      "protected" : false,
      "id_str" : "2798058029",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013704894518349824\/a65k7PEh_normal.jpg",
      "id" : 2798058029,
      "verified" : true
    }
  },
  "id" : 978245556626739201,
  "created_at" : "2018-03-26 12:21:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978223620337160192\/photo\/1",
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/STSFD0vG7R",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZNYYMxXkAAqJYL.jpg",
      "id_str" : "978222716837924864",
      "id" : 978222716837924864,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZNYYMxXkAAqJYL.jpg",
      "sizes" : [ {
        "h" : 466,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 639,
        "resize" : "fit",
        "w" : 932
      }, {
        "h" : 639,
        "resize" : "fit",
        "w" : 932
      }, {
        "h" : 639,
        "resize" : "fit",
        "w" : 932
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/STSFD0vG7R"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978223620337160192",
  "text" : "You want to Advertise in our Webpage? This site is still up and running. Last update is 2018. https:\/\/t.co\/STSFD0vG7R",
  "id" : 978223620337160192,
  "created_at" : "2018-03-26 10:54:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R-Ladies Dublin",
      "screen_name" : "RLadiesDublin",
      "indices" : [ 3, 17 ],
      "id_str" : "822851102915891200",
      "id" : 822851102915891200
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978221478679326720",
  "text" : "RT @RLadiesDublin: Our next meet up will be for those who want to learn how to write reproducible reports with R Markdown. And those who ju\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "BOIStartUps",
        "screen_name" : "BOIstartups",
        "indices" : [ 201, 213 ],
        "id_str" : "3003233451",
        "id" : 3003233451
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BOIWorkbench",
        "indices" : [ 226, 239 ]
      }, {
        "text" : "RLadies",
        "indices" : [ 240, 248 ]
      } ],
      "urls" : [ {
        "indices" : [ 249, 272 ],
        "url" : "https:\/\/t.co\/7uSrCFaoPk",
        "expanded_url" : "https:\/\/buff.ly\/2IFhdu8",
        "display_url" : "buff.ly\/2IFhdu8"
      } ]
    },
    "geo" : { },
    "id_str" : "976802176688381953",
    "text" : "Our next meet up will be for those who want to learn how to write reproducible reports with R Markdown. And those who just like to hang out with a nice bunch \uD83D\uDE04Kick off at 6pm on Tuesday, 27th March at @BOIstartups Grand Canal #BOIWorkbench #RLadies https:\/\/t.co\/7uSrCFaoPk",
    "id" : 976802176688381953,
    "created_at" : "2018-03-22 12:46:01 +0000",
    "user" : {
      "name" : "R-Ladies Dublin",
      "screen_name" : "RLadiesDublin",
      "protected" : false,
      "id_str" : "822851102915891200",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/822853432121978882\/sOiJhldD_normal.jpg",
      "id" : 822851102915891200,
      "verified" : false
    }
  },
  "id" : 978221478679326720,
  "created_at" : "2018-03-26 10:45:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Paul Kidd",
      "screen_name" : "paulkidd",
      "indices" : [ 3, 12 ],
      "id_str" : "14071129",
      "id" : 14071129
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978215120185815040",
  "text" : "RT @paulkidd: AUSTRALIA:\nTampa: meh\nChildren overboard: meh\nOffshore detention: meh\nDeaths in custody: meh\nIndigenous health gap: meh\nEnvir\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977858002546929664",
    "text" : "AUSTRALIA:\nTampa: meh\nChildren overboard: meh\nOffshore detention: meh\nDeaths in custody: meh\nIndigenous health gap: meh\nEnvironmental destruction: meh\nWorkers ripped off: meh\nBall tampering in cricket: OMG WE HAVE LOST OUR WAY WHAT HAS BECOME OF US?",
    "id" : 977858002546929664,
    "created_at" : "2018-03-25 10:41:30 +0000",
    "user" : {
      "name" : "Paul Kidd",
      "screen_name" : "paulkidd",
      "protected" : false,
      "id_str" : "14071129",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/926920438315233280\/_uUKFFrL_normal.jpg",
      "id" : 14071129,
      "verified" : true
    }
  },
  "id" : 978215120185815040,
  "created_at" : "2018-03-26 10:20:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978211978069110784",
  "text" : "Not sure the single parent I know who on zerohour contract taking in \u00A316k a year, work 7 days a week, a private renter who has no sofa and TV find out that the tax she pay is funding someone 20 years bereavement support who get interviewed by BBC in her nice kitchen.",
  "id" : 978211978069110784,
  "created_at" : "2018-03-26 10:08:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "indices" : [ 3, 12 ],
      "id_str" : "875971",
      "id" : 875971
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/1TCPZZ8IsB",
      "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/978157101426999297",
      "display_url" : "twitter.com\/TODAYonline\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978184138787315713",
  "text" : "RT @Rubenerd: That's a handsome bird. https:\/\/t.co\/1TCPZZ8IsB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 24, 47 ],
        "url" : "https:\/\/t.co\/1TCPZZ8IsB",
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/978157101426999297",
        "display_url" : "twitter.com\/TODAYonline\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "978157680672915456",
    "text" : "That's a handsome bird. https:\/\/t.co\/1TCPZZ8IsB",
    "id" : 978157680672915456,
    "created_at" : "2018-03-26 06:32:19 +0000",
    "user" : {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "protected" : false,
      "id_str" : "875971",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011136325364301827\/hmrtp1V1_normal.jpg",
      "id" : 875971,
      "verified" : false
    }
  },
  "id" : 978184138787315713,
  "created_at" : "2018-03-26 08:17:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 51, 74 ],
      "url" : "https:\/\/t.co\/wqfheybiPO",
      "expanded_url" : "https:\/\/docs.google.com\/forms\/d\/e\/1FAIpQLSfaszQTEbGxPX__iphCv-8yG-2uIb7exZUMJVKsjaH4p9eu1g\/viewform",
      "display_url" : "docs.google.com\/forms\/d\/e\/1FAI\u2026"
    }, {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/AkD3uXYScU",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978006935252422657",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978059507992424449",
  "text" : "I have set up an anonymous survey for some input.  https:\/\/t.co\/wqfheybiPO https:\/\/t.co\/AkD3uXYScU",
  "id" : 978059507992424449,
  "created_at" : "2018-03-26 00:02:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978050435247493123\/photo\/1",
      "indices" : [ 196, 219 ],
      "url" : "https:\/\/t.co\/x7HBoqAxIm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZK7qLMWAAASS33.jpg",
      "id_str" : "978050402326282240",
      "id" : 978050402326282240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZK7qLMWAAASS33.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 382,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/x7HBoqAxIm"
    } ],
    "hashtags" : [ {
      "text" : "docker",
      "indices" : [ 33, 40 ]
    }, {
      "text" : "DSLearnings",
      "indices" : [ 183, 195 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978050435247493123",
  "text" : "If you run Jupyter Notebook in a #docker container environment and dataframe plot throws this \nNameError: name '_converter' is not defined \nJust restarting server (docker container)  #DSLearnings https:\/\/t.co\/x7HBoqAxIm",
  "id" : 978050435247493123,
  "created_at" : "2018-03-25 23:26:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "indices" : [ 3, 7 ],
      "id_str" : "5120691",
      "id" : 5120691
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "education",
      "indices" : [ 111, 121 ]
    } ],
    "urls" : [ {
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/cehlR6yPKv",
      "expanded_url" : "http:\/\/wef.ch\/2FIFxNe",
      "display_url" : "wef.ch\/2FIFxNe"
    } ]
  },
  "geo" : { },
  "id_str" : "978048757995659264",
  "text" : "RT @wef: These are the world's best business schools, according to the Financial Times https:\/\/t.co\/cehlR6yPKv #education https:\/\/t.co\/875A\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/978026999838072832\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/875Awek6kN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZKmX0VX0AAhIce.jpg",
        "id_str" : "978026997208305664",
        "id" : 978026997208305664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZKmX0VX0AAhIce.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 609
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1020,
          "resize" : "fit",
          "w" : 914
        }, {
          "h" : 1020,
          "resize" : "fit",
          "w" : 914
        }, {
          "h" : 1020,
          "resize" : "fit",
          "w" : 914
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/875Awek6kN"
      } ],
      "hashtags" : [ {
        "text" : "education",
        "indices" : [ 102, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 78, 101 ],
        "url" : "https:\/\/t.co\/cehlR6yPKv",
        "expanded_url" : "http:\/\/wef.ch\/2FIFxNe",
        "display_url" : "wef.ch\/2FIFxNe"
      } ]
    },
    "geo" : { },
    "id_str" : "978026999838072832",
    "text" : "These are the world's best business schools, according to the Financial Times https:\/\/t.co\/cehlR6yPKv #education https:\/\/t.co\/875Awek6kN",
    "id" : 978026999838072832,
    "created_at" : "2018-03-25 21:53:02 +0000",
    "user" : {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "protected" : false,
      "id_str" : "5120691",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/565498192171507712\/r2Hb2gvX_normal.png",
      "id" : 5120691,
      "verified" : true
    }
  },
  "id" : 978048757995659264,
  "created_at" : "2018-03-25 23:19:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 157, 180 ],
      "url" : "https:\/\/t.co\/qinRslZaO1",
      "expanded_url" : "https:\/\/theitriskmanager.wordpress.com\/2014\/09\/02\/cynefin-and-the-business-analyst-product-owner\/",
      "display_url" : "theitriskmanager.wordpress.com\/2014\/09\/02\/cyn\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978046116380991493",
  "text" : "Whether you should be doing Business Analysis, Product Management or JFDI, the Cynefin framework is a particularly useful tool to determine which approach.  https:\/\/t.co\/qinRslZaO1",
  "id" : 978046116380991493,
  "created_at" : "2018-03-25 23:09:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/UX1y5GpRVU",
      "expanded_url" : "https:\/\/repo2docker.readthedocs.io",
      "display_url" : "repo2docker.readthedocs.io"
    } ]
  },
  "geo" : { },
  "id_str" : "978045122481967110",
  "text" : "Turn git repositories into Jupyter enabled Docker Images https:\/\/t.co\/UX1y5GpRVU FTW \uD83D\uDE4C\uD83D\uDC4F",
  "id" : 978045122481967110,
  "created_at" : "2018-03-25 23:05:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DSLearnings",
      "indices" : [ 25, 37 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/HNT7C3Z7je",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/977980332568870912",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978036508895731712",
  "text" : "Wrong hashtag. Should be #DSLearnings https:\/\/t.co\/HNT7C3Z7je",
  "id" : 978036508895731712,
  "created_at" : "2018-03-25 22:30:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/978036024940158977\/photo\/1",
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/wafRQVzDr1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZKug2VX4AAUjeI.jpg",
      "id_str" : "978035948457025536",
      "id" : 978035948457025536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZKug2VX4AAUjeI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 285,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 1144
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 1144
      }, {
        "h" : 480,
        "resize" : "fit",
        "w" : 1144
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/wafRQVzDr1"
    } ],
    "hashtags" : [ {
      "text" : "DSlearnings",
      "indices" : [ 63, 75 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978036024940158977",
  "text" : "Plot not showing in Jupyter Notebook. \nTry \n%matplotlib inline\n#DSlearnings https:\/\/t.co\/wafRQVzDr1",
  "id" : 978036024940158977,
  "created_at" : "2018-03-25 22:28:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "docker",
      "indices" : [ 129, 136 ]
    }, {
      "text" : "DataScience",
      "indices" : [ 137, 149 ]
    }, {
      "text" : "rstats",
      "indices" : [ 150, 157 ]
    } ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/wmRYZ32oAL",
      "expanded_url" : "https:\/\/parente.github.io\/docker-stacker\/",
      "display_url" : "parente.github.io\/docker-stacker\/"
    }, {
      "indices" : [ 104, 127 ],
      "url" : "https:\/\/t.co\/VtcAyhxq6S",
      "expanded_url" : "https:\/\/phpdocker.io\/generator",
      "display_url" : "phpdocker.io\/generator"
    }, {
      "indices" : [ 159, 182 ],
      "url" : "https:\/\/t.co\/ni0LHI3QBE",
      "expanded_url" : "https:\/\/github.com\/jupyter\/docker-stacks\/issues\/469",
      "display_url" : "github.com\/jupyter\/docker\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "978031942414356481",
  "text" : "Seeking a few good men and women to continue the good work at https:\/\/t.co\/wmRYZ32oAL along the vein of https:\/\/t.co\/VtcAyhxq6S  #docker #DataScience #rstats  https:\/\/t.co\/ni0LHI3QBE",
  "id" : 978031942414356481,
  "created_at" : "2018-03-25 22:12:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978006935252422657",
  "text" : "When it comes to wedding preparation, what is the one job you try desperately avoid doing?",
  "id" : 978006935252422657,
  "created_at" : "2018-03-25 20:33:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "ethics",
      "indices" : [ 108, 115 ]
    }, {
      "text" : "privacy",
      "indices" : [ 116, 124 ]
    }, {
      "text" : "books",
      "indices" : [ 125, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "978006128343830529",
  "text" : "RT @EmeraldDeLeeuw: Recommendations please \uD83D\uDE00best books on ethics and technology? Any help much appreciated. #ethics #privacy #books #recomm\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "ethics",
        "indices" : [ 88, 95 ]
      }, {
        "text" : "privacy",
        "indices" : [ 96, 104 ]
      }, {
        "text" : "books",
        "indices" : [ 105, 111 ]
      }, {
        "text" : "recommendations",
        "indices" : [ 112, 128 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "978002385560854528",
    "text" : "Recommendations please \uD83D\uDE00best books on ethics and technology? Any help much appreciated. #ethics #privacy #books #recommendations",
    "id" : 978002385560854528,
    "created_at" : "2018-03-25 20:15:13 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 978006128343830529,
  "created_at" : "2018-03-25 20:30:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E50",
      "screen_name" : "shimmertje8",
      "indices" : [ 0, 12 ],
      "id_str" : "237988530",
      "id" : 237988530
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "977982644980826112",
  "geo" : { },
  "id_str" : "977995411360231424",
  "in_reply_to_user_id" : 237988530,
  "text" : "@shimmertje8 You mean via post?",
  "id" : 977995411360231424,
  "in_reply_to_status_id" : 977982644980826112,
  "created_at" : "2018-03-25 19:47:31 +0000",
  "in_reply_to_screen_name" : "shimmertje8",
  "in_reply_to_user_id_str" : "237988530",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "DSLearning",
      "indices" : [ 0, 11 ]
    } ],
    "urls" : [ {
      "indices" : [ 172, 195 ],
      "url" : "https:\/\/t.co\/Kj68AyHE6j",
      "expanded_url" : "http:\/\/pandas.read",
      "display_url" : "pandas.read"
    } ]
  },
  "geo" : { },
  "id_str" : "977980332568870912",
  "text" : "#DSLearning \nBy default the read_csv function will read a comma-separated file; some data are are separated by tabs. use the sep parameter and indicate a tab with \\t\n\ndf = https:\/\/t.co\/Kj68AyHE6j_csv('..\/data\/gapminder.tsv', sep='\\t')",
  "id" : 977980332568870912,
  "created_at" : "2018-03-25 18:47:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Estonian World",
      "screen_name" : "EstonianWorld",
      "indices" : [ 3, 17 ],
      "id_str" : "583775894",
      "id" : 583775894
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Soviets",
      "indices" : [ 108, 116 ]
    }, {
      "text" : "Siberia",
      "indices" : [ 120, 128 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977973376609374211",
  "text" : "RT @EstonianWorld: On 25 March, 20,000 candles, one for each of the men, women and children deported by the #Soviets to #Siberia in 1949, w\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/EstonianWorld\/status\/977699150358810625\/photo\/1",
        "indices" : [ 279, 302 ],
        "url" : "https:\/\/t.co\/Sbk2Rr4FZv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZF8MF6W0AEJW6N.jpg",
        "id_str" : "977699141303324673",
        "id" : 977699141303324673,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZF8MF6W0AEJW6N.jpg",
        "sizes" : [ {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Sbk2Rr4FZv"
      } ],
      "hashtags" : [ {
        "text" : "Soviets",
        "indices" : [ 89, 97 ]
      }, {
        "text" : "Siberia",
        "indices" : [ 101, 109 ]
      }, {
        "text" : "Estonia",
        "indices" : [ 138, 146 ]
      }, {
        "text" : "Estonian",
        "indices" : [ 165, 174 ]
      } ],
      "urls" : [ {
        "indices" : [ 255, 278 ],
        "url" : "https:\/\/t.co\/aZdPm0z3IB",
        "expanded_url" : "https:\/\/goo.gl\/4BweKV",
        "display_url" : "goo.gl\/4BweKV"
      } ]
    },
    "geo" : { },
    "id_str" : "977699150358810625",
    "text" : "On 25 March, 20,000 candles, one for each of the men, women and children deported by the #Soviets to #Siberia in 1949, will be lighted in #Estonia. Nearly 3% of the #Estonian population were seized in a few days and dispatched to remote areas of Siberia. https:\/\/t.co\/aZdPm0z3IB https:\/\/t.co\/Sbk2Rr4FZv",
    "id" : 977699150358810625,
    "created_at" : "2018-03-25 00:10:16 +0000",
    "user" : {
      "name" : "Estonian World",
      "screen_name" : "EstonianWorld",
      "protected" : false,
      "id_str" : "583775894",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/648426326064435202\/ha63kc9g_normal.jpg",
      "id" : 583775894,
      "verified" : false
    }
  },
  "id" : 977973376609374211,
  "created_at" : "2018-03-25 18:19:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeryl",
      "screen_name" : "j9ryl",
      "indices" : [ 3, 9 ],
      "id_str" : "409822678",
      "id" : 409822678
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/j9ryl\/status\/976831573478854665\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/rCi34nQvGm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5nGldX4AAAvOA.jpg",
      "id_str" : "976831532018163712",
      "id" : 976831532018163712,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5nGldX4AAAvOA.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1638
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 544
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1638
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rCi34nQvGm"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/j9ryl\/status\/976831573478854665\/photo\/1",
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/rCi34nQvGm",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5nGlaXUAEWAdI.jpg",
      "id_str" : "976831532005543937",
      "id" : 976831532005543937,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5nGlaXUAEWAdI.jpg",
      "sizes" : [ {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1638
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 544
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1638
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rCi34nQvGm"
    } ],
    "hashtags" : [ {
      "text" : "Singapore",
      "indices" : [ 23, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977937592011378690",
  "text" : "RT @j9ryl: My city. \uD83C\uDF06\uD83C\uDF03 #Singapore \uD83C\uDDF8\uD83C\uDDEC https:\/\/t.co\/rCi34nQvGm",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/j9ryl\/status\/976831573478854665\/photo\/1",
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/rCi34nQvGm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5nGldX4AAAvOA.jpg",
        "id_str" : "976831532018163712",
        "id" : 976831532018163712,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5nGldX4AAAvOA.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1638
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 544
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1638
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rCi34nQvGm"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/j9ryl\/status\/976831573478854665\/photo\/1",
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/rCi34nQvGm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5nGlaXUAEWAdI.jpg",
        "id_str" : "976831532005543937",
        "id" : 976831532005543937,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5nGlaXUAEWAdI.jpg",
        "sizes" : [ {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1638
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 544
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1638
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rCi34nQvGm"
      } ],
      "hashtags" : [ {
        "text" : "Singapore",
        "indices" : [ 12, 22 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976831573478854665",
    "text" : "My city. \uD83C\uDF06\uD83C\uDF03 #Singapore \uD83C\uDDF8\uD83C\uDDEC https:\/\/t.co\/rCi34nQvGm",
    "id" : 976831573478854665,
    "created_at" : "2018-03-22 14:42:50 +0000",
    "user" : {
      "name" : "Jeryl",
      "screen_name" : "j9ryl",
      "protected" : false,
      "id_str" : "409822678",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/800514777302724608\/FD1HqwII_normal.jpg",
      "id" : 409822678,
      "verified" : false
    }
  },
  "id" : 977937592011378690,
  "created_at" : "2018-03-25 15:57:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "indices" : [ 3, 19 ],
      "id_str" : "756198498723328000",
      "id" : 756198498723328000
    }, {
      "name" : "Levenshulme Market",
      "screen_name" : "levymarket",
      "indices" : [ 110, 121 ],
      "id_str" : "531410380",
      "id" : 531410380
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977936219941277696",
  "text" : "RT @jackiepenangkit: Delighted to be awarded a Level 5 Food Hygiene Rating. Looking forward to the 10th April @levymarket event at The Univ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Levenshulme Market",
        "screen_name" : "levymarket",
        "indices" : [ 89, 100 ],
        "id_str" : "531410380",
        "id" : 531410380
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jackiepenangkit\/status\/977932909532368901\/photo\/1",
        "indices" : [ 161, 184 ],
        "url" : "https:\/\/t.co\/x7tHeWKB0F",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZJQwz2W4AAX7bX.jpg",
        "id_str" : "977932868574961664",
        "id" : 977932868574961664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZJQwz2W4AAX7bX.jpg",
        "sizes" : [ {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/x7tHeWKB0F"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/jackiepenangkit\/status\/977932909532368901\/photo\/1",
        "indices" : [ 161, 184 ],
        "url" : "https:\/\/t.co\/x7tHeWKB0F",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZJQw1YXkAA3g6b.jpg",
        "id_str" : "977932868986048512",
        "id" : 977932868986048512,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZJQw1YXkAA3g6b.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/x7tHeWKB0F"
      } ],
      "hashtags" : [ {
        "text" : "jackiepenangkitchen",
        "indices" : [ 140, 160 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977932909532368901",
    "text" : "Delighted to be awarded a Level 5 Food Hygiene Rating. Looking forward to the 10th April @levymarket event at The University of Manchester. #jackiepenangkitchen https:\/\/t.co\/x7tHeWKB0F",
    "id" : 977932909532368901,
    "created_at" : "2018-03-25 15:39:09 +0000",
    "user" : {
      "name" : "JackiePenangKitchen",
      "screen_name" : "jackiepenangkit",
      "protected" : false,
      "id_str" : "756198498723328000",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846652874033651712\/CfemJirj_normal.jpg",
      "id" : 756198498723328000,
      "verified" : false
    }
  },
  "id" : 977936219941277696,
  "created_at" : "2018-03-25 15:52:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/AuLM0HfHUd",
      "expanded_url" : "https:\/\/www.manchestereveningnews.co.uk\/news\/moment-police-officers-take-down-14455623#ICID=sharebar_twitter",
      "display_url" : "manchestereveningnews.co.uk\/news\/moment-po\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977931767394029568",
  "text" : "I think you can hear the sounds of Taser.. https:\/\/t.co\/AuLM0HfHUd",
  "id" : 977931767394029568,
  "created_at" : "2018-03-25 15:34:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tong Bingxue \u4EDD\u51B0\u96EA",
      "screen_name" : "tongbingxue",
      "indices" : [ 3, 15 ],
      "id_str" : "3511226595",
      "id" : 3511226595
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977920743295025153",
  "text" : "RT @tongbingxue: West by East &amp; East by West.\n\nThe first is a CDV by Pun Lun Photographer in HK, ca. 1870s; \n\nThe second is by anonymous Ph\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/tongbingxue\/status\/977914016428445696\/photo\/1",
        "indices" : [ 184, 207 ],
        "url" : "https:\/\/t.co\/wzjRoVf749",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZI_koHX4AAKOug.jpg",
        "id_str" : "977913967569002496",
        "id" : 977913967569002496,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZI_koHX4AAKOug.jpg",
        "sizes" : [ {
          "h" : 809,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 809,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 809,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 429
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wzjRoVf749"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/tongbingxue\/status\/977914016428445696\/photo\/1",
        "indices" : [ 184, 207 ],
        "url" : "https:\/\/t.co\/wzjRoVf749",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZI_koHW4AAcTrg.jpg",
        "id_str" : "977913967568936960",
        "id" : 977913967568936960,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZI_koHW4AAcTrg.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 862
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 862
        }, {
          "h" : 1024,
          "resize" : "fit",
          "w" : 862
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 572
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wzjRoVf749"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977914016428445696",
    "text" : "West by East &amp; East by West.\n\nThe first is a CDV by Pun Lun Photographer in HK, ca. 1870s; \n\nThe second is by anonymous Photographer in northern China Shandong Province,ca. 1930s. https:\/\/t.co\/wzjRoVf749",
    "id" : 977914016428445696,
    "created_at" : "2018-03-25 14:24:04 +0000",
    "user" : {
      "name" : "Tong Bingxue \u4EDD\u51B0\u96EA",
      "screen_name" : "tongbingxue",
      "protected" : false,
      "id_str" : "3511226595",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/912311827098304512\/-FipW-dj_normal.jpg",
      "id" : 3511226595,
      "verified" : false
    }
  },
  "id" : 977920743295025153,
  "created_at" : "2018-03-25 14:50:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Laura Koenig",
      "screen_name" : "2nickels",
      "indices" : [ 3, 12 ],
      "id_str" : "15294712",
      "id" : 15294712
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/2nickels\/status\/977594934743232514\/photo\/1",
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/9osX7LNpFj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZEdaEBX4AI7crR.jpg",
      "id_str" : "977594927709413378",
      "id" : 977594927709413378,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZEdaEBX4AI7crR.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 1024
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9osX7LNpFj"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977917461361250305",
  "text" : "RT @2nickels: A six-year-old just handed this to me. https:\/\/t.co\/9osX7LNpFj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/2nickels\/status\/977594934743232514\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/9osX7LNpFj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZEdaEBX4AI7crR.jpg",
        "id_str" : "977594927709413378",
        "id" : 977594927709413378,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZEdaEBX4AI7crR.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9osX7LNpFj"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977594934743232514",
    "text" : "A six-year-old just handed this to me. https:\/\/t.co\/9osX7LNpFj",
    "id" : 977594934743232514,
    "created_at" : "2018-03-24 17:16:09 +0000",
    "user" : {
      "name" : "Laura Koenig",
      "screen_name" : "2nickels",
      "protected" : false,
      "id_str" : "15294712",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630492900900610048\/D5j-u-Es_normal.jpg",
      "id" : 15294712,
      "verified" : false
    }
  },
  "id" : 977917461361250305,
  "created_at" : "2018-03-25 14:37:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sue Fletcher-Watson",
      "screen_name" : "SueReviews",
      "indices" : [ 3, 14 ],
      "id_str" : "1300316983",
      "id" : 1300316983
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977899538341089280",
  "text" : "RT @SueReviews: A thread on how to access journal articles, for people who don't have access via an employer... \n\nObv open access is best,\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976555546470789120",
    "text" : "A thread on how to access journal articles, for people who don't have access via an employer... \n\nObv open access is best, but in the meantime, here are some get-arounds, since I've seen a few folks on here struggling to locate articles \/ coming up against paywalls",
    "id" : 976555546470789120,
    "created_at" : "2018-03-21 20:26:00 +0000",
    "user" : {
      "name" : "Sue Fletcher-Watson",
      "screen_name" : "SueReviews",
      "protected" : false,
      "id_str" : "1300316983",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/975080086302068737\/M6G8C1g2_normal.jpg",
      "id" : 1300316983,
      "verified" : false
    }
  },
  "id" : 977899538341089280,
  "created_at" : "2018-03-25 13:26:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Search Engine Land",
      "screen_name" : "sengineland",
      "indices" : [ 3, 15 ],
      "id_str" : "1059801",
      "id" : 1059801
    }, {
      "name" : "Google",
      "screen_name" : "Google",
      "indices" : [ 39, 46 ],
      "id_str" : "20536157",
      "id" : 20536157
    }, {
      "name" : "Greg Sterling",
      "screen_name" : "gsterling",
      "indices" : [ 118, 128 ],
      "id_str" : "2575811",
      "id" : 2575811
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GDPR",
      "indices" : [ 32, 37 ]
    }, {
      "text" : "EU",
      "indices" : [ 111, 114 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977896485940580352",
  "text" : "RT @sengineland: To comply with #GDPR, @Google asks publishers to manage user-data consent for ad targeting in #EU by @gsterling https:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 22, 29 ],
        "id_str" : "20536157",
        "id" : 20536157
      }, {
        "name" : "Greg Sterling",
        "screen_name" : "gsterling",
        "indices" : [ 101, 111 ],
        "id_str" : "2575811",
        "id" : 2575811
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/sengineland\/status\/977407841647591425\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/9gpghZBBJE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZBzQF5X4AEQRXf.jpg",
        "id_str" : "977407839437316097",
        "id" : 977407839437316097,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZBzQF5X4AEQRXf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 450,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/9gpghZBBJE"
      } ],
      "hashtags" : [ {
        "text" : "GDPR",
        "indices" : [ 15, 20 ]
      }, {
        "text" : "EU",
        "indices" : [ 94, 97 ]
      } ],
      "urls" : [ {
        "indices" : [ 112, 135 ],
        "url" : "https:\/\/t.co\/lNNV9FkDXB",
        "expanded_url" : "https:\/\/selnd.com\/2FXFCO2",
        "display_url" : "selnd.com\/2FXFCO2"
      } ]
    },
    "geo" : { },
    "id_str" : "977407841647591425",
    "text" : "To comply with #GDPR, @Google asks publishers to manage user-data consent for ad targeting in #EU by @gsterling https:\/\/t.co\/lNNV9FkDXB https:\/\/t.co\/9gpghZBBJE",
    "id" : 977407841647591425,
    "created_at" : "2018-03-24 04:52:43 +0000",
    "user" : {
      "name" : "Search Engine Land",
      "screen_name" : "sengineland",
      "protected" : false,
      "id_str" : "1059801",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/679041505714167812\/T98iuCH3_normal.png",
      "id" : 1059801,
      "verified" : true
    }
  },
  "id" : 977896485940580352,
  "created_at" : "2018-03-25 13:14:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977859727463014400",
  "text" : "Weather in Dublin now. \uD83D\uDE0E\uD83C\uDF1E",
  "id" : 977859727463014400,
  "created_at" : "2018-03-25 10:48:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "zeynep tufekci",
      "screen_name" : "zeynep",
      "indices" : [ 3, 10 ],
      "id_str" : "65375759",
      "id" : 65375759
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/l4mdhJ2n6O",
      "expanded_url" : "https:\/\/twitter.com\/jguynn\/status\/977191361454555141",
      "display_url" : "twitter.com\/jguynn\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977856770994589696",
  "text" : "RT @zeynep: You\u2019d be surprised how many people don\u2019t know that Facebook owns Instagram and WhatsApp. https:\/\/t.co\/l4mdhJ2n6O",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/l4mdhJ2n6O",
        "expanded_url" : "https:\/\/twitter.com\/jguynn\/status\/977191361454555141",
        "display_url" : "twitter.com\/jguynn\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977193343997435904",
    "text" : "You\u2019d be surprised how many people don\u2019t know that Facebook owns Instagram and WhatsApp. https:\/\/t.co\/l4mdhJ2n6O",
    "id" : 977193343997435904,
    "created_at" : "2018-03-23 14:40:23 +0000",
    "user" : {
      "name" : "zeynep tufekci",
      "screen_name" : "zeynep",
      "protected" : false,
      "id_str" : "65375759",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/852492056798916608\/bydhBkye_normal.jpg",
      "id" : 65375759,
      "verified" : true
    }
  },
  "id" : 977856770994589696,
  "created_at" : "2018-03-25 10:36:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "HY \u667A\u6167\u6021\u4EBA",
      "screen_name" : "tanhuiyi",
      "indices" : [ 3, 12 ],
      "id_str" : "82807134",
      "id" : 82807134
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977856640350408704",
  "text" : "RT @tanhuiyi: Orchard Road will go smoke-free on July 1, with a ban on lighting up in public areas in the precinct, with the exception of d\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "977529787332767744",
    "geo" : { },
    "id_str" : "977530049749397505",
    "in_reply_to_user_id" : 82807134,
    "text" : "Orchard Road will go smoke-free on July 1, with a ban on lighting up in public areas in the precinct, with the exception of designated smoking areas.",
    "id" : 977530049749397505,
    "in_reply_to_status_id" : 977529787332767744,
    "created_at" : "2018-03-24 12:58:20 +0000",
    "in_reply_to_screen_name" : "tanhuiyi",
    "in_reply_to_user_id_str" : "82807134",
    "user" : {
      "name" : "HY \u667A\u6167\u6021\u4EBA",
      "screen_name" : "tanhuiyi",
      "protected" : false,
      "id_str" : "82807134",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/983740474732113920\/iEU43p56_normal.jpg",
      "id" : 82807134,
      "verified" : false
    }
  },
  "id" : 977856640350408704,
  "created_at" : "2018-03-25 10:36:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "indices" : [ 3, 7 ],
      "id_str" : "380648579",
      "id" : 380648579
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "EarthHour",
      "indices" : [ 116, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977856483147862016",
  "text" : "RT @AFP: Singapore's Marina Bay Sands hotel and resort are plunged into darkness as the lights are switched off for #EarthHour #connect2ear\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AFP\/status\/977533732566466560\/photo\/1",
        "indices" : [ 136, 159 ],
        "url" : "https:\/\/t.co\/VefyybIksK",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZDluGzXkAAT4xx.jpg",
        "id_str" : "977533699402207232",
        "id" : 977533699402207232,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZDluGzXkAAT4xx.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 511
        }, {
          "h" : 1331,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 1331,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 902
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/VefyybIksK"
      } ],
      "hashtags" : [ {
        "text" : "EarthHour",
        "indices" : [ 107, 117 ]
      }, {
        "text" : "connect2earthour",
        "indices" : [ 118, 135 ]
      } ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "977520400006512640",
    "geo" : { },
    "id_str" : "977533732566466560",
    "in_reply_to_user_id" : 380648579,
    "text" : "Singapore's Marina Bay Sands hotel and resort are plunged into darkness as the lights are switched off for #EarthHour #connect2earthour https:\/\/t.co\/VefyybIksK",
    "id" : 977533732566466560,
    "in_reply_to_status_id" : 977520400006512640,
    "created_at" : "2018-03-24 13:12:58 +0000",
    "in_reply_to_screen_name" : "AFP",
    "in_reply_to_user_id_str" : "380648579",
    "user" : {
      "name" : "AFP news agency",
      "screen_name" : "AFP",
      "protected" : false,
      "id_str" : "380648579",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/697343883630481408\/c08JBfBB_normal.jpg",
      "id" : 380648579,
      "verified" : true
    }
  },
  "id" : 977856483147862016,
  "created_at" : "2018-03-25 10:35:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rajesh Suseelan",
      "screen_name" : "rsn",
      "indices" : [ 3, 7 ],
      "id_str" : "14169541",
      "id" : 14169541
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977856093257945088",
  "text" : "RT @rsn: Interesting tidbit : chillies are not native to India. They were brought to India by the Spanish from South America, in the late 1\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 229, 252 ],
        "url" : "https:\/\/t.co\/4ciAawqsrq",
        "expanded_url" : "https:\/\/twitter.com\/KarlreMarks\/status\/977547294697578497",
        "display_url" : "twitter.com\/KarlreMarks\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977565292384718848",
    "text" : "Interesting tidbit : chillies are not native to India. They were brought to India by the Spanish from South America, in the late 15gh century. They were soon cultivated in India &amp; became an inalienable part of Indian cuisine https:\/\/t.co\/4ciAawqsrq",
    "id" : 977565292384718848,
    "created_at" : "2018-03-24 15:18:22 +0000",
    "user" : {
      "name" : "Rajesh Suseelan",
      "screen_name" : "rsn",
      "protected" : false,
      "id_str" : "14169541",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/441724365839687680\/ZBqTuMED_normal.jpeg",
      "id" : 14169541,
      "verified" : false
    }
  },
  "id" : 977856093257945088,
  "created_at" : "2018-03-25 10:33:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "A Mancino-Williams",
      "screen_name" : "Manda_like_wine",
      "indices" : [ 3, 19 ],
      "id_str" : "589621591",
      "id" : 589621591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977855702264963072",
  "text" : "RT @Manda_like_wine: Just passed a mum with her little girl, no older than 7, who was crying over a skinned knee.\nMum: I don't think we nee\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977299937963765761",
    "text" : "Just passed a mum with her little girl, no older than 7, who was crying over a skinned knee.\nMum: I don't think we need to cry over this anymore.\nLittle girl, still crying: This is in NO WAY a WE situation.",
    "id" : 977299937963765761,
    "created_at" : "2018-03-23 21:43:57 +0000",
    "user" : {
      "name" : "A Mancino-Williams",
      "screen_name" : "Manda_like_wine",
      "protected" : false,
      "id_str" : "589621591",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/726939397036220416\/YSfOHn6s_normal.jpg",
      "id" : 589621591,
      "verified" : true
    }
  },
  "id" : 977855702264963072,
  "created_at" : "2018-03-25 10:32:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eddie Du",
      "screen_name" : "Edourdoo",
      "indices" : [ 3, 12 ],
      "id_str" : "20044440",
      "id" : 20044440
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977855428611792896",
  "text" : "RT @Edourdoo: In 1957, 1,275 Singaporeans spoke Mandarin at home, while 1,074,745 identified as native speakers of various CN \u2018dialects\u2019.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977507829987803136",
    "text" : "In 1957, 1,275 Singaporeans spoke Mandarin at home, while 1,074,745 identified as native speakers of various CN \u2018dialects\u2019. \n\nIn 2010, Mandarin was main household language of 1,211,505 individuals aged 5 years and over, whereas only 487,031 individuals spoke \u2018dialects\u2019 at home.",
    "id" : 977507829987803136,
    "created_at" : "2018-03-24 11:30:02 +0000",
    "user" : {
      "name" : "Eddie Du",
      "screen_name" : "Edourdoo",
      "protected" : false,
      "id_str" : "20044440",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000669568751\/56adc18070e25792ea98d90fad615383_normal.jpeg",
      "id" : 20044440,
      "verified" : false
    }
  },
  "id" : 977855428611792896,
  "created_at" : "2018-03-25 10:31:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 15, 38 ],
      "url" : "https:\/\/t.co\/XfsrXqmT3U",
      "expanded_url" : "https:\/\/twitter.com\/Trashvis\/status\/898311866149068800",
      "display_url" : "twitter.com\/Trashvis\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977855305873752065",
  "text" : "\"Let Break Up\" https:\/\/t.co\/XfsrXqmT3U",
  "id" : 977855305873752065,
  "created_at" : "2018-03-25 10:30:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "indices" : [ 3, 15 ],
      "id_str" : "41085467",
      "id" : 41085467
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/977831057855672320\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/PN9xH2ckO7",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZH0JsLUQAAwp5A.jpg",
      "id_str" : "977831041430732800",
      "id" : 977831041430732800,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZH0JsLUQAAwp5A.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 800
      }, {
        "h" : 453,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 533,
        "resize" : "fit",
        "w" : 800
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/PN9xH2ckO7"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 63, 86 ],
      "url" : "https:\/\/t.co\/EGCGHhPZtq",
      "expanded_url" : "https:\/\/tdy.sg\/2DSplDZ",
      "display_url" : "tdy.sg\/2DSplDZ"
    } ]
  },
  "geo" : { },
  "id_str" : "977854237496496128",
  "text" : "RT @TODAYonline: Uber agrees on Southeast Asian sale to Grab  \nhttps:\/\/t.co\/EGCGHhPZtq https:\/\/t.co\/PN9xH2ckO7",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TODAYonline\/status\/977831057855672320\/photo\/1",
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/PN9xH2ckO7",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZH0JsLUQAAwp5A.jpg",
        "id_str" : "977831041430732800",
        "id" : 977831041430732800,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZH0JsLUQAAwp5A.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 533,
          "resize" : "fit",
          "w" : 800
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/PN9xH2ckO7"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 46, 69 ],
        "url" : "https:\/\/t.co\/EGCGHhPZtq",
        "expanded_url" : "https:\/\/tdy.sg\/2DSplDZ",
        "display_url" : "tdy.sg\/2DSplDZ"
      } ]
    },
    "geo" : { },
    "id_str" : "977831057855672320",
    "text" : "Uber agrees on Southeast Asian sale to Grab  \nhttps:\/\/t.co\/EGCGHhPZtq https:\/\/t.co\/PN9xH2ckO7",
    "id" : 977831057855672320,
    "created_at" : "2018-03-25 08:54:26 +0000",
    "user" : {
      "name" : "TODAY",
      "screen_name" : "TODAYonline",
      "protected" : false,
      "id_str" : "41085467",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/606854638801846272\/pi_RUbVK_normal.png",
      "id" : 41085467,
      "verified" : true
    }
  },
  "id" : 977854237496496128,
  "created_at" : "2018-03-25 10:26:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PJ Timmins",
      "screen_name" : "PJTimmins",
      "indices" : [ 3, 13 ],
      "id_str" : "21041794",
      "id" : 21041794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/mSLOeqI3wW",
      "expanded_url" : "http:\/\/ow.ly\/ofEL30j32e4",
      "display_url" : "ow.ly\/ofEL30j32e4"
    } ]
  },
  "geo" : { },
  "id_str" : "977853922621755392",
  "text" : "RT @PJTimmins: Three Ireland to spend \u20AC100m a year on new https:\/\/t.co\/mSLOeqI3wW 5G network",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 43, 66 ],
        "url" : "https:\/\/t.co\/mSLOeqI3wW",
        "expanded_url" : "http:\/\/ow.ly\/ofEL30j32e4",
        "display_url" : "ow.ly\/ofEL30j32e4"
      } ]
    },
    "geo" : { },
    "id_str" : "977850108388823040",
    "text" : "Three Ireland to spend \u20AC100m a year on new https:\/\/t.co\/mSLOeqI3wW 5G network",
    "id" : 977850108388823040,
    "created_at" : "2018-03-25 10:10:08 +0000",
    "user" : {
      "name" : "PJ Timmins",
      "screen_name" : "PJTimmins",
      "protected" : false,
      "id_str" : "21041794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3734032532\/e42d86f363d6c5d46cbe3f673ad13b9e_normal.jpeg",
      "id" : 21041794,
      "verified" : false
    }
  },
  "id" : 977853922621755392,
  "created_at" : "2018-03-25 10:25:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E50",
      "screen_name" : "shimmertje8",
      "indices" : [ 0, 12 ],
      "id_str" : "237988530",
      "id" : 237988530
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "977847272909160449",
  "geo" : { },
  "id_str" : "977847490509762560",
  "in_reply_to_user_id" : 237988530,
  "text" : "@shimmertje8 Thanks for the feedback. I aware this could be a depart from tradition where wedding invite cards are sent via post.",
  "id" : 977847490509762560,
  "in_reply_to_status_id" : 977847272909160449,
  "created_at" : "2018-03-25 09:59:43 +0000",
  "in_reply_to_screen_name" : "shimmertje8",
  "in_reply_to_user_id_str" : "237988530",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/CXX6qyuASk",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/977523280184856580",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977845502506164230",
  "text" : "Instead of writing and printing cards, all invite is done via SMS. https:\/\/t.co\/CXX6qyuASk",
  "id" : 977845502506164230,
  "created_at" : "2018-03-25 09:51:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maaret Pyh\u00E4j\u00E4rvi",
      "screen_name" : "maaretp",
      "indices" : [ 3, 11 ],
      "id_str" : "143488289",
      "id" : 143488289
    }, {
      "name" : "Mary Poppendieck",
      "screen_name" : "mpoppendieck",
      "indices" : [ 81, 94 ],
      "id_str" : "200631648",
      "id" : 200631648
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977838476887576577",
  "text" : "RT @maaretp: It\u2019s not a microservice if you can\u2019t deploy it independently, tells @mpoppendieck to a room full of people where no one can do\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Mary Poppendieck",
        "screen_name" : "mpoppendieck",
        "indices" : [ 68, 81 ],
        "id_str" : "200631648",
        "id" : 200631648
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973220810973499394",
    "text" : "It\u2019s not a microservice if you can\u2019t deploy it independently, tells @mpoppendieck to a room full of people where no one can do that yet.",
    "id" : 973220810973499394,
    "created_at" : "2018-03-12 15:34:57 +0000",
    "user" : {
      "name" : "Maaret Pyh\u00E4j\u00E4rvi",
      "screen_name" : "maaretp",
      "protected" : false,
      "id_str" : "143488289",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024731980905304064\/zunzxh8__normal.jpg",
      "id" : 143488289,
      "verified" : false
    }
  },
  "id" : 977838476887576577,
  "created_at" : "2018-03-25 09:23:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/8rLJvFR5Q4",
      "expanded_url" : "https:\/\/www.msn.com\/en-ie\/money\/homesandproperty\/irelands-top-10-most-expensive-penthouses-and-the-views-are-magnificent\/ar-BBJLRdg",
      "display_url" : "msn.com\/en-ie\/money\/ho\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977838133436928000",
  "text" : "At such price tag, you can get a better deal in Australia (with sunshine throw in) https:\/\/t.co\/8rLJvFR5Q4",
  "id" : 977838133436928000,
  "created_at" : "2018-03-25 09:22:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/instagram.com\" rel=\"nofollow\"\u003EInstagram\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "together4yes",
      "indices" : [ 0, 13 ]
    } ],
    "urls" : [ {
      "indices" : [ 76, 99 ],
      "url" : "https:\/\/t.co\/c3TxTVXtLz",
      "expanded_url" : "https:\/\/www.instagram.com\/p\/BgvZGErgcm_\/",
      "display_url" : "instagram.com\/p\/BgvZGErgcm_\/"
    } ]
  },
  "geo" : { },
  "id_str" : "977834442369716224",
  "text" : "#together4yes Picked this up yesterday at Grafton Street. @ Dublin, Ireland https:\/\/t.co\/c3TxTVXtLz",
  "id" : 977834442369716224,
  "created_at" : "2018-03-25 09:07:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Linnea Dunne",
      "screen_name" : "linneadunne",
      "indices" : [ 3, 15 ],
      "id_str" : "20404115",
      "id" : 20404115
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "repealthe8th",
      "indices" : [ 70, 83 ]
    }, {
      "text" : "Together4Yes",
      "indices" : [ 95, 108 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977833138020855808",
  "text" : "RT @linneadunne: Bus driver just let me travel for free because of my #repealthe8th badge. \uD83D\uDCAA\uD83D\uDC96\uD83D\uDE4C #Together4Yes",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "repealthe8th",
        "indices" : [ 53, 66 ]
      }, {
        "text" : "Together4Yes",
        "indices" : [ 78, 91 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977151686191013890",
    "text" : "Bus driver just let me travel for free because of my #repealthe8th badge. \uD83D\uDCAA\uD83D\uDC96\uD83D\uDE4C #Together4Yes",
    "id" : 977151686191013890,
    "created_at" : "2018-03-23 11:54:51 +0000",
    "user" : {
      "name" : "Linnea Dunne",
      "screen_name" : "linneadunne",
      "protected" : false,
      "id_str" : "20404115",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/998471778589540352\/50bZ-02k_normal.jpg",
      "id" : 20404115,
      "verified" : false
    }
  },
  "id" : 977833138020855808,
  "created_at" : "2018-03-25 09:02:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Westside Fireman",
      "screen_name" : "WestsideFireman",
      "indices" : [ 3, 19 ],
      "id_str" : "1580986370",
      "id" : 1580986370
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977805930447491074",
  "text" : "RT @WestsideFireman: I carried an M-4 in the Afghanistan desert for almost a year. If you feel you need this or another weapon of war like\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/WestsideFireman\/status\/977591093083754498\/photo\/1",
        "indices" : [ 282, 305 ],
        "url" : "https:\/\/t.co\/Z4xauYDbAY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZEZ6gvXkAAyCco.jpg",
        "id_str" : "977591087127826432",
        "id" : 977591087127826432,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZEZ6gvXkAAyCco.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 905,
          "resize" : "fit",
          "w" : 323
        }, {
          "h" : 905,
          "resize" : "fit",
          "w" : 323
        }, {
          "h" : 905,
          "resize" : "fit",
          "w" : 323
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 243
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Z4xauYDbAY"
      } ],
      "hashtags" : [ {
        "text" : "VetsForGunReform",
        "indices" : [ 264, 281 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977591093083754498",
    "text" : "I carried an M-4 in the Afghanistan desert for almost a year. If you feel you need this or another weapon of war like it to protect your home; you need to re-evaluate the way you\u2019re living your life &amp; make fewer enemies. These have no place in civilian hands.\n#VetsForGunReform https:\/\/t.co\/Z4xauYDbAY",
    "id" : 977591093083754498,
    "created_at" : "2018-03-24 17:00:54 +0000",
    "user" : {
      "name" : "Westside Fireman",
      "screen_name" : "WestsideFireman",
      "protected" : false,
      "id_str" : "1580986370",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000110698318\/fe06e94824672648962a113b6b600ec9_normal.jpeg",
      "id" : 1580986370,
      "verified" : false
    }
  },
  "id" : 977805930447491074,
  "created_at" : "2018-03-25 07:14:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maryrose Lyons",
      "screen_name" : "maryrose",
      "indices" : [ 0, 9 ],
      "id_str" : "1291131",
      "id" : 1291131
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "977626322674610176",
  "geo" : { },
  "id_str" : "977636609662308352",
  "in_reply_to_user_id" : 1291131,
  "text" : "@maryrose I felt nothing maybe I seldom hang around there.",
  "id" : 977636609662308352,
  "in_reply_to_status_id" : 977626322674610176,
  "created_at" : "2018-03-24 20:01:46 +0000",
  "in_reply_to_screen_name" : "maryrose",
  "in_reply_to_user_id_str" : "1291131",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/977538245864185857\/photo\/1",
      "indices" : [ 21, 44 ],
      "url" : "https:\/\/t.co\/Cqro227u7m",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZDp2QBX4AA0cWE.jpg",
      "id_str" : "977538237362331648",
      "id" : 977538237362331648,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZDp2QBX4AA0cWE.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Cqro227u7m"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977538245864185857",
  "text" : "Got this email today https:\/\/t.co\/Cqro227u7m",
  "id" : 977538245864185857,
  "created_at" : "2018-03-24 13:30:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mark Stanley",
      "screen_name" : "markstanley",
      "indices" : [ 3, 15 ],
      "id_str" : "14132298",
      "id" : 14132298
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/iIakAgw9lZ",
      "expanded_url" : "http:\/\/jrnl.ie\/3918123",
      "display_url" : "jrnl.ie\/3918123"
    } ]
  },
  "geo" : { },
  "id_str" : "977535621873979392",
  "text" : "RT @markstanley: Love this idea  https:\/\/t.co\/iIakAgw9lZ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 39 ],
        "url" : "https:\/\/t.co\/iIakAgw9lZ",
        "expanded_url" : "http:\/\/jrnl.ie\/3918123",
        "display_url" : "jrnl.ie\/3918123"
      } ]
    },
    "geo" : { },
    "id_str" : "977527670652194816",
    "text" : "Love this idea  https:\/\/t.co\/iIakAgw9lZ",
    "id" : 977527670652194816,
    "created_at" : "2018-03-24 12:48:52 +0000",
    "user" : {
      "name" : "Mark Stanley",
      "screen_name" : "markstanley",
      "protected" : false,
      "id_str" : "14132298",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/749953968957251585\/KSsphMJ__normal.jpg",
      "id" : 14132298,
      "verified" : false
    }
  },
  "id" : 977535621873979392,
  "created_at" : "2018-03-24 13:20:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Anna Scott",
      "screen_name" : "Anna_D_Scott",
      "indices" : [ 3, 16 ],
      "id_str" : "111041991",
      "id" : 111041991
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977535036495974401",
  "text" : "RT @Anna_D_Scott: 'It'd be a shame if misuse of personal data meant we withdrew permission when data collection can help us all as a societ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jeni Tennison",
        "screen_name" : "JeniT",
        "indices" : [ 164, 170 ],
        "id_str" : "14727078",
        "id" : 14727078
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Census",
        "indices" : [ 127, 134 ]
      }, {
        "text" : "CambridgeAnalytica",
        "indices" : [ 236, 255 ]
      } ],
      "urls" : [ {
        "indices" : [ 212, 235 ],
        "url" : "https:\/\/t.co\/KRagVyaXay",
        "expanded_url" : "https:\/\/www.bbc.co.uk\/iplayer\/episode\/b09w7mwj\/breakfast-24032018",
        "display_url" : "bbc.co.uk\/iplayer\/episod\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977534107054993409",
    "text" : "'It'd be a shame if misuse of personal data meant we withdrew permission when data collection can help us all as a society, eg #Census data for town planning' Hear @JeniT's full BBC Breakfast interview [at 1:24] https:\/\/t.co\/KRagVyaXay\n#CambridgeAnalytica",
    "id" : 977534107054993409,
    "created_at" : "2018-03-24 13:14:27 +0000",
    "user" : {
      "name" : "Anna Scott",
      "screen_name" : "Anna_D_Scott",
      "protected" : false,
      "id_str" : "111041991",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1000068829253431296\/3gZ6kRNT_normal.jpg",
      "id" : 111041991,
      "verified" : false
    }
  },
  "id" : 977535036495974401,
  "created_at" : "2018-03-24 13:18:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977523280184856580",
  "text" : "Anyone getting marry soon? I working on a solution that going to save u up to $300 when it comes to RSVP guests.",
  "id" : 977523280184856580,
  "created_at" : "2018-03-24 12:31:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "broadsheet_ie",
      "screen_name" : "broadsheet_ie",
      "indices" : [ 3, 17 ],
      "id_str" : "169585130",
      "id" : 169585130
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/broadsheet_ie\/status\/977132719091191808\/photo\/1",
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/CrHi1ZaBUc",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY95B1sUMAACudT.jpg",
      "id_str" : "977132716662468608",
      "id" : 977132716662468608,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY95B1sUMAACudT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 434,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 434,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 434,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 434,
        "resize" : "fit",
        "w" : 600
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/CrHi1ZaBUc"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 42, 65 ],
      "url" : "https:\/\/t.co\/xYx8E1kGjc",
      "expanded_url" : "http:\/\/www.broadsheet.ie\/2018\/03\/23\/thank-you-nurse-polly\/",
      "display_url" : "broadsheet.ie\/2018\/03\/23\/tha\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977501597000458240",
  "text" : "RT @broadsheet_ie: Thank You, Nurse\u00A0Polly https:\/\/t.co\/xYx8E1kGjc https:\/\/t.co\/CrHi1ZaBUc",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/publicize.wp.com\/\" rel=\"nofollow\"\u003EWordPress.com\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/broadsheet_ie\/status\/977132719091191808\/photo\/1",
        "indices" : [ 47, 70 ],
        "url" : "https:\/\/t.co\/CrHi1ZaBUc",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY95B1sUMAACudT.jpg",
        "id_str" : "977132716662468608",
        "id" : 977132716662468608,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY95B1sUMAACudT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 434,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 434,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 434,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 434,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/CrHi1ZaBUc"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/xYx8E1kGjc",
        "expanded_url" : "http:\/\/www.broadsheet.ie\/2018\/03\/23\/thank-you-nurse-polly\/",
        "display_url" : "broadsheet.ie\/2018\/03\/23\/tha\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977132719091191808",
    "text" : "Thank You, Nurse\u00A0Polly https:\/\/t.co\/xYx8E1kGjc https:\/\/t.co\/CrHi1ZaBUc",
    "id" : 977132719091191808,
    "created_at" : "2018-03-23 10:39:29 +0000",
    "user" : {
      "name" : "broadsheet_ie",
      "screen_name" : "broadsheet_ie",
      "protected" : false,
      "id_str" : "169585130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459448067675017218\/7lJx6AX2_normal.jpeg",
      "id" : 169585130,
      "verified" : false
    }
  },
  "id" : 977501597000458240,
  "created_at" : "2018-03-24 11:05:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Springwise Intelligence",
      "screen_name" : "springwise",
      "indices" : [ 3, 14 ],
      "id_str" : "14863500",
      "id" : 14863500
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/springwise\/status\/977425540729753601\/photo\/1",
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/EWA3bZKtQv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZCDWT1WkAAcShH.jpg",
      "id_str" : "977425538443808768",
      "id" : 977425538443808768,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZCDWT1WkAAcShH.jpg",
      "sizes" : [ {
        "h" : 750,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 750,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 398,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 703,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EWA3bZKtQv"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/DOF8f0kjK8",
      "expanded_url" : "https:\/\/www.springwise.com\/organization-provides-homeless-people-address\/",
      "display_url" : "springwise.com\/organization-p\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977448156140068864",
  "text" : "RT @springwise: Organization provides homeless people with an address \nhttps:\/\/t.co\/DOF8f0kjK8 https:\/\/t.co\/EWA3bZKtQv",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/springwise\/status\/977425540729753601\/photo\/1",
        "indices" : [ 79, 102 ],
        "url" : "https:\/\/t.co\/EWA3bZKtQv",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZCDWT1WkAAcShH.jpg",
        "id_str" : "977425538443808768",
        "id" : 977425538443808768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZCDWT1WkAAcShH.jpg",
        "sizes" : [ {
          "h" : 750,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 750,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 398,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 703,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/EWA3bZKtQv"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/DOF8f0kjK8",
        "expanded_url" : "https:\/\/www.springwise.com\/organization-provides-homeless-people-address\/",
        "display_url" : "springwise.com\/organization-p\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977425540729753601",
    "text" : "Organization provides homeless people with an address \nhttps:\/\/t.co\/DOF8f0kjK8 https:\/\/t.co\/EWA3bZKtQv",
    "id" : 977425540729753601,
    "created_at" : "2018-03-24 06:03:03 +0000",
    "user" : {
      "name" : "Springwise Intelligence",
      "screen_name" : "springwise",
      "protected" : false,
      "id_str" : "14863500",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/798820732222382080\/ngBH8DW0_normal.jpg",
      "id" : 14863500,
      "verified" : false
    }
  },
  "id" : 977448156140068864,
  "created_at" : "2018-03-24 07:32:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "indices" : [ 3, 16 ],
      "id_str" : "36889519",
      "id" : 36889519
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/977346817657483264\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/6msvVNUDy9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DZA7wA-UMAAmFYI.jpg",
      "id_str" : "977346815220527104",
      "id" : 977346815220527104,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZA7wA-UMAAmFYI.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 424,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6msvVNUDy9"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977447432450658305",
  "text" : "RT @RaminNasibov: This hotel in Singapore is how all cities should look. https:\/\/t.co\/6msvVNUDy9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/977346817657483264\/photo\/1",
        "indices" : [ 55, 78 ],
        "url" : "https:\/\/t.co\/6msvVNUDy9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DZA7wA-UMAAmFYI.jpg",
        "id_str" : "977346815220527104",
        "id" : 977346815220527104,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DZA7wA-UMAAmFYI.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 640
        }, {
          "h" : 424,
          "resize" : "fit",
          "w" : 640
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6msvVNUDy9"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977346817657483264",
    "text" : "This hotel in Singapore is how all cities should look. https:\/\/t.co\/6msvVNUDy9",
    "id" : 977346817657483264,
    "created_at" : "2018-03-24 00:50:14 +0000",
    "user" : {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "protected" : false,
      "id_str" : "36889519",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/896399812450557953\/gP3PRbXP_normal.jpg",
      "id" : 36889519,
      "verified" : true
    }
  },
  "id" : 977447432450658305,
  "created_at" : "2018-03-24 07:30:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "indices" : [ 3, 11 ],
      "id_str" : "47333",
      "id" : 47333
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977447310027378688",
  "text" : "RT @topgold: \"Subscribe with Google\" includes Les \u00C9chos, Fairfax Media, Le Figaro, NYT, FT, WaPo, Gatehouse Media, Grupo Globo, The Mainich\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 225, 248 ],
        "url" : "https:\/\/t.co\/xayQr0y9rP",
        "expanded_url" : "https:\/\/blog.google\/topics\/google-news-initiative\/introducing-subscribe-google\/",
        "display_url" : "blog.google\/topics\/google-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977349951587061760",
    "text" : "\"Subscribe with Google\" includes Les \u00C9chos, Fairfax Media, Le Figaro, NYT, FT, WaPo, Gatehouse Media, Grupo Globo, The Mainichi, McClatchy, La Naci\u00F3n, NRC Media, Le Parisien, Reforma, la Repubblica, The Telegraph, USA TODAY. https:\/\/t.co\/xayQr0y9rP",
    "id" : 977349951587061760,
    "created_at" : "2018-03-24 01:02:41 +0000",
    "user" : {
      "name" : "Bernie Goldbach",
      "screen_name" : "topgold",
      "protected" : false,
      "id_str" : "47333",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2333398164\/zm5v8guw0rfdbwn412x8_normal.png",
      "id" : 47333,
      "verified" : false
    }
  },
  "id" : 977447310027378688,
  "created_at" : "2018-03-24 07:29:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/gleam.io\" rel=\"nofollow\"\u003EGleam Competition App\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Pi Hut",
      "screen_name" : "ThePiHut",
      "indices" : [ 3, 12 ],
      "id_str" : "761589870",
      "id" : 761589870
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977318307182264320",
  "text" : "RT @ThePiHut: We're running a giveaway to celebrate the release of the new Raspberry Pi 3 B+! Five (oh yes five!) Pi 3 B+ boards are up for\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ThePiHut\/status\/973847817843367938\/photo\/1",
        "indices" : [ 177, 200 ],
        "url" : "https:\/\/t.co\/zP2A04IkxM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYPNbb5XUAEvJdW.jpg",
        "id_str" : "973847815670681601",
        "id" : 973847815670681601,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYPNbb5XUAEvJdW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        }, {
          "h" : 600,
          "resize" : "fit",
          "w" : 600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zP2A04IkxM"
      } ],
      "hashtags" : [ {
        "text" : "PiDay",
        "indices" : [ 170, 176 ]
      } ],
      "urls" : [ {
        "indices" : [ 146, 169 ],
        "url" : "https:\/\/t.co\/ivYB8UJBLZ",
        "expanded_url" : "https:\/\/buff.ly\/2Dq1cUU",
        "display_url" : "buff.ly\/2Dq1cUU"
      } ]
    },
    "geo" : { },
    "id_str" : "973847817843367938",
    "text" : "We're running a giveaway to celebrate the release of the new Raspberry Pi 3 B+! Five (oh yes five!) Pi 3 B+ boards are up for grabs - enter here: https:\/\/t.co\/ivYB8UJBLZ #PiDay https:\/\/t.co\/zP2A04IkxM",
    "id" : 973847817843367938,
    "created_at" : "2018-03-14 09:06:27 +0000",
    "user" : {
      "name" : "The Pi Hut",
      "screen_name" : "ThePiHut",
      "protected" : false,
      "id_str" : "761589870",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/459370515426074626\/33KBfE8u_normal.jpeg",
      "id" : 761589870,
      "verified" : false
    }
  },
  "id" : 977318307182264320,
  "created_at" : "2018-03-23 22:56:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 108, 131 ],
      "url" : "https:\/\/t.co\/Fb4S5yCD3z",
      "expanded_url" : "https:\/\/www.reuters.com\/article\/us-usa-china-southchinasea\/exclusive-u-s-warship-sails-near-disputed-islands-in-south-china-sea-officials-say-idUSKBN1GZ0VY",
      "display_url" : "reuters.com\/article\/us-usa\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977315486034747392",
  "text" : "Exclusive: U.S. warship sails near disputed South China Sea island, officials say | Article [AMP] | Reuters https:\/\/t.co\/Fb4S5yCD3z",
  "id" : 977315486034747392,
  "created_at" : "2018-03-23 22:45:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ned Batchelder",
      "screen_name" : "nedbat",
      "indices" : [ 3, 10 ],
      "id_str" : "16273417",
      "id" : 16273417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977264565644877825",
  "text" : "RT @nedbat: Some days, being a Python expert means, \u201Creally good at running pip freeze and looking up code on GitHub.\u201D",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977257992914653184",
    "text" : "Some days, being a Python expert means, \u201Creally good at running pip freeze and looking up code on GitHub.\u201D",
    "id" : 977257992914653184,
    "created_at" : "2018-03-23 18:57:16 +0000",
    "user" : {
      "name" : "Ned Batchelder",
      "screen_name" : "nedbat",
      "protected" : false,
      "id_str" : "16273417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/60300066\/face-jr-square_normal.jpg",
      "id" : 16273417,
      "verified" : false
    }
  },
  "id" : 977264565644877825,
  "created_at" : "2018-03-23 19:23:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dslearnings",
      "indices" : [ 17, 29 ]
    } ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/CTzdOZcnrW",
      "expanded_url" : "https:\/\/twitter.com\/SteveDX_\/status\/977250409671782400",
      "display_url" : "twitter.com\/SteveDX_\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977252486540136450",
  "text" : "You are welcome. #Dslearnings https:\/\/t.co\/CTzdOZcnrW",
  "id" : 977252486540136450,
  "created_at" : "2018-03-23 18:35:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Professional Argologist",
      "screen_name" : "Tesseraconteur",
      "indices" : [ 3, 18 ],
      "id_str" : "281670339",
      "id" : 281670339
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977241676572971009",
  "text" : "RT @Tesseraconteur: fun fact: nestle owns the rights to a clean water source close to Flint, Michigan where children are being poisoned w l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 254, 277 ],
        "url" : "https:\/\/t.co\/u5uDca3jde",
        "expanded_url" : "https:\/\/twitter.com\/nestle\/status\/976742046714261505?s=21",
        "display_url" : "twitter.com\/nestle\/status\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976877487807516673",
    "text" : "fun fact: nestle owns the rights to a clean water source close to Flint, Michigan where children are being poisoned w lead contaminated water\n\nresidents don\u2019t get to drink the clean water bc it\u2019s exclusively used to bottle Nestle\u2019s most expensive brand\n\nhttps:\/\/t.co\/u5uDca3jde",
    "id" : 976877487807516673,
    "created_at" : "2018-03-22 17:45:17 +0000",
    "user" : {
      "name" : "Professional Argologist",
      "screen_name" : "Tesseraconteur",
      "protected" : false,
      "id_str" : "281670339",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1034734869216092161\/s7R8y1qi_normal.jpg",
      "id" : 281670339,
      "verified" : false
    }
  },
  "id" : 977241676572971009,
  "created_at" : "2018-03-23 17:52:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "indices" : [ 3, 13 ],
      "id_str" : "10737",
      "id" : 10737
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977224048953446400",
  "text" : "RT @patphelan: Blogger writes list of Irish top 50 marketers, misses loads, no criteria disclosed, \u201Ctop 50 marketers\u201D go wild celebrating a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977126080401289216",
    "text" : "Blogger writes list of Irish top 50 marketers, misses loads, no criteria disclosed, \u201Ctop 50 marketers\u201D go wild celebrating and all link to article, wonder who the marketer is here? I know \uD83D\uDE02",
    "id" : 977126080401289216,
    "created_at" : "2018-03-23 10:13:06 +0000",
    "user" : {
      "name" : "Pat",
      "screen_name" : "patphelan",
      "protected" : false,
      "id_str" : "10737",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1040211830617108481\/Da2QRIvE_normal.jpg",
      "id" : 10737,
      "verified" : true
    }
  },
  "id" : 977224048953446400,
  "created_at" : "2018-03-23 16:42:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mothership.sg",
      "screen_name" : "MothershipSG",
      "indices" : [ 3, 16 ],
      "id_str" : "1619325942",
      "id" : 1619325942
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/MothershipSG\/status\/976857495250653187\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/xH4Y2lXXwN",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5-mTxXkAIOR5G.jpg",
      "id_str" : "976857365793443842",
      "id" : 976857365793443842,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5-mTxXkAIOR5G.jpg",
      "sizes" : [ {
        "h" : 489,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 462,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 489,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 489,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/xH4Y2lXXwN"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977199840366092288",
  "text" : "RT @MothershipSG: Memes about Facebook that are safe to post on Twitter https:\/\/t.co\/xH4Y2lXXwN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MothershipSG\/status\/976857495250653187\/photo\/1",
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/xH4Y2lXXwN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5-mTxXkAIOR5G.jpg",
        "id_str" : "976857365793443842",
        "id" : 976857365793443842,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5-mTxXkAIOR5G.jpg",
        "sizes" : [ {
          "h" : 489,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 462,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 489,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 489,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/xH4Y2lXXwN"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976857495250653187",
    "text" : "Memes about Facebook that are safe to post on Twitter https:\/\/t.co\/xH4Y2lXXwN",
    "id" : 976857495250653187,
    "created_at" : "2018-03-22 16:25:50 +0000",
    "user" : {
      "name" : "Mothership.sg",
      "screen_name" : "MothershipSG",
      "protected" : false,
      "id_str" : "1619325942",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/880244811273388032\/mzvF03Lz_normal.jpg",
      "id" : 1619325942,
      "verified" : true
    }
  },
  "id" : 977199840366092288,
  "created_at" : "2018-03-23 15:06:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977171259623641089",
  "text" : "I realise when I offer my service I am also competing with Google Search -someone motivated enough to search for the solution and decide to do it themselves.",
  "id" : 977171259623641089,
  "created_at" : "2018-03-23 13:12:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/MYw663vtJr",
      "expanded_url" : "https:\/\/support.google.com\/analytics\/answer\/2763052?hl=en&ref_topic=2919631",
      "display_url" : "support.google.com\/analytics\/answ\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977166440292896769",
  "text" : "GDPR considers an IP address to be data that can be used to identify a person. Google made it easy to anonymize IP adressess https:\/\/t.co\/MYw663vtJr",
  "id" : 977166440292896769,
  "created_at" : "2018-03-23 12:53:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977157517565419522",
  "text" : "RT @ronanlyons: Trinity is holding a very relevant seminar on \"The Future of News in an Age of Social Media\", on April 11. Among those spea\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "mark little",
        "screen_name" : "marklittlenews",
        "indices" : [ 142, 157 ],
        "id_str" : "59503113",
        "id" : 59503113
      }, {
        "name" : "Vinay Nair",
        "screen_name" : "vinaynair",
        "indices" : [ 162, 172 ],
        "id_str" : "19442209",
        "id" : 19442209
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 215, 238 ],
        "url" : "https:\/\/t.co\/dJNyYeJ3XF",
        "expanded_url" : "http:\/\/www.tcd.ie\/ssp\/events\/lectures\/social-media-04-18\/index.php",
        "display_url" : "tcd.ie\/ssp\/events\/lec\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "977134668452716544",
    "text" : "Trinity is holding a very relevant seminar on \"The Future of News in an Age of Social Media\", on April 11. Among those speaking will be alums @marklittlenews and @vinaynair. More info (and link to register) here:   https:\/\/t.co\/dJNyYeJ3XF",
    "id" : 977134668452716544,
    "created_at" : "2018-03-23 10:47:13 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 977157517565419522,
  "created_at" : "2018-03-23 12:18:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "indices" : [ 3, 11 ],
      "id_str" : "1000417794",
      "id" : 1000417794
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoogleAnalytics",
      "indices" : [ 43, 59 ]
    }, {
      "text" : "GDPR",
      "indices" : [ 66, 71 ]
    }, {
      "text" : "hireyap",
      "indices" : [ 83, 91 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977126956264239105",
  "text" : "RT @hireyap: If you need help to configure #GoogleAnalytics to be #GDPR compliant, #hireyap",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "GoogleAnalytics",
        "indices" : [ 30, 46 ]
      }, {
        "text" : "GDPR",
        "indices" : [ 53, 58 ]
      }, {
        "text" : "hireyap",
        "indices" : [ 70, 78 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "977125001018138624",
    "text" : "If you need help to configure #GoogleAnalytics to be #GDPR compliant, #hireyap",
    "id" : 977125001018138624,
    "created_at" : "2018-03-23 10:08:49 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 977126956264239105,
  "created_at" : "2018-03-23 10:16:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/fc5rSfVV8B",
      "expanded_url" : "https:\/\/twitter.com\/clareconway\/status\/977120874234081280",
      "display_url" : "twitter.com\/clareconway\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977126472317128704",
  "text" : "Let tear down the barrier to integrations. If you take people in, why these barriers? https:\/\/t.co\/fc5rSfVV8B",
  "id" : 977126472317128704,
  "created_at" : "2018-03-23 10:14:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cork City Fire Brigade",
      "screen_name" : "CorkCityFire",
      "indices" : [ 3, 16 ],
      "id_str" : "1427961692",
      "id" : 1427961692
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977116281747968000",
  "text" : "RT @CorkCityFire: Calling All Community and Voluntary Groups\nAfter a recent upgrade of our current operational A.E.D.S. We giving away 6 ex\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/CorkCityFire\/status\/976825444266897408\/photo\/1",
        "indices" : [ 268, 291 ],
        "url" : "https:\/\/t.co\/Uam7bR19lC",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY5gWvVW4AAwRjc.jpg",
        "id_str" : "976824112965410816",
        "id" : 976824112965410816,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY5gWvVW4AAwRjc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1600,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Uam7bR19lC"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976825444266897408",
    "text" : "Calling All Community and Voluntary Groups\nAfter a recent upgrade of our current operational A.E.D.S. We giving away 6 ex operational  Laerdal FR2 defibrillators.\nEmail to fireoff@corkcity.ie by 31\/03\/2018 stating the following:\nGroup location, size &amp; Charity No. https:\/\/t.co\/Uam7bR19lC",
    "id" : 976825444266897408,
    "created_at" : "2018-03-22 14:18:29 +0000",
    "user" : {
      "name" : "Cork City Fire Brigade",
      "screen_name" : "CorkCityFire",
      "protected" : false,
      "id_str" : "1427961692",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1031887969320361984\/1EwzUXOy_normal.jpg",
      "id" : 1427961692,
      "verified" : false
    }
  },
  "id" : 977116281747968000,
  "created_at" : "2018-03-23 09:34:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/oTf5vESaID",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/977104283610112002",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977106094345670656",
  "text" : "How different ad formats influence consumers differently based on their movement down the conversion funnel. \uD83D\uDE07 https:\/\/t.co\/oTf5vESaID",
  "id" : 977106094345670656,
  "created_at" : "2018-03-23 08:53:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 188, 211 ],
      "url" : "https:\/\/t.co\/Rzumggl9Lf",
      "expanded_url" : "http:\/\/repository.cmu.edu\/cgi\/viewcontent.cgi?article=1399&context=heinzworks",
      "display_url" : "repository.cmu.edu\/cgi\/viewconten\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "977104283610112002",
  "text" : "Using Hidden Markov Model (HMM) to capture user's deliberation process and movement down the conversion funnel as a result of different ad exposures to address the attribution issue [PDF] https:\/\/t.co\/Rzumggl9Lf",
  "id" : 977104283610112002,
  "created_at" : "2018-03-23 08:46:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "aj \u26A1\uFE0F \uD83C\uDF5C",
      "screen_name" : "ajlkn",
      "indices" : [ 3, 9 ],
      "id_str" : "55303867",
      "id" : 55303867
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977093761477087234",
  "text" : "RT @ajlkn: Beginner devs: if you find yourself routinely looking up seemingly basic things like\n\nremove last character from string\nfind val\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976853917312090112",
    "text" : "Beginner devs: if you find yourself routinely looking up seemingly basic things like\n\nremove last character from string\nfind value in array\nreverse sort array\n\n... don't feel bad cause these are literally things I've googled recently\n\n(with 10+ years experience! \uD83E\uDD23)",
    "id" : 976853917312090112,
    "created_at" : "2018-03-22 16:11:37 +0000",
    "user" : {
      "name" : "aj \u26A1\uFE0F \uD83C\uDF5C",
      "screen_name" : "ajlkn",
      "protected" : false,
      "id_str" : "55303867",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/738608465212366849\/etPIzWeg_normal.jpg",
      "id" : 55303867,
      "verified" : false
    }
  },
  "id" : 977093761477087234,
  "created_at" : "2018-03-23 08:04:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ruben Schade \uD83D\uDD30",
      "screen_name" : "Rubenerd",
      "indices" : [ 0, 9 ],
      "id_str" : "875971",
      "id" : 875971
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "977085317835407360",
  "geo" : { },
  "id_str" : "977088776420970496",
  "in_reply_to_user_id" : 875971,
  "text" : "@Rubenerd WHat your take on this machine? I suppose you are running Ubuntu 16.04?",
  "id" : 977088776420970496,
  "in_reply_to_status_id" : 977085317835407360,
  "created_at" : "2018-03-23 07:44:52 +0000",
  "in_reply_to_screen_name" : "Rubenerd",
  "in_reply_to_user_id_str" : "875971",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Maggie Resists Trump",
      "screen_name" : "Stop_Trump20",
      "indices" : [ 3, 16 ],
      "id_str" : "3334843313",
      "id" : 3334843313
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Stop_Trump20\/status\/976554634847408129\/video\/1",
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/DvNIzSviM1",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/976554447760445441\/pu\/img\/Dlh1EoZKVxPFhseZ.jpg",
      "id_str" : "976554447760445441",
      "id" : 976554447760445441,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/976554447760445441\/pu\/img\/Dlh1EoZKVxPFhseZ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 480
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 480
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/DvNIzSviM1"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "977079924036198401",
  "text" : "RT @Stop_Trump20: First Lady of Poland needs some kind of award for this....\uD83D\uDE02\uD83D\uDE02\uD83D\uDE02 https:\/\/t.co\/DvNIzSviM1",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Stop_Trump20\/status\/976554634847408129\/video\/1",
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/DvNIzSviM1",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/976554447760445441\/pu\/img\/Dlh1EoZKVxPFhseZ.jpg",
        "id_str" : "976554447760445441",
        "id" : 976554447760445441,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/976554447760445441\/pu\/img\/Dlh1EoZKVxPFhseZ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 480
        }, {
          "h" : 414,
          "resize" : "fit",
          "w" : 480
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DvNIzSviM1"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976554634847408129",
    "text" : "First Lady of Poland needs some kind of award for this....\uD83D\uDE02\uD83D\uDE02\uD83D\uDE02 https:\/\/t.co\/DvNIzSviM1",
    "id" : 976554634847408129,
    "created_at" : "2018-03-21 20:22:23 +0000",
    "user" : {
      "name" : "Maggie Resists Trump",
      "screen_name" : "Stop_Trump20",
      "protected" : false,
      "id_str" : "3334843313",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023998532238434304\/u3FgZBF7_normal.jpg",
      "id" : 3334843313,
      "verified" : false
    }
  },
  "id" : 977079924036198401,
  "created_at" : "2018-03-23 07:09:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/976983743629062144\/photo\/1",
      "indices" : [ 221, 244 ],
      "url" : "https:\/\/t.co\/yaNER9HvZM",
      "media_url" : "http:\/\/pbs.twimg.com\/tweet_video_thumb\/DY7xf6AXcAA9m2y.jpg",
      "id_str" : "976983699635007488",
      "id" : 976983699635007488,
      "media_url_https" : "https:\/\/pbs.twimg.com\/tweet_video_thumb\/DY7xf6AXcAA9m2y.jpg",
      "sizes" : [ {
        "h" : 340,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 620
      }, {
        "h" : 340,
        "resize" : "fit",
        "w" : 620
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/yaNER9HvZM"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976983743629062144",
  "text" : "\"DUBLIN FIREFIGHTERS ARE worried about a lack of long ladders for high-rise buildings and have revealed that only one may be available on some days in the capital.\" Let count the number of high-rise building in Dublin... https:\/\/t.co\/yaNER9HvZM",
  "id" : 976983743629062144,
  "created_at" : "2018-03-23 00:47:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sgSMU",
      "indices" : [ 161, 167 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976981698717118467",
  "text" : "\"SMU to make global exposure a requirement for graduating students\"\nMe : I got a FREE spare room here in Dublin for rent. Just bring bengawan solo cake will do. #sgSMU",
  "id" : 976981698717118467,
  "created_at" : "2018-03-23 00:39:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Melvin of York",
      "screen_name" : "MelvinofYork",
      "indices" : [ 3, 16 ],
      "id_str" : "166326702",
      "id" : 166326702
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976970926305792000",
  "text" : "RT @MelvinofYork: It's funny how we say \"a bug hit my windshield\"\u00A0when we are the ones going 70mph. I'll bet the bug's family describes it\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976348878600536065",
    "text" : "It's funny how we say \"a bug hit my windshield\"\u00A0when we are the ones going 70mph. I'll bet the bug's family describes it differently",
    "id" : 976348878600536065,
    "created_at" : "2018-03-21 06:44:47 +0000",
    "user" : {
      "name" : "Melvin of York",
      "screen_name" : "MelvinofYork",
      "protected" : false,
      "id_str" : "166326702",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1077448609\/melvinofyork_normal.JPG",
      "id" : 166326702,
      "verified" : false
    }
  },
  "id" : 976970926305792000,
  "created_at" : "2018-03-22 23:56:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/cm5FpDIAiK",
      "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/archaeologist-hails-extraordinary-viking-village-find-in-dublin-1.3437108",
      "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976969707948474368",
  "text" : "RT @ronanlyons: What a wonderful discovery! \nArchaeologist hails \u2018extraordinary\u2019 Viking village find in Dublin https:\/\/t.co\/cm5FpDIAiK",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 95, 118 ],
        "url" : "https:\/\/t.co\/cm5FpDIAiK",
        "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/archaeologist-hails-extraordinary-viking-village-find-in-dublin-1.3437108",
        "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976968189983805442",
    "text" : "What a wonderful discovery! \nArchaeologist hails \u2018extraordinary\u2019 Viking village find in Dublin https:\/\/t.co\/cm5FpDIAiK",
    "id" : 976968189983805442,
    "created_at" : "2018-03-22 23:45:42 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 976969707948474368,
  "created_at" : "2018-03-22 23:51:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mike Volpe",
      "screen_name" : "mvolpe",
      "indices" : [ 3, 10 ],
      "id_str" : "9759932",
      "id" : 9759932
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976968708248735745",
  "text" : "RT @mvolpe: The email nurturing sequence for every ecommerce company:\n\n1) Thank you for your purchase. Here's a discount for next time.\n2)\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973600524959436800",
    "text" : "The email nurturing sequence for every ecommerce company:\n\n1) Thank you for your purchase. Here's a discount for next time.\n2) Discount code.\n3) Discount code.\n4) Discount code.\n5) Discount code.\n6) Discount code.\n7) Discount code.\n8) Sorry you unsubscribed. Have a discount code.",
    "id" : 973600524959436800,
    "created_at" : "2018-03-13 16:43:48 +0000",
    "user" : {
      "name" : "Mike Volpe",
      "screen_name" : "mvolpe",
      "protected" : false,
      "id_str" : "9759932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037908617440186368\/JYArBtOD_normal.jpg",
      "id" : 9759932,
      "verified" : true
    }
  },
  "id" : 976968708248735745,
  "created_at" : "2018-03-22 23:47:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Peter Fader",
      "screen_name" : "faderp",
      "indices" : [ 3, 10 ],
      "id_str" : "138271896",
      "id" : 138271896
    }, {
      "name" : "Nike",
      "screen_name" : "Nike",
      "indices" : [ 31, 36 ],
      "id_str" : "415859364",
      "id" : 415859364
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CustomerValuation",
      "indices" : [ 97, 115 ]
    }, {
      "text" : "CustomerCentricity",
      "indices" : [ 120, 139 ]
    } ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/IampTkuabW",
      "expanded_url" : "https:\/\/news.nike.com\/news\/nike-data-analytics-zodiac",
      "display_url" : "news.nike.com\/news\/nike-data\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976962968381476865",
  "text" : "RT @faderp: Zodiac acquired by @Nike: https:\/\/t.co\/IampTkuabW.  What an amazing step forward for #CustomerValuation and #CustomerCentricity!",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Nike",
        "screen_name" : "Nike",
        "indices" : [ 19, 24 ],
        "id_str" : "415859364",
        "id" : 415859364
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "CustomerValuation",
        "indices" : [ 85, 103 ]
      }, {
        "text" : "CustomerCentricity",
        "indices" : [ 108, 127 ]
      } ],
      "urls" : [ {
        "indices" : [ 26, 49 ],
        "url" : "https:\/\/t.co\/IampTkuabW",
        "expanded_url" : "https:\/\/news.nike.com\/news\/nike-data-analytics-zodiac",
        "display_url" : "news.nike.com\/news\/nike-data\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976946912824889345",
    "text" : "Zodiac acquired by @Nike: https:\/\/t.co\/IampTkuabW.  What an amazing step forward for #CustomerValuation and #CustomerCentricity!",
    "id" : 976946912824889345,
    "created_at" : "2018-03-22 22:21:09 +0000",
    "user" : {
      "name" : "Peter Fader",
      "screen_name" : "faderp",
      "protected" : false,
      "id_str" : "138271896",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2988492689\/c18781a0c267de071ff323cb9d37c076_normal.jpeg",
      "id" : 138271896,
      "verified" : true
    }
  },
  "id" : 976962968381476865,
  "created_at" : "2018-03-22 23:24:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Arvind Narayanan",
      "screen_name" : "random_walker",
      "indices" : [ 3, 17 ],
      "id_str" : "10834752",
      "id" : 10834752
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976960361520824320",
  "text" : "RT @random_walker: Tech buzzwords explained:\nAI\u2014regression\nBig data\u2014data\nBlockchain\u2014database\nAlgorithm\u2014automated decision-making\nCloud\u2014Inte\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976836626121977858",
    "text" : "Tech buzzwords explained:\nAI\u2014regression\nBig data\u2014data\nBlockchain\u2014database\nAlgorithm\u2014automated decision-making\nCloud\u2014Internet\nCrypto\u2014cryptocurrency\nDark web\u2014Onion service\nData science\u2014statistics done by nonstatisticians\nDisruption\u2014competition\nViral\u2014popular\nIoT\u2014malware-ready device",
    "id" : 976836626121977858,
    "created_at" : "2018-03-22 15:02:55 +0000",
    "user" : {
      "name" : "Arvind Narayanan",
      "screen_name" : "random_walker",
      "protected" : false,
      "id_str" : "10834752",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/994916186516209666\/SMCNzeTI_normal.jpg",
      "id" : 10834752,
      "verified" : true
    }
  },
  "id" : 976960361520824320,
  "created_at" : "2018-03-22 23:14:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Flightradar24",
      "screen_name" : "flightradar24",
      "indices" : [ 3, 17 ],
      "id_str" : "134196350",
      "id" : 134196350
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "AI139",
      "indices" : [ 19, 25 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976934483369824256",
  "text" : "RT @flightradar24: #AI139 has landed in Tel Aviv, after being the first commercial flight to transit Saudi Arabian airspace en route to Isr\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/flightradar24\/status\/976916514380091392\/photo\/1",
        "indices" : [ 163, 186 ],
        "url" : "https:\/\/t.co\/wwoXmOBedD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY60SMKX0AAQdjV.jpg",
        "id_str" : "976916393781350400",
        "id" : 976916393781350400,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY60SMKX0AAQdjV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 834,
          "resize" : "fit",
          "w" : 1153
        }, {
          "h" : 834,
          "resize" : "fit",
          "w" : 1153
        }, {
          "h" : 492,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 834,
          "resize" : "fit",
          "w" : 1153
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wwoXmOBedD"
      } ],
      "hashtags" : [ {
        "text" : "AI139",
        "indices" : [ 0, 6 ]
      } ],
      "urls" : [ {
        "indices" : [ 139, 162 ],
        "url" : "https:\/\/t.co\/0amEXONPpk",
        "expanded_url" : "https:\/\/www.flightradar24.com\/data\/flights\/ai139#10cb0f07",
        "display_url" : "flightradar24.com\/data\/flights\/a\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "976862697823272964",
    "geo" : { },
    "id_str" : "976916514380091392",
    "in_reply_to_user_id" : 134196350,
    "text" : "#AI139 has landed in Tel Aviv, after being the first commercial flight to transit Saudi Arabian airspace en route to Israel in 70 years. \n\nhttps:\/\/t.co\/0amEXONPpk https:\/\/t.co\/wwoXmOBedD",
    "id" : 976916514380091392,
    "in_reply_to_status_id" : 976862697823272964,
    "created_at" : "2018-03-22 20:20:21 +0000",
    "in_reply_to_screen_name" : "flightradar24",
    "in_reply_to_user_id_str" : "134196350",
    "user" : {
      "name" : "Flightradar24",
      "screen_name" : "flightradar24",
      "protected" : false,
      "id_str" : "134196350",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/710502600593035265\/eU_SCiU9_normal.jpg",
      "id" : 134196350,
      "verified" : true
    }
  },
  "id" : 976934483369824256,
  "created_at" : "2018-03-22 21:31:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Frier",
      "screen_name" : "sarahfrier",
      "indices" : [ 3, 14 ],
      "id_str" : "15823646",
      "id" : 15823646
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976934245909368832",
  "text" : "RT @sarahfrier: Sandberg on CNBC had a few key talking points: We know this is an issue of trust, we didn't realize the gravity of it soone\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976919705125576704",
    "text" : "Sandberg on CNBC had a few key talking points: We know this is an issue of trust, we didn't realize the gravity of it sooner, we're making investments in fixing it but there are always going to be bad actors on our platform.",
    "id" : 976919705125576704,
    "created_at" : "2018-03-22 20:33:02 +0000",
    "user" : {
      "name" : "Sarah Frier",
      "screen_name" : "sarahfrier",
      "protected" : false,
      "id_str" : "15823646",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/966066623638466560\/I8rrSv9e_normal.jpg",
      "id" : 15823646,
      "verified" : true
    }
  },
  "id" : 976934245909368832,
  "created_at" : "2018-03-22 21:30:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/976918857041530880\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/HI49GmQwAt",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY62edHW0AELNLb.jpg",
      "id_str" : "976918803513790465",
      "id" : 976918803513790465,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY62edHW0AELNLb.jpg",
      "sizes" : [ {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HI49GmQwAt"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/976918857041530880\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/HI49GmQwAt",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY62edlXkAACw9b.jpg",
      "id_str" : "976918803639668736",
      "id" : 976918803639668736,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY62edlXkAACw9b.jpg",
      "sizes" : [ {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/HI49GmQwAt"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976918857041530880",
  "text" : "https:\/\/t.co\/HI49GmQwAt",
  "id" : 976918857041530880,
  "created_at" : "2018-03-22 20:29:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/976918390358102016\/photo\/1",
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/oaImgepFiF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY62D02W0AELPTV.jpg",
      "id_str" : "976918346028470273",
      "id" : 976918346028470273,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY62D02W0AELPTV.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oaImgepFiF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976918390358102016",
  "text" : "FB COO on CNBC. She keep mentioning about finding \"bad actor\" https:\/\/t.co\/oaImgepFiF",
  "id" : 976918390358102016,
  "created_at" : "2018-03-22 20:27:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 3, 15 ],
      "id_str" : "168148402",
      "id" : 168148402
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/pJD7JbI0JB",
      "expanded_url" : "https:\/\/twitter.com\/BIUK\/status\/976790609804406784",
      "display_url" : "twitter.com\/BIUK\/status\/97\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976907540939370496",
  "text" : "RT @m4riannelee: Driverless, no less! https:\/\/t.co\/pJD7JbI0JB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 21, 44 ],
        "url" : "https:\/\/t.co\/pJD7JbI0JB",
        "expanded_url" : "https:\/\/twitter.com\/BIUK\/status\/976790609804406784",
        "display_url" : "twitter.com\/BIUK\/status\/97\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976905798201806848",
    "text" : "Driverless, no less! https:\/\/t.co\/pJD7JbI0JB",
    "id" : 976905798201806848,
    "created_at" : "2018-03-22 19:37:46 +0000",
    "user" : {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "protected" : false,
      "id_str" : "168148402",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/797414171038060544\/7AkMKjj5_normal.jpg",
      "id" : 168148402,
      "verified" : false
    }
  },
  "id" : 976907540939370496,
  "created_at" : "2018-03-22 19:44:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexander",
      "screen_name" : "37paday",
      "indices" : [ 3, 11 ],
      "id_str" : "897431451225948161",
      "id" : 897431451225948161
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976907101187530758",
  "text" : "RT @37paday: 1\/ Brexit has finally broken my mum",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976590988452954112",
    "text" : "1\/ Brexit has finally broken my mum",
    "id" : 976590988452954112,
    "created_at" : "2018-03-21 22:46:50 +0000",
    "user" : {
      "name" : "Alexander",
      "screen_name" : "37paday",
      "protected" : false,
      "id_str" : "897431451225948161",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/931856654605455365\/8uggKv9u_normal.jpg",
      "id" : 897431451225948161,
      "verified" : false
    }
  },
  "id" : 976907101187530758,
  "created_at" : "2018-03-22 19:42:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Asmus Leth Olsen",
      "screen_name" : "AsmusOlsen",
      "indices" : [ 3, 14 ],
      "id_str" : "1205899746",
      "id" : 1205899746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976905053691932672",
  "text" : "RT @AsmusOlsen: Cambridge Analytica\u2019s business model is like a really weird mixed methods paper: In Study 1 we run Big 5 on MTurk  (n=50,00\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 188, 211 ],
        "url" : "https:\/\/t.co\/qrbjTBBFc1",
        "expanded_url" : "https:\/\/www.theatlantic.com\/politics\/archive\/2018\/03\/cambridge-analyticas-self-own\/556016\/",
        "display_url" : "theatlantic.com\/politics\/archi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976189535645249539",
    "text" : "Cambridge Analytica\u2019s business model is like a really weird mixed methods paper: In Study 1 we run Big 5 on MTurk  (n=50,000). In Study 2 we send a prostitute to a politicians home (n=1). https:\/\/t.co\/qrbjTBBFc1",
    "id" : 976189535645249539,
    "created_at" : "2018-03-20 20:11:36 +0000",
    "user" : {
      "name" : "Asmus Leth Olsen",
      "screen_name" : "AsmusOlsen",
      "protected" : false,
      "id_str" : "1205899746",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943790537781411840\/FTIqJmRD_normal.jpg",
      "id" : 1205899746,
      "verified" : false
    }
  },
  "id" : 976905053691932672,
  "created_at" : "2018-03-22 19:34:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976903999294201861",
  "text" : "Anyone here play the latest World of Tanks? The 1.0 version is brilliant.",
  "id" : 976903999294201861,
  "created_at" : "2018-03-22 19:30:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Leisha Santorelli",
      "screen_name" : "BBCLeisha",
      "indices" : [ 3, 13 ],
      "id_str" : "21071916",
      "id" : 21071916
    }, {
      "name" : "Facebook",
      "screen_name" : "facebook",
      "indices" : [ 106, 115 ],
      "id_str" : "2425151",
      "id" : 2425151
    }, {
      "name" : "Google",
      "screen_name" : "Google",
      "indices" : [ 117, 124 ],
      "id_str" : "20536157",
      "id" : 20536157
    }, {
      "name" : "Twitter",
      "screen_name" : "Twitter",
      "indices" : [ 129, 137 ],
      "id_str" : "783214",
      "id" : 783214
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "fakenews",
      "indices" : [ 61, 70 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976810351655313408",
  "text" : "RT @BBCLeisha: Singapore is considering legislation to fight #fakenews so they're grilling officials from @Facebook, @Google and @Twitter i\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Facebook",
        "screen_name" : "facebook",
        "indices" : [ 91, 100 ],
        "id_str" : "2425151",
        "id" : 2425151
      }, {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 102, 109 ],
        "id_str" : "20536157",
        "id" : 20536157
      }, {
        "name" : "Twitter",
        "screen_name" : "Twitter",
        "indices" : [ 114, 122 ],
        "id_str" : "783214",
        "id" : 783214
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "fakenews",
        "indices" : [ 46, 55 ]
      } ],
      "urls" : [ {
        "indices" : [ 248, 271 ],
        "url" : "https:\/\/t.co\/ymIHXdSiAX",
        "expanded_url" : "https:\/\/reut.rs\/2IJNq3k",
        "display_url" : "reut.rs\/2IJNq3k"
      } ]
    },
    "geo" : { },
    "id_str" : "976768585321218048",
    "text" : "Singapore is considering legislation to fight #fakenews so they're grilling officials from @Facebook, @Google and @Twitter in parliament today. But the tech giants are concerned new rules could be used to compromise free speech. More context here: https:\/\/t.co\/ymIHXdSiAX",
    "id" : 976768585321218048,
    "created_at" : "2018-03-22 10:32:32 +0000",
    "user" : {
      "name" : "Leisha Santorelli",
      "screen_name" : "BBCLeisha",
      "protected" : false,
      "id_str" : "21071916",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028753716013355014\/ACzCRMNS_normal.jpg",
      "id" : 21071916,
      "verified" : true
    }
  },
  "id" : 976810351655313408,
  "created_at" : "2018-03-22 13:18:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Leisha Santorelli",
      "screen_name" : "BBCLeisha",
      "indices" : [ 3, 13 ],
      "id_str" : "21071916",
      "id" : 21071916
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976809494071140352",
  "text" : "RT @BBCLeisha: Midway through parliament and many tense exchanges between Singapore law minister K Shanmugam, a former litigator, and Faceb\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Twitter",
        "screen_name" : "Twitter",
        "indices" : [ 246, 254 ],
        "id_str" : "783214",
        "id" : 783214
      }, {
        "name" : "Google",
        "screen_name" : "Google",
        "indices" : [ 259, 266 ],
        "id_str" : "20536157",
        "id" : 20536157
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976736121219514368",
    "text" : "Midway through parliament and many tense exchanges between Singapore law minister K Shanmugam, a former litigator, and Facebook's Simon Milner. The latter has just appealed to committee chairman over being singled out in panel that also includes @Twitter and @Google executives.",
    "id" : 976736121219514368,
    "created_at" : "2018-03-22 08:23:32 +0000",
    "user" : {
      "name" : "Leisha Santorelli",
      "screen_name" : "BBCLeisha",
      "protected" : false,
      "id_str" : "21071916",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028753716013355014\/ACzCRMNS_normal.jpg",
      "id" : 21071916,
      "verified" : true
    }
  },
  "id" : 976809494071140352,
  "created_at" : "2018-03-22 13:15:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elizabeth Law \u601D\u654F",
      "screen_name" : "lizzlaw_",
      "indices" : [ 3, 12 ],
      "id_str" : "986182992",
      "id" : 986182992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976808815629885440",
  "text" : "RT @lizzlaw_: Been nearly an hour of exchanges between K Shanmugam and Simon Milner of Facebook, who admitted the company \"could have done\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "976711788644286467",
    "geo" : { },
    "id_str" : "976717663375302656",
    "in_reply_to_user_id" : 986182992,
    "text" : "Been nearly an hour of exchanges between K Shanmugam and Simon Milner of Facebook, who admitted the company \"could have done better\"\n\nStarting to sound a bit like a cross-examination. The law minister is a former litigator.",
    "id" : 976717663375302656,
    "in_reply_to_status_id" : 976711788644286467,
    "created_at" : "2018-03-22 07:10:12 +0000",
    "in_reply_to_screen_name" : "lizzlaw_",
    "in_reply_to_user_id_str" : "986182992",
    "user" : {
      "name" : "Elizabeth Law \u601D\u654F",
      "screen_name" : "lizzlaw_",
      "protected" : false,
      "id_str" : "986182992",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/905295755497402368\/Q2x-Wuqt_normal.jpg",
      "id" : 986182992,
      "verified" : true
    }
  },
  "id" : 976808815629885440,
  "created_at" : "2018-03-22 13:12:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elizabeth Law \u601D\u654F",
      "screen_name" : "lizzlaw_",
      "indices" : [ 3, 12 ],
      "id_str" : "986182992",
      "id" : 986182992
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976808759581429761",
  "text" : "RT @lizzlaw_: Internet giants Facebook, Google and Twitter are testifying before a special Singapore parliamentary committee on fake news.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976710994767462400",
    "text" : "Internet giants Facebook, Google and Twitter are testifying before a special Singapore parliamentary committee on fake news. \n\nThis is the first time FB facing a committee since news of data breach broke last weekend",
    "id" : 976710994767462400,
    "created_at" : "2018-03-22 06:43:42 +0000",
    "user" : {
      "name" : "Elizabeth Law \u601D\u654F",
      "screen_name" : "lizzlaw_",
      "protected" : false,
      "id_str" : "986182992",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/905295755497402368\/Q2x-Wuqt_normal.jpg",
      "id" : 986182992,
      "verified" : true
    }
  },
  "id" : 976808759581429761,
  "created_at" : "2018-03-22 13:12:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "d12",
      "indices" : [ 43, 47 ]
    }, {
      "text" : "IrishWater",
      "indices" : [ 49, 60 ]
    }, {
      "text" : "BeWaterSmart",
      "indices" : [ 61, 74 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976805798595751936",
  "text" : "Is there any water restriction in place in #d12? #IrishWater #BeWaterSmart  Thanks",
  "id" : 976805798595751936,
  "created_at" : "2018-03-22 13:00:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/aoHS7iKud0",
      "expanded_url" : "https:\/\/twitter.com\/search?q=%236Nations%20virign%20media&src=typd",
      "display_url" : "twitter.com\/search?q=%236N\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976790322033381377",
  "text" : "Who says television is dying? https:\/\/t.co\/aoHS7iKud0 MAterial for your next presentation.",
  "id" : 976790322033381377,
  "created_at" : "2018-03-22 11:58:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "indices" : [ 3, 18 ],
      "id_str" : "1626294277",
      "id" : 1626294277
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/976458419124056064\/photo\/1",
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/44hSGmZNZj",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYz15BoV4AA6Gix.jpg",
      "id_str" : "976425579271544832",
      "id" : 976425579271544832,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYz15BoV4AA6Gix.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/44hSGmZNZj"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976774517073866753",
  "text" : "RT @spectatorindex: HISTORY: On this day in 2006, Twitter was founded. https:\/\/t.co\/44hSGmZNZj",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/spectatorindex\/status\/976458419124056064\/photo\/1",
        "indices" : [ 51, 74 ],
        "url" : "https:\/\/t.co\/44hSGmZNZj",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYz15BoV4AA6Gix.jpg",
        "id_str" : "976425579271544832",
        "id" : 976425579271544832,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYz15BoV4AA6Gix.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/44hSGmZNZj"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976458419124056064",
    "text" : "HISTORY: On this day in 2006, Twitter was founded. https:\/\/t.co\/44hSGmZNZj",
    "id" : 976458419124056064,
    "created_at" : "2018-03-21 14:00:03 +0000",
    "user" : {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "protected" : false,
      "id_str" : "1626294277",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839067030904922112\/LH4xqz-d_normal.jpg",
      "id" : 1626294277,
      "verified" : false
    }
  },
  "id" : 976774517073866753,
  "created_at" : "2018-03-22 10:56:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "indices" : [ 3, 15 ],
      "id_str" : "18721044",
      "id" : 18721044
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976771416380133376",
  "text" : "RT @klillington: \u201CWe need leaders not in love with money but in love with justice. Not in love with publicity but in love with humanity. Le\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "952935545688207360",
    "text" : "\u201CWe need leaders not in love with money but in love with justice. Not in love with publicity but in love with humanity. Leaders who can subject their particular egos to the pressing urgencies of the great cause of freedom\u2026\u201D \u2013 Dr. Martin Luther King, Jr.",
    "id" : 952935545688207360,
    "created_at" : "2018-01-15 16:08:33 +0000",
    "user" : {
      "name" : "Dr Karlin Lillington",
      "screen_name" : "klillington",
      "protected" : false,
      "id_str" : "18721044",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/788417211299946496\/_sDVko9l_normal.jpg",
      "id" : 18721044,
      "verified" : false
    }
  },
  "id" : 976771416380133376,
  "created_at" : "2018-03-22 10:43:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Deborah Kay",
      "screen_name" : "debbiediscovers",
      "indices" : [ 3, 19 ],
      "id_str" : "3060836348",
      "id" : 3060836348
    }, {
      "name" : "Twitter",
      "screen_name" : "Twitter",
      "indices" : [ 38, 46 ],
      "id_str" : "783214",
      "id" : 783214
    }, {
      "name" : "GitHub",
      "screen_name" : "github",
      "indices" : [ 58, 65 ],
      "id_str" : "13334762",
      "id" : 13334762
    }, {
      "name" : "Deborah Kay",
      "screen_name" : "debbiediscovers",
      "indices" : [ 71, 87 ],
      "id_str" : "3060836348",
      "id" : 3060836348
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "TwitterTips",
      "indices" : [ 88, 100 ]
    } ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/encbrD6lPy",
      "expanded_url" : "https:\/\/digitaldiscovery.sg\/blog\/host-twitter-archive\/",
      "display_url" : "digitaldiscovery.sg\/blog\/host-twit\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976770068452380672",
  "text" : "RT @debbiediscovers: How to host your @Twitter Archive on @Github - by @debbiediscovers #TwitterTips\n\n https:\/\/t.co\/encbrD6lPy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/app.agorapulse.com\" rel=\"nofollow\"\u003EAgoraPulse Manager\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Twitter",
        "screen_name" : "Twitter",
        "indices" : [ 17, 25 ],
        "id_str" : "783214",
        "id" : 783214
      }, {
        "name" : "GitHub",
        "screen_name" : "github",
        "indices" : [ 37, 44 ],
        "id_str" : "13334762",
        "id" : 13334762
      }, {
        "name" : "Deborah Kay",
        "screen_name" : "debbiediscovers",
        "indices" : [ 50, 66 ],
        "id_str" : "3060836348",
        "id" : 3060836348
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "TwitterTips",
        "indices" : [ 67, 79 ]
      } ],
      "urls" : [ {
        "indices" : [ 82, 105 ],
        "url" : "https:\/\/t.co\/encbrD6lPy",
        "expanded_url" : "https:\/\/digitaldiscovery.sg\/blog\/host-twitter-archive\/",
        "display_url" : "digitaldiscovery.sg\/blog\/host-twit\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976594571781079041",
    "text" : "How to host your @Twitter Archive on @Github - by @debbiediscovers #TwitterTips\n\n https:\/\/t.co\/encbrD6lPy",
    "id" : 976594571781079041,
    "created_at" : "2018-03-21 23:01:04 +0000",
    "user" : {
      "name" : "Deborah Kay",
      "screen_name" : "debbiediscovers",
      "protected" : false,
      "id_str" : "3060836348",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1027068089618198536\/wIl1K2Fm_normal.jpg",
      "id" : 3060836348,
      "verified" : false
    }
  },
  "id" : 976770068452380672,
  "created_at" : "2018-03-22 10:38:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Faheem",
      "screen_name" : "wanfaheem_",
      "indices" : [ 3, 14 ],
      "id_str" : "199225954",
      "id" : 199225954
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/o2NOkRWAhF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_TKfUQAEesPI.jpg",
      "id_str" : "976013711465791489",
      "id" : 976013711465791489,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_TKfUQAEesPI.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 646
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1137,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1137,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1137,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/o2NOkRWAhF"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/o2NOkRWAhF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_UDpVQAEJNGW.jpg",
      "id_str" : "976013726808621057",
      "id" : 976013726808621057,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_UDpVQAEJNGW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1514,
        "resize" : "fit",
        "w" : 1406
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 631
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1114
      }, {
        "h" : 1514,
        "resize" : "fit",
        "w" : 1406
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/o2NOkRWAhF"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/o2NOkRWAhF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_VhdVoAABcYm.jpg",
      "id_str" : "976013751991246848",
      "id" : 976013751991246848,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_VhdVoAABcYm.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/o2NOkRWAhF"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/o2NOkRWAhF",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_V9dVAAABSsK.jpg",
      "id_str" : "976013759507398656",
      "id" : 976013759507398656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_V9dVAAABSsK.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1137,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1137,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 646
      }, {
        "h" : 1137,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/o2NOkRWAhF"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976768303921352704",
  "text" : "RT @wanfaheem_: Rt this and ill make a pic of yours :) https:\/\/t.co\/o2NOkRWAhF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/o2NOkRWAhF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_TKfUQAEesPI.jpg",
        "id_str" : "976013711465791489",
        "id" : 976013711465791489,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_TKfUQAEesPI.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 646
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1137,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1137,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1137,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o2NOkRWAhF"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/o2NOkRWAhF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_UDpVQAEJNGW.jpg",
        "id_str" : "976013726808621057",
        "id" : 976013726808621057,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_UDpVQAEJNGW.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1514,
          "resize" : "fit",
          "w" : 1406
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 631
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1114
        }, {
          "h" : 1514,
          "resize" : "fit",
          "w" : 1406
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o2NOkRWAhF"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/o2NOkRWAhF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_VhdVoAABcYm.jpg",
        "id_str" : "976013751991246848",
        "id" : 976013751991246848,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_VhdVoAABcYm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1080,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o2NOkRWAhF"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/wanfaheem_\/status\/976013763022225408\/photo\/1",
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/o2NOkRWAhF",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYt_V9dVAAABSsK.jpg",
        "id_str" : "976013759507398656",
        "id" : 976013759507398656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYt_V9dVAAABSsK.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1137,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1137,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 646
        }, {
          "h" : 1137,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/o2NOkRWAhF"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976013763022225408",
    "text" : "Rt this and ill make a pic of yours :) https:\/\/t.co\/o2NOkRWAhF",
    "id" : 976013763022225408,
    "created_at" : "2018-03-20 08:33:09 +0000",
    "user" : {
      "name" : "Faheem",
      "screen_name" : "wanfaheem_",
      "protected" : false,
      "id_str" : "199225954",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1022799795415539713\/_cf3OxER_normal.jpg",
      "id" : 199225954,
      "verified" : false
    }
  },
  "id" : 976768303921352704,
  "created_at" : "2018-03-22 10:31:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 30, 53 ],
      "url" : "https:\/\/t.co\/44UDJAh4VN",
      "expanded_url" : "https:\/\/twitter.com\/standardnews\/status\/976617274462359552",
      "display_url" : "twitter.com\/standardnews\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976764957315469312",
  "text" : "RT @paddy_be: Oh the irony... https:\/\/t.co\/44UDJAh4VN",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 39 ],
        "url" : "https:\/\/t.co\/44UDJAh4VN",
        "expanded_url" : "https:\/\/twitter.com\/standardnews\/status\/976617274462359552",
        "display_url" : "twitter.com\/standardnews\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976763884706189312",
    "text" : "Oh the irony... https:\/\/t.co\/44UDJAh4VN",
    "id" : 976763884706189312,
    "created_at" : "2018-03-22 10:13:52 +0000",
    "user" : {
      "name" : "patrick breslin",
      "screen_name" : "MrPaddyBreslin",
      "protected" : false,
      "id_str" : "519497243",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/767016611488366593\/A0sW_mtc_normal.jpg",
      "id" : 519497243,
      "verified" : false
    }
  },
  "id" : 976764957315469312,
  "created_at" : "2018-03-22 10:18:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "SNSM",
      "screen_name" : "SauveteursenMer",
      "indices" : [ 23, 39 ],
      "id_str" : "219235380",
      "id" : 219235380
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/EzGhNskLS2",
      "expanded_url" : "https:\/\/youtu.be\/N1RK96-kDbc",
      "display_url" : "youtu.be\/N1RK96-kDbc"
    } ]
  },
  "geo" : { },
  "id_str" : "976759173454155777",
  "text" : "The mermaid video from @SauveteursenMer https:\/\/t.co\/EzGhNskLS2 \uD83D\uDE02",
  "id" : 976759173454155777,
  "created_at" : "2018-03-22 09:55:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hannah Fry",
      "screen_name" : "FryRsquared",
      "indices" : [ 3, 15 ],
      "id_str" : "273375532",
      "id" : 273375532
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/FryRsquared\/status\/976750905105186816\/photo\/1",
      "indices" : [ 111, 134 ],
      "url" : "https:\/\/t.co\/GxZ6qJqlXL",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY4dxTdX4AA7eAL.jpg",
      "id_str" : "976750902060244992",
      "id" : 976750902060244992,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY4dxTdX4AA7eAL.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/GxZ6qJqlXL"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976756102963527681",
  "text" : "RT @FryRsquared: Remember that flu thing you all downloaded? Well, the results are on telly tonight: BBC4 9pm. https:\/\/t.co\/GxZ6qJqlXL",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/FryRsquared\/status\/976750905105186816\/photo\/1",
        "indices" : [ 94, 117 ],
        "url" : "https:\/\/t.co\/GxZ6qJqlXL",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY4dxTdX4AA7eAL.jpg",
        "id_str" : "976750902060244992",
        "id" : 976750902060244992,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY4dxTdX4AA7eAL.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/GxZ6qJqlXL"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976750905105186816",
    "text" : "Remember that flu thing you all downloaded? Well, the results are on telly tonight: BBC4 9pm. https:\/\/t.co\/GxZ6qJqlXL",
    "id" : 976750905105186816,
    "created_at" : "2018-03-22 09:22:17 +0000",
    "user" : {
      "name" : "Hannah Fry",
      "screen_name" : "FryRsquared",
      "protected" : false,
      "id_str" : "273375532",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/549666331805483009\/vYR8d3e0_normal.jpeg",
      "id" : 273375532,
      "verified" : true
    }
  },
  "id" : 976756102963527681,
  "created_at" : "2018-03-22 09:42:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Prof Jackie Carter",
      "screen_name" : "JackieCarter",
      "indices" : [ 3, 16 ],
      "id_str" : "19656274",
      "id" : 19656274
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/JackieCarter\/status\/976747813685137408\/photo\/1",
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/eh1Ke9Z62C",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DY4a8wKX0AA0xYu.jpg",
      "id_str" : "976747800208855040",
      "id" : 976747800208855040,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY4a8wKX0AA0xYu.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eh1Ke9Z62C"
    } ],
    "hashtags" : [ {
      "text" : "data4devfest",
      "indices" : [ 62, 75 ]
    }, {
      "text" : "wiasn",
      "indices" : [ 76, 82 ]
    }, {
      "text" : "nomanels",
      "indices" : [ 83, 92 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976755819487342593",
  "text" : "RT @JackieCarter: Now *this* is what a panel should look like #data4devfest #wiasn #nomanels https:\/\/t.co\/eh1Ke9Z62C",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JackieCarter\/status\/976747813685137408\/photo\/1",
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/eh1Ke9Z62C",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DY4a8wKX0AA0xYu.jpg",
        "id_str" : "976747800208855040",
        "id" : 976747800208855040,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DY4a8wKX0AA0xYu.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eh1Ke9Z62C"
      } ],
      "hashtags" : [ {
        "text" : "data4devfest",
        "indices" : [ 44, 57 ]
      }, {
        "text" : "wiasn",
        "indices" : [ 58, 64 ]
      }, {
        "text" : "nomanels",
        "indices" : [ 65, 74 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976747813685137408",
    "text" : "Now *this* is what a panel should look like #data4devfest #wiasn #nomanels https:\/\/t.co\/eh1Ke9Z62C",
    "id" : 976747813685137408,
    "created_at" : "2018-03-22 09:10:00 +0000",
    "user" : {
      "name" : "Prof Jackie Carter",
      "screen_name" : "JackieCarter",
      "protected" : false,
      "id_str" : "19656274",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1345208779\/Jackie_5_normal.JPG",
      "id" : 19656274,
      "verified" : false
    }
  },
  "id" : 976755819487342593,
  "created_at" : "2018-03-22 09:41:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 57, 80 ],
      "url" : "https:\/\/t.co\/oJ1VS2uzNg",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/av\/world-australia-43481261\/eddie-woo-the-maths-teacher-who-became-an-unlikely-star",
      "display_url" : "bbc.com\/news\/av\/world-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976562251757912064",
  "text" : "Eddie Woo: The maths teacher who became an unlikely star https:\/\/t.co\/oJ1VS2uzNg",
  "id" : 976562251757912064,
  "created_at" : "2018-03-21 20:52:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 88, 111 ],
      "url" : "https:\/\/t.co\/2sJ0RBovGV",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/world-us-canada-43494337",
      "display_url" : "bbc.com\/news\/world-us-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976561462536736770",
  "text" : "RT @edwinksl: BBC News - Facebook's Zuckerberg admits mistakes over Cambridge Analytica https:\/\/t.co\/2sJ0RBovGV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 74, 97 ],
        "url" : "https:\/\/t.co\/2sJ0RBovGV",
        "expanded_url" : "http:\/\/www.bbc.com\/news\/world-us-canada-43494337",
        "display_url" : "bbc.com\/news\/world-us-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976555289456644098",
    "text" : "BBC News - Facebook's Zuckerberg admits mistakes over Cambridge Analytica https:\/\/t.co\/2sJ0RBovGV",
    "id" : 976555289456644098,
    "created_at" : "2018-03-21 20:24:59 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 976561462536736770,
  "created_at" : "2018-03-21 20:49:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MEN Business",
      "screen_name" : "MENBusinessDesk",
      "indices" : [ 3, 19 ],
      "id_str" : "143851022",
      "id" : 143851022
    }, {
      "name" : "Michelle Hua",
      "screen_name" : "MadewithGlove",
      "indices" : [ 89, 103 ],
      "id_str" : "2373801085",
      "id" : 2373801085
    }, {
      "name" : "Lou Cordwell OBE",
      "screen_name" : "Loucordwell",
      "indices" : [ 104, 116 ],
      "id_str" : "170043056",
      "id" : 170043056
    }, {
      "name" : "Alison Ross",
      "screen_name" : "Ali_R_MCR",
      "indices" : [ 117, 127 ],
      "id_str" : "138678841",
      "id" : 138678841
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "tech",
      "indices" : [ 63, 68 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976545180428984320",
  "text" : "RT @MENBusinessDesk: We're about to reveal the top 30 women in #tech. Who's on the list? @MadewithGlove @Loucordwell @Ali_R_MCR \nhttps:\/\/t.\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Michelle Hua",
        "screen_name" : "MadewithGlove",
        "indices" : [ 68, 82 ],
        "id_str" : "2373801085",
        "id" : 2373801085
      }, {
        "name" : "Lou Cordwell OBE",
        "screen_name" : "Loucordwell",
        "indices" : [ 83, 95 ],
        "id_str" : "170043056",
        "id" : 170043056
      }, {
        "name" : "Alison Ross",
        "screen_name" : "Ali_R_MCR",
        "indices" : [ 96, 106 ],
        "id_str" : "138678841",
        "id" : 138678841
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "tech",
        "indices" : [ 42, 47 ]
      } ],
      "urls" : [ {
        "indices" : [ 108, 131 ],
        "url" : "https:\/\/t.co\/H26TIHANr6",
        "expanded_url" : "https:\/\/www.manchestereveningnews.co.uk\/business\/business-news\/who-top-30-women-tech-14440349",
        "display_url" : "manchestereveningnews.co.uk\/business\/busin\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976503707947945986",
    "text" : "We're about to reveal the top 30 women in #tech. Who's on the list? @MadewithGlove @Loucordwell @Ali_R_MCR \nhttps:\/\/t.co\/H26TIHANr6",
    "id" : 976503707947945986,
    "created_at" : "2018-03-21 17:00:01 +0000",
    "user" : {
      "name" : "MEN Business",
      "screen_name" : "MENBusinessDesk",
      "protected" : false,
      "id_str" : "143851022",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037642693801848832\/0XTqYWbh_normal.jpg",
      "id" : 143851022,
      "verified" : false
    }
  },
  "id" : 976545180428984320,
  "created_at" : "2018-03-21 19:44:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 3, 12 ],
      "id_str" : "10410242",
      "id" : 10410242
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/uY1P8Zl8vu",
      "expanded_url" : "https:\/\/twitter.com\/onlinehubie\/status\/976526635418734592",
      "display_url" : "twitter.com\/onlinehubie\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976544686520328193",
  "text" : "RT @enormous: Genuinely curious about this. Pls RT. GRMA! https:\/\/t.co\/uY1P8Zl8vu",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 44, 67 ],
        "url" : "https:\/\/t.co\/uY1P8Zl8vu",
        "expanded_url" : "https:\/\/twitter.com\/onlinehubie\/status\/976526635418734592",
        "display_url" : "twitter.com\/onlinehubie\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976526996674174977",
    "text" : "Genuinely curious about this. Pls RT. GRMA! https:\/\/t.co\/uY1P8Zl8vu",
    "id" : 976526996674174977,
    "created_at" : "2018-03-21 18:32:33 +0000",
    "user" : {
      "name" : "enormous",
      "screen_name" : "enormous",
      "protected" : false,
      "id_str" : "10410242",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1011921428680192000\/mSeGaMcb_normal.jpg",
      "id" : 10410242,
      "verified" : false
    }
  },
  "id" : 976544686520328193,
  "created_at" : "2018-03-21 19:42:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\uD83C\uDF31 Matthew Martin \u24CB",
      "screen_name" : "vegdevops",
      "indices" : [ 3, 13 ],
      "id_str" : "3892439001",
      "id" : 3892439001
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976542021484793856",
  "text" : "RT @vegdevops: We should raise taxes on sugary or people who drink sugary drinks will raise your taxes (or premiums) on you via high health\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 182, 205 ],
        "url" : "https:\/\/t.co\/nWnieKsSBG",
        "expanded_url" : "https:\/\/www.washingtonpost.com\/news\/wonk\/wp\/2018\/03\/21\/why-the-british-soda-tax-might-work-better-than-any-of-the-soda-taxes-that-came-before-it\/?utm_term=.6e169c8d5ccf",
        "display_url" : "washingtonpost.com\/news\/wonk\/wp\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976537071887241216",
    "text" : "We should raise taxes on sugary or people who drink sugary drinks will raise your taxes (or premiums) on you via high healthcare costs paid for by public healthcare or private ins.\n\nhttps:\/\/t.co\/nWnieKsSBG",
    "id" : 976537071887241216,
    "created_at" : "2018-03-21 19:12:35 +0000",
    "user" : {
      "name" : "\uD83C\uDF31 Matthew Martin \u24CB",
      "screen_name" : "vegdevops",
      "protected" : false,
      "id_str" : "3892439001",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/651834414020841474\/5Ril8C0__normal.jpg",
      "id" : 3892439001,
      "verified" : false
    }
  },
  "id" : 976542021484793856,
  "created_at" : "2018-03-21 19:32:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/DdCP25S9WX",
      "expanded_url" : "https:\/\/hireyap.netlify.com\/post\/most-effective-ethical-and-modern-approach-to-data-teamwork\/",
      "display_url" : "hireyap.netlify.com\/post\/most-effe\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976419303431004160",
  "text" : "https:\/\/t.co\/DdCP25S9WX",
  "id" : 976419303431004160,
  "created_at" : "2018-03-21 11:24:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976402624202395650",
  "text" : "We still in the dark about other people on own own planet and yet we want to go far far away to Mars.",
  "id" : 976402624202395650,
  "created_at" : "2018-03-21 10:18:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 171, 194 ],
      "url" : "https:\/\/t.co\/KgdbWvEdvX",
      "expanded_url" : "http:\/\/str.sg\/oTyN",
      "display_url" : "str.sg\/oTyN"
    } ]
  },
  "geo" : { },
  "id_str" : "976401830581358598",
  "text" : "Some Americans thought the Malaysian flag was \"an American flag desecrated with ISIS symbols\". The Kansas chapter of the ACLU has filed a lawsuit on behalf of a Malaysian https:\/\/t.co\/KgdbWvEdvX &lt; The problem when you live in your own world, in your little town.",
  "id" : 976401830581358598,
  "created_at" : "2018-03-21 10:15:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "Shareaholic",
      "screen_name" : "Shareaholic",
      "indices" : [ 108, 120 ],
      "id_str" : "791635",
      "id" : 791635
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/47Xhdk84oL",
      "expanded_url" : "http:\/\/go.shr.lc\/2cW23C5",
      "display_url" : "go.shr.lc\/2cW23C5"
    } ]
  },
  "geo" : { },
  "id_str" : "976396262319763456",
  "text" : "RT @edwinksl: 10 Ex-Routines Of Singaporeans In The 90s With No Relevance Now - https:\/\/t.co\/47Xhdk84oL via @Shareaholic",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Shareaholic",
        "screen_name" : "Shareaholic",
        "indices" : [ 94, 106 ],
        "id_str" : "791635",
        "id" : 791635
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 66, 89 ],
        "url" : "https:\/\/t.co\/47Xhdk84oL",
        "expanded_url" : "http:\/\/go.shr.lc\/2cW23C5",
        "display_url" : "go.shr.lc\/2cW23C5"
      } ]
    },
    "geo" : { },
    "id_str" : "976300018989596672",
    "text" : "10 Ex-Routines Of Singaporeans In The 90s With No Relevance Now - https:\/\/t.co\/47Xhdk84oL via @Shareaholic",
    "id" : 976300018989596672,
    "created_at" : "2018-03-21 03:30:37 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 976396262319763456,
  "created_at" : "2018-03-21 09:53:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GDPR",
      "indices" : [ 122, 127 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976395592460009472",
  "text" : "Past few days, I got several of those \"whether I am still interested to receive email newsletter\"? IS this got to do with #GDPR",
  "id" : 976395592460009472,
  "created_at" : "2018-03-21 09:50:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheLifeSavingForce",
      "screen_name" : "SCDF",
      "indices" : [ 3, 8 ],
      "id_str" : "20117836",
      "id" : 20117836
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976358556541902848",
  "text" : "RT @SCDF: Firefighting ops at Pulau Busing ongoing. SCDF working with CERT members to carry out boundary cooling of the adjacent tanks. Lar\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976111410328494080\/photo\/1",
        "indices" : [ 260, 283 ],
        "url" : "https:\/\/t.co\/uQJGhVDSN8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvYH9sVoAA7Q8Y.jpg",
        "id_str" : "976111375587057664",
        "id" : 976111375587057664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvYH9sVoAA7Q8Y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uQJGhVDSN8"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976111410328494080\/photo\/1",
        "indices" : [ 260, 283 ],
        "url" : "https:\/\/t.co\/uQJGhVDSN8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvYH9uU8AIYwvY.jpg",
        "id_str" : "976111375595401218",
        "id" : 976111375595401218,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvYH9uU8AIYwvY.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uQJGhVDSN8"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976111410328494080\/photo\/1",
        "indices" : [ 260, 283 ],
        "url" : "https:\/\/t.co\/uQJGhVDSN8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvYH9xVAAAvUsv.jpg",
        "id_str" : "976111375607988224",
        "id" : 976111375607988224,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvYH9xVAAAvUsv.jpg",
        "sizes" : [ {
          "h" : 777,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 330,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 777,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 583,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uQJGhVDSN8"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976111410328494080\/photo\/1",
        "indices" : [ 260, 283 ],
        "url" : "https:\/\/t.co\/uQJGhVDSN8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvYIACU0AAga1n.jpg",
        "id_str" : "976111376216150016",
        "id" : 976111376216150016,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvYIACU0AAga1n.jpg",
        "sizes" : [ {
          "h" : 777,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 330,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 777,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 583,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/uQJGhVDSN8"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976111410328494080",
    "text" : "Firefighting ops at Pulau Busing ongoing. SCDF working with CERT members to carry out boundary cooling of the adjacent tanks. Large Monitors being set up to tackle the raging oil storage tank fire. \n\nThis is expected to be an extended firefighting operations. https:\/\/t.co\/uQJGhVDSN8",
    "id" : 976111410328494080,
    "created_at" : "2018-03-20 15:01:10 +0000",
    "user" : {
      "name" : "TheLifeSavingForce",
      "screen_name" : "SCDF",
      "protected" : false,
      "id_str" : "20117836",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686719619009130496\/s_Lk-ozY_normal.png",
      "id" : 20117836,
      "verified" : false
    }
  },
  "id" : 976358556541902848,
  "created_at" : "2018-03-21 07:23:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheLifeSavingForce",
      "screen_name" : "SCDF",
      "indices" : [ 3, 8 ],
      "id_str" : "20117836",
      "id" : 20117836
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976358170489704448",
  "text" : "RT @SCDF: Firefighters taking turns to recuperate after arduous prolonged firefighting operations. Upon a short rest, they will go back int\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976134455227138048\/photo\/1",
        "indices" : [ 167, 190 ],
        "url" : "https:\/\/t.co\/FtfH6Rsx1N",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvtFlAVwAAc7jR.jpg",
        "id_str" : "976134424344510464",
        "id" : 976134424344510464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvtFlAVwAAc7jR.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FtfH6Rsx1N"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976134455227138048\/photo\/1",
        "indices" : [ 167, 190 ],
        "url" : "https:\/\/t.co\/FtfH6Rsx1N",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvtFlLVMAAO2TF.jpg",
        "id_str" : "976134424390610944",
        "id" : 976134424390610944,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvtFlLVMAAO2TF.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FtfH6Rsx1N"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/SCDF\/status\/976134455227138048\/photo\/1",
        "indices" : [ 167, 190 ],
        "url" : "https:\/\/t.co\/FtfH6Rsx1N",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYvtFkuUQAAh-NP.jpg",
        "id_str" : "976134424268914688",
        "id" : 976134424268914688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYvtFkuUQAAh-NP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/FtfH6Rsx1N"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976134455227138048",
    "text" : "Firefighters taking turns to recuperate after arduous prolonged firefighting operations. Upon a short rest, they will go back into the thick of action at ground zero. https:\/\/t.co\/FtfH6Rsx1N",
    "id" : 976134455227138048,
    "created_at" : "2018-03-20 16:32:44 +0000",
    "user" : {
      "name" : "TheLifeSavingForce",
      "screen_name" : "SCDF",
      "protected" : false,
      "id_str" : "20117836",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/686719619009130496\/s_Lk-ozY_normal.png",
      "id" : 20117836,
      "verified" : false
    }
  },
  "id" : 976358170489704448,
  "created_at" : "2018-03-21 07:21:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CSAIL at MIT",
      "screen_name" : "MIT_CSAIL",
      "indices" : [ 3, 13 ],
      "id_str" : "82364810",
      "id" : 82364810
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976244146460659713",
  "text" : "RT @MIT_CSAIL: \"Programming is like cooking: in Python, you use pre-made bolognese sauce; in C++, you start from fresh tomatoes and minced\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Gustavo V. Barroso",
        "screen_name" : "gv_barroso",
        "indices" : [ 211, 222 ],
        "id_str" : "843079412",
        "id" : 843079412
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MIT_CSAIL\/status\/976126217341501440\/photo\/1",
        "indices" : [ 262, 285 ],
        "url" : "https:\/\/t.co\/np4OFNHh6x",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYQiB3XW0AUWM2y.jpg",
        "id_str" : "973940834855997445",
        "id" : 973940834855997445,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYQiB3XW0AUWM2y.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1125,
          "resize" : "fit",
          "w" : 1500
        }, {
          "h" : 1125,
          "resize" : "fit",
          "w" : 1500
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/np4OFNHh6x"
      } ],
      "hashtags" : [ {
        "text" : "tuesdaythoughts",
        "indices" : [ 245, 261 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976126217341501440",
    "text" : "\"Programming is like cooking: in Python, you use pre-made bolognese sauce; in C++, you start from fresh tomatoes and minced meat; in Assembly, you have a farm where you grow your tomatoes and raise your cow.\" - @gv_barroso h\/t @programmerwisdom #tuesdaythoughts https:\/\/t.co\/np4OFNHh6x",
    "id" : 976126217341501440,
    "created_at" : "2018-03-20 16:00:00 +0000",
    "user" : {
      "name" : "CSAIL at MIT",
      "screen_name" : "MIT_CSAIL",
      "protected" : false,
      "id_str" : "82364810",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/885505956272115712\/U81HpDxb_normal.jpg",
      "id" : 82364810,
      "verified" : true
    }
  },
  "id" : 976244146460659713,
  "created_at" : "2018-03-20 23:48:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Craig Daniel",
      "screen_name" : "craigdaniel",
      "indices" : [ 3, 15 ],
      "id_str" : "15034351",
      "id" : 15034351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 67, 90 ],
      "url" : "https:\/\/t.co\/rGUCkA19aV",
      "expanded_url" : "https:\/\/twitter.com\/jensenharris\/status\/973780676016144384",
      "display_url" : "twitter.com\/jensenharris\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976239776205606912",
  "text" : "RT @craigdaniel: Good thread to read for product or design people. https:\/\/t.co\/rGUCkA19aV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 50, 73 ],
        "url" : "https:\/\/t.co\/rGUCkA19aV",
        "expanded_url" : "https:\/\/twitter.com\/jensenharris\/status\/973780676016144384",
        "display_url" : "twitter.com\/jensenharris\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "973899173060804609",
    "text" : "Good thread to read for product or design people. https:\/\/t.co\/rGUCkA19aV",
    "id" : 973899173060804609,
    "created_at" : "2018-03-14 12:30:31 +0000",
    "user" : {
      "name" : "Craig Daniel",
      "screen_name" : "craigdaniel",
      "protected" : false,
      "id_str" : "15034351",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/937467788817440768\/3WTSyTge_normal.jpg",
      "id" : 15034351,
      "verified" : false
    }
  },
  "id" : 976239776205606912,
  "created_at" : "2018-03-20 23:31:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Manchester",
      "indices" : [ 0, 11 ]
    } ],
    "urls" : [ {
      "indices" : [ 12, 35 ],
      "url" : "https:\/\/t.co\/522u1V8vok",
      "expanded_url" : "https:\/\/twitter.com\/jackiepenangkit\/status\/976235843882049536",
      "display_url" : "twitter.com\/jackiepenangki\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976236692934938624",
  "text" : "#Manchester https:\/\/t.co\/522u1V8vok",
  "id" : 976236692934938624,
  "created_at" : "2018-03-20 23:18:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/9Pm3k1KDZi",
      "expanded_url" : "https:\/\/twitter.com\/markstanley\/status\/976223012776218624",
      "display_url" : "twitter.com\/markstanley\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976227472046985216",
  "text" : "\uD83E\uDD14 https:\/\/t.co\/9Pm3k1KDZi",
  "id" : 976227472046985216,
  "created_at" : "2018-03-20 22:42:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976219764065529856",
  "text" : "Watched Amazing Hotels on BBC 2. Ashford Castle is amazing.",
  "id" : 976219764065529856,
  "created_at" : "2018-03-20 22:11:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dan Kim",
      "screen_name" : "dankim",
      "indices" : [ 3, 10 ],
      "id_str" : "7979212",
      "id" : 7979212
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976217540421652480",
  "text" : "RT @dankim: $5 for a cup of coffee? Sure.\n$5 for a burger? Yep.\n$25 for a t-shirt? Fair.\n$75\/month for cable? Duh.\n$800 for a phone: Obviou\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975854126138617857",
    "text" : "$5 for a cup of coffee? Sure.\n$5 for a burger? Yep.\n$25 for a t-shirt? Fair.\n$75\/month for cable? Duh.\n$800 for a phone: Obviously.\n$5\/year to upgrade a 100% free app &amp; support a crew of indie developers: WHAT THE FUCK, I'M NOT MADE OF MONEY, I HAVE A FAMILY TO FEED, ONE-STAR.",
    "id" : 975854126138617857,
    "created_at" : "2018-03-19 21:58:48 +0000",
    "user" : {
      "name" : "Dan Kim",
      "screen_name" : "dankim",
      "protected" : false,
      "id_str" : "7979212",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/927922400347398145\/00oGSiiO_normal.jpg",
      "id" : 7979212,
      "verified" : false
    }
  },
  "id" : 976217540421652480,
  "created_at" : "2018-03-20 22:02:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scott Hanselman",
      "screen_name" : "shanselman",
      "indices" : [ 3, 14 ],
      "id_str" : "5676102",
      "id" : 5676102
    }, {
      "name" : "Microsoft Azure",
      "screen_name" : "Azure",
      "indices" : [ 55, 61 ],
      "id_str" : "17000457",
      "id" : 17000457
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976193404907458560",
  "text" : "RT @shanselman: Very clever site breaking down all the @azure VM sizes, what you get, how fast, CPUs, disk, bandwidth, IOPS, everything. ht\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Microsoft Azure",
        "screen_name" : "Azure",
        "indices" : [ 39, 45 ],
        "id_str" : "17000457",
        "id" : 17000457
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/NO3ii9IqyU",
        "expanded_url" : "http:\/\/www.azureinstances.info\/",
        "display_url" : "azureinstances.info"
      } ]
    },
    "geo" : { },
    "id_str" : "975892406410166272",
    "text" : "Very clever site breaking down all the @azure VM sizes, what you get, how fast, CPUs, disk, bandwidth, IOPS, everything. https:\/\/t.co\/NO3ii9IqyU",
    "id" : 975892406410166272,
    "created_at" : "2018-03-20 00:30:55 +0000",
    "user" : {
      "name" : "Scott Hanselman",
      "screen_name" : "shanselman",
      "protected" : false,
      "id_str" : "5676102",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/951729445009637376\/0V25QMDk_normal.jpg",
      "id" : 5676102,
      "verified" : true
    }
  },
  "id" : 976193404907458560,
  "created_at" : "2018-03-20 20:26:59 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Doc Norton",
      "screen_name" : "DocOnDev",
      "indices" : [ 3, 12 ],
      "id_str" : "21751427",
      "id" : 21751427
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976192881236070400",
  "text" : "RT @DocOnDev: Merit rating\n  rewards people\n    that do well in the system.\n\nIt does not\n  reward attempts\n    to improve the system\n\n- Dem\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "976094809462116352",
    "text" : "Merit rating\n  rewards people\n    that do well in the system.\n\nIt does not\n  reward attempts\n    to improve the system\n\n- Deming",
    "id" : 976094809462116352,
    "created_at" : "2018-03-20 13:55:12 +0000",
    "user" : {
      "name" : "Doc Norton",
      "screen_name" : "DocOnDev",
      "protected" : false,
      "id_str" : "21751427",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/831515338005114881\/qllPDO7w_normal.jpg",
      "id" : 21751427,
      "verified" : false
    }
  },
  "id" : 976192881236070400,
  "created_at" : "2018-03-20 20:24:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Command Line Magic",
      "screen_name" : "climagic",
      "indices" : [ 3, 12 ],
      "id_str" : "91333167",
      "id" : 91333167
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "RaspberryPi",
      "indices" : [ 67, 79 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976145879211085824",
  "text" : "RT @climagic: A week ago I had to be escorted to checkout to buy a #RaspberryPi.\rYesterday someone at another store tried to tell me they a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/suso.suso.org\/xulu\/Command_Line_Magic\" rel=\"nofollow\"\u003ECLI Magic poster\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "RaspberryPi",
        "indices" : [ 53, 65 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975739719396085760",
    "text" : "A week ago I had to be escorted to checkout to buy a #RaspberryPi.\rYesterday someone at another store tried to tell me they are illegal.\rI think ID-required and 21-day waiting periods are next.\rSingle board computing is not a crime.",
    "id" : 975739719396085760,
    "created_at" : "2018-03-19 14:24:12 +0000",
    "user" : {
      "name" : "Command Line Magic",
      "screen_name" : "climagic",
      "protected" : false,
      "id_str" : "91333167",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/535876218\/climagic-icon_normal.png",
      "id" : 91333167,
      "verified" : false
    }
  },
  "id" : 976145879211085824,
  "created_at" : "2018-03-20 17:18:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976126830934740994",
  "text" : "\"Make your kitchen a no-fly zone for less this summer\" An email newsletter subject line from a catering equipment supplier.",
  "id" : 976126830934740994,
  "created_at" : "2018-03-20 16:02:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "Bloomberg",
      "screen_name" : "business",
      "indices" : [ 109, 118 ],
      "id_str" : "34713362",
      "id" : 34713362
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/xdSPjUmT17",
      "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2018-03-20\/singtel-seeks-edge-with-cross-border-mobile-wallets-platform",
      "display_url" : "bloomberg.com\/news\/articles\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976114513710809088",
  "text" : "RT @edwinksl: Singtel is tapping 50 million users making mobile payments in Asia https:\/\/t.co\/xdSPjUmT17 via @business",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Bloomberg",
        "screen_name" : "business",
        "indices" : [ 95, 104 ],
        "id_str" : "34713362",
        "id" : 34713362
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/xdSPjUmT17",
        "expanded_url" : "https:\/\/www.bloomberg.com\/news\/articles\/2018-03-20\/singtel-seeks-edge-with-cross-border-mobile-wallets-platform",
        "display_url" : "bloomberg.com\/news\/articles\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "976108910095585281",
    "text" : "Singtel is tapping 50 million users making mobile payments in Asia https:\/\/t.co\/xdSPjUmT17 via @business",
    "id" : 976108910095585281,
    "created_at" : "2018-03-20 14:51:14 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 976114513710809088,
  "created_at" : "2018-03-20 15:13:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 143, 166 ],
      "url" : "https:\/\/t.co\/LxLR8PsKfd",
      "expanded_url" : "http:\/\/veekaybee.github.io\/2015\/05\/30\/static-sites-suck\/",
      "display_url" : "veekaybee.github.io\/2015\/05\/30\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "976094441382535168",
  "text" : "From experience, starting a blog with Jekyll or another static site generator is an undertaking.  3 years ago I will not kill myself doing it. https:\/\/t.co\/LxLR8PsKfd",
  "id" : 976094441382535168,
  "created_at" : "2018-03-20 13:53:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "IrishGradsAssnSg",
      "screen_name" : "Irish_Graduates",
      "indices" : [ 3, 19 ],
      "id_str" : "3269666112",
      "id" : 3269666112
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 38, 52 ]
    }, {
      "text" : "Singapore",
      "indices" : [ 63, 73 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976035352338223104",
  "text" : "RT @Irish_Graduates: Yes, there was a #StPatricksDay parade in #Singapore. Yes, the Irish Graduates Association of Singapore members were t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Irish_Graduates\/status\/975974474913742848\/photo\/1",
        "indices" : [ 146, 169 ],
        "url" : "https:\/\/t.co\/tpbmvJJGhm",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYtblzcU8AAH2-7.jpg",
        "id_str" : "975974449278152704",
        "id" : 975974449278152704,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYtblzcU8AAH2-7.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1156,
          "resize" : "fit",
          "w" : 1495
        }, {
          "h" : 1156,
          "resize" : "fit",
          "w" : 1495
        }, {
          "h" : 928,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 526,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/tpbmvJJGhm"
      } ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 17, 31 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 42, 52 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975974474913742848",
    "text" : "Yes, there was a #StPatricksDay parade in #Singapore. Yes, the Irish Graduates Association of Singapore members were there. We even had a banner! https:\/\/t.co\/tpbmvJJGhm",
    "id" : 975974474913742848,
    "created_at" : "2018-03-20 05:57:02 +0000",
    "user" : {
      "name" : "IrishGradsAssnSg",
      "screen_name" : "Irish_Graduates",
      "protected" : false,
      "id_str" : "3269666112",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/931066906752118784\/kvxcx5Qq_normal.jpg",
      "id" : 3269666112,
      "verified" : false
    }
  },
  "id" : 976035352338223104,
  "created_at" : "2018-03-20 09:58:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Barack Obama",
      "screen_name" : "BarackObama",
      "indices" : [ 3, 15 ],
      "id_str" : "813286",
      "id" : 813286
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "976015673708240897",
  "text" : "RT @BarackObama: In Singapore with young people who are advocating for education, empowering young women, and getting involved all over Sou\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 214, 237 ],
        "url" : "https:\/\/t.co\/so4Uqouu8M",
        "expanded_url" : "https:\/\/twitter.com\/ganeshmuren\/status\/975711729609920512",
        "display_url" : "twitter.com\/ganeshmuren\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975751255296487424",
    "text" : "In Singapore with young people who are advocating for education, empowering young women, and getting involved all over Southeast Asia with a profoundly optimistic commitment to building the world they want to see. https:\/\/t.co\/so4Uqouu8M",
    "id" : 975751255296487424,
    "created_at" : "2018-03-19 15:10:02 +0000",
    "user" : {
      "name" : "Barack Obama",
      "screen_name" : "BarackObama",
      "protected" : false,
      "id_str" : "813286",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/822547732376207360\/5g0FC8XX_normal.jpg",
      "id" : 813286,
      "verified" : true
    }
  },
  "id" : 976015673708240897,
  "created_at" : "2018-03-20 08:40:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "CambridgeAnalytics",
      "indices" : [ 92, 111 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975911483610730496",
  "text" : "Before this evening: I am a data science nerd.\nNow: puts hands behind back while whistling.\n#CambridgeAnalytics",
  "id" : 975911483610730496,
  "created_at" : "2018-03-20 01:46:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Python",
      "indices" : [ 71, 78 ]
    } ],
    "urls" : [ {
      "indices" : [ 135, 158 ],
      "url" : "https:\/\/t.co\/EzTOwiazD5",
      "expanded_url" : "https:\/\/theintercept.com\/2018\/03\/16\/twitter-bot-detector-software\/",
      "display_url" : "theintercept.com\/2018\/03\/16\/twi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975906788980723713",
  "text" : "\"He then wrote a few hundred lines of code in the programming language #Python to create a bot to hunt down and expose fake accounts.\" https:\/\/t.co\/EzTOwiazD5",
  "id" : 975906788980723713,
  "created_at" : "2018-03-20 01:28:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jamillah Knowles \uD83D\uDCBE",
      "screen_name" : "jemimah_knight",
      "indices" : [ 3, 18 ],
      "id_str" : "5945982",
      "id" : 5945982
    }, {
      "name" : "Linda Noakes",
      "screen_name" : "lnoakes",
      "indices" : [ 94, 102 ],
      "id_str" : "20457046",
      "id" : 20457046
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/3J2bHbTwFa",
      "expanded_url" : "https:\/\/twitter.com\/ThatEricAlper\/status\/974105292106444800",
      "display_url" : "twitter.com\/ThatEricAlper\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975904328966967296",
  "text" : "RT @jemimah_knight: This is still making me laugh and I've watched it five times already. h\/t @lnoakes https:\/\/t.co\/3J2bHbTwFa",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Linda Noakes",
        "screen_name" : "lnoakes",
        "indices" : [ 74, 82 ],
        "id_str" : "20457046",
        "id" : 20457046
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/3J2bHbTwFa",
        "expanded_url" : "https:\/\/twitter.com\/ThatEricAlper\/status\/974105292106444800",
        "display_url" : "twitter.com\/ThatEricAlper\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "974635480523399168",
    "text" : "This is still making me laugh and I've watched it five times already. h\/t @lnoakes https:\/\/t.co\/3J2bHbTwFa",
    "id" : 974635480523399168,
    "created_at" : "2018-03-16 13:16:21 +0000",
    "user" : {
      "name" : "Jamillah Knowles \uD83D\uDCBE",
      "screen_name" : "jemimah_knight",
      "protected" : false,
      "id_str" : "5945982",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/532681426714382337\/b2tHMG0V_normal.jpeg",
      "id" : 5945982,
      "verified" : true
    }
  },
  "id" : 975904328966967296,
  "created_at" : "2018-03-20 01:18:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 113 ],
      "url" : "https:\/\/t.co\/Ooz8xtl2Mf",
      "expanded_url" : "https:\/\/www.todayonline.com\/singapore\/tpg-singapores-4th-telco-offers-2-year-free-mobile-plan-citizens-aged-65-and-above#.WrBa73X50zg.twitter",
      "display_url" : "todayonline.com\/singapore\/tpg-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975897484693254144",
  "text" : "TPG, Singapore\u2019s 4th telco, offers 2-year free mobile plan for citizens aged 65 and above https:\/\/t.co\/Ooz8xtl2Mf",
  "id" : 975897484693254144,
  "created_at" : "2018-03-20 00:51:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "onlinemarketing",
      "indices" : [ 78, 94 ]
    }, {
      "text" : "advertising",
      "indices" : [ 95, 107 ]
    }, {
      "text" : "digitalmarketing",
      "indices" : [ 108, 125 ]
    }, {
      "text" : "ppcchat",
      "indices" : [ 126, 134 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975863167447388160",
  "text" : "RT @brosindigital: The most terrifying question a client can ask an agency...\n#onlinemarketing #advertising #digitalmarketing #ppcchat #seo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/brosindigital\/status\/975850228715806721\/photo\/1",
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/Vjx3YIsUSD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYrqnFnXcAExAgy.jpg",
        "id_str" : "975850226522222593",
        "id" : 975850226522222593,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYrqnFnXcAExAgy.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 787,
          "resize" : "fit",
          "w" : 1100
        }, {
          "h" : 787,
          "resize" : "fit",
          "w" : 1100
        }, {
          "h" : 487,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 787,
          "resize" : "fit",
          "w" : 1100
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Vjx3YIsUSD"
      } ],
      "hashtags" : [ {
        "text" : "onlinemarketing",
        "indices" : [ 59, 75 ]
      }, {
        "text" : "advertising",
        "indices" : [ 76, 88 ]
      }, {
        "text" : "digitalmarketing",
        "indices" : [ 89, 106 ]
      }, {
        "text" : "ppcchat",
        "indices" : [ 107, 115 ]
      }, {
        "text" : "seochat",
        "indices" : [ 116, 124 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975850228715806721",
    "text" : "The most terrifying question a client can ask an agency...\n#onlinemarketing #advertising #digitalmarketing #ppcchat #seochat https:\/\/t.co\/Vjx3YIsUSD",
    "id" : 975850228715806721,
    "created_at" : "2018-03-19 21:43:19 +0000",
    "user" : {
      "name" : "We Are Bind",
      "screen_name" : "wearebindltd",
      "protected" : false,
      "id_str" : "973601438235529216",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/975116965101654017\/2dXqDZQ2_normal.jpg",
      "id" : 973601438235529216,
      "verified" : false
    }
  },
  "id" : 975863167447388160,
  "created_at" : "2018-03-19 22:34:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sarah Wheeler",
      "screen_name" : "MyLostRomance",
      "indices" : [ 0, 14 ],
      "id_str" : "17593753",
      "id" : 17593753
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "974690828181524480",
  "geo" : { },
  "id_str" : "975816172418555908",
  "in_reply_to_user_id" : 17593753,
  "text" : "@MyLostRomance Are u able to follow me back so I can DM u? Thanks",
  "id" : 975816172418555908,
  "in_reply_to_status_id" : 974690828181524480,
  "created_at" : "2018-03-19 19:27:59 +0000",
  "in_reply_to_screen_name" : "MyLostRomance",
  "in_reply_to_user_id_str" : "17593753",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elaine (fierce shining light) Larkin",
      "screen_name" : "elainelarkin",
      "indices" : [ 3, 16 ],
      "id_str" : "15452449",
      "id" : 15452449
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975815235264241669",
  "text" : "RT @elainelarkin: Technical question. I have two legacy free G Suite accounts and a few domain names I'd like to get rid of, I'd like to mi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975354105693696000",
    "text" : "Technical question. I have two legacy free G Suite accounts and a few domain names I'd like to get rid of, I'd like to migrate the emails into one email a\/c ideally my personal gmail and I don't think I'm able for the headwreckery to do it myself. Ideas?",
    "id" : 975354105693696000,
    "created_at" : "2018-03-18 12:51:54 +0000",
    "user" : {
      "name" : "Elaine (fierce shining light) Larkin",
      "screen_name" : "elainelarkin",
      "protected" : false,
      "id_str" : "15452449",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1039984575567613952\/fsQrS8gm_normal.jpg",
      "id" : 15452449,
      "verified" : false
    }
  },
  "id" : 975815235264241669,
  "created_at" : "2018-03-19 19:24:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Simon",
      "screen_name" : "mbsimon",
      "indices" : [ 3, 11 ],
      "id_str" : "28867988",
      "id" : 28867988
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975815099096190976",
  "text" : "RT @mbsimon: \uD83D\uDC4B I ran the Obama 2008 data-driven microtargeting team. How dare you! We didn\u2019t steal private Facebook profile data from voter\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 276, 299 ],
        "url" : "https:\/\/t.co\/CyVIFDB2Tz",
        "expanded_url" : "https:\/\/twitter.com\/CamAnalytica\/status\/975081782625226752",
        "display_url" : "twitter.com\/CamAnalytica\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975231597183229953",
    "text" : "\uD83D\uDC4B I ran the Obama 2008 data-driven microtargeting team. How dare you! We didn\u2019t steal private Facebook profile data from voters under false pretenses. OFA voluntarily solicited opinions of hundreds of thousands of voters. We didn\u2019t commit theft to do our groundbreaking work. https:\/\/t.co\/CyVIFDB2Tz",
    "id" : 975231597183229953,
    "created_at" : "2018-03-18 04:45:06 +0000",
    "user" : {
      "name" : "Michael Simon",
      "screen_name" : "mbsimon",
      "protected" : false,
      "id_str" : "28867988",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/573526145674563584\/iIe1o-8C_normal.jpeg",
      "id" : 28867988,
      "verified" : true
    }
  },
  "id" : 975815099096190976,
  "created_at" : "2018-03-19 19:23:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "indices" : [ 3, 16 ],
      "id_str" : "36889519",
      "id" : 36889519
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/975746472892420096\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/NZ8GjxUJAg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYqMPoyU0AA2Gt9.jpg",
      "id_str" : "975746469553557504",
      "id" : 975746469553557504,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYqMPoyU0AA2Gt9.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NZ8GjxUJAg"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/975746472892420096\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/NZ8GjxUJAg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYqMPoiVQAABajm.jpg",
      "id_str" : "975746469486477312",
      "id" : 975746469486477312,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYqMPoiVQAABajm.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NZ8GjxUJAg"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/975746472892420096\/photo\/1",
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/NZ8GjxUJAg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYqMPrFU8AAnox9.jpg",
      "id_str" : "975746470170128384",
      "id" : 975746470170128384,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYqMPrFU8AAnox9.jpg",
      "sizes" : [ {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/NZ8GjxUJAg"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975748072218165249",
  "text" : "RT @RaminNasibov: Colorful Playground In Hong Kong (by Guillaume Dutreix) https:\/\/t.co\/NZ8GjxUJAg",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/975746472892420096\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/NZ8GjxUJAg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYqMPoyU0AA2Gt9.jpg",
        "id_str" : "975746469553557504",
        "id" : 975746469553557504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYqMPoyU0AA2Gt9.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NZ8GjxUJAg"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/975746472892420096\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/NZ8GjxUJAg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYqMPoiVQAABajm.jpg",
        "id_str" : "975746469486477312",
        "id" : 975746469486477312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYqMPoiVQAABajm.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NZ8GjxUJAg"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/975746472892420096\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/NZ8GjxUJAg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYqMPrFU8AAnox9.jpg",
        "id_str" : "975746470170128384",
        "id" : 975746470170128384,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYqMPrFU8AAnox9.jpg",
        "sizes" : [ {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/NZ8GjxUJAg"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975746472892420096",
    "text" : "Colorful Playground In Hong Kong (by Guillaume Dutreix) https:\/\/t.co\/NZ8GjxUJAg",
    "id" : 975746472892420096,
    "created_at" : "2018-03-19 14:51:02 +0000",
    "user" : {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "protected" : false,
      "id_str" : "36889519",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/896399812450557953\/gP3PRbXP_normal.jpg",
      "id" : 36889519,
      "verified" : true
    }
  },
  "id" : 975748072218165249,
  "created_at" : "2018-03-19 14:57:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Lee",
      "screen_name" : "AdrianLeeSA",
      "indices" : [ 3, 15 ],
      "id_str" : "33936903",
      "id" : 33936903
    }, {
      "name" : "Forbes",
      "screen_name" : "Forbes",
      "indices" : [ 124, 131 ],
      "id_str" : "91478624",
      "id" : 91478624
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975747892450414593",
  "text" : "RT @AdrianLeeSA: Jack-ing the SE Asia markets... \"Alibaba Doubles Down On Southeast Asia, Pumps Another $2B Into Lazada via @forbes https:\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Forbes",
        "screen_name" : "Forbes",
        "indices" : [ 107, 114 ],
        "id_str" : "91478624",
        "id" : 91478624
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/vNns9yW8vb",
        "expanded_url" : "https:\/\/www.forbes.com\/sites\/russellflannery\/2018\/03\/19\/alibaba-doubles-down-on-southeast-asia-pumps-another-2-bln-into-lazada\/#212c0b7294c5",
        "display_url" : "forbes.com\/sites\/russellf\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975734792854614016",
    "text" : "Jack-ing the SE Asia markets... \"Alibaba Doubles Down On Southeast Asia, Pumps Another $2B Into Lazada via @forbes https:\/\/t.co\/vNns9yW8vb\"",
    "id" : 975734792854614016,
    "created_at" : "2018-03-19 14:04:37 +0000",
    "user" : {
      "name" : "Adrian Lee",
      "screen_name" : "AdrianLeeSA",
      "protected" : false,
      "id_str" : "33936903",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/807449488793038848\/rQrUoExA_normal.jpg",
      "id" : 33936903,
      "verified" : false
    }
  },
  "id" : 975747892450414593,
  "created_at" : "2018-03-19 14:56:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "indices" : [ 3, 11 ],
      "id_str" : "1000417794",
      "id" : 1000417794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975689406068731904",
  "text" : "RT @hireyap: Mobile bringing most traffic to e-commerce sites in SEA - Southeast Asia (Malaysia, Singapore, Vietnam, The Philippines, Indon\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 191, 214 ],
        "url" : "https:\/\/t.co\/5cRao2GOO1",
        "expanded_url" : "http:\/\/techwireasia.com\/2018\/02\/smart-e-commerce-insights-southeast-asia\/",
        "display_url" : "techwireasia.com\/2018\/02\/smart-\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975313459473801216",
    "text" : "Mobile bringing most traffic to e-commerce sites in SEA - Southeast Asia (Malaysia, Singapore, Vietnam, The Philippines, Indonesia, Thailand.) When it comes to conversion, desktop is king.   https:\/\/t.co\/5cRao2GOO1",
    "id" : 975313459473801216,
    "created_at" : "2018-03-18 10:10:23 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 975689406068731904,
  "created_at" : "2018-03-19 11:04:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/515FQUT3Gb",
      "expanded_url" : "https:\/\/twitter.com\/Aidan_Regan\/status\/975379378963087361",
      "display_url" : "twitter.com\/Aidan_Regan\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975688597713113089",
  "text" : "The shit hit the fan. https:\/\/t.co\/515FQUT3Gb",
  "id" : 975688597713113089,
  "created_at" : "2018-03-19 11:01:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Aidan Regan",
      "screen_name" : "Aidan_Regan",
      "indices" : [ 3, 15 ],
      "id_str" : "923574392",
      "id" : 923574392
    }, {
      "name" : "Rebecca Moynihan",
      "screen_name" : "RebeccaMoy",
      "indices" : [ 17, 28 ],
      "id_str" : "6294502",
      "id" : 6294502
    }, {
      "name" : "Kevins Hurling & Camogie",
      "screen_name" : "KevinsHurling",
      "indices" : [ 29, 43 ],
      "id_str" : "600983865",
      "id" : 600983865
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975688091569664000",
  "text" : "RT @Aidan_Regan: @RebeccaMoy @KevinsHurling My partner put it perfectly: how is it even legal that public money is used to subsidize a priv\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rebecca Moynihan",
        "screen_name" : "RebeccaMoy",
        "indices" : [ 0, 11 ],
        "id_str" : "6294502",
        "id" : 6294502
      }, {
        "name" : "Kevins Hurling & Camogie",
        "screen_name" : "KevinsHurling",
        "indices" : [ 12, 26 ],
        "id_str" : "600983865",
        "id" : 600983865
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "975409170689220608",
    "geo" : { },
    "id_str" : "975484022813011968",
    "in_reply_to_user_id" : 6294502,
    "text" : "@RebeccaMoy @KevinsHurling My partner put it perfectly: how is it even legal that public money is used to subsidize a private fee paying school?",
    "id" : 975484022813011968,
    "in_reply_to_status_id" : 975409170689220608,
    "created_at" : "2018-03-18 21:28:09 +0000",
    "in_reply_to_screen_name" : "RebeccaMoy",
    "in_reply_to_user_id_str" : "6294502",
    "user" : {
      "name" : "Aidan Regan",
      "screen_name" : "Aidan_Regan",
      "protected" : false,
      "id_str" : "923574392",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/772426565254774784\/-s9UZa6z_normal.jpg",
      "id" : 923574392,
      "verified" : true
    }
  },
  "id" : 975688091569664000,
  "created_at" : "2018-03-19 10:59:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/qiJqH4OsAx",
      "expanded_url" : "https:\/\/docs.docker.com\/toolbox\/toolbox_install_windows\/#looking-for-troubleshooting-help",
      "display_url" : "docs.docker.com\/toolbox\/toolbo\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "975349922680791040",
  "geo" : { },
  "id_str" : "975351242326200321",
  "in_reply_to_user_id" : 9465632,
  "text" : "By the way, I am using Docker Toolbox on Windows 8. \"A Legacy desktop solution\". https:\/\/t.co\/qiJqH4OsAx",
  "id" : 975351242326200321,
  "in_reply_to_status_id" : 975349922680791040,
  "created_at" : "2018-03-18 12:40:31 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Docker",
      "indices" : [ 0, 7 ]
    } ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/Lq0KvFNm4F",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/973494588928331776",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975349922680791040",
  "text" : "#Docker Go easy as I am not a software engineer\/developer. Thank You https:\/\/t.co\/Lq0KvFNm4F",
  "id" : 975349922680791040,
  "created_at" : "2018-03-18 12:35:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Marianne Lee",
      "screen_name" : "m4riannelee",
      "indices" : [ 0, 12 ],
      "id_str" : "168148402",
      "id" : 168148402
    }, {
      "name" : "k-leb k",
      "screen_name" : "dcatdemon",
      "indices" : [ 13, 23 ],
      "id_str" : "130048954",
      "id" : 130048954
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "sneachta",
      "indices" : [ 58, 67 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "975321252444495872",
  "geo" : { },
  "id_str" : "975326389938393088",
  "in_reply_to_user_id" : 168148402,
  "text" : "@m4riannelee @dcatdemon It snowing shower here in Dublin. #sneachta",
  "id" : 975326389938393088,
  "in_reply_to_status_id" : 975321252444495872,
  "created_at" : "2018-03-18 11:01:46 +0000",
  "in_reply_to_screen_name" : "m4riannelee",
  "in_reply_to_user_id_str" : "168148402",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Satbir Singh",
      "screen_name" : "SatbirLSingh",
      "indices" : [ 3, 16 ],
      "id_str" : "57490139",
      "id" : 57490139
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975319424302448640",
  "text" : "RT @SatbirLSingh: Pro tip: when you're talking policy and you can't tell if someone's genuinely well informed or just really confident abou\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Wakanda",
        "indices" : [ 192, 200 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974651278944079872",
    "text" : "Pro tip: when you're talking policy and you can't tell if someone's genuinely well informed or just really confident about everything they say, tell them you're worried about the situation in #Wakanda. Three people have already told me they're worried too.",
    "id" : 974651278944079872,
    "created_at" : "2018-03-16 14:19:07 +0000",
    "user" : {
      "name" : "Satbir Singh",
      "screen_name" : "SatbirLSingh",
      "protected" : false,
      "id_str" : "57490139",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/987720495746428928\/vfVigPx3_normal.jpg",
      "id" : 57490139,
      "verified" : false
    }
  },
  "id" : 975319424302448640,
  "created_at" : "2018-03-18 10:34:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "975309894034354176",
  "geo" : { },
  "id_str" : "975310104026402816",
  "in_reply_to_user_id" : 9465632,
  "text" : "SEA - Southeast Asia (e.g. Malaysia, Singapore, Vietnam, The Philippines, Indonesia, Thailand)",
  "id" : 975310104026402816,
  "in_reply_to_status_id" : 975309894034354176,
  "created_at" : "2018-03-18 09:57:03 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "975309256584032257",
  "geo" : { },
  "id_str" : "975309894034354176",
  "in_reply_to_user_id" : 9465632,
  "text" : "Though mobile is bringing in the most traffic to e-commerce sites in SEA, when it comes to conversion rates,  desktop reign. Desktop tend to have a higher purchase intent and more time to go through with the purchase.",
  "id" : 975309894034354176,
  "in_reply_to_status_id" : 975309256584032257,
  "created_at" : "2018-03-18 09:56:13 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/j98W9AaXv0",
      "expanded_url" : "http:\/\/techwireasia.com\/2018\/02\/smart-e-commerce-insights-southeast-asia\/",
      "display_url" : "techwireasia.com\/2018\/02\/smart-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975309256584032257",
  "text" : "What you should know about the state of e-commerce in Southeast Asia - https:\/\/t.co\/j98W9AaXv0",
  "id" : 975309256584032257,
  "created_at" : "2018-03-18 09:53:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "indices" : [ 3, 14 ],
      "id_str" : "2870653293",
      "id" : 2870653293
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975308573432537090",
  "text" : "RT @susie_dent: Sitzmark: an impression in the snow made by someone falling backwards onto their bottom (also available as a verb). Morning.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975305657808228352",
    "text" : "Sitzmark: an impression in the snow made by someone falling backwards onto their bottom (also available as a verb). Morning.",
    "id" : 975305657808228352,
    "created_at" : "2018-03-18 09:39:23 +0000",
    "user" : {
      "name" : "Susie Dent",
      "screen_name" : "susie_dent",
      "protected" : false,
      "id_str" : "2870653293",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/531825590878228480\/4s2F9F6W_normal.jpeg",
      "id" : 2870653293,
      "verified" : true
    }
  },
  "id" : 975308573432537090,
  "created_at" : "2018-03-18 09:50:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/hYVVYVr6Tb",
      "expanded_url" : "https:\/\/www.rockpapershotgun.com\/2018\/03\/15\/all-rise-and-no-fall-how-civilization-reinforces-a-dangerous-myth\/",
      "display_url" : "rockpapershotgun.com\/2018\/03\/15\/all\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975273023749197824",
  "text" : "RT @edwinksl: All rise and no fall: how Civilization reinforces a dangerous myth https:\/\/t.co\/hYVVYVr6Tb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 67, 90 ],
        "url" : "https:\/\/t.co\/hYVVYVr6Tb",
        "expanded_url" : "https:\/\/www.rockpapershotgun.com\/2018\/03\/15\/all-rise-and-no-fall-how-civilization-reinforces-a-dangerous-myth\/",
        "display_url" : "rockpapershotgun.com\/2018\/03\/15\/all\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975224880441495552",
    "text" : "All rise and no fall: how Civilization reinforces a dangerous myth https:\/\/t.co\/hYVVYVr6Tb",
    "id" : 975224880441495552,
    "created_at" : "2018-03-18 04:18:24 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 975273023749197824,
  "created_at" : "2018-03-18 07:29:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "indices" : [ 3, 7 ],
      "id_str" : "5120691",
      "id" : 5120691
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/wef\/status\/975250460151025664\/photo\/1",
      "indices" : [ 116, 139 ],
      "url" : "https:\/\/t.co\/k3VFr2UMfb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYjJH9DV4AA8fyS.jpg",
      "id_str" : "975250457810558976",
      "id" : 975250457810558976,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYjJH9DV4AA8fyS.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      }, {
        "h" : 295,
        "resize" : "fit",
        "w" : 485
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/k3VFr2UMfb"
    } ],
    "hashtags" : [ {
      "text" : "stress",
      "indices" : [ 108, 115 ]
    } ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/2XXwujonmU",
      "expanded_url" : "http:\/\/wef.ch\/2GE0VRK",
      "display_url" : "wef.ch\/2GE0VRK"
    } ]
  },
  "geo" : { },
  "id_str" : "975269157813084160",
  "text" : "RT @wef: Working yourself to death is such a problem in Japan there's a name for it https:\/\/t.co\/2XXwujonmU #stress https:\/\/t.co\/k3VFr2UMfb",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/wef\/status\/975250460151025664\/photo\/1",
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/k3VFr2UMfb",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYjJH9DV4AA8fyS.jpg",
        "id_str" : "975250457810558976",
        "id" : 975250457810558976,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYjJH9DV4AA8fyS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        }, {
          "h" : 295,
          "resize" : "fit",
          "w" : 485
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/k3VFr2UMfb"
      } ],
      "hashtags" : [ {
        "text" : "stress",
        "indices" : [ 99, 106 ]
      } ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/2XXwujonmU",
        "expanded_url" : "http:\/\/wef.ch\/2GE0VRK",
        "display_url" : "wef.ch\/2GE0VRK"
      } ]
    },
    "geo" : { },
    "id_str" : "975250460151025664",
    "text" : "Working yourself to death is such a problem in Japan there's a name for it https:\/\/t.co\/2XXwujonmU #stress https:\/\/t.co\/k3VFr2UMfb",
    "id" : 975250460151025664,
    "created_at" : "2018-03-18 06:00:03 +0000",
    "user" : {
      "name" : "World Economic Forum",
      "screen_name" : "wef",
      "protected" : false,
      "id_str" : "5120691",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/565498192171507712\/r2Hb2gvX_normal.png",
      "id" : 5120691,
      "verified" : true
    }
  },
  "id" : 975269157813084160,
  "created_at" : "2018-03-18 07:14:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/975140744112558080\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/OOgLzOAMZ1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYhlVsiWAAA-9va.jpg",
      "id_str" : "975140742732578816",
      "id" : 975140742732578816,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYhlVsiWAAA-9va.jpg",
      "sizes" : [ {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/OOgLzOAMZ1"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/VCvqnCjw10",
      "expanded_url" : "http:\/\/ift.tt\/2plUyKC",
      "display_url" : "ift.tt\/2plUyKC"
    } ]
  },
  "geo" : { },
  "id_str" : "975140744112558080",
  "text" : "Red pepper stuffed with Quorn Mince top with sliced cheese pop into oven https:\/\/t.co\/VCvqnCjw10 https:\/\/t.co\/OOgLzOAMZ1",
  "id" : 975140744112558080,
  "created_at" : "2018-03-17 22:44:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/975110887487844352\/photo\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/Yq3zAMxcIJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYhJ9C0VQAAy0Zr.jpg",
      "id_str" : "975110632402927616",
      "id" : 975110632402927616,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYhJ9C0VQAAy0Zr.jpg",
      "sizes" : [ {
        "h" : 481,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1414,
        "resize" : "fit",
        "w" : 2000
      }, {
        "h" : 1414,
        "resize" : "fit",
        "w" : 2000
      }, {
        "h" : 848,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Yq3zAMxcIJ"
    } ],
    "hashtags" : [ {
      "text" : "IKEA",
      "indices" : [ 43, 48 ]
    }, {
      "text" : "programming",
      "indices" : [ 94, 106 ]
    } ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/9V2BXCqa3i",
      "expanded_url" : "https:\/\/idea-instructions.com\/",
      "display_url" : "idea-instructions.com"
    } ]
  },
  "geo" : { },
  "id_str" : "975126118259978240",
  "text" : "RT @randal_olson: Explaining algorithms as #IKEA instruction manuals: https:\/\/t.co\/9V2BXCqa3i #programming https:\/\/t.co\/Yq3zAMxcIJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/975110887487844352\/photo\/1",
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/Yq3zAMxcIJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYhJ9C0VQAAy0Zr.jpg",
        "id_str" : "975110632402927616",
        "id" : 975110632402927616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYhJ9C0VQAAy0Zr.jpg",
        "sizes" : [ {
          "h" : 481,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1414,
          "resize" : "fit",
          "w" : 2000
        }, {
          "h" : 1414,
          "resize" : "fit",
          "w" : 2000
        }, {
          "h" : 848,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Yq3zAMxcIJ"
      } ],
      "hashtags" : [ {
        "text" : "IKEA",
        "indices" : [ 25, 30 ]
      }, {
        "text" : "programming",
        "indices" : [ 76, 88 ]
      } ],
      "urls" : [ {
        "indices" : [ 52, 75 ],
        "url" : "https:\/\/t.co\/9V2BXCqa3i",
        "expanded_url" : "https:\/\/idea-instructions.com\/",
        "display_url" : "idea-instructions.com"
      } ]
    },
    "geo" : { },
    "id_str" : "975110887487844352",
    "text" : "Explaining algorithms as #IKEA instruction manuals: https:\/\/t.co\/9V2BXCqa3i #programming https:\/\/t.co\/Yq3zAMxcIJ",
    "id" : 975110887487844352,
    "created_at" : "2018-03-17 20:45:26 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 975126118259978240,
  "created_at" : "2018-03-17 21:45:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Valerian Saliou",
      "screen_name" : "valeriansaliou",
      "indices" : [ 3, 18 ],
      "id_str" : "560204987",
      "id" : 560204987
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975090926098964480",
  "text" : "RT @valeriansaliou: Come on, EU, you\u2019re making me more libertarian each time one of those restrictive laws are proposed. https:\/\/t.co\/VZHsW\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.apple.com\/\" rel=\"nofollow\"\u003EOS X\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/VZHsWruCDU",
        "expanded_url" : "https:\/\/blog.github.com\/2018-03-14-eu-proposal-upload-filters-code\/",
        "display_url" : "blog.github.com\/2018-03-14-eu-\u2026"
      } ]
    },
    "geo" : {
      "type" : "Point",
      "coordinates" : [ 47.494762, 19.060379 ]
    },
    "id_str" : "974243616527921152",
    "text" : "Come on, EU, you\u2019re making me more libertarian each time one of those restrictive laws are proposed. https:\/\/t.co\/VZHsWruCDU",
    "id" : 974243616527921152,
    "created_at" : "2018-03-15 11:19:13 +0000",
    "user" : {
      "name" : "Valerian Saliou",
      "screen_name" : "valeriansaliou",
      "protected" : false,
      "id_str" : "560204987",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963466868974604289\/T_MkOXC9_normal.jpg",
      "id" : 560204987,
      "verified" : false
    }
  },
  "id" : 975090926098964480,
  "created_at" : "2018-03-17 19:26:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "PJ Gallagher",
      "screen_name" : "pjgallagher",
      "indices" : [ 3, 15 ],
      "id_str" : "22624932",
      "id" : 22624932
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/pjgallagher\/status\/975010304873713664\/photo\/1",
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/IaGUQ42KBY",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYfusQLWAAgQyFm.jpg",
      "id_str" : "975010288373202952",
      "id" : 975010288373202952,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYfusQLWAAgQyFm.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 747,
        "resize" : "fit",
        "w" : 1328
      }, {
        "h" : 747,
        "resize" : "fit",
        "w" : 1328
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IaGUQ42KBY"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975063134502350848",
  "text" : "RT @pjgallagher: Happy St Patrick\u2019s Day. How could you not love Ireland. https:\/\/t.co\/IaGUQ42KBY",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/pjgallagher\/status\/975010304873713664\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/IaGUQ42KBY",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYfusQLWAAgQyFm.jpg",
        "id_str" : "975010288373202952",
        "id" : 975010288373202952,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYfusQLWAAgQyFm.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 747,
          "resize" : "fit",
          "w" : 1328
        }, {
          "h" : 747,
          "resize" : "fit",
          "w" : 1328
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IaGUQ42KBY"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "975010304873713664",
    "text" : "Happy St Patrick\u2019s Day. How could you not love Ireland. https:\/\/t.co\/IaGUQ42KBY",
    "id" : 975010304873713664,
    "created_at" : "2018-03-17 14:05:46 +0000",
    "user" : {
      "name" : "PJ Gallagher",
      "screen_name" : "pjgallagher",
      "protected" : false,
      "id_str" : "22624932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028975732582752257\/yluiCAPB_normal.jpg",
      "id" : 22624932,
      "verified" : true
    }
  },
  "id" : 975063134502350848,
  "created_at" : "2018-03-17 17:35:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Bill",
      "screen_name" : "offdutybilll",
      "indices" : [ 0, 13 ],
      "id_str" : "1384135308",
      "id" : 1384135308
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "974939502081867777",
  "geo" : { },
  "id_str" : "975054761224523776",
  "in_reply_to_user_id" : 1384135308,
  "text" : "@offdutybilll Irish Grand Slam on St Patrick's day!",
  "id" : 975054761224523776,
  "in_reply_to_status_id" : 974939502081867777,
  "created_at" : "2018-03-17 17:02:25 +0000",
  "in_reply_to_screen_name" : "offdutybilll",
  "in_reply_to_user_id_str" : "1384135308",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Elian Peltier",
      "screen_name" : "ElianPeltier",
      "indices" : [ 3, 16 ],
      "id_str" : "176478023",
      "id" : 176478023
    }, {
      "name" : "The New York Times",
      "screen_name" : "nytimes",
      "indices" : [ 98, 106 ],
      "id_str" : "807095",
      "id" : 807095
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/yuFLzoGx1K",
      "expanded_url" : "https:\/\/nyti.ms\/2FMlo5o",
      "display_url" : "nyti.ms\/2FMlo5o"
    } ]
  },
  "geo" : { },
  "id_str" : "975052471629500417",
  "text" : "RT @ElianPeltier: Cyberattacks Put Russian Fingers on the Switch at Power Plants, U.S. Says   via @NYTimes https:\/\/t.co\/yuFLzoGx1K",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The New York Times",
        "screen_name" : "nytimes",
        "indices" : [ 80, 88 ],
        "id_str" : "807095",
        "id" : 807095
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 89, 112 ],
        "url" : "https:\/\/t.co\/yuFLzoGx1K",
        "expanded_url" : "https:\/\/nyti.ms\/2FMlo5o",
        "display_url" : "nyti.ms\/2FMlo5o"
      } ]
    },
    "geo" : { },
    "id_str" : "974425064908230656",
    "text" : "Cyberattacks Put Russian Fingers on the Switch at Power Plants, U.S. Says   via @NYTimes https:\/\/t.co\/yuFLzoGx1K",
    "id" : 974425064908230656,
    "created_at" : "2018-03-15 23:20:14 +0000",
    "user" : {
      "name" : "Elian Peltier",
      "screen_name" : "ElianPeltier",
      "protected" : false,
      "id_str" : "176478023",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876732746416234496\/Fa-wtGS4_normal.jpg",
      "id" : 176478023,
      "verified" : true
    }
  },
  "id" : 975052471629500417,
  "created_at" : "2018-03-17 16:53:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "This is Statistics",
      "screen_name" : "ThisisStats",
      "indices" : [ 3, 15 ],
      "id_str" : "2532976740",
      "id" : 2532976740
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Guinness",
      "indices" : [ 28, 37 ]
    }, {
      "text" : "statistics",
      "indices" : [ 96, 107 ]
    }, {
      "text" : "StPatricksDay",
      "indices" : [ 108, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975040131672440834",
  "text" : "RT @ThisisStats: Here\u2019s how #Guinness beer played a role in the discovery of the Student t-test #statistics #StPatricksDay https:\/\/t.co\/TRU\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Guinness",
        "indices" : [ 11, 20 ]
      }, {
        "text" : "statistics",
        "indices" : [ 79, 90 ]
      }, {
        "text" : "StPatricksDay",
        "indices" : [ 91, 105 ]
      } ],
      "urls" : [ {
        "indices" : [ 106, 129 ],
        "url" : "https:\/\/t.co\/TRURCQEsMY",
        "expanded_url" : "http:\/\/thisisstatistics.org\/st-patricks-statistics-legacy-behind-guinness\/",
        "display_url" : "thisisstatistics.org\/st-patricks-st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975032826004373506",
    "text" : "Here\u2019s how #Guinness beer played a role in the discovery of the Student t-test #statistics #StPatricksDay https:\/\/t.co\/TRURCQEsMY",
    "id" : 975032826004373506,
    "created_at" : "2018-03-17 15:35:15 +0000",
    "user" : {
      "name" : "This is Statistics",
      "screen_name" : "ThisisStats",
      "protected" : false,
      "id_str" : "2532976740",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/493824953079197696\/7-n8c8P0_normal.jpeg",
      "id" : 2532976740,
      "verified" : false
    }
  },
  "id" : 975040131672440834,
  "created_at" : "2018-03-17 16:04:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "googleanalytics",
      "indices" : [ 94, 110 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975026071820267520",
  "text" : "How do you keep track of implementation and configuration changes in your web analytics tool? #googleanalytics",
  "id" : 975026071820267520,
  "created_at" : "2018-03-17 15:08:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "From the Flight Deck",
      "screen_name" : "Golfcharlie232",
      "indices" : [ 3, 18 ],
      "id_str" : "930755719",
      "id" : 930755719
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "avgeek",
      "indices" : [ 83, 90 ]
    }, {
      "text" : "pilotsview",
      "indices" : [ 91, 102 ]
    }, {
      "text" : "northernlights",
      "indices" : [ 103, 118 ]
    }, {
      "text" : "Auroraborealis",
      "indices" : [ 119, 134 ]
    } ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/IugGywDoMc",
      "expanded_url" : "https:\/\/petapixel.com\/2018\/03\/16\/this-u-2-spy-plane-pilot-photographed-the-northern-lights-up-close\/",
      "display_url" : "petapixel.com\/2018\/03\/16\/thi\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "975025526065848320",
  "text" : "RT @Golfcharlie232: U-2 photography of the Northern Lights https:\/\/t.co\/IugGywDoMc #avgeek #pilotsview #northernlights #Auroraborealis",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "avgeek",
        "indices" : [ 63, 70 ]
      }, {
        "text" : "pilotsview",
        "indices" : [ 71, 82 ]
      }, {
        "text" : "northernlights",
        "indices" : [ 83, 98 ]
      }, {
        "text" : "Auroraborealis",
        "indices" : [ 99, 114 ]
      } ],
      "urls" : [ {
        "indices" : [ 39, 62 ],
        "url" : "https:\/\/t.co\/IugGywDoMc",
        "expanded_url" : "https:\/\/petapixel.com\/2018\/03\/16\/this-u-2-spy-plane-pilot-photographed-the-northern-lights-up-close\/",
        "display_url" : "petapixel.com\/2018\/03\/16\/thi\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "975025207407730688",
    "text" : "U-2 photography of the Northern Lights https:\/\/t.co\/IugGywDoMc #avgeek #pilotsview #northernlights #Auroraborealis",
    "id" : 975025207407730688,
    "created_at" : "2018-03-17 15:04:59 +0000",
    "user" : {
      "name" : "From the Flight Deck",
      "screen_name" : "Golfcharlie232",
      "protected" : false,
      "id_str" : "930755719",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/675437992551559173\/Fdru0LpD_normal.jpg",
      "id" : 930755719,
      "verified" : false
    }
  },
  "id" : 975025526065848320,
  "created_at" : "2018-03-17 15:06:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/975023130199691264\/photo\/1",
      "indices" : [ 79, 102 ],
      "url" : "https:\/\/t.co\/1QHOl4HzU6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYf6XlFW4AAluHB.jpg",
      "id_str" : "975023127347519488",
      "id" : 975023127347519488,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYf6XlFW4AAluHB.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/1QHOl4HzU6"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/KaNci054XS",
      "expanded_url" : "http:\/\/ift.tt\/2IwVSTv",
      "display_url" : "ift.tt\/2IwVSTv"
    } ]
  },
  "geo" : { },
  "id_str" : "975023130199691264",
  "text" : "Bread is here. Please form an orderly queue. Thank You https:\/\/t.co\/KaNci054XS https:\/\/t.co\/1QHOl4HzU6",
  "id" : 975023130199691264,
  "created_at" : "2018-03-17 14:56:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Keith Coleman \uD83C\uDF31\uD83D\uDE00\uD83D\uDE4C",
      "screen_name" : "kcoleman",
      "indices" : [ 3, 12 ],
      "id_str" : "1076061",
      "id" : 1076061
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "975001776846966784",
  "text" : "RT @kcoleman: OH (from an awesome Lyft driver): \u201CToday has been great. I\u2019ve been blessed by the algorithm.\u201D\n\nImmediately had an eerie feeli\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter for  iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974495158841499648",
    "text" : "OH (from an awesome Lyft driver): \u201CToday has been great. I\u2019ve been blessed by the algorithm.\u201D\n\nImmediately had an eerie feeling that this could become an increasingly common way to describe a day.",
    "id" : 974495158841499648,
    "created_at" : "2018-03-16 03:58:45 +0000",
    "user" : {
      "name" : "Keith Coleman \uD83C\uDF31\uD83D\uDE00\uD83D\uDE4C",
      "screen_name" : "kcoleman",
      "protected" : false,
      "id_str" : "1076061",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/804066370967044096\/XOqnRpML_normal.jpg",
      "id" : 1076061,
      "verified" : false
    }
  },
  "id" : 975001776846966784,
  "created_at" : "2018-03-17 13:31:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "indices" : [ 3, 16 ],
      "id_str" : "36889519",
      "id" : 36889519
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/974986167048392706\/photo\/1",
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/P27BYbkp3R",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYfYuqaX4AIPvYc.jpg",
      "id_str" : "974986140519489538",
      "id" : 974986140519489538,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYfYuqaX4AIPvYc.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 901
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1537
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1537
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/P27BYbkp3R"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/jZhIUaEga7",
      "expanded_url" : "http:\/\/instagram.com\/raminnasibov",
      "display_url" : "instagram.com\/raminnasibov"
    } ]
  },
  "geo" : { },
  "id_str" : "974999721537400832",
  "text" : "RT @RaminNasibov: Metro in Berlin\nhttps:\/\/t.co\/jZhIUaEga7 https:\/\/t.co\/P27BYbkp3R",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/974986167048392706\/photo\/1",
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/P27BYbkp3R",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYfYuqaX4AIPvYc.jpg",
        "id_str" : "974986140519489538",
        "id" : 974986140519489538,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYfYuqaX4AIPvYc.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 901
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1537
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1537
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/P27BYbkp3R"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 16, 39 ],
        "url" : "https:\/\/t.co\/jZhIUaEga7",
        "expanded_url" : "http:\/\/instagram.com\/raminnasibov",
        "display_url" : "instagram.com\/raminnasibov"
      } ]
    },
    "geo" : { },
    "id_str" : "974986167048392706",
    "text" : "Metro in Berlin\nhttps:\/\/t.co\/jZhIUaEga7 https:\/\/t.co\/P27BYbkp3R",
    "id" : 974986167048392706,
    "created_at" : "2018-03-17 12:29:51 +0000",
    "user" : {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "protected" : false,
      "id_str" : "36889519",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/896399812450557953\/gP3PRbXP_normal.jpg",
      "id" : 36889519,
      "verified" : true
    }
  },
  "id" : 974999721537400832,
  "created_at" : "2018-03-17 13:23:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "indices" : [ 3, 19 ],
      "id_str" : "38400130",
      "id" : 38400130
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974999423121010689",
  "text" : "RT @ChannelNewsAsia: WATCH: Tourists are flung into the air after a chair lift goes out of control at a Georgian ski resort https:\/\/t.co\/B2\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelNewsAsia\/status\/974809358105063424\/video\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/ph1MHbNjFo",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/974808864393592834\/pu\/img\/4QZIkc93mOTtoR0R.jpg",
        "id_str" : "974808864393592834",
        "id" : 974808864393592834,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/974808864393592834\/pu\/img\/4QZIkc93mOTtoR0R.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/ph1MHbNjFo"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 103, 126 ],
        "url" : "https:\/\/t.co\/B2bO8PS2Kd",
        "expanded_url" : "https:\/\/cna.asia\/2GBr0AS",
        "display_url" : "cna.asia\/2GBr0AS"
      } ]
    },
    "geo" : { },
    "id_str" : "974809358105063424",
    "text" : "WATCH: Tourists are flung into the air after a chair lift goes out of control at a Georgian ski resort https:\/\/t.co\/B2bO8PS2Kd (\uD83D\uDCF9: Reuters) https:\/\/t.co\/ph1MHbNjFo",
    "id" : 974809358105063424,
    "created_at" : "2018-03-17 00:47:16 +0000",
    "user" : {
      "name" : "Channel NewsAsia",
      "screen_name" : "ChannelNewsAsia",
      "protected" : false,
      "id_str" : "38400130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876772485215141889\/k58i8c8N_normal.jpg",
      "id" : 38400130,
      "verified" : true
    }
  },
  "id" : 974999423121010689,
  "created_at" : "2018-03-17 13:22:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caroline O.",
      "screen_name" : "RVAwonk",
      "indices" : [ 3, 11 ],
      "id_str" : "2316383071",
      "id" : 2316383071
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BREAKING",
      "indices" : [ 13, 22 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974957478248148992",
  "text" : "RT @RVAwonk: #BREAKING: Facebook has suspended Cambridge Analytica for fraudulently obtaining data from Facebook users... and (it appears)\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BREAKING",
        "indices" : [ 0, 9 ]
      } ],
      "urls" : [ {
        "indices" : [ 177, 200 ],
        "url" : "https:\/\/t.co\/5HjWPY0vUd",
        "expanded_url" : "https:\/\/newsroom.fb.com\/news\/h\/suspending-cambridge-analytica\/",
        "display_url" : "newsroom.fb.com\/news\/h\/suspend\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "974833666982645760",
    "text" : "#BREAKING: Facebook has suspended Cambridge Analytica for fraudulently obtaining data from Facebook users... and (it appears) using it to help the Trump campaign target ads.  \n\nhttps:\/\/t.co\/5HjWPY0vUd",
    "id" : 974833666982645760,
    "created_at" : "2018-03-17 02:23:52 +0000",
    "user" : {
      "name" : "Caroline O.",
      "screen_name" : "RVAwonk",
      "protected" : false,
      "id_str" : "2316383071",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/754103991966003200\/9JwV0DTh_normal.jpg",
      "id" : 2316383071,
      "verified" : false
    }
  },
  "id" : 974957478248148992,
  "created_at" : "2018-03-17 10:35:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "indices" : [ 3, 11 ],
      "id_str" : "574253",
      "id" : 574253
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 13, 36 ],
      "url" : "https:\/\/t.co\/wPovwQxN7W",
      "expanded_url" : "https:\/\/www.apple.com\/families\/",
      "display_url" : "apple.com\/families\/"
    } ]
  },
  "geo" : { },
  "id_str" : "974938783127883777",
  "text" : "RT @mrbrown: https:\/\/t.co\/wPovwQxN7W Parents, you may want to check out the new Apple Families page for all their family features and paren\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/wPovwQxN7W",
        "expanded_url" : "https:\/\/www.apple.com\/families\/",
        "display_url" : "apple.com\/families\/"
      } ]
    },
    "geo" : { },
    "id_str" : "974834678216536065",
    "text" : "https:\/\/t.co\/wPovwQxN7W Parents, you may want to check out the new Apple Families page for all their family features and parental controls in one place. I have been using Apple\u2019s parental controls since my kids started on iPhones. It\u2019s useful stuff.",
    "id" : 974834678216536065,
    "created_at" : "2018-03-17 02:27:53 +0000",
    "user" : {
      "name" : "mrbrown",
      "screen_name" : "mrbrown",
      "protected" : false,
      "id_str" : "574253",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/963343848343678976\/BRaq7rdU_normal.jpg",
      "id" : 574253,
      "verified" : true
    }
  },
  "id" : 974938783127883777,
  "created_at" : "2018-03-17 09:21:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alex Macheras",
      "screen_name" : "AlexInAir",
      "indices" : [ 3, 13 ],
      "id_str" : "191881312",
      "id" : 191881312
    }, {
      "name" : "Qantas",
      "screen_name" : "Qantas",
      "indices" : [ 42, 49 ],
      "id_str" : "218730857",
      "id" : 218730857
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UAE",
      "indices" : [ 74, 78 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974936508670103552",
  "text" : "RT @AlexInAir: Latest: Australian airline @Qantas will completely end all #UAE flights &amp; operations next week \u2014\n\u2022 Qantas to exit Dubai, rep\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Qantas",
        "screen_name" : "Qantas",
        "indices" : [ 27, 34 ],
        "id_str" : "218730857",
        "id" : 218730857
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/AlexInAir\/status\/974602587369504770\/photo\/1",
        "indices" : [ 193, 216 ],
        "url" : "https:\/\/t.co\/aCo7u6H3nd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYZ74S1XUAE-FD6.jpg",
        "id_str" : "974602576430845953",
        "id" : 974602576430845953,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYZ74S1XUAE-FD6.jpg",
        "sizes" : [ {
          "h" : 1162,
          "resize" : "fit",
          "w" : 1124
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1162,
          "resize" : "fit",
          "w" : 1124
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 658
        }, {
          "h" : 1162,
          "resize" : "fit",
          "w" : 1124
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aCo7u6H3nd"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/AlexInAir\/status\/974602587369504770\/photo\/1",
        "indices" : [ 193, 216 ],
        "url" : "https:\/\/t.co\/aCo7u6H3nd",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYZ74SiXcAE7Ko4.jpg",
        "id_str" : "974602576351162369",
        "id" : 974602576351162369,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYZ74SiXcAE7Ko4.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1007,
          "resize" : "fit",
          "w" : 1124
        }, {
          "h" : 609,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1007,
          "resize" : "fit",
          "w" : 1124
        }, {
          "h" : 1007,
          "resize" : "fit",
          "w" : 1124
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aCo7u6H3nd"
      } ],
      "hashtags" : [ {
        "text" : "UAE",
        "indices" : [ 59, 63 ]
      }, {
        "text" : "Singapore",
        "indices" : [ 144, 154 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974602587369504770",
    "text" : "Latest: Australian airline @Qantas will completely end all #UAE flights &amp; operations next week \u2014\n\u2022 Qantas to exit Dubai, replacing UAE with #Singapore - \u2018a more preferred passenger option\u2019 https:\/\/t.co\/aCo7u6H3nd",
    "id" : 974602587369504770,
    "created_at" : "2018-03-16 11:05:38 +0000",
    "user" : {
      "name" : "Alex Macheras",
      "screen_name" : "AlexInAir",
      "protected" : false,
      "id_str" : "191881312",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001823627279589386\/naNxPvhH_normal.jpg",
      "id" : 191881312,
      "verified" : true
    }
  },
  "id" : 974936508670103552,
  "created_at" : "2018-03-17 09:12:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "National Geographic",
      "screen_name" : "NatGeo",
      "indices" : [ 3, 10 ],
      "id_str" : "17471979",
      "id" : 17471979
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974935565572460544",
  "text" : "RT @NatGeo: Our April issue is devoted to exploring race\u2014how it defines, separates and unites us. Read the story behind the cover: https:\/\/\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/www.spredfast.com\/\" rel=\"nofollow\"\u003ESpredfast app\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/NatGeo\/status\/973176442673618944\/photo\/1",
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/5kunxfDrHt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYFq0P4WkAAa6C4.jpg",
        "id_str" : "973176440337371136",
        "id" : 973176440337371136,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYFq0P4WkAAa6C4.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 468
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1408
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 825
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1408
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5kunxfDrHt"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 119, 142 ],
        "url" : "https:\/\/t.co\/PPTVg3UpM8",
        "expanded_url" : "http:\/\/on.natgeo.com\/2Hr9LBy",
        "display_url" : "on.natgeo.com\/2Hr9LBy"
      } ]
    },
    "geo" : { },
    "id_str" : "973176442673618944",
    "text" : "Our April issue is devoted to exploring race\u2014how it defines, separates and unites us. Read the story behind the cover: https:\/\/t.co\/PPTVg3UpM8 https:\/\/t.co\/5kunxfDrHt",
    "id" : 973176442673618944,
    "created_at" : "2018-03-12 12:38:39 +0000",
    "user" : {
      "name" : "National Geographic",
      "screen_name" : "NatGeo",
      "protected" : false,
      "id_str" : "17471979",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/921336759979597825\/VTSJ5mRt_normal.jpg",
      "id" : 17471979,
      "verified" : true
    }
  },
  "id" : 974935565572460544,
  "created_at" : "2018-03-17 09:08:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Iris Classon",
      "screen_name" : "IrisClasson",
      "indices" : [ 3, 15 ],
      "id_str" : "388958428",
      "id" : 388958428
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "PowerShell",
      "indices" : [ 38, 49 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974933252472475648",
  "text" : "RT @IrisClasson: Tip of the Day! Love #PowerShell, but in need for a GUI to run those scripts? PoshGUI is free and awesome :D  https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrisClasson\/status\/974718650656706561\/photo\/1",
        "indices" : [ 134, 157 ],
        "url" : "https:\/\/t.co\/RTnQVqDMo4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYblH-_XcAEErb6.jpg",
        "id_str" : "974718294702911489",
        "id" : 974718294702911489,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYblH-_XcAEErb6.jpg",
        "sizes" : [ {
          "h" : 1020,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1020,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 638,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 361,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RTnQVqDMo4"
      } ],
      "hashtags" : [ {
        "text" : "PowerShell",
        "indices" : [ 21, 32 ]
      } ],
      "urls" : [ {
        "indices" : [ 110, 133 ],
        "url" : "https:\/\/t.co\/c2v64xGyX3",
        "expanded_url" : "https:\/\/poshgui.com\/Editor",
        "display_url" : "poshgui.com\/Editor"
      } ]
    },
    "geo" : { },
    "id_str" : "974718650656706561",
    "text" : "Tip of the Day! Love #PowerShell, but in need for a GUI to run those scripts? PoshGUI is free and awesome :D  https:\/\/t.co\/c2v64xGyX3 https:\/\/t.co\/RTnQVqDMo4",
    "id" : 974718650656706561,
    "created_at" : "2018-03-16 18:46:50 +0000",
    "user" : {
      "name" : "Iris Classon",
      "screen_name" : "IrisClasson",
      "protected" : false,
      "id_str" : "388958428",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1035952658199273472\/8HrTIzFn_normal.jpg",
      "id" : 388958428,
      "verified" : false
    }
  },
  "id" : 974933252472475648,
  "created_at" : "2018-03-17 08:59:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/974930059998957568\/photo\/1",
      "indices" : [ 71, 94 ],
      "url" : "https:\/\/t.co\/H8e80dAmQ4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYeluM_X4AAE3Of.jpg",
      "id_str" : "974930057528598528",
      "id" : 974930057528598528,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYeluM_X4AAE3Of.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/H8e80dAmQ4"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/dOIEotOJEF",
      "expanded_url" : "http:\/\/ift.tt\/2HGLtU4",
      "display_url" : "ift.tt\/2HGLtU4"
    } ]
  },
  "geo" : { },
  "id_str" : "974930059998957568",
  "text" : "First time in Ireland, I came across a Tingkat https:\/\/t.co\/dOIEotOJEF https:\/\/t.co\/H8e80dAmQ4",
  "id" : 974930059998957568,
  "created_at" : "2018-03-17 08:46:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/974927518762455040\/photo\/1",
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/gPTa89jMjy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYejZs0X4AEl0hs.jpg",
      "id_str" : "974927506271887361",
      "id" : 974927506271887361,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYejZs0X4AEl0hs.jpg",
      "sizes" : [ {
        "h" : 450,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 306
      }, {
        "h" : 450,
        "resize" : "fit",
        "w" : 306
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gPTa89jMjy"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974927518762455040",
  "text" : "Some said the future of search is voice. https:\/\/t.co\/gPTa89jMjy",
  "id" : 974927518762455040,
  "created_at" : "2018-03-17 08:36:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "indices" : [ 3, 12 ],
      "id_str" : "130745589",
      "id" : 130745589
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974922085406568448",
  "text" : "RT @drfeifei: A countless number of failures... :) I still fail on a daily basis. But I\u2019ve learned the most important thing is to get up an\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 142, 165 ],
        "url" : "https:\/\/t.co\/1hOHlqVyxx",
        "expanded_url" : "https:\/\/twitter.com\/natanielruizg\/status\/974673471987838976",
        "display_url" : "twitter.com\/natanielruizg\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "974702889376428037",
    "text" : "A countless number of failures... :) I still fail on a daily basis. But I\u2019ve learned the most important thing is to get up and keep trying :) https:\/\/t.co\/1hOHlqVyxx",
    "id" : 974702889376428037,
    "created_at" : "2018-03-16 17:44:12 +0000",
    "user" : {
      "name" : "Fei-Fei Li",
      "screen_name" : "drfeifei",
      "protected" : false,
      "id_str" : "130745589",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/841385099799085056\/R1iX4QGX_normal.jpg",
      "id" : 130745589,
      "verified" : false
    }
  },
  "id" : 974922085406568448,
  "created_at" : "2018-03-17 08:15:12 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/HGTqtB4fws",
      "expanded_url" : "http:\/\/po.st\/J3yr04",
      "display_url" : "po.st\/J3yr04"
    } ]
  },
  "geo" : { },
  "id_str" : "974921773291638784",
  "text" : "RT @edwinksl: How Taipei Metro turned itself around - and the lessons for Singapore's MRT system https:\/\/t.co\/HGTqtB4fws via @ChannelNewsAs\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Channel NewsAsia",
        "screen_name" : "ChannelNewsAsia",
        "indices" : [ 111, 127 ],
        "id_str" : "38400130",
        "id" : 38400130
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 83, 106 ],
        "url" : "https:\/\/t.co\/HGTqtB4fws",
        "expanded_url" : "http:\/\/po.st\/J3yr04",
        "display_url" : "po.st\/J3yr04"
      } ]
    },
    "geo" : { },
    "id_str" : "974840642227572736",
    "text" : "How Taipei Metro turned itself around - and the lessons for Singapore's MRT system https:\/\/t.co\/HGTqtB4fws via @ChannelNewsAsia",
    "id" : 974840642227572736,
    "created_at" : "2018-03-17 02:51:35 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 974921773291638784,
  "created_at" : "2018-03-17 08:13:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 0, 15 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "974917553050005504",
  "geo" : { },
  "id_str" : "974920767350636544",
  "in_reply_to_user_id" : 18319658,
  "text" : "@EmeraldDeLeeuw Same to you too \uD83C\uDF89",
  "id" : 974920767350636544,
  "in_reply_to_status_id" : 974917553050005504,
  "created_at" : "2018-03-17 08:09:58 +0000",
  "in_reply_to_screen_name" : "EmeraldDeLeeuw",
  "in_reply_to_user_id_str" : "18319658",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "HireYap",
      "indices" : [ 137, 145 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974792993357156352",
  "text" : "DOM Element?! \nID or CSS selector?! \nGoogle Tag Manager can be overwhelming for marketer without forthcoming developer support.\nConsider #HireYap",
  "id" : 974792993357156352,
  "created_at" : "2018-03-16 23:42:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/rk92Pf5Lqq",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/business-43287225",
      "display_url" : "bbc.com\/news\/business-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974711954433601536",
  "text" : "The German entrepreneurs celebrating their mistakes https:\/\/t.co\/rk92Pf5Lqq",
  "id" : 974711954433601536,
  "created_at" : "2018-03-16 18:20:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/974706007699017729\/photo\/1",
      "indices" : [ 127, 150 ],
      "url" : "https:\/\/t.co\/69qxOQad2m",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYbZ8JUWsAAQkUi.jpg",
      "id_str" : "974705996688961536",
      "id" : 974705996688961536,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYbZ8JUWsAAQkUi.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 398,
        "resize" : "fit",
        "w" : 1057
      }, {
        "h" : 398,
        "resize" : "fit",
        "w" : 1057
      }, {
        "h" : 256,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 398,
        "resize" : "fit",
        "w" : 1057
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/69qxOQad2m"
    } ],
    "hashtags" : [ {
      "text" : "GoogleAnalytics",
      "indices" : [ 110, 126 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974706007699017729",
  "text" : "How does Organic Search relates to Direct Session??? Thanks I'm having trouble understanding about it. Thanks #GoogleAnalytics https:\/\/t.co\/69qxOQad2m",
  "id" : 974706007699017729,
  "created_at" : "2018-03-16 17:56:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Python",
      "indices" : [ 116, 123 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974639319871840257",
  "text" : "RT @randal_olson: Forensic analysis reveals that Russians were cracking into U.S. government systems and installing #Python 2.7. Probably t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/974637971122085889\/photo\/1",
        "indices" : [ 190, 213 ],
        "url" : "https:\/\/t.co\/YF3bEHFhoN",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYab2olW4AQ5r32.jpg",
        "id_str" : "974637732281638916",
        "id" : 974637732281638916,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYab2olW4AQ5r32.jpg",
        "sizes" : [ {
          "h" : 344,
          "resize" : "fit",
          "w" : 1970
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 119,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 344,
          "resize" : "fit",
          "w" : 1970
        }, {
          "h" : 210,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YF3bEHFhoN"
      } ],
      "hashtags" : [ {
        "text" : "Python",
        "indices" : [ 98, 105 ]
      }, {
        "text" : "netsec",
        "indices" : [ 144, 151 ]
      }, {
        "text" : "programming",
        "indices" : [ 152, 164 ]
      } ],
      "urls" : [ {
        "indices" : [ 166, 189 ],
        "url" : "https:\/\/t.co\/geFjgckpUL",
        "expanded_url" : "https:\/\/www.us-cert.gov\/ncas\/alerts\/TA18-074A",
        "display_url" : "us-cert.gov\/ncas\/alerts\/TA\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "974637971122085889",
    "text" : "Forensic analysis reveals that Russians were cracking into U.S. government systems and installing #Python 2.7. Probably the worst crime of all! #netsec #programming\n\nhttps:\/\/t.co\/geFjgckpUL https:\/\/t.co\/YF3bEHFhoN",
    "id" : 974637971122085889,
    "created_at" : "2018-03-16 13:26:14 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 974639319871840257,
  "created_at" : "2018-03-16 13:31:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/974620170298720256\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/Dn4hbJEeJ0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYaL1-bXcAAMC7o.jpg",
      "id_str" : "974620128779399168",
      "id" : 974620128779399168,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYaL1-bXcAAMC7o.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Dn4hbJEeJ0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974620170298720256",
  "text" : "https:\/\/t.co\/Dn4hbJEeJ0",
  "id" : 974620170298720256,
  "created_at" : "2018-03-16 12:15:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/bKQBXaJJX8",
      "expanded_url" : "https:\/\/medium.com\/self-taught-motivation\/using-marketplaces-to-build-an-mvp-c3b21ee416d5",
      "display_url" : "medium.com\/self-taught-mo\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974593283996487680",
  "text" : "\u201CUsing Marketplaces to Build an MVP.\u201D https:\/\/t.co\/bKQBXaJJX8",
  "id" : 974593283996487680,
  "created_at" : "2018-03-16 10:28:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "indices" : [ 3, 19 ],
      "id_str" : "1721598590",
      "id" : 1721598590
    }, {
      "name" : "John Paul Phelan",
      "screen_name" : "JPPhelan",
      "indices" : [ 33, 42 ],
      "id_str" : "19964133",
      "id" : 19964133
    }, {
      "name" : "MND Singapore",
      "screen_name" : "MNDSingapore",
      "indices" : [ 95, 108 ],
      "id_str" : "72515526",
      "id" : 72515526
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StPatricksDay",
      "indices" : [ 43, 57 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974583055821230080",
  "text" : "RT @IrlEmbSingapore: On day 1 of @JPPhelan #StPatricksDay visit to Singapore, the Minister met @MNDSingapore Senior Minister of State Koh P\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "John Paul Phelan",
        "screen_name" : "JPPhelan",
        "indices" : [ 12, 21 ],
        "id_str" : "19964133",
        "id" : 19964133
      }, {
        "name" : "MND Singapore",
        "screen_name" : "MNDSingapore",
        "indices" : [ 74, 87 ],
        "id_str" : "72515526",
        "id" : 72515526
      }, {
        "name" : "URA Singapore",
        "screen_name" : "URAsg",
        "indices" : [ 188, 194 ],
        "id_str" : "77921229",
        "id" : 77921229
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/974119301010505728\/photo\/1",
        "indices" : [ 229, 252 ],
        "url" : "https:\/\/t.co\/aUOKrOSppX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYTEU1EVMAAPJiP.jpg",
        "id_str" : "974119281540542464",
        "id" : 974119281540542464,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYTEU1EVMAAPJiP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2000,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 2000,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 1172,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 664,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aUOKrOSppX"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/IrlEmbSingapore\/status\/974119301010505728\/photo\/1",
        "indices" : [ 229, 252 ],
        "url" : "https:\/\/t.co\/aUOKrOSppX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYTEU1GU8AAPq2z.jpg",
        "id_str" : "974119281548914688",
        "id" : 974119281548914688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYTEU1GU8AAPq2z.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1514,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 887,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 503,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1514,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aUOKrOSppX"
      } ],
      "hashtags" : [ {
        "text" : "StPatricksDay",
        "indices" : [ 22, 36 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974119301010505728",
    "text" : "On day 1 of @JPPhelan #StPatricksDay visit to Singapore, the Minister met @MNDSingapore Senior Minister of State Koh Poh Koon to discuss urban planning, development &amp; housing. Visited @URAsg to see the plan in action! \uD83C\uDDEE\uD83C\uDDEA\uD83C\uDDF8\uD83C\uDDEC\u2618\uFE0F https:\/\/t.co\/aUOKrOSppX",
    "id" : 974119301010505728,
    "created_at" : "2018-03-15 03:05:14 +0000",
    "user" : {
      "name" : "Irish Emb Singapore",
      "screen_name" : "IrlEmbSingapore",
      "protected" : false,
      "id_str" : "1721598590",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1014019232655335425\/3ZL5mFae_normal.jpg",
      "id" : 1721598590,
      "verified" : true
    }
  },
  "id" : 974583055821230080,
  "created_at" : "2018-03-16 09:48:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974582737603571712",
  "text" : "IS it time now to upgrade to Windows 10?",
  "id" : 974582737603571712,
  "created_at" : "2018-03-16 09:46:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/974554073843118080\/photo\/1",
      "indices" : [ 33, 56 ],
      "url" : "https:\/\/t.co\/IgkoUU2TH1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYZPviVU8AA96Av.png",
      "id_str" : "974554047460995072",
      "id" : 974554047460995072,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYZPviVU8AA96Av.png",
      "sizes" : [ {
        "h" : 794,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 794,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/IgkoUU2TH1"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974554073843118080",
  "text" : "five a day campaign launch in DC https:\/\/t.co\/IgkoUU2TH1",
  "id" : 974554073843118080,
  "created_at" : "2018-03-16 07:52:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Twitter Moments",
      "screen_name" : "TwitterMoments",
      "indices" : [ 3, 18 ],
      "id_str" : "3260518932",
      "id" : 3260518932
    }, {
      "name" : "Candace Jean Andersen",
      "screen_name" : "mycandacejean",
      "indices" : [ 25, 39 ],
      "id_str" : "107932445",
      "id" : 107932445
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974550052239626240",
  "text" : "RT @TwitterMoments: When @mycandacejean stumbled upon a photo from the 1971 International Conference on Biology of Whales, she noticed the\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Candace Jean Andersen",
        "screen_name" : "mycandacejean",
        "indices" : [ 5, 19 ],
        "id_str" : "107932445",
        "id" : 107932445
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 229, 252 ],
        "url" : "https:\/\/t.co\/nc4Bnli0Ta",
        "expanded_url" : "https:\/\/twitter.com\/i\/moments\/974310739891568641",
        "display_url" : "twitter.com\/i\/moments\/9743\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "974320140450807808",
    "text" : "When @mycandacejean stumbled upon a photo from the 1971 International Conference on Biology of Whales, she noticed the only unidentified person was a woman. That sparked a sleuthing mission to find her and do her legacy justice. https:\/\/t.co\/nc4Bnli0Ta",
    "id" : 974320140450807808,
    "created_at" : "2018-03-15 16:23:18 +0000",
    "user" : {
      "name" : "Twitter Moments",
      "screen_name" : "TwitterMoments",
      "protected" : false,
      "id_str" : "3260518932",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/651463624330907648\/OzaAcuSR_normal.png",
      "id" : 3260518932,
      "verified" : true
    }
  },
  "id" : 974550052239626240,
  "created_at" : "2018-03-16 07:36:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/P8mEHimh5q",
      "expanded_url" : "https:\/\/twitter.com\/MeredithJCanode\/status\/974269034949525505",
      "display_url" : "twitter.com\/MeredithJCanod\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974549738744832001",
  "text" : "Some fascinating detective work here. I just Keep swiping....check it out. https:\/\/t.co\/P8mEHimh5q",
  "id" : 974549738744832001,
  "created_at" : "2018-03-16 07:35:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 3, 26 ],
      "url" : "https:\/\/t.co\/wVrUpy4jNo",
      "expanded_url" : "https:\/\/twitter.com\/slappysquirrel\/status\/974438375624232960",
      "display_url" : "twitter.com\/slappysquirrel\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974547193905336321",
  "text" : "?! https:\/\/t.co\/wVrUpy4jNo",
  "id" : 974547193905336321,
  "created_at" : "2018-03-16 07:25:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Alexandra Guimaraes",
      "screen_name" : "AlecGuimaraes",
      "indices" : [ 3, 17 ],
      "id_str" : "78318034",
      "id" : 78318034
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "LearnInbound",
      "indices" : [ 119, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974400771373531137",
  "text" : "RT @AlecGuimaraes: GTM offers you a way to tailor your metrics to your business, type of pages, conversion expected... #LearnInbound",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "LearnInbound",
        "indices" : [ 100, 113 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974364331012960258",
    "text" : "GTM offers you a way to tailor your metrics to your business, type of pages, conversion expected... #LearnInbound",
    "id" : 974364331012960258,
    "created_at" : "2018-03-15 19:18:53 +0000",
    "user" : {
      "name" : "Alexandra Guimaraes",
      "screen_name" : "AlecGuimaraes",
      "protected" : false,
      "id_str" : "78318034",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/807932176444624896\/wZ4LhudR_normal.jpg",
      "id" : 78318034,
      "verified" : false
    }
  },
  "id" : 974400771373531137,
  "created_at" : "2018-03-15 21:43:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 3, 18 ],
      "id_str" : "22997944",
      "id" : 22997944
    }, {
      "name" : "Tom Bennet",
      "screen_name" : "tomcbennet",
      "indices" : [ 40, 51 ],
      "id_str" : "364475936",
      "id" : 364475936
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "GoogleTagManager",
      "indices" : [ 64, 81 ]
    }, {
      "text" : "LearnInbound",
      "indices" : [ 106, 119 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974395250809737216",
  "text" : "RT @paulinesargent: Some good tips from @tomcbennet about using #GoogleTagManager for tracking engagement #LearnInbound https:\/\/t.co\/X9P0Ok\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Tom Bennet",
        "screen_name" : "tomcbennet",
        "indices" : [ 20, 31 ],
        "id_str" : "364475936",
        "id" : 364475936
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/974366807866867713\/photo\/1",
        "indices" : [ 100, 123 ],
        "url" : "https:\/\/t.co\/X9P0OkAJQ8",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYWlcPyW0AA_iOS.jpg",
        "id_str" : "974366799088242688",
        "id" : 974366799088242688,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYWlcPyW0AA_iOS.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/X9P0OkAJQ8"
      } ],
      "hashtags" : [ {
        "text" : "GoogleTagManager",
        "indices" : [ 44, 61 ]
      }, {
        "text" : "LearnInbound",
        "indices" : [ 86, 99 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974366807866867713",
    "text" : "Some good tips from @tomcbennet about using #GoogleTagManager for tracking engagement #LearnInbound https:\/\/t.co\/X9P0OkAJQ8",
    "id" : 974366807866867713,
    "created_at" : "2018-03-15 19:28:44 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "protected" : false,
      "id_str" : "22997944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002264625130409984\/wlpdomJK_normal.jpg",
      "id" : 22997944,
      "verified" : false
    }
  },
  "id" : 974395250809737216,
  "created_at" : "2018-03-15 21:21:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/3dwmzGDYqJ",
      "expanded_url" : "https:\/\/twitter.com\/LearnInbound\/status\/974367102512631808",
      "display_url" : "twitter.com\/LearnInbound\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974395192202727424",
  "text" : "Dear Marketers,  I can be your \"developers\" to work with if you struggle with Google Tag Manager https:\/\/t.co\/3dwmzGDYqJ",
  "id" : 974395192202727424,
  "created_at" : "2018-03-15 21:21:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Rick Viscomi",
      "screen_name" : "rick_viscomi",
      "indices" : [ 3, 16 ],
      "id_str" : "89405591",
      "id" : 89405591
    }, {
      "name" : "iworkwithData",
      "screen_name" : "mryap",
      "indices" : [ 18, 24 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 99, 122 ],
      "url" : "https:\/\/t.co\/atQHz3B7P6",
      "expanded_url" : "https:\/\/beta.httparchive.org\/reports\/state-of-the-web#bytesTotal",
      "display_url" : "beta.httparchive.org\/reports\/state-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974390436600172544",
  "text" : "RT @rick_viscomi: @mryap Averages can be misleading. The latest median data puts desktop at 1.7MB\n\nhttps:\/\/t.co\/atQHz3B7P6 https:\/\/t.co\/nXG\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "iworkwithData",
        "screen_name" : "mryap",
        "indices" : [ 0, 6 ],
        "id_str" : "9465632",
        "id" : 9465632
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/rick_viscomi\/status\/974388272242872322\/photo\/1",
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/nXGLBtl0BX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYW49iMW0AA5o9w.jpg",
        "id_str" : "974388261685743616",
        "id" : 974388261685743616,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYW49iMW0AA5o9w.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/nXGLBtl0BX"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/atQHz3B7P6",
        "expanded_url" : "https:\/\/beta.httparchive.org\/reports\/state-of-the-web#bytesTotal",
        "display_url" : "beta.httparchive.org\/reports\/state-\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "974383086522814467",
    "geo" : { },
    "id_str" : "974388272242872322",
    "in_reply_to_user_id" : 9465632,
    "text" : "@mryap Averages can be misleading. The latest median data puts desktop at 1.7MB\n\nhttps:\/\/t.co\/atQHz3B7P6 https:\/\/t.co\/nXGLBtl0BX",
    "id" : 974388272242872322,
    "in_reply_to_status_id" : 974383086522814467,
    "created_at" : "2018-03-15 20:54:02 +0000",
    "in_reply_to_screen_name" : "mryap",
    "in_reply_to_user_id_str" : "9465632",
    "user" : {
      "name" : "Rick Viscomi",
      "screen_name" : "rick_viscomi",
      "protected" : false,
      "id_str" : "89405591",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/845375467058278400\/DXuIVBHm_normal.jpg",
      "id" : 89405591,
      "verified" : false
    }
  },
  "id" : 974390436600172544,
  "created_at" : "2018-03-15 21:02:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 3, 18 ],
      "id_str" : "22997944",
      "id" : 22997944
    }, {
      "name" : "\uD835\uDE74\uD835\uDE96\uD835\uDE92\uD835\uDE95\uD835\uDEA2 \uD835\uDE76\uD835\uDE9B\uD835\uDE98\uD835\uDE9C\uD835\uDE9C\uD835\uDE96\uD835\uDE8A\uD835\uDE97",
      "screen_name" : "goutaste",
      "indices" : [ 52, 61 ],
      "id_str" : "1027718371",
      "id" : 1027718371
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/OchP9KsFGK",
      "expanded_url" : "http:\/\/images.guide",
      "display_url" : "images.guide"
    } ]
  },
  "geo" : { },
  "id_str" : "974383549112619008",
  "text" : "RT @paulinesargent: Have you got an image strategy? @goutaste recommends reading https:\/\/t.co\/OchP9KsFGK to help speed up you website #lear\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\uD835\uDE74\uD835\uDE96\uD835\uDE92\uD835\uDE95\uD835\uDEA2 \uD835\uDE76\uD835\uDE9B\uD835\uDE98\uD835\uDE9C\uD835\uDE9C\uD835\uDE96\uD835\uDE8A\uD835\uDE97",
        "screen_name" : "goutaste",
        "indices" : [ 32, 41 ],
        "id_str" : "1027718371",
        "id" : 1027718371
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/974373606217134080\/photo\/1",
        "indices" : [ 128, 151 ],
        "url" : "https:\/\/t.co\/YWJZCs7QMo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYWrn6sXkAUNNaP.jpg",
        "id_str" : "974373596654178309",
        "id" : 974373596654178309,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYWrn6sXkAUNNaP.jpg",
        "sizes" : [ {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YWJZCs7QMo"
      } ],
      "hashtags" : [ {
        "text" : "learnInbound",
        "indices" : [ 114, 127 ]
      } ],
      "urls" : [ {
        "indices" : [ 61, 84 ],
        "url" : "https:\/\/t.co\/OchP9KsFGK",
        "expanded_url" : "http:\/\/images.guide",
        "display_url" : "images.guide"
      } ]
    },
    "geo" : { },
    "id_str" : "974373606217134080",
    "text" : "Have you got an image strategy? @goutaste recommends reading https:\/\/t.co\/OchP9KsFGK to help speed up you website #learnInbound https:\/\/t.co\/YWJZCs7QMo",
    "id" : 974373606217134080,
    "created_at" : "2018-03-15 19:55:45 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "protected" : false,
      "id_str" : "22997944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002264625130409984\/wlpdomJK_normal.jpg",
      "id" : 22997944,
      "verified" : false
    }
  },
  "id" : 974383549112619008,
  "created_at" : "2018-03-15 20:35:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 93, 116 ],
      "url" : "https:\/\/t.co\/HA9DPs7ebV",
      "expanded_url" : "https:\/\/whatdoesmysitecost.com\/",
      "display_url" : "whatdoesmysitecost.com"
    } ]
  },
  "geo" : { },
  "id_str" : "974383086522814467",
  "text" : "Find out how much it costs for someone to use your site on mobile networks around the world. https:\/\/t.co\/HA9DPs7ebV \nAccording to the June 15, 2017 run of HTTP Archive, the average site now weighs 2.987 MB",
  "id" : 974383086522814467,
  "created_at" : "2018-03-15 20:33:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Learn Inbound",
      "screen_name" : "LearnInbound",
      "indices" : [ 3, 16 ],
      "id_str" : "2182865797",
      "id" : 2182865797
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974380432505569280",
  "text" : "RT @LearnInbound: If you present the work for developers as problems\u2014not tasks and orders\u2014they'll love you for it... And be able to help yo\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "\uD835\uDE74\uD835\uDE96\uD835\uDE92\uD835\uDE95\uD835\uDEA2 \uD835\uDE76\uD835\uDE9B\uD835\uDE98\uD835\uDE9C\uD835\uDE9C\uD835\uDE96\uD835\uDE8A\uD835\uDE97",
        "screen_name" : "goutaste",
        "indices" : [ 168, 177 ],
        "id_str" : "1027718371",
        "id" : 1027718371
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/LearnInbound\/status\/974372804798926851\/photo\/1",
        "indices" : [ 195, 218 ],
        "url" : "https:\/\/t.co\/C8Ys8Mism1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYWq103XUAA4gsi.jpg",
        "id_str" : "974372736096227328",
        "id" : 974372736096227328,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYWq103XUAA4gsi.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 3024,
          "resize" : "fit",
          "w" : 4032
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/C8Ys8Mism1"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/LearnInbound\/status\/974372804798926851\/photo\/1",
        "indices" : [ 195, 218 ],
        "url" : "https:\/\/t.co\/C8Ys8Mism1",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYWq105XkAIcw9-.jpg",
        "id_str" : "974372736104632322",
        "id" : 974372736104632322,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYWq105XkAIcw9-.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 3024,
          "resize" : "fit",
          "w" : 4032
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/C8Ys8Mism1"
      } ],
      "hashtags" : [ {
        "text" : "LearnInbound",
        "indices" : [ 181, 194 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974372804798926851",
    "text" : "If you present the work for developers as problems\u2014not tasks and orders\u2014they'll love you for it... And be able to help you better \uD83D\uDE4C\n\n\"Developers are problem solvers!\"\n\n@goutaste at #LearnInbound https:\/\/t.co\/C8Ys8Mism1",
    "id" : 974372804798926851,
    "created_at" : "2018-03-15 19:52:34 +0000",
    "user" : {
      "name" : "Learn Inbound",
      "screen_name" : "LearnInbound",
      "protected" : false,
      "id_str" : "2182865797",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/503147789979619328\/DAepAR_c_normal.jpeg",
      "id" : 2182865797,
      "verified" : false
    }
  },
  "id" : 974380432505569280,
  "created_at" : "2018-03-15 20:22:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Microsoft Research",
      "screen_name" : "MSFTResearch",
      "indices" : [ 3, 16 ],
      "id_str" : "21457289",
      "id" : 21457289
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974277451428892672",
  "text" : "RT @MSFTResearch: This is \"a dream that all of us had. We just didn't realize we'd be able to hit it so soon.\" Microsoft reaches human pari\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/prod2.sprinklr.com\" rel=\"nofollow\"\u003ESprinklr Prod2\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Xuedong Huang",
        "screen_name" : "xdh",
        "indices" : [ 196, 200 ],
        "id_str" : "19739933",
        "id" : 19739933
      }, {
        "name" : "Microsoft Translator",
        "screen_name" : "mstranslator",
        "indices" : [ 201, 214 ],
        "id_str" : "77333869",
        "id" : 77333869
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MSFTResearch\/status\/973907477665284098\/photo\/1",
        "indices" : [ 239, 262 ],
        "url" : "https:\/\/t.co\/5CyUBvbQn9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYQDsHWW0AEug-M.jpg",
        "id_str" : "973907475840815105",
        "id" : 973907475840815105,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYQDsHWW0AEug-M.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5CyUBvbQn9"
      } ],
      "hashtags" : [ {
        "text" : "AI",
        "indices" : [ 130, 133 ]
      } ],
      "urls" : [ {
        "indices" : [ 215, 238 ],
        "url" : "https:\/\/t.co\/65mRsT0zVK",
        "expanded_url" : "http:\/\/msft.social\/W2eoUD",
        "display_url" : "msft.social\/W2eoUD"
      } ]
    },
    "geo" : { },
    "id_str" : "973907477665284098",
    "text" : "This is \"a dream that all of us had. We just didn't realize we'd be able to hit it so soon.\" Microsoft reaches human parity using #AI to translate test set of news stories from Chinese to English @xdh @mstranslator https:\/\/t.co\/65mRsT0zVK https:\/\/t.co\/5CyUBvbQn9",
    "id" : 973907477665284098,
    "created_at" : "2018-03-14 13:03:31 +0000",
    "user" : {
      "name" : "Microsoft Research",
      "screen_name" : "MSFTResearch",
      "protected" : false,
      "id_str" : "21457289",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/861981051479924736\/gZaIkPyp_normal.jpg",
      "id" : 21457289,
      "verified" : false
    }
  },
  "id" : 974277451428892672,
  "created_at" : "2018-03-15 13:33:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Abhishek Singh",
      "screen_name" : "shekitup",
      "indices" : [ 3, 12 ],
      "id_str" : "3379659130",
      "id" : 3379659130
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "90skid",
      "indices" : [ 19, 26 ]
    }, {
      "text" : "StreetFighter",
      "indices" : [ 43, 57 ]
    }, {
      "text" : "arcade",
      "indices" : [ 94, 101 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974242433570570240",
  "text" : "RT @shekitup: As a #90skid I loved playing #StreetFighter II with my sister, so I rebuilt the #arcade classic as a multiplayer AR game to a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/shekitup\/status\/973945992226770944\/video\/1",
        "indices" : [ 181, 204 ],
        "url" : "https:\/\/t.co\/5jrwxEYq35",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/973931812169928705\/pu\/img\/nZP__3KiTibZMV3v.jpg",
        "id_str" : "973931812169928705",
        "id" : 973931812169928705,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/973931812169928705\/pu\/img\/nZP__3KiTibZMV3v.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/5jrwxEYq35"
      } ],
      "hashtags" : [ {
        "text" : "90skid",
        "indices" : [ 5, 12 ]
      }, {
        "text" : "StreetFighter",
        "indices" : [ 29, 43 ]
      }, {
        "text" : "arcade",
        "indices" : [ 80, 87 ]
      }, {
        "text" : "ARKit",
        "indices" : [ 159, 165 ]
      }, {
        "text" : "madewithunity",
        "indices" : [ 166, 180 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973945992226770944",
    "text" : "As a #90skid I loved playing #StreetFighter II with my sister, so I rebuilt the #arcade classic as a multiplayer AR game to actually take it into the streets\n\n#ARKit #madewithunity https:\/\/t.co\/5jrwxEYq35",
    "id" : 973945992226770944,
    "created_at" : "2018-03-14 15:36:34 +0000",
    "user" : {
      "name" : "Abhishek Singh",
      "screen_name" : "shekitup",
      "protected" : false,
      "id_str" : "3379659130",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/643980156890152960\/owtRczKH_normal.jpg",
      "id" : 3379659130,
      "verified" : false
    }
  },
  "id" : 974242433570570240,
  "created_at" : "2018-03-15 11:14:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MeasureCamp Manchester",
      "screen_name" : "MeasureCampMCR",
      "indices" : [ 3, 18 ],
      "id_str" : "960533915780952065",
      "id" : 960533915780952065
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "MeasureCampMCR",
      "indices" : [ 109, 124 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974241636468305920",
  "text" : "RT @MeasureCampMCR: So 3 tickets have been released from the queue and are available. Wont be there for long #MeasureCampMCR https:\/\/t.co\/B\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MeasureCampMCR\/status\/974233736622628864\/photo\/1",
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/BIGbpv5Raz",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYUsW9zX0AASwds.jpg",
        "id_str" : "974233667454357504",
        "id" : 974233667454357504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYUsW9zX0AASwds.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 200
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 200
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 200
        }, {
          "h" : 200,
          "resize" : "fit",
          "w" : 200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BIGbpv5Raz"
      } ],
      "hashtags" : [ {
        "text" : "MeasureCampMCR",
        "indices" : [ 89, 104 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974233736622628864",
    "text" : "So 3 tickets have been released from the queue and are available. Wont be there for long #MeasureCampMCR https:\/\/t.co\/BIGbpv5Raz",
    "id" : 974233736622628864,
    "created_at" : "2018-03-15 10:39:57 +0000",
    "user" : {
      "name" : "MeasureCamp Manchester",
      "screen_name" : "MeasureCampMCR",
      "protected" : false,
      "id_str" : "960533915780952065",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1026440273059082240\/j7scKDXn_normal.jpg",
      "id" : 960533915780952065,
      "verified" : false
    }
  },
  "id" : 974241636468305920,
  "created_at" : "2018-03-15 11:11:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974226334615564288",
  "text" : "RT @getoptimise: Someone nice enough to ask whether my PC can be used for mining  cryptocurrency when I visit the site. I don't think so. h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/974226076758102016\/photo\/1",
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/BlAxtdQ05b",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYUlZb2X4AAgeeS.jpg",
        "id_str" : "974226013298352128",
        "id" : 974226013298352128,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYUlZb2X4AAgeeS.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 470,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 648,
          "resize" : "fit",
          "w" : 938
        }, {
          "h" : 648,
          "resize" : "fit",
          "w" : 938
        }, {
          "h" : 648,
          "resize" : "fit",
          "w" : 938
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/BlAxtdQ05b"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "974226076758102016",
    "text" : "Someone nice enough to ask whether my PC can be used for mining  cryptocurrency when I visit the site. I don't think so. https:\/\/t.co\/BlAxtdQ05b",
    "id" : 974226076758102016,
    "created_at" : "2018-03-15 10:09:31 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 974226334615564288,
  "created_at" : "2018-03-15 10:10:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CSSconf EU",
      "screen_name" : "CSSconfeu",
      "indices" : [ 3, 13 ],
      "id_str" : "1433730475",
      "id" : 1433730475
    }, {
      "name" : "HJ Chen @ Refresh \uD83C\uDDEA\uD83C\uDDEA",
      "screen_name" : "hj_chen",
      "indices" : [ 30, 38 ],
      "id_str" : "30177862",
      "id" : 30177862
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974219052255244288",
  "text" : "RT @CSSconfeu: Chen Hui Jing (@hj_chen) will start a conversation about typography and typesetting! Because \u201CThe web is not just left-to-ri\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "HJ Chen @ Refresh \uD83C\uDDEA\uD83C\uDDEA",
        "screen_name" : "hj_chen",
        "indices" : [ 15, 23 ],
        "id_str" : "30177862",
        "id" : 30177862
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 130, 153 ],
        "url" : "https:\/\/t.co\/RQHJFBi4mv",
        "expanded_url" : "https:\/\/2018.cssconf.eu\/speakers\/chen-hui-jing\/",
        "display_url" : "2018.cssconf.eu\/speakers\/chen-\u2026"
      } ]
    },
    "in_reply_to_status_id_str" : "974207144403890177",
    "geo" : { },
    "id_str" : "974207847302037505",
    "in_reply_to_user_id" : 1433730475,
    "text" : "Chen Hui Jing (@hj_chen) will start a conversation about typography and typesetting! Because \u201CThe web is not just left-to-right\u201D. https:\/\/t.co\/RQHJFBi4mv",
    "id" : 974207847302037505,
    "in_reply_to_status_id" : 974207144403890177,
    "created_at" : "2018-03-15 08:57:05 +0000",
    "in_reply_to_screen_name" : "CSSconfeu",
    "in_reply_to_user_id_str" : "1433730475",
    "user" : {
      "name" : "CSSconf EU",
      "screen_name" : "CSSconfeu",
      "protected" : false,
      "id_str" : "1433730475",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/927643057532669958\/Zh87sUjr_normal.jpg",
      "id" : 1433730475,
      "verified" : false
    }
  },
  "id" : 974219052255244288,
  "created_at" : "2018-03-15 09:41:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Zainab Boladale",
      "screen_name" : "ZainabBoladale",
      "indices" : [ 3, 18 ],
      "id_str" : "4870733470",
      "id" : 4870733470
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rentcrisis",
      "indices" : [ 24, 35 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974001195214716928",
  "text" : "RT @ZainabBoladale: The #rentcrisis is bad. I've been to a number of viewings with dozens of people applying for the same overpriced &amp; ofte\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "rentcrisis",
        "indices" : [ 4, 15 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971653021666029568",
    "text" : "The #rentcrisis is bad. I've been to a number of viewings with dozens of people applying for the same overpriced &amp; often very tiny room. I've been looking for 2 months &amp; it's exhausting. I just want a place that's comfortable. Do you know anyone renting a room? Please DM.",
    "id" : 971653021666029568,
    "created_at" : "2018-03-08 07:45:07 +0000",
    "user" : {
      "name" : "Zainab Boladale",
      "screen_name" : "ZainabBoladale",
      "protected" : false,
      "id_str" : "4870733470",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1017362871943090177\/uwuvVRe8_normal.jpg",
      "id" : 4870733470,
      "verified" : false
    }
  },
  "id" : 974001195214716928,
  "created_at" : "2018-03-14 19:15:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 77, 100 ],
      "url" : "https:\/\/t.co\/zNmfD0faYy",
      "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/971688572284801025",
      "display_url" : "twitter.com\/getoptimise\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "974001142177959936",
  "text" : "RT @getoptimise: For a start. I just need to find 10 people to sign this up. https:\/\/t.co\/zNmfD0faYy",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/zNmfD0faYy",
        "expanded_url" : "https:\/\/twitter.com\/getoptimise\/status\/971688572284801025",
        "display_url" : "twitter.com\/getoptimise\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "971870236361347073",
    "text" : "For a start. I just need to find 10 people to sign this up. https:\/\/t.co\/zNmfD0faYy",
    "id" : 971870236361347073,
    "created_at" : "2018-03-08 22:08:15 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 974001142177959936,
  "created_at" : "2018-03-14 19:15:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "974001105960144899",
  "text" : "RT @getoptimise: Are you struggling to get a grip on using data analytics to keep track and analyse your marketing and product activities?\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "HireYap",
        "indices" : [ 122, 130 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973267912495595520",
    "text" : "Are you struggling to get a grip on using data analytics to keep track and analyse your marketing and product activities? #HireYap",
    "id" : 973267912495595520,
    "created_at" : "2018-03-12 18:42:07 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 974001105960144899,
  "created_at" : "2018-03-14 19:15:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "UpStarterHQ",
      "indices" : [ 16, 28 ]
    }, {
      "text" : "UpStarter",
      "indices" : [ 29, 39 ]
    }, {
      "text" : "HireYap",
      "indices" : [ 77, 85 ]
    } ],
    "urls" : [ {
      "indices" : [ 53, 76 ],
      "url" : "https:\/\/t.co\/QlDpyIY4JJ",
      "expanded_url" : "https:\/\/www.linkedin.com\/in\/ssyap\/",
      "display_url" : "linkedin.com\/in\/ssyap\/"
    } ]
  },
  "geo" : { },
  "id_str" : "973990576038309890",
  "text" : "Could not be at #UpStarterHQ #UpStarter this evening https:\/\/t.co\/QlDpyIY4JJ #HireYap",
  "id" : 973990576038309890,
  "created_at" : "2018-03-14 18:33:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    }, {
      "name" : "TechCrunch",
      "screen_name" : "TechCrunch",
      "indices" : [ 117, 128 ],
      "id_str" : "816653",
      "id" : 816653
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/Z3qlui5Zq8",
      "expanded_url" : "http:\/\/tcrn.ch\/2pef4fY",
      "display_url" : "tcrn.ch\/2pef4fY"
    } ]
  },
  "geo" : { },
  "id_str" : "973957174828453888",
  "text" : "RT @edwinksl: Microsoft announces breakthrough in Chinese-to-English machine translation https:\/\/t.co\/Z3qlui5Zq8 via @techcrunch",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TechCrunch",
        "screen_name" : "TechCrunch",
        "indices" : [ 103, 114 ],
        "id_str" : "816653",
        "id" : 816653
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 75, 98 ],
        "url" : "https:\/\/t.co\/Z3qlui5Zq8",
        "expanded_url" : "http:\/\/tcrn.ch\/2pef4fY",
        "display_url" : "tcrn.ch\/2pef4fY"
      } ]
    },
    "geo" : { },
    "id_str" : "973956755247063040",
    "text" : "Microsoft announces breakthrough in Chinese-to-English machine translation https:\/\/t.co\/Z3qlui5Zq8 via @techcrunch",
    "id" : 973956755247063040,
    "created_at" : "2018-03-14 16:19:20 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 973957174828453888,
  "created_at" : "2018-03-14 16:21:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 160, 183 ],
      "url" : "https:\/\/t.co\/AsknipPh4S",
      "expanded_url" : "https:\/\/porter.io\/",
      "display_url" : "porter.io"
    } ]
  },
  "geo" : { },
  "id_str" : "973885704051838976",
  "text" : "This apps delivered relevant content to your Inbox based on software projects you track, find interesting and discover on GitHub (i.e. starred repos on Github) https:\/\/t.co\/AsknipPh4S",
  "id" : 973885704051838976,
  "created_at" : "2018-03-14 11:37:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/Oog4auL6fd",
      "expanded_url" : "https:\/\/mobile.twitter.com\/notetoshelf",
      "display_url" : "mobile.twitter.com\/notetoshelf"
    } ]
  },
  "geo" : { },
  "id_str" : "973876033584881664",
  "text" : "For more techie stuff, do checkout my another Twitter account https:\/\/t.co\/Oog4auL6fd",
  "id" : 973876033584881664,
  "created_at" : "2018-03-14 10:58:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "OpenGraph",
      "indices" : [ 131, 141 ]
    } ],
    "urls" : [ {
      "indices" : [ 5, 28 ],
      "url" : "https:\/\/t.co\/tJe6p9tpaX",
      "expanded_url" : "https:\/\/cards-dev.twitter.com\/validator",
      "display_url" : "cards-dev.twitter.com\/validator"
    } ]
  },
  "geo" : { },
  "id_str" : "973871916334354432",
  "text" : "This https:\/\/t.co\/tJe6p9tpaX does not really give you the full preview. You need to tweet it to verify how your Twittercard looks. #OpenGraph",
  "id" : 973871916334354432,
  "created_at" : "2018-03-14 10:42:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973861444327559169",
  "text" : "It could be a wild goose chase if you do not google with the right combination of keywords...",
  "id" : 973861444327559169,
  "created_at" : "2018-03-14 10:00:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cambridge University",
      "screen_name" : "Cambridge_Uni",
      "indices" : [ 3, 17 ],
      "id_str" : "33474655",
      "id" : 33474655
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 106, 129 ],
      "url" : "https:\/\/t.co\/h8uWznhEpb",
      "expanded_url" : "http:\/\/www.cam.ac.uk\/stephenhawking",
      "display_url" : "cam.ac.uk\/stephenhawking"
    } ]
  },
  "geo" : { },
  "id_str" : "973838251487383552",
  "text" : "RT @Cambridge_Uni: \"Look up at the stars and not down at your feet\" - Professor Stephen Hawking\n1942-2018 https:\/\/t.co\/h8uWznhEpb https:\/\/t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Cambridge_Uni\/status\/973798360632168448\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/RVeQx2BTxP",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYOgNSZW4AEDPz5.jpg",
        "id_str" : "973798094579097601",
        "id" : 973798094579097601,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYOgNSZW4AEDPz5.jpg",
        "sizes" : [ {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 512,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 340,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/RVeQx2BTxP"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 87, 110 ],
        "url" : "https:\/\/t.co\/h8uWznhEpb",
        "expanded_url" : "http:\/\/www.cam.ac.uk\/stephenhawking",
        "display_url" : "cam.ac.uk\/stephenhawking"
      } ]
    },
    "geo" : { },
    "id_str" : "973798360632168448",
    "text" : "\"Look up at the stars and not down at your feet\" - Professor Stephen Hawking\n1942-2018 https:\/\/t.co\/h8uWznhEpb https:\/\/t.co\/RVeQx2BTxP",
    "id" : 973798360632168448,
    "created_at" : "2018-03-14 05:49:56 +0000",
    "user" : {
      "name" : "Cambridge University",
      "screen_name" : "Cambridge_Uni",
      "protected" : false,
      "id_str" : "33474655",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875634893534904323\/Alz7fmFM_normal.jpg",
      "id" : 33474655,
      "verified" : true
    }
  },
  "id" : 973838251487383552,
  "created_at" : "2018-03-14 08:28:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "indices" : [ 3, 12 ],
      "id_str" : "17746927",
      "id" : 17746927
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/P81HRY0RhF",
      "expanded_url" : "https:\/\/www.reuters.com\/article\/us-crypto-currencies-google\/google-to-ban-ads-on-cryptocurrencies-related-products-idUSKCN1GQ0GD",
      "display_url" : "reuters.com\/article\/us-cry\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "973837696287289344",
  "text" : "RT @edwinksl: Google to ban ads on cryptocurrencies, related products https:\/\/t.co\/P81HRY0RhF",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/P81HRY0RhF",
        "expanded_url" : "https:\/\/www.reuters.com\/article\/us-crypto-currencies-google\/google-to-ban-ads-on-cryptocurrencies-related-products-idUSKCN1GQ0GD",
        "display_url" : "reuters.com\/article\/us-cry\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "973792599655043072",
    "text" : "Google to ban ads on cryptocurrencies, related products https:\/\/t.co\/P81HRY0RhF",
    "id" : 973792599655043072,
    "created_at" : "2018-03-14 05:27:02 +0000",
    "user" : {
      "name" : "Edwin Khoo",
      "screen_name" : "edwinksl",
      "protected" : false,
      "id_str" : "17746927",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/506366266035019776\/2eMmLsdK_normal.jpeg",
      "id" : 17746927,
      "verified" : false
    }
  },
  "id" : 973837696287289344,
  "created_at" : "2018-03-14 08:26:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Manchester Uni News",
      "screen_name" : "UoMNews",
      "indices" : [ 3, 11 ],
      "id_str" : "179522150",
      "id" : 179522150
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973635200545587200",
  "text" : "RT @UoMNews: Very sad to hear the news that Prof Sir John Sulston has died.  The Nobel Prize winner's pioneering work on the human genome l\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 229, 252 ],
        "url" : "https:\/\/t.co\/gn8bFOv8fc",
        "expanded_url" : "http:\/\/www.bbc.co.uk\/news\/science-environment-43349774",
        "display_url" : "bbc.co.uk\/news\/science-e\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "972136542616645632",
    "text" : "Very sad to hear the news that Prof Sir John Sulston has died.  The Nobel Prize winner's pioneering work on the human genome leaves an incredible legacy and he was joint lead of our Institute for Science, Ethics &amp; Innovation https:\/\/t.co\/gn8bFOv8fc",
    "id" : 972136542616645632,
    "created_at" : "2018-03-09 15:46:27 +0000",
    "user" : {
      "name" : "Manchester Uni News",
      "screen_name" : "UoMNews",
      "protected" : false,
      "id_str" : "179522150",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/869570892598267904\/GJwyZ4EC_normal.jpg",
      "id" : 179522150,
      "verified" : true
    }
  },
  "id" : 973635200545587200,
  "created_at" : "2018-03-13 19:01:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Felton",
      "screen_name" : "JimMFelton",
      "indices" : [ 3, 14 ],
      "id_str" : "2904913023",
      "id" : 2904913023
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/JimMFelton\/status\/973552345668898816\/photo\/1",
      "indices" : [ 56, 79 ],
      "url" : "https:\/\/t.co\/0CouhAMXMJ",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYLApYNX0AEcm_H.jpg",
      "id_str" : "973552286571155457",
      "id" : 973552286571155457,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYLApYNX0AEcm_H.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 381,
        "resize" : "fit",
        "w" : 666
      }, {
        "h" : 381,
        "resize" : "fit",
        "w" : 666
      }, {
        "h" : 381,
        "resize" : "fit",
        "w" : 666
      }, {
        "h" : 381,
        "resize" : "fit",
        "w" : 666
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/0CouhAMXMJ"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973627017555038209",
  "text" : "RT @JimMFelton: WOULD YOU LIKE TO BE SECRETARY OF STATE https:\/\/t.co\/0CouhAMXMJ",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JimMFelton\/status\/973552345668898816\/photo\/1",
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/0CouhAMXMJ",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYLApYNX0AEcm_H.jpg",
        "id_str" : "973552286571155457",
        "id" : 973552286571155457,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYLApYNX0AEcm_H.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 666
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 666
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 666
        }, {
          "h" : 381,
          "resize" : "fit",
          "w" : 666
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/0CouhAMXMJ"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973552345668898816",
    "text" : "WOULD YOU LIKE TO BE SECRETARY OF STATE https:\/\/t.co\/0CouhAMXMJ",
    "id" : 973552345668898816,
    "created_at" : "2018-03-13 13:32:21 +0000",
    "user" : {
      "name" : "James Felton",
      "screen_name" : "JimMFelton",
      "protected" : false,
      "id_str" : "2904913023",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/863860130009546754\/-2Zr0kqI_normal.jpg",
      "id" : 2904913023,
      "verified" : true
    }
  },
  "id" : 973627017555038209,
  "created_at" : "2018-03-13 18:29:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Caitriona O'Kennedy",
      "screen_name" : "caitrionaok_IDA",
      "indices" : [ 3, 19 ],
      "id_str" : "15630427",
      "id" : 15630427
    }, {
      "name" : "fDi Magazine",
      "screen_name" : "fDiMagazine",
      "indices" : [ 61, 73 ],
      "id_str" : "132168970",
      "id" : 132168970
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dublin",
      "indices" : [ 34, 41 ]
    }, {
      "text" : "InvestInIreland",
      "indices" : [ 106, 122 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973615247796899840",
  "text" : "RT @caitrionaok_IDA: Great to see #Dublin ranking so well in @fDiMagazine FDI Reinvestment Ranking 2018.  #InvestInIreland https:\/\/t.co\/zqZ\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "fDi Magazine",
        "screen_name" : "fDiMagazine",
        "indices" : [ 40, 52 ],
        "id_str" : "132168970",
        "id" : 132168970
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/caitrionaok_IDA\/status\/973246986664235013\/photo\/1",
        "indices" : [ 102, 125 ],
        "url" : "https:\/\/t.co\/zqZwbyMMJA",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYGqORNWsAUIG_u.jpg",
        "id_str" : "973246156603043845",
        "id" : 973246156603043845,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYGqORNWsAUIG_u.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 568,
          "resize" : "fit",
          "w" : 1512
        }, {
          "h" : 255,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 451,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 568,
          "resize" : "fit",
          "w" : 1512
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/zqZwbyMMJA"
      } ],
      "hashtags" : [ {
        "text" : "Dublin",
        "indices" : [ 13, 20 ]
      }, {
        "text" : "InvestInIreland",
        "indices" : [ 85, 101 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973246986664235013",
    "text" : "Great to see #Dublin ranking so well in @fDiMagazine FDI Reinvestment Ranking 2018.  #InvestInIreland https:\/\/t.co\/zqZwbyMMJA",
    "id" : 973246986664235013,
    "created_at" : "2018-03-12 17:18:58 +0000",
    "user" : {
      "name" : "Caitriona O'Kennedy",
      "screen_name" : "caitrionaok_IDA",
      "protected" : false,
      "id_str" : "15630427",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/843756423012143105\/FVwGHwLr_normal.jpg",
      "id" : 15630427,
      "verified" : false
    }
  },
  "id" : 973615247796899840,
  "created_at" : "2018-03-13 17:42:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/0b04tEw2Ri",
      "expanded_url" : "https:\/\/twitter.com\/dr_pete\/status\/973569320704987137",
      "display_url" : "twitter.com\/dr_pete\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "973586694288031744",
  "text" : "A High Stake Game https:\/\/t.co\/0b04tEw2Ri",
  "id" : 973586694288031744,
  "created_at" : "2018-03-13 15:48:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 171, 194 ],
      "url" : "https:\/\/t.co\/UEr2TcqDwx",
      "expanded_url" : "https:\/\/getoptimise.netlify.com\/post\/writings-on-the-button-the-grammar-of-interactivity\/",
      "display_url" : "getoptimise.netlify.com\/post\/writings-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "973530588694175745",
  "text" : "If you are on WordPress, a plugin can resolve Open Graph Tag not showing on Twitter. Since moving to Static Site, I need to fix this by writing code to do the same thing. https:\/\/t.co\/UEr2TcqDwx",
  "id" : 973530588694175745,
  "created_at" : "2018-03-13 12:05:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 125, 148 ],
      "url" : "https:\/\/t.co\/g0lAzaZ7wd",
      "expanded_url" : "http:\/\/getoptimise.netlify.com\/post\/social-graft\/",
      "display_url" : "getoptimise.netlify.com\/post\/social-gr\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "973500815167156224",
  "text" : "You\u2019ve created a compelling website and brand, don\u2019t squander it away when you try to share your content on social network.  https:\/\/t.co\/g0lAzaZ7wd",
  "id" : 973500815167156224,
  "created_at" : "2018-03-13 10:07:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "VictoriaG (MPhil (failed))",
      "screen_name" : "ancientnmodern",
      "indices" : [ 3, 18 ],
      "id_str" : "79141911",
      "id" : 79141911
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973498151326879744",
  "text" : "RT @ancientnmodern: Medieval picture of the day. Mary Beard and Lucy Worsley fight to the death for supremacy of BBC4 https:\/\/t.co\/QgvPXaa6\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ancientnmodern\/status\/973320811728850946\/photo\/1",
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/QgvPXaa6Ry",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYHuHYOX0AAvxdV.jpg",
        "id_str" : "973320805017964544",
        "id" : 973320805017964544,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYHuHYOX0AAvxdV.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 268,
          "resize" : "fit",
          "w" : 236
        }, {
          "h" : 268,
          "resize" : "fit",
          "w" : 236
        }, {
          "h" : 268,
          "resize" : "fit",
          "w" : 236
        }, {
          "h" : 268,
          "resize" : "fit",
          "w" : 236
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/QgvPXaa6Ry"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973320811728850946",
    "text" : "Medieval picture of the day. Mary Beard and Lucy Worsley fight to the death for supremacy of BBC4 https:\/\/t.co\/QgvPXaa6Ry",
    "id" : 973320811728850946,
    "created_at" : "2018-03-12 22:12:19 +0000",
    "user" : {
      "name" : "VictoriaG (MPhil (failed))",
      "screen_name" : "ancientnmodern",
      "protected" : false,
      "id_str" : "79141911",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1010181007725744128\/R6TojsEq_normal.jpg",
      "id" : 79141911,
      "verified" : false
    }
  },
  "id" : 973498151326879744,
  "created_at" : "2018-03-13 09:57:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 0, 12 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/p3dIQRzFHy",
      "expanded_url" : "https:\/\/twitter.com\/UpStarterHQ\/status\/973494607186186240",
      "display_url" : "twitter.com\/UpStarterHQ\/st\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "973491107291156480",
  "geo" : { },
  "id_str" : "973497961001996288",
  "in_reply_to_user_id" : 19793936,
  "text" : "@gianniponzi There is an career event tomorrow in Dublin. https:\/\/t.co\/p3dIQRzFHy",
  "id" : 973497961001996288,
  "in_reply_to_status_id" : 973491107291156480,
  "created_at" : "2018-03-13 09:56:15 +0000",
  "in_reply_to_screen_name" : "gianniponzi",
  "in_reply_to_user_id_str" : "19793936",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973495370415255554",
  "text" : "I lost 2 domain name for good. Unless I forked out a minimum of USD 199. \"If you do not renew the domain within the renew grace period, it enters a \"redemption period\" at the Registry and placed into the auction. Usually, domains are auctioned off successfully.\"",
  "id" : 973495370415255554,
  "created_at" : "2018-03-13 09:45:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973494588928331776",
  "text" : "IS there a way to access docker container from another PC on a local area network?  Thanks.",
  "id" : 973494588928331776,
  "created_at" : "2018-03-13 09:42:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "hello!",
      "screen_name" : "mryap",
      "indices" : [ 3, 9 ],
      "id_str" : "9465632",
      "id" : 9465632
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973480152213741569",
  "text" : "RT @mryap: A high conversion rate for signing up does not equal as many users who are actually using your services (after signing up) #geto\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "getoptimise",
        "indices" : [ 123, 135 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "856867017286176768",
    "text" : "A high conversion rate for signing up does not equal as many users who are actually using your services (after signing up) #getoptimise",
    "id" : 856867017286176768,
    "created_at" : "2017-04-25 13:46:31 +0000",
    "user" : {
      "name" : "hello!",
      "screen_name" : "mryap",
      "protected" : false,
      "id_str" : "9465632",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
      "id" : 9465632,
      "verified" : false
    }
  },
  "id" : 973480152213741569,
  "created_at" : "2018-03-13 08:45:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Real Insights",
      "screen_name" : "Real_Insights",
      "indices" : [ 3, 17 ],
      "id_str" : "113643914",
      "id" : 113643914
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "retail",
      "indices" : [ 109, 116 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 70 ],
      "url" : "https:\/\/t.co\/YHOpTNK8os",
      "expanded_url" : "http:\/\/eepurl.com\/dmRc_v",
      "display_url" : "eepurl.com\/dmRc_v"
    } ]
  },
  "geo" : { },
  "id_str" : "973479544513581061",
  "text" : "RT @Real_Insights: Death of the Irish Shop???: https:\/\/t.co\/YHOpTNK8os What a great blog from the formidable #retail guru Linda Ward of @Re\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Linda Ward",
        "screen_name" : "RetailRenewal",
        "indices" : [ 117, 131 ],
        "id_str" : "757581724373090304",
        "id" : 757581724373090304
      }, {
        "name" : "The English Market",
        "screen_name" : "EnglishMarket",
        "indices" : [ 151, 165 ],
        "id_str" : "117529666",
        "id" : 117529666
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "retail",
        "indices" : [ 90, 97 ]
      }, {
        "text" : "retail",
        "indices" : [ 182, 189 ]
      }, {
        "text" : "disruptors",
        "indices" : [ 190, 201 ]
      }, {
        "text" : "shops",
        "indices" : [ 202, 208 ]
      }, {
        "text" : "trends",
        "indices" : [ 209, 216 ]
      } ],
      "urls" : [ {
        "indices" : [ 28, 51 ],
        "url" : "https:\/\/t.co\/YHOpTNK8os",
        "expanded_url" : "http:\/\/eepurl.com\/dmRc_v",
        "display_url" : "eepurl.com\/dmRc_v"
      } ]
    },
    "geo" : { },
    "id_str" : "971768298001006594",
    "text" : "Death of the Irish Shop???: https:\/\/t.co\/YHOpTNK8os What a great blog from the formidable #retail guru Linda Ward of @RetailRenewal &amp; our very own @EnglishMarket gets a mention! #retail #disruptors #shops #trends",
    "id" : 971768298001006594,
    "created_at" : "2018-03-08 15:23:11 +0000",
    "user" : {
      "name" : "Real Insights",
      "screen_name" : "Real_Insights",
      "protected" : false,
      "id_str" : "113643914",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/994208624611332096\/Oggtc24A_normal.jpg",
      "id" : 113643914,
      "verified" : false
    }
  },
  "id" : 973479544513581061,
  "created_at" : "2018-03-13 08:43:04 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Python",
      "indices" : [ 34, 41 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973290090020696065",
  "text" : "RT @randal_olson: The downside of #Python package names like Falcon, Celery, Pandas, NumPy, etc. is that we sound ridiculous to outsiders w\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/973283849609166848\/photo\/1",
        "indices" : [ 176, 199 ],
        "url" : "https:\/\/t.co\/kTg0MU0uez",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYHMV8XVMAAqbC9.jpg",
        "id_str" : "973283671842041856",
        "id" : 973283671842041856,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYHMV8XVMAAqbC9.jpg",
        "sizes" : [ {
          "h" : 248,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 530,
          "resize" : "fit",
          "w" : 1454
        }, {
          "h" : 530,
          "resize" : "fit",
          "w" : 1454
        }, {
          "h" : 437,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/kTg0MU0uez"
      } ],
      "hashtags" : [ {
        "text" : "Python",
        "indices" : [ 16, 23 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973283849609166848",
    "text" : "The downside of #Python package names like Falcon, Celery, Pandas, NumPy, etc. is that we sound ridiculous to outsiders when describing the packages involved in our workflows. https:\/\/t.co\/kTg0MU0uez",
    "id" : 973283849609166848,
    "created_at" : "2018-03-12 19:45:27 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 973290090020696065,
  "created_at" : "2018-03-12 20:10:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Stig Abell",
      "screen_name" : "StigAbell",
      "indices" : [ 3, 13 ],
      "id_str" : "509212441",
      "id" : 509212441
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973119993368993792",
  "text" : "RT @StigAbell: \"I just read a book about Stockholm Syndrome, it started off badly but by the end I really liked it.\"\nRIP Ken Dodd",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "973098423930015744",
    "text" : "\"I just read a book about Stockholm Syndrome, it started off badly but by the end I really liked it.\"\nRIP Ken Dodd",
    "id" : 973098423930015744,
    "created_at" : "2018-03-12 07:28:38 +0000",
    "user" : {
      "name" : "Stig Abell",
      "screen_name" : "StigAbell",
      "protected" : false,
      "id_str" : "509212441",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001518322662477824\/1JlVdvbJ_normal.jpg",
      "id" : 509212441,
      "verified" : true
    }
  },
  "id" : 973119993368993792,
  "created_at" : "2018-03-12 08:54:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "John Breslin",
      "screen_name" : "johnbreslin",
      "indices" : [ 3, 15 ],
      "id_str" : "822659",
      "id" : 822659
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973109994706161664",
  "text" : "RT @johnbreslin: Tim Berners-Lee: we must regulate tech firms to prevent 'weaponised' web\n\nThe inventor of the world wide web warns over co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 202, 225 ],
        "url" : "https:\/\/t.co\/AImY9YHfmD",
        "expanded_url" : "https:\/\/www.theguardian.com\/technology\/2018\/mar\/11\/tim-berners-lee-tech-companies-regulations?CMP=Share_iOSApp_Other",
        "display_url" : "theguardian.com\/technology\/201\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "973106094133465088",
    "text" : "Tim Berners-Lee: we must regulate tech firms to prevent 'weaponised' web\n\nThe inventor of the world wide web warns over concentration of power among a few companies \u2018controlling which ideas are shared\u2019 https:\/\/t.co\/AImY9YHfmD",
    "id" : 973106094133465088,
    "created_at" : "2018-03-12 07:59:06 +0000",
    "user" : {
      "name" : "John Breslin",
      "screen_name" : "johnbreslin",
      "protected" : false,
      "id_str" : "822659",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012071432447234048\/u04cGTRi_normal.jpg",
      "id" : 822659,
      "verified" : true
    }
  },
  "id" : 973109994706161664,
  "created_at" : "2018-03-12 08:14:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "summer shook",
      "screen_name" : "tigerkite",
      "indices" : [ 3, 13 ],
      "id_str" : "1235201",
      "id" : 1235201
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973099432333004800",
  "text" : "RT @tigerkite: I see a trend in academia where passions get exploited. People are told \"it's 80 hours a week, but you're doing what you lov\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "972547058862346241",
    "text" : "I see a trend in academia where passions get exploited. People are told \"it's 80 hours a week, but you're doing what you love.\" Just because you working on your passion, doesn't mean people aren't playing you. We're all people with needs beyond one passion.",
    "id" : 972547058862346241,
    "created_at" : "2018-03-10 18:57:42 +0000",
    "user" : {
      "name" : "summer shook",
      "screen_name" : "tigerkite",
      "protected" : false,
      "id_str" : "1235201",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/904360767981584384\/5wqnCqMA_normal.jpg",
      "id" : 1235201,
      "verified" : false
    }
  },
  "id" : 973099432333004800,
  "created_at" : "2018-03-12 07:32:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ahmed Abdulrahman \uD83D\uDE3B",
      "screen_name" : "_ahmed_ab",
      "indices" : [ 3, 13 ],
      "id_str" : "734983338",
      "id" : 734983338
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "973098033897492480",
  "text" : "RT @_ahmed_ab: \u201CWhere are you from?\u201D\n\n\u201CIraq.\u201D\n\n\u201COh.. which part of Iran, Tehran??\u201D\n\n\u201CIraa with Q not N.\u201D\n\n\u201CAhhh! okay!\u201D https:\/\/t.co\/NqGPn6\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 105, 128 ],
        "url" : "https:\/\/t.co\/NqGPn6wH6C",
        "expanded_url" : "https:\/\/twitter.com\/sarasoueidan\/status\/972793521320099840",
        "display_url" : "twitter.com\/sarasoueidan\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "973056777557303296",
    "text" : "\u201CWhere are you from?\u201D\n\n\u201CIraq.\u201D\n\n\u201COh.. which part of Iran, Tehran??\u201D\n\n\u201CIraa with Q not N.\u201D\n\n\u201CAhhh! okay!\u201D https:\/\/t.co\/NqGPn6wH6C",
    "id" : 973056777557303296,
    "created_at" : "2018-03-12 04:43:08 +0000",
    "user" : {
      "name" : "Ahmed Abdulrahman \uD83D\uDE3B",
      "screen_name" : "_ahmed_ab",
      "protected" : false,
      "id_str" : "734983338",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/846237929492172801\/GI1ms1zr_normal.jpg",
      "id" : 734983338,
      "verified" : false
    }
  },
  "id" : 973098033897492480,
  "created_at" : "2018-03-12 07:27:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/972973036046618625\/photo\/1",
      "indices" : [ 168, 191 ],
      "url" : "https:\/\/t.co\/y9NwMHERe6",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYCxxy6X4AIGO-m.jpg",
      "id_str" : "972972988550340610",
      "id" : 972972988550340610,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYCxxy6X4AIGO-m.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/y9NwMHERe6"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 144, 167 ],
      "url" : "https:\/\/t.co\/fMKrdUWxLT",
      "expanded_url" : "https:\/\/en.m.wikipedia.org\/wiki\/Water_supply_and_sanitation_in_Singapore",
      "display_url" : "en.m.wikipedia.org\/wiki\/Water_sup\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972973036046618625",
  "text" : "Grateful there is no water restrictions in place where I stay but I always prepare.  Maybe I came from a country where water is very precious.\n\nhttps:\/\/t.co\/fMKrdUWxLT https:\/\/t.co\/y9NwMHERe6",
  "id" : 972973036046618625,
  "created_at" : "2018-03-11 23:10:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 177, 200 ],
      "url" : "https:\/\/t.co\/ArcNxSC2j0",
      "expanded_url" : "https:\/\/twitter.com\/Independent_ie\/status\/972744012649172992",
      "display_url" : "twitter.com\/Independent_ie\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972969886673760256",
  "text" : "\"Some might think it's a joke to be prepared as best you can for something going badly wrong, but last week's snow showed how out of touch large sections of the population are\" https:\/\/t.co\/ArcNxSC2j0",
  "id" : 972969886673760256,
  "created_at" : "2018-03-11 22:57:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "spydergrrl\uD83D\uDD77\uFE0F",
      "screen_name" : "spydergrrl",
      "indices" : [ 3, 14 ],
      "id_str" : "18618482",
      "id" : 18618482
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972969335047286784",
  "text" : "RT @spydergrrl: I always twitch when I see an event promote \"a panel of strong powerful women\" instead \"a panel of experts\". It strikes me\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "972562371414757376",
    "text" : "I always twitch when I see an event promote \"a panel of strong powerful women\" instead \"a panel of experts\". It strikes me as disingenuous, as if the women were invited as an explicit act of inclusion or representation, not as equals.",
    "id" : 972562371414757376,
    "created_at" : "2018-03-10 19:58:33 +0000",
    "user" : {
      "name" : "spydergrrl\uD83D\uDD77\uFE0F",
      "screen_name" : "spydergrrl",
      "protected" : false,
      "id_str" : "18618482",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1029723759706603521\/kNzVbgYS_normal.jpg",
      "id" : 18618482,
      "verified" : false
    }
  },
  "id" : 972969335047286784,
  "created_at" : "2018-03-11 22:55:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sean Percival",
      "screen_name" : "Percival",
      "indices" : [ 3, 12 ],
      "id_str" : "2156951",
      "id" : 2156951
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/Percival\/status\/972790429384093696\/photo\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/B7KdQEhTlI",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DYAKc_gX4AUWYiO.jpg",
      "id_str" : "972789012711858181",
      "id" : 972789012711858181,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYAKc_gX4AUWYiO.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 628,
        "resize" : "fit",
        "w" : 626
      }, {
        "h" : 628,
        "resize" : "fit",
        "w" : 626
      }, {
        "h" : 628,
        "resize" : "fit",
        "w" : 626
      }, {
        "h" : 628,
        "resize" : "fit",
        "w" : 626
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/B7KdQEhTlI"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972963297594101763",
  "text" : "RT @Percival: Mobile user experience, Europe (2018) https:\/\/t.co\/B7KdQEhTlI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Percival\/status\/972790429384093696\/photo\/1",
        "indices" : [ 38, 61 ],
        "url" : "https:\/\/t.co\/B7KdQEhTlI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DYAKc_gX4AUWYiO.jpg",
        "id_str" : "972789012711858181",
        "id" : 972789012711858181,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DYAKc_gX4AUWYiO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 626
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 626
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 626
        }, {
          "h" : 628,
          "resize" : "fit",
          "w" : 626
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/B7KdQEhTlI"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "972790429384093696",
    "text" : "Mobile user experience, Europe (2018) https:\/\/t.co\/B7KdQEhTlI",
    "id" : 972790429384093696,
    "created_at" : "2018-03-11 11:04:46 +0000",
    "user" : {
      "name" : "Sean Percival",
      "screen_name" : "Percival",
      "protected" : false,
      "id_str" : "2156951",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/891020901956952064\/yF_ZsO3a_normal.jpg",
      "id" : 2156951,
      "verified" : true
    }
  },
  "id" : 972963297594101763,
  "created_at" : "2018-03-11 22:31:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darragh McCashin",
      "screen_name" : "DarraghMcCashin",
      "indices" : [ 3, 19 ],
      "id_str" : "419652244",
      "id" : 419652244
    }, {
      "name" : "TEAM ITN",
      "screen_name" : "TEAM_ITN",
      "indices" : [ 65, 74 ],
      "id_str" : "801789630236467205",
      "id" : 801789630236467205
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/zXom3qS8Gw",
      "expanded_url" : "https:\/\/twitter.com\/acamh\/status\/972452761186029568",
      "display_url" : "twitter.com\/acamh\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972959545294245889",
  "text" : "RT @DarraghMcCashin: Ireland included in dataset. Data of use to @TEAM_ITN https:\/\/t.co\/zXom3qS8Gw",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TEAM ITN",
        "screen_name" : "TEAM_ITN",
        "indices" : [ 44, 53 ],
        "id_str" : "801789630236467205",
        "id" : 801789630236467205
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 54, 77 ],
        "url" : "https:\/\/t.co\/zXom3qS8Gw",
        "expanded_url" : "https:\/\/twitter.com\/acamh\/status\/972452761186029568",
        "display_url" : "twitter.com\/acamh\/status\/9\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "972602645155368960",
    "text" : "Ireland included in dataset. Data of use to @TEAM_ITN https:\/\/t.co\/zXom3qS8Gw",
    "id" : 972602645155368960,
    "created_at" : "2018-03-10 22:38:35 +0000",
    "user" : {
      "name" : "Darragh McCashin",
      "screen_name" : "DarraghMcCashin",
      "protected" : false,
      "id_str" : "419652244",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/949743183969292289\/QOc75OLx_normal.jpg",
      "id" : 419652244,
      "verified" : false
    }
  },
  "id" : 972959545294245889,
  "created_at" : "2018-03-11 22:16:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Florian Ederer",
      "screen_name" : "florianederer",
      "indices" : [ 3, 17 ],
      "id_str" : "1262082343",
      "id" : 1262082343
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972945521273966593",
  "text" : "RT @florianederer: You're twice as likely to make it in English professional football if you're born in September rather than August. https\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 115, 138 ],
        "url" : "https:\/\/t.co\/TtC8CdHHZx",
        "expanded_url" : "https:\/\/twitter.com\/ewen_\/status\/972426014738518016",
        "display_url" : "twitter.com\/ewen_\/status\/9\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "972811208846954496",
    "text" : "You're twice as likely to make it in English professional football if you're born in September rather than August. https:\/\/t.co\/TtC8CdHHZx",
    "id" : 972811208846954496,
    "created_at" : "2018-03-11 12:27:20 +0000",
    "user" : {
      "name" : "Florian Ederer",
      "screen_name" : "florianederer",
      "protected" : false,
      "id_str" : "1262082343",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/458774330009067521\/JQTX1h3T_normal.jpeg",
      "id" : 1262082343,
      "verified" : false
    }
  },
  "id" : 972945521273966593,
  "created_at" : "2018-03-11 21:21:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "indices" : [ 3, 17 ],
      "id_str" : "150246405",
      "id" : 150246405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/poBaykphil",
      "expanded_url" : "http:\/\/bit.ly\/2FnVuce",
      "display_url" : "bit.ly\/2FnVuce"
    } ]
  },
  "geo" : { },
  "id_str" : "972933273386184705",
  "text" : "RT @thejournal_ie: Clare Co Council looking at idea of 'autonomous cars' to address rural transport issues https:\/\/t.co\/poBaykphil",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/poBaykphil",
        "expanded_url" : "http:\/\/bit.ly\/2FnVuce",
        "display_url" : "bit.ly\/2FnVuce"
      } ]
    },
    "geo" : { },
    "id_str" : "972912792851263490",
    "text" : "Clare Co Council looking at idea of 'autonomous cars' to address rural transport issues https:\/\/t.co\/poBaykphil",
    "id" : 972912792851263490,
    "created_at" : "2018-03-11 19:11:00 +0000",
    "user" : {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "protected" : false,
      "id_str" : "150246405",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948950612800139264\/bebzFmrQ_normal.jpg",
      "id" : 150246405,
      "verified" : true
    }
  },
  "id" : 972933273386184705,
  "created_at" : "2018-03-11 20:32:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972880807391105024",
  "text" : "Anyone here help to set up a F&amp;B biz before? What tools do you use to organise ingredients, menu, equipment? (beside spreadsheet)",
  "id" : 972880807391105024,
  "created_at" : "2018-03-11 17:03:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Existential Comics",
      "screen_name" : "existentialcoms",
      "indices" : [ 3, 19 ],
      "id_str" : "2163374389",
      "id" : 2163374389
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972799242170454016",
  "text" : "RT @existentialcoms: \"If you give up negotiating power over your labor, I will give you frozen yogurt and rides\" is probably the most peak\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/existentialcoms\/status\/962075501996097538\/photo\/1",
        "indices" : [ 168, 191 ],
        "url" : "https:\/\/t.co\/pxYiLJs0xt",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DVn5zubU8AAVnyt.jpg",
        "id_str" : "962074662451605504",
        "id" : 962074662451605504,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DVn5zubU8AAVnyt.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 183,
          "resize" : "fit",
          "w" : 904
        }, {
          "h" : 138,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 183,
          "resize" : "fit",
          "w" : 904
        }, {
          "h" : 183,
          "resize" : "fit",
          "w" : 904
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/pxYiLJs0xt"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "962048747940192256",
    "geo" : { },
    "id_str" : "962075501996097538",
    "in_reply_to_user_id" : 2163374389,
    "text" : "\"If you give up negotiating power over your labor, I will give you frozen yogurt and rides\" is probably the most peak Silicon Valley capitalist overlord mentality yet. https:\/\/t.co\/pxYiLJs0xt",
    "id" : 962075501996097538,
    "in_reply_to_status_id" : 962048747940192256,
    "created_at" : "2018-02-09 21:27:28 +0000",
    "in_reply_to_screen_name" : "existentialcoms",
    "in_reply_to_user_id_str" : "2163374389",
    "user" : {
      "name" : "Existential Comics",
      "screen_name" : "existentialcoms",
      "protected" : false,
      "id_str" : "2163374389",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/792916557403795456\/d-iEnfPD_normal.jpg",
      "id" : 2163374389,
      "verified" : false
    }
  },
  "id" : 972799242170454016,
  "created_at" : "2018-03-11 11:39:47 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "indices" : [ 3, 16 ],
      "id_str" : "36889519",
      "id" : 36889519
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/972568786485301248\/photo\/1",
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/sxlaArEYf9",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DX9CJ76WkAAdlVP.jpg",
      "id_str" : "972568783004012544",
      "id" : 972568783004012544,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DX9CJ76WkAAdlVP.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 514
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 514
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 455
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 514
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sxlaArEYf9"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972579128779165696",
  "text" : "RT @RaminNasibov: Northrop F-5E Tiger II https:\/\/t.co\/sxlaArEYf9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/972568786485301248\/photo\/1",
        "indices" : [ 23, 46 ],
        "url" : "https:\/\/t.co\/sxlaArEYf9",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DX9CJ76WkAAdlVP.jpg",
        "id_str" : "972568783004012544",
        "id" : 972568783004012544,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DX9CJ76WkAAdlVP.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 514
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 514
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 455
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 514
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sxlaArEYf9"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "972568786485301248",
    "text" : "Northrop F-5E Tiger II https:\/\/t.co\/sxlaArEYf9",
    "id" : 972568786485301248,
    "created_at" : "2018-03-10 20:24:02 +0000",
    "user" : {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "protected" : false,
      "id_str" : "36889519",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/896399812450557953\/gP3PRbXP_normal.jpg",
      "id" : 36889519,
      "verified" : true
    }
  },
  "id" : 972579128779165696,
  "created_at" : "2018-03-10 21:05:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "BBC",
      "screen_name" : "BBC",
      "indices" : [ 3, 7 ],
      "id_str" : "19701628",
      "id" : 19701628
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/BBC\/status\/972381291143249920\/video\/1",
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/Q5I70XxT7T",
      "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/943157249815535618\/img\/5-h8Kq3WNlXWnRfT.jpg",
      "id_str" : "943157249815535618",
      "id" : 943157249815535618,
      "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/943157249815535618\/img\/5-h8Kq3WNlXWnRfT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 675,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 383,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      }, {
        "h" : 720,
        "resize" : "fit",
        "w" : 1280
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Q5I70XxT7T"
    } ],
    "hashtags" : [ {
      "text" : "BBCdad",
      "indices" : [ 61, 68 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972549836183605249",
  "text" : "RT @BBC: \uD83D\uDE4C 1 year ago today the world became a better place.\n#BBCdad https:\/\/t.co\/Q5I70XxT7T",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/studio.twitter.com\" rel=\"nofollow\"\u003EMedia Studio\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/BBC\/status\/972381291143249920\/video\/1",
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/Q5I70XxT7T",
        "media_url" : "http:\/\/pbs.twimg.com\/amplify_video_thumb\/943157249815535618\/img\/5-h8Kq3WNlXWnRfT.jpg",
        "id_str" : "943157249815535618",
        "id" : 943157249815535618,
        "media_url_https" : "https:\/\/pbs.twimg.com\/amplify_video_thumb\/943157249815535618\/img\/5-h8Kq3WNlXWnRfT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        }, {
          "h" : 720,
          "resize" : "fit",
          "w" : 1280
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Q5I70XxT7T"
      } ],
      "hashtags" : [ {
        "text" : "BBCdad",
        "indices" : [ 52, 59 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "972381291143249920",
    "text" : "\uD83D\uDE4C 1 year ago today the world became a better place.\n#BBCdad https:\/\/t.co\/Q5I70XxT7T",
    "id" : 972381291143249920,
    "created_at" : "2018-03-10 07:59:00 +0000",
    "user" : {
      "name" : "BBC",
      "screen_name" : "BBC",
      "protected" : false,
      "id_str" : "19701628",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/662708106\/bbc_normal.png",
      "id" : 19701628,
      "verified" : true
    }
  },
  "id" : 972549836183605249,
  "created_at" : "2018-03-10 19:08:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/STQfT3TSmA",
      "expanded_url" : "http:\/\/a.msn.com\/01\/en-ie\/BBK4kgv?ocid=st",
      "display_url" : "a.msn.com\/01\/en-ie\/BBK4k\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972546960828837889",
  "text" : "The Man Bringing Trump and Kim Jong-un Together https:\/\/t.co\/STQfT3TSmA",
  "id" : 972546960828837889,
  "created_at" : "2018-03-10 18:57:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/972509827221123072\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/joiOq4qfKp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DX8MhO6W0AAkXEd.jpg",
      "id_str" : "972509809613393920",
      "id" : 972509809613393920,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DX8MhO6W0AAkXEd.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 806
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 472
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 268
      }, {
        "h" : 2743,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/joiOq4qfKp"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972509827221123072",
  "text" : "Due to protest march today, bus get stuck...\uD83D\uDE12 They don't plan for alternatives routes? https:\/\/t.co\/joiOq4qfKp",
  "id" : 972509827221123072,
  "created_at" : "2018-03-10 16:29:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 37, 60 ],
      "url" : "https:\/\/t.co\/YJPU9YsPZ1",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/programmes\/b09vb7m3",
      "display_url" : "bbc.co.uk\/programmes\/b09\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972398835866710016",
  "text" : "Did anyone watch this last evening?  https:\/\/t.co\/YJPU9YsPZ1",
  "id" : 972398835866710016,
  "created_at" : "2018-03-10 09:08:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "179569408",
      "id" : 179569408
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972393712742518784",
  "text" : "RT @DublinAirport: T\u00EDr gan teanga, t\u00EDr gan anam is an Irish saying which translates as A country without a language is a country without a\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.hootsuite.com\" rel=\"nofollow\"\u003EHootsuite\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DublinAirport\/status\/971440400190894083\/photo\/1",
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/oPho0YONNw",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXs_5OqXcAMjwe0.jpg",
        "id_str" : "971440397049360387",
        "id" : 971440397049360387,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXs_5OqXcAMjwe0.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 667,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 667,
          "resize" : "fit",
          "w" : 1000
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 667,
          "resize" : "fit",
          "w" : 1000
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oPho0YONNw"
      } ],
      "hashtags" : [ {
        "text" : "SnaG18",
        "indices" : [ 126, 133 ]
      }, {
        "text" : "Gaeilge",
        "indices" : [ 134, 142 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971440400190894083",
    "text" : "T\u00EDr gan teanga, t\u00EDr gan anam is an Irish saying which translates as A country without a language is a country without a soul. #SnaG18 #Gaeilge https:\/\/t.co\/oPho0YONNw",
    "id" : 971440400190894083,
    "created_at" : "2018-03-07 17:40:14 +0000",
    "user" : {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "protected" : false,
      "id_str" : "179569408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013556776321650694\/bdnXxxoU_normal.jpg",
      "id" : 179569408,
      "verified" : true
    }
  },
  "id" : 972393712742518784,
  "created_at" : "2018-03-10 08:48:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/aysYlDtUJe",
      "expanded_url" : "http:\/\/www.straitstimes.com\/singapore\/peoples-park-complex-peoples-park-centre-golden-mile-complex-and-golden-mile-tower",
      "display_url" : "straitstimes.com\/singapore\/peop\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972388167134441472",
  "text" : "There is a stall that sell yummy kaya toast at Golden Mile Complex \uD83D\uDC94https:\/\/t.co\/aysYlDtUJe",
  "id" : 972388167134441472,
  "created_at" : "2018-03-10 08:26:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karen Tay",
      "screen_name" : "karentzy",
      "indices" : [ 3, 12 ],
      "id_str" : "282115973",
      "id" : 282115973
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SXSW",
      "indices" : [ 25, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972215321800118272",
  "text" : "RT @karentzy: Join me at #SXSW - why is Uber selling to Grab, Alibaba buying Lazada, and Google scaling their massive engineering presence\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "SXSW",
        "indices" : [ 11, 16 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971267112831455236",
    "text" : "Join me at #SXSW - why is Uber selling to Grab, Alibaba buying Lazada, and Google scaling their massive engineering presence in Singapore building products for the Next Billion Users? Answer: Southeast Asia is a market of 600 million, and Singapore is the digital Capital of Asia.",
    "id" : 971267112831455236,
    "created_at" : "2018-03-07 06:11:39 +0000",
    "user" : {
      "name" : "Karen Tay",
      "screen_name" : "karentzy",
      "protected" : false,
      "id_str" : "282115973",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/918560959383269376\/6hNErXjc_normal.jpg",
      "id" : 282115973,
      "verified" : false
    }
  },
  "id" : 972215321800118272,
  "created_at" : "2018-03-09 20:59:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/7NCRF96G9T",
      "expanded_url" : "https:\/\/blog.drift.com\/year-without-forms\/",
      "display_url" : "blog.drift.com\/year-without-f\u2026"
    }, {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/kvbmcSFaM7",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/866307722811232258",
      "display_url" : "twitter.com\/mryap\/status\/8\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972196552369491970",
  "text" : "A Year Without Lead Forms https:\/\/t.co\/7NCRF96G9T https:\/\/t.co\/kvbmcSFaM7",
  "id" : 972196552369491970,
  "created_at" : "2018-03-09 19:44:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/972155330900758528\/photo\/1",
      "indices" : [ 87, 110 ],
      "url" : "https:\/\/t.co\/8iXyaATk4t",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DX3KHFOXUAAgvIN.jpg",
      "id_str" : "972155317592215552",
      "id" : 972155317592215552,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DX3KHFOXUAAgvIN.jpg",
      "sizes" : [ {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/8iXyaATk4t"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972155330900758528",
  "text" : "The odds when a biz logo appear to be near identical to an existing one on a timeline. https:\/\/t.co\/8iXyaATk4t",
  "id" : 972155330900758528,
  "created_at" : "2018-03-09 17:01:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/FkwdsiBPsR",
      "expanded_url" : "https:\/\/www.forbes.com\/sites\/lizryan\/2018\/03\/06\/no-i-wont-write-a-marketing-plan-as-part-of-the-interview-process\/",
      "display_url" : "forbes.com\/sites\/lizryan\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972153647261016066",
  "text" : "Have you being ask to do something like this in an interview? https:\/\/t.co\/FkwdsiBPsR",
  "id" : 972153647261016066,
  "created_at" : "2018-03-09 16:54:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 102, 125 ],
      "url" : "https:\/\/t.co\/pLEKCMwgbB",
      "expanded_url" : "https:\/\/www.linkedin.com\/feed\/update\/urn:li:activity:6377160902390710272",
      "display_url" : "linkedin.com\/feed\/update\/ur\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "972146223712014336",
  "text" : "\"when our product doesn't work in a certain country we think of that as a bug rather a feature gap\".  https:\/\/t.co\/pLEKCMwgbB",
  "id" : 972146223712014336,
  "created_at" : "2018-03-09 16:24:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nippon.com",
      "screen_name" : "nippon_en",
      "indices" : [ 3, 13 ],
      "id_str" : "352160450",
      "id" : 352160450
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972099619118338048",
  "text" : "RT @nippon_en: Today is \"Thank You Day\" in Japan because 3\/9 can be pronounced \"san ky\u016B!\" Thank you to all our followers for reading our ar\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "\u30B5\u30F3\u30AD\u30E5\u30FC\u306E\u65E5",
        "indices" : [ 132, 140 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971996237003018240",
    "text" : "Today is \"Thank You Day\" in Japan because 3\/9 can be pronounced \"san ky\u016B!\" Thank you to all our followers for reading our articles.\n#\u30B5\u30F3\u30AD\u30E5\u30FC\u306E\u65E5",
    "id" : 971996237003018240,
    "created_at" : "2018-03-09 06:28:56 +0000",
    "user" : {
      "name" : "Nippon.com",
      "screen_name" : "nippon_en",
      "protected" : false,
      "id_str" : "352160450",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/876959427374469120\/xbdwXvZl_normal.jpg",
      "id" : 352160450,
      "verified" : false
    }
  },
  "id" : 972099619118338048,
  "created_at" : "2018-03-09 13:19:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Robert E Kelly",
      "screen_name" : "Robert_E_Kelly",
      "indices" : [ 3, 18 ],
      "id_str" : "489218573",
      "id" : 489218573
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972041725345783808",
  "text" : "RT @Robert_E_Kelly: On the Trump-Kim Summit: Summits normally come at the end of a long series of negotiations at lower levels in which lot\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971923381091094529",
    "text" : "On the Trump-Kim Summit: Summits normally come at the end of a long series of negotiations at lower levels in which lots of devils in the details r hammered out. Trump, always the publicity-seeker, is just diving right in, which is why the Korea analyst community is responding \/1",
    "id" : 971923381091094529,
    "created_at" : "2018-03-09 01:39:26 +0000",
    "user" : {
      "name" : "Robert E Kelly",
      "screen_name" : "Robert_E_Kelly",
      "protected" : false,
      "id_str" : "489218573",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/543237153350631424\/CxfvlCxP_normal.jpeg",
      "id" : 489218573,
      "verified" : true
    }
  },
  "id" : 972041725345783808,
  "created_at" : "2018-03-09 09:29:41 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Una McGuinness",
      "screen_name" : "AspireUna",
      "indices" : [ 0, 10 ],
      "id_str" : "2545084847",
      "id" : 2545084847
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "972021340902445056",
  "geo" : { },
  "id_str" : "972025542886256640",
  "in_reply_to_user_id" : 2545084847,
  "text" : "@AspireUna It going to be awkward...",
  "id" : 972025542886256640,
  "in_reply_to_status_id" : 972021340902445056,
  "created_at" : "2018-03-09 08:25:23 +0000",
  "in_reply_to_screen_name" : "AspireUna",
  "in_reply_to_user_id_str" : "2545084847",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "972025179885981697",
  "text" : "I reckon Live Chat is replacing Contact Form on website. It redundant to have both. Isn't it?",
  "id" : 972025179885981697,
  "created_at" : "2018-03-09 08:23:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 176, 199 ],
      "url" : "https:\/\/t.co\/mQBdHF3gEX",
      "expanded_url" : "https:\/\/twitter.com\/damocrat\/status\/971482516912836608",
      "display_url" : "twitter.com\/damocrat\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971855821469552640",
  "text" : "The Younger one do not want to be care workers. They also do not want to take care of the old one.  You still need immigrants to wash your ass if you bed-ridden. Just staying. https:\/\/t.co\/mQBdHF3gEX",
  "id" : 971855821469552640,
  "created_at" : "2018-03-08 21:10:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971853414761189377",
  "text" : "With Dask, you will be able to take Python workflows &amp; scale them up to large datasets on your workstation without the need to migrate to a distributed computing environment. Is there anything similar for R? Thanks",
  "id" : 971853414761189377,
  "created_at" : "2018-03-08 21:01:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/y90gR4Zn2z",
      "expanded_url" : "https:\/\/sc.mp\/2AzGW2D",
      "display_url" : "sc.mp\/2AzGW2D"
    } ]
  },
  "geo" : { },
  "id_str" : "971834349166518272",
  "text" : "China\u2019s cash-killers force rival payment services to innovate https:\/\/t.co\/y90gR4Zn2z",
  "id" : 971834349166518272,
  "created_at" : "2018-03-08 19:45:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971820724028624896",
  "text" : "On the bus again on my way back home....along that same route",
  "id" : 971820724028624896,
  "created_at" : "2018-03-08 18:51:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Nathan Yau",
      "screen_name" : "flowingdata",
      "indices" : [ 3, 15 ],
      "id_str" : "14109167",
      "id" : 14109167
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971807700760846336",
  "text" : "RT @flowingdata: Visualizing outliers sometimes means highlighting the extraordinary. Other times it serves as background context. Chart ac\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 133, 156 ],
        "url" : "https:\/\/t.co\/j1lxwEUzrT",
        "expanded_url" : "http:\/\/flowingdata.com\/2018\/03\/07\/visualizing-outliers\/",
        "display_url" : "flowingdata.com\/2018\/03\/07\/vis\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "971479781739921408",
    "text" : "Visualizing outliers sometimes means highlighting the extraordinary. Other times it serves as background context. Chart accordingly. https:\/\/t.co\/j1lxwEUzrT",
    "id" : 971479781739921408,
    "created_at" : "2018-03-07 20:16:43 +0000",
    "user" : {
      "name" : "Nathan Yau",
      "screen_name" : "flowingdata",
      "protected" : false,
      "id_str" : "14109167",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/986346563642404864\/dUr9UrOk_normal.jpg",
      "id" : 14109167,
      "verified" : true
    }
  },
  "id" : 971807700760846336,
  "created_at" : "2018-03-08 17:59:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971807242382200833\/photo\/1",
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/5ubqnGNbpz",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXyNdztW0AA5yhq.jpg",
      "id_str" : "971807162841354240",
      "id" : 971807162841354240,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXyNdztW0AA5yhq.jpg",
      "sizes" : [ {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5ubqnGNbpz"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971807242382200833",
  "text" : "More like half-hour delay. \uD83D\uDE32\uD83D\uDE20\uD83D\uDE31 https:\/\/t.co\/5ubqnGNbpz",
  "id" : 971807242382200833,
  "created_at" : "2018-03-08 17:57:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "971805612400168960",
  "geo" : { },
  "id_str" : "971806234444746755",
  "in_reply_to_user_id" : 9465632,
  "text" : "Imagine emergency vehicles taking this route...",
  "id" : 971806234444746755,
  "in_reply_to_status_id" : 971805612400168960,
  "created_at" : "2018-03-08 17:53:56 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "971804805168549888",
  "geo" : { },
  "id_str" : "971805612400168960",
  "in_reply_to_user_id" : 9465632,
  "text" : "Situation aggrevated as there is a fast food restaurant along this route.",
  "id" : 971805612400168960,
  "in_reply_to_status_id" : 971804805168549888,
  "created_at" : "2018-03-08 17:51:27 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971804805168549888\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/6H5UAFbmgD",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXyLS1yWAAI4E1n.jpg",
      "id_str" : "971804775397326850",
      "id" : 971804775397326850,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXyLS1yWAAI4E1n.jpg",
      "sizes" : [ {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/6H5UAFbmgD"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971804805168549888",
  "text" : "Traffic grid lock.  I called it \"The Devil Alley\" https:\/\/t.co\/6H5UAFbmgD",
  "id" : 971804805168549888,
  "created_at" : "2018-03-08 17:48:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Database Hulk",
      "screen_name" : "DATABASE_HULK",
      "indices" : [ 3, 17 ],
      "id_str" : "417514427",
      "id" : 417514427
    }, {
      "name" : "Hadley Wickham",
      "screen_name" : "hadleywickham",
      "indices" : [ 20, 34 ],
      "id_str" : "69133574",
      "id" : 69133574
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971798820752429056",
  "text" : "RT @DATABASE_HULK: .@hadleywickham SAY, \"80% f the time importing data is boring and tedious, and then 20% of the time it's just endless sc\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Hadley Wickham",
        "screen_name" : "hadleywickham",
        "indices" : [ 1, 15 ],
        "id_str" : "69133574",
        "id" : 69133574
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "NICAR18",
        "indices" : [ 152, 160 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971765242425499649",
    "text" : ".@hadleywickham SAY, \"80% f the time importing data is boring and tedious, and then 20% of the time it's just endless screaming.\" HULK FEEL UNDERSTOOD. #NICAR18",
    "id" : 971765242425499649,
    "created_at" : "2018-03-08 15:11:02 +0000",
    "user" : {
      "name" : "Database Hulk",
      "screen_name" : "DATABASE_HULK",
      "protected" : false,
      "id_str" : "417514427",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1649502488\/Nerd_Hulk_normal",
      "id" : 417514427,
      "verified" : false
    }
  },
  "id" : 971798820752429056,
  "created_at" : "2018-03-08 17:24:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 0, 11 ],
      "id_str" : "26903787",
      "id" : 26903787
    }, {
      "name" : "Mayur Agarwal",
      "screen_name" : "mayurjango",
      "indices" : [ 134, 145 ],
      "id_str" : "14989069",
      "id" : 14989069
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/X4Y6ozpakt",
      "expanded_url" : "https:\/\/mobile.twitter.com\/i\/bookmarks",
      "display_url" : "mobile.twitter.com\/i\/bookmarks"
    } ]
  },
  "in_reply_to_status_id_str" : "970251638941331456",
  "geo" : { },
  "id_str" : "971763905260326914",
  "in_reply_to_user_id" : 26903787,
  "text" : "@Dotnetster Learn that you can access Twitter Bookmarks from your desktop browser via this link - https:\/\/t.co\/X4Y6ozpakt  hat-tip to @mayurjango",
  "id" : 971763905260326914,
  "in_reply_to_status_id" : 970251638941331456,
  "created_at" : "2018-03-08 15:05:44 +0000",
  "in_reply_to_screen_name" : "Dotnetster",
  "in_reply_to_user_id_str" : "26903787",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Cluster",
      "screen_name" : "cluster_ie",
      "indices" : [ 3, 14 ],
      "id_str" : "2396889272",
      "id" : 2396889272
    }, {
      "name" : "European Commission \uD83C\uDDEA\uD83C\uDDFA",
      "screen_name" : "EU_Commission",
      "indices" : [ 20, 34 ],
      "id_str" : "157981564",
      "id" : 157981564
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971759205387526150",
  "text" : "RT @cluster_ie: The @EU_Commission are currently gathering data from businesses across Europe to inform start-up and scale-up policy. \uD83C\uDDEA\uD83C\uDDFA\n\n@\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "European Commission \uD83C\uDDEA\uD83C\uDDFA",
        "screen_name" : "EU_Commission",
        "indices" : [ 4, 18 ],
        "id_str" : "157981564",
        "id" : 157981564
      }, {
        "name" : "Department of Business, Enterprise and Innovation",
        "screen_name" : "EnterInnov",
        "indices" : [ 122, 133 ],
        "id_str" : "144888395",
        "id" : 144888395
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 226, 249 ],
        "url" : "https:\/\/t.co\/uhGitTNkvj",
        "expanded_url" : "http:\/\/startupmonitor.eu\/limesurvey\/index.php\/628959?token=survey86&newtest=Y&lang=en",
        "display_url" : "startupmonitor.eu\/limesurvey\/ind\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "971742831667830785",
    "text" : "The @EU_Commission are currently gathering data from businesses across Europe to inform start-up and scale-up policy. \uD83C\uDDEA\uD83C\uDDFA\n\n@EnterInnov is asking small Irish businesses to help by taking part in this anonymous online survey \uD83D\uDE4F \n\nhttps:\/\/t.co\/uhGitTNkvj",
    "id" : 971742831667830785,
    "created_at" : "2018-03-08 13:41:59 +0000",
    "user" : {
      "name" : "Cluster",
      "screen_name" : "cluster_ie",
      "protected" : false,
      "id_str" : "2396889272",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001849561273466880\/5XkSSTe6_normal.jpg",
      "id" : 2396889272,
      "verified" : false
    }
  },
  "id" : 971759205387526150,
  "created_at" : "2018-03-08 14:47:03 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971746709348016128\/photo\/1",
      "indices" : [ 145, 168 ],
      "url" : "https:\/\/t.co\/w0cazodktg",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXxWei2WkAAGPkn.jpg",
      "id_str" : "971746702356025344",
      "id" : 971746702356025344,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXxWei2WkAAGPkn.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 464,
        "resize" : "fit",
        "w" : 1011
      }, {
        "h" : 312,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 464,
        "resize" : "fit",
        "w" : 1011
      }, {
        "h" : 464,
        "resize" : "fit",
        "w" : 1011
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/w0cazodktg"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971746709348016128",
  "text" : "Time to get personal site up. A change from WordPress (database) as backend to Github for storing flat file. Here the drafted content on GitHub. https:\/\/t.co\/w0cazodktg",
  "id" : 971746709348016128,
  "created_at" : "2018-03-08 13:57:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971704546367016961\/photo\/1",
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/YyPIu2UkRy",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXwwF3_X0AEK5GU.jpg",
      "id_str" : "971704497092415489",
      "id" : 971704497092415489,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXwwF3_X0AEK5GU.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YyPIu2UkRy"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971704546367016961",
  "text" : "Does having a pps number counted as proof of connection to Ireland? https:\/\/t.co\/YyPIu2UkRy",
  "id" : 971704546367016961,
  "created_at" : "2018-03-08 11:09:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971676708297420805",
  "text" : "Some big name cloud computing platform should step up its game on introducing new signups on their product's features. Giving a 30-days free trial without hand holding just do not do the job.",
  "id" : 971676708297420805,
  "created_at" : "2018-03-08 09:19:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971673063992561664\/photo\/1",
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/EqGLAjqeCa",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXwTfwvW0AIICMk.jpg",
      "id_str" : "971673055985586178",
      "id" : 971673055985586178,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXwTfwvW0AIICMk.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 255
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 255
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 255
      }, {
        "h" : 287,
        "resize" : "fit",
        "w" : 255
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/EqGLAjqeCa"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971673063992561664",
  "text" : "Some shy away from it but this biz compare what they offer with other competitors on their website. https:\/\/t.co\/EqGLAjqeCa",
  "id" : 971673063992561664,
  "created_at" : "2018-03-08 09:04:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "IWD2018",
      "indices" : [ 0, 8 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971649652218097664",
  "text" : "#IWD2018 is now an official holiday in several countries. It is a big thing in Vietnam.",
  "id" : 971649652218097664,
  "created_at" : "2018-03-08 07:31:44 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kate Carruthers",
      "screen_name" : "kcarruthers",
      "indices" : [ 3, 15 ],
      "id_str" : "7950832",
      "id" : 7950832
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971647102106456064",
  "text" : "RT @kcarruthers: Hilarious to think that the job I do now didn\u2019t even exist when I *left* high school \uD83D\uDE44 As if teachers don\u2019t have enough ac\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/kcarruthers\/status\/966118791959650307\/photo\/1",
        "indices" : [ 143, 166 ],
        "url" : "https:\/\/t.co\/J8cl4z4ntk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DWhX6VGVoAAuZtf.jpg",
        "id_str" : "966118779678728192",
        "id" : 966118779678728192,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DWhX6VGVoAAuZtf.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1920,
          "resize" : "fit",
          "w" : 1080
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/J8cl4z4ntk"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "966118791959650307",
    "text" : "Hilarious to think that the job I do now didn\u2019t even exist when I *left* high school \uD83D\uDE44 As if teachers don\u2019t have enough actual education to do https:\/\/t.co\/J8cl4z4ntk",
    "id" : 966118791959650307,
    "created_at" : "2018-02-21 01:14:04 +0000",
    "user" : {
      "name" : "Kate Carruthers",
      "screen_name" : "kcarruthers",
      "protected" : false,
      "id_str" : "7950832",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2914559705\/3f5d523f6b5f1cc5871cdac5c6d7d746_normal.jpeg",
      "id" : 7950832,
      "verified" : false
    }
  },
  "id" : 971647102106456064,
  "created_at" : "2018-03-08 07:21:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 4, 27 ],
      "url" : "https:\/\/t.co\/WagrvYFjwp",
      "expanded_url" : "https:\/\/twitter.com\/ajplus\/status\/971581274090426369",
      "display_url" : "twitter.com\/ajplus\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971645085669576705",
  "text" : "Ffs https:\/\/t.co\/WagrvYFjwp",
  "id" : 971645085669576705,
  "created_at" : "2018-03-08 07:13:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "avgeek",
      "indices" : [ 35, 42 ]
    } ],
    "urls" : [ {
      "indices" : [ 43, 66 ],
      "url" : "https:\/\/t.co\/f4Od2AJr5o",
      "expanded_url" : "https:\/\/twitter.com\/BoeingDefense\/status\/960786055866798080",
      "display_url" : "twitter.com\/BoeingDefense\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971511418519474179",
  "text" : "Just love the sound of jet roaring #avgeek https:\/\/t.co\/f4Od2AJr5o",
  "id" : 971511418519474179,
  "created_at" : "2018-03-07 22:22:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The RSAF",
      "screen_name" : "TheRSAF",
      "indices" : [ 3, 11 ],
      "id_str" : "723323122011635712",
      "id" : 723323122011635712
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 54 ],
      "url" : "https:\/\/t.co\/RvnmupVJ02",
      "expanded_url" : "https:\/\/twitter.com\/1819_sg\/status\/962545691951169536",
      "display_url" : "twitter.com\/1819_sg\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971510810211123200",
  "text" : "RT @TheRSAF: An inspiring read https:\/\/t.co\/RvnmupVJ02",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 18, 41 ],
        "url" : "https:\/\/t.co\/RvnmupVJ02",
        "expanded_url" : "https:\/\/twitter.com\/1819_sg\/status\/962545691951169536",
        "display_url" : "twitter.com\/1819_sg\/status\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "962974681073532929",
    "text" : "An inspiring read https:\/\/t.co\/RvnmupVJ02",
    "id" : 962974681073532929,
    "created_at" : "2018-02-12 09:00:29 +0000",
    "user" : {
      "name" : "The RSAF",
      "screen_name" : "TheRSAF",
      "protected" : false,
      "id_str" : "723323122011635712",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/953835853360476161\/0lAaoQzd_normal.jpg",
      "id" : 723323122011635712,
      "verified" : true
    }
  },
  "id" : 971510810211123200,
  "created_at" : "2018-03-07 22:20:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "indices" : [ 0, 10 ],
      "id_str" : "911413417",
      "id" : 911413417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "971270944848252928",
  "geo" : { },
  "id_str" : "971508385437900800",
  "in_reply_to_user_id" : 911413417,
  "text" : "@Dr_Draper I can't believe those 'copyrighting' is from Accenture",
  "id" : 971508385437900800,
  "in_reply_to_status_id" : 971270944848252928,
  "created_at" : "2018-03-07 22:10:23 +0000",
  "in_reply_to_screen_name" : "Dr_Draper",
  "in_reply_to_user_id_str" : "911413417",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "indices" : [ 3, 13 ],
      "id_str" : "911413417",
      "id" : 911413417
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/XoOytmu6X9",
      "expanded_url" : "https:\/\/twitter.com\/gideonspanier\/status\/971057975665324033",
      "display_url" : "twitter.com\/gideonspanier\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971504876944543750",
  "text" : "RT @Dr_Draper: Accenture doing what Accenture does best. Talking bollocks. https:\/\/t.co\/XoOytmu6X9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/XoOytmu6X9",
        "expanded_url" : "https:\/\/twitter.com\/gideonspanier\/status\/971057975665324033",
        "display_url" : "twitter.com\/gideonspanier\/\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "971266730424242176",
    "text" : "Accenture doing what Accenture does best. Talking bollocks. https:\/\/t.co\/XoOytmu6X9",
    "id" : 971266730424242176,
    "created_at" : "2018-03-07 06:10:08 +0000",
    "user" : {
      "name" : "Ryan Wallman",
      "screen_name" : "Dr_Draper",
      "protected" : false,
      "id_str" : "911413417",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2778032626\/f43efb67d690165e4c0003ca3fa2694b_normal.jpeg",
      "id" : 911413417,
      "verified" : false
    }
  },
  "id" : 971504876944543750,
  "created_at" : "2018-03-07 21:56:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 38, 61 ],
      "url" : "https:\/\/t.co\/7MJ59s6B0V",
      "expanded_url" : "https:\/\/twitter.com\/larrykim\/status\/951671065218109440",
      "display_url" : "twitter.com\/larrykim\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971470377519067138",
  "text" : "Don\u2019t build your brand on rented land https:\/\/t.co\/7MJ59s6B0V",
  "id" : 971470377519067138,
  "created_at" : "2018-03-07 19:39:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Mara Averick",
      "screen_name" : "dataandme",
      "indices" : [ 3, 13 ],
      "id_str" : "3230388598",
      "id" : 3230388598
    }, {
      "name" : "Steph de Silva",
      "screen_name" : "StephdeSilva",
      "indices" : [ 82, 95 ],
      "id_str" : "425745533",
      "id" : 425745533
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "rstats",
      "indices" : [ 120, 127 ]
    } ],
    "urls" : [ {
      "indices" : [ 96, 119 ],
      "url" : "https:\/\/t.co\/OdEQKdUsRm",
      "expanded_url" : "https:\/\/buff.ly\/2oKDv5o",
      "display_url" : "buff.ly\/2oKDv5o"
    } ]
  },
  "geo" : { },
  "id_str" : "971469974043877380",
  "text" : "RT @dataandme: Still \uD83D\uDC9Cing this: \"Where do things live in R? R for Excel Users\" by @StephdeSilva https:\/\/t.co\/OdEQKdUsRm #rstats https:\/\/t.c\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Steph de Silva",
        "screen_name" : "StephdeSilva",
        "indices" : [ 67, 80 ],
        "id_str" : "425745533",
        "id" : 425745533
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/dataandme\/status\/971023105220071424\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/JpkmHomwDD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXnEXdhX4AU3jKu.jpg",
        "id_str" : "971023102015692805",
        "id" : 971023102015692805,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXnEXdhX4AU3jKu.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 405,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 405,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 359,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 405,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JpkmHomwDD"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/dataandme\/status\/971023105220071424\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/JpkmHomwDD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXnEXdqWAAATBVw.jpg",
        "id_str" : "971023102053318656",
        "id" : 971023102053318656,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXnEXdqWAAATBVw.jpg",
        "sizes" : [ {
          "h" : 427,
          "resize" : "fit",
          "w" : 767
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 427,
          "resize" : "fit",
          "w" : 767
        }, {
          "h" : 379,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 427,
          "resize" : "fit",
          "w" : 767
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JpkmHomwDD"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/dataandme\/status\/971023105220071424\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/JpkmHomwDD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXnEXeUX0AAyZNe.jpg",
        "id_str" : "971023102229598208",
        "id" : 971023102229598208,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXnEXeUX0AAyZNe.jpg",
        "sizes" : [ {
          "h" : 654,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 579,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 768
        }, {
          "h" : 654,
          "resize" : "fit",
          "w" : 768
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JpkmHomwDD"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/dataandme\/status\/971023105220071424\/photo\/1",
        "indices" : [ 113, 136 ],
        "url" : "https:\/\/t.co\/JpkmHomwDD",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXnEXdjX0AAKJ0T.jpg",
        "id_str" : "971023102024077312",
        "id" : 971023102024077312,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXnEXdjX0AAKJ0T.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 447,
          "resize" : "fit",
          "w" : 767
        }, {
          "h" : 447,
          "resize" : "fit",
          "w" : 767
        }, {
          "h" : 447,
          "resize" : "fit",
          "w" : 767
        }, {
          "h" : 396,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/JpkmHomwDD"
      } ],
      "hashtags" : [ {
        "text" : "rstats",
        "indices" : [ 105, 112 ]
      } ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/OdEQKdUsRm",
        "expanded_url" : "https:\/\/buff.ly\/2oKDv5o",
        "display_url" : "buff.ly\/2oKDv5o"
      } ]
    },
    "geo" : { },
    "id_str" : "971023105220071424",
    "text" : "Still \uD83D\uDC9Cing this: \"Where do things live in R? R for Excel Users\" by @StephdeSilva https:\/\/t.co\/OdEQKdUsRm #rstats https:\/\/t.co\/JpkmHomwDD",
    "id" : 971023105220071424,
    "created_at" : "2018-03-06 14:02:03 +0000",
    "user" : {
      "name" : "Mara Averick",
      "screen_name" : "dataandme",
      "protected" : false,
      "id_str" : "3230388598",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/812016485069680640\/tKpsducS_normal.jpg",
      "id" : 3230388598,
      "verified" : false
    }
  },
  "id" : 971469974043877380,
  "created_at" : "2018-03-07 19:37:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jenni Field",
      "screen_name" : "mrsjennifield",
      "indices" : [ 3, 17 ],
      "id_str" : "18720688",
      "id" : 18720688
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StormEmma",
      "indices" : [ 102, 112 ]
    }, {
      "text" : "BeastFromTheEast",
      "indices" : [ 118, 135 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971463541919244288",
  "text" : "RT @mrsjennifield: I\u2019m fascinated by the various levels of customer service from organisations around #StormEmma  and #BeastFromTheEast - m\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/mrsjennifield\/status\/971455893320724480\/photo\/1",
        "indices" : [ 201, 224 ],
        "url" : "https:\/\/t.co\/mYEedGCuif",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXtN-xgX4AIGS2d.jpg",
        "id_str" : "971455885464821762",
        "id" : 971455885464821762,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXtN-xgX4AIGS2d.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 523,
          "resize" : "fit",
          "w" : 623
        }, {
          "h" : 523,
          "resize" : "fit",
          "w" : 623
        }, {
          "h" : 523,
          "resize" : "fit",
          "w" : 623
        }, {
          "h" : 523,
          "resize" : "fit",
          "w" : 623
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/mYEedGCuif"
      } ],
      "hashtags" : [ {
        "text" : "StormEmma",
        "indices" : [ 83, 93 ]
      }, {
        "text" : "BeastFromTheEast",
        "indices" : [ 99, 116 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971455893320724480",
    "text" : "I\u2019m fascinated by the various levels of customer service from organisations around #StormEmma  and #BeastFromTheEast - my mum has to provide a meteorological report to get a refund on theatre tickets! https:\/\/t.co\/mYEedGCuif",
    "id" : 971455893320724480,
    "created_at" : "2018-03-07 18:41:48 +0000",
    "user" : {
      "name" : "Jenni Field",
      "screen_name" : "mrsjennifield",
      "protected" : false,
      "id_str" : "18720688",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/785890724474654722\/o7y5hzsJ_normal.jpg",
      "id" : 18720688,
      "verified" : false
    }
  },
  "id" : 971463541919244288,
  "created_at" : "2018-03-07 19:12:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 220, 243 ],
      "url" : "https:\/\/t.co\/AnbbYWElbt",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971138343198183430",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971461985421455360",
  "text" : "Grapevine has it that some hospital are well organised and quickly put in place plans that ensure their staff welfare and safety when they comes in to work during red weather alert. There is demonstration of leadership. https:\/\/t.co\/AnbbYWElbt",
  "id" : 971461985421455360,
  "created_at" : "2018-03-07 19:06:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971454833675980800\/photo\/1",
      "indices" : [ 23, 46 ],
      "url" : "https:\/\/t.co\/9ytBljGFQ0",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXtNBDEXcAIAl7M.jpg",
      "id_str" : "971454825027301378",
      "id" : 971454825027301378,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXtNBDEXcAIAl7M.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 1009
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 1009
      }, {
        "h" : 318,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 472,
        "resize" : "fit",
        "w" : 1009
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/9ytBljGFQ0"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971454833675980800",
  "text" : "Is the same substance? https:\/\/t.co\/9ytBljGFQ0",
  "id" : 971454833675980800,
  "created_at" : "2018-03-07 18:37:35 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "indices" : [ 3, 12 ],
      "id_str" : "22199970",
      "id" : 22199970
    }, {
      "name" : "Andy Barnov",
      "screen_name" : "progapandist",
      "indices" : [ 104, 117 ],
      "id_str" : "739840385128759301",
      "id" : 739840385128759301
    }, {
      "name" : "Andrey Sitnik",
      "screen_name" : "andreysitnik",
      "indices" : [ 122, 135 ],
      "id_str" : "2402097420",
      "id" : 2402097420
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971334519591665665",
  "text" : "RT @LeaVerou: Want to speak at more conferences but are worried about your accent?\nRead this article by @progapandist and @andreysitnik\nhtt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Andy Barnov",
        "screen_name" : "progapandist",
        "indices" : [ 90, 103 ],
        "id_str" : "739840385128759301",
        "id" : 739840385128759301
      }, {
        "name" : "Andrey Sitnik",
        "screen_name" : "andreysitnik",
        "indices" : [ 108, 121 ],
        "id_str" : "2402097420",
        "id" : 2402097420
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 122, 145 ],
        "url" : "https:\/\/t.co\/tad8gRRSFI",
        "expanded_url" : "https:\/\/evilmartians.com\/chronicles\/speaking-with-an-accent",
        "display_url" : "evilmartians.com\/chronicles\/spe\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "971177485076451329",
    "text" : "Want to speak at more conferences but are worried about your accent?\nRead this article by @progapandist and @andreysitnik\nhttps:\/\/t.co\/tad8gRRSFI",
    "id" : 971177485076451329,
    "created_at" : "2018-03-07 00:15:30 +0000",
    "user" : {
      "name" : "Lea Verou",
      "screen_name" : "LeaVerou",
      "protected" : false,
      "id_str" : "22199970",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/584963092120899586\/TxkxQ7Y5_normal.png",
      "id" : 22199970,
      "verified" : true
    }
  },
  "id" : 971334519591665665,
  "created_at" : "2018-03-07 10:39:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "indices" : [ 3, 18 ],
      "id_str" : "1626294277",
      "id" : 1626294277
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971331279789740032",
  "text" : "RT @spectatorindex: BREAKING: Sri Lankan government has ordered blocking Facebook",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971313981188182016",
    "text" : "BREAKING: Sri Lankan government has ordered blocking Facebook",
    "id" : 971313981188182016,
    "created_at" : "2018-03-07 09:17:53 +0000",
    "user" : {
      "name" : "The Spectator Index",
      "screen_name" : "spectatorindex",
      "protected" : false,
      "id_str" : "1626294277",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/839067030904922112\/LH4xqz-d_normal.jpg",
      "id" : 1626294277,
      "verified" : false
    }
  },
  "id" : 971331279789740032,
  "created_at" : "2018-03-07 10:26:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Andrew Barclay \uD83D\uDCDD",
      "screen_name" : "andrewreporting",
      "indices" : [ 3, 19 ],
      "id_str" : "124055156",
      "id" : 124055156
    }, {
      "name" : "Abacus",
      "screen_name" : "AbacusNews",
      "indices" : [ 129, 140 ],
      "id_str" : "869084593047064577",
      "id" : 869084593047064577
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/XrRuDnJ4i2",
      "expanded_url" : "https:\/\/www.abacusnews.com\/tencents-new-digital-contracts-will-let-kids-trade-good-grades-screen-time\/article\/2136001",
      "display_url" : "abacusnews.com\/tencents-new-d\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "971331250916151297",
  "text" : "RT @andrewreporting: Tencent\u2019s new digital contracts will let kids trade good grades for screen time https:\/\/t.co\/XrRuDnJ4i2 via @AbacusNews",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Abacus",
        "screen_name" : "AbacusNews",
        "indices" : [ 108, 119 ],
        "id_str" : "869084593047064577",
        "id" : 869084593047064577
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/XrRuDnJ4i2",
        "expanded_url" : "https:\/\/www.abacusnews.com\/tencents-new-digital-contracts-will-let-kids-trade-good-grades-screen-time\/article\/2136001",
        "display_url" : "abacusnews.com\/tencents-new-d\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "971202729908424704",
    "text" : "Tencent\u2019s new digital contracts will let kids trade good grades for screen time https:\/\/t.co\/XrRuDnJ4i2 via @AbacusNews",
    "id" : 971202729908424704,
    "created_at" : "2018-03-07 01:55:49 +0000",
    "user" : {
      "name" : "Andrew Barclay \uD83D\uDCDD",
      "screen_name" : "andrewreporting",
      "protected" : false,
      "id_str" : "124055156",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1012161203819298817\/-6RuuzWQ_normal.jpg",
      "id" : 124055156,
      "verified" : false
    }
  },
  "id" : 971331250916151297,
  "created_at" : "2018-03-07 10:26:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "indices" : [ 3, 16 ],
      "id_str" : "367008160",
      "id" : 367008160
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971309337003118593",
  "text" : "RT @managerspeak: The management team are extremely grateful to the people who have come to work in spite of the weather to fulfill their z\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969510094630215680",
    "text" : "The management team are extremely grateful to the people who have come to work in spite of the weather to fulfill their zero hour contracts.",
    "id" : 969510094630215680,
    "created_at" : "2018-03-02 09:49:53 +0000",
    "user" : {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "protected" : false,
      "id_str" : "367008160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3689160554\/8dc837089c6fa2aa3348074a6b1e8ec1_normal.jpeg",
      "id" : 367008160,
      "verified" : false
    }
  },
  "id" : 971309337003118593,
  "created_at" : "2018-03-07 08:59:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "indices" : [ 3, 16 ],
      "id_str" : "367008160",
      "id" : 367008160
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971309134254624768",
  "text" : "RT @managerspeak: We've interrogated the data and it's not telling us what we want to hear. We may need to resort to torturing it.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970631819912777728",
    "text" : "We've interrogated the data and it's not telling us what we want to hear. We may need to resort to torturing it.",
    "id" : 970631819912777728,
    "created_at" : "2018-03-05 12:07:13 +0000",
    "user" : {
      "name" : "Management Speak",
      "screen_name" : "managerspeak",
      "protected" : false,
      "id_str" : "367008160",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3689160554\/8dc837089c6fa2aa3348074a6b1e8ec1_normal.jpeg",
      "id" : 367008160,
      "verified" : false
    }
  },
  "id" : 971309134254624768,
  "created_at" : "2018-03-07 08:58:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Karthik Balakrishnan",
      "screen_name" : "karthikb351",
      "indices" : [ 3, 15 ],
      "id_str" : "49362318",
      "id" : 49362318
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971304992752717825",
  "text" : "RT @karthikb351: Was slightly mindblown when I discovered this.\n\nThe two parts to the word \u201Chelicopter\u201D are not \u201Cheli\u201D and \u201Ccopter\u201D,  but \u201C\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970686473530851328",
    "text" : "Was slightly mindblown when I discovered this.\n\nThe two parts to the word \u201Chelicopter\u201D are not \u201Cheli\u201D and \u201Ccopter\u201D,  but \u201Chelico\u201D meaning spiral, and \u201Cpter\u201D meaning one with wings, like pterodactyl.",
    "id" : 970686473530851328,
    "created_at" : "2018-03-05 15:44:24 +0000",
    "user" : {
      "name" : "Karthik Balakrishnan",
      "screen_name" : "karthikb351",
      "protected" : false,
      "id_str" : "49362318",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/812663869084614656\/1dOp_Jjq_normal.jpg",
      "id" : 49362318,
      "verified" : false
    }
  },
  "id" : 971304992752717825,
  "created_at" : "2018-03-07 08:42:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "indices" : [ 3, 15 ],
      "id_str" : "3760488922",
      "id" : 3760488922
    }, {
      "name" : "Kate Marvel",
      "screen_name" : "DrKateMarvel",
      "indices" : [ 113, 126 ],
      "id_str" : "2354013972",
      "id" : 2354013972
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971170622075219968",
  "text" : "RT @drjuliashaw: \"Courage is the resolve to do well without the assurance of a happy ending\u201D\n\nBeautiful quote by @DrKateMarvel on standing\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Kate Marvel",
        "screen_name" : "DrKateMarvel",
        "indices" : [ 96, 109 ],
        "id_str" : "2354013972",
        "id" : 2354013972
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971170270147997696",
    "text" : "\"Courage is the resolve to do well without the assurance of a happy ending\u201D\n\nBeautiful quote by @DrKateMarvel on standing up for science &amp; ethics",
    "id" : 971170270147997696,
    "created_at" : "2018-03-06 23:46:50 +0000",
    "user" : {
      "name" : "Dr Julia Shaw",
      "screen_name" : "drjuliashaw",
      "protected" : false,
      "id_str" : "3760488922",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/989936592591650817\/TRkR3cmy_normal.jpg",
      "id" : 3760488922,
      "verified" : true
    }
  },
  "id" : 971170622075219968,
  "created_at" : "2018-03-06 23:48:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971169220582105088",
  "text" : "Time to pull out the staff grievous and complaint procedure policy booklet and blow away the cobwebs...",
  "id" : 971169220582105088,
  "created_at" : "2018-03-06 23:42:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/qngYbXYEGe",
      "expanded_url" : "http:\/\/po.st\/fwRXhR",
      "display_url" : "po.st\/fwRXhR"
    } ]
  },
  "geo" : { },
  "id_str" : "971142666670034947",
  "text" : "My 8-year-old, the computer prodigy - and why we moved him to Melbourne https:\/\/t.co\/qngYbXYEGe",
  "id" : 971142666670034947,
  "created_at" : "2018-03-06 21:57:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jeff Gothelf",
      "screen_name" : "jboogie",
      "indices" : [ 3, 11 ],
      "id_str" : "9384812",
      "id" : 9384812
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "SenseAndRespond",
      "indices" : [ 49, 65 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971138714146627585",
  "text" : "RT @jboogie: I\u2019m giving away 10 signed copies of #SenseAndRespond . To enter, retweet this by 9:00 ET \/ 15:00 CET tomorrow. https:\/\/t.co\/wL\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jboogie\/status\/971022787656781824\/photo\/1",
        "indices" : [ 111, 134 ],
        "url" : "https:\/\/t.co\/wLG3BuGBKh",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXnEE-CVwAAJZq3.jpg",
        "id_str" : "971022784326385664",
        "id" : 971022784326385664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXnEE-CVwAAJZq3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 4032,
          "resize" : "fit",
          "w" : 3024
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 900
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1536
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/wLG3BuGBKh"
      } ],
      "hashtags" : [ {
        "text" : "SenseAndRespond",
        "indices" : [ 36, 52 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "971022787656781824",
    "text" : "I\u2019m giving away 10 signed copies of #SenseAndRespond . To enter, retweet this by 9:00 ET \/ 15:00 CET tomorrow. https:\/\/t.co\/wLG3BuGBKh",
    "id" : 971022787656781824,
    "created_at" : "2018-03-06 14:00:47 +0000",
    "user" : {
      "name" : "Jeff Gothelf",
      "screen_name" : "jboogie",
      "protected" : false,
      "id_str" : "9384812",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1038705935471063040\/723yLaBX_normal.jpg",
      "id" : 9384812,
      "verified" : true
    }
  },
  "id" : 971138714146627585,
  "created_at" : "2018-03-06 21:41:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/971138343198183430\/photo\/1",
      "indices" : [ 134, 157 ],
      "url" : "https:\/\/t.co\/5BfDXojyd3",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXotGJ0X4AUGOIW.jpg",
      "id_str" : "971138253389815813",
      "id" : 971138253389815813,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXotGJ0X4AUGOIW.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 471,
        "resize" : "fit",
        "w" : 622
      }, {
        "h" : 471,
        "resize" : "fit",
        "w" : 622
      }, {
        "h" : 471,
        "resize" : "fit",
        "w" : 622
      }, {
        "h" : 471,
        "resize" : "fit",
        "w" : 622
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/5BfDXojyd3"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "971138343198183430",
  "text" : "Shouldn't the hospital nursing manager\/director make the arrangement? i.e. Liaise with community to clear the snow, arrange transport https:\/\/t.co\/5BfDXojyd3",
  "id" : 971138343198183430,
  "created_at" : "2018-03-06 21:39:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/zq9gkhzOgm",
      "expanded_url" : "http:\/\/po.st\/KOXMeJ",
      "display_url" : "po.st\/KOXMeJ"
    } ]
  },
  "geo" : { },
  "id_str" : "971091301641965568",
  "text" : "Singapore libraries have a new remit: Equip seniors citizen with digital skills https:\/\/t.co\/zq9gkhzOgm",
  "id" : 971091301641965568,
  "created_at" : "2018-03-06 18:33:02 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 66, 89 ],
      "url" : "https:\/\/t.co\/YLEYAF6zxS",
      "expanded_url" : "http:\/\/entm.ag\/27j",
      "display_url" : "entm.ag\/27j"
    } ]
  },
  "geo" : { },
  "id_str" : "970996515425660933",
  "text" : "User Experience Is the Most Important Metric You Aren't Measuring https:\/\/t.co\/YLEYAF6zxS",
  "id" : 970996515425660933,
  "created_at" : "2018-03-06 12:16:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "NDRC",
      "screen_name" : "NDRC_hq",
      "indices" : [ 3, 11 ],
      "id_str" : "42607907",
      "id" : 42607907
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970987587677704193",
  "text" : "RT @NDRC_hq: Consumer marketplaces are perhaps second only to SaaS in terms of business models which appear most frequently among NDRC appl\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Fergus O'Dea",
        "screen_name" : "fergusodea",
        "indices" : [ 211, 222 ],
        "id_str" : "167769913",
        "id" : 167769913
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 237, 260 ],
        "url" : "https:\/\/t.co\/f31vNKQjGV",
        "expanded_url" : "http:\/\/www.ndrc.ie\/news-events\/thoughts\/the-marketplace-dilemma",
        "display_url" : "ndrc.ie\/news-events\/th\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "970980812849074176",
    "text" : "Consumer marketplaces are perhaps second only to SaaS in terms of business models which appear most frequently among NDRC applicants. It is also perhaps the business model that we invest in most infrequently.   @fergusodea explains why. https:\/\/t.co\/f31vNKQjGV",
    "id" : 970980812849074176,
    "created_at" : "2018-03-06 11:14:00 +0000",
    "user" : {
      "name" : "NDRC",
      "screen_name" : "NDRC_hq",
      "protected" : false,
      "id_str" : "42607907",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/826004993182425092\/SgcQJbNM_normal.jpg",
      "id" : 42607907,
      "verified" : true
    }
  },
  "id" : 970987587677704193,
  "created_at" : "2018-03-06 11:40:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970966340457594881",
  "text" : "What data is missing from Google Analytics for your needs? What do you use to fill that gap?",
  "id" : 970966340457594881,
  "created_at" : "2018-03-06 10:16:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/jhO0frJVJf",
      "expanded_url" : "http:\/\/gs.statcounter.com\/platform-market-share\/desktop-mobile-tablet\/ireland",
      "display_url" : "gs.statcounter.com\/platform-marke\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "970961437479915520",
  "geo" : { },
  "id_str" : "970962020731445248",
  "in_reply_to_user_id" : 9465632,
  "text" : "Figure for Ireland desktop usage is 42% https:\/\/t.co\/jhO0frJVJf",
  "id" : 970962020731445248,
  "in_reply_to_status_id" : 970961437479915520,
  "created_at" : "2018-03-06 09:59:20 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 95, 118 ],
      "url" : "https:\/\/t.co\/0SLZPbDj0B",
      "expanded_url" : "http:\/\/gs.statcounter.com\/platform-market-share\/desktop-mobile-tablet",
      "display_url" : "gs.statcounter.com\/platform-marke\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970961437479915520",
  "text" : "Despite the rise of mobile device usage, desktop still accounts for 44% of all online sessions https:\/\/t.co\/0SLZPbDj0B",
  "id" : 970961437479915520,
  "created_at" : "2018-03-06 09:57:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970960125707112449",
  "text" : "Muted Word, Muted Account and Bookmark - Twitter features I am willing to pay for it if Twitter decide to charge.",
  "id" : 970960125707112449,
  "created_at" : "2018-03-06 09:51:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Suzy Byrne",
      "screen_name" : "suzybie",
      "indices" : [ 3, 11 ],
      "id_str" : "796264",
      "id" : 796264
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970959478081425409",
  "text" : "RT @suzybie: If you know someone who needs prioritisation for water due to medical need or is otherwise a 'disabled\/older customer and need\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Water",
        "screen_name" : "IrishWater",
        "indices" : [ 146, 157 ],
        "id_str" : "1147795944",
        "id" : 1147795944
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 187, 210 ],
        "url" : "https:\/\/t.co\/GNpnPV1jlj",
        "expanded_url" : "https:\/\/www.water.ie\/support\/vulnerable-user\/",
        "display_url" : "water.ie\/support\/vulner\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "970948251607289858",
    "text" : "If you know someone who needs prioritisation for water due to medical need or is otherwise a 'disabled\/older customer and needs water delivered - @IrishWater has a registration form here https:\/\/t.co\/GNpnPV1jlj",
    "id" : 970948251607289858,
    "created_at" : "2018-03-06 09:04:37 +0000",
    "user" : {
      "name" : "Suzy Byrne",
      "screen_name" : "suzybie",
      "protected" : false,
      "id_str" : "796264",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2180218929\/36_12_23_normal.gif",
      "id" : 796264,
      "verified" : false
    }
  },
  "id" : 970959478081425409,
  "created_at" : "2018-03-06 09:49:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "indices" : [ 3, 12 ],
      "id_str" : "19456433",
      "id" : 19456433
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970953687362359296",
  "text" : "RT @jonboyes: People asking how the Russians could nip into the UK, poison their double agent and nip out again undetected. Maybe they're s\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Americans",
        "screen_name" : "TheAmericansFX",
        "indices" : [ 212, 227 ],
        "id_str" : "546447987",
        "id" : 546447987
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "sleepercells",
        "indices" : [ 241, 254 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970946030740168704",
    "text" : "People asking how the Russians could nip into the UK, poison their double agent and nip out again undetected. Maybe they're still here, maybe they're Dave your next door neighbour who works as an accountant? See @TheAmericansFX for details. #sleepercells",
    "id" : 970946030740168704,
    "created_at" : "2018-03-06 08:55:47 +0000",
    "user" : {
      "name" : "Jon Boyes",
      "screen_name" : "jonboyes",
      "protected" : false,
      "id_str" : "19456433",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3386817686\/4b1b7303154ea92ea270a5afec16da70_normal.jpeg",
      "id" : 19456433,
      "verified" : false
    }
  },
  "id" : 970953687362359296,
  "created_at" : "2018-03-06 09:26:13 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "bbcbreakfast",
      "indices" : [ 41, 54 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970949781798440960",
  "text" : "Dad pass out while wife during labour. \uD83D\uDE02\uD83D\uDE01#bbcbreakfast",
  "id" : 970949781798440960,
  "created_at" : "2018-03-06 09:10:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Carrie Gracie",
      "screen_name" : "BBCCarrie",
      "indices" : [ 3, 13 ],
      "id_str" : "120071591",
      "id" : 120071591
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "equal",
      "indices" : [ 126, 132 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970944527610601473",
  "text" : "RT @BBCCarrie: Old Chinese saying: WOMEN HOLD UP HALF THE SKY. But MEN hold up the other half. Together we'll make a fair and #equal workpl\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "equal",
        "indices" : [ 111, 117 ]
      }, {
        "text" : "bbctransparency",
        "indices" : [ 180, 196 ]
      } ],
      "urls" : [ {
        "indices" : [ 198, 221 ],
        "url" : "https:\/\/t.co\/fo9RBifjZR",
        "expanded_url" : "http:\/\/bit.ly\/2oLXYqC",
        "display_url" : "bit.ly\/2oLXYqC"
      } ]
    },
    "geo" : { },
    "id_str" : "970942640874278912",
    "text" : "Old Chinese saying: WOMEN HOLD UP HALF THE SKY. But MEN hold up the other half. Together we'll make a fair and #equal workplace. Read our open letter from hundreds of staff urging #bbctransparency. https:\/\/t.co\/fo9RBifjZR",
    "id" : 970942640874278912,
    "created_at" : "2018-03-06 08:42:19 +0000",
    "user" : {
      "name" : "Carrie Gracie",
      "screen_name" : "BBCCarrie",
      "protected" : false,
      "id_str" : "120071591",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/949609854871789569\/Kmd7ZO3h_normal.jpg",
      "id" : 120071591,
      "verified" : true
    }
  },
  "id" : 970944527610601473,
  "created_at" : "2018-03-06 08:49:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jacques Fortier",
      "screen_name" : "jacquesgt",
      "indices" : [ 3, 13 ],
      "id_str" : "16897176",
      "id" : 16897176
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/jacquesgt\/status\/970368984926232576\/photo\/1",
      "indices" : [ 85, 108 ],
      "url" : "https:\/\/t.co\/XRIa07K44G",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXdxceaVMAABTkv.jpg",
      "id_str" : "970368978735411200",
      "id" : 970368978735411200,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXdxceaVMAABTkv.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 2611,
        "resize" : "fit",
        "w" : 2611
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/XRIa07K44G"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970926681304305664",
  "text" : "RT @jacquesgt: Gotta be more careful about the reading material I leave lying around https:\/\/t.co\/XRIa07K44G",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/jacquesgt\/status\/970368984926232576\/photo\/1",
        "indices" : [ 70, 93 ],
        "url" : "https:\/\/t.co\/XRIa07K44G",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXdxceaVMAABTkv.jpg",
        "id_str" : "970368978735411200",
        "id" : 970368978735411200,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXdxceaVMAABTkv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 2611,
          "resize" : "fit",
          "w" : 2611
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/XRIa07K44G"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970368984926232576",
    "text" : "Gotta be more careful about the reading material I leave lying around https:\/\/t.co\/XRIa07K44G",
    "id" : 970368984926232576,
    "created_at" : "2018-03-04 18:42:49 +0000",
    "user" : {
      "name" : "Jacques Fortier",
      "screen_name" : "jacquesgt",
      "protected" : false,
      "id_str" : "16897176",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/805700004572778496\/uboL-mB8_normal.jpg",
      "id" : 16897176,
      "verified" : false
    }
  },
  "id" : 970926681304305664,
  "created_at" : "2018-03-06 07:38:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ChannelSight",
      "screen_name" : "ChannelSight",
      "indices" : [ 3, 16 ],
      "id_str" : "790059415",
      "id" : 790059415
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ChannelSight\/status\/970920958415589376\/photo\/1",
      "indices" : [ 98, 121 ],
      "url" : "https:\/\/t.co\/KxyUmPOqYM",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXlnd2XXkAEaP4c.jpg",
      "id_str" : "970920957182447617",
      "id" : 970920957182447617,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXlnd2XXkAEaP4c.jpg",
      "sizes" : [ {
        "h" : 824,
        "resize" : "fit",
        "w" : 1279
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 438,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 824,
        "resize" : "fit",
        "w" : 1279
      }, {
        "h" : 773,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/KxyUmPOqYM"
    } ],
    "hashtags" : [ {
      "text" : "eBay",
      "indices" : [ 18, 23 ]
    }, {
      "text" : "ecommerce",
      "indices" : [ 44, 54 ]
    }, {
      "text" : "Qoo10",
      "indices" : [ 64, 70 ]
    } ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/OFlDFbUCy8",
      "expanded_url" : "https:\/\/clclp.me\/4XTi",
      "display_url" : "clclp.me\/4XTi"
    } ]
  },
  "geo" : { },
  "id_str" : "970926584424292352",
  "text" : "RT @ChannelSight: #eBay to acquire Japanese #ecommerce platform #Qoo10.jp https:\/\/t.co\/OFlDFbUCy8 https:\/\/t.co\/KxyUmPOqYM",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.CliClap.com\" rel=\"nofollow\"\u003ECliClap\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ChannelSight\/status\/970920958415589376\/photo\/1",
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/KxyUmPOqYM",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXlnd2XXkAEaP4c.jpg",
        "id_str" : "970920957182447617",
        "id" : 970920957182447617,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXlnd2XXkAEaP4c.jpg",
        "sizes" : [ {
          "h" : 824,
          "resize" : "fit",
          "w" : 1279
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 438,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 824,
          "resize" : "fit",
          "w" : 1279
        }, {
          "h" : 773,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KxyUmPOqYM"
      } ],
      "hashtags" : [ {
        "text" : "eBay",
        "indices" : [ 0, 5 ]
      }, {
        "text" : "ecommerce",
        "indices" : [ 26, 36 ]
      }, {
        "text" : "Qoo10",
        "indices" : [ 46, 52 ]
      } ],
      "urls" : [ {
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/OFlDFbUCy8",
        "expanded_url" : "https:\/\/clclp.me\/4XTi",
        "display_url" : "clclp.me\/4XTi"
      } ]
    },
    "geo" : { },
    "id_str" : "970920958415589376",
    "text" : "#eBay to acquire Japanese #ecommerce platform #Qoo10.jp https:\/\/t.co\/OFlDFbUCy8 https:\/\/t.co\/KxyUmPOqYM",
    "id" : 970920958415589376,
    "created_at" : "2018-03-06 07:16:09 +0000",
    "user" : {
      "name" : "ChannelSight",
      "screen_name" : "ChannelSight",
      "protected" : false,
      "id_str" : "790059415",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/770660774566035456\/SZAJuLWR_normal.jpg",
      "id" : 790059415,
      "verified" : false
    }
  },
  "id" : 970926584424292352,
  "created_at" : "2018-03-06 07:38:31 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Leo Babauta",
      "screen_name" : "zen_habits",
      "indices" : [ 3, 14 ],
      "id_str" : "15859268",
      "id" : 15859268
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970804990611779584",
  "text" : "RT @zen_habits: 'Develop a mind that clings to nothing.' ~the Diamond Sutra",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "967446575642329089",
    "text" : "'Develop a mind that clings to nothing.' ~the Diamond Sutra",
    "id" : 967446575642329089,
    "created_at" : "2018-02-24 17:10:12 +0000",
    "user" : {
      "name" : "Leo Babauta",
      "screen_name" : "zen_habits",
      "protected" : false,
      "id_str" : "15859268",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/514865734816509952\/k7puRwOr_normal.jpeg",
      "id" : 15859268,
      "verified" : true
    }
  },
  "id" : 970804990611779584,
  "created_at" : "2018-03-05 23:35:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970803115585888258",
  "text" : "Over heard in a Dublin supermarket today. Woman on phone: \"There is a lot of food here\"",
  "id" : 970803115585888258,
  "created_at" : "2018-03-05 23:27:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sam Newman",
      "screen_name" : "samnewman",
      "indices" : [ 3, 13 ],
      "id_str" : "6108292",
      "id" : 6108292
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970791032605544448",
  "text" : "RT @samnewman: Dear Software Vendors. On your website, I need screenshots of your product at an absolute minimum, and ideally I want a vide\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/tapbots.com\/software\/tweetbot\/mac\" rel=\"nofollow\"\u003ETweetbot for Mac\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969947813185507329",
    "text" : "Dear Software Vendors. On your website, I need screenshots of your product at an absolute minimum, and ideally I want a video showing how it is used. Don\u2019t make me sign up for a demo to see your product.",
    "id" : 969947813185507329,
    "created_at" : "2018-03-03 14:49:14 +0000",
    "user" : {
      "name" : "Sam Newman",
      "screen_name" : "samnewman",
      "protected" : false,
      "id_str" : "6108292",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/585298659589267456\/xAPhHpWz_normal.jpg",
      "id" : 6108292,
      "verified" : true
    }
  },
  "id" : 970791032605544448,
  "created_at" : "2018-03-05 22:39:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jared Spool",
      "screen_name" : "jmspool",
      "indices" : [ 3, 11 ],
      "id_str" : "849101",
      "id" : 849101
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/YkwMKKK5W9",
      "expanded_url" : "https:\/\/twitter.com\/deray\/status\/970082827655172096",
      "display_url" : "twitter.com\/deray\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970790911910252544",
  "text" : "RT @jmspool: The way we treat people from low-income communities is reprehensible, by design. https:\/\/t.co\/YkwMKKK5W9",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 81, 104 ],
        "url" : "https:\/\/t.co\/YkwMKKK5W9",
        "expanded_url" : "https:\/\/twitter.com\/deray\/status\/970082827655172096",
        "display_url" : "twitter.com\/deray\/status\/9\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "970091187980906496",
    "text" : "The way we treat people from low-income communities is reprehensible, by design. https:\/\/t.co\/YkwMKKK5W9",
    "id" : 970091187980906496,
    "created_at" : "2018-03-04 00:18:57 +0000",
    "user" : {
      "name" : "Jared Spool",
      "screen_name" : "jmspool",
      "protected" : false,
      "id_str" : "849101",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/866315684673748993\/N_qMCtMo_normal.jpg",
      "id" : 849101,
      "verified" : false
    }
  },
  "id" : 970790911910252544,
  "created_at" : "2018-03-05 22:39:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ProductTank Dublin",
      "screen_name" : "ProductTankDub",
      "indices" : [ 3, 18 ],
      "id_str" : "763988027857174528",
      "id" : 763988027857174528
    }, {
      "name" : "Rich Mironov",
      "screen_name" : "RichMironov",
      "indices" : [ 38, 50 ],
      "id_str" : "19206111",
      "id" : 19206111
    }, {
      "name" : "ProductTank",
      "screen_name" : "producttank",
      "indices" : [ 59, 71 ],
      "id_str" : "148274843",
      "id" : 148274843
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970788269595217921",
  "text" : "RT @ProductTankDub: Delighted to have @RichMironov back to @producttank Dublin next month to talk about \"Your audiences real Roadmap questi\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Rich Mironov",
        "screen_name" : "RichMironov",
        "indices" : [ 18, 30 ],
        "id_str" : "19206111",
        "id" : 19206111
      }, {
        "name" : "ProductTank",
        "screen_name" : "producttank",
        "indices" : [ 39, 51 ],
        "id_str" : "148274843",
        "id" : 148274843
      }, {
        "name" : "HubSpot",
        "screen_name" : "HubSpot",
        "indices" : [ 135, 143 ],
        "id_str" : "14458280",
        "id" : 14458280
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 177, 200 ],
        "url" : "https:\/\/t.co\/DxP3rYAUgE",
        "expanded_url" : "http:\/\/meetu.ps\/e\/DYScd\/vpt0R\/d",
        "display_url" : "meetu.ps\/e\/DYScd\/vpt0R\/d"
      } ]
    },
    "geo" : { },
    "id_str" : "970708166567694338",
    "text" : "Delighted to have @RichMironov back to @producttank Dublin next month to talk about \"Your audiences real Roadmap questions\". Thanks to @HubSpot for hosting &amp; sponsoring     https:\/\/t.co\/DxP3rYAUgE",
    "id" : 970708166567694338,
    "created_at" : "2018-03-05 17:10:36 +0000",
    "user" : {
      "name" : "ProductTank Dublin",
      "screen_name" : "ProductTankDub",
      "protected" : false,
      "id_str" : "763988027857174528",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/786640291398815744\/RaM-4uKz_normal.jpg",
      "id" : 763988027857174528,
      "verified" : false
    }
  },
  "id" : 970788269595217921,
  "created_at" : "2018-03-05 22:28:54 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 86, 109 ],
      "url" : "https:\/\/t.co\/u8yzgcMOMb",
      "expanded_url" : "http:\/\/str.sg\/oSQx",
      "display_url" : "str.sg\/oSQx"
    } ]
  },
  "geo" : { },
  "id_str" : "970771885460922369",
  "text" : "Parliament: 500 companies placed on MOM's watchlist for unfairly favouring foreigners https:\/\/t.co\/u8yzgcMOMb",
  "id" : 970771885460922369,
  "created_at" : "2018-03-05 21:23:48 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Holly Brockwell",
      "screen_name" : "holly",
      "indices" : [ 0, 6 ],
      "id_str" : "7555262",
      "id" : 7555262
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "970712727965532162",
  "geo" : { },
  "id_str" : "970714686374187008",
  "in_reply_to_user_id" : 7555262,
  "text" : "@holly Will that attract the attention of your cat?",
  "id" : 970714686374187008,
  "in_reply_to_status_id" : 970712727965532162,
  "created_at" : "2018-03-05 17:36:30 +0000",
  "in_reply_to_screen_name" : "holly",
  "in_reply_to_user_id_str" : "7555262",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "tonyofla",
      "screen_name" : "tonyofla",
      "indices" : [ 3, 12 ],
      "id_str" : "8252502",
      "id" : 8252502
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/D0KVc33f86",
      "expanded_url" : "https:\/\/www.theguardian.com\/uk-news\/2018\/mar\/05\/trevor-baylis-inventor-wind-up-radio-dies-aged-80?CMP=Share_iOSApp_Other",
      "display_url" : "theguardian.com\/uk-news\/2018\/m\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970708565177655298",
  "text" : "RT @tonyofla: Trevor Baylis, inventor of the wind-up radio, dies aged 80 https:\/\/t.co\/D0KVc33f86",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 59, 82 ],
        "url" : "https:\/\/t.co\/D0KVc33f86",
        "expanded_url" : "https:\/\/www.theguardian.com\/uk-news\/2018\/mar\/05\/trevor-baylis-inventor-wind-up-radio-dies-aged-80?CMP=Share_iOSApp_Other",
        "display_url" : "theguardian.com\/uk-news\/2018\/m\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "970692121421377536",
    "text" : "Trevor Baylis, inventor of the wind-up radio, dies aged 80 https:\/\/t.co\/D0KVc33f86",
    "id" : 970692121421377536,
    "created_at" : "2018-03-05 16:06:50 +0000",
    "user" : {
      "name" : "tonyofla",
      "screen_name" : "tonyofla",
      "protected" : false,
      "id_str" : "8252502",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/647693385881489409\/IODazz9V_normal.png",
      "id" : 8252502,
      "verified" : false
    }
  },
  "id" : 970708565177655298,
  "created_at" : "2018-03-05 17:12:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970673313944096774",
  "text" : "What\u2019s the single, most important factor in successfully converting online prospects into paying customers?",
  "id" : 970673313944096774,
  "created_at" : "2018-03-05 14:52:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adam Grant",
      "screen_name" : "AdamMGrant",
      "indices" : [ 3, 14 ],
      "id_str" : "1059273780",
      "id" : 1059273780
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970666091214188548",
  "text" : "RT @AdamMGrant: United Airlines just replaced quarterly performance bonuses for employees with a lottery for a small number of large prizes\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 212, 235 ],
        "url" : "https:\/\/t.co\/4kP43akQw1",
        "expanded_url" : "https:\/\/www.linkedin.com\/pulse\/dear-united-airlines-motivate-people-dont-do-adam-grant",
        "display_url" : "linkedin.com\/pulse\/dear-uni\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "970646576027918336",
    "text" : "United Airlines just replaced quarterly performance bonuses for employees with a lottery for a small number of large prizes.\nRewards shouldn't be given out at random. Work isn\u2019t a game show. It\u2019s our livelihood.\nhttps:\/\/t.co\/4kP43akQw1",
    "id" : 970646576027918336,
    "created_at" : "2018-03-05 13:05:52 +0000",
    "user" : {
      "name" : "Adam Grant",
      "screen_name" : "AdamMGrant",
      "protected" : false,
      "id_str" : "1059273780",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/657898413913083904\/U0uvDqz5_normal.jpg",
      "id" : 1059273780,
      "verified" : true
    }
  },
  "id" : 970666091214188548,
  "created_at" : "2018-03-05 14:23:24 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jobcare",
      "screen_name" : "JobcareDublin",
      "indices" : [ 3, 17 ],
      "id_str" : "61151320",
      "id" : 61151320
    }, {
      "name" : "Jobcare",
      "screen_name" : "JobcareDublin",
      "indices" : [ 75, 89 ],
      "id_str" : "61151320",
      "id" : 61151320
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970634050900840448",
  "text" : "RT @JobcareDublin: Welcome back! The snow is thawing &amp; we're delighted @JobcareDublin's doors are open again &amp; we're back to normal! We're\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Jobcare",
        "screen_name" : "JobcareDublin",
        "indices" : [ 56, 70 ],
        "id_str" : "61151320",
        "id" : 61151320
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/JobcareDublin\/status\/970628871086313472\/photo\/1",
        "indices" : [ 288, 311 ],
        "url" : "https:\/\/t.co\/IXJP8uQsKg",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXhdTKZXUAEjG07.jpg",
        "id_str" : "970628303487979521",
        "id" : 970628303487979521,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXhdTKZXUAEjG07.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 788,
          "resize" : "fit",
          "w" : 940
        }, {
          "h" : 570,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 788,
          "resize" : "fit",
          "w" : 940
        }, {
          "h" : 788,
          "resize" : "fit",
          "w" : 940
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/IXJP8uQsKg"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970628871086313472",
    "text" : "Welcome back! The snow is thawing &amp; we're delighted @JobcareDublin's doors are open again &amp; we're back to normal! We're looking forward to Spring &amp; the new season. If you're unemployed &amp; would like some help &amp; support in your jobsearch, get in touch! Working Matters! https:\/\/t.co\/IXJP8uQsKg",
    "id" : 970628871086313472,
    "created_at" : "2018-03-05 11:55:30 +0000",
    "user" : {
      "name" : "Jobcare",
      "screen_name" : "JobcareDublin",
      "protected" : false,
      "id_str" : "61151320",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2934994273\/1913d7c9b15f8b66e3a1c34a66369cee_normal.jpeg",
      "id" : 61151320,
      "verified" : false
    }
  },
  "id" : 970634050900840448,
  "created_at" : "2018-03-05 12:16:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Simon Carswell",
      "screen_name" : "SiCarswell",
      "indices" : [ 3, 14 ],
      "id_str" : "150224923",
      "id" : 150224923
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970566821169500160",
  "text" : "RT @SiCarswell: Atlantic quietly closes its doors in Ireland after 30 years of philanthropy - historic act of extraordinary generosity come\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "The Irish Times",
        "screen_name" : "IrishTimes",
        "indices" : [ 139, 150 ],
        "id_str" : "15084853",
        "id" : 15084853
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 151, 174 ],
        "url" : "https:\/\/t.co\/ihTH5531oS",
        "expanded_url" : "https:\/\/www.irishtimes.com\/news\/ireland\/irish-news\/atlantic-quietly-closes-its-doors-in-ireland-after-30-years-of-philanthropy-1.3412071",
        "display_url" : "irishtimes.com\/news\/ireland\/i\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969839901276590081",
    "text" : "Atlantic quietly closes its doors in Ireland after 30 years of philanthropy - historic act of extraordinary generosity comes to an end\nvia @IrishTimes\nhttps:\/\/t.co\/ihTH5531oS",
    "id" : 969839901276590081,
    "created_at" : "2018-03-03 07:40:25 +0000",
    "user" : {
      "name" : "Simon Carswell",
      "screen_name" : "SiCarswell",
      "protected" : false,
      "id_str" : "150224923",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/829820657932312576\/QhXaax5q_normal.jpg",
      "id" : 150224923,
      "verified" : true
    }
  },
  "id" : 970566821169500160,
  "created_at" : "2018-03-05 07:48:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Edwin Hernandez",
      "screen_name" : "boi",
      "indices" : [ 32, 36 ],
      "id_str" : "7333262",
      "id" : 7333262
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 75, 98 ],
      "url" : "https:\/\/t.co\/w1Srn32UHy",
      "expanded_url" : "https:\/\/events.bizzabo.com\/207799?share=true",
      "display_url" : "events.bizzabo.com\/207799?share=t\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970564161355894784",
  "text" : "I registered for Data Analytics @BOI - The Inside Track. Check it out here https:\/\/t.co\/w1Srn32UHy",
  "id" : 970564161355894784,
  "created_at" : "2018-03-05 07:38:22 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "(((Jon Benjamin)))",
      "screen_name" : "JonBenjamin19",
      "indices" : [ 3, 17 ],
      "id_str" : "215435356",
      "id" : 215435356
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970556938948882432",
  "text" : "RT @JonBenjamin19: Best ever Oscars quote? \n\n\u201CSo tonight, enjoy yourselves because nothing can take the sting out of the world's economic p\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970393010528759813",
    "text" : "Best ever Oscars quote? \n\n\u201CSo tonight, enjoy yourselves because nothing can take the sting out of the world's economic problems like watching millionaires present each other with small golden statues.\u201D - Billy Crystal",
    "id" : 970393010528759813,
    "created_at" : "2018-03-04 20:18:17 +0000",
    "user" : {
      "name" : "(((Jon Benjamin)))",
      "screen_name" : "JonBenjamin19",
      "protected" : false,
      "id_str" : "215435356",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1028044714686201856\/ORuEu-fo_normal.jpg",
      "id" : 215435356,
      "verified" : true
    }
  },
  "id" : 970556938948882432,
  "created_at" : "2018-03-05 07:09:40 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jon Tennant",
      "screen_name" : "Protohedgehog",
      "indices" : [ 3, 17 ],
      "id_str" : "352650591",
      "id" : 352650591
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970556750293274624",
  "text" : "RT @Protohedgehog: Remember: The reason why Wiley, Elsevier, SpringerNature and co can achieve 37%+ profit margins is because they do not p\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969995220854198273",
    "text" : "Remember: The reason why Wiley, Elsevier, SpringerNature and co can achieve 37%+ profit margins is because they do not pay authors or reviewers for their free labour\/content. A profoundly exploitative practice draining away much needed resources.",
    "id" : 969995220854198273,
    "created_at" : "2018-03-03 17:57:36 +0000",
    "user" : {
      "name" : "Jon Tennant",
      "screen_name" : "Protohedgehog",
      "protected" : false,
      "id_str" : "352650591",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/983966406873092096\/fcMvB7d1_normal.jpg",
      "id" : 352650591,
      "verified" : true
    }
  },
  "id" : 970556750293274624,
  "created_at" : "2018-03-05 07:08:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/wyZUVhvZoR",
      "expanded_url" : "https:\/\/github.com\/mryap\/MyDockerCompose\/blob\/master\/database\/docker-compose.yml",
      "display_url" : "github.com\/mryap\/MyDocker\u2026"
    } ]
  },
  "in_reply_to_status_id_str" : "970459088273461249",
  "geo" : { },
  "id_str" : "970465223747522560",
  "in_reply_to_user_id" : 9465632,
  "text" : "The Docker compose file that make it possible is at https:\/\/t.co\/wyZUVhvZoR",
  "id" : 970465223747522560,
  "in_reply_to_status_id" : 970459088273461249,
  "created_at" : "2018-03-05 01:05:14 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 25, 48 ],
      "url" : "https:\/\/t.co\/nntgdivbem",
      "expanded_url" : "https:\/\/twitter.com\/notetoshelf\/status\/970457118699646976",
      "display_url" : "twitter.com\/notetoshelf\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970459088273461249",
  "text" : "Achievement unlock. \uD83D\uDE4C\uD83C\uDF96\uFE0F\uD83C\uDF89 https:\/\/t.co\/nntgdivbem",
  "id" : 970459088273461249,
  "created_at" : "2018-03-05 00:40:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/970401368396713989\/photo\/1",
      "indices" : [ 213, 236 ],
      "url" : "https:\/\/t.co\/7SFoTpKqtv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXeOmTWX4AAa9Rl.jpg",
      "id_str" : "970401033401917440",
      "id" : 970401033401917440,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXeOmTWX4AAa9Rl.jpg",
      "sizes" : [ {
        "h" : 344,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 659,
        "resize" : "fit",
        "w" : 1302
      }, {
        "h" : 659,
        "resize" : "fit",
        "w" : 1302
      }, {
        "h" : 607,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/7SFoTpKqtv"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/F10rC0Mzal",
      "expanded_url" : "https:\/\/registry-1.docker.io\/v2\/dockette\/adminer\/manifests\/latest",
      "display_url" : "registry-1.docker.io\/v2\/dockette\/ad\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970401368396713989",
  "text" : "Error response from daemon: Get https:\/\/t.co\/F10rC0Mzal: unauthorized: incorrect username or password \n\nI have problem with the Docker pull command now. The set-up appears to be working fine. \n\nAny advice? Thanks https:\/\/t.co\/7SFoTpKqtv",
  "id" : 970401368396713989,
  "created_at" : "2018-03-04 20:51:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970395059123302401",
  "text" : "So school reopen only for 3rd and 6th year students only. Another off day for him...\uD83D\uDE10",
  "id" : 970395059123302401,
  "created_at" : "2018-03-04 20:26:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/SHueoZlsxw",
      "expanded_url" : "https:\/\/atlassian.design\/",
      "display_url" : "atlassian.design"
    } ]
  },
  "geo" : { },
  "id_str" : "970370952335888384",
  "text" : "Atlassian Design Guidelines  - combines disciplines of design, development, content, illustration.  https:\/\/t.co\/SHueoZlsxw",
  "id" : 970370952335888384,
  "created_at" : "2018-03-04 18:50:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Matt Andrews",
      "screen_name" : "mattpointblank",
      "indices" : [ 3, 18 ],
      "id_str" : "14132114",
      "id" : 14132114
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970335214730956800",
  "text" : "RT @mattpointblank: If you want to know what the definitive worst company culture is, look at this startup's recruitment page: https:\/\/t.co\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Steve Jalim",
        "screen_name" : "stevejalim",
        "indices" : [ 136, 147 ],
        "id_str" : "9036362",
        "id" : 9036362
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 107, 130 ],
        "url" : "https:\/\/t.co\/pOp1dAF9ic",
        "expanded_url" : "https:\/\/www.healthiq.com\/careers",
        "display_url" : "healthiq.com\/careers"
      } ]
    },
    "geo" : { },
    "id_str" : "969515360637595648",
    "text" : "If you want to know what the definitive worst company culture is, look at this startup's recruitment page: https:\/\/t.co\/pOp1dAF9ic (via @stevejalim)",
    "id" : 969515360637595648,
    "created_at" : "2018-03-02 10:10:49 +0000",
    "user" : {
      "name" : "Matt Andrews",
      "screen_name" : "mattpointblank",
      "protected" : false,
      "id_str" : "14132114",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1008462093430022145\/PnMymxQk_normal.jpg",
      "id" : 14132114,
      "verified" : false
    }
  },
  "id" : 970335214730956800,
  "created_at" : "2018-03-04 16:28:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "stormemma",
      "indices" : [ 37, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970329024219279361",
  "text" : "A lot of High 5 and Pat on the back  #stormemma",
  "id" : 970329024219279361,
  "created_at" : "2018-03-04 16:04:01 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970320120353607680",
  "text" : "RT @getoptimise: Moore highlights the chasm that exists between early adopters of a product &amp; early majority. Crossing the chasm can be ach\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "970318119746129920",
    "text" : "Moore highlights the chasm that exists between early adopters of a product &amp; early majority. Crossing the chasm can be achieved by understanding how to secure a specific niche as a beachhead first.",
    "id" : 970318119746129920,
    "created_at" : "2018-03-04 15:20:42 +0000",
    "user" : {
      "name" : "Your Partner for Analytics",
      "screen_name" : "hireyap",
      "protected" : false,
      "id_str" : "1000417794",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1025794423848951808\/NcuN9sxU_normal.jpg",
      "id" : 1000417794,
      "verified" : false
    }
  },
  "id" : 970320120353607680,
  "created_at" : "2018-03-04 15:28:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Michael Tegos",
      "screen_name" : "michaeltegos",
      "indices" : [ 3, 16 ],
      "id_str" : "183638997",
      "id" : 183638997
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 94, 117 ],
      "url" : "https:\/\/t.co\/3vgDI5NNiI",
      "expanded_url" : "https:\/\/twitter.com\/dtiffroberts\/status\/969871931544612864",
      "display_url" : "twitter.com\/dtiffroberts\/s\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970305962597810177",
  "text" : "RT @michaeltegos: Yikes, I'll never complain about stray Ofos and Mobikes in Singapore again. https:\/\/t.co\/3vgDI5NNiI",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 76, 99 ],
        "url" : "https:\/\/t.co\/3vgDI5NNiI",
        "expanded_url" : "https:\/\/twitter.com\/dtiffroberts\/status\/969871931544612864",
        "display_url" : "twitter.com\/dtiffroberts\/s\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "970225363421839360",
    "text" : "Yikes, I'll never complain about stray Ofos and Mobikes in Singapore again. https:\/\/t.co\/3vgDI5NNiI",
    "id" : 970225363421839360,
    "created_at" : "2018-03-04 09:12:07 +0000",
    "user" : {
      "name" : "Michael Tegos",
      "screen_name" : "michaeltegos",
      "protected" : false,
      "id_str" : "183638997",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1304352222\/frankavatar_normal.jpg",
      "id" : 183638997,
      "verified" : false
    }
  },
  "id" : 970305962597810177,
  "created_at" : "2018-03-04 14:32:23 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ross McLoughlin",
      "screen_name" : "Dotnetster",
      "indices" : [ 0, 11 ],
      "id_str" : "26903787",
      "id" : 26903787
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "970251638941331456",
  "geo" : { },
  "id_str" : "970252817221988354",
  "in_reply_to_user_id" : 26903787,
  "text" : "@Dotnetster Thanks. I came across on Mobile few days back but it just not available on desktop for the same account.",
  "id" : 970252817221988354,
  "in_reply_to_status_id" : 970251638941331456,
  "created_at" : "2018-03-04 11:01:12 +0000",
  "in_reply_to_screen_name" : "Dotnetster",
  "in_reply_to_user_id_str" : "26903787",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "enormous",
      "screen_name" : "enormous",
      "indices" : [ 0, 9 ],
      "id_str" : "10410242",
      "id" : 10410242
    }, {
      "name" : "Vinnie Meyler",
      "screen_name" : "themeyler",
      "indices" : [ 10, 20 ],
      "id_str" : "75879359",
      "id" : 75879359
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "970248491044933632",
  "geo" : { },
  "id_str" : "970251184547287040",
  "in_reply_to_user_id" : 10410242,
  "text" : "@enormous @themeyler \uD83D\uDE04",
  "id" : 970251184547287040,
  "in_reply_to_status_id" : 970248491044933632,
  "created_at" : "2018-03-04 10:54:43 +0000",
  "in_reply_to_screen_name" : "enormous",
  "in_reply_to_user_id_str" : "10410242",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970245649961816064",
  "text" : "This new Bookmarks feature on Twitter must be buried dig inside those snow drift. I still can't find it on my account.",
  "id" : 970245649961816064,
  "created_at" : "2018-03-04 10:32:43 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Christopher Hope",
      "screen_name" : "christopherhope",
      "indices" : [ 0, 16 ],
      "id_str" : "22776133",
      "id" : 22776133
    }, {
      "name" : "Simon Coveney",
      "screen_name" : "simoncoveney",
      "indices" : [ 17, 30 ],
      "id_str" : "118999126",
      "id" : 118999126
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/970243607100784640\/photo\/1",
      "indices" : [ 32, 55 ],
      "url" : "https:\/\/t.co\/95g02zZOWn",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXb_ab2WkAAv-TJ.jpg",
      "id_str" : "970243599362330624",
      "id" : 970243599362330624,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXb_ab2WkAAv-TJ.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 752
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 752
      }, {
        "h" : 500,
        "resize" : "fit",
        "w" : 752
      }, {
        "h" : 452,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/95g02zZOWn"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "970228949421879296",
  "geo" : { },
  "id_str" : "970243607100784640",
  "in_reply_to_user_id" : 22776133,
  "text" : "@christopherhope @simoncoveney  https:\/\/t.co\/95g02zZOWn",
  "id" : 970243607100784640,
  "in_reply_to_status_id" : 970228949421879296,
  "created_at" : "2018-03-04 10:24:36 +0000",
  "in_reply_to_screen_name" : "christopherhope",
  "in_reply_to_user_id_str" : "22776133",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Eoin Kelleher",
      "screen_name" : "eoinyk",
      "indices" : [ 3, 10 ],
      "id_str" : "15207657",
      "id" : 15207657
    }, {
      "name" : "Christopher Hope",
      "screen_name" : "christopherhope",
      "indices" : [ 12, 28 ],
      "id_str" : "22776133",
      "id" : 22776133
    }, {
      "name" : "Simon Coveney",
      "screen_name" : "simoncoveney",
      "indices" : [ 29, 42 ],
      "id_str" : "118999126",
      "id" : 118999126
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970240427541630976",
  "text" : "RT @eoinyk: @christopherhope @simoncoveney There is no \"British Isles free trade area\" you fantasist. And even if there was, why would Irel\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Christopher Hope",
        "screen_name" : "christopherhope",
        "indices" : [ 0, 16 ],
        "id_str" : "22776133",
        "id" : 22776133
      }, {
        "name" : "Simon Coveney",
        "screen_name" : "simoncoveney",
        "indices" : [ 17, 30 ],
        "id_str" : "118999126",
        "id" : 118999126
      } ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "in_reply_to_status_id_str" : "970228949421879296",
    "geo" : { },
    "id_str" : "970235803417956358",
    "in_reply_to_user_id" : 22776133,
    "text" : "@christopherhope @simoncoveney There is no \"British Isles free trade area\" you fantasist. And even if there was, why would Ireland want to give up its EU membership (with 500m people) for a much smaller free trade area with Britain only. Use your head",
    "id" : 970235803417956358,
    "in_reply_to_status_id" : 970228949421879296,
    "created_at" : "2018-03-04 09:53:36 +0000",
    "in_reply_to_screen_name" : "christopherhope",
    "in_reply_to_user_id_str" : "22776133",
    "user" : {
      "name" : "Eoin Kelleher",
      "screen_name" : "eoinyk",
      "protected" : false,
      "id_str" : "15207657",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/881309073316216832\/_nV5-oVd_normal.jpg",
      "id" : 15207657,
      "verified" : false
    }
  },
  "id" : 970240427541630976,
  "created_at" : "2018-03-04 10:11:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970079363139022849",
  "text" : "Watched Life's a breeze on RTE 2. Enjoyed it!",
  "id" : 970079363139022849,
  "created_at" : "2018-03-03 23:31:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gianni Ponzi",
      "screen_name" : "gianniponzi",
      "indices" : [ 0, 12 ],
      "id_str" : "19793936",
      "id" : 19793936
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "970034996047175686",
  "geo" : { },
  "id_str" : "970035898011279361",
  "in_reply_to_user_id" : 19793936,
  "text" : "@gianniponzi How long is the power outage?",
  "id" : 970035898011279361,
  "in_reply_to_status_id" : 970034996047175686,
  "created_at" : "2018-03-03 20:39:15 +0000",
  "in_reply_to_screen_name" : "gianniponzi",
  "in_reply_to_user_id_str" : "19793936",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Arshia",
      "screen_name" : "arshia__",
      "indices" : [ 3, 12 ],
      "id_str" : "562291920",
      "id" : 562291920
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970024213959663618",
  "text" : "RT @arshia__: DEVS: documentation isn't important! also it's easy!\nMANAGERS: documentation won't get you promoted!\nINSTRUCTORS: documentati\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969715729862836224",
    "text" : "DEVS: documentation isn't important! also it's easy!\nMANAGERS: documentation won't get you promoted!\nINSTRUCTORS: documentation won't get you marks!\n\nwhen in reality the past 3 technical problems I've had to deal with have been because of bad or no documentation \uD83D\uDE10",
    "id" : 969715729862836224,
    "created_at" : "2018-03-02 23:27:01 +0000",
    "user" : {
      "name" : "Arshia",
      "screen_name" : "arshia__",
      "protected" : false,
      "id_str" : "562291920",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1023032200487481345\/UiRrZjuc_normal.jpg",
      "id" : 562291920,
      "verified" : false
    }
  },
  "id" : 970024213959663618,
  "created_at" : "2018-03-03 19:52:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "asciinema",
      "screen_name" : "asciinema",
      "indices" : [ 10, 20 ],
      "id_str" : "492583995",
      "id" : 492583995
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/GdgT8tbwRn",
      "expanded_url" : "https:\/\/asciinema.org\/a\/QdZIIaLI9J1Qh1PogBU7iRwMx",
      "display_url" : "asciinema.org\/a\/QdZIIaLI9J1Q\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "970019584307757056",
  "text" : "I running @asciinema in Docker container https:\/\/t.co\/GdgT8tbwRn but I unable to navigate to a particular folder on my PC. Any advice? Thanks",
  "id" : 970019584307757056,
  "created_at" : "2018-03-03 19:34:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "970011991665528833",
  "text" : "Great to see volunteers with 4 x 4 vehicles pitching in during this weather.  What if 4 x  4 vehicles dealer jump into the fray? (With car dealers branding) Will this form of marketing works? I want to know.",
  "id" : 970011991665528833,
  "created_at" : "2018-03-03 19:04:15 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "ian bremmer",
      "screen_name" : "ianbremmer",
      "indices" : [ 3, 14 ],
      "id_str" : "60783724",
      "id" : 60783724
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/ianbremmer\/status\/963210820653023233\/photo\/1",
      "indices" : [ 114, 137 ],
      "url" : "https:\/\/t.co\/niGXVvF4m4",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DV4DIgIX4AAbWn8.jpg",
      "id_str" : "963210814902755328",
      "id" : 963210814902755328,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV4DIgIX4AAbWn8.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 439,
        "resize" : "fit",
        "w" : 660
      }, {
        "h" : 439,
        "resize" : "fit",
        "w" : 660
      }, {
        "h" : 439,
        "resize" : "fit",
        "w" : 660
      }, {
        "h" : 439,
        "resize" : "fit",
        "w" : 660
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/niGXVvF4m4"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969987875331862528",
  "text" : "RT @ianbremmer: Chinese police, wearing sunglasses with facial-recognition technology, welcome you to the Matrix. https:\/\/t.co\/niGXVvF4m4",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/ianbremmer\/status\/963210820653023233\/photo\/1",
        "indices" : [ 98, 121 ],
        "url" : "https:\/\/t.co\/niGXVvF4m4",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DV4DIgIX4AAbWn8.jpg",
        "id_str" : "963210814902755328",
        "id" : 963210814902755328,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DV4DIgIX4AAbWn8.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 439,
          "resize" : "fit",
          "w" : 660
        }, {
          "h" : 439,
          "resize" : "fit",
          "w" : 660
        }, {
          "h" : 439,
          "resize" : "fit",
          "w" : 660
        }, {
          "h" : 439,
          "resize" : "fit",
          "w" : 660
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/niGXVvF4m4"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "963210820653023233",
    "text" : "Chinese police, wearing sunglasses with facial-recognition technology, welcome you to the Matrix. https:\/\/t.co\/niGXVvF4m4",
    "id" : 963210820653023233,
    "created_at" : "2018-02-13 00:38:49 +0000",
    "user" : {
      "name" : "ian bremmer",
      "screen_name" : "ianbremmer",
      "protected" : false,
      "id_str" : "60783724",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1003710089927319552\/MvLp0ONJ_normal.jpg",
      "id" : 60783724,
      "verified" : true
    }
  },
  "id" : 969987875331862528,
  "created_at" : "2018-03-03 17:28:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "VeryBritishProblems",
      "screen_name" : "SoVeryBritish",
      "indices" : [ 3, 17 ],
      "id_str" : "1023072078",
      "id" : 1023072078
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969987693651382272",
  "text" : "RT @SoVeryBritish: Phrases that mean nothing will happen:\nLeave it with me\nI\u2019ll have a word\nI\u2019ll see what I can find\nConsider it done\nI\u2019ll\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "953596645429661697",
    "text" : "Phrases that mean nothing will happen:\nLeave it with me\nI\u2019ll have a word\nI\u2019ll see what I can find\nConsider it done\nI\u2019ll make some calls\nI\u2019ll think about it\nCertainly a possibility\nLet\u2019s come back to that\nGood idea\nMaybe\nIt\u2019s on my list\nMight see you down there\nI\u2019ll look into it",
    "id" : 953596645429661697,
    "created_at" : "2018-01-17 11:55:31 +0000",
    "user" : {
      "name" : "VeryBritishProblems",
      "screen_name" : "SoVeryBritish",
      "protected" : false,
      "id_str" : "1023072078",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2996456104\/b707959f192bba5c31c07058f91a183b_normal.png",
      "id" : 1023072078,
      "verified" : true
    }
  },
  "id" : 969987693651382272,
  "created_at" : "2018-03-03 17:27:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Muhammad Nawaz",
      "screen_name" : "MuhammaddNawaz",
      "indices" : [ 3, 18 ],
      "id_str" : "946197562809376770",
      "id" : 946197562809376770
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969983788674625539",
  "text" : "RT @MuhammaddNawaz: Heart broken,photo of 4 years child who is migrating from Syria to Jordan, the UNHCR team caught him when he was migrat\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/MuhammaddNawaz\/status\/969222357830205441\/photo\/1",
        "indices" : [ 277, 300 ],
        "url" : "https:\/\/t.co\/eyLuj5M0uE",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXNeiZqX4AA04wh.jpg",
        "id_str" : "969222289911963648",
        "id" : 969222289911963648,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXNeiZqX4AA04wh.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 640,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 453,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/eyLuj5M0uE"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969222357830205441",
    "text" : "Heart broken,photo of 4 years child who is migrating from Syria to Jordan, the UNHCR team caught him when he was migrating alone in desert from Syria to Jordan. \nthe only thing he had in his shopper were the clothes of his mom and sister,who killed in Syria.\ni cried too much. https:\/\/t.co\/eyLuj5M0uE",
    "id" : 969222357830205441,
    "created_at" : "2018-03-01 14:46:32 +0000",
    "user" : {
      "name" : "Muhammad Nawaz",
      "screen_name" : "MuhammaddNawaz",
      "protected" : false,
      "id_str" : "946197562809376770",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/946386363611582464\/KDw3KgrM_normal.jpg",
      "id" : 946197562809376770,
      "verified" : false
    }
  },
  "id" : 969983788674625539,
  "created_at" : "2018-03-03 17:12:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "P\u00E1draig Belton",
      "screen_name" : "PadraigBelton",
      "indices" : [ 3, 17 ],
      "id_str" : "14671775",
      "id" : 14671775
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 22, 45 ],
      "url" : "https:\/\/t.co\/xqKuQ42KER",
      "expanded_url" : "https:\/\/twitter.com\/naheednazir\/status\/969289549498155014",
      "display_url" : "twitter.com\/naheednazir\/st\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969981796829065216",
  "text" : "RT @PadraigBelton: \u2764\uFE0F https:\/\/t.co\/xqKuQ42KER",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 3, 26 ],
        "url" : "https:\/\/t.co\/xqKuQ42KER",
        "expanded_url" : "https:\/\/twitter.com\/naheednazir\/status\/969289549498155014",
        "display_url" : "twitter.com\/naheednazir\/st\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969949611396861952",
    "text" : "\u2764\uFE0F https:\/\/t.co\/xqKuQ42KER",
    "id" : 969949611396861952,
    "created_at" : "2018-03-03 14:56:22 +0000",
    "user" : {
      "name" : "P\u00E1draig Belton",
      "screen_name" : "PadraigBelton",
      "protected" : false,
      "id_str" : "14671775",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/53815024\/patrick-belton_normal.jpg",
      "id" : 14671775,
      "verified" : false
    }
  },
  "id" : 969981796829065216,
  "created_at" : "2018-03-03 17:04:16 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 3, 26 ],
      "url" : "https:\/\/t.co\/OjbOAm6JSG",
      "expanded_url" : "https:\/\/twitter.com\/mrbrown\/status\/969463766353035265",
      "display_url" : "twitter.com\/mrbrown\/status\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969981517119328258",
  "text" : "\uD83D\uDE02\uD83D\uDE2C https:\/\/t.co\/OjbOAm6JSG",
  "id" : 969981517119328258,
  "created_at" : "2018-03-03 17:03:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Scott Lynch",
      "screen_name" : "scottlynch78",
      "indices" : [ 3, 16 ],
      "id_str" : "61838384",
      "id" : 61838384
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969978253938225152",
  "text" : "RT @scottlynch78: Hi, folks. Just a quick note from someone who answered a lot of carbon monoxide calls in my years as a firefighter-- chec\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "949124259191906304",
    "text" : "Hi, folks. Just a quick note from someone who answered a lot of carbon monoxide calls in my years as a firefighter-- check up on your surroundings after a heavy snowfall or a hard freeze. Check your ventilation, your chimneys, your furnaces. Pay attention to feeling weird.",
    "id" : 949124259191906304,
    "created_at" : "2018-01-05 03:43:51 +0000",
    "user" : {
      "name" : "Scott Lynch",
      "screen_name" : "scottlynch78",
      "protected" : false,
      "id_str" : "61838384",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/560846955966763008\/wBJ7Gv6y_normal.png",
      "id" : 61838384,
      "verified" : true
    }
  },
  "id" : 969978253938225152,
  "created_at" : "2018-03-03 16:50:11 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MIT Technology Review",
      "screen_name" : "techreview",
      "indices" : [ 3, 14 ],
      "id_str" : "15808647",
      "id" : 15808647
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969975519876919296",
  "text" : "RT @techreview: Lawyers have used similar methods to ply their trade for hundreds of years, but they\u2019d better watch out, because artificial\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/www.socialflow.com\" rel=\"nofollow\"\u003ESocialFlow\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 164, 187 ],
        "url" : "https:\/\/t.co\/RKVKEmNtEL",
        "expanded_url" : "https:\/\/trib.al\/ccX663B",
        "display_url" : "trib.al\/ccX663B"
      } ]
    },
    "geo" : { },
    "id_str" : "952703772463173632",
    "text" : "Lawyers have used similar methods to ply their trade for hundreds of years, but they\u2019d better watch out, because artificial intelligence is moving in on the field. https:\/\/t.co\/RKVKEmNtEL",
    "id" : 952703772463173632,
    "created_at" : "2018-01-15 00:47:34 +0000",
    "user" : {
      "name" : "MIT Technology Review",
      "screen_name" : "techreview",
      "protected" : false,
      "id_str" : "15808647",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875400785294532608\/feOza3es_normal.jpg",
      "id" : 15808647,
      "verified" : true
    }
  },
  "id" : 969975519876919296,
  "created_at" : "2018-03-03 16:39:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Clive Thompson",
      "screen_name" : "pomeranian99",
      "indices" : [ 3, 16 ],
      "id_str" : "661403",
      "id" : 661403
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969968334396588035",
  "text" : "RT @pomeranian99: When the NRA holds its annual convention \u2014 and 80,000 of its members are in a conference center instead of at home with t\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/tapbots.com\/tweetbot\" rel=\"nofollow\"\u003ETweetbot for i\u039FS\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 182, 205 ],
        "url" : "https:\/\/t.co\/uvrMsCbnaB",
        "expanded_url" : "https:\/\/www.scientificamerican.com\/article\/all-talk-no-bolt-action-gun-injuries-drop-during-nra-conventions\/",
        "display_url" : "scientificamerican.com\/article\/all-ta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969559133987966976",
    "text" : "When the NRA holds its annual convention \u2014 and 80,000 of its members are in a conference center instead of at home with their guns \u2014 the US gun-injury rate temporarily drops by 20%: https:\/\/t.co\/uvrMsCbnaB",
    "id" : 969559133987966976,
    "created_at" : "2018-03-02 13:04:45 +0000",
    "user" : {
      "name" : "Clive Thompson",
      "screen_name" : "pomeranian99",
      "protected" : false,
      "id_str" : "661403",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/378800000407595163\/163d76384315dfd651daa13e44b724e4_normal.jpeg",
      "id" : 661403,
      "verified" : true
    }
  },
  "id" : 969968334396588035,
  "created_at" : "2018-03-03 16:10:46 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Niamh Redmond",
      "screen_name" : "nredmond",
      "indices" : [ 3, 12 ],
      "id_str" : "8724262",
      "id" : 8724262
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969966967481389056",
  "text" : "RT @nredmond: \u201CIt seems that most Western startups are focused on \u201Cassisted living for rich hipsters\u201D rather than solving some of the \u201Cbig\u201D\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 200, 223 ],
        "url" : "https:\/\/t.co\/2i4CrMrtfD",
        "expanded_url" : "http:\/\/tcrn.ch\/28UJ8TF",
        "display_url" : "tcrn.ch\/28UJ8TF"
      } ]
    },
    "geo" : { },
    "id_str" : "954215835697954817",
    "text" : "\u201CIt seems that most Western startups are focused on \u201Cassisted living for rich hipsters\u201D rather than solving some of the \u201Cbig\u201D needs the world is facing.\u201D\n\nThe opportunities Silicon Valley doesn\u2019t see https:\/\/t.co\/2i4CrMrtfD",
    "id" : 954215835697954817,
    "created_at" : "2018-01-19 04:55:58 +0000",
    "user" : {
      "name" : "Niamh Redmond",
      "screen_name" : "nredmond",
      "protected" : false,
      "id_str" : "8724262",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/985669828382896128\/KnzyBdVG_normal.jpg",
      "id" : 8724262,
      "verified" : false
    }
  },
  "id" : 969966967481389056,
  "created_at" : "2018-03-03 16:05:20 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Jennifer Swaine",
      "screen_name" : "jenswaine",
      "indices" : [ 0, 10 ],
      "id_str" : "25050225",
      "id" : 25050225
    }, {
      "name" : "frandango",
      "screen_name" : "fran_lawlor",
      "indices" : [ 11, 23 ],
      "id_str" : "39656444",
      "id" : 39656444
    }, {
      "name" : "Niall Harbison",
      "screen_name" : "NiallHarbison",
      "indices" : [ 24, 38 ],
      "id_str" : "14613723",
      "id" : 14613723
    }, {
      "name" : "Glissed",
      "screen_name" : "Glissed_",
      "indices" : [ 48, 57 ],
      "id_str" : "2794392280",
      "id" : 2794392280
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/xRXPzQc1PI",
      "expanded_url" : "http:\/\/www.glissed.com\/professional",
      "display_url" : "glissed.com\/professional"
    } ]
  },
  "in_reply_to_status_id_str" : "969124197560668160",
  "geo" : { },
  "id_str" : "969944193715011584",
  "in_reply_to_user_id" : 25050225,
  "text" : "@jenswaine @fran_lawlor @NiallHarbison Checkout @Glissed_  https:\/\/t.co\/xRXPzQc1PI",
  "id" : 969944193715011584,
  "in_reply_to_status_id" : 969124197560668160,
  "created_at" : "2018-03-03 14:34:51 +0000",
  "in_reply_to_screen_name" : "jenswaine",
  "in_reply_to_user_id_str" : "25050225",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/InSsWllf44",
      "expanded_url" : "https:\/\/twitter.com\/etiennerouzeaud\/status\/969568641212612609",
      "display_url" : "twitter.com\/etiennerouzeau\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969933838792232960",
  "text" : "Adminer as front end for Postgres, MySQL, MongoDB in a Docker environment https:\/\/t.co\/InSsWllf44",
  "id" : 969933838792232960,
  "created_at" : "2018-03-03 13:53:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "cliffski",
      "screen_name" : "cliffski",
      "indices" : [ 3, 12 ],
      "id_str" : "15218212",
      "id" : 15218212
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/cliffski\/status\/969569229149212672\/photo\/1",
      "indices" : [ 70, 93 ],
      "url" : "https:\/\/t.co\/YS3DyUzerq",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXSaCyZXUAElZlT.jpg",
      "id_str" : "969569192470073345",
      "id" : 969569192470073345,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXSaCyZXUAElZlT.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 726,
        "resize" : "fit",
        "w" : 1920
      }, {
        "h" : 454,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 726,
        "resize" : "fit",
        "w" : 1920
      }, {
        "h" : 257,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/YS3DyUzerq"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969932155001495552",
  "text" : "RT @cliffski: Silicon valley is the best TV show ever made. Evidence: https:\/\/t.co\/YS3DyUzerq",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/cliffski\/status\/969569229149212672\/photo\/1",
        "indices" : [ 56, 79 ],
        "url" : "https:\/\/t.co\/YS3DyUzerq",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXSaCyZXUAElZlT.jpg",
        "id_str" : "969569192470073345",
        "id" : 969569192470073345,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXSaCyZXUAElZlT.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 726,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 454,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 726,
          "resize" : "fit",
          "w" : 1920
        }, {
          "h" : 257,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/YS3DyUzerq"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969569229149212672",
    "text" : "Silicon valley is the best TV show ever made. Evidence: https:\/\/t.co\/YS3DyUzerq",
    "id" : 969569229149212672,
    "created_at" : "2018-03-02 13:44:52 +0000",
    "user" : {
      "name" : "cliffski",
      "screen_name" : "cliffski",
      "protected" : false,
      "id_str" : "15218212",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/666973209607200768\/bHnDxcNX_normal.jpg",
      "id" : 15218212,
      "verified" : false
    }
  },
  "id" : 969932155001495552,
  "created_at" : "2018-03-03 13:47:00 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Phil Bonner",
      "screen_name" : "Phil_Bonner",
      "indices" : [ 3, 15 ],
      "id_str" : "1000189620",
      "id" : 1000189620
    }, {
      "name" : "Irish Air Corps",
      "screen_name" : "IrishAirCorps",
      "indices" : [ 32, 46 ],
      "id_str" : "171238684",
      "id" : 171238684
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "No3Ops",
      "indices" : [ 82, 89 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969929166287228928",
  "text" : "RT @Phil_Bonner: Teams from the @IrishAirCorps working around the clock to ensure #No3Ops Wing standby crews can get our Helicopters out of\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Irish Air Corps",
        "screen_name" : "IrishAirCorps",
        "indices" : [ 15, 29 ],
        "id_str" : "171238684",
        "id" : 171238684
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/Phil_Bonner\/status\/969613735001313280\/photo\/1",
        "indices" : [ 177, 200 ],
        "url" : "https:\/\/t.co\/lxKrWRpXJX",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXTCi4EX0AQdnlz.jpg",
        "id_str" : "969613724213563396",
        "id" : 969613724213563396,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXTCi4EX0AQdnlz.jpg",
        "sizes" : [ {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 576,
          "resize" : "fit",
          "w" : 1024
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/lxKrWRpXJX"
      } ],
      "hashtags" : [ {
        "text" : "No3Ops",
        "indices" : [ 65, 72 ]
      }, {
        "text" : "BeastFromTheEast",
        "indices" : [ 137, 154 ]
      }, {
        "text" : "sneachta",
        "indices" : [ 155, 164 ]
      }, {
        "text" : "resilience",
        "indices" : [ 165, 176 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969613735001313280",
    "text" : "Teams from the @IrishAirCorps working around the clock to ensure #No3Ops Wing standby crews can get our Helicopters out of their igloos. #BeastFromTheEast #sneachta #resilience https:\/\/t.co\/lxKrWRpXJX",
    "id" : 969613735001313280,
    "created_at" : "2018-03-02 16:41:43 +0000",
    "user" : {
      "name" : "Phil Bonner",
      "screen_name" : "Phil_Bonner",
      "protected" : false,
      "id_str" : "1000189620",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3274259133\/f122a685bd0869b02bffaf66fd305d49_normal.jpeg",
      "id" : 1000189620,
      "verified" : false
    }
  },
  "id" : 969929166287228928,
  "created_at" : "2018-03-03 13:35:08 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "indices" : [ 3, 16 ],
      "id_str" : "36889519",
      "id" : 36889519
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/969916356090040322\/photo\/1",
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/gEMUBkkt36",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXXVyP6X0AAX0iO.jpg",
      "id_str" : "969916354009747456",
      "id" : 969916354009747456,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXXVyP6X0AAX0iO.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 576
      }, {
        "h" : 768,
        "resize" : "fit",
        "w" : 576
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/gEMUBkkt36"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969925143454797824",
  "text" : "RT @RaminNasibov: Ice skating on the canals of Amsterdam. https:\/\/t.co\/gEMUBkkt36",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/buffer.com\" rel=\"nofollow\"\u003EBuffer\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/RaminNasibov\/status\/969916356090040322\/photo\/1",
        "indices" : [ 40, 63 ],
        "url" : "https:\/\/t.co\/gEMUBkkt36",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXXVyP6X0AAX0iO.jpg",
        "id_str" : "969916354009747456",
        "id" : 969916354009747456,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXXVyP6X0AAX0iO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 510
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 576
        }, {
          "h" : 768,
          "resize" : "fit",
          "w" : 576
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/gEMUBkkt36"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969916356090040322",
    "text" : "Ice skating on the canals of Amsterdam. https:\/\/t.co\/gEMUBkkt36",
    "id" : 969916356090040322,
    "created_at" : "2018-03-03 12:44:14 +0000",
    "user" : {
      "name" : "R\u039BMIN N\u039BSIBOV",
      "screen_name" : "RaminNasibov",
      "protected" : false,
      "id_str" : "36889519",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/896399812450557953\/gP3PRbXP_normal.jpg",
      "id" : 36889519,
      "verified" : true
    }
  },
  "id" : 969925143454797824,
  "created_at" : "2018-03-03 13:19:09 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "I Am Devloper",
      "screen_name" : "iamdevloper",
      "indices" : [ 3, 15 ],
      "id_str" : "564919357",
      "id" : 564919357
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969910968833794048",
  "text" : "RT @iamdevloper: having your president on twitter is like your manager who \"used to code\" make some last minute production changes to a cli\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969556359753576448",
    "text" : "having your president on twitter is like your manager who \"used to code\" make some last minute production changes to a client project\n\n\uD83E\uDD26\u200D\u2642\uFE0F",
    "id" : 969556359753576448,
    "created_at" : "2018-03-02 12:53:44 +0000",
    "user" : {
      "name" : "I Am Devloper",
      "screen_name" : "iamdevloper",
      "protected" : false,
      "id_str" : "564919357",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/477397164453527552\/uh2w1u1o_normal.jpeg",
      "id" : 564919357,
      "verified" : false
    }
  },
  "id" : 969910968833794048,
  "created_at" : "2018-03-03 12:22:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dublin12",
      "indices" : [ 122, 131 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969901376682840066",
  "text" : "Just back from the local Aldi Store, Long Mile Road. Parking space might be limited as they are still clearing the snow.  #Dublin12",
  "id" : 969901376682840066,
  "created_at" : "2018-03-03 11:44:42 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 123, 146 ],
      "url" : "https:\/\/t.co\/TrfWgH9YEo",
      "expanded_url" : "https:\/\/twitter.com\/heliumlife\/status\/969900287149326336",
      "display_url" : "twitter.com\/heliumlife\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969900904139902976",
  "text" : "\"The downward slope of the forgetting curve can be softened by repeating the learned information at particular intervals.\" https:\/\/t.co\/TrfWgH9YEo",
  "id" : 969900904139902976,
  "created_at" : "2018-03-03 11:42:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969872931751911424\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/cUysyiuXkd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXWuRUZX4AMuH0L.jpg",
      "id_str" : "969872907324350467",
      "id" : 969872907324350467,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXWuRUZX4AMuH0L.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cUysyiuXkd"
    }, {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969872931751911424\/photo\/1",
      "indices" : [ 0, 23 ],
      "url" : "https:\/\/t.co\/cUysyiuXkd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXWuRUiW4AA77eT.jpg",
      "id_str" : "969872907362033664",
      "id" : 969872907362033664,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXWuRUiW4AA77eT.jpg",
      "sizes" : [ {
        "h" : 510,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1536,
        "resize" : "fit",
        "w" : 2048
      }, {
        "h" : 2976,
        "resize" : "fit",
        "w" : 3968
      }, {
        "h" : 900,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/cUysyiuXkd"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "969872664536993792",
  "geo" : { },
  "id_str" : "969872931751911424",
  "in_reply_to_user_id" : 9465632,
  "text" : "https:\/\/t.co\/cUysyiuXkd",
  "id" : 969872931751911424,
  "in_reply_to_status_id" : 969872664536993792,
  "created_at" : "2018-03-03 09:51:40 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Dublin12",
      "indices" : [ 91, 100 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969872664536993792",
  "text" : "Can confirm that both Aldi and Lidl are not open yet along Long Mile Road and Walkinstown. #Dublin12",
  "id" : 969872664536993792,
  "created_at" : "2018-03-03 09:50:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 0, 15 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "969862448365932545",
  "geo" : { },
  "id_str" : "969872093004357632",
  "in_reply_to_user_id" : 22997944,
  "text" : "@paulinesargent Nurses after shift trying to dig themselves out from the hospital car park to get to their car to go home - my sources told me.",
  "id" : 969872093004357632,
  "in_reply_to_status_id" : 969862448365932545,
  "created_at" : "2018-03-03 09:48:20 +0000",
  "in_reply_to_screen_name" : "paulinesargent",
  "in_reply_to_user_id_str" : "22997944",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969871404610084864\/photo\/1",
      "indices" : [ 81, 104 ],
      "url" : "https:\/\/t.co\/oCQJ53qiuk",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXWs4uRXkAEvwth.jpg",
      "id_str" : "969871385261740033",
      "id" : 969871385261740033,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXWs4uRXkAEvwth.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 510
      }, {
        "h" : 2048,
        "resize" : "fit",
        "w" : 1536
      }, {
        "h" : 3968,
        "resize" : "fit",
        "w" : 2976
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 900
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oCQJ53qiuk"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969871404610084864",
  "text" : "Supply line stuck just shy few metres away from a local supermarket loading bay. https:\/\/t.co\/oCQJ53qiuk",
  "id" : 969871404610084864,
  "created_at" : "2018-03-03 09:45:36 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "indices" : [ 3, 18 ],
      "id_str" : "22997944",
      "id" : 22997944
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/969862448365932545\/photo\/1",
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/rYFdNWiyPT",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXWkwLWXcAIuq87.jpg",
      "id_str" : "969862442355486722",
      "id" : 969862442355486722,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXWkwLWXcAIuq87.jpg",
      "sizes" : [ {
        "h" : 804,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 570,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 804,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 804,
        "resize" : "fit",
        "w" : 960
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/rYFdNWiyPT"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969871023670812673",
  "text" : "RT @paulinesargent: From the Crumlin Facebook page. Can anyone help? https:\/\/t.co\/rYFdNWiyPT",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/paulinesargent\/status\/969862448365932545\/photo\/1",
        "indices" : [ 49, 72 ],
        "url" : "https:\/\/t.co\/rYFdNWiyPT",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXWkwLWXcAIuq87.jpg",
        "id_str" : "969862442355486722",
        "id" : 969862442355486722,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXWkwLWXcAIuq87.jpg",
        "sizes" : [ {
          "h" : 804,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 570,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 804,
          "resize" : "fit",
          "w" : 960
        }, {
          "h" : 804,
          "resize" : "fit",
          "w" : 960
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/rYFdNWiyPT"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969862448365932545",
    "text" : "From the Crumlin Facebook page. Can anyone help? https:\/\/t.co\/rYFdNWiyPT",
    "id" : 969862448365932545,
    "created_at" : "2018-03-03 09:10:01 +0000",
    "user" : {
      "name" : "Pauline Sargent",
      "screen_name" : "paulinesargent",
      "protected" : false,
      "id_str" : "22997944",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1002264625130409984\/wlpdomJK_normal.jpg",
      "id" : 22997944,
      "verified" : false
    }
  },
  "id" : 969871023670812673,
  "created_at" : "2018-03-03 09:44:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 48, 71 ],
      "url" : "https:\/\/t.co\/dwoVxahvFf",
      "expanded_url" : "https:\/\/www.hackterms.com\/docker",
      "display_url" : "hackterms.com\/docker"
    } ]
  },
  "geo" : { },
  "id_str" : "969838911555035142",
  "text" : "The definition of Docker is exactly what it is. https:\/\/t.co\/dwoVxahvFf",
  "id" : 969838911555035142,
  "created_at" : "2018-03-03 07:36:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CNET",
      "screen_name" : "CNET",
      "indices" : [ 90, 95 ],
      "id_str" : "30261067",
      "id" : 30261067
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/Nxgezn6wwo",
      "expanded_url" : "http:\/\/cnet.co\/2F8l2dr",
      "display_url" : "cnet.co\/2F8l2dr"
    } ]
  },
  "geo" : { },
  "id_str" : "969838446561976321",
  "text" : "Creative's new Super X-Fi audio tech is frigging mind-blowing https:\/\/t.co\/Nxgezn6wwo via @CNET",
  "id" : 969838446561976321,
  "created_at" : "2018-03-03 07:34:39 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "indices" : [ 3, 18 ],
      "id_str" : "18319658",
      "id" : 18319658
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 80, 103 ],
      "url" : "https:\/\/t.co\/JkOuBtzoMB",
      "expanded_url" : "https:\/\/twitter.com\/wsj\/status\/969586383257030657",
      "display_url" : "twitter.com\/wsj\/status\/969\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969834916992245760",
  "text" : "RT @EmeraldDeLeeuw: When we say equality, this is exactly what we don\u2019t want. \uD83D\uDE44 https:\/\/t.co\/JkOuBtzoMB",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 60, 83 ],
        "url" : "https:\/\/t.co\/JkOuBtzoMB",
        "expanded_url" : "https:\/\/twitter.com\/wsj\/status\/969586383257030657",
        "display_url" : "twitter.com\/wsj\/status\/969\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969773069962305537",
    "text" : "When we say equality, this is exactly what we don\u2019t want. \uD83D\uDE44 https:\/\/t.co\/JkOuBtzoMB",
    "id" : 969773069962305537,
    "created_at" : "2018-03-03 03:14:52 +0000",
    "user" : {
      "name" : "Emerald \u2764\uFE0F Privacy",
      "screen_name" : "EmeraldDeLeeuw",
      "protected" : false,
      "id_str" : "18319658",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1005517888537677824\/WvtVRv7V_normal.jpg",
      "id" : 18319658,
      "verified" : false
    }
  },
  "id" : 969834916992245760,
  "created_at" : "2018-03-03 07:20:37 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Lidl",
      "indices" : [ 162, 167 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969833606775541760",
  "text" : "There is always a justification for such wonton destruction of properties under the guises of great civil disobedience movement against the excess of capitalism. #Lidl",
  "id" : 969833606775541760,
  "created_at" : "2018-03-03 07:15:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "indices" : [ 3, 17 ],
      "id_str" : "150246405",
      "id" : 150246405
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969701257882619904",
  "text" : "RT @thejournal_ie: There is no fire at the Lidl store on Fortunestown Road. The photograph being shared online of a blaze at a supermarket\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/sproutsocial.com\" rel=\"nofollow\"\u003ESprout Social\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 207, 230 ],
        "url" : "https:\/\/t.co\/S4mxZbSfdM",
        "expanded_url" : "http:\/\/jrnl.ie\/3882869",
        "display_url" : "jrnl.ie\/3882869"
      }, {
        "indices" : [ 231, 254 ],
        "url" : "https:\/\/t.co\/0FZElqMRSG",
        "expanded_url" : "https:\/\/twitter.com\/DubFireBrigade\/status\/969684478137380865",
        "display_url" : "twitter.com\/DubFireBrigade\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969687211930259457",
    "text" : "There is no fire at the Lidl store on Fortunestown Road. The photograph being shared online of a blaze at a supermarket is NOT of the Dublin shop. Garda\u00ED are at the scene of the Citywest incident. More here https:\/\/t.co\/S4mxZbSfdM https:\/\/t.co\/0FZElqMRSG",
    "id" : 969687211930259457,
    "created_at" : "2018-03-02 21:33:41 +0000",
    "user" : {
      "name" : "TheJournal.ie",
      "screen_name" : "thejournal_ie",
      "protected" : false,
      "id_str" : "150246405",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/948950612800139264\/bebzFmrQ_normal.jpg",
      "id" : 150246405,
      "verified" : true
    }
  },
  "id" : 969701257882619904,
  "created_at" : "2018-03-02 22:29:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "Lidl",
      "indices" : [ 16, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969684491261313024",
  "text" : "RT @logitechxs: #Lidl City West Dublin has been looted and severly damaged. Videos appearing online of a stolen digger smashing the roof of\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "Lidl",
        "indices" : [ 0, 5 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969682597289787397",
    "text" : "#Lidl City West Dublin has been looted and severly damaged. Videos appearing online of a stolen digger smashing the roof of the building and collapsing it. Disgusting and so dissapointing to see this in our community.",
    "id" : 969682597289787397,
    "created_at" : "2018-03-02 21:15:21 +0000",
    "user" : {
      "name" : "Adam",
      "screen_name" : "adimacc",
      "protected" : false,
      "id_str" : "22359346",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1008411608538910725\/42_TGe6S_normal.jpg",
      "id" : 22359346,
      "verified" : false
    }
  },
  "id" : 969684491261313024,
  "created_at" : "2018-03-02 21:22:53 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 101, 124 ],
      "url" : "https:\/\/t.co\/EnyJ9Vnh3y",
      "expanded_url" : "http:\/\/cnb.cx\/2FFZZvK",
      "display_url" : "cnb.cx\/2FFZZvK"
    } ]
  },
  "geo" : { },
  "id_str" : "969648407366299648",
  "text" : "Alibaba just set up its first joint research center outside China to explore artificial intelligence https:\/\/t.co\/EnyJ9Vnh3y",
  "id" : 969648407366299648,
  "created_at" : "2018-03-02 18:59:30 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "indices" : [ 3, 16 ],
      "id_str" : "49413866",
      "id" : 49413866
    }, {
      "name" : "Urban Dictionary",
      "screen_name" : "urbandictionary",
      "indices" : [ 46, 62 ],
      "id_str" : "15518000",
      "id" : 15518000
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/969620497422938112\/photo\/1",
      "indices" : [ 103, 126 ],
      "url" : "https:\/\/t.co\/Zc96kdMnOV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXTIl5rUQAIDmBW.jpg",
      "id_str" : "969620373254717442",
      "id" : 969620373254717442,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXTIl5rUQAIDmBW.jpg",
      "sizes" : [ {
        "h" : 812,
        "resize" : "fit",
        "w" : 974
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 812,
        "resize" : "fit",
        "w" : 974
      }, {
        "h" : 567,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 812,
        "resize" : "fit",
        "w" : 974
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Zc96kdMnOV"
    } ],
    "hashtags" : [ {
      "text" : "programming",
      "indices" : [ 76, 88 ]
    }, {
      "text" : "tech",
      "indices" : [ 89, 94 ]
    }, {
      "text" : "webdev",
      "indices" : [ 95, 102 ]
    } ],
    "urls" : [ {
      "indices" : [ 18, 41 ],
      "url" : "https:\/\/t.co\/RF9fPUOlDu",
      "expanded_url" : "http:\/\/hackterms.com",
      "display_url" : "hackterms.com"
    } ]
  },
  "geo" : { },
  "id_str" : "969629213690130434",
  "text" : "RT @randal_olson: https:\/\/t.co\/RF9fPUOlDu: An @urbandictionary for techies. #programming #tech #webdev https:\/\/t.co\/Zc96kdMnOV",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Urban Dictionary",
        "screen_name" : "urbandictionary",
        "indices" : [ 28, 44 ],
        "id_str" : "15518000",
        "id" : 15518000
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/randal_olson\/status\/969620497422938112\/photo\/1",
        "indices" : [ 85, 108 ],
        "url" : "https:\/\/t.co\/Zc96kdMnOV",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXTIl5rUQAIDmBW.jpg",
        "id_str" : "969620373254717442",
        "id" : 969620373254717442,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXTIl5rUQAIDmBW.jpg",
        "sizes" : [ {
          "h" : 812,
          "resize" : "fit",
          "w" : 974
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 812,
          "resize" : "fit",
          "w" : 974
        }, {
          "h" : 567,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 812,
          "resize" : "fit",
          "w" : 974
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/Zc96kdMnOV"
      } ],
      "hashtags" : [ {
        "text" : "programming",
        "indices" : [ 58, 70 ]
      }, {
        "text" : "tech",
        "indices" : [ 71, 76 ]
      }, {
        "text" : "webdev",
        "indices" : [ 77, 84 ]
      } ],
      "urls" : [ {
        "indices" : [ 0, 23 ],
        "url" : "https:\/\/t.co\/RF9fPUOlDu",
        "expanded_url" : "http:\/\/hackterms.com",
        "display_url" : "hackterms.com"
      } ]
    },
    "geo" : { },
    "id_str" : "969620497422938112",
    "text" : "https:\/\/t.co\/RF9fPUOlDu: An @urbandictionary for techies. #programming #tech #webdev https:\/\/t.co\/Zc96kdMnOV",
    "id" : 969620497422938112,
    "created_at" : "2018-03-02 17:08:35 +0000",
    "user" : {
      "name" : "Randy Olson",
      "screen_name" : "randal_olson",
      "protected" : false,
      "id_str" : "49413866",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/977666344496676864\/IEZlOHYw_normal.jpg",
      "id" : 49413866,
      "verified" : false
    }
  },
  "id" : 969629213690130434,
  "created_at" : "2018-03-02 17:43:14 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969611750919364608",
  "text" : "RT @lisaocarroll: May's message to the EU (see photo) - We can't fix Irish border on our own. \nSources in Dublin gov saying this amounts to\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/lisaocarroll\/status\/969593042691940352\/photo\/1",
        "indices" : [ 179, 202 ],
        "url" : "https:\/\/t.co\/6LNps9a87j",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXSvqdRW0AEOjee.jpg",
        "id_str" : "969592963738292225",
        "id" : 969592963738292225,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXSvqdRW0AEOjee.jpg",
        "sizes" : [ {
          "h" : 598,
          "resize" : "fit",
          "w" : 915
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 915
        }, {
          "h" : 598,
          "resize" : "fit",
          "w" : 915
        }, {
          "h" : 444,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/6LNps9a87j"
      } ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969593042691940352",
    "text" : "May's message to the EU (see photo) - We can't fix Irish border on our own. \nSources in Dublin gov saying this amounts to request for  tri-lateral talks \nLondon, Dublin, Brussels https:\/\/t.co\/6LNps9a87j",
    "id" : 969593042691940352,
    "created_at" : "2018-03-02 15:19:30 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 969611750919364608,
  "created_at" : "2018-03-02 16:33:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Python Hub",
      "screen_name" : "PythonHub",
      "indices" : [ 3, 13 ],
      "id_str" : "574827627",
      "id" : 574827627
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969611365865451521",
  "text" : "RT @PythonHub: I made a Twitter bot that describes food images from reddit using Microsoft Cognitive Services and it's pretty accurate! htt\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 121, 144 ],
        "url" : "https:\/\/t.co\/2kVKiezeWk",
        "expanded_url" : "https:\/\/www.reddit.com\/r\/Python\/comments\/7yseey\/i_made_a_twitter_bot_that_describes_food_images\/",
        "display_url" : "reddit.com\/r\/Python\/comme\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "965785689001484288",
    "text" : "I made a Twitter bot that describes food images from reddit using Microsoft Cognitive Services and it's pretty accurate! https:\/\/t.co\/2kVKiezeWk",
    "id" : 965785689001484288,
    "created_at" : "2018-02-20 03:10:26 +0000",
    "user" : {
      "name" : "Python Hub",
      "screen_name" : "PythonHub",
      "protected" : false,
      "id_str" : "574827627",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/2213121578\/icon_normal.png",
      "id" : 574827627,
      "verified" : false
    }
  },
  "id" : 969611365865451521,
  "created_at" : "2018-03-02 16:32:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 2, 25 ],
      "url" : "https:\/\/t.co\/wzas6ONEgY",
      "expanded_url" : "https:\/\/twitter.com\/MrJonathanHayes\/status\/969610636811472897",
      "display_url" : "twitter.com\/MrJonathanHaye\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969611275297751041",
  "text" : "\uD83D\uDE01 https:\/\/t.co\/wzas6ONEgY",
  "id" : 969611275297751041,
  "created_at" : "2018-03-02 16:31:57 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "indices" : [ 3, 16 ],
      "id_str" : "25876418",
      "id" : 25876418
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969574659690782726",
  "text" : "RT @lisaocarroll: May softens position on immigration red lines \n\nUK citizens will still want to work and study in EU just as EU citizens h\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969573723727712256",
    "text" : "May softens position on immigration red lines \n\nUK citizens will still want to work and study in EU just as EU citizens here\n\nIndeed businesses must be able to attract and employ the people they need. \n\nNeed reciprocal commiement",
    "id" : 969573723727712256,
    "created_at" : "2018-03-02 14:02:44 +0000",
    "user" : {
      "name" : "lisa o'carroll",
      "screen_name" : "lisaocarroll",
      "protected" : false,
      "id_str" : "25876418",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/3561423749\/300f99f1152e297e9f2ed64c510acbe9_normal.jpeg",
      "id" : 25876418,
      "verified" : false
    }
  },
  "id" : 969574659690782726,
  "created_at" : "2018-03-02 14:06:27 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 46, 69 ],
      "url" : "https:\/\/t.co\/MvK4lCQQAL",
      "expanded_url" : "http:\/\/www.bbc.com\/news\/uk-scotland-43255368",
      "display_url" : "bbc.com\/news\/uk-scotla\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969574162732912640",
  "text" : "Near-miss bus driver: 'My training kicked in' https:\/\/t.co\/MvK4lCQQAL",
  "id" : 969574162732912640,
  "created_at" : "2018-03-02 14:04:28 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Adrian Shanahan",
      "screen_name" : "adrianshanahan",
      "indices" : [ 3, 18 ],
      "id_str" : "14775534",
      "id" : 14775534
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 100, 123 ],
      "url" : "https:\/\/t.co\/4IOTITHKUA",
      "expanded_url" : "https:\/\/twitter.com\/KildareMet\/status\/969528545721626624",
      "display_url" : "twitter.com\/KildareMet\/sta\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969564965521838080",
  "text" : "RT @adrianshanahan: And people have the neck to suggest goverment red alert warnings were overkill. https:\/\/t.co\/4IOTITHKUA",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 80, 103 ],
        "url" : "https:\/\/t.co\/4IOTITHKUA",
        "expanded_url" : "https:\/\/twitter.com\/KildareMet\/status\/969528545721626624",
        "display_url" : "twitter.com\/KildareMet\/sta\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969535111598354432",
    "text" : "And people have the neck to suggest goverment red alert warnings were overkill. https:\/\/t.co\/4IOTITHKUA",
    "id" : 969535111598354432,
    "created_at" : "2018-03-02 11:29:18 +0000",
    "user" : {
      "name" : "Adrian Shanahan",
      "screen_name" : "adrianshanahan",
      "protected" : false,
      "id_str" : "14775534",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/947818050102980610\/jFkfKXa3_normal.jpg",
      "id" : 14775534,
      "verified" : false
    }
  },
  "id" : 969564965521838080,
  "created_at" : "2018-03-02 13:27:56 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969557523501060096\/photo\/1",
      "indices" : [ 65, 88 ],
      "url" : "https:\/\/t.co\/68fIBxWiLv",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXSPbelX4AME75J.jpg",
      "id_str" : "969557522020556803",
      "id" : 969557522020556803,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXSPbelX4AME75J.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/68fIBxWiLv"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/GtoDPQahdZ",
      "expanded_url" : "http:\/\/ift.tt\/2FaJ6br",
      "display_url" : "ift.tt\/2FaJ6br"
    } ]
  },
  "geo" : { },
  "id_str" : "969557523501060096",
  "text" : "30cm snow in some part of the local area https:\/\/t.co\/GtoDPQahdZ https:\/\/t.co\/68fIBxWiLv",
  "id" : 969557523501060096,
  "created_at" : "2018-03-02 12:58:21 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/ifttt.com\" rel=\"nofollow\"\u003EIFTTT\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969552102509998081\/photo\/1",
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/nCZB59SbAp",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXSKf7YW0AAiE6Z.jpg",
      "id_str" : "969552100911927296",
      "id" : 969552100911927296,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXSKf7YW0AAiE6Z.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 640,
        "resize" : "fit",
        "w" : 640
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/nCZB59SbAp"
    } ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 26, 49 ],
      "url" : "https:\/\/t.co\/DcvCWuqk1v",
      "expanded_url" : "http:\/\/ift.tt\/2oKdgeF",
      "display_url" : "ift.tt\/2oKdgeF"
    } ]
  },
  "geo" : { },
  "id_str" : "969552102509998081",
  "text" : "18 cm snow on the 2nd day https:\/\/t.co\/DcvCWuqk1v https:\/\/t.co\/nCZB59SbAp",
  "id" : 969552102509998081,
  "created_at" : "2018-03-02 12:36:49 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "James Wootton",
      "screen_name" : "decodoku",
      "indices" : [ 3, 12 ],
      "id_str" : "4201214013",
      "id" : 4201214013
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 115, 138 ],
      "url" : "https:\/\/t.co\/zpsWsKs48d",
      "expanded_url" : "https:\/\/mybinder.org\/v2\/gh\/decodoku\/A_Game_to_Benchmark_Quantum_Computers\/master?filepath=Play_Quantum_Awesomeness.ipynb",
      "display_url" : "mybinder.org\/v2\/gh\/decodoku\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969484279506984960",
  "text" : "RT @decodoku: You can now play a game on a quantum computer from the comfort of your own browser. Just press 'run'\nhttps:\/\/t.co\/zpsWsKs48d\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/decodoku\/status\/969149730876985344\/photo\/1",
        "indices" : [ 125, 148 ],
        "url" : "https:\/\/t.co\/j05hzXQKHk",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXMcexXXUAAVyN3.jpg",
        "id_str" : "969149659787776000",
        "id" : 969149659787776000,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXMcexXXUAAVyN3.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 536,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 544,
          "resize" : "fit",
          "w" : 690
        }, {
          "h" : 544,
          "resize" : "fit",
          "w" : 690
        }, {
          "h" : 544,
          "resize" : "fit",
          "w" : 690
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/j05hzXQKHk"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 101, 124 ],
        "url" : "https:\/\/t.co\/zpsWsKs48d",
        "expanded_url" : "https:\/\/mybinder.org\/v2\/gh\/decodoku\/A_Game_to_Benchmark_Quantum_Computers\/master?filepath=Play_Quantum_Awesomeness.ipynb",
        "display_url" : "mybinder.org\/v2\/gh\/decodoku\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969149730876985344",
    "text" : "You can now play a game on a quantum computer from the comfort of your own browser. Just press 'run'\nhttps:\/\/t.co\/zpsWsKs48d https:\/\/t.co\/j05hzXQKHk",
    "id" : 969149730876985344,
    "created_at" : "2018-03-01 09:57:56 +0000",
    "user" : {
      "name" : "James Wootton",
      "screen_name" : "decodoku",
      "protected" : false,
      "id_str" : "4201214013",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/882534825994878976\/zm7UEuDM_normal.jpg",
      "id" : 4201214013,
      "verified" : false
    }
  },
  "id" : 969484279506984960,
  "created_at" : "2018-03-02 08:07:19 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Steven Dilla",
      "screen_name" : "StevenDilla",
      "indices" : [ 3, 15 ],
      "id_str" : "14263101",
      "id" : 14263101
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969323579199324161",
  "text" : "RT @StevenDilla: How do Americans feel about big tech? Most trust Amazon as much as their bank and Facebook less than Google. And most peop\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/StevenDilla\/status\/969240697135030273\/photo\/1",
        "indices" : [ 186, 209 ],
        "url" : "https:\/\/t.co\/y7ApKnNXwo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXNvQvFU0AI9qhv.jpg",
        "id_str" : "969240678122180610",
        "id" : 969240678122180610,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXNvQvFU0AI9qhv.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 593,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1125
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y7ApKnNXwo"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/StevenDilla\/status\/969240697135030273\/photo\/1",
        "indices" : [ 186, 209 ],
        "url" : "https:\/\/t.co\/y7ApKnNXwo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXNvQvQVMAAKFhQ.jpg",
        "id_str" : "969240678168342528",
        "id" : 969240678168342528,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXNvQvQVMAAKFhQ.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 593,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 981,
          "resize" : "fit",
          "w" : 1125
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y7ApKnNXwo"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/StevenDilla\/status\/969240697135030273\/photo\/1",
        "indices" : [ 186, 209 ],
        "url" : "https:\/\/t.co\/y7ApKnNXwo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXNvQvBVQAAffv_.jpg",
        "id_str" : "969240678105432064",
        "id" : 969240678105432064,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXNvQvBVQAAffv_.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 601,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 994,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 994,
          "resize" : "fit",
          "w" : 1125
        }, {
          "h" : 994,
          "resize" : "fit",
          "w" : 1125
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/y7ApKnNXwo"
      } ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 162, 185 ],
        "url" : "https:\/\/t.co\/VlpabzW5Om",
        "expanded_url" : "https:\/\/www.theverge.com\/platform\/amp\/2017\/10\/27\/16550640\/verge-tech-survey-amazon-facebook-google-twitter-popularity?__twitter_impression=true",
        "display_url" : "theverge.com\/platform\/amp\/2\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969240697135030273",
    "text" : "How do Americans feel about big tech? Most trust Amazon as much as their bank and Facebook less than Google. And most people wouldn't care if Twitter went away.\n\nhttps:\/\/t.co\/VlpabzW5Om https:\/\/t.co\/y7ApKnNXwo",
    "id" : 969240697135030273,
    "created_at" : "2018-03-01 15:59:24 +0000",
    "user" : {
      "name" : "Steven Dilla",
      "screen_name" : "StevenDilla",
      "protected" : false,
      "id_str" : "14263101",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/868879185623695360\/8SyPrhdG_normal.jpg",
      "id" : 14263101,
      "verified" : false
    }
  },
  "id" : 969323579199324161,
  "created_at" : "2018-03-01 21:28:45 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 137, 160 ],
      "url" : "https:\/\/t.co\/YR1b1Rmbx4",
      "expanded_url" : "http:\/\/www.dailymail.co.uk\/travel\/travel_news\/article-5385841\/Japanese-Singaporean-passports-worlds-powerful.html#ixzz58XDIt3QO",
      "display_url" : "dailymail.co.uk\/travel\/travel_\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969322100132589568",
  "text" : "The Singaporean and Japanese passports give their citizens the chance of visa-free travel to a total of 180 countries around the globe.  https:\/\/t.co\/YR1b1Rmbx4",
  "id" : 969322100132589568,
  "created_at" : "2018-03-01 21:22:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 3, 17 ],
      "id_str" : "179569408",
      "id" : 179569408
    }, {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "indices" : [ 53, 67 ],
      "id_str" : "179569408",
      "id" : 179569408
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/DublinAirport\/status\/969319502394613760\/video\/1",
      "indices" : [ 107, 130 ],
      "url" : "https:\/\/t.co\/oVD4d566bo",
      "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/969319412225531904\/pu\/img\/JgT0AW-DhZzPVZmO.jpg",
      "id_str" : "969319412225531904",
      "id" : 969319412225531904,
      "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/969319412225531904\/pu\/img\/JgT0AW-DhZzPVZmO.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 720
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/oVD4d566bo"
    } ],
    "hashtags" : [ {
      "text" : "stormemma",
      "indices" : [ 78, 88 ]
    }, {
      "text" : "BeastFromTheEast",
      "indices" : [ 89, 106 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969320334783545344",
  "text" : "RT @DublinAirport: Stormy conditions on the airfield @DublinAirport right now #stormemma #BeastFromTheEast https:\/\/t.co\/oVD4d566bo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Dublin Airport",
        "screen_name" : "DublinAirport",
        "indices" : [ 34, 48 ],
        "id_str" : "179569408",
        "id" : 179569408
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/DublinAirport\/status\/969319502394613760\/video\/1",
        "indices" : [ 88, 111 ],
        "url" : "https:\/\/t.co\/oVD4d566bo",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/969319412225531904\/pu\/img\/JgT0AW-DhZzPVZmO.jpg",
        "id_str" : "969319412225531904",
        "id" : 969319412225531904,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/969319412225531904\/pu\/img\/JgT0AW-DhZzPVZmO.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/oVD4d566bo"
      } ],
      "hashtags" : [ {
        "text" : "stormemma",
        "indices" : [ 59, 69 ]
      }, {
        "text" : "BeastFromTheEast",
        "indices" : [ 70, 87 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969319502394613760",
    "text" : "Stormy conditions on the airfield @DublinAirport right now #stormemma #BeastFromTheEast https:\/\/t.co\/oVD4d566bo",
    "id" : 969319502394613760,
    "created_at" : "2018-03-01 21:12:33 +0000",
    "user" : {
      "name" : "Dublin Airport",
      "screen_name" : "DublinAirport",
      "protected" : false,
      "id_str" : "179569408",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1013556776321650694\/bdnXxxoU_normal.jpg",
      "id" : 179569408,
      "verified" : true
    }
  },
  "id" : 969320334783545344,
  "created_at" : "2018-03-01 21:15:51 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969319898513100800",
  "text" : "Aware there are some nurses who walk from Rathmines to Crumlin children hospital for their shift today.",
  "id" : 969319898513100800,
  "created_at" : "2018-03-01 21:14:07 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "indices" : [ 3, 14 ],
      "id_str" : "18189672",
      "id" : 18189672
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 107 ],
      "url" : "https:\/\/t.co\/Ne6yhHoX5b",
      "expanded_url" : "https:\/\/twitter.com\/jnrbaker\/status\/969280600417980417",
      "display_url" : "twitter.com\/jnrbaker\/statu\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969319253638893568",
  "text" : "RT @ronanlyons: Dublin looks fantastic in its new white coat! Stay warm everyone... https:\/\/t.co\/Ne6yhHoX5b",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 68, 91 ],
        "url" : "https:\/\/t.co\/Ne6yhHoX5b",
        "expanded_url" : "https:\/\/twitter.com\/jnrbaker\/status\/969280600417980417",
        "display_url" : "twitter.com\/jnrbaker\/statu\u2026"
      } ]
    },
    "geo" : { },
    "id_str" : "969318248390029314",
    "text" : "Dublin looks fantastic in its new white coat! Stay warm everyone... https:\/\/t.co\/Ne6yhHoX5b",
    "id" : 969318248390029314,
    "created_at" : "2018-03-01 21:07:34 +0000",
    "user" : {
      "name" : "Ronan Lyons",
      "screen_name" : "ronanlyons",
      "protected" : false,
      "id_str" : "18189672",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/850078237384478721\/KUkeiiaN_normal.jpg",
      "id" : 18189672,
      "verified" : false
    }
  },
  "id" : 969319253638893568,
  "created_at" : "2018-03-01 21:11:33 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Mater Foundation",
      "screen_name" : "TheMaterFoundat",
      "indices" : [ 3, 19 ],
      "id_str" : "236813539",
      "id" : 236813539
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969311084816617478",
  "text" : "RT @TheMaterFoundat: Many of our amazing hospital staff are sleeping overnight in the Mater so they can care for patients tomorrow. #StormE\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Ciara Dowling",
        "screen_name" : "cirdowling",
        "indices" : [ 210, 221 ],
        "id_str" : "539487105",
        "id" : 539487105
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/TheMaterFoundat\/status\/969265998560989185\/video\/1",
        "indices" : [ 233, 256 ],
        "url" : "https:\/\/t.co\/DpiPLl9L73",
        "media_url" : "http:\/\/pbs.twimg.com\/ext_tw_video_thumb\/969262886517387265\/pu\/img\/q3FC8dgVj-isbqyE.jpg",
        "id_str" : "969262886517387265",
        "id" : 969262886517387265,
        "media_url_https" : "https:\/\/pbs.twimg.com\/ext_tw_video_thumb\/969262886517387265\/pu\/img\/q3FC8dgVj-isbqyE.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 680,
          "resize" : "fit",
          "w" : 383
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 675
        }, {
          "h" : 1280,
          "resize" : "fit",
          "w" : 720
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/DpiPLl9L73"
      } ],
      "hashtags" : [ {
        "text" : "StormEmma",
        "indices" : [ 111, 121 ]
      }, {
        "text" : "ThankYou",
        "indices" : [ 223, 232 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969265998560989185",
    "text" : "Many of our amazing hospital staff are sleeping overnight in the Mater so they can care for patients tomorrow. #StormEmma\n\nGive this as many retweets as you can before they get up in the morning\n\nVideo cred to @cirdowling\n\n#ThankYou https:\/\/t.co\/DpiPLl9L73",
    "id" : 969265998560989185,
    "created_at" : "2018-03-01 17:39:56 +0000",
    "user" : {
      "name" : "The Mater Foundation",
      "screen_name" : "TheMaterFoundat",
      "protected" : false,
      "id_str" : "236813539",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/875371828377726976\/5NK8ne3M_normal.jpg",
      "id" : 236813539,
      "verified" : false
    }
  },
  "id" : 969311084816617478,
  "created_at" : "2018-03-01 20:39:06 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "indices" : [ 3, 15 ],
      "id_str" : "85053026",
      "id" : 85053026
    }, {
      "name" : "TV3 Ireland",
      "screen_name" : "TV3Ireland",
      "indices" : [ 18, 29 ],
      "id_str" : "1035068071797501952",
      "id" : 1035068071797501952
    }, {
      "name" : "TV3 Ireland",
      "screen_name" : "TV3Ireland",
      "indices" : [ 104, 115 ],
      "id_str" : "1035068071797501952",
      "id" : 1035068071797501952
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "StormEmma",
      "indices" : [ 61, 71 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969268388634689538",
  "text" : "RT @gavindbrown: .@TV3Ireland 3News giving great coverage of #StormEmma around Ireland today. Well done @TV3Ireland on the weather special.",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/#!\/download\/ipad\" rel=\"nofollow\"\u003ETwitter for iPad\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "TV3 Ireland",
        "screen_name" : "TV3Ireland",
        "indices" : [ 1, 12 ],
        "id_str" : "1035068071797501952",
        "id" : 1035068071797501952
      }, {
        "name" : "TV3 Ireland",
        "screen_name" : "TV3Ireland",
        "indices" : [ 87, 98 ],
        "id_str" : "1035068071797501952",
        "id" : 1035068071797501952
      } ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "StormEmma",
        "indices" : [ 44, 54 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969261345865691137",
    "text" : ".@TV3Ireland 3News giving great coverage of #StormEmma around Ireland today. Well done @TV3Ireland on the weather special.",
    "id" : 969261345865691137,
    "created_at" : "2018-03-01 17:21:27 +0000",
    "user" : {
      "name" : "Gavin D. Brown",
      "screen_name" : "gavindbrown",
      "protected" : false,
      "id_str" : "85053026",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/844192952914231296\/mLoaFZ86_normal.jpg",
      "id" : 85053026,
      "verified" : false
    }
  },
  "id" : 969268388634689538,
  "created_at" : "2018-03-01 17:49:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969238324480237568",
  "text" : "Unfollow someone who just tweet every official announcement\/content of the organisation she works for. More personality please.",
  "id" : 969238324480237568,
  "created_at" : "2018-03-01 15:49:58 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Darach \u00D3",
      "screen_name" : "darachos",
      "indices" : [ 3, 12 ],
      "id_str" : "925483707032571904",
      "id" : 925483707032571904
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969217464071589889",
  "text" : "RT @darachos: The Beast from the East will only last a few days but the memory of the businesses who refused to release their staff in time\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969204743359868928",
    "text" : "The Beast from the East will only last a few days but the memory of the businesses who refused to release their staff in time to get home safely should endure.",
    "id" : 969204743359868928,
    "created_at" : "2018-03-01 13:36:32 +0000",
    "user" : {
      "name" : "Darach \u00D3",
      "screen_name" : "darachos",
      "protected" : false,
      "id_str" : "925483707032571904",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1024641034570092544\/TM5eV-lr_normal.jpg",
      "id" : 925483707032571904,
      "verified" : false
    }
  },
  "id" : 969217464071589889,
  "created_at" : "2018-03-01 14:27:05 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 32 ],
      "url" : "https:\/\/t.co\/r7XSnnuyQ2",
      "expanded_url" : "https:\/\/www.flightradar24.com\/53.43,-6.26\/14",
      "display_url" : "flightradar24.com\/53.43,-6.26\/14"
    }, {
      "indices" : [ 89, 112 ],
      "url" : "https:\/\/t.co\/Dwgi1FZCAZ",
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969214496895102976",
      "display_url" : "twitter.com\/mryap\/status\/9\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969216573075574784",
  "text" : "The link https:\/\/t.co\/r7XSnnuyQ2 and it appears they are making round along the runaway. https:\/\/t.co\/Dwgi1FZCAZ",
  "id" : 969216573075574784,
  "created_at" : "2018-03-01 14:23:32 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969214496895102976\/photo\/1",
      "indices" : [ 34, 57 ],
      "url" : "https:\/\/t.co\/eiOlexhlXX",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXNXbJ4WAAA9oML.jpg",
      "id_str" : "969214468835115008",
      "id" : 969214468835115008,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXNXbJ4WAAA9oML.jpg",
      "sizes" : [ {
        "h" : 740,
        "resize" : "fit",
        "w" : 1366
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 650,
        "resize" : "fit",
        "w" : 1200
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 740,
        "resize" : "fit",
        "w" : 1366
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/eiOlexhlXX"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969214496895102976",
  "text" : "Are those snow clearing vehicles? https:\/\/t.co\/eiOlexhlXX",
  "id" : 969214496895102976,
  "created_at" : "2018-03-01 14:15:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Brett Berson",
      "screen_name" : "brettberson",
      "indices" : [ 3, 15 ],
      "id_str" : "15366556",
      "id" : 15366556
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969184191295512578",
  "text" : "RT @brettberson: I just learned from a former longtime Amazon employee, the idea for Prime came from an IC engineer.  He wrote up a 6 page\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968882064513581056",
    "text" : "I just learned from a former longtime Amazon employee, the idea for Prime came from an IC engineer.  He wrote up a 6 page memo. He was inspired by the Costco membership model.  It was built as a test.  It's now the key pillar of Amazon.  The best ideas can come from anywhere.",
    "id" : 968882064513581056,
    "created_at" : "2018-02-28 16:14:19 +0000",
    "user" : {
      "name" : "Brett Berson",
      "screen_name" : "brettberson",
      "protected" : false,
      "id_str" : "15366556",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/943883595239661568\/HJXMCtY9_normal.jpg",
      "id" : 15366556,
      "verified" : false
    }
  },
  "id" : 969184191295512578,
  "created_at" : "2018-03-01 12:14:52 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969181065725214720\/photo\/1",
      "indices" : [ 59, 82 ],
      "url" : "https:\/\/t.co\/3fYQuXF1ic",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXM5CDCX4AETs0S.jpg",
      "id_str" : "969181052152569857",
      "id" : 969181052152569857,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXM5CDCX4AETs0S.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 368,
        "resize" : "fit",
        "w" : 680
      }, {
        "h" : 732,
        "resize" : "fit",
        "w" : 1352
      }, {
        "h" : 732,
        "resize" : "fit",
        "w" : 1352
      }, {
        "h" : 650,
        "resize" : "fit",
        "w" : 1200
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/3fYQuXF1ic"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "969178765745164288",
  "geo" : { },
  "id_str" : "969181065725214720",
  "in_reply_to_user_id" : 9465632,
  "text" : "However, I can't find this feature available on Desktop... https:\/\/t.co\/3fYQuXF1ic",
  "id" : 969181065725214720,
  "in_reply_to_status_id" : 969178765745164288,
  "created_at" : "2018-03-01 12:02:27 +0000",
  "in_reply_to_screen_name" : "mryap",
  "in_reply_to_user_id_str" : "9465632",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969178765745164288\/photo\/1",
      "indices" : [ 52, 75 ],
      "url" : "https:\/\/t.co\/S9mYMPtzNe",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXM28CAXkAA-vrX.jpg",
      "id_str" : "969178749773254656",
      "id" : 969178749773254656,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXM28CAXkAA-vrX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1200,
        "resize" : "fit",
        "w" : 675
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 383
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/S9mYMPtzNe"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969178765745164288",
  "text" : "Just spotted the new? Bookmarks feature on Twitter. https:\/\/t.co\/S9mYMPtzNe",
  "id" : 969178765745164288,
  "created_at" : "2018-03-01 11:53:18 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/mobile.twitter.com\" rel=\"nofollow\"\u003ETwitter Lite\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Sin\u00E9ad Gleeson",
      "screen_name" : "sineadgleeson",
      "indices" : [ 3, 17 ],
      "id_str" : "19706585",
      "id" : 19706585
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 84 ],
      "url" : "https:\/\/t.co\/ry4mQ6pygW",
      "expanded_url" : "http:\/\/7.am",
      "display_url" : "7.am"
    } ]
  },
  "geo" : { },
  "id_str" : "969174280662548481",
  "text" : "RT @sineadgleeson: Normal day. \n\nScene: a child\u2019s bedroom. \n\nhttps:\/\/t.co\/ry4mQ6pygW: \u201CChild! Get up! For the 10th time!\"\n8.10am: \"Where ar\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ ],
      "urls" : [ {
        "indices" : [ 42, 65 ],
        "url" : "https:\/\/t.co\/ry4mQ6pygW",
        "expanded_url" : "http:\/\/7.am",
        "display_url" : "7.am"
      } ]
    },
    "geo" : { },
    "id_str" : "968747088032026625",
    "text" : "Normal day. \n\nScene: a child\u2019s bedroom. \n\nhttps:\/\/t.co\/ry4mQ6pygW: \u201CChild! Get up! For the 10th time!\"\n8.10am: \"Where are your shoes?!\"\n8.20am: \"Why isn't your hair brushed?\"\n\nSnow day:\n7.05am Child already fully dressed and running around in the back garden.",
    "id" : 968747088032026625,
    "created_at" : "2018-02-28 07:17:58 +0000",
    "user" : {
      "name" : "Sin\u00E9ad Gleeson",
      "screen_name" : "sineadgleeson",
      "protected" : false,
      "id_str" : "19706585",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1003628689555410947\/98GqnV7W_normal.jpg",
      "id" : 19706585,
      "verified" : true
    }
  },
  "id" : 969174280662548481,
  "created_at" : "2018-03-01 11:35:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Richard Chambers",
      "screen_name" : "newschambers",
      "indices" : [ 3, 16 ],
      "id_str" : "28076811",
      "id" : 28076811
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969122296416923648",
  "text" : "RT @newschambers: This guy is walking 10km total to work in the Mater Hospital in Dublin. \u201CPatient care is the most important thing today.\u201D\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/newschambers\/status\/969105036797194241\/photo\/1",
        "indices" : [ 140, 163 ],
        "url" : "https:\/\/t.co\/WFQriueyQI",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXLz4uqXUAQVb--.jpg",
        "id_str" : "969105025762021380",
        "id" : 969105025762021380,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXLz4uqXUAQVb--.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 510,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 1536,
          "resize" : "fit",
          "w" : 2048
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1200
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/WFQriueyQI"
      } ],
      "hashtags" : [ {
        "text" : "BeastFromTheEast",
        "indices" : [ 122, 139 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "969105036797194241",
    "text" : "This guy is walking 10km total to work in the Mater Hospital in Dublin. \u201CPatient care is the most important thing today.\u201D #BeastFromTheEast https:\/\/t.co\/WFQriueyQI",
    "id" : 969105036797194241,
    "created_at" : "2018-03-01 07:00:20 +0000",
    "user" : {
      "name" : "Richard Chambers",
      "screen_name" : "newschambers",
      "protected" : false,
      "id_str" : "28076811",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1037015088610402304\/rHdLo8T2_normal.jpg",
      "id" : 28076811,
      "verified" : true
    }
  },
  "id" : 969122296416923648,
  "created_at" : "2018-03-01 08:08:55 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/mryap\/status\/969121181461893122\/photo\/1",
      "indices" : [ 72, 95 ],
      "url" : "https:\/\/t.co\/Bss4BcURmH",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXMCgXsW4AACuoX.jpg",
      "id_str" : "969121099953922048",
      "id" : 969121099953922048,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXMCgXsW4AACuoX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 288,
        "resize" : "fit",
        "w" : 220
      }, {
        "h" : 288,
        "resize" : "fit",
        "w" : 220
      }, {
        "h" : 288,
        "resize" : "fit",
        "w" : 220
      }, {
        "h" : 288,
        "resize" : "fit",
        "w" : 220
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/Bss4BcURmH"
    } ],
    "hashtags" : [ {
      "text" : "BeastfromtheEast",
      "indices" : [ 39, 56 ]
    }, {
      "text" : "breadmageddon",
      "indices" : [ 57, 71 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969121181461893122",
  "text" : "No bread? Qu'ils mangent de la brioche #BeastfromtheEast #breadmageddon https:\/\/t.co\/Bss4BcURmH",
  "id" : 969121181461893122,
  "created_at" : "2018-03-01 08:04:29 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "CrumlinShoppinCentre",
      "screen_name" : "OfficCrumlinSC",
      "indices" : [ 3, 18 ],
      "id_str" : "4803187336",
      "id" : 4803187336
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BeastfromtheEast",
      "indices" : [ 103, 120 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969120661779243008",
  "text" : "RT @OfficCrumlinSC: Please check on your elderly neighbours, they may have some bread you could steal\n\n#BeastfromtheEast",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ ],
      "hashtags" : [ {
        "text" : "BeastfromtheEast",
        "indices" : [ 83, 100 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968771475191795712",
    "text" : "Please check on your elderly neighbours, they may have some bread you could steal\n\n#BeastfromtheEast",
    "id" : 968771475191795712,
    "created_at" : "2018-02-28 08:54:53 +0000",
    "user" : {
      "name" : "CrumlinShoppinCentre",
      "screen_name" : "OfficCrumlinSC",
      "protected" : false,
      "id_str" : "4803187336",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/968098477455691778\/QpD2YcaI_normal.jpg",
      "id" : 4803187336,
      "verified" : false
    }
  },
  "id" : 969120661779243008,
  "created_at" : "2018-03-01 08:02:25 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "An Garda S\u00EDoch\u00E1na",
      "screen_name" : "GardaTraffic",
      "indices" : [ 3, 16 ],
      "id_str" : "295705264",
      "id" : 295705264
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969120410896928768",
  "text" : "RT @GardaTraffic: Garda members from Crumlin and Sundrive Rd stations helping deliver 220 meals to senior citizens in Dublin 12, unable to\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/GardaTraffic\/status\/968871347559587841\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/KdgHvsax7n",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXIe7jlWkAE3ivo.jpg",
        "id_str" : "968870878351101953",
        "id" : 968870878351101953,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXIe7jlWkAE3ivo.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KdgHvsax7n"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/GardaTraffic\/status\/968871347559587841\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/KdgHvsax7n",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXIe7jkX4AAd2K1.jpg",
        "id_str" : "968870878346993664",
        "id" : 968870878346993664,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXIe7jkX4AAd2K1.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KdgHvsax7n"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/GardaTraffic\/status\/968871347559587841\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/KdgHvsax7n",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXIe7jpWsAAw4Tm.jpg",
        "id_str" : "968870878367887360",
        "id" : 968870878367887360,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXIe7jpWsAAw4Tm.jpg",
        "sizes" : [ {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KdgHvsax7n"
      }, {
        "expanded_url" : "https:\/\/twitter.com\/GardaTraffic\/status\/968871347559587841\/photo\/1",
        "indices" : [ 272, 295 ],
        "url" : "https:\/\/t.co\/KdgHvsax7n",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXIe7jxXkAEKqAl.jpg",
        "id_str" : "968870878401499137",
        "id" : 968870878401499137,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXIe7jxXkAEKqAl.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 675,
          "resize" : "fit",
          "w" : 1200
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 900,
          "resize" : "fit",
          "w" : 1600
        }, {
          "h" : 383,
          "resize" : "fit",
          "w" : 680
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/KdgHvsax7n"
      } ],
      "hashtags" : [ {
        "text" : "WinterReady",
        "indices" : [ 259, 271 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968871347559587841",
    "text" : "Garda members from Crumlin and Sundrive Rd stations helping deliver 220 meals to senior citizens in Dublin 12, unable to attend their local centers. Remember to check in on elderly and vulnerable in your community and make sure they warm with plenty of food. #WinterReady https:\/\/t.co\/KdgHvsax7n",
    "id" : 968871347559587841,
    "created_at" : "2018-02-28 15:31:44 +0000",
    "user" : {
      "name" : "An Garda S\u00EDoch\u00E1na",
      "screen_name" : "GardaTraffic",
      "protected" : false,
      "id_str" : "295705264",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1350634007\/image_normal.jpg",
      "id" : 295705264,
      "verified" : true
    }
  },
  "id" : 969120410896928768,
  "created_at" : "2018-03-01 08:01:26 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "indices" : [ 3, 9 ],
      "id_str" : "37874853",
      "id" : 37874853
    } ],
    "media" : [ {
      "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/969118030524432384\/photo\/1",
      "indices" : [ 97, 120 ],
      "url" : "https:\/\/t.co\/sIIjHEJvYo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/DXL_tanU8AAN8KX.jpg",
      "id_str" : "969118025541545984",
      "id" : 969118025541545984,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXL_tanU8AAN8KX.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 404
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 404
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 404
      }, {
        "h" : 543,
        "resize" : "fit",
        "w" : 404
      } ],
      "media_alt" : "",
      "display_url" : "pic.twitter.com\/sIIjHEJvYo"
    } ],
    "hashtags" : [ {
      "text" : "Elephant",
      "indices" : [ 11, 20 ]
    }, {
      "text" : "Malaysia",
      "indices" : [ 55, 64 ]
    } ],
    "urls" : [ {
      "indices" : [ 73, 96 ],
      "url" : "https:\/\/t.co\/00UfA3krQB",
      "expanded_url" : "http:\/\/str.sg\/oSZn",
      "display_url" : "str.sg\/oSZn"
    } ]
  },
  "geo" : { },
  "id_str" : "969119592441372674",
  "text" : "RT @STcom: #Elephant strolls through school canteen in #Malaysia's Sabah https:\/\/t.co\/00UfA3krQB https:\/\/t.co\/sIIjHEJvYo",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"https:\/\/about.twitter.com\/products\/tweetdeck\" rel=\"nofollow\"\u003ETweetDeck\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/STcom\/status\/969118030524432384\/photo\/1",
        "indices" : [ 86, 109 ],
        "url" : "https:\/\/t.co\/sIIjHEJvYo",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXL_tanU8AAN8KX.jpg",
        "id_str" : "969118025541545984",
        "id" : 969118025541545984,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXL_tanU8AAN8KX.jpg",
        "sizes" : [ {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 543,
          "resize" : "fit",
          "w" : 404
        }, {
          "h" : 543,
          "resize" : "fit",
          "w" : 404
        }, {
          "h" : 543,
          "resize" : "fit",
          "w" : 404
        }, {
          "h" : 543,
          "resize" : "fit",
          "w" : 404
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/sIIjHEJvYo"
      } ],
      "hashtags" : [ {
        "text" : "Elephant",
        "indices" : [ 0, 9 ]
      }, {
        "text" : "Malaysia",
        "indices" : [ 44, 53 ]
      } ],
      "urls" : [ {
        "indices" : [ 62, 85 ],
        "url" : "https:\/\/t.co\/00UfA3krQB",
        "expanded_url" : "http:\/\/str.sg\/oSZn",
        "display_url" : "str.sg\/oSZn"
      } ]
    },
    "geo" : { },
    "id_str" : "969118030524432384",
    "text" : "#Elephant strolls through school canteen in #Malaysia's Sabah https:\/\/t.co\/00UfA3krQB https:\/\/t.co\/sIIjHEJvYo",
    "id" : 969118030524432384,
    "created_at" : "2018-03-01 07:51:58 +0000",
    "user" : {
      "name" : "The Straits Times",
      "screen_name" : "STcom",
      "protected" : false,
      "id_str" : "37874853",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/630988935720648704\/HkmsHBTM_normal.jpg",
      "id" : 37874853,
      "verified" : true
    }
  },
  "id" : 969119592441372674,
  "created_at" : "2018-03-01 07:58:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Dr. Phil O'Halloran",
      "screen_name" : "philohalloran",
      "indices" : [ 3, 17 ],
      "id_str" : "511572039",
      "id" : 511572039
    }, {
      "name" : "Beaumont Hospital",
      "screen_name" : "Beaumont_Dublin",
      "indices" : [ 76, 92 ],
      "id_str" : "919701471935033351",
      "id" : 919701471935033351
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969115340218224640",
  "text" : "RT @philohalloran: Full compliment of Neurosurgery docs at morning handover @Beaumont_Dublin. Busy TRAUMA night!!PLEASE be careful &amp; stay o\u2026",
  "retweeted_status" : {
    "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/iphone\" rel=\"nofollow\"\u003ETwitter for iPhone\u003C\/a\u003E",
    "entities" : {
      "user_mentions" : [ {
        "name" : "Beaumont Hospital",
        "screen_name" : "Beaumont_Dublin",
        "indices" : [ 57, 73 ],
        "id_str" : "919701471935033351",
        "id" : 919701471935033351
      } ],
      "media" : [ {
        "expanded_url" : "https:\/\/twitter.com\/philohalloran\/status\/968757689923899393\/photo\/1",
        "indices" : [ 167, 190 ],
        "url" : "https:\/\/t.co\/aUcJNYe3c6",
        "media_url" : "http:\/\/pbs.twimg.com\/media\/DXG3-VLW4AA9kKv.jpg",
        "id_str" : "968757676325920768",
        "id" : 968757676325920768,
        "media_url_https" : "https:\/\/pbs.twimg.com\/media\/DXG3-VLW4AA9kKv.jpg",
        "sizes" : [ {
          "h" : 680,
          "resize" : "fit",
          "w" : 584
        }, {
          "h" : 150,
          "resize" : "crop",
          "w" : 150
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1760
        }, {
          "h" : 2048,
          "resize" : "fit",
          "w" : 1760
        }, {
          "h" : 1200,
          "resize" : "fit",
          "w" : 1031
        } ],
        "media_alt" : "",
        "display_url" : "pic.twitter.com\/aUcJNYe3c6"
      } ],
      "hashtags" : [ {
        "text" : "sneachta",
        "indices" : [ 140, 149 ]
      }, {
        "text" : "beastfromtheast",
        "indices" : [ 150, 166 ]
      } ],
      "urls" : [ ]
    },
    "geo" : { },
    "id_str" : "968757689923899393",
    "text" : "Full compliment of Neurosurgery docs at morning handover @Beaumont_Dublin. Busy TRAUMA night!!PLEASE be careful &amp; stay out of harms way #sneachta #beastfromtheast https:\/\/t.co\/aUcJNYe3c6",
    "id" : 968757689923899393,
    "created_at" : "2018-02-28 08:00:06 +0000",
    "user" : {
      "name" : "Dr. Phil O'Halloran",
      "screen_name" : "philohalloran",
      "protected" : false,
      "id_str" : "511572039",
      "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/928702488362016771\/DV47E2vR_normal.jpg",
      "id" : 511572039,
      "verified" : false
    }
  },
  "id" : 969115340218224640,
  "created_at" : "2018-03-01 07:41:17 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969115179924512768",
  "text" : "\"On-demand Ironing Service.\" You really can't iron your own clothes?",
  "id" : 969115179924512768,
  "created_at" : "2018-03-01 07:40:38 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "BeastFromTheEast",
      "indices" : [ 152, 169 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "969110631302533126",
  "text" : "Thinking of those Healthcare professional living outside County Dublin heading to work in Dublin hospital for duty in this morning blizzard conditions. #BeastFromTheEast",
  "id" : 969110631302533126,
  "created_at" : "2018-03-01 07:22:34 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 226, 249 ],
      "url" : "https:\/\/t.co\/CFfKkW29Z3",
      "expanded_url" : "http:\/\/time.com\/5168048\/norway-olympics-medals-winter-games-skiiing\/",
      "display_url" : "time.com\/5168048\/norway\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969109441709858822",
  "text" : "Norway's athletes - some of whom work as plumbers, carpenters and teachers - do not receive prize money or bonuses from their federations for winning medals. Instead they are given commemorative gold-coloured shoes. And cake. https:\/\/t.co\/CFfKkW29Z3",
  "id" : 969109441709858822,
  "created_at" : "2018-03-01 07:17:50 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 83, 106 ],
      "url" : "https:\/\/t.co\/bX9d9utKrj",
      "expanded_url" : "http:\/\/www.bbc.co.uk\/sport\/rugby-union\/43235314",
      "display_url" : "bbc.co.uk\/sport\/rugby-un\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "969103987701149701",
  "text" : "Eddie Jones says he was 'physically and verbally abused' after Calcutta Cup defeat https:\/\/t.co\/bX9d9utKrj",
  "id" : 969103987701149701,
  "created_at" : "2018-03-01 06:56:10 +0000",
  "user" : {
    "name" : "hello!",
    "screen_name" : "mryap",
    "protected" : false,
    "id_str" : "9465632",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/1001768944603156481\/OfbQcV3H_normal.jpg",
    "id" : 9465632,
    "verified" : false
  }
} ]